import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ToastModule } from 'primeng/toast';
import { PaginatorModule } from 'primeng/paginator';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { MaterialModule } from '../material/material.module';
import { SharedModule } from '@shared';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';
import { BandejaLibroOperacionesComponent } from 'src/app/web/aprovechamiento/bandeja-libro-operaciones/bandeja-libro-operaciones.component';
import { ModalSolicitarNumeroLoComponent } from 'src/app/web/aprovechamiento/components/modal-solicitar-numero-lo/modal-solicitar-numero-lo.component';
import { EvaluacionLibroOperacionesComponent } from 'src/app/web/aprovechamiento/evaluacion-libro-operaciones/evaluacion-libro-operaciones.component';
import { ModalAddPlanesLoComponent } from 'src/app/web/aprovechamiento/components/modal-add-planes-lo/modal-add-planes-lo.component';


@NgModule({
  declarations: [
    BandejaLibroOperacionesComponent,
    EvaluacionLibroOperacionesComponent,
    //Componentes
    ModalSolicitarNumeroLoComponent,
    ModalAddPlanesLoComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    // PrimeNG modules
    ConfirmDialogModule,
    TableModule,
    CardModule,
    AccordionModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    RadioButtonModule,
    TabViewModule,
    ButtonModule,
    ConfirmPopupModule,
    ToastModule,
    PaginatorModule,
    InputTextareaModule,
    FieldsetModule,
    // Modulos material angular
    MaterialModule,

    SharedModule,
    CargaArchivoGeneralModule,
  ]
})
export class BandejaLibroOperacionesModule { }
