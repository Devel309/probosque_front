import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';
import { SharedModule } from '@shared';
import { MaterialModule } from '../material/material.module';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PaginatorModule } from 'primeng/paginator';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';
import { CardModule } from 'primeng/card';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FormsModule } from '@angular/forms';
import { SincronizacionComponent } from 'src/app/web/aprovechamiento/sincronizacion/sincronizacion.component';
import { DetalleSincronizacionComponent } from 'src/app/web/aprovechamiento/sincronizacion/detalle-sincronizacion/detalle-sincronizacion.component';
import { TabTaladoComponent } from 'src/app/web/aprovechamiento/sincronizacion/tabs/tab-talado/tab-talado.component';
import { TabTrozadoComponent } from 'src/app/web/aprovechamiento/sincronizacion/tabs/tab-trozado/tab-trozado.component';
import { TabTransformacionComponent } from 'src/app/web/aprovechamiento/sincronizacion/tabs/tab-transformacion/tab-transformacion.component';
import { TabDespachoComponent } from 'src/app/web/aprovechamiento/sincronizacion/tabs/tab-despacho/tab-despacho.component';
import { ModalDetalleTaladoComponent } from 'src/app/web/aprovechamiento/components/modal-detalle-talado/modal-detalle-talado.component';
import { ModalDetalleTrozadoComponent } from 'src/app/web/aprovechamiento/components/modal-detalle-trozado/modal-detalle-trozado.component';
import { ModalDetalleTransformacionComponent } from 'src/app/web/aprovechamiento/components/modal-detalle-transformacion/modal-detalle-transformacion.component';
import { ModalNuevaSincronizacionComponent } from 'src/app/web/aprovechamiento/components/modal-nueva-sincronizacion/modal-nueva-sincronizacion.component';
import { ModalDespachoTrozaComponent } from 'src/app/web/aprovechamiento/components/modal-despacho-troza/modal-despacho-troza.component';
import { ModalDespachoProductoComponent } from 'src/app/web/aprovechamiento/components/modal-despacho-producto/modal-despacho-producto.component';
import { TabGuiaForestalComponent } from 'src/app/web/aprovechamiento/sincronizacion/tabs/tab-guiaForestal/tab-guiaForestal.component';



@NgModule({
  declarations: [
    SincronizacionComponent,
    DetalleSincronizacionComponent,
    //tabs
    TabTaladoComponent,
    TabTrozadoComponent,
    TabTransformacionComponent,
    TabDespachoComponent,
    TabGuiaForestalComponent,
    //Components
    ModalDetalleTaladoComponent,
    ModalDetalleTrozadoComponent,
    ModalDetalleTransformacionComponent,
    ModalNuevaSincronizacionComponent,
    ModalDespachoTrozaComponent,
    ModalDespachoProductoComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    // PrimeNG modules
    ConfirmDialogModule,
    TableModule,
    CardModule,
    AccordionModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    RadioButtonModule,
    TabViewModule,
    ButtonModule,
    ConfirmPopupModule,
    ToastModule,
    PaginatorModule,
    InputTextareaModule,
    FieldsetModule,
    // Modulos material angular
    MaterialModule,

    SharedModule,
    CargaArchivoGeneralModule,
  ]
})
export class SincronizacionModule { }
