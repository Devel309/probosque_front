import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEstablecimientoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/bandeja-establecimiento-bl.component';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';
import { SharedModule } from '@shared';
import { MaterialModule } from '../material/material.module';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ToastModule } from 'primeng/toast';
import { PaginatorModule } from 'primeng/paginator';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RegistroEstablecimientoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/registro-establecimiento-bl/registro-establecimiento-bl.component';
import { NuevaIniciativaBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/nueva-iniciativa-bl/nueva-iniciativa-bl.component';
import { EvaluacionBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/evaluacion-bl/evaluacion-bl.component';
import { ModalBeneficiarioBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/components/modal-beneficiario-bl/modal-beneficiario-bl.component';
import { EstudioTecnicoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/estudio-tecnico-bl/estudio-tecnico-bl.component';
import { RegistroComiteTecnicoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/registro-comite-tecnico-bl/registro-comite-tecnico-bl.component';
import { EvaluacionComiteTecnicoComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/evaluacion-comite-tecnico/evaluacion-comite-tecnico.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EvaluarComiteTecnicoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/components/evaluar-comite-tecnico-bl/evaluar-comite-tecnico-bl.component';
import { EvaluacionCampoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/evaluacion-campo-bl/evaluacion-campo-bl.component';
import { ResultadoEvaluacionBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/resultado-evaluacion-bl/resultado-evaluacion-bl.component';
import { ModalDocumentosEvalCampoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/components/modal-documentos-eval-campo-bl/modal-documentos-eval-campo-bl.component';
import { InformeSaneamientoLegalComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/estudio-tecnico-bl/componentes/informe-saneamiento-legal/informe-saneamiento-legal.component';
import { BandejaEvaluacionComiteBlComponent } from 'src/app/web/bosque-local/bandeja-evaluacion-comite-bl/bandeja-evaluacion-comite-bl.component';
import { EnvioResultadosBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/envio-resultados-bl/envio-resultados-bl.component';
import { EstablecimientoBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/tabs/establecimiento-bl/establecimiento-bl.component';
import { ExpedienteEvaluacionBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/components/expediente-evaluacion-bl/expediente-evaluacion-bl.component';
import { ModalExpedienteCompletoEvalBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/components/modal-expediente-completo-eval-bl/modal-expediente-completo-eval-bl.component';
import { ModalInfoEspecialBlComponent } from 'src/app/web/bosque-local/bandeja-establecimiento-bl/components/modal-info-especial-bl/modal-info-especial-bl.component';
import { FieldsetModule } from 'primeng/fieldset';



@NgModule({
  declarations: [
    BandejaEstablecimientoBlComponent,
    BandejaEvaluacionComiteBlComponent,
    RegistroEstablecimientoBlComponent,
    RegistroComiteTecnicoBlComponent,
    EvaluacionComiteTecnicoComponent,
    //tabs
    NuevaIniciativaBlComponent,
    EvaluacionBlComponent,
    EstudioTecnicoBlComponent,
    EvaluacionCampoBlComponent,
    ResultadoEvaluacionBlComponent,
    EnvioResultadosBlComponent,
    EstablecimientoBlComponent,
    //Componentes
    ModalBeneficiarioBlComponent,
    ModalDocumentosEvalCampoBlComponent,
    EvaluarComiteTecnicoBlComponent,
    InformeSaneamientoLegalComponent,
    ExpedienteEvaluacionBlComponent,
    ModalExpedienteCompletoEvalBlComponent,
    ModalInfoEspecialBlComponent,
  ],
  imports: [
    ConfirmDialogModule,
    CommonModule,
    FormsModule,
    // PrimeNG modules
    TableModule,
    CardModule,
    AccordionModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    RadioButtonModule,
    TabViewModule,
    ButtonModule,
    TableModule,
    ConfirmPopupModule,
    ToastModule,
    PaginatorModule,
    InputTextareaModule,
    FieldsetModule,
    // Modulos material angular
    MaterialModule,

    SharedModule,
    CargaArchivoGeneralModule,
  ],
  exports: [

  ]
})
export class BandejaEstablecimientoBlModule { }
