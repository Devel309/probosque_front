import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';
import { FieldsetModule } from 'primeng/fieldset';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { RequisitoPgmfSimpleModule } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.module';
import { SolicitarOpicionEvaluacionModule } from 'src/app/shared/components/solicitar-opicion-evaluacion/solicitar-opicion-evaluacion.module';
import { GeneracionDeclaracionManejoDemaComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/generacion-declaracion-manejo-dema.component';
import { ModalActividadesAprovechamiento } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-actividades-aprovechamiento-equipos/modal-actividades-aprovechamiento/modal-actividades-aprovechamiento';
import { TabActividadesAprovechamientoEquiposComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-actividades-aprovechamiento-equipos/tab-actividades-aprovechamiento-equipos.component';
import { TabAnexosComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-anexos/tab-anexos.component';
import { TabCargaEnvioArchivosComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-carga-envio-documentos/tab-carga-envio-documentos.component';
import { ModalFormularioCronogramaComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-cronograma-actividades/modal/modal-formulario-zonas/modal-formulario-cronograma.component';
import { ModalTipoActividadesComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-cronograma-actividades/modal/modal-tipo-actividades/modal-tipo-actividades.component';
import { TabCronogramaActividadesComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-cronograma-actividades/tab-cronograma-actividades.component';
import { ModalFormularioImpactosNegativosComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-impactos-ambientales-negativos/modal/modal-formulario-impactos-negativos/modal-formulario-impactos-negativos.component';
import { TabImpactosAmbientalesNegativosComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-impactos-ambientales-negativos/tab-impactos-ambientales-negativos.component';
import { TabInformacionGeneralComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-informacion-general/tab-informacion-general.component';
import { ModalActividadesMpumf } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-medidas-proteccion-unidad-manejo-forestal/modal-actividades-mpumf/modal-actividades-mpumf';
import { TabMedidasProteccionUnidadManejoForestalComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-medidas-proteccion-unidad-manejo-forestal/tab-medidas-proteccion-unidad-manejo-forestal.component';
import { TabRecursosForestalesMaderablesComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-recursos-forestales-maderables/tab-recursos-forestales-maderables.component';
import { TabRecursosForestalesNoMaderablesComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-recursos-forestales-no-maderables/tab-recursos-forestales-no-maderables.component';
import { GuardarLaborSilviculturalModalComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-sistema-manejo-laborales-silviculturales/guardar-labor-silvicultural-modal/guardar-labor-silvicultural-modal.component';
import { TabSistemaManejoLaboralesSilviculturalesComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-sistema-manejo-laborales-silviculturales/tab-sistema-manejo-laborales-silviculturales.component';
import { ModalFormularioZonasComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-zonificacion-ordenamiento-interno-area/modal/modal-formulario-zonas/modal-formulario-zonas.component';
import { TabZonificacionOrdenamientoInternoAreaComponent } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-zonificacion-ordenamiento-interno-area/tab-zonificacion-ordenamiento-interno-area.component';
import { EvaluacionArchivoModule } from '../../shared/components/evaluacion-archivo/evaluacion-archivo.module';
import { VisorPdfModule } from '../../shared/components/visor-pdf/visor-pdf.module';




@NgModule({
  declarations: [
    GeneracionDeclaracionManejoDemaComponent,
    TabInformacionGeneralComponent,
    TabZonificacionOrdenamientoInternoAreaComponent,
    TabRecursosForestalesMaderablesComponent,
    TabRecursosForestalesNoMaderablesComponent,
    TabSistemaManejoLaboralesSilviculturalesComponent,
    TabMedidasProteccionUnidadManejoForestalComponent,
    TabActividadesAprovechamientoEquiposComponent,
    TabImpactosAmbientalesNegativosComponent,
    TabCronogramaActividadesComponent,
    TabCargaEnvioArchivosComponent,
    GuardarLaborSilviculturalModalComponent,
    ModalFormularioZonasComponent,
    ModalFormularioCronogramaComponent,
    ModalTipoActividadesComponent,
    ModalFormularioImpactosNegativosComponent,
    ModalActividadesMpumf,
    TabAnexosComponent,
    ModalActividadesAprovechamiento,
    
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        // material angular
        MatTabsModule,
        MatDatepickerModule,
        //primeNG
        ToastModule,
        ConfirmPopupModule,
        DropdownModule,
        TableModule,
        PaginatorModule,
        DialogModule,
        ButtonModule,
        MultiSelectModule,
        AccordionModule,
        RadioButtonModule,
        FieldsetModule,
        CheckboxModule,
        PanelModule,
        EditorModule,
        ConfirmDialogModule,
        SharedModule,
        VisorPdfModule,
        EvaluacionArchivoModule,
        SolicitarOpicionEvaluacionModule,
        RequisitoPgmfSimpleModule,
        InputButtonsCodigoModule
    ],
  exports:[
    TabInformacionGeneralComponent,
    TabZonificacionOrdenamientoInternoAreaComponent,
    TabRecursosForestalesMaderablesComponent,
    TabRecursosForestalesNoMaderablesComponent,
    TabSistemaManejoLaboralesSilviculturalesComponent,
    TabMedidasProteccionUnidadManejoForestalComponent,
    TabActividadesAprovechamientoEquiposComponent,
    TabImpactosAmbientalesNegativosComponent,
    TabCronogramaActividadesComponent,
    TabCargaEnvioArchivosComponent,
    TabCargaEnvioArchivosComponent,
    TabAnexosComponent,
  ],
  providers:[ConfirmationService]
})
export class GeneracionDeclaracionManejoDemaModule { }
