import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEvaluacionPgmfModule } from 'src/app/web/planificacion/evaluacion-pgmf/bandeja-evaluacion-pgmf/bandeja-evaluacion-pgmf.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BandejaEvaluacionPgmfModule
  ]
})
export class EvaluacionPgmfModule { }
