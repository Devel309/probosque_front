import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaPgmfConsolidadoComponent } from 'src/app/web/planificacion/bandeja-pgmf-consolidado/bandeja-pgmf-consolidado.component';
import { SharedModule } from "@shared";
import { MatButtonModule } from '@angular/material/button';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { AppRoutingModule } from 'src/app/app-routing.module';



@NgModule({
  declarations: [
    BandejaPgmfConsolidadoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    // Material Componets
    MatButtonModule,
    // PrimeNG Components
    DropdownModule,
    ButtonModule,
    FieldsetModule
   
  
  ]
})
export class BandejaPgmfConsolidadoModule { }
