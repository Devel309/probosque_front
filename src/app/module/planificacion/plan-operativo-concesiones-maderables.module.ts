import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogTipoEspecieMultiselectModule } from 'src/app/shared/components/dialog-tipo-especie-multiselect/dialog-tipo-especie-multiselect.module';
import { InputButtonsModule } from 'src/app/shared/components/input-button/input-buttons.module';
// import { MomentDateAdapter, MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
// import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { PlanOperativoConcesionesMaderablesComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/plan-operativo-concesiones-maderables.component';
import { ModalAprovechamientoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-actividades-aprovechamiento/modals/modal-aprovechamiento/modal-aprovechamiento.component';
import { ModalOperacionesComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-actividades-aprovechamiento/modals/modal-operaciones/modal-operaciones.component';
import { TabActividadesAprovechamientoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-actividades-aprovechamiento/tab-actividades-aprovechamiento.component';
import { IntervencionesFormComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-actividades-silviculturales/intervenciones-form/intervenciones-form.component';
import { TabActividadesSilviculturalesComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-actividades-silviculturales/tab-actividades-silviculturales.component';
import { TabAnexosComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-anexos/tab-anexos.component';
import { GuardarModalComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-capacitacion/modal/guardar.modal.component';
import { TabCapacitacionComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-capacitacion/tab-capacitacion.component';
import { ModalFormularioCronogramaComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-cronograma/modal/modal-formulario-zonas/modal-formulario-cronograma.component';
import { TabCronogramaComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-cronograma/tab-cronograma.component';
import { EvaluacionAmbientalTabla } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/evaluacion-ambiental-tabla/evaluacion-ambiental.tabla';
import { ActividadComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/actividad/actividad.component';
import { AprovechamientoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/aprovechamiento/aprovechamiento.component';
import { ContingenciaComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/contingencia/contingencia.component';
import { EvaluacionAmbientaloComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/evaluacion-ambiental/evaluacion-ambiental.component';
import { FactorAmbientalComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/factor-ambiental/factor-ambiental.component';
import { ImpactoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/impacto/impacto.component';
import { TabEvaluacionAmbientalComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/tab-evaluacion-ambiental.component';
import { TabInformacionAreaManejoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-area-manejo/tab-informacion-area-manejo.component';
import { TablaCoordenadasComponentModule } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-basica-parcela-corta/components/tabla-coordenadas/tabla-coordenadas.module';
import { TabInformacionBasicaParcelaCortaComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-basica-parcela-corta/tab-informacion-basica-parcela-corta';
import { ContratoForm } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-general/contrato-form/contrato-form';
import { RegenteForm } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-general/regente-form/regente-form';
import { TabInformacionGeneralComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-general/tab-informacion-general.component';
import { ModalMonitoreoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-monitoreo/modal/modal-formulario-impactos-negativos/modal-formulario-impactos-negativos.component';
import { TabMonitoreoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-monitoreo/tab-monitoreo.component';
import { TabObjetivosManejoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-objetivos-manejo/tab-objetivos-manejo.component';
import { TabOrdenamientoParcelaProteccionComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-ordenamiento-parcela-proteccion/tab-ordenamiento-parcela-proteccion.component';
import { GuardarPariticipacionCiudadanaModal } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-participacion-ciudadana/modal/guardar-participacion-ciudadana.modal';
import { TabParticipacionCiudadanaComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-participacion-ciudadana/tab-participacion-ciudadana.component';
import { TabProgramaInversionesComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-programa-inversiones/tab-programa-inversiones.component';
import { InputButtonsMapsCodigoModule } from 'src/app/shared/components/input-button-maps/input-buttons-maps.module';
import { TabResumenActividadesComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-resumen-actividades/resumen-actividades.component';
import { ResumenComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-resumen-actividades/resumen/resumen.component';
import { BandejaPOCCComponent } from '../../web/planificacion/plan-operativo-concesiones-maderables/bandeja-POCC.component';
import { ButtonsDownloadFileModule } from '../../shared/components/button-download-file/buttons-download-file.module';
import { TabCargaEnvioArchivosComponent } from '../../web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-carga-envio-documentos/tab-carga-envio-documentos.component';
import { AnexoUploadFileModule } from 'src/app/shared/components/anexo-upload-file/anexo-upload-file.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { MapaPorAccordeonComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-ordenamiento-parcela-proteccion/components/mapa-por-accordeon/mapa-por-accordeon.component';
import { MapPorAcordeonComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-ordenamiento-parcela-proteccion/components/map-por-acordeon/map-por-acordeon.component';
import { PaginatorModule } from 'primeng/paginator';
import { ModalInfoContratoComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-informacion-general/modal-info-contrato/modal-info-contrato.component';
import { FormActividadComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/form-actividad/form-actividad.component';
import { FormActividadVigilanciaComponent } from 'src/app/web/planificacion/plan-operativo-concesiones-maderables/tabs/tab-evaluacion-ambiental/modal/form-actividad-vigilancia/form-actividad-vigilancia.component';
import { RequisitoPgmfSimpleModule } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.module';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [
    PlanOperativoConcesionesMaderablesComponent,
    TabInformacionGeneralComponent,
    TabObjetivosManejoComponent,
    TabInformacionAreaManejoComponent,
    TabMonitoreoComponent,
    TabParticipacionCiudadanaComponent,
    TabCapacitacionComponent,
    TabCargaEnvioArchivosComponent,
    TabOrdenamientoParcelaProteccionComponent,
    TabProgramaInversionesComponent,
    TabCronogramaComponent,
    TabEvaluacionAmbientalComponent,
    EvaluacionAmbientalTabla,
    //TabAspectosComplementariosComponent,
    TabAnexosComponent,
    TabResumenActividadesComponent,
    TabActividadesAprovechamientoComponent,
    TabActividadesSilviculturalesComponent,
    TabInformacionBasicaParcelaCortaComponent,
    GuardarModalComponent,
    GuardarPariticipacionCiudadanaModal,
    ModalFormularioCronogramaComponent,
    ModalMonitoreoComponent,
    ModalAprovechamientoComponent,
    ModalOperacionesComponent,

    ActividadComponent,
    AprovechamientoComponent,
    ContingenciaComponent,
    FactorAmbientalComponent,
    ImpactoComponent,
    EvaluacionAmbientaloComponent,
    ResumenComponent,
    RegenteForm,
    ContratoForm,
    BandejaPOCCComponent,
    IntervencionesFormComponent,
    MapaPorAccordeonComponent,
    MapPorAcordeonComponent, 
    ModalInfoContratoComponent,
    FormActividadComponent,
    FormActividadVigilanciaComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    MatTabsModule,
    MatInputModule,
    MatDatepickerModule,
    // MatMomentDateModule,
    MatNativeDateModule,
    MatButtonModule,
    MatFormFieldModule,
    MatToolbarModule,

    //primeng
    TabViewModule,
    ButtonModule,
    AccordionModule,
    CalendarModule,
    DropdownModule,
    TableModule,
    ListboxModule,
    CheckboxModule,
    RadioButtonModule,
    PanelModule,
    AutoCompleteModule,
    ToastModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
    MultiSelectModule,
    InputTextareaModule,
    InputMaskModule,
    InputNumberModule,
    SharedModule,
    InputButtonsModule,
    DialogTipoEspecieMultiselectModule,
    TablaCoordenadasComponentModule,
    ToastModule,
    ButtonsDownloadFileModule,
    AnexoUploadFileModule,
    InputButtonsCodigoModule,
    PaginatorModule,
    InputButtonsMapsCodigoModule,
    RequisitoPgmfSimpleModule,
  ],
  exports: [
    TabInformacionGeneralComponent,
    TabObjetivosManejoComponent,
    TabInformacionAreaManejoComponent,
    TabMonitoreoComponent,
    TabParticipacionCiudadanaComponent,
    TabCapacitacionComponent,
    TabCargaEnvioArchivosComponent,
    TabOrdenamientoParcelaProteccionComponent,
    TabProgramaInversionesComponent,
    TabCronogramaComponent,
    EvaluacionAmbientalTabla,
    //TabAspectosComplementariosComponent,
    TabAnexosComponent,
    TabResumenActividadesComponent,
    TabActividadesAprovechamientoComponent,
    TabActividadesSilviculturalesComponent,
    TabInformacionBasicaParcelaCortaComponent,
    GuardarModalComponent,
    GuardarPariticipacionCiudadanaModal,
    RegenteForm,
    ContratoForm,
    MapaPorAccordeonComponent,
    MapPorAcordeonComponent,
    ModalInfoContratoComponent
  ],
  providers: [
    // DatePipe,
    // {
    //   provide: DateAdapter,
    //   useClass: MomentDateAdapter,
    //   deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    // },
    // {
    //   provide: MAT_DATE_FORMATS,
    //   // provide: MAT_DATE_FORMATS,
    //   // useValue: {strict: true}
    //   useValue: MY_FORMATS
    //  },
  ],
})
export class PlanOperativoConcesionesMaderablesModule {}
