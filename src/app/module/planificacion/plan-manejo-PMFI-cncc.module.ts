import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AnexoUploadFileModule } from 'src/app/shared/components/anexo-upload-file/anexo-upload-file.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { InputButtonImgCodigoModule } from 'src/app/shared/components/input-button-img-codigo/input-button-img-codigo.module';
import { InputButtonsMapsCodigoModule } from 'src/app/shared/components/input-button-maps/input-buttons-maps.module';
import { InputImgButtonsModule } from 'src/app/shared/components/input-img-button/input-img-buttons.module';
import { DetallePlanOperativoPMFICnccComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/detalle/detalle-plan-operativo-PMFI-cncc.component';
import { ModalDescripcionComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-descripcion/modal-descripcion.component';
import { ModalFormularioAntecedentesComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-antecedentes/modal-formulario-antecedentes.component';
import { ModalFormularioAprovechamientoForestalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-aprovechamiento-forestal/modal-formulario-aprovechamiento-forestal.component';
import { ModalFormularioCategoriaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-categoria/modal-formulario-categoria.component';
import { ModalFormularioFaunaSilvestreComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-fauna-silvestre/modal-formulario-fauna-silvestre.component';
import { ModalFormularioFinesManerablesComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-fines-maderables/modal-formulario-fines-maderables.component';
import { ModalFormularioHidrografiaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-hidrografia/modal-formulario-hidrografia.component';
import { ModalFormularioInfraestructuraComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/modal/modal-formulario-infraestructura/modal-formulario-infraestructura.component';
import { PlanOperativoPMFICnccComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/plan-operativo-PMFI-cncc.component';
import { Anexo5ComponentPOPAC } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/components/anexo5/anexo5.component';
import { Anexo7ComponentPOPAC } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/components/anexo7/anexo7.component';
import { Anexo8ComponentPOPAC } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/components/anexo8/anexo8.component';
import { MapCustomComponentPOPAC } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/components/map-custom/map-custom.component';
import { ModalAnexo5Module } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/modal/modal-anexo5/modal-anexo5.module';
import { ModalAnexo7Component } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/modal/modal.actividades/modal.actividades.component';
import { TabAnexosComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-anexos/tab-anexos.component';
import { TablaFaunaSilvestreComponentPOPAC } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-aspectos-biologicos/componets/tabla-fauna-silvestre/tabla-fauna-silvestre.component';
import { TabAspectosBiologicosComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-aspectos-biologicos/tab-aspectos-biologicos.component';
import { TabAspectosComplementariosComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-aspectos-complementarios/tab-aspectos-complementarios.component';
import { ModalConfirmationEvaluacionComponentAF } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-aspectos-fisicos/modal/modal-confirmation-guardar/modal-confirmation-guardar.component';
import { TabAspectosFisicosComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-aspectos-fisicos/tab-aspectos-fisicos.component';
import { ModalConfirmationGuardarComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-cronograma-actividades/modal/modal-confirmation-guardar/modal-confirmation-guardar.component';
import { ModalFormularioCronogramaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-cronograma-actividades/modal/modal-formulario-zonas/modal-formulario-cronograma.component';
import { TabCronogramaActividadesComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-cronograma-actividades/tab-cronograma-actividades.component';
import { TabDesarrolloActividadesComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-desarrollo-actividades/tab-desarrollo-actividades.component';
import { EvaluacionAmbientalTabla } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/evaluacion-ambiental-tabla/evaluacion-ambiental.tabla';
import { ActividadComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/actividad/actividad.component';
import { AprovechamientoComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/aprovechamiento/aprovechamiento.component';
import { ContingenciaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/contingencia/contingencia.component';
import { EvaluacionAmbientaloComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/evaluacion-ambiental/evaluacion-ambiental.component';
import { FactorAmbientalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/factor-ambiental/factor-ambiental.component';
import { FormActividadVigilanciaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/form-actividad-vigilancia/form-actividad-vigilancia.component';
import { FormActividadComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/form-actividad/form-actividad.component';
import { ImpactoComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/impacto/impacto.component';
import { ModalConfirmationEvaluacionComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/modal/modal-confirmation-guardar/modal-confirmation-guardar.component';
import { TabEvaluacionAmbientalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-evaluacion-ambiental/tab-evaluacion-ambiental.component';
import { TabFinalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-final/tab-final.component';
import { TablaAreaManejoContinuaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/components/tabla-area-manejo-continua/tabla-area-manejo-continua.component';
import { TablaAreaManejoDivididaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/components/tabla-area-manejo-dividida/tabla-area-manejo-dividida.component';
import { TablaAreaZonificacionComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/components/tabla-area-zonificacion/tabla-area-zonificacion.component';
import { TablaAspectosFisicosComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/components/tabla-aspectos-fisicos/tabla-aspectos-fisicos.component';
import { TablaHidrografiaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/components/tabla-hidrografia/tabla-hidrografia.component';
import { TablaInformativaMapaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/components/tabla-informativa-mapa/tabla-informativa-mapa.component';
import { ModalRegistrarComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/modal-registrar/modal-registrar.component';
import { TabInformacionBasicaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-basica/tab-informacion-basica.component';
import { RegenteForm } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-general/regente-form/regente-form';
import { TabInformacionGeneralComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-general/tab-informacion-general.component';
import { ModalConfirmationGuardarSocioeconomicoComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-socioeconomica/modal-confirmation-guardar/modal-confirmation-guardar.component';
import { ModalFormularioOtrosComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-socioeconomica/modal-formulario-otros/modal-formulario-otros.component';
import { ModalFormularioSocieconomicaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-socioeconomica/modal-formulario-socieconomica/modal-formulario-socieconomica.component';
import { TabInformacionSocioeconomicaComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-informacion-socioeconomica/tab-informacion-socioeconomica.component';
import { TabObjetivosManejoForestalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-objetivos-manejo/tab-objetivos-manejo.component';
import { TablaSuperficieAnioOperativoComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-ordenamiento-y-proteccion/components/tabla-superficie-anio-operativo/tabla-superficie-anio-operativo.component';
import { TablaSuperficiePrimerBloqueComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-ordenamiento-y-proteccion/components/tabla-superficie-primer-bloque/tabla-superficie-primer-bloque.component';
import { TablaSuperficieComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-ordenamiento-y-proteccion/components/tabla-superficie/tabla-superficie.component';
import { FormMedidaProteccionComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-ordenamiento-y-proteccion/modal/form-medida-proteccion/form-medida-proteccion.component';
import { TabOrdenamientoProteccionComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-ordenamiento-y-proteccion/tab-ordenamiento-y-proteccion.component';
import { TablaArbolesAprovechablesModule } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-potencial-produccion-forestal/components/tabla-arboles-aprovechables/tabla-arboles-aprovechables.module';
import { TablaCensoComercialNoMaderableModule } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-potencial-produccion-forestal/components/tabla-censo-comercial-no-maderable/tabla-censo-forestal-no-maderable.module';
import { TablaCensoForestalMaderableModule } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-potencial-produccion-forestal/components/tabla-censo-forestal-maderable/tabla-censo-forestal-maderable.module';
import { TablaFustalesModule } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-potencial-produccion-forestal/components/tabla-fustales/tabla-fustales.module';
import { TablaInventarioNoMaderableModule } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-potencial-produccion-forestal/components/tabla-inventario-no-maderable/tabla-inventario-no-maderable.module';
import { TabPotencialProduccionForestalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-potencial-produccion-forestal/tab-potencial-produccion-forestal.component';
import { ModalConfirmationComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-rentabilidad/modal/modal-confirmation-guardar/modal-confirmation-guardar.component';
import { TabRentabilidadComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-rentabilidad/tab-rentabilidad.component';
import { TabSistemaManejoForestalComponent } from 'src/app/web/planificacion/plan-operativo-PMFI-cncc/tabs/tab-sistema-manejo-forestal/tab-sistema-manejo-forestal.component';
import { InputButtonTypeCodigoModule } from '../../shared/components/input-button-type-codigo/input-button-type-codigo.module';




export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};
@NgModule({
  declarations: [
    PlanOperativoPMFICnccComponent,
    DetallePlanOperativoPMFICnccComponent,
    TabObjetivosManejoForestalComponent,
    TabInformacionBasicaComponent,
    TabAspectosFisicosComponent,
    TabEvaluacionAmbientalComponent,
    ActividadComponent,
    ContingenciaComponent,
    ImpactoComponent,
    EvaluacionAmbientaloComponent,
    AprovechamientoComponent,
    FactorAmbientalComponent,
    EvaluacionAmbientalTabla,
    ModalFormularioHidrografiaComponent,
    ModalFormularioFaunaSilvestreComponent,
    ModalFormularioInfraestructuraComponent,
    ModalFormularioAntecedentesComponent,
    ModalFormularioCategoriaComponent,
    ModalFormularioFinesManerablesComponent,
    ModalFormularioAprovechamientoForestalComponent,
    ModalDescripcionComponent,
    TabAspectosBiologicosComponent,
    TabInformacionSocioeconomicaComponent,
    TabOrdenamientoProteccionComponent,
    TabInformacionGeneralComponent,
    TabSistemaManejoForestalComponent,
    TabPotencialProduccionForestalComponent,
    TabDesarrolloActividadesComponent,
    TabRentabilidadComponent,
    TabAnexosComponent,
    TabFinalComponent,
    TabAspectosComplementariosComponent,
    TabCronogramaActividadesComponent,
    RegenteForm,
    ModalFormularioCronogramaComponent,
    TablaFaunaSilvestreComponentPOPAC,
    ModalConfirmationGuardarComponent,
    ModalConfirmationComponent,
    TablaInformativaMapaComponent,
    FormMedidaProteccionComponent,
    TablaSuperficieComponent,
    TablaSuperficieAnioOperativoComponent,
    TablaSuperficiePrimerBloqueComponent,
    TablaAreaManejoContinuaComponent,
    TablaAreaManejoDivididaComponent,
    TablaAreaZonificacionComponent,
    ModalConfirmationEvaluacionComponent,
    ModalConfirmationEvaluacionComponentAF,
    Anexo5ComponentPOPAC,
    Anexo7ComponentPOPAC,
    Anexo8ComponentPOPAC,
    ModalAnexo7Component,
    ModalRegistrarComponent,
    TablaAspectosFisicosComponent,
    TablaHidrografiaComponent,
    MapCustomComponentPOPAC,
    FormActividadVigilanciaComponent,
    FormActividadComponent,
    ModalConfirmationGuardarSocioeconomicoComponent,
    ModalFormularioOtrosComponent,
    ModalFormularioSocieconomicaComponent
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,

        // material angular
        MatTabsModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatIconModule,

        //primeng
        TabViewModule,
        ButtonModule,
        AccordionModule,
        CalendarModule,
        DropdownModule,
        TableModule,
        ListboxModule,
        CheckboxModule,
        RadioButtonModule,
        PanelModule,
        AutoCompleteModule,
        ToastModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmPopupModule,
        InputTextModule,
        MultiSelectModule,
        AnexoUploadFileModule,
        InputButtonsCodigoModule,
        PaginatorModule,
        SharedModule,
        InputTextareaModule,
        BreadcrumbModule,
        InputImgButtonsModule,
        InputButtonImgCodigoModule,
        InputButtonTypeCodigoModule,
        InputButtonsMapsCodigoModule,
        TablaArbolesAprovechablesModule,
        TablaCensoForestalMaderableModule,
        TablaFustalesModule,
        TablaInventarioNoMaderableModule,
        TablaCensoComercialNoMaderableModule,
        ModalAnexo5Module,
    ],
  exports: [
    DetallePlanOperativoPMFICnccComponent,
    TabObjetivosManejoForestalComponent,
    TabInformacionBasicaComponent,
    TabAspectosFisicosComponent,
    TabAspectosBiologicosComponent,
    TabEvaluacionAmbientalComponent,
    EvaluacionAmbientalTabla,
    TabCronogramaActividadesComponent,
    ActividadComponent,
    ContingenciaComponent,
    ImpactoComponent,
    TabInformacionGeneralComponent,
    TablaFaunaSilvestreComponentPOPAC
  ],
  providers: [
    DatePipe,
    ConfirmationService,
    MessageService,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PlanOperativoPMFICnccModule {}
