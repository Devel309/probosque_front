import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SharedModule } from '@shared';
import { NgxMaskModule } from 'ngx-mask';
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { InputButtonDocCodigoModule } from 'src/app/shared/components/input-button-doc-codigo/input-button-doc-codigo.module';
import { InputButtonsMapsCodigoModule } from 'src/app/shared/components/input-button-maps/input-buttons-maps.module';
import { InputButtonsZipCodigoModule } from 'src/app/shared/components/input-button-zip-codigo/input-buttons-zip.module';
import { InputButtonsModule } from 'src/app/shared/components/input-button/input-buttons.module';
import { InputImgButtonsModule } from 'src/app/shared/components/input-img-button/input-img-buttons.module';
import { ModalInfoPlanComponent } from 'src/app/shared/components/modal-info-plan/modal-info-plan.component';
import { BandejaPoac } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/bandeja-poac.component';
import { PlanOperativoComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/plan-operativo-ccnn-ealta.component';
import { TabActividadesAprovechamientoComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-actividades-aprovechamiento/tab-actividades-aprovechamiento.component';
import { TabActividadesSilviculturalesComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-actividades-silviculturales/tab-actividades-silviculturales.component';
import { TabAnexosComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-anexos/tab-anexos.component';
import { TabAspectosComplementariosComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-aspectos-complementarios/tab-aspectos-complementarios.component';
import { TabCapacitacionComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-capacitacion/tab-capacitacion.component';
import { ModalFormularioCronogramaComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-cronograma-actividades/modal/modal-formulario-zonas/modal-formulario-cronograma.component';
import { TabCronogramaActividadesComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-cronograma-actividades/tab-cronograma-actividades.component';
import { TabGenerarPdfPlanComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-generar-pdf-plan-po/tab-generar-pdf-plan-po.component';
import { TabInformacionAreaAprovechamientoComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-informacion-area-aprovechamiento/tab-informacion-area-aprovechamiento.component';
import { TabInformacionGeneralComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-informacion-general/tab-informacion-general.component';
import { TabMonitoreoComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-monitoreo/tab-monitoreo.component';
import { TabObjetivosManejoComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-objetivos-manejo/tab-objetivos-manejo.component';
import { TabOpinionesComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-opiniones/tab-opiniones.component';
import { TabOrganizacionDesarrolloActividadComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-organizacion-desarrollo-actividad/tab-organizacion-desarrollo-actividad.component';
import { TabParticipacionComunalComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-participacion-comunal/tab-participacion-comunal.component';
import { TabProteccionBosqueComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-proteccion-bosque/tab-proteccion-bosque.component';
import { TabRentabilidadManejoForestalComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-rentabilidad-manejo-forestal/tab-rentabilidad-manejo-forestal.component';
import { ModalRecomendacionesComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-resumen-actividades/modal/modal-recomendaciones/modal-recomendaciones.component';
import { ModalActividadesComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-resumen-actividades/modal/modal.actividades/modal.actividades.component';
import { TabResumenActividadesComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-resumen-actividades/resumen-actividades.component';
import { TabResumenComponent } from 'src/app/web/planificacion/plan-operativo-ccnn-ealta/tabs/tab-resumen/tab-resumen.component';
import { EvaluacionArchivoModule } from '../../shared/components/evaluacion-archivo/evaluacion-archivo.module';
import { EvaluacionSimpleModule } from '../../shared/components/evaluacion-simple/evaluacion-simple.module';
import { InputButtonTypeCodigoModule } from '../../shared/components/input-button-type-codigo/input-button-type-codigo.module';
import { EvaluacionCampo2Module } from '../evaluacion/evaluacion-campo2.module';
import { OpinionesModule } from '../evaluacion/opiniones.module';
import { ResultadoEvaluacionModule } from '../evaluacion/resultado-evaluacion.module';
import { MensajeErrorControlModule } from '../mensaje-error-control.module';
import {RequisitoPgmfSimpleModule} from '../../shared/components/requisitos-tupa/requisitos-tupa.module';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@NgModule({
  declarations: [
    PlanOperativoComponent,
    TabInformacionGeneralComponent,
    TabObjetivosManejoComponent,
    TabInformacionAreaAprovechamientoComponent,
    TabProteccionBosqueComponent,
    TabMonitoreoComponent,
    TabParticipacionComunalComponent,
    TabCapacitacionComponent,
    TabOrganizacionDesarrolloActividadComponent,
    TabRentabilidadManejoForestalComponent,
    TabCronogramaActividadesComponent,
    ModalFormularioCronogramaComponent,
    TabAspectosComplementariosComponent,
    TabAnexosComponent,
    TabResumenActividadesComponent,
    TabActividadesAprovechamientoComponent,
    TabActividadesSilviculturalesComponent,
    TabGenerarPdfPlanComponent,
    TabOpinionesComponent,
    ModalActividadesComponent,
    ModalRecomendacionesComponent,
    BandejaPoac,
    ModalInfoPlanComponent,
    TabResumenComponent
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MensajeErrorControlModule,
        NgxMaskModule.forRoot(),
        // material angular
        MatTabsModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatFormFieldModule,
        MatToolbarModule,

        //primeng
        TabViewModule,
        ButtonModule,
        AccordionModule,
        CalendarModule,
        DropdownModule,
        TableModule,
        ListboxModule,
        CheckboxModule,
        RadioButtonModule,
        PanelModule,
        AutoCompleteModule,
        ToastModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmPopupModule,
        MultiSelectModule,
        TooltipModule,
        FieldsetModule,

        ResultadoEvaluacionModule,
        EvaluacionSimpleModule,
        EvaluacionArchivoModule,
        OpinionesModule,
        EvaluacionCampo2Module,
        SharedModule,
        InputButtonsCodigoModule,
        InputButtonsMapsCodigoModule,
        InputButtonDocCodigoModule,
        InputButtonsModule,
        InputImgButtonsModule,
        InputButtonTypeCodigoModule,
        InputButtonsZipCodigoModule,
        RequisitoPgmfSimpleModule
    ],
  exports: [
    TabInformacionGeneralComponent,
    TabObjetivosManejoComponent,
    TabInformacionAreaAprovechamientoComponent,
    TabProteccionBosqueComponent,
    TabMonitoreoComponent,
    TabParticipacionComunalComponent,
    TabCapacitacionComponent,
    TabOrganizacionDesarrolloActividadComponent,
    TabRentabilidadManejoForestalComponent,
    TabCronogramaActividadesComponent,
    TabAspectosComplementariosComponent,
    TabAnexosComponent,
    TabResumenActividadesComponent,
    TabActividadesAprovechamientoComponent,
    TabActividadesSilviculturalesComponent,
    TabGenerarPdfPlanComponent,
    ModalInfoPlanComponent,

  ],
  providers: [
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PlanOperativoModule {}
