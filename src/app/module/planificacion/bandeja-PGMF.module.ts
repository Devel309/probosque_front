import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BandejaPGMFComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/bandeja-PGMF/bandeja-PGMF.component';
import {SharedModule} from '@shared';

@NgModule({
    declarations: [
      BandejaPGMFComponent
    ],
    imports: [
      CommonModule,
      AppRoutingModule,
      FormsModule,
      ReactiveFormsModule,

      //primeng
      DropdownModule,
      TableModule,
      FieldsetModule,
      ButtonModule,
      CheckboxModule,
      PaginatorModule,
      SharedModule

    ]
  })
  export class BandejaPGMFModule { }
