import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaContratoConcesionPfdmComponent } from 'src/app/web/planificacion/bandeja-contrato-concesion-pfdm/bandeja-contrato-concesion-pfdm.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';



@NgModule({
  declarations: [
    BandejaContratoConcesionPfdmComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    //******* PrimeNg Modules */
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    PaginatorModule,
    RadioButtonModule
  ]
})
export class BandejaContratoConcesionPfdmModule { }
