import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BandejaGeneracionDema } from 'src/app/web/planificacion/generacion-declaracion-manejo-dema/bandeja-generacion-dema.component';
import {SolicitudSanSharedModule} from '../../shared/components/solicitud-san-shared/solicitud-san-shared.module';
import {ToastModule} from 'primeng/toast';
import { TextColorEstadoModule } from 'src/app/shared/components/text-color-estado/text-color-estado.module';

@NgModule({
    declarations: [
      BandejaGeneracionDema
    ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    //primeng
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    SolicitudSanSharedModule,
    ToastModule,
    TextColorEstadoModule

  ]
})
  export class BandejaGeneracionDemaModule { }
