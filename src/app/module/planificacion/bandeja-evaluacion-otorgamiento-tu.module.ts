import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { BandejaEvaluacionOtorgamientoTuComponent } from 'src/app/web/planificacion/bandeja-evaluacion-otorgamiento-tu/bandeja-evaluacion-otorgamiento-tu.component';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { SharedModule } from '@shared';



@NgModule({
  declarations: [
    BandejaEvaluacionOtorgamientoTuComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,


    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,

    ToastModule,
    SharedModule
  ]
})
export class BandejaEvaluacionOtorgamientoTuModule { }
