import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NgPrimeModule } from '../ngprime/ngprime.module';

import { SharedModule } from '@shared';
import { ContratoElaboracionPgmfComponent } from 'src/app/web/planificacion/contrato-elaboracion-pgmf/contrato-elaboracion-pgmf.component';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmDialogModule } from 'primeng/confirmdialog';


@NgModule({
  declarations: [
    ContratoElaboracionPgmfComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    NgPrimeModule,
    ConfirmPopupModule,
    SharedModule
  ],
  exports: [
    ContratoElaboracionPgmfComponent
  ]
})
export class ContratoElaboracionPgmfModule { }
