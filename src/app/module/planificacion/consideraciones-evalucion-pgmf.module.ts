import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';

import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { AccordionModule } from 'primeng/accordion';

import { ConsideracionesEvalucionPgmfComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/consideraciones-evalucion-pgmf/consideraciones-evalucion-pgmf.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {DialogModule} from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { MatDialogModule } from '@angular/material/dialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DropdownModule } from 'primeng/dropdown';
import { SharedModule } from '@shared';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';

@NgModule({
  declarations: [
    ConsideracionesEvalucionPgmfComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    //primeng
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    RadioButtonModule,
    AccordionModule,
    PaginatorModule,
    MatDatepickerModule,
    DialogModule,
    ToastModule,

    SharedModule,
    CargaArchivoGeneralModule,
  ]
})
export class ConsideracionesEvalucionPgmfModule { }
