import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipModule } from 'primeng/chip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { InputSwitchModule } from 'primeng/inputswitch';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ButtonEvaluarSolicitudOtorgamientoModule } from 'src/app/shared/components/button-evaluar-solicitud-otorgamiento/button-evaluar-solicitud-otorgamiento.module';
import { ButtonsFilePermisoForestalModule } from 'src/app/shared/components/buttons-file-permiso-forestal/buttons-file-permiso-forestal.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { InputButtonsModule } from 'src/app/shared/components/input-button/input-buttons.module';
import { AdjuntoModalComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal-ccnn/datos-otorgamiento/modal/adjunto.modal.component';
import { AnexosPfcrComponent } from 'src/app/web/planificacion/revision-permiso-forestal/components/anexos-pfcr/anexos-pfcr.component';
import { DialogPlanesComponent } from 'src/app/web/planificacion/revision-permiso-forestal/components/anexos-pfcr/dialog-tipo-especie/dialog-planes.component';
import { DatosAprovechamientoPfcrComponent } from 'src/app/web/planificacion/revision-permiso-forestal/components/datos-aprovechamiento-pfcr/datos-aprovechamiento-pfcr.component';
import { InformacionGeneralPfcrComponent } from 'src/app/web/planificacion/revision-permiso-forestal/components/informacion-general-pfcr/informacion-general-pfcr.component';
import { MapaForestalComponent } from 'src/app/web/planificacion/revision-permiso-forestal/components/mapa-forestal/mapa-forestal.component';
import { DownloadMapaComponent } from 'src/app/web/planificacion/revision-permiso-forestal/components/mapa/download-mapa.component';
import { ModalEvaluacionAnexosComponent } from 'src/app/web/planificacion/revision-permiso-forestal/datos-otorgamiento/modal/modal-evaluacion-anexo/modal-evaluacion-anexo.component';
import { ModalEvaluarAreaComponent } from 'src/app/web/planificacion/revision-permiso-forestal/datos-otorgamiento/modal/modal-evaluar-area/modal-evaluar-area.component';
import { ModalEvaluarPlanManejoComponent } from 'src/app/web/planificacion/revision-permiso-forestal/datos-otorgamiento/modal/modal-evaluar-plan-manejo/modal-evaluar-plan-manejo.component';
import { ConsultarEntidadesComponent } from 'src/app/web/planificacion/revision-permiso-forestal/modal/consultar-entidades/consultar-entidades.component';
import { ConsultarEntidadesPFModule } from 'src/app/web/planificacion/revision-permiso-forestal/modal/consultar-entidades/consultar-entidades.module';
import { InspeccionOcularComponent } from 'src/app/web/planificacion/revision-permiso-forestal/modal/inspeccion-ocular/inspeccion-ocular.component';
import { ValidarRequisitosModal } from 'src/app/web/planificacion/revision-permiso-forestal/modal/validar-requisitos-modal/validar-requisitos.modal';
import { ModalEvaluacionPersonaComponent } from 'src/app/web/planificacion/revision-permiso-forestal/tipo-registro/model/modal-evaluacion-persona/modal-evaluacion-persona.component';
import { EvaluacionArchivoPermisoForestalModule } from '../../shared/components/evaluacion-archivo-permiso-forestal/evaluacion-archivo-permiso-forestal.module';
import { EvaluacionArchivoModule } from '../../shared/components/evaluacion-archivo/evaluacion-archivo.module';
import { InformeEvaluacionSharedModule } from '../../shared/components/informe-evaluacion-shared/informe-evaluacion-shared.module';
import { ValidarRequisitosPermisoForestalModule } from '../../shared/components/validar-requisitos-permiso-forestal/validar-requisitos-permiso-forestal.module';
import { RecepcionDocumentosFisicosComponent } from '../../web/planificacion/revision-permiso-forestal/components/recepcion-documentos-fisicos/recepcion-documentos-fisicos.component';
import { PermisoResultadoEvaluacionComponent } from '../../web/planificacion/revision-permiso-forestal/components/resultado-evaluacion/permiso-resultado-evaluacion.component';
import { DatosOtorgamientoComponent } from '../../web/planificacion/revision-permiso-forestal/datos-otorgamiento/datos-otorgamiento.component';
import { RevisionPermisosForestales } from '../../web/planificacion/revision-permiso-forestal/revision-permiso-forestal.component';
import { SolicitudPermisoForestalGeneral } from '../../web/planificacion/revision-permiso-forestal/tipo-registro/solicitud-permiso-forestal-general/solicitud-permiso-forestal-general.component';
import { SolicitudPermisoForestalJuridica } from '../../web/planificacion/revision-permiso-forestal/tipo-registro/solicitud-permiso-forestal-juridica/solicitud-permiso-forestal-juridica.component';
import { SolicitudPermisoForestalNatural } from '../../web/planificacion/revision-permiso-forestal/tipo-registro/solicitud-permiso-forestal-natural/solicitud-permiso-forestal-natural.component';
import { SolicitudAccesoSharedModule } from '../solicitud-acceso-shared.module';
@NgModule({
  declarations: [
    RevisionPermisosForestales,
    DatosOtorgamientoComponent,
    SolicitudPermisoForestalGeneral,
    SolicitudPermisoForestalNatural,
    SolicitudPermisoForestalJuridica,
    ModalEvaluacionPersonaComponent,
    ModalEvaluacionAnexosComponent,
    ModalEvaluarAreaComponent,
    ModalEvaluarPlanManejoComponent,
    ValidarRequisitosModal,
    DatosAprovechamientoPfcrComponent,
    InformacionGeneralPfcrComponent,
    PermisoResultadoEvaluacionComponent,
    AnexosPfcrComponent,
    DownloadMapaComponent,
    AdjuntoModalComponent,
    RecepcionDocumentosFisicosComponent,
    MapaForestalComponent,
    InspeccionOcularComponent,
    DialogPlanesComponent
  ],
  imports: [
    SolicitudAccesoSharedModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    AutoCompleteModule,
    ToastModule,
    DialogModule,
    MatDividerModule,
    CardModule,
    MatDatepickerModule,
    ConfirmDialogModule,
    MatTabsModule,
    EvaluacionArchivoModule,
    SharedModule,
    AccordionModule,
    InputButtonsCodigoModule,
    InputButtonsModule,
    InformeEvaluacionSharedModule,
    ConfirmPopupModule,
    ButtonsFilePermisoForestalModule,
    ValidarRequisitosPermisoForestalModule,
    EvaluacionArchivoPermisoForestalModule,
    ButtonEvaluarSolicitudOtorgamientoModule,
    PanelModule,
    ChipModule,
    InputSwitchModule,
    ConsultarEntidadesPFModule,
  ],
  providers: [MessageService, ConfirmationService],
  exports: [
    DatosOtorgamientoComponent,
    SolicitudPermisoForestalGeneral,
    SolicitudPermisoForestalNatural,
    SolicitudPermisoForestalJuridica,
    DatosAprovechamientoPfcrComponent,
    InformacionGeneralPfcrComponent,
    AnexosPfcrComponent,
    DownloadMapaComponent,
    AdjuntoModalComponent,
    MapaForestalComponent,
    InspeccionOcularComponent,
  ],
})
export class RevisionPermisosForestalesModule {}
