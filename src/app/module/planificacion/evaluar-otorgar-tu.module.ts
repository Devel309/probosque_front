import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluarOtorgarTuComponent } from 'src/app/web/planificacion/bandeja-evaluacion-otorgamiento-tu/evaluar-otorgar-tu/evaluar-otorgar-tu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import {InputNumberModule} from 'primeng/inputnumber';
import {RadioButtonModule} from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';


@NgModule({
  declarations: [
    EvaluarOtorgarTuComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    DropdownModule,
    TableModule,
    ButtonModule,
    InputNumberModule,
    RadioButtonModule,

    BreadcrumbModule,
    ToastModule
  ]
})
export class EvaluarOtorgarTuModule { }
