import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaSolicitudComponent } from 'src/app/web/planificacion/Impugnacion/bandeja-solicitud/bandeja-solicitud.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BandejaImpugnacionModule } from './bandeja-impugnacion.module';
import { ImpugnacionRegistroModule } from 'src/app/shared/components/impugnacion-registro/impugnacion-registro.module';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';



@NgModule({
  declarations: [BandejaSolicitudComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    ButtonModule,
    ToastModule,
    BandejaImpugnacionModule,
    ImpugnacionRegistroModule
  ]
})
export class BandejaSolicitudModule { }
