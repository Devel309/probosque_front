import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaPermisosForestales } from 'src/app/web/planificacion/bandeja-permisos-forestales/bandeja-permisos-forestales.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { CheckboxModule } from 'primeng/checkbox';
import { TagModule } from 'primeng/tag';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';

import { ResultadoEvaluacionSolicitudOtorgamientoComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento/resultado-evaluacion-solicitud-otorgamiento.component';
import { FirmezaResolucionComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento/modal/firmeza-resolucion/firmeza-resolucion.component';
//import { RevisionPermisosForestalesComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/revision-permisos-forestales/revision-permisos-forestales.component';

import { NotificacionOtorgamientoPermisoComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/notificacion-otorgamiento-permiso/notificacion-otorgamiento-permiso.component';
import { NotificacionOtorgamientoPermisoDetalleComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/notificacion-otorgamiento-permiso-detalle/notificacion-otorgamiento-permiso-detalle.component';
import { ProcedimientoAdministrativoSancionadorComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/procedimiento-administrativo-sancionador/procedimiento-administrativo-sancionador.component';
import { ResultadoEvaluacionSolicitudOtorgamientoDetalleComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento-detalle/resultado-evaluacion-solicitud-otorgamiento-detalle.component';
import { ModalNotificacionCasoFavorableComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento-detalle/modal/notificacion-caso-favorable/notificacion-caso-favorable.component';
import { ModalNotificacionCasoDesfavorableComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento-detalle/modal/notificacion-caso-desfavorable/notificacion-caso-desfavorable.component';
import { ModalPlantillaFirmezaResolucionComponent } from 'src/app/web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento-detalle/modal/plantilla-firmeza-resolucion/plantilla-firmeza-resolucion.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {ButtonImpugnacionModule} from '../../shared/components/button-impugnacion/button-impugnacion.module';

@NgModule({
  declarations: [
    BandejaPermisosForestales,
    ResultadoEvaluacionSolicitudOtorgamientoComponent,
    FirmezaResolucionComponent,
    ResultadoEvaluacionSolicitudOtorgamientoDetalleComponent,
    ModalNotificacionCasoFavorableComponent,
    ModalNotificacionCasoDesfavorableComponent,
    ModalPlantillaFirmezaResolucionComponent,
    // RevisionPermisosForestalesComponent

    NotificacionOtorgamientoPermisoComponent,
    NotificacionOtorgamientoPermisoDetalleComponent,
    ProcedimientoAdministrativoSancionadorComponent
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,

        DropdownModule,
        TableModule,
        FieldsetModule,
        ButtonModule,
        CheckboxModule,
        PaginatorModule,
        TagModule,
        BreadcrumbModule,
        CardModule,
        ToastModule,
        MatDatepickerModule,
        ButtonImpugnacionModule

    ]
})
export class BandejaPermisosForestalesModule { }
