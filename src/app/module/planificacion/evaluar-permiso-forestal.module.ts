import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { CheckboxModule } from 'primeng/checkbox';
import { CardModule } from 'primeng/card';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { SolicitudAccesoSharedModule } from '../solicitud-acceso-shared.module';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { DatosOtorgamientoComponent } from '../../web/planificacion/evaluar-permiso-forestal/datos-otorgamiento/datos-otorgamiento.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SolicitudPermisoForestalGeneral } from '../../web/planificacion/evaluar-permiso-forestal/tipo-registro/solicitud-permiso-forestal-general/solicitud-permiso-forestal-general.component';
import { SolicitudPermisoForestalNatural } from '../../web/planificacion/evaluar-permiso-forestal/tipo-registro/solicitud-permiso-forestal-natural/solicitud-permiso-forestal-natural.component';
import { SolicitudPermisoForestalJuridica } from '../../web/planificacion/evaluar-permiso-forestal/tipo-registro/solicitud-permiso-forestal-juridica/solicitud-permiso-forestal-juridica.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ModalEvaluacionPersonaComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal/tipo-registro/model/modal-evaluacion-persona/modal-evaluacion-persona.component';
import { DialogModule } from 'primeng/dialog';
import { ModalEvaluacionAnexosComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal/datos-otorgamiento/modal/modal-evaluacion-anexo/modal-evaluacion-anexo.component';
import { MatDividerModule } from '@angular/material/divider';
import { ModalEvaluarAreaComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal/datos-otorgamiento/modal/modal-evaluar-area/modal-evaluar-area.component';
import { ModalEvaluarPlanManejoComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal/datos-otorgamiento/modal/modal-evaluar-plan-manejo/modal-evaluar-plan-manejo.component';
import { ValidarRequisitosModal } from 'src/app/web/planificacion/evaluar-permiso-forestal/modal/validar-requisitos-modal/validar-requisitos.modal';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EvaluarPermisoForestalComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal/evaluar-permiso-forestal.component';

@NgModule({
  declarations: [
    EvaluarPermisoForestalComponent,
    DatosOtorgamientoComponent,
    SolicitudPermisoForestalGeneral,
    SolicitudPermisoForestalNatural,
    SolicitudPermisoForestalJuridica,
    ModalEvaluacionPersonaComponent,
    ModalEvaluacionAnexosComponent,
    ModalEvaluarAreaComponent,
    ModalEvaluarPlanManejoComponent,
    ValidarRequisitosModal,
  ],
  imports: [
    SolicitudAccesoSharedModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    AutoCompleteModule,
    ToastModule,
    DialogModule,
    MatDividerModule,
    CardModule,
    MatDatepickerModule,
    ConfirmDialogModule,
  ],
  providers: [MessageService, ConfirmationService],
  exports: [
    DatosOtorgamientoComponent,
    SolicitudPermisoForestalGeneral,
    SolicitudPermisoForestalNatural,
    SolicitudPermisoForestalJuridica,
  ],
})
export class EvaluarPermisoForestalModule {}
