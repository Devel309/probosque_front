import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaSolicitudOpinionModule } from 'src/app/web/planificacion/solicitud-opinion/bandeja-solicitud-opinion/bandeja-solicitud-opinion.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BandejaSolicitudOpinionModule,
  ]
})
export class SolicitudOpinionModule { }
