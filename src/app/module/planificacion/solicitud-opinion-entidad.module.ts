import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaSolicitudOpinionModule } from 'src/app/web/planificacion/bandeja-solicitud-opinion/bandeja-solicitud-opinion.module';
import { BandejaSolicitudOpinionpmfiDemaPModule } from 'src/app/shared/components/bandeja-solicitud-opinion/bandeja-solicitud-opinion.module';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { PaginatorModule } from 'primeng/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ModalSolicitudOpinionModule } from 'src/app/shared/components/modal-solicitud-opinion/modal-solicitud-opinion.module';
import { BandejaSolicitudOpinionComponent } from 'src/app/shared/components/bandeja-solicitud-opinion/bandeja-solicitud-opinion.component';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DropdownModule,
    FieldsetModule,
    PaginatorModule,
    MatDatepickerModule,
    ModalSolicitudOpinionModule,
    BandejaSolicitudOpinionModule,
    BandejaSolicitudOpinionpmfiDemaPModule
  ]
})
export class SolicitudOpinionEntidadModule { }
