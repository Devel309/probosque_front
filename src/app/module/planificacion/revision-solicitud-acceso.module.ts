import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { FieldsetModule } from 'primeng/fieldset';
// import { ButtonModule } from 'primeng/button';
// import { CheckboxModule } from 'primeng/checkbox';
// import { DropdownModule } from 'primeng/dropdown';

import { RevisionSolicitudAccesoComponent } from 'src/app/web/planificacion/revision-solicitud-acceso/revision-solicitud-acceso.component';
import { SolicitudAccesoSharedModule } from '../solicitud-acceso-shared.module';
//import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import { NgPrimeModule } from '../ngprime/ngprime.module';


@NgModule({
  declarations: [
    RevisionSolicitudAccesoComponent
  ],
  imports: [
    SolicitudAccesoSharedModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgPrimeModule,
    // FieldsetModule,
    // ButtonModule,
    // CheckboxModule,
    // DropdownModule,
    //ToastModule
  ],
  exports:[

  ],
  providers:[    MessageService],
  //schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class RevisionSolicitudAccesoModule { }
