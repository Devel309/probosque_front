import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaPlantacionForestalComponent } from 'src/app/web/planificacion/bandeja-plantacion-forestal/bandeja-plantacion-forestal.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { NgPrimeModule } from '../ngprime/ngprime.module';
import {ConfirmPopupModule} from 'primeng/confirmpopup';



@NgModule({
  declarations: [
    BandejaPlantacionForestalComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgPrimeModule,
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    ConfirmPopupModule
  ]
})
export class BandejaPlantacionForestalModule { }
