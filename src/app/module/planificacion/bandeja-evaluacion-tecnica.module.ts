import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { ToastModule } from 'primeng/toast';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { BandejaEvaluacionTecnicaComponent } from 'src/app/web/planificacion/bandeja-evaluacion-tecnica/bandeja-evaluacion-tecnica.component';
import { GenerarResolucionComponent } from 'src/app/web/planificacion/bandeja-evaluacion-tecnica/generar-resolucion/generar-resolucion.component';
import { ModalResolucionComponent } from 'src/app/web/planificacion/bandeja-evaluacion-tecnica/modal-resolucion/modal-resolucion.component';
import { InputTextareaModule } from 'primeng/inputtextarea';


@NgModule({
  declarations: [
    BandejaEvaluacionTecnicaComponent,
    GenerarResolucionComponent,
    ModalResolucionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,


    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    BreadcrumbModule,
    InputTextareaModule,

    ToastModule
  ]
})
export class BandejaEvaluacionTecnicaModule { }















