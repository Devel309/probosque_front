import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MaterialModule } from '../material/material.module';
import { MensajeErrorControlModule } from '../mensaje-error-control.module';
import { SolicitudSanSharedModule } from '../../shared/components/solicitud-san-shared/solicitud-san-shared.module';
import { TextColorEstadoModule } from 'src/app/shared/components/text-color-estado/text-color-estado.module';
import { BandejaDocumentosDigitalizadosComponent } from 'src/app/web/planificacion/bandeja-documentos-digitalizados/bandeja-documentos-digitalizados.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@NgModule({
  declarations: [
    BandejaDocumentosDigitalizadosComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MensajeErrorControlModule,
    // material angular
    MaterialModule,
    //primeng
    TabViewModule,
    ButtonModule,
    AccordionModule,
    CalendarModule,
    DropdownModule,
    TableModule,
    ListboxModule,
    CheckboxModule,
    RadioButtonModule,
    PanelModule,
    AutoCompleteModule,
    ToastModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
    MultiSelectModule,
    TooltipModule,
    FieldsetModule,
    SharedModule,
    PaginatorModule,
    SolicitudSanSharedModule,
    TextColorEstadoModule
  ],
  exports: [
  ],
  providers: [
    DatePipe,
    ConfirmationService,
    MessageService,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [ MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS ],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class BandejaDocumentosDigitalizadosModule { }
