import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { ElaboracionDeclaracionMpafppComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/elaboracion-declaracion-mpafpp.component';
import { DropdownModule } from 'primeng/dropdown';
import { MpafppInformacionGeneralComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-informacion-general/mpafpp-informacion-general.component';
import { TableModule } from 'primeng/table';
import { MpafppInformacionUmfComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-informacion-umf/mpafpp-informacion-umf.component';
import { MpafppOrdenamientoUmfComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-ordenamiento-umf/mpafpp-ordenamiento-umf.component';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { ConfirmationService } from 'primeng/api';
import { MensajeErrorControlModule } from '../mensaje-error-control.module';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { MpafppAprobechamientoUmfComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-aprobechamiento-umf/mpafpp-aprobechamiento-umf.component';
import { MpafppAprobechamientoDrmfComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-aprobechamiento-drmf/mpafpp-aprobechamiento-drmf.component';
import { MpafppSistenaManejoSilviculComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-sistena-manejo-silvicul/mpafpp-sistena-manejo-silvicul.component';
import { MpafppTabOchoComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-tab-ocho/mpafpp-tab-ocho.component';
import { MpafppTabNueveComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-tab-nueve/mpafpp-tab-nueve.component';
import { MpafppCronogramaActividadesComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-cronograma-actividades/mpafpp-cronograma-actividades.component';
import {MultiSelectModule} from 'primeng/multiselect';
import { AnexoFormatoDemaComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/anexo-formato-dema/anexo-formato-dema.component';
import { AccordionModule } from 'primeng/accordion';
import { ModalDetalleVolumenesComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/anexo-formato-dema/modal/modal-detalle-volumenes/modal-detalle-volumenes.component';
import { TabCargaDescargaArchivosComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/tab-carga-descarga-archivos/tab-carga-descarga-archivos.component';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_FORMATS } from './plan-general-manejo.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CheckboxModule } from 'primeng/checkbox';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { SharedModule } from '@shared';
import { RegistroActividadComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/modal/registro-actividad/registro-actividad.component';
import { ModalCronogramaMpafppComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-cronograma-actividades/modal-cronograma-mpafpp/modal-cronograma-mpafpp.component';
import { ModalFormularioOrdenamientoUmfComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-ordenamiento-umf/modal-formulario-ordenamiento-umf/modal-formulario-ordenamiento-umf.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MpafppMedidasProteccionComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-medidas-proteccion-umf/mpafpp-medidas-proteccion-umf';
import { FormMedidaProteccionComponent } from 'src/app/web/planificacion/elaboracion-declaracion-mpafpp/tabs/mpafpp-medidas-proteccion-umf/form-medida-proteccion/form-medida-proteccion.component';
import { InputTextareaModule } from 'primeng/inputtextarea';

@NgModule({
  declarations: [
    ElaboracionDeclaracionMpafppComponent,
    MpafppInformacionGeneralComponent,
    MpafppInformacionUmfComponent,
    ModalFormularioOrdenamientoUmfComponent,
    MpafppOrdenamientoUmfComponent,
    MpafppAprobechamientoUmfComponent,
    MpafppAprobechamientoDrmfComponent,
    MpafppSistenaManejoSilviculComponent,
    MpafppTabOchoComponent,
    MpafppTabNueveComponent,
    MpafppCronogramaActividadesComponent,
    AnexoFormatoDemaComponent,
    ModalDetalleVolumenesComponent,
    TabCargaDescargaArchivosComponent,
    RegistroActividadComponent,
    ModalCronogramaMpafppComponent,
    MpafppMedidasProteccionComponent,
    FormMedidaProteccionComponent,
  ],
  imports: [
    NgxMaskModule.forRoot(),
    MensajeErrorControlModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    MatTabsModule,
    //primeNG
    RadioButtonModule,
    ToastModule,
    ConfirmPopupModule,
    DropdownModule,
    TableModule,
    PaginatorModule,
    DialogModule,
    ButtonModule,
    MultiSelectModule,
    CheckboxModule,
    AccordionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    InputButtonsCodigoModule,
    InputTextareaModule,
    SharedModule
  ],
  exports:[
    MpafppInformacionGeneralComponent,
    MpafppInformacionUmfComponent,
    ModalFormularioOrdenamientoUmfComponent,
    MpafppOrdenamientoUmfComponent,
    MpafppAprobechamientoUmfComponent,
    MpafppAprobechamientoDrmfComponent,
    MpafppSistenaManejoSilviculComponent,
    MpafppTabOchoComponent,
    MpafppTabNueveComponent,
    MpafppCronogramaActividadesComponent,
    AnexoFormatoDemaComponent,
    TabCargaDescargaArchivosComponent,
    RegistroActividadComponent,
    ModalCronogramaMpafppComponent,
    MpafppMedidasProteccionComponent,
    FormMedidaProteccionComponent,
  ],
  providers:[ConfirmationService,
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },]
})
export class ElaboracionDeclaracionMpafppModule { }
