import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AnexoUploadFileModule } from 'src/app/shared/components/anexo-upload-file/anexo-upload-file.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { TablaDocumentosModule } from 'src/app/shared/components/tabla-documentos/tabla-documentos.module';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { ConcesionForestalMaderablesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/concesion-forestal-maderables.component';
import { ModalFormularioAreasDegradadasComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-areasDegradas/modal-formulario-areasDegradas.component';
import { ModalFormularioEspeciesFloraPortegerComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-especies-flora-proteger/modal-formulario-especies-flora-proteger.component';
import { ModalFormularioEspeciesFloraComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-especies-flora/modal-formulario-especies-flora.component';
import { ModalFormularioEspeciesImportantesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-especies-importantes/modal-formulario-especies-importantes.component';
import { ModalFormularioEspeciesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-especies/modal-formulario-especies.component';
import { ModalFormularioEspecificacionesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-especificaciones/modal-formulario-especificaciones.component';
import { ModalFormularioUsoPotencialComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/modal/modal-formulario-uso-potencial/modal-formulario-uso-potencial.component';
import { BloqueQuinquenalComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/ordenamiento-proteccion/division-administrativa/bloque-quinquenal/bloque-quinquenal.component';
import { ParcelaCortaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/ordenamiento-proteccion/division-administrativa/parcela-corta/parcela-corta.component';
import { ProteccionVigilanciaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/ordenamiento-proteccion/protecion-vigilancia/protecion-vigilancia.component';
import { TabOrdenamientoProteccionComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/ordenamiento-proteccion/tab-ordenamiento-proteccion.component';
import { TabAnexosComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-anexos/tab-anexos.component';
import { NuevoTemaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-capacitacion/nuevo-tema/nuevo-tema.component';
import { TabCapacitacionComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-capacitacion/tab-capacitacion.component';
import { ModalFormularioCronogramaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-cronograma-actividades/modal-formulario-cronograma/modal-formulario-cronograma.component';
import { TabCronogramaActividadesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-cronograma-actividades/tab-cronograma-actividades.component';
import { TabDescargaEnvioComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-descarga-envio/tab-descarga-envio.component';
import { EvaluacionAmbientalTabla } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/evaluacion-ambiental-tabla/evaluacion-ambiental.tabla';
import { ActividadComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/modal/actividad/actividad.component';
import { AprovechamientoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/modal/aprovechamiento/aprovechamiento.component';
import { ContingenciaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/modal/contingencia/contingencia.component';
import { EvaluacionAmbientaloComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/modal/evaluacion-ambiental/evaluacion-ambiental.component';
import { FactorAmbientalComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/modal/factor-ambiental/factor-ambiental.component';
import { ImpactoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/modal/impacto/impacto.component';
import { TabEvaluacionImpactoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-evaluacion-impacto/tab-evaluacion-impacto.component';
import { ActividadesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/actividades/actividades.component';
import { TablaFaunaSilvestreComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/components/tabla-fauna-silvestre/tabla-fauna-silvestre.component';
import { ModalAccesibilidadRutasComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-accesibilidad-rutas/modal-accesibilidad-rutas.component';
import { ModalAntecedenteComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-antecedente/modal-antecedente.component';
import { ModalAspectoFisicoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-aspecto-fisico/modal-aspecto-fisico.component';
import { ModalCaracterizacionComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-caracterizacion/modal-caracterizacion.component';
import { ModalConflictoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-conflicto/modal-conflicto.component';
import { ModalDescripcionComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-descripcion/modal-descripcion.component';
import { ModalFaunaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-fauna/modal-fauna.component';
import { ModalInfraestructuraComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-infraestructura/modal-infraestructura.component';
import { ModalTipoBosqueComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-tipo-bosque/modal-tipo-bosque.component';
import { ModalUbicacionComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/modal/modal-ubicacion/modal-ubicacion.component';
import { TanInformacionBasicaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-basica/tab-informacion-basica.component';
import { RegenteForm } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-general/regente-form/regente-form';
import { TabInformacionGeneralComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-informacion-general/tab-informacion-general.component';
import { TabManejoForestalComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-manejo-forestal/tab-manejo-forestal.component';
import { ModalFormularioComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-monitoreo/modal-formulario/modal-formulario.component';
import { TabMonitoreoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-monitoreo/tab-monitoreo.component';
import { TabObjetivosManejoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-objetivos-manejo/tab-objetivos-manejo.component';
import { FuncionesComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-organizacion-manejo/modal/funciones/funciones.component';
import { RequerimientoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-organizacion-manejo/modal/requerimiento/requerimiento.component';
import { TabOrganizacionManejoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-organizacion-manejo/tab-organizacion-manejo.component';
import { ModalRelacionamientoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-participacion-ciudadana/relacionamiento/relacionamiento.component';
import { TabParticipacionCiudadanaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-participacion-ciudadana/tab-participacion-ciudadana.component';
import { MapaComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-prod-recurso/mapa/mapa-component';
import { TabProdRecursoComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-prod-recurso/tab-prod-recurso.component';
import { TabProgramaInversionComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/tabs/tab-programa-inversion/tab-programa-inversion.component';
import { LineamientoSharedModule } from '../../shared/components/lineamiento-shared/lineamiento-shared.module';
import { MapPorAcordeonComponent } from '../../web/planificacion/concesion-forestal-maderables/tabs/ordenamiento-proteccion/map-por-acordeon/map-por-acordeon.component';

@NgModule({
  declarations: [
    ConcesionForestalMaderablesComponent,
    ActividadComponent,
    AprovechamientoComponent,
    ContingenciaComponent,
    EvaluacionAmbientaloComponent,
    FactorAmbientalComponent,
    ImpactoComponent,
    NuevoTemaComponent,
    ModalDescripcionComponent,
    FuncionesComponent,
    RequerimientoComponent,
    EvaluacionAmbientalTabla,
    ModalUbicacionComponent,
    ModalAccesibilidadRutasComponent,
    ModalAspectoFisicoComponent,
    ModalFaunaComponent,
    ModalFormularioUsoPotencialComponent,
    ModalFormularioEspeciesComponent,
    ModalFormularioEspeciesFloraComponent,
    ModalFormularioEspeciesFloraPortegerComponent,
    ModalFormularioEspecificacionesComponent,
    ModalTipoBosqueComponent,
    ModalCaracterizacionComponent,
    ModalFormularioEspeciesImportantesComponent,
    ModalInfraestructuraComponent,
    ModalAntecedenteComponent,
    ModalConflictoComponent,
    MapPorAcordeonComponent,
    ModalFormularioAreasDegradadasComponent,
    TabInformacionGeneralComponent,
    TabObjetivosManejoComponent,
    TabOrdenamientoProteccionComponent,
    ProteccionVigilanciaComponent,
    TabMonitoreoComponent,
    TabParticipacionCiudadanaComponent,
    TabCapacitacionComponent,
    TabOrganizacionManejoComponent,
    TabProgramaInversionComponent,
    TanInformacionBasicaComponent,
    TabEvaluacionImpactoComponent,
    TabAnexosComponent,
    TabProdRecursoComponent,
    TabCronogramaActividadesComponent,
    TabManejoForestalComponent,
    TabDescargaEnvioComponent,

    ModalRelacionamientoComponent,
    RegenteForm,

    MapaComponent,

    ModalFormularioComponent,

    //AnexoUploadFileComponent
    ModalFormularioCronogramaComponent,
    BloqueQuinquenalComponent,
    ParcelaCortaComponent,
    TablaFaunaSilvestreComponent,
    ActividadesComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    // Material Componets
    MatStepperModule,
    MatTabsModule,
    MatInputModule,
    MatNativeDateModule,
    MatButtonModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    // PrimeNG Components
    ProgressSpinnerModule,
    TableModule,
    PaginatorModule,
    RadioButtonModule,
    CalendarModule,
    MatDatepickerModule,
    FieldsetModule,
    CheckboxModule,
    InputTextareaModule,
    RadioButtonModule,
    ToastModule,
    BrowserModule,
    DialogModule,
    TabViewModule,
    ButtonModule,
    AccordionModule,
    CalendarModule,
    DropdownModule,
    TableModule,
    ListboxModule,
    CheckboxModule,
    RadioButtonModule,
    PanelModule,
    AutoCompleteModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
    InputTextModule,
    MultiSelectModule,
    InputNumberModule,
    InputTextareaModule,
    BreadcrumbModule,
    TablaDocumentosModule,
    LineamientoSharedModule,
    AnexoUploadFileModule,
    InputButtonsCodigoModule,
    UploadInputButtonsModule,
  ],
  exports: [
    NuevoTemaComponent,

    FuncionesComponent,
    RequerimientoComponent,
    EvaluacionAmbientalTabla,

    ModalUbicacionComponent,
    ModalAccesibilidadRutasComponent,
    ModalAspectoFisicoComponent,
    ModalFaunaComponent,
    ModalFormularioUsoPotencialComponent,
    ModalFormularioEspeciesComponent,
    ModalFormularioEspeciesFloraComponent,
    ModalFormularioEspeciesFloraPortegerComponent,
    ModalFormularioEspecificacionesComponent,
    ModalFormularioAreasDegradadasComponent,

    TabInformacionGeneralComponent,
    TabObjetivosManejoComponent,
    TabOrdenamientoProteccionComponent,
    ProteccionVigilanciaComponent,
    TabMonitoreoComponent,
    TabParticipacionCiudadanaComponent,
    TabCapacitacionComponent,
    TabOrganizacionManejoComponent,
    TabProgramaInversionComponent,
    TanInformacionBasicaComponent,
    TabEvaluacionImpactoComponent,
    TabCronogramaActividadesComponent,
    TabManejoForestalComponent,
    TabDescargaEnvioComponent,

    ModalFormularioComponent,
    //AnexoUploadFileComponent
    TablaFaunaSilvestreComponent,
    ActividadComponent
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' }],
})
export class ConcesionForestalMaderablesModule {}
