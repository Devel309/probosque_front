import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatDividerModule } from '@angular/material/divider';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CheckboxModule } from 'primeng/checkbox';
import { DividerModule } from 'primeng/divider';
import { FileUploadModule } from 'primeng/fileupload';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CalendarModule } from 'primeng/calendar';
import { FieldsetModule } from 'primeng/fieldset';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { BandejaContratoComponent } from 'src/app/web/planificacion/bandeja-contrato/bandeja-contrato.component';
import { ModalExpedienteComponent } from 'src/app/web/planificacion/bandeja-contrato/modal-expediente/modal-expediente.component';
import { TooltipModule } from 'primeng/tooltip';
import { ModalEnviarRespuestaComponent } from 'src/app/web/planificacion/bandeja-contrato-concesion-pfdm/modal-enviar-respuesta/modal-enviar-respuesta.component';
import { EnvioInformacionOtorgamientoComponent } from 'src/app/web/planificacion/envio-informacion-otorgamiento/envio-informacion-otorgamiento.component';
import { ModalEnvioInformacionOtorgamientoComponent } from 'src/app/web/planificacion/envio-informacion-otorgamiento/modal-envio-informacion-otorgamiento/modal-envio-informacion-otorgamiento.component';
@NgModule({
  declarations: [
    EnvioInformacionOtorgamientoComponent,
    ModalEnvioInformacionOtorgamientoComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    DialogModule,
    AccordionModule,
    PaginatorModule,
    RadioButtonModule,
    HttpClientModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    FormsModule,
    CheckboxModule,
    // componentes material
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    DividerModule,
    InputTextareaModule,
    CalendarModule,
    MatDatepickerModule,
    FieldsetModule,
    BreadcrumbModule,
    DropdownModule,
    TooltipModule,
  ],
  exports: [
    CdkStepperModule,
    MatButtonModule,
    MatChipsModule,
    MatStepperModule,
    ModalEnvioInformacionOtorgamientoComponent,
  ],
})
export class EnvioInformacionOtorgamientoModule {}
