import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FieldsetModule } from 'primeng/fieldset';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DropdownModule } from 'primeng/dropdown';

import { BandejaImpugnacionComponent } from 'src/app/shared/components/bandeja-impugnacion/bandeja-impugnacion.component';
import { SharedModule } from '@shared';


@NgModule({
  declarations: [BandejaImpugnacionComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    FieldsetModule,
    TableModule,
    PaginatorModule,
    DropdownModule,
    MatDatepickerModule,
    SharedModule
  ],
  exports:[BandejaImpugnacionComponent]
})
export class BandejaImpugnacionModule { }
