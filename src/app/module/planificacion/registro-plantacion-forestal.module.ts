import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistroPlantacionForestalComponent } from 'src/app/web/planificacion/registro-plantacion-forestal/registro-plantacion-forestal.component';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DialogModule } from 'primeng/dialog';
import { PlantacionForestalSharedModule } from '../plantacion-forestal-shared.module';
import { ToastModule } from 'primeng/toast';



@NgModule({
  declarations: [
    RegistroPlantacionForestalComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // primeng
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    RadioButtonModule,
    DropdownModule,
    DialogModule,
    ToastModule,
    PlantacionForestalSharedModule
  ],
  exports:[
    RegistroPlantacionForestalComponent
  ]
})
export class RegistroPlantacionForestalModule { }
