import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatSliderModule } from '@angular/material/slider';
import { MatStepperModule } from '@angular/material/stepper';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DividerModule } from 'primeng/divider';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RippleModule } from 'primeng/ripple';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { DetalleResultadoComponent } from 'src/app/web/planificacion/generacion-contrato/detalle-resultado/detalle-resultado.component';
import { GeneracionContratoComponent } from 'src/app/web/planificacion/generacion-contrato/generacion-contrato.component';
import { ModalContratoGeneradoComponent } from 'src/app/web/planificacion/generacion-contrato/modal-contrato-generado/modal-contrato-generado.component';
import { ModalContratoComponent } from 'src/app/web/planificacion/generacion-contrato/modal-contrato/modal-contrato.component';
import { ModalGeneracionContratoComponent } from 'src/app/web/planificacion/generacion-contrato/modal-generacion-contrato/modal-generacion-contrato.component';
import { ModalInformacionEspacialComponent } from 'src/app/web/planificacion/generacion-contrato/modal-informacion-espacial/modal-informacion-espacial.component';
import { ModalInformacionComponent } from 'src/app/web/planificacion/generacion-contrato/modal-informacion/modal-informacion.component';


@NgModule({
  declarations: [
    GeneracionContratoComponent,
    ModalGeneracionContratoComponent,
    DetalleResultadoComponent,
    ModalContratoGeneradoComponent,
    ModalInformacionComponent,
    ModalInformacionEspacialComponent,
    ModalContratoComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    DialogModule,
    AccordionModule,
    PaginatorModule,
    RadioButtonModule,
    HttpClientModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    FormsModule,
    CheckboxModule,
    // componentes material
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    DividerModule,
    InputTextareaModule,
    CalendarModule,
    MatDatepickerModule,
    FieldsetModule,
    BreadcrumbModule,
    DropdownModule,
    ConfirmPopupModule,

    UploadInputButtonsModule,
    SharedModule,
  ],
  exports: [
    CdkStepperModule,
    MatButtonModule,
    MatChipsModule,
    MatStepperModule,
    ModalGeneracionContratoComponent,
    ModalContratoComponent,
    ModalContratoGeneradoComponent,
    ModalInformacionComponent,
    ModalInformacionEspacialComponent
  ],
})
export class GeneracionContratoModule {}
