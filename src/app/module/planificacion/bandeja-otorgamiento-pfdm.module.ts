import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { ModalResolucionOtorgamientoPfdmComponent } from 'src/app/web/planificacion/bandeja-otorgamiento-pfdm/modal-resolucion-otorgamiento-pfdm/modal-resolucion-otorgamiento-pfdm.component';
import { GenerarResolucionOtorgamientoPfdmComponent } from 'src/app/web/planificacion/bandeja-otorgamiento-pfdm/generar-resolucion-otorgamiento-pfdm/generar-resolucion-otorgamiento-pfdm.component';
import { BandejaOtorgamientoPfdmComponent } from 'src/app/web/planificacion/bandeja-otorgamiento-pfdm/bandeja-otorgamiento-pfdm.component';

@NgModule({
  declarations: [
    BandejaOtorgamientoPfdmComponent,
    GenerarResolucionOtorgamientoPfdmComponent,
    ModalResolucionOtorgamientoPfdmComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    RadioButtonModule,
    ToastModule,
    BreadcrumbModule,
    InputTextareaModule,
    InputNumberModule,
  ],
  exports: [ModalResolucionOtorgamientoPfdmComponent],
})
export class BandejaOtorgamientoPfdmModule {}
