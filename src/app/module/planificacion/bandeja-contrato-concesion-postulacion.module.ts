import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaContratoConcesionPostulacionComponent } from 'src/app/web/planificacion/bandeja-contrato-concesion-postulacion/bandeja-contrato-concesion-postulacion.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { ModalPublicacionComponent } from 'src/app/web/planificacion/bandeja-contrato-concesion-postulacion/modal-publicacion/modal-publicacion.component';
import { ModalEvaluacionComponent } from 'src/app/web/planificacion/bandeja-contrato-concesion-postulacion/modal-evaluacion/modal-evaluacion.component';
import { RadioButtonModule } from 'primeng/radiobutton';



@NgModule({
  declarations: [
    BandejaContratoConcesionPostulacionComponent,
    ModalPublicacionComponent,
    ModalEvaluacionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    //******* PrimeNg Modules */
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    DialogModule,
    PaginatorModule,
    RadioButtonModule
  ]
})
export class BandejaContratoConcesionPostulacionModule { }
