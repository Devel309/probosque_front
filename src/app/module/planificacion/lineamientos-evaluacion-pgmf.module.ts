import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineamientosEvaluacionPgmfComponent } from 'src/app/web/planificacion/concesion-forestal-maderables/lineamientos-evaluacion-pgmf/lineamientos-evaluacion-pgmf.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AccordionModule } from 'primeng/accordion';
import { PaginatorModule } from 'primeng/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { SolicitudOpinionLineamientoModule } from 'src/app/shared/components/solicitud-opinion-lineamiento/solicitud-opinion-lineamiento.module';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { SharedModule } from '@shared';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';



@NgModule({
  declarations: [
    LineamientosEvaluacionPgmfComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    //*********/
    UploadInputButtonsModule,
    SolicitudOpinionLineamientoModule,
    //primeng
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    RadioButtonModule,
    AccordionModule,
    PaginatorModule,
    MatDatepickerModule,
    DialogModule,
    ToastModule,
    ConfirmPopupModule,

    SharedModule,
    CargaArchivoGeneralModule,
  ]
})
export class LineamientosEvaluacionPgmfModule { }
