import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RevicionPlantacionForestalComponent } from 'src/app/web/planificacion/revicion-plantacion-forestal/revicion-plantacion-forestal.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { PlantacionForestalSharedModule } from '../plantacion-forestal-shared.module';



@NgModule({
  declarations: [
    RevicionPlantacionForestalComponent
  ],
  imports: [
    PlantacionForestalSharedModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // primeng
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    RadioButtonModule,
    DropdownModule,
    DialogModule    
  ]

})
export class RevicionPlantacionForestalModule { }
