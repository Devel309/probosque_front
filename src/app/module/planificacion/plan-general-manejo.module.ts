import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
// import { MatTabsModule } from '@angular/material/tabs';
// import { MatInputModule } from "@angular/material/input";
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatNativeDateModule } from '@angular/material/core';
// import { MatButtonModule } from '@angular/material/button';
// import { MatFormFieldModule } from '@angular/material/form-field';
// import { MatToolbarModule } from '@angular/material/toolbar';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogTipoBosqueModule } from 'src/app/shared/components/dialog-tipo-bosque/dialog-tipo-bosque.module';
import { DialogTipoEspecieMultiselectFaunaModule } from 'src/app/shared/components/dialog-tipo-especie-multiselect-fauna/dialog-tipo-especie-multiselect.module';
import { DialogTipoEspecieMultiselectModule } from 'src/app/shared/components/dialog-tipo-especie-multiselect/dialog-tipo-especie-multiselect.module';
import { DialogTipoEspecieModule } from 'src/app/shared/components/dialog-tipo-especie/dialog-tipo-especie.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { InputButtonsMapsCodigoModule } from 'src/app/shared/components/input-button-maps/input-buttons-maps.module';
import { BandejaPgmfa } from 'src/app/web/planificacion/plan-general-manejo/bandeja-pgmfa.component';
import { PlanGeneralManejoComponent } from 'src/app/web/planificacion/plan-general-manejo/plan-general-manejo.component';
import { TabAnexosComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-anexos/tab-anexos.component';
import { TabAspectosComplementariosComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-aspectos-complementarios/tab-aspectos-complementarios.component';
import { TabCapacitacionComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-capacitacion/tab-capacitacion.component';
import { ModalFormularioCronogramaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-cronograma/modal/modal-formulario-zonas/modal-formulario-cronograma.component';
import { TabCronogramaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-cronograma/tab-cronograma.component';
import { TabDuracionRvisionPlanComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-duracion-rvision-plan/tab-duracion-rvision-plan.component';
import { TablaAreaManejoContinuaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-area-manejo-continua/tabla-area-manejo-continua.component';
import { TablaAreaManejoDivididaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-area-manejo-dividida/tabla-area-manejo-dividida.component';
import { ModalFormEspeciesComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-fauna-silvestre/modal-form-especies/modal-form-especies.component';
import { TablaFaunaSilvestreComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-fauna-silvestre/tabla-fauna-silvestre.component';
import { TablaFisiografiaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-fisiografia/tabla-fisiografia.component';
import { TablaHidrografiaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-hidrografia/tabla-hidrografia.component';
import { TablaMatrizAccesibilidadComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-matriz-accesibilidad/tabla-matriz-accesibilidad.component';
import { TabInformacionAreaManejoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/tab-informacion-area-manejo.component';
import { ModalFormularioReforestacionComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/modal-formulario-reforestacion/modal-formulario-reforestacion.component';
import { TablaActividadesOrdenamientoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/tabla-actividades-ordenamiento/tabla-actividades-ordenamiento.component';
import { TablaConstruccionCaminosComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/tabla-construccion-caminos/tabla-construccion-caminos.component';
import { TablaEspeciesAprovecharComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/tabla-especies-aprovechar/tabla-especies-aprovechar.component';
import { TablaEspeciesProtegerComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/tabla-especies-proteger/tabla-especies-proteger.component';
import { TablaEspecificacionesCaminosComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/tabla-especificaciones-caminos/tabla-especificaciones-caminos.component';
import { TablaManejoVerticesComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/tabla-manejo-vertices/tabla-manejo-vertices.component';
import { TabManejoBosqueComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/tab-manejo-bosque.component';
import { TabMonitoreoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-monitoreo/tab-monitoreo.component';
import { TabObjetivosManejoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-objetivos-manejo/tab-objetivos-manejo.component';
import { TabOpinionesComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-opiniones/tab-opiniones.component';
import { ModalFormularioOrdenInternoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-ordenamiento-area-manejo/modal-formulario-orden-interno/modal-formulario-orden-interno.component';
import { TabOrdenamientoAreaManejoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-ordenamiento-area-manejo/tab-ordenamiento-area-manejo.component';
import { TabOrganizacionDesarrolloActividadComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-organizacion-desarrollo-actividad/tab-organizacion-desarrollo-actividad.component';
import { TabParticipacionComunalComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-participacion-comunal/tab-participacion-comunal.component';
import { TablaPotencialProduccionRfComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-potencial-produccion-rf/components/tabla-potencial-produccion/tabla-potencial-produccion-rf.component';
import { TabPotencialProduccionRfComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-potencial-produccion-rf/tab-potencial-produccion-rf.component';
import { TabProteccionBosqueComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-proteccion-bosque/tab-proteccion-bosque.component';
import { TabRentabilidadManejoForestalComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-rentabilidad-manejo-forestal/tab-rentabilidad-manejo-forestal.component';
import { TabResumenEjecutivoComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-resumen-ejecutivo/tab-resumen-ejecutivo.component';
import { TabResumenComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-resumen/tab-resumen.component';
import { EvaluacionArchivoModule } from '../../shared/components/evaluacion-archivo/evaluacion-archivo.module';
import { InputButtonsModule } from '../../shared/components/input-button/input-buttons.module';
import { InputImgButtonsModule } from '../../shared/components/input-img-button/input-img-buttons.module';
import { EvaluacionOtorgamientoPermisoComponent } from '../../web/evaluacion/evaluacion-otorgamiento-permiso/evaluacion-otorgamiento-permiso.component';
import { EvaluacionPlanGeneralManejoComponent } from '../../web/evaluacion/evaluacion-plan-general-manejo/evaluacion-plan-general-manejo.component';
import { EvaluacionPlanOperativoCcnnComponent } from '../../web/evaluacion/evaluacion-plan-operativo-ccnn/evaluacion-plan-operativo-ccnn.component';
import { EvaluacionCampo2Module } from '../evaluacion/evaluacion-campo2.module';
import { OpinionesModule } from '../evaluacion/opiniones.module';
import { ResultadoEvaluacionModule } from '../evaluacion/resultado-evaluacion.module';
import { MaterialModule } from '../material/material.module';
import { MensajeErrorControlModule } from '../mensaje-error-control.module';
import { BloqueQuinquenalComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/bloque-quinquenal/bloque-quinquenal.component';
import { ParcelaCortaComponent } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-manejo-bosque/components/parcela-corta/parcela-corta.component';
import { ModalInfoPermisoComponent } from 'src/app/shared/components/modal-info-permiso/modal-info-permiso.component';
import { ProgressBarModule } from 'primeng/progressbar';
import { ModalInfoEspacialEvalComponent } from 'src/app/web/evaluacion/modal-info-espacial-eval/modal-info-espacial-eval.component';
import {RequisitoPgmfSimpleModule} from '../../shared/components/requisitos-tupa/requisitos-tupa.module';
import { EvaluarImpugnacionSharedModule } from 'src/app/shared/components/evaluar-impugnacion-shared/evaluar-impugnacion-shared.module';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@NgModule({
  declarations: [
    PlanGeneralManejoComponent,
    TabResumenEjecutivoComponent,
    TabObjetivosManejoComponent,
    TabDuracionRvisionPlanComponent,
    TabInformacionAreaManejoComponent,
    TabOrdenamientoAreaManejoComponent,
    TabPotencialProduccionRfComponent,
    TabManejoBosqueComponent,
    TabProteccionBosqueComponent,
    TabMonitoreoComponent,
    TabParticipacionComunalComponent,
    TabCapacitacionComponent,
    TabOrganizacionDesarrolloActividadComponent,
    TabRentabilidadManejoForestalComponent,
    TabCronogramaComponent,
    TabAspectosComplementariosComponent,
    TabAnexosComponent,
    TabOpinionesComponent,
    TablaPotencialProduccionRfComponent,
    TablaActividadesOrdenamientoComponent,
    TablaEspeciesAprovecharComponent,
    TablaEspecificacionesCaminosComponent,
    TablaHidrografiaComponent,
    TablaAreaManejoDivididaComponent,
    TablaAreaManejoContinuaComponent,
    TablaEspeciesProtegerComponent,
    TablaMatrizAccesibilidadComponent,
    TablaFaunaSilvestreComponent,
    TablaFisiografiaComponent,
    BandejaPgmfa,
    ModalFormularioOrdenInternoComponent,
    ModalFormularioCronogramaComponent,
    ModalFormEspeciesComponent,
    EvaluacionPlanOperativoCcnnComponent,
    EvaluacionPlanGeneralManejoComponent,
    EvaluacionOtorgamientoPermisoComponent,
    TabResumenComponent,
    TablaConstruccionCaminosComponent,
    ModalFormularioReforestacionComponent,
    TablaManejoVerticesComponent,
    BloqueQuinquenalComponent,
    ParcelaCortaComponent,
    ModalInfoPermisoComponent,
    ModalInfoEspacialEvalComponent
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MensajeErrorControlModule,
        // material angular
        MaterialModule,

        //primeng
        ProgressBarModule,
        TabViewModule,
        ButtonModule,
        AccordionModule,
        CalendarModule,
        DropdownModule,
        TableModule,
        ListboxModule,
        CheckboxModule,
        RadioButtonModule,
        PanelModule,
        AutoCompleteModule,
        ToastModule,
        DialogModule,
        ConfirmDialogModule,
        ConfirmPopupModule,
        MultiSelectModule,
        OpinionesModule,
        EvaluacionCampo2Module,
        TooltipModule,
        FieldsetModule,
        SharedModule,
        EvaluacionArchivoModule,
        ResultadoEvaluacionModule,
        InputButtonsModule,
        DialogTipoBosqueModule,
        DialogTipoEspecieModule,
        InputImgButtonsModule,
        DialogTipoEspecieMultiselectFaunaModule,
        DialogTipoEspecieMultiselectModule,
        PaginatorModule,
        InputButtonsCodigoModule,
        InputButtonsMapsCodigoModule,
        RequisitoPgmfSimpleModule,
        EvaluarImpugnacionSharedModule,
    ],
  exports: [
    TabResumenEjecutivoComponent,
    TabObjetivosManejoComponent,
    TabDuracionRvisionPlanComponent,
    TabInformacionAreaManejoComponent,
    TabOrdenamientoAreaManejoComponent,
    TabPotencialProduccionRfComponent,
    TabManejoBosqueComponent,
    TabProteccionBosqueComponent,
    TabMonitoreoComponent,
    TabParticipacionComunalComponent,
    TabCapacitacionComponent,
    TabOrganizacionDesarrolloActividadComponent,
    TabRentabilidadManejoForestalComponent,
    TabCronogramaComponent,
    TabAspectosComplementariosComponent,
    TabAnexosComponent,
    TabOpinionesComponent,
    TablaPotencialProduccionRfComponent,
    TablaActividadesOrdenamientoComponent,
    TablaEspeciesAprovecharComponent,
    TablaEspecificacionesCaminosComponent,
    TablaHidrografiaComponent,
    TablaAreaManejoDivididaComponent,
    TablaAreaManejoContinuaComponent,
    TablaEspeciesProtegerComponent,
    TablaMatrizAccesibilidadComponent,
    TablaFaunaSilvestreComponent,
    TablaFisiografiaComponent,
    ModalFormularioOrdenInternoComponent,
    ModalFormEspeciesComponent,
    EvaluacionPlanOperativoCcnnComponent,
    EvaluacionPlanGeneralManejoComponent,
    TabResumenComponent,
    TablaConstruccionCaminosComponent,
    TablaManejoVerticesComponent,
    BloqueQuinquenalComponent,
    ParcelaCortaComponent,
    ModalInfoPermisoComponent,
    ModalInfoEspacialEvalComponent,
    
  ],
  providers: [
    DatePipe,
    ConfirmationService,
    MessageService,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PlanGeneralManejoModule {}
