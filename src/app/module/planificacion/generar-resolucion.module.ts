import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { GenerarResolucionComponent } from 'src/app/web/planificacion/bandeja-evaluacion-otorgamiento-tu/generar-resolucion/generar-resolucion.component';
import { ModalResolucionComponent } from 'src/app/web/planificacion/bandeja-evaluacion-otorgamiento-tu/modal-resolucion/modal-resolucion.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { SharedModule } from '@shared';

@NgModule({
  declarations: [GenerarResolucionComponent, ModalResolucionComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,

    BreadcrumbModule,
    ToastModule,
    InputTextareaModule,
    MatDatepickerModule,

    UploadInputButtonsModule,
    SharedModule
  ],
  exports: [ModalResolucionComponent],
})
export class GenerarResolucionModule {}
