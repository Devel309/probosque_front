import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDividerModule } from '@angular/material/divider';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CheckboxModule } from 'primeng/checkbox';
import { DividerModule } from 'primeng/divider';
import { FileUploadModule } from 'primeng/fileupload';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CalendarModule } from 'primeng/calendar';
import { ConsultaSolicitudComponent } from 'src/app/web/planificacion/ampliacion-solicitud/ampliacion-solicitud.component';
import { ModalNuevaConsultaSolicitudComponent } from 'src/app/web/planificacion/ampliacion-solicitud/modal/modal-ampliacion-solicitud/modal-ampliacion-solicitud.component';
import { ModalSolicitudPermisoForestalComponent } from 'src/app/web/planificacion/ampliacion-solicitud/modal/modal-solicitud-permiso-forestal/modal-solicitud-permiso-forestal.component';

@NgModule({
  declarations: [
    ConsultaSolicitudComponent,
    ModalNuevaConsultaSolicitudComponent,
    ModalSolicitudPermisoForestalComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    DialogModule,
    AccordionModule,
    PaginatorModule,
    RadioButtonModule,
    HttpClientModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    FormsModule,
    CheckboxModule,
    // componentes material
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    DividerModule,
    InputTextareaModule,
    CalendarModule,
    MatDatepickerModule,
  ],
  exports: [
    ConsultaSolicitudComponent,
    ModalNuevaConsultaSolicitudComponent,
    ModalSolicitudPermisoForestalComponent
  ]
})
export class AmpliacionSolicitudModule {}
