import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatDividerModule } from '@angular/material/divider';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CheckboxModule } from 'primeng/checkbox';
import { DividerModule } from 'primeng/divider';
import { FileUploadModule } from 'primeng/fileupload';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CalendarModule } from 'primeng/calendar';
import { ModalNuevaOposicionComponent } from 'src/app/web/planificacion/oposicion/modal-nueva-oposicion/modal-nueva-oposicion.component';
import { OposicionComponent } from 'src/app/web/planificacion/oposicion/oposicion.component';
import { FieldsetModule } from 'primeng/fieldset';
import { ListaOposicionComponent } from 'src/app/web/planificacion/oposicion/lista-oposicion/lista-oposicion.component';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { AnexoUploadInputButtonsModule } from 'src/app/shared/components/anexo-upload-input-button/anexo-upload-input-buttons.module';
import { SharedModule } from '@shared';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';

@NgModule({
  declarations: [
    OposicionComponent,
    ModalNuevaOposicionComponent,
    ListaOposicionComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    DialogModule,
    AccordionModule,
    PaginatorModule,
    RadioButtonModule,
    HttpClientModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    FormsModule,
    CheckboxModule,
    // componentes material
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    DividerModule,
    InputTextareaModule,
    CalendarModule,
    MatDatepickerModule,
    FieldsetModule,
    //
    UploadInputButtonsModule,
    AnexoUploadInputButtonsModule,
    SharedModule,
    CargaArchivoGeneralModule,
  ],
  exports: [
    CdkStepperModule,
    MatButtonModule,
    MatChipsModule,
    MatStepperModule,
    ModalNuevaOposicionComponent,
    ListaOposicionComponent
  ],
})
export class OposicionModule {}
