import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ToastModule } from 'primeng/toast';
import { BandejaProcesoConcesionPfdmComponent } from 'src/app/web/planificacion/bandeja-proceso-concesion-pfdm/bandeja-proceso-concesion-pfdm.component';
import { GenerarResolucionProcesoConcesionPfdmComponent } from 'src/app/web/planificacion/bandeja-proceso-concesion-pfdm/generar-resolucion-proceso-concesion-pfdm/generar-resolucion-proceso-concesion-pfdm.component';
import { ModalResolucionProcesoConcesionPfdmComponent } from 'src/app/web/planificacion/bandeja-proceso-concesion-pfdm/modal-resolucion-proceso-concesion-pfdm/modal-resolucion-proceso-concesion-pfdm.component';
import { EvaluarOtorgarProcesoConcesionPfdmComponent } from 'src/app/web/planificacion/bandeja-proceso-concesion-pfdm/evaluar-otorgar-proceso-concesion-pfdm/evaluar-otorgar-proceso-concesion-pfdm.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputNumberModule } from 'primeng/inputnumber';
import { EvaluacionContratoConcesionPfdmComponent } from 'src/app/web/planificacion/bandeja-proceso-concesion-pfdm/evaluacion-contrato-concesion-pfdm/evaluacion-contrato-concesion-pfdm.component';

@NgModule({
  declarations: [
    BandejaProcesoConcesionPfdmComponent,
    GenerarResolucionProcesoConcesionPfdmComponent,
    EvaluarOtorgarProcesoConcesionPfdmComponent,
    EvaluacionContratoConcesionPfdmComponent,
    ModalResolucionProcesoConcesionPfdmComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    RadioButtonModule,
    ToastModule,
    BreadcrumbModule,
    InputTextareaModule,
    InputNumberModule
  ],
  exports: [
    ModalResolucionProcesoConcesionPfdmComponent
  ],
})
export class BandejaProcesoConcesionPfdmModule {}
