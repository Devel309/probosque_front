import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEvaluacionCampoModule } from 'src/app/web/planificacion/evaluacion-campo/bandeja-evaluacion-campo/bandeja-evaluacion-campo.module';
import { RegistroEvaluacionCampoModule } from 'src/app/web/planificacion/evaluacion-campo/registro-evaluacion-campo/registro-evaluacion-campo.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BandejaEvaluacionCampoModule,
    RegistroEvaluacionCampoModule,
  ]
})
export class EvaluacionCampoModule { }
