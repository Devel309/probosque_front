import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { CheckboxModule } from 'primeng/checkbox';
import { CardModule } from 'primeng/card';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { SolicitudAccesoSharedModule } from '../solicitud-acceso-shared.module';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { DialogModule } from 'primeng/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EvaluarPermisoForestalCCNNComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal-ccnn/evaluar-permiso-forestal-ccnn.component';
import { DatosOtorgamientoComponent } from 'src/app/web/planificacion/evaluar-permiso-forestal-ccnn/datos-otorgamiento/datos-otorgamiento.component';
import { SolicitudPermisoForestalGeneral } from 'src/app/web/planificacion/evaluar-permiso-forestal-ccnn/tipo-registro/solicitud-permiso-forestal-general/solicitud-permiso-forestal-general.component';
import { ValidarRequisitosModal } from 'src/app/web/planificacion/evaluar-permiso-forestal-ccnn/modal/validar-requisitos-modal/validar-requisitos.modal';

@NgModule({
  declarations: [
    EvaluarPermisoForestalCCNNComponent,
    DatosOtorgamientoComponent,
    SolicitudPermisoForestalGeneral,
    ValidarRequisitosModal,
  ],
  imports: [
    SolicitudAccesoSharedModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    AutoCompleteModule,
    ToastModule,
    DialogModule,
    MatDividerModule,
    CardModule,
    MatDatepickerModule,
    ConfirmDialogModule,
  ],
  providers: [MessageService, ConfirmationService],
  exports: [
    DatosOtorgamientoComponent,
    SolicitudPermisoForestalGeneral,
  ],
})
export class EvaluarPermisoForestalCCNNModule {}
