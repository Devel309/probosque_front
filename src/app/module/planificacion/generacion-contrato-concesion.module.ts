import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { MatDividerModule } from '@angular/material/divider';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { CheckboxModule } from 'primeng/checkbox';
import { DividerModule } from 'primeng/divider';
import { FileUploadModule } from 'primeng/fileupload';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CalendarModule } from 'primeng/calendar';
import { FieldsetModule } from 'primeng/fieldset';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { GeneracionContratoConcesionComponent } from 'src/app/web/planificacion/generacion-contrato-concesion/generacion-contrato-concesion.component';
import { ModalGeneracionContratoConcesionComponent } from 'src/app/web/planificacion/generacion-contrato-concesion/modal-generacion-contrato-concesion/modal-generacion-contrato-concesion.component';
import { ModalContratoGeneradoConcesionComponent } from 'src/app/web/planificacion/generacion-contrato-concesion/modal-contrato-generado-concesion/modal-contrato-generado-concesion.component';
import { ModalInformacionConcesionComponent } from 'src/app/web/planificacion/generacion-contrato-concesion/modal-informacion-concesion/modal-informacion-concesion.component';
import { DetalleResultadoConcesionComponent } from 'src/app/web/planificacion/generacion-contrato-concesion/detalle-resultado-concesion/detalle-resultado-concesion.component';
@NgModule({
  declarations: [
    GeneracionContratoConcesionComponent,
    ModalGeneracionContratoConcesionComponent,
    DetalleResultadoConcesionComponent,
    ModalContratoGeneradoConcesionComponent,
    ModalInformacionConcesionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    DialogModule,
    AccordionModule,
    PaginatorModule,
    RadioButtonModule,
    HttpClientModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    FormsModule,
    CheckboxModule,
    // componentes material
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    DividerModule,
    InputTextareaModule,
    CalendarModule,
    MatDatepickerModule,
    FieldsetModule,
    BreadcrumbModule,
    DropdownModule,
  ],
  exports: [
    CdkStepperModule,
    MatButtonModule,
    MatChipsModule,
    MatStepperModule,
    ModalGeneracionContratoConcesionComponent,
    ModalContratoGeneradoConcesionComponent,
    ModalInformacionConcesionComponent
  ],
})
export class GeneracionContratoConcesionModule {}
