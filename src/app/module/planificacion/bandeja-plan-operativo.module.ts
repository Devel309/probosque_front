import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { PaginatorModule } from 'primeng/paginator';
import { BandejaSolicitudAcceso } from 'src/app/web/planificacion/bandeja-solicitud-acceso/bandeja-solicitud-acceso.component';
import { BandejaPlanOperativo } from 'src/app/web/planificacion/bandeja-plan-operativo/bandeja-plan-operativo.component';
import { ToastModule } from 'primeng/toast';
import { ModalPlanOperativoComponent } from 'src/app/web/planificacion/bandeja-plan-operativo/components/modal/modal-plan-operativo.component';

@NgModule({
  declarations: [BandejaPlanOperativo, ModalPlanOperativoComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    //primeng
    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    ToastModule,
    DropdownModule,
  ],
  exports: [ModalPlanOperativoComponent],
})
export class BandejaPlanOperativoModule {}
