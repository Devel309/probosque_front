import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ConfirmationService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';
import { FieldsetModule } from 'primeng/fieldset';
import { MultiSelectModule } from 'primeng/multiselect';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { RequisitoPgmfSimpleModule } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.module';
import { TextColorEstadoModule } from 'src/app/shared/components/text-color-estado/text-color-estado.module';
import { BandejaPmfi } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/bandeja-pmficomponent';
import { FormulacionPMFIConcesionPFDMComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/formulacion-pmfi-concesion-pfdm.component';
import { PmfiAnexosComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-anexos/pmfi-anexos.component';
import { PmfiModalFormularioCronogramaComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-cronograma/modal/modal-formulario-zonas/modal-formulario-cronograma.component';
import { PmfiCronogramaComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-cronograma/pmfi-cronograma.component';
import { ModalMaderablesComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-especies-recursos/modals/modal-formulario-zonas/modal-formulario-maderables.component';
import { PmfiEspeciesRecursosComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-especies-recursos/pmfi-especies-recursos.component';
import { PmfiImpactoAmbientalNegativoComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-impacto-ambiental-negativo/pmfi-impacto-ambiental-negativo.component';
import { ParcelaCortaComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-informacion-area/components/parcela-corta/parcela-corta.component';
import { ModalFormEspeciesComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-informacion-area/modal-form-especies/modal-form-especies.component';
import { PmfiInformacionAreaComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-informacion-area/pmfi-informacion-area.component';
import { ModalInfoContratoComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-informacion-general/modal-info-contrato/modal-info-contrato.component';
import { PmfiInformacionGeneralComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-informacion-general/pmfi-informacion-general.component';
import { AprovechamientoFormComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-manejo-aprovechamiento/aprovechamiento-form/aprovechamiento-form.component';
import { LaboresSilviculturalesFormComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-manejo-aprovechamiento/labores-silviculturales-form/labores-silviculturales-form.component';
import { PmfiManejoAprovechamientoComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-manejo-aprovechamiento/pmfi-manejo-aprovechamiento.component';
import { RegistroModificacionModalComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-manejo-aprovechamiento/registro-modificacion-modal/registro-modificacion-modal.component';
import { PmfiMedidaProteccionInpactoAmbientalComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-medida-proteccion-inpacto-ambiental/pmfi-medida-proteccion-inpacto-ambiental.component';
import { PmfiObjetivosComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-objetivos/pmfi-objetivos.component';
import { ModalFormularioOrdenInternoComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-ordenamiento-interno/modal-formulario-orden-interno/modal-formulario-orden-interno.component';
import { PmfiOrdenamientoInternoComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-ordenamiento-interno/pmfi-ordenamiento-interno.component';
import { PmfiResumenComponent } from 'src/app/web/planificacion/formulacion-pmfi-concesion-pfdm/tabs/pmfi-resumen/pmfi-resumen.component';
import { EvaluacionArchivoModule } from '../../shared/components/evaluacion-archivo/evaluacion-archivo.module';
import { SolicitudSanSharedModule } from '../../shared/components/solicitud-san-shared/solicitud-san-shared.module';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@NgModule({
  declarations: [
    FormulacionPMFIConcesionPFDMComponent,
    BandejaPmfi,
    PmfiInformacionGeneralComponent,
    PmfiInformacionAreaComponent,
    PmfiOrdenamientoInternoComponent,
    ModalFormularioOrdenInternoComponent,
    PmfiEspeciesRecursosComponent,
    PmfiManejoAprovechamientoComponent,
    PmfiMedidaProteccionInpactoAmbientalComponent,
    PmfiCronogramaComponent,
    PmfiAnexosComponent,
    PmfiResumenComponent,
    AprovechamientoFormComponent,
    LaboresSilviculturalesFormComponent,
    RegistroModificacionModalComponent,
    PmfiObjetivosComponent,
    PmfiImpactoAmbientalNegativoComponent,
    ModalFormEspeciesComponent,
    PmfiModalFormularioCronogramaComponent,
    ModalInfoContratoComponent,
    ParcelaCortaComponent,
    ModalMaderablesComponent
    
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        // material angular
        MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        //primeNG
        ToastModule,
        ConfirmPopupModule,
        DropdownModule,
        TableModule,
        PaginatorModule,
        DialogModule,
        ButtonModule,
        MultiSelectModule,
        AccordionModule,
        RadioButtonModule,
        FieldsetModule,
        CheckboxModule,
        PanelModule,
        EditorModule,
        AutoCompleteModule,
        ConfirmDialogModule,
        SharedModule,
        RequisitoPgmfSimpleModule,
        InputButtonsCodigoModule,
        SolicitudSanSharedModule,
        TextColorEstadoModule,
        EvaluacionArchivoModule,
        CalendarModule,

    ],
  exports:[
    PmfiInformacionGeneralComponent,
    PmfiInformacionAreaComponent,
    PmfiOrdenamientoInternoComponent,
    PmfiEspeciesRecursosComponent,
    PmfiManejoAprovechamientoComponent,
    PmfiMedidaProteccionInpactoAmbientalComponent,
    PmfiCronogramaComponent,
    PmfiAnexosComponent,
    PmfiResumenComponent,
    PmfiObjetivosComponent,
    PmfiImpactoAmbientalNegativoComponent,
    ModalInfoContratoComponent,
    ParcelaCortaComponent
  ],
  providers:[
    ConfirmationService,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class FormulacionPMFIConcesionPFDMModule { }
