import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluacionContratoConcesionComponent } from 'src/app/web/planificacion/evaluacion-contrato-concesion/evaluacion-contrato-concesion.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputNumberModule } from 'primeng/inputnumber';
import { RadioButtonModule } from 'primeng/radiobutton';



@NgModule({
  declarations: [
    EvaluacionContratoConcesionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    DropdownModule,
    TableModule,
    ButtonModule,
    InputNumberModule,
    RadioButtonModule
  ]
})
export class EvaluacionContratoConcesionModule { }
