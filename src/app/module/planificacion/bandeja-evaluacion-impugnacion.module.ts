import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEvaluacionImpugnacionComponent } from 'src/app/web/planificacion/Impugnacion/bandeja-evaluacion-impugnacion/bandeja-evaluacion-impugnacion.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { BandejaImpugnacionModule } from './bandeja-impugnacion.module';
import { ImpugnacionRegistroModule } from 'src/app/shared/components/impugnacion-registro/impugnacion-registro.module';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { ToastModule } from 'primeng/toast';



@NgModule({
  declarations: [BandejaEvaluacionImpugnacionComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    ButtonModule,
    CheckboxModule,
    RadioButtonModule,
    BandejaImpugnacionModule,
    ImpugnacionRegistroModule,
    UploadInputButtonsModule,
    ToastModule,
  ]
})
export class BandejaEvaluacionSolicitudModule { }
