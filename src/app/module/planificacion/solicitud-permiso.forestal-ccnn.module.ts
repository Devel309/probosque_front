import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { CheckboxModule } from 'primeng/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SolicitudPermisoForestalCCNNComponent } from 'src/app/web/planificacion/solicitud-permiso-forestal-ccnn/solicitud-permiso-forestal-ccnn.component';



@NgModule({
  declarations: [
    SolicitudPermisoForestalCCNNComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    DropdownModule,
    TableModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    MatDatepickerModule
  ]
})
export class SolicitudPermisoForestalCCNNModule { }
