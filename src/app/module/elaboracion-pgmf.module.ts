import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';

import {PanelModule} from 'primeng/panel';
import { AccordionModule } from 'primeng/accordion';
import {FieldsetModule} from 'primeng/fieldset';
import {TableModule } from 'primeng/table';
import {DropdownModule } from 'primeng/dropdown';

import {MatStepperModule} from '@angular/material/stepper';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { MatIconModule} from '@angular/material/icon';

import {ElaboracionPgmfComponent} from '../web/planificacion/elaboracion-pgmf/elaboracion-pgmf.component';
import {StepperComponent} from '../web/planificacion/elaboracion-pgmf/stepper/stepper.component';






@NgModule({
  declarations: [
    ElaboracionPgmfComponent,
    StepperComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,

    PanelModule,
    FieldsetModule,
    AccordionModule,
    TableModule,
    DropdownModule,

    MatStepperModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    BrowserAnimationsModule
  ],
  exports: [
    StepperComponent
  ]
})
export class ElaboracionPgmfModule { }
