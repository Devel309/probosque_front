export class PccActividadesSilviculturales {
  constructor(data?: any) {
    if (data) {
      this.idActividadSilviculturalDet = data.idActividadSilviculturalDet
        ? data.idActividadSilviculturalDet
        : 0;
      this.idTipo = data.idTipo ? data.idTipo : '';
      this.idTipoTratamiento = data.idTipoTratamiento
        ? data.idTipoTratamiento
        : '';
      this.tipoTratamiento = data.tipoTratamiento ? data.tipoTratamiento : null;
      this.accion = data.accion ? data.accion : 0;
      this.descripcionDetalle = data.descripcionDetalle
        ? data.descripcionDetalle
        : null;
      this.tratamiento = data.tratamiento ? data.tratamiento : '';
      this.justificacion = data.justificacion ? data.justificacion : null;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
    }
  }

  idActividadSilviculturalDet: number = 0;
  idTipo: string = '';
  idTipoTratamiento: string = '';
  tipoTratamiento: string | null = null;
  accion: number = 0;
  descripcionDetalle: string | null = null;
  tratamiento: string = '';
  justificacion: string | null = null;
  idUsuarioRegistro: number = 0;
}


export class PccActividadesSilviculturalesCabecera {
  constructor(data?: any) {
    if (data) {
      this.idActSilvicultural = data.idActSilvicultural
        ? data.idActSilvicultural
        : 0;
      this.actividad = data.actividad ? data.actividad : '';
      this.anexo = data.anexo
        ? data.anexo
        : null;
      this.codigoTipoActSilvicultural = data.codigoTipoActSilvicultural ? data.codigoTipoActSilvicultural : '';
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.listActividadSilvicultural = data.listActividadSilvicultural
        ? data.listActividadSilvicultural
        : [];
      this.monitoreo = data.monitoreo ? data.monitoreo : '';
      this.observacion = data.observacion ? data.observacion : null;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
    }
  }

  idActSilvicultural: number = 0;
  actividad: string | null = null;
  anexo: string | null = null;
  codigoTipoActSilvicultural: string | null = '';
  idPlanManejo: number = 0;
  idUsuarioRegistro: number = 0;

  listActividadSilvicultural: PccActividadesSilviculturales[] = [];
  monitoreo: string = '';
  observacion: string = '';
}
