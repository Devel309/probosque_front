import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MantenimientoPerfilComponent } from '../web/seguridad/mantenimiento-perfil/mantenimiento-perfil.component';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import { ConfirmationService } from 'primeng/api';
import { MantenimientoAplicacionComponent } from '../web/seguridad/mantenimiento-aplicacion/mantenimiento-aplicacion.component';
import { MantenimientoAccionesComponent } from '../web/seguridad/mantenimiento-acciones/mantenimiento-acciones.component';
import { MantenimientoOpcionesComponent } from '../web/seguridad/mantenimiento-opciones/mantenimiento-opciones.component';
import { MantenimientoUsuariosComponent } from '../web/seguridad/mantenimiento-usuarios/mantenimiento-usuarios.component';
import { MantenimientoEntidadesComponent } from '../web/seguridad/mantenimiento-entidades/mantenimiento-entidades.component';
import { ProcesoPerfilAplicacionComponent } from '../web/seguridad/proceso-perfil-aplicacion/proceso-perfil-aplicacion.component';
import { ProcesoAccionOpcionComponent } from '../web/seguridad/proceso-accion-opcion/proceso-accion-opcion.component';
import { ProcesoAplicacionPerfilUsuarioComponent } from '../web/seguridad/proceso-aplicacion-perfil-usuario/proceso-aplicacion-perfil-usuario.component';
import { ProcesoAccionOpcionAplicacionPerfilComponent } from '../web/seguridad/proceso-accion-opcion-aplicacion-perfil/proceso-accion-opcion-aplicacion-perfil.component';
import { ToastModule } from 'primeng/toast';
//import { MensajeErrorControlComponent } from '../components/mensaje-error-control/mensaje-error-control.component';
import { MensajeErrorControlModule } from './mensaje-error-control.module';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { AuthGuard } from '../web/login/AuthGuard';
import { RouterModule, Routes } from '@angular/router';
import {PasswordModule} from 'primeng/password';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { SharedModule } from '../shared/shared.module';
import { MantenimientoParametrosComponent } from '../web/seguridad/mantenimiento-parametros/mantenimiento-parametros.component';
import { ProcesoPermisoPerfilComponent } from '../web/seguridad/proceso-permiso-perfil/proceso-permiso-perfil.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { MatInputModule } from '@angular/material/input';








const routes: Routes = [
{
  path: 'seguridad/mantenimiento/perfiles',
  component: MantenimientoPerfilComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/mantenimiento/aplicaciones',
  component: MantenimientoAplicacionComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/mantenimiento/acciones',
  component: MantenimientoAccionesComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/mantenimiento/opciones',
  component: MantenimientoOpcionesComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/mantenimiento/usuarios',
  component: MantenimientoUsuariosComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/mantenimiento/entidades',
  component: MantenimientoEntidadesComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/mantenimiento/parametros',
  component: MantenimientoParametrosComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/proceso/perfil-aplicacion',
  component: ProcesoPerfilAplicacionComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/proceso/accion-opcion',
  component: ProcesoAccionOpcionComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/proceso/aplicacion-perfil-usuario',
  component: ProcesoAplicacionPerfilUsuarioComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/proceso/accion-opcion-aplicacion-perfil',
  component: ProcesoAccionOpcionAplicacionPerfilComponent,canActivate: [AuthGuard] 
},
{
  path: 'seguridad/proceso/permiso-perfil',
  component: ProcesoPermisoPerfilComponent,canActivate: [AuthGuard] 
}

];

@NgModule({
  declarations: [
    MantenimientoPerfilComponent,
    MantenimientoAplicacionComponent,
    MantenimientoAccionesComponent,
    MantenimientoOpcionesComponent,
    MantenimientoUsuariosComponent,
    MantenimientoEntidadesComponent,
    MantenimientoParametrosComponent,
    ProcesoPerfilAplicacionComponent,
    ProcesoAccionOpcionComponent,
    ProcesoAplicacionPerfilUsuarioComponent,
    ProcesoAccionOpcionAplicacionPerfilComponent,
    ProcesoPermisoPerfilComponent,
    
  ],
  imports: [
    RouterModule.forRoot(routes),
    MensajeErrorControlModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    ToastModule,
    ConfirmPopupModule,
    DropdownModule,
    TableModule,
    PaginatorModule,
    DialogModule,
    ButtonModule,
    RadioButtonModule,
    PasswordModule,
    AutoCompleteModule,
    CheckboxModule,
    MultiSelectModule,
    SharedModule,
    MatInputModule
  ],
  exports: [RouterModule],
  providers:[ConfirmationService]
})
export class MantenimientoPerfilModule { }
