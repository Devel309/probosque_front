import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MaterialModule } from '../material/material.module';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ToastModule } from 'primeng/toast';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { SharedModule } from '@shared';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SolicitudOpinionLineamientoModule } from 'src/app/shared/components/solicitud-opinion-lineamiento/solicitud-opinion-lineamiento.module';
import { InputNumberModule } from 'primeng/inputnumber';
import { CalificacionConcesionPFDMComponent } from 'src/app/web/concesion/calificacion-concesion-pfdm/calificacion-concesion-pfdm.component';
import { EvaluarPropuestaCalificacionConcesionPfdmComponent } from 'src/app/web/concesion/calificacion-concesion-pfdm/evaluar-propuesta-calificacion/evaluar-propuesta-calificacion-concesion-pfdm.component';

@NgModule({
  declarations: [
    CalificacionConcesionPFDMComponent,
    EvaluarPropuestaCalificacionConcesionPfdmComponent
  ],
  imports: [
    SolicitudOpinionLineamientoModule,
    CommonModule,
    FormsModule,
    // PrimeNG modules
    TableModule,
    CardModule,
    AccordionModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    RadioButtonModule,
    OverlayPanelModule,
    TabViewModule,
    ButtonModule,
    TableModule,
    ConfirmPopupModule,
    ToastModule,
    PaginatorModule,
    InputTextareaModule,
    UploadInputButtonsModule,
    InputNumberModule,

    // Modulos material angular
    MaterialModule,

    SharedModule,
    CargaArchivoGeneralModule,

  ],
  exports: [

  ],
  // providers:[ConfirmationService]
})
export class CalificacionConcesionPFDMModule { }
