import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitudConcesionPFDMComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/solicitud-concesion-pfdm.component';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { Anexo1PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo1-pfdn/anexo1-pfdn.component';
import { Anexo2PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo2-pfdn/anexo2-pfdn.component';
import { Anexo3PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo3-pfdn/anexo3-pfdn.component';
import { Anexo4PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo4-pfdn/anexo4-pfdn.component';
import { Anexo5PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo5-pfdn/anexo5-pfdn.component';
import { Anexo6PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo6-pfdn/anexo6-pfdn.component';
import { Anexo7PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo7-pfdn/anexo7-pfdn.component';
import { RegistroSolicitudConcesionPfdmComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm.component';
import { CardModule } from 'primeng/card';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MaterialModule } from '../material/material.module';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ToastModule } from 'primeng/toast';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { SharedModule } from '@shared';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';
import { ModalRequisitosComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/modales/modal-requisitos/modal-requisitos.component';
import { EvaluarSolicitudComponent } from 'src/app/web/concesion/components/evaluar-solicitud/evaluar-solicitud.component';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EvaluarSolicitudLineamientoComponent } from 'src/app/web/concesion/components/evaluar-solicitud-lineamiento/evaluar-solicitud-lineamiento.component';
import { ModalOpinionComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/modales/modal-opinion/modal-opinion.component';
import { Anexo8PfdnComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/tabs/anexo8-pfdn/anexo8-pfdn.component';
import { SolicitudOpinionLineamientoModule } from 'src/app/shared/components/solicitud-opinion-lineamiento/solicitud-opinion-lineamiento.module';
import { EvaluacionPropuestaComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/evaluacion-propuesta/evaluacion-propuesta.component';
import { InputNumberModule } from 'primeng/inputnumber';
import { ModalNuevaSolicitudComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/modales/modal-nueva-solicitud/modal-nueva-solicitud.component';
import { AnexoFormatoPropuestaComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/anexo-formato-propuesta/anexo-formato-propuesta.component';
import { ModalEvidenciaComponent } from 'src/app/web/concesion/solicitud-concesion-pfdm/modales/modal-evidencia/modal-evidencia.component';

@NgModule({
  declarations: [
    SolicitudConcesionPFDMComponent,
    RegistroSolicitudConcesionPfdmComponent,
    EvaluacionPropuestaComponent,
    ModalRequisitosComponent,
    ModalOpinionComponent,
    ModalNuevaSolicitudComponent,
    ModalEvidenciaComponent,
    Anexo1PfdnComponent,
    Anexo2PfdnComponent,
    Anexo3PfdnComponent,
    Anexo4PfdnComponent,
    Anexo5PfdnComponent,
    Anexo6PfdnComponent,
    Anexo7PfdnComponent,
    Anexo8PfdnComponent,
    EvaluarSolicitudComponent,
    EvaluarSolicitudLineamientoComponent,
    AnexoFormatoPropuestaComponent,
  ],
  imports: [
    SolicitudOpinionLineamientoModule,
    CommonModule,
    FormsModule,
    // PrimeNG modules
    TableModule,
    CardModule,
    AccordionModule,
    DialogModule,
    DropdownModule,
    CheckboxModule,
    RadioButtonModule,
    OverlayPanelModule,
    TabViewModule,
    ButtonModule,
    TableModule,
    ConfirmPopupModule,
    ToastModule,
    PaginatorModule,
    InputTextareaModule,
    UploadInputButtonsModule,
    InputNumberModule,

    // Modulos material angular
    MaterialModule,

    SharedModule,
    CargaArchivoGeneralModule,



  ],
  exports: [
    Anexo1PfdnComponent,
    Anexo2PfdnComponent,
    Anexo3PfdnComponent,
    Anexo4PfdnComponent,
    Anexo5PfdnComponent,
    Anexo6PfdnComponent,
    Anexo7PfdnComponent,
    Anexo8PfdnComponent,
    EvaluarSolicitudComponent,
    EvaluarSolicitudLineamientoComponent,
    ModalRequisitosComponent,
    ModalOpinionComponent,
    ModalNuevaSolicitudComponent,
    ModalEvidenciaComponent,
    AnexoFormatoPropuestaComponent,
  ],
  // providers:[ConfirmationService]
})
export class SolicitudConcesionPFDMModule { }
