import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
  MatDialogModule
} from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatSliderModule } from '@angular/material/slider';
import { MatStepperModule } from '@angular/material/stepper';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DividerModule } from 'primeng/divider';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RippleModule } from 'primeng/ripple';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { TooltipModule } from 'primeng/tooltip';
import { AppRoutingModule } from '../app-routing.module';
import { AnexoUploadInputButtonsModule } from '../shared/components/anexo-upload-input-button/anexo-upload-input-buttons.module';
import { SolicitudOpinionLineamientoModule } from '../shared/components/solicitud-opinion-lineamiento/solicitud-opinion-lineamiento.module';
import { UploadInputButtonsModule } from '../shared/components/upload-input-buttons/upload-input-buttons.module';
import { BandejaPostulacionComponent } from '../web/planificacion/bandeja-postulacion/bandeja-postulacion.component';
import { CargarResolucionComponent } from '../web/planificacion/bandeja-postulacion/cargar-resolucion/cargar-resolucion.component';
import { DatosCancelacionComponent } from '../web/planificacion/bandeja-postulacion/datos-cancelacion/datos-cancelacion.component';
import { ModalDocumentosComponent } from '../web/planificacion/bandeja-postulacion/modal-documentos/modal-documentos.component';
import { ModalFileListComponent } from '../web/planificacion/bandeja-postulacion/modal-file-list/modal-file-list.component';
import { ModalRecepcionComponent } from '../web/planificacion/bandeja-postulacion/modal-recepcion/modal-recepcion.component';
import { RevisarComponent } from '../web/planificacion/bandeja-postulacion/revisar/revisar.component';
import { EvaluacionRecursoInpugnacionComponent } from '../web/planificacion/Impugnacion/evaluacion-recurso-inpugnacion/evaluacion-recurso-inpugnacion.component';
import { SolicitudRecursoInpugnacionComponent } from '../web/planificacion/Impugnacion/solicitar-recurso-inpugnacion/solicitar-recurso-inpugnacion.component';
import { LanzarPostulacionComponent } from '../web/planificacion/lanzar-postulacion/lanzar-postulacion.component';
import { ModalRegistroSolicitudesComponent } from '../web/planificacion/otorgamiento-derechos/modal-registro-solicitudes/modal-registro-solicitudes.component';
import {
  ConsultarEntidadComponent,
  NuevoOtorgamientoDerechoComponent
} from '../web/planificacion/otorgamiento-derechos/nuevo-otorgamiento-derecho/nuevo-otorgamiento-derecho.component';
import { OtorgamientoDerechosComponent } from '../web/planificacion/otorgamiento-derechos/otorgamiento-derechos.component';
import { Anexo1 } from '../web/planificacion/procesos-ofertados/anexos/anexo1';
import { Anexo2 } from '../web/planificacion/procesos-ofertados/anexos/anexo2';
import { Anexo3 } from '../web/planificacion/procesos-ofertados/anexos/anexo3';
import { Anexo4 } from '../web/planificacion/procesos-ofertados/anexos/anexo4';
import { AnexoDeclaracionJuradaComponent } from '../web/planificacion/procesos-ofertados/anexos/declaracion-jurada';
import { AnexosOtrosComponent } from '../web/planificacion/procesos-ofertados/anexos/otros';
import { ModalObservacionComponent } from '../web/planificacion/procesos-ofertados/modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../web/planificacion/procesos-ofertados/modal-validar-anexo/modal-validar-anexo.component';
import { PostularOfertaComponent } from '../web/planificacion/procesos-ofertados/postular-oferta/postular-oferta.component';
import { ProcesosOfertadosComponent } from '../web/planificacion/procesos-ofertados/procesos-ofertados.component';
import { ModalAutorizacionPublicacionComponent } from '../web/planificacion/bandeja-postulacion/modal-autorizacion-publicacion/modal-autorizacion-publicacion.component';




@NgModule({
  declarations: [
    OtorgamientoDerechosComponent,
    LanzarPostulacionComponent,
    NuevoOtorgamientoDerechoComponent,
    ConsultarEntidadComponent,
    ProcesosOfertadosComponent,
    BandejaPostulacionComponent,
    RevisarComponent,
    //anexos
    Anexo1,
    Anexo2,
    Anexo3,
    Anexo4,
    AnexoDeclaracionJuradaComponent,
    AnexosOtrosComponent,
    PostularOfertaComponent,
    ModalObservacionComponent,
    ModalValidarAnexoComponent,
    ModalRecepcionComponent,
    ModalDocumentosComponent,
    EvaluacionRecursoInpugnacionComponent,
    SolicitudRecursoInpugnacionComponent,
    ModalRegistroSolicitudesComponent,
    DatosCancelacionComponent,
    ModalFileListComponent,
    CargarResolucionComponent,
    ModalAutorizacionPublicacionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    DialogModule,
    AccordionModule,
    PaginatorModule,
    RadioButtonModule,
    HttpClientModule,
    ToastModule,
    ButtonModule,
    RippleModule,
    FileUploadModule,
    FormsModule,
    CheckboxModule,
    ConfirmPopupModule,
    FieldsetModule,

    // componentes material
    MatSliderModule,
    MatDialogModule,
    MatButtonModule,
    MatChipsModule,
    MatDividerModule,
    DividerModule,
    TooltipModule,
    ConfirmDialogModule,
    CalendarModule,
    MatDatepickerModule,
    SolicitudOpinionLineamientoModule,
    SharedModule,

    UploadInputButtonsModule,
    AnexoUploadInputButtonsModule,
    PanelModule
  ],
  exports: [
    CdkStepperModule,
    MatButtonModule,
    MatChipsModule,
    MatStepperModule,
    ModalObservacionComponent,
    ModalValidarAnexoComponent,
    NuevoOtorgamientoDerechoComponent,
    ConsultarEntidadComponent,
    ModalRecepcionComponent,
    ModalDocumentosComponent,
    ModalRegistroSolicitudesComponent,
    ModalFileListComponent,
    ModalAutorizacionPublicacionComponent
  ],
})
export class PlanificacionModule { }
