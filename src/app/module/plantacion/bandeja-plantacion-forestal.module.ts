import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { PaginatorModule } from 'primeng/paginator';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { ConfirmationService } from 'primeng/api';
import { MensajeErrorControlModule } from '../mensaje-error-control.module';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import {MultiSelectModule} from 'primeng/multiselect';
import { AccordionModule } from 'primeng/accordion';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CheckboxModule } from 'primeng/checkbox';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { SharedModule } from '@shared';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { AutoCompleteModule } from 'primeng/autocomplete';

import { BandejaPlantacionForestalComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/bandeja-plantacion-forestal.component';
import { RegistroPlantacionForestalComponent } from '../../web/plantacion/bandeja-plantacion-forestal/registro-plantacion-forestal/registro-plantacion-forestal.component';
import { InformacionSolicitanteComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/informacion-solicitante/informacion-solicitante.component';
import { InformacionGeneralAreaPlantadaComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/informacion-general-area-plantada/informacion-general-area-plantada.component';
import { AnexoComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/anexo/anexo.component';
import { ResultadosEvaluacionComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/resultados-evaluacion/resultados-evaluacion.component';
import { ObservacionesComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/observaciones/observaciones.component';
import { TabAnexoDosComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/tab-anexo-dos/tab-anexo-dos.component';
import { InformacionDelAreaComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/informacion-del-area/informacion-del-area.component';
import { DetallePlantacionForestalComponent } from '../../web/plantacion/bandeja-plantacion-forestal/tab/detalle-plantacion-forestal/detalle-plantacion-forestal.component';
import { ModalRequisitosBpfComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/components/modal-requisitos-bpf/modal-requisitos-bpf.component';
import { EvaluarSolicitudBpfComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/components/evaluar-solicitud-bpf/evaluar-solicitud-bpf.component';
import { ModalFaunaBpfComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/components/modal-fauna-bpf/modal-fauna-bpf.component';
import { CargaArchivoGeneralModule } from 'src/app/shared/components/formulario/carga-archivo-general/carga-archivo-general.module';
import { NuevaPlantacionForestalComponent } from 'src/app/web/plantacion/modales/nueva-plantacion-forestal/nueva-plantacion-forestal.component';
import { TooltipModule } from 'primeng/tooltip';
import { FiscalizacionPlantacionForestalComponent } from 'src/app/web/plantacion/fiscalizacion-plantacion-forestal/fiscalizacion-plantacion-forestal.component';
import { ModalRequisitosFpfComponent } from 'src/app/web/plantacion/fiscalizacion-plantacion-forestal/components/modal-requisitos-fpf/modal-requisitos-fpf.component';
import { RegistroEvaluacionCampoModule } from 'src/app/web/planificacion/evaluacion-campo/registro-evaluacion-campo/registro-evaluacion-campo.module';
import { EvaluacionCampoFpfComponent } from 'src/app/web/plantacion/fiscalizacion-plantacion-forestal/evaluacion-campo-fpf/evaluacion-campo-fpf.component';
import { ModalNotificarPasBpfComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/components/modal-notificar-pas-bpf/modal-notificar-pas-bpf.component';
import { ModalGenerarCertificadoBpfComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/components/modal-generar-certificado-bpf/modal-generar-certificado-bpf.component';
import { ModalFamiliaresBpfComponent } from 'src/app/web/plantacion/bandeja-plantacion-forestal/components/modal-familiares-bpf/modal-familiares-bpf.component';
// import { TablaDocumentosComponent } from '../../web/planificacion/registro-plantacion-forestal/components/tabla-documentos/tabla-documentos.component';
// import { ObservacionesPlantacionForestalComponent } from '../../web/planificacion/registro-plantacion-forestal/components/observaciones/observaciones.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@NgModule({
  declarations: [
    BandejaPlantacionForestalComponent,
    FiscalizacionPlantacionForestalComponent,
    EvaluacionCampoFpfComponent,
    RegistroPlantacionForestalComponent,
    InformacionSolicitanteComponent,
    InformacionGeneralAreaPlantadaComponent,
    AnexoComponent,
    ResultadosEvaluacionComponent,
    ObservacionesComponent,
    TabAnexoDosComponent,
    InformacionDelAreaComponent,
    DetallePlantacionForestalComponent,
    ModalRequisitosBpfComponent,
    EvaluarSolicitudBpfComponent,
    ModalFaunaBpfComponent,
    // TablaDocumentosComponent,
    // ObservacionesPlantacionForestalComponent
    NuevaPlantacionForestalComponent,
    ModalRequisitosFpfComponent,
    ModalNotificarPasBpfComponent,
    ModalGenerarCertificadoBpfComponent,
    ModalFamiliaresBpfComponent,
  ],
  imports: [
    NgxMaskModule.forRoot(),
    MensajeErrorControlModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    MatTabsModule,
    //primeNG
    RadioButtonModule,
    ToastModule,
    ConfirmPopupModule,
    DropdownModule,
    TableModule,
    PaginatorModule,
    DialogModule,
    ButtonModule,
    MultiSelectModule,
    CheckboxModule,
    AccordionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    InputButtonsCodigoModule,
    InputTextareaModule,
    SharedModule,
    FieldsetModule,
    AutoCompleteModule,
    TooltipModule,

    CargaArchivoGeneralModule,
    RegistroEvaluacionCampoModule,
  ],
  exports: [
    BandejaPlantacionForestalComponent,
    FiscalizacionPlantacionForestalComponent,
    EvaluacionCampoFpfComponent,
    RegistroPlantacionForestalComponent,
    InformacionSolicitanteComponent,
    ResultadosEvaluacionComponent,
    InformacionGeneralAreaPlantadaComponent,
    AnexoComponent,
    ObservacionesComponent,
    TabAnexoDosComponent,
    ModalRequisitosBpfComponent,
    EvaluarSolicitudBpfComponent,
    ModalFaunaBpfComponent,
    ModalRequisitosFpfComponent,
    ModalNotificarPasBpfComponent,
    ModalGenerarCertificadoBpfComponent,
    ModalFamiliaresBpfComponent,
    // TablaDocumentosComponent
  ],
  providers:[ConfirmationService,
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },]
})
export class BandejaPlantacionForestalModule { }
