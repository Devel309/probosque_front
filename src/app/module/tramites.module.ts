import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TramitesComponent } from '../web/tramites/tramites.component';

import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import {PaginatorModule} from 'primeng/paginator';



@NgModule({
  declarations: [
    TramitesComponent
  ],
  imports: [
    CommonModule,
    DropdownModule,
    TableModule,
    PaginatorModule
  ]
})
export class TramitesModule { }
