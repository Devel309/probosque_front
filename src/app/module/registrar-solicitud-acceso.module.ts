import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';

import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import {MessageService} from "primeng/api";

import { RegistrarSolicitudAcessoComponent } from '../web/registrar-solicitud-acesso/registrar-solicitud-acesso.component';
import { SolicitudAccesoSharedModule } from './solicitud-acceso-shared.module';
import {ToastModule} from "primeng/toast";


@NgModule({
  declarations: [
    RegistrarSolicitudAcessoComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,

    SolicitudAccesoSharedModule,
    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    DropdownModule,
    ToastModule




  ],
  exports:[

  ],
  providers:[
    MessageService
  ]

})
export class RegistrarSolicitudAccesoModule { }
