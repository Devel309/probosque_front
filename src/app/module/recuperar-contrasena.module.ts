import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { RecuperarContrasenaComponent } from '../web/recuperar-contrasena/recuperar-contrasena.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {ToastModule} from 'primeng/toast';
import { PasswordModule } from 'primeng/password';
import { MensajeErrorControlModule } from './mensaje-error-control.module';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    RecuperarContrasenaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ToastModule,
    PasswordModule,
    MensajeErrorControlModule,
    MatInputModule
  ]
})
export class RecuperarContrasenaModule { }
