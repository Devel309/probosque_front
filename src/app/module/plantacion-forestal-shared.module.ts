import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RpfInformacionSolicitanteComponent } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-informacion-solicitante/rpf-informacion-solicitante.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/fieldset';
import { RpfAreaComponent } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-area/rpf-area.component';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RpfAreaPlantadaComponent } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-area-plantada/rpf-area-plantada.component';
import { RpfDetallePlantacion1Component } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-detalle-plantacion1/rpf-detalle-plantacion1.component';
import { RpfDetallePlantacion2Component } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-detalle-plantacion2/rpf-detalle-plantacion2.component';
import { RpfAnexoComponent } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-anexo/rpf-anexo.component';
import { FileUploadModule } from 'primeng/fileupload';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ObservacionesPlantacionForestalComponent } from '../web/planificacion/registro-plantacion-forestal/components/observaciones/observaciones.component';
import { RpfObservacionesComponent } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-observaciones/rpf-observaciones.component';
import { RpfProcedimientoAdministrativoSancionadorComponent } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-procedimiento-administrativo-sancionador/procedimiento-administrativo-sancionador.component';
import { TableModule } from 'primeng/table';
import { RpfAnexo3Component } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-anexo3/rpf-anexo3.component';
import { RpfAnexo2Component } from '../web/planificacion/registro-plantacion-forestal/seccion/rpf-anexo2/rpf-anexo2.component';
import { TablaDocumentosComponent } from '../web/planificacion/registro-plantacion-forestal/components/tabla-documentos/tabla-documentos.component';
import { AutoCompleteModule } from 'primeng/autocomplete';


@NgModule({
  declarations: [
    RpfInformacionSolicitanteComponent,
    RpfAreaComponent,
    RpfAreaPlantadaComponent,
    RpfDetallePlantacion1Component,
    RpfDetallePlantacion2Component,
    RpfAnexoComponent,
    RpfObservacionesComponent,
    RpfProcedimientoAdministrativoSancionadorComponent,
    RpfAnexo2Component,
    RpfAnexo3Component,
    ObservacionesPlantacionForestalComponent,
    TablaDocumentosComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    //prime ng
    FileUploadModule,
    CheckboxModule,
    ReactiveFormsModule,
    DropdownModule,
    FieldsetModule,
    RadioButtonModule,
    DialogModule,
    ButtonModule,
    ConfirmPopupModule,
    TableModule,
    AutoCompleteModule,
  ],
  exports: [
    RpfInformacionSolicitanteComponent,
    RpfAreaComponent,
    RpfAreaPlantadaComponent,
    RpfDetallePlantacion1Component,
    RpfDetallePlantacion2Component,
    RpfAnexoComponent,
    RpfObservacionesComponent,
    RpfProcedimientoAdministrativoSancionadorComponent,
    RpfAnexo2Component,
    RpfAnexo3Component,
    ObservacionesPlantacionForestalComponent,
    TablaDocumentosComponent,
  ]
})
export class PlantacionForestalSharedModule { }
