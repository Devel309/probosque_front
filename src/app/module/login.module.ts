import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { LoginComponent } from '../web/login/login.component';
import {AppRoutingModule} from '../app-routing.module';
import {ToastModule} from 'primeng/toast';
import { PasswordModule } from 'primeng/password';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ToastModule,
    PasswordModule,
    MatInputModule
  ]
})
export class LoginModule { }
