import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppRoutingModule} from '../app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { EvaluacionAprobacionPmComponent } from '../web/planificacion/evaluacion-aprobacion-pm/evaluacion-aprobacion-pm.component';
import {MatStepperModule} from '@angular/material/stepper';
import {TableModule} from 'primeng/table';
import {PaginatorModule} from 'primeng/paginator';
import {RadioButtonModule} from 'primeng/radiobutton';


@NgModule({
  declarations: [
    EvaluacionAprobacionPmComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material Componets
    MatStepperModule,
    // PrimeNG Components
    TableModule,
    PaginatorModule,
    RadioButtonModule
    

  ],
  exports: [

  ]
})
export class EvaluacionAprobacionPmModule { }
