import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from '../web/inicio/inicio.component';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatDatepickerModule } from '@angular/material/datepicker';

import {CalendarModule} from 'primeng/calendar';
import {MenuModule} from 'primeng/menu';
import {ChartModule} from 'primeng/chart';



@NgModule({
  declarations: [
    InicioComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    MatDatepickerModule,
    // primeng components
    MenuModule,
    CalendarModule,
    ChartModule

  ]
})
export class InicioModule { }
