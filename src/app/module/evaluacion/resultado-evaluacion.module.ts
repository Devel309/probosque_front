import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ResultadoEvaluacionComponent } from "src/app/web/evaluacion/resultado-evaluacion/resultado-evaluacion.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AccordionModule } from "primeng/accordion";
import { DropdownModule } from "primeng/dropdown";
import { InformeEvaluacionSharedModule } from "src/app/shared/components/informe-evaluacion-shared/informe-evaluacion-shared.module";
import { UploadInputButtonsModule } from "src/app/shared/components/upload-input-buttons/upload-input-buttons.module";
import { RadioButtonModule } from "primeng/radiobutton";
import { InputButtonsModule } from "src/app/shared/components/input-button/input-buttons.module";
import { TableModule } from "primeng/table";
import { ToastModule } from "primeng/toast";
import { InputButtonsCodigoModule } from "src/app/shared/components/input-button-codigo/input-buttons.module";

@NgModule({
  declarations: [ResultadoEvaluacionComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AccordionModule,
    DropdownModule,
    InformeEvaluacionSharedModule,
    UploadInputButtonsModule,
    RadioButtonModule,
    InputButtonsModule,
    TableModule,
    ToastModule,
    InputButtonsCodigoModule
  ],
  exports: [ResultadoEvaluacionComponent],
})
export class ResultadoEvaluacionModule {}
