import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OpinionesComponent } from 'src/app/web/evaluacion/opiniones/opiniones.component';
import { SolicitarOpicionEvaluacionModule } from 'src/app/shared/components/solicitar-opicion-evaluacion/solicitar-opicion-evaluacion.module';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';



@NgModule({
  declarations: [OpinionesComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SolicitarOpicionEvaluacionModule,
    DialogModule,
    ButtonModule,
    AccordionModule
  ],
  exports:[OpinionesComponent]
})
export class OpinionesModule { }
