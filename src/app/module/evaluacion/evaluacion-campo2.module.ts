import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluacionCampoComponent } from 'src/app/web/evaluacion/evaluacion-campo/evaluacion-campo.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { DropdownModule } from 'primeng/dropdown';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { PanelModule } from 'primeng/panel';
import { EvaluacionSimpleModule } from 'src/app/shared/components/evaluacion-simple/evaluacion-simple.module';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DialogModule } from 'primeng/dialog';
import {ToastModule} from 'primeng/toast';
import {EvaluacionArchivoModule} from '../../shared/components/evaluacion-archivo/evaluacion-archivo.module';



@NgModule({
  declarations: [EvaluacionCampoComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UploadInputButtonsModule,
    DropdownModule,
    MatDatepickerModule,
    EvaluacionSimpleModule,
    PanelModule,
    ButtonModule,
    TableModule,
    ToastModule,
    RadioButtonModule,
    DialogModule,
    EvaluacionArchivoModule
  ],
  exports:[
    EvaluacionCampoComponent
  ]
})
export class EvaluacionCampo2Module { }
