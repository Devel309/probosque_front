import { NgModule } from '@angular/core';

import { FieldsetModule } from 'primeng/fieldset';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { ChartModule } from 'primeng/chart';
import { DialogModule } from 'primeng/dialog';
import { MultiSelectModule } from 'primeng/multiselect';



@NgModule({

  imports: [
    FieldsetModule,
    RadioButtonModule,
    DropdownModule,
    TableModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    ToastModule,
    CalendarModule,
    ChartModule,
    MultiSelectModule
  ],
  exports: [
    FieldsetModule,
    RadioButtonModule,
    DropdownModule,
    TableModule,
    ButtonModule,
    CheckboxModule,
    PaginatorModule,
    ToastModule,
    CalendarModule,
    ChartModule,
    DialogModule,
    MultiSelectModule
  ],
  declarations: [],
})
export class NgPrimeModule { }
