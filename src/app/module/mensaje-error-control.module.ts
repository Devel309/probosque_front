import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MensajeErrorControlComponent } from '../components/mensaje-error-control/mensaje-error-control.component';



@NgModule({
  declarations: [
    MensajeErrorControlComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    MensajeErrorControlComponent
  ]
})
export class MensajeErrorControlModule { }
