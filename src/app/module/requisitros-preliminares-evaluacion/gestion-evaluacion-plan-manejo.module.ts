import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GestionEvaluacionPlanManejoComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-manejo/gestion-evaluacion-plan-manejo.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../material/material.module";
import { TabMesaPartesComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-manejo/tabs/tab-mesa-partes/tab-mesa-partes.component";
import { RadioButtonModule } from "primeng/radiobutton";
import { TableModule } from "primeng/table";
import { EvaluacionSimpleModule } from "src/app/shared/components/evaluacion-simple/evaluacion-simple.module";
import { PanelModule } from "primeng/panel";
import { UploadInputButtonsModule } from "src/app/shared/components/upload-input-buttons/upload-input-buttons.module";
import { DropdownModule } from "primeng/dropdown";
import { DialogModule } from "primeng/dialog";
import { TabRequisitosPgmfComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-manejo/tabs/tab-requisitos-pgmf/tab-requisitos-pgmf.component";
import { RequisitoPgmfSimpleModule } from "src/app/shared/components/requisito-pgmf-simple/requisito-pgmf-simple.module";
import { ButtonModule } from "primeng/button";
import { AccordionModule } from "primeng/accordion";
import { ConfirmationService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ToastModule } from "primeng/toast";
import { ConfirmPopupModule } from "primeng/confirmpopup";

@NgModule({
  declarations: [
    GestionEvaluacionPlanManejoComponent,
    TabMesaPartesComponent,
    TabRequisitosPgmfComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    MaterialModule,
    RadioButtonModule,
    TableModule,
    EvaluacionSimpleModule,
    PanelModule,
    UploadInputButtonsModule,
    DropdownModule,
    DialogModule,
    RequisitoPgmfSimpleModule,
    ButtonModule,
    AccordionModule,
    ConfirmDialogModule,
    ToastModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
  ],
})
export class GestionEvaluacionPlanManejoModule {}
