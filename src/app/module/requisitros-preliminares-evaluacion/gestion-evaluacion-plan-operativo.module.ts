import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../material/material.module";
import { RadioButtonModule } from "primeng/radiobutton";
import { TableModule } from "primeng/table";
import { EvaluacionSimpleModule } from "src/app/shared/components/evaluacion-simple/evaluacion-simple.module";
import { PanelModule } from "primeng/panel";
import { UploadInputButtonsModule } from "src/app/shared/components/upload-input-buttons/upload-input-buttons.module";
import { DropdownModule } from "primeng/dropdown";
import { DialogModule } from "primeng/dialog";
import { RequisitoPgmfSimpleModule } from "src/app/shared/components/requisito-pgmf-simple/requisito-pgmf-simple.module";
import { ButtonModule } from "primeng/button";
import { AccordionModule } from "primeng/accordion";
import { GestionEvaluacionPlanOperativoComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-operativo/gestion-evaluacion-plan-operativo.component";
import { TabMesaPartesComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-operativo/tabs/tab-mesa-partes/tab-mesa-partes.component";
import { TabMedidasCorrectivasComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-operativo/tabs/tab-medidas-correctivas/tab-medidas-correctivas.component";
import { ConfirmPopupModule } from "primeng/confirmpopup";
import { InputButtonsModule } from "src/app/shared/components/input-button/input-buttons.module";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { ToastModule } from "primeng/toast";
import { PaginatorModule } from "primeng/paginator";
import { TabRequisitosPoacComponent } from "src/app/web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-operativo/tabs/tab-requisitos-poac/tab-requisitos-poac.component";
import { ConfirmDialogModule } from "primeng/confirmdialog";

@NgModule({
  declarations: [
    GestionEvaluacionPlanOperativoComponent,
    TabMesaPartesComponent,
    TabRequisitosPoacComponent,
    TabMedidasCorrectivasComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    MaterialModule,
    RadioButtonModule,
    TableModule,
    EvaluacionSimpleModule,
    PanelModule,
    UploadInputButtonsModule,
    DropdownModule,
    DialogModule,
    RequisitoPgmfSimpleModule,
    ButtonModule,
    AccordionModule,
    ConfirmPopupModule,
    ToastModule,
    PaginatorModule,
    ConfirmDialogModule,

    InputButtonsModule,
    MatDatepickerModule,
  ],
})
export class GestionEvaluacionPlanOperativojoModule {}
