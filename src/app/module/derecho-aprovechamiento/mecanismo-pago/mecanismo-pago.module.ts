import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MecanismoPagoComponent } from 'src/app/web/derecho-aprovechamiento/mecanismo-pago/mecanismo-pago.component';
import { RegistroMecanismoPagoComponent } from 'src/app/web/derecho-aprovechamiento/mecanismo-pago/registro-mecanismo-pago/registro-mecanismo-pago.component';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MatTabsModule } from '@angular/material/tabs';
import { ToastModule } from 'primeng/toast';
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from 'primeng/dialog';
import { TabInformacionGeneralRdaComponent } from 'src/app/web/derecho-aprovechamiento/mecanismo-pago/registro-mecanismo-pago/tabs/tab-informacion-general-rda/tab-informacion-general-rda.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { TabRegistroPagoDaComponent } from 'src/app/web/derecho-aprovechamiento/mecanismo-pago/registro-mecanismo-pago/tabs/tab-registro-pago/tab-registro-pago-da.component';
import { ValidarPagoDaComponent } from '../../../web/derecho-aprovechamiento/mecanismo-pago/validar-pago/validar-pago-da.component';
import { ButtonsFilePagoModule } from 'src/app/shared/components/buttons-file-pago/buttons-file-pago.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ModalRegistroPagoModule } from 'src/app/web/derecho-aprovechamiento/mecanismo-pago/modal/registro-pago/registro-pago.module';
import { NgxMaskModule } from 'ngx-mask';
import { MessageService } from 'primeng/api';
import { VerPagoModule } from '../../../web/derecho-aprovechamiento/mecanismo-pago/modal/ver-pago/ver-pago.module';
import { TextColorEstadoModule } from 'src/app/shared/components/text-color-estado/text-color-estado.module';
import { TablaEspeciesModule } from 'src/app/web/derecho-aprovechamiento/mecanismo-pago/components/tabla-especies/tabla-especies.module';
import { DialogTipoEspeciePlanMultiselectModule } from 'src/app/shared/components/dialog-tipo-especie-plan-multiselect/dialog-tipo-especie-plan-multiselect.module';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { SharedModule } from '@shared';

import { ChipModule } from 'primeng/chip';

@NgModule({
  declarations: [
    MecanismoPagoComponent,
    ValidarPagoDaComponent,
    RegistroMecanismoPagoComponent,
    TabInformacionGeneralRdaComponent,
    TabRegistroPagoDaComponent
  ],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaskModule,
        //Modulos de Primeng
        MatTabsModule,
        MatDatepickerModule,
        FieldsetModule,
        DropdownModule,
        TableModule,
        AccordionModule,
        ButtonModule,
        RadioButtonModule,
        ToastModule,
        CheckboxModule,
        DialogModule,
        ButtonsFilePagoModule,
        ModalRegistroPagoModule,
        VerPagoModule,
        TextColorEstadoModule,
        TablaEspeciesModule,
        DialogTipoEspeciePlanMultiselectModule,
        ConfirmDialogModule,
        SharedModule,
        ChipModule
    ],
  exports: [

  ],
  providers:[
    MessageService
  ]
})
export class MecanismoPagoModule { }
