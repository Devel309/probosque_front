import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitudBeneficioComponent } from 'src/app/web/regimen-promocional/solicitud-beneficio.component';
import { TableModule } from 'primeng/table';
import { BandejaBeneficioTHComponent } from 'src/app/web/regimen-promocional/beneficioTH/bandeja-beneficioTH/bandeja-beneficio-th.component';
import { BandejaSolicitudBeneficioComponent } from 'src/app/web/regimen-promocional/solicitud-beneficio/bandeja-solicitud-beneficio/bandeja-solicitud-beneficio.component';
import { RegistroSolicitudBeneficioComponent } from 'src/app/web/regimen-promocional/solicitud-beneficio/registro-solicitud-beneficio/registro-solicitud-beneficio.component';
import { FormsModule } from '@angular/forms';
import { BuscarThModule } from 'src/app/shared/components/buscar-th/buscar-th.module';



@NgModule({
  declarations: [
    SolicitudBeneficioComponent,
    BandejaBeneficioTHComponent,
    BandejaSolicitudBeneficioComponent,
    RegistroSolicitudBeneficioComponent

  ],
  imports: [
    CommonModule,
    TableModule,
    FormsModule,
    BuscarThModule
  ]
})
export class RegimenPromocionalModule { }
