import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SolicitudAccesoNaturalComponent } from '../web/registrar-solicitud-acesso/tipo-registro/solicitud-acceso-natural/solicitud-acceso-natural.component';
import { SolicitudAccesoJuridicaComponent } from '../web/registrar-solicitud-acesso/tipo-registro/solicitud-acceso-juridica/solicitud-acceso-juridica.component';
import { SolicitudAccesoGeneralComponent } from '../web/registrar-solicitud-acesso/tipo-registro/solicitud-acceso-general/solicitud-acceso-general.component';
import { DropdownModule } from 'primeng/dropdown';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';




@NgModule({
  declarations: [
    SolicitudAccesoNaturalComponent,
    SolicitudAccesoJuridicaComponent,
    SolicitudAccesoGeneralComponent
  ],
  imports:[
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    FieldsetModule,
    ButtonModule,
    CheckboxModule,
    DropdownModule,
  ],
  exports: [
    SolicitudAccesoNaturalComponent,
    SolicitudAccesoJuridicaComponent,
    SolicitudAccesoGeneralComponent
  ]
})
export class SolicitudAccesoSharedModule { }
