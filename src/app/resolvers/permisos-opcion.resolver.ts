import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { delay, finalize, tap, map, catchError } from 'rxjs/operators';
import { LoadingComponent } from '../components/loading/loading.component';
import { AuthService } from '../service/auth.service';
import { UsuarioService } from '../service/base/usuario.service';
import { varLocalStor } from '../shared/util-local-storage';

@Injectable({
  providedIn: 'root'
})
export class PermisosOpcionResolver implements Resolve<any> {
  datosPerimisos: any;
  constructor(
    private dialog: MatDialog,
    private authService: AuthService,
    private usuarioSrv: UsuarioService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.datosPerimisos = JSON.parse(`${localStorage.getItem(varLocalStor.PERMISO_OPCION)}`);
    if (this.datosPerimisos && state.url === this.datosPerimisos.url) {
      const params = {
        idOpcion: this.datosPerimisos.idOpcion,
        perfil: this.usuarioSrv.usuario.sirperfil,
        url: null
      }
      return this.obtenerPermisos(params);
    } else {
      const params = {
        idOpcion: null,
        perfil: this.usuarioSrv.usuario.sirperfil,
        url: state.url
      }
      return this.obtenerPermisos(params);
    }
  }

  obtenerPermisos(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.authService.ObtenerPermisosOpcion(params).pipe(
      tap({
        next: (resp) => resp
      }),
      finalize(() => this.dialog.closeAll())
    );
  }
}
