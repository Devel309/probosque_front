import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';

@Component({
  selector: 'mensaje-error-control',
  templateUrl: './mensaje-error-control.component.html',
  styleUrls: ['./mensaje-error-control.component.scss']
})
export class MensajeErrorControlComponent implements OnInit {

  @Input() control: any ;
  init:any;
  constructor() { 

  }

  get errorMessage() {
    for (const propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
            return ValidatorsExtend.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
        }
    }
    return null;
}

  ngOnInit(): void {
  }

}
