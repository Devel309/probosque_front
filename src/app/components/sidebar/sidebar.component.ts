import { Component, Input, OnInit } from '@angular/core';
import { Router, RouterStateSnapshot } from "@angular/router";
import { JwtHelperService } from '@auth0/angular-jwt';
import { ConfirmationService } from 'primeng/api';
import { interval } from 'rxjs';
import { MenuNavegacion, MenuNavegacionItem } from 'src/app/model/menu';
import { UsuarioModel } from 'src/app/model/Usuario';
import { AuthService } from 'src/app/service/auth.service';

import { ConfigService } from "../../service/config.service";
//import {UsuarioModel} from "src/app/model/UsuarioModel";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input('openSidebar') openSidebar: boolean = true;
  itemsMenuAvatar: any[] = [];
  navigation: any[] = [];
  objUser = {} as UsuarioModel;


  objInicial: String = "";
  objEnviromente: any;
  objNombre: String = "";
  objNomPerfil: String = "";
  objTimeExpTxt: String = "";
  objTimeExp: number = 59;
  objTimeMinExp: number = 59;

  helper = new JwtHelperService();



  hideMenu(): void {

    this.openSidebar = true;
  }

  logout(): void {
    sessionStorage.clear();
    localStorage.clear();
    this._router.navigate(['login'], { replaceUrl: true });

    this._configService.config = {
      useLayout: false
    };


    // const decodedToken = helper.decodeToken(myRawToken);
    // const expirationDate = helper.getTokenExpirationDate(myRawToken);
    // const isExpired = helper.isTokenExpired(myRawToken);
  }


  constructor(
    public _configService: ConfigService,
    public _router: Router,
    private servAuth: AuthService,
    private confirmationService: ConfirmationService,
    // private helpejwt:  JwtHelperService

  ) {

  }



  ngOnInit(): void {
    if (localStorage.getItem("usuario") != null) {
      this.objUser = JSON.parse('' + localStorage.getItem("usuario")?.toString());
      this.contadorA();

      if (this.objUser != null) {

        this.objInicial =  this.objUser.idtipoDocumento == 4 ? (this.objUser.razonSocial && this.objUser.razonSocial != null ? this.objUser.razonSocial.substring(0, 2): '') : this.objUser.nombres.substring(0, 1) + this.objUser.apellidoPaterno.substring(0, 1);
        this.objNombre = this.objUser.idtipoDocumento == 4 ? (this.objUser.razonSocial && this.objUser.razonSocial != null ? this.objUser.razonSocial.toUpperCase() : '') : this.objUser.apellidoPaterno.toUpperCase() + ' ,' + this.objUser.nombres.toUpperCase();
        this.objNomPerfil = this.objUser.sirperfil;
        let param = {
          "aplicacion": 0,
          "codigoPergil": this.objUser.perfiles[0].codigoPerfil
        }; //"0003"
        this.servAuth.ObtenerMenu(param).subscribe(
          (result: any) => {
            if (result.success) {
              this.navigation = result.data;
            } else {
            }
          }, (error: any) => {
            //si existe usuario , refrescar token y hay un error limpiar todo , si no setear el token nuevamente

            sessionStorage.clear();
            localStorage.clear();
            window.location.href = './login';
          }
        );
      }


      this.itemsMenuAvatar = [
        {
          label: 'Salir',
          icon: 'pi pi-sign-out',
          command: () => {
            this.logout();
          }
        },
      ];

    }
  }
  contadorA() {
    let timm = localStorage.getItem('timem');
    let tims = localStorage.getItem('times');
    let objTimeExp = Number.parseInt((tims == undefined ? 0 : tims).toString());
    let objTimeMinExp = Number.parseInt((timm == undefined ? 60 : timm).toString());
    let _interval = setInterval(() => {
      this.objTimeExpTxt = `${objTimeMinExp.toString().padStart(2, '00')}:${objTimeExp.toString().padStart(2, '00')}`;
      if (objTimeExp <= 1) {
        objTimeExp = 60;
        objTimeMinExp--;
        localStorage.setItem('timem', objTimeMinExp.toString());

      }
      if (objTimeMinExp < 1) {
        this.confirmar();
        clearInterval(_interval);
      }
      if (objTimeMinExp == 59) { localStorage.setItem('timem', "59"); }
      localStorage.setItem('times', objTimeExp.toString());
      objTimeExp--;
    }, 900);
  }
  confirmar() {
    this.confirmationService.confirm({
      message: 'El tiempo de sesión ha caducado. ¿Desea continuar?',
      icon: 'pi pi-exclamation-triangle',
      key: 'alerta',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        let objTimeMinExp = Number.parseInt((60).toString());
        localStorage.setItem('timem', objTimeMinExp.toString());
        localStorage.setItem('times', "0")
        this.contadorA();
      },
      reject:() =>{
        sessionStorage.clear();
        localStorage.clear();
        // this._router.navigate(['login'], { queryParams: { returnUrl: state.url } });
        window.location.href = './login';
      }
    })
  }
}
