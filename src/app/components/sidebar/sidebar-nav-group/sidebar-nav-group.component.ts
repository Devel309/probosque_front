import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuNavegacionItem } from 'src/app/model/menu';

@Component({
  selector: 'sidebar-nav-group',
  templateUrl: './sidebar-nav-group.component.html',
  styleUrls: ['./sidebar-nav-group.component.scss']
})
export class SidebarNavGroupComponent implements OnInit {

  @HostBinding('class')
  classes = 'nav-collapsable nav-item';
  @Input() item: MenuNavegacionItem = new MenuNavegacionItem();
  @Input() route:any;


  toggleOpen(e:any,item:MenuNavegacionItem):void{;
    item.groupOpen = !item.groupOpen;
  }

  private expandMenu(menu:MenuNavegacionItem, parent? : MenuNavegacionItem,childerns? :MenuNavegacionItem[]):void
  {
    

    if(childerns && childerns.length > 0)
    {
      
        childerns.forEach(el => {
          
          if(el.url == this.route.location._platformLocation.location.pathname)
          {

            menu.groupOpen = true;
            if(parent)
              parent.groupOpen = true;
            

           // this.classes= 'nav-collapsable expand nav-item';
           

          }
          if(el.children && el.children.length >0)
          {
            this.expandMenu(el,menu,el.children);
          }

        });
    }

  }



  ngAfterViewInit():void {
   
    
  }

  ngOnInit(): void {
    if(this.route)
    {
      this.expandMenu(this.item,undefined,this.item.children);
      
    }
  }

}
