import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { MenuNavegacionItem } from 'src/app/model/menu';
import { AuthService } from 'src/app/service/auth.service';
import { varLocalStor } from 'src/app/shared/util-local-storage';

@Component({
  selector: 'sidebar-nav-item',
  templateUrl: './sidebar-nav-item.component.html',
  styleUrls: ['./sidebar-nav-item.component.scss']
})
export class SidebarNavItemComponent implements OnInit {

  @HostBinding('class')
  classes = 'nav-item';

  @Input() item: MenuNavegacionItem = new MenuNavegacionItem();


  constructor() {   }

  ngOnInit(): void {
    
  }

  guardarDatos(item: any) {
    let dataOpcion = {idOpcion: item.idOpcion, type: item.type, url: item.url}
    localStorage.setItem(varLocalStor.PERMISO_OPCION, JSON.stringify(dataOpcion));
  }

}
