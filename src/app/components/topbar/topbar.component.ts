import { Component, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  @Output() toggleSidebar: EventEmitter<any> = new EventEmitter();

  resultados:string[] = [];
  txtSerach:string = '';
  placeholder:string = 'Aquí puede realizar su busqueda ...';

  buscar(e:any):void{
    this.resultados = ['dato busqueda  prueba 1','dato busqueda prueba 2','dato busqueda prueba 3'];
  }


  toggle(): void {
    this.toggleSidebar.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }


  abrirUrlMapa(){
    window.open("https://1drv.ms/u/s!Akun_qhd6hLSgpARyN-dbKJLjt1-UQ?e=gRFWmH", "_blank");
  }

}
