import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-observaciones',
  templateUrl: './modal-observaciones.component.html',
})
export class ModalObservacionesComponent implements OnInit {
  descripcion: string = '';
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      this.descripcion = this.config.data.item.observaciones;
    }
  }

  enviar = () => {
    if (this.descripcion === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Observaciones, es obligatorio',
      });
    } else this.ref.close(this.descripcion);
  };
}
