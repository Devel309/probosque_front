import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';

@Component({
  selector: 'app-modal-file',
  templateUrl: './modal-file.component.html',
})
export class ModalFileComponent implements OnInit {
  autoResize: boolean = true;
  descripcion: string = '';
  file!: any;
  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private config: DynamicDialogConfig,
    private procesoPostulaionService: ProcesoPostulaionService,
  ) {}

  ngOnInit() {
    if(this.config.data.item.tieneAutorizacion != null){
      this.obtenerAutorizacion();
    }
  }

  obtenerAutorizacion = () => {
    let params = {
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
      idUsuarioPostulacion: this.config.data.item.idUsuarioPostulacion,
    };

    this.procesoPostulaionService
      .obtenerAutorizacion(params)
      .subscribe((result: any) => {
        // ;
      });
  };

  cargarAchivo(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.file = e.srcElement.files[0];
      }
    }
  }

  validarEnvio = (): boolean => {
    if (this.descripcion === '') {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'NOTA',
        detail: 'Debe ingresar una descripción',
      });
      return false;
    } else if (this.file === undefined || this.file === null) {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'NOTA',
        detail: 'Debe adjuntar un archivo',
      });
      return false;
    }

    return true;
  };

  enviar = () => {
    if (this.validarEnvio()) {
      let obj1 = {
        Descripcion: this.descripcion,
        file: this.file,
      };
      this.ref.close(obj1);
    }
  };
}
