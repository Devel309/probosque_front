import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ListarImpugnacionRequest } from 'src/app/model/Impugnacion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ListarSolicitudOpinionRequest } from 'src/app/model/solicitud-opinion';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { ImpugnacionService } from 'src/app/service/impugnacion.service';
import { OposicionService } from 'src/app/service/oposicion/oposicion.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { SolicitudOpinionService } from 'src/app/service/solicitud-opinion.service';

@Component({
  selector: 'app-modal-autorizacion',
  templateUrl: './modal-autorizacion.component.html',
  providers: [MessageService],
})
export class ModalAutorizacionComponent implements OnInit {
  usuario!: UsuarioModel;

  autorizacion: any[] = [];
  solicitudes: any[] = [];
  impugnaciones: any[] = [];
  solicitudImpugnacion: any[] = [];
  evidencias: any[] = [];
  oposiciones: any[] = [];
  listaNotificaPlantFor: any = [];

  listarSolicitudOpinionRequest: ListarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();
  listarImpugnacionRequest: ListarImpugnacionRequest = new ListarImpugnacionRequest();
  refLoad!: DynamicDialogRef;

  isPerfilARFFS: boolean = false;
  isPerfilTitularTH: boolean = false;

  constructor(
    public config: DynamicDialogConfig,
    private serv: PlanificacionService,
    private usuarioServ: UsuarioService,
    public dialogService: DialogService,
    private solicitudOpinionServ: SolicitudOpinionService,
    private router: Router,
    private impugnacionServ: ImpugnacionService,
    private oposicionService: OposicionService,
    private solicitudConcesionService: SolicitudConcesionService,
    private servPf: PermisoForestalService,
  ) { }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isPerfilARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    this.isPerfilTitularTH = this.usuario.sirperfil === Perfiles.TITULARTH;

    if (this.isPerfilARFFS){
      this.listarImpugnacionRequest.pageNum = null;
      this.listarImpugnacionRequest.pageSize = null;
      this.listarResolucionImpugnada();
      this.listarConcesionEvidencias();
      this.notificarSolicitudImpugnacion();
      this.listarBandejaPlantacioForestal({notificarActualizadoARFFS: true, codigoPerfil:this.usuario.sirperfil ,pageNum: 1, pageSize:1000});
    } else if(this.isPerfilTitularTH) {
      this.listarBandejaPlantacioForestal({actualizarInformacionRNPF: true, codigoPerfil:this.usuario.sirperfil ,pageNum: 1, pageSize:1000});
    }

    this.listarSolicitudesOpinion();
    this.cargarAutorizacion();
    this.listarOposicion();
  }

  cargarAutorizacion = () => {
    this.autorizacion = this.config.data.item;
    // this.autorizacion.forEach((element) => {
    //   this.obtenerArchivo(element);
    // });
  };



  listarSolicitudesOpinion() {
    this.listarSolicitudOpinionRequest.siglaEntidad = this.usuario.sirperfil;
    this.listarSolicitudOpinionRequest.estSolicitudOpinion = "ESOPPEND";

    this.solicitudOpinionServ.listarSolicitudOpinion(this.listarSolicitudOpinionRequest).subscribe((result: any) => {
      if (result.data) {
        this.solicitudes = [...result.data];
      }

    })
  }

  listarResolucionImpugnada() {
    this.listarImpugnacionRequest.perfil = this.usuario.sirperfil;

    this.impugnacionServ.listarResolucionImpugnada(this.listarImpugnacionRequest)
      .subscribe((result: any) => {
        this.impugnaciones = result.data;
      })

  }

  notificarSolicitudImpugnacion() {
    let param: any ={
      perfil: this.usuario.sirperfil
    };
    this.impugnacionServ.notificarSolicitudImpugnacion(param)
      .subscribe((result: any) => {
        this.solicitudImpugnacion =result.data
      })

  }

  listarConcesionEvidencias() {
    let param: any ={
      codigoPerfil: this.usuario.sirperfil,
      pageNum: 1,
      pageSize: 100,
      evidenciaRemitida:true
    };
    this.solicitudConcesionService.listarSolicitudConcesionPorFiltro(param)
      .subscribe((result: any) => {
        this.evidencias = result.data;
      })

  }

  listarBandejaPlantacioForestal(params: any) {
    this.listaNotificaPlantFor = [];
    this.servPf.BandejaSolicitudPermisosForestales(params).subscribe((result: any) => {
      if (result.success && result.data) this.listaNotificaPlantFor = result.data;
    });
  }

  descargarArchivoGestion(item: any) {

    this.router.navigate(['/planificacion/bandeja-solicitud-opinion'])


    // this.refLoad = this.dialogService.open(LoadingComponent, {
    //   closable: false,
    // });

    // this.archivoServ.obtenerArchivo(item.documentoGestion).pipe(tap((res: any) => {
    //   if (res.data) {
    //     DownloadFile(res.data.file, res.data.nombre, "application/octet-stream");
    //   }
    // }, error => {

    // })).pipe(finalize(() => this.refLoad.close()))
    //   .subscribe();

  }


  verImpugnacion(data: any) {
    this.router.navigate(['/planificacion/bandeja-evaluacion-impugnacion'])
  }
  verEvidencia(data: any) {
    this.router.navigate(['/concesion/solicitud-concesion-PFDM'])
  }

  verInfoSolPlantForestal(data: any) {
    this.router.navigate(['/plantacion/bandeja-plantacion-forestal'])
  }

  obtenerArchivo = (element: any) => {
    let params = {
      codigoAnexo: null,
      idDocumentoAdjunto: element.idDocumentoAdjunto,
      idProcesoPostulacion: null,
      idTipoDocumento: 6,
      idUsuarioAdjunta: null,
    };
    this.serv.obtenerAdjuntos(params).subscribe((result: any) => {
      //debugger;
      if (result != null) {
        if (result.data != null) {
          if (result.data.length > 0) {
            let anexoActual = result.data[0];
            element.file = anexoActual.file;
            element.nombreDocumento = anexoActual.nombreDocumento;
          }
        }
      }

    });
  };

  descargar = (item: any) => {
    // debugger;
    DownloadFile(item.file, item.nombreDocumento, 'application/octet-stream');
  };

  listarOposicion = () => {
    const params = {
      idEstado: 1,
      idUsuarioPostulacion: this.usuario.idusuario,
      tipoDocumentoGestion: 'TPMPFDM',
      pageNum: 1,
      pageSize: 10
    };

    this.oposicionService.listarOposicion(params).subscribe((resp: any) => {

      if (resp.data && resp.data.length > 0) {

        this.oposiciones = resp.data;

      }
    });
  };
  verOposicion(data: any) {
    this.router.navigate(['/planificacion/consulta-oposicion'])

  }
}
