import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { UsuarioModel } from '@models';
import { UsuarioService } from '@services';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})



export class AppComponent implements
  OnChanges,
  OnInit,
  OnDestroy {
  objUser = {} as UsuarioModel;

  paramsLogin = { username: "", password: "", codApp: "MC", tokenRecaptcha: null, token: null };

  data = 10;
  constructor(private servAuth: AuthService, private usuarioSev: UsuarioService) {
    //  if (this.usuarioSev.usuario!=null || this.validarUsuario() == false) {
    //  if(this.objUser==null){
    //    sessionStorage.clear();
    //   localStorage.clear();
    //    window.location.href = './';
    //    }
    // }
  // console.log(`new - data is ${this.data}`);
  }
  ngOnChanges() {
    //console.log(`ngOnChanges - data is ${this.data}`);
  }
  ngOnInit() {
   // console.log(`ngOnInit  - data is ${this.data}`);
  //  this.paramsLogin.username=this.usuarioSev.usuario.usuario;
  //   if (this.validarUsuario() == false) {
  //     sessionStorage.clear();
  //     localStorage.clear();
  //     window.location.href = './';
  //   }
  }
  // ngDoCheck() {
  //   console.log('ngDoCheck');
  // }
  // ngAfterContentInit() {
  //   console.log('ngAfterContentInit');
  // }
  // ngAfterContentChecked() {
  //   console.log('ngAfterContentChecked');
  // }
  // ngAfterViewInit() {
  //   console.log('ngAfterViewInit');

  // }
  // ngAfterViewChecked() {
  //   console.log('ngAfterViewChecked');
  // }
  ngOnDestroy() {
    console.log('ngOnDestroy');
    sessionStorage.clear();
    localStorage.clear();
    window.location.href = './';
  }
  addNumber(): void {
    this.data += 10;
  }
  deleteNumber(): void {
    this.data -= 10;
  }


  validarUsuario(): any {
    this.servAuth.validarLogin(this.paramsLogin).subscribe(
      (result: any) => {
        let succes = (result.success == true ? "success" : "warn");
        console.log('--Conexción--');
        return true;
      }, (error: any) => {
        console.log('Problemas de Conexión Comuníquese con Soporte');
        sessionStorage.clear();
        localStorage.clear();
        window.location.href = './';
        return false;
      }
    );
  }

}
