import { Injectable } from "@angular/core";
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse,
    HttpHeaders,
} from "@angular/common/http";
import { environment } from '../../environments/environment';
import { Observable, throwError } from "rxjs";
import { Router } from "@angular/router";


@Injectable({
    providedIn: "root",
})
export class HttpConfigInterceptor implements HttpInterceptor {

    urlBase: string = environment.urlbase;
    urlSerfor: string = environment.urlSerfor;
    urlToken: string = "/api/autenticacion/token";
    urlAutenticar: string = 'api/autenticacion/login';
    token: string = "";
    structMenu: any;

    constructor(private router: Router) {

    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        //debugger;;
        let auth = req;
        // const token = sessionStorage.getItem("token");
        const token = localStorage.getItem("token");
        // if (req.url == "/corews/auth" || req.url == "/corews/api/regente/") {
        //     // const tokenSerfor = sessionStorage.getItem("tokenForestal");
        //     const tokenSerfor = localStorage.getItem("tokenForestal");

        //     if (tokenSerfor != null && req.url != "/corews/auth") {
        //         auth = req.clone({
        //             url: `${this.urlSerfor}${auth.url}`,
        //             setHeaders: {
        //                 Authorization: 'Bearer ' + tokenSerfor,
        //             },
        //         });

        //     } else {
        //         auth = req.clone({ url: `${this.urlSerfor}${req.url}`, });
        //     }
        // }
        // else {
            if (token != null) {
                if (req.url.includes("https://sniffs.serfor.gob.pe/apigeoforestal")) {
                    auth = req.clone({ url: `${req.url}`, });
                } else {

                          //  if(auth.url.indexOf("mcsniffs-rest/") !== -1){
                          //   this.urlBase="http://localhost:8080/";
                          // }

                    auth = req.clone({
                        url: `${this.urlBase}${auth.url}`,
                        setHeaders: {
                            Authorization: token,
                        },
                    });
                }
            } else {
              // if(auth.url.indexOf("mcsniffs-rest/") !== -1){
              //   this.urlBase="http://localhost:8080/";
              // }

                auth = req.clone({ url: `${this.urlBase}${req.url}`, });
            }
            //console.log(auth);
        // }

        return next.handle(auth);
    }

}

