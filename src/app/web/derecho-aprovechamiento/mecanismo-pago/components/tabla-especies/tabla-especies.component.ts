import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  ArchivoService,
  ParametroValorService,
  UsuarioService,
} from '@services';
import { DownloadFile, ToastService } from '@shared';
import * as moment from 'moment';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoPagos } from 'src/app/model/util/CodigoPagos';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { DerechoAprovechamientoService } from 'src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service';
import { PagoArchivoService } from 'src/app/service/derecho-aprovechamiento/pago-archivo.service';
import { CategoriasComponent } from 'src/app/shared/components/categorias/categorias.component';
import { DialogTipoEspeciePlanMultiselectComponent } from 'src/app/shared/components/dialog-tipo-especie-plan-multiselect/dialog-tipo-especie-plan-multiselect.component';
import { DialogTipoOperativoComponent } from '../../modal/dialog-tipo-operativo/dialog-tipo-operativo.component';

@Component({
  selector: 'tabla-especies-DA',
  templateUrl: './tabla-especies.component.html',
  styleUrls: ['./tabla-especies.component.scss'],
})
export class TablaEspeciesComponent implements OnInit {
  @Input() idPago: number = 0;
  @Input() especies: any[] = [];
  @Input() montoTotal: number = 0;
  @Input() defaultT: boolean = false;
  @Input() nrDocumentoGestion: number = 0;
  @Input() cabeceraEspecies: PagoGuiaModel = new PagoGuiaModel();
  @Input() tipoPlan: string = '';
  @Input() estadoPago: string = '';
  @Input() pagoForm: any;
  ref: DynamicDialogRef = new DynamicDialogRef();
  @Output() nuevaEspecie = new EventEmitter();
  @Output() editEspecie = new EventEmitter();
  @Output() eliminaEspecie = new EventEmitter();
  @Output() editCabeceraEspecie = new EventEmitter();
  @Output() updateGuiaPago = new EventEmitter();
  @Input() isPerfilArffs!: boolean;


  nombreComun: string = '';
  minDate = moment(new Date()).format('YYYY-MM-DD');
  cboDescuentos: any[] = [];
  selectDescuentos: any[] = [];

  form: EspeciesModel = {} as EspeciesModel;
  verModalEspecies: boolean = false;
  indexEspecie: number = 0;
  categorias: any[] = [];
  valores: any[] = [];
  listPO: any[] = [];
  selectedValuesPO: any[] = [];
  selectPO: any;
  perfiles = Perfiles;
  perfil: any;
  idArchivoC : number = 0;

  CodigoEstadoPagos = CodigoEstadoPagos

  constructor(
    public dialogService: DialogService,
    private messageService: MessageService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private pagoArchivoService: PagoArchivoService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private parametroValorService: ParametroValorService,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }
    /* this.form.descripcion =  */
    this.listarDescuento();
    this.listarParametro();
    // this.listarPO();
  }

  listarParametro() {
    var params = {
      prefijo: 'CATEESPEC',
    };

    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.categorias = response.data;
      });
  }

  listarDescuento() {
    const params = {
      idDescuento: null,
      pageNum: 1,
      pageSize: 10,
    };
    this.derechoAprovechamientoServ
      .listarDescuento(params)
      .subscribe((result: any) => {
        this.cboDescuentos = result.data;
      });
  }

  validarCategoria(data: any) {
    const categoriaSeleccionada = this.categorias.filter((t: any) => {
      return data.value === t.codigo;
    });
    const categoria = categoriaSeleccionada[0];
    this.form.valorEstadoNatural = Number(categoria.valorTerciario);
    this.form.comentarios = categoria.valorSecundario;
  }

  onChangePO(event: any, data: any) {
    if (event.checked === true && this.selectedValuesPO.length != 0) {
      this.selectedValuesPO = [data];
      this.selectedValuesPO.forEach((response) => {
        this.selectPO = response;
      });
    }
  }

  openModalMultiEspeciesFlora = (mesaje: string) => {
    
    this.ref = this.dialogService.open(
      DialogTipoEspeciePlanMultiselectComponent,
      {
        header: mesaje,
        width: '50%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        data: {
          idPlanManejo: this.nrDocumentoGestion,
          tipoPlan: this.tipoPlan,
        },
      }
    );

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          let obj = new EspeciesModel(element);
          obj.idEspecie = parseInt(element.idCodigoEspecie);
          obj.nombreComun = element.textNombreComun;
          obj.nombreCientifico = element.textNombreCientifico;
          this.nuevaEspecie.emit(obj);
        });
        this.successMensaje('Se agregaron las especies Flora correctamente.');
      }
    });
  };

  successMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  editarEspecies(data: any, index: number) {
    this.valores = [];
    this.selectDescuentos = [];
    this.idArchivoC = 0;
    this.verModalEspecies = true;
    this.indexEspecie = index;
    this.form = { ...data };
    this.nombreComun = data.nombreComun;
    if (data.descripcion != null && data.descripcion != '') {
      this.valores = data.descripcion.split(',');
      this.valores.forEach((x) => {
        this.selectDescuentos.push(Number(x));
      });
    }
  }

  editarEspecie() {
    if (!this.validaCampos()) return;
    this.editEspecie.emit({ index: this.indexEspecie, form: this.form });
    this.verModalEspecies = false;
  }

  verCategorias() {
    this.ref = this.dialogService.open(CategoriasComponent, {
      header: 'Categorías',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        data: this.categorias,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {});
  }

  calculoMonto() {
    const { volumen, valorEstadoNatural, descuentoTotal } = this.form;

    if (volumen > 0 && valorEstadoNatural > 0) {
      const monto = volumen * valorEstadoNatural;
      this.form.monto = monto;
    }

    // if (descuentoTotal > 0) {
      this.calculoMontoFinal();
    // }
  }

  calculoMontoFinal() {
    const { monto } = this.form;
    if (this.selectDescuentos.length > 0) {
      let totalDescuento = 0;
      this.selectDescuentos.map((value) => {
        totalDescuento += value;
      });
      this.form.descripcion = this.selectDescuentos.join();

      this.form.descuentoTotal = monto * (totalDescuento / 100);
      const montoT = monto - this.form.descuentoTotal;
      this.form.montoFinal = montoT;
    } else {
      this.form.descuentoTotal = 0;
      const montoT = monto - this.form.descuentoTotal;
      this.form.montoFinal = montoT;
    }
  }

  onChangeEventVolumen(event: any) {
    this.form.volumen = event.target.value;
    this.calculoMonto();
  }

  validaCampos() {
    let validar = true;
    let msj = '';

    if (!this.form.volumen) {
      validar = false;
      msj = msj + '(*) Debe ingresar volumen (m3).\n';
    }
    if (!this.form.valorEstadoNatural) {
      validar = false;
      msj = msj + '(*) Debe ingresar volumen estado natural.\n';
    }
    if (!this.form.categoria) {
      validar = false;
      msj = msj + '(*) Debe ingresar una categoría.\n';
    }
    // if (this.selectDescuentos.length == 0) {
    //   validar = false;
    //   msj = msj + '(*) Debe seleccionar descuento.\n';
    // }
    if (!this.form.fechaComprobante && 
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO && 
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO && 
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL) {
      validar = false;
      msj = msj + '(*) Debe ingresar: Fecha de Comprobante.\n';
    }
    if (this.form.fechaComprobante && 
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO && 
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO && 
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL) {
      let fechaC = new Date(this.form.fechaComprobante)
      let fechaI = new Date(this.pagoForm.fechaInicio)
      let fechaF = new Date(this.pagoForm.fechaFin)
      if (fechaC <= fechaI) {
        validar = false;
        msj = msj += "(*) Fecha Comprobante debe ser mayor a Fecha Inicio de vigencia: " + moment(new Date(this.pagoForm.fechaInicio)).format("DD/MM/YYYY") + ".\n";
      }
      if (fechaC > fechaF) {
        validar = false;
        msj = msj += "(*) Fecha Comprobante debe ser menor a Fecha Fin de vigencia: " + moment(new Date(this.pagoForm.fechaFin)).format("DD/MM/YYYY") + ".\n";
      }
    }
    if (!this.form.observacion && 
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO && 
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO && 
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL) {
      validar = false;
      msj = msj + '(*) Debe ingresar: Observaciones.\n';
    }
    if(this.idArchivoC == 0 && 
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO && 
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO && 
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL){
      validar = false;
      msj = msj + '(*) Debe adjuntar: Documento.\n';
    }

    // if (this.montoTotal > 0 && this.form.montoFinal > this.montoTotal) {
    //   validar = false;
    //   msj = msj + `(*) Monto final debe ser menor a ${this.montoTotal}.\n`;
    // }

    if (!validar) this.toast.warn(msj);
    return validar;
  }

  eliminar(event: any, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idPagoGuia == 0) {
          this.eliminaEspecie.emit(index);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  eliminarPagoGuia(event: any, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idPagoGuiaEspecie == 0) {
          this.eliminaEspecie.emit(index);
        } else {
          var params = {
            // idPagoGuia: data.idPagoGuia,
            idPagoGuiaEspecie: data.idPagoGuiaEspecie,
            idUsuarioElimina: this.usuarioService.idUsuario
          }
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.derechoAprovechamientoServ
          .eliminarPagoGuiaEspecie(params)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((response: any) => {
            if (response.success == true) {
              this.toast.ok('Se eliminó el registro correctamente.');
              this.eliminaEspecie.emit(index);
            } else {
              this.toast.error(response?.message);
            }
          });
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  listarArchivos(codigoProceso: string, codigoTipoDocumento: string) {
    var params = {
      idPago: this.idPago,
      codigoArchivo: codigoProceso,
      codigoSubArchivo: null,
      codigoTipoDocumento: codigoTipoDocumento,
      idArchivo: null,
    };

    this.pagoArchivoService.listarArchivo(params).subscribe((result: any) => {
      if (!!result.data.length) {
        result.data.forEach((element: any) => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          if (element.idArchivo) {
            let params = {
              idArchivo: element.idArchivo,
            };
            this.archivoServ
              .descargarArchivoGeneral(params)
              .subscribe((result: any) => {
                this.dialog.closeAll();
                if (result.data !== null && result.data !== undefined) {
                  DownloadFile(
                    result.data.archivo,
                    result.data.nombeArchivo,
                    result.data.contenTypeArchivo
                  );
                }
                (error: HttpErrorResponse) => {
                  this.errorMensaje(error.message);
                  this.dialog.closeAll();
                };
              });
          }
        });
      } else {
        this.errorMensaje('No se ha registrado archivo.');
      }
    });
  }

  descargarArchivo(index: number) {
    const codigoProceso = 'PAGOCOMPRO' + index;
    const codigoTipoDocumento = 'PAGOCOMPRO';
    this.listarArchivos(codigoProceso, codigoTipoDocumento);
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  registroArchivo(idArchivo: number) {
    this.idArchivoC = idArchivo;
  }

  eliminarArchivo(idArchivo: number) {
    this.idArchivoC = 0;
  }
}

export class PagoGuiaModel {
  idPagoGuia: number = 0;
  codPagoGuia: string = '';
  idPago: number = 0;
  nrDocumentoGestion: number = 0;
  tipoDocumentoGestion: string = '';
  observacion: string = '';
  detalle: string = '';
  descripcion: string = '';
  idUsuarioRegistro: number = 0;
  listaGuiaEspecie: EspeciesModel[] = [];

  constructor(data?: any) {
    if (data) {
      this.idPagoGuia = data.idPagoGuia ? data.idPagoGuia : 0;
      this.codPagoGuia = data.codPagoGuiá ? data.codPagoGuiá : '';
      this.idPago = data.idPago ? data.idPago : 0;
      this.nrDocumentoGestion = data.nrDocumentoGestion ? data.nrDocumentoGestion : 0;
      this.tipoDocumentoGestion = data.tipoDocumentoGestion ? data.tipoDocumentoGestion : '';
      this.observacion = data.observacion ? data.observacion : '';
      this.detalle = data.detalle ? data.detalle : '';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro : 0;
      this.listaGuiaEspecie = data.listaGuiaEspecie ? data.listaGuiaEspecie : [];
    }
  }
}

export class EspeciesModel {
  idPagoGuiaEspecie: number = 0;
  idEspecie: number = 0;
  volumen: number = 0;
  valorEstadoNatural: number = 0;
  monto: number = 0;
  descuento: number = 0;
  montoFinal: number = 0;
  descuentoTotal: number = 0;
  montoTotal: number = 0;
  fechaComprobante: string = '';
  comentarios: string = '';
  observacion: string = '';
  detalle: string = '';
  descripcion: string = '';
  nombreComun: string = '';
  nombreCientifico: string = '';
  idUsuarioRegistro: number = 0;
  perfil: string = '';
  categoria: string = '';
  codPagoGuiaEspecie: string = '';

  constructor(data?: any) {
    if (data) {
      this.idPagoGuiaEspecie = data.idPagoGuiaEspecie
        ? data.idPagoGuiaEspecie
        : 0;
      this.idEspecie = data.idEspecie ? data.idEspecie : 0;
      this.volumen = data.volumen ? data.volumen : null;
      this.valorEstadoNatural = data.valorEstadoNatural
        ? data.valorEstadoNatural
        : null;
      this.monto = data.monto ? data.monto : null;
      this.descuento = data.descuento ? data.descuento : 0;
      this.montoFinal = data.montoFinal ? data.montoFinal : null;
      this.descuentoTotal = data.descuentoTotal ? data.descuentoTotal : null;
      this.montoTotal = data.montoTotal ? data.montoTotal : 0;
      this.fechaComprobante = data.fechaComprobante
        ? data.fechaComprobante
        : '';
      this.comentarios = data.comentarios ? data.comentarios : '';
      this.observacion = data.observacion ? data.observacion : '';
      this.detalle = data.detalle ? data.detalle : '';
      this.nombreComun = data.nombreComun ? data.nombreComun : '';
      this.perfil = data.perfil ? data.perfil : '';
      this.nombreCientifico = data.nombreCientifico
        ? data.nombreCientifico
        : '';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.categoria = data.categoria ? data.categoria : '';
      this.codPagoGuiaEspecie = data.codPagoGuiaEspecie
        ? data.codPagoGuiaEspecie
        : '';
    }
  }
}
