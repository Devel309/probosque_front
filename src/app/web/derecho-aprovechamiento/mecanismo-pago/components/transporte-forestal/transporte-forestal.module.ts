import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from '@shared';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { ButtonsFilePagoModule } from 'src/app/shared/components/buttons-file-pago/buttons-file-pago.module';
import { DialogEspeciesModule } from 'src/app/shared/components/categorias/dcategorias.module';
import { TransporteForestalComponent } from './transporte-forestal.component';



@NgModule({
  declarations: [
    TransporteForestalComponent
  ],
  exports: [
    TransporteForestalComponent
  ],
  imports: [
    CommonModule,
    DropdownModule,
    TableModule,
    ButtonsFilePagoModule,
    ReactiveFormsModule,
    FormsModule,
    DialogModule,
    MatDatepickerModule,
    ButtonModule,
    ConfirmPopupModule,
    MultiSelectModule,
    SharedModule,
    CheckboxModule,
    DialogEspeciesModule
  ]
})
export class TransporteForestalModule { }
