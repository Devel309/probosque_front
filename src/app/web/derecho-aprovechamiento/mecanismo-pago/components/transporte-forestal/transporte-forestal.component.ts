import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import * as moment from "moment";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "transporte-forestal",
  templateUrl: "./transporte-forestal.component.html",
  styleUrls: ["./transporte-forestal.component.scss"],
})
export class TransporteForestalComponent implements OnInit {
  verModalEspecies: boolean = false;
  form: EspeciesModel = {} as EspeciesModel;
  minDate = moment(new Date()).format("YYYY-MM-DD");
  constructor(public dialogService: DialogService) {}

  ngOnInit(): void {}

  nuevaGuia() {
    this.verModalEspecies = true;
    this.form = new EspeciesModel();
  }
}

export class EspeciesModel {
  guia: string = "";
  fechaComprobante: string = "";
  resolucion: string = "";
  fechaExpancion: string = "";
  fechaVencimiento: string = "";
  planOperativo: string = "";

  constructor(data?: any) {
    if (data) {
      this.guia = data.guia ? data.guia : "";
      this.fechaComprobante = data.fechaComprobante
        ? data.fechaComprobante
        : "";
      this.resolucion = data.resolucion ? data.resolucion : "";
      this.fechaExpancion = data.fechaExpancion ? data.fechaExpancion : "";
      this.fechaVencimiento = data.fechaVencimiento
        ? data.fechaVencimiento
        : "";
      this.planOperativo = data.planOperativo ? data.planOperativo : "";
    }
  }
}
