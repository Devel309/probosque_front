import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaCronogramaPagosAnioComponent } from './tabla-cronograma-pagos-anio.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsFilePagoModule } from 'src/app/shared/components/buttons-file-pago/buttons-file-pago.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ButtonModule } from 'primeng/button';
import { SharedModule } from '@shared';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [TablaCronogramaPagosAnioComponent],
  exports: [TablaCronogramaPagosAnioComponent],
  imports: [
    CommonModule,
    TableModule,
    DialogModule,
    ReactiveFormsModule,
    FormsModule,
    ButtonsFilePagoModule,
    MatDatepickerModule,
    ButtonModule,
    SharedModule,
    ConfirmPopupModule
  ],
})
export class TablaCronogramaPagosAnioModule {}
