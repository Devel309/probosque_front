import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { DowloadFileLocal, DownloadFile, ToastService } from '@shared';
import * as moment from 'moment';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CronogramaPA, PagoDA, PeriodoPA } from 'src/app/model/pagoDA';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigoEstadoPagos } from 'src/app/model/util/CodigoPagos';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { DerechoAprovechamientoService } from 'src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service';
import { PagoArchivoService } from 'src/app/service/derecho-aprovechamiento/pago-archivo.service';
import { IComboModel } from '../../registro-mecanismo-pago/tabs/tab-informacion-general-rda/tab-informacion-general-rda.component';

@Component({
  selector: 'tabla-cronograma-pagos-anio',
  templateUrl: './tabla-cronograma-pagos-anio.component.html',
  styleUrls: ['./tabla-cronograma-pagos-anio.component.scss'],
})
export class TablaCronogramaPagosAnioComponent implements OnInit {
  @Input() idPago: number = 0;
  @Input() cronogramaPagos: any[] = [];
  @Input() pagoForm: any;
  @Input() isPerfilArffs!: boolean;
  @Input() estadoPago: string = '';
  @Output() nuevoAnio = new EventEmitter();
  @Output() nuevoPeriodo = new EventEmitter();
  @Output() listCronograma = new EventEmitter();

  mecanismoTabIndex: number = 0;
  form: PagoDA = new PagoDA();

  cronogramaContext: CronogramaPA = new CronogramaPA();
  periodoContext: PeriodoPA = new PeriodoPA();

  listSelectMecaPago: any[] = [];

  tituloAnioModal: string = '';
  tituloPeriodoModal: string = '';

  verModalPeriodo: boolean = false;
  verModalAnhio: boolean = false;
  loading = false;
  minDate = moment(new Date()).format('YYYY-MM-DD');
  indexPeriodo: number = 0;
  indexAnio: number = 0;
  idArchivo: number = 0;
  isPerfilTitularTH: boolean = false;
  usuario!: UsuarioModel;
  perfiles = Perfiles;
  perfil: any;
  idArchivoC: number = 0;

  constructor(
    private pagoArchivoService: PagoArchivoService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private messageService: MessageService,
    private usuarioService: UsuarioService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private derechoAprovechamientoServ: DerechoAprovechamientoService
  ) {}

  ngOnInit(): void {
    this.usuario = this.usuarioService.usuario;
    this.isPerfilTitularTH = this.usuario.sirperfil === Perfiles.TITULARTH;
    this.form.idPago = this.idPago;
    let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }
    if (this.cronogramaPagos.length == 0) {
      this.calcularCronogAnual();
    }
  }

  private calcularCronogAnual(): void {
    let dInicio: Date = new Date(this.pagoForm.fechaInicio);

    for (let index: number = 0; index < (this.pagoForm.vigencia || 0); index++) {
      // let dFin = new Date(dInicio.getFullYear() + 1, dInicio.getMonth(), dInicio.getDate());
      let dFin = new Date(dInicio.getFullYear(), 11, 31);
      
      let item: CronogramaPA = new CronogramaPA({
        anio: (index + 1),
        fechaFin: dFin,
        fechaInicio: dInicio,
        idPagoCronograma: 0,
        idUsuarioRegistro: this.usuarioService.idUsuario,
        listaPeriodo: [],
        perfil: this.usuarioService.usuario.sirperfil,
        verPeriodo: false
      });

      this.pagoForm.listaCronograma.push(item);
      // dInicio = dFin;
      var fin = new Date(dFin);
      fin.setDate(fin.getDate() + 1);
      dInicio = fin;
    }
  }

  emitirListCronograma() {
    this.listCronograma.emit(this.pagoForm.listaCronograma);
  }

  agregarAnhio(): void {
    const obj = new CronogramaAnioModel(this.cronogramaContext);
    // this.cronogramaPagos.push(obj);

    this.nuevoAnio.emit(obj);
    this.verModalAnhio = false;
  }

  agregarPeriodo(): void {
    if (this.validationFields(this.periodoContext)) {
      if (this.tituloPeriodoModal == 'Editar Periodo Pago') {
        this.cronogramaContext.listaPeriodo[this.indexPeriodo] =
          new PeriodoPA(this.periodoContext);
      } else {
        this.cronogramaContext.listaPeriodo.push(
          new PeriodoPA(this.periodoContext)
        );
      }
      this.verModalPeriodo = false;
      this.emitirListCronograma();
    }
  }

  validationFields(data: PeriodoPA) {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!data.descripcion) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Periodo.\n";
    }
    if (!data.fechaLimite) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Fecha Límite.\n";
    } else {
      let fechaL = new Date(data.fechaLimite)
      let fechaI = new Date(this.cronogramaContext.fechaInicio)
      let fechaF = new Date(this.cronogramaContext.fechaFin)
      
      if (fechaL <= fechaI) {
        validar = false;
        mensaje = mensaje += "(*) Fecha Límite debe ser mayor a Fecha Inicio del Año " + this.indexAnio + ": " + moment(new Date(this.cronogramaContext.fechaInicio)).format("DD/MM/YYYY") + ".\n";
      }
      if (fechaL > fechaF) {
        validar = false;
        mensaje = mensaje += "(*) Fecha Límite debe ser menor a Fecha Fin del Año " + this.indexAnio + ": " + moment(new Date(this.cronogramaContext.fechaFin)).format("DD/MM/YYYY") + ".\n";
      }
    }
    if (!data.monto) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Monto.\n";
    } else {
      if (data.monto > this.pagoForm.montoPagoAnual) {
        validar = false;
        mensaje = mensaje += "(*) Monto no debe ser mayor a Monto de pago por DA anual: " + Number(this.pagoForm.montoPagoAnual).toFixed(2) + ".\n";
      }
    }
    if (!data.montoFinal) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Monto Final.\n";
    } else {
      if (data.montoFinal > this.pagoForm.montoPagoAnual) {
        validar = false;
        mensaje = mensaje += "(*) Monto Final no debe ser mayor a Monto de pago por DA anual: " + Number(this.pagoForm.montoPagoAnual).toFixed(2) + ".\n";
      }
    }
    if (!data.fechaComprobante &&
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO &&
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO &&
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL) {
      validar = false;
      mensaje = mensaje + '(*) Debe ingresar: Fecha de Comprobante.\n';
    }
    if (data.fechaComprobante && 
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO && 
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO && 
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL) {
      let fechaC = new Date(data.fechaComprobante)
      let fechaL = new Date(data.fechaLimite)
      let fechaI = new Date(this.cronogramaContext.fechaInicio)
      let fechaF = new Date(this.cronogramaContext.fechaFin)
      if (fechaC <= fechaI) {
        validar = false;
        mensaje = mensaje += "(*) Fecha Comprobante debe ser mayor a Fecha Inicio del Año " + this.indexAnio + ": " + moment(new Date(this.cronogramaContext.fechaInicio)).format("DD/MM/YYYY") + ".\n";
      }
      if (fechaC > fechaF) {
        validar = false;
        mensaje = mensaje += "(*) Fecha Comprobante debe ser menor a Fecha Fin del Año " + this.indexAnio + ": " + moment(new Date(this.cronogramaContext.fechaFin)).format("DD/MM/YYYY") + ".\n";
      }
      if (fechaC > fechaL) {
        validar = false;
        mensaje = mensaje += "(*) Fecha Comprobante debe ser menor o igual a Fecha Límite: " + moment(new Date(data.fechaLimite)).format("DD/MM/YYYY") + ".\n";
      }
    }
    if (!data.observacion &&
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO &&
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO &&
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL) {
      validar = false;
      mensaje = mensaje + '(*) Debe ingresar: Observaciones.\n';
    }
    if(this.idArchivoC == 0 && 
      this.estadoPago != CodigoEstadoPagos.PAGO_VALIDO && 
      this.estadoPago != CodigoEstadoPagos.PAGO_OBSERVADO && 
      this.perfil != this.perfiles.AUTORIDAD_REGIONAL){
      validar = false;
      mensaje = mensaje + '(*) Debe adjuntar: Documento.\n';
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  onChangeEventMonto(event: any) {
    this.periodoContext.monto = event.target.value;
  }

  onChangeEventMontoFinal(event: any) {
    this.periodoContext.montoFinal = event.target.value;
  }

  onChangeEventMontoBalance(event: any) {
    this.periodoContext.balancePago = event.target.value;
  }

  verPeriodosPago(data: CronogramaAnioModel): void {
    data.verPeriodo = !data.verPeriodo;
  }

  abrirModalPeriodo(
    accion: string, 
    cronograma: CronogramaPA, 
    periodo?: any, 
    index?: any
  ): void {
    this.cronogramaContext = cronograma;
    this.indexPeriodo = index;
    this.idArchivoC = 0;
    if (accion == 'N') {
      this.periodoContext = new PeriodoPA();
      this.periodoContext.idPagoPeriodo = 0;
      this.periodoContext.idUsuarioRegistro = this.usuarioService.idUsuario;
      this.periodoContext.perfil = this.usuarioService.usuario.sirperfil;
      this.tituloPeriodoModal = 'Agregar Periodo Pago';
      this.indexAnio = cronograma.anio || 0;
      // this.periodoContext.periodo= String(cronograma.listaPeriodo.length+1);
    } else if (accion == 'E') {
      this.periodoContext = new PeriodoPA(periodo);
      this.tituloPeriodoModal = 'Editar Periodo Pago';
    }
    if (!this.pagoForm.montoPagoHA || this.pagoForm.montoPagoHA <= 0) {
      this.toast.warn("Debe ingresar Monto de pago por hectárea para agregar periodo.");
    } else {
      this.verModalPeriodo = true;
    }
  }

  editarModalPeriodo(
    cronograma: CronogramaPA,
    periodo?: any,
    index?: any
  ): void {
    this.cronogramaContext = cronograma;
    this.indexPeriodo = index;
    this.indexAnio = cronograma.anio || 0;
    this.periodoContext = new PeriodoPA(periodo);
    this.tituloPeriodoModal = 'Editar Periodo Pago';
    this.idArchivoC = 0;

    this.verModalPeriodo = true;
  }

  eliminarPeriodo(event: any, index: number, indexP: number, periodo: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (periodo.idPagoPeriodo == 0) {
          this.pagoForm.listaCronograma[index].listaPeriodo.splice(indexP, 1);
        } else {
          var params = {
            idPagoPeriodo: periodo.idPagoPeriodo,
            idUsuarioElimina: this.usuarioService.idUsuario
          }
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.derechoAprovechamientoServ
          .eliminarPago(params)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((response: any) => {
            if (response.success == true) {
              this.toast.ok('Se eliminó el registro correctamente.');
              this.pagoForm.listaCronograma[index].listaPeriodo.splice(indexP, 1);
              this.emitirListCronograma();
            } else {
              this.toast.error(response?.message);
            }
          });
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  abrirModalAnio(accion: string, cronograma?: any): void {
    if (accion == 'A') {
      this.cronogramaContext = new CronogramaPA();
      this.cronogramaContext.anio = this.cronogramaPagos.length + 1;
      this.tituloAnioModal = 'Agregar Nuevo Año';
    } else if (accion == 'E') {
      this.cronogramaContext = new CronogramaPA(cronograma);
      this.tituloAnioModal = 'Editar Nuevo Año';
    }

    this.verModalAnhio = true;
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  listarArchivos(codigoProceso: string, subCodigoArchivo: string, codigoTipoDocumento: string) {
    var params = {
      idPago: this.idPago,
      codigoArchivo: codigoProceso,
      codigoSubArchivo: subCodigoArchivo,
      codigoTipoDocumento: codigoTipoDocumento,
      idArchivo: null,
    };

    this.pagoArchivoService.listarArchivo(params).subscribe((result: any) => {
      if (!!result.data.length) {
        result.data.forEach((element: any) => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          if (element.idArchivo) {
            let params = {
              idArchivo: element.idArchivo,
            };
            this.archivoServ
              .descargarArchivoGeneral(params)
              .subscribe((result: any) => {
                this.dialog.closeAll();
                if (result.data !== null && result.data !== undefined) {
                  DownloadFile(
                    result.data.archivo,
                    result.data.nombeArchivo,
                    result.data.contenTypeArchivo
                  );
                }
                (error: HttpErrorResponse) => {
                  this.errorMensaje(error.message);
                  this.dialog.closeAll();
                };
              });
          }
        });
      } else {
        this.errorMensaje('No se ha registrado archivo.');
      }
    });
  }

  descargarArchivo(anio: number, periodo: number) {
    const codigoProceso = 'PAGOCOMPROA';
    const subCodigoArchivo = 'ANIO' + anio + 'P' + periodo; 
    const codigoTipoDocumento = 'PAGOCOMPRO';
    this.listarArchivos(codigoProceso, subCodigoArchivo, codigoTipoDocumento);
  }

  registroArchivo(idArchivo: number) {
    this.idArchivoC = idArchivo;
  }

  eliminarArchivo(idArchivo: number) {
    this.idArchivoC = 0;
  }
}

export class CronogramaAnioModel {
  constructor(data?: any) {
    if (data) {
      this.verPeriodo = data.verPeriodo;
      this.idPagoCronograma = data.idPagoCronograma;
      this.codTipoPagoPeriodo = data.codTipoPagoPeriodo;
      this.fechaInicio = data.fechaInicio;
      this.fechaFin = data.fechaFin;
      this.anio = data.anio;
      this.observacion = data.observacion;
      this.detalle = data.detalle;
      this.descripcion = data.descripcion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.listaPeriodo = data.listaPeriodo;
      return;
    }
  }

  public idPagoCronograma: number = 0;
  public codTipoPagoPeriodo: string = '';
  public fechaInicio: Date = new Date();
  public fechaFin: Date = new Date();
  public anio: number = 0;
  public observacion: string = '';
  public detalle: string = '';
  public descripcion: string = '';
  public idUsuarioRegistro: number = 0;
  public listaPeriodo: PeriodoPagoAnualModel[] = [];
  public verPeriodo: boolean = false;
}

export class DataRegistroDA {
  constructor() {}

  public selectedTH: number = 0;

  public nroTH: string = '';
  public titularTH: string = '';

  public mp_superficie: boolean = false;
  public mp_valor_volumen: boolean = false;
  public modalidad: string = '';
  public inicioVigencia: Date = new Date();
  public finVigencia?: Date;
  public nroDocuemnto: string = '';
  public nombreEntidad: string = '';
  public ubigeo: string = '';
  public representanteNombre: string = '';
  public representanteDoc = '';

  public tipo_monto_pago_ha: number = 1;
  public monedaMontoPagoHA: number = 1;

  public tipoPersona: string = '';

  public cronogramaPagos: CronogramaAnioModel[] = [];
}

export class PeriodoPagoAnualModel {
  constructor(data?: any) {
    if (data) {
      this.idPagoPeriodo = data.idPagoPeriodo;
      this.codTipoPagoPeriodo = data.codTipoPagoPeriodo;
      this.fechaComprobante = data.fechaComprobante;
      this.observacion = data.observacion;
      this.detalle = data.detalle;
      this.descripcion = data.descripcion;
      this.balancePago = data.balancePago;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.periodo = data.periodo;
      this.fechaLimite = data.fechaLimite;
      this.monto = data.monto;
      this.montoFinal = data.montoFinal;
      return;
    }
  }

  public idPagoPeriodo: number = 0;
  public codTipoPagoPeriodo: string = '';
  public fechaLimite: Date = new Date();
  public fechaComprobante: Date = new Date();
  public periodo: string = '';
  public observacion: string = '';
  public detalle: string = '';
  public descripcion: string = '';
  public monto: number = 0;
  public montoFinal: number = 0;
  public balancePago: number = 0;
  public idUsuarioRegistro: number = 0;
}
