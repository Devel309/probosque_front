import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaCronogramaPagosAnioComponent } from './tabla-cronograma-pagos-anio.component';

describe('TablaCronogramaPagosAnioComponent', () => {
  let component: TablaCronogramaPagosAnioComponent;
  let fixture: ComponentFixture<TablaCronogramaPagosAnioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaCronogramaPagosAnioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaCronogramaPagosAnioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
