import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaDescuentosDAComponent } from './tabla-descuentos-da.component';

describe('TablaDescuentosDAComponent', () => {
  let component: TablaDescuentosDAComponent;
  let fixture: ComponentFixture<TablaDescuentosDAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaDescuentosDAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaDescuentosDAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
