import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaDescuentosDAComponent } from '../tabla-descuentos-da/tabla-descuentos-da.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';



@NgModule({
  declarations: [
    TablaDescuentosDAComponent
  ],
  exports: [
    TablaDescuentosDAComponent
  ],
  imports: [
    CommonModule,
    TableModule,
    ButtonModule
  ]
})
export class TablaDescuentosDAModule { }
