import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { DerechoAprovechamientoService } from 'src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service';

@Component({
  selector: 'tabla-descuentos-da',
  templateUrl: './tabla-descuentos-da.component.html',
  styleUrls: ['./tabla-descuentos-da.component.scss']
})
export class TablaDescuentosDAComponent implements OnInit {
  listDescuento: any = [];
  totalDescuento: number = 0;
  perfiles = Perfiles;
  perfil: any;

  constructor(
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private usuarioService: UsuarioService
  ) { }

  ngOnInit(): void {
    let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }
    this.listarDescuento();
  }

  listarDescuento() {
    const params = {
      idDescuento: null,
      pageNum: 1,
      pageSize: 10,
    };
    this.derechoAprovechamientoServ
      .listarDescuento(params)
      .subscribe((result: any) => {
        if (result.data.length > 0) {
          this.listDescuento = result.data;
          result.data.forEach((item: any) => {
            this.totalDescuento += Number(item.porcentaje);
          });
        }
      });
  }

}
