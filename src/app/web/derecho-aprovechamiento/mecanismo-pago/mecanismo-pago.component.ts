import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { Page, PlanManejoMSG } from "@models";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { LazyLoadEvent } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EvaluacionListarRequest } from "src/app/model/EvaluacionListarRequest";
import { PagoDA } from "src/app/model/pagoDA";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CodigoEstadoEvaluacion } from "src/app/model/util/CodigoEstadoEvaluacion";
import { CodigoEstadoPlanManejo } from "src/app/model/util/CodigoEstadoPlanManejo";
import { Perfiles } from "src/app/model/util/Perfiles";
import { DerechoAprovechamientoService } from "src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { GenericoService } from "src/app/service/generico.service";
import { ModalRegistroPagoComponent } from "./modal/registro-pago/registro-pago.component";

interface IPago {
  nrDocumentoGestion: any;
  idPago: any;
}

@Component({
  selector: "app-mecanismo-pago",
  templateUrl: "./mecanismo-pago.component.html",
  styleUrls: ["./mecanismo-pago.component.scss"],
})
export class MecanismoPagoComponent implements OnInit {
  listSelectMecaPago: any[] = [];
  listMecanismoPago: any[] = [];
  usuario!: UsuarioModel;
  mecanismoPago: EvaluacionRequest;
  evaluacionRequest: EvaluacionListarRequest;

  PAGO: IPago = {
    nrDocumentoGestion: null,
    idPago: null,
  };

  loading = false;
  totalRecords = 0;
  perfiles = Perfiles;
  perfil: any;

  planes: any[] = [];
  PLAN = {};
  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  first :number = 0;

  constructor(
    private usuarioService: UsuarioService,
    private router: Router,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private toast: ToastService,
    public dialogService: DialogService,
    private apiEvaluacionService: EvaluacionService
  ) {
    this.usuario = this.usuarioService.usuario;
    this.mecanismoPago = new EvaluacionRequest();
    this.evaluacionRequest = new EvaluacionListarRequest();
  }

  ngOnInit(): void {
    let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }

    //console.log("PERFILESSS ",  this.usuarioService.usuario);
    this.listarMecanismosPago();
    this.listarPago();
    this.mecanismoPago = new EvaluacionRequest();
    this.buscar();
  }

  actualizarValidacion(numero:number) {
    this.limpiar();
  }

  listarMecanismosPago() {
    const params = { prefijo: "MECPAGO" };
    this.genericoServ
      .listarPorFiltroParametro(params)
      .subscribe((result: any) => {
        this.listSelectMecaPago = result.data;
        // this.mecanismoPago.mecanismoPago = '0';
      });
  }

  buscar() {
    const pageSize = 10;
    const pageNumber = 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPago(page).subscribe();
    // this.listarPlanes(page).subscribe();
  }

  limpiar() {
    this.first = 0;
    this.mecanismoPago = new EvaluacionRequest();
    this.buscar();
  }

  listarPago(page?: Page) {
    const params = {
      idUsuarioRegistro: this.usuarioService.idUsuario,
      pageNum: page ? page.pageNumber : 1,
      pageSize: page ? page.pageSize : 10,
    };
    const r = { ...params, ...this.mecanismoPago };
    this.loading = true;
    return this.filtrarPagos(r)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(tap((res: any) => {
        this.listMecanismoPago = res.data;
        this.totalRecords = res.totalRecords ? res.totalRecords : 0;
      }));
    /* this.derechoAprovechamientoServ
      .listarPago(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.listMecanismoPago = res.data;
        this.totalRecords = res.totalrecord ? res.totalrecord : 0;
      }); */
  }

  filtrarPagos(request: any) {
    return this.derechoAprovechamientoServ
      .listarPago(request)
      .pipe(tap({ error: () => this.toast.error(PlanManejoMSG.ERR.LIST) }));
  }

  listarPlanes(page?: Page) {
    this.mecanismoPago.codigoEstado = CodigoEstadoPlanManejo.APROBADO;

    const r = { ...this.PLAN, ...this.mecanismoPago };
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(tap((res) => (this.planes = res.data)));
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiEvaluacionService
      .filtrarEvaluacion(request, page)
      .pipe(tap({ error: () => this.toast.error(PlanManejoMSG.ERR.LIST) }));
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPago(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
    /*  this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    }); */
  }

  nuevo() {
    let params = new PagoDA();
    params.idPago = 0;
    params.idUsuarioRegistro = this.usuarioService.idUsuario;
    params.codigoEstado = "PAGOPEND";
    params.perfil = this.usuarioService.usuario.perfiles[0].codigoPerfil.toString();

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .registrarPago([params])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok("Se registró mecanismo de pago por DA correctamente.");
          this.navigate(res.data[0].idPago);
        } else {
          this.toast.error("Ocurrió un error.");
        }
      });
  }

  btnVer(data: any) {
    this.navigate(data.idPago);
  }

  btnEditar(data: any) {
    this.navigate(data.idPago);
  }

  btnValidar(data: any) {
    this.navigate(data.idPago);
  }

  btnVRegistrar(data: any) {
    if (data.codigoTH) {
      const ref = this.dialogService.open(ModalRegistroPagoComponent, {
        header: "Registro de pago por DA",
        width: "75%",
        data: {
          id: data.idPago,
          nrDocumentoGestion: data.nrDocumentoGestion,
          tipoPlan: data.tipoDocumentoGestion
        },
      });
    }
    // this.navigate(data.idPago);
  }

  btnVerPorarffs(data: any) {
    if (data.codigoTH) {
      const ref = this.dialogService.open(ModalRegistroPagoComponent, {
        header: "Registro de pago por DA",
        width: "75%",
        data: {
          id: data.idPago,
          nrDocumentoGestion: data.nrDocumentoGestion,
          tipoPlan: data.tipoDocumentoGestion
        },
      });
    }
    // this.navigate(data.idPago);
  }

  navigate(idPago: number) {
    const uri = "DerechoAprovechamiento/RegistroDA";
    this.router.navigate([uri, idPago]);
  }
}
export class MecanismoPago {
  idTH: number | null;
  mecanismoPago: string | null;
  nombreTitular: string | null;
  dniTitular: string | null;
  nrDocumentoGestion: number | null;
  idPago: number | null;

  constructor(obj?: any) {
    this.idTH = null;
    this.mecanismoPago = null;
    this.nombreTitular = null;
    this.dniTitular = null;
    this.nrDocumentoGestion = null;
    this.idPago = null;
  }
}

export class EvaluacionRequest {
  nrDocumentoGestion: string | null;
  idPago: string | null;
  codigoTH: string | null;
  mecanismoPago: string | null;
  nroDocumento: string | null;
  codigoEstado: string | null;
  idUsuarioRegistro: string | null;

  constructor(obj?: any) {
    this.nrDocumentoGestion = null;
    this.idPago = null;
    this.codigoTH = null;
    this.mecanismoPago = null;
    this.nroDocumento = null;
    this.codigoEstado = null;
    this.idUsuarioRegistro = null;
    if (obj) Object.assign(this, obj);
  }
}
