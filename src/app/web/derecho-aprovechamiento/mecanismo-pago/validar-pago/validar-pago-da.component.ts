import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CodigoPermisoForestal} from '../../../../model/util/CodigoPermisoForestal';
import {LoadingComponent} from '../../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {PagoDA} from '../../../../model/pagoDA';
import {DerechoAprovechamientoService} from '../../../../service/derecho-aprovechamiento/derecho-aprovechamiento.service';
import {MatDialog} from '@angular/material/dialog';
import {descargarArchivo, ToastService} from '@shared';
import {UsuarioService} from '@services';
import {CodigoEstadoEvaluacion} from '../../../../model/util/CodigoEstadoEvaluacion';
import {Perfiles} from '../../../../model/util/Perfiles';
import {ModelArchivo} from '../../../planificacion/concesion-forestal-maderables/lineamientos-evaluacion-pgmf/lineamientos-evaluacion-pgmf.component';
import {CodigoEstadoPagos} from '../../../../model/util/CodigoPagos';
import {ConfirmationService} from 'primeng/api';



@Component({
  selector: 'app-validar-pago-da',
  templateUrl: './validar-pago-da.component.html',
  styleUrls: ['./validar-pago-da.component.scss']
})
export class ValidarPagoDaComponent implements OnInit {
  @Input() idPago!: number;
  @Input() nrDocumentoGestion!: number;
  @Input() tipoPlan!: '';
  @Input() estadoPago!: string;
  @Output() actualizarValidacion: EventEmitter<number> = new EventEmitter();
  isShowModal2_2:boolean=false;
  //dataBuscador = {} as IBuscador;
  activeState: boolean[] = [false, false];
  toggle(index: number) {
    this.activeState[index] = !this.activeState[index];
  }

  listaAux = [
    {anio: '2021', fechainicial: '01/01/2021', fechafin: '01/31/2021'},
    {anio: '2022', fechainicial: '01/01/2021', fechafin: '01/31/2021'},
  ];

  listaAux2 = [
    {numResol: '2021', descPlan: 'prueba 2'},
    {numResol: '2022', descPlan: 'prueba 2'},
  ];

  listaAux3 = [
    {prueba3: 'prueba 3'},
    {prueba3: 'prueba 3'},
  ];

  observacion:string="";
  codigoEstadoText:string="";
  codigoEstado:string="";

  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;

  checkValidado :boolean = false;
  isRegisterByArffs :boolean = false;


  form: PagoDA = new PagoDA();

  constructor(private derechoAprovechamientoServ: DerechoAprovechamientoService,
              private dialog: MatDialog,
              private toast: ToastService,
              private usuarioService: UsuarioService,
              private confirmationService: ConfirmationService,
              ) { }

  ngOnInit(): void {
  }

  verPDF(){

    if(this.nrDocumentoGestion == null || this.idPago == null){
      this.toast.warn("No existe un pago registrado");
      return;
    }

      // if(this.estadoPago == CodigoEstadoPagos.PAGO_VALIDO) {
      //   this.toast.warn("El pago ya se encuentra en estado Validado");
      //   return;
      // }

      // if(this.estadoPago == CodigoEstadoPagos.PAGO_OBSERVADO) {
      //   this.toast.warn("El pago ya se encuentra en estado Observado ");
      //   return;
      // }

    //if(this.estado == CodigoPermisoForestal.ESTADO_IMPUGNADO || this.estado == CodigoPermisoForestal.ESTADO_PRESENTADO){
      this.isShowModal2_2 = true;
      /*this.showFormatoSan = true;
      this.listarTipoSolicitud();
      this.listarImpugnacion();*/
    //this.form = new PagoDA();


    this.listarPago();
    //}
  }

  btnBuscar() {
    
  }

  onChangeValidarPago(a:any, b:any){

  }

  btnVer(fila: any){
    
  }

  btnGuardar() {



    this.confirmationService.confirm({
      message: '¿Desea realizar la validación del pago?',
      icon: 'pi pi-exclamation-triangle',
      key: 'confirmvalidaPago',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {

        //console.log("VALIDANDO PAGO", this.observacion);

        let check = this.checkValidado;
        //console.log("CHECK ", check);

        if(!this.checkValidado) {
          if ( this.observacion == '') {
            this.toast.warn("Ingrese una observación");
            return;
          }
        }

        this.actualizarObservaciones();

      }
    });




  }
  cronogramaPagos: any[] = [];

  listarPago() {

    const params = {
      nrDocumentoGestion: this.nrDocumentoGestion,
      idPago: this.idPago,
      pageNum: 1,
      pageSize: 10,
    };
    this.derechoAprovechamientoServ
      .listarPago(params)
      //.pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.data.length > 0) {
          this.form = new PagoDA(res.data[0]);
          this.idPago = res.data[0].idPago;
          this.form.modalidadTH = res.data[0].mecanismoPago;
          this.cronogramaPagos = this.form.listaCronograma;

           this.observacion = this.form.observacion==null?"":this.form.observacion;
           this.checkValidado = this.form.conforme == this.codigoConforme;
           this.codigoEstadoText = res.data[0].codigoEstadoText;
           this.codigoEstado = res.data[0].codigoEstado;

         // if(this.form.perfil == Perfiles.AUTORIDAD_REGIONAL){
            //this.isRegisterByArffs = true;
            if(this.form.modalidadTH == 'MECPAGOMOD2') {
              //consulto service de guias
              this.derechoAprovechamientoServ
                .listarGuiaPago({
                  idPago:this.idPago,
                  idPagoGuia:null,
                  pageNum:1,
                  pageSize:10
                })
                .subscribe((res: any) => {
                  res.data.forEach((element: any) => {
                    element.listaGuiaEspecie.forEach((e: any) => {
                      if (e.perfil== Perfiles.AUTORIDAD_REGIONAL) {
                        this.isRegisterByArffs = true;
                      }
                    });
                  });
                });

            } else if(this.form.mecanismoPago == 'MECPAGOMOD3') {
              //mixto
              this.form.listaCronograma.forEach((element: any) => {
                element.listaPeriodo.forEach((e: any) => {

                  if (e.perfil== Perfiles.AUTORIDAD_REGIONAL) {
                    this.isRegisterByArffs = true;
                  }
                });
              });

              this.derechoAprovechamientoServ
                .listarGuiaPago({
                  idPago:this.idPago,
                  idPagoGuia:null,
                  pageNum:1,
                  pageSize:10
                })
                .subscribe((res: any) => {
                  res.data.forEach((element: any) => {
                    element.listaGuiaEspecie.forEach((e: any) => {
                      if (e.perfil== Perfiles.AUTORIDAD_REGIONAL) {
                        this.isRegisterByArffs = true;
                      }
                    });
                  });
                });


            }


          //} else if(this.form.perfil == Perfiles.TITULARTH) {
          //  this.isRegisterByArffs = false;
        //  }

          //this.isRegisterByArffs = false;
        }
      });
  }


  actualizarObservaciones() {

    this.form.fechaInicio = this.form.fechaInicio
      ? new Date(this.form.fechaInicio)
      : null;
    this.form.fechaFin = this.form.fechaFin
      ? new Date(this.form.fechaFin)
      : null;
    this.form.idPago = this.idPago;
    this.form.idUsuarioRegistro = this.usuarioService.idUsuario;

    //llenar validaciones
    this.form.conforme = this.checkValidado?this.codigoConforme:this.codigoObservado;
    this.form.codigoEstado = this.checkValidado?"PAGOVALD":"PAGOOBS";
    this.form.observacion = this.observacion;

    
    //console.log(this.cronogramaPagos);

     var params = new PagoDA(this.form);

     this.dialog.open(LoadingComponent, { disableClose: true });
     this.derechoAprovechamientoServ
       .registrarPago([ params ])
       .pipe(finalize(() => this.dialog.closeAll()))
       .subscribe((res: any) => {
         this.dialog.closeAll();
         if (res.success == true) {
           this.toast.ok('Se registró la validación de pago por DA correctamente.');
           this.isShowModal2_2 = false;
           this.actualizarValidacion.emit(1);

         } else {
           this.toast.error('Ocurrió un error.');
         }
       });


  }



}
