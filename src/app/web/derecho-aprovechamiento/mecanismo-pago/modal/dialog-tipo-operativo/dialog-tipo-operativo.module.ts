import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MensajeErrorControlModule } from 'src/app/module/mensaje-error-control.module';
import { MaterialModule } from 'src/app/module/material/material.module';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { ListboxModule } from 'primeng/listbox';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogTipoOperativoComponent } from './dialog-tipo-operativo.component';
import { PaginatorModule } from 'primeng/paginator';

@NgModule({
  declarations: [DialogTipoOperativoComponent],
  entryComponents: [DialogTipoOperativoComponent],
  imports: [
    CommonModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MensajeErrorControlModule,
    // material angular
    MaterialModule,

    //primeng
    TabViewModule,
    ButtonModule,
    DropdownModule,
    TableModule,
    ListboxModule,
    CheckboxModule,
    RadioButtonModule,
    AutoCompleteModule,
    ToastModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
    MultiSelectModule,
    PaginatorModule,
  ],
})
export class DialogTipoOperativoModule {}
