import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EspeciesFauna } from "src/app/model/medioTrasporte";
import { DerechoAprovechamientoService } from "src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service";

@Component({
  selector: "app-dialog-tipo-operativo",
  templateUrl: "./dialog-tipo-operativo.component.html",
  styleUrls: ["./dialog-tipo-operativo.component.scss"],
})
export class DialogTipoOperativoComponent implements OnInit {
  queryFauna: string = "";
  comboListPlanesOperativos: EspeciesFauna[] = [];
  selectedValues: any[] = [];

  totalRecords: number = 0;
  invalid: boolean = false;

  constructor(
    private dialog: MatDialog,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private pagoService:DerechoAprovechamientoService

  ) {}

  ngOnInit(): void {
    this.invalid = this.config.data.invalid;
    this.listarOperativoPorPGM();
  }

  loadData(e: any) {
    const pageSize = Number(e.rows);
    this.listarOperativoPorPGM();
  }

  filtrarFauna() {
    this.listarOperativoPorPGM();
  }

  listarOperativoPorPGM() {
    let params = {
      nroGestion: this.config.data.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pagoService
      .listarOperativosPorPGM(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.totalRecords = result.totalrecord ? result.totalrecord : 0;
        this.comboListPlanesOperativos = [...result.data];
      });
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.queryFauna == null || this.queryFauna == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Una Especie.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  agregar() {
    this.ref.close(this.selectedValues);
  }
  cerrarModal() {
    this.ref.close();
  }
}
