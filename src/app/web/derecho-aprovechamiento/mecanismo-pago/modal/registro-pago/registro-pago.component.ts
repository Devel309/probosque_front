import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PagoDA } from 'src/app/model/pagoDA';
import { DerechoAprovechamientoService } from 'src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service';
import { GenericoService } from 'src/app/service/generico.service';
import { TablaCronogramaPagosAnioComponent } from '../../components/tabla-cronograma-pagos-anio/tabla-cronograma-pagos-anio.component';
import {
  EspeciesModel,
  PagoGuiaModel,
} from '../../components/tabla-especies/tabla-especies.component';
import { IComboMonedaModel, IComboTipoModel } from '../../registro-mecanismo-pago/tabs/tab-registro-pago/tab-registro-pago-da.component';
import { DialogTipoOperativoComponent } from '../dialog-tipo-operativo/dialog-tipo-operativo.component';
import {Perfiles} from '../../../../../model/util/Perfiles';
import { ApiForestalService } from 'src/app/service/api-forestal.service';

@Component({
  selector: 'app-registro-pago',
  templateUrl: './registro-pago.component.html',
  styleUrls: ['./registro-pago.component.scss'],
})
export class ModalRegistroPagoComponent implements OnInit {
  @ViewChild('TablaCronogramaPagosAnioComponent', { static: true })
  TablaCronogramaPagosAnioComponent!: TablaCronogramaPagosAnioComponent;
  idPago: number = 0;
  nrDocumentoGestion: number = 0;
  mecanismoTabIndex: number = 0;
  montoTotal: number = 0;
  form: PagoDA = new PagoDA();
  listSelectMecaPago: any[] = [];
  cronogramaPagos: any[] = [];
  listaEspecies: any[] = [];
  cabeceraEspecies: PagoGuiaModel = new PagoGuiaModel();
  loading = false;
  disabledGuiaPago = true;
  disableTab1: boolean = true;
  disableTab2: boolean = true;
  tipoPlan: string = '';
  nrDocumentoGestionPO: number = 0;
  tipoPlanPO: string = '';

  isPerfilArrfs : boolean = false;
  perfiles = Perfiles;
  perfil: any;

  cboMonedas: IComboMonedaModel[] = [
    { text: "S/", value: "SOLES" },
    { text: "$", value: "DOLARES" }
  ];
  cboTipoMontoPA: IComboTipoModel[] = [
    { text: 'Oferta', value: "1" },
    { text: 'UIT', value: "2" }
  ];
  tipoCambio!: number;
  uit!: number;
  listPO: any[] = [];

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    public dialogService: DialogService,
    private apiForestalService: ApiForestalService
  ) {}

  ngOnInit(): void {
    if (this.usuarioService.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL){
      this.isPerfilArrfs = true;
    }
    
    let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }

    this.idPago = this.config.data.id;
    this.nrDocumentoGestion = this.config.data.nrDocumentoGestion;
    this.tipoPlan = this.config.data.tipoPlan;
    this.form.idPago = this.idPago;
    this.listarMecanismosPago();

    // this.listarModalidad();
    this.listarPago();
    this.consultarUIT();
    // this.tipoCambio = 3.82;
    // this.uit = 4600;
  }

  listarMecanismosPago() {
    const params = { prefijo: 'MECPAGO' };
    this.genericoServ
      .listarPorFiltroParametro(params)
      .subscribe((result: any) => {
        this.listSelectMecaPago = result.data;
      });
  }

  listarModalidad() {
    const params = {
      nrDocumentoGestion: this.nrDocumentoGestion,
    };
    this.derechoAprovechamientoServ
      .listarModalidad(params)
      .subscribe((result: any) => {
        this.form.modalidadTH = result.data[0].modalidadTH;
        this.onChangeMP(this.form.modalidadTH);
      });
  }

  guardarMecanismo2() {
    this.guardarPago();
    this.guardarGuiaPago();
  }

  listarPago() {
    this.cronogramaPagos = [];
    const params = {
      nrDocumentoGestion: this.nrDocumentoGestion,
      idPago: null,
      pageNum: 1,
      pageSize: 10,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .listarPago(params)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.data.length > 0) {
          this.form = new PagoDA(res.data[0]);
          this.idPago = res.data[0].idPago;
          this.form.modalidadTH = res.data[0].mecanismoPago;
          this.onChangeMP(this.form.modalidadTH);
          this.cronogramaPagos = this.form.listaCronograma;

          if (
            this.form.modalidadTH == 'MECPAGOMOD2' ||
            this.form.modalidadTH == 'MECPAGOMOD3'
          ) {
            this.listarGuiaPago();
          }
        } else {
          this.listarModalidad();
        }
      });
  }

  listarGuiaPago() {
    this.listaEspecies = [];
    this.cabeceraEspecies = new PagoGuiaModel();
    const params = {
      idPagoGuia: null,
      idPago: this.idPago,
      pageNum: 1,
      pageSize: 10,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .listarGuiaPago(params)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe(
        (res: any) => {
          this.dialog.closeAll();
          if (res.data.length > 0) {
            this.cabeceraEspecies = new PagoGuiaModel(res.data[0]);
            this.cabeceraEspecies.nrDocumentoGestion= res.data[0].nrDocumentoGestion
            this.listaEspecies = this.cabeceraEspecies.listaGuiaEspecie;
            this.listPO = [];
            this.listPO.push(this.cabeceraEspecies);
            this.validaCamposEspecie();
            this.calcularMontoTotal();
          }
        },
        () => {
          this.dialog.closeAll();
        }
      );
  }

  consultarUIT() {
    this.tipoCambio = 3.725;
    this.uit = 4600;
    this.apiForestalService
      .consultarUIT()
      .subscribe((result: any) => {
        if (result.dataService) {
          this.tipoCambio = result.dataService.respuesta.compra;
          this.uit = Number(result.dataService.respuesta.tasa.replace(',', ''));
        }
      })
  }

  onChangeMP(e: any): void {
    if (e == 'MECPAGOMOD1') {
      this.mecanismoTabIndex = 0;
      this.disableTab1 = true;
      this.disableTab2 = false;
    } else if (e == 'MECPAGOMOD2') {
      this.mecanismoTabIndex = 1;
      this.disableTab1 = true;
      this.disableTab2 = false;
    } else if (e == 'MECPAGOMOD3') {
      this.mecanismoTabIndex = 0;
      this.disableTab1 = false;
      this.disableTab2 = false;
    }
  }

  guardarPago() {
    this.form.fechaInicio = this.form.fechaInicio
      ? new Date(this.form.fechaInicio)
      : null;
    this.form.fechaFin = this.form.fechaFin
      ? new Date(this.form.fechaFin)
      : null;
    this.form.idPago = this.idPago;
    this.form.idUsuarioRegistro = this.usuarioService.idUsuario;

    var params = new PagoDA(this.form);
    params.perfil = this.usuarioService.usuario.sirperfil;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .registrarPago([params])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.success == true) {
          this.toast.ok('Se registró mecanismo de pago por DA correctamente.');
          this.listarPago();
        } else {
          this.toast.error('Ocurrió un error.');
        }
      });
  }

  guardarGuiaPago() {
    const arrayEspecies: any[] = [];

    const cabecera = new PagoGuiaModel(this.cabeceraEspecies);
    // cabecera.nrDocumentoGestion = this.nrDocumentoGestion;
    cabecera.idUsuarioRegistro = this.usuarioService.idUsuario;
    cabecera.idPago = this.idPago;

    this.listaEspecies.map((value) => {
      const obj = new EspeciesModel(value);
      obj.idUsuarioRegistro = this.usuarioService.idUsuario;
      obj.volumen = parseInt(value.volumen);
      obj.valorEstadoNatural = parseInt(value.valorEstadoNatural);
      obj.perfil = this.usuarioService.usuario.sirperfil;
      arrayEspecies.push(obj);
    });

    cabecera.listaGuiaEspecie = arrayEspecies;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .registrarGuiaPago([cabecera])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          this.dialog.closeAll();
          if (res.success == true) {
            this.toast.ok('Se registró guía de pago correctamente.');
            this.listarGuiaPago();
          } else {
            this.toast.error('Ocurrió un error.');
          }
        },
        () => {
          this.dialog.closeAll();
        }
      );
  }

  agregarAnhio(obj: any) {
    this.cronogramaPagos.push(obj);
  }

  nuevaEspecie(obj: any) {
    this.listaEspecies.push(obj);
    this.validaCamposEspecie();
  }

  calcularMontoTotal() {
    this.montoTotal = 0;
    this.listaEspecies.map((value: any) => {
      this.montoTotal += value.montoFinal;
    });
  }

  eliminarEspecie(index: any) {
    this.listaEspecies.splice(index, 1);
    this.validaCamposEspecie();
    this.calcularMontoTotal();
  }

  editarEspecie(response: { index: number; form: any }) {
    this.listaEspecies[response.index] = response.form;
    this.validaCamposEspecie();
    this.calcularMontoTotal();
  }

  validaCamposEspecie() {
    const lista = this.listaEspecies.filter(
      (value: any) => value.volumen == null
    );
    this.disabledGuiaPago = lista.length > 0;
  }

  openModalOperativo = (mesaje: string) => {
    this.ref = this.dialogService.open(DialogTipoOperativoComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        idPlanManejo: this.nrDocumentoGestion,
      },
    });

    this.ref.onClose.subscribe((response: any) => {
      if (response) {
        this.cabeceraEspecies.descripcion = response.nombrePlan;
        this.cabeceraEspecies.tipoDocumentoGestion = response.tipoPlan;
        this.cabeceraEspecies.nrDocumentoGestion = response.nroGestion;
        this.cabeceraEspecies.detalle = response.resolucion;

        this.guardarGuiaPago();
      }
    });
  };

  closeModal() {
    this.ref.close();
  }

  changeMontoXha(event: any): void {
    this.form.montoPagoHA = event.target.value;
    this.form.montoPagoAnual = Number(Number((this.form.areHA || 0) * event.target.value).toFixed(2));

    this.onChangeMoneda({ value: this.form.tipoMoneda });
  }

  onChangeMoneda(event?: any) {
    if (event.value == 'DOLARES' || this.form.tipoMoneda == 'DOLARES') {
      this.form.montoPagoAnualCalculado = Number(Number((this.form.montoPagoAnual || 0) * this.tipoCambio).toFixed(2));
    } else {
      this.form.montoPagoAnualCalculado = null;
    }
  }

  onChangeTipo(event: any) {
    if (event.value == '2') {
      this.form.montoPagoHA = Number(Number(this.uit * (0.01 / 100)).toFixed(2));
    } else {
      this.form.montoPagoHA = null;
    }
    this.changeMontoXha({ target: { value: this.form.montoPagoHA } });
  }
}
