import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '@shared';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { TablaCronogramaPagosAnioModule } from '../../components/tabla-cronograma-pagos-anio/tabla-cronograma-pagos-anio.module';
import { TablaDescuentosDAModule } from '../../components/tabla-descuentos-da/tabla-descuentos-da.module';
import { TablaEspeciesModule } from '../../components/tabla-especies/tabla-especies.module';
import { TransporteForestalModule } from '../../components/transporte-forestal/transporte-forestal.module';
import { DialogTipoOperativoModule } from '../dialog-tipo-operativo/dialog-tipo-operativo.module';
import { ModalRegistroPagoComponent } from './registro-pago.component';

@NgModule({
  declarations: [ModalRegistroPagoComponent],
  exports: [ModalRegistroPagoComponent],
  entryComponents: [ModalRegistroPagoComponent],
  imports: [
    CommonModule,
    DynamicDialogModule,
    ToastModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    ReactiveFormsModule,
    FormsModule,
    MatTabsModule,
    DialogModule,
    MatDatepickerModule,
    TablaDescuentosDAModule,
    TablaCronogramaPagosAnioModule,
    TablaEspeciesModule,
    DialogTipoOperativoModule,
    SharedModule,
    TransporteForestalModule
  ],
})
export class ModalRegistroPagoModule {}
