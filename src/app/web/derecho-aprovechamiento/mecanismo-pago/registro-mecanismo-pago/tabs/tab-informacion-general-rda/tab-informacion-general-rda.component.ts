import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Page } from "@models";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { DerechoAprovechamientoService } from "src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service";
import { ToastService } from "@shared";
import { PagoDA } from "src/app/model/pagoDA";
import { Perfiles } from "src/app/model/util/Perfiles";
import { PlanificacionService, UsuarioService } from "@services";
import { LazyLoadEvent } from "primeng/api";

@Component({
  selector: "app-tab-informacion-general-rda",
  templateUrl: "./tab-informacion-general-rda.component.html",
  styleUrls: ["./tab-informacion-general-rda.component.scss"],
})
export class TabInformacionGeneralRdaComponent implements OnInit {
  @Input() idPago!: number;
  @Output() public siguiente = new EventEmitter();
  //@Input() perfil!: string;

  form: PagoDA = new PagoDA();
  nombreTitularCompleto: string = "";
  nombreRepresentanteCompleto: string = "";
  perfil: any;
  perfiles = Perfiles;
  verModalBuscarTH: boolean = false;
  queryTH: string = "";
  lstTitular: any[] = [];
  totalRecords = 0;
  selectedValues: any[] = [];
  selectTH: any;
  isRegistrado: boolean = false;
  idPlanTmp: number = 0;
  idDepartamento: any = 0;
  idProvincia: any = 0;
  idDistrito: any = 0;
  lstDepartamentos: any = [];
  lstProvincias: any = [];
  lstDistritos: any = [];
  Perfiles = Perfiles;

  constructor(
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private planificacionService: PlanificacionService
  ) {}

  ngOnInit(): void {
    this.listarPago();
    let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }
  }

  listarPago() {
    const params = {
      idPago: this.idPago,
      pageNum: 1,
      pageSize: 10,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ.listarPago(params).subscribe((res: any) => {
      this.dialog.closeAll();
      if (res.data.length > 0) {
        this.form = new PagoDA(res.data[0]);

        if (this.form.ubigeo) {
          this.idDepartamento = parseInt(this.form.ubigeo.substr(0, 2));
          this.idProvincia = parseInt(this.form.ubigeo.substr(0, 4));
          this.idDistrito = parseInt(this.form.ubigeo);
          this.listarPorFiltroDepartamento(true);
        }
        if (this.form.tipoPersona == "TPERJURI") {
          this.nombreTitularCompleto = this.form.nombreTitular
            ? this.form.nombreTitular
            : "";
          this.nombreRepresentanteCompleto =
            (this.form.nombreRepresentante || "") +
            " " +
            (this.form.paternoRepresentante || "") +
            " " +
            (this.form.maternoRepresentante || "");
        } else {
          this.nombreTitularCompleto =
            (this.form.nombreTitular || "") +
            " " +
            (this.form.paternoTitular || "") +
            " " +
            (this.form.maternoTitular || "");
        }
        if (this.form.codigoTH) {
          this.isRegistrado = true;
        }
      }
    });
  }

  abrirModalBuscarTH(): void {
    this.listarTH();
    this.verModalBuscarTH = true;
    this.queryTH = "";
  }

  listarTH(page?: Page) {
    this.lstTitular = [];
    const params = {
      nroGestion: this.idPlanTmp == 0 ? this.form.nrDocumentoGestion : null,
      pageNum: page ? page.pageNumber : 1,
      pageSize: page ? page.pageSize : 10,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ.listarTH(params).subscribe((res: any) => {
      this.dialog.closeAll();
      this.lstTitular = res.data;
      this.totalRecords = res.totalrecord ? res.totalrecord : 0;
      // if (this.isRegistrado) {
      //   this.selectedValues = [res.data[0]];
      // }
    });
  }

  filtrarTH() {
    this.lstTitular = [];
    const params = {
      codigoTH: this.queryTH != "" ? this.queryTH : null,
      pageNum: 1,
      pageSize: 10,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ.listarTH(params).subscribe((res: any) => {
      this.dialog.closeAll();
      this.lstTitular = res.data;
      this.totalRecords = res.totalrecord ? res.totalrecord : 0;
    });
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarTH(page);
  }

  onChangeTH(event: any, data: any) {
    if (event.checked === true && this.selectedValues.length != 0) {
      this.selectedValues = [data];
      this.selectedValues.forEach((response) => {
        this.selectTH = response;
      });
    }
  }

  seleccionarTH(): void {
    if (!this.validarTH()) return;

    this.form.perfil = this.usuarioService.usuario.sirperfil;
    this.form.nrDocumentoGestion = this.selectTH.nroGestion;
    this.form.mecanismoPago = this.selectTH.mecanismo;
    this.form.codigoTH = this.selectTH.codigoTH;
    this.form.fechaInicio = this.selectTH.fechaInicio;
    this.form.fechaFin = this.selectTH.fechaFin;
    this.form.ubigeo = this.selectTH.ubigeo;
    this.form.tipoPersona = this.selectTH.tipoTH;
    this.form.nroDocumento = this.selectTH.numeroTH;
    this.form.areHA = this.selectTH.areaTotal;
    this.form.vigencia = this.selectTH.vigencia;
    this.form.modalidadTH = this.selectTH.modalidadTH;
    this.form.tipoDocumentoGestion = this.selectTH.tipoPlan;
    this.form.descripcion = this.selectTH.nombrePlan;

    if (this.form.ubigeo) {
      this.idDepartamento = parseInt(this.form.ubigeo.substr(0, 2));
      this.idProvincia = parseInt(this.form.ubigeo.substr(0, 4));
      this.idDistrito = parseInt(this.form.ubigeo);
      this.listarPorFiltroDepartamento(true);
    }

    if (this.selectTH.tipoTH == "TPERJURI") {
      this.nombreTitularCompleto = this.selectTH.nombreTH;
      this.form.nombreTitular = this.selectTH.nombreTH;
      this.nombreRepresentanteCompleto =
        (this.selectTH.nombreRepresentante || "") +
        " " +
        (this.selectTH.paternoRepresentante || "") +
        " " +
        (this.selectTH.maternoRepresentante || "");
      this.form.nombreRepresentante = this.selectTH.nombreRepresentante;
      this.form.paternoRepresentante = this.selectTH.paternoRepresentante;
      this.form.maternoRepresentante = this.selectTH.maternoRepresentante;
      if (this.selectTH.tipoDocumentoRepresentante == "TDOCDNI")
        this.form.tipoDocumentoRepresentante = "DNI";
      this.form.nroDocumentoRepresentante = this.selectTH.nroDocumentoRepresentante;
    } else {
      this.nombreTitularCompleto =
        (this.selectTH.nombreTH || "") +
        " " +
        (this.selectTH.paternoTH || "") +
        " " +
        (this.selectTH.maternoTH || "");
      this.form.nombreTitular = this.selectTH.nombreTH;
      this.form.paternoTitular = this.selectTH.paternoTH;
      this.form.maternoTitular = this.selectTH.maternoTH;
      if (this.selectTH.tipoDocumento == "TDOCDNI")
        this.form.tipoDocumento = "DNI";
      if (this.selectTH.tipoDocumento == "TDOCRUC")
        this.form.tipoDocumento = "RUC";
      this.form.nroDocumento = this.selectTH.nroDocumentoRepresentante;
    }
    this.idPlanTmp = this.selectTH.nroGestion;

    this.verModalBuscarTH = false;
  }

  validarTH(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectTH == null || this.selectTH == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Título Habilitante.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  guardarPago() {
    if (!this.validarDatos()) return;
    this.form.fechaInicio = this.form.fechaInicio
      ? new Date(this.form.fechaInicio)
      : null;
    this.form.fechaFin = this.form.fechaFin
      ? new Date(this.form.fechaFin)
      : null;
    // this.form.idPago = this.form.idPago ? this.form.idPago : 0;
    this.form.idPago = this.idPago ? this.idPago : 0;
    this.form.idUsuarioRegistro = this.usuarioService.idUsuario;

    var params = new PagoDA(this.form);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .registrarPago([params])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.success == true) {
          this.toast.ok("Se registró mecanismo de pago por DA correctamente.");
          this.idPlanTmp = 0;
          this.listarPago();
        } else {
          this.toast.error("Ocurrió un error.");
        }
      });
  }

  validarDatos(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.form.codigoTH == null || this.form.codigoTH == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Número de TH.\n";
    }

    if (
      this.nombreTitularCompleto == null ||
      this.nombreTitularCompleto == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Titular de TH.\n";
    }

    if (this.form.modalidadTH == null || this.form.modalidadTH == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Modalidad de TH o AAD.\n";
    }

    if (this.form.vigencia == null) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Vigencia.\n";
    }

    if (this.form.vigencia == 0) {
      validar = false;
      mensaje = mensaje += "(*) La Vigencia debe ser mayor a 0.\n";
    }

    if (this.form.fechaInicio == null || this.form.fechaInicio == "") {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe ingresar: Fecha Inicio vigencia de TH o AAD.\n";
    }

    if (this.form.fechaFin == null || this.form.fechaFin == "") {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe ingresar: Fecha Fin vigencia de TH o AAD.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  selectDPD(origen: string) {
    if (origen == "d") {
      this.listarPorFilroProvincia(true);
    }
    if (origen == "p") {
      this.listarPorFilroDistrito(true);
    }
  }

  listarPorFiltroDepartamento(ischange: boolean = false) {
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };

    this.planificacionService
      .listarPorFiltroDepartamento_core_central(params)
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.lstDepartamentos = result.data;
            if (this.lstDepartamentos && this.lstDepartamentos.length > 0)
              this.lstDepartamentos.unshift({
                idDepartamento: 0,
                nombreDepartamento: "Seleccione",
              });
            else
              this.lstDepartamentos = [
                { idDepartamento: 0, nombreDepartamento: "Seleccione" },
              ];
            // this.requestInfoMunicipio.idDepartamento = ischange
            //   ? this.lstDepartamentos[0].idDepartamento
            //   : this.requestInfoMunicipio.idDepartamento ||
            //     this.lstDepartamentos[0].idDepartamento;
            this.listarPorFilroProvincia(ischange);
          }
        },
        (error) => {}
      );
  }

  listarPorFilroProvincia(ischange: boolean = false) {
    const idDepartamento = this.idDepartamento;
    this.planificacionService
      .listarPorFilroProvincia({ idDepartamento: idDepartamento })
      .subscribe((result: any) => {
        if (result.success) {
          this.lstProvincias = result.data;
          if (this.lstProvincias && this.lstProvincias.length > 0)
            this.lstProvincias.unshift({
              idProvincia: 0,
              nombreProvincia: "Seleccione",
            });
          else
            this.lstProvincias = [
              { idProvincia: 0, nombreProvincia: "Seleccione" },
            ];

          this.listarPorFilroDistrito(ischange);
        }
      });
  }

  listarPorFilroDistrito(ischange: boolean = false) {
    const idProvincia = this.idProvincia;
    this.planificacionService
      .listarPorFilroDistrito({ idProvincia: idProvincia })
      .subscribe((result: any) => {
        if (result.success) {
          this.lstDistritos = result.data;
          if (this.lstDistritos && this.lstDistritos.length > 0)
            this.lstDistritos.unshift({
              codDistrito: "",
              nombreDistrito: "Seleccione",
            });
          else
            this.lstDistritos = [
              { codDistrito: "", nombreDistrito: "Seleccione" },
            ];
        }
      });
  }
}

export interface IComboModel {
  value: number;
  text: string;
}
