import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Page } from '@models';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { DerechoAprovechamientoService } from 'src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service';
import { GenericoService } from 'src/app/service/generico.service';
import { DownloadFile, ToastService } from "@shared";
import { CronogramaPA, PagoDA, PeriodoPA } from 'src/app/model/pagoDA';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { ArchivoService, UsuarioService } from '@services';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { PagoGuiaModel } from '../../../components/tabla-especies/tabla-especies.component';
import { ApiForestalService } from 'src/app/service/api-forestal.service';

@Component({
  selector: 'app-tab-registro-pago-da',
  templateUrl: './tab-registro-pago-da.component.html',
  styleUrls: [ './tab-registro-pago-da.component.scss' ]
})
export class TabRegistroPagoDaComponent implements OnInit {
  @Input() idPago!: number;
  @Output() public regresar = new EventEmitter();

  hoy: Date = new Date();
  mecanismoTabIndex: number = 0;
  baseContext: DataRegistroDA = new DataRegistroDA();
  cronogramaContext: CronogramaPA = new CronogramaPA();
  periodoContext: PeriodoPA = new PeriodoPA();
  lstTitular: ITituloHabilitante[] = [
    { selected: 1, nroTH: '0001', titular: 'titular 0001', modalidad: 'modalidad 0001', inicioVigencia: new Date(), finVigencia: new Date('30/06/2022'), nroDocumento: '40987654', nombreEntidad: 'Javier Perez', ubigeo: '001010', tipoPersona: 'N', nombreRepresentante: '', nroDocRepresentante: '' },
    { selected: 2, nroTH: '0002', titular: 'titular 0002', modalidad: 'modalidad 0002', inicioVigencia: new Date(), finVigencia: new Date('29/10/2022'), nroDocumento: '20987665431', nombreEntidad: 'Demo Prueba SAC', ubigeo: '001010', tipoPersona: 'J', nombreRepresentante: 'Jose Luna', nroDocRepresentante: '40987654' },
    { selected: 3, nroTH: '0003', titular: 'titular 0003', modalidad: 'modalidad 0003', inicioVigencia: new Date(), finVigencia: new Date('30/06/2022'), nroDocumento: '40987654', nombreEntidad: 'Javier Perez', ubigeo: '001010', tipoPersona: 'N', nombreRepresentante: '', nroDocRepresentante: '' },
    { selected: 4, nroTH: '0004', titular: 'titular 0004', modalidad: 'modalidad 0004', inicioVigencia: new Date(), finVigencia: new Date('29/10/2022'), nroDocumento: '20987665431', nombreEntidad: 'Demo Prueba SAC', ubigeo: '001010', tipoPersona: 'J', nombreRepresentante: 'Jose Luna', nroDocRepresentante: '40987654' },
    { selected: 5, nroTH: '0005', titular: 'titular 0005', modalidad: 'modalidad 0005', inicioVigencia: new Date(), finVigencia: new Date('30/06/2022'), nroDocumento: '40987654', nombreEntidad: 'Javier Perez', ubigeo: '001010', tipoPersona: 'N', nombreRepresentante: '', nroDocRepresentante: '' },
    { selected: 6, nroTH: '0006', titular: 'titular 0006', modalidad: 'modalidad 0006', inicioVigencia: new Date(), finVigencia: new Date('29/10/2022'), nroDocumento: '20987665431', nombreEntidad: 'Demo Prueba SAC', ubigeo: '001010', tipoPersona: 'J', nombreRepresentante: 'Jose Luna', nroDocRepresentante: '40987654' },
    { selected: 7, nroTH: '0007', titular: 'titular 0007', modalidad: 'modalidad 0007', inicioVigencia: new Date(), finVigencia: new Date('30/06/2022'), nroDocumento: '40987654', nombreEntidad: 'Javier Perez', ubigeo: '001010', tipoPersona: 'N', nombreRepresentante: '', nroDocRepresentante: '' },
    { selected: 8, nroTH: '0008', titular: 'titular 0008', modalidad: 'modalidad 0008', inicioVigencia: new Date(), finVigencia: new Date('29/10/2022'), nroDocumento: '20987665431', nombreEntidad: 'Demo Prueba SAC', ubigeo: '001010', tipoPersona: 'J', nombreRepresentante: 'Jose Luna', nroDocRepresentante: '40987654' }
  ];

  accionPeriodo: string = "";
  indexPeriodo: number = 0;
  verModalBuscarTH: boolean = false;
  tituloAnioModal: string = "";
  tituloPeriodoModal: string = "";
  verModalPeriodo: boolean = false;
  cboMonedas: IComboMonedaModel[] = [
    { text: "S/", value: "SOLES" },
    { text: "$", value: "DOLARES" }
  ];
  cboTipoMontoPA: IComboTipoModel[] = [
    { text: 'Oferta', value: "1" },
    { text: 'UIT', value: "2" }
  ];
  cboPlanesManejo: IComboModel[] = [
    { text: '0001 | Descripción de Plan de Manejo 0001', value: 1 },
    { text: '0002 | Descripción de Plan de Manejo 0002', value: 2 },
    { text: '0003 | Descripción de Plan de Manejo 0003', value: 3 },
    { text: '0004 | Descripción de Plan de Manejo 0004', value: 4 },
    { text: '0005 | Descripción de Plan de Manejo 0005', value: 5 },
    { text: '0006 | Descripción de Plan de Manejo 0006', value: 6 },
    { text: '0007 | Descripción de Plan de Manejo 0007', value: 7 }
  ];

  form: PagoDA = new PagoDA;
  loading = false;
  totalRecords = 0;
  selectedValues: any[] = [];
  selectTH: any;
  nombreTitularCompleto: string = '';
  nombreRepresentanteCompleto: string = '';
  listSelectMecaPago: any[] = [];
  disableTab1: boolean = true;
  disableTab2: boolean = true;
  perfiles = Perfiles;
  perfil: any;
  tipoCambio!: number;
  uit!: number;
  listDescuento: any = [];
  totalDescuento: number = 0;
  listaEspecies: any[] = [];
  disabledGuiaPago = true;
  montoTotal: number = 0;
  listDocumentos: any = [];
  totalRecordsID = 0;
  disableR1: boolean = true;
  disableR2: boolean = true;
  disableR3: boolean = true;
  tipoPlan: string = '';
  tipoPlanDescripcion: string = '';
  idPlanManejo!: number;
  isRegistrado: boolean = false;

  cabeceraEspecies: PagoGuiaModel = new PagoGuiaModel();

  constructor(
    private genericoServ: GenericoService,
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private archivoService: ArchivoService,
    private messageService: MessageService,
    private apiForestalService: ApiForestalService
  ) { }

  ngOnInit(): void {
    this.consultarUIT();
    this.listarPago();
    this.listarMecanismosPago();
    this.listarDescuento();
    this.perfil = this.usuarioService.usuario.sirperfil;
    // this.tipoCambio = 3.725;
    // this.uit = 4600;
  }

  listarPago() {
    const params = {
      idPago: this.idPago,
      pageNum: 1,
      pageSize: 10
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .listarPago(params)
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.data.length > 0) {
          this.form = new PagoDA(res.data[ 0 ]);
          this.tipoPlan = this.form.tipoDocumentoGestion || '';
          this.tipoPlanDescripcion = this.form.descripcion || '';
          this.idPlanManejo = this.form.nrDocumentoGestion || 0;
          // this.onChangeMP({ value: this.form.mecanismoPago });
          this.clickMP({ value: this.form.mecanismoPago });
          if (this.form.listaCronograma.length == 0) {
            this.calcularCronogAnual();
          }
          if (this.form.codigoTH) {
            this.isRegistrado = true;
          }
        }
      });
  }

  seleccionarTH(data: any): void {
    this.form.perfil = this.usuarioService.usuario.sirperfil;
    this.form.nrDocumentoGestion = data.nroGestion;
    this.form.mecanismoPago = data.mecanismo;
    this.form.codigoTH = data.codigoTH;
    this.form.fechaInicio = data.fechaInicio;
    this.form.fechaFin = data.fechaFin;
    this.form.ubigeo = data.ubigeo;
    this.form.tipoPersona = data.tipoTH;
    this.form.nroDocumento = data.numeroTH;
    this.form.areHA = data.areaTotal;
    this.form.vigencia = data.vigencia;
    this.form.modalidadTH = data.modalidadTH;
    this.form.tipoDocumentoGestion = this.selectTH.tipoPlan;
    this.form.descripcion = data.nombrePlan;
    this.tipoPlan = data.tipoPlan;
    this.tipoPlanDescripcion = data.nombrePlan;
    if (data.tipoTH == 'TPERJURI') {
      this.nombreTitularCompleto = data.nombreTH;
      this.form.nombreTitular = data.nombreTH;
      this.nombreRepresentanteCompleto = (data.nombreRepresentante || '') + ' ' + (data.paternoRepresentante || '') + ' ' + (data.maternoRepresentante || '');
      this.form.nombreRepresentante = data.nombreRepresentante;
      this.form.paternoRepresentante = data.paternoRepresentante;
      this.form.maternoRepresentante = data.maternoRepresentante;
      if (data.tipoDocumentoRepresentante == 'TDOCDNI') this.form.tipoDocumentoRepresentante = 'DNI';
      this.form.nroDocumentoRepresentante = data.nroDocumentoRepresentante;
    } else {
      this.nombreTitularCompleto = (data.nombreTH || '') + ' ' + (data.paternoTH || '') + ' ' + (data.maternoTH || '');
      this.form.nombreTitular = data.nombreTH;
      this.form.paternoTitular = data.paternoTH;
      this.form.maternoTitular = data.maternoTH;
      if (data.tipoDocumento == 'TDOCDNI') this.form.tipoDocumento = 'DNI';
      if (data.tipoDocumento == 'TDOCRUC') this.form.tipoDocumento = 'RUC';
      this.form.nroDocumento = data.nroDocumentoRepresentante;
    }

    this.onChangeMP({ value: data.mecanismo });
    this.calcularCronogAnual();
  }

  private calcularCronogAnual(): void {
    let dInicio: Date = new Date(this.form.fechaInicio);

    for (let index: number = 0; index < (this.form.vigencia || 0); index++) {
      let dFin = new Date(dInicio.getFullYear() + 1, dInicio.getMonth(), dInicio.getDate());
      let item: CronogramaPA = new CronogramaPA({
        anio: (index + 1),
        fechaFin: dFin,
        fechaInicio: dInicio,
        idPagoCronograma: 0,
        idUsuarioRegistro: this.usuarioService.idUsuario,
        listaPeriodo: [],
        perfil: this.usuarioService.usuario.sirperfil,
        verPeriodo: false
      });
      // this.baseContext.cronogramaPagos.push(item);
      this.form.listaCronograma.push(item);
      dInicio = dFin;
    }
  }

  listarMecanismosPago() {
    const params = { prefijo: 'MECPAGO' };
    this.genericoServ.listarPorFiltroParametro(params).subscribe(
      (result: any) => {
        this.listSelectMecaPago = result.data;
      }
    );
  }

  listarDescuento() {
    const params = {
      idDescuento: null,
      pageNum: 1,
      pageSize: 10,
    };
    this.derechoAprovechamientoServ
      .listarDescuento(params)
      .subscribe((result: any) => {
        if (result.data.length > 0) {
          this.listDescuento = result.data;
          result.data.forEach((item: any) => {
            this.totalDescuento += Number(item.porcentaje);
          });
        }
      });
  }

  consultarUIT() {
    this.tipoCambio = 3.725;
    this.uit = 4600;
    this.apiForestalService
      .consultarUIT()
      .subscribe((result: any) => {
        if (result.dataService) {
          this.tipoCambio = result.dataService.respuesta.compra;
          this.uit = Number(result.dataService.respuesta.tasa.replace(',', ''));
        }
      })
  }

  onChangeMP(e: any): void {
    if (e.value == 'MECPAGOMOD1') {
      this.mecanismoTabIndex = 0;
      this.disableTab1 = false;
      this.disableTab2 = true;
    } else if (e.value == 'MECPAGOMOD2') {
      this.mecanismoTabIndex = 1;
      this.disableTab1 = true;
      this.disableTab2 = false;
      this.listarGuiaPago();
    } else if (e.value == 'MECPAGOMOD3') {
      this.mecanismoTabIndex = 0;
      this.disableTab1 = false;
      this.disableTab2 = false;
      this.listarGuiaPago();
    }
  }

  clickMP(e: any): void {
    if (this.form.mecanismoPago == 'MECPAGOMOD1') {
      this.mecanismoTabIndex = 0;
      this.disableTab1 = false;
      this.disableTab2 = true;
      this.disableR1 = false;
      this.disableR2 = true;
      this.disableR3 = true;
    } else if (this.form.mecanismoPago == 'MECPAGOMOD2') {
      this.mecanismoTabIndex = 1;
      this.disableTab1 = true;
      this.disableTab2 = false;
      this.disableR1 = true;
      this.disableR2 = false;
      this.disableR3 = true;
      this.listarGuiaPago();
    } else if (this.form.mecanismoPago == 'MECPAGOMOD3') {
      this.mecanismoTabIndex = 0;
      this.disableTab1 = false;
      this.disableTab2 = false;
      this.disableR1 = true;
      this.disableR2 = true;
      this.disableR3 = false;
      this.listarGuiaPago();
    }
  }

  listarGuiaPago() {
    this.listaEspecies = [];
    const params = {
      idPagoGuia: null,
      idPago: this.form.idPago || 0,
      pageNum: 1,
      pageSize: 10,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .listarGuiaPago(params)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.data.length > 0) {
          this.cabeceraEspecies = new PagoGuiaModel(res.data[0]);
          this.cabeceraEspecies.nrDocumentoGestion= res.data[0].nrDocumentoGestion
          this.listaEspecies = this.cabeceraEspecies.listaGuiaEspecie;
          this.validaCamposEspecie();
          this.calcularMontoTotal();
        }
      });
  }

  validaCamposEspecie() {
    const lista = this.listaEspecies.filter(
      (value: any) => value.volumen == null
    );
    this.disabledGuiaPago = lista.length > 0;
  }

  calcularMontoTotal() {
    this.listaEspecies.map((value: any) => {
      this.montoTotal += value.montoFinal;
    });
  }

  changeMontoXha(event: any): void {
    this.form.montoPagoAnual = (this.form.areHA || 0) * event.target.value;

    this.onChangeMoneda({ value: this.form.tipoMoneda });
  }

  onChangeMoneda(event?: any) {
    if (event.value == 'DOLARES' || this.form.tipoMoneda == 'DOLARES') {
      this.form.montoPagoAnualCalculado = (this.form.montoPagoAnual || 0) * this.tipoCambio;
    } else {
      this.form.montoPagoAnualCalculado = null;
    }
  }

  onChangeTipo(event: any) {
    if (event.value == '2') {
      this.form.montoPagoHA = this.uit * (0.01 / 100);
    } else {
      this.form.montoPagoHA = null;
    }
    this.changeMontoXha({ target: { value: this.form.montoPagoHA } });
  }

  verPeriodosPago(data: CronogramaPA): void {
    data.verPeriodo = !data.verPeriodo;
  }

  abrirModalPeriodo(accion: string, cronograma: CronogramaPA, periodo?: any, index?: any): void {
    this.cronogramaContext = cronograma;
    this.accionPeriodo = accion;
    this.indexPeriodo = index;
    if (accion == "N") {
      this.periodoContext = new PeriodoPA();
      this.periodoContext.idPagoPeriodo = 0;
      this.periodoContext.idUsuarioRegistro = this.usuarioService.idUsuario;
      this.periodoContext.perfil = this.usuarioService.usuario.sirperfil;
      this.tituloPeriodoModal = "Agregar Periodo Pago";
    } else if (accion == "E") {
      this.periodoContext = new PeriodoPA(periodo);
      this.tituloPeriodoModal = "Editar Periodo Pago";
    }
    if (!this.form.montoPagoHA || this.form.montoPagoHA <= 0) {
      this.toast.warn("antes de agregar un periodo debe colocar un Monto de pago por hectárea");
    } else {
      this.verModalPeriodo = true;
    }
  }

  agregarPeriodo(): void {
    if (this.accionPeriodo == "N") {
      this.cronogramaContext.listaPeriodo.push(new PeriodoPA(this.periodoContext));
    } else if (this.accionPeriodo == "E") {
      // let index = this.cronogramaContext.listaPeriodo.findIndex(x => x.idPagoPeriodo == this.periodoContext.idPagoPeriodo);
      this.cronogramaContext.listaPeriodo[this.indexPeriodo] = this.periodoContext;
    }
    this.verModalPeriodo = false;
  }

  listarArchivos() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      extension: 'pdf',
      tipoDocumento: 'REREEVPL'
    };
    this.loading = true;
    this.anexosService
      .listarArchivosPlanManejo(params)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          this.totalRecordsID = response.data.length;
          response.data.forEach((item: any) => {
            if (item.extension === '.pdf') {
              this.listDocumentos.push(item);
            }
          });
        }
        this.dialog.closeAll();
      });
  }

  descargarArchivo(idArchivo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoService
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.errorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.errorMensaje('No se ha encontrado un documento vinculado.');
      this.dialog.closeAll();
    }
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  guardarPago() {
    this.form.fechaInicio = this.form.fechaInicio ? new Date(this.form.fechaInicio) : null;
    this.form.fechaFin = this.form.fechaFin ? new Date(this.form.fechaFin) : null;
    this.form.idPago = this.form.idPago ? this.form.idPago : 0;
    this.form.idUsuarioRegistro = this.usuarioService.idUsuario;

    var params = new PagoDA(this.form);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.derechoAprovechamientoServ
      .registrarPago([ params ])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        this.dialog.closeAll();
        if (res.success == true) {
          this.toast.ok('Se registró mecanismo de pago por DA correctamente.')
          this.listarPago();
        } else {
          this.toast.error('Ocurrió un error.')
        }
      });
  }

  regresarTab() {
    this.regresar.emit();
  }
}

export class DataRegistroDA {
  constructor() { }

  public selectedTH: number = 0;

  public nroTH: string = "";
  public titularTH: string = "";

  public mp_superficie: boolean = false;
  public mp_valor_volumen: boolean = false;
  public modalidad: string = "";
  public inicioVigencia: Date = new Date();
  public finVigencia?: Date;
  public nroDocuemnto: string = "";
  public nombreEntidad: string = "";
  public ubigeo: string = "";
  public representanteNombre: string = "";
  public representanteDoc = "";

  public montoDaAnual: number = 0;
  public pagoXha: number = 0;
  public tipo_monto_pago_ha: number = 1;
  public monedaMontoPagoHA: number = 1;

  public tipoPersona: string = "";
  public cronogramaPagos: CronogramaPagosAnualModel[] = [];

}

export class CronogramaPagosAnualModel {
  constructor(data?: any) {
    if (data) {
      this.anhio = data.anhio;
      this.fechaFin = data.fechaFin;
      this.fechaInicio = data.fechaInicio;
      this.verPeriodo = data.verPeriodo;
      this.periodos = data.periodos;
      return;
    }
  }

  public anhio: string = "";
  public fechaInicio: Date = new Date();
  public fechaFin = new Date();
  public verPeriodo: boolean = false;
  public periodos: PeriodoPagoAnualModel[] = [];
}

export class PeriodoPagoAnualModel {
  constructor(data?: any) {
    if (data) {
      this.idPagoPeriodo = data.idPagoPeriodo;
      this.codTipoPagoPeriodo = data.codTipoPagoPeriodo;
      this.fechaComprobante = data.fechaComprobante;
      this.observacion = data.observacion;
      this.detalle = data.detalle;
      this.descripcion = data.descripcion;
      this.balancePago = data.balancePago;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.periodo = data.periodo;
      this.fechaLimite = data.fechaLimite;
      this.monto = data.monto;
      this.montoFinal = data.montoFinal;
      this.id = data.id;
      return;
    }
  }

  public id: string = new Date().toISOString();
  public idPagoPeriodo: number = 0;
  public codTipoPagoPeriodo: string = "";
  public fechaLimite: Date = new Date();
  public fechaComprobante: Date = new Date();
  public periodo: string = "";
  public observacion: string = "";
  public detalle: string = "";
  public descripcion: string = "";
  public monto: number = 0;
  public montoFinal: number = 0;
  public balancePago: number = 0;
  public idUsuarioRegistro: number = 0;

}

export interface ITituloHabilitante {
  selected: number;
  nroTH: string;
  titular: string;
  modalidad: string;
  inicioVigencia: Date;
  finVigencia: Date;
  nroDocumento: string;
  nombreEntidad: string;
  ubigeo: string;
  tipoPersona: string;
  nombreRepresentante: string;
  nroDocRepresentante: string;
}

export interface IComboMonedaModel {
  value: string;
  text: string;
}

export interface IComboTipoModel {
  value: string;
  text: string;
}

export interface IComboModel {
  value: number;
  text: string;
}
