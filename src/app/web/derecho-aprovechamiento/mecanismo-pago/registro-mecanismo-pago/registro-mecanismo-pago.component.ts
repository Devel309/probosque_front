import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Page } from '@models';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DerechoAprovechamientoService } from 'src/app/service/derecho-aprovechamiento/derecho-aprovechamiento.service';
import {Perfiles} from '../../../../model/util/Perfiles';

@Component({
  selector: 'app-registro-mecanismo-pago',
  templateUrl: './registro-mecanismo-pago.component.html',
  styleUrls: [ './registro-mecanismo-pago.component.scss' ]
})
export class RegistroMecanismoPagoComponent implements OnInit {
  idPago!: number;
  idPlanManejo!: number;
  codigoTH!: string;
  tabIndex: number = 0;
  //perfil :String="";


  constructor(
    private route: ActivatedRoute,
    private derechoAprovechamientoServ: DerechoAprovechamientoService,
    private usuarioService: UsuarioService,
  ) {
    this.idPago = Number(this.route.snapshot.paramMap.get('idPago'));
  }

  ngOnInit(): void {

   /* let idUsuarioA = this.usuarioService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.perfil = Perfiles.AUTORIDAD_REGIONAL_CAJA;
    } else {
      this.perfil = this.usuarioService.usuario.sirperfil;
    }*/
    this.listarPago();
  }

  listarPago() {
    const params = {
      idPago: this.idPago,
      pageNum: 1,
      pageSize: 10
    }
    this.derechoAprovechamientoServ
      .listarPago(params)
      .subscribe((res: any) => {
        if (res.data.length > 0) {
          this.codigoTH = res.data[0].codigoTH;
          this.idPlanManejo = res.data[0].nrDocumentoGestion;
        }
      });
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

}
