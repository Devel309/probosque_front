import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/service/config.service';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { SolicitudAccesoService } from 'src/app/service/solicitudAcceso.service';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { IDPerfil } from 'src/app/shared/const';
import { CodigoAplicacion } from 'src/app/shared/const';
import { MessageService } from 'primeng/api';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { ToastService } from '@shared';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';

@Component({
  selector: 'app-registrar-solicitud-acesso',
  templateUrl: './registrar-solicitud-acesso.component.html',
  styleUrls: ['./registrar-solicitud-acesso.component.scss'],
  providers: [MessageService],
})
export class RegistrarSolicitudAcessoComponent implements OnInit {
  perfiles = IDPerfil;
  aplicaciones = CodigoAplicacion;
  dataBase: any = {
    disabled: false,
    tipoPersona: 'Natural',
  };

  archivo!: File;

  validar: boolean = false;

  constructor(
    private serv: ParametroValorService,
    private messageService: MessageService,
    private servSA: SolicitudAccesoService,
    private router: Router,
    public _configService: ConfigService,
    private pideService: PideService,
    private _toast: ToastService,
    private dialog: MatDialog,
  ) {
    this._configService.config = {
      useLayout: false,
    };
  }
  solicitudAcceso = {} as SolicitudAccesoModel;
  idSolicitudAcceso!: String;

  ngOnInit(): void {}

  enviarSolicitudAcceso() {
    this.solicitudAcceso.idUsuarioRegistro = 3647; //cambiar por usuario actual
    this.solicitudAcceso.idPerfil = this.perfiles.ID_ADMIN_MC_SNIFFS;
    this.solicitudAcceso.codigoAplicacion = this.aplicaciones.MC_SNIFFS;
    if (!this.validarRegistroSolicitudAcceso()) {
      return;
    }
   // return;
   this.dialog.open(LoadingComponent, { disableClose: true });
    this.servSA
      .registrarSolicitudAcceso(this.solicitudAcceso)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.idSolicitudAcceso = result.data.codigo;
        // this.enviarCorreoValidacion();
        this.enviarArchivoSolicitudAcceso(this.idSolicitudAcceso);
        //this.toast('success', 'La solicitud se registró correctamente.');
        if(result.success){
          this._toast.ok('La solicitud se registró correctamente.');
        }else{
          this._toast.warn(result.message);
          return;
        }
        
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 2000);
      }, () => {
        this.dialog.closeAll();
      });
  }

  enviarCorreoValidacion(): any {
    var params = {
      content: 'Se ha registrado una nueva solicitud de Acceso.',
      email: 'soporte_sharepoint@valtx.pe', //colocar correo del revisor.
      subject: 'Mensaje de Solicitud de Acceso',
    };

    this.servSA.enviarCorreo(params).subscribe((result: any) => {
      //alert(result.message);
    });
  }

  validarRegistroSolicitudAcceso(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';


    if (this.solicitudAcceso.codigoTipoPersona == null) {
      validar = false;
      this.toast('warn', '(*) Debe seleccionar un tipo de persona.');
    }

    if (this.solicitudAcceso.codigoTipoActor == null) {
      validar = false;
      this.toast('warn', '(*) Debe seleccionar un tipo de actor.');
    }

    if (this.solicitudAcceso.codigoTipoActor == 'TACTTCNC') {
      if (this.solicitudAcceso.codigoTipoCncc == null) {
        validar = false;
        this.toast('warn', '(*) Debe seleccionar un tipo de CNCC.');
      }
    }

    if (this.solicitudAcceso.codigoTipoDocumento == null) {
      validar = false;
      this.toast(
        'warn',
        '(*) Debe seleccionar un tipo de documento de Persona.'
      );
    }

    if (
      this.solicitudAcceso.numeroDocumento == null ||
      this.solicitudAcceso.numeroDocumento == ''
    ) {
      validar = false;
      this.toast('warn', '(*) Ingrese un número de documento.');
    } else {
      if (
        (this.solicitudAcceso.codigoTipoDocumento == 'TDOCDNI' ||
          this.solicitudAcceso.codigoTipoDocumento == 'TDOCEXTR') &&
        this.solicitudAcceso.numeroDocumento.length != 8
      ) {
        validar = false;
        this.toast(
          'warn',
          '(*) El número de documento de DNI y/o Carnet de Extranjería debe tener un max. de 8 caracteres.'
        );
      }
    }

    if (
      this.solicitudAcceso.nombres == null ||
      this.solicitudAcceso.nombres == ''
    ) {
      validar = false;
      this.toast('warn', '(*) Ingrese sus nombres completos.');
    }

    if (
      this.solicitudAcceso.apellidoPaterno == null ||
      this.solicitudAcceso.apellidoPaterno == ''
    ) {
      validar = false;
      this.toast('warn', '(*) Ingrese su apellido paterno.');
    }

    if (
      this.solicitudAcceso.apellidoMaterno == null ||
      this.solicitudAcceso.apellidoMaterno == ''
    ) {
      validar = false;
      this.toast('warn', '(*) Ingrese su apellido materno.');
    }

    if (this.solicitudAcceso.codigoTipoPersona == 'TPERJURI') {
      //validar documento ruc solo si se ha seleccionado jurídica

      if (!this.solicitudAcceso.isValidEmpresaPIDE) {
        validar = false;
        this.toast('warn', '(*) Debe validar el PIDE para tipo de persona Jurídica.');
      }


      if (
        this.solicitudAcceso.numeroRucEmpresa == null ||
        this.solicitudAcceso.numeroRucEmpresa == ''
      ) {
        validar = false;
        this.toast('warn', '(*) Ingrese el RUC de la empresa.');
      } else {
        if (this.solicitudAcceso.numeroRucEmpresa.length != 11) {
          validar = false;
          this.toast(
            'warn',
            '(*) El número de RUC debe tener un max. de 11 caracteres.'
          );
        }
      }
      if (
        this.solicitudAcceso.razonSocialEmpresa == null ||
        this.solicitudAcceso.razonSocialEmpresa == ''
      ) {
        validar = false;
        this.toast('warn', '(*) Ingrese la razón social de la empresa.');
      }


      if (
        this.solicitudAcceso.emailEmpresa == null ||
        this.solicitudAcceso.emailEmpresa == ''
      ) {
        validar = false;
        this.toast('warn', '(*) Ingrese un email para los Datos de la Empresa.');
      }else{
        if(!this.validarEmail(this.solicitudAcceso.emailEmpresa)){
          validar = false;
          this.toast('warn', '(*) El email para los Datos de la Empresa tienene un formato incorrecto.');
        }
      }

      if (
        this.solicitudAcceso.email == null ||
        this.solicitudAcceso.email == ''
      ) {
        validar = false;
        this.toast('warn', '(*) Ingrese un email para los Datos de la Persona.');
      }else{
        if(!this.validarEmail(this.solicitudAcceso.email)){
          validar = false;
          this.toast('warn', '(*) El email para los Datos de la Persona tienene un formato incorrecto.');
        }
      }

    } else if (this.solicitudAcceso.codigoTipoPersona == 'TPERNATU'){

      if (!this.solicitudAcceso.isValidPIDE) {
        validar = false;
        this.toast('warn', '(*) Debe validar el PIDE para tipo de persona Natural.');
      }


      if (
        this.solicitudAcceso.email == null ||
        this.solicitudAcceso.email == ''
      ) {
        validar = false;
        this.toast('warn', '(*) Ingrese un email para los Datos de la Persona.');
      }else{
        if(!this.validarEmail(this.solicitudAcceso.email)){
          validar = false;
          this.toast('warn', '(*) El email para los Datos de la Persona tienene un formato incorrecto.');
        }
      }



    }

    if (
      this.solicitudAcceso.idDepartamentoPersona == null ||
      this.solicitudAcceso.idDepartamentoPersona == 0
    ) {
      validar = false;
      this.toast('warn', '(*) Debe seleccionar el departamento.');
    }



    if (
      this.solicitudAcceso.idProvinciaPersona == null ||
      this.solicitudAcceso.idProvinciaPersona == 0
    ) {
      validar = false;
      this.toast('warn', '(*) Debe seleccionar la provincia.');
    }

    if (
      this.solicitudAcceso.idDistritoPersona == null ||
      this.solicitudAcceso.idDistritoPersona == 0
    ) {
      validar = false;
      this.toast('warn', '(*) Debe seleccionar el distrito.');
    }

    if (this.archivo == undefined) {
      validar = false;
      this.toast('warn', '(*) Debe adjuntar un archivo.');
    }

    return validar;
  }


   validarEmail(valor:any) {
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
          return true;
    } else {
        return false;
    }
  }

  ErrorMensaje(mensaje: any) {
    //this.messageService.add({severity:"warn", summary: "", detail: mensaje});
  }

  obtenerArchivo(file: File) {
    this.archivo = file;
  }

  enviarArchivoSolicitudAcceso(idSolicitudAcceso: String) {
    let file: File = this.archivo!;
    this.servSA
      .cargarArchivoSolicitudAcceso(file, idSolicitudAcceso)
      .subscribe((result: any) => {
        console.info(result);
      });
  }

  validarPide() {
    // var params = {
    // };

    // this.pideService.consultarDNI(params).subscribe((result: any) => {

    // })

    this.validar = true;
  }

  toast(severty: string, msg: string) {
    this.messageService.add({ severity: severty, summary: '', detail: msg });
  }
}
