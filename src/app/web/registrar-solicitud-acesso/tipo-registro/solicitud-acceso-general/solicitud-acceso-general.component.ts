import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { DepartamentoModel } from "src/app/model/Departamento";
import { ProvinciaModel } from "src/app/model/Provincia";
import { DistritoModel } from 'src/app/model/Distrito';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import {SolicitudAccesoService} from "../../../../service/solicitudAcceso.service";
import { DownloadFile} from "../../../../shared/util";
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'solicitud-acceso-general',
  templateUrl: './solicitud-acceso-general.component.html',
  styleUrls: ['./solicitud-acceso-general.component.scss']
})
export class SolicitudAccesoGeneralComponent implements OnInit {
  constructor(
    private serv : ParametroValorService,
    private servCoreCentral : CoreCentralService,
    private servSA : SolicitudAccesoService,
    private dialog: MatDialog,
    private router: Router) { }
  //Parametro
  departamento={} as DepartamentoModel;
  listDepartamento: DepartamentoModel[]=[];
  provincia={} as ProvinciaModel;
  listProvincia: ProvinciaModel[]=[];
  distrito={} as DistritoModel;
  listDistrito: DistritoModel[]=[];
  lstEstado:any[]=[];
  lstTipoPersona:any[]=[];
  lstTipoActor:any[]=[];
  lstTipoCNCC:any[]=[];

  @Input ('dataBase') dataBase:any={
    disabled:false,
    tipoPersona:''
  };
  @Input ('solicitudAcceso') solicitudAcceso:any={};


  @Output() asignarArchivo = new EventEmitter<File>();
  file!: File;
  fileName:string='';

  ngOnInit(): void {
    this.listarEstado();
    this.listarTipoPersona();
    this.listarTipoActor();
    this.listarTipoCNCC();

    this.listarPorFiltroDepartamento(this.departamento);
    this.onGetNombreArchivo();
  }

/******************************************************/
/*Métodos**********************************************/
/******************************************************/
listarEstado(){
  var params ={ prefijo: 'ESAC' }
  //acá debe obtener de tabla estados
  this.serv.listarPorCodigoParametroValor(params).subscribe(
    (result : any)=>{this.lstEstado = result.data;}
  );
};
listarTipoPersona(){
  var params ={ prefijo: 'TPER' }
  this.serv.listarPorCodigoParametroValor(params).subscribe(
    (result : any)=>{this.lstTipoPersona = result.data;}
  );
};
listarTipoActor(){
  var params ={ prefijo: 'TACT' }
  this.serv.listarPorCodigoParametroValor(params).subscribe(
    (result : any)=>{this.lstTipoActor = result.data;}
  );
};
listarTipoCNCC(){
  var params ={ prefijo: 'TCNC' }
  this.serv.listarPorCodigoParametroValor(params).subscribe(
    (result : any)=>{this.lstTipoCNCC = result.data;}
  );
};
listarPorFiltroDepartamento(departamento: DepartamentoModel) {

  this.dialog.open(LoadingComponent, { disableClose: true });

  this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result:any) => {
        this.dialog.closeAll();

       this.listDepartamento=(result.data);
       this.listarPorFilroProvincia(this.provincia);
      },(error:any)=>{
        //this.messageService.add({severity:"warn", summary: "", detail: error.message});
      }
  );
};
listarPorFilroProvincia(provincia: ProvinciaModel) {
  this.dialog.open(LoadingComponent, { disableClose: true });
  this.servCoreCentral.listarPorFilroProvincia(provincia).subscribe(
      (result:any) => {
        this.dialog.closeAll();
          this.listProvincia = result.data;
          this.listarPorFilroDistrito(this.distrito);
      }
  );
};
listarPorFilroDistrito(distrito: DistritoModel) {
  this.dialog.open(LoadingComponent, { disableClose: true });
  this.servCoreCentral.listarPorFilroDistrito(distrito).subscribe(
      (result:any) => {
        this.dialog.closeAll();
          this.listDistrito = result.data;
      }
  );
};
onSelectedProvincia(param: any){
  this.listProvincia=[];
  this.listDistrito=[];
  const provincia={} as ProvinciaModel;
        provincia.idDepartamento=param.value;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servCoreCentral.listarPorFilroProvincia(provincia).subscribe(
      (result:any) => {
          this.dialog.closeAll();
          this.listProvincia = result.data;
      }
  );
};
onSelectedDistrito(param: any){
    this.listDistrito=[];
    const distrito={} as ProvinciaModel;
          distrito.idProvincia=param.value;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servCoreCentral.listarPorFilroDistrito(distrito).subscribe(
      (result:any) => {
        this.dialog.closeAll();
          this.listDistrito = result.data;
      }
  );

};

  onFileSelected(event:any):void {
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.file = event.target.files[0];
    this.asignarArchivo.emit(this.file);
    this.fileName = event.target.files[0].name;
    setTimeout(() => {this.dialog.closeAll();},1000);

  }

  onGetNombreArchivo():void{
    if(this.dataBase.disabled){
      this.servSA.obtenerArchivoSolicitud(this.solicitudAcceso.idSolicitudAcceso).subscribe(
        (result:any) => {

          this.fileName = result.data.nombre.concat('.',result.data.extension);
        });
    }
  };

  onDescargarArchivo():void{
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.servSA.descargarArchivoSolicitudAcceso(this.solicitudAcceso.idSolicitudAcceso).subscribe(
      (data:any) => {
        let archive:string =  this.fileName;
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
        this.dialog.closeAll();
      });
  }
}
