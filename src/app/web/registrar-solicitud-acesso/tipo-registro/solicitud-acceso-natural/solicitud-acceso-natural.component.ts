import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { ParametroValorService } from 'src/app/service/parametro.service';
//import { MessageService } from 'primeng/api';

@Component({
  selector: 'solicitud-acceso-natural',
  templateUrl: './solicitud-acceso-natural.component.html',
  styleUrls: ['./solicitud-acceso-natural.component.scss']
})
export class SolicitudAccesoNaturalComponent implements OnInit {
  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private serv: ParametroValorService,
    private pideService: PideService
  ) { }
  //Parametro
  lstTipoDocumento: any[] = [];

  @Input('dataBase') dataBase: any = {
    disabled: false,
    tipoPersona: ''
  };

  @Input('solicitudAcceso') solicitudAcceso: any = {};
  checkedRepresentante: boolean = false;

  validaPIDEClass: boolean = false;

  ngOnInit(): void {
    this.listarTipoDocumento();
    this.checkedRepresentante = this.solicitudAcceso.codigoTipoPersona == 'TPERJURI';
  }

  /*Métodos**********************************************/
  listarTipoDocumento() {
    var params = { prefijo: 'TDOCIN' }
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoDocumento = result.data;
      }
    );
  };

  validarPide() {
    if (!this.solicitudAcceso.codigoTipoDocumento) {
      this.ErrorMensaje('(*) Debe seleccionar un tipo de documento de Persona.');
      return;

    } else if (!this.solicitudAcceso.numeroDocumento) {
      this.ErrorMensaje('(*) Ingrese un número de documento.');
      return;
    }

    let params = {
      numDNIConsulta: this.solicitudAcceso.numeroDocumento
    };

    if (this.solicitudAcceso.codigoTipoDocumento === "TDOCDNI") {
      if(this.solicitudAcceso.numeroDocumento.toString().length !== 8){
        this.ErrorMensaje('(*) El número de documento de DNI debe tener 8 digitos.');
        return;
      }
      this.consultarDNI(params);

    } else if (this.solicitudAcceso.codigoTipoDocumento === "TDOCRUC") {
      if(this.solicitudAcceso.numeroDocumento.toString().length !== 11){
        this.ErrorMensaje('(*) El número de documento de RUC debe tener 11 digitos.');
        return;
      }
      this.consultarRazonSocial({"numRUC": this.solicitudAcceso.numeroDocumento});

    } else if (this.solicitudAcceso.codigoTipoDocumento === "TDOCEXTR") {
      if(this.solicitudAcceso.numeroDocumento.toString().length > 20){
        this.ErrorMensaje('(*) El número de documento de CE debe tener un máximo de 20 carácteres.');
        return;
      }
      
      // this.consultarDocumentoCE(params);
    }

  }

  consultarDNI(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if(result.dataService){

          let persona = result.dataService.datosPersona;
          if(result.dataService.datosPersona){

            this.solicitudAcceso.nombres = persona.prenombres;
            this.solicitudAcceso.apellidoMaterno = persona.apSegundo;
            this.solicitudAcceso.apellidoPaterno = persona.apPrimer;

            this.cambiarValidPIDE(true);
            this.SuccessMensaje('Se validó existencia de DNI en RENIEC.');
          }else{
            this.cambiarValidPIDE(false);
            this.ErrorMensaje('DNI ingresado no existe en RENIEC.');
          }
        }else{
          this.cambiarValidPIDE(false);
          this.ErrorMensaje('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.cambiarValidPIDE(false);
        this.ErrorMensaje('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    )
  }

  consultarRazonSocial(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if(result.dataService){
          let respuesta = result.dataService.respuesta;
          if(respuesta.esHabido){

            this.solicitudAcceso.nombres = respuesta.ddp_nombre;
            this.solicitudAcceso.apellidoMaterno = '';
            this.solicitudAcceso.apellidoPaterno = '';

            this.cambiarValidPIDE(true);
            this.SuccessMensaje('Se validó existencia de RUC en SUNAT.');
          }else{
            this.cambiarValidPIDE(false);
            this.ErrorMensaje('RUC ingresado no existe en SUNAT.');
          }
        }else{
          this.cambiarValidPIDE(false);
          this.ErrorMensaje('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.cambiarValidPIDE(false);
        this.ErrorMensaje('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    )
  }

  consultarDocumentoCE(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDocumentoCE(params).subscribe(
      (result: any) => {
        
        this.SuccessMensaje(result.message)
      },
      (error) => {
        this.dialog.closeAll();
        this.ErrorMensaje('Ocurrió un error');
      }
    )
  }

  changeTipoDoc(){
    this.solicitudAcceso.numeroDocumento = "";
    this.cambiarValidPIDE(false);
  }

  changeNumDoc(){
    this.cambiarValidPIDE(false);
  }

  cambiarValidPIDE(valor: boolean){
    this.validaPIDEClass = valor;
    this.solicitudAcceso.isValidPIDE = valor;
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({severity: 'success', summary: '', detail: mensaje });
  }


}
