import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import {MessageService} from 'primeng/api';
import {LoadingComponent} from '../../../../components/loading/loading.component';
import {MatDialog} from '@angular/material/dialog';
import {PideService} from '../../../../service/evaluacion/pide.service';

@Component({
  selector: 'solicitud-acceso-juridica',
  templateUrl: './solicitud-acceso-juridica.component.html',
  styleUrls: ['./solicitud-acceso-juridica.component.scss']
})
export class SolicitudAccesoJuridicaComponent implements OnInit {
  constructor(private serv : ParametroValorService, private router: Router,private messageService: MessageService,private dialog: MatDialog, private pideService: PideService) { }
  //Parametro
  lstTipoDocumento:any[]=[];
  validaPIDEClass: boolean = false;
  @Input ('dataBase') dataBase:any={
    disabled:false
  };
  @Input ('solicitudAcceso') solicitudAcceso:any={};

  ngOnInit(): void {
    this.listarTipoDocumento();
  }

/******************************************************/
/*Métodos**********************************************/
/******************************************************/

listarTipoDocumento(){
    var params ={ prefijo: 'TPER' }
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result : any)=>{this.lstTipoDocumento = result.data;}
    );
  };

  validarPide() {
    if (!this.solicitudAcceso.numeroRucEmpresa) {
      this.ErrorMensaje('(*) Ingrese un número de RUC.');
      return;
    }
    this.consultarRazonSocial({"numRUC": this.solicitudAcceso.numeroRucEmpresa});



  }



  consultarRazonSocial(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if(result.dataService){
          let respuesta = result.dataService.respuesta;
          if(respuesta.esHabido){

            this.solicitudAcceso.razonSocialEmpresa = respuesta.ddp_nombre;
            this.solicitudAcceso.direccionEmpresa = respuesta.ddp_nomvia + '- '+respuesta.ddp_refer1;

            this.cambiarValidPIDE(true);
            this.SuccessMensaje('Se validó existencia de RUC en SUNAT.');
          }else{
            this.cambiarValidPIDE(false);
            this.ErrorMensaje('RUC ingresado no existe en SUNAT.');
          }
        }else{
          this.cambiarValidPIDE(false);
          this.ErrorMensaje('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.cambiarValidPIDE(false);
        this.ErrorMensaje('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    )
  }

  cambiarValidPIDE(valor: boolean){
    this.validaPIDEClass = valor;
    this.solicitudAcceso.isValidEmpresaPIDE = valor;
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({severity: 'success', summary: '', detail: mensaje });
  }

}
