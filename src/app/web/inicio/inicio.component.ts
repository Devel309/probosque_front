import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "@services";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { ModalAutorizacionComponent } from "src/app/components/modal/modal-autorizacion/modal-autorizacion.component";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { Perfiles } from "src/app/model/util/Perfiles";
import { ConfigService } from "src/app/service/config.service";
import { GenericoService } from "src/app/service/generico.service";
import { NotificacionService } from "src/app/service/notificacion";
import { ProcesoPostulaionService } from "src/app/service/proceso-postulacion/proceso-postulacion.service";
import { GenericModalAutorizacionComponent } from "src/app/shared/components/generic-modal-autorizacion/generic-modal-autorizacion.component";
import { ValidarCondicionMinimaComponent } from "src/app/shared/components/validar-condicion-minima/validar-condicion-minima.component";

@Component({
  selector: "app-inicio",
  templateUrl: "./inicio.component.html",
  styleUrls: ["./inicio.component.scss"],
})
export class InicioComponent implements OnInit {
  data: any;
  opcioneMenuPart: any;
  chartOptions: any;
  ref!: DynamicDialogRef;
  usuario = {} as UsuarioModel;
  notificaciones: any[] = [];

  constructor(
    public dialogService: DialogService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private genericoService: GenericoService,
    private usuarioServ: UsuarioService,
    private notificacionService: NotificacionService
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  descargarReporte() {
    this.descargar();
  }

  private descargar() {
    let param = {};
    this.genericoService.descargarReporte(param).subscribe(
      (result: any) => {
        this.descargarFormato(result.data);
      },
      (error: any) => {}
    );
  }

  private descargarFormato(formato: string) {
    const a = document.createElement("a");
    document.body.appendChild(a);

    const b64Data = formato;
    const contentType3 = "application/pdf";
    const file: File = new File(
      [this._base64ToArrayBuffer(b64Data)],
      "this.tempBien.nombreArchivoCompleto",
      { type: contentType3 }
    );
    const blobUrl = window.URL.createObjectURL(file);
    a.href = blobUrl;
    a.download = "REPORTE PROBOSQUES";
    a.click();
    window.URL.revokeObjectURL(blobUrl);
  }

  private _base64ToArrayBuffer(base64: any): any {
    const binary_string = window.atob(base64);
    const len = binary_string.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  ngOnInit() {
    this.listadoNotificacion();
    if (localStorage.getItem("usuario") == null) {
      sessionStorage.clear();
      localStorage.clear();
      window.location.href = "./";
    }

    this.opcioneMenuPart = [
      { label: "opcion 1", icon: "pi pi-fw pi-plus" },
      { label: "opcion 2", icon: "pi pi-fw pi-download" },
      { label: "opcion 3", icon: "pi pi-fw pi-refresh" },
    ];

    this.chartOptions = {
      legend: { display: false },
    };

    this.data = {
      labels: ["Aprobados", "Presentados", "Observados", "En proceso"],
      datasets: [
        {
          data: [100, 0,0 , 0],
          backgroundColor: ["#4A962C", "#80B56B", "#FFCE56", "#CBDB29"],
          hoverBackgroundColor: ["#4A962C", "#80B56B", "#B6D5AA", "#CBDB29"],
        },
      ],
    };
    
  }

  listadoNotificacion() {
    var params = {
      idNotificacion: null,
      codigoPerfil: this.usuario.sirperfil,
      idUsuario: this.usuarioServ.idUsuario,
    };
    this.notificacionService
      .listarNotificacion(params)
      .pipe(finalize(() => {
        if (
          localStorage.getItem("token") &&
          this.notificaciones.length != 0
        ) {
          this.show();
        }
      }))
      .subscribe((response: any) => {
        if (!!response.data) {
          this.notificaciones = response.data;
        }
      });
  }

  show = () => {
    let params = {
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .obtenerAutorizacion(params)
      .subscribe((result: any) => {
        if (result.data.length > 0) {
          // this.ref = this.dialogService.open(ModalAutorizacionComponent, {
          this.ref = this.dialogService.open(
            GenericModalAutorizacionComponent,
            {
              width: "70%",
              contentStyle: { "max-height": "500px", overflow: "auto" },
              baseZIndex: 10000,
              //closable: false,
              closeOnEscape: true,
              data: {
                item: result.data,
                notificaciones:  this.notificaciones
              },
            }
          );

          this.ref.onClose.subscribe((resp: any) => {});
        }
      });
  };

  ngOnDestroy() {
    if (this.ref) {
      this.ref.close();
    }
  }

  abrir() {
    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: "Validar Condiciones Mínimas",
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        item: "hola",
      },
    });
  }
}
