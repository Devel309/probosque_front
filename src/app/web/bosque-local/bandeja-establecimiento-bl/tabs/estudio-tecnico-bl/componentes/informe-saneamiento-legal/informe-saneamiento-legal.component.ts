import { Component, Input, OnInit } from '@angular/core';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';

@Component({
  selector: 'app-informe-saneamiento-legal',
  templateUrl: './informe-saneamiento-legal.component.html',
  styleUrls: ['./informe-saneamiento-legal.component.scss']
})
export class InformeSaneamientoLegalComponent implements OnInit {

  constructor(
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
  ) { }

  @Input() disabledEstudioTec: boolean = false;
  @Input() idPlan: number = 0;
  @Input() showEvalComiteTec: boolean = false;
  @Input() disabledEvalComiteTec: boolean = false;

  codigoSOLBL = CodigosSOLBL;
  codSeccion: string = CodigosSOLBL.TAB_3;
  codProceso: string = CodigosSOLBL.CODIGO_PROCESO;
  adjunto3_8_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_8}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  codTab3_8: string = CodigosSOLBL.TAB_3_8;
  codDocMapa1: string = CodigosSOLBL.TAB_3_3 + "_A";
  codDocMapa2: string = CodigosSOLBL.TAB_3_8 + "_A";

  ngOnInit(): void {
    this.obtenerInfoAdjuntoReporteDireccion();
  }

  obtenerInfoAdjuntoReporteDireccion() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_1,
      tipoArchivo: CodigosSOLBL.TAB_2_1
    };

    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBosqueLocal(param).subscribe(resp => {
      if (resp.success && resp.data.length > 0) {
        let files3_8_1 = resp.data.filter((file: any) => file.tipoArchivo === CodigosSOLBL.TAB_2_1);
        if (files3_8_1.length > 0) {
          this.adjunto3_8_1.idSolBosqueLocalArchivo = files3_8_1[0].idSolBosqueLocalArchivo;
          this.adjunto3_8_1.idArchivo = files3_8_1[0].idArchivo;
        }
      }
    });
  }

  /******** GENERAL ***********/
  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, adjunto: any) {
    if (!ok) return;
    adjunto.idArchivo = 0;
  }
}
