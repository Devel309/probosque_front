import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { RegenteForestalModel } from "@models";
import { PoccInformacionGeneral, RegenteForestal } from "src/app/model/pocc-informacion-general";
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalEstudioTecnicoService } from 'src/app/service/bosque-local/solicitud-bosque-local-estudio-tecnico.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { Router } from '@angular/router';
import { data } from 'jquery';
@Component({
  selector: 'tab-estudio-tecnico-bl',
  templateUrl: './estudio-tecnico-bl.component.html',
  styleUrls: ['./estudio-tecnico-bl.component.scss']
})
export class EstudioTecnicoBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() disabledEstudioTec: boolean = false;
  @Input() showEvalComiteTec: boolean = false;
  @Input() disabledEvalComiteTec: boolean = false;
  @Input() evaluacionGabinete: boolean = false;
  @Input() showEvalObserv: boolean = false;
  /*****************************************************/
  usuario!: UsuarioModel;
  form: PoccInformacionGeneral = new PoccInformacionGeneral();
  selectRegente: RegenteForestalModel = new RegenteForestalModel();
  idBosqueLocalEvaluacion: number =0;

  codigoSOLBL = CodigosSOLBL;
  codProceso: string = CodigosSOLBL.CODIGO_PROCESO;
  codSeccion: string = CodigosSOLBL.TAB_3;
  isContieneSuperposicion: boolean = false;
  listaGabinete: any = [];


  regentes: any[] = [];
  queryRegente: string = "";
  idSolBosqueLocalEstudioTecnico!: number;
  verModalRegente: boolean = false;
  //Mapa
  codTab3_3: string = CodigosSOLBL.TAB_3_3;
  codTab3_6: string = CodigosSOLBL.TAB_3_6;
  codTab3_8: string = CodigosSOLBL.TAB_3_8;
  codDocMapa1: string = CodigosSOLBL.TAB_3_3 + "_A";
  codDocMapa2: string = CodigosSOLBL.TAB_3_8 + "_A";
  tipoArchivoGeneral:string = 'TPVERTICES';
  tipoGeometria:string = 'TPVERTICES'
  //3.1
  requestAcredGL: any = {};
  adjunto3_1_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_1}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  adjunto3_1_2: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_1}_B`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.2
  requestAcredArea: any = {};
  adjunto3_2_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_2}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.3
  requestAcredCateg: any = {};
  adjunto3_3_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_3}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  adjunto3_3_2: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_3}_B`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.4
  requestJustArea: any = {};
  adjunto3_4_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_4}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.5
  requestListaInicial: any = {};
  adjunto3_5_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_5}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.6
  requestUbicacion: any = {};
  adjunto3_6_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_6}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  adjunto3_6_2: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_6}_B`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  adjunto3_6_3: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_6}_C`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  listaCoordenadas: any[] = [];
  //3.7
  requestModalidad: any = {};
  adjunto3_7_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_7}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.8
  requestInforme: any = {};
  adjunto3_8_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_3_8}_A`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  //3.9
  requestDesEnvioInfo: any = {};
  isCheckEnviarET: boolean = false;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private forestalServ: ApiForestalService,
    private usuarioServ: UsuarioService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,

    private solicitudBosqueLocalEstudioTecnicoService: SolicitudBosqueLocalEstudioTecnicoService,
    private router: Router,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarSolicitudBosqueLocalEstudioTecnico();
    this.obtenerInfoAdjuntoResultadoEvaluacion();
    this.listarSolicitudBosqueLocal();
    this.listarSolicitudBosqueLocalEvaluacion();
  }

  /******** 3.1 ***********/
  // Servicios
  listVertices(items:any){
    this.listaCoordenadas = items;
  }
  contieneSuperposicion(item: boolean) {
    if (item === true) {
      this.isContieneSuperposicion = true;
    } else {
      this.isContieneSuperposicion = false;
    }
  }



  listarSolicitudBosqueLocalEstudioTecnico() {

    const param: any = {
      idSolBosqueLocal: this.idPlan,
    }

    this.solicitudBosqueLocalEstudioTecnicoService.listarSolicitudBosqueLocalEstudioTecnico(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if(resp.success) {
        //this.toast.ok(resp.message);
        if(resp.data.length!==0){
          this.requestAcredGL.documentoRegente = resp.data[0].numeroDocumentoRegente;
          this.requestAcredGL.nombreRegente = resp.data[0].nombreRegente;
          this.requestAcredGL.licenciaRegente = resp.data[0].licenciaRgente;
          this.idSolBosqueLocalEstudioTecnico = resp.data[0].idSolBosqueLocalEstudioTecnico;

          this.requestModalidad.text1 = resp.data[0].procedimientoProyectado;
          this.requestModalidad.text2 = resp.data[0].procedimientoEmision;
          this.requestModalidad.text3 = resp.data[0].procedimientoCobro;
          this.requestModalidad.text4 = resp.data[0].actividadProteccion;
          this.requestDesEnvioInfo.enviar = resp.data[0].envioNotificacion;
          this.isCheckEnviarET = this.requestDesEnvioInfo.enviar || false;
          this.requestModalidad.notificacion =  resp.data[0].notificacionTexto;
        }
      }

      }, (error: HttpErrorResponse) =>this.toast.error("Ocurrió un error al listar información del Estudio Técnico"));
  }

  listarSolicitudBosqueLocalEvaluacion() {
    const params = { "idSolBosqueLocal": this.idPlan };
    this.solicitudBosqueLocalService.listarSolicitudBosqueLocal(params).subscribe(resp => {
      if (resp.data.length > 0) {
        this.idBosqueLocalEvaluacion = resp.data[0].idBosqueLocalEvaluacion;
      }
    });
  }


  actualizarAcreditacionGobiernoLocal() {

    const param: any = {
      idSolBosqueLocal: this.idPlan,
      idSolBosqueLocalEstudioTecnico: this.idSolBosqueLocalEstudioTecnico,
      idRegente: this.requestAcredGL.idRegente,
      numeroDocumentoRegente: this.requestAcredGL.documentoRegente,
      nombreRegente: this.requestAcredGL.nombreRegente,
      licenciaRgente: this.requestAcredGL.licenciaRegente,
      idUsuarioModificacion: this.usuario.idusuario

    }
    
    this.solicitudBosqueLocalEstudioTecnicoService.actualizarSolicitudBosqueLocalEstudioTecnico(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if(resp.success) {
        this.toast.ok(resp.message);

      }

      }, (error: HttpErrorResponse) =>this.toast.error("Ocurrió un error al guardar Acreditación de Gobierno Local y su Representante"));
  }

  obtenerInfoAdjuntoResultadoEvaluacion() {
    let param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_3
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data.length > 0) {
          let files3_1_1: any[] = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_1_1.tipoArchivo);
          if (files3_1_1 && files3_1_1.length > 0) {
            this.adjunto3_1_1.idSolBosqueLocalArchivo = files3_1_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_1_1.idArchivo = files3_1_1[0].idArchivo;
          }

          let files3_1_2 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_1_2.tipoArchivo);
          if(files3_1_2 && files3_1_2.length > 0){
            this.adjunto3_1_2.idSolBosqueLocalArchivo = files3_1_2[0].idSolBosqueLocalArchivo;
            this.adjunto3_1_2.idArchivo = files3_1_2[0].idArchivo;
          }

          let files3_2_1 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_2_1.tipoArchivo);
          if(files3_2_1 && files3_2_1.length > 0){
            this.adjunto3_2_1.idSolBosqueLocalArchivo = files3_2_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_2_1.idArchivo = files3_2_1[0].idArchivo;
          }

          let files3_3_1 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_3_1.tipoArchivo);
          if(files3_3_1.length > 0){
            this.adjunto3_3_1.idSolBosqueLocalArchivo = files3_3_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_3_1.idArchivo = files3_3_1[0].idArchivo;
          }

          let files3_3_2 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_3_2.tipoArchivo);
          if(files3_3_2.length > 0){
            this.adjunto3_3_2.idSolBosqueLocalArchivo = files3_3_2[0].idSolBosqueLocalArchivo;
            this.adjunto3_3_2.idArchivo = files3_3_2[0].idArchivo;
          }

          let files3_4_1 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_4_1.tipoArchivo);
          if(files3_4_1 && files3_4_1.length > 0){
            this.adjunto3_4_1.idSolBosqueLocalArchivo = files3_4_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_4_1.idArchivo = files3_4_1[0].idArchivo;
          }

          let files3_5_1 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_5_1.tipoArchivo);
          if(files3_5_1 && files3_5_1.length > 0){
            this.adjunto3_5_1.idSolBosqueLocalArchivo = files3_5_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_5_1.idArchivo = files3_5_1[0].idArchivo;
          }

          let files3_6_1 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_6_1.tipoArchivo);
          if(files3_6_1 && files3_6_1.length > 0){
            this.adjunto3_6_1.idSolBosqueLocalArchivo = files3_6_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_6_1.idArchivo = files3_6_1[0].idArchivo;
          }

          let files3_6_2 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_6_2.tipoArchivo);
          if(files3_6_2 && files3_6_2.length > 0){
            this.adjunto3_6_2.idSolBosqueLocalArchivo = files3_6_2[0].idSolBosqueLocalArchivo;
            this.adjunto3_6_2.idArchivo = files3_6_2[0].idArchivo;
          }

          let files3_7_1 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto3_7_1.tipoArchivo);
          if (files3_7_1 && files3_7_1.length > 0) {
            this.adjunto3_7_1.idSolBosqueLocalArchivo = files3_7_1[0].idSolBosqueLocalArchivo;
            this.adjunto3_7_1.idArchivo = files3_7_1[0].idArchivo;
          }

        }
      }, (error) => this.errorMensaje(error, true));
  }

  registrarInfoAdjuntoCredencialesAlcalde() {
    if (this.adjunto3_1_1.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_1_1.idSolBosqueLocalArchivo ? this.adjunto3_1_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_1_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_1,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_1_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

        this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe(resp => {
            if (resp.success) {
              
              this.adjunto3_1_1.idSolBosqueLocalArchivo = resp.codigo;

            } else {
              this.toast.warn(resp.message);
            }
          }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }
  }


  registrarInfoAdjuntoReglamentoOrganizacionFinanzas() {
    if (this.adjunto3_1_2.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_1_2.idSolBosqueLocalArchivo ? this.adjunto3_1_2.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_1_2.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_1,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_1_2.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            
            this.adjunto3_1_2.idSolBosqueLocalArchivo = resp.codigo;

          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }

  listarRegente() {
    this.forestalServ.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {

        this.regentes.push({
          idRegente: element.id,
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
        });
      });
    });
  }

  // Botones
  btnBuscarRegente() {
    this.abrirModalRegente();
  }

  btnGuardarAcredGL() {


    if (!this.validarAcredGL()) return;
    this.registrarInfoAdjuntoCredencialesAlcalde();
    this.registrarInfoAdjuntoReglamentoOrganizacionFinanzas();
    //this.requestAcredGL.idArchivo1 = this.adjunto3_1_1.idArchivo;
    //this.requestAcredGL.idArchivo2 = this.adjunto3_1_2.idArchivo;
    this.actualizarAcreditacionGobiernoLocal();
    //console.log("enviar1: ", this.requestAcredGL);
  }

  //Funciones
  abrirModalRegente() {
    this.listarRegente();
    this.selectRegente = new RegenteForestalModel();
    this.verModalRegente = true;
    this.queryRegente = "";
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegente();
    }
  }
  guardarRegente() {
    this.form.regente = new RegenteForestal(this.selectRegente);

    this.form.regente.idPlanManejo = this.idPlan;
    this.requestAcredGL.idRegente = this.form.regente?.idRegente!;
    this.requestAcredGL.documentoRegente = this.form.regente?.numeroDocumento!;
    this.requestAcredGL.nombreRegente = `${this.selectRegente.nombres} ${this.selectRegente.apellidos}`;
    this.requestAcredGL.licenciaRegente = this.form.regente?.numeroLicencia!;

    this.form.regente.idUsuarioRegistro = this.usuario.idusuario;

    this.verModalRegente = false;

  }

  validarAcredGL(): boolean {
    let valido = true;
    if (!this.adjunto3_1_1.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Copia de Credenciales de Alcalde.");
    }
    if (!this.adjunto3_1_2.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Copia del Reglamento de Organización.");
    }
    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  /******** 3.2 ***********/
  // Servicios
  registrarInfoAdjuntoReporteEmitido() {
    if (this.adjunto3_2_1.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_2_1.idSolBosqueLocalArchivo ? this.adjunto3_2_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_2_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_2,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_2_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            this.toast.ok("Se registró Acreditación del Área como Tierra de Dominio Público correctamente")
            this.adjunto3_2_1.idSolBosqueLocalArchivo = resp.codigo;

          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }


   listarSolicitudBosqueLocal() {

    const param: any = {
      idSolBosqueLocal: this.idPlan,
    }

    this.solicitudBosqueLocalService.listarSolicitudBosqueLocal(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if(resp.success) {
        //this.toast.ok(resp.message);
        if(resp.data.length!==0){
          this.idBosqueLocalEvaluacion= resp.data;
          
        }
      }

      }, (error: HttpErrorResponse) =>this.toast.error("Ocurrió un error al listar información del Estudio Técnico"));
  }
  // Botones
  btnGuardarAcredArea() {
    if (!this.validarAcredArea()) return;
    this.registrarInfoAdjuntoReporteEmitido()
    //this.requestAcredArea.idArchivo1 = this.adjunto3_2_1.idArchivo;

    //console.log("enviar2: ", this.requestAcredArea);
  }

  //Funciones
  validarAcredArea(): boolean {
    let valido = true;
    if (!this.adjunto3_2_1.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Reporte Emitido por ARFFS o SERFOR.");
    }
    return valido;
  }

  /******** 3.3 ***********/
  // Servicios
  registrarInfoAdjuntoZonifPdf() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto3_3_1.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto3_3_1.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_3,
      codigoSubSeccion: CodigosSOLBL.TAB_3_3,
      tipoArchivo: this.adjunto3_3_1.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.adjunto3_3_1.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarInfoAdjuntoZonifInterna() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto3_3_2.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto3_3_2.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_3,
      codigoSubSeccion: CodigosSOLBL.TAB_3_3,
      tipoArchivo: this.adjunto3_3_2.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.adjunto3_3_2.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  // Botones
  btnGuardarAcredCateg() {
    if (!this.validarAcredCateg()) return;
    if(this.isContieneSuperposicion) {
      this.registrarInfoAdjuntoZonifPdf();
    } else {
      this.registrarInfoAdjuntoZonifInterna();
    }
  }

  //Funciones
  validarAcredCateg(): boolean {
    let valido = true;
    if (!this.adjunto3_3_1.idArchivo && this.isContieneSuperposicion) {
      valido = false;
      this.toast.warn("Debe de adjuntar Zonificación.");
    }
    if (!this.adjunto3_3_2.idArchivo && !this.isContieneSuperposicion) {
      valido = false;
      this.toast.warn("Debe de adjuntar Zonificación Interna del Área.");
    }
    return valido;
  }

  /******** 3.4 ***********/
  // Servicios
  registrarInfoAdjuntoJustificacion() {
    if (this.adjunto3_4_1.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_4_1.idSolBosqueLocalArchivo ? this.adjunto3_4_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_4_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_4,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_4_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            this.toast.ok("Se registró Justificación del Área Propuesta correctamente")
            this.adjunto3_4_1.idSolBosqueLocalArchivo = resp.codigo;

          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }

  // Botones
  btnGuardarJustArea() {
    if (!this.validarJustArea()) return;
    this.registrarInfoAdjuntoJustificacion()
    //this.requestJustArea.idArchivo1 = this.adjunto3_4_1.idArchivo;

    //console.log("enviar4: ", this.requestJustArea);
  }

  //Funciones
  validarJustArea(): boolean {
    let valido = true;
    if (!this.adjunto3_4_1.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Justificación.");
    }
    return valido;
  }

  /******** 3.5 ***********/
  // Servicios
  registrarInfoAdjuntoBeneficiarios() {
    if (this.adjunto3_5_1.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_5_1.idSolBosqueLocalArchivo ? this.adjunto3_5_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_5_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_5,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_5_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            this.toast.ok("Se registró Justificación del Área Propuesta correctamente")
            this.adjunto3_5_1.idSolBosqueLocalArchivo = resp.codigo;

          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }

  // Botones
  btnGuardarListaInicial() {
    if (!this.validarListaInicial()) return;
    this.registrarInfoAdjuntoBeneficiarios();
    //this.requestListaInicial.idArchivo1 = this.adjunto3_5_1.idArchivo;

    //console.log("enviar5: ", this.requestListaInicial);
  }

  //Funciones
  validarListaInicial(): boolean {
    let valido = true;
    if (!this.adjunto3_5_1.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Lista de Beneficiarios.");
    }
    return valido;
  }

  /******** 3.6 ***********/
  // Servicios
  registrarInfoAdjuntoMapaUbicacion() {
    if (this.adjunto3_6_1.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_6_1.idSolBosqueLocalArchivo ? this.adjunto3_6_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_6_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_6,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_6_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            // this.toast.ok("Se registró Justificación del Área Propuesta correctamente")
            this.adjunto3_6_1.idSolBosqueLocalArchivo = resp.codigo;

          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }

  registrarInfoAdjuntoComprimidoShapeFile() {
    if (this.adjunto3_6_2.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_6_2.idSolBosqueLocalArchivo ? this.adjunto3_6_2.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_6_2.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_6,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_6_2.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            this.toast.ok("Se registró Justificación del Área Propuesta correctamente")
            this.adjunto3_6_2.idSolBosqueLocalArchivo = resp.codigo;

          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }



  // Botones
  btnGuardarUbicacion() {
    if (!this.validarUbicacion()) return;
    this.registrarInfoAdjuntoMapaUbicacion();
    this.registrarInfoAdjuntoComprimidoShapeFile();
    //this.requestUbicacion.idArchivo1 = this.adjunto3_6_1.idArchivo;
    //this.requestUbicacion.idArchivo2 = this.adjunto3_6_2.idArchivo;
    //this.requestUbicacion.idArchivo3 = this.adjunto3_6_3.idArchivo;

    //console.log("enviar6: ", this.requestUbicacion);
  }

  //Funciones
  validarUbicacion(): boolean {
    let valido = true;
    if (!this.adjunto3_6_1.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Mapa de Ubicación.");
    }
    if (!this.adjunto3_6_2.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Comprimido ShapeFile.");
    }
    // if (!this.adjunto3_6_3.idArchivo) {
    //   valido = false;
    //   this.toast.warn("(*) Debe de adjuntar ShapeFile (Vértice).");
    // }
    return valido;
  }

  /******** 3.7 ***********/
  // Servicios

  registrarInfoAdjuntoArchivoComplementario() {
    if (this.adjunto3_7_1.idArchivo != 0) {
      var request = {
        idSolBosqueLocalArchivo: this.adjunto3_7_1.idSolBosqueLocalArchivo ? this.adjunto3_7_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto3_7_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_3,// CodigosPFDM.TAB_6
        codigoSubSeccion: CodigosSOLBL.TAB_3_7,//CodigosPFDM.TAB_6_1
        tipoArchivo: this.adjunto3_7_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
      
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            
            this.adjunto3_7_1.idSolBosqueLocalArchivo = resp.codigo;
            // this.toast.ok(resp.message);
          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }
  }


  actualizarModalidadSistemaAdministracion() {

    const param: any = {
      idSolBosqueLocal: this.idPlan,
      idSolBosqueLocalEstudioTecnico: this.idSolBosqueLocalEstudioTecnico,
      procedimientoProyectado: this.requestModalidad.text1,
      procedimientoEmision: this.requestModalidad.text2,
      procedimientoCobro: this.requestModalidad.text3,
      actividadProteccion: this.requestModalidad.text4
    }
    
    this.solicitudBosqueLocalEstudioTecnicoService.actualizarSolicitudBosqueLocalEstudioTecnico(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if(resp.success) {
        this.toast.ok("Se guardó Modalidad O sistema de Administración correctamente");

      }

      }, (error: HttpErrorResponse) =>this.toast.error("Ocurrió un error al guardarModalidad O sistema de Administración a Emplearse"));
  }

  // Botones
  btnGuardarModalidad() {
    if (!this.validarModalidad()) return;
    this.registrarInfoAdjuntoArchivoComplementario();
    this.actualizarModalidadSistemaAdministracion();
    // this.requestModalidad.idArchivo1 = this.adjunto3_7_1.idArchivo;
    // console.log("enviar7: ", this.requestModalidad);
  }

  //Funciones
  validarModalidad(): boolean {
    let valido = true;
    if (!this.adjunto3_7_1.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Archivo Complementario.");
    }
    return valido;
  }

  /******** 3.9 ***********/
  // Servicios
  actualizarDescargaEnvioInformacion() {

    const param: any = {
      idSolBosqueLocal: this.idPlan,
      idSolBosqueLocalEstudioTecnico: this.idSolBosqueLocalEstudioTecnico,
      notificacionTexto: this.requestModalidad.notificacion,
      envioNotificacion: this.requestDesEnvioInfo.enviar,
    }
    this.solicitudBosqueLocalEstudioTecnicoService.actualizarSolicitudBosqueLocalEstudioTecnico(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if(resp.success) {
        this.isCheckEnviarET = true;
        this.toast.ok('Se guardo datos de Descarga y Envío de información correctamente');
      }

      }, (error: HttpErrorResponse) =>this.toast.error("Ocurrió un error al guardar datos de Descarga y Envío de información"));
  }

  // Botones
  btnDescargaEnvioInfo() {
    if(!this.validarDescargaEnvio()) return;
    this.actualizarDescargaEnvioInformacion();
  }

  validarDescargaEnvio(): boolean {
    let valido = true;
    if(!this.requestDesEnvioInfo.enviar) {
      valido = false;
      this.toast.warn("(*) Debe de Marcar Enviar Estudio Técnico.");
    }
    return true;
  }

  btnEnviarNotif() {
    if(!this.isCheckEnviarET) {
      this.toast.warn("(*) Debe de Marcar Enviar Estudio Técnico, de la sección Descarga y envío de Información y guadar los cambios.");
      return;
    }
    this.actualizarEstado(CodigoEstadoBL.CREACION_COMITE);
  }

  actualizarEstado(estado: string) {
    const param: any = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: estado,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-establecimiento-BL"]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }



  /******** GENERAL ***********/
  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, adjunto: any) {
    if (!ok) return;

    adjunto.idArchivo = 0;
    if (adjunto.idSolBosqueLocalArchivo) {
      const params = {
        idSolBosqueLocalArchivo: adjunto.idSolBosqueLocalArchivo,
        idUsuarioElimina: this.usuario.idusuario
      };


      this.eliminarInfoAdjuntoIniciativa(params, adjunto);
    }


  }

  eliminarInfoAdjuntoIniciativa(params: any, adjunto: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          adjunto.idSolBosqueLocalArchivo = 0;
          //this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }



}
