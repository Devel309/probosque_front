import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { finalize, tap } from 'rxjs/operators';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { ConfirmationService } from 'primeng/api';
import { GenericoService } from 'src/app/service/generico.service';
import { ConvertDateToString, ConvertNumberToDate, DownloadFile } from 'src/app/shared/util';
import { SolicitudBosqueLocalGeometriaService } from 'src/app/service/solicitud/solicitud-bosque-local-geometria.service';

@Component({
  selector: 'tab-resultado-evaluacion-bl',
  templateUrl: './resultado-evaluacion-bl.component.html',
  styleUrls: ['./resultado-evaluacion-bl.component.scss']
})
export class ResultadoEvaluacionBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() disabledEvalComiteTec: boolean = false;
  @Input() disabledAction: boolean = false;
  @Input() showEvalObserv:boolean = false;

  /*****************************************************/
  usuario!: UsuarioModel;
  RADIO_RESUL = {FAV: "FAV", DESFAV: "DESFAV", OBS: "OBS"};

  requestInforme: any = {};
  requestDocumento: any = {};
  adjunto5_1_1: any = { tipoArchivo: `${CodigosSOLBL.TAB_5_1_A}`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };
  adjunto5_1_2: any = { tipoArchivo: `${CodigosSOLBL.TAB_5_1_B}`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };

  requestResultado: any = {};
  isObservadp: boolean = false;

  comboTiposDocumento: any[] = [];
  minDate: Date = new Date(2021, 0, 1);

  file: any = {};

  isMapaEvalCampo: boolean = false;

  constructor(
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private router: Router,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private confirmationService: ConfirmationService,
    private genericoServ: GenericoService,
    private serviceSolBosqueLocalGeometria: SolicitudBosqueLocalGeometriaService,

  ) {
    this.usuario = this.usuarioServ.usuario;
  }


  ngOnInit(): void {
    this.listarComboTiposDocumentos();
    this.obtenerInfoEvaluacionResultado(true);
    this.obtenerInfoAdjuntosResultadoEvaluacion();
    this.obtenerMapaEvalCampo();
  //  this.isObservadp = this.estadoSolicitud === CodigoEstadoBL.OBS_EXPED_TEC &&  this.usuario.sirperfil=="MUNICIPIO";
  }

  /******** 5.1 ***********/
  //  Servicios
  obtenerMapaEvalCampo() {
    const params = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.CODIGO_PROCESO,
      codigoSubSeccion: CodigosSOLBL.TAB_4_2,
    };
    this.serviceSolBosqueLocalGeometria.listarGeometria(params).subscribe(resp => {
      if(resp.success && resp.data && resp.data.length > 0) {
        this.isMapaEvalCampo = true;
      }
    });
  }
  action() {
    this.disabledAction=this.requestResultado.resultado;
  }

  listarComboTiposDocumentos() {
    const params: any = {prefijo: "TDOCGE"};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.success) {
        this.comboTiposDocumento = result.data;
      }
    }, () => this.dialog.closeAll());
  }

  obtenerInfoEvaluacionResultado(load: boolean) {
    const param = {
      idSolBosqueLocal: this.idPlan
    };

    if(load) this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.listarSolicitudBosqueLocalEvaluacion(param)
    .pipe(finalize(() => {
      if(load) this.dialog.closeAll();
    }))
    .subscribe(resp => {
      if (resp.data && resp.data.length > 0) {
        this.requestResultado.idBosqueLocalEvaluacion = resp.data[0].idBosqueLocalEvaluacion;
        this.requestResultado.resultado = resp.data[0].codigoResultadoEvaluacion;
        this.requestResultado.evalGabinete = resp.data[0].evalGabineteObservado || false;
        this.requestResultado.evalCampo = resp.data[0].evalCampoObservado || false;
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  obtenerInfoAdjuntosResultadoEvaluacion() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_5,
      codigoSubSeccion: CodigosSOLBL.TAB_5_1
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success && resp.data && resp.data.length > 0) {
        let file5_1: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto5_1_1.tipoArchivo);
        if (file5_1 && file5_1.length > 0) {
          this.adjunto5_1_1.idSolBosqueLocalArchivo = file5_1[0].idSolBosqueLocalArchivo;
          this.adjunto5_1_1.idArchivo = file5_1[0].idArchivo;
        }

        let files5_2 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto5_1_2.tipoArchivo);
        if (files5_2 && files5_2.length > 0) {
          this.adjunto5_1_2.idSolBosqueLocalArchivo = files5_2[0].idSolBosqueLocalArchivo;
          this.adjunto5_1_2.idArchivo = files5_2[0].idArchivo;
          this.requestDocumento.asunto = files5_2[0].asunto;
          this.requestDocumento.numeroDocumento = files5_2[0].numeroDocumento;
          this.requestDocumento.fechaDocumento = files5_2[0].fechaDocumento ? ConvertNumberToDate(files5_2[0].fechaDocumento) : null;
          this.requestDocumento.tipoDocumento = files5_2[0].tipoDocumento;
        }

      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  //  Botones
  btnGenerarInforme() {
    let param = {
      idSolBosqueLocal: this.idPlan,
      evalGabineteObservado: this.requestResultado.evalGabinete,
      evalCampoObservado: this.requestResultado.evalCampo
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.descargarEvaluacionComiteTecnico(param).pipe(finalize(() => {
    this.dialog.closeAll();}))
    .subscribe(resp => {
      if (resp.success ) {
        this.file = resp;
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  btnDescargarInforme() {
    if(this.file.archivo) DownloadFile(this.file.archivo, this.file.nombeArchivo, 'application/octet-stream');
  }

  btnGuardarInforme(isEnvio: number) {
    if(!this.validarCamposInforme()) return;
    this.registrarResultadoEvaluacion(isEnvio);
  }

  btnSubsanacion() {
   this.actualizarEstado(CodigoEstadoBL.EVAL_EXPED_TEC);
  }

  actualizarEstado(estado: string) {
    const param: any = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: estado,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-establecimiento-BL"]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //  Funciones

  validarCamposInforme(): boolean {
    let valido = true;

    if(!this.requestResultado.resultado){
      valido = false;
      this.toast.warn("(*) Debe seleccionar el tipo de resultado.");
    } else {
      if(!this.adjunto5_1_1.idArchivo) {
        valido = false;
        this.toast.warn("(*) Debe de adjuntar Informe Firmado.");
      }

      if(this.requestResultado.resultado === this.RADIO_RESUL.OBS) {
        if(!this.requestResultado.evalGabinete && !this.requestResultado.evalCampo) {
          valido = false;
          this.toast.warn("(*) Debe seleccionar la Evaluación (Eval. Gabinte y/o Eval. Campo).");
        }
      } else if(this.requestResultado.resultado === this.RADIO_RESUL.FAV || this.requestResultado.resultado === this.RADIO_RESUL.DESFAV){
        if(!this.requestDocumento.asunto) {
          valido = false;
          this.toast.warn("(*) Debe ingresar el Asunto.");
        }
        if(!this.requestDocumento.numeroDocumento) {
          valido = false;
          this.toast.warn("(*) Debe ingresar el Número de Documento.");
        }
        if(!this.requestDocumento.fechaDocumento) {
          valido = false;
          this.toast.warn("(*) Debe ingresar la Fecha de Documento.");
        }
        if(!this.requestDocumento.tipoDocumento) {
          valido = false;
          this.toast.warn("(*) Debe ingresar el Tipo de Documento.");
        }
        if(!this.adjunto5_1_2.idArchivo) {
          valido = false;
          this.toast.warn(`(*) Debe adjuntar Documento.`);
        }
      } else {
        valido = false;
        this.toast.warn("(*) Debe seleccionar el tipo de resultado.");
      }
    }

    return valido;
  }

  seleccionarEvalGabinete(event: any){
    if(event.checked) {
    this.requestResultado.evalGabinete = true;
    this.requestResultado.evalCampo = false;
    }
  }

  seleccionarEvalCampo(event: any){
    if(event.checked) {
    this.requestResultado.evalGabinete = false;
    this.requestResultado.evalCampo = true;
    }
  }


  /******** 5.2 ***********/
  //  Servicios
  enviarGobiernoLocalServ() {
    const params = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: CodigoEstadoBL.OBS_EXPED_TEC,
      idUsuarioModificacion: this.usuario.idusuario,
      perfilUsuario: JSON.parse("" + localStorage.getItem("usuario")).sirperfil
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.enviarGobiernoLocal(params).subscribe((resp) => {
      this.dialog.closeAll();
      if(resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-evauacion-comite-BL"]);
      } else {
        this.toast.warn(resp.message);
      }

    }, (error) => this.errorMensaje(error, true));
  }

  registrarResultadoEvaluacion(isEnvio: number) {

    let listaEvaluacion : any [] = [];

    listaEvaluacion.push({
      idBosqueLocalEvaluacion: this.requestResultado.idBosqueLocalEvaluacion ? this.requestResultado.idBosqueLocalEvaluacion : 0,
      idSolBosqueLocal: this.idPlan,
      codigoResultadoEvaluacion: this.requestResultado.resultado,
      evalGabineteObservado: this.requestResultado.resultado == this.RADIO_RESUL.OBS? this.requestResultado.evalGabinete : false,
      evalCampoObservado: this.requestResultado.resultado == this.RADIO_RESUL.OBS? this.requestResultado.evalCampo : false,
      idUsuarioRegistro: this.usuario.idusuario
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarSolicitudBosqueLocalEvaluacion(listaEvaluacion).subscribe(resp => {
        if (resp.success) {
          this.registrarInfoDocumentosEvaluacion(isEnvio);
          this.obtenerInfoEvaluacionResultado(false);
        } else {
          this.toast.warn(resp.message);
          this.dialog.closeAll();
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  registrarInfoDocumentosEvaluacion(isEnvio: number) {
    let listaDocumentos: any[] = [];

    if (this.adjunto5_1_1.idArchivo) {
      listaDocumentos.push({
        idSolBosqueLocalArchivo: this.adjunto5_1_1.idSolBosqueLocalArchivo ? this.adjunto5_1_1.idSolBosqueLocalArchivo : 0,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto5_1_1.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_5,
        codigoSubSeccion: CodigosSOLBL.TAB_5_1,
        tipoArchivo: this.adjunto5_1_1.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      });
    }

    if (this.requestResultado.resultado !== this.RADIO_RESUL.OBS) {
      listaDocumentos.push({
        idSolBosqueLocalArchivo: this.adjunto5_1_2.idSolBosqueLocalArchivo ? this.adjunto5_1_2.idSolBosqueLocalArchivo : 0,
        asunto: this.requestDocumento.asunto,
        numeroDocumento: this.requestDocumento.numeroDocumento,
        fechaDocumento: this.requestDocumento.fechaDocumento,
        tipoDocumento: this.requestDocumento.tipoDocumento,
        idSolBosqueLocal: this.idPlan,
        idArchivo: this.adjunto5_1_2.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_5,
        codigoSubSeccion: CodigosSOLBL.TAB_5_1,
        tipoArchivo: this.adjunto5_1_2.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      });
    }

    this.solicitudBosqueLocalService.registrarInfoAdjuntosSolicitudBosqueLocal(listaDocumentos).subscribe(resp => {
        if (resp.success) {
          switch (isEnvio) {
            case 0:
              this.toast.ok("Se actualizó la Solicitud Correctamente.");
              this.obtenerInfoAdjuntosResultadoEvaluacion();
              break;
            case 1:
              this.enviarGobiernoLocalServ();
              break;
            case 2:
              let auxEstado = "";
              if (this.requestResultado.resultado === this.RADIO_RESUL.FAV) {
                auxEstado = CodigoEstadoBL.FAV_EXPED_TEC;
              } else if (this.requestResultado.resultado === this.RADIO_RESUL.DESFAV) {
                auxEstado = CodigoEstadoBL.DESFAV_EXPED_TEC;
              }
              this.actualizarEstado(auxEstado);
              break;

            default:
              break;
          }
        } else {
          this.toast.warn(resp.message);
          this.dialog.closeAll();
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  //  Botones
  btnEnviarGobLocal(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de Enviar a Gobierno Local?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.btnGuardarInforme(1);
      },
    });
  }

  btnEnviarEvalArffs(event: any) {
    if(!this.validarCargaMapaCampo()) return;
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de Enviar los Resultados a ARFFS?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.btnGuardarInforme(2);
      },
    });
  }

  //  Funciones
  validarCargaMapaCampo(): boolean {
    let valido = true;

    if(!this.isMapaEvalCampo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar los archivos de Área y Vértice, de la sección 'Evaluación en Campo -> Mapa'");
    }

    return valido;
  }

  /******** GENERAL ***********/
  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, adjunto: any) {
    if (!ok) return;
    adjunto.idArchivo = 0;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
