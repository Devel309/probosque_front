import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { ConfirmationService } from 'primeng/api';
import { DownloadFile, ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudBosqueLocalService as SolBosquelocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';

@Component({
  selector: 'tab-evaluacion-bl',
  templateUrl: './evaluacion-bl.component.html',
  styleUrls: ['./evaluacion-bl.component.scss']
})
export class EvaluacionBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() isDisbledFormu: boolean = false;
  @Input() disabledEval: boolean = false;
  @Input() showEstudioTec: boolean = false;
  @Input() estadoSolicitud: string = "";
  /*****************************************************/
  @Input() disabledEstudioTec: boolean = false;
  codProceso: string = CodigosSOLBL.CODIGO_PROCESO;
  codTab2_2: string = CodigosSOLBL.TAB_2_2;
  codDocMapa1: string = CodigosSOLBL.TAB_2_2+"_A";
  usuario!: UsuarioModel;

  requestEvaluacion: any = { tipo: null };
  listaBeneficiarios: any[] = [];
  idBosqueLocalEvaluacion: any = 0;
  RADIO_EVAL: { FAV: string, NOFAV: string } = { FAV: "RESFAV", NOFAV: "RESNOFAV" };

  adjunto2_1: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_1}` };
  adjunto2_3_1: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_3_A}` };
  adjunto2_3_2: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_3_B}` };
  adjunto2_3_3: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_3_C}` };
  adjunto2_4: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_4}` };
  adjunto2_5: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_5}` };

  adjuntoFirmado: any = { tipoArchivo: "AdjuntoFirmado", idArchivo: 0 };


  listaEnvioMunicipios: any[] = [];
  listaEnvioOposicion: any[] = [{ nombreDoc: "archivo1.pdf", comentario: "comentario" }, { nombreDoc: "archivo2.pdf", comentario: "comentario2" },];

  adjunto2_3: any = { idSolBosqueLocalArchivo: 0, tipoArchivo: "", idArchivo: 0 };

  requestInfoMunicipio: any = { idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_4_A}`, idDepartamento: 0, idProvincia: 0, nombreArchivo: null, codigoUbigeo: "", comentarios: null };
  lstDepartamentos: any = [];
  lstProvincias: any = [];
  lstDistritos: any = [];

  tituloModalEnvioMunicipio: string = "";
  verModalEnvioMunicipio: boolean = false;

  isEstEval_Benef: boolean = false;
  isEstPub_Benef: boolean = false;

  isGuardoFav_NoFav: boolean = false;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private archivoServ: ArchivoService,
    private confirmationService: ConfirmationService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private solBosquelocalService: SolBosquelocalService,
    private planificacionService: PlanificacionService,
    private router: Router,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.isEstEval_Benef = this.estadoSolicitud === CodigoEstadoBL.EVAL_BENEF;
    this.isEstPub_Benef = this.estadoSolicitud === CodigoEstadoBL.PUBLICA_BENEF;
    this.listarBeneficiarios();
    this.listarSolicitudBosqueLocalEvaluacion();
    this.obtenerInfoAdjuntoResultadoEvaluacion();
  }

  //SERVICIOS
  listarBeneficiarios() {
    const params = {
      "idBosqueLocalBeneficiario": null,
      "idSolBosqueLocal": this.idPlan,
      "pageNum": 1,
      "pageSize": 100
    }
    this.solBosquelocalService.listarSolicitudBosqueLocalBeneficiario(params).subscribe(resp => {
      if (resp.success && resp.data) {
        this.listaBeneficiarios = resp.data;
      }
    });
  }

  listarSolicitudBosqueLocalEvaluacion() {
    const params = { "idSolBosqueLocal": this.idPlan };
    this.solBosquelocalService.listarSolicitudBosqueLocalEvaluacion(params).subscribe(resp => {
      if (resp.data.length > 0) {
        this.idBosqueLocalEvaluacion = resp.data[0].idBosqueLocalEvaluacion;
        this.requestEvaluacion.tipo = resp.data[0].codigoResultadoEvaluacion ? resp.data[0].codigoResultadoEvaluacion?.trim() : null;
        this.isGuardoFav_NoFav = !!this.requestEvaluacion.tipo;
      }
    });
  }

  selectDPD(origen: string) {
    if (origen == 'd') {
      this.listarPorFilroProvincia(true);
    }
    if (origen == 'p') {
      this.listarPorFilroDistrito(true);
    }
  }

  listarPorFiltroDepartamento(ischange: boolean = false) {
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };

    this.planificacionService.listarPorFiltroDepartamento_core_central(params).subscribe((result: any) => {
      if (result.success) {
        this.lstDepartamentos = result.data;
        if (this.lstDepartamentos && this.lstDepartamentos.length > 0) this.lstDepartamentos.unshift({ idDepartamento: 0, nombreDepartamento: "Seleccione" });
        else this.lstDepartamentos = [({ idDepartamento: 0, nombreDepartamento: "Seleccione" })];
        this.requestInfoMunicipio.idDepartamento = ischange ? this.lstDepartamentos[0].idDepartamento : (this.requestInfoMunicipio.idDepartamento || this.lstDepartamentos[0].idDepartamento);
        this.listarPorFilroProvincia(ischange);
      }
    }, (error) => this.errorMensaje(error));
  }

  listarPorFilroProvincia(ischange: boolean = false) {
    let idDepartamento = this.requestInfoMunicipio.idDepartamento;
    this.planificacionService.listarPorFilroProvincia({ idDepartamento: idDepartamento, }).subscribe((result: any) => {
      if (result.success) {
        this.lstProvincias = result.data;
        if (this.lstProvincias && this.lstProvincias.length > 0) this.lstProvincias.unshift({ idProvincia: 0, nombreProvincia: "Seleccione" });
        else this.lstProvincias = [({ idProvincia: 0, nombreProvincia: "Seleccione" })];
        this.requestInfoMunicipio.idProvincia = ischange ? this.lstProvincias[0].idProvincia : (this.requestInfoMunicipio.idProvincia || this.lstProvincias[0].idProvincia);
        this.listarPorFilroDistrito(ischange);
      }
    });
  }

  listarPorFilroDistrito(ischange: boolean = false) {
    let idProvincia = this.requestInfoMunicipio.idProvincia;
    this.planificacionService.listarPorFilroDistrito({ idProvincia: idProvincia }).subscribe((result: any) => {
      if (result.success) {
        this.lstDistritos = result.data;
        if (this.lstDistritos && this.lstDistritos.length > 0) this.lstDistritos.unshift({ codDistrito: '', nombreDistrito: "Seleccione" });
        else this.lstDistritos = [({ codDistrito: '', nombreDistrito: "Seleccione" })];
        this.requestInfoMunicipio.codigoUbigeo = ischange ? this.lstDistritos[0].codDistrito : (this.requestInfoMunicipio.codigoUbigeo || this.lstDistritos[0].codDistrito);
      }
    });
  }

  actualizarEstado(estado: string) {
    const param: any = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: estado,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-establecimiento-BL"]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnDescargarIniciativaBL() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_1,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.obtenerInfoAdjuntosSolicitudBL(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success && resp.data && resp.data.length > 0) {
        let file: any[] = resp.data.filter((x: any) => x.tipoArchivo === CodigosSOLBL.TAB_1_5);
          if (file.length > 0) this.btnDescargarArchivoTabla(file[0].idArchivo);
          else this.toast.warn("No se encontró Lista Previa de Beneficiarios");
      } else {
        this.toast.warn("No se encontró el documento Iniciativa de Bosque Local.");
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnDescargarListaBenef() {
    const param = { idSolBosqueLocal: this.idPlan };
   // debugger
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.ArchivoPDFSolicitudBosqueLocalBeneficiario(param).subscribe((resp:any) => {
      this.dialog.closeAll();
      if (resp.success && resp.archivo) {
        DownloadFile(resp.archivo, resp.nombeArchivo, resp.contenTypeArchivo);
      }
      else{
        this.toast.warn(resp.message);
      }

    }, (error) => this.errorMensaje(error, true));

  }

  btnEnviarMunicipioArffs(event: any) {
    if(!this.validarEnviarMunicipioArffs()) return;

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de enviar a Municipio y ARFFS?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if(this.requestEvaluacion.tipo === this.RADIO_EVAL.FAV) {
          this.actualizarEstado(CodigoEstadoBL.PUBLICA_BENEF);
        } else {
          this.actualizarEstado(CodigoEstadoBL.EVAL_DESFAVORABLE);
        }
      },
    });

  }

  validarEnviarMunicipioArffs() {
    let valido = true;
    if (!this.adjunto2_1.idSolBosqueLocalArchivo) {
      valido = false;
      this.toast.warn("(*) Debe Guardar Reporte de Dirección Regional de la sección 2.1");
    }
    if (this.requestEvaluacion.tipo === this.RADIO_EVAL.FAV) {
      let auxTieneObs = this.listaBeneficiarios.some(x => (x.flagListaFinal !== true && !x.observaciones));
      let auxTodosObs = this.listaBeneficiarios.every(x => (x.flagListaFinal !== true));
      if (auxTodosObs) {
        valido = false;
        this.isGuardoFav_NoFav = false;
        this.toast.warn("(*) Debe seleccionar al menos un beneficiario de la sección 2.3.");
      } else if (auxTieneObs) {
        valido = false;
        this.isGuardoFav_NoFav = false;
        this.toast.warn("(*) Debe ingresar las observaciones de los registros que desmarcó de la sección 2.3.");
      } else if (!this.isGuardoFav_NoFav) {
        valido = false;
        this.toast.warn("(*) Debe Guardar Decisión Final de Beneficiarios de la sección 2.3");
      }
    } else {
      if (!this.isGuardoFav_NoFav) {
        valido = false;
        this.toast.warn("(*) Debe Guardar Decisión Final de Beneficiarios de la sección 2.3");
      }
    }

    return valido;
  }

  btnEnviarListaFinal() {
    this.actualizarEstado(CodigoEstadoBL.PATRON_FINAL_BENEF);
  }

  registrarArchivoFile(file: any, archivo: any) {
    
    archivo.nombreArchivo = file.nombre;
  }

  btnDescargarArchivoTabla(idArchivo: number) {
    if (idArchivo) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  btnDescargarDocumentoMunicipio(idArchivo: number) {
    if (idArchivo && idArchivo > 0) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  btnGuardarInfoMunicipio() {
    if (!this.validarInfoMunicipio()) return;
    this.registrarInfoMunicipio();
  }

  btnEliminarDocumento(event: any, id: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarInfoMunicipio(id, index);
      },
    });
  }

  openModalEnvioMunicipio(tipo: string, data: any): void {
    this.lstProvincias = [];
    this.lstDistritos = [];
    this.limpiarCampos();

    if (tipo == "C") {
      this.listarPorFiltroDepartamento();
      this.tituloModalEnvioMunicipio = "Registrar Información Documento";
    } else if (tipo == "E") {
      this.tituloModalEnvioMunicipio = "Editar Información Documento";
    }
    this.verModalEnvioMunicipio = true;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  obtenerInfoAdjuntoResultadoEvaluacion() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_2,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success && resp.data && resp.data.length > 0) {

        let file2_1: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto2_1.tipoArchivo);
        if (file2_1.length > 0) {
          this.adjunto2_1.idSolBosqueLocalArchivo = file2_1[0].idSolBosqueLocalArchivo;
          this.adjunto2_1.idArchivo = file2_1[0].idArchivo;
        }

        let file2_3_1: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto2_3_1.tipoArchivo);
        if (file2_3_1.length > 0) {
          this.adjunto2_3_1.idSolBosqueLocalArchivo = file2_3_1[0].idSolBosqueLocalArchivo;
          this.adjunto2_3_1.idArchivo = file2_3_1[0].idArchivo;
        }

        let file2_3_2: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto2_3_2.tipoArchivo);
        if (file2_3_2.length > 0) {
          this.adjunto2_3_2.idSolBosqueLocalArchivo = file2_3_2[0].idSolBosqueLocalArchivo;
          this.adjunto2_3_2.idArchivo = file2_3_2[0].idArchivo;
        }

        let file2_3_3: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto2_3_3.tipoArchivo);
        if (file2_3_3.length > 0) {
          this.adjunto2_3_3.idSolBosqueLocalArchivo = file2_3_3[0].idSolBosqueLocalArchivo;
          this.adjunto2_3_3.idArchivo = file2_3_3[0].idArchivo;
        }

        let file2_5: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto2_5.tipoArchivo);
        if (file2_5.length > 0) {
          this.adjunto2_5.idSolBosqueLocalArchivo = file2_5[0].idSolBosqueLocalArchivo;
          this.adjunto2_5.idArchivo = file2_5[0].idArchivo;
        }


        let files = resp.data.filter((file: any) => file.tipoArchivo === this.requestInfoMunicipio.tipoArchivo);
        if (files && files.length > 0) {
          this.setInfoEnvioMunicipio(files);
        }
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInfoDocumentoMunicipio() {
    let param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_4,
      tipoArchivo: CodigosSOLBL.TAB_2_4_A
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          this.setInfoEnvioMunicipio(resp.data);

        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  setInfoEnvioMunicipio(lista: any[]) {
    this.listaEnvioMunicipios = lista;
  }

  registrarInfoMunicipio() {

    const request = {
      idSolBosqueLocalArchivo: this.requestInfoMunicipio.idSolBosqueLocalArchivo ? this.requestInfoMunicipio.idSolBosqueLocalArchivo : 0,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.requestInfoMunicipio.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_4,
      tipoArchivo: CodigosSOLBL.TAB_2_4_A,
      nombreArchivo: this.requestInfoMunicipio.nombreArchivo,
      codigoUbigeoMunicipio: this.requestInfoMunicipio.codigoUbigeo,
      observacion: this.requestInfoMunicipio.comentarios,
      idUsuarioRegistro: this.usuario.idusuario
    };
    
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.requestInfoMunicipio.idSolBosqueLocalArchivo = resp.codigo;
          this.toast.ok(resp.message);
          this.verModalEnvioMunicipio = false;
          this.obtenerInfoDocumentoMunicipio();
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  eliminarArchivoIniciativa(ok: boolean, condicion: number) {
    if (!ok) return;

    switch (condicion) {
      case 1:
        this.adjuntoFirmado.idArchivo = 0;
        if (this.adjuntoFirmado.idSolBosqueLocalArchivo) {
          this.eliminarInfoAdjuntoIniciativa(this.adjuntoFirmado.idSolBosqueLocalArchivo, condicion);
        }
        break;
      default:
        break;
    }

  }

  eliminarInfoMunicipio(id: number, index: number) {
    this.requestInfoMunicipio.idArchivo = 0;
    if (id && id > 0) {
      let request = {
        idSolBosqueLocalArchivo: id,
        idUsuarioElimina: this.usuario.idusuario
      };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            this.toast.ok(resp.message);
            this.listaEnvioMunicipios.splice(index, 1);
          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }

  }

  validarInfoMunicipio(): boolean {
    let valido = true;
    var mensaje = "";
    if (!this.requestInfoMunicipio.idArchivo) {
      mensaje = mensaje + '(*) Debe adjuntar el documento asociado.\n';
      valido = false;
    }
    if (this.requestInfoMunicipio.idDepartamento == 0) {
      mensaje = mensaje + '(*) Debe seleccionar el departamento.\n';
      valido = false;
    }
    if (this.requestInfoMunicipio.idProvincia == 0) {
      mensaje = mensaje + '(*) Debe seleccionar la provincia.\n';
      valido = false;
    }
    if (this.requestInfoMunicipio.codigoUbigeo == "") {
      mensaje = mensaje + '(*) Debe seleccionar el distrito del municipio.\n';
      valido = false;
    }

    if (mensaje != "") this.toast.warn(mensaje);

    return valido;
  }

  limpiarCampos(): void {
    this.requestInfoMunicipio = { idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_2_4_A}`, idDepartamento: 0, idProvincia: 0, nombreArchivo: null, codigoUbigeo: "", comentarios: null };
  }

  /********************************************/
  //2.1
  btnGuardarInfoSaneamiento() {
    if (!this.adjunto2_1.idArchivo) {
      this.toast.warn('(*) Debe adjuntar Reporte de de Dirección Regional Agraria.');
      return;
    }
    this.registrarInfoSaneamiento();
  }

  registrarInfoSaneamiento() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto2_1.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto2_1.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_1,
      tipoArchivo: this.adjunto2_1.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        this.adjunto2_1.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //2.3
  btnGuardarDecisionFinal() {
    if (!this.validarDecisionFinal()) return;

    if (this.requestEvaluacion.tipo === this.RADIO_EVAL.NOFAV) {
      this.btnGuardarEstado();
    } else {
      this.registrarBeneficiarios(this.listaBeneficiarios);
    }
  }

  validarDecisionFinal(): boolean {
    let valido = true;
    if (!this.requestEvaluacion.tipo) {
      valido = false;
      this.toast.warn("(*) Debe seleccionar la evaluación.");
    } else {
      if (this.requestEvaluacion.tipo === this.RADIO_EVAL.NOFAV) {
        if (!this.adjunto2_3_3.idArchivo) {
          valido = false;
          this.toast.warn("(*) Debe de adjuntar Reporte de Evaluación.");
        }
      } else {
        if (this.isEstEval_Benef) {
          let auxTieneObs = this.listaBeneficiarios.some(x => (x.flagListaFinal !== true && !x.observaciones));
          let auxTodosObs = this.listaBeneficiarios.every(x => (x.flagListaFinal !== true));
          if (auxTodosObs) {
            valido = false;
            this.toast.warn("(*) Debe seleccionar al menos un beneficiario.");
          } else if (auxTieneObs) {
            valido = false;
            this.toast.warn("(*) Debe ingresar las observaciones de los registros que desmarcó.");
          }
        } else if (this.isEstPub_Benef) {
          let auxTieneOpo = this.listaBeneficiarios.some(x => (x.tieneOposicion === true && !x.idAdjuntoOposicion));
          if (auxTieneOpo) {
            valido = false;
            this.toast.warn("(*) Debe de adjuntar los documentos que seleccionó con oposición.");
          }
        }
      }
    }
    return valido;
  }

  registrarBeneficiarios(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.registrarSolicitudBosqueLocalBeneficiario(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) this.btnGuardarEstado();
      else this.toast.warn(resp.message);
    }, (error) => this.errorMensaje(error, true));
  }

  btnGuardarEstado() {
    const params = [{
      "idBosqueLocalEvaluacion": this.idBosqueLocalEvaluacion,
      "idSolBosqueLocal": this.idPlan,
      "codigoResultadoEvaluacion": this.requestEvaluacion.tipo,
      "idArchivoReporte": null,
      "idUsuarioRegistro": this.usuario.idusuario
    }
    ]
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.registrarSolicitudBosqueLocalEvaluacion(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.isGuardoFav_NoFav = true;
        if (this.requestEvaluacion.tipo === this.RADIO_EVAL.NOFAV) {
          this.registrarAdjuntoNoFavorable();
        } else {
          if (!this.isEstPub_Benef && this.adjunto2_3_1.idArchivo) {
            this.registrarAdjuntoLsitaPrevia();
          } else if (this.isEstPub_Benef && this.adjunto2_3_2.idArchivo) {
            this.registrarAdjuntoLsitaFinal();
          } else {
            this.toast.ok("Se actualizó la decisión final de beneficiarios correctamente");
          }
        }
      }
      else this.toast.warn(resp.message);
    }, (error) => this.errorMensaje(error, true));
  }

  registrarAdjuntoLsitaPrevia() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto2_3_1.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto2_3_1.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_3,
      tipoArchivo: this.adjunto2_3_1.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.adjunto2_3_1.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok("Se actualizó la decisión final de beneficiarios correctamente");
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarAdjuntoLsitaFinal() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto2_3_2.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto2_3_2.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_3,
      tipoArchivo: this.adjunto2_3_2.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.adjunto2_3_2.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok("Se actualizó la decisión final de beneficiarios correctamente");
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarAdjuntoNoFavorable() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto2_3_3.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto2_3_3.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_3,
      tipoArchivo: this.adjunto2_3_3.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.adjunto2_3_3.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok("Se actualizó la decisión final de beneficiarios correctamente");
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //2.4
  btnGuardarReporteEval() {
    if (!this.adjunto2_5.idArchivo) {
      this.toast.warn('(*) Debe adjuntar Reporte de Evaluación Firmado.');
      return;
    }
    this.registrarReporteEval();
  }

  registrarReporteEval() {
    const request = {
      idSolBosqueLocalArchivo: this.adjunto2_5.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto2_5.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_2,
      codigoSubSeccion: CodigosSOLBL.TAB_2_5,
      tipoArchivo: this.adjunto2_5.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        // this.adjunto2_3.idSolBosqueLocalArchivo = resp.codigo;
        this.adjunto2_5.idSolBosqueLocalArchivo = resp.codigo;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  /* ******* GENERAL******* */
  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivoNew(ok: boolean, archivo: any) {
    if (!ok) return;
    archivo.idArchivo = 0;
    if (archivo.idSolBosqueLocalArchivo) {
      const params = {
        idSolBosqueLocalArchivo: archivo.idSolBosqueLocalArchivo,
        idUsuarioElimina: this.usuario.idusuario
      };
      this.eliminarAdjuntoBLServ(params, archivo);
    }
  }

  eliminarAdjuntoBLServ(params: any, archivo: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(params).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        archivo.idSolBosqueLocalArchivo = 0;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarInfoAdjuntoIniciativa(id: number, condicion: number) {

    let request = {
      idSolBosqueLocalArchivo: id,
      idUsuarioElimina: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          if (condicion == 1) {
            this.adjunto2_1.idSolBosqueLocalArchivo = 0;
          }
          else if (condicion == 2) {
            this.adjunto2_3.idSolBosqueLocalArchivo = 0;
          }
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  eliminarArchivo(ok: boolean, condicion: number) {
    if (!ok) return;
    switch (condicion) {
      case 1:
        this.adjunto2_1.idArchivo = 0;
        if (this.adjunto2_1.idSolBosqueLocalArchivo) {
          this.eliminarInfoAdjuntoIniciativa(this.adjunto2_1.idSolBosqueLocalArchivo, condicion);
        }
        break;
      case 2:
        this.adjunto2_3.idArchivo = 0;
        if (this.adjunto2_3.idSolBosqueLocalArchivo) {
          this.eliminarInfoAdjuntoIniciativa(this.adjunto2_3.idSolBosqueLocalArchivo, condicion);
        }
        break;
      default:
        break;
    }
  }

}
