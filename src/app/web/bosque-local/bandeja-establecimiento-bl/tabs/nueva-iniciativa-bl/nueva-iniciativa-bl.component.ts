import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, CoreCentralService, UsuarioService } from '@services';
import { ConfirmationService } from 'primeng/api';
import { DownloadFile, ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { BosqueLocalService } from 'src/app/service/solicitud-bosque-local/bosque-local.service';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { SolicitudBosqueLocalService as SolBosquelocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { ModalBeneficiarioBlComponent } from '../../components/modal-beneficiario-bl/modal-beneficiario-bl.component';
import { finalize } from 'rxjs/operators';
import { MunicipioLocalService } from 'src/app/service/municipio-persona.service';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { Router } from '@angular/router';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { PlantillaConst } from "src/app/shared/plantilla.const";
import { MapCustomSolBosqueLocalComponent } from 'src/app/shared/components/map-custom-sol-bosque-local/map-custom-sol-bosque-local.component';
@Component({
  selector: 'tab-nueva-iniciativa-bl',
  templateUrl: './nueva-iniciativa-bl.component.html',
  styleUrls: ['./nueva-iniciativa-bl.component.scss']
})
export class NuevaIniciativaBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() isDisbledFormu: boolean = false;
  /*****************************************************/
  @ViewChild(MapCustomSolBosqueLocalComponent) map!: MapCustomSolBosqueLocalComponent;
  usuario!: UsuarioModel;
  plantillaConst =PlantillaConst;
  codigoProceso:string = CodigosSOLBL.CODIGO_PROCESO;
  codigoSubSeccion:string= CodigosSOLBL.TAB_1_3;
  tipoArchivoArea:string = 'SOLBL1_SSA3';
  tipoArchivoPunto:string = 'SOLBL1_SSP3';
  validarRegistroTipoBeneficiario:boolean = false;
  listaDominio: any[] = [];

  requestDatosRep: any = {
    idSolBosqueLocal: 0,
    idSolicitante: 0,
    gobiernoLocal: null,
    idUsuario: 0,
    nombresRepresentante: "",
    numeroDocumento: "",
    idPersona: 0,
  };

  comboDepart: any[] = [];
  comboProvincia: any[] = [];
  comboDistrito: any[] = [];

  requestBenef: any = {};
  listaBeneficiarios: any[] = [];
  listaPobLocal: any[] = [];
  listaPeqExtarctor: any[] = [];

  adjunto1_3: any = { idSolBosqueLocalArchivo: 0, tipoArchivo: CodigosSOLBL.TAB_1_3, idArchivo: 0 };
  adjunto1_5: any = { idSolBosqueLocalArchivo: 0, tipoArchivo: CodigosSOLBL.TAB_1_5, idArchivo: 0 };

  disabledEnviar: boolean = true;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private dialogService: DialogService,
    private serCoreCentral: CoreCentralService,
    private usuarioServ: UsuarioService,
    private evaluacionService: EvaluacionService,
    private bosqueLocalService: BosqueLocalService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private solBosquelocalService: SolBosquelocalService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService,
    private municipioLocalService: MunicipioLocalService,
    private router: Router,
    private archivoService: ArchivoService
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.requestDatosRep.idSolBosqueLocal = this.idPlan;
    this.requestDatosRep.idSolicitante = this.usuario.idusuario;
    this.requestDatosRep.idUsuario = this.usuario.idusuario;

    this.listarSolicitudBosqueLocalFinalidad();
    this.obtenerSolicitud();
    this.listarBeneficiarios();
    this.obtenerInfoAdjuntoBosqueLocal();
    this.validarlistarBeneficiarios();
  }

  listarSolicitudBosqueLocalFinalidad() {
    this.listaDominio = [];
    let param = {
      idSolBosqueLocal: this.idPlan
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.bosqueLocalService
      .listarSolicitudBosqueLocalFinalidad(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            let obj: any = {};
            obj.idBosqueLocalFinalidad = element.idBosqueLocalFinalidad;
            obj.idSolBosqueLocal = element.idSolBosqueLocal;
            obj.codigoFinalidad = element.codigoFinalidad;
            obj.seleccionado = element.seleccionado;
            obj.descripcion = element.descripcion;
            this.listaDominio.push(obj);
          });
        } else {
          this.listarInspectoresDocumentos();
        }
      });
  }

  listarInspectoresDocumentos() {
    let paramPrefijo = {
      prefijo: "TDOM",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .listarParametroPorPrefijo(paramPrefijo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          response.data.forEach((element: any) => {
            let obj: any = {};
            obj.idBosqueLocalFinalidad = null;
            obj.idSolBosqueLocal = this.idPlan;
            obj.codigoFinalidad = element.codigo;
            obj.descripcion = element.valorPrimario;
            this.listaDominio.push(obj);
          });
        }
      });
  }

  actualizarEstado(event: any) {
    this.validarDisbledEnviar();
    if (!this.disabledEnviar) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: 'Una vez enviada la solicitud, esta no podrá ser editada. ¿Desea Continuar?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          this.actualizarEstadoTrue();
        },
      });
    }
  }

  actualizarEstadoTrue() {

    let param: any = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: CodigoEstadoBL.EVAL_BENEF,
      idUsuarioModificacion: this.usuario.idusuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.toast.ok(resp.message);
          this.router.navigate(["/bosque-local/bandeja-establecimiento-BL"]);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  obtenerInfoAdjuntoBosqueLocal() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_1
    };

    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBosqueLocal(param)
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          let files: any[] = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto1_3.tipoArchivo);
          if (files.length > 0) {
            this.adjunto1_3.idSolBosqueLocalArchivo = files[0].idSolBosqueLocalArchivo;
            this.adjunto1_3.idArchivo = files[0].idArchivo;
          }

          let files5 = resp.data.filter((file: any) => file.tipoArchivo === this.adjunto1_5.tipoArchivo);
          if (files5.length > 0) {
            this.adjunto1_5.idSolBosqueLocalArchivo = files5[0].idSolBosqueLocalArchivo;
            this.adjunto1_5.idArchivo = files5[0].idArchivo;
            //this.validarDisbledEnviar();
          }
        }
      }, (error) => this.errorMensaje(error));
  }

  registrarSolicitudBosqueLocalFinalidad() {

    this.listaDominio.forEach((element: any) => {
      element.idUsuarioRegistro = this.usuario.idusuario
    });
    this.bosqueLocalService
      .registrarSolicitudBosqueLocalFinalidad(this.listaDominio)
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok('Se Guardó Correctamente la Finalidad de Bosque Local');
          this.listarSolicitudBosqueLocalFinalidad();
        }
      });
  }

  //SERVICIOS
  listaComboDepart() {
    this.serCoreCentral.listarPorFiltroDepartamento({}).subscribe((resp: any) => {
      if (resp.success) {
        this.comboDepart = resp.data;
        //  this.requestDatosRep.idDepartamento = this.comboDepart[0].idDepartamento;
        this.listaComboProvincia();
      }
    });
  }

  listaComboProvincia(ischange: boolean = false) {
    this.comboProvincia = [];
    if (ischange) {
      this.requestDatosRep.idProvincia = null;
    }

    const prams = { idDepartamento: this.requestDatosRep.idDepartamento };
    this.serCoreCentral.listarPorFilroProvincia(prams).subscribe((resp: any) => {
      if (resp.success) {
        this.comboProvincia = resp.data;
        //  this.requestDatosRep.idProvincia = this.requestDatosRep.idProvincia ? this.requestDatosRep.idProvincia : this.comboProvincia[0].idProvincia;
        this.listaComboDistrito(ischange);
      }
    });
  }

  listaComboDistrito(ischange: boolean = false) {
    this.comboDistrito = [];
    if (ischange) {
      this.requestDatosRep.numeroDocumento = "";
      this.requestDatosRep.nombresRepresentante = "";
      this.requestDatosRep.gobiernoLocal = null;
      //this.validarDisbledEnviar();
    }
    const prams = { idProvincia: this.requestDatosRep.idProvincia };
    this.serCoreCentral.listarPorFilroDistrito(prams).subscribe((resp: any) => {
      if (resp.success) {
        this.comboDistrito = resp.data;
        if (this.requestDatosRep.gobiernoLocal)
          this.obtenerDatosMunicipio();
      }
    });
  }

  obtenerDatosMunicipio() {
    this.requestDatosRep.numeroDocumento = "";
    this.requestDatosRep.nombresRepresentante = "";
    this.requestDatosRep.idPersona = 0;
    //this.validarDisbledEnviar();
    const params = { idDistrito: this.requestDatosRep.gobiernoLocal };
    this.municipioLocalService.listarMunicipioPersona(params).subscribe(resp => {
      if (resp.success && resp.data) {
        if (resp.data.length > 0) {
          this.requestDatosRep.numeroDocumento = resp.data[0].numeroDocumento;
          this.requestDatosRep.nombresRepresentante = resp.data[0].nombrePersona;
          this.requestDatosRep.idPersona = resp.data[0].idPersona;
        } else {
          this.toast.warn("No se encontraron datos del Representante. Comuníquese con el Administrador.");
        }
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error));
  }

  obtenerSolicitud() {
    const params = { idSolBosqueLocal: this.idPlan };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.obtenerSolicitudBosqueLocal(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data.length > 0) {
        
        
        this.requestDatosRep.idDepartamento = resp.data[0].idDepartamento || null;
        this.requestDatosRep.idProvincia = resp.data[0].idProvincia || null;
        this.requestDatosRep.gobiernoLocal = resp.data[0].gobiernoLocal || null;
        this.requestDatosRep.numeroDocumento = resp.data[0].numeroDocumento || "";
        this.requestDatosRep.nombresRepresentante = resp.data[0].nombresRepresentante || "";
        //this.validarDisbledEnviar();
        this.listaComboDepart();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarSolicitud(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.actualizarSolicitudBosqueLocal(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        //this.validarDisbledEnviar();
      }
      else this.toast.warn(resp.message);
    }, (error) => this.errorMensaje(error, true));
  }

  listarBeneficiarios() {
    const params = { "idSolBosqueLocal": this.idPlan, "pageNum": 1, "pageSize": 200 }
    this.solBosquelocalService.listarSolicitudBosqueLocalBeneficiario(params).subscribe(resp => {
      if (resp.success && resp.data) {
        this.listaBeneficiarios = resp.data;

      }
    });
  }

  registrarBeneficiarios(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosquelocalService.registrarSolicitudBosqueLocalBeneficiario(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarBeneficiarios();
      }
      else this.toast.warn(resp.message);
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarBeneficiariosServ(id: number, index: number) {
    
    const params = { idBosqueLocalBeneficiario: id, idUsuario: this.usuario.idusuario }
    if (id === 0) {
      this.listaBeneficiarios.splice(index, 1);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solBosquelocalService.eliminarSolicitudBosqueLocalBeneficiario(params).subscribe(resp => {
        this.dialog.closeAll();
        if (resp.success) {
          this.toast.ok(resp.message);
          this.listaBeneficiarios.splice(index, 1);
        }
        else this.toast.warn(resp.message);
      }, (error) => this.errorMensaje(error, true));
    }
  }

  //BOTONES
  btnGuardarFinalidad() {
    let siMarco: boolean = this.listaDominio.some(x => x.seleccionado === true);
    if (!siMarco) {
      this.toast.warn("(*) Debe de Seleccionar al menos un Tipo de Finalidad.");
      return;
    }
    this.registrarSolicitudBosqueLocalFinalidad();
  }

  btnGuardarDatosRep() {
    if (!this.requestDatosRep.numeroDocumento) {
      this.toast.warn("No se encontraron datos del Representante. Comuníquese con el Administrador.");
      return;
    }
    this.actualizarSolicitud(this.requestDatosRep);
  }

  btnGuardarArea() {
    if (!this.validarInfoAdjuntoCroquisArea()) return;
    this.registrarInfoAdjuntoCroquisArea();
  }

  btnGuardarBeneficiarios() {
    if (this.listaBeneficiarios.length === 0) {
      this.toast.warn("(*) Debe Agregar al menos un registro");
      return;
    }
    this.registrarBeneficiarios(this.listaBeneficiarios);
    this.validarlistarBeneficiarios();
  }

  btnGuardarIniciativa() {
    if (!this.validarInfoAdjuntoDocumentoFirmadoIniciativa()) return;
    this.registrarInfoAdjuntoDocumentoFirmadoIniciativa();

  }

  validarInfoAdjuntoCroquisArea(): boolean {
    let valido = true;
    if (!this.adjunto1_3.idArchivo) {
      this.toast.warn('(*) Debe adjuntar el croquis del área.');
      valido = false;
    }

    return valido;

  }

  registrarInfoAdjuntoCroquisArea() {

    const request = {
      idSolBosqueLocalArchivo: this.adjunto1_3.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto1_3.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_1,
      codigoSubSeccion: CodigosSOLBL.TAB_1_3,
      tipoArchivo: this.adjunto1_3.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.adjunto1_3.idSolBosqueLocalArchivo = resp.codigo;
          this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  validarInfoAdjuntoDocumentoFirmadoIniciativa(): boolean {
    let valido = true;
    if (!this.adjunto1_5.idArchivo) {
      this.toast.warn('(*) Debe adjuntar el documento firmado.');
      valido = false;
    }

    return valido;

  }

  registrarInfoAdjuntoDocumentoFirmadoIniciativa() {

    const request = {
      idSolBosqueLocalArchivo: this.adjunto1_5.idSolBosqueLocalArchivo,
      idSolBosqueLocal: this.idPlan,
      idArchivo: this.adjunto1_5.idArchivo,
      codigoSeccion: CodigosSOLBL.TAB_1,
      codigoSubSeccion: CodigosSOLBL.TAB_1_5,
      tipoArchivo: this.adjunto1_5.tipoArchivo,
      idUsuarioRegistro: this.usuario.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.adjunto1_5.idSolBosqueLocalArchivo = resp.codigo;
          this.toast.ok(resp.message);
          //this.validarDisbledEnviar();
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  btnDescargarPlantillaIniciativa() {
          const NombreGenerado: string = this.plantillaConst.PlantillaSolicitudInteresBosqueLocal;

          this.dialog.open(LoadingComponent, { disableClose: true });
          this.archivoService.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
            this.dialog.closeAll();
            if (data.isSuccess) {
              DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
            } else {
              this.toast.error('Ocurrió un problema, intente nuevamente');
            }
          }, () => {
            this.dialog.closeAll();
            this.toast.error('Ocurrió un problema, intente nuevamente');
          });

  }


  registrarArchivo(id: number, condicion: number) {
    switch (condicion) {
      case 3:
        this.adjunto1_3.idArchivo = id;
        break;
      case 5:
        this.adjunto1_5.idArchivo = id;
        break;
      default:
        break;
    }

  }

  eliminarArchivo(ok: boolean, condicion: number) {
    if (!ok) return;

    switch (condicion) {
      case 3:
        this.adjunto1_3.idArchivo = 0;
        if (this.adjunto1_3.idSolBosqueLocalArchivo) {
          this.eliminarInfoAdjuntoIniciativa(this.adjunto1_3.idSolBosqueLocalArchivo, condicion);
        }
        break;
      case 5:
        this.adjunto1_5.idArchivo = 0;
        if (this.adjunto1_5.idSolBosqueLocalArchivo) {
          this.eliminarInfoAdjuntoIniciativa(this.adjunto1_5.idSolBosqueLocalArchivo, condicion);
        }
        break;

      default:
        break;
    }

  }

  eliminarInfoAdjuntoIniciativa(id: number, condicion: number) {

    let request = {
      idSolBosqueLocalArchivo: id,
      idUsuarioElimina: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          if (condicion == 3)
            this.adjunto1_3.idSolBosqueLocalArchivo = 0;
          else if (condicion == 5)
            this.adjunto1_5.idSolBosqueLocalArchivo = 0;
          //this.validarDisbledEnviar();
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  validarlistarBeneficiarios (){

    const params = { "idSolBosqueLocal": this.idPlan, "pageNum": 1, "pageSize": 200 }

    let data = this.solBosquelocalService.listarSolicitudBosqueLocalBeneficiario(params).subscribe(resp => {
      if (resp.success && resp.data) {
        
        if(resp.data.length>0){
           this.validarRegistroTipoBeneficiario = true;
        }else{
          this.validarRegistroTipoBeneficiario = false;
        }
        //this.listaBeneficiarios = resp.data;
      }
     // this.validarRegistroTipoBeneficiario = false;
    });
    
  }

  validarDisbledEnviar() {
    
    let item = this.map._filesSHP.find(
      (e: any) => e.inServer === false
    );
    
    
    this.validarlistarBeneficiarios();

    //alert(this.validarRegistroTipoBeneficiario);

    if(!this.validarRegistroTipoBeneficiario){

      this.disabledEnviar = true;
      this.toast.warn("(*) Debe Agregar el mínimo de Beneficiarios en Tipos de Beneficiarios");

    }else if (!this.requestDatosRep.numeroDocumento) {
      this.disabledEnviar = true;
      this.toast.warn("(*) Debe Agregar Datos Del Representante");
    }
    else if (!this.adjunto1_5.idSolBosqueLocalArchivo) {
      this.disabledEnviar = true;
      this.toast.warn("(*) Debe Adjuntar Documento En Iniciativa Del Bosque Local");
    } else if (typeof item === 'object') {
      this.toast.warn("Debe guardar los archivos SHP cargados en 1.3 Croquis del Área");
      this.disabledEnviar = true;
    }
    else this.disabledEnviar = false;

  }

  //MODALES
  btnModalBeneficiario(fila: any, index: number) {
    let isEdit = !!fila;
    const refResp = this.dialogService.open(ModalBeneficiarioBlComponent, {
      header: `${isEdit ? 'Editar' : 'Registrar'} Beneficiario`,
      width: '580px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: false,
        isEdit: isEdit,
        idUser: this.usuario.idusuario,
        idPlan: this.idPlan,
        datos: isEdit ? { ...fila } : {},
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      
      if (resp) {
        if (isEdit) this.listaBeneficiarios[index] = { ...resp };
        else this.listaBeneficiarios.push(resp);
      }
    });
  }

  btnEliminar(event: any, id: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarBeneficiariosServ(id, index);
        this.validarlistarBeneficiarios();
      },
    });
  }

  btnDescargarArchivoTabla(idArchivo: number) {
    if (idArchivo) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  //FUNCIONES
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
