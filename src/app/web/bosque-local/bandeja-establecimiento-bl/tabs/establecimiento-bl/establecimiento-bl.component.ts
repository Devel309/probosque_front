import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ArchivoService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ModalDocumentosEvalCampoBlComponent } from '../../components/modal-documentos-eval-campo-bl/modal-documentos-eval-campo-bl.component';
import { ModalInfoEspecialBlComponent } from '../../components/modal-info-especial-bl/modal-info-especial-bl.component';

@Component({
  selector: 'tab-establecimiento-bl',
  templateUrl: './establecimiento-bl.component.html',
  styleUrls: ['./establecimiento-bl.component.scss']
})
export class EstablecimientoBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() estadoSolicitud: string = "";
  @Input() disabledEstableciBL: boolean = false;
  /*****************************************************/
  usuario!: UsuarioModel;
  requestEnviar: any = {};

  listaDocumentos: any[] = [];
  listaTipoDocuemntos: any[] = [];
  prefijoDoc: string = "TDOCESTBOS";

  isEnvioGeometria: number = 0;
  isCodigoTH: boolean = false;
  dataModalGeom: any = {};

  constructor(
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private archivoServ: ArchivoService,
    private genericoServ: GenericoService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private router: Router,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarComboTiposDocumentos();
    this.obtenerSolicitudBLServ();
    this.obtenerInfoDocumentosEvaluacionCampo();
  }

  //SERVICIOS
  listarComboTiposDocumentos() {
    const params: any = { prefijo: this.prefijoDoc };
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success) this.listaTipoDocuemntos = result.data;
    });
  }

  obtenerSolicitudBLServ() {
    const param = { idSolBosqueLocal: this.idPlan, };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerSolicitudBosqueLocal(param).subscribe(resp => {
      if (resp.success && resp.data && resp.data.length > 0) {
        this.dataModalGeom = resp.data[0];
        this.requestEnviar.codigoTH = resp.data[0].codigoTH;
        this.isEnvioGeometria = resp.data[0].idGeometria || 0;
        this.isCodigoTH = resp.data[0].codigoTH ? true : false;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInfoDocumentosEvaluacionCampo() {
    let param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_7,
      codigoSubSeccion: CodigosSOLBL.TAB_7_1
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          this.listaDocumentos = resp.data;
        }
      }, (error) => this.errorMensaje(error, true));
  }

  registrarInfoDocumentosEvaluacionCampo(params: any, isEnviar: boolean) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarInfoAdjuntosSolicitudBosqueLocal(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.obtenerInfoDocumentosEvaluacionCampo();
          if (isEnviar) {
            this.notificarCreacionTH();
          } else {
            this.toast.ok("Se Actualizó la Solicitud Correctamente.");
          }
        } else {
          this.toast.warn(resp.message);
        }
      }, (error) => this.errorMensaje(error, true));
  }

  eliminarDocumentoServ(id: number, index: number) {
    if (id === 0) {
      this.listaDocumentos.splice(index, 1);
    } else {
      const param = { idSolBosqueLocalArchivo: id, idUsuarioElimina: this.usuario.idusuario, };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(param).subscribe(resp => {
        this.dialog.closeAll();
        if (resp.success) {
          this.listaDocumentos.splice(index, 1);
          this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  generarCodigoTHServ() {
    const param = { idSolBosqueLocal: this.idPlan, idUsuarioRegistro: this.usuario.idusuario };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.generarTHSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.requestEnviar.codigoTH = resp.data.codigoTH;
        this.actualizarSolicitudBLServ(false, null, false, true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarSolicitudBLServ(isRegDoc: boolean, listaDoc: any, isEnviar: boolean, isGenerar: boolean) {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoTH: this.requestEnviar.codigoTH,
      idUsuarioRegistro: this.usuario.idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.isCodigoTH = true;
        if (!isGenerar) {
          if (isEnviar) {
            if (isRegDoc) this.registrarInfoDocumentosEvaluacionCampo(listaDoc, isEnviar);
            else this.notificarCreacionTH();
          } else {
            if (isRegDoc) this.registrarInfoDocumentosEvaluacionCampo(listaDoc, isEnviar);
            else this.toast.ok("Se Actualizó la Solicitud Correctamente.");
          }
        }
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  notificarCreacionTH() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoTH: this.requestEnviar.codigoTH,
      estadoSolicitud: CodigoEstadoBL.SOL_TH,
      idUsuarioRegistro: this.usuario.idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.notificarCreacionTH(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        // this.toast.ok("Se Actualizó la Solicitud Correctamente.");
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-evauacion-comite-BL"]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnGenerarTH() {
    this.generarCodigoTHServ();
  }

  btnModalDocumento() {
    const respRef = this.dialogService.open(ModalDocumentosEvalCampoBlComponent, {
      header: 'Registrar Documento',
      width: '500px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        idPlan: this.idPlan, idUser: this.usuario.idusuario,
        codigoSeccion: `${CodigosSOLBL.TAB_7}`,
        codigoSubSeccion: `${CodigosSOLBL.TAB_7_1}`,
        prefijoDoc: this.prefijoDoc
      },
    });

    respRef.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listaDocumentos.push(resp);
      }
    });
  }

  btnEliminarDoc(event: any, id: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarDocumentoServ(id, index);
      },
    });
  }

  btnDescargarDoc(idArchivo: number) {
    if (idArchivo) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  btnGuardar(isEnviar: boolean) {
    let aux = this.listaDocumentos.filter(x => x.idSolBosqueLocalArchivo === 0);
    if (this.requestEnviar.codigoTH && !this.isCodigoTH && aux.length > 0) {
      this.actualizarSolicitudBLServ(true, aux, isEnviar, false);
    } else if (this.requestEnviar.codigoTH && !this.isCodigoTH) {
      this.actualizarSolicitudBLServ(false, null, isEnviar, false);
    } else if (aux.length > 0) {
      this.registrarInfoDocumentosEvaluacionCampo(aux, isEnviar);
    } else if (isEnviar) {
      this.notificarCreacionTH();
    }
  }

  btnEnviar(event: any) {
    if (!this.validarEnviar()) return;
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de Enviar a Notificación?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.btnGuardar(true);
      },
    });
  }

  btnEnviarGeometria() {
    const respRef = this.dialogService.open(ModalInfoEspecialBlComponent, {
      header: 'Enviar Información Espacial',
      width: '900px', style: { margin: '15px' }, contentStyle: { overflow: 'auto' },
      data: { idUser: this.usuario.idusuario, idPlan: this.idPlan, datos: this.dataModalGeom },
    });

    respRef.onClose.subscribe((resp: any) => {
      if (resp) {
        this.isEnvioGeometria = resp;
      }
    });
  }

  //FUNCIONES
  validarEnviar(): boolean {
    let valido = true;
    if (!this.requestEnviar.codigoTH) {
      valido = false;
      this.toast.warn("(*) Debe Generar el Código TH.");
    }
    if (!this.isEnvioGeometria) {
      valido = false;
      this.toast.warn("(*) Debe de Enviar la Geometría.");
    }
    if (this.listaDocumentos.length === 0) {
      valido = false;
      this.toast.warn("(*) Debe registrar al menos un documento.");
    } else {
      let faltaDoc = false;
      let msjDoc = "Debe adjuntar los documentos de tipo:\n";
      let auxTipTabla = this.listaDocumentos.map(x => x.tipoArchivo);
      
      this.listaTipoDocuemntos.forEach(x => {
        if (!auxTipTabla.includes(x.codigo) && x.codigo !== "TDOCESTBOSOTROS") {
          faltaDoc = true;
          msjDoc = msjDoc + x.valorPrimario + "\n"
        }
      });
      if (faltaDoc) {
        valido = false;
        this.toast.warn(msjDoc);
      }
    }

    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
