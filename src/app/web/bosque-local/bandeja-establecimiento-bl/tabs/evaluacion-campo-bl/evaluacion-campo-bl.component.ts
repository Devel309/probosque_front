import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { finalize } from 'rxjs/operators';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalService as SolBosquelocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { ModalDocumentosEvalCampoBlComponent } from '../../components/modal-documentos-eval-campo-bl/modal-documentos-eval-campo-bl.component';

@Component({
  selector: 'tab-evaluacion-campo-bl',
  templateUrl: './evaluacion-campo-bl.component.html',
  styleUrls: ['./evaluacion-campo-bl.component.scss']
})
export class EvaluacionCampoBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() disabledEvalComiteTec: boolean = false;
  @Input() evaluacionCampo: boolean = false;
  @Input() showEvalObserv:  boolean = false;

  /*****************************************************/
  usuario!: UsuarioModel;

  codigoProceso: string = CodigosSOLBL.CODIGO_PROCESO;
  codigoSubSeccion: string = CodigosSOLBL.TAB_4_2;
  tipoArchivoArea: string = CodigosSOLBL.TAB_4_2 + "_A";
  tipoArchivoPunto: string = CodigosSOLBL.TAB_4_2 + "_P";

  listaDocumentos: any[] = [];

  requestReporteCampo: any = {};
  adjunto4_3: any = { tipoArchivo: `${CodigosSOLBL.TAB_4_3_A}`, idArchivo: 0, idSolBosqueLocalArchivo: 0 };

  constructor(
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private archivoServ: ArchivoService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private solBosqueLocalService: SolBosquelocalService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    if(this.idPlan){
      this.obtenerInfoAdjuntosEvaluacionCampo();
    }
  }

  /******** 4.1 ***********/
  //  Servicios
  obtenerInfoAdjuntosEvaluacionCampo() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_4,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(param)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success && resp.data && resp.data.length > 0) {
        let file4_3: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto4_3.tipoArchivo);
        if (file4_3 && file4_3.length > 0) {
          this.adjunto4_3.idSolBosqueLocalArchivo = file4_3[0].idSolBosqueLocalArchivo;
          this.adjunto4_3.idArchivo = file4_3[0].idArchivo;
        }

        let files = resp.data.filter((file: any) => file.codigoSubSeccion === `${CodigosSOLBL.TAB_4_1}`);
        if (files && files.length > 0) {
          this.setInfoDocumentos(files);
        }

      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  obtenerInfoDocumentosEvaluacionCampo() {
    let param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_4,
      codigoSubSeccion: CodigosSOLBL.TAB_4_1
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          this.setInfoDocumentos(resp.data);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  registrarInfoDocumentosEvaluacionCampo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosqueLocalService.registrarInfoAdjuntosSolicitudBosqueLocal(this.listaDocumentos)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.toast.ok(resp.message);
          this.obtenerInfoDocumentosEvaluacionCampo();
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));


  }

  registrarReporteCampo() {

    this.requestReporteCampo.idSolBosqueLocalArchivo = this.adjunto4_3.idSolBosqueLocalArchivo ? this.adjunto4_3.idSolBosqueLocalArchivo : 0;
    this.requestReporteCampo.idSolBosqueLocal = this.idPlan;
    this.requestReporteCampo.idArchivo = this.adjunto4_3.idArchivo;
    this.requestReporteCampo.codigoSeccion = CodigosSOLBL.TAB_4;
    this.requestReporteCampo.codigoSubSeccion = CodigosSOLBL.TAB_4_3;
    this.requestReporteCampo.tipoArchivo = this.adjunto4_3.tipoArchivo;
    this.requestReporteCampo.idUsuarioRegistro = this.usuario.idusuario;

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(this.requestReporteCampo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.adjunto4_3.idSolBosqueLocalArchivo = resp.codigo;
          this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  eliminarDocumentoServ(id: number, index: number) {
    if (id && id > 0) {
      let request = {
        idSolBosqueLocalArchivo: id,
        idUsuarioElimina: this.usuario.idusuario
      };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudBosqueLocalService.eliminarInfoAdjuntoSolicitudBosqueLocal(request)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {
            this.toast.ok(resp.message);
            this.listaDocumentos.splice(index, 1);
          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }else{
      this.listaDocumentos.splice(index, 1);
    }
  }

  //  Botones
  btnModalDocumento() {
    const respRef = this.dialogService.open(ModalDocumentosEvalCampoBlComponent, {
      header: 'Registrar Documento',
      width: '500px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        idPlan: this.idPlan, idUser: this.usuario.idusuario,
        codigoSeccion: `${CodigosSOLBL.TAB_4}`, 
        codigoSubSeccion: `${CodigosSOLBL.TAB_4_1}`,
        prefijoDoc: "TDOCGE"
      },
    });

    respRef.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listaDocumentos.push(resp);
      }
    });
  }

  btnEliminarDocumento(event: any, id: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarDocumentoServ(id, index);
      },
    });
  }

  btnDescargarDocumento(idArchivo: number) {
    if (idArchivo) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  btnGuardarDocumentos() {
    if (this.listaDocumentos.length === 0) {
      this.toast.warn("(*) Debe registrar al menos un documento.");
      return;
    }

    this.registrarInfoDocumentosEvaluacionCampo();
  }

  //  Funciones

  setInfoDocumentos(lista: any[]) {
    this.listaDocumentos = lista;
  }

  /******** 4.2 ***********/


  /******** 4.3 ***********/
  //  Servicios

  //  Botones
  btnGuardarReporteCampo() {
    if (!this.adjunto4_3.idArchivo) {
      this.toast.warn("(*) Debe de adjuntar el reporte de campo.");
      return;
    }

   this.registrarReporteCampo();
  }

  //  Funciones


  /******** GENERAL ***********/
  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, adjunto: any) {
    if (!ok) return;
    adjunto.idArchivo = 0;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
