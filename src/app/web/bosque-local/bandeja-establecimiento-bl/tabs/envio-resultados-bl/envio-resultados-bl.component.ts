import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'tab-envio-resultados-bl',
  templateUrl: './envio-resultados-bl.component.html',
  styleUrls: ['./envio-resultados-bl.component.scss']
})
export class EnvioResultadosBlComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idPlan: number = 0;
  @Input() disabledEnvioResultado: boolean = false;
  @Input() estadoSolicitud: string = "";
  /*****************************************************/
  usuario!: UsuarioModel;
  idSolBosqueLocalArchivo: number =0;
  tituloBloque: string = "";
  labelAdjunto1: string = "";
  lableValidAdjunto1: string = "";
  
  isAprobacion: boolean = false;
  isDenegacion: boolean = false;
  aparecer:boolean = false;
  listaEnvioMunicipios: any[] = [];
  requestResultado: any = {};
  adjunto6_1_A: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_6_1}_A` };
  adjunto6_1_B: any = { idSolBosqueLocalArchivo: 0, idArchivo: 0, tipoArchivo: `${CodigosSOLBL.TAB_6_1}_B` };
  listaDocumentos: any[] = [
    {anexo: "anexo1", nombre: "prueab1.pdf", idArchivo: 1},
    {anexo: "anexo2", nombre: "prueab2.pdf", idArchivo: 2},
  ];

  comboTiposDocumento: any[] = [];
  minDate: Date = new Date(2021, 0, 1);

  constructor(
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private router: Router,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private genericoServ: GenericoService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarComboTiposDocumentos();
    this.isAprobacion = this.estadoSolicitud === CodigoEstadoBL.FAV_EXPED_TEC || this.estadoSolicitud === CodigoEstadoBL.APROBADA;
    this.isDenegacion = this.estadoSolicitud === CodigoEstadoBL.DESFAV_EXPED_TEC || this.estadoSolicitud === CodigoEstadoBL.DENEGADA;

    if(this.isAprobacion) {
      this.tituloBloque = "Aprobación de Bosque Local";
      this.labelAdjunto1 = "Cagar Documento de Aprobación de Bosque Local: *";
      this.lableValidAdjunto1 = "Aprobación"
    } else if(this.isDenegacion) {
      this.tituloBloque = "Denegación de Bosque Local";
      this.labelAdjunto1 = "Cagar Documento de Denegación de Bosque Local: *";
      this.lableValidAdjunto1 = "Denegación"
    }

    this.listarSolicitud();
    this.obtenerInfoAdjuntoResultadoEvaluacion();
  }

  //  Servicios
  listarComboTiposDocumentos() {
    const params: any = {prefijo: "TDOCGE"};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.success) {
        this.comboTiposDocumento = result.data;
      }
    }, () => this.dialog.closeAll());
  }

  //BOTONES
  btnDescargarArchivoTabla(id: number) {

  }

  
  listarSolicitud() {
    
    const params = { "idSolBosqueLocal": this.idPlan,
    codigoSeccion: CodigosSOLBL.TAB_6,
    codigoSubSeccion: CodigosSOLBL.TAB_6_1};
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(params).subscribe(resp => {
      if (resp.data.length > 0) {
        
        this.idSolBosqueLocalArchivo = resp.data[0].idSolBosqueLocalArchivo;
        this.requestResultado.fechaDoc = new Date(resp.data[0].fechaDocumento);
        this.requestResultado.numeroDoc= resp.data[0].numeroDocumento;
        this.requestResultado.tipodoc= resp.data[0].tipoDocumento;
        this.requestResultado.asunto= resp.data[0].asunto;
        this.aparecer=true;
      }
    });
  }

  obtenerInfoAdjuntoResultadoEvaluacion() {
    const param = {
      idSolBosqueLocal: this.idPlan,
      codigoSeccion: CodigosSOLBL.TAB_6,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success && resp.data && resp.data.length > 0) {

        let file2_1: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto6_1_A.tipoArchivo);
        if (file2_1.length > 0) {
          this.adjunto6_1_A.idSolBosqueLocalArchivo = file2_1[0].idSolBosqueLocalArchivo;
          this.adjunto6_1_A.idArchivo = file2_1[0].idArchivo;
        }

        let file2_3_1: any[] = resp.data.filter((x: any) => x.tipoArchivo === this.adjunto6_1_B.tipoArchivo);
        if (file2_3_1.length > 0) {
          this.adjunto6_1_B.idSolBosqueLocalArchivo = file2_3_1[0].idSolBosqueLocalArchivo;
          this.adjunto6_1_B.idArchivo = file2_3_1[0].idArchivo;
        }
        
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnGuardar(){
    if(!this.validarEnvioGobLocal()) return;
    let param = {
      
      "fechaDocumento": this.requestResultado.fechaDoc,
        "numeroDocumento": this.requestResultado.numeroDoc,
        "tipoDocumento":this.requestResultado.tipodoc,
        "asunto": this.requestResultado.asunto,
        idSolBosqueLocal: this.idPlan,
        idSolBosqueLocalArchivo: this.adjunto6_1_A.idSolBosqueLocalArchivo,
        idArchivo: this.adjunto6_1_A.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_6,
        codigoSubSeccion: CodigosSOLBL.TAB_6_1,
        tipoArchivo: this.adjunto6_1_A.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario,
        
      };
      if(this.idSolBosqueLocalArchivo>0){
       this.Actualizar();
       this.Actualizar2()
      }
      else{
     this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.Guardar2();
          this.toast.ok("Se guardó los resultados correctamente");
          this.listarSolicitud();
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
    }
  }


  Actualizar(){
    if(!this.validarEnvioGobLocal()) return;
    let param = {
      "fechaDocumento": this.requestResultado.fechaDoc,
        "numeroDocumento": this.requestResultado.numeroDoc,
        "tipoDocumento":this.requestResultado.tipodoc,
        "asunto": this.requestResultado.asunto,
        idSolBosqueLocalArchivo: this.adjunto6_1_A.idSolBosqueLocalArchivo,
        idArchivo: this.adjunto6_1_A.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_6,
        codigoSubSeccion: CodigosSOLBL.TAB_6_1,
        tipoArchivo: this.adjunto6_1_A.tipoArchivo,
        idUsuario: this.usuario.idusuario
      };
      
      this.solicitudBosqueLocalService.actualizarInfoAdjuntoSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.toast.ok("Se actualizó los resultados correctamente");
          
         // this.listarSolicitud();
          
          if(this.adjunto6_1_A.tipoArchivo!=this.adjunto6_1_B.tipoArchivo){
            
            //this.Guardar2();
          }
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

     
      
  }

  Guardar2(){
    if(!this.validarEnvioGobLocal()) return;
    let param = {
      
        idSolBosqueLocal: this.idPlan,
        idSolBosqueLocalArchivo: this.adjunto6_1_B.idSolBosqueLocalArchivo,
        idArchivo: this.adjunto6_1_B.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_6,
        codigoSubSeccion: CodigosSOLBL.TAB_6_1,
        tipoArchivo: this.adjunto6_1_B.tipoArchivo,
        idUsuarioRegistro: this.usuario.idusuario
      };
     this.solicitudBosqueLocalService.registrarInfoAdjuntoSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
  
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }
    Actualizar2(){
    if(!this.validarEnvioGobLocal()) return;
    let param = {
      
        idSolBosqueLocal: this.idPlan,
        idSolBosqueLocalArchivo: this.adjunto6_1_B.idSolBosqueLocalArchivo,
        idArchivo: this.adjunto6_1_B.idArchivo,
        codigoSeccion: CodigosSOLBL.TAB_6,
        codigoSubSeccion: CodigosSOLBL.TAB_6_1,
        tipoArchivo: this.adjunto6_1_B.tipoArchivo,
        idUsuario: this.usuario.idusuario
      };
     this.solicitudBosqueLocalService.actualizarInfoAdjuntoSolicitudBosqueLocal(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
  
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }
  btnEnviarGobLocal() {
    this.actualizarEstado(CodigoEstadoBL.DENEGADA);
  
    
  }

  btnEnviarSerfor() {
    this.actualizarEstado(CodigoEstadoBL.APROBADA);
  }


  actualizarEstado(estado: string) {
    const param: any = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: estado,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-evauacion-comite-BL"]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  /* ******* GENERAL ******* */
  validarEnvioGobLocal(): boolean {
    let valido = true;
    if(!this.requestResultado.asunto) {
      valido = false;
      this.toast.warn("(*) Debe ingresar el Asunto.");
    }
    if(!this.requestResultado.numeroDoc) {
      valido = false;
      this.toast.warn("(*) Debe ingresar el Número de Documento.");
    }
    if(!this.requestResultado.fechaDoc) {
      valido = false;
      this.toast.warn("(*) Debe ingresar la Fecha de Documento.");
    }
    if(!this.requestResultado.tipodoc) {
      valido = false;
      this.toast.warn("(*) Debe ingresar el Tipo de Documento.");
    }
    if(!this.adjunto6_1_A.idArchivo) {
      valido = false;
      this.toast.warn(`(*) Debe adjuntar Documento de ${this.lableValidAdjunto1}.`);
    }
    if(!this.adjunto6_1_B.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe adjuntar Informe Técnico.");
    }

    return valido;
  }

  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, archivo: any) {
    if (!ok) return;
    archivo.idArchivo = 0;
  }



  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
