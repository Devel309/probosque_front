import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { varLocalStor } from 'src/app/shared/util-local-storage';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
@Component({
  selector: 'app-evaluacion-comite-tecnico',
  templateUrl: './evaluacion-comite-tecnico.component.html',
  styleUrls: ['./evaluacion-comite-tecnico.component.scss']
})
export class EvaluacionComiteTecnicoComponent implements OnInit, OnDestroy {

  tabIndex: number = 0;
  urlBandeja = "/bosque-local/bandeja-establecimiento-BL";
  idPlan: number = 0;
  estadoSolicitud: string = "";
  disabledEstudioTec: boolean = false;
  showEvalComiteTec: boolean = false;
  disabledEvalComiteTec: boolean = false;
  evaluacionGabinete: boolean = false;
  evaluacionCampo: boolean = false;
  showEnvioResultado: boolean = false;
  disabledEnvioResultado: boolean = false;
  showEstableciBL: boolean = false;
  disabledEstableciBL: boolean = false;
  usuario!: UsuarioModel;
  showEvalObserv: boolean = false;
  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService

  ) {
    this.usuario = this.usuarioServ.usuario;
    this.idPlan = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    if (!this.idPlan) this.router.navigate([this.urlBandeja]);

    let auxLocalStoragedata = localStorage.getItem(varLocalStor.BJA_SOL_EEVAL_CT_BL);
    if (auxLocalStoragedata) {
      let auxData = JSON.parse("" + auxLocalStoragedata);
      if (auxData.idSolicitud === this.idPlan) {
        
        this.disabledEstudioTec = auxData.disabledEstudioTec || false;
        this.estadoSolicitud = auxData.estadoSolicitud || "";
        this.showEvalComiteTec = auxData.showEvalComiteTec || false;
        this.disabledEvalComiteTec = auxData.disabledEvalComiteTec || false;

        this.showEvalObserv =  (this.estadoSolicitud=="SEBESTOBS" && this.usuario.sirperfil=="MUNICIPIO") || false;

        this.evaluacionGabinete = auxData.evaluacionGabinete || false;
        this.evaluacionCampo = auxData.evaluacionCampo || false;
        this.showEnvioResultado = auxData.showEnvioResultado || false;
        this.disabledEnvioResultado = auxData.disabledEnvioResultado || false;
        this.showEstableciBL = auxData.showEstableciBL || false;
        this.disabledEstableciBL = auxData.disabledEstableciBL || false;
      } else {
        this.router.navigate([this.urlBandeja]);
      }
    } else {
      this.router.navigate([this.urlBandeja]);
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem(varLocalStor.BJA_SOL_EEVAL_CT_BL);
  }

  ngOnInit(): void {
  }

  //BOTONES
  tabSiguiente(): void {
    this.tabIndex = this.tabIndex + 1;
  }

  tabAnterior(): void {
    this.tabIndex = this.tabIndex - 1;
  }

  //FUNCIONES
  tabChange(event: any) {
    this.tabIndex = event;
  }


}
