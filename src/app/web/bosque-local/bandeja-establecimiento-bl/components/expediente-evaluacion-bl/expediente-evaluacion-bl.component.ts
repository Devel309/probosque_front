import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ArchivoService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudBosqueLocalService as SolBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';

@Component({
    selector: 'app-expediente-evaluacion-bl',
    templateUrl: './expediente-evaluacion-bl.component.html',
    styleUrls: ['./expediente-evaluacion-bl.component.scss']
  })

export class ExpedienteEvaluacionBlComponent implements OnInit {

 
 /*****Variables para control de bloqueo***************/
 @Input() idPlan: number = 0;
 @Input() disabledEnvioResultado: boolean = false;
 @Input() estadoSolicitud: string = "";
 @Input() codigoSeccion: string | null = null;
 /*****************************************************/
 totalRecords: number = 0;
 listaExpediente: any[] = [];
 optionsPage = {pageNum: 1, pageSize: 10};

 constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private solBosqueLocalService: SolBosqueLocalService,
  ) {  }

  ngOnInit(): void {
    this.listarAdjuntosResultadoEvaluacion();
  }

  /* ******* SERVICIOS ******* */
  listarAdjuntosResultadoEvaluacion() {
    this.listaExpediente = [];
   
    this.totalRecords = 0;
    const params = {
      idSolBosqueLocal:  this.idPlan, codigoSeccion: this.codigoSeccion
      , pageNum: this.optionsPage.pageNum
      , pageSize: this.optionsPage.pageSize
    };
    

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solBosqueLocalService.listarAdjuntosSolicitudBL(params).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success && resp.data && resp.data.length > 0) {
        this.listaExpediente = resp.data;
        this.totalRecords = resp.totalRecord || 0;
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  /* ******* BOTONES ******* */
  btnDescargarArchivoTabla(idArchivo: number) {
    if (idArchivo) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }
  
  /* ******* FUNCIONES ******* */
  loadData(event: any) {
    this.optionsPage.pageNum = event.first + 1;
    this.optionsPage.pageSize = event.rows;
    this.listarAdjuntosResultadoEvaluacion();
  }

  /* ******* GENERAL ******* */
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}