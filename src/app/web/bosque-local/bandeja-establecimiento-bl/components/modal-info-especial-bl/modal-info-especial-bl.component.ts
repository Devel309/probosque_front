import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MapApi, OgcGeometryType, ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import * as moment from "moment";
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudBosqueLocalGeometriaService } from 'src/app/service/solicitud/solicitud-bosque-local-geometria.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SolicitudBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';

@Component({
  selector: 'app-modal-info-especial-bl',
  templateUrl: './modal-info-especial-bl.component.html',
  styleUrls: ['./modal-info-especial-bl.component.scss']
})
export class ModalInfoEspecialBlComponent implements OnInit {
  requestEntity: any = {};
  isDisabled: boolean = false;
  coordinates: any = [];
  minDateInicio: any = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private toast: ToastService,
    private dialog: MatDialog,
    private apiForestalService: ApiForestalService,
    private mapApi: MapApi,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private solicitudBosqueLocalGeometriaService: SolicitudBosqueLocalGeometriaService,

  ) {
    this.requestEntity.idProcesoPostulacion = this.config.data?.idPlan;
    this.requestEntity.contra = this.config.data?.datos?.codigoTH;
    this.requestEntity.nrodoc = this.config.data?.datos?.numeroDocumentoSolicitante;
    this.requestEntity.nomtit = this.config.data?.datos?.nombreSolicitante;
    this.requestEntity.razonSocialTitular = this.config.data?.datos?.nombresRepresentante;
  }

  ngOnInit(): void {
    this.obtenerGeometria();
  }

  obtenerGeometria() {
    this.coordinates = [];
    const params = { idSolBosqueLocal: this.config.data.idPlan, tipoGeometria: "TPAREA" };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalGeometriaService.listarGeometria(params).subscribe((result: any) => {
      this.dialog.closeAll();
      let cont: number = 0;
      result.data.forEach((t: any) => {
        let geometryJson: any = this.mapApi.wktParse(t.geometry_wkt);
        if (geometryJson.type.toUpperCase() === OgcGeometryType.POLYGON && cont === 0) {
          cont++;
          this.coordinates = geometryJson.coordinates;
          
        }
      });
    }, () => this.dialog.closeAll())
  }

  insertDerechoForestal(paramsOut: any) {
    this.actualizarGeometriaServ(199);
    return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiForestalService.insertDerechoForestal(paramsOut).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.dataService != null) {
        if (response.dataService.status == "400") {
          this.toast.warn(response.dataService.message);
        }
        else {
          if (response.dataService.data.mensaje != null) {
            if (response.dataService.data.observacionesCampos != null && response.dataService.data.observacionesCampos.length > 0) {
              let mensaje = "";
              let obs = "";
              mensaje = response.dataService.data.mensaje;
              response.dataService.data.observacionesCampos.forEach((t: any) => {
                obs = obs + "\n" + t.campo + " : " + t.mensaje;
              });
              mensaje = mensaje + "\n" + obs;
              this.toast.warn(mensaje);
            }
            if (response.dataService.data.idsGeometrias != null && response.dataService.data.idsGeometrias.length > 0) {
              this.actualizarGeometriaServ(parseInt(response.dataService.data.idsGeometrias[0]));
            }
          }
        }
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarGeometriaServ(idGeom: number) {
    const params = {
      "idSolBosqueLocal": this.config.data.idPlan,
      "idGeometria": idGeom,
      "idUsuarioRegistro": this.config.data.idUser
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarSolicitudBosqueLocal(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Se registró la Geometría de la Solicitud Correctamente");
        this.ref.close(idGeom);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnEnviar() {
    const paramsIn = this.getParams();
    this.insertDerechoForestal(paramsIn);
  }

  getParams() {
    const params = {
      "codProceso": this.requestEntity.codProceso,
      "geometria": {
        "poligono": this.coordinates
      },
      "atributos": this.requestEntity,
    }
    return params;
  }

  btnClose() {
    this.ref.close();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
