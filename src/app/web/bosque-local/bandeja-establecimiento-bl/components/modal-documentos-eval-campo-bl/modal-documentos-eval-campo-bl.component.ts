import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'app-modal-documentos-eval-campo-bl',
  templateUrl: './modal-documentos-eval-campo-bl.component.html',
  styleUrls: ['./modal-documentos-eval-campo-bl.component.scss']
})
export class ModalDocumentosEvalCampoBlComponent implements OnInit {
  comboTiposDocumento: any[] = []
  requestModal = {
    idSolBosqueLocal: 0, idSolBosqueLocalArchivo: 0, idArchivo: 0, nombreArchivo: "",
    tipoArchivo: "", tipoDocumento: "", codigoSeccion: "", codigoSubSeccion: "", idUsuarioModificacion: 0
  };
  prefijoDoc = "";

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private genericoServ: GenericoService,
  ) {
    this.requestModal.idSolBosqueLocal = this.config.data?.idPlan;
    this.requestModal.idUsuarioModificacion = this.config.data?.idUser;
    this.requestModal.codigoSeccion = this.config.data?.codigoSeccion;
    this.requestModal.codigoSubSeccion = this.config.data?.codigoSubSeccion;
    this.prefijoDoc = this.config.data?.prefijoDoc;
  }

  ngOnInit(): void {
    this.listarComboTiposDocumentos();
  }

  //SERVICIOS
  listarComboTiposDocumentos() {
    const params: any = { prefijo: this.prefijoDoc };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.comboTiposDocumento = result.data;
        this.requestModal.tipoArchivo = this.comboTiposDocumento[0].codigo;
      }
    }, () => this.dialog.closeAll());
  }

  //BOTONES
  btnAgregar() {
    if (!this.validarAgregar()) return;
    this.requestModal.tipoDocumento = this.comboTiposDocumento.find(x => x.codigo === this.requestModal.tipoArchivo ).valorPrimario;
    this.ref.close(this.requestModal);
  }

  btnCancelar() {
    this.ref.close();
  }

  /******** GENERAL ***********/
  validarAgregar(): boolean {
    let valido = true;

    if (!this.requestModal.tipoArchivo) {
      valido = false;
      this.toast.warn("(*) Debe seleccionar un Tipo de Documento.");
    }

    if (!this.requestModal.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar Documento.");
    }

    return valido;
  }

  registrarArchivoFile(file: any, archivo: any) {
    archivo.idArchivo = file.idArchivo;
    archivo.nombreArchivo = file.nombre;
  }

  eliminarArchivo(ok: boolean, adjunto: any) {
    if (!ok) return;
    adjunto.idArchivo = 0;
    adjunto.nombreArchivo = "";
  }

}
