import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { EstudioTecnicoBlComponent } from '../../tabs/estudio-tecnico-bl/estudio-tecnico-bl.component';
@Component({
  selector: 'app-evaluar-comite-tecnico-bl',
  templateUrl: './evaluar-comite-tecnico-bl.component.html',
  styleUrls: ['./evaluar-comite-tecnico-bl.component.scss']
})
export class EvaluarComiteTecnicoBlComponent implements OnInit {
  @Input() idPlan: number = 0;
  @Input() codSeccion: string = "";
  @Input() codSubSeccion: string = "";
  @Input() idBosqueLocalEvaluacion: number =0;
  @Input() disabled: boolean = false;
  @Input() estadoSolicitud: string = "";
  usuario!: UsuarioModel;
  valor: String ="";
  id: number=0;
  requestEvaluar: any = {};
  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private estudioTecnicoBlComponent:EstudioTecnicoBlComponent
  ) {
    this.usuario = this.usuarioServ.usuario;

  }

  ngOnInit(): void {

    this.listarGabinete();
  }

  btnGuardar() {
    if (!this.validar()) return;
    if(this.valor ==this.codSubSeccion){

    let param = [{

      "idSolBosqueLocal": this.idPlan,
      "codigoSeccion": this.codSeccion,
      "codigoSubSeccion": this.codSubSeccion,
      "validado" :this.requestEvaluar.conforme,
      "observacion": this.requestEvaluar.observacion,
      "idUsuarioRegistro":this.usuario.idusuario,
     "idSolBosqueLocalEvaluacion":this.estudioTecnicoBlComponent.idBosqueLocalEvaluacion,
      "idSolBosqueLocalEvaluacionDetalle": this.id }];
       this.solicitudBosqueLocalService.actualizarGabinete(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Se Actualizo la evaluación  gabinete Correctamente.");

      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));

    }
    else{
      const param = [{
      "idSolBosqueLocal": this.idPlan,
      "codigoSeccion": this.codSeccion,
      "codigoSubSeccion": this.codSubSeccion,
      "validado" :this.requestEvaluar.conforme,
      "observacion": this.requestEvaluar.observacion,
      "idUsuarioRegistro":this.usuario.idusuario,
     "idSolBosqueLocalEvaluacion":this.estudioTecnicoBlComponent.idBosqueLocalEvaluacion,
      "idSolBosqueLocalEvaluacionDetalle":-1 }];
      this.solicitudBosqueLocalService.guardarGabinete(param).subscribe(resp => {
        this.dialog.closeAll();
        if (resp.success) {
          this.toast.ok("Se registro la evaluacion gabinete Correctamente");
        } else {
          this.toast.warn(resp.message);
        }
      }, (error) => this.errorMensaje(error, true));
  }
  }

  validar(): boolean {
    let validado = true;
    if (!(this.requestEvaluar.conforme === true || this.requestEvaluar.conforme === false)) {
      validado = false;
      this.toast.warn("(*) Debe de seleccionar una opción.");
    } else {
      if (this.requestEvaluar.conforme === false && !this.requestEvaluar.observacion) {
        validado = false;
        this.toast.warn("(*) Debe ingresar la observación.");
      }
      if (this.requestEvaluar.conforme === true) this.requestEvaluar.observacion = "";
    }
    return validado;
  }


  listarGabinete() {

    var params = { "idSolBosqueLocal": this.idPlan,
   "codigoSeccion": this.codSeccion,
   "codigoSubSeccion": this.codSubSeccion};

    this.solicitudBosqueLocalService.listarGabinete(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        if(resp.data && resp.data.length > 0) {
          this.requestEvaluar.conforme = resp.data[0].validado;
          this.requestEvaluar.observacion = resp.data[0].observacion;
          this.valor=resp.data[0].codigoSubSeccion;
          this.id=resp.data[0].idSolBosqueLocalEvaluacionDetalle;
        }
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }


  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
