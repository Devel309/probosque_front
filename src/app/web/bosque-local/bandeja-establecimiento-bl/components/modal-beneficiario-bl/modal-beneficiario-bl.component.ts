import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { ToastService, validarEmail } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ModelBeneficiarioBL } from 'src/app/model/bosque-local/ModelBeneficiarioBL';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { GenericoService } from 'src/app/service/generico.service';
import { PlantillaConst } from "src/app/shared/plantilla.const";
import { ArchivoTipo, DownloadFile } from "@shared";
@Component({
  selector: 'app-modal-beneficiario-bl',
  templateUrl: './modal-beneficiario-bl.component.html',
  styleUrls: ['./modal-beneficiario-bl.component.scss']
})
export class ModalBeneficiarioBlComponent implements OnInit {
  requestModal = new ModelBeneficiarioBL();
  comboCondicion: any[] = [];
  isValidarPide: boolean = false;
  adjuntoDJ: any = { tipoArchivo: "TDCODJBL", idArchivo: 0 };
  adjuntoResid: any = { tipoArchivo: "TDCORESIDBL", idArchivo: 0 };
  auxResidencia: string = "";
  plantillaConst =PlantillaConst;
  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private toast: ToastService,
    private dialog: MatDialog,
    private pideService: PideService,
    private genericoService: GenericoService,
    private archivoService: ArchivoService,
  ) {

    if (this.config.data?.isEdit) {
      this.isValidarPide = true;
      this.adjuntoResid.idArchivo = this.config.data.datos.idCertificado || 0;
      this.adjuntoDJ.idArchivo = this.config.data.datos.declaracionJurada || 0;
      this.requestModal.setData(this.config.data?.datos);
      this.auxResidencia = this.requestModal.residencia;
    }

    this.requestModal.idSolBosqueLocal = this.config.data.idPlan;
    this.requestModal.idUsuario = this.config.data?.idUser;
  }

  ngOnInit(): void {
    this.listarComboCondicion();
  }

  PARAM_CONDICION: { POB: string, EXT: string } = { POB: "TCONDPOB", EXT: "TCONDEXT" };
  //SERVICIOS
  listarComboCondicion() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "TCOND" }).subscribe((resp: any) => {
      if (resp.success) this.comboCondicion = resp.data;
    });
  }

  //BOTONES

  btnDescargarPlantillaDJ() {
    const NombreGenerado: string = this.plantillaConst.PlantillaDeclaracionJuradaPepequenoExtractor;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }
  btnValidarPIDE() {
    if (!this.requestModal.numeroDocumento) {
      this.toast.warn("(*) Debe agregar el DNI.");
      return;
    }
    const params = { "numDNIConsulta": this.requestModal.numeroDocumento }
    this.consultarDNI(params);
  }

  btnAgregar() {
    if (!this.validar()) return;
    this.requestModal.idCertificado = (this.requestModal.condicion === this.PARAM_CONDICION.POB && this.requestModal.flagActualizarResidencia) ? this.adjuntoResid.idArchivo : null;
    this.requestModal.declaracionJurada = this.requestModal.condicion === this.PARAM_CONDICION.EXT ? this.adjuntoDJ.idArchivo : null;
    this.requestModal.condicionDescripcion = this.comboCondicion.find(x => x.codigo === this.requestModal.condicion).valorPrimario || "";
    this.ref.close(this.requestModal);
  }

  registrarArchivo(id: number, archivo: any) {
    archivo.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, archivo: any) {
    if (!ok) return;
    archivo.idArchivo = 0;
  }


  //FUNCIONES
  validar(): boolean {
    let valido = true;

    if (!this.requestModal.condicion) {
      valido = false;
      this.toast.warn("(*) Debe seleccionar la condición.");
    } else {
      if (!this.requestModal.numeroDocumento) {
        valido = false;
        this.toast.warn("(*) Debe agregar el DNI.");
      } else if (!this.isValidarPide) {
        valido = false;
        this.toast.warn("(*) Debe validar el DNI en  RENIEC.");
      }

      if (this.requestModal.condicion === this.PARAM_CONDICION.POB) {
        if (this.requestModal.flagActualizarResidencia && !this.requestModal.residencia) {
          valido = false;
          this.toast.warn("(*) Debe agregar la residencia.");
        }
        if (this.requestModal.flagActualizarResidencia && !this.adjuntoResid.idArchivo) {
          valido = false;
          this.toast.warn("(*) Debe adjuntar Certificado de Residencia.");
        }
      } else if (this.requestModal.condicion === this.PARAM_CONDICION.EXT) {
        if (!this.adjuntoDJ.idArchivo) {
          valido = false;
          this.toast.warn("(*) Debe adjuntar Declaración Jurada.");
        }
      }

      if (!this.requestModal.telefono) {
        valido = false;
        this.toast.warn("(*) Debe agregar el teléfono.");
      }
      if (!this.requestModal.email) {
        valido = false;
        this.toast.warn("(*) Debe agregar el email.");
      } else if (!validarEmail(this.requestModal.email)) {
        valido = false;
        this.toast.warn("(*) Debe ingresar un email válido.");
      }
      if (!this.requestModal.cargaFamiliar) {
        valido = false;
        this.toast.warn("(*) Debe agregar la carga familiar.");
      }
    }

    return valido;
  }

  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  consultarDNI(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {
        if (result.dataService.datosPersona) {
          let persona = result.dataService.datosPersona;
          this.requestModal.nombreCompleto = `${persona.prenombres} ${persona.apPrimer} ${persona.apSegundo}`;
          this.requestModal.residencia = persona.direccion;
          this.requestModal.ubigeoTexto = persona.ubigeo;
          this.auxResidencia = persona.direccion;
          this.cambiarValidPIDE(true);
          this.toast.ok('Se validó el DNI en RENIEC.');
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(result.dataService.deResultado);
        }
      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
    }
    )

  }

  cambiarValidPIDE(valor: boolean) {
    this.isValidarPide = valor;
  }

  changeCondicion() {
    this.requestModal.flagActualizarResidencia = false;
    if (this.requestModal.condicion === this.PARAM_CONDICION.EXT) {
      this.requestModal.residencia = this.auxResidencia;
    }
  }

  changeActResid() {
    this.requestModal.residencia = this.auxResidencia;
  }

  changeNumDoc() {
    this.requestModal.nombreCompleto = "";
    this.requestModal.residencia = "";
    this.requestModal.ubigeoTexto = "";
    this.auxResidencia = "";
    this.cambiarValidPIDE(false);
  }

}
