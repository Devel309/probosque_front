import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';

@Component({
    selector: 'app-modal-expediente-completo-eval-bl',
    templateUrl: './modal-expediente-completo-eval-bl.component.html',
    styleUrls: ['./modal-expediente-completo-eval-bl.component.scss']
  })
  export class ModalExpedienteCompletoEvalBlComponent implements OnInit {
    idPlan: number = 0;
    disabledEnvioResultado: boolean = false;

    constructor(
        private dialog: MatDialog,
        private ref: DynamicDialogRef,
        private config: DynamicDialogConfig,
      ) {
       this.idPlan = this.config.data?.idPlan;
       this.disabledEnvioResultado = this.config.data?.disabledEnvioResultado;
      }
    
      ngOnInit(): void {
       
      }
  }