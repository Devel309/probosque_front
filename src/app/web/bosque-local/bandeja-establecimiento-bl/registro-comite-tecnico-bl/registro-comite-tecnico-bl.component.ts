import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { UsuarioBLModel } from 'src/app/model/bosque-local/UsuarioBLModel';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { SolicitudBosqueLocalComiteTecnicoService } from 'src/app/service/bosque-local/solicitud-bosque-local-comite-tecnico.service';
import { SolicitudBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';

@Component({
  selector: 'app-registro-comite-tecnico-bl',
  templateUrl: './registro-comite-tecnico-bl.component.html',
  styleUrls: ['./registro-comite-tecnico-bl.component.scss']
})
export class RegistroComiteTecnicoBlComponent implements OnInit {
  tabIndex: number = 0;
  urlBandeja = "/bosque-local/bandeja-establecimiento-BL";
  idPlan: number = 0;
  verModalUsuario: boolean = false;
  queryDNI: string = "";
  queryNombre: string = "";
  usuarios: any[] = [];
  selectUsuario: UsuarioBLModel = new UsuarioBLModel();
  usuario!: UsuarioModel;
  listaComite: UsuarioBLModel[]=[];
  isSelectUsuario:boolean = false
  isDisabled: boolean = false;
  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private router: Router,
    private activaRoute: ActivatedRoute,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private confirmationService: ConfirmationService,
    private usuarioServ: UsuarioService,
    private solicitudBosqueLocalComiteTecnicoService: SolicitudBosqueLocalComiteTecnicoService
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.idPlan = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    if (!this.idPlan) this.router.navigate([this.urlBandeja]);
  }

  ngOnInit(): void {
    this.isDisabled = localStorage.getItem("verComite")=='2'?true:false;

    
    this.listarComiteTecnico();
  }


  //SERVICIOS
  listarComiteTecnico() {
    this.listaComite = [];
    let params:any = {
      "idBosqueLocalComiteTecnicoPersona":null,
      "fechaPresentacion": null,
      "dni": null,
      "resultadoSolicitud": null,
      "idSolBosqueLocal": this.idPlan
    };

    this.solicitudBosqueLocalComiteTecnicoService.listarSolicitudBosqueLocalComiteTecnico(params).subscribe((result: any) => {

      
      this.isSelectUsuario= false
      result.data.forEach((data:any)=>{
        let comite: UsuarioBLModel = new UsuarioBLModel();

        comite.idComite = data.idSolBosqueLocalComiteTecnico;
        comite.idPersona = data.idPersona;
        comite.idSolBosqueLocal = data.idSolBosqueLocal;
        comite.numeroDocumento = data.numDocumento;
        comite.nombres =  data.persona;
        comite.departamento = data.departamento;
        comite.perfil = data.perfil;
        comite.emailUsuario = data.email;
        comite.telefono = data.telefono;
        comite.idDocAcreditacion =data.idDocAcreditacion;
        this.listaComite.push(comite);
      });

      this.listaComite.length

    });
  }


  listarUsuario() {

    let params:any = {};

    this.solicitudBosqueLocalService.listarUsuarioSolicitudBosqueLocal(params).subscribe((result: any) => {

      
      this.usuarios = [];

      let listaAux: any[] = [...result.data];


      this.listaComite.forEach((item) => {
        for (let index=0; index<listaAux.length; index++) {
          // Si el índice de ambos array coinciden se procederá
          // a eliminar el elemento de «datos»:
          if (listaAux[index].idPersona == item.idPersona) {
            listaAux.splice(index, 1);
          }
        }
      });


      listaAux.forEach((element: any) => {




      this.usuarios.push({

        idPersona: element.idPersona,
        idUsuario: element.idUsuario,
        nombres: element.nombresUsuario,
        perfil: element.perfilUsuario,
        //apellidoPaterno: element.apellidoPaternoUsuario,
        //apellidoMaterno: element.apellidoMaternoUsuario,
        telefono: element.telefonoUsuario,
        numeroDocumento: element.numeroDocumentoUsuario,
        emailUsuario: element.emailUsuario,
        departamento: element.nombreDepartamentoUsuario
      });



      });
    });
  }

  eliminarComiteTecnico(id:any, index:any){

    const param: any = {

      idSolBosqueLocalComiteTecnico: id,
      idUsuarioElimina: this.usuario.idusuario

    }

    this.solicitudBosqueLocalComiteTecnicoService.eliminarSolicitudBosqueLocalComiteTecnico(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {

          this.listaComite.splice(index,1)
          this.toast.ok("Se eliminó de registro de Comité Técnico Correctamente")
        } else {
          this.toast.warn(resp.message);
        }
    }, (error: HttpErrorResponse) => this.toast.error("Error al eliminar Comité Técnico"));


  }
  eliminarComiteTecnicoLocal( index:any){


          this.listaComite.splice(index,1)
          this.toast.ok("Se eliminó de registro de Comité Técnico Correctamente")


  }

  //BOTONES
  btnModalComite(fila: any, index: number) {
    this.abrirModalUsuario();
  }


  btnEliminarComite(event: any, id: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        //
        if(id!=0){
          this.eliminarComiteTecnico(id, index);
        }else{
          this.eliminarComiteTecnicoLocal( index);
        }

      },
    });
  }

  btnDescargarArchivoTabla(id: number) {

  }

  validarGuardar(): boolean {
    let valido = true;

    this.listaComite.forEach((l)=>{

      if(l.idDocAcreditacion ==0){
        valido = false;
      }

    })
    if(!valido){
      this.toast.warn("(*) Debe de adjuntar Documento Acreditación.");

    }
    return valido;
  }


  guardarListaComite(){
    let lista: any[]=[];
    
    this.listaComite.forEach((comite)=>{
      const param: any = {

        idSolBosqueLocalComiteTecnico: comite.idComite,
        idSolBosqueLocal: this.idPlan,
        idPersona: comite.idPersona,
        idDocumentoAcreditacion: comite.idDocAcreditacion,
        idUsuarioRegistro: this.usuario.idusuario

      }
      lista.push(param);

    });
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalComiteTecnicoService.registrarComiteTecnicoSolicitudBosqueLocal(lista)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {

        if (resp.success) {

          this.listarComiteTecnico();

          this.toast.ok("Se registró Comité Técnico Correctamente");
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.toast.error("Error al registrar Comité Técnico"));
  }

  btnGuardar() {

    if (!this.validarGuardar()) return;
    this.guardarListaComite();

  }

  btnEnviar(event: any) {

      //if(!this.validarEnviarMunicipioArffs()) return;
    let isValidadLista = false;
    this.listaComite.forEach((c)=>{
      if(c.idComite == 0){
        isValidadLista = true;
      }
    });

      this.confirmationService.confirm({
        target: event.target || undefined,
        message: '¿Está seguro de enviar el expediente al comité?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          if(isValidadLista){

            if (!this.validarGuardar()) return;

             this.guardarListaComite()
             this.actualizarEstado(CodigoEstadoBL.EVAL_EXPED_TEC);
          }else{
            if (!this.validarGuardar()) return;

             this.actualizarEstado(CodigoEstadoBL.EVAL_EXPED_TEC);
          }
        },
      });



  }

  actualizarEstado(estado: string) {
    const param: any = {
      idSolBosqueLocal: this.idPlan,
      estadoSolicitud: estado,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.actualizarEstadoSolicitudBosqueLocal(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(["/bosque-local/bandeja-establecimiento-BL"]);
      } else {
        this.toast.warn(resp.message);
      }
    },  );
  }


  btnRegresar() {
    this.router.navigate([this.urlBandeja]);
  }

  //FUNCIONES
  abrirModalUsuario() {
    this.listarUsuario();
    this.selectUsuario = new UsuarioBLModel();
    this.verModalUsuario = true;
    this.queryNombre = "";
    this.queryDNI = "";

    this.usuarios = [];
  }

  filtrarUsuario() {

    if (this.queryNombre && this.queryDNI) {

      let aux: any[] =this.usuarios.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryDNI.toLowerCase()));


      this.usuarios = aux.filter((r) =>
        r.nombres
          .toLowerCase()
          .includes(this.queryNombre.toLowerCase())
      );
    }else if(this.queryDNI) {
      this.usuarios = this.usuarios.filter((r) =>
        String(r.numeroDocumento).toLocaleLowerCase()

          .includes(this.queryDNI.toLocaleLowerCase())
      );
    }else if(this.queryNombre) {
      this.usuarios = this.usuarios.filter((r) =>
        r.nombres
          .toLowerCase()
          .includes(this.queryNombre.toLowerCase())
      );
    }

    else {
      this.listarUsuario();
    }
    this.queryDNI = '';
    this.queryNombre = '';
  }
  i:number = 0;
  guardarUsuario() {


    

    if (this.selectUsuario.idPersona === 0){
      this.toast.warn("(*) Debe de seleccionar un usuario para agregar.");
      return;
    }

    this.usuarios = [];
    let usuarioComite: any = new UsuarioBLModel();
    usuarioComite.idComite = 0;
    usuarioComite.idSolBosqueLocal = 0;
    usuarioComite.numeroDocumento= this.selectUsuario.numeroDocumento;
    usuarioComite.nombres= this.selectUsuario.nombres;
    usuarioComite.perfil= this.selectUsuario.perfil;
    usuarioComite.departamento= this.selectUsuario.departamento;
    usuarioComite.emailUsuario= this.selectUsuario.emailUsuario;
    usuarioComite.telefono= this.selectUsuario.telefono;
    usuarioComite.idDocAcreditacion = 0;
    usuarioComite.idPersona = this.selectUsuario.idPersona;




    this.listaComite.push(usuarioComite);

    this.verModalUsuario = false;

  }


  registrarArchivo(archivo:any, comite:any){
    comite.idDocAcreditacion = archivo;

  }
  cancelarModal(){

    this.verModalUsuario = false
    this.usuarios = [];
  }

  tabChange(event: any) {
    this.tabIndex = event;
  }

  tabSiguiente(): void {
    this.tabIndex = this.tabIndex + 1;
  }

  tabAnterior(): void {
    this.tabIndex = this.tabIndex - 1;
  }
}
