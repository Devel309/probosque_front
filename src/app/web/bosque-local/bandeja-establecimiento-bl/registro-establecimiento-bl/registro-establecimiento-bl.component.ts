import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { varLocalStor } from 'src/app/shared/util-local-storage';

@Component({
  selector: 'app-registro-establecimiento-bl',
  templateUrl: './registro-establecimiento-bl.component.html',
  styleUrls: ['./registro-establecimiento-bl.component.scss']
})
export class RegistroEstablecimientoBlComponent implements OnInit, OnDestroy {
  tabIndex: number = 0;
  urlBandeja = "/bosque-local/bandeja-establecimiento-BL";
  idPlan: number = 0;
  isDisbledFormu: boolean = false;
  showIniciativa: boolean = true;
  disabledEval: boolean = false;
  showEval: boolean = false;
  showEstudioTec: boolean = false;
  disabledEstudioTec: boolean = false;
  estadoSolicitud: string = "";
  evaluacionGabinete: boolean = false;

  codEstado = CodigoEstadoBL;

  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
  ) {
    this.idPlan = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    if (!this.idPlan) this.router.navigate([this.urlBandeja]);

    let auxLocalStoragedata = localStorage.getItem(varLocalStor.BJA_SOL_BL);
    if (auxLocalStoragedata) {
      let auxData = JSON.parse("" + auxLocalStoragedata);
      if (auxData.idSolicitud === this.idPlan) {
        this.isDisbledFormu = auxData.disabledForm || false;
        this.showIniciativa = auxData.showIniciativa || false;
        this.disabledEval = auxData.disabledEval || false;
        this.showEval = auxData.showEval || false;
        this.showEstudioTec = auxData.showEstudioTec || false;
        this.disabledEstudioTec = auxData.disabledEstudioTec || false;
        this.estadoSolicitud = auxData.estadoSolicitud || "";
        this.evaluacionGabinete = auxData.evaluacionGabinete || false;
      } else {
        this.router.navigate([this.urlBandeja]);
      }
    } else {
      this.router.navigate([this.urlBandeja]);
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem(varLocalStor.BJA_SOL_BL);
  }

  ngOnInit(): void {
  }

  //BOTONES
  tabSiguiente(): void {
    this.tabIndex = this.tabIndex + 1;
  }

  tabAnterior(): void {
    this.tabIndex = this.tabIndex - 1;
  }

  //FUNCIONES
  tabChange(event: any) {
    this.tabIndex = event;
  }

}
