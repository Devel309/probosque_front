import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ArchivoService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoBL } from 'src/app/model/bosque-local/CodigoEstadoBL';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { CodigosSOLBL } from 'src/app/model/util/SOLBL/SOLBL';
import { SolicitudBosqueLocalService } from 'src/app/service/bosque-local/solicitud-bosque-local.service';
import { GenericoService } from 'src/app/service/generico.service';
import { IrPlanService } from 'src/app/shared/services/ir-plan.service';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { ModalExpedienteCompletoEvalBlComponent } from './components/modal-expediente-completo-eval-bl/modal-expediente-completo-eval-bl.component';


@Component({
  selector: 'app-bandeja-establecimiento-bl',
  templateUrl: './bandeja-establecimiento-bl.component.html',
  styleUrls: ['./bandeja-establecimiento-bl.component.scss']
})
export class BandejaEstablecimientoBlComponent implements OnInit {
  usuario!: UsuarioModel;
  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  requestFiltro: any = {};
  comboEstado: any[] = [];
  listaBandeja: any = [];
  objbuscar: any = {};
  totalRecords: number = 0;

  codEstado = CodigoEstadoBL;
  isPerBeneficiario: boolean = false;
  isPerMunicipio: boolean = false;
  isPerArffs: boolean = false;
  isPerSerfor: boolean = false;
  isPerCompEstad: boolean = false;

  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private solicitudBosqueLocalService: SolicitudBosqueLocalService,
    private genericoService: GenericoService,
    private dialogService: DialogService,
    private irPlanService: IrPlanService,
    private archivoServ: ArchivoService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
    this.isPerBeneficiario = this.usuario.sirperfil === Perfiles.BENEFICIARIO;
    this.isPerMunicipio = this.usuario.sirperfil === Perfiles.MUNICIPIO;
    this.isPerArffs = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    this.isPerSerfor = this.usuario.sirperfil === Perfiles.SERFOR;
    this.isPerCompEstad = this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO;
  }

  ngOnInit(): void {
    this.listarComboEstados();
    this.setObjBuscar();
    this.listarBandeja();
  }


  //SERVICIOS
  listarBandeja() {
    this.listaBandeja = [];
    
    this.totalRecords = 0;
    const params = {
      idSolBosqueLocal: this.objbuscar.idSolicitud
      , nombreSolicitante: this.objbuscar.solicitante
      , numeroDocumentoSolicitante: this.objbuscar.documento
      , estadoSolicitud: this.objbuscar.estado
      , pageNum: this.objbuscar.pageNum
      , pageSize: this.objbuscar.pageSize
      , perfil: this.usuario.sirperfil
      , idUsuario: this.usuario.idusuario
      ,bandejaBosqueLocal: 1
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.listarSolicitudBosqueLocal(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaBandeja = resp.data;
        this.totalRecords = resp.totalRecord || 0;
        if (this.listaBandeja?.length === 0) this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  descargarListaBeneficiario(id: number, isPrevia: boolean) {
    const param = {
      idSolBosqueLocal: id,
      codigoSeccion: CodigosSOLBL.TAB_2,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.obtenerInfoAdjuntosSolicitudBL(param).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success && resp.data && resp.data.length > 0) {
        if (isPrevia) {
          let file: any[] = resp.data.filter((x: any) => x.tipoArchivo === CodigosSOLBL.TAB_2_3_A);
          if (file.length > 0) this.btnDescargarArchivoTabla(file[0].idArchivo);
          else this.toast.warn("No se encontró Lista Previa de Beneficiarios");
        } else {
          let file: any[] = resp.data.filter((x: any) => x.tipoArchivo === CodigosSOLBL.TAB_2_3_B);
          if (file.length > 0) this.btnDescargarArchivoTabla(file[0].idArchivo);
          else this.toast.warn("No se encontró Lista Final de Beneficiarios");
        }
      } else {
        if (isPrevia) {
          this.toast.warn("No se encontró Lista Previa de Beneficiarios");
        } else {
          this.toast.warn("No se encontró Lista Final de Beneficiarios");
        }
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnDescargarArchivoTabla(idArchivo: number) {
    if (idArchivo) {
      const params = { idArchivo: idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  // BOTONES
  btnVer(id: number, estado: string, disabledForm: boolean, disabledEval: boolean, showEval: boolean,
    showEstudioTec: boolean, disabledEstudioTec: boolean, showIniciativa: boolean) {
    this.irPlan(false, id, estado, disabledForm, disabledEval, showEval, showEstudioTec, disabledEstudioTec, showIniciativa, false);
  }

  btnNuevo() {
    const params = { "idSolicitante": this.usuario.idusuario }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudBosqueLocalService.registrarSolicitudBosqueLocal(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) this.irPlan(false, resp.codigo, resp.data.estadoSolicitud, false, true, false, false, true, true, true);
      else this.toast.warn(resp.message);
    }, (error) => this.errorMensaje(error, true));
  }

  btnBuscar() {
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.listarBandeja();
  }

  btnLimpiar() {
    this.setObjBuscar();
    this.listarBandeja();
  }

  btnDescargarArchivo(id: number, isPrevia: boolean) {
    this.descargarListaBeneficiario(id, isPrevia);
  }

  btnCrearComite(id: number) {
    this.router.navigate(["/bosque-local/registro-comite-tecnico-BL", id]);
    localStorage.setItem('verComite', "1");
  }
  btnVerComite(id: number) {
    this.router.navigate(["/bosque-local/registro-comite-tecnico-BL", id]);
    localStorage.setItem('verComite', "2");
  }

  btnEvalComite(id: number, estado: string, disabledEstudioTec: boolean, disabledEvalComiteTec: boolean,evaluacionGabinete: boolean , evaluacionCampo: boolean, 
    showEnvioResultado: boolean, disabledEnvioResultado: boolean,  showEstableciBL: boolean, disabledEstableciBL: boolean) {
    this.irEvalComite(false, id, estado, disabledEstudioTec, true, disabledEvalComiteTec, evaluacionGabinete, evaluacionCampo, 
      showEnvioResultado, disabledEnvioResultado, showEstableciBL, disabledEstableciBL);
  }

  btnVerExpediente(id: number) {

    const respRef = this.dialogService.open(ModalExpedienteCompletoEvalBlComponent, {
      header: 'Expedientes',
      width: '500px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        idPlan: id,
        disabledEnvioResultado: false
      },
      });
  
     respRef.onClose.subscribe((resp: any) => {
  
     });
  }

  // FUNCIONES
  irPlan(nuevaVentana: boolean, id: number, estado: string, disabledForm: boolean, disabledEval: boolean, showEval: boolean,
    showEstudioTec: boolean, disabledEstudioTec: boolean, showIniciativa: boolean, evaluacionGabinete: boolean) {
    this.irPlanService.irFormBosqueLocal_LS(nuevaVentana, id, estado, disabledForm, disabledEval, showEval, showEstudioTec, disabledEstudioTec, showIniciativa, evaluacionGabinete);
  }

  irEvalComite(nuevaVentana: boolean, id: number, estado: string, disabledEstudioTec: boolean,
    showEvalComiteTec: boolean, disabledEvalComiteTec: boolean, evaluacionGabinete: boolean, evaluacionCampo: boolean,
    showEnvioResultado: boolean, disabledEnvioResultado: boolean, showEstableciBL: boolean, disabledEstableciBL: boolean) {
    this.irPlanService.irEvalComiteTecnicoBosqueLocal_LS(nuevaVentana, id, estado, disabledEstudioTec, showEvalComiteTec, disabledEvalComiteTec,
      evaluacionGabinete, evaluacionCampo, showEnvioResultado, disabledEnvioResultado, showEstableciBL, disabledEstableciBL);
  }

  loadData(event: any) {
    this.objbuscar.pageNum = event.first + 1;
    this.objbuscar.pageSize = event.rows;
    this.listarBandeja();
  }

  setObjBuscar(): void {
    this.objbuscar = {
      idSolicitud: null,
      solicitante: null,
      documento: null,
      estado: null,
      pageNum: 1,
      pageSize: 10,
    };
  }

  listarComboEstados() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "SEBL" }).subscribe((resp: any) => {
      if (resp.success) this.comboEstado = resp.data;
    });
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
