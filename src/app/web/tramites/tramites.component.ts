import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tramites',
  templateUrl: './tramites.component.html',
  styleUrls: ['./tramites.component.scss']
})
export class TramitesComponent implements OnInit {

  estados:any[] = [];
  dataDemo:ModelDemo[]=[];

  constructor() { }

  ngOnInit(): void {

    for (let index = 0; index < 3; index++) {
      this.dataDemo.push({
        expediente:'SOL-000' +  index,
        solictante:'solictante' + index,
        tipoTramite:'Otorgamiento de titulo habilitante',
        descripcionTramite:'Permiso para aprovechamiento forestal maderable en predios privados',
        estado:'Presentado'
       });
      
    }

    this.estados = [
      {value:'0',text:'Todos'}
    ];

  }

}


export interface ModelDemo
{
  expediente:string;
  solictante:string;
  tipoTramite:string;
  descripcionTramite:string;
  estado:string;
}


