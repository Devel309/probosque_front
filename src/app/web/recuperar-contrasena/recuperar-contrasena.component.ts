import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';
import { AuthService } from 'src/app/service/auth.service';
import { ConfigService } from 'src/app/service/config.service';
@Component({
  selector: 'app-recuperar-contrasena',
  templateUrl: './recuperar-contrasena.component.html',
  styleUrls: ['./recuperar-contrasena.component.scss'],
  providers: [MessageService],
})
export class RecuperarContrasenaComponent implements OnInit {
  recuperar: FormGroup;
  // = {
  //   username: null,
  //   numeroDocumento: null,
  //   codigo: '0',
  //   token: '',
  //   clave: null,
  //   reclave: null,
  // };
  verSmsconfirmacion: boolean = true;
  verSeccionClave: boolean = false;
  verSeccionCodigo: boolean = false;
  btnShowverConfirmacion: boolean = false;
  btnShowverSeccionCodigo: boolean = false;
  hide: boolean = false;
  btnShowverSeccionClave = false;
  // md5 = new Md5();

  accionMantenimiento: string = '';

  constructor(
    public _configService: ConfigService,
    private _router: Router,
    private servAuth: AuthService,
    private messageService: MessageService,
    private fb: FormBuilder
  ) {
    this._configService.config = {
      useLayout: false,
    };

    this.recuperar = this.fb.group({
      username: [null],
      numeroDocumento: [null],
      codigo: ['0'],
      token: [''],
      clave: new FormControl(
        null,
        [
          Validators.required,
          Validators.minLength(8),
          ValidatorsExtend.passwordValid(),
          ValidatorsExtend.passwordValidReverseSame()
        ],
       
      ),
      reclave: [null],
    });
  }

  ngOnInit(): void {}

  enviarContrasenia() {
    
  }
  enviarContrasena(): void {
    // PedirCodigo
    

    const recuperar = this.recuperar.value;
    recuperar.codigo = '3';
    this.btnShowverSeccionClave = true;
    if (
      (recuperar.clave == '' && recuperar.clave == '') ||
      (recuperar.reclave == null && recuperar.reclave == null)
    ) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Ingrese Clave Nueva',
      });
      this.btnShowverConfirmacion = false;
    }
    if (recuperar.clave == recuperar.reclave) {
      // recuperar.clave = this.md5.appendStr(recuperar.clave).end().toString() ;
      //recuperar.clave = CryptoJS.MD5(recuperar.clave.trim()).toString();
      recuperar.clave = recuperar.clave.trim();

      this.servAuth.PedirCodigo(recuperar).subscribe(
        (result: any) => {
          // let succes=(result.success==true?"success":"warn");
          if (result.success) {
            // this.verSeccionCodigo = true;
            let succes = result.success == true ? 'success' : 'warn';

            this.messageService.add({
              severity: succes,
              summary: '',
              detail: result.message,
            });
            this._router.navigate(['/login']);
          } else {
            this.messageService.add({
              severity: 'warn',
              summary: '',
              detail: result.message,
            });
            this.btnShowverSeccionClave = false;
          }
        },
        (error: any) => {
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: error.error.message,
          });
          this.btnShowverSeccionClave = false;
        }
      );
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Las contraseñas no coinciden',
      });
      this.btnShowverSeccionClave = false;
    }
  }

  SolicitarCodigo() {
    const recuperar = this.recuperar.value;
    
    recuperar.codigo = '1';
    this.btnShowverConfirmacion = true;

    if (recuperar.username == '' || recuperar.username == null) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Ingrese Usuario',
      });
      this.btnShowverConfirmacion = false;
      return;
    }

    if (recuperar.numeroDocumento == '' || recuperar.numeroDocumento == null) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Ingrese Número Documento',
      });
      this.btnShowverConfirmacion = false;
      return;
    }

    if (recuperar.username.length < 7) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'El Usuario debe tener más de 7 caracteres',
      });
      this.btnShowverConfirmacion = false;
      return;
    }

    if (recuperar.numeroDocumento.length < 7) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'El Número Documento debe tener más de 7 caracteres',
      });
      this.btnShowverConfirmacion = false;
      return;
    }
    /*
    if( 
      (this.recuperar.username.length<7 ) &&   (this.recuperar.numeroDocumento.length<7 )
      ){
    this.messageService.add({ severity: "warn", summary: "", detail: "Ingrese Usuario y Número Documento"});
    this.btnShowverConfirmacion = false;
    return;
    
  }
*/

    this.servAuth.PedirCodigo(recuperar).subscribe(
      (result: any) => {
        if (result.success) {
          localStorage.setItem('username', recuperar.username);

          this.verSmsconfirmacion = false;
          this.verSeccionCodigo = true;
       
          let succes = result.success == true ? 'success' : 'warn';

          this.messageService.add({
            severity: succes,
            summary: '',
            detail: result.message,
          });
        } else {
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: result.message,
          });
          this.btnShowverConfirmacion = false;
        }
      },
      (error: any) => {
        this.btnShowverConfirmacion = false;
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.error.message,
        });
      }
    );
  }

  validaCodigo() {
    const recuperar = this.recuperar.value;
    recuperar.codigo = '2';
    this.btnShowverSeccionCodigo = true;
    this.servAuth.PedirCodigo(recuperar).subscribe(
      (result: any) => {
        // let succes=(result.success==true?"success":"warn");
        if (result.success) {
          this.verSeccionCodigo = false;
          this.verSeccionClave = true;
       
          let succes = result.success == true ? 'success' : 'warn';

          this.messageService.add({
            severity: succes,
            summary: '',
            detail: result.message,
          });
        } else {
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: result.message,
          });
          this.btnShowverSeccionCodigo = false;
        }
      },
      (error: any) => {
        this.btnShowverSeccionCodigo = false;
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.error.message,
        });
      }
    );
  }

  onConfirm() {
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }

  get clave() {
    
    
    return this.recuperar.get('clave');
  }
}
