import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MapApi, OgcGeometryType, ToastService } from '@shared';
import * as moment from "moment";
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigosTiposPersona } from 'src/app/model/util/CodigosTiposPerona';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { PermisoForestalGeometriaService } from 'src/app/service/planificacion/permiso-forestal/permiso-forestal-geometria.service';

@Component({
  selector: 'app-modal-info-espacial-eval',
  templateUrl: './modal-info-espacial-eval.component.html',
  styleUrls: ['./modal-info-espacial-eval.component.scss']
})
export class ModalInfoEspacialEvalComponent implements OnInit {
  requestEntity: any = {};
  isDisabled: boolean = false;
  isPersonaJuridica: boolean = false;
  coordinates: any = [];
  minDateInicio: any = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private toast: ToastService,
    private dialog: MatDialog,
    private mapApi: MapApi,
    private apiForestalService: ApiForestalService,
    private permisoForestalService: PermisoForestalService,
    private permisoForestalGeometriaService: PermisoForestalGeometriaService,
  ) {
    this.requestEntity.idProcesoPostulacion = this.config.data.item.idPermisoForestal;
    this.requestEntity.contra = this.config.data.item.codigoUnico;
    this.requestEntity.nomdep = this.config.data.item?.informacionGeneral?.idDepartamento;
    this.requestEntity.nompro = this.config.data.item?.informacionGeneral?.idProvincia;
    this.requestEntity.nomdis = this.config.data.item?.informacionGeneral?.idDistrito;

    this.requestEntity.codigoTipoPersonaTitular = this.config.data.item?.informacionGeneral?.codTipoPersona;
    this.requestEntity.numruc = this.config.data.item?.informacionGeneral?.rucComunidad;
    this.requestEntity.nrodoc = this.config.data.item?.informacionGeneral?.documentoElaborador;
    this.requestEntity.nomtit = this.config.data.item?.informacionGeneral?.federacionComunidad;
    this.requestEntity.razonSocialTitular = this.config.data.item?.informacionGeneral?.nombreElaborador + " " + this.config.data.item?.informacionGeneral?.apellidosElaborador;

    if (this.config.data.item?.informacionGeneral?.codTipoPersona === CodigosTiposPersona.JURIDICA) {
      this.isPersonaJuridica = true;
    }

    // this.isDisabled = this.config.data.item?.idGeometria || false;
  }

  ngOnInit(): void {
    this.obtenerSolPlantForGeometria();
    
  }

  //SERVICIOS
  obtenerSolPlantForGeometria() {
    this.coordinates = [];
    let params = { idPermisoForestal: this.config.data.item.idPermisoForestal };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalGeometriaService.listarPermisoForestalGeometria(params).subscribe((result: any) => {
      this.dialog.closeAll();
      let cont: number = 0;
      if(result.success && result.data) {
        result.data.forEach((t: any) => {
          let geometryJson: any = this.mapApi.wktParse(t.geometry_wkt);
          if (geometryJson.type.toUpperCase() === OgcGeometryType.POLYGON && cont === 0) {
            cont++;
            this.coordinates = geometryJson.coordinates;
          }
        });
      }
    }, (error) => this.errorMensaje(error, true));
  }

  insertDerechoForestal(paramsOut: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiForestalService.insertDerechoForestal(paramsOut).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.dataService != null) {

        if (response.dataService.status == "400") {
          this.toast.warn(response.dataService.message);
        } else {
          if (response.dataService.data.mensaje != null) {
            if (response.dataService.data.observacionesCampos != null && response.dataService.data.observacionesCampos.length > 0) {

              let mensaje = "";
              let obs = "";
              mensaje = response.dataService.data.mensaje;
              response.dataService.data.observacionesCampos.forEach((t: any) => {
                obs = obs + "\n" + t.campo + " : " + t.mensaje;
              });
              mensaje = mensaje + "\n" + obs;
              this.toast.warn(mensaje);
            }
            if (response.dataService.data.idsGeometrias != null && response.dataService.data.idsGeometrias.length > 0) {
              this.actualizarSolPlantForGeometria(parseInt(response.dataService.data.idsGeometrias[0]));
            }
          }
        }
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarSolPlantForGeometria(idOut: number) {
    const params = {
      "idPermisoForestal": this.config.data.item.idPermisoForestal,
      "idGeometria": idOut,
      "idUsuarioRegistro": this.config.data.idUser
    };
    

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarPermisoForestal(params).subscribe(result => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.ref.close(true);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnEnviar() {
    const paramsIn = this.getParams();
    this.insertDerechoForestal(paramsIn);
  }

  btnClose() {
    this.ref.close();
  }

  //FUNCIONES
  getParams() {
    const params = {
      "codProceso": this.requestEntity.codProceso,
      "geometria": {
        "poligono": this.coordinates
      },
      "atributos": this.requestEntity,
    }
    return params;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
