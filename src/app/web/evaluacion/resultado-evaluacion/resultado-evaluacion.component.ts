import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ResultadosEvaluacionDetalle } from 'src/app/model/resultadosEvaluacion';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { ResultadosEvaluacionService } from 'src/app/service/evaluacion/resultadosEvaluacion.service';

@Component({
  selector: 'resultado-evaluacion',
  templateUrl: './resultado-evaluacion.component.html',
  styleUrls: ['./resultado-evaluacion.component.scss'],
})
export class ResultadoEvaluacionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoPlan!: string;
  @Input() numeracion!: string;

  radioSelect: string = 'FAVO';
  classEstado: string = '';
  archivoRecurso: any = {};
  listDocumentos: any[] = [];
  perfiles: any[] = [];
  perfilesAgregados: any[] = [];
  perfilObj: any;

  listaDetalle: ResultadosEvaluacionDetalle[] = [];
  idEvalResultadoCarga: number = 0;
  idEvalResultadoEnvio: number = 0;
  datos: any;

  listaDetalleEnviar: ResultadosEvaluacionDetalle[] = [];

  favorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  desFavorableObj: ResultadosEvaluacionDetalle =
    new ResultadosEvaluacionDetalle();
  observadoObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();

  @Input() disabled!: boolean;
  constructor(
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private user: UsuarioService,
    private resultadosEvaluacionService: ResultadosEvaluacionService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit() {
    this.listTipoDocumento();
    this.listPerfiles();
    this.listadoCargaInforme();
    this.listadoEnviar();
  }

  listadoCargaInforme() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codResultado: this.codigoPlan,
      subCodResultado: 'ENINEVA',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoCarga = element.idEvalResultado;
              if (element.codResultadoDet == 'FAVO') {
                this.favorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == 'DESF') {
                this.desFavorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == 'OBSE') {
                this.observadoObj = new ResultadosEvaluacionDetalle(element);
              }
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listadoEnviar() {
    this.perfilesAgregados = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codResultado: this.codigoPlan,
      subCodResultado: 'ENIN',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoEnvio = element.idEvalResultado;
              this.filtrarPerfil(element);
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  filtrarPerfil(element: any) {
    this.datos = this.perfiles.filter((x) => x.codigo === element.perfil);
    this.datos.forEach((item: any) => {
      this.perfilesAgregados.push({
        ...item,
        idEvalResultadoDet: element.idEvalResultadoDet,
      });
    });
  }

  generarDescargarReporte() {
    const payload = {
      codigoProceso: this.codigoPlan,
      idPlanManejo: this.idPlanManejo,
    };
    this.evaluacionService
      .generarInformacionEvaluacion(payload)
      .subscribe((data: any) => {
        this.toast.ok('Se generó el Reporte de Evaluación correctamente.');

        DownloadFile(data.archivo, data.nombeArchivo, '');
      });
  }

  listTipoDocumento() {
    var params = {
      prefijo: 'TDRESULTEVAL',
    };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listDocumentos = [...response.data];
      });
  }

  listPerfiles() {
    var params = {
      prefijo: 'AUTSAN',
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.perfiles = [...response.data];
      });
  }

  onChange(event: any) {
    this.listaDetalle = [];
    var obj = new ResultadosEvaluacionDetalle(event.obj);
    obj.codResultadoDet = event.codResultadoDet;
    obj.idUsuarioRegistro = this.user.idUsuario;

    this.listaDetalle.push(obj);

    var params = [
      {
        idEvalResultado: this.idEvalResultadoCarga,
        idPlanManejo: this.idPlanManejo,
        codResultado: this.codigoPlan,
        subCodResultado: 'ENINEVA',
        descripcionCab: 'Carga de Informe de Evaluación',
        codInforme: 'radioSelect',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalle,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoCargaInforme();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  agregarPerfil() {

    let idPerfilAgregar = this.perfilObj.codigo;
    let encontrado:boolean = false;

    this.perfilesAgregados.forEach((item:any)=>{
      if(item.codigo == idPerfilAgregar){
        encontrado = true;
        return;
      }
    })

    if(!encontrado){
      this.perfilesAgregados.push({ ...this.perfilObj, idEvalResultadoDet: 0 });
    }else{
      this.toast.warn("El perfil " +this.perfilObj.valorPrimario+" ya se encuentra agregado.");
    }

  }

  guardarEnviar() {
    this.perfilesAgregados.forEach((item) => {
      var obj = new ResultadosEvaluacionDetalle();
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.perfil = item.codigo;
      obj.codResultadoDet = 'NOPER';
      obj.idEvalResultadoDet = item.idEvalResultadoDet
        ? item.idEvalResultadoDet
        : 0;
      this.listaDetalleEnviar.push(obj);
    });

    var params = [
      {
        idEvalResultado: this.idEvalResultadoEnvio,
        idPlanManejo: this.idPlanManejo,
        codResultado: this.codigoPlan,
        subCodResultado: 'ENIN',
        descripcionCab: 'Envio de información',
        codInforme: '',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalleEnviar,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoEnviar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openEliminarPerfil(event: Event, index: number, perfil: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (perfil.idEvalResultadoDet != 0) {
          var params = {
            idEvalResultado: this.idEvalResultadoEnvio,
            idEvalResultadoDet: perfil.idEvalResultadoDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.resultadosEvaluacionService
            .eliminarEvaluacionesultado(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.perfilesAgregados.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.perfilesAgregados.splice(index, 1);
        }
      },
    });
  }

  finalizar() {
    const body = {
      idPlanManejo: this.idPlanManejo,
      idUsuarioModificacion: this.user.idUsuario,
      codigoEstado: 'EEVALFIN',
    };
    this.evaluacionService.actualizarPlanManejo(body).subscribe(() => {
      this.toast.ok('Se ha Finalizado la evaluación');
      this.classEstado = 'req_aprobado_class';
    });
  }
}
