import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { finalize, tap } from 'rxjs/operators';

import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PlanManejoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { Page, PlanManejoMSG as MSG } from '@models';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { EvaluacionListarRequest } from '../../../model/EvaluacionListarRequest';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from '../../../model/util/CodigoEstadoPlanManejo';
import { Perfiles } from '../../../model/util/Perfiles';
import { EvaluacionPlanManejoCcnnService } from '../../../service/evaluacion/evaluacion-plan-manejo-forestal-ccnn/evaluacion-plan-manejo-ccnn.service';
import { CodigoEstadoEvaluacion } from '../../../model/util/CodigoEstadoEvaluacion';
import { CodigoProceso } from '../../../model/util/CodigoProceso';
import { EvaluacionService } from '../../../service/evaluacion/evaluacion.service';
import { CodigoPrerequisitos } from '../../../model/util/CodigoPrerequisitos';
import { CodigoMedidasCorrectivas } from '../../../model/util/CodigoMedidasCorrectivas';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'evaluacion-plan-operativo-ccnn',
  templateUrl: './evaluacion-plan-operativo-ccnn.component.html',
  styleUrls: ['./evaluacion-plan-operativo-ccnn.component.scss'],
})
export class EvaluacionPlanOperativoCcnnComponent implements OnInit {
  //f!: FormGroup;
  planes: any[] = [];
  usuario!: UsuarioModel;
  isPerfilArffs: boolean = false;
  isPerfilMdp: boolean = false;

  tituloBandeja: string = '';

  evaluacionRequest: EvaluacionListarRequest;

  PLAN = {
    idTipoProceso: 3, //proceso PGMFA
    idTipoEscala: 2, //
    idTipoPlan: 9, // PLAN POAC
    idSolicitud: 1,
    descripcion: 'POAC',
  };

  codigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  codigoEstadoMesaPartes = CodigoEstadoMesaPartes;
  Perfiles = Perfiles;
  CodigoMedidasCorrectivas = CodigoMedidasCorrectivas;

  loading = false;
  totalRecords = 0;

  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  codigoPrerequisitos = CodigoPrerequisitos;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private apiEvaluacionPlanManejo: EvaluacionPlanManejoCcnnService,
    private user: UsuarioService,
    private toast: ToastService,
    private apiEvaluacionService: EvaluacionService,
    private activaRoute: ActivatedRoute,
    private confirmationService: ConfirmationService
  ) {
    //this.f = this.initForm();
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      //this.tituloBandeja = "Bandeja de Entrada Evaluación "+this.PLAN.descripcion;
      this.tituloBandeja =
        'Bandeja de Entrada Evaluación ' + CodigoProceso.PLAN_OPERATIVO_PO;
      this.isPerfilArffs = true;
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      //this.tituloBandeja = "Bandeja de Entrada Mesa de Partes "+this.PLAN.descripcion;
      this.tituloBandeja =
        'Bandeja de Entrada Mesa de Partes ' + CodigoProceso.PLAN_OPERATIVO_PO;
      this.isPerfilMdp = true;
    } else {
      //this.tituloBandeja = "Bandeja de Entrada Evaluación "+this.PLAN.descripcion;
      this.tituloBandeja =
        'Bandeja de Entrada Evaluación ' + CodigoProceso.PLAN_OPERATIVO_PO;
      this.isPerfilArffs = false;
      this.isPerfilMdp = false;
    }
    this.buscar();
  }

  /* initForm() {
    return this.fb.group({
      dniElaborador: [null],
      rucComunidad: [null],
      nombreElaborador: [null],
      idPlanManejo: [null],
    });
  }*/

  buscar() {
    this.listarPlanes().subscribe();
  }

  limpiar() {
    // this.f.reset();
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.buscar();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
  }

  nuevoPlan() {
    const body = { ...this.PLAN, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => this.navigate(res.data.idPlanManejo));
  }

  verRequisitosPrevios(idPlanManejo: number,tipoplan:any) {
    this.navigateRequisitosPrevios(idPlanManejo,tipoplan);
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo);
  }

  evaluarPlan(idPlanManejo: number, codigoEstado: string) {
    //if (codigoEstado === this.codigoEstadoMesaPartes.COMPLETADOMP) {
    //if (codigoEstado === CodigoMedidasCorrectivas.EVAL_COMPLETADO) { se comenta no solo cuando este completado requisitos sino siempre
    const body = {
      idPlanManejo: idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      estadoEvaluacion: CodigoEstadoEvaluacion.PRESENTADO,
      fechaEvaluacionInicial: new Date().toISOString(),
      fechaEvaluacionFinal: null,
      codigoEvaluacion: CodigoProceso.PLAN_OPERATIVO,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiEvaluacionPlanManejo
      .registrarEvaluacionPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => {
        const uri = 'planificacion/plan-operativo-ccnn-ealta';
        this.router.navigate([uri, idPlanManejo]);
      });
    // }
  }

  listarPlanes(page?: Page) {
    //const r = { ...this.PGMFA, ...this.f.value }
    if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.evaluacionRequest.codigoEstado = 'EEVAEVAL,EMDCOMP,EPLMIMP,EPLMPROC,EPLMNOPROC,EPLMFAVO,EPLMDESFAVO,EEVAOBS,EPLMAPROB,EPLMBORD,EPLMBORN';
      /*this.evaluacionRequest.codigoEstado =
        CodigoEstadoMesaPartes.COMPLETADOMP +
        ',' +
        CodigoPrerequisitos.EVAL_PRESENTADO +
        ',' +
        CodigoPrerequisitos.EVAL_NO_PRESENTADO +
        ',' +
        CodigoPrerequisitos.EVAL_OBSERVADO +
        ',' +
        CodigoPrerequisitos.EVAL_EN_EVALUACION +
        ',' +
        CodigoPrerequisitos.EVAL_COMPLETADO +
        ',' +
        CodigoMedidasCorrectivas.EVAL_PRESENTADO +
        ',' +
        CodigoMedidasCorrectivas.EVAL_NO_PRESENTADO +
        ',' +
        CodigoMedidasCorrectivas.EVAL_OBSERVADO +
        ',' +
        CodigoMedidasCorrectivas.EVAL_EN_EVALUACION +
<<<<<<< HEAD
        "," +
        CodigoMedidasCorrectivas.EVAL_COMPLETADO+
        "," +
        CodigoEstadoEvaluacion.FINALIZADO;*/
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      this.evaluacionRequest.codigoEstado = 'EMDPRES,EMDOBS,EMDNPRE,EMDEVAL,EMDCOMP,EMDPRSU';
      /*CodigoEstadoMesaPartes.PRESENTADOMDP +
      "," +
      CodigoEstadoMesaPartes.COMPLETADOMP +
      "," +
      CodigoEstadoMesaPartes.OBSERVADOMP +
      "," +
      CodigoEstadoMesaPartes.NOPRESENTADOMP +
      "," +
      CodigoEstadoMesaPartes.PRESENTADOSUBSANADOMP;

        ',' +
        CodigoMedidasCorrectivas.EVAL_COMPLETADO +
        ',' +
        CodigoEstadoEvaluacion.FINALIZADO;*/
    }

    const r = { ...this.PLAN, ...this.evaluacionRequest };
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(tap((res) => (this.planes = res.data)));
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body).pipe(
      tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE),
      })
    );
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiEvaluacionService
      .filtrarEvaluacion(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  navigate(idPlan: number) {
    //const uri = 'planificacion/plan-general-manejo';
    const uri = 'planificacion/plan-operativo-ccnn-ealta';
    this.router.navigate([uri, idPlan]);
  }

  navigateRequisitosPrevios(idPlan: number, tipoPlan:any) {
    const uri = 'planificacion/evaluacion/requisitos-previos-poac';
    this.router.navigate([uri, idPlan,tipoPlan]);
  }

  verRequisitosMesaPartes(idPlanManejo: any, tipoPlan: any) {
    const uri = 'planificacion/evaluacion/requisitos-mesa-partes-poac';
    

    this.router.navigate([uri, idPlanManejo,tipoPlan]);
  }

  registroNotificacionEmit(event: any) {
    this.buscar();
  }

  remitePlan(event: any, idPlanManejo: any, codEstado: any) {
    if (
      codEstado == CodigoEstadoMesaPartes.EN_EVALUACION ||
      codEstado == CodigoEstadoPlanManejo.FAVORABLE ||
      codEstado == CodigoEstadoPlanManejo.DESFAVORABLE
    ) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message:
          '¿Está seguro de remitir la información a Componente Estadístico/OSINFOR?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          const params = {
            idPlanManejo: idPlanManejo,
            remitido: true,
            idUsuarioModificacion: this.usuario.idusuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.apiEvaluacionService.actualizarPlanManejo(params).subscribe(
            (result: any) => {
              this.dialog.closeAll();
              if (result.success) {
                this.toast.ok('Se remitió el Plan Manejo correctamente.');
                this.listarPlanes();
              } else {
                this.toast.warn(result.message);
              }
            },
            () => {
              this.toast.error(Mensajes.MSJ_ERROR_CATCH);
              this.dialog.closeAll();
            }
          );
        },
      });
    }
  }
}
