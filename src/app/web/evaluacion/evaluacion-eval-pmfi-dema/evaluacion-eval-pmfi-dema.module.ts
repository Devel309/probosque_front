import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEvalPmfiDemaModule } from '../../planificacion/bandeja-eval-pmfi-dema/bandeja-eval-pmfi-dema.module';
import { EvaluacionEvalPmfiDemaComponent } from './evaluacion-eval-pmfi-dema.component';



@NgModule({
  declarations: [EvaluacionEvalPmfiDemaComponent],
  exports: [EvaluacionEvalPmfiDemaComponent],
  imports: [
    CommonModule,
    BandejaEvalPmfiDemaModule
  ]
})
export class EvaluacionEvalPmfiDemaModule { }
