import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from '@models';
import { PlanManejoService, UsuarioService } from '@services';
import { isNullOrEmpty, ToastService } from '@shared';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { EvaluacionListarRequest } from 'src/app/model/EvaluacionListarRequest';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigoEstadoMesaPartes, CodigoEstadoPlanManejo } from 'src/app/model/util/CodigoEstadoPlanManejo';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { EvaluacionPlanManejoCcnnService } from 'src/app/service/evaluacion/evaluacion-plan-manejo-forestal-ccnn/evaluacion-plan-manejo-ccnn.service';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';

interface plan {
  idTipoProceso: any;
  idTipoPlan: any;
  codigoEstado: string;
  descripcion: string;
}
@Component({
  selector: 'app-evaluacion-eval-pmfi-dema',
  templateUrl: './evaluacion-eval-pmfi-dema.component.html',
  styleUrls: [ './evaluacion-eval-pmfi-dema.component.scss' ],
})
export class EvaluacionEvalPmfiDemaComponent implements OnInit {
  planes: any[] = [];
  usuario!: UsuarioModel;
  tituloBandeja: string = 'Bandeja Evaluación PMFI - DEMA';
  evaluacionRequest: EvaluacionListarRequest;

  PLAN: plan = {
    idTipoProceso: 3,
    idTipoPlan: null,
    codigoEstado: 'EPLMPRES,EPLMEEVAL',
    descripcion: 'DEMA',
  };
  loading = false;
  totalRecords = 0;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private apiEvaluacionPlanManejo: EvaluacionPlanManejoCcnnService,
    private user: UsuarioService,
    private toast: ToastService,
    private apiEvaluacionService: EvaluacionService,
    private confirmationService: ConfirmationService,
    private evaluacionService: EvaluacionService,
  ) {
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionListarRequest();
  }

  ngOnInit(): void {
    switch (this.usuario.sirperfil) {
      case Perfiles.AUTORIDAD_REGIONAL:
        this.setCodigoEstado('EEVAEVAL,EMDCOMP,EPLMIMP,EPLMPROC,EPLMNOPROC,EPLMFAVO,EPLMDESFAVO,EEVAOBS,EPLMAPROB,EPLMBORD,EPLMBORN');
        break;
      case Perfiles.MESA_DE_PARTES:
        this.setCodigoEstado('EMDPRES,EMDOBS,EMDNPRE,EMDEVAL,EMDCOMP,EMDPRSU');
        break;

      case Perfiles.COMPONENTE_ESTADISTICO:
        this.setCodigoEstado('EEVAEVAL,EMDCOMP,EPLMIMP,EPLMPROC,EPLMNOPROC,EPLMFAVO,EPLMDESFAVO,EEVAOBS,EPLMAPROB,EPLMBORD,EPLMBORN,EMDPRES,EMDOBS,EMDNPRE,EMDEVAL,EMDCOMP,EMDPRSU,EPLMPRES,EPLMEEVAL');
        break;
      case Perfiles.SERFOR:
        this.setCodigoEstado('EEVAEVAL,EMDCOMP,EPLMIMP,EPLMPROC,EPLMNOPROC,EPLMFAVO,EPLMDESFAVO,EEVAOBS,EPLMAPROB,EPLMBORD,EPLMBORN,EMDPRES,EMDOBS,EMDNPRE,EMDEVAL,EMDCOMP,EMDPRSU,EPLMPRES,EPLMEEVAL');
        break;
      default:
        break;
    }
  }

  setCodigoEstado(codigos: string) {
    this.PLAN = {
      idTipoProceso: 3,
      idTipoPlan: null,
      codigoEstado: codigos,
      descripcion: 'DEMA',
    };
  }

  nuevoPlan() { }

  buscar() {
    this.listarPlanes();
  }

  limpiar() {
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.buscar();
  }

  listarPlanes(page?: Page) {
    if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }

    if (!isNullOrEmpty(this.evaluacionRequest.filtroEstado)) {
      let cods = this.evaluacionRequest.filtroEstado ? this.evaluacionRequest.filtroEstado?.toString() : '';
      this.setCodigoEstado(cods);
    } else {
      this.ngOnInit();
    }

    this.apiEvaluacionService
      .listarPlanManejoEvaluacionPmfiDema(
        this.PLAN,
        this.evaluacionRequest,
        page
      )
      .subscribe((res: any) => {
        this.planes = res.data;
        this.totalRecords = res.totalRecords;
      });
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page);
  }

  evaluarPlan(data: any) {
    let url;
    if (data.tipoPlan == 'DEMA') {
      url = 'planificacion/generar-declaracion-manejo-dema';
    } else if (data.tipoPlan == 'PMFI') {
      url = 'planificacion/formulacion-PMFI';
    } else {
      url = 'planificacion/plan-manejo-forestal-intermedio';
    }
    this.router.navigate([ url, data.idPlanManejo ]);
  }

  remitirPlan(data: any) {
    let event = data.event;
    let idPlanManejo = data.idPlanManejo;
    let codEstado = data.codEstado;
    if (
      codEstado == CodigoEstadoMesaPartes.EN_EVALUACION ||
      codEstado == CodigoEstadoPlanManejo.FAVORABLE ||
      codEstado == CodigoEstadoPlanManejo.DESFAVORABLE
    ) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: '¿Está seguro de remitir la información a Componente Estadístico/OSINFOR?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          const params = {
            idPlanManejo: idPlanManejo,
            remitido: true,
            idUsuarioModificacion: this.usuario.idusuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.evaluacionService
            .actualizarPlanManejo(params)
            .subscribe(
              (result: any) => {
                this.dialog.closeAll();
                if (result.success) {
                  // this.toast.ok(result.message);
                  this.toast.ok('Se remitió el Plan Manejo correctamente.');
                  this.listarPlanes();
                } else {
                  this.toast.warn(result.message);
                }
              },
              () => {
                this.toast.error(Mensajes.MSJ_ERROR_CATCH);
                this.dialog.closeAll();
              }
            );
        },
      });
    }
  }

  verPlan(data: any) {
    let url;
    if (data.tipoPlan == 'DEMA') {
      url = 'planificacion/generar-declaracion-manejo-dema';
    } else {
      url = 'planificacion/formulacion-PMFI';
    }
    this.router.navigate([ url, data.idPlanManejo ]);
  }

  verRequisitosPrevios(payload: any) {
    const uri = 'planificacion/evaluacion/requisitos-previos';

    this.router.navigate([ uri, payload.idPlanManejo, payload.tipoPlan ]);
  }

  verRequisitosMesaPartes(payload: any) {
    const uri = 'planificacion/evaluacion/requisitos-mesa-partes';

    this.router.navigate([ uri, payload.idPlanManejo, payload.tipoPlan ]);
  }
}
