import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import * as moment from "moment";
import { EvaluacionService } from "../../../service/evaluacion/evaluacion.service";
import { LoadingComponent } from "../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { AnexosService } from "../../../service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { ArchivoService, UsuarioService } from "@services";
import { MatDialog } from "@angular/material/dialog";
import { DowloadFileLocal, DownloadFile, ToastService } from "@shared";
import { CodigoProceso } from "../../../model/util/CodigoProceso";
import { CodigosTabEvaluacion } from "../../../model/util/CodigosTabEvaluacion";
import { EvaluacionArchivoModel } from "../../../model/Comun/EvaluacionArchivoModel";
import { CodigoEstadoEvaluacion } from "../../../model/util/CodigoEstadoEvaluacion";
import { ConfirmationService } from "primeng/api";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "evaluacion-campo",
  templateUrl: "./evaluacion-campo.component.html",
  styleUrls: ["./evaluacion-campo.component.scss"],
})
export class EvaluacionCampoComponent implements OnInit {
  @Input() idPlanManejo!: number;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  minDateInicio: any = moment(new Date()).format("YYYY-MM-DD");
  maxDateInicio: any;
  minDateFin: any = moment(new Date()).format("YYYY-MM-DD");
  fechaInicio: any;
  fechaFin: any;
  archivoAdjunto: any = {};
  //evaluacionInnerModel:LineamientoInnerModel = new LineamientoInnerModel();
  verModalMantenimiento: boolean = false;
  selectedIsnpector: any = null;

  listInspectores: Inspector[] = [];
  listInspectoresBuscar: Inspector[] = [];
  listDocumentos: Documento[] = [];

  listAdjuntosDocumentos: any[] = [];
  paramBuscar: string = "";

  indexEdit: number = -1;

  inspector: Inspector = new Inspector();
  idTipoDocumento: string = "";

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_18;

  codigoAcordeon: string = CodigosTabEvaluacion.PGMFA_TAB_18_1;
  codigoAcordeonInspector: string = CodigosTabEvaluacion.PGMFA_TAB_18_2;

  evaluacionInspector: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeonInspector,
  });

  evaluacionAcordeon: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  constructor(
    private evaluacionService: EvaluacionService,
    private anexosService: AnexosService,
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService
  ) {}

  ngOnInit(): void {
    this.listarInspectoresDocumentos();

    this.obtenerEvaluacion();
  }

  selectFechaInicio(e: any): void {
    this.minDateFin = e.value;
  }

  selectFechaFin(e: any): void {
    this.maxDateInicio = e.value;
  }

  listarInspectoresDocumentos() {
    let paramPrefijo = {
      prefijo: "TDOCGE",
    };

    this.evaluacionService
      .listarParametroPorPrefijo(paramPrefijo)
      .subscribe((response: any) => {
        //console.log("Res ", response);
        if (response.data) {
          response.data.forEach((element: any) => {
            let obj: Documento = new Documento();
            obj.idTipoDocumento = element.valorSecundario + "";
            obj.descripcion = element.valorPrimario;
            this.listDocumentos.push(obj);
          });
        }
      });

    let paramPrefijoIns = {
      prefijo: "INSPECTOR",
    };

    this.evaluacionService
      .listarParametroPorPrefijo(paramPrefijoIns)
      .subscribe((response: any) => {
        //console.log("Res ", response);
        if (response.data) {
          response.data.forEach((element: any) => {
            let objIns: Inspector = new Inspector();
            //objIns = Object.assign(objIns, element);
            objIns.codigo = element.valorSecundario;
            objIns.valorPrimario = element.valorPrimario;
            this.listInspectores.push(objIns);
            this.listInspectoresBuscar.push(objIns);
          });
        }
      });
    /*this.evaluacionService.obtenerInspectorAndTipoDocumento(params).subscribe((response: any) => {

      if(response.data.listaParametro) {
        response.data.listaParametro.forEach((element: any) => {
            let objIns : Inspector = new Inspector();
            objIns = Object.assign(objIns, element);
            this.listInspectores.push(objIns);
            this.listInspectoresBuscar.push(objIns);
        });
      }

      if(response.data.listaDocumento) {
        response.data.listaDocumento.forEach((element: any) => {
          let obj : Documento = new Documento();
          obj.idTipoDocumento = element.idTipoDocumento+"";
          obj.descripcion = element.descripcion;
          //obj = Object.assign(obj, element);
          this.listDocumentos.push(obj);
        });
      }

    });*/
  }

  openModalInspector() {
    this.paramBuscar = "";
    this.listInspectoresBuscar = this.listInspectores;
    this.verModalMantenimiento = true;
  }

  seleccionarInspector() {
    
    this.inspector = this.listInspectores[this.selectedIsnpector];
    this.verModalMantenimiento = false;
  }

  buscarInspectores() {
    this.listInspectoresBuscar = this.listInspectores.filter(
      (eins: Inspector) => eins.valorPrimario?.includes(this.paramBuscar)
    );
  }

  subirArchivoEvaluacionCampo(file: any) {
    let item = {
      id: this.usuarioService.idUsuario,
      tipoDocumento: 50,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.anexosService
      .cargarAnexos(item.id, item.tipoDocumento, file.file)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.archivoAdjunto.idArchivo = result.data;
      });
  }

  agregarTipoDocumento() {
    if (!this.idTipoDocumento) {
      this.toast.warn("Debe seleccionar un tipo de documento");
      return;
    }

    if (!this.archivoAdjunto.idArchivo) {
      this.toast.warn("Debe cargar un documento");
      return;
    }

    if (this.indexEdit >= 0) {
      let doc = this.listDocumentos.find(
        (doc: Documento) => doc.idTipoDocumento == this.idTipoDocumento
      );
      if (doc) {
        this.listAdjuntosDocumentos[this.indexEdit].idTipoDocumento =
          doc.idTipoDocumento;
        this.listAdjuntosDocumentos[this.indexEdit].tipoDocumento =
          doc.descripcion;
        this.listAdjuntosDocumentos[
          this.indexEdit
        ].idArchivo = this.archivoAdjunto.idArchivo;
        this.listAdjuntosDocumentos[
          this.indexEdit
        ].nombreArchivo = this.archivoAdjunto.nombre;
      }
      this.indexEdit = -1;
      this.archivoAdjunto = {};
      this.idTipoDocumento = "";
    } else {
      let doc = this.listDocumentos.find(
        (doc: Documento) => doc.idTipoDocumento == this.idTipoDocumento
      );
      if (doc) {
        this.listAdjuntosDocumentos.push({
          idEvaluacionDet: 0,
          idTipoDocumento: doc.idTipoDocumento,
          tipoDocumento: doc.descripcion,
          idArchivo: this.archivoAdjunto.idArchivo,
          nombreArchivo: this.archivoAdjunto.nombre,
          codigoEvaluacionDet: this.codigoProceso,
          codigoEvaluacionDetSub: this.codigoTab,
          codigoEvaluacionDetPost: this.codigoTab + doc.idTipoDocumento,
        });

        this.archivoAdjunto = {};
        this.idTipoDocumento = "";
      }
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  eliminarTipoDocumento(event: any, data: any, rowIndex: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvaluacionDet == 0) {
          this.listAdjuntosDocumentos.splice(rowIndex, 1);
        } else {
          let array: any[] = [];
          let param = {
            idEvaluacionDet: data.idEvaluacionDet,
            idUsuarioElimina: this.usuarioService.idUsuario,
          };
          array.push(param);
          this.evaluacionService
            .eliminarEvaluacionDetalle(array)
            .subscribe((response: any) => {
              
              this.listAdjuntosDocumentos.splice(rowIndex, 1);
            });
        }
      },
      reject: () => {},
    });
  }

  editarTipoDocumento(event: any, data: any, rowIndex: number) {
    this.indexEdit = rowIndex;

    this.idTipoDocumento = data.idTipoDocumento;

    this.idTipoDocumento = "39";

    

    this.dialog.open(LoadingComponent, { disableClose: true });

    if (data.idArchivo) {
      let params = {
        idArchivo: data.idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            this.archivoAdjunto.archivo = result.data.archivo;
            this.archivoAdjunto.nombre = result.data.nombeArchivo;
            this.archivoAdjunto.idArchivo = data.idArchivo;
            //DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
          }
          (error: HttpErrorResponse) => {
            //this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.dialog.closeAll();
    }
  }

  obtenerEvaluacion() {
    this.listAdjuntosDocumentos = [];

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              // this.fechaInicio = this.evaluacion.fechaEvaluacionInicial;
              // this.fechaFin = this.evaluacion.fechaEvaluacionFinal;
              // this.inspector.codigo = this.evaluacion.descripcion;
              // this.inspector.valorPrimario = this.evaluacion.detalle;

              this.evaluacionAcordeon = Object.assign(
                this.evaluacionAcordeon,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon
                )
              );
              this.evaluacionInspector = Object.assign(
                this.evaluacionInspector,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeonInspector
                )
              );

              if (this.evaluacionInspector) {
                this.inspector.codigo = this.evaluacionInspector.detalle;
                this.inspector.valorPrimario = this.evaluacionInspector.descripcion;
                this.fechaInicio = this.evaluacionInspector.fechaEvaluacionDetInicial;
                this.fechaFin = this.evaluacionInspector.fechaEvaluacionDetFinal;
              }

              let listaAdjuntos = this.evaluacion.listarEvaluacionDetalle.filter(
                (x: any) =>
                  x.codigoEvaluacionDetPost != this.codigoAcordeon &&
                  x.codigoEvaluacionDetPost != this.codigoAcordeonInspector
              );
              listaAdjuntos.forEach((ele: any) => {
                let objeto = {
                  idEvaluacionDet: ele.idEvaluacionDet,
                  idTipoDocumento: ele.observacion,
                  tipoDocumento: ele.descripcion,
                  idArchivo: ele.idArchivo,
                  nombreArchivo: ele.detalle,
                  codigoEvaluacionDet: ele.codigoEvaluacionDet,
                  codigoEvaluacionDetSub: ele.codigoEvaluacionDetSub,
                  codigoEvaluacionDetPost: ele.codigoEvaluacionDetPost,
                };

                this.listAdjuntosDocumentos.push(objeto);
              });
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (this.inspector.valorPrimario == "") {
      this.toast.warn("Debe seleccionar un inspector");
      return;
    }

    if (this.fechaInicio == "") {
      this.toast.warn("Debe seleccionar una fecha Inicial");
      return;
    }

    if (this.fechaFin == "") {
      this.toast.warn("Debe seleccionar una fecha Final");
      return;
    }

    // if(this.evaluacion) {

    if (!this.evaluacion) {
      this.evaluacion.idEvaluacion = 0;
      this.evaluacion.idUsuarioRegistro = this.usuarioService.idUsuario;
      //this.evaluacion.estadoEvaluacion = CodigoEstadoEvaluacion.PRESENTADO;
      this.evaluacion.codigoEvaluacion = this.codigoProceso;
    }

    //this.evaluacion.fechaEvaluacionInicial  = this.fechaInicio;
    //this.evaluacion.fechaEvaluacionFinal    = this.fechaFin;
    // this.evaluacion.descripcion             = this.inspector.codigo ;
    //this.evaluacion.detalle                 = this.inspector.valorPrimario;

    this.evaluacion.listarEvaluacionDetalle = [];
    this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAcordeon);

    this.evaluacionInspector.fechaEvaluacionDetInicial = this.fechaInicio;
    this.evaluacionInspector.fechaEvaluacionDetFinal = this.fechaFin;
    this.evaluacionInspector.detalle = this.inspector.codigo;
    this.evaluacionInspector.descripcion = this.inspector.valorPrimario;

    this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionInspector);

    this.listAdjuntosDocumentos.forEach((ele: any) => {
      let objeto = {
        idEvaluacionDet: ele.idEvaluacionDet,
        observacion: ele.idTipoDocumento,
        descripcion: ele.tipoDocumento,
        idArchivo: ele.idArchivo,
        detalle: ele.nombreArchivo,
        codigoEvaluacionDet: ele.codigoEvaluacionDet,
        codigoEvaluacionDetSub: ele.codigoEvaluacionDetSub,
        codigoEvaluacionDetPost: ele.codigoEvaluacionDetPost,
      };
      this.evaluacion.listarEvaluacionDetalle.push(objeto);
    });

    this.evaluacion.listarEvaluacionDetalle[0].conforme;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .registrarEvaluacionPlanManejo(this.evaluacion)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success) {
          if (
            this.evaluacion.listarEvaluacionDetalle[0].conforme == "EEVARECI"
          ) {
            this.toast.ok("Se registró evaluación Conforme.");
            this.obtenerEvaluacion();
          } else {
            this.toast.ok(
              "Se registró evaluación, el cual se encuentra observado."
            );
          }
        } else {
          this.toast.error(res?.message);
        }
      });
    // }
  }
}

export class Inspector {
  constructor(data?: any) {
    if (data) {
      this.codigo = data.codigo;
      this.valorPrimario = data.valorPrimario;
    }
  }
  codigo: string | null = null;
  valorPrimario: string | null = null;
}

export class Documento {
  constructor(data?: any) {
    if (data) {
      this.idTipoDocumento = data.idTipoDocumento;
      this.descripcion = data.descripcion;
    }
  }
  idTipoDocumento: string | null = null;
  descripcion: string | null = null;
}
