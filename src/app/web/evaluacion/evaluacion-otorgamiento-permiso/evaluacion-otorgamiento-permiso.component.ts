import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { finalize, tap } from "rxjs/operators";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PlanManejoService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { Page, PlanManejoMSG as MSG } from "@models";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ConfirmationService, LazyLoadEvent } from "primeng/api";
import { EvaluacionListarRequest } from "../../../model/EvaluacionListarRequest";
import { UsuarioModel } from "../../../model/seguridad/usuario";
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from "../../../model/util/CodigoEstadoPlanManejo";
import { Perfiles } from "../../../model/util/Perfiles";
import { EvaluacionPlanManejoCcnnService } from "../../../service/evaluacion/evaluacion-plan-manejo-forestal-ccnn/evaluacion-plan-manejo-ccnn.service";
import { CodigoEstadoEvaluacion } from "../../../model/util/CodigoEstadoEvaluacion";
import { CodigoProceso } from "../../../model/util/CodigoProceso";
import { EvaluacionService } from "../../../service/evaluacion/evaluacion.service";
import { CodigoPrerequisitos } from "../../../model/util/CodigoPrerequisitos";
import { CodigoMedidasCorrectivas } from "../../../model/util/CodigoMedidasCorrectivas";
import {EvaluacionRequest} from '../../../model/PFCR/EvaluacionRequest';
import {CodigoPermisoForestal} from '../../../model/util/CodigoPermisoForestal';
import { Mensajes } from "src/app/model/util/Mensajes";
import { PermisoForestalService } from "src/app/service/permisoForestal.service";
import { DialogService } from "primeng/dynamicdialog";
import { ModalInfoEspacialEvalComponent } from "../modal-info-espacial-eval/modal-info-espacial-eval.component";

@Component({
  selector: "evaluacion-otorgamiento-permiso",
  templateUrl: "./evaluacion-otorgamiento-permiso.component.html",
  styleUrls: ["./evaluacion-otorgamiento-permiso.component.scss"],
})
export class EvaluacionOtorgamientoPermisoComponent implements OnInit {
  //f!: FormGroup;
  planes: any[] = [];
  usuario!: UsuarioModel;
  isPerfilArffs: boolean = false;
  isPerfilMdp: boolean = false;

  tituloBandeja :string = "";

  evaluacionRequest: EvaluacionRequest;
  Perfiles = Perfiles;
  PLAN = {
    idTipoProceso: 2, //
    idTipoEscala: 2, //
    idTipoPermiso: 1, //
    idSolicitud: 1,
    descripcion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
  };

  codigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  codigoEstadoMesaPartes = CodigoEstadoMesaPartes;
  codigoPrerequisitos = CodigoPrerequisitos;

  loading = false;
  totalRecords = 0;

  isPerfilOsinfor: boolean = false;
  isPerfilCompEstad: boolean = false;
  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    private apiEvaluacionPlanManejo: EvaluacionPlanManejoCcnnService,
    private apiEvaluacionService: EvaluacionService,
    private confirmationService: ConfirmationService,
    private permisoForestalService: PermisoForestalService,
    public dialogService: DialogService,
  ) {
    //this.f = this.initForm();
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionRequest();
    //this.evaluacionRequest.fecha = new Date().toDateString();
    //this.evaluacionRequest.fechaRegistro = new Date();
  }

  ngOnInit(): void {
    this.isPerfilOsinfor = this.usuario.sirperfil === Perfiles.OSINFOR;
    this.isPerfilCompEstad = this.usuario.sirperfil === Perfiles.SERFOR;

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.tituloBandeja = "Bandeja de Entrada de Evaluación "+CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
      this.isPerfilArffs = true;
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      this.tituloBandeja = "Bandeja de Entrada de Evaluación Mesa de Partes "+CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
      this.isPerfilMdp = true;
    } else {
      this.tituloBandeja = "Bandeja de Entrada de Evaluación "+CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
      this.isPerfilArffs = false;
      this.isPerfilMdp = false;
    }
    this.buscar();
  }

  buscar() {
    this.listarPlanes().subscribe();
  }

  limpiar() {
    // this.f.reset();
    this.evaluacionRequest = new EvaluacionRequest();
    //this.evaluacionRequest.fecha = new Date().toDateString();
    //this.evaluacionRequest.fechaRegistro = new Date();
    this.buscar();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
  }

  nuevoPlan() {
    const body = { ...this.PLAN, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => this.navigate(res.data.idPlanManejo));
  }

  verRequisitosPrevios(idPlanManejo: number) {
    this.navigateRequisitosPrevios(idPlanManejo);
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo);
  }

  listarPlanes(page?: Page) {
    /*if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }*/

    if (this.evaluacionRequest.filtroEstado != null && this.evaluacionRequest.filtroEstado != '') {
      this.evaluacionRequest.codigoEstado = this.evaluacionRequest.filtroEstado;
    } else {
      if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
        this.evaluacionRequest.codigoEstado =
          CodigoPermisoForestal.ESTADO_REGISTRADO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION +
          "," +
          CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE +
          "," +
          CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_PROCEDE +
          "," +
          CodigoPermisoForestal.ESTADO_IMPUGNADO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO +
          "," +
          CodigoPermisoForestal.ESTADO_EVAL_EMITIDO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_NOPROCEDE +
          "," +
          CodigoPermisoForestal.ESTADO_COMPLETADO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_PENDIENTE +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_VALIDADO;
      }else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
        this.evaluacionRequest.codigoEstado =
          CodigoPermisoForestal.ESTADO_REGISTRADO +
          "," +
          CodigoPermisoForestal.ESTADO_PRESENTADO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION +
          "," +
          CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE +
          "," +
          CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_PROCEDE +
          "," +
          CodigoPermisoForestal.ESTADO_IMPUGNADO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO +
          "," +
          CodigoPermisoForestal.ESTADO_EVAL_EMITIDO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_NOPROCEDE +
          "," +
          CodigoPermisoForestal.ESTADO_COMPLETADO +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_PENDIENTE +
          "," +
          CodigoPermisoForestal.ESTADO_VAL_VALIDADO;
      }
    }
    /*

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.evaluacionRequest.codigoEstado =
        CodigoEstadoMesaPartes.COMPLETADOMP +
        "," +
        CodigoPrerequisitos.EVAL_PRESENTADO +
        "," +
        CodigoPrerequisitos.EVAL_NO_PRESENTADO +
        "," +
        CodigoPrerequisitos.EVAL_OBSERVADO +
        "," +
        CodigoPrerequisitos.EVAL_EN_EVALUACION +
        "," +
        CodigoPrerequisitos.EVAL_COMPLETADO;
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      this.evaluacionRequest.codigoEstado =
        CodigoEstadoMesaPartes.PRESENTADOMP +
        "," +
        CodigoEstadoMesaPartes.COMPLETADOMP +
        "," +
        CodigoEstadoMesaPartes.OBSERVADOMP +
        "," +
        CodigoEstadoMesaPartes.NOPRESENTADOMP +
        "," +
        CodigoEstadoMesaPartes.PRESENTADOSUBSANADOMP;
    }*/

    let fecha = null;
   if(this.evaluacionRequest.fechaRegistro){
      fecha = new Date(this.evaluacionRequest.fechaRegistro!);
   }


    //this.evaluacionRequest.fechaRegistro = fecha.toISOString();

    let param = {
      idPermisoForestal : this.evaluacionRequest.idPermisoForestal,
      fechaRegistro     : fecha?.toISOString(),
      codigoEstado      : this.evaluacionRequest.codigoEstado,
      codigoPerfil: (this.isPerfilOsinfor || this.isPerfilCompEstad) ? this.usuario.sirperfil : null ,
    }


    const r = { ...this.PLAN, ...param };
    //const r = { ...this.PLAN };

    this.loading = true;

    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(tap((res) => (
        this.planes = res.data
      )));
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body).pipe(
      tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE),
      })
    );
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiEvaluacionService
      .filtrarEvaluacionOtorgamientoPermiso(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  navigate(idPlan: number) {
    const uri = "planificacion/revision-permisos-forestales";
    this.router.navigate([uri, idPlan]);
  }

  navigateRequisitosPrevios(idPlan: number) {
    const uri = "requisitos-preliminares/gestion-evaluacion";
    this.router.navigate([uri, idPlan]);
  }

  validarPlan(idPlanManejo: number, codigoEstado: string) {

    //console.log("PROCEDE ", codigoEstado);

    if (codigoEstado == CodigoPermisoForestal.ESTADO_VAL_PROCEDE || codigoEstado == CodigoPermisoForestal.ESTADO_PRESENTADO) {
      const uri = "planificacion/revision-permisos-forestales";
      this.router.navigate([uri, idPlanManejo]);
    }

  }

  evaluarPlan(idPlanManejo: number, codigoEstado: string) {

    if ( this.usuario.sirperfil === Perfiles.OSINFOR || this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO ||
      codigoEstado == CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION ||
      codigoEstado == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE ||
      codigoEstado == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE
    ) {
      const uri = "planificacion/revision-permisos-forestales";
      this.router.navigate([uri, idPlanManejo]);
    }

  }

  verDetalle(idPermisoForestal: number, codigoEstado: string){
    if ( this.usuario.sirperfil === Perfiles.OSINFOR || this.usuario.sirperfil === Perfiles.SERFOR ||  this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO) {
      const uri = "planificacion/revision-permisos-forestales/";
      this.router.navigate([uri, idPermisoForestal]);
    }
  }

  remitirPlan(event: any, idPermisoForestal: number, codigoEstado: string) {
    if (codigoEstado == CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION ||
        codigoEstado == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE ||
        codigoEstado == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE
    ) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: '¿Está seguro de remitir la información a Componente Estadístico/OSINFOR?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          const params = {
            "idPermisoForestal": idPermisoForestal,
            "remitido": true,
            "idUsuarioModificacion": this.usuario.idusuario
          }
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.permisoForestalService.permisoForestalActualizarRemitido(params).subscribe((result: any) => {
            this.dialog.closeAll();
            if (result.success) {
              this.toast.ok(result.message);
              this.listarPlanes().subscribe();
            } else {
              this.toast.warn(result.message);
            }
          }, () => {
            this.toast.error(Mensajes.MSJ_ERROR_CATCH);
            this.dialog.closeAll();
          });
        },
      });
    }
  }

  btnEviarInfoEspacial(fila: any) {
    const refResp = this.dialogService.open(ModalInfoEspacialEvalComponent, {
      header: 'Enviar Información Espacial',
      width: '900px', style: { margin: '15px' }, contentStyle: { overflow: 'auto' },
      data: { item: { ...fila }, idUser: this.usuario.idusuario },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.buscar();
    });
  }
}
