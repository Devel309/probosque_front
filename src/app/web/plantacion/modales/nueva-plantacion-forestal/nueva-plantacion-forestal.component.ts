import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { GenericoService } from 'src/app/service/generico.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';

@Component({
  selector: 'app-nueva-plantacion-forestal',
  templateUrl: './nueva-plantacion-forestal.component.html',
  styleUrls: ['./nueva-plantacion-forestal.component.scss']
})
export class NuevaPlantacionForestalComponent implements OnInit {
  idUsuario: number;
  comboTipoMod = [];
  modalidad: string | null = null;

  listaContratos: any[] = [];
  selectRadio: any;

  TIPOMOD = { predio: "TMPFPP", th: "TMPFTH", dominio: "TMPFDP" };
  totalRecords: number = 0;
  queryHabilitante: string = "";
  objbuscar = {
    pageNum: 1,
    pageSize: 10,
  };

  constructor(
    public dialog: MatDialog,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService,
    private genericoService: GenericoService,
    private solPlantForservice: SolPlantacionForestalService,
    private censoForestal: CensoForestalService,
  ) {
    this.idUsuario = this.config.data.idUser;
  }

  ngOnInit(): void {
    this.listarComboModalidad();
    this.listarContartos();
  }

  listarComboModalidad() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "TMPF" }).subscribe((resp: any) => {
      if (resp.success) this.comboTipoMod = resp.data;
    });
  }

  listarContartos() {
    this.listaContratos = [];
    this.totalRecords = 0;
    const params = {
      pageNum: this.objbuscar.pageNum,
      pageSize: this.objbuscar.pageSize,
      tipoProceso: 1,
      codigoTituloTh: this.queryHabilitante?(this.queryHabilitante.trim()!= ""? this.queryHabilitante.trim(): null):null,
      idUsuarioRegistro: this.idUsuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestal.listaTHContratoPorFiltro(params).subscribe((resp:any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaContratos = resp.data;
        this.totalRecords = resp.totalRecord;
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  guardar() {
    if (!this.modalidad) {
      this.toast.warn("(*) Debe seleccionar un tipo de modalidad.");
      return;
    };

    if (this.modalidad === this.TIPOMOD.th && !this.selectRadio) {
      this.toast.warn("(*) Debe seleccionar un contrato.");
      return;
    }

    let params = {}
    if (this.modalidad === this.TIPOMOD.th) {
      params = { codigoModalidad: this.modalidad, idUsuarioRegistro: this.idUsuario, idContrato: this.selectRadio.idContrato }
    } else {
      params = { codigoModalidad: this.modalidad, idUsuarioRegistro: this.idUsuario }
    }
      console.log(params);
   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantForservice.registrarSolPlantacacionForestal(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        let dataOut = { id: result.codigo, tipo: this.modalidad }
        this.ref.close(dataOut);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  cancelar() {
    this.ref.close();
  }

  filtrarContratoHabilitante() {
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.listarContartos();
  }

  limpiar(){
    this.queryHabilitante = '';
    this.filtrarContratoHabilitante();
  }
  
  loadData(event: any) {
    this.objbuscar.pageSize = Number(event.rows);
    this.objbuscar.pageNum = (Number(event.first) / this.objbuscar.pageSize) + 1;
    this.listarContartos();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
