import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoreCentralService } from '@services';
import { MessageService } from 'primeng/api';
import { SolicitudPlantacionForestalModel } from 'src/app/model/Solicitud';
import { DetalleDetallePlantacion1, DetalleDetallePlanteacion2model, DetalleVerticesModel, PlantacionForestalModel } from 'src/app/model/util/dataDemoMPAFPP';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { varLocalStor } from 'src/app/shared/util-local-storage';

@Component({
  selector: 'app-registro-plantacion-forestal',
  templateUrl: './registro-plantacion-forestal.component.html',
  styleUrls: ['./registro-plantacion-forestal.component.scss']
})
export class RegistroPlantacionForestalComponent implements OnInit, OnDestroy {
  idBPF: number = 0;
  isDisbledFormu: boolean = false;
  isDisbledEval: boolean = false;
  isShowEval: boolean = false;
  isTM_Predio: boolean = false;
  isTM_TH: boolean = false;
  isTM_Dominio: boolean = false;
  isSolicitudHija: boolean = false;
  isAnexo3 : boolean = false;

  urlBandeja = "/plantacion/bandeja-plantacion-forestal";
  usuario: any;
  idPlan: number = 0;
  tabIndex: number = 0;
  // codMedidaGeneral: string = "MPUMFGRAL";
  codigoProceso: string = "";
  // tipoProceso!: ICodigoGeneral;
  plantacion: PlantacionForestalModel = new PlantacionForestalModel();
  solicitud: SolicitudPlantacionForestalModel = new SolicitudPlantacionForestalModel();
  idPersona: any = null;
  ListaDepartamento = [];
  ListaDistritoInformacion = [];
  ListaProvinciaInformacion = [];
  ListaDistritoRepr = [];
  ListaProvinciaRepr = [];
  ListaDistritoArea = [];
  ListaProvinciaArea = [];

  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
    private servPf: PermisoForestalService,
    private messageService: MessageService,
    private core: CoreCentralService,
  ) {
    this.idBPF = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    // if (!this.idBPF) this.router.navigate([this.urlBandeja]);

    let auxLocalStorageBPFdata = localStorage.getItem(varLocalStor.BJA_PLANT_FORE);
    if (auxLocalStorageBPFdata) {
      let auxData = JSON.parse("" + auxLocalStorageBPFdata);
      if (auxData.BPFidSolicitud === this.idBPF) {
        this.isDisbledFormu = auxData.BPFdisabledForm || false;
        this.isDisbledEval = auxData.BPFdisabledEval || false;
        this.isShowEval = auxData.BPFshowEval || false;
        this.isTM_Predio = auxData.isTM_Predio || false;
        this.isTM_TH = auxData.isTM_TH || false;
        this.isTM_Dominio = auxData.isTM_Dominio || false;
        this.isSolicitudHija = auxData.isSolicitudHija || false;
        this.isAnexo3 = auxData.isAnexo3 || false;
      } else {
        this.router.navigate([this.urlBandeja]);
      }
    } else {
      this.router.navigate([this.urlBandeja]);
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem(varLocalStor.BJA_PLANT_FORE);
  }

  ngOnInit(): void {
  }

  tabChange(event: any) {
    this.tabIndex = event;
  }

  tabSiguiente(): void {
    this.tabIndex = this.tabIndex + 1;
  }

  tabAnterior(): void {
    this.tabIndex = this.tabIndex - 1;
  }

  cancelar(): void {
    this.router.navigate([this.urlBandeja]);
  }

  obtener(): void {
    let dto = { "idSolicitudPlantacion": this.idBPF };
    this.servPf.ObtenerPermisosForestales(dto).subscribe(
      (result: any) => {
        //console.log(result);
        if (result.isSuccess) {

          this.solicitud.solPlantacionForestal = result.data[0];
          if (this.solicitud.solPlantacionForestal.id_sol_persona < 1) {
            this.ObtenerPersona(this.solicitud.solPlantacionForestal.id_sol_persona, null, true);
          }

          //this.ObtenerPersona(this._data.solPlantacionForestal.id_sol_representante, null, false);
          this.listarInformacionUbicacion(1);
          this.listarInformacionUbicacion(2);

          this.obtenerArea(this.solicitud.solPlantacionForestal.id_solplantforest);
          this.listarAreaUbicacion();
          this.obtenerAreaPlantada(this.solicitud.solPlantacionForestal.id_solplantforest);
          this.obtenerDetallePlantacionForestal(this.solicitud.solPlantacionForestal.id_solplantforest);
          this.obtenerDetalleAreaCordenada(this.solicitud.solPlantacionForestal.id_solplantforest);
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }

  ObtenerPersona(persona: any, idUsuario: any, pbooelan: boolean) {
    let dto = { "idPersona": persona, "idUsuario": idUsuario };
    /**/
    this.servPf.listarPorFiltroPersona(dto).subscribe(
      (result: any) => {

        if (result.isSuccess) {
          if (pbooelan) {
            this.solicitud.solPlantacionForestal.persona = {
              ...result.data[0],
              nombres: (result.data[0].apellidoPaterno + " " + result.data[0].apellidoMaterno + " " + result.data[0].nombres)
            }
            this.idPersona = result.data[0].idPersona;
          } else {
            this.solicitud.solPlantacionForestal.persona.nombres = result.data[0].razon_social_empresa;
            this.solicitud.solPlantacionForestal.persona.email_empresa = result.data[0].email_empresa;
            this.solicitud.solPlantacionForestal.persona.direccion_empresa = result.data[0].direccion_empresa;
            this.solicitud.solPlantacionForestal.persona.razon_social_empresa = result.data[0].numero_ruc_empresa;
          }
          this.listarInformacionUbicacion(1);
          //this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }
  listarInformacionUbicacion(tipo: any) {
    // this.listarPorFiltroDepartamento();
    this.listarPorFilroProvincia(tipo);
    this.listarPorFilroDistrito(tipo);
  }
  listarPorFilroProvincia(tipo: any) {
    var provincia;
    if (tipo == 1) {
      provincia = {
        "idDepartamento": this.solicitud.solPlantacionForestal.persona.id_departamento
      };
    }
    if (tipo == 2) {
      provincia = {
        "idDepartamento": this.solicitud.solPlantacionForestal.persona.id_departamento_empresa
      };
    }

    if (
      this.solicitud.solPlantacionForestal.persona.id_departamento > 0
      || this.solicitud.solPlantacionForestal.persona.id_departamento_empresa > 0) {
      this.core.listarPorFilroProvincia(provincia).subscribe(
        (result: any) => {
          if (result.isSuccess) {
            if (tipo == 1) {
              this.ListaProvinciaInformacion = result.data;
            }
            else if (tipo == 2) {
              this.ListaProvinciaRepr = result.data;
            }
          }
        });
    }
  }

  listarPorFilroDistrito(tipo: any) {
    let provincia;
    if (tipo == 1) {
      provincia = {
        "idProvincia": this.solicitud.solPlantacionForestal.persona.id_provincia
      };
    }
    if (tipo == 2) {
      provincia = {
        "idProvincia": this.solicitud.solPlantacionForestal.persona.id_provincia_empresa
      };
    }
    if (this.solicitud.solPlantacionForestal.persona.id_departamento > 0
      || this.solicitud.solPlantacionForestal.persona.id_provincia_empresa > 0) {
      this.core.listarPorFilroDistrito(provincia).subscribe(
        (result: any) => {
          if (result.isSuccess) {


            if (tipo == 1) {
              this.ListaDistritoInformacion = result.data;
            }
            else if (tipo == 2) {
              this.ListaDistritoRepr = result.data;
            }
          }

        });
    }
  }

  listarAreaUbicacion() {
    this.listarPorFilroProvinciaArea();
    this.listarPorFilroDistritoArea();
  }

  listarPorFilroProvinciaArea() {
    let provincia = {
      "idDepartamento": this.solicitud.solPlantacionForestalArea.id_departamento
    };
    this.core.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
        if (result.isSuccess) {
          this.ListaProvinciaArea = result.data;
        }
      });

  }
  listarPorFilroDistritoArea() {
    let provincia = {
      "idProvincia": this.solicitud.solPlantacionForestalArea.id_provincia
    };
    this.core.listarPorFilroDistrito(provincia).subscribe(
      (result: any) => {
        if (result.isSuccess) {
          this.ListaDistritoArea = result.data;
        }
      });
  }

  obtenerArea(id_solplantforest: number) {

    let params = {
      id_solplantforest
    }
    this.servPf.ObtenerPermisosForestalesArea(params).subscribe((result: any) => {
      this.solicitud.solPlantacionForestalArea = result.data[0];
      this.listarPorFilroProvinciaArea();
      this.listarPorFilroDistritoArea();
      /*if (result.data) {
        this.data.areaPlantada.areaTotal = result.data[0].area_total_plantacion;
        this.data.areaPlantada.mesAnhio = result.data[0].mes_anio_plantacion;
        this.data.areaPlantada.lstDetalle = result.data;
      }*/
/**
 this.data.area.tipoNumero = (
            this._data.solPlantacionForestalArea.num_cesion_agroforestal == null ?
              this._data.solPlantacionForestalArea.num_concesion_agroforestal :
              this._data.solPlantacionForestalArea.num_cesion_agroforestal
          );
          this.data.area.tipo = (
            this._data.solPlantacionForestalArea.num_cesion_agroforestal == null ? "P" : "I"
          );
 */

    })
  }

  obtenerAreaPlantada(id_solplantforest: number) {
    let params = {
      id_solplantforest
    }
    this.servPf.obtenerSolicitudPlantacacionAreaPant(params).subscribe((result: any) => {
      if (result.data) {
        this.plantacion.areaPlantada.areaTotal = result.data[0].area_total_plantacion;
        this.plantacion.areaPlantada.mesAnhio = result.data[0].mes_anio_plantacion;
        this.plantacion.areaPlantada.lstDetalle = result.data;
      }
    })
  }

  obtenerDetallePlantacionForestal(id_solplantforest: number) {
    let params = {
      id_solplantforest
    }

    this.servPf.obtenerSolicitudPlantacacionDet(params).subscribe((result: any) => {
      var datav;

      var ar_det: DetalleDetallePlantacion1[] = [];
      result.data.forEach((element: any) => {
        datav = {} as DetalleDetallePlantacion1;

        datav.id_solplantforest_det = element.id_solplantforest_det;
        datav.espcs_nom_comun = element.espcs_nom_comun;
        datav.espcs_nom_cientifico = element.espcs_nom_cientifico;
        datav.total_arbol_matas_espcs_existentes = element.total_arbol_matas_espcs_existentes;
        datav.produccion_estimada = element.produccion_estimada;
        datav.coord_este = element.coord_este;
        datav.coord_norte = element.coord_norte;
        ar_det.push(datav);

      });
      this.plantacion.detalleAreaPlanteada1.lstDetalle = ar_det;

    })

  }

  obtenerDetalleAreaCordenada(id_solplantforest: number) {
    let params = {
      id_solplantforest
    }

    this.servPf.obtenerSolicitudPlantacacionDetCoord(params).subscribe((result: any) => {
      this.solicitud.solPlantacionForestalDetCoord = result.data;

      var datav = {} as DetalleVerticesModel;
      var datac = {} as DetalleDetallePlanteacion2model;
      var ar_coord: DetalleDetallePlanteacion2model[] = [];


      this.solicitud.solPlantacionForestalDetCoord.forEach(element => {
        datac = {} as DetalleDetallePlanteacion2model;

        datac.id_solplantforest_coord_det = element.id_solplantforest_coord_det;
        datac.areabloque = element.areabloque;
        datac.areabloque_unidad = element.areabloque_unidad;
        datac.bloquesector = element.bloquesector;
        datac.detalle = [];
        element.detalle.forEach(element => {
          datav = {} as DetalleVerticesModel;
          datav.id_bloquesector = element.id_bloquesector;
          datav.zonaeste = element.zonaeste;
          datav.zonanorte = element.zonanorte;
          datav.observaciones = element.observaciones;
          datav.vertice = element.vertice;
          datac.detalle.push(datav)
        })
        ar_coord.push(datac);
      })
      //console.log(ar_coord);

      this.plantacion.detalleAreaPlanteada2.lstDetalle = ar_coord;


    })
  }

  guardar(): void {
    let datoStr: any = localStorage.getItem('plantacionesForestales');
    let datos: any[] = [];
    if (!datoStr) {
      datos = [];
    } else {
      datos = JSON.parse(datoStr);
    }
    datos.push(this.plantacion);
    localStorage.setItem('plantacionesForestales', JSON.stringify(datos));

    this.solicitud.solPlantacionForestal.id_solplantforest = this.idBPF;
    if(this.idPersona!=null){
      this.solicitud.solPlantacionForestal.id_sol_persona = this.idPersona;
    }

    this.solicitud.solPlantacionForestal.idUsuarioRegistro = this.usuario.idusuario;
    this.solicitud.solPlantacionForestal.estado = "A";

    if( this.solicitud.solPlantacionForestal.persona.idTipoPersona==2
        &&( this.solicitud.solPlantacionForestal.persona.ruc==null
        || this.solicitud.solPlantacionForestal.persona.ruc =="")
      ){
      this.messageService.add({ severity: "warn", summary: "", detail: "Ingrese el Numero de R.U.C"});
      return;
    }


    this.servPf.RegistroSolicitudPermisosForestales(this.solicitud.solPlantacionForestal).subscribe(
      (result: any) => {
        //;
        
        if (result.isSuccess) {
          //capturar el result.codigo y enviarlo en cada metodo result.codigo => *id_solplantforest*

          // this.registrarSolicitudPlantacacionArea(result.codigo);
          // this.registrarSolicitudPlantacacionAreaPlant(result.codigo)
          // this.registrarSolicitudPlantacacionDet(result.codigo)
          // this.registrarSolicitudPlantacacionDetCoord(result.codigo);

          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          //this.ListarCronogramaActividades(this.objbuscar);
          // this.router.navigateByUrl('/planificacion/bandeja-plantacion-forestal');
        } else {
          this.messageService.add({ severity: "warn", summary: "1. Informacón del Solicitante", detail: result.message });
        }

      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "1. Informacón del Solicitante", detail: error.message });
      });

  }
}
