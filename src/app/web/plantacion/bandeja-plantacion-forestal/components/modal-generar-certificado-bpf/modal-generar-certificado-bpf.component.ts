import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';

@Component({
  selector: 'app-modal-generar-certificado-bpf',
  templateUrl: './modal-generar-certificado-bpf.component.html',
  styleUrls: ['./modal-generar-certificado-bpf.component.scss']
})
export class ModalGenerarCertificadoBpfComponent implements OnInit {
  disabledIN: boolean = false;
  idArchivoCertif: number = 0;
  tipodocArchivoCertif: string = "";
  @Input() idBPF!: number;
  certificado: string = "";
  numeroCerti: string = "";
 
  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private permisoForestalService: PermisoForestalService,
  ) {
    this.disabledIN = this.config.data.isDisabled;
    this.idBPF=this.config.data.idSolPlantacion;
  }

  ngOnInit(): void {
    this.listarSolicitudPlantacionForestal();
    this.obtenerInfoEvaluacionSolicitudPlantacionForestal();
  }

  //SERVICIOS

  registrarArchivo(id: number) {
    this.idArchivoCertif = id;
  }

  eliminarArchivo(ok: boolean) {
    if (ok) this.idArchivoCertif = 0;
  }

  //BOTONES
  btnGuardar() {

  }

  btnCancelar() {
    this.ref.close();
  }

  listarSolicitudPlantacionForestal() {

    let params: any = {
      idSolicitudPlantacionForestal : this.idBPF
    }

    this.permisoForestalService.listarSolicitudPlantacionForestal(params).subscribe((resp: any) => {
      if (resp.success) {
        this.certificado = resp.data[0].nroRegistro;
        
      }
      (error: HttpErrorResponse) => {
        this.toast.ok("ERROR");
      };
    });
  }
  obtenerInfoEvaluacionSolicitudPlantacionForestal() {
    const params: any = {
    idSolPlantacionForestal: this.idBPF
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInfoEvaluacionSolicitudPlantacionForestal(params).subscribe((result: any) => {
      this.dialog.closeAll();
      
      if (result?.success && result.validateBusiness !== false) {
        if (result.data && result.data.length > 0) {
          this.idArchivoCertif= result.data[0].idArchivoCertificado
          //console.log('obtenerInfoEvaluacionSolicitudPlantacionForestal',result.data[0])
        }
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  //FUNCIONES
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
