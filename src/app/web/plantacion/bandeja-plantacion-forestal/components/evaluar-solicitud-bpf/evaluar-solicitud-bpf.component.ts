import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from 'rxjs/operators';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ConsultaSolicitudesComponent } from 'src/app/web/planificacion/ampliacion-solicitudes/ampliacion-solicitudes.component';
import { FileModel } from 'src/app/model/util/File';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { ConfirmationService } from 'primeng/api';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';
import { TipoPlanManejo } from 'src/app/model/util/codigos/TipoPlanManejo';
import { varLocalStor } from 'src/app/shared/util-local-storage';
@Component({
  selector: 'app-evaluar-solicitud-bpf',
  templateUrl: './evaluar-solicitud-bpf.component.html',
  styleUrls: ['./evaluar-solicitud-bpf.component.scss'],
})
export class EvaluarSolicitudBpfComponent implements OnInit {
  @Input() disabled: boolean = false;
  @Input() idBPF: number = 0;
  @Input() codSeccion: string = '';
  @Input() codSubSeccion: string = '';
  @Input() isLayout2: boolean = false;
  @Input() codigoProceso!: string;
  @Input() isSuperposicion: boolean = false;
  @Input() isAparecer: boolean = false;
  @Input() isTM_Dominio: boolean = false;
  usuario!: UsuarioModel;
  requestEvaluar: any = {};
  _filesSHP: FileModel[] = [];
  public _layers: CustomCapaModel[] = [];

  isEvaluacionCampo: boolean = false;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private permisoForestalService: PermisoForestalService,
    private confirmationService: ConfirmationService,
    private user: UsuarioService,
    private evaluacionCampoService: EvaluacionCampoService
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.requestEvaluar.idSolPlantacionForestalEvaluacion = null;
    this.requestEvaluar.idSolPlantacionForestalEvaluacionDetalle = null;
    this.requestEvaluar.codigoSeccion = this.codSeccion;
    this.requestEvaluar.codigoSubSeccion = this.codSubSeccion;
    this.requestEvaluar.conforme = null;
    this.requestEvaluar.observacion = null;
    this.requestEvaluar.nuAlerta = 1;

    if (this.idBPF) this.obtenerInfoDetalle();
    this.isAparecer =
      JSON.parse('' + localStorage.getItem(varLocalStor.BJA_PLANT_FORE))
        .estadoOpcion == 'CERTIFICADO';
    this.obtenerInfoTitular();
  }

  //SERVICIOS
  obtenerInfoTitular() {
    let param = { idSolicitudPlantacionForestal: this.idBPF };

    this.permisoForestalService
      .obtenerInformacionTitular(param)
      .subscribe((resp) => {
        if (resp.success && resp.data && resp.data.length > 0) {
          let datos = resp.data[0];
          this.isEvaluacionCampo = !!datos.idEvaluacionCampo;
        }
      });
  }

  obtenerInfo() {
    const params = { idSolPlantacionForestal: this.idBPF };
    this.permisoForestalService
      .obtenerInfoEvaluacionSolicitudPlantacionForestal(params)
      .subscribe((resp) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.length > 0) {
          this.requestEvaluar.idSolPlantacionForestalEvaluacion =
            resp.data[0].idSolPlantacionForestalEvaluacion;
          this.listar();
        }
      });
  }

  obtenerInfoDetalle(load: boolean = false) {
    const params = {
      idSolPlantacionForestal: this.idBPF,
      codigoSeccion: this.requestEvaluar.codigoSeccion,
      codigoSubSeccion: this.requestEvaluar.codigoSubSeccion,
    };

    this.permisoForestalService
      .obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal(params)
      .subscribe(
        (resp) => {
          if (load) this.dialog.closeAll();
          if (resp.success && resp.data && resp.data.length > 0) {
            this.requestEvaluar.conforme = resp.data[0].conforme;
            this.requestEvaluar.observacion = resp.data[0].observacion;
            this.requestEvaluar.idSolPlantacionForestalEvaluacion =
              resp.data[0].idSolPlantacionForestalEvaluacion;
            this.requestEvaluar.idSolPlantacionForestalEvaluacionDetalle =
              resp.data[0].idSolPlantacionForestalEvaluacionDetalle;
          }
        },
        (error) => this.errorMensaje(error, load)
      );
  }

  listar(load: boolean = false) {
    const params = {
      idSolPlantacionForestalEvaluacion:
        this.requestEvaluar.idSolPlantacionForestalEvaluacion,
      codigoSeccion: this.requestEvaluar.codigoSeccion,
      codigoSubSeccion: this.requestEvaluar.codigoSubSeccion,
    };

    this.permisoForestalService
      .obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal(params)
      .subscribe(
        (resp) => {
          if (load) this.dialog.closeAll();
          if (resp.success && resp.data && resp.data.length > 0) {
            this.requestEvaluar.conforme = resp.data[0].conforme;
            this.requestEvaluar.observacion = resp.data[0].observacion;
            this.requestEvaluar.idSolPlantacionForestalEvaluacion =
              resp.data[0].idSolPlantacionForestalEvaluacion;
            this.requestEvaluar.idSolPlantacionForestalEvaluacionDetalle =
              resp.data[0].idSolPlantacionForestalEvaluacionDetalle;
          }
        },
        (error) => this.errorMensaje(error, load)
      );
  }

  registrarEvaluacion() {
    let request = {
      idSolPlantacionForestal: this.idBPF,
      fechaInicio: new Date(),
      fechaFin: new Date(),
      idUsuarioRegistro: this.usuario.idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .registrarInfoEvaluacionSolicitudPlantacionForestal(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (resp) => {
          if (resp.success) {
            this.requestEvaluar.idSolPlantacionForestalEvaluacion =
              resp.data.idSolPlantacionForestalEvaluacion;
            this.registrar();
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error: HttpErrorResponse) => this.errorMensaje(error, true)
      );
  }

  registrar() {
    this.requestEvaluar.idUsuarioRegistro = this.usuario.idusuario;
    this.requestEvaluar.idUsuarioModificacion = this.usuario.idusuario;
    let params = [];
    params.push(this.requestEvaluar);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .registrarInfoDetalleEvaluacionSolicitudPlantacionForestal(params)
      .subscribe(
        (resp) => {
          if (resp.success) {
            this.toast.ok(resp.message);
            this.listar(true);
          } else {
            this.dialog.closeAll();
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  //BOTONES
  btnGuardar() {
    if (!this.validar()) return;
    if (
      this.requestEvaluar.idSolPlantacionForestalEvaluacion != null &&
      this.requestEvaluar.idSolPlantacionForestalEvaluacion > 0
    ) {
      this.registrar();
    } else {
      this.registrarEvaluacion();
    }
  }

  btnAlerta() {
    if (
      this.requestEvaluar.idSolPlantacionForestalEvaluacion != null &&
      this.requestEvaluar.idSolPlantacionForestalEvaluacion > 0
    ) {
      this.registrar();
    }
  }
  validar(): boolean {
    let validado = true;
    if (
      !(
        this.requestEvaluar.conforme === true ||
        this.requestEvaluar.conforme === false
      )
    ) {
      validado = false;
      this.toast.warn('(*) Debe de seleccionar una opción.');
    } else {
      if (
        this.requestEvaluar.conforme === false &&
        !this.requestEvaluar.observacion
      ) {
        validado = false;
        this.toast.warn('(*) Debe ingresar la observación.');
      }

      if (this.requestEvaluar.conforme === true)
        this.requestEvaluar.observacion = '';
    }

    return validado;
  }

  btnValidarSuperPos() {
    // this.validarSuperposicionEvent.emit('Click');
    let codigoTipo: any = this.codigoProceso;
    let fileUpload: any = [];

    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          //codigoTipoPGMF: codigoTipo,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    this._layers.forEach((t: any) => {
      if (t.overlap === true) {
        validateOverlap = true;
      }
    });
    let validateOverlap: any = false;
    // let validateOverlap = true;
    if (validateOverlap === true) {
      this.isSuperposicion = true;
    }
  }

  btnSolInspOcular() {
    let params = {
      idEvaluacionCampo: 0,
      documentoGestion: this.idBPF,
      tipoDocumentoGestion: TipoPlanManejo.PLANTACIONES_FORESTALES,
      idUsuarioRegistro: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionCampoService.registroEvaluacionCampo(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        this.obtenerInfoTitular();
        if (result.success) {
          this.toast.ok(
            'Se registró la Solicitud de Inspección Ocular correctamente.'
          );
        } else {
          this.toast.warn(
            'No se registró la Solicitud de Inspección Ocular correctamente.'
          );
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  //FUNCIONES
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
