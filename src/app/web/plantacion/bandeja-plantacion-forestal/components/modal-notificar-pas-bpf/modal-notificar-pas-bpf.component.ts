import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';

@Component({
  selector: 'app-modal-notificar-pas-bpf',
  templateUrl: './modal-notificar-pas-bpf.component.html',
  styleUrls: ['./modal-notificar-pas-bpf.component.scss']
})
export class ModalNotificarPasBpfComponent implements OnInit {

  isNotificarPasSolicitante: boolean = false;
  isNotificarPasSerfor: boolean = false;
  isBlockGuardar: boolean = false;

  ischange: boolean = false;

  fileResolNull = { idArchivo: 0, idSolPlantForestArchivo: 0, tipoArchivo: "TDOCRESOLDECNUL" };

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private solPlantacionForestalService: SolPlantacionForestalService,
    private permisoForestalService: PermisoForestalService,
  ) {
    this.isNotificarPasSolicitante = this.config.data.notificarPasSolicitante || false;
    this.isNotificarPasSerfor = this.config.data.notificarPasSerfor || false;
  }
  
  ngOnInit(): void {
    this.obtenerDocumentos();
  }

  //SERVICIOS
  obtenerDocumentos() {
    const param = { idSolPlantForest: this.config.data.idSolPlantacion };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInfoAdjuntosSolPlantacionForestal(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        //Docuemnto Resolución nulidad
        this.fileResolNull = resp.data.find((z: any) => z.tipoArchivo === this.fileResolNull.tipoArchivo) || this.fileResolNull;
        this.validShorGuardar();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  notificarPasSolicitante() {
    const params = {
      idSolicitante: this.config.data.idSolicitante,
      idSolicitudPlantacionForestal: this.config.data.idSolPlantacion,
      notificarPasSolicitante: true,
      idUsuarioModificacion: this.config.data.idUser,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.notificarPasSolicitanteServ(params).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.ischange = true;
        this.isNotificarPasSolicitante = true;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  notificarPassSerfor() {
    const params = {
      idSolicitudPlantacionForestal: this.config.data.idSolPlantacion,
      notificarPasSerfor: true,
      idUsuarioModificacion: this.config.data.idUser,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.notificarPassSerforServ(params).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.ischange = true;
        this.isNotificarPasSerfor = true;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  enviarDocResolNulidad() {
    const params = [{
      idSolPlantForest: this.config.data.idSolPlantacion,
      idSolPlantForestArchivo: this.fileResolNull.idSolPlantForestArchivo,
      idArchivo: this.fileResolNull.idArchivo,
      tipoArchivo: this.fileResolNull.tipoArchivo,
      idUsuarioRegistro: this.config.data.idUser,
      codigoSeccion: null,
      codigoSubSeccion: null,
      descripcion: null,
    }];

    this.permisoForestalService.registrarInfoAdjuntosSolPlantacionForestal(params).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Se actualizó la solicitud de Plantación Forestal correctamente.");
        this.ref.close(true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarArchivo(id: number) {
    this.fileResolNull.idArchivo = id;
  }

  eliminarArchivo(ok: boolean) {
    if (ok) this.fileResolNull.idArchivo = 0;
  }

  //BOTONES
  btnNotificarUser(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de notificar al usuario?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.notificarPasSolicitante();
      },
    });
  }

  btnNotificarSerfor(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de notificar a SERFOR?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.notificarPassSerfor();
      },
    });
  }

  btnGuardar() {
    if(!this.validarGuardar()) return;
    this.enviarDocResolNulidad();
  }

  btnCancelar() {
    this.ref.close(this.ischange);
  }

  //FUNCIONES
  validarGuardar(): boolean {
    let valido = true;
    if(!this.isNotificarPasSolicitante) {
      valido = false;
      this.toast.warn("(*) Debe de notificar al usuario.");
    }
    if(!this.isNotificarPasSerfor) {
      valido = false;
      this.toast.warn("(*) Debe de notificar a SERFOR.");
    }
    if(!this.fileResolNull.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe de adjuntar un documento.");
    }
    return valido;
  }

  validShorGuardar() {
    this.isBlockGuardar = this.isNotificarPasSolicitante && this.isNotificarPasSerfor && !!this.fileResolNull.idArchivo;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }


}
