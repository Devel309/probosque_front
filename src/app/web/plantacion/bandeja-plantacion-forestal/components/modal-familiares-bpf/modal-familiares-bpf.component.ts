import { Component, OnInit } from '@angular/core';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { IfamiliarPersonaBpf } from 'src/app/model/plantacion/IGestionPlanFor';

@Component({
  selector: 'app-modal-familiares-bpf',
  templateUrl: './modal-familiares-bpf.component.html',
  styleUrls: ['./modal-familiares-bpf.component.scss']
})
export class ModalFamiliaresBpfComponent implements OnInit {

  request: IfamiliarPersonaBpf = {
    idSolPlantacionForestal: this.config.data?.idPlan,
    idSolPlantacionForestalPersona: 0,
    dni: "", nombres: "", apellidoPaterno: "", apellidoMaterno: "",
    idUsuarioRegistro: this.config.data?.idUser
  };

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
  }

  //BOTONES
  btnAgregar() {
    if (!this.validar()) return;
    this.ref.close(this.request);
  }

  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  validar() {
    let valido = true;
    let msj = "";
    if (!this.request.dni) {
      valido = false;
      this.toast.warn("(*) Debe agregar el DNI");
    }
    if (!this.request.nombres) {
      valido = false;
      this.toast.warn("(*) Debe agregar el nombre.");
    }
    if (!this.request.apellidoPaterno) {
      valido = false;
      this.toast.warn("(*) Debe agregar el apellido paterno.");
      msj = msj + "(*) Debe agregar el apellido paterno.\n"
    }
    if (!this.request.apellidoMaterno) {
      valido = false;
      this.toast.warn("(*) Debe agregar el apellido materno.");
    }

    return valido;
  }


}
