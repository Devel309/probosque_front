import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EspeciesFauna } from 'src/app/model/medioTrasporte';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { AreaSistemaPlantacionService } from 'src/app/service/areaSistemaPlantacion.service';
import { InformacionBasicaService } from 'src/app/service/informacion-basica.service';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { AccesibilidadVias } from 'src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-hidrografia/models';

@Component({
  selector: 'app-modal-fauna-bpf',
  templateUrl: './modal-fauna-bpf.component.html',
  styleUrls: ['./modal-fauna-bpf.component.scss']
})
export class ModalFaunaBpfComponent implements OnInit {

  queryFauna: string = "";
  comboListEspeciesFauna: EspeciesFauna[] = [];
  busqueda: EspeciesFauna[] = [];
  optionPage = { pageNum: 1, pageSize: 10, };
  totalRecords: any = 0;
  listaFauna: any[] = [];
  selectedValues: AccesibilidadVias[] = [];
  valorRespModal: boolean = false;
  beforeData: any;
  isDisabled: boolean = false;
  isNuevaSpecie : boolean = false;
  nombreComun!: string;
  nombreCientifico!: string;

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private informacionBasicaService: InformacionBasicaService,
    private areaSistemaPlantacionService: AreaSistemaPlantacionService,
    private usuarioServ: UsuarioService,
  ) {
    this.beforeData = config.data;
    this.isDisabled = this.config.data.isDisabled;
    this.isNuevaSpecie = this.config.data.isNuevaSpecie;
  }

  ngOnInit() {
    
    if(this.isNuevaSpecie){
      this.listarSistemaPlantacionNuevaEspecie();
    }else{
      this.listarEspecieForestalSistemaPlantacionEspecie();
    }

  }

  btnGuardarNuevaEspecie(){
    

    if(this.nombreCientifico == null || this.nombreCientifico.trim() ==''){
      this.toast.warn('Nombre Cientifico es requerido');
      return;
    }

    if(this.nombreComun == null || this.nombreComun.trim() ==''){
      this.toast.warn('Nombre Comun es requerido');
      return;
    }

    let validar: boolean = false;
    
    this.comboListEspeciesFauna.forEach((x)=>{

      if( x.nombreCientifico === this.nombreCientifico){
        validar = true;
        
      }
    });

    if(validar){
      this.toast.warn('Nombre Cientifico ya se encuentra Registrado');
      return;
    }



    let param = [{
      idAreaSistemaPlantacion: this.beforeData.idAreaSistemaPlantacion,
      idSistemaPlantacionEspecie: 0,
      idUsuarioRegistro : this.usuarioServ.idUsuario,
      nombreComun: this.nombreComun,
      nombreCientifico: this.nombreCientifico
    }]
  
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.registrarSistemaPlantacionNuevaEspecie(param).subscribe((res: any) => {
      this.dialog.closeAll();
      if (res.success) {
        this.toast.ok(res.message);
        this.listarSistemaPlantacionNuevaEspecie();
      } else {
        this.toast.warn(res.message);
      }
    }, (error: any) => this.errorMensaje(error, true));

    this.nombreCientifico = '';
    this.nombreComun = '';
  }


  listarEspecieForestalSistemaPlantacionEspecie() {
    let param = {
      idAreaSistemaPlantacion: this.beforeData.idAreaSistemaPlantacion,
      idSistemaPlantacionEspecie: null,
      idEspecie: null,
      nombreComun: null,
      nombreCientifico: this.queryFauna || null,
      autor: null,
      familia: null,
      ...this.optionPage
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.listarEspecieForestalSistemaPlantacionEspecie(param)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.totalRecords = result.totalRecord;
          this.comboListEspeciesFauna = result.data.map((item: any) => {
            return {
              ...item,
              check: item.idSistemaPlantacionEspecie ? true : false,
              // idInfBasica: this.config.data.idInfBasica
            }
          });
          this.busqueda=this.comboListEspeciesFauna;
        } else {
          this.totalRecords = 0;
          this.comboListEspeciesFauna = [];
        }
      }, (error: any) => this.errorMensaje(error, true));
  }

  listarSistemaPlantacionNuevaEspecie() {
    let param = {
      idAreaSistemaPlantacion: this.beforeData.idAreaSistemaPlantacion,
      idSistemaPlantacionEspecie: 0,
      ...this.optionPage
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.listarSistemaPlantacionNuevaEspecie(param)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.totalRecords = result.totalRecord;
          this.comboListEspeciesFauna = result.data.map((item: any) => {
            return {
              ...item,
              check: item.idSistemaPlantacionEspecie ? true : false,
              // idInfBasica: this.config.data.idInfBasica
            }
          });
        } else {
          this.totalRecords = 0;
          this.comboListEspeciesFauna = [];
        }
      }, (error: any) => this.errorMensaje(error, true));
  }


  //SERVICIOS
  guardarSistemaPlantacionEspecie(lista: []) {
    let data = lista.map((item: any) => {
      return {
        ...item,
        idAreaSistemaPlantacion: this.beforeData.idAreaSistemaPlantacion,
        idUsuarioRegistro: this.usuarioServ.idUsuario
      }
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.registrarSistemaPlantacionEspecieLista(data).subscribe((res: any) => {
      this.dialog.closeAll();
      if (res.success) {
        this.toast.ok(res.message);
        this.listarEspecieForestalSistemaPlantacionEspecie();
      } else {
        this.toast.warn(res.message);
      }
    }, (error: any) => this.errorMensaje(error, true));

  }

  //BOTONES
  btnAceptar() {
    let listaEnviar: any = this.comboListEspeciesFauna.filter((item: any) => item.check);
    if (listaEnviar.length === 0) {
      this.toast.warn("(*) Debe Selecionar registros en la tabla.");
      return;
    }
    this.guardarSistemaPlantacionEspecie(listaEnviar);
  }

  btnBuscar() {
    this.optionPage = { pageNum: 1, pageSize: 10, };
    this.listarEspecieForestalSistemaPlantacionEspecie();
  }

  btnCancelar() {
    this.cerrarModal(this.valorRespModal);
  }

  //FUNCIONES
  onChangeFauna(event: any, data: any) {
    if (event.checked === false && data.idInfBasicaDet !== 0) {
      let params = [{
        idSistemaPlantacionEspecie: data.idSistemaPlantacionEspecie,
        idUsuarioElimina: this.usuarioServ.idUsuario,
      }];

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.areaSistemaPlantacionService.eliminarSistemaPlantacionEspecieLista(params).subscribe((response: any) => {
        this.dialog.closeAll();
        // data.idInfBasicaDet = 0;
        this.valorRespModal = true;
      }, (error: any) => this.errorMensaje(error, true));
    }
  }

  btnEliminar(data: any){
    let params = [{
      idSistemaPlantacionEspecie: data.idSistemaPlantacionEspecie,
      idUsuarioElimina: this.usuarioServ.idUsuario,
    }];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.eliminarSistemaPlantacionEspecieLista(params).subscribe((response: any) => {
      this.dialog.closeAll();
      // data.idInfBasicaDet = 0;
      this.listarSistemaPlantacionNuevaEspecie();
      this.valorRespModal = true;

    }, (error: any) => this.errorMensaje(error, true));
  }

  loadData(e: any) {
    this.optionPage = { pageNum: e.first + 1, pageSize: e.rows, };
    this.listarEspecieForestalSistemaPlantacionEspecie();
  }

  cerrarModal(valor?: any) {
    this.ref.close(valor);
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  onKey(value:any) { 
    //this.listaActividadOptions = this.search(value);/
    if(value.length == 0){
      this.listarEspecieForestalSistemaPlantacionEspecie();
    }
    this.totalRecords = 0;
    this.comboListEspeciesFauna=[];
    this.selectSearch(value);
  }

  selectSearch(value: string) { 
    let filter = value.toLowerCase();
    console.log(filter);
    console.log(this.comboListEspeciesFauna);
    for ( let i = 0 ; i < this.busqueda.length; i ++ ) {
      let option = this.busqueda[i];
      
      if (option.nombreCientifico.toLowerCase().indexOf(filter) >= 0) {
        this.comboListEspeciesFauna.push( option );
      }
    }
  }

}
