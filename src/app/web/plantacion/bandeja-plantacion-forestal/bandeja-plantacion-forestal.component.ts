import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoPlantFor } from 'src/app/model/plantacion/CodigoEstadoPlantFor';
import { IFiltroBjaSolPla } from 'src/app/model/plantacion/IGestionPlanFor';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { GenericoService } from 'src/app/service/generico.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';
import { IrPlanService } from 'src/app/shared/services/ir-plan.service';
import { NuevaPlantacionForestalComponent } from '../modales/nueva-plantacion-forestal/nueva-plantacion-forestal.component';
import { ModalGenerarCertificadoBpfComponent } from './components/modal-generar-certificado-bpf/modal-generar-certificado-bpf.component';
import { ModalNotificarPasBpfComponent } from './components/modal-notificar-pas-bpf/modal-notificar-pas-bpf.component';
import { ModalRequisitosBpfComponent } from './components/modal-requisitos-bpf/modal-requisitos-bpf.component';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
@Component({
  selector: 'app-bandeja-plantacion-forestal',
  templateUrl: './bandeja-plantacion-forestal.component.html',
  styleUrls: ['./bandeja-plantacion-forestal.component.scss'],
})
export class BandejaPlantacionForestalComponent implements OnInit {
  idSolicitante: number = 0;

  usuario!: UsuarioModel;
  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  objbuscar = {} as IFiltroBjaSolPla;
  totalRecords: number = 0;

  ListaBandeja: any[] = [];
  comboTipoMod: any[] = [];
  comboEstados: any[] = [];

  isPerTitularTH: boolean = false;
  isPerARFFS: boolean = false;
  isPerMDP: boolean = false;
  isperOsiSerComp: boolean = false;
  isPerSERFOR: boolean = false;

  TIPOMOD = { predio: "TMPFPP", th: "TMPFTH", dominio: "TMPFDP" };
  codEstado = CodigoEstadoPlantFor;

  optionsRuta: any;

  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private servPf: PermisoForestalService,
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private dialogService: DialogService,
    private genericoService: GenericoService,
    private irPlanService: IrPlanService,
    private solPlantacionForestalService: SolPlantacionForestalService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};

    this.isPerTitularTH = this.usuario.sirperfil === Perfiles.TITULARTH;
    this.isPerARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    this.isPerMDP = this.usuario.sirperfil === Perfiles.MESA_DE_PARTES;
    this.isperOsiSerComp = (this.usuario.sirperfil === Perfiles.SERFOR || this.usuario.sirperfil === Perfiles.OSINFOR || this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO);
    this.isPerSERFOR = this.usuario.sirperfil === Perfiles.SERFOR;

    
    this.setObjBuscar();
  }

  ngOnInit(): void {
    this.objbuscar.codigoPerfil = this.usuario.sirperfil;
    this.listarComboModalidad();
    this.listarComboEstados();
    this.listarBandeja();
  }

  //SERVICIOS
  listarComboModalidad() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "TMPF" }).subscribe((resp: any) => {
      if (resp.success) this.comboTipoMod = resp.data;
    });
  }

  listarComboEstados() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "SPF" }).subscribe((resp: any) => {
      if (resp.success) this.comboEstados = resp.data;
    });
  }

  listarBandeja() {
    this.ListaBandeja = [];
    this.totalRecords = 0;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPf.BandejaSolicitudPermisosForestales(this.objbuscar).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data && result.data.length > 0) {
        this.ListaBandeja = result.data;
        this.totalRecords = result.totalRecord;
      } else {
        this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      }
    }, (error: any) => this.errorMensaje(error, true));
  }

  eliminar(id: any) {
    let dto = { "idSolicitudPlantacionForestal": id, "idUsuarioElimina": this.usuario.idusuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPf.EliminarSistemaPlantacionForestal(dto).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.listarBandeja();
      } else {
        this.toast.warn(result.message);
      }
    }, (error: any) => this.errorMensaje(error, true));
  }

  remitir(id: number) {

    const param = {
      idSolicitudPlantacionForestal: id,
      idUsuarioModificacion: this.usuario.idusuario,
      remitir: true
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPf.actualizarSolicitudPlantacionForestal(param)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok(result.message);
          this.listarBandeja();
        } else {
          this.toast.warn(result.message);
        }
      }, (error) => this.errorMensaje(error, true));
  }

  notificar(id: number) {

    const param = {

      idSolicitudPlantacionForestal: id,
      idSolicitante: this.usuario.idusuario,
      idUsuarioModificacion: this.usuario.idusuario,
      notificarSolicitante: true
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPf.notificarSolicitante(param)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok(result.message);
          this.listarBandeja();
        } else {
          this.toast.warn(result.message);
        }
      }, (error) => this.errorMensaje(error, true));
  }

  clonarSolPlantFor(fila: any) {
    const params = {"idSolicitudPlantacion": fila.idSolicitudPlantacion}
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.clonarPlantacionForestal(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.btnVer(resp.codigo, fila.tipoModalidad, false, true, false, fila.estadoSolicitud, "", true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnNuevo(): void {
    const refResp = this.dialogService.open(NuevaPlantacionForestalComponent, {
      header: 'Registro de Plantación Forestal',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        idUser: this.usuario.idusuario
      }
    });

    refResp.onClose.subscribe((resp: any) => {

      if (resp) this.btnVer(resp?.id, resp?.tipo, false, true, false, CodigoEstadoPlantFor.BORRADOR, '', false);
    });

  }

  btnBuscar() {
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.listarBandeja();
  }

  btnLimpiar() {
    this.setObjBuscar();
    this.listarBandeja();
  }

  btnVer(id: number, tipoModalidad: string, disabledForm: boolean, disabledEval: boolean, showEval: boolean, estadoSolicitud: string, activar: string, isSolHija: boolean) {
    const isTM_Predio = tipoModalidad === this.TIPOMOD.predio;
    const isTM_TH = tipoModalidad === this.TIPOMOD.th;
    const isTM_Dominio = tipoModalidad === this.TIPOMOD.dominio;
    const IsTM_estado = estadoSolicitud;
    const IsTM_activar = activar;
    this.irPlanService.irFormPlantacionForestal_LS(false, id, disabledForm, disabledEval, showEval, IsTM_estado, isTM_Predio, isTM_TH, isTM_Dominio, false, IsTM_activar, isSolHija);
  }

  btnEliminar(event: any, id: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar la solicitud?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminar(id);
      },
    });
  }

  btnEvalRequisitos(id: any, disabled: boolean) {
    const refResp = this.dialogService.open(ModalRequisitosBpfComponent, {
      header: 'Documentos Presentados',
      width: '60%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: disabled,
        idSolPlantacion: id,
        idUser: this.usuario.idusuario,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listarBandeja();
    });
  }

  btnRemitir(event: any, fila: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de remitir a SERFOR?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.remitir(fila.idSolicitudPlantacion);
      },
    });
  }

  btnNotificar(event: any, fila: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de notificar al solicitante?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.notificar(fila.idSolicitudPlantacion);
      },
    });
  }

  btnNotificarPAS(fila: any) {
    const refResp = this.dialogService.open(ModalNotificarPasBpfComponent, {
      header: 'Nulidad',
      width: '40%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: false,
        idSolPlantacion: fila.idSolicitudPlantacion,
        notificarPasSerfor: fila?.notificarPasSerfor,
        notificarPasSolicitante: fila.notificarPasSolicitante,
        idSolicitante: fila.idSolicitante,
        idUser: this.usuario.idusuario,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listarBandeja();
    });
  }

  btnGenerarCertif(fila: any) {
    const refResp = this.dialogService.open(ModalGenerarCertificadoBpfComponent, {
      header: 'Ver Certificado',
      width: '40%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: false,
        idSolPlantacion: fila.idSolicitudPlantacion,
        idUser: this.usuario.idusuario,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listarBandeja();
    });
  }

  btnInfoOpcional(id: number, tipoModalidad: string, disabledForm: boolean, disabledEval: boolean, showEval: boolean, estadoSolicitud: string) {
    const isTM_Predio = tipoModalidad === this.TIPOMOD.predio;
    const isTM_TH = tipoModalidad === this.TIPOMOD.th;
    const isTM_Dominio = tipoModalidad === this.TIPOMOD.dominio;
    const IsTM_estado = estadoSolicitud;
    const isAnexo3 = true;
    this.irPlanService.irFormPlantacionForestal_LS(false, id, disabledForm, disabledEval, showEval, IsTM_estado, isTM_Predio, isTM_TH, isTM_Dominio, isAnexo3, "", true)
  }

  btnClonar(event: any, fila: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Desea registrar la información opcional?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.clonarSolPlantFor(fila);
      },
    });
  }

  btnNotificarRegInfoTitular(event: any, fila: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Solicitar actualizar información en RNPF?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.notificarRegInfoTitular(fila);
      },
    });
  }

  notificarRegInfoTitular(fila: any) {
    const params = {
      idSolicitudPlantacionForestal: fila.idSolicitudPlantacion,
      actualizarInformacionRNPF: true,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPf.actualizarInformacionTitular(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Se solicitó actualización de información en RNPF");
        this.listarBandeja();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //FUNCIONES
  loadData(event: any) {
    this.objbuscar.pageNum = event.first + 1;
    this.objbuscar.pageSize = event.rows;
    this.objbuscar.codigoPerfil = this.usuario.sirperfil;
    this.listarBandeja();
  }

  setObjBuscar(): void {
    this.objbuscar = {
      codigoPerfil: this.usuario.sirperfil,
      idSolicitudPlantacion: null,
      tipoModalidad: null,
      nombreSolicitante: null,
      numeroDocumentoSolicitante: null,
      remitir: this.isPerSERFOR ? true : null,
      estadoSolicitud: null,
      pageNum: 1,
      pageSize: 10,
    };
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }



  /**********************************************/
  verModalCheck: boolean = false;
  titulocheck = 'Registrar';
  tabList: any[] = [{
    valor: "Informacion del solicitante"
  }, {
    valor: "Del area"
  }, {
    valor: "Informacion General del area planteada"
  }, {
    valor: "Detalle de plantacion forestal"
  },
  {
    valor: "Anexo"
  }];

  openModalCheck() {
    this.verModalCheck = true;
  }

  guardar() {
    this.verModalCheck = false;
  }

}
