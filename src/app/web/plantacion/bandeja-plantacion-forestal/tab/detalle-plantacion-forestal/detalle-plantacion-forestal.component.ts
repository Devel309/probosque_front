import { Component, Input, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';
import { SolPlantacionForestalGeometriaService } from 'src/app/service/solicitud/sol-plantacion-forestal-geometria.service';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { SolPlantacionForestalDetalle } from 'src/app/model/plantacion/SolPlantacionForestalDetalle';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { AreaSistemaPlantacionService } from 'src/app/service/areaSistemaPlantacion.service';

@Component({
  selector: 'app-detalle-plantacion-forestal',
  templateUrl: './detalle-plantacion-forestal.component.html',
  styleUrls: ['./detalle-plantacion-forestal.component.scss']
})
export class DetallePlantacionForestalComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  codigoPLFOR = CodigosPLFOR;
  detalleAreaPlanteada1Context: SolPlantacionForestalDetalle = new SolPlantacionForestalDetalle();

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";

  coordVertice: string = "";
  comboCoordVertice: any[] = [];
  comboBloques: any[] = [];
  listaBloques: any[] = [];
  areaBloques: any = {};
  listadoEspecies: any[] = [];
  totalAreaArbol: number = 0;
  totalAreaBloques: number = 0;
  listaUnMedida :any []=[];
  idSolPlantacionForestalAreaParam: number = 0;
  alturaPromedio: number | null = null;

  constructor(
    private user: UsuarioService,
    private serSolPlantForest: SolPlantacionForestalService,
    private dialog: MatDialog,
    private toast: ToastService,
    private areaSistemaPlantacionService: AreaSistemaPlantacionService,
    private serviceSolPlantacionForestalGeometria: SolPlantacionForestalGeometriaService,
  ) { }

  ngOnInit(): void {
    this.listarEspeciesSistemaPlantacion();
    this.obtenerCapas();
    this.listarSuperficie();
    this.ListarUnidadMedida();
  }

  //SERVICIOS
  listarEspeciesSistemaPlantacion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSolPlantForest.listarEspeciesSistemaPlantacion(this.idBPF).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.listadoEspecies = result.data.map((x: any) => {
          return {
            ...x,
            idSolPlantForest: this.idBPF,
            totalArbolesExistentes: x.totalArbolesExistentes || null, 
            produccionEstimada: x.produccionEstimada || null,
            idUsuarioRegistro: this.user.idUsuario
          }
        });
        this.calcularTotalArbol();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerCapas() {
    let item = { idSolPlantForest: this.idBPF, codigoSeccion: CodigosPLFOR.TAB_3, codigoSubSeccion: CodigosPLFOR.TAB_3_1 };
    this.serviceSolPlantacionForestalGeometria.listarGeometria(item).subscribe((result: any) => {
      if (result.success) {
        result.data.forEach((t: any) => {
          if (t.tipoGeometria === 'TPPUNTO') this.listarVertices(t.properties)
          else if (t.tipoGeometria === 'TPAREA') this.listarComboBloque(t.properties)
        });
      }
    });
  }

  listarSuperficie() {
    let params: any = { idSolPlantaForestalArea: this.idBPF }
    this.areaSistemaPlantacionService.listarSuperficie(params).subscribe((result: any) => {
      if (result.data && result.data.length > 0) {
        this.idSolPlantacionForestalAreaParam = result.data[0].idSolPlantaForestalArea;
        this.alturaPromedio = result.data[0].alturaPromedio;
      }
    });
  }

  registrarAlturaPromedioPlan() {
    let params: any = {
      idSolPlantaForestal: this.idBPF,
      idSolPlantaForestalArea: this.idSolPlantacionForestalAreaParam,
      alturaPromedio: this.alturaPromedio,
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.registrarSupMesPlan(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        //
      }
      else this.toast.warn(resp.message)
    }, (error) => this.errorMensaje(error, true));
  }
    ListarUnidadMedida() {
    let params: any = {
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.listarUnidadMedida(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaUnMedida=resp.data;
      }
      else this.toast.warn(resp.message)
    }, (error) => this.errorMensaje(error, true));
  }

  RegistrarDetalle() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSolPlantForest.registrarSolPlantacacionForestalDetalle(this.listadoEspecies).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.listarEspeciesSistemaPlantacion();
      } else {
        this.toast.error(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  RegistrarAreaBloque() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSolPlantForest.registrarAreaBloque(this.listaBloques[0]).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        let auxNumBloque = Number(result.data.bloque.split(" ")[1]);
        this.listarAreaBloque(auxNumBloque);
      } else {
        this.toast.error(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnRegDetalle() {
    if(this.listadoEspecies.length > 0) {
      this.RegistrarDetalle();
      if (this.alturaPromedio && this.idSolPlantacionForestalAreaParam) this.registrarAlturaPromedioPlan();
    }
    else {
      this.toast.warn("No hay registros para guardar.");
    }
  }

  btnRegistrarAreaBloque() {
    if(this.listaBloques.length > 0) {
      this.RegistrarAreaBloque();
    } else {
      this.toast.warn("No hay registros para guardar.");
    }
  }

  listarVertices(properties: any) {
    let auxLista: any[] = JSON.parse(properties);
    if (auxLista.length > 0) {
      auxLista.sort(function (a, b) {
        if (a.BLOQUE > b.BLOQUE) {
          return 1;
        }
        if (a.BLOQUE < b.BLOQUE) {
          return -1;
        }
        return 0;
      });
    }
    this.comboCoordVertice = auxLista.map((x: any) => {
      return { ...x, bloqueVertice: `Bloque: ${x.BLOQUE} - Vertice: ${x.VERTICE} ( E: ${x.ESTE} : N: ${x.NORTE} )` }
    });
  }

  listarComboBloque(properties: any) {
    let items = JSON.parse(properties);
    let sumArea = 0;
    items.forEach((t: any) => {
      sumArea += Number(t.area);
    });
    this.totalAreaBloques = parseFloat(sumArea.toFixed(3));
    this.comboBloques = items;
    if(this.comboBloques.length > 0) {
      this.coordVertice = this.comboBloques[0].ET_ID;
      this.onSelectedBloque({value: this.coordVertice});
    }
  }

  onSelectedVertice(param: any) {

    let idVertice = param.value;
    let item = this.comboCoordVertice.find(
      (e: any) => e.OBJECTID == idVertice
    );

    this.detalleAreaPlanteada1Context.coordenadaEste = item.ESTE;
    this.detalleAreaPlanteada1Context.coordenadaNorte = item.NORTE;
    this.detalleAreaPlanteada1Context.numVertice = item.OBJECTID;
  }

  private listarLocalAreaBloques(idBoque: number) {

    let items = this.comboCoordVertice.filter(
      (e: any) => e.BLOQUE == idBoque
    );

    let bloque = this.comboBloques.find((e: any) => e.ET_ID == idBoque);

    let newList: any = []

    if (items.length) {
      items.forEach((t: any) => {
        newList.push({
          label: `BLOQUE ${t.BLOQUE}`,
          vertice: t.VERTICE,
          este: t.ESTE,
          norte: t.NORTE,
          area: bloque.area.toFixed(2),
          observacion: ''
        })
      });
    };

    let newItem = newList.groupBy((t: any) => t.label);
    newItem[0].area = bloque.area.toFixed(2);

    let detalle: any = [];

    newItem[0].value.forEach((i: any) => {

      detalle.push({
        idAreaBloqueDetalle: null,
        nroVertice: i.vertice,
        coordenadaEste: i.este,
        coordenadaNorte: i.norte,
        observacion: i.observacion,
      });

    })

    this.listaBloques = [
      {
        idAreaBloque: null,
        idSolPlantForest: this.idBPF,
        idSolPlantForestArea: null,
        bloque: newItem[0].key,
        area: newItem[0].area,
        alturaPromedio: null,
        areaBloqueDetalle: detalle
      }
    ];
  }

  private listarAreaBloque(bloque: number) {

    let params = {
      idSolPlantForest: this.idBPF,
      bloque: `BLOQUE ${bloque}`,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSolPlantForest.listarAreaBloque(params).subscribe(
      (result: any) => {
        this.dialog.closeAll()
        if (result.success) {

          if (result.data.idAreaBloque) {

            let detalle: any = [];
            result.data.areaBloqueDetalle.forEach((i: any) => {

              detalle.push({
                idAreaBloqueDetalle: i.idAreaBloqueDetalle,
                nroVertice: i.nroVertice,
                coordenadaEste: i.coordenadaEste,
                coordenadaNorte: i.coordenadaNorte,
                observacion: i.observacion,
              });
            });

            this.areaBloques = {

              idAreaBloque: result.data.idAreaBloque,
              idSolPlantForest: this.idBPF,
              idSolPlantForestArea: null,
              bloque: result.data.bloque,
              area: result.data.area,
              alturaPromedio: null,
              areaBloqueDetalle: detalle
            };

            this.listaBloques[0] = this.areaBloques;

          } else {
            this.listarLocalAreaBloques(bloque)
          }
        }
      },
      (error) => {

      });
  }



  //FUNCIONES
  onSelectedBloque(param: any) {
    let idBoque = param.value;
    this.listarAreaBloque(idBoque);
  }

  calcularTotalArbol() {
    let sumTotal = 0;
    this.listadoEspecies.forEach((t: any) => {
      sumTotal += Number(t.totalArbolesExistentes)
    });
    this.totalAreaArbol = sumTotal;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  //MODAL
  openModal(data: any) {
    this.detalleAreaPlanteada1Context = new SolPlantacionForestalDetalle(data);
    this.tituloModalMantenimiento = "Editar Registro"
    this.verModalMantenimiento = true;
  }

  guardar(data: SolPlantacionForestalDetalle): void {
    if (!this.validarGuardarModal()) return;

    let item = this.listadoEspecies.find(x => x.idEspecie == data.idEspecie);
    item.totalArbolesExistentes = data.totalArbolesExistentes;
    item.produccionEstimada = data.produccionEstimada;
    item.coordenadaEste = data.coordenadaEste;
    item.coordenadaNorte = data.coordenadaNorte;
    item.numVertice = data.numVertice;
    item.idSolPlantForestDetalle = data.idSolPlantForestDetalle;
    item.unidadMedida=data.unidadMedida;
    item.isChange = true;
    this.calcularTotalArbol();

    this.verModalMantenimiento = false;
  }

  validarGuardarModal(): boolean {
    let valido = true;

    if (!this.detalleAreaPlanteada1Context.totalArbolesExistentes) {
      valido = false;
      this.toast.warn("(*) Debe agregar el total de árboles.");
    }
    if (!this.detalleAreaPlanteada1Context.produccionEstimada) {
      valido = false;
      this.toast.warn("(*) Debe agregar la estimación estimada.");
    }
    if (!this.detalleAreaPlanteada1Context.numVertice) {
      valido = false;
      this.toast.warn("(*) Debe seleccionar las coordenadas.");
    }
    if (!this.detalleAreaPlanteada1Context.coordenadaEste) {
      valido = false;
      this.toast.warn("(*) Debe Ingresar Coordenda Este.");
    }
    if (!this.detalleAreaPlanteada1Context.coordenadaNorte) {
      valido = false;
      this.toast.warn("(*) Debe seleccionar Coordenada Norte.");
    }
    if (!this.detalleAreaPlanteada1Context.unidadMedida) {
      valido = false;
      this.toast.warn("(*) Seleccione Unidad de Medida");
    }
    

    return valido;
  }

}
