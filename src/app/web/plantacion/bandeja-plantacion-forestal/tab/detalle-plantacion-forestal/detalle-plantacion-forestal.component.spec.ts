import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePlantacionForestalComponent } from './detalle-plantacion-forestal.component';

describe('DetallePlantacionForestalComponent', () => {
  let component: DetallePlantacionForestalComponent;
  let fixture: ComponentFixture<DetallePlantacionForestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallePlantacionForestalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePlantacionForestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
