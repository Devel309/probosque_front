import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAnexoDosComponent } from './tab-anexo-dos.component';

describe('TabAnexoDosComponent', () => {
  let component: TabAnexoDosComponent;
  let fixture: ComponentFixture<TabAnexoDosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabAnexoDosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAnexoDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
