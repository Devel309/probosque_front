import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-tab-anexo-dos',
  templateUrl: './tab-anexo-dos.component.html',
  styleUrls: ['./tab-anexo-dos.component.scss']
})
export class TabAnexoDosComponent implements OnInit {

  @Input() idSolicitante: number = 0;


  fileList: any[] = [
    {
      descripcion: "Certificado de registro de plantaciones.pdf",
      url: "fake",
      date: new Date(),
    }
  ];
  file: any = {};

  fileUpdate: any[] = [{
    descripcion: "ultima actualizacion",
    date: new Date(),
  }];


  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {

  }

  ngOnInit(): void {

  }

  notificar(detail: string) {
    this.messageService.add({ severity: 'success', detail });
  }

  onFileChange(e: any) {
    this.file = {};
    this.file.url = URL.createObjectURL(e.target.files[0]);
    this.file.file = e.srcElement.files[0];
    this.file.descripcion = e.srcElement.files[0].name;
    this.file.date = new Date();
    this.fileList.push(this.file);

    e.preventDefault();
    e.stopPropagation();

  }

  verArchivo(d: any) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', d.url);
    link.setAttribute('download', d.descripcion);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  openEliminarRegistro(event: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.fileList.splice(index, 1);
      },
      reject: () => {
        //reject action
      }
    });
  }

}
