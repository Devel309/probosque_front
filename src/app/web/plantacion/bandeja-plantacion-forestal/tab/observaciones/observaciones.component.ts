import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PlanManejoService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';

@Component({
  selector: 'app-observaciones',
  templateUrl: './observaciones.component.html',
  styleUrls: ['./observaciones.component.scss']
})
export class ObservacionesComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  disabledEnviar: boolean = true;
  isChangeCheck: boolean = false;
  adjuntoA6_1: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_6_1 + "_1", idSolPlantForestArchivo: 0 }
  usuario!: UsuarioModel;
  urlBandeja = "/plantacion/bandeja-plantacion-forestal";

  requestTabla: any[] = [
    { titulo: "1. Información del solicitante", conforme: false, codigoAnexo: "PFDMA1", codigoEstadoAnexo: "EANBORR", descripcionCodigoEstado: "En Borrador" },
    { titulo: "2. Información del área", conforme: false, codigoAnexo: "PFDMA2", codigoEstadoAnexo: "EANBORR", descripcionCodigoEstado: "En Borrador" },
    { titulo: "3. Información general del área plantada", conforme: false, codigoAnexo: "PFDMA3", codigoEstadoAnexo: "EANBORR", descripcionCodigoEstado: "En Borrador" },
    { titulo: "4. Detalle de plantación forestal.", conforme: false, codigoAnexo: "PFDMA4", codigoEstadoAnexo: "EANBORR", descripcionCodigoEstado: "En Borrador" },
    { titulo: "5. Anexo", conforme: false, codigoAnexo: "PFDMA5", codigoEstadoAnexo: "EANBORR", descripcionCodigoEstado: "En Borrador" },
  ];

  listaSolPlanForestal: any = {};
  okBloque1: boolean = false;
  okBloque2: boolean = false;

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private permisoForestalService: PermisoForestalService,
    private planManejoService: PlanManejoService,
    private solPlantacionForestalService: SolPlantacionForestalService,
    private usuarioServ: UsuarioService,
    private router: Router,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarSolicitudPlantacionForestal();
    this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR.TAB_6_1);
  }

  //SERICIOS
  listarSolicitudPlantacionForestal() {
    const params: any = { idSolicitudPlantacionForestal: this.idBPF };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.listarSolicitudPlantacionForestal(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.requestTabla.forEach((element: any, index: number) => {
          if (index === 0) element.conforme = resp.data[0].bloqueTab01; if (element.conforme) element.descripcionCodigoEstado = "Completado";
          if (index === 1) element.conforme = resp.data[0].bloqueTab02; if (element.conforme) element.descripcionCodigoEstado = "Completado";
          if (index === 2) element.conforme = resp.data[0].bloqueTab03; if (element.conforme) element.descripcionCodigoEstado = "Completado";
          if (index === 3) element.conforme = resp.data[0].bloqueTab04; if (element.conforme) element.descripcionCodigoEstado = "Completado";
          if (index === 4) element.conforme = resp.data[0].bloqueTab05; if (element.conforme) element.descripcionCodigoEstado = "Completado";
        });

        this.okBloque1 = this.requestTabla.every(x => x.conforme === true);
        this.validarShowEnviarRev();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR_TAB_6_: any) {
    const params = {
      idSolPlantForest: this.idBPF,
      codigoSeccion: CodigosPLFOR.TAB_6,
      codigoSubSeccion: CodigosPLFOR_TAB_6_,
    };
    this.solPlantacionForestalService.obtenerInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      if (resp.success) {
        if (CodigosPLFOR_TAB_6_ === CodigosPLFOR.TAB_6_1) {
          this.adjuntoA6_1 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA6_1.tipoArchivo) || this.adjuntoA6_1;
          this.okBloque2 = !!this.adjuntoA6_1.idSolPlantForestArchivo;
          this.validarShowEnviarRev();
        }
      }
    });
  }

  actualizarInformacionTitular(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarInformacionTitular(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarSolicitudPlantacionForestal();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }


  descargarFormatoPlantacionesForestales() {
    let param: any = { idSolicitudPlantacionForestal: this.idBPF };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.descargarSolicitudPlantacionesForestales(param).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        DownloadFile(result.archivo, result.nombeArchivo, "application/octet-stream");
      }

      (error: HttpErrorResponse) => {
        this.toast.ok("ERROR");
      };
    });
  }

  registrarInfoAdjuntosSolPlanFor(params: any, CodigosPLFOR_TAB_6_?: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.registrarInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR_TAB_6_);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  enviarSolicitudRevision(tipoEnvio: string) {
    let request = {
      idSolicitudPlantacion: this.idBPF,
      tipoEnvio: tipoEnvio,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.enviarSolicitud(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Información enviada a ARFFS con éxito");
        this.router.navigate([this.urlBandeja]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnGuardar() {
    const auxAllFalse = this.requestTabla.every(x => x.conforme !== true);
    if (auxAllFalse) {
      this.toast.warn("(*) Debe seleccionar al menos un registro.");
      return;
    }

    let obj: any = {
      idSolicitudPlantacionForestal: this.idBPF,
      idUsuario: this.usuario.idusuario
    };

    this.requestTabla.forEach((element: any, index: number) => {
      if (index === 0) obj.bloqueTab01 = element.conforme;
      if (index === 1) obj.bloqueTab02 = element.conforme;
      if (index === 2) obj.bloqueTab03 = element.conforme;
      if (index === 3) obj.bloqueTab04 = element.conforme;
      if (index === 4) obj.bloqueTab05 = element.conforme;
    });

    this.actualizarInformacionTitular(obj);
  }

  btnGuardarAnexo() {
    if (this.adjuntoA6_1.idArchivo == 0) {
      this.toast.warn("Adjuntar Título de propiedad ")
      return
    }

    let params = [{
      idSolPlantForestArchivo: this.adjuntoA6_1.idSolPlantForestArchivo,
      idArchivo: this.adjuntoA6_1.idArchivo,
      tipoArchivo: this.adjuntoA6_1.tipoArchivo,
      codigoSeccion: CodigosPLFOR.TAB_6,
      codigoSubSeccion: CodigosPLFOR.TAB_6_1,
      idSolPlantForest: this.idBPF,
      idUsuarioRegistro: this.usuario.idusuario,
    }];

    this.registrarInfoAdjuntosSolPlanFor(params, CodigosPLFOR.TAB_6_1);
  }

  btnEnviarRevision(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: 'Esta acción enviará la solicitud a Mesa de Partes y ya no podrá editarla. ¿Desea continuar?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.enviarSolicitudRevision("ENVIARSC");
      },
    });
  }

  btnEnviarArffs(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: 'Esta acción enviará la solicitud a ARFFS y ya no podrá editarla. ¿Desea continuar?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.enviarSolicitudArrfs();
      },
    });
  }

  enviarSolicitudArrfs() {
    const params = {
      idSolicitudPlantacionForestal: this.idBPF,
      notificarActualizadoARFFS: true,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarInformacionTitular(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Su actualización ha sido enviada a ARRFS.");
        this.router.navigate([this.urlBandeja]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //FUNCIONES
  validarShowEnviarRev() {
    if (this.okBloque1 && this.okBloque2) this.disabledEnviar = false;
    else this.disabledEnviar = true;
  }

  clickCheck(data: any) {
    if (data.conforme) {
      data.codigoEstadoAnexo = 'EANCOMP';
      data.descripcionCodigoEstado = 'Completado';
    } else {
      data.codigoEstadoAnexo = 'EANBORR';
      data.descripcionCodigoEstado = 'En Borrador';
    }

    this.okBloque1 = false;
    this.validarShowEnviarRev();
  }

  registrarArchivo(id: number, cond: number) {
    switch (cond) {
      case 1:
        this.okBloque2 = false;
        this.validarShowEnviarRev();
        this.adjuntoA6_1.idArchivo = id;
        break;
      default:
        break;
    }
  }


  eliminarArchivo(ok: boolean, cond: number) {
    if (!ok) return;
    switch (cond) {
      case 1:
        this.okBloque2 = false;
        this.validarShowEnviarRev();
        this.adjuntoA6_1.idArchivo = 0;
        break;
      default:
        break;
    }
  }

  //FUNCIONES
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
