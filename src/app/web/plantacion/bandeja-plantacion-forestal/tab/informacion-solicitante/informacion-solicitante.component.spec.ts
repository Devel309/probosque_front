import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionSolicitanteComponent } from './informacion-solicitante.component';

describe('InformacionSolicitanteComponent', () => {
  let component: InformacionSolicitanteComponent;
  let fixture: ComponentFixture<InformacionSolicitanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformacionSolicitanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionSolicitanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
