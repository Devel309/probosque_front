import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CoreCentralService, ParametroValorService, UsuarioService } from '@services';
import { ToastService, validarEmail } from '@shared';
import { MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { solPlantacionForestalModel } from 'src/app/model/Solicitud';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from "rxjs/operators";
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
@Component({
  selector: 'app-informacion-solicitante',
  templateUrl: './informacion-solicitante.component.html',
  styleUrls: ['./informacion-solicitante.component.scss']
})
export class InformacionSolicitanteComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isStatusEval: boolean = false;
  @Input() isTM_Predio: boolean = false;
  @Input() isTM_TH: boolean = false;
  @Input() isTM_Dominio: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  codigoPLFOR = CodigosPLFOR;

  @Input('solPlantacionForestal') data: solPlantacionForestalModel = new solPlantacionForestalModel();

  comboTipoPersona: ComboModel2[] = DataDemoMPAFPP.TipoPersona;
  comboUbigeo: ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;

  @Input() ListaDistrito = [];
  @Input() ListaProvincia = [];
  @Input() ListaDepartamento = [];

  @Input() ListaDistritoRepr = [];
  @Input() ListaProvinciaRepr = [];
  ListaDepartamentoRepr = [];
  isShowRepresentante:boolean = false;
  @Input() idSolicitante: number = 0;

  usuario!: UsuarioModel;
  requestInfoTitular: any = {};
  requestInfoReprLegal: any = {};
  // ListaDepartamentoRepr = [];
  // ListaProvinciaRepr = [];
  // ListaDistritoRepr = [];
  lstTipoDocumento: any[] = [];

  constructor(
    public dialog: MatDialog,
    private core: CoreCentralService,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private permisoForestalService: PermisoForestalService,
    private messageService: MessageService,
    private parametroValorService: ParametroValorService
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarTipoDocumento();
    this.listarPorFiltroDepartamento();
    this.obtenerInfoTitular();
  }

  listarTipoDocumento() {
    const params = { prefijo: 'TPER' };
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoDocumento = result.data;
      }
    );
  };
  listarPorFiltroDepartamento() {
    let depart = {};
    this.core.listarPorFiltroDepartamento(depart).subscribe(
      (result: any) => {
        if (result.success) {
          this.ListaDepartamento = result.data;
          this.ListaDepartamentoRepr = result.data;
        }
      });
  }


  listarPorFilroDistrito(dato: any, tipo: any, ischange: boolean = false) {
    if(ischange && tipo === 1) {
      this.requestInfoTitular.idDistrito = null;
    }

    if(ischange && tipo === 2) {
      this.requestInfoReprLegal.idDistrito = null;
    }

    let provincia = { "idProvincia": dato.value };
    this.core.listarPorFilroDistrito(provincia).subscribe(
      (result: any) => {
        if (result.success) {
          if (tipo == 1) {
            this.ListaDistrito = result.data;
            if(ischange) {
              this.requestInfoTitular.idDistrito = result.data[0].idDistrito
            }
          }
          else if (tipo == 2) {
            this.ListaDistritoRepr = result.data;
            if(ischange) {
              this.requestInfoReprLegal.idDistrito = result.data[0].idDistrito
            }
          }
        } else {
          this.toast.warn(result.message);
        }
      }, (error: any) => {
        this.toast.error(error.message);
      });
  }

  listarPorFilroProvincia(dato: any, ischange: boolean = false, tipo: any) {
    if(ischange && tipo === 1) {
      this.requestInfoTitular.idProvincia = null;
      this.requestInfoTitular.idDistrito = null;
    }

    if(ischange && tipo === 2) {
      this.requestInfoReprLegal.idProvincia = null;
      this.requestInfoReprLegal.idDistrito = null;
    }

    let departamento = {
      idDepartamento: dato.value//3
    };
    this.core.listarPorFilroProvincia(departamento).subscribe(
      (result: any) => {

        if (result.success) {
          if (tipo == 1) {
            this.ListaProvincia = result.data;
            if(ischange){
              this.requestInfoTitular.idProvincia = result.data[0].idProvincia;
            } else {
              this.requestInfoTitular.idProvincia = this.requestInfoTitular.idProvincia || result.data[0].idProvincia;
            }
            this.listarPorFilroDistrito({ value: this.requestInfoTitular.idProvincia }, tipo, ischange);
          }
          else if (tipo == 2) {
            this.ListaProvinciaRepr = result.data;
            if(ischange){
              this.requestInfoReprLegal.idProvincia = result.data[0].idProvincia;
            } else {
              this.requestInfoReprLegal.idProvincia = this.requestInfoReprLegal.idProvincia || result.data[0].idProvincia;
            }
            this.listarPorFilroDistrito({ value: this.requestInfoReprLegal.idProvincia }, tipo, ischange);
          }
        } else {
          this.toast.warn(result.message);
        }
      }, (error: any) => {
        this.toast.error(error.message);
      });
  }

  setDataTitular(datos: any) {
    //Info Solicitante
    this.requestInfoTitular.estadoSolicitud = datos.solicitante.estadoSolicitud;
    this.requestInfoTitular.idSolicitudPlantacionForestal = datos.idSolicitudPlantacionForestal;
    this.requestInfoTitular.idSolicitante = datos.idSolicitante;

    if(datos.solicitante){
      this.requestInfoTitular.tipoPersona = datos.solicitante.tipoPersona;
      this.requestInfoTitular.idTipoPersona = datos.solicitante.idTipoPersona;
      this.requestInfoTitular.nombres = datos.solicitante.razonSocialEmpresa;
      this.requestInfoTitular.numeroDocumento = datos.solicitante.numeroDocumento;
      this.requestInfoTitular.ruc = datos.solicitante.ruc;
      this.requestInfoTitular.direccion = datos.solicitante.direccion;
      this.requestInfoTitular.direccionNumero = datos.solicitante.direccionNumero;
      this.requestInfoTitular.sector = datos.solicitante.sector;
      this.requestInfoTitular.telefono = datos.solicitante.telefono;
      this.requestInfoTitular.celular = datos.solicitante.celular;
      this.requestInfoTitular.correo = datos.solicitante.correo;

      this.requestInfoTitular.idDepartamento = datos.solicitante.idDepartamento;
      this.requestInfoTitular.idProvincia = datos.solicitante.idProvincia;
      this.requestInfoTitular.idDistrito = datos.solicitante.idDistrito;
    }

    if(datos.representanteLegal){
      this.requestInfoReprLegal.nombres = datos.representanteLegal.nombres;
      this.requestInfoReprLegal.numeroDocumento = datos.representanteLegal.numeroDocumento;
      this.requestInfoReprLegal.direccion = datos.representanteLegal.direccion;
      this.requestInfoReprLegal.direccionNumero = datos.representanteLegal.direccionNumero;
      this.requestInfoReprLegal.sector = datos.representanteLegal.sector;
      this.requestInfoReprLegal.telefono = datos.representanteLegal.telefono;
      this.requestInfoReprLegal.celular = datos.representanteLegal.celular;
      this.requestInfoReprLegal.correo = datos.representanteLegal.correo;

      this.requestInfoReprLegal.idDepartamento = datos.representanteLegal.idDepartamento;
      this.requestInfoReprLegal.idProvincia = datos.representanteLegal.idProvincia;
      this.requestInfoReprLegal.idDistrito = datos.representanteLegal.idDistrito;
      this.requestInfoReprLegal.sector = datos.representanteLegal.sector;
    }

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  obtenerInfoTitular() {
    let param = {
      idSolicitudPlantacionForestal : this.idBPF
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.permisoForestalService.obtenerInformacionTitular(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {
        let datos = resp.data[0];

        if(this.isTM_Dominio){
          this.isShowRepresentante = false;
        } else{
          this.isShowRepresentante = datos?.idSolicitudRepresentante ? true: false;
        }

        this.setDataTitular(datos);
        let dpto = { value: this.requestInfoTitular.idDepartamento}
        let dptoReprLegal = { value: this.requestInfoReprLegal.idDepartamento}
        this.listarPorFilroProvincia(dpto, false, 1);
        this.listarPorFilroProvincia(dptoReprLegal, false, 2);
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

   actualizarInfoTitular() {

    let request = {
      idSolicitudPlantacionForestal : this.idBPF,
      solicitante : {
        idTipoPersona: this.requestInfoTitular.idTipoPersona,
        direccion : this.requestInfoTitular.direccion,
        direccionNumero : this.requestInfoTitular.direccionNumero,
        sector : this.requestInfoTitular.sector,
        idDistrito : this.requestInfoTitular.idDistrito,
        telefono : this.requestInfoTitular.telefono,
        celular : this.requestInfoTitular.celular,
        correo : this.requestInfoTitular.correo
      },
      idUsuarioModificacion : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarInformacionTitular(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }
  actualizarInfoRepresentante() {

    let request = {
      idSolicitudPlantacionForestal : this.idBPF,
      representanteLegal : {

        direccion : this.requestInfoReprLegal.direccion,
        direccionNumero : this.requestInfoReprLegal.direccionNumero,
        sector : this.requestInfoReprLegal.sector,
        idDistrito : this.requestInfoReprLegal.idDistrito,
        telefono : this.requestInfoReprLegal.telefono,
        celular : this.requestInfoReprLegal.celular,
        correo : this.requestInfoReprLegal.correo

      },
      idUsuarioModificacion : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarInformacionTitular(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }
  //BOTONES
  btnRegInfoTitular() {
    if(!this.validarRegTitular()) return;
    this.actualizarInfoTitular();
  }

  btnRegInfoRepresentante() {
    if(!this.validarRegRepre()) return;
    this.actualizarInfoRepresentante();
  }

  //FUNCIONES
  validarRegTitular(): boolean {
    let valido = true;

    if (this.requestInfoTitular.correo && !validarEmail(this.requestInfoTitular.correo)) {
      valido = false;
      this.toast.warn("(*) Debe agregar un correo váido.");
    }

    return valido;
  }

  validarRegRepre(): boolean {
    let valido = true;

    if (this.requestInfoReprLegal.correo && !validarEmail(this.requestInfoReprLegal.correo)) {
      valido = false;
      this.toast.warn("(*) Debe agregar un correo váido.");
    }

    return valido;
  }

}
