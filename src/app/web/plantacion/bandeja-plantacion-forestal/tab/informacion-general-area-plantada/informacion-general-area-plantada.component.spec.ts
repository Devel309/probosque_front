import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionGeneralAreaPlantadaComponent } from './informacion-general-area-plantada.component';

describe('InformacionGeneralAreaPlantadaComponent', () => {
  let component: InformacionGeneralAreaPlantadaComponent;
  let fixture: ComponentFixture<InformacionGeneralAreaPlantadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformacionGeneralAreaPlantadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionGeneralAreaPlantadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
