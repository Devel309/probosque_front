import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { AreaPlantadaModel, DetalleAreaPlantadaModel } from 'src/app/model/util/dataDemoMPAFPP';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { AreaSistemaPlantacionService } from 'src/app/service/areaSistemaPlantacion.service';
import { GenericoService } from 'src/app/service/generico.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ModalFaunaBpfComponent } from '../../components/modal-fauna-bpf/modal-fauna-bpf.component';
declare const shp: any;

import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from 'src/app/model/util/Mensajes';
const moment = _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-informacion-general-area-plantada',
  templateUrl: './informacion-general-area-plantada.component.html',
  styleUrls: ['./informacion-general-area-plantada.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class InformacionGeneralAreaPlantadaComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  codigoPLFOR = CodigosPLFOR;
  codigoProceso: string = CodigosPLFOR.TAB_3;
  codigoSubSeccion: string = CodigosPLFOR.TAB_3_1;
  tipoArchivoArea: string = CodigosPLFOR.TAB_3_1 + '_S1';
  tipoArchivoPunto: string = CodigosPLFOR.TAB_3_1 + '_S2';

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accion: string = "";
  areaPlantadaContext: any = {};
  comboUnidadMedida = [{ valor: "ha" }, { valor: "m2" }, { valor: "mts" }, { valor: "acres" }]

  /****************/
  usuario!: UsuarioModel;
  requestAreaPlan: any = { aniomes: moment() };
  minDate = new Date(2020, 0, 1);
  listaCoordenadas: any[] = [];
  listaAreaSp: any[] = [];
  rowAreaPlantacion: number = 0;
  totalRecords: number = 0;
  idSolPlantacionForestalAreaParamIn: number = 0;

  constructor(
    private servPf: PermisoForestalService,
    private dialogService: DialogService,
    private usuarioServ: UsuarioService,
    private confirmationService: ConfirmationService,
    private areaSistemaPlantacionService: AreaSistemaPlantacionService,
    private genericoServ: GenericoService,
    private toast: ToastService,
    private dialog: MatDialog,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarAreaSistemaPlantacion();
    this.listarSuperficie();
  }

  /********MAPA********/
  listarVertices(items: any) {
    this.listaCoordenadas = items;
    localStorage.setItem("coordenadasBPF_tab3", JSON.stringify(items));
  }
  calcularAreaTotal(areaTotal: number) {
    if(this.requestAreaPlan.superficieHa==0 || this.requestAreaPlan.superficieHa==""){
      this.requestAreaPlan.superficieHa = areaTotal;
    }

  }
  /********FECHA MES Y AÑO***********/
  chosenYearHandler(normalizedYear: any) {
    const ctrlValue = this.requestAreaPlan.aniomes;
    ctrlValue.year(normalizedYear.year());
    this.requestAreaPlan.aniomes = ctrlValue;
  }

  chosenMonthHandler(normalizedMonth: any, datepicker: any) {
    const ctrlValue = this.requestAreaPlan.aniomes;
    ctrlValue.month(normalizedMonth.month());
    this.requestAreaPlan.aniomes = moment(ctrlValue);
    datepicker.close();
  }
  /*******************/
  //SERVICIOS
  private listarAreaSistemaPlantacion() {
    let params: any = {
      idSolPlantForest: this.idBPF
    }
    this.areaSistemaPlantacionService.listarAreaSistemaPlantacion(params).subscribe((result: any) => {
      if (result.success) {
        this.listaAreaSp = result.data;
        this.totalRecords = this.listaAreaSp.length;
      } else {
        this.toast.error("(*) Ocurrio un error.");
      }
      (error: HttpErrorResponse) => {
        this.toast.error("(*) Ocurrio un error.");
      };
    });
  }

  private listarSuperficie() {
    let params: any = { idSolPlantaForestalArea: this.idBPF };

    this.areaSistemaPlantacionService.listarSuperficie(params).subscribe((result: any) => {
      if (result.data && result.data.length > 0) {
        this.idSolPlantacionForestalAreaParamIn = result.data[0].idSolPlantaForestalArea;
        this.requestAreaPlan.superficieHa = result.data[0].areaTotalPlantacion;
        this.requestAreaPlan.aniomes = result.data[0].mesAnhoPlantacion ? moment(result.data[0].mesAnhoPlantacion) : moment();
      }
    });
  }

  registrarSupMesPlan() {
    let params: any = {};
    params = {
      idSolPlantaForestal: this.idBPF,
      idSolPlantaForestalArea: this.idSolPlantacionForestalAreaParamIn,
      areaTotalPlantacion: this.requestAreaPlan.superficieHa,
      mesAnhoPlantacion: this.requestAreaPlan.aniomes
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.areaSistemaPlantacionService.registrarSupMesPlan(params).subscribe((resp: any) => {
      if (resp.success) {
        this.listarSuperficie();
        this.registrarAreaSistemaPlantacion();
      } else {
        this.toast.warn(resp.message);
        this.dialog.closeAll();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarAreaSistemaPlantacion() {
    let params: any = [];

    this.listaAreaSp.forEach(element => {
      element.idUsuario = this.usuario.idusuario;
      element.idSolPlantForest = this.idBPF;
      params.push(element);
    });

    this.areaSistemaPlantacionService.registrarAreaSistemaPlantacion(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarAreaSistemaPlantacion();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarSistemaPlantacion(data: any, indexFila: number) {
    if (data.idAreaSistemaPlantacion) {
      let dto = {
        idAreaSistemaPlantacion: data.idAreaSistemaPlantacion,
        idUsuarioElimina: this.usuario.idusuario
      };

      this.areaSistemaPlantacionService.eliminarAreaSistemaPlantacion(dto).subscribe((result: any) => {
        this.listaAreaSp.splice(indexFila, 1);
        this.toast.ok(result.message);
      })
    } else {
      this.listaAreaSp.splice(indexFila, 1);
      this.toast.ok("Se eliminó la fila correctamente.");
    }
  }

  //BOTONES
  btnRegAreaPlan() {
    this.registrarSupMesPlan();
  }

  btnEliminar(event: any, data: any, indexFila: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar la solicitud?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarSistemaPlantacion(data, indexFila);
      },
    });
  }

  btnModalEspecies(data: any, titulo: any, isNuevaSpecie: any) {
    let auxDisabled = data.idAreaSistemaPlantacion === 0 || this.isDisbledFormu;
    
    let dataSend = {
      idPlanManejo: this.idBPF,
      isDisabled: auxDisabled,
      isNuevaSpecie : isNuevaSpecie,
      ...data
    }
    this.dialogService.open(ModalFaunaBpfComponent, {
      header: titulo,
      width: "70%",
      contentStyle: { overflow: "auto" },
      data: dataSend
    });
  }

  //FUNCIONES
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }


  //MODAL
  openModal(row: number, tipo: string) {
    this.accion = tipo;
    this.rowAreaPlantacion = row;
    if (tipo == "C") {
      this.areaPlantadaContext = {};
      /*  this.areaPlantadaContext = new DetalleAreaPlantadaModel(); */
      this.tituloModalMantenimiento = "Nuevo Registro"
    } else if (tipo == "E") {
      this.areaPlantadaContext = { ...this.listaAreaSp[this.rowAreaPlantacion] };
      this.tituloModalMantenimiento = "Editar Registro"
    }
    this.verModalMantenimiento = true;
  }

  guardar(): void {
    if (!this.validarModal()) return;

    if (this.accion == 'C') {
      this.listaAreaSp.push({ ...this.areaPlantadaContext });
    } else {
      this.listaAreaSp[this.rowAreaPlantacion] = { ...this.areaPlantadaContext };
    }
    this.verModalMantenimiento = false;
  }

  validarModal(): boolean {
    let valido = true;

    if (!this.areaPlantadaContext.nombre) {
      this.toast.warn("(*) Debe de agregar un sistema de plantación.");
      valido = false;
    }
    if (!this.areaPlantadaContext.codigoUm) {
      this.toast.warn("(*) Debe de seleccionar una unidad de medida.");
      valido = false;
    }
    if (!this.areaPlantadaContext.cantidad) {
      this.toast.warn("(*) Debe de agregar una.");
      valido = false;
    }
    if (!this.areaPlantadaContext.fines) {
      this.toast.warn("(*) Debe de agregar fines.");
      valido = false;
    }

    return valido;
  }

}

