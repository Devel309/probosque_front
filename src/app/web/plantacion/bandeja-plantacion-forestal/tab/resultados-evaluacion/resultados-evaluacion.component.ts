import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CoreCentralService, ParametroValorService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { SolicitudPlantacionForestalModel, solPlantacionForestalModel } from 'src/app/model/Solicitud';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from "rxjs/operators";
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { ContratoService } from 'src/app/service/contrato.service';
import { InformacionAreaService } from 'src/app/service/informacion-area.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';
import { varLocalStor } from 'src/app/shared/util-local-storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-resultados-evaluacion',
  templateUrl: './resultados-evaluacion.component.html',
  styleUrls: ['./resultados-evaluacion.component.scss',
  ]
})
export class ResultadosEvaluacionComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isMostrar: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  codigoPLFOR = CodigosPLFOR;

  @Input('solPlantacionForestal') data: solPlantacionForestalModel = new solPlantacionForestalModel();

  comboTipoPersona: ComboModel2[] = DataDemoMPAFPP.TipoPersona;
  comboUbigeo: ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;

  @Input() ListaDistrito = [];
  @Input() ListaProvincia = [];
  @Input() ListaDepartamento = [];

  @Input() ListaDistritoRepr = [];
  @Input() ListaProvinciaRepr = [];
  ListaDepartamentoRepr = [];
  isShowRepresentante: boolean = false;
  @Input() idSolicitante: number = 0;

  usuario!: UsuarioModel;
  codRadio = { FAV: "TRESEVALFAV", DESF: "TRESEVALDFA", SUB: "TRESEVALOBS" };
  evaluado: any = null;
  requestEntity: any = {};
  // ListaDepartamentoRepr = [];
  // ListaProvinciaRepr = [];
  // ListaDistritoRepr = [];
  lstTipoDocumento: any[] = [];
  idSolPlantacionForestalEvaluacionParamIn: number = 0;
  idArchivoCertificado: number = 0;
  nroRegistro: string = "";
  idDepartamentoArea: any;
  isDisbledGenReg: boolean = false;

  urlBandejaPlantFore = "/plantacion/bandeja-plantacion-forestal";

  constructor(
    public dialog: MatDialog,
    private core: CoreCentralService,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private permisoForestalService: PermisoForestalService,
    private messageService: MessageService,
    private parametroValorService: ParametroValorService,
    private contratoService: ContratoService,
    private informacionAreaService: InformacionAreaService,
    private solPlantacionForestalService: SolPlantacionForestalService,
    private confirmationService: ConfirmationService,
    private router: Router,
  ) {
    this.usuario = this.usuarioServ.usuario;

  }

  ngOnInit(): void {
    this.listarSolicitudPlantacionForestal();
    this.obtenerInfoEvaluacionSolicitudPlantacionForestal();
    this.obtenerInfoTitular();
    this.isMostrar = JSON.parse("" + localStorage.getItem(varLocalStor.BJA_PLANT_FORE)).estadoOpcion == 'CERTIFICADO'
  }

  btnAprobarSolicitud() {
    this.actualizarResultadoDeEvaluacionForestal(this.codRadio.FAV, "APROBARSPF");
  }

  btnEnviarComunicado() {

  }

  enviarSolicitud(tipoEnvio: string) {

    let request = {
      idSolicitudPlantacion: this.idBPF,
      tipoEnvio: tipoEnvio,
      observacion: tipoEnvio === 'EVALUACIONOBS' ? this.requestEntity.observaciones : null,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.enviarSolicitud(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {

        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  validar(): boolean {
    let validado = true;

    if (this.evaluado === this.codRadio.SUB && !this.requestEntity.observaciones) {
      validado = false;
      this.toast.warn("(*) Debe ingresar las observaciones.");
    }

    return validado;
  }

  btnDenegarSolicitud() {
    this.actualizarResultadoDeEvaluacionForestal(this.codRadio.DESF, "DENEGARSPF");
  }

  validarGuardarEval() {
    if (!this.validar()) return;
    this.actualizarResultadoDeEvaluacionForestal(this.codRadio.SUB, "EVALUACIONOBS");
  }

  obtenerInfoEvaluacionSolicitudPlantacionForestal() {
    const params: any = {
      idSolPlantacionForestal: this.idBPF
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInfoEvaluacionSolicitudPlantacionForestal(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result?.success && result.validateBusiness !== false) {
        if (result.data && result.data.length > 0) {
          this.idSolPlantacionForestalEvaluacionParamIn = result.data[0].idSolPlantacionForestalEvaluacion
          this.evaluado = result.data[0].codigoResultadoEvaluacion
          this.requestEntity.observaciones = result.data[0].observacion
          this.idArchivoCertificado = result.data[0].idArchivoCertificado
          
        }
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarResultadoDeEvaluacionForestal(codRadio: string, codEstado: string) {
    const params: any = {
      idSolPlantacionForestalEvaluacion: this.idSolPlantacionForestalEvaluacionParamIn,
      idSolPlantacionForestal: this.idBPF,
      //fechaInicio: "2022-02-12",
      //fechaFin: "2022-02-17",
      //diasEvaluacion: 5,
      codigoResultadoEvaluacion: codRadio,
      observacion: codRadio === this.codRadio.SUB ? this.requestEntity.observaciones : '',
      comunicadoEnviado: false,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarResultadoDeEvaluacionForestal(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result?.success && result.validateBusiness !== false) {
        this.enviarSolicitud(codEstado);
        this.obtenerInfoEvaluacionSolicitudPlantacionForestal();
        this.toast.ok(result.message);
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
  objGenerado: any = {};

  btnGenerarCertificado() {
    let param: any = { idSolicitudPlantacionForestal: this.idBPF };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.descargarSolicitudPlantacionForestal_PDF(param).subscribe((result: any) => {
      this.dialog.closeAll();

      if (result.success) {
        this.objGenerado = {
          archivo: result.archivo,
          nombeArchivo: result.nombeArchivo,
          contenTypeArchivo: result.contenTypeArchivo
        }
        this.toast.ok("CERTIFICADO GENERADO");
      }
      (error: HttpErrorResponse) => {
        this.toast.ok("ERROR");
      };
    });

  }

  btnDescargarCertificado() {

    if (Object.entries(this.objGenerado).length !== 0) {
      DownloadFile(
        this.objGenerado.archivo,
        this.objGenerado.nombeArchivo,
        this.objGenerado.contenTypeArchivo
      );
      this.objGenerado = {};
    } else {
      this.toast.warn("PARA DESCARGAR EL CERTIFICADO, ANTES DEBE DE GENERARLO");
    }

  }

  registrarArchivo(id: number) {

    this.idArchivoCertificado = id;

  }

  eliminarArchivo(ok: boolean) {
    if (ok) {

      if (this.idArchivoCertificado) {

        const params: any = {
          idSolPlantacionForestalEvaluacion: this.idSolPlantacionForestalEvaluacionParamIn,
          idArchivoCertificado: 0,
          idUsuarioModificacion: this.usuario.idusuario
        }

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.permisoForestalService.actualizarResultadoDeEvaluacionForestal(params).subscribe((result: any) => {
          this.dialog.closeAll();
          if (result?.success && result.validateBusiness !== false) {

            this.obtenerInfoEvaluacionSolicitudPlantacionForestal();
            this.toast.ok(result.message);
          } else {
            this.toast.warn(result?.message);
          }
        }, (error) => this.errorMensaje(error, true));
      }

      this.idArchivoCertificado = 0;
    }


  }

  btnGuardarCertificado() {
    const params: any = {
      idSolPlantacionForestalEvaluacion: this.idSolPlantacionForestalEvaluacionParamIn,
      idArchivoCertificado: this.idArchivoCertificado,
      idUsuarioModificacion: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarResultadoDeEvaluacionForestal(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result?.success && result.validateBusiness !== false) {
        this.actualizarSolicitud();
        this.obtenerInfoEvaluacionSolicitudPlantacionForestal();
        this.toast.ok(result.message);
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnGenerarRegistro() {


    const params = {
      "idDepartamento": this.idDepartamentoArea,
      "derecho": 'REG',
      "recurso": 'PLT'
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.generarCodigoContrato(params).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.nroRegistro = result.data;
        this.isDisbledGenReg = this.nroRegistro != null && this.nroRegistro != "";

        this.toast.ok('Se generó el código de registro correctamente.');
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarSolicitudPlantacionForestal() {

    let params: any = {
      idSolicitudPlantacionForestal: this.idBPF
    }

    this.permisoForestalService.listarSolicitudPlantacionForestal(params).subscribe((resp: any) => {
      if (resp.success) {
        this.nroRegistro = resp.data[0].nroRegistro;

        this.isDisbledGenReg = this.nroRegistro != null && this.nroRegistro != "";
      }
      (error: HttpErrorResponse) => {
        this.toast.ok("ERROR");
      };
    });
  }


  obtenerInfoTitular() {
    let param = {
      idSolicitudPlantacionForestal: this.idBPF
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInformacionTitular(param)
      .pipe(finalize(() => this.dialog.closeAll())).subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          let datos = resp.data[0];

          this.idDepartamentoArea = datos?.solicitante?.idDepartamento || null;

        }
      });
  }

  actualizarSolicitud() {
    let param = {
      idSolicitudPlantacionForestal: this.idBPF,
      nroRegistro: this.nroRegistro,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarSolicitudPlantacionForestal(param).subscribe(resp => {
      if (resp.success) {

      } else {
        this.dialog.closeAll();
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  
  //SERVICIOS
  validarInformacionClon(valor: boolean) {
    const params = {
      "idSolicitudPlantacionHijo": this.idBPF,
      "informacionOpcionalActualizado": valor
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.validarClonPlantacionForestal(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate([this.urlBandejaPlantFore]);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONESS
  btnActualizarInfo(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de actualizar la información?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.validarInformacionClon(true);
      },
    });
  }

  btnRechazarInfo(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de rechazar la información?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.validarInformacionClon(false);
      },
    });
  }
}
