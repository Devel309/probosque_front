import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionDelAreaComponent } from './informacion-del-area.component';

describe('InformacionDelAreaComponent', () => {
  let component: InformacionDelAreaComponent;
  let fixture: ComponentFixture<InformacionDelAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformacionDelAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionDelAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
