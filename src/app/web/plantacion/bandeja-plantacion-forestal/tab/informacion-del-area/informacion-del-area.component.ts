import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CoreCentralService, ParametroValorService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { InformacionAreaService } from 'src/app/service/informacion-area.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';

@Component({
  selector: 'app-informacion-del-area',
  templateUrl: './informacion-del-area.component.html',
  styleUrls: ['./informacion-del-area.component.scss']
})
export class InformacionDelAreaComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isTM_Predio: boolean = false;
  @Input() isTM_TH: boolean = false;
  @Input() isTM_Dominio: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  codigoPLFOR = CodigosPLFOR;
  codigoProceso: string = CodigosPLFOR.TAB_2;
  codigoSubSeccion: string = CodigosPLFOR.TAB_2_1;
  tipoArchivoArea: string = CodigosPLFOR.TAB_2_1 + '_S1';
  tipoArchivoPunto: string = CodigosPLFOR.TAB_2_1 + '_S2';
  requestPredio: any = {};
  requestTH: any = {};
  showBotonCargarCustom: boolean = true;
  showInTH: boolean = false;
  loadedFile: boolean = false;
  validaPIDEClass: boolean = false;
  validaPIDEClass1: boolean = false;
  ListaDistrito = [];
  ListaProvincia = [];
  ListaDepartamento = [];
  listaDatos = [];
  isDni: boolean = false;
  isRuc: boolean = false;
  isDni1: boolean = false;
  isRuc1: boolean = false;
  idSolPlantaForestalAreaParamIn: number = 0;
  usuarioOpositor: any = {
    numeroDocumento: '',
  };
  listaCoordenadas: any[] = [];
  usuario!: UsuarioModel;
  adjuntoA2_1: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_2_1 + "_1", idSolPlantForestArchivo: 0 }
  adjuntoA2_2: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_2_2 + "_2", idSolPlantForestArchivo: 0 }
  totalSuper: String = "";
  constructor(
    private core: CoreCentralService,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionAreaService: InformacionAreaService,
    private solPlantacionForestalService: SolPlantacionForestalService,
    private usuarioServ: UsuarioService,
    private permisoForestalService: PermisoForestalService,
    private parametroValorService: ParametroValorService,
    private confirmationService: ConfirmationService,
    private pideService: PideService,

  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    if (this.isTM_TH === true) {
      this.showBotonCargarCustom = false;
    }
    this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR.TAB_2_1);
    this.obtenerInfoTitular();
    this.listarSolPlantacionForestalArea();
    this.listarTipoDocumento();
  }

  //FUNCIONES
  registrarArchivo(id: number, cond: number) {
    switch (cond) {
      case 1: this.adjuntoA2_1.idArchivo = id; break;
      case 2: this.adjuntoA2_2.idArchivo = id; break;
      default: break;
    }
  }

  eliminarArchivo(ok: boolean, cond: number) {
    if (!ok) return;
    switch (cond) {
      case 1: this.adjuntoA2_1.idArchivo = 0; break;
      case 2: this.adjuntoA2_2.idArchivo = 0; break;
      default: break;
    }
  }
  /********MAPA********/
  listarVertices(items: any) {
    this.listaCoordenadas = items;
    localStorage.setItem("coordenadasBPF_tab2", JSON.stringify(items));
  }
  calcularAreaTotal(areaTotal: number) {
    if (areaTotal == 0 ) {
      this.toast.warn("No se pudo obtener el valor de Superficie(Ha).");
      this.btnRegPredio()
    }
      //se retira para que el área siempre se cargue
    this.requestPredio.superficieHa = areaTotal;
    // }
  }
  /*******************/
  listTipoDoc: any = [];
  //SERVICIOS
  listarTipoDocumento() {
    var params = { prefijo: 'TDOCI' }
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.listTipoDoc = result.data.filter((item: any) => (item.codigo === CodigosTiposDoc.DNI || item.codigo === CodigosTiposDoc.RUC));
      }
    );
  };

  obtenerInfoTitular() {
    let param = {
      idSolicitudPlantacionForestal: this.idBPF
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInformacionTitular(param)
      .pipe(finalize(() => this.dialog.closeAll())).subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          let datos = resp.data[0];
          //Bloque 2.1
          this.requestPredio.num_documento = datos?.solicitante?.numeroDocumento;
          this.requestPredio.ruc = datos?.solicitante?.ruc;
          this.requestPredio.propietario_predio = datos?.solicitante?.nombres || datos?.solicitante?.razonSocialEmpresa;
          this.requestPredio.id_departamento = datos?.solicitante?.idDepartamento || null;
          this.requestPredio.id_provincia = datos?.solicitante?.idProvincia || null;
          this.requestPredio.id_distrito = datos?.solicitante?.idDistrito || null;
          //Bloque 2.2
          this.requestTH.tipoContratoTH = datos.tipoContratoTH;
          this.requestTH.numeroContrato = datos.numeroContrato;
          if (this.isTM_TH === true) {
            this.requestPredio.superficieHa = datos.totalSuperficie;
          }
          this.listarPorFiltroDepartamento();
        }
      });
  }

  obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR_TAB_2_: any) {

    const params = {
      idSolPlantForest: this.idBPF,
      codigoSeccion: CodigosPLFOR.TAB_2,
      codigoSubSeccion: CodigosPLFOR_TAB_2_,
    };
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.obtenerInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      if (resp.success) {
        load.close()
        if (CodigosPLFOR_TAB_2_ === CodigosPLFOR.TAB_2_1) {
          this.adjuntoA2_1 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA2_1.tipoArchivo) || this.adjuntoA2_1;
          this.adjuntoA2_2 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA2_2.tipoArchivo) || this.adjuntoA2_2;
        }

      } else {
        load.close()
      }
    });


  }

  registrarInfoAdjuntosSolPlanFor(params: any, CodigosPLFOR_TAB_2_?: any) {
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.registrarInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      if (resp.success) {
        load.close();
        this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR_TAB_2_);
      } else {
        load.close();
      }
    });
  }

  listarPorFiltroDepartamento() {
    this.core.listarPorFiltroDepartamento({}).subscribe((result: any) => {
      if (result.success) {
        this.ListaDepartamento = result.data;
        this.requestPredio.id_departamento = this.requestPredio.id_departamento || result.data[0].idDepartamento;
        this.listarPorFilroProvincia();
      }
    });
  }


  listarPorFilroDistrito() {
    let provincia = { "idProvincia": this.requestPredio.id_provincia };
    this.core.listarPorFilroDistrito(provincia).subscribe((result: any) => {
      if (result.success) {
        this.ListaDistrito = result.data;
        this.requestPredio.codDistrito = this.requestPredio.codDistrito || result.data[0].idDistrito;
      }
    });
  }

  listarPorFilroProvincia() {
    let provincia = { "idDepartamento": this.requestPredio.id_departamento };
    this.core.listarPorFilroProvincia(provincia).subscribe((result: any) => {
      if (result.success) {
        this.ListaProvincia = result.data;
        this.requestPredio.id_provincia = this.requestPredio.id_provincia || result.data[0].idProvincia;
        this.listarPorFilroDistrito();
      }
    });
  }



  listarSolPlantacionForestalArea() {
    let params = { idSolPlantaForestalArea: this.idBPF };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaService.listarSolPlantacionForestalArea(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.data && resp.data.length > 0) {
        console.log("lista",resp.data[0]);
        this.idSolPlantaForestalAreaParamIn = resp.data[0].idSolPlantaForestalArea;
        if (resp.data[0].area > 0)
          this.requestPredio.predio = resp.data[0].area;
        this.requestPredio.predio = resp.data[0].predio;
        this.requestPredio.caserio = resp.data[0].caserioComunidad;
        this.requestPredio.cond1 = resp.data[0].inversionista;
        this.requestPredio.superficieHa = resp.data[0].area;
        this.requestPredio.cond1 = resp.data[0].inversionista;
        this.requestPredio.tipoDoc1 = resp.data[0].codigoTipodocInversionista;
        this.requestPredio.numDoc1 = resp.data[0].numerDocInversionista;
        this.requestPredio.nombres = resp.data[0].nombreInversionista;
        this.requestPredio.cond2 = resp.data[0].propietario;
        this.requestPredio.tipoDoc2 = resp.data[0].codigoTipoDocPropietario;
        this.requestPredio.numDoc2 = resp.data[0].numeroDocPropietario;
        this.requestPredio.datos= resp.data[0].nombrePropietario;
        this.requestPredio.descAdjInv = resp.data[0].desAdjuntoInversionista;
        this.requestPredio.descAdjProp = resp.data[0].desAdjuntoPropietario;
        this.requestPredio.zonaUTM = resp.data[0].zona;
        this.requestPredio.codDistrito = resp.data[0].idDistrito;
        this.requestPredio.id_provincia = resp.data[0].idProvincia;
        this.requestPredio.id_departamento = resp.data[0].idDepartamento;
        this.seleccionarTipoDocumento();
        this.seleccionarTipoDocumento1();
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  actualizarInfoTitular(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarInformacionTitular(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnRegTH() {
    const params = {
      idSolicitudPlantacionForestal: this.idBPF,
      tipoContratoTH: this.requestTH.tipoContratoTH,
      numeroContrato: this.requestTH.numeroContrato,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.actualizarInfoTitular(params);
  }

  btnRegPredio() {

    if(!this.loadedFile){
      this.toast.warn('La Superficie(ha) no se guardara,porque aun no guardo el archivo del Area.');
    }
    let params: any = {};
    if (this.isTM_Predio) {
      params = {
        idSolPlantaForestalArea: this.idSolPlantaForestalAreaParamIn,
        idSolPlantaForestal: this.idBPF,
        predio: this.requestPredio.predio,
        idDistrito: +this.requestPredio.codDistrito,
        caserioComunidad: this.requestPredio.caserio,
        area: this.loadedFile? this.requestPredio.superficieHa:0,
        zona: this.requestPredio.zonaUTM,
        inversionista: this.requestPredio.cond1 || 0,
        codigoTipodocInversionista: this.requestPredio.tipoDoc1,
        numerDocInversionista: this.requestPredio.numDoc1,
        nombreInversionista:this.requestPredio.nombres,
        desAdjuntoInversionista: this.requestPredio.descAdjInv,
        propietario: this.requestPredio.cond2 || 0,
        codigoTipoDocPropietario: this.requestPredio.tipoDoc2,
        numeroDocPropietario: this.requestPredio.numDoc2,
        nombrePropietario:this.requestPredio.datos,
        desAdjuntoPropietario: this.requestPredio.descAdjProp,
      }
        console.log();
      this.btnRegAnexo_1();
    } else {
      params = {
        idSolPlantaForestalArea: this.idSolPlantaForestalAreaParamIn,
        idSolPlantaForestal: this.idBPF,
        predio: this.requestPredio.predio,
        idDistrito: +this.requestPredio.codDistrito,
        caserioComunidad: this.requestPredio.caserio,
        area: this.requestPredio.superficieHa,
        zona: this.requestPredio.zonaUTM,
      }
    }

    this.informacionAreaService.registrarSolicitudPlantacacionArea(params).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
        this.listarSolPlantacionForestalArea();
      } else {
        this.toast.warn(result?.message);
      }
    });
  }

  btnRegAnexo_1() {

    // if(this.adjuntoA2_1.idArchivo == 0 && this.adjuntoA2_2.idArchivo == 0){
    //   this.toast.warn("Adjuntar Contrato")
    //   return
    // }

    let arrayParams: any[] = [this.adjuntoA2_1, this.adjuntoA2_2];
    let auxParams: any[] = []
    arrayParams.forEach(x => {
      if (x.idArchivo != 0) {
        let params = {
          idSolPlantForestArchivo: x.idSolPlantForestArchivo,
          idArchivo: x.idArchivo,
          tipoArchivo: x.tipoArchivo,
          codigoSeccion: CodigosPLFOR.TAB_2,
          codigoSubSeccion: CodigosPLFOR.TAB_2_1,
          idSolPlantForest: this.idBPF,
          idUsuarioRegistro: this.usuario.idusuario,

        };
        auxParams.push(params);
      }


    });


    this.registrarInfoAdjuntosSolPlanFor(auxParams, CodigosPLFOR.TAB_2_1);
  }

  //FUNCIONES
  changeDepartamento() {
    this.requestPredio.id_provincia = null;
    this.requestPredio.codDistrito = null;
    this.listarPorFilroProvincia();
  }

  changeProvincia() {
    this.requestPredio.codDistrito = null;
    this.listarPorFilroDistrito();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  validarPide(tipo:number) {
    if (!this.requestPredio.tipoDoc1) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;
    }

    if (this.requestPredio.tipoDoc1 === CodigosTiposDoc.DNI) {
      
      if (!this.requestPredio.numDoc1) {
        this.toast.warn('(*) Debe ingresar un número de DNI.');
        return;
      }
      if (this.requestPredio.numDoc1.toString().length !== 8) {
        this.toast.warn('(*) El número de documento de DNI debe tener 8 digitos.');
        return;
      }
      this.consultarDNI({ "numDNIConsulta": this.requestPredio.numDoc1 });

    } else if (this.requestPredio.tipoDoc1=== CodigosTiposDoc.RUC) {
      if (!this.requestPredio.numDoc1) {
        this.toast.warn('(*) Debe ingresar un número de RUC.');
        return;
      }
      if (this.requestPredio.numDoc1.toString().length !== 11) {
        this.toast.warn('(*) El número de documento de RUC debe tener 11 digitos.');
        return;
      }

      if (tipo === 2) {
        this.consultarDNI({ "numDNIConsulta": this.requestPredio.numDoc1 });
        return;
      }

      this.consultarRazonSocial({ "numRUC": this.requestPredio.numDoc1 });

    }
  }
  seleccionarTipoDocumento = () => {
    if (this.requestPredio.tipoDoc1 === CodigosTiposDoc.DNI) {
      this.isDni = true;
      this.isRuc = false;
    } else if(this.requestPredio.tipoDoc1 === CodigosTiposDoc.RUC){
      this.isDni = false;
      this.isRuc = true;
    }
  };

  consultarDNI(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.datosPersona) {
          let persona = result.dataService.datosPersona;
          this.requestPredio.nombres  = persona.apPrimer+" "+persona.apSegundo+" "+persona.prenombres;   
          this.cambiarValidPIDE(true);
          this.toast.ok('Se validó el DNI en RENIEC.');
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(result.dataService.deResultado);
        }
      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
    }
    )
  }
  cambiarValidPIDE(valor: boolean) {
    this.validaPIDEClass = valor;
  }
  consultarRazonSocial(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.respuesta.ddp_nombre) {
          let respuesta = result.dataService.respuesta;
          if (result.dataService.respuesta.esHabido) {
            this.cambiarValidPIDE(true);
            this.toast.ok('Se validó existencia de RUC en SUNAT.');
            this.requestPredio.nombres = respuesta.ddp_nombre;
            
          } else {
            this.cambiarValidPIDE(false);
            this.toast.warn(`RUC ingresado en estado: ${result.dataService.respuesta.desc_estado} y ${result.dataService.respuesta.desc_flag22}.`);
          }
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(`No se ha encontrado información para el RUC ${params.numRUC}.`);
        }

      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
    }
    )
  }
  //VALIDACION DE PIDE PARA EL PROPIETARIO

  validarPide1(tipo:number) {
    if (!this.requestPredio.tipoDoc2) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;
    }

    if (this.requestPredio.tipoDoc2 === CodigosTiposDoc.DNI) {
      
      if (!this.requestPredio.numDoc2) {
        this.toast.warn('(*) Debe ingresar un número de DNI.');
        return;
      }
      if (this.requestPredio.numDoc2.toString().length !== 8) {
        this.toast.warn('(*) El número de documento de DNI debe tener 8 digitos.');
        return;
      }
      this.consultarDNI1({ "numDNIConsulta": this.requestPredio.numDoc2 });

    } else if (this.requestPredio.tipoDoc2=== CodigosTiposDoc.RUC) {
      if (!this.requestPredio.numDoc2) {
        this.toast.warn('(*) Debe ingresar un número de RUC.');
        return;
      }
      if (this.requestPredio.numDoc2.toString().length !== 11) {
        this.toast.warn('(*) El número de documento de RUC debe tener 11 digitos.');
        return;
      }

      if (tipo === 2) {
        this.consultarDNI1({ "numDNIConsulta": this.requestPredio.numDoc2 });
        return;
      }

      this.consultarRazonSocial1({ "numRUC": this.requestPredio.numDoc2 });

    }
  }
  seleccionarTipoDocumento1 = () => {
    if (this.requestPredio.tipoDoc2 === CodigosTiposDoc.DNI) {
      this.isDni1 = true;
      this.isRuc1 = false;
    } else if(this.requestPredio.tipoDoc2 === CodigosTiposDoc.RUC){
      this.isDni1 = false;
      this.isRuc1 = true;
    }
  };

  consultarDNI1(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.datosPersona) {
          let persona = result.dataService.datosPersona;
          this.requestPredio.datos  = persona.apPrimer+" "+persona.apSegundo+" "+persona.prenombres;        
          this.cambiarValidPIDE1(true);
          this.toast.ok('Se validó el DNI en RENIEC.');
        } else {
          this.cambiarValidPIDE1(false);
          this.toast.warn(result.dataService.deResultado);
        }
      } else {
        this.cambiarValidPIDE1(false);
        this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE1(false);
      this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
    }
    )
  }
  cambiarValidPIDE1(valor: boolean) {
    this.validaPIDEClass1 = valor;
  }
  consultarRazonSocial1(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.respuesta.ddp_nombre) {
          let respuesta = result.dataService.respuesta;
          if (result.dataService.respuesta.esHabido) {
            this.cambiarValidPIDE1(true);
            this.toast.ok('Se validó existencia de RUC en SUNAT.');
            this.requestPredio.datos = respuesta.ddp_nombre;
          } else {
            this.cambiarValidPIDE1(false);
            this.toast.warn(`RUC ingresado en estado: ${result.dataService.respuesta.desc_estado} y ${result.dataService.respuesta.desc_flag22}.`);
          }
        } else {
          this.cambiarValidPIDE1(false);
          this.toast.warn(`No se ha encontrado información para el RUC ${params.numRUC}.`);
        }

      } else {
        this.cambiarValidPIDE1(false);
        this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE1(false);
      this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
    }
    )
  }
}
