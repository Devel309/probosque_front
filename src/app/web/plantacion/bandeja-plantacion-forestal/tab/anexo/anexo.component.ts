import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ContratoModel } from 'src/app/model/contratoModel';
import { IfamiliarPersonaBpf } from 'src/app/model/plantacion/IGestionPlanFor';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPLFOR } from 'src/app/model/util/PLFOR/CodigosPLFOR';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { ContratoService } from 'src/app/service/contrato.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolPlantacionForestalService } from 'src/app/service/sol-plantacion-forestal.service';
import { ModalFamiliaresBpfComponent } from '../../components/modal-familiares-bpf/modal-familiares-bpf.component';

@Component({
  selector: 'app-anexo',
  templateUrl: './anexo.component.html',
  styleUrls: ['./anexo.component.scss']
})
export class AnexoComponent implements OnInit {
  /*****Variables para control de bloqueo***************/
  @Input() idBPF!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isTM_Predio: boolean = false;
  @Input() isTM_TH: boolean = false;
  @Input() isTM_Dominio: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  /*****************************************************/
  codigoPLFOR = CodigosPLFOR;

  usuario!: UsuarioModel;
  TIPO_ASAMBLEA = { COM: "TPLANFORESTC", FAM: "TPLANFORESTF", GRU: "TPLANFORESTG" };
  comboTipoAsamblea: any[] = [];
  tablaAsamblea: IfamiliarPersonaBpf[] = [];
  requestAnexo3 = { ccnn: false, codigoAsamblea: null };
  isPerJur: boolean = false;

  adjuntoA2_1: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_5_2 + "_1", idSolPlantForestArchivo: 0 }
  adjuntoA3_1: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_5_3 + "_1", idSolPlantForestArchivo: 0 }
  adjuntoA3_2: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_5_3 + "_2", idSolPlantForestArchivo: 0 }
  adjuntoA3_3: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_5_3 + "_3", idSolPlantForestArchivo: 0 }
  adjuntoA3_4: any = { idArchivo: 0, tipoArchivo: CodigosPLFOR.TAB_5_3 + "_4", idSolPlantForestArchivo: 0 }
  labelAdjunto2_1: string = "Adjuntar Título de Propiedad o documento que acredite derecho a propiedad:";
  numeroTH: string = "";
  contratoRequestEntity = {} as ContratoModel;
  fileContrato: any = {};
  TipoArchivo = TiposArchivos;
  listContrato!: any[];
  totalRecords: number = 0;
  idContrato: number=0;
  tipoDocumentoGestion: String="";

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private parametroValorService: ParametroValorService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private solPlantacionForestalService: SolPlantacionForestalService,
    private permisoForestalService: PermisoForestalService,
    private contratoService: ContratoService,
    private messageService: MessageService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    if(this.isTM_TH) {
      this.labelAdjunto2_1 = "Descargar Contrato Firmado de TH:";
    }

    this.listarContrato();
    this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR.TAB_5_2);
    this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR.TAB_5_3);
    this.listarTipoAsamblea();
    this.obtenerInfoTitular();
    this.listarSolPlantForPersona(false);
  }

  //SERVICIOS
  listarTipoAsamblea() {
    var params = { prefijo: 'TPLANFOREST' }
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.comboTipoAsamblea = result.data;
      }
    );
  };

  obtenerInfoTitular() {
    let param = { idSolicitudPlantacionForestal: this.idBPF };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInformacionTitular(param).subscribe(resp => {
      if (resp.success && resp.data && resp.data.length > 0) {
        let datos = resp.data[0];
        this.requestAnexo3.ccnn = datos.ccnn;
        this.requestAnexo3.codigoAsamblea = datos.codigoAsamblea;
        this.isPerJur = !!datos.idSolicitudRepresentante;
        this.numeroTH = datos.numeroContrato;
      }
    });
  }

  listarContrato = () => {
    const params = {
      documentoGestion: null,
      tipoDocumentoGestion: null,
      // codigoEstadoContrato: this.selectedEstadoContrato || null,
      idUsuarioPostulacion: this.usuario.idusuario,
      perfil: this.usuario.sirperfil,
      pageNum: 1,
      pageSize: 10000
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.listarContrato(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data.length > 0) {
        this.listContrato = result.data;
        this.ObtenerID();
        this.obtenerContrato();
        //this.totalRecords = result.totalRecord;
      } else {
        this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
        this.listContrato = [];
        this.totalRecords = 0;
      }
    }, (error) => this.errorMensaje(error, true));
  };

  private ObtenerID() {
    let aux = this.listContrato.find((i: any) => i.codigoTituloH=== this.numeroTH);

    if (aux) {
      this.idContrato = aux.idContrato;
      this.tipoDocumentoGestion = aux.tipoDocumentoGestion;
    }


  }

  obtenerContrato = () => {
    let param: any = { idContrato: this.idContrato, tipoDocumentoGestion: this.tipoDocumentoGestion };
    this.contratoService.obtenerContratoGeneral(param).subscribe((result: any) => {
      if (result.success)
      this.convertData(result.data);
      else this.warnMensaje(result.message);
    }, (error) => this.errorMensaje
    );
  };

  private convertData(data: any) {
    let con = data[0];
    let aux4 = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.FIRMA_ARFFS);
    if (aux4) {
      this.adjuntoA2_1 = aux4;
      this.adjuntoA2_1.nombre = this.adjuntoA2_1?.nombreArchivo;
      this.adjuntoA2_1.idArchivo = aux4.idArchivo;

    }

  }

  obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR_TAB_5_: any) {
    const params = {
      idSolPlantForest: this.idBPF,
      codigoSeccion: CodigosPLFOR.TAB_5,
      codigoSubSeccion: CodigosPLFOR_TAB_5_,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.obtenerInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        if (CodigosPLFOR_TAB_5_ === CodigosPLFOR.TAB_5_2)
          this.adjuntoA2_1 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA2_1.tipoArchivo) || this.adjuntoA2_1;
        else if (CodigosPLFOR_TAB_5_ === CodigosPLFOR.TAB_5_3) {
          this.adjuntoA3_1 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA3_1.tipoArchivo) || this.adjuntoA3_1;
          this.adjuntoA3_2 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA3_2.tipoArchivo) || this.adjuntoA3_2;
          this.adjuntoA3_3 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA3_3.tipoArchivo) || this.adjuntoA3_3;
          this.adjuntoA3_4 = resp.data.find((x: any) => x.tipoArchivo === this.adjuntoA3_4.tipoArchivo) || this.adjuntoA3_4;
        }
      }
    }, () => this.dialog.closeAll());
  }

  registrarInfoAdjuntosSolPlanFor(params: any, CodigosPLFOR_TAB_5_?: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.registrarInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.obtenerInfoAdjuntosSolPlanFor(CodigosPLFOR_TAB_5_);
        this.toast.ok("Se actualizó la Solicitud Plantación Forestal correctamente.");
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarInfoAdjuntosSolPlanFor(id: number) {
    const params = { idSolPlantForestArchivo: id, idUsuarioElimina: this.usuario.idusuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.eliminarInfoAdjuntosSolPlanFor(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) this.toast.ok(resp.message)
      else this.toast.warn(resp.message)
    }, (error) => this.errorMensaje(error, true));
  }

  registrarArchivo(id: number, obj: any) {
    if (id) obj.idArchivo = id;
  }

  eliminarArchivo(ok: boolean, obj: any) {
    if (!ok) return;
    obj.idArchivo = 0;
    if (obj.idSolPlantForestArchivo) {
      this.eliminarInfoAdjuntosSolPlanFor(obj.idSolPlantForestArchivo);
      obj.idSolPlantForestArchivo = 0;
    }
  }

  actualizarInfoTitular() {
    let request = {
      idSolicitudPlantacionForestal: this.idBPF,
      ccnn: this.requestAnexo3.ccnn,
      codigoAsamblea: this.requestAnexo3.codigoAsamblea,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarInformacionTitular(request).subscribe(resp => {
      this.dialog.closeAll()
      if (resp.success) {
        // this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarSolPlantForPersona(params: IfamiliarPersonaBpf[]) {
    this.solPlantacionForestalService.registrarSolPlantForPersonaServ(params).subscribe((resp) => {
      if (resp.success) {
        this.listarSolPlantForPersona(false);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarSolPlantForPersona(isLoad: boolean) {
    const params = { idSolPlantacionForestal: this.idBPF };
    if(isLoad) this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.listarSolPlantForPersonaServ(params).subscribe((resp) => {
      if(isLoad) this.dialog.closeAll();
      if (resp.success) {
        this.tablaAsamblea = resp.data;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, isLoad));
  }

  eliminarSolPlantForPersona(id: number) {
    const params = { idSolPlantacionForestalPersona: id, idUsuarioElimina: this.usuario.idusuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solPlantacionForestalService.eliminarSolPlantForPersonaServ(params).subscribe((resp) => {
      if (resp.success) {
        this.listarSolPlantForPersona(true);
      } else {
        this.dialog.closeAll();
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnRegAnexo_2() {
    if (this.adjuntoA2_1.idArchivo == 0) {
      this.toast.warn("(*) Debe  " + this.labelAdjunto2_1)
      return
    }

    let params = [{
      idSolPlantForestArchivo: this.adjuntoA2_1.idSolPlantForestArchivo,
      idArchivo: this.adjuntoA2_1.idArchivo,
      tipoArchivo: this.adjuntoA2_1.tipoArchivo,
      codigoSeccion: CodigosPLFOR.TAB_5,
      codigoSubSeccion: CodigosPLFOR.TAB_5_2,
      idSolPlantForest: this.idBPF,
      idUsuarioRegistro: this.usuario.idusuario,

    }];
    this.registrarInfoAdjuntosSolPlanFor(params, CodigosPLFOR.TAB_5_2);
  }

  btnRegAnexo_3() {
    if (!this.validarAnexo3()) return;

    let arrayParams: any[] = [this.adjuntoA3_1, this.adjuntoA3_2, this.adjuntoA3_3, this.adjuntoA3_4];
    let auxParams: any[] = []
    arrayParams.forEach(x => {
      if (x.idArchivo != 0) {
        let params = {
          idSolPlantForestArchivo: x.idSolPlantForestArchivo,
          idArchivo: x.idArchivo,
          tipoArchivo: x.tipoArchivo,
          codigoSeccion: CodigosPLFOR.TAB_5,
          codigoSubSeccion: CodigosPLFOR.TAB_5_3,
          idSolPlantForest: this.idBPF,
          idUsuarioRegistro: this.usuario.idusuario,
        };
        auxParams.push(params);
      }
    });
    this.registrarInfoAdjuntosSolPlanFor(auxParams, CodigosPLFOR.TAB_5_3);

    this.actualizarInfoTitular();

    if (this.requestAnexo3.codigoAsamblea !== this.TIPO_ASAMBLEA.COM) {
      let auxTabla = this.tablaAsamblea.filter(x => x.idSolPlantacionForestalPersona === 0);
      if (auxTabla.length > 0) this.registrarSolPlantForPersona(auxTabla);
    }
  }

  btnMdAgregar() {
    const refResp = this.dialogService.open(ModalFamiliaresBpfComponent, {
      header: 'Nuevo Registro',
      width: '40%',
      contentStyle: { overflow: 'auto' },
      data: { idUser: this.usuario.idusuario, idPlan: this.idBPF }
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) {
        this.tablaAsamblea.push(resp);
      }
    });
  }

  btnMdEliminar(event: any, fila: IfamiliarPersonaBpf, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      key: "cargaDelete",
      accept: () => {
        if (fila.idSolPlantacionForestalPersona) {
          this.eliminarSolPlantForPersona(fila.idSolPlantacionForestalPersona);
        } else {
          this.tablaAsamblea.splice(index, 1);
        }
      },
    });
  }

  //FUNCIONES
  validarAnexo3() {
    let valido = true;

    if (this.isPerJur && !this.adjuntoA3_2.idArchivo) {
      valido = false;
      this.toast.warn("(*) Debe adjuntar documento de Representación Legal.");
    }

    if (this.requestAnexo3.ccnn) {
      if (this.isPerJur && !this.adjuntoA3_3.idArchivo) {
        valido = false;
        this.toast.warn("(*) Debe adjuntar Acta de Asamblea Comunal.");
      }

      if (!this.requestAnexo3.codigoAsamblea) {
        valido = false;
        this.toast.warn("(*) Debe seleccionar un tipo de asamblea.");
      }
    }

    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'success', summary: '', detail: mensaje });
  }

  warnMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'warn', summary: '', detail: mensaje });
  }


}
