import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-evaluacion-campo-fpf',
  templateUrl: './evaluacion-campo-fpf.component.html',
  styleUrls: ['./evaluacion-campo-fpf.component.scss']
})
export class EvaluacionCampoFpfComponent implements OnInit {

  isPlanFor: boolean = true;
  idSolPlaCampo: number = 0;

  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
  ) {
    this.idSolPlaCampo = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    if(!this.idSolPlaCampo) this.router.navigate(['plantacion/fiscalizacion-plantacion-forestal']);
  }

  ngOnInit(): void {
  }

}
