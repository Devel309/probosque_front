import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ArchivoService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';

@Component({
  selector: 'app-modal-requisitos-fpf',
  templateUrl: './modal-requisitos-fpf.component.html',
  styleUrls: ['./modal-requisitos-fpf.component.scss']
})
export class ModalRequisitosFpfComponent implements OnInit {

  requestModal: any = {};
  listaDocumentos: any[] = [];
  disabledIN: boolean = false;
  disabledPlantilla: boolean = false;
  tieneObs: boolean | null = null;

  showFormatofiscal: boolean = false;
  evaluaFav: any = null;

  idEvaluacionCampo: number = 0;

  fileInfoFisca = { idArchivo: 0, idSolPlantForestArchivo: 0, tipoArchivo: "TDOCINFFIS" };

  constructor(
    private router: Router,
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private toast: ToastService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private permisoForestalService: PermisoForestalService,
  ) {
    this.disabledIN = this.config.data.isDisabled;
    this.requestModal.idSolicitudPlantacionForestal = this.config.data.idSolPlantacion;
    this.requestModal.idUsuarioModificacion = this.config.data.idUser;

    this.idEvaluacionCampo = this.config.data.idEvaluacionCampo || 0;
    this.evaluaFav = this.config.data.favorable;
    
  }

  ngOnInit(): void {
    this.obtenerDocumentos();
  }

  //SERVICIOS

  obtenerDocumentos() {
    const param = { idSolPlantForest: this.requestModal.idSolicitudPlantacionForestal };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.obtenerInfoAdjuntosSolPlantacionForestal(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {

        let allNull = resp.data.every((x: any) => (x.conformeFiscalizacion === null));

        if (allNull) {
          this.listaDocumentos = resp.data.map((item: any) => {
            return { ...item, "idUsuarioModificacion": this.requestModal.idUsuarioModificacion }
          });
          this.showFormatofiscal = false;
        } else {
          this.listaDocumentos = resp.data.filter((y: any) => y.conformeFiscalizacion !== null).map((item: any) => {
            return { ...item, "idUsuarioModificacion": this.requestModal.idUsuarioModificacion }
          });
          this.showFormatofiscal = true;
        }

        //Plantilla Informe Fiscalización
        this.fileInfoFisca = resp.data.find((z: any) => z.tipoArchivo === this.fileInfoFisca.tipoArchivo) || this.fileInfoFisca;
        this.disabledPlantilla = (this.evaluaFav !== null && this.fileInfoFisca.idArchivo) ? true : false;

      }
    }, (error) => this.errorMensaje(error, true));
  }

  descargar(id: number) {
    if (id) {
      const params = { idArchivo: id };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  crearObserverDocumentos() {
    let auxListaDoc = this.listaDocumentos.filter((item: any) => item.conformeFiscalizacion !== null);
    let observer = from(auxListaDoc);

    this.dialog.open(LoadingComponent, { disableClose: true });
    observer.pipe(concatMap((item: any) => this.actualizarDocuemntos(item))).pipe(finalize(() => {
      this.dialog.closeAll();
      this.toast.ok("Se actualizó la solicitud de Plantación Forestal correctamente.");
      this.obtenerDocumentos();
    })).subscribe((result: any) => {
      // 
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarDocuemntos(item: any) {
    return this.permisoForestalService.actualizarInfoAdjuntosSolPlantacionForestal(item);
  }

  actualizarSolicitud(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.actualizarSolicitudPlantacionForestal(params).subscribe((resp: any) => {
      if (resp.success) {
        this.enviarDocInformeFiscal();
      } else {
        this.dialog.closeAll();
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  enviarDocInformeFiscal() {
    const params = [{
      idSolPlantForest: this.config.data.idSolPlantacion,
      idSolPlantForestArchivo: this.fileInfoFisca.idSolPlantForestArchivo,
      idArchivo: this.fileInfoFisca.idArchivo,
      tipoArchivo: this.fileInfoFisca.tipoArchivo,
      idUsuarioRegistro: this.config.data.idUser,
      codigoSeccion: null,
      codigoSubSeccion: null,
      descripcion: null,
    }];

    this.permisoForestalService.registrarInfoAdjuntosSolPlantacionForestal(params).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok("Se actualizó la solicitud de Plantación Forestal correctamente.");
        this.ref.close(true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnDescargar(idFile: any) {
    this.descargar(idFile)
  }

  btnGuardar() {
    if (!this.validarGuardar()) return;
    this.crearObserverDocumentos();
  }

  btnEnviarResp() {
    if (!this.validarEviarResp()) return;
    const params = {
      "idSolicitudPlantacionForestal": this.config.data.idSolPlantacion,
      "favorable": this.evaluaFav,
      "idUsuarioModificacion": this.config.data.idUser,
    }
    this.actualizarSolicitud(params);
  }

  btnCancelar() {
    this.ref.close();
  }

  btnVerInpsOcular() {
    if (!this.idEvaluacionCampo) return;
    const url = this.router.serializeUrl(this.router.createUrlTree([`/plantacion/evaluacion-campo-plantacion-forestal/${this.idEvaluacionCampo}`]));
    window.open(url, '_blank');
  }

  btnDescargarPlant() {

  }

  registrarArchivo(id: number) {
    this.fileInfoFisca.idArchivo = id;
  }

  eliminarArchivo(ok: boolean) {
    this.fileInfoFisca.idArchivo = 0;
  }


  //FUNCIONES
  validarGuardar(): boolean {
    let validado = true;
    let hayNull = this.listaDocumentos.some((item: any) => item.conformeFiscalizacion === null);
    let isConformeFalseSinObs = this.listaDocumentos.some((item: any) => (item.conformeFiscalizacion === false && !item.obsFiscalizacion));

    if (hayNull) {
      validado = false;
      this.toast.warn("(*) Debe seleccionar todos los registros.");
    }

    if (isConformeFalseSinObs) {
      validado = false;
      this.toast.warn("(*) Debe ingresar las observaciones");
    }

    return validado;
  }

  validarEviarResp(): boolean {
    let validado = true;

    if (!this.fileInfoFisca.idArchivo) {
      validado = false;
      this.toast.warn("(*) Debe adjuntar Informe de fiscalización.");
    }

    if (this.evaluaFav === null) {
      validado = false;
      this.toast.warn("(*) Debe seleccionar favorable o desfavorable.");
    }

    return validado;
  }

  clickValidarRadio(fila: any) {
    if (fila.conformeFiscalizacion) fila.obsFiscalizacion = "";
  }

  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
