import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoPlantFor } from 'src/app/model/plantacion/CodigoEstadoPlantFor';
import { IFiltroBjsFiscaPlan } from 'src/app/model/plantacion/IGestionPlanFor';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { GenericoService } from 'src/app/service/generico.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ModalRequisitosFpfComponent } from './components/modal-requisitos-fpf/modal-requisitos-fpf.component';

@Component({
  selector: 'app-fiscalizacion-plantacion-forestal',
  templateUrl: './fiscalizacion-plantacion-forestal.component.html',
  styleUrls: ['./fiscalizacion-plantacion-forestal.component.scss']
})
export class FiscalizacionPlantacionForestalComponent implements OnInit {
  usuario!: UsuarioModel;
  ListaBandeja: any[] = [];
  comboTipoMod: any[] = [];
  objbuscar = {} as IFiltroBjsFiscaPlan;
  totalRecords: number = 0;
  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private dialogService: DialogService,
    private usuarioServ: UsuarioService,
    private genericoService: GenericoService,
    private servPf: PermisoForestalService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.setObjBuscar();
  }

  ngOnInit(): void {
    this.listarComboModalidad();
    this.listarBandeja();
  }

  //SERVICIOS
  listarComboModalidad() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "TMPF" }).subscribe((resp: any) => {
      if (resp.success) this.comboTipoMod = resp.data;
    });
  }

  listarBandeja() {
    this.ListaBandeja = [];
    this.totalRecords = 0;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPf.BandejaSolicitudPermisosForestales(this.objbuscar).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data && result.data.length > 0) {
        this.ListaBandeja = result.data;
        this.totalRecords = result.totalRecord;
      } else {
        this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      }
    }, (error: any) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnBuscar() {
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.listarBandeja();
  }

  btnLimpiar() {
    this.setObjBuscar();
    this.listarBandeja();
  }

  btnFiscalReq(fila: any) {
    const refResp = this.dialogService.open(ModalRequisitosFpfComponent, {
      header: 'Revisión de Documentos para Fiscalización',
      width: '60%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: false,
        idSolPlantacion: fila.idSolicitudPlantacion,
        idEvaluacionCampo: fila.idEvaluacionCampo,
        favorable: fila.favorable,
        idUser: this.usuario.idusuario,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listarBandeja();
    });
  }

  //FUNCIONES
  loadData(event: any) {
    this.objbuscar.pageNum = event.first + 1;
    this.objbuscar.pageSize = event.rows;
    this.listarBandeja();
  }

  setObjBuscar(): void {
    this.objbuscar = {
      idSolicitudPlantacion: null,
      tipoModalidad: null,
      nombreSolicitante: null,
      numeroDocumentoSolicitante: null,
      estadoSolicitud: CodigoEstadoPlantFor.APROBADA_SOL,
      codigoPerfil: this.usuario.sirperfil,
      pageNum: 1,
      pageSize: 10,
    };
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }


}
