import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Page } from "@models";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, ToastService } from "@shared";
import {
  ConfirmationService,
  LazyLoadEvent,
  MessageService,
} from "primeng/api";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { CodigoEstadoMesaPartesEvaluacion } from "src/app/model/util/CodigoEstadoMesaPartes";
import { FileModel } from "src/app/model/util/File";
import { MesaPartesService } from "src/app/service/evaluacion/evaluacion-plan-operativo/mesa-partes.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { EvaluacionPlanManejoCcnnService } from "../../../../../service/evaluacion/evaluacion-plan-manejo-forestal-ccnn/evaluacion-plan-manejo-ccnn.service";

@Component({
  selector: "tab-mesa-partes",
  templateUrl: "./tab-mesa-partes.component.html",
  styleUrls: ["./tab-mesa-partes.component.scss"],
})
export class TabMesaPartesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  evaluacion: LineamientoInnerModel = new LineamientoInnerModel();
  verModalMantenimiento: boolean = false;

  queryFauna: string = "";

  idMesaPartes: number = 0;

  listProcedimientosAdministrativos: any[] = [];

  verModalFauna: boolean = false;
  selectedValues: string = "";

  editar: boolean = false;

  listadoTupa: any = [
    {
      requisito: "Requisito TUPA 01",
      conforme: "true",
      descripcion: "Requisito_01.pdf",
    },
    {
      requisito: "Requisito TUPA 02",
      conforme: "true",
      descripcion: "Requisito_02.pdf",
    },
    {
      requisito: "Requisito TUPA 03",
      conforme: "true",
      descripcion: "Requisito_03.pdf",
    },
    {
      requisito: "Requisito TUPA 04",
      conforme: "true",
      descripcion: "Requisito_04.pdf",
    },
  ];

  lineamiento: any = {};
  archivoAdjunto: any = {};

  edit: boolean = false;
  indexLineamiento!: any;

  pendiente: boolean = false;

  mesaPartesObj: MesaPartesCabeceraModel = new MesaPartesCabeceraModel();
  mesaPartesDetalleObj: MesaPartesDetalleModel = new MesaPartesDetalleModel();
  listMesaPartesDetalle: MesaPartesDetalleModel[] = [];
  codigoEdoMesaPartesEvaluacion = CodigoEstadoMesaPartesEvaluacion;
  codEstadoConforme: string = "";

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  files: FileModel[] = [];
  filePMFI: FileModel = {} as FileModel;
  cargarPMFI: boolean = false;
  eliminarPMFI: boolean = true;
  idArchivoPMFI: number = 0;
  tieneArchivo: boolean = false;
  verEnviar1: boolean = false;

  constructor(
    private evaluacionPlanManejoService: EvaluacionPlanManejoCcnnService,
    private confirmationService: ConfirmationService,
    private mesaPartesService: MesaPartesService,
    private toast: ToastService,
    private anexosService: AnexosService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private archivoServ: ArchivoService
  ) {}

  ngOnInit(): void {
    this.filePMFI.inServer = false;
    this.filePMFI.descripcion = "PDF";
    let params = {
      pageNum: 1,
      pageSize: 10,
      ubigeo: "000000",
    };

    const allItems =
      this.listadoTupa.filter((data: any) => data.conforme == "true").length ==
      this.listadoTupa.length;

    if (!!allItems) {
      this.evaluacion.conforme = true;
    } else {
      this.evaluacion.conforme = false;
    }

    //this.listarProcedimientosAdministrativos(params);
  }

  loadMesaPartes(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });

    this.listMesaPartes(page);
  }

  listMesaPartes(page?: Page) {
    var params = {
      idPlanManejo: this.idPlanManejo,
      pageNum: page?.pageNumber,
      pageSize: page?.pageSize,
    };
    this.mesaPartesService
      .listarMesaPartes(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.evaluacion.observacion = response.data[0].descripcion;
          this.codEstadoConforme = response.data[0].conforme;
          response.data.forEach((element: any) => {
            this.idMesaPartes = element.idMesaPartes;
            if (element.listarMesaPartesDetalle.length != 0) {
              this.listadoTupa = [];
              element.listarMesaPartesDetalle.forEach((element: any) => {
                this.listadoTupa.push(element);
              });
              const allItems =
                this.listadoTupa.filter((data: any) => data.conforme == "true")
                  .length == this.listadoTupa.length;

              if (!!allItems) {
                this.evaluacion.conforme = true;
              } else {
                this.evaluacion.conforme = false;
              }
            }
          });
          this.editar = false;
        } else {
          this.editar = true;
        }
      });
  }

  lazyLoadProcedimiento(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNum = Number(e.first) / pageSize + 1;

    let params = {
      pageNum: pageNum,
      pageSize: pageSize,
      ubigeo: "000000",
    };

    this.listarProcedimientosAdministrativos(params);
  }

  listarProcedimientosAdministrativos(params: any) {
    this.listProcedimientosAdministrativos = [];

    this.evaluacionPlanManejoService
      .listarProcedimientoAdministrativo(params)
      .subscribe((result: any) => {
        result.data.forEach((ele: any) => {
          let obje = {
            codigoProcedimiento: ele.codigoProcedimiento,
            idProcedimiento: ele.idProcedimiento,
            procedimientoAdministrativo: ele.procedimientoAdministrativo,
            ubigeo: ele.ubigeo,
          };

          this.listProcedimientosAdministrativos.push(obje);
        });
      });
  }

  abrirModalEspecieFauna() {
    //this.selecEspeciesFauna = {} as AccesibilidadVias;
    this.selectedValues = "";
    this.verModalFauna = true;
  }

  editarLineamiento(data: any, index: number) {
    this.filePMFI.nombreFile = "";
    this.queryFauna = "";
    this.lineamiento = {};
    this.archivoAdjunto = {};
    this.indexLineamiento = null;
    this.edit = true;

    this.queryFauna = data.requisito;
    this.lineamiento.conforme = data.conforme;
    this.archivoAdjunto.nombre = data.nombreArchivo;
    this.indexLineamiento = index;
    this.listarArchivoDemaFirmado(data.idArchivo);

    this.verModalMantenimiento = true;
  }

  abrirModalMantenimiento() {
    this.queryFauna = "";
    this.filePMFI.nombreFile = "";
    this.lineamiento = {};
    this.archivoAdjunto = {};
    this.edit = false;
    this.verModalMantenimiento = true;
  }

  registrarLineamiento() {
    if (this.edit) {
      this.filePMFI.nombreFile = "";
      this.listadoTupa[
        this.indexLineamiento
      ].nombreArchivo = this.archivoAdjunto.nombre;
      this.listadoTupa[this.indexLineamiento].requisito = this.queryFauna;
      this.listadoTupa[
        this.indexLineamiento
      ].conforme = this.lineamiento.conforme;
      this.pendiente = true;
    } else {
      this.lineamiento.nombreArchivo = this.archivoAdjunto.nombre;
      this.lineamiento.requisito = this.queryFauna;
      this.listadoTupa.push(this.lineamiento);
      this.pendiente = true;
    }

    this.verModalMantenimiento = false;
  }

  eliminarLineamiento(event: any, index: number, data: any): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el archivo?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idMesaPartesDet != 0) {
          var params = {
            idMesaPartesDet: data.idMesaPartesDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.mesaPartesService
            .eliminarMesaPartesDetalle(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.listadoTupa.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listadoTupa.splice(index, 1);
        }
      },
    });
  }

  seleccionarProcedimiento(data: any) {
    this.queryFauna = data.codigoProcedimiento;
    this.verModalFauna = false;
  }

  filtrarFauna() {
    if (this.queryFauna) {
      this.listProcedimientosAdministrativos = this.listProcedimientosAdministrativos.filter(
        (r) =>
          r.procedimientoAdministrativo
            .toLowerCase()
            .includes(this.queryFauna.toLowerCase())
      );
    } else {
      let params = {
        pageNum: 1,
        pageSize: 10,
        ubigeo: "000000",
      };
      this.listarProcedimientosAdministrativos(params);
    }
  }

  guardarMesaPartes() {
    if (!this.evaluacion.observacion && !this.evaluacion.conforme) {
      this.errorMensaje("(*) Debe ingresar Observacón.");
      return;
    }

    this.listMesaPartesDetalle = [];

    this.listadoTupa.forEach((item: any) => {
      var obj = new MesaPartesDetalleModel(item);
      this.listMesaPartesDetalle.push(obj);
    });

    var params = {
      idMesaPartes: !!this.idMesaPartes ? this.idMesaPartes : 0,
      idPlanManejo: this.idPlanManejo,
      comentario: "",
      codigo: "",
      conforme: !!this.evaluacion.conforme
        ? this.codigoEdoMesaPartesEvaluacion.COMPLETADO
        : this.codigoEdoMesaPartesEvaluacion.OBSERVADO,
      observacion: !this.evaluacion.conforme ? this.evaluacion.observacion : "",
      detalle: "",
      descripcion: "",
      idUsuarioRegistro: this.user.idUsuario,
      listarMesaPartesDetalle: this.listMesaPartesDetalle,
    };
    

    this.mesaPartesService.registrarMesaPartes(params).subscribe(
      (response: any) => {
        if (response.success) {
          if (this.evaluacion.conforme == true) {
            this.toast.ok(
              "Mesa de Partes validó los requisitos TUPA correctamente."
            );
          } else {
            this.toast.ok(
              "Mesa de Partes validó los requisitos TUPA, el cual se encuentra observado."
            );
          }
          this.pendiente = false;
          this.listMesaPartes();
        } else {
          this.toast.error(response?.message);
          this.listMesaPartes();
        }
      },
      () => {
        this.toast.error("Ha ocurrido un error, intente nuevamente.");
        this.listMesaPartes();
      }
    );
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  descargaArchivo(idArchivo: number) {
    //this.listarArchivoDemaFirmado(idArchivo);

    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.toast.error(error.message);
            this.dialog.closeAll();
          };
        });
    }
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === "PDF") {
          include = TabMesaPartesComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "el tipo de documento no es válido",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB",
          });
        } else {
          if (type === "PDF") {
            this.filePMFI.nombreFile = e.target.files[0].name;
            this.filePMFI.file = e.target.files[0];
            this.filePMFI.descripcion = type;
            this.filePMFI.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.filePMFI);
          }
        }
      }
    }
  }

  guardarArchivoPMFIFirmado() {
    if (this.files.length != 0) {
      this.files.forEach((t: any) => {
        if (t.inServer !== true) {
          this.tieneArchivo = true;
          let item = {
            id: this.user.idUsuario,
            tipoDocumento: 52,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.anexosService
            .cargarAnexos(item.id, item.tipoDocumento, t.file)
            .subscribe((result: any) => {
              this.dialog.closeAll();
              this.registrarArchivo(result.data);
              this.lineamiento.idArchivo = result.data;
            });
        }
      });
    }
  }

  registrarArchivo(id: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: "POACLIN",
      descripcion: "",
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: "",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success == true) {
        this.toast.ok("Se cargó el archivo correctamente.");
        this.listarArchivoDemaFirmado();
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  listarArchivoDemaFirmado(id?: number) {
    var params = {
      idArchivo: id ? id : null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 52,
      codigoProceso: "POACLIN",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarPMFI = true;
        this.eliminarPMFI = false;
        result.data.forEach((element: any) => {
          this.filePMFI.nombreFile = element.nombreArchivo;
          this.idArchivoPMFI = element.idArchivo;
          this.lineamiento.detalle = element.nombreArchivo;
        });
      } else {
        this.eliminarPMFI = true;
        this.cargarPMFI = false;
      }
    });
  }

  eliminarArchivoPmfi() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivoPMFI))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el archivo correctamente");
        this.cargarPMFI = false;
        this.eliminarPMFI = true;
        this.filePMFI.nombreFile = "";
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  changeConforme(value: string) {
    const allItems =
      this.listadoTupa.filter((data: any) => data.conforme == "true").length ==
      this.listadoTupa.length;

    if (value == "SI" && !!allItems) {
      this.evaluacion.conforme = true;
    } else {
      this.evaluacion.conforme = false;
    }
  }
}

export class MesaPartesDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.conforme = data.conforme ? data.conforme : false;
      this.idArchivo = data.idArchivo ? data.idArchivo : 0;
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.codigo = data.codigo ? data.codigo : "";
      this.requisito = data.requisito ? data.requisito : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.idMesaPartesDet = data.idMesaPartesDet ? data.idMesaPartesDet : null;

      return;
    }
  }
  id!: number | null;
  conforme!: boolean | null;
  codigo: string = "";
  idArchivo: number = 0;
  requisito: string = "";
  observacion: string = "";
  detalle: string = "";
  descripcion: string = "";
  idMesaPartesDet!: number | null;
}

export class MesaPartesCabeceraModel {
  constructor(data?: any) {
    if (data) {
      this.comentario = data.comentario ? data.comentario : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.codigo = data.codigo ? data.codigo : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.conforme = data.conforme ? data.conforme : "";

      return;
    }
  }
  comentario: string = "";
  codigo: string = "";
  conforme!: boolean;
  observacion: string = "";
  detalle: string = "";
  descripcion: string = "";
}
