import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { finalize } from "rxjs/operators";
import {
  RequisitoDetalleModel,
  RequisitoModel,
} from "src/app/model/requisitosModel";
import { PideService } from "src/app/service/evaluacion/pide.service";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { EvaluacionService } from "src/app/service/opiniones/evaluacion.service";
import { RequisitoSimpleModel } from "src/app/shared/components/requisito-pgmf-simple/requisito-pgmf-simple.component";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { EvaluacionRequisitosPreliminaresService } from "../../../../../service/evaluacion/evaluacion-plan-manejo-forestal-ccnn/evaluacion-requisitos-preliminares.service";
import { CodigoPrerequisitos } from "../../../../../model/util/CodigoPrerequisitos";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { CodigoEstadoMesaPartes } from "../../../../../model/util/CodigoEstadoPlanManejo";
import { CodigoMedidasCorrectivas } from "../../../../../model/util/CodigoMedidasCorrectivas";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
@Component({
  selector: "tab-requisitos-poac",
  templateUrl: "./tab-requisitos-poac.component.html",
  styleUrls: ["./tab-requisitos-poac.component.scss"],
})
export class TabRequisitosPoacComponent implements OnInit {
  @Input() idPlanManejo!: number;

  verModalMantenimiento: boolean = false;
  verModalMantenimientoRepre: boolean = false;
  dialogRepresentante: string = "";
  dialogFechaInicial: string = "";
  dialogFechaFinal: string = "";
  dialogTipoRepre: string = "";

  dialogMedida: string | null = null;
  dialogIndex: number = 0;
  dialogTitulo: string = "";
  dialogSubtitulo: string = "";
  dialogConforme: boolean | null = null;
  dialogTipo: string = "";
  dialogIdDetalle: number = 0;

  isDisabledRequisitos: boolean = false;
  //isDiabledMedidasCorrectivas: boolean = false;

  numeroDocumento: string | null = null;

  requisitoSuscripcion: RequisitoModel = new RequisitoModel();
  evaluacionSuscripcion: RequisitoSimpleModel = new RequisitoSimpleModel();

  requisitoMedidasCautelares: RequisitoModel = new RequisitoModel();
  evaluacionMedidasCautelares: RequisitoSimpleModel = new RequisitoSimpleModel();

  requisitoRestricciones: RequisitoModel = new RequisitoModel();
  evaluacionRestricciones: RequisitoSimpleModel = new RequisitoSimpleModel();

  requisitoMedidasCorrectivas: RequisitoModel = new RequisitoModel();
  evaluacionMedidasCorrectivas: RequisitoSimpleModel = new RequisitoSimpleModel();

  requisitoVigenciaDePoder: RequisitoModel = new RequisitoModel();
  evaluacionVigenciaDePoder: RequisitoSimpleModel = new RequisitoSimpleModel();

  requisitoVigenciaJuntaDirectiva: RequisitoModel = new RequisitoModel();
  evaluacionVigenciaJuntaDirectiva: RequisitoSimpleModel = new RequisitoSimpleModel();

  requisitoSuperficieDelArea: RequisitoModel = new RequisitoModel();
  evaluacionSuperficieDelArea: RequisitoSimpleModel = new RequisitoSimpleModel();

  listaRequisitosModel: RequisitoModel[] = [];

  lstDemo: any[] = [];

  noValido: boolean = false;

  constructor(
    private evaluacionRequisitosPreliminaresService: EvaluacionRequisitosPreliminaresService,
    private dialog: MatDialog,
    private userService: UsuarioService,
    private confirmationService: ConfirmationService,
    private pideService: PideService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private informacionGeneralService: InformacionGeneralService
  ) {}

  ngOnInit(): void {
    this.evaluacionSuscripcion.titulo = "Requisitos de Suscripción";
    this.evaluacionSuscripcion.classEstado = "req_nothing_class";
    this.evaluacionSuscripcion.icon = "pi-times-circle";

    this.evaluacionMedidasCautelares.titulo =
      "Requisitos de Medidas cautelares";
    this.evaluacionMedidasCautelares.classEstado = "req_nothing_class";
    this.evaluacionMedidasCautelares.icon = "pi-times-circle";

    this.evaluacionRestricciones.titulo = "Requisitos de Restricciones";
    this.evaluacionRestricciones.classEstado = "req_nothing_class";
    this.evaluacionRestricciones.icon = "pi-times-circle";

    this.evaluacionMedidasCorrectivas.titulo =
      "Requisitos de Medidas Correctivas";
    this.evaluacionMedidasCorrectivas.classEstado = "req_nothing_class";
    this.evaluacionMedidasCorrectivas.icon = "pi-times-circle";

    this.evaluacionVigenciaDePoder.titulo = "Requisitos de Vigencia de Poder";
    this.evaluacionVigenciaDePoder.classEstado = "req_nothing_class";
    this.evaluacionVigenciaDePoder.icon = "pi-times-circle";

    this.evaluacionVigenciaJuntaDirectiva.titulo =
      "Requisitos de Vigencia de Junta Directiva";
    this.evaluacionVigenciaJuntaDirectiva.classEstado = "req_nothing_class";
    this.evaluacionVigenciaJuntaDirectiva.icon = "pi-times-circle";

    this.evaluacionSuperficieDelArea.titulo = "Superficie de Área (ha)";
    this.evaluacionSuperficieDelArea.classEstado = "req_nothing_class";
    this.evaluacionSuperficieDelArea.icon = "pi-times-circle";

    this.requisitoSuscripcion.idPlanManejo = this.idPlanManejo;
    this.requisitoSuscripcion.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoSuscripcion.tipoRequisito =
      CodigoPrerequisitos.POAC_ACORDEON_SUSCRIPCION;
    this.requisitoSuscripcion.codigoRequisito = CodigoProceso.PLAN_OPERATIVO;

    this.requisitoMedidasCautelares.idPlanManejo = this.idPlanManejo;
    this.requisitoMedidasCautelares.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoMedidasCautelares.tipoRequisito =
      CodigoPrerequisitos.POAC_ACORDEON_MEDIDAS_CAUTELARES;
    this.requisitoMedidasCautelares.codigoRequisito =
      CodigoProceso.PLAN_OPERATIVO;

    this.requisitoRestricciones.idPlanManejo = this.idPlanManejo;
    this.requisitoRestricciones.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoRestricciones.tipoRequisito =
      CodigoPrerequisitos.POAC_ACORDEON_RESTRICCIONES;
    this.requisitoRestricciones.codigoRequisito = CodigoProceso.PLAN_OPERATIVO;

    this.requisitoMedidasCorrectivas.idPlanManejo = this.idPlanManejo;
    this.requisitoMedidasCorrectivas.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoMedidasCorrectivas.tipoRequisito =
      CodigoPrerequisitos.POAC_ACORDEON_MEDIDAS_CORRECTIVAS;
    this.requisitoMedidasCorrectivas.codigoRequisito =
      CodigoProceso.PLAN_OPERATIVO;

    this.requisitoVigenciaDePoder.idPlanManejo = this.idPlanManejo;
    this.requisitoVigenciaDePoder.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoVigenciaDePoder.tipoRequisito = "VIGENPOD";
    this.requisitoVigenciaDePoder.codigoRequisito =
      CodigoProceso.PLAN_OPERATIVO;

    this.requisitoVigenciaJuntaDirectiva.idPlanManejo = this.idPlanManejo;
    this.requisitoVigenciaJuntaDirectiva.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoVigenciaJuntaDirectiva.tipoRequisito = "VIGENDIR";
    this.requisitoVigenciaJuntaDirectiva.codigoRequisito =
      CodigoProceso.PLAN_OPERATIVO;

    this.requisitoSuperficieDelArea.idPlanManejo = this.idPlanManejo;
    this.requisitoSuperficieDelArea.idUsuarioRegistro = this.userService.idUsuario;
    this.requisitoSuperficieDelArea.tipoRequisito =
      CodigoPrerequisitos.POAC_ACORDEON_SUPERFICIE_AREA;
    this.requisitoSuperficieDelArea.codigoRequisito =
      CodigoProceso.PLAN_OPERATIVO;

    this.listarRequisitos();
    this.buscarPlanEvaluacion();
    this.listResumenEjecutivo();
  }

  buscarPlanEvaluacion() {
    //this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .obtenerPlanEvaluacion(this.idPlanManejo)
      .subscribe((res) => {
        if (res.data) {
          if (res.data.length >= 0) {
            let planManejo = res.data[0];

            let codigoEstado = planManejo.codEstado;
            if (
              codigoEstado == CodigoMedidasCorrectivas.EVAL_COMPLETADO ||
              codigoEstado == CodigoMedidasCorrectivas.EVAL_OBSERVADO
            ) {
              //this.isDiabledMedidasCorrectivas = true;
              this.isDisabledRequisitos = true;
            }
          }
        }
      });
  }

  listResumenEjecutivo() {
    var params = {
      codigoProceso: "POAC",
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          response.data.forEach((element: any) => {
            this.requisitoMedidasCautelares.comunidad =
              element.apellidoPaternoElaboraDema;
            this.requisitoRestricciones.comunidad =
              element.apellidoPaternoElaboraDema;
            this.requisitoMedidasCorrectivas.comunidad =
              element.apellidoPaternoElaboraDema;
            this.requisitoVigenciaDePoder.comunidad =
              element.apellidoPaternoElaboraDema;
            this.requisitoVigenciaJuntaDirectiva.comunidad =
              element.apellidoPaternoElaboraDema;
          });
        }
      });
  }

  verModalRepresentante(tipo: string) {
    this.dialogRepresentante = "";
    this.dialogFechaFinal = "";
    this.dialogFechaInicial = "";
    this.verModalMantenimientoRepre = true;
    this.dialogTipoRepre = tipo;
  }

  verModal(tipo: string) {
    let type: string = "";
    if (tipo == "MEDCAUTE") {
      type = "Medidas Cautelares";
    } else if (tipo == "RESTRIC") {
      type = "Restricciones";
    } else if (tipo == "MEDCORRE") {
      type = "Medidas Correctivas";
    }
    this.dialogTitulo = "Agregar";
    this.dialogSubtitulo = type;
    this.dialogMedida = null;
    this.dialogConforme = true;
    this.verModalMantenimiento = true;
    this.dialogTipo = tipo;
    this.dialogIdDetalle = 0;
  }

  verModalV2(
    tipoEdit: string,
    codigoAcordeon: string,
    data: RequisitoDetalleModel,
    rowIndex: number
  ) {
    this.dialogIdDetalle = 0;
    if (tipoEdit == "U") {
      this.dialogTitulo = "Editar";
      this.dialogMedida = data.medida;
      this.dialogConforme = data.isConformidad;
      this.dialogIndex = rowIndex;
      this.dialogIdDetalle = data.idRequisitoDet;
    } else if (tipoEdit == "C") {
      this.dialogTitulo = "Agregar";
      this.dialogMedida = null;
      this.dialogConforme = null;
    }
    this.verModalMantenimiento = true;
    this.dialogTipo = codigoAcordeon;
  }

  openEliminar(
    e: any,
    data: RequisitoDetalleModel,
    codigoTipo: string,
    rowIndex: number
  ): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idRequisitoDet != 0) {
          var params = {
            idRequisitoDet: data.idRequisitoDet,
            idUsuarioElimina: this.userService.idUsuario,
          };
          this.evaluacionRequisitosPreliminaresService
            .eliminarRequisitoDetalle(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              if (result.success == true) {
                // this.toast.ok(result?.message);
                let msg = "";
                if (codigoTipo == "MEDCAUTE") {
                  msg = "Se eliminó medida cautelar.";
                } else if (codigoTipo == "RESTRIC") {
                  msg = "Se eliminó restricción.";
                } else if (codigoTipo == "MEDCORRE") {
                  msg = "Se eliminó medida correctiva.";
                } else if (codigoTipo == "VIGENPOD") {
                  msg = "Se eliminó representante legal.";
                } else if (codigoTipo == "VIGENDIR") {
                  msg = "Se eliminó representante legal.";
                }
                this.toast.ok(msg);
                this.listarRequisitos();
              } else {
                this.toast.error(result?.message);
              }
            });
        } else if (codigoTipo == "MEDCAUTE") {
          this.requisitoMedidasCautelares.evaluacionDetalle.splice(rowIndex, 1);
        } else if (codigoTipo == "RESTRIC") {
          this.requisitoRestricciones.evaluacionDetalle.splice(rowIndex, 1);
        } else if (codigoTipo == "MEDCORRE") {
          this.requisitoMedidasCorrectivas.evaluacionDetalle.splice(
            rowIndex,
            1
          );
        } else if (codigoTipo == "VIGENPOD") {
          this.requisitoVigenciaDePoder.evaluacionDetalle.splice(rowIndex, 1);
        } else if (codigoTipo == "VIGENDIR") {
          this.requisitoVigenciaJuntaDirectiva.evaluacionDetalle.splice(
            rowIndex,
            1
          );
        }
      },
      reject: () => {},
    });
  }

  agregarDetalle() {
    if (!this.validarDetalle()) {
      return;
    }

    let reqDetalle: RequisitoDetalleModel = new RequisitoDetalleModel();
    reqDetalle.medida = this.dialogMedida;
    reqDetalle.idRequisitoDet = this.dialogIdDetalle;
    reqDetalle.idUsuarioRegistro = this.userService.idUsuario;

    if (this.dialogConforme == true) {
      reqDetalle.isConformidad = true;
      reqDetalle.conformidad = CodigoPrerequisitos.EVAL_COMPLETADO;
    } else if (this.dialogConforme == false) {
      reqDetalle.isConformidad = false;
      reqDetalle.conformidad = CodigoPrerequisitos.EVAL_OBSERVADO;
    }

    if (this.dialogTitulo == "Editar") {
      switch (this.dialogTipo) {
        case "MEDCAUTE": {
          this.requisitoMedidasCautelares.evaluacionDetalle[
            this.dialogIndex
          ] = reqDetalle;
          break;
        }
        case "RESTRIC": {
          this.requisitoRestricciones.evaluacionDetalle[
            this.dialogIndex
          ] = reqDetalle;
          break;
        }
        case "MEDCORRE": {
          this.requisitoMedidasCorrectivas.evaluacionDetalle[
            this.dialogIndex
          ] = reqDetalle;
          break;
        }
        default: {
          break;
        }
      }
    } else if (this.dialogTitulo == "Agregar") {
      switch (this.dialogTipo) {
        case "MEDCAUTE": {
          this.requisitoMedidasCautelares.evaluacionDetalle.push(reqDetalle);
          break;
        }
        case "RESTRIC": {
          this.requisitoRestricciones.evaluacionDetalle.push(reqDetalle);
          break;
        }
        case "MEDCORRE": {
          this.requisitoMedidasCorrectivas.evaluacionDetalle.push(reqDetalle);
          break;
        }
        default: {
          break;
        }
      }
    }

    this.verModalMantenimiento = false;
  }

  validarDetalle() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.dialogMedida == null || this.dialogMedida == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar " + this.dialogSubtitulo + ".\n";
    }

    if (!validar) this.toast.warn(mensaje);
    return validar;
  }

  agregarDetalleRepresentante() {
    let reqDetalle: RequisitoDetalleModel = new RequisitoDetalleModel();
    reqDetalle.medida = this.dialogRepresentante;
    reqDetalle.descripcion = this.dialogFechaInicial;
    reqDetalle.detalle = this.dialogFechaFinal;
    reqDetalle.idUsuarioRegistro = this.userService.idUsuario;

    switch (this.dialogTipoRepre) {
      case "VIGENPOD": {
        this.requisitoVigenciaDePoder.evaluacionDetalle.push(reqDetalle);
        break;
      }
      case "VIGENDIR": {
        this.requisitoVigenciaJuntaDirectiva.evaluacionDetalle.push(reqDetalle);
        break;
      }
      default: {
        break;
      }
    }
    this.verModalMantenimientoRepre = false;
  }

  listarRequisitos() {
    this.requisitoMedidasCautelares.evaluacionDetalle = [];
    let body = {
      codigoRequisito: CodigoProceso.PLAN_OPERATIVO,
      idPlanManejo: this.idPlanManejo,
      tipoRequisito: null, //"SUSCRIP"
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionRequisitosPreliminaresService
      .listarRequisito(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        //this.listaRequisitosModel = result.data;

        result.data.forEach((req: any) => {
          switch (req.tipoRequisito) {
            case CodigoPrerequisitos.POAC_ACORDEON_SUSCRIPCION: {
              //const returnedTarget = Object.assign(target, source);
              this.requisitoSuscripcion = Object.assign(
                this.requisitoSuscripcion,
                req
              );
              this.numeroDocumento = this.requisitoSuscripcion.numeroDocumentoTitular;

              if (this.requisitoSuscripcion.titularHabilitado == "SI") {
                this.requisitoSuscripcion.isTitularHabilitado = true;
              } else if (this.requisitoSuscripcion.titularHabilitado == "NO") {
                this.requisitoSuscripcion.isTitularHabilitado = false;
              }

              if (this.requisitoSuscripcion.regenteHabilitado == "SI") {
                this.requisitoSuscripcion.isRegenteHabilitado = true;
              } else if (this.requisitoSuscripcion.regenteHabilitado == "NO")
                this.requisitoSuscripcion.isRegenteHabilitado = false;

              if (
                this.requisitoSuscripcion.evaluacion ==
                CodigoPrerequisitos.EVAL_COMPLETADO
              ) {
                this.evaluacionSuscripcion.evaluacion.conforme = true;
                this.evaluacionSuscripcion.classEstado = "req_conforme_class";
                this.evaluacionSuscripcion.icon = "pi-check-circle";
              } else if (
                this.requisitoSuscripcion.evaluacion ==
                CodigoPrerequisitos.EVAL_OBSERVADO
              ) {
                this.evaluacionSuscripcion.evaluacion.conforme = false;
                this.evaluacionSuscripcion.evaluacion.observacion = this.requisitoSuscripcion.observacion;
                this.evaluacionSuscripcion.classEstado = "req_observado_class";
                this.evaluacionSuscripcion.icon = "pi-info-circle";
              }

              break;
            }
            case CodigoPrerequisitos.POAC_ACORDEON_MEDIDAS_CAUTELARES: {
              this.requisitoMedidasCautelares = Object.assign(
                this.requisitoMedidasCautelares,
                req
              );

              this.requisitoMedidasCautelares.evaluacionDetalle?.forEach(
                (detalle: any) => {
                  if (
                    detalle.conformidad == CodigoPrerequisitos.EVAL_COMPLETADO
                  ) {
                    detalle.isConformidad = true;
                  } else if (
                    detalle.conformidad == CodigoPrerequisitos.EVAL_OBSERVADO
                  ) {
                    detalle.isConformidad = false;
                  }
                }
              );

              if (
                this.requisitoMedidasCautelares.evaluacion ==
                CodigoPrerequisitos.EVAL_COMPLETADO
              ) {
                this.evaluacionMedidasCautelares.evaluacion.conforme = true;
                this.evaluacionMedidasCautelares.classEstado =
                  "req_conforme_class";
                this.evaluacionMedidasCautelares.icon = "pi-check-circle";
              } else if (
                this.requisitoMedidasCautelares.evaluacion ==
                CodigoPrerequisitos.EVAL_OBSERVADO
              ) {
                this.evaluacionMedidasCautelares.evaluacion.conforme = false;
                this.evaluacionMedidasCautelares.evaluacion.observacion = this.requisitoMedidasCautelares.observacion;
                this.evaluacionMedidasCautelares.classEstado =
                  "req_observado_class";
                this.evaluacionMedidasCautelares.icon = "pi-info-circle";
              }
              break;
            }
            case CodigoPrerequisitos.POAC_ACORDEON_RESTRICCIONES: {
              this.requisitoRestricciones = Object.assign(
                this.requisitoRestricciones,
                req
              );

              this.requisitoRestricciones.evaluacionDetalle?.forEach(
                (detalle: any) => {
                  if (
                    detalle.conformidad == CodigoPrerequisitos.EVAL_COMPLETADO
                  ) {
                    detalle.isConformidad = true;
                  } else if (
                    detalle.conformidad == CodigoPrerequisitos.EVAL_OBSERVADO
                  ) {
                    detalle.isConformidad = false;
                  }
                }
              );

              if (
                this.requisitoRestricciones.evaluacion ==
                CodigoPrerequisitos.EVAL_COMPLETADO
              ) {
                this.evaluacionRestricciones.evaluacion.conforme = true;
                this.evaluacionRestricciones.classEstado = "req_conforme_class";
                this.evaluacionRestricciones.icon = "pi-check-circle";
              } else if (
                this.requisitoRestricciones.evaluacion ==
                CodigoPrerequisitos.EVAL_OBSERVADO
              ) {
                this.evaluacionRestricciones.evaluacion.conforme = false;
                this.evaluacionRestricciones.evaluacion.observacion = this.requisitoRestricciones.observacion;
                this.evaluacionRestricciones.classEstado =
                  "req_observado_class";

                this.evaluacionRestricciones.icon = "pi-info-circle";
              }
              break;
            }

            case CodigoPrerequisitos.POAC_ACORDEON_MEDIDAS_CORRECTIVAS: {
              this.requisitoMedidasCorrectivas = Object.assign(
                this.requisitoMedidasCorrectivas,
                req
              );

              this.requisitoMedidasCorrectivas.evaluacionDetalle?.forEach(
                (detalle: any) => {
                  if (
                    detalle.conformidad == CodigoPrerequisitos.EVAL_COMPLETADO
                  ) {
                    detalle.isConformidad = true;
                  } else if (
                    detalle.conformidad == CodigoPrerequisitos.EVAL_OBSERVADO
                  ) {
                    detalle.isConformidad = false;
                  }
                }
              );

              if (
                this.requisitoMedidasCorrectivas.evaluacion ==
                CodigoPrerequisitos.EVAL_COMPLETADO
              ) {
                this.evaluacionMedidasCorrectivas.evaluacion.conforme = true;
                this.evaluacionMedidasCorrectivas.classEstado =
                  "req_conforme_class";

                this.evaluacionMedidasCorrectivas.icon = "pi-check-circle";
              } else if (
                this.requisitoMedidasCorrectivas.evaluacion ==
                CodigoPrerequisitos.EVAL_OBSERVADO
              ) {
                this.evaluacionMedidasCorrectivas.evaluacion.conforme = false;
                this.evaluacionMedidasCorrectivas.evaluacion.observacion = this.requisitoMedidasCorrectivas.observacion;
                this.evaluacionMedidasCorrectivas.classEstado =
                  "req_observado_class";

                this.evaluacionMedidasCorrectivas.icon = "pi-info-circle";
              }
              break;
            }
            case CodigoPrerequisitos.PGMFA_ACORDEON_VIGENCIA_DE_PODER: {
              this.requisitoVigenciaDePoder = Object.assign(
                this.requisitoVigenciaDePoder,
                req
              );

              if (
                this.requisitoVigenciaDePoder.evaluacion ==
                CodigoPrerequisitos.EVAL_COMPLETADO
              ) {
                this.evaluacionVigenciaDePoder.evaluacion.conforme = true;
              } else if (
                this.requisitoVigenciaDePoder.evaluacion ==
                CodigoPrerequisitos.EVAL_OBSERVADO
              ) {
                this.evaluacionVigenciaDePoder.evaluacion.conforme = false;
                this.evaluacionVigenciaDePoder.evaluacion.observacion = this.requisitoVigenciaDePoder.observacion;
              }
              break;
            }
            case "VIGENDIR": {
              this.requisitoVigenciaJuntaDirectiva = Object.assign(
                this.requisitoVigenciaJuntaDirectiva,
                req
              );

              if (
                this.requisitoVigenciaJuntaDirectiva.evaluacion ==
                CodigoPrerequisitos.EVAL_COMPLETADO
              ) {
                this.evaluacionVigenciaJuntaDirectiva.evaluacion.conforme = true;
              } else if (
                this.requisitoVigenciaJuntaDirectiva.evaluacion ==
                CodigoPrerequisitos.EVAL_OBSERVADO
              ) {
                this.evaluacionVigenciaJuntaDirectiva.evaluacion.conforme = false;
                this.evaluacionVigenciaJuntaDirectiva.evaluacion.observacion = this.requisitoVigenciaJuntaDirectiva.observacion;
              }
              break;
            }
            case CodigoPrerequisitos.POAC_ACORDEON_SUPERFICIE_AREA: {
              this.requisitoSuperficieDelArea = Object.assign(
                this.requisitoSuperficieDelArea,
                req
              );

              switch (this.requisitoSuperficieDelArea.evaluacion) {
                case CodigoPrerequisitos.EVAL_COMPLETADO: {
                  this.evaluacionSuperficieDelArea.evaluacion.conforme = true;
                  this.evaluacionSuperficieDelArea.classEstado =
                    "req_conforme_class";
                  this.evaluacionSuperficieDelArea.icon = "pi-check-circle";
                  break;
                }
                case CodigoPrerequisitos.EVAL_OBSERVADO: {
                  this.evaluacionSuperficieDelArea.evaluacion.conforme = false;
                  this.evaluacionSuperficieDelArea.evaluacion.observacion = this.requisitoSuperficieDelArea.observacion;

                  this.evaluacionSuperficieDelArea.classEstado =
                    "req_observado_class";
                  this.evaluacionSuperficieDelArea.icon = "pi-info-circle";
                  break;
                }
              }
              break;
            }
            default: {
              break;
            }
          }
        });
      });
  }

  guardarRequisitosPlan() {
    if (
      EvaluacionUtils.validarLineamientoModel([
        this.evaluacionSuscripcion.evaluacion,
        this.evaluacionMedidasCautelares.evaluacion,
        this.evaluacionRestricciones.evaluacion,
        this.evaluacionMedidasCorrectivas.evaluacion,
      ])
    ) {
      if (!this.validarRequisitosPlan()) {
        return;
      }
      this.registrarRequisitosPlan();
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  validarRequisitosPlan(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    //Requisitos de Suscripción
    if (
      ((this.requisitoSuscripcion.titular == null ||
        this.requisitoSuscripcion.titular == "") &&
        (this.requisitoSuscripcion.numeroDocumentoTitular == null ||
          this.requisitoSuscripcion.numeroDocumentoTitular == "")) ||
      ((this.requisitoSuscripcion.regente == null ||
        this.requisitoSuscripcion.regente == "") &&
        (this.requisitoSuscripcion.numeroDocumentoRegente == null ||
          this.requisitoSuscripcion.numeroDocumentoRegente == ""))
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe completar: Requisitos de Suscripción.\n";
    }

    //Requisitos de Medidas Cautelares
    if (
      this.requisitoMedidasCautelares.comunidad == null ||
      this.requisitoMedidasCautelares.comunidad == "" ||
      this.requisitoMedidasCautelares.titularConsecion == null ||
      this.requisitoMedidasCautelares.titularConsecion == "" ||
      this.requisitoMedidasCautelares.tituloHabilitante == null ||
      this.requisitoMedidasCautelares.tituloHabilitante == ""
    ) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe completar: Requisitos de Medidas Cautelares.\n";
    }
    if (
      this.requisitoMedidasCautelares.evaluacionDetalle == null ||
      this.requisitoMedidasCautelares.evaluacionDetalle.length == 0
    ) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe agregar al menos una Medida Cautelar: Requisitos de Medidas Cautelares.\n";
    }

    //Requisitos de Restricciones
    if (
      this.requisitoRestricciones.comunidad == null ||
      this.requisitoRestricciones.comunidad == "" ||
      this.requisitoRestricciones.titularConsecion == null ||
      this.requisitoRestricciones.titularConsecion == "" ||
      this.requisitoRestricciones.tituloHabilitante == null ||
      this.requisitoRestricciones.tituloHabilitante == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe completar: Requisitos de Restricciones.\n";
    }
    if (
      this.requisitoRestricciones.evaluacionDetalle == null ||
      this.requisitoRestricciones.evaluacionDetalle.length == 0
    ) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe agregar al menos una Restricción: Requisitos de Restricciones.\n";
    }

    //Requisitos de Medidas Correctivas
    if (
      this.requisitoMedidasCorrectivas.comunidad == null ||
      this.requisitoMedidasCorrectivas.comunidad == ""
    ) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe completar: Requisitos de Medidas Correctivas.\n";
    }
    if (
      this.requisitoMedidasCorrectivas.evaluacionDetalle == null ||
      this.requisitoMedidasCorrectivas.evaluacionDetalle.length == 0
    ) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe agregar al menos una Medida Correctiva: Requisitos de Medidas Correctivas.\n";
    }

    if (!validar) this.toast.warn(mensaje);
    return validar;
  }

  registrarRequisitosPlan() {
    this.listaRequisitosModel = [];

    if (this.evaluacionSuscripcion.evaluacion.conforme == true) {
      this.requisitoSuscripcion.evaluacion =
        CodigoPrerequisitos.EVAL_COMPLETADO;
    } else if (this.evaluacionSuscripcion.evaluacion.conforme == false) {
      this.requisitoSuscripcion.evaluacion = CodigoPrerequisitos.EVAL_OBSERVADO;
    }

    this.requisitoSuscripcion.observacion = this.evaluacionSuscripcion.evaluacion.observacion;

    if (this.requisitoSuscripcion.isTitularHabilitado == true) {
      this.requisitoSuscripcion.titularHabilitado = "SI";
    } else if (this.requisitoSuscripcion.isTitularHabilitado == false) {
      this.requisitoSuscripcion.titularHabilitado = "NO";
    }

    if (this.requisitoSuscripcion.isRegenteHabilitado == true) {
      this.requisitoSuscripcion.regenteHabilitado = "SI";
    } else if (this.requisitoSuscripcion.isRegenteHabilitado == false) {
      this.requisitoSuscripcion.regenteHabilitado = "NO";
    }

    //*******************************
    //SETEO DE MEDIDAS CAUTELARES
    //*******************************  //
    //CASO CONTRARIO NULL, NO SELECCIONO NADA
    switch (this.evaluacionMedidasCautelares.evaluacion.conforme) {
      case true: {
        this.requisitoMedidasCautelares.evaluacion =
          CodigoPrerequisitos.EVAL_COMPLETADO;
        break;
      }
      case false: {
        this.requisitoMedidasCautelares.evaluacion =
          CodigoPrerequisitos.EVAL_OBSERVADO;
        break;
      }
    }

    this.requisitoMedidasCautelares.observacion = this.evaluacionMedidasCautelares.evaluacion.observacion;

    this.requisitoMedidasCautelares.evaluacionDetalle.forEach(
      (detalle: any) => {
        switch (detalle.isConformidad) {
          case true: {
            detalle.conformidad = CodigoPrerequisitos.EVAL_COMPLETADO;
            break;
          }
          case false: {
            detalle.conformidad = CodigoPrerequisitos.EVAL_OBSERVADO;
            break;
          }
        }
      }
    );

    ////RESTRICCIONES
    switch (this.evaluacionRestricciones.evaluacion.conforme) {
      case true: {
        this.requisitoRestricciones.evaluacion =
          CodigoPrerequisitos.EVAL_COMPLETADO;
        break;
      }
      case false: {
        this.requisitoRestricciones.evaluacion =
          CodigoPrerequisitos.EVAL_OBSERVADO;
        break;
      }
    }

    this.requisitoRestricciones.observacion = this.evaluacionRestricciones.evaluacion.observacion;

    this.requisitoRestricciones.evaluacionDetalle.forEach((detalle: any) => {
      switch (detalle.isConformidad) {
        case true: {
          detalle.conformidad = CodigoPrerequisitos.EVAL_COMPLETADO;
          break;
        }
        case false: {
          detalle.conformidad = CodigoPrerequisitos.EVAL_OBSERVADO;
          break;
        }
      }
    });

    // MEDIDAS CORRECTIVAS
    switch (this.evaluacionMedidasCorrectivas.evaluacion.conforme) {
      case true: {
        this.requisitoMedidasCorrectivas.evaluacion =
          CodigoPrerequisitos.EVAL_COMPLETADO;
        break;
      }
      case false: {
        this.requisitoMedidasCorrectivas.evaluacion =
          CodigoPrerequisitos.EVAL_OBSERVADO;
        break;
      }
    }

    this.requisitoMedidasCorrectivas.observacion = this.evaluacionMedidasCorrectivas.evaluacion.observacion;

    this.requisitoMedidasCorrectivas.evaluacionDetalle.forEach(
      (detalle: any) => {
        switch (detalle.isConformidad) {
          case true: {
            detalle.conformidad = CodigoPrerequisitos.EVAL_COMPLETADO;
            break;
          }
          case false: {
            detalle.conformidad = CodigoPrerequisitos.EVAL_OBSERVADO;
            break;
          }
        }
      }
    );

    //VIGENCIA DEL PODER
    switch (this.evaluacionVigenciaDePoder.evaluacion.conforme) {
      case true: {
        this.requisitoVigenciaDePoder.evaluacion =
          CodigoPrerequisitos.EVAL_COMPLETADO;
        break;
      }
      case false: {
        this.requisitoVigenciaDePoder.evaluacion =
          CodigoPrerequisitos.EVAL_OBSERVADO;
        break;
      }
    }
    this.requisitoVigenciaDePoder.observacion = this.evaluacionVigenciaDePoder.evaluacion.observacion;

    //VIGENCIA DE JUNTA DIRECTIVA
    switch (this.evaluacionVigenciaJuntaDirectiva.evaluacion.conforme) {
      case true: {
        this.requisitoVigenciaJuntaDirectiva.evaluacion =
          CodigoPrerequisitos.EVAL_COMPLETADO;
        break;
      }
      case false: {
        this.requisitoVigenciaJuntaDirectiva.evaluacion =
          CodigoPrerequisitos.EVAL_OBSERVADO;
        break;
      }
    }
    this.requisitoVigenciaJuntaDirectiva.observacion = this.evaluacionVigenciaJuntaDirectiva.evaluacion.observacion;

    //SUPERFIE DEL AREA
    switch (this.evaluacionSuperficieDelArea.evaluacion.conforme) {
      case true: {
        this.requisitoSuperficieDelArea.evaluacion =
          CodigoPrerequisitos.EVAL_COMPLETADO;
        break;
      }
      case false: {
        this.requisitoSuperficieDelArea.evaluacion =
          CodigoPrerequisitos.EVAL_OBSERVADO;
        break;
      }
    }

    this.requisitoSuperficieDelArea.observacion = this.evaluacionSuperficieDelArea.evaluacion.observacion;

    //setando e model
    this.listaRequisitosModel.push(this.requisitoSuscripcion);
    this.listaRequisitosModel.push(this.requisitoMedidasCautelares);
    this.listaRequisitosModel.push(this.requisitoRestricciones);
    this.listaRequisitosModel.push(this.requisitoMedidasCorrectivas);

    //seteando mensaje
    let msg = "";
    if (
      this.requisitoSuscripcion.evaluacion ==
        CodigoPrerequisitos.EVAL_COMPLETADO &&
      this.requisitoMedidasCautelares.evaluacion ==
        CodigoPrerequisitos.EVAL_COMPLETADO &&
      this.requisitoRestricciones.evaluacion ==
        CodigoPrerequisitos.EVAL_COMPLETADO &&
      this.requisitoMedidasCorrectivas.evaluacion ==
        CodigoPrerequisitos.EVAL_COMPLETADO
    ) {
      msg = "Se registró los requisitos del Plan Operativo correctamente.";
    } else {
      msg =
        "Se registró los requisitos del Plan Operativo, los cuales se encuentran observados.";
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionRequisitosPreliminaresService
      .registrarRequisito(this.listaRequisitosModel)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.success == true) {
          // this.toast.ok(result?.message);
          this.toast.ok(msg);
          this.listarRequisitos();
        } else {
          this.toast.error(result?.message);
        }
      });
  }

  validarTitular() {
    var params = {
      numDNIConsulta: this.requisitoSuscripcion.numeroDocumentoTitular
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService
      .consultarDNI(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (!!response.dataService.datosPersona) {
            //this.toast.ok(response?.dataService.deResultado);
            this.toast.ok("Se validó el titular correctamente.");
            this.requisitoSuscripcion.titular =
              response.dataService.datosPersona.prenombres +
              " " +
              response.dataService.datosPersona.apPrimer +
              " " +
              response.dataService.datosPersona.apSegundo;
            this.requisitoSuscripcion.isTitularHabilitado = true;
            this.requisitoSuscripcion.titularHabilitado =
              CodigoPrerequisitos.EVAL_COMPLETADO;

            this.noValido = false;
          } else {
            this.toast.warn(response?.dataService.deResultado);
            this.noValido = true;
            this.requisitoSuscripcion.titular = "";
            this.requisitoSuscripcion.isTitularHabilitado = false;
            this.requisitoSuscripcion.titularHabilitado =
              CodigoPrerequisitos.EVAL_OBSERVADO;
          }
        } else {
          this.toast.warn(response?.dataService.deResultado);
        }
      });
  }

  validarRegente() {
    this.noValido = false;
    var params = {
      numeroDocumento: this.requisitoSuscripcion.numeroDocumentoRegente,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .obtenerRegente(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (!!response.data.id) {
            if (response.data.estado == "V") {
              this.requisitoSuscripcion.isRegenteHabilitado = true;
              this.requisitoSuscripcion.regenteHabilitado =
                CodigoPrerequisitos.EVAL_COMPLETADO;
            } else {
              this.requisitoSuscripcion.isRegenteHabilitado = false;
              this.requisitoSuscripcion.regenteHabilitado =
                CodigoPrerequisitos.EVAL_OBSERVADO;
            }
            if (!!response.data.nombres && !!response.data.apellidos) {
              this.requisitoSuscripcion.regente = (
                response.data.nombres +
                " " +
                response.data.apellidos
              ).toUpperCase();
            } else if (!!response.data.nombres) {
              this.requisitoSuscripcion.regente = response.data.nombres.toUpperCase();
            } else if (!!response.data.apellidos) {
              this.requisitoSuscripcion.regente = response.data.apellidos.toUpperCase();
            }
            this.toast.ok("Se validó el Regente correctamente.");
            this.noValido = false;
          } else {
            this.toast.warn("No se encontro el Documento");
            this.requisitoSuscripcion.regente = "";
            this.noValido = true;
            this.requisitoSuscripcion.isRegenteHabilitado = false;
            this.requisitoSuscripcion.regenteHabilitado =
              CodigoPrerequisitos.EVAL_OBSERVADO;
          }
        } else {
          this.toast.error("Ocurrió un error al realizar la operación.");
        }
      });
  }

  evaluacion(conforme: boolean, type: string) {
    switch (type) {
      case "suscription":
        this.evaluacionSuscripcion.icon = !!conforme
          ? "pi-check-circle"
          : "pi-info-circle";
        this.evaluacionSuscripcion.classEstado = !!conforme
          ? "req_conforme_class"
          : "req_observado_class";
        break;
      case "medidasCautelares":
        this.evaluacionMedidasCautelares.icon = !!conforme
          ? "pi-check-circle"
          : "pi-info-circle";
        this.evaluacionMedidasCautelares.classEstado = !!conforme
          ? "req_conforme_class"
          : "req_observado_class";
        break;
      case "restricciones":
        this.evaluacionRestricciones.icon = !!conforme
          ? "pi-check-circle"
          : "pi-info-circle";
        this.evaluacionRestricciones.classEstado = !!conforme
          ? "req_conforme_class"
          : "req_observado_class";
        break;
      case "medidasCorrectivas":
        this.evaluacionMedidasCorrectivas.icon = !!conforme
          ? "pi-check-circle"
          : "pi-info-circle";
        this.evaluacionMedidasCorrectivas.classEstado = !!conforme
          ? "req_conforme_class"
          : "req_observado_class";
        break;
      default:
        this.evaluacionSuperficieDelArea.icon = !!conforme
          ? "pi-check-circle"
          : "pi-info-circle";
        this.evaluacionSuperficieDelArea.classEstado = !!conforme
          ? "req_conforme_class"
          : "req_observado_class";
        break;
    }
  }
}
