import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UsuarioService} from '@services';
import {Perfiles} from '../../../model/util/Perfiles';
import {UsuarioModel} from '../../../model/seguridad/usuario';

@Component({
  selector: 'app-gestion-evaluacion-plan-operativo',
  templateUrl: './gestion-evaluacion-plan-operativo.component.html',
  styleUrls: ['./gestion-evaluacion-plan-operativo.component.scss']
})
export class GestionEvaluacionPlanOperativoComponent implements OnInit {

  idPlanManejo!: number;

  usuario!: UsuarioModel;
  isShowTabMesaDePartes : boolean = false;
  isShowTabRequisitos : boolean = false;
  isShowMedidasCorrectivas : boolean = false;


  constructor(private route: ActivatedRoute,private usuarioService: UsuarioService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.usuario = this.usuarioService.usuario;
  }

  ngOnInit(): void {
    if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      this.isShowTabMesaDePartes = true;
      this.isShowTabRequisitos = false;
      this.isShowMedidasCorrectivas = false;
    } else {
      this.isShowTabMesaDePartes = false;
      this.isShowTabRequisitos = true;
      this.isShowMedidasCorrectivas = true;
    }

  }


}
