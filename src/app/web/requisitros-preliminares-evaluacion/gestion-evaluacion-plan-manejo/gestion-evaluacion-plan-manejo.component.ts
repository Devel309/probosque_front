import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Perfiles} from '../../../model/util/Perfiles';
import {UsuarioService} from '@services';
import {UsuarioModel} from '../../../model/seguridad/usuario';

@Component({
  selector: 'app-gestion-evaluacion-plan-manejo',
  templateUrl: './gestion-evaluacion-plan-manejo.component.html',
  styleUrls: ['./gestion-evaluacion-plan-manejo.component.scss']
})
export class GestionEvaluacionPlanManejoComponent implements OnInit {

  idPlanManejo!: number;
  usuario!: UsuarioModel;
  isShowTabMesaDePartes : boolean = false;
  isShowTabRequisitos : boolean = false;
  isShowMedidasCorrectivas : boolean = false;


  ngOnInit(): void {
    if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      this.isShowTabMesaDePartes = true;
      this.isShowTabRequisitos = false;
      this.isShowMedidasCorrectivas = false;
    } else {
      this.isShowTabMesaDePartes = false;
      this.isShowTabRequisitos = true;
      this.isShowMedidasCorrectivas = true;
    }
  }

  constructor(private route: ActivatedRoute,private usuarioService: UsuarioService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.usuario = this.usuarioService.usuario;
  }


}
