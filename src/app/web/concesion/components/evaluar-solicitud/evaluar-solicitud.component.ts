import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-evaluar-solicitud',
  templateUrl: './evaluar-solicitud.component.html',
  styleUrls: ['./evaluar-solicitud.component.scss']
})
export class EvaluarSolicitudComponent implements OnInit {
  @Input() disabled: boolean = false;
  @Input() idPFDM: number = 0;
  @Input() codSeccion: string = "";
  @Input() codSubSeccion: string = "";

  usuario!: UsuarioModel;
  requestEvaluar: any = {};

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private solicitudConcesionService: SolicitudConcesionService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.requestEvaluar.idSolicitudConcesionEvaluacion = null;
    this.requestEvaluar.idSolicitudConcesionEvaluacionDetalle = null;
    this.requestEvaluar.codigoSeccion = this.codSeccion;
    this.requestEvaluar.codigoSubSeccion = this.codSubSeccion;
    this.requestEvaluar.conforme = null;
    this.requestEvaluar.observacion = null;

    if (this.idPFDM) this.obtenerInfo();
  }

  //SERVICIOS
  obtenerInfo() {
    const params = { "idSolicitudConcesion": this.idPFDM }
    this.solicitudConcesionService.obtenerInfoSolConEval(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.requestEvaluar.idSolicitudConcesionEvaluacion = resp.data[0].idSolicitudConcesionEvaluacion;
        this.listar();
      }
    });
  }

  listar(load: boolean = false) {
    const params = {
      "idSolicitudConcesionEvaluacion": this.requestEvaluar.idSolicitudConcesionEvaluacion,
      "codigoSeccion": this.requestEvaluar.codigoSeccion,
      "codigoSubSeccion": this.requestEvaluar.codigoSubSeccion
    }

    this.solicitudConcesionService.listarSolConEvalDetalle(params).subscribe(resp => {
      if(load) this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.requestEvaluar.conforme = resp.data[0].conforme;
        this.requestEvaluar.observacion = resp.data[0].observacion;
        this.requestEvaluar.idSolicitudConcesionEvaluacionDetalle = resp.data[0].idSolicitudConcesionEvaluacionDetalle;
      }
    }, (error) => this.errorMensaje(error, load));
  }

  registrar() {
    this.requestEvaluar.idUsuarioRegistro = this.usuario.idusuario;
    let params = [];
    params.push(this.requestEvaluar);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarSolConEvalDetalle(params).subscribe(resp => {
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listar(true);
      } else {
        this.dialog.closeAll();
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizar() {
    this.requestEvaluar.idUsuarioModificacion = this.usuario.idusuario;
    let params = [];
    params.push(this.requestEvaluar);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarSolConEvalDetalle(params).subscribe(resp => {
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listar(true);
      } else {
        this.dialog.closeAll();
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnGuardar() {
    if (!this.validar()) return;

    if (this.requestEvaluar.idSolicitudConcesionEvaluacionDetalle) {
      this.actualizar();
    } else {
      this.registrar();
    }
  }

  validar(): boolean {
    let validado = true;
    if (!(this.requestEvaluar.conforme === true || this.requestEvaluar.conforme === false)) {
      validado = false;
      this.toast.warn("(*) Debe de seleccionar una opción.");
    } else {

      if (this.requestEvaluar.conforme === false && !this.requestEvaluar.observacion) {
        validado = false;
        this.toast.warn("(*) Debe ingresar la descripción.");
      }

      if (this.requestEvaluar.conforme === true) this.requestEvaluar.observacion = "";
    }

    return validado;
  }

  //FUNCIONES
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
