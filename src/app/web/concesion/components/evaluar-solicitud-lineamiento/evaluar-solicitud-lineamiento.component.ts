import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, ParametroValorService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { ValidarCondicionMinimaComponent } from 'src/app/shared/components/validar-condicion-minima/validar-condicion-minima.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { finalize } from 'rxjs/operators';
import { EntidadService } from 'src/app/service/entidad.service';
import { GenericoService } from 'src/app/service/generico.service';
import { MessageService } from 'primeng/api';
import { SolicitudOpinionRegistro } from 'src/app/model/solicitud-opinion';
import { SolicitudOpinionService } from 'src/app/service/solicitud-opinion.service';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionEvaluacionDetalleService } from 'src/app/service/concesion/solicitud-concesion-evaluluacion-detalle.service';
import { SolicitudConcesionEvaluacionService } from 'src/app/service/concesion/solicitud-concesion-evaluacion.service';
import {CargaArchivoGeneralComponent} from "../../../../shared/components/formulario/carga-archivo-general/carga-archivo-general.component"
@Component({
  selector: 'app-evaluar-solicitud-lineamiento',
  templateUrl: './evaluar-solicitud-lineamiento.component.html',
  styleUrls: ['./evaluar-solicitud-lineamiento.component.scss']
})
export class EvaluarSolicitudLineamientoComponent implements OnInit {
  @Input() idPFDM!: number;
  @Input() disabled: boolean = false;
  @Output() validarSuperposicionEvent = new EventEmitter<any>();

  @ViewChild(CargaArchivoGeneralComponent) cargaArchivoGeneralComponent!: CargaArchivoGeneralComponent;

  usuario!: UsuarioModel;
  idArchivo: number = 0;
  idEvaluacionSolicitudConcesionParamIn: number = 0;
  idArchivoSustentoEvaluacion!: number;
  comboCategoria: any[] = [];
  listaEvaluacion: any[] = [];
  comboEntidad: any[] = [];
  comboTipoDocumento: any = [];
  lstVerficarPGFM: any[] = [];
  lstUbicacionEspecial: any[] = [];

  modelCategoria: any = null;
  modelCheckCateg: any = null;
  modelCheckSer: any = null;
  modelCheckAna: any = null;
  ref!: DynamicDialogRef;
  informacionSolicitante: any;
  verModalSolicitud: boolean = false;
  displayConfirm: boolean = false;

  opinion: any = {};
  archivoAdjunto: any = {};
  informacion: any = {};
  idDocumentoAdjunto!: number;
  idPlanManejoEvaluacion!: number;
  mensajeConfirmEnviar: string = "¿Esta seguro de enviar la evaluación?";

  evaluacionDetalleRequestEntity: any = {};
  isDisabledAna: boolean = false;
  isDisabledSer: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private toast: ToastService,
    private dialog: MatDialog,
    private solicitudConcesionService: SolicitudConcesionService,
    public dialogService: DialogService,
    private archivoServ: ArchivoService,
    private entidadServ: EntidadService,
    private genericoServ: GenericoService,
    private messageService: MessageService,
    private opinionServ: SolicitudOpinionService,
    private planManejoEvServ: PlanManejoEvaluacionService,
    private solicitudConcesionEvaluacionDetalleService: SolicitudConcesionEvaluacionDetalleService,
    private solicitudConcesionEvaluacionService: SolicitudConcesionEvaluacionService,
    private usuarioServ: UsuarioService,
    private parametroValorService: ParametroValorService
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.idPFDM = this.route.snapshot.paramMap.get('idParam') ? Number(this.route.snapshot.paramMap.get('idParam')) : 0;
   }

  ngOnInit(): void {
    this.listarInfoSolicita();
    this.listarComboCategoria();
    if(this.idPFDM) this.obtenerInfoEvaluacion();
    this.listarComboEntidad();
    this.listarComboTipoDocumentos();
    this.cargarElementosDetalleEvaluacion();
  }

  //SERVICIOS
  listarComboCategoria() {
    const params = { prefijo: "CZONI" };
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe((response: any) => {
      if(response.success) this.comboCategoria = response.data;
    });
  }

  cargarElementosDetalleEvaluacion() {

    var params = { prefijo: "LINSUP" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {

        let e : any = {};
        response.data.forEach((element:any) => {
          e = {};
          e.item = element.codigo.substring(6,7);
          e.conforme= null
          e.observacion= null
          e.codigo = element.codigo;
          e.idLineamiento= null
          e.descripcion= element.valor1

          this.listaEvaluacion.push(e);
       });

      });
  }

  obtenerSolicitudConcesionEvaluacionDetalle() {
    const params = {
      idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
      codigoSeccion: CodigosPFDM.TAB_1,
      codigoSubSeccion: CodigosPFDM.TAB_1_3
     };
    this.solicitudConcesionEvaluacionDetalleService.listarSolicitudConcesionEvaluacionDetalle(params).subscribe(
      (result: any) => {
        this.evaluacionDetalleRequestEntity.data = result.data;
        for (let i = 0; i < this.listaEvaluacion.length; i++) { 
          for(let j = 0; j < result.data.length; j++){
            if(this.listaEvaluacion[i].codigo === result.data[j].codigoLineamiento){
              this.listaEvaluacion[i].conforme = result.data[j].conforme ? result.data[j].conforme : result.data[j].conforme;
              this.listaEvaluacion[i].observacion = result.data[j].conforme ? null : result.data[j].observacion;
              this.listaEvaluacion[i].idLineamiento = result.data[j].idSolicitudConcesionEvaluacionDetalle;
            }
          }
        }
      }
    );
  };

  //BOTONES
  validarSuperposicion() {
    this.validarSuperposicionEvent.emit('Click');

  }

  btnGuardarEval() {
    if (!this.validarGuardarEval()) return;
    this.actualizarInfoEvaluacion();
    var listaReg: any[]= [];
    if(this.evaluacionDetalleRequestEntity.data.length == 0){
      for (let index = 0; index < this.listaEvaluacion.length; index++) {    
        let element: any = {
          idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
          idSolicitudConcesion : this.idPFDM,
          codigoSeccion: CodigosPFDM.TAB_1,
          codigoSubSeccion: CodigosPFDM.TAB_1_3,
          conforme: this.listaEvaluacion[index].conforme,
          lineamiento: this.listaEvaluacion[index].codigo ? true : false,
          codigoLineamiento: this.listaEvaluacion[index].codigo,
          observacion: this.listaEvaluacion[index].observacion,
          idUsuarioRegistro: this.usuario.idusuario
        }
        listaReg.push(element);
      }
      
      this.solicitudConcesionEvaluacionDetalleService.registrarSolicitudConcesionEvaluacionDetalle(listaReg).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {
          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      });
    }else{
      for (let index = 0; index < this.listaEvaluacion.length; index++) {    
        let element: any = {
          idSolicitudConcesionEvaluacionDetalle: this.listaEvaluacion[index].idLineamiento,
          idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
          idSolicitudConcesion : this.idPFDM,
          codigoSeccion: CodigosPFDM.TAB_1,
          codigoSubSeccion: CodigosPFDM.TAB_1_3,
          conforme: this.listaEvaluacion[index].conforme,
          lineamiento: this.listaEvaluacion[index].codigo ? true : false,
          codigoLineamiento: this.listaEvaluacion[index].codigo,
          observacion: this.listaEvaluacion[index].observacion,
          idUsuarioModificacion: this.usuario.idusuario
        }
        listaReg.push(element);
      }
      
      this.solicitudConcesionEvaluacionDetalleService.actualizarSolicitudConcesionEvaluacionDetalle(listaReg).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {
          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      });
    }
  }
  
  enviarSolcitud(){
    if (!this.validarSolicitudOpinion()) return;


    this.dialog.open(LoadingComponent, { disableClose: true });

    let params: SolicitudOpinionRegistro = {
      asunto: this.opinion.asunto,
      estSolicitudOpinion: "ESOPPEND",
      fechaDocGestion: this.opinion.fechaDocGestion,
      //fileDocumentoGestion: this.archivoAdjunto.file,
      idUsuarioRegistro: this.usuario.idusuario,
      nroDocGestion: this.idPFDM,
      numeroDocumento: this.opinion.numeroDocumento,
      siglaEntidad: this.opinion.siglaEntidad,
      tipoDocGestion: "TPMPFDM",
      tipoDocumento: this.opinion.tipoDocumento,
      documentoGestion: this.idDocumentoAdjunto
    }


    this.opinionServ.registrarSolicitudOpinion(params)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((res: any) => {
      if (res.success) {
        this.SuccessMensaje(res.message);
        this.verModalSolicitud = false;
        if(res.data.siglaEntidad === "ANA") {
          this.isDisabledAna = true;
        }else if(res.data.siglaEntidad === "SERNANP"){
          this.isDisabledSer = true;
        }
        this.actualizarIdSolicitudOpinion(res.data.idSolicitudOpinion, res.data.siglaEntidad);
      }
    }, error => {
      this.ErrorMensaje("Ocurrió un error");
    });

  }
  actualizarIdSolicitudOpinion(idSolicitudOpinion: number, tipoSolicitud: string) {

    let params = {
      idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
      idSolicitudOpinion: idSolicitudOpinion,
      tipoSolicitud: tipoSolicitud
    }

    this.planManejoEvServ.actualizarIdSolicitudOpinionPlanManejoEvaluacion(params).subscribe((result: any) => {
      if (result.success) {
        if (tipoSolicitud == "MINCUL") {
          this.informacion.idMincul = 1;
        }

        if (tipoSolicitud == "ANA") {
          this.informacion.idAna = 1;
        }

        if (tipoSolicitud == "SERNANP") {
          this.informacion.idSernap = 1;
        }

      } else {
        this.ErrorMensaje("Ocurrió un Error al actualizar la solicitud")
      }
    }, error => {
      this.ErrorMensaje("Ocurrió un error")
    })

  }

  cerrarModal(){
    this.verModalSolicitud = false;
  }

  btnValidarCM() {
    let tipoDocumentoSolicitante ="";
    if(this.informacionSolicitante.tipoDocumentoSolicitante == 'TDOCRUC'){
      tipoDocumentoSolicitante = "RUC";
    }else if (this.informacionSolicitante.tipoDocumentoSolicitante == 'TDOCDNI'){
      tipoDocumentoSolicitante = "DNI";
    }


    let param = {
      nombre: this.informacionSolicitante.nombreSolicitanteUnico,
      apellidoPaterno: "",
      apellidoMaterno: "",
      tipoDocumento: tipoDocumentoSolicitante,
      numeroDocumento: this.informacionSolicitante.numeroDocumentoSolicitanteUnico
    };

    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: 'Validar Condiciones Mínimas',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        idCondicionMinima: null,
        obj: param
      },
    });
  }

  listarInfoSolicita() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        
        this.informacionSolicitante = resp.data[0];

        //this.setDataSolicita(datos);
        //this.listarPorFiltroDepartamento(false, 1);
      }
    }, (error) => this.toast.error(error));
  }

  registrarArchivo(idArchivo: number) {
    this.idArchivoSustentoEvaluacion = idArchivo;
  }

  eliminarArchivo(val: any) {
    this.idArchivoSustentoEvaluacion =0;
  }

  //FUNCIONES
  changeCheckCat() {
    if (this.modelCheckCateg) {
      this.modelCategoria = null;
    }else{
      this.cargaArchivoGeneralComponent.eliminar()
    }
  }

  listarComboEntidad() {
    this.entidadServ.listaComboEntidad({}).subscribe((result: any) => {
      this.comboEntidad = result.data;
    })
  }

  listarComboTipoDocumentos() {
    let params = { prefijo: 'TDOCGE' }

    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboTipoDocumento = result.data;
    })
  }

  abrirModalSolicitud(entidad: string) {
    this.opinion = {};
    this.opinion.siglaEntidad = entidad;
    this.archivoAdjunto = {};

    this.verModalSolicitud = true;

    // this.listarArchivosAsociados();
    // this.fileFirmadoPGMF.inServer = false;
    // this.fileFirmadoPGMF.descripcion = "PDF";
  }




  registrarAdjunto(file: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ.cargar(this.usuario.idusuario, "TPMPFDM", file.file)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any) => {

      if (result.success) {

        this.idDocumentoAdjunto = result.data;
        this.toast.ok("Se registró el archivo correctamente.");
      }
    }, error => {
      this.toast.error("Ocurrió un error al realizar la operación");
    });
  }

  eliminarAdjunto(){

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ.eliminarArchivo(this.idDocumentoAdjunto, this.usuario.idusuario)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any) => {

      if (result.success) {

        this.idDocumentoAdjunto = 0;
        this.toast.ok("Se eliminó el archivo correctamente");
      }
    }, error => {
      this.toast.error("Ocurrió un error al realizar la operación");
    });

    this.archivoAdjunto = {};
  }

  clickValidarRadio(fila: any) {
    if (fila.conforme) fila.observacion = "";

    // let auxTodoOk = this.listaDocumentos.every((item: any) => (item.conforme === true));
    // if (auxTodoOk) this.tieneObs = false;
    // else this.tieneObs = true;
  }

  validarGuardarEval(): boolean {
    let validado = true;
    let hayNulos = this.listaEvaluacion.some((item: any) => item.conforme === null);
    let isConformeFalseSinObs = this.listaEvaluacion.some((item: any) => (item.conforme === false && !item.observacion));

    if (hayNulos) {
      validado = false;
      this.toast.warn("(*) Existen items sin evaluar");
    } else if (isConformeFalseSinObs) {
      validado = false;
      this.toast.warn("(*) Debe ingresar las observaciones");
    }

    return validado;
  }

  obtenerInfoEvaluacion() {

    let request = {
      idSolicitudConcesion : this.idPFDM
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionEvaluacionService.obtenerInfoEvaluacionSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        if(resp.data && resp.data.length > 0){
          this.idEvaluacionSolicitudConcesionParamIn = resp.data[0].idSolicitudConcesionEvaluacion;
          this.modelCategoria = resp.data[0].codigoCatZonificacion;
          this.modelCheckCateg = resp.data[0].sinCategoria;
          this.modelCheckSer = resp.data[0].solicitaOpinionSernamp;
          this.modelCheckAna = resp.data[0].solicitaOpinionAna;

          this.isDisabledSer = resp.data[0].solicitaOpinionSernamp;
          this.isDisabledAna = resp.data[0].solicitaOpinionAna;
          this.idArchivoSustentoEvaluacion= resp.data[0].idArchivoSutento;
        }
        this.obtenerSolicitudConcesionEvaluacionDetalle();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  actualizarInfoEvaluacion() {

    let request = {
      idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
      idSolicitudConcesion : this.idPFDM,
      codigoCatZonificacion: this.modelCategoria,
      idArchivoSutento: this.idArchivoSustentoEvaluacion,
      sinCategoria:  this.modelCheckCateg,
      solicitaOpinionSernamp : this.modelCheckSer,
      solicitaOpinionAna : this.modelCheckAna,
      idUsuarioModificacion : this.usuario.idusuario
    }
    
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionEvaluacionService.actualizarInfoEvaluacionSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
  validarSolicitudOpinion() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.opinion.siglaEntidad) {
      validar = false;
      mensaje = mensaje +=
        '(*) Seleccione entidad.\n';
    }

    if (!this.opinion.tipoDocumento) {
      validar = false;
      mensaje = mensaje +=
        '(*) Seleccione tipo documento.\n';
    }
    if (!this.opinion.fechaDocGestion) {
      validar = false;
      mensaje = mensaje +=
        '(*) Ingrese fecha documento.\n';
    }

    if (!this.opinion.asunto) {
      validar = false;
      mensaje = mensaje +=
        '(*) Ingrese asunto.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }
}
