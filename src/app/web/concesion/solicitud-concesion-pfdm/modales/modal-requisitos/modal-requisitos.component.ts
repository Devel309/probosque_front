import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { ConvertNumberToDate, DownloadFile, ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-modal-requisitos',
  templateUrl: './modal-requisitos.component.html',
  styleUrls: ['./modal-requisitos.component.scss']
})
export class ModalRequisitosComponent implements OnInit {

  requestModal: any = {};
  listaDocumentos = [];
  disabledIN: boolean = false;
  tieneObs: boolean | null = null;
  isClickBtnGuadar: boolean = false;
  isClickBtnSubsanar: boolean = false;
  isClickBtnEvaluar: boolean = false;

  showBtnSubsanar: boolean = false;
  showBtnEvaluar: boolean = false;

  constructor(
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private toast: ToastService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private solicitudConcesionService: SolicitudConcesionService,
  ) {
    this.disabledIN = this.config.data.isDisabled;
  }

  ngOnInit(): void {
    this.requestModal.idSolicitudConcesion = this.config.data.idSolConcesion;
    this.requestModal.idUsuarioModificacion = this.config.data.idUser;
    this.obtenerConcesion();
    this.obtenerDocumentos();
  }

  //SERVICIOS
  obtenerConcesion() {
    let param = { idSolicitudConcesion: this.requestModal.idSolicitudConcesion };
    this.solicitudConcesionService.listarSolicitudConcesion(param).subscribe(resp => {
      if (resp.success && resp.data) {
        this.requestModal.idSolicitante = resp.data[0].idSolicitante;
        this.requestModal.nroTramite = resp.data[0].nroTramite;
        this.requestModal.fechaTramite = resp.data[0].fechaTramite ? ConvertNumberToDate(resp.data[0].fechaTramite) : null;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error));
  }

  obtenerDocumentos() {
    this.listaDocumentos = [];
    const param = { idSolicitudConcesion: this.requestModal.idSolicitudConcesion };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.listaDocumentos = resp.data.map((item: any) => {
          return { ...item, "idUsuarioModificacion": this.requestModal.idUsuarioModificacion }
        });

        this.validarBotones();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  validarBotones() {
    let allNull = this.listaDocumentos.every((item: any) => item.conforme === null);
    let allconforme = this.listaDocumentos.every((item: any) => item.conforme === true);
    
    if (allNull) {
      this.showBtnEvaluar = false;
      this.showBtnSubsanar = false;
    }
    else if (allconforme) {
      this.showBtnEvaluar = true;
      this.showBtnSubsanar = false;
    } else {
      this.showBtnSubsanar = true;
      this.showBtnEvaluar = false;
    }
  }

  descargar(id: number) {
    if (id) {
      const params = { idArchivo: id };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  actualizarSolicitud() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarSolicitudConcesion(this.requestModal).subscribe(resp => {
      if (resp.success) {
        this.crearObserverDocuemntos(resp.message);
      } else {
        this.dialog.closeAll();
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  crearObserverDocuemntos(mensaje: string = "") {
    let auxListaDoc = this.listaDocumentos.filter((item: any) => item.conforme !== null);
    let observer = from(auxListaDoc);

    observer.pipe(concatMap((item: any) => this.actualizarDocuemntos(item))).pipe(finalize(() => {
      if (this.isClickBtnSubsanar) {
        this.enviarSolicitud("SUBSANAR");
      } else if (this.isClickBtnEvaluar) {
        this.enviarSolicitud("EVALUADOR");
      } else {
        this.dialog.closeAll();
        this.toast.ok(mensaje);
        this.obtenerConcesion();
        this.obtenerDocumentos();
      }
    })).subscribe((result: any) => {
      // 
    }, (error) => this.errorMensaje(error, true)
    );
  }

  actualizarDocuemntos(item: any) {
    return this.solicitudConcesionService.actualizarInfoAdjuntosSolicitudConcesion(item);
  }

  enviarSolicitud(tipoEnvio: string) {
    const params = {
      "idSolicitudConcesion": this.requestModal.idSolicitudConcesion,
      "tipoEnvio": tipoEnvio,
      "idUsuarioModificacion": this.requestModal.idUsuarioModificacion
    };
    this.solicitudConcesionService.enviarSolicitudConcesion(params).subscribe(resp => {
      this.dialog.closeAll();
      this.limpiarClicks();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.ref.close(true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => {
      this.errorMensaje(error, true);
      this.limpiarClicks();
    });
  }

  //BOTONES
  btnDescargar(idFile: any) {
    this.descargar(idFile)
  }

  btnSubsanar() {
    if (!this.validarEnviar()) return;
    
    this.isClickBtnSubsanar = true;
    this.actualizarSolicitud();
  }

  btnEvaluador() {
    if (!this.validarEnviar()) return;
    
    this.isClickBtnEvaluar = true;
    this.actualizarSolicitud();
  }

  btnGuardar() {
    if (!this.validarGuardar()) return;
    this.actualizarSolicitud();
  }

  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  limpiarClicks() {
    this.isClickBtnSubsanar = false;
    this.isClickBtnEvaluar = false;
  }

  validarGuardar(): boolean {
    let validado = true;
    if (!this.requestModal.nroTramite) {
      validado = false;
      this.toast.warn("(*) Debe ingresar el número de trámite.");
    }

    if (!this.requestModal.fechaTramite) {
      validado = false;
      this.toast.warn("(*) Debe ingresar la fecha de trámite.");
    }

    let allNull = this.listaDocumentos.every((item: any) => item.conforme === null);
    let isConformeFalseSinObs = this.listaDocumentos.some((item: any) => (item.conforme === false && !item.observacion));

    if (allNull) {
      validado = false;
      this.toast.warn("(*) Debe seleccionar al menos un registro.");
    }

    if (isConformeFalseSinObs) {
      validado = false;
      this.toast.warn("(*) Debe ingresar las observaciones");
    }

    return validado;
  }

  validarEnviar(): boolean {
    let validado = true;
    if (!this.requestModal.nroTramite) {
      validado = false;
      this.toast.warn("(*) Debe ingresar el número de trámite.");
    }

    if (!this.requestModal.fechaTramite) {
      validado = false;
      this.toast.warn("(*) Debe ingresar la fecha de trámite.");
    }

    let hayNull = this.listaDocumentos.some((item: any) => item.conforme === null);
    let isConformeFalseSinObs = this.listaDocumentos.some((item: any) => (item.conforme === false && !item.observacion));

    if (hayNull) {
      validado = false;
      this.toast.warn("(*) Debe dar conformidad a todos los registros.");
    }

    if (isConformeFalseSinObs) {
      validado = false;
      this.toast.warn("(*) Debe ingresar las observaciones");
    }

    return validado;
  }

  clickValidarRadio(fila: any) {
    if (fila.conforme) fila.observacion = "";

    let auxTodoOk = this.listaDocumentos.every((item: any) => (item.conforme === true));
    if (auxTodoOk) {
      this.showBtnEvaluar = true;
      this.showBtnSubsanar = false;
    } else {
      this.showBtnSubsanar = true;
      this.showBtnEvaluar = false;
    }
  }

  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
