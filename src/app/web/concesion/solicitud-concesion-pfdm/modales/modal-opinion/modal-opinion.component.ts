import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ListarSolicitudOpinionRequest } from 'src/app/model/solicitud-opinion';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { SolicitudOpinionService } from 'src/app/service/solicitud-opinion.service';

@Component({
  selector: 'app-modal-opinion',
  templateUrl: './modal-opinion.component.html',
  styleUrls: ['./modal-opinion.component.scss']
})
export class ModalOpinionComponent implements OnInit {
  disabledIN: boolean = false;
  listaOpinion: any[] = [{ id: 111, entidad: "ANA", fecha: "21/01/2022 9:00AM", estado: "Pendiente" }];;
  listarSolicitudOpinionRequest: ListarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();
  usuario!: UsuarioModel;
  tipoDocGestion:any = "TPMPFDM";

  constructor(
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private toast: ToastService,
    private dialog: MatDialog,
    private solicitudConcesionService: SolicitudConcesionService,
    private usuarioServ: UsuarioService,
    private solicitudOpinionServ: SolicitudOpinionService,
    private router: Router, 
  ) {
    this.disabledIN = this.config.data.isDisabled;
  }

  ngOnInit(): void {
    
    this.usuario = this.usuarioServ.usuario;
    this.listarSolicitudOpinion()
    //this.listar();
  }

  btnVerSolicitud(fila: any) {
    this.ref.close();
    const uri = "planificacion/bandeja-solicitud-opinion";
    this.router.navigate([uri]);
  }

  btnCancelar() {
    this.ref.close();
  }

  listar() {

    //this.listaOpinion = [{ id: 111, entidad: "ANA", fecha: "21/01/2022 9:00AM", estado: "Pendiente" }];
  }

  listarSolicitudOpinion(){
    this.dialog.open(LoadingComponent, { disableClose: true });
    
    this.listarSolicitudOpinionRequest.siglaEntidad = this.usuario.sirperfil;
    this.listarSolicitudOpinionRequest.nroDocGestion = this.config.data.idSolConcesion;
    this.listarSolicitudOpinionRequest.tipoDocGestion = this.tipoDocGestion;

    this.solicitudOpinionServ.listarSolicitudOpinion(this.listarSolicitudOpinionRequest).subscribe((result: any) => {
      if (result.data) {
        
        this.listaOpinion = [...result.data];
      }
      this.dialog.closeAll();
    })
  }

}
