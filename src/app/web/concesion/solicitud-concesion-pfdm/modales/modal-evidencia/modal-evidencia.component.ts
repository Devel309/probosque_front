import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-modal-evidencia',
  templateUrl: './modal-evidencia.component.html',
  styleUrls: ['./modal-evidencia.component.scss']
})
export class ModalEvidenciaComponent implements OnInit {
  isDisabled: boolean = false;
  isPerArrf: boolean = false;
  labelBtnguardar: string = "Guardar y Enviar";
  evidencia: any = {};
  mostrarBtnGuardar: boolean = true;

  constructor(
    private ref: DynamicDialogRef,
    private dialog: MatDialog,
    private config: DynamicDialogConfig,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private solicitudConcesionService: SolicitudConcesionService,
  ) { }

  ngOnInit(): void {
    if (this.config.data.perfil === Perfiles.AUTORIDAD_REGIONAL) {
      this.isDisabled = true;
      this.isPerArrf = true;
      this.labelBtnguardar = "Guardar";
    } else if (this.config.data.perfil === Perfiles.TITULARTH) {
      this.isDisabled = !!this.config.data?.item?.nombreEvidencia;
      this.mostrarBtnGuardar = !this.isDisabled;
    }

    this.evidencia.idSolicitudConcesion = this.config.data?.item?.idConcesion;
    this.evidencia.idArchivoEvidencia = this.config.data?.item?.idArchivoEvidencia;
    this.evidencia.nombreEvidencia = this.config.data?.item?.nombreEvidencia;
    this.evidencia.descripcionEvidencia = this.config.data?.item?.descripcionEvidencia;
    this.evidencia.evidenciaRemitida = true;
    this.evidencia.verificadoAcreditaEvidencia = this.config.data?.item?.verificadoAcreditaEvidencia;
    this.evidencia.observacionAcreditaEvidencia = this.config.data?.item?.observacionAcreditaEvidencia || '';
  }

  registrarArchivo(idArchivo: number) {
    this.evidencia.idArchivoEvidencia = idArchivo;
  }

  eliminarArchivo(ok: boolean) {
    this.evidencia.idArchivoEvidencia = 0;
  }

  btnGuardar(event: any) {

    if (!this.isPerArrf && !this.validarGuardar()) return;

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Desea acreditar la evidencia de publicación?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.guardar();
      }
    });
  }

  btnCerrar() {
    this.ref.close();
  }

  guardar(): void {
    this.solicitudConcesionService.actualizarSolicitudConcesion(this.evidencia).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.ref.close(true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  validarGuardar() {
    let valido = true;
    if (!this.evidencia.nombreEvidencia) {
      valido = false;
      this.toast.warn("(*) Debe ingresar Nombre de Archivo.");
    }
    if (!this.evidencia.descripcionEvidencia) {
      valido = false;
      this.toast.warn("(*) Debe ingresar Descripción.");
    }
    if (!this.evidencia.idArchivoEvidencia) {
      valido = false;
      this.toast.warn("(*) Debe adjuntar archivo.");
    }
    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}

