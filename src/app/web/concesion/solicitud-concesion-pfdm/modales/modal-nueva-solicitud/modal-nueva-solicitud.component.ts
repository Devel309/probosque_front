import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoConcesion } from 'src/app/model/Concesion/CodigoEstadoConcesion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-modal-nueva-solicitud',
  templateUrl: './modal-nueva-solicitud.component.html',
  styleUrls: ['./modal-nueva-solicitud.component.scss']
})
export class ModalNuevaSolicitudComponent implements OnInit {

  @Input() data: any;

  modelTipoSolicitud: any = "1";
  modelSelectRadio: any = null;
  listaSolicitudes: any[]=[];
  usuario!: UsuarioModel;
  requestFiltro: any = {};
  optionPage: any = { pageNum: 1, pageSize: 10, totalRecords: 0 };

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private router: Router,
    private solicitudConcesionService: SolicitudConcesionService,
  ) {
    this.usuario = this.usuarioServ.usuario;

   }

  ngOnInit(): void {
    this.listar(); 
  }

  registrarSolicitudConcecionNueva() {
    const params = { "idSolicitudConcesion": 0, "idSolicitante": this.config.data.idUser };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInformacionSolicitante(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.toast.ok(resp.message);
        let auxOut = {id: resp.data?.idSolicitudConcesion, isAsoc: false};
        this.ref.close(auxOut);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarSolicitudConcecionAsociada() {
    const params = { "idSolicitudConcesion": 0, "idSolicitante": this.config.data.idUser, "idSolicitudConcesionPadre": this.modelSelectRadio};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInformacionSolicitante(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.toast.ok(resp.message);
        let auxOut = {id: resp.data?.idSolicitudConcesion, isAsoc: true};
        this.ref.close(auxOut);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnCrearNueva(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: 'Esta acción creará una nueva solicitud. ¿Desea continuar?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.registrarSolicitudConcecionNueva();
      },
    });
  }

  btnCrearAsoc(event: any) {
    if(!this.modelSelectRadio) {
      this.toast.warn("(*) Debe Seleccionar un registro.");
      return;
    }

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: 'Esta acción creará una solicitud asociada. ¿Desea continuar?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.registrarSolicitudConcecionAsociada();
      },
    });
  }

  btnCancelar() {
    this.ref.close();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  listar() {

    this.listaSolicitudes = [];
    const params = {
      codigoPerfil: this.usuario.sirperfil
      , idSolicitante: this.usuario.idusuario
      , estadoSolicitud: 'SCESTPUB'
      , pageNum: this.optionPage.pageNum
      , pageSize: this.optionPage.pageSize
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.listarSolicitudConcesionPorFiltro(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaSolicitudes = resp.data.map((item: any) => {
          return { ...item, auxColor: this.getColorEstado(item.estadoSolicitud) }
        });
        this.optionPage.totalRecords = resp.totalRecord || 0;
        if(this.listaSolicitudes?.length === 0 ) this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  
  getColorEstado(estadoIn: string): string {
    let strColor = "";
    switch (estadoIn) {
      case CodigoEstadoConcesion.BORRADOR: strColor = ""; break;
      case CodigoEstadoConcesion.PRESENTADA: strColor = "estAzul"; break;
      case CodigoEstadoConcesion.OBSERVADO_REQ: strColor = "estNaranja"; break;
      case CodigoEstadoConcesion.NO_ADMITIDO: strColor = "estMarron"; break;
      case CodigoEstadoConcesion.EVALUACION: strColor = "estAmarilo"; break;
      case CodigoEstadoConcesion.OBSERVADO_SOL: strColor = "estRojo"; break;
      default: break;
    }
    return strColor;
  }

}
