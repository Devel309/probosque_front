import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { SolicitudConcesionEvaluacionCalificacionService } from 'src/app/service/concesion/solicitud-concesion-evaluacion-calificacion.service';
import { SolicitudConcesionEvaluacionService } from 'src/app/service/concesion/solicitud-concesion-evaluacion.service';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-evaluacion-propuesta',
  templateUrl: './evaluacion-propuesta.component.html',
  styleUrls: ['./evaluacion-propuesta.component.scss']
})
export class EvaluacionPropuestaComponent implements OnInit, OnDestroy {
  idPFDM: number = 0;
  idSCEval: number = 0;
  evaluar: any = {
    puntaje11: null,
    puntaje12: null,
    puntaje13: null,
    puntaje14: null,
    puntaje15: null,
    puntaje16: null,
    puntaje21: null,
    puntaje22: null,
    puntaje23: null,
    puntaje24: null,
    puntaje25: null,
    puntaje26: null,
    puntaje31: null,
    puntaje32: null,
    puntaje33: null,
    puntaje34: null,
    puntaje35: null,
    puntaje36: null,
    puntaje37: null,
    puntaje41: null,
    puntaje42: null,
    puntaje43: null,
    puntaje51: null,
    puntaje52: null,
    puntaje61: null,
    puntaje62: null

  }
  adicional: boolean = true;
  puntajeExtra: number = 0;
  puntajeTotal: number = 0;
  puntajeObtenido: number = 0;

  usuario!: UsuarioModel;

  propuestasRequestEntity: any = {};
  codigosPFDM = CodigosPFDM;
  idEvaluacionSolicitudConcesionParamIn: number = 0;
  validarPrimerPostulante = localStorage.getItem("SCEvalPrimerPostulante");
  calificacionFinalizada = localStorage.getItem("calificacionFinalizada");
  page=localStorage.getItem("page");
  SPadre = localStorage.getItem("SPadre");
  isPrimerPostulante = false;
  isCalificacionFinalizada = false;
  stylesClass: any = {
    w100px: 'evalTu',
  };

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private router: Router,
    private activaRoute: ActivatedRoute,
    private usuarioServ: UsuarioService,
    private solicitudConcesionEvaluacionService: SolicitudConcesionEvaluacionService,
    private solicitudConcesionService: SolicitudConcesionService,
    private solicitudConcesionEvaluacionCalificacionService: SolicitudConcesionEvaluacionCalificacionService,
  ) {
    this.usuario = this.usuarioServ.usuario;

    this.idPFDM = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    if (!this.idPFDM) this.router.navigate(['/concesion/solicitud-concesion-PFDM']);

    let auxLocalStorageId = localStorage.getItem("SCEvalPropid");
     

    if (auxLocalStorageId) {
      if (Number(auxLocalStorageId) === this.idPFDM) {
        //
      }
      else {
        this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
      }
    } else {
      this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem("SCEvalPropid");
    localStorage.removeItem("page");
    localStorage.removeItem("SCEvalPrimerPostulante");
    localStorage.removeItem("calificacionFinalizada");
  }

  ngOnInit(): void {
    
    if(this.validarPrimerPostulante =='primero'){
      this.isPrimerPostulante = true;
    }
    if(this.calificacionFinalizada =='finalizada'){
      this.isCalificacionFinalizada = true;
    }
    
    this.obtenerInfoEvaluacion();
    
    this.obtenerInfoAdjunto();
  }

  obtenerInfoAdjunto() {
    let param = { 
      idSolicitudConcesion : this.idPFDM,
      codigoSeccion: "TABPRO",
      codigoSubSeccion: "SUBPRO"
    };
  
    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {
        this.propuestasRequestEntity.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
        this.propuestasRequestEntity.idArchivo = resp.data[0].idArchivo;
        
       } 
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

  //BOTONES
  btnGrabar() {
    if (this.puntajeObtenido > 100) {
      this.toast.warn('No debe superar los 100 puntos');
      return ;
    }

    // if (!this.propuestasRequestEntity.idArchivo) {
    //   this.toast.warn('Debe ingresar el adjunto');
    //   return ;
    // }
    var lstArchivos: any[]= [];

    lstArchivos.push({
      idSolicitudConcesionArchivo: this.propuestasRequestEntity.idSolicitudConcesionArchivo || 0,
      idSolicitudConcesion: this.idPFDM,
      idArchivo: this.propuestasRequestEntity.idArchivo,
      codigoSeccion: "TABPRO",//CodigosPFDM.TAB_2,
      codigoSubSeccion: "SUBPRO",//CodigosPFDM.TAB_2_3,
      idUsuarioRegistro: this.usuario.idusuario
    });


    let concesionEvaluacion: any = {
      idSolicitudConcesionEvaluacion: this.idSCEval,
      puntajeObtenido : this.puntajeObtenido,

      //puntajeTotal: this.puntajeTotal,
      lstSolicitudConcesionEvaluacionCalificacion: this.getValueCriterio(),
      lstSolicitudConcesionEvaluacionCalificacionArchivo: lstArchivos,
      idUsuarioRegistro: this.usuario.idusuario
    }

    if(this.isPrimerPostulante){

      concesionEvaluacion = {
        idSolicitudConcesionEvaluacion: this.idSCEval,
        puntajeObtenido : this.puntajeObtenido,
        puntajeExtra: this.puntajeExtra,
        //puntajeTotal: this.puntajeTotal,
  
        lstSolicitudConcesionEvaluacionCalificacion: this.getValueCriterio(),
        lstSolicitudConcesionEvaluacionCalificacionArchivo: lstArchivos,
        idUsuarioRegistro: this.usuario.idusuario
      }
      
    }

    if(this.idEvaluacionSolicitudConcesionParamIn == 0){
      this.solicitudConcesionEvaluacionCalificacionService.registrarSolicitudConcesionEvaluacionCalificacion(concesionEvaluacion).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      });
    }else{
      this.solicitudConcesionEvaluacionCalificacionService.actualizarSolicitudConcesionEvaluacionCalificacion(concesionEvaluacion).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {
            this.toast.ok(result?.message);
          } else {
            this.toast.warn(result?.message);
          }
        });
    }
  

  }

  btnCancelar(){
    if(this.page =='1'){
      this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
    }
    if(this.page =='2'){
      this.router.navigate(['/concesion/calificacion-propuesta-concesion/', this.SPadre]);
    }
  }

  btnDescargarPlant() { 
    let params: any = {};
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudConcesionService.descargarPlantillaCriteriosCalificacionPropuesta(params).subscribe((data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
      
    

  }

  registrarArchivo(id: number) {
    this.propuestasRequestEntity.idArchivo = id;
  }
  eliminarArchivo(ok: boolean) {
    if(ok){
      if(this.propuestasRequestEntity.idSolicitudConcesionArchivo) this.eliminarInfoAdjuntos();
    }
  }

  eliminarInfoAdjuntos() {

    let request = {
      idSolicitudConcesionArchivo : this.propuestasRequestEntity.idSolicitudConcesionArchivo,
      idUsuarioElimina : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  //FUNCIONES
  sumaTotal() {

    this.puntajeObtenido = 0;
    this.puntajeExtra = 0;
    this.puntajeTotal = 0;

    for (let clave in this.evaluar) {
      this.puntajeObtenido += this.evaluar[clave] || 0;
    }
    if(this.isPrimerPostulante){
      
    this.puntajeExtra = 0.1*this.puntajeObtenido;
    }
    this.puntajeTotal = this.puntajeObtenido + this.puntajeExtra;
  }

  onInputOM = (event: any, posicion: number) => {

    // if (posicion === 1) this.evaluar.puntaje11 = event.value | event;
    // else if (posicion === 2) this.evaluar.puntaje12 = event.value | event;
    this.sumaTotal();
  };

  onInputJI = (event: any, posicion: number) => {

  };

  onInputSF = (event: any, posicion: number) => {

  };

  onInputEP = (event: any, posicion: number) => {

  };

  onInputEQ = (event: any, posicion: number) => {

  };

  obtenerInfoEvaluacion() {

    let request = {
      idSolicitudConcesion: this.idPFDM
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionEvaluacionService.obtenerInfoEvaluacionSolicitudConcesion(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          if (resp.data && resp.data.length > 0) {
            // alert("bien")
            this.idSCEval = resp.data[0].idSolicitudConcesionEvaluacion;
           
            
            this.puntajeObtenido= resp.data[0].puntajeObtenido;
            this.puntajeExtra= resp.data[0].puntajeExtra;
            this.puntajeTotal= resp.data[0].puntajeTotal;


            
          }
          this.listarSolicitudConcesionEvaluacionCalificacion();
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  listarSolicitudConcesionEvaluacionCalificacion() {
    // alert(this.idSCEval)
 
    
    const params = { "idSolicitudConcesionEvaluacion": this.idSCEval };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionEvaluacionCalificacionService.listarSolicitudConcesionEvaluacionCalificacion(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        
        this.setValueCriterio(resp.data);

        
        this.idEvaluacionSolicitudConcesionParamIn = resp.data[0].idSolicitudConcesionEvaluacion;
        // alert(this.idEvaluacionSolicitudConcesionParamIn)
        //this.setDataSolicita(datos);
        //this.listarPorFiltroDepartamento(false, 1);
      }
    }, (error) => this.toast.error(error));
  }

  setValueCriterio(data: any) {

    for (let item of data) {

      switch (item.codigoCriterio) {
        //objetivos y metas
        case "PUNTAJE11":
          this.evaluar.puntaje11 = item.numPuntaje;
          break;
        case "PUNTAJE12":
          this.evaluar.puntaje12 = item.numPuntaje;
          break;
        case "PUNTAJE13":
          this.evaluar.puntaje13 = item.numPuntaje;
          break;
        case "PUNTAJE14":
          this.evaluar.puntaje14 = item.numPuntaje;
          break;
        case "PUNTAJE15":
          this.evaluar.puntaje15 = item.numPuntaje;
          break;
        case "PUNTAJE16":
          this.evaluar.puntaje16 = item.numPuntaje;
          break;
        //caracterizacion fisica
        case "PUNTAJE21":
          this.evaluar.puntaje21 = item.numPuntaje;
          break;
        case "PUNTAJE22":
          this.evaluar.puntaje22 = item.numPuntaje;
          break;
        case "PUNTAJE23":
          this.evaluar.puntaje23 = item.numPuntaje;
          break;
        case "PUNTAJE24":
          this.evaluar.puntaje24 = item.numPuntaje;
          break;
        case "PUNTAJE25":
          this.evaluar.puntaje25 = item.numPuntaje;
          break;
        case "PUNTAJE26":
          this.evaluar.puntaje26 = item.numPuntaje;
          break;
        //justificacion de la importancia
        case "PUNTAJE31":
          this.evaluar.puntaje31 = item.numPuntaje;
          break;
        case "PUNTAJE32":
          this.evaluar.puntaje32 = item.numPuntaje;
          break;
        case "PUNTAJE33":
          this.evaluar.puntaje33 = item.numPuntaje;
          break;
        case "PUNTAJE34":
          this.evaluar.puntaje34 = item.numPuntaje;
          break;
        case "PUNTAJE35":
          this.evaluar.puntaje35 = item.numPuntaje;
          break;
        case "PUNTAJE36":
          this.evaluar.puntaje36 = item.numPuntaje;
          break;
        case "PUNTAJE37":
          this.evaluar.puntaje37 = item.numPuntaje;
          break;
        //sustento financiero
        case "PUNTAJE41":
          this.evaluar.puntaje41 = item.numPuntaje;
          break;
        case "PUNTAJE42":
          this.evaluar.puntaje42 = item.numPuntaje;
          break;
        case "PUNTAJE43":
          this.evaluar.puntaje43 = item.numPuntaje;
          break;
        //experiencia del postor
        case "PUNTAJE51":
          this.evaluar.puntaje51 = item.numPuntaje;
          break;
        case "PUNTAJE52":
          this.evaluar.puntaje52 = item.numPuntaje;
          break;
        //equipo tecnico
        case "PUNTAJE61":
          this.evaluar.puntaje61 = item.numPuntaje;
          break;
        case "PUNTAJE62":
          this.evaluar.puntaje62 = item.numPuntaje;
          break;
      }


    }
  }

  getValueCriterio() {

    let lsCriterios=[];

    for (let clave in this.evaluar) {

      let  reg=   {
        idSolicitudConcesionEvaluacion: this.idSCEval,

        numPuntaje: this.evaluar[clave],
        codigoCriterio: clave.toUpperCase(),

      }
      lsCriterios.push(reg);
    }
    return lsCriterios;
  }


}
