import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ContratoConcesionModel } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { varLocalStor } from 'src/app/shared/util-local-storage';

@Component({
  selector: 'app-anexo-formato-propuesta',
  templateUrl: './anexo-formato-propuesta.component.html',
  styleUrls: ['./anexo-formato-propuesta.component.scss']
})
export class AnexoFormatoPropuestaComponent implements OnInit, OnDestroy {

  data: ContratoConcesionModel = new ContratoConcesionModel();

  isDisbledFormu: boolean = false;
  isDisbledEval: boolean = false;
  isShowEval: boolean = false;

  isRemiteInformacionAnexo6 : boolean = false;

  idPFDM: number = 0;
  urlBandeja: string = '/concesion/solicitud-concesion-PFDM';

  constructor(private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioService : UsuarioService,
    private solicitudConcesionService: SolicitudConcesionService) {

    this.idPFDM = this.route.snapshot.paramMap.get('idParam') ? Number(this.route.snapshot.paramMap.get('idParam')) : 0;

    let auxLocalStorageSCPFDMdata = localStorage.getItem(varLocalStor.BJA_SOL_CONCESION);
    if (auxLocalStorageSCPFDMdata) {
      let auxData = JSON.parse("" + auxLocalStorageSCPFDMdata);
      if (auxData.SCidSolicitud === this.idPFDM) {
        this.isDisbledFormu = auxData.SCdisabledForm || false;
        this.isDisbledEval = auxData.SCdisabledEval || false;
        this.isShowEval = auxData.SCshowEval || false;
        this.isRemiteInformacionAnexo6 = auxData.SCdisabledForm || false;
        localStorage.removeItem(varLocalStor.BJA_SOL_CONCESION);
      } else {
        this.router.navigate([this.urlBandeja]);
      }
    } else {
      this.router.navigate([this.urlBandeja]);
    }

  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    localStorage.removeItem(varLocalStor.BJA_SOL_CONCESION);
  }

  regresar() {
    this.router.navigate([this.urlBandeja]);
  }

  remitirInformacion(){

    let remitir: any = {};

    remitir.idSolicitudConcesion = this.idPFDM;
    remitir.remiteInformacionAnexo6 = true;
    remitir.idUsuarioModificacion = this.usuarioService.idUsuario;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarSolicitudConcesion(remitir).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.isRemiteInformacionAnexo6 = true;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
