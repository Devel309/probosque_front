import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService, UsuarioService } from '@services';
import { MapApi, ToastService } from '@shared';
import { indexOf } from 'lodash';
import { ConfirmationService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { ContratoConcesionModel, DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { MapCustomPFDMComponent } from 'src/app/shared/components/map-custom-pfdm/map-custom-pfdm.component';
import { validarEmail } from 'src/app/shared/util';

@Component({
  selector: 'app-anexo1-pfdn',
  templateUrl: './anexo1-pfdn.component.html',
  styleUrls: ['./anexo1-pfdn.component.css',],
})
export class Anexo1PfdnComponent implements OnInit {
  @ViewChild("map") appMapPFDM!: MapCustomPFDMComponent;
  @Input() data: ContratoConcesionModel = new ContratoConcesionModel();
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isSolicitudHija: boolean = false;
  @Output() btnValidarSuperAnexo1 = new EventEmitter<any>();

  superficie: string = '';
  zonaUTM: string = '';
  listVertices: any = [];
  resultEvaliacin1: string = '';
  resultEvaliacin2: string = '';

  rd_fuentesF: any = null;

  tituloModalMantenimiento: string = '';
  verModalMantenimiento: boolean = false;
  tipoAccion: string = '';

  comboActividad: any[] = [];
  lstData: any[] = [];
  lstDataComplementaria: any[] = [];
  lstConsescion: any[] = [];
  context: any = {};
  otrosEspecificar: string = '';
  /*****************/
  requestInfoSolicitante: any = {};
  requestInfoSolicitado: any = { vigenciaAnio: 40 };
  requestArea: any = {};
  requestActividad: any = {};
  requestNotifica: any = {};

  modalidad: any = null;
  departamentos: any = [];
  solicitudConcesion: any = [];
  departamentoSelect: any = null;
  departamentosArea: any = [];
  departamentoAreaSelect: any = null;
  provincias: any = [];
  provinciaSelect: any = null;
  provinciasArea: any = [];
  provinciaAreaSelect: any = null;
  distritos: any = [];
  distritoSelect: any = null;
  distritosArea: any = [];
  distritoAreaSelect: any = null;

  idPGMF = 0;
  codProceso = 'PFDMA1';
  codigoSubSeccion = 'PFDMA1_SS3';

  listaBosque: any[] = [];
  totalAreaTipoBosque: string = '';
  totalPorcentaTipoBosque: string = '';

  listaCoordenadas: any[] = [];

  notificaCorreo: any = "";
  usuario!: UsuarioModel;
  MODALIDAD: { PFDM: string, ECOT: string, CONS: string, } = {
    PFDM: "SCMODPFDM", ECOT: "SCMODECOT", CONS: "SCMODCONS",
  }

  touchedAll_I: string = "";
  touchedAll_II: string = "";
  touchedAll_III: string = "";
  touchedAll_IV: string = "";

  codigosPFDM = CodigosPFDM;

  constructor(
    private confirmationService: ConfirmationService,
    private planificacionService: PlanificacionService,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private solicitudConcesionService: SolicitudConcesionService,
    private parametroValorService: ParametroValorService,
    private mapApi: MapApi,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.cargarCombo();
    if (this.idPFDM) {
      this.listarPorFiltroDepartamento(false, 2);
      this.listarInfoSolicita();
      this.obtenerInfoAreaSolicitada();
    } else {
      this.listarPorFiltroDepartamento();
    }

    let d: any = { idSolicitudConcesion: this.idPFDM };
    this.obtenerInformacionActividadFuenteFinanciamiento(d);

    this.listarSolicitudConcesion(d);
  }

  //MAPA
  listarVertices(items: any) {
    this.listaCoordenadas = items;
    localStorage.setItem("coordenadasPFDM", JSON.stringify(items));
  }
  calcularAreaTotal(areaTotal: number) {
    this.requestArea.superficieHa = areaTotal;
  }
  validarSuperposicion(message: any) {
    let listaGeometria = this.appMapPFDM._listaCapas;
    let item: any = listaGeometria.find(
      (e: any) => e.tipoGeometria == 'TPAREA'
    );
    if (item !== null) {
      let geometry: any = this.mapApi.wktParse(item.geometry_wkt);
      if (geometry.coordinates.length === 1) {

        this.appMapPFDM.servicioSuperposicionPlanificacion(geometry.coordinates);
      } else {
        geometry.coordinates.forEach((t: any) => {
          
          this.appMapPFDM.servicioSuperposicionPlanificacion(t);

        });

      }

    }
  }

  // SERVICIOS
  listarInfoSolicita() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        let datos = resp.data[0];
        this.setDataSolicita(datos);
        this.listarPorFiltroDepartamento(false, 1);
      }
    }, (error) => this.errorMensaje(error, true));
  }


  obtenerInfoAreaSolicitada() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAreaSolicitada(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        let datosArea = resp.data[0];
        this.setInfoAreaSolicitada(datosArea);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarSolicitante() {
    this.requestInfoSolicitante.idSolicitudConcesion = 0;
    this.requestInfoSolicitante.idSolicitante = 3;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInformacionSolicitante(this.requestInfoSolicitante).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        // this.listarInfoSolicita();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarInformacion(request: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarInformacionSolicitante(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarInfoSolicita();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  /** -------------------------WORKING-------------------------- */
  listarSolicitudConcesion(request: any) {
    this.solicitudConcesionService.listarSolicitudConcesion(request).subscribe(resp => {
      if (resp.success) {
        this.requestActividad.descripcionOtros = resp.data[0].descripcionOtros;
        this.requestActividad.codigoFuenteFinanciamiento = resp.data[0].codigoFuenteFinanciamiento;

      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarSolicitudConcesion(request: any) {
    this.solicitudConcesionService.actualizarSolicitudConcesion(request).subscribe(resp => {
      if (resp.success) {
        let d: any = {
          idSolicitudConcesion: this.idPFDM
        };
        this.listarSolicitudConcesion(d);
        this.toast.ok(resp.message);

      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarInformacionActividades(param: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInformacionActividadFuenteFinanciamiento(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        let d: any = { idSolicitudConcesion: this.idPFDM };
        this.obtenerInformacionActividadFuenteFinanciamiento(d);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInformacionActividadFuenteFinanciamiento(param: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionActividadFuenteFinanciamiento(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {

        if (resp.data && resp.data.length > 0) {

          this.lstData = resp.data.filter(
            (eins: any) => eins.complementario === false
          );

          this.lstDataComplementaria = resp.data.filter(
            (eins: any) => eins.complementario
          );

          this.lstDataComplementaria.forEach(element => {
            element.check = null;
            if (element.descripcion !== null && element.descripcion !== undefined && element.descripcion !== "") {
              element.check = true;
            }
          });
        } else {
          this.cargarActividadComplementaria();
        }

      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarSolicitado() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarInformacionSobreSolicitado(this.requestInfoSolicitado).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarInfoSolicita();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  setDataSolicita(datos: any) {
    //Info Solicitante
    this.requestInfoSolicitante.idSolicitudConcesion = datos.idSolicitudConcesion;
    this.requestInfoSolicitante.idSolicitante = datos.idSolicitante;
    this.requestInfoSolicitante.nroPartidaRegistral = datos.nroPartidaRegistral;
    this.requestInfoSolicitante.numeroDireccion = datos.numeroDireccion;
    this.requestInfoSolicitante.sectorCaserio = datos.sectorCaserio;
    this.requestInfoSolicitante.telefonoFijo = datos.telefonoFijo;
    this.requestInfoSolicitante.telefonoCelular = datos.telefonoCelular;
    this.requestInfoSolicitante.correoElectronico = datos.correoElectronico;
    this.requestInfoSolicitante.nombreSolicitanteUnico = datos.nombreSolicitanteUnico;
    this.requestInfoSolicitante.direccionSolicitante = datos.direccionSolicitante;
    this.requestInfoSolicitante.idDepartamentosolicitante = datos.idDepartamentosolicitante;
    this.requestInfoSolicitante.idProvinciaSolicitante = datos.idProvinciaSolicitante;
    this.requestInfoSolicitante.idDistritoSolicitante = datos.idDistritoSolicitante;
    this.requestInfoSolicitante.idUsuarioModificacion = this.usuario.idusuario;

    this.requestInfoSolicitante.tipoDocumentoSolicitante = datos.tipoDocumentoSolicitante;
    this.requestInfoSolicitante.soliDni = datos.tipoDocumentoSolicitante === CodigosTiposDoc.DNI
      ? datos.numeroDocumentoSolicitanteUnico : "---";
    this.requestInfoSolicitante.soliRuc = datos.tipoDocumentoSolicitante === CodigosTiposDoc.RUC
      ? datos.numeroDocumentoSolicitanteUnico : "---";

    //Info Solicitado
    this.requestInfoSolicitado.idSolicitudConcesion = datos.idSolicitudConcesion;
    this.requestInfoSolicitado.idSolicitante = datos.idSolicitante;
    this.requestInfoSolicitado.codigoModalidad = datos.codigoModalidad;
    this.requestInfoSolicitado.objetivo = datos.objetivo;
    this.requestInfoSolicitado.recurso = datos.recurso;
    this.requestInfoSolicitado.importancia = datos.importancia;
    this.requestInfoSolicitado.descripcionProyecto = datos.descripcionProyecto;
    this.requestInfoSolicitado.idUsuarioModificacion = this.usuario.idusuario;
    this.data.anexo4.isShow = datos.codigoModalidad === this.MODALIDAD.CONS ? true : false;

    //Notificaciones
    this.requestNotifica.idSolicitudConcesion = datos.idSolicitudConcesion;
    this.requestNotifica.idSolicitante = datos.idSolicitante;
    this.requestNotifica.correoElectronicoNotif = datos.correoElectronicoNotif;
    this.requestNotifica.idUsuarioModificacion = this.usuario.idusuario;

  }

  setInfoAreaSolicitada(datos: any) {
    this.requestArea.idSolicitudConcesionArea = datos.idSolicitudConcesionArea;
    this.requestArea.superficieHa = datos.superficieHa;
    this.requestArea.zonaUTM = datos.zonaUTM;
    this.requestArea.observacion = datos.observacion;

    this.departamentoAreaSelect = datos.idDepartamentoArea;
    this.provinciaAreaSelect = datos.idProvinciaArea;
    this.distritoAreaSelect = datos.codigoUbigeo;
    this.listarPorFilroProvincia(false, 2);
  }

  registrarAreaSolicitada() {
    this.requestArea.idSolicitudConcesion = this.idPFDM;
    this.requestArea.codigoUbigeo = this.distritoAreaSelect;
    this.requestArea.idUsuarioRegistro = this.usuario.idusuario;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInformacionAreaSolicitada(this.requestArea).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.requestArea.idSolicitudConcesionArea = resp.codigo;
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarAreaSolicitada() {
    this.requestArea.idSolicitudConcesion = this.idPFDM;
    this.requestArea.codigoUbigeo = this.distritoAreaSelect;
    this.requestArea.idUsuarioModificacion = this.usuario.idusuario;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarInformacionAreaSolicitada(this.requestArea).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        //this.listarInfoSolicita();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  private validarFormSolicitante(): boolean {
    let valido = true;

    this.requestInfoSolicitante.nroPartidaRegistral = this.requestInfoSolicitante.nroPartidaRegistral?.trim();
    this.requestInfoSolicitante.direccionSolicitante = this.requestInfoSolicitante.direccionSolicitante?.trim();
    this.requestInfoSolicitante.sectorCaserio = this.requestInfoSolicitante.sectorCaserio?.trim();
    this.requestInfoSolicitante.correoElectronico = this.requestInfoSolicitante.correoElectronico?.trim();

    if (!this.requestInfoSolicitante.numeroDireccion) valido = false;
    if (!this.requestInfoSolicitante.telefonoFijo) valido = false;
    if (!this.requestInfoSolicitante.telefonoFijo) valido = false;
    if (!this.requestInfoSolicitante.nroPartidaRegistral) valido = false;
    if (!this.requestInfoSolicitante.direccionSolicitante) valido = false;
    // if(!this.requestInfoSolicitante.sectorCaserio) valido = false;
    if (!this.requestInfoSolicitante.correoElectronico) valido = false;

    if (!valido) this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.");

    return valido;
  }

  private validarFormSolicitado(): boolean {
    let valido = true;

    this.requestInfoSolicitado.objetivo = this.requestInfoSolicitado.objetivo?.trim();
    this.requestInfoSolicitado.recurso = this.requestInfoSolicitado.recurso?.trim();
    this.requestInfoSolicitado.importancia = this.requestInfoSolicitado.importancia?.trim();
    this.requestInfoSolicitado.descripcionProyecto = this.requestInfoSolicitado.descripcionProyecto?.trim();

    if (!this.requestInfoSolicitado.codigoModalidad) valido = false;
    if (!this.requestInfoSolicitado.objetivo) valido = false;
    if (!this.requestInfoSolicitado.recurso) valido = false;
    if (!this.requestInfoSolicitado.importancia) valido = false;
    if (!this.requestInfoSolicitado.descripcionProyecto) valido = false;

    if (!valido) this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.");

    return valido;
  }

  private validarFormArea(): boolean {
    let valido = true;

    this.requestArea.observacion = this.requestArea.observacion?.trim();

    if (!this.requestArea.superficieHa) valido = false;
    if (!this.requestArea.zonaUTM) valido = false;
    // if(!this.requestArea.observacion) valido = false;

    if (!valido) this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.");

    return valido;
  }

  private validarFormNotifica(): boolean {
    let valido = true;

    this.requestNotifica.correoElectronicoNotif = this.requestNotifica.correoElectronicoNotif?.trim();
    if (!this.requestNotifica.correoElectronicoNotif) valido = false;
    if (!this.requestNotifica.correoElectronicoNotif) valido = false;

    if (!valido) this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.");

    return valido;
  }

  // BOTONES
  btnRegInfoSolicitante() {
    this.touchedAll_I = "ng-touched";
    if (!this.validarFormSolicitante()) return;

    if (!validarEmail(this.requestInfoSolicitante.correoElectronico)) {
      this.toast.warn('(*) Debe ingresar un correo válido.');
      return;
    }

    this.actualizarInformacion(this.requestInfoSolicitante);
  }

  btnRegInfoSolicitado() {
    this.touchedAll_II = "ng-touched";
    if (!this.validarFormSolicitado()) return;
    this.actualizarSolicitado();
  }

  btnRegAreaSolicitada() {
    this.touchedAll_III = "ng-touched";
    if (!this.validarFormArea()) return;

    if (this.idPFDM) {
      if (this.requestArea.idSolicitudConcesionArea)
        this.actualizarAreaSolicitada();
      else
        this.registrarAreaSolicitada();
    }
  }

  btnRegInfoActividades() {
    this.touchedAll_IV = "ng-touched";

    if (document.getElementsByClassName('anexo1Actividades_required').length > 0 || !this.requestActividad.codigoFuenteFinanciamiento) {
      this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.")
      return;
    }

    let infoActividades: any[] = [];
    let e: any = {};

    this.lstData.forEach(element => {
      e = {};
      e = { ...element };
      e.complementario = false;
      e.idSolicitudConcesion = this.idPFDM;
      e.idSolicitudConcesionActividad = null;
      if (element.idSolicitudConcesionActividad.toString().includes("NOT")) {
        e.idActividad = 0;
      } else {
        e.idSolicitudConcesionActividad = element.idSolicitudConcesionActividad;
      }
      e.id = undefined;
      e.idUsuarioRegistro = this.usuario.idusuario;
      infoActividades.push(e);

    });

    this.lstDataComplementaria.forEach(element => {
      e = {};
      e = { ...element };
      e.complementario = true;
      e.idSolicitudConcesion = this.idPFDM;
      e.idSolicitudConcesionActividad = element.idSolicitudConcesionActividad;
      e.idUsuarioRegistro = JSON.parse('' + localStorage.getItem('usuario')).idusuario;
      infoActividades.push(e);
    });

    this.registrarInformacionActividades(infoActividades);

    this.requestActividad.idSolicitudConcesion = this.idPFDM;
    //this.requestActividad.idUsuarioRegistro  = JSON.parse('' + localStorage.getItem('usuario')).idusuario;
    this.requestActividad.idUsuarioRegistro = this.usuario.idusuario;
    this.actualizarSolicitudConcesion(this.requestActividad);
  }

  limpiarCaja() {
    this.requestActividad.descripcionOtros = '';
  }
  btnRegInfoNotifica() {
    if (!this.validarFormNotifica()) return;

    if (!validarEmail(this.requestNotifica.correoElectronicoNotif)) {
      this.toast.warn('(*) Debe ingresar un correo válido.');
      return;
    }

    this.actualizarInformacion(this.requestNotifica);
  }

  checkActividad(fila: any) {
    if (!fila.check) fila.descripcion = "";
  }

  selectDPD(tipo: number, origen: string) {
    if (origen == 'd') {
      this.listarPorFilroProvincia(true, tipo);
    }
    if (origen == 'p') {
      this.listarPorFilroDistrito(true, tipo);
    }
  }

  listarPorFiltroDepartamento(ischange: boolean = false, tipo: number = 0) {
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };

    this.planificacionService.listarPorFiltroDepartamento_core_central(params).subscribe((result: any) => {
      if (result.success) {
        if (tipo === 1) {
          this.departamentos = result.data;
          this.requestInfoSolicitante.idDepartamentosolicitante = ischange ? result.data[0].idDepartamento : (this.requestInfoSolicitante.idDepartamentosolicitante || result.data[0].idDepartamento);
        } else if (tipo === 2) {
          this.departamentosArea = result.data;
          this.departamentoAreaSelect = ischange ? result.data[0].idDepartamento : this.departamentoAreaSelect != null ? this.departamentoAreaSelect : result.data[0].idDepartamento;
        }

        this.listarPorFilroProvincia(ischange, tipo);
      }
    }, (error) => this.errorMensaje(error));
  }
  listarPorFilroProvincia(ischange: boolean = false, tipo: number = 0) {
    let idDepartamento = null;
    if (tipo === 1) idDepartamento = this.requestInfoSolicitante.idDepartamentosolicitante;
    else if (tipo === 2) idDepartamento = this.departamentoAreaSelect;
    else idDepartamento = this.requestInfoSolicitante.idDepartamentosolicitante;

    this.planificacionService.listarPorFilroProvincia({ idDepartamento: idDepartamento, }).subscribe((result: any) => {
      if (result.success) {
        if (tipo === 1) {
          this.provincias = result.data;
          this.requestInfoSolicitante.idProvinciaSolicitante = ischange ? result.data[0].idProvincia : (this.requestInfoSolicitante.idProvinciaSolicitante || result.data[0].idProvincia);
        } else if (tipo === 2) {
          this.provinciasArea = result.data;
          this.provinciaAreaSelect = ischange ? result.data[0].idProvincia : this.provinciaAreaSelect != null ? this.provinciaAreaSelect : result.data[0].idProvincia;
        } else {
          this.provincias = result.data;
          this.provinciasArea = result.data;
          this.requestInfoSolicitante.idProvinciaSolicitante = this.requestInfoSolicitante.idProvinciaSolicitante || result.data[0].idProvincia;
          this.provinciaAreaSelect = result.data[0].idProvincia;
        }

        this.listarPorFilroDistrito(ischange, tipo);
      }
    });
  }
  listarPorFilroDistrito(ischange: boolean = false, tipo: number = 0) {
    let idProvincia = null;
    if (tipo === 1) idProvincia = this.requestInfoSolicitante.idProvinciaSolicitante;
    else if (tipo === 2) idProvincia = this.provinciaAreaSelect;
    else idProvincia = this.requestInfoSolicitante.idProvinciaSolicitante;

    this.planificacionService.listarPorFilroDistrito({ idProvincia: idProvincia }).subscribe((result: any) => {
      if (result.success) {
        if (tipo === 1) {
          this.distritos = result.data;
          this.requestInfoSolicitante.idDistritoSolicitante = ischange ? result.data[0].idDistrito : (this.requestInfoSolicitante.idDistritoSolicitante || result.data[0].idDistrito);
        } else if (tipo === 2) {
          this.distritosArea = result.data;
          this.distritoAreaSelect = ischange ? result.data[0].idDistrito : this.distritoAreaSelect != null ? this.distritoAreaSelect : result.data[0].idDistrito;
        } else {
          this.distritos = result.data;
          this.distritosArea = result.data;
          this.requestInfoSolicitante.idDistritoSolicitante = this.requestInfoSolicitante.idDistritoSolicitante || result.data[0].idDistrito;
          this.distritoAreaSelect = result.data[0].idDistrito;
        }
      }
    });
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  /****************/

  private cargarActividadComplementaria() {
    this.lstDataComplementaria = [];

    var params = { prefijo: "SOLCONACTC" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {

        let e: any = {};
        response.data.forEach((element: any) => {
          e = {};
          e.codigoActividad = element.codigo;
          e.nombre = element.valor1;

          this.lstDataComplementaria.push(e)
        });

      });
  }

  //MODAL
  openModal(data: any, tipo: string): void {
    

    this.tipoAccion = tipo;
    if (tipo == 'C') {
      this.context = new DataModel();
      this.tituloModalMantenimiento = 'Nuevo Registro';
    } else if (tipo == 'E') {
      this.context = new DataModel(data);
      this.tituloModalMantenimiento = 'Editar Registro';
    }

    this.verModalMantenimiento = true;
  }

  private cargarCombo() {
    this.comboActividad = [];
    var params = { prefijo: "SOLCONACT" };
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe((response: any) => {
      this.comboActividad = response.data.map((item: any) => {
        return { codigoActividad: item.codigo, valor1: item.valor1 }
      });
    });
  }

  openEliminar(event: Event, data: DataModel): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.removerRegistro(data);
      },
      reject: () => { },
    });
  }

  validar1(): boolean {
    let validado = true;
    if (!this.context.codigoActividad) {
      validado = false;
      this.toast.warn("(*) Debe seleccionar una actividad.");
    } else {
      if (this.context.codigoActividad === "SOLCONACTOTR" && !this.context.descripcionRecurso) {
        validado = false;
        this.toast.warn("(*) Debe especificar la actividad.");
      }
    }
    if (!this.context.periodo) {
      validado = false;
      this.toast.warn("(*) Debe ingresar un periodo.");
    }
    if (!this.context.presupuesto) {
      validado = false;
      this.toast.warn("(*) Debe ingresar un presupuesto.");
    }
    this.context.descripcion = this.context.descripcion?.trim();
    if (!this.context.descripcion) {
      validado = false;
      this.toast.warn("(*) Debe ingresar una descripcion.");
    }
    return validado;
  }

  touchedAllModal: string = "";
  guardar(): void {
    this.touchedAllModal = "ng-touched";
    if (!this.validar1()) return;

    let obj = this.context;
    // if (obj.idSolicitudConcesionActividad > 0) {
    //   obj.codigo = DataDemoMPAFPP.ActividadesAnexo1Erika.filter(
    //     (x) => x.value == obj.idActividad
    //   )[0].text;
    // }

    this.comboActividad.forEach(element => {
      if (obj.codigoActividad === element.codigoActividad) {
        obj.nombre = element.valor1;
      }
    });

    // if (obj.idSolicitudConcesionActividad===null || obj.idSolicitudConcesionActividad===undefined) {
    //   if (this.tipoAccion == 'C') {
    //     this.lstData.push(obj);
    //   } else {
    //     let index = this.lstData.findIndex((x) => x.id == obj.id);
    //     this.lstData[index] = obj;
    //   }
    // }
    if (this.tipoAccion == 'C') {
      this.lstData.push(obj);
    } else {
      let index = this.lstData.findIndex((x) => x.idSolicitudConcesionActividad == obj.idSolicitudConcesionActividad);
      this.lstData[index] = obj;
    }

    this.verModalMantenimiento = false;
  }

  private removerRegistro(data: DataModel): void {
    const index = this.lstData.indexOf(data, 0);
    this.lstData.splice(index, 1);
  }

  sinEspacio(cadena: string) {
    return cadena.replace(/\s{2,}/g, ' ').trim();
  }

}

export class DataModel {
  constructor(data?: any) {
    if (data) {
      this.idSolicitudConcesionActividad = data.idSolicitudConcesionActividad;
      this.codigoActividad = data.codigoActividad;
      this.actividad = data.actividad;
      this.periodo = data.periodo;
      this.presupuesto = data.presupuesto;
      this.descripcion = data.descripcion;
      this.descripcionRecurso = data.descripcionRecurso;
      return;
    } else {
      this.idSolicitudConcesionActividad = Date.now().toString() + "NOT";
    }
  }

  idSolicitudConcesionActividad: number | string = 0;
  codigoActividad: number | string = "";
  descripcionRecurso?: string = "";
  actividad: String = '';
  periodo: string = '';
  presupuesto: string = '';
  descripcion: string = '';
}
