import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ContratoConcesionModel } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-anexo3-pfdn',
  templateUrl: './anexo3-pfdn.component.html',
  styleUrls: ['./anexo3-pfdn.component.scss']
})
export class Anexo3PfdnComponent implements OnInit {
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;

  @Input() data: ContratoConcesionModel = new ContratoConcesionModel();
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  tipoAccion: string = "";

  lstData: DataModel[] = [];
  context: DataModel = new DataModel();

  informacionSolicitante: any = {};
  informacionAreaSolicitada: any = {};

  usuario!: UsuarioModel;

  codigosPFDM = CodigosPFDM;

  constructor(
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private solicitudConcesionService: SolicitudConcesionService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }


  ngOnInit(): void {
    this.obtenerInfoSolicitante();
    this.obtenerInfoAreaSolicitada();
    this.listarIngresoCuenta();
  }

  //SERVICIOS
  obtenerInfoSolicitante() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.informacionSolicitante = resp.data[0];

      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInfoAreaSolicitada() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAreaSolicitada(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.informacionAreaSolicitada = resp.data[0];
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarIngresoCuenta() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.listarSolicitudConcesionIngresoCuenta(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.lstData = resp.data;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarIngresoCuenta(data: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarSolicitudConcesionIngresoCuenta(data).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarIngresoCuenta();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarIngresoCuenta(id: any) {
    const params = { "idSolicitudConcesionIngresoCuenta": id, "idUsuarioElimina": this.usuario.idusuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarSolicitudConcesionIngresoCuenta(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarIngresoCuenta();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnGuardar() {
    if (this.lstData.length === 0) {
      this.toast.warn("(*) Debe agregar registros a la tabla.");
      return;
    }

    const dataOut = this.lstData.filter(item => !item.idSolicitudConcesionIngresoCuenta);
    if(dataOut.length === 0) {
      this.toast.warn("(*) Debe agregar nuevos registros a la tabla.");
      return;
    }

    this.registrarIngresoCuenta(dataOut);
  }

  btnEliminar(event: Event, data: DataModel): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.removerRegistro(data);
      }
    });
  }

  removerRegistro(data: DataModel): void {
    if (data.idSolicitudConcesionIngresoCuenta) {
      this.eliminarIngresoCuenta(data.idSolicitudConcesionIngresoCuenta);
    } else {
      const index = this.lstData.indexOf(data, 0);
      this.lstData.splice(index, 1);
    }
  }

  //MODAL
  btnAbrirModal(data: any, tipo: string): void {
    this.touchedAll = "";
    this.tipoAccion = tipo;
    if (tipo == 'C') {
      this.context = new DataModel();
      this.tituloModalMantenimiento = "Nuevo Registro";
    }
    else if (tipo == 'E') {
      this.context = new DataModel(data);
      this.tituloModalMantenimiento = "Editar Registro";
    }

    this.verModalMantenimiento = true;
  }

  guardarModal(): void {
    if (!this.validarAgregar()) return;
    let obj = this.context;
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.idSolicitudConcesion = this.idPFDM;

    if (this.tipoAccion == 'C') {
      this.lstData.push(obj);
    } else {
      let index = this.lstData.findIndex(x => x.id == obj.id);
      this.lstData[index] = obj;
    }

    this.verModalMantenimiento = false;
  }
  touchedAll: string = "";
  validarAgregar(): boolean {
    this.touchedAll = "ng-touched";
    let validado = true;

    this.context.codigoTipoDocumento = this.context.codigoTipoDocumento?.trim();
    this.context.descripcion = this.context.descripcion?.trim();
    if (!this.context.codigoTipoDocumento) {
      validado = false;
      this.toast.warn("(*) Debe ingresar el tipo.");
    }
    if (!this.context.descripcion) {
      this.toast.warn("(*) Debe ingresar la descripción.");
      validado = false;
    }
    if (!this.context.monto) {
      this.toast.warn("(*) Debe ingresar el monto.");
      validado = false;
    }
    return validado;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}

export class DataModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.idSolicitudConcesion = data.idSolicitudConcesion;
      this.idSolicitudConcesionIngresoCuenta = data.idSolicitudConcesionIngresoCuenta;
      this.codigoTipoDocumento = data.codigoTipoDocumento;
      this.numeroTipoDocumento = data.numeroTipoDocumento;
      this.descripcion = data.descripcion;
      this.monto = data.monto;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      return;
    } else {
      this.id = new Date().toISOString();
    }
  }

  id: string = '';
  idSolicitudConcesion: number = 0;
  idSolicitudConcesionIngresoCuenta: number = 0;
  codigoTipoDocumento: String = "";
  numeroTipoDocumento: String = "";
  descripcion: string = "";
  monto: number | null = null;
  idUsuarioRegistro: number = 0;
}


