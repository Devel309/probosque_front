import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ParametroValorService, PlanificacionService, UsuarioService } from '@services';
import { ToastService, validarEmail } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { finalize } from "rxjs/operators";
import { ContratoConcesionModel, DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionResponsableService } from 'src/app/service/concesion/solicitud-concesion-responsable.service';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { SolicitudExperienciaLaboralService } from 'src/app/service/concesion/solicitud-responsable-experiencia-laboral.service';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { isTypeNode } from 'typescript';
import { ConsoleLogger } from '@angular/compiler-cli/src/ngtsc/logging';

@Component({
  selector: 'app-anexo2-pfdn',
  templateUrl: './anexo2-pfdn.component.html',
  styleUrls: ['./anexo2-pfdn.component.css'],
})
export class Anexo2PfdnComponent implements OnInit {
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false; //Se utiliza para bloqueo general de los formularios no modificar
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;

  comboUbigeo: ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;

  tituloModalMantenimiento: string = '';
  verModalMantenimiento: boolean = false;
  tipoAccion: string = '';

  lstData: DataModel[] = [];
  context: DataModel = new DataModel();

  tituloModalMantenimiento2: string = '';
  verModalMantenimiento2: boolean = false;
  tipoAccion2: string = '';

  lstData2: DataModel2[] = [];
  context2: DataModel2 = new DataModel2();

  /************/
  lstTipoDocumento: any[] = [];
  lstExperienciaLaboral: any[] = [];
  requestDatosGeneral: any = {};
  validaPIDEClass: boolean = false;

  formacionAcademicaRequest: any = {};
  experienciaRequestEntity: any = {};
  usuario!: UsuarioModel;
  idSolicitudConcesionResponsableParamIn: number = 0;

  departamentos: any = [];
  provincias: any = [];
  distritos: any = [];

  touchedAll_I: string = "";
  touchedAll_II: string = "";
  touchedAllModal_II: string = "";
  touchedAllModal_III: string = "";

  codigosPFDM = CodigosPFDM;

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private parametroValorService: ParametroValorService,
    private solicitudExperienciaLaboralService: SolicitudExperienciaLaboralService,
    private pideService: PideService,
    private usuarioServ: UsuarioService,
    private solicitudConcesionResponsableService: SolicitudConcesionResponsableService,
    private planificacionService: PlanificacionService,
    private solicitudConcesionService: SolicitudConcesionService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {

    this.listarPorFiltroDepartamento();
    this.listarTipoDocumento();

    if(this.idPFDM){
      this.obtenerSolicitudConcesionResponsable();
      this.obtenerInfoAdjuntoFormacionAcademica();
      this.obtenerInfoAdjuntoExperienciaLaboral()
    }

  }

  listarTipoDocumento() {
    const params = { prefijo: 'TDOCI' };
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoDocumento = result.data.filter((item: any) => (item.codigo === CodigosTiposDoc.DNI || item.codigo === CodigosTiposDoc.RUC));
      }
    );
  };

  obtenerInfoFormacionAcademica() {
    let param = {
      idSolicitudConcesionResponsable: this.idSolicitudConcesionResponsableParamIn,
      idSolicitudConcesion : this.idPFDM,
      descripcionGrado: '',
      descripcionEspecialidad: ''
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionResponsableService.obtenerInformacionResponsableFormacionProfesional(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
      this.lstData = [];
       if (resp.success && resp.data && resp.data.length > 0) {
        resp.data.forEach((element: any) => {
         this.lstData.push(new DataModel({
          idResponsableFormacionAcademica: element.idResponsableFormacionAcademica,
          idResponsableSolicitudConcesion: element.idSolicitudConcesionResponsable,
           grado: element.descripcionGrado,
           especialidad:element.descripcionEspecialidad,
           universidad: element.nombreUniversidad,
           anhio: element.anioTitulo
         }));
        });
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

   

   obtenerInfoAdjuntoFormacionAcademica() {
    let param = {
      idSolicitudConcesion : this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_2,
      codigoSubSeccion: CodigosPFDM.TAB_2_2
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {
        this.formacionAcademicaRequest.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
        this.formacionAcademicaRequest.idArchivo = resp.data[0].idArchivo;
        
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

   obtenerInfoAdjuntoExperienciaLaboral() {
    let param = { 
      idSolicitudConcesion : this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_2,
      codigoSubSeccion: CodigosPFDM.TAB_2_3
    };
  
    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {
        this.experienciaRequestEntity.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
        this.experienciaRequestEntity.idArchivo = resp.data[0].idArchivo;
        
       } 
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

  listarExperienciaLaboral() {
    const params = { idSolicitudConcesionResponsable: this.idSolicitudConcesionResponsableParamIn };
    this.solicitudExperienciaLaboralService.obtenerExperienciaLaboral(params).subscribe(
      (result: any) => {
        this.lstData2 = result.data;
        // this.isDisbledFormu = false;
      }
    );
  };

  private validarFormGeneral(): boolean{
    let valido = true;

    this.requestDatosGeneral.sector = this.requestDatosGeneral.sector?.trim();
    this.requestDatosGeneral.email = this.requestDatosGeneral.email?.trim();

    if(!this.requestDatosGeneral.numeroDocumento) valido = false;
    // if(!this.requestDatosGeneral.sector) valido = false;
    if(!this.requestDatosGeneral.telefonoFijo) valido = false;
    if(!this.requestDatosGeneral.telefonoCelular) valido = false;
    if(!this.requestDatosGeneral.email) valido = false;
  
    if(!valido) this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.");
    
    return valido;
  }

  btnRegDatosGeneral() {
    this.touchedAll_I = "ng-touched";
    if(!this.validarFormGeneral()) return;

    if (!this.requestDatosGeneral.email) {
      this.toast.warn("(*) Debe ingresar un correo.");
      return;
    } else if (!validarEmail(this.requestDatosGeneral.email)) {
      this.toast.warn('(*) Debe ingresar un correo válido.');
      return;
    }

    if(!this.validaPIDEClass) {
      this.toast.warn("(*) Debe Validar PIDE.");
      return;
    }

    if (this.idSolicitudConcesionResponsableParamIn == 0) {
      this.registrarSolicitudConcesionResponsable()
    }else{
      this.actualizarSolicitudConcesionResponsable();

    }

  }

  btnRegFormacionAcademica() {
    this.touchedAll_II = "ng-touched";

    if (!this.idSolicitudConcesionResponsableParamIn || this.idSolicitudConcesionResponsableParamIn == 0) {
      this.toast.warn('(*) Debe registrar los datos generales.');
      return;
    }

    this.formacionAcademicaRequest.profesion = this.formacionAcademicaRequest.profesion?.trim();
    if(!this.formacionAcademicaRequest.profesion) {
      this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.")
      return;
    }

    if(this.idPFDM) this.actualizarFormacionAcademica();
  }

  btnRegExperiencia() {
    if (this.idSolicitudConcesionResponsableParamIn == 0) {
      this.toast.warn('(*) Debe registrar datos generales.');
      return;
    }
    if (this.lstData2.length == 0) return;

    // this.isDisbledFormu = true;

    var listaReg: any[]= [];
    var listaAct: any[]= [];
    var lstArchivos: any[]= [];

    for (let index = 0; index < this.lstData2.length; index++) {
      if(isNaN(Number(this.lstData2[index].idResponsableExperienciaLaboral))){
          let element: any = {
            respuesta: this.experienciaRequestEntity.respuesta,
            idSolicitudConcesionResponsable: this.idSolicitudConcesionResponsableParamIn,
            entidad: this.lstData2[index].entidad,
            cargo: this.lstData2[index].cargo,
            tiempo: this.lstData2[index].tiempo,
            descripcion: this.lstData2[index].descripcion,
            telefonoContacto: this.lstData2[index].telefonoContacto,
            idUsuarioRegistro: this.usuario.idusuario
          }
          listaReg.push(element);

      }else{
        let element: any = {
          respuesta: this.experienciaRequestEntity.respuesta,
          idResponsableExperienciaLaboral: this.lstData2[index].idResponsableExperienciaLaboral,
          idSolicitudConcesionResponsable: this.idSolicitudConcesionResponsableParamIn,
          entidad: this.lstData2[index].entidad,
          cargo: this.lstData2[index].cargo,
          tiempo: this.lstData2[index].tiempo,
          descripcion: this.lstData2[index].descripcion,
          telefonoContacto: this.lstData2[index].telefonoContacto,
          idUsuarioModificacion: this.usuario.idusuario
        }
        listaAct.push(element);
      }
    }

      if(this.experienciaRequestEntity.idArchivo){
        
        lstArchivos.push({
          idSolicitudConcesionArchivo: this.experienciaRequestEntity.idSolicitudConcesionArchivo?this.experienciaRequestEntity.idSolicitudConcesionArchivo:0,
          idSolicitudConcesion: this.idPFDM,
          idArchivo: this.experienciaRequestEntity.idArchivo,
          codigoSeccion: CodigosPFDM.TAB_2,
          codigoSubSeccion: CodigosPFDM.TAB_2_3,
          idUsuarioRegistro: this.usuario.idusuario
        });
      }

      if(listaReg.length > 0){
        let request = {
          idSolicitudConcesionResponsable : this.idSolicitudConcesionResponsableParamIn,
          idSolicitudConcesion : this.idPFDM,
          lstResponsableExperienciaLaboral: listaReg,
          lstResponsableExperienciaLaboralArchivo : lstArchivos,
          idUsuarioModificacion : this.usuario.idusuario
        };
        this.registrarExperienciaLaboral(request);
      }
      if(listaAct.length > 0){
        let request = {
          idSolicitudConcesionResponsable : this.idSolicitudConcesionResponsableParamIn,
          idSolicitudConcesion : this.idPFDM,
          lstResponsableExperienciaLaboral: listaAct,
          lstResponsableExperienciaLaboralArchivo : lstArchivos,
          idUsuarioModificacion : this.usuario.idusuario
        };
        this.actualizarExperienciaLaboral(request);
      }

  }

  registrarExperienciaLaboral(paramsIn: any) {
    this.solicitudExperienciaLaboralService.registrarExperienciaLaboral(paramsIn).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
        this.listarExperienciaLaboral();
      } else {
        this.toast.warn(result?.message);
        // this.isDisbledFormu = false;
      }
    });
  };

  actualizarExperienciaLaboral(paramsIn: any) {
    this.solicitudExperienciaLaboralService.actualizarExperienciaLaboral(paramsIn).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.experienciaRequestEntity.idSolicitudConcesionArchivo=result.data.idSolicitudConcesionArchivo;
        this.toast.ok(result?.message);
        this.listarExperienciaLaboral();
      } else {
        this.toast.warn(result?.message);
        // this.isDisbledFormu = false;
      }
    });
  };

  validarExperienciaLaboral(): boolean {
    let validado = true;

    this.context2.entidad = this.context2.entidad?.trim();
    this.context2.cargo = this.context2.cargo?.trim();
    this.context2.descripcion = this.context2.descripcion?.trim();
    this.context2.telefonoContacto = this.context2.telefonoContacto?.trim();
    
    if (!this.context2.entidad) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la entidad.');
    }
    if (!this.context2.cargo) {
      validado = false;
      this.toast.warn('(*) Debe ingresar el cargo.');
    }
    if(!(this.context2.tiempo)){
      validado = false;
      this.toast.warn('(*) Debe ingresar el tiempo.');
    }
    if (!this.context2.descripcion) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la descripción.');
    }
    if(!(this.context2.telefonoContacto)){
      validado = false;
      this.toast.warn('(*) Debe ingresar el teléfono de referencia.');
    }

    return validado;
  }

  validarFormacioModal(): boolean {
    let validado = true;

    this.context.grado = this.context.grado?.trim();
    this.context.especialidad = this.context.especialidad?.trim();
    this.context.universidad = this.context.universidad?.trim();

    if (!this.context.grado) {
      validado = false;
      this.toast.warn('(*) Debe ingresar el grado académico.');
    }
    if (!this.context.especialidad) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la especialidad.');
    }
    if(!(this.context.universidad)){
      validado = false;
      this.toast.warn('(*) Debe ingresar la universidad.');
    }
    if (!this.context.anhio) {
      validado = false;
      this.toast.warn('(*) Debe ingresar el año de emisión del titulo.');
    }

    return validado;
  }

  changeTipoDoc() {
    this.requestDatosGeneral.numeroDocumento = "";
    this.requestDatosGeneral.nombreCompleto = "";
    this.requestDatosGeneral.direccion = "";
    this.cambiarValidPIDE(false);
  }

  changeNumDoc() {
    this.requestDatosGeneral.nombreCompleto = "";
    this.requestDatosGeneral.direccion = "";
    this.cambiarValidPIDE(false);
  }

  cambiarValidPIDE(valor: boolean) {
    this.validaPIDEClass = valor;
    this.requestDatosGeneral.isValidPIDE = valor;
  }

  validarPide() {
    if (!this.requestDatosGeneral.codigoTipoDocumento) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;
    }

    if (this.requestDatosGeneral.codigoTipoDocumento === CodigosTiposDoc.DNI) {
      if (!this.requestDatosGeneral.numeroDocumento) {
        this.toast.warn('(*) Debe ingresar un número de DNI.');
        return;
      }
      if (this.requestDatosGeneral.numeroDocumento.toString().length !== 8) {
        this.toast.warn('(*) El número de documento de DNI debe tener 8 digitos.');
        return;
      }
      this.consultarDNI({ "numDNIConsulta": this.requestDatosGeneral.numeroDocumento });

    } else if (this.requestDatosGeneral.codigoTipoDocumento === CodigosTiposDoc.RUC) {
      if (!this.requestDatosGeneral.numeroDocumento) {
        this.toast.warn('(*) Debe ingresar un número de RUC.');
        return;
      }
      if (this.requestDatosGeneral.numeroDocumento.toString().length !== 11) {
        this.toast.warn('(*) El número de documento de RUC debe tener 11 digitos.');
        return;
      }
      this.consultarRazonSocial({ "numRUC": this.requestDatosGeneral.numeroDocumento });

    }
  }

  consultarDNI(params: any) {
    
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.datosPersona) {
          let persona = result.dataService.datosPersona;
          this.requestDatosGeneral.nombres = `${persona.prenombres}`;
          this.requestDatosGeneral.apellidoPaterno = `${persona.apPrimer}`;
          this.requestDatosGeneral.apellidoMaterno = `${persona.apSegundo}`;
          this.requestDatosGeneral.nombreCompleto = `${persona.apPrimer} ${persona.apSegundo} ${persona.prenombres}`;
          this.requestDatosGeneral.direccion = persona.direccion;
          this.cambiarValidPIDE(true);
          this.toast.ok('Se validó el DNI en RENIEC.');
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(result.dataService.deResultado);
        }
      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
    }
    )
    
  }

  consultarRazonSocial(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.respuesta.ddp_nombre) {
          let respuesta = result.dataService.respuesta;
          if (result.dataService.respuesta.esHabido) {
            this.requestDatosGeneral.nombres = respuesta.ddp_nombre;
            this.requestDatosGeneral.apellidoPaterno = null;
            this.requestDatosGeneral.apellidoMaterno = null;
            this.requestDatosGeneral.nombreCompleto = respuesta.ddp_nombre;
            this.requestDatosGeneral.direccion = `${respuesta.desc_tipzon} ${respuesta.ddp_nomzon} ${respuesta.ddp_nomvia}`;
            this.cambiarValidPIDE(true);
            this.toast.ok('Se validó existencia de RUC en SUNAT.');
          } else {
            this.cambiarValidPIDE(false);
            this.toast.warn(`RUC ingresado en estado: ${result.dataService.respuesta.desc_estado} y ${result.dataService.respuesta.desc_flag22}.`);
          }
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(`No se ha encontrado información para el RUC ${params.numRUC}.`);
        }

      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
    }
    )
  }




  openModal(data: any, tipo: string): void {
    this.tipoAccion = tipo;
    if (tipo == 'C') {
      this.context = new DataModel();
      this.context.idResponsableFormacionAcademica = this.obtenerCorrelativo();
      this.context.idResponsableSolicitudConcesion = this.idSolicitudConcesionResponsableParamIn;
      this.tituloModalMantenimiento = 'Nuevo Registro';
    } else if (tipo == 'E') {
      this.context = new DataModel(data);
      this.tituloModalMantenimiento = 'Editar Registro';
    }

    this.verModalMantenimiento = true;
  }

  openEliminar(event: Event, data: DataModel): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.removerRegistro(data);
      },
      reject: () => { },
    });
  }

  obtenerCorrelativo(): number{

     let correlativo: number = 0;

    if(this.lstData && this.lstData.length > 0){
      this.lstData.forEach((item: any) => {
        if(correlativo > item.idResponsableFormacionAcademica) correlativo = item.idResponsableFormacionAcademica;
      });
      correlativo -= 1;
    }else{
      correlativo -= 1;
    }

    return correlativo;
  }

  guardar(): void {
    this.touchedAllModal_II = "ng-touched";
    if (!this.validarFormacioModal()) return;

    let obj = this.context;

    if (this.tipoAccion == 'C') {
      this.lstData.push(obj);
    } else {
      let index = this.lstData.findIndex((x) => x.idResponsableFormacionAcademica == obj.idResponsableFormacionAcademica);
      this.lstData[index] = obj;
    }

    this.verModalMantenimiento = false;
  }

  private removerRegistro(data: DataModel): void {

    

    
    const index = this.lstData.indexOf(data, 0);

    let param = {
      idResponsableFormacionAcademica: this.lstData[index].idResponsableFormacionAcademica,
      idUsuarioElimina: this.usuario.idusuario
    };

    


    this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudConcesionResponsableService.eliminarInformacionResponsableFormacionProfesional(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.toast.ok("Se eliminó registro de formación academica correctamente")
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

      this.lstData.splice(index, 1);
  }

  openModal2(data: any, tipo: string): void {
    this.tipoAccion2 = tipo;
    if (tipo == 'C') {
      this.context2 = new DataModel2();
      this.tituloModalMantenimiento2 = 'Nuevo Registro';
    } else if (tipo == 'E') {
      this.context2 = new DataModel2(data);
      this.tituloModalMantenimiento2 = 'Editar Registro';
    }

    this.verModalMantenimiento2 = true;
  }

  openEliminar2(event: Event, data: DataModel2): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if(isNaN(Number(data.idResponsableExperienciaLaboral))){
          this.removerRegistro2(data);
        }else{
          this.eliminarExperienciaLaboral(Number(data.idResponsableExperienciaLaboral));
          this.removerRegistro2(data);
        }
      },
      reject: () => { },
    });
  }

  registrarFormacionAcademica() {

    var lista: any[]= [];

    if(this.lstData && this.lstData.length > 0){
      this.lstData.forEach((item: any) => {
        let element: any = {
          idResponsableFormacionAcademica: item.idResponsableFormacionAcademica,
          idSolicitudConcesionResponsable: 1,
          idSolicitudConcesion: 1,
          codigoGrado: '',
          descripcionGrado: item.grado,
          codigoEspecialidad: '',
          descripcionEspecialidad: item.especialidad,
          nombreUniversidad: item.universidad,
          anioTitulo: item.anhio,
          idUsuarioRegistro: this.usuario.idusuario
        }
        lista.push(element);
      });

    }

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionResponsableService.registrarInformacionResponsableFormacionProfesional(lista)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.obtenerInfoFormacionAcademica();
        this.toast.ok(resp.message);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  actualizarFormacionAcademica() {

    var lista: any[]= [];
    var lstArchivos: any[]= [];

    if(this.lstData && this.lstData.length > 0){
      this.lstData.forEach((item: any) => {
        let element: any = {
          idResponsableFormacionAcademica: item.idResponsableFormacionAcademica,
          idSolicitudConcesionResponsable: item.idResponsableSolicitudConcesion,
          idSolicitudConcesion: this.idPFDM,
          codigoGrado: '',
          descripcionGrado: item.grado,
          codigoEspecialidad: '',
          descripcionEspecialidad: item.especialidad,
          nombreUniversidad: item.universidad,
          anioTitulo: item.anhio,
          idUsuarioRegistro: this.usuario.idusuario,
          idUsuarioModificacion: this.usuario.idusuario
        }
        lista.push(element);
      });

    }

    if(this.formacionAcademicaRequest.idArchivo){
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formacionAcademicaRequest.idSolicitudConcesionArchivo?this.formacionAcademicaRequest.idSolicitudConcesionArchivo: 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formacionAcademicaRequest.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_2,
        codigoSubSeccion: CodigosPFDM.TAB_2_2,
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }
    let request = {
      idSolicitudConcesionResponsable : this.idSolicitudConcesionResponsableParamIn,
      idSolicitudConcesion : this.idPFDM,
      profesion: this.formacionAcademicaRequest.profesion,
      numeroColegiatura: this.formacionAcademicaRequest.nroColegiatura,
      lstResponsableFormacionAcademica: lista,
      lstResponsableFormacionAcademicaArchivo : lstArchivos,
      idUsuarioModificacion : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionResponsableService.actualizarInformacionResponsableFormacionProfesional(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.formacionAcademicaRequest.idSolicitudConcesionArchivo=resp.data.idSolicitudConcesionArchivo;
        this.obtenerInfoFormacionAcademica();
        this.toast.ok(resp.message);
      } else {
        
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  eliminarExperienciaLaboral(idExperienciaLaboral: number) {
    const paramsOut = {
      respuesta: this.experienciaRequestEntity.respuesta,
      idResponsableExperienciaLaboral: idExperienciaLaboral,
      idUsuarioElimina: this.usuario.idusuario
    }
    this.solicitudExperienciaLaboralService.eliminarExperienciaLaboral(paramsOut).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
      } else {
        this.toast.warn(result?.message);
      }
    });
  };

  guardar2(): void {
    this.touchedAllModal_III = "ng-touched";
    if (!this.validarExperienciaLaboral()) return;
    let obj = this.context2;

    if (this.tipoAccion2 == 'C') {
      this.lstData2.push(obj);
    } else {
      let index = this.lstData2.findIndex((x) => x.idResponsableExperienciaLaboral == obj.idResponsableExperienciaLaboral);
      this.lstData2[index] = obj;
    }

    this.verModalMantenimiento2 = false;
  }

  private removerRegistro2(data: DataModel2): void {
    const index = this.lstData2.indexOf(data, 0);
    this.lstData2.splice(index, 1);
  }

  getParamsRegExpLab(dataIn: any) {
    const paramsOut = {
      respuesta: this.experienciaRequestEntity.respuesta,
      idSolicitudConcesionResponsable: this.idSolicitudConcesionResponsableParamIn,
      entidad: dataIn.entidad,
      cargo: dataIn.cargo,
      tiempo: dataIn.tiempo,
      descripcion: dataIn.descripcion,
      telefonoContacto: dataIn.telefonoContacto,
      idUsuarioRegistro: this.usuario.idusuario
    }
    return paramsOut;
  }

  getParamsActExpLab(dataIn: any) {
    const paramsOut = {
      respuesta: this.experienciaRequestEntity.respuesta,
      idResponsableExperienciaLaboral: dataIn.idResponsableExperienciaLaboral,
      idSolicitudConcesionResponsable: this.idSolicitudConcesionResponsableParamIn,
      entidad: dataIn.entidad,
      cargo: dataIn.cargo,
      tiempo: dataIn.tiempo,
      descripcion: dataIn.descripcion,
      telefonoContacto: dataIn.telefonoContacto,
      idUsuarioModificacion: this.usuario.idusuario
    }
    return paramsOut;
  }

  obtenerSolicitudConcesionResponsable() {
    const params = { idSolicitudConcesion: this.idPFDM };
    this.solicitudConcesionResponsableService.obtenerSolicitudConcesionResponsable(params).subscribe(
      (result: any) => {
        this.requestDatosGeneral = result.data;
        if(this.requestDatosGeneral.codigoTipoDocumento=="TDOCDNI"){
          this.requestDatosGeneral.nombreCompleto = this.requestDatosGeneral.apellidoPaterno+" "+this.requestDatosGeneral.apellidoMaterno+" "+this.requestDatosGeneral.nombres;
        }else{
          this.requestDatosGeneral.nombreCompleto = this.requestDatosGeneral.nombres;
        }

        if(this.requestDatosGeneral.idSolicitudConcesionResponsable!=null){
          this.idSolicitudConcesionResponsableParamIn=this.requestDatosGeneral.idSolicitudConcesionResponsable;
        }

        if(this.requestDatosGeneral.nombreCompleto!="" && this.requestDatosGeneral.direccion!=""){
          this.validaPIDEClass = true;
        }

        this.listarInfoSolicita();
        this.setInfoCabeceraFormacionProfesional(this.requestDatosGeneral);
        this.obtenerInfoFormacionAcademica();
        this.listarExperienciaLaboral();
      }
    );
  };

  setInfoCabeceraFormacionProfesional(data: any){
    this.formacionAcademicaRequest.profesion = data.profesion;
    this.formacionAcademicaRequest.nroColegiatura = data.numeroColegiatura;
  }

  registrarSolicitudConcesionResponsable() {

    this.requestDatosGeneral.idSolicitudConcesion=this.idPFDM;
    this.requestDatosGeneral.idUsuarioRegistro=this.usuario.idusuario;

    this.solicitudConcesionResponsableService.registrarSolicitudConcesionResponsable(this.requestDatosGeneral).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.idSolicitudConcesionResponsableParamIn=result.codigo;
        this.toast.ok(result?.message);
      } else {
        this.toast.warn(result?.message);
      }
    });
  };

  actualizarSolicitudConcesionResponsable() {
    this.requestDatosGeneral.idUsuarioModificacion=this.usuario.idusuario;

    this.solicitudConcesionResponsableService.actualizarSolicitudConcesionResponsable(this.requestDatosGeneral).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
      } else {
        this.toast.warn(result?.message);
      }
    });
  };

  selectDPD(tipo: number, origen: string) {
    if (origen == 'd') {
      this.listarPorFilroProvincia(true, tipo);
    }
    if (origen == 'p') {
      this.listarPorFilroDistrito(true, tipo);
    }
  }

  listarPorFiltroDepartamento(ischange: boolean = false, tipo: number = 0) {
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };

    this.planificacionService.listarPorFiltroDepartamento_core_central(params).subscribe((result: any) => {
      if (result.success) {
        this.departamentos = result.data;
        this.requestDatosGeneral.idDepartamento = ischange ? result.data[0].idDepartamento : (this.requestDatosGeneral.idDepartamento || result.data[0].idDepartamento);

        this.listarPorFilroProvincia(ischange, tipo);
      }
    }, (error) => this.errorMensaje(error));
  }

  listarPorFilroProvincia(ischange: boolean = false, tipo: number = 0) {
    let idDepartamento = null;
    idDepartamento = this.requestDatosGeneral.idDepartamento;

    this.planificacionService.listarPorFilroProvincia({ idDepartamento: idDepartamento, }).subscribe((result: any) => {
      if (result.success) {
        //console.log("Lista provincias", result.data);
        this.provincias = result.data;
        this.requestDatosGeneral.idProvincia = ischange ? result.data[0].idProvincia : (this.requestDatosGeneral.idProvincia || result.data[0].idProvincia);

        this.listarPorFilroDistrito(ischange, tipo);
      }
    });
  }

  listarPorFilroDistrito(ischange: boolean = false, tipo: number = 0) {
    let idProvincia = null;
    idProvincia = this.requestDatosGeneral.idProvincia;

    this.planificacionService.listarPorFilroDistrito({ idProvincia: idProvincia }).subscribe((result: any) => {
      if (result.success) {
        this.distritos = result.data;
        this.requestDatosGeneral.idDistrito = ischange ? result.data[0].idDistrito : (this.requestDatosGeneral.idDistrito || result.data[0].idDistrito);
      }
    });
  }

  registrarArchivo(idArchivo: number, condicion: number) {
    switch (condicion) {
      case 1:
        this.formacionAcademicaRequest.idArchivo = idArchivo;
        break;
      case 2:
        
        this.experienciaRequestEntity.idArchivo = idArchivo;

        break;
      default:
        break;
    }
  }

  eliminarArchivo(ok: boolean, condicion: number) {
    switch (condicion) {
      case 1:
        if(ok){
          if(this.formacionAcademicaRequest.idSolicitudConcesionArchivo) this.eliminarInfoAdjuntoFormacionAcademica();
        }
        break;
      case 2:
        
        if(ok){
          if(this.experienciaRequestEntity.idSolicitudConcesionArchivo) this.eliminarInfoAdjuntoExperienciaLaboral();
        }
        break;
      default:
        break;
    }
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  listarInfoSolicita() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        let datos = resp.data[0];
        this.requestDatosGeneral.razonsocial=datos.nombreSolicitanteUnico;
        
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarInfoAdjuntoFormacionAcademica() {

    let request = {
      idSolicitudConcesionArchivo : this.formacionAcademicaRequest.idSolicitudConcesionArchivo,
      idUsuarioElimina : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.formacionAcademicaRequest.idSolicitudConcesionArchivo=0;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  eliminarInfoAdjuntoExperienciaLaboral() {

    let request = {
      idSolicitudConcesionArchivo : this.experienciaRequestEntity.idSolicitudConcesionArchivo,
      idUsuarioModificacion : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.experienciaRequestEntity.idSolicitudConcesionArchivo=0;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }
}

export class DataModel {
  constructor(data?: any) {
    if (data) {
      this.idResponsableFormacionAcademica = data.idResponsableFormacionAcademica;
      this.idResponsableSolicitudConcesion = data.idResponsableSolicitudConcesion;
      this.grado = data.grado;
      this.especialidad = data.especialidad;
      this.universidad = data.universidad;
      this.anhio = data.anhio;
      return;
    }
  }

  idResponsableFormacionAcademica!: number;
  idResponsableSolicitudConcesion!: number;
  grado: String = '';
  especialidad: string = '';
  universidad: string = '';
  anhio!: number;
}

export class DataModel2 {
  constructor(data?: any) {
    if (data) {
      this.idResponsableExperienciaLaboral = data.idResponsableExperienciaLaboral;
      this.entidad = data.entidad;
      this.cargo = data.cargo;
      this.tiempo = data.tiempo;
      this.descripcion = data.descripcion;
      this.telefonoContacto = data.telefonoContacto;
      return;
    } else {
      this.idResponsableExperienciaLaboral = new Date().toISOString();
    }
  }

  idResponsableExperienciaLaboral: string = '';
  entidad: String = '';
  cargo: string = '';
  tiempo: string = '';
  descripcion: string = '';
  telefonoContacto: string = '';
}
