import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ParametroValorService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoConcesion } from 'src/app/model/Concesion/CodigoEstadoConcesion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ContratoConcesionModel } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { SolicitudConcesionEvaluacionService } from 'src/app/service/concesion/solicitud-concesion-evaluacion.service';
import { SolicitudConcesionEvaluacionDetalleService } from 'src/app/service/concesion/solicitud-concesion-evaluluacion-detalle.service';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { ContratoService } from 'src/app/service/contrato.service';

@Component({
  selector: 'app-anexo8-pfdn',
  templateUrl: './anexo8-pfdn.component.html',
  styleUrls: ['./anexo8-pfdn.component.scss']
})
export class Anexo8PfdnComponent implements OnInit {
  @Input('data') data: ContratoConcesionModel = new ContratoConcesionModel();
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;

  usuario!: UsuarioModel;
  codRadio = { FAV: "TRESEVALFAV", DESF: "TRESEVALDFA", SUB: "TRESEVALOBS" };
  evaluado: any = null;
  requestEntity: any = {};
  idEvaluacionSolicitudConcesionParamIn: number = 0;
  evaluacionDetalleRequestEntity: any = {data: []};

  listaItems: any[] = [];
  isShowComunicado: boolean = false;
  isEstadoPublicado: boolean = false;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private solicitudConcesionEvaluacionService: SolicitudConcesionEvaluacionService,
    private solicitudConcesionService: SolicitudConcesionService,
    private usuarioServ: UsuarioService,
    private contratoService: ContratoService,
    private router: Router,
    private parametroValorService: ParametroValorService,
    private solicitudConcesionEvaluacionDetalleService: SolicitudConcesionEvaluacionDetalleService
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.obtenerInfoEvaluacion();
    this.obtenerInfoAdjuntoResultadoEvaluacion();
    this.obtenerResolucion();
    this.cargarElementosMotivosDenegacion();
  }

  //SERVICIOS
  obtenerSolicitudConcesion() {
    let param = { idSolicitudConcesion: this.idPFDM, };
    this.solicitudConcesionService.listarSolicitudConcesion(param).subscribe(resp => {
      if (resp.success && resp.data) {
        this.isEstadoPublicado = resp.data[0].estadoSolicitud === CodigoEstadoConcesion.PUBLICADO;
      } else {
        this.toast.warn(resp.message);
      }
    });
  }

  obtenerInfoEvaluacion() {
    const request = { idSolicitudConcesion: this.idPFDM };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionEvaluacionService.obtenerInfoEvaluacionSolicitudConcesion(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        if (resp.data && resp.data.length > 0) {
          this.idEvaluacionSolicitudConcesionParamIn = resp.data[0].idSolicitudConcesionEvaluacion;
          this.requestEntity.observaciones = resp.data[0].observacion ? resp.data[0].observacion : "";
          this.evaluado = resp.data[0].codigoResultadoEvaluacion || null;
          this.isShowComunicado = resp.data[0].comunicadoEnviado || false;
          this.obtenerSolicitudConcesionEvaluacionDetalle(resp.data[0].idSolicitudConcesionEvaluacion);
        }
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  cargarElementosMotivosDenegacion() {
    var params = { prefijo: "TMDE" };
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe((response: any) => {
      let e: any = {};
      response.data.forEach((element: any) => {
        e = {};
        e.item = element.codigo.substring(4, 5);
        e.codigo = element.codigo;
        e.check = null;
        e.motivo = element.valor1;
        e.idLineamiento = null
        this.listaItems.push(e);
      });

    });
  }

  obtenerSolicitudConcesionEvaluacionDetalle(idSolicitudConcesionEvaluacion: number) {
    const params = {
      idSolicitudConcesionEvaluacion: idSolicitudConcesionEvaluacion,
      codigoSeccion: CodigosPFDM.TAB_8,
      codigoSubSeccion: CodigosPFDM.TAB_8_1
    };
    this.solicitudConcesionEvaluacionDetalleService.listarSolicitudConcesionEvaluacionDetalle(params).subscribe(
      (result: any) => {
        this.evaluacionDetalleRequestEntity.data = result.data;
        for (let i = 0; i < this.listaItems.length; i++) {
          for (let j = 0; j < result.data.length; j++) {
            if (this.listaItems[i].codigo === result.data[j].codigoLineamiento) {
              this.listaItems[i].check = result.data[j].lineamiento ? true : null;
              this.listaItems[i].idLineamiento = result.data[j].idSolicitudConcesionEvaluacionDetalle
            }
          }
        }
      }
    );
  };

  btnDescargarPlantInforme() {
    let params: any={};

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.descargarPlantillaCriteriosCalificacionPropuesta(params).subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, (error) => this.errorMensaje("Ocurrió un error. Contactar con el administrador del sistema", true));
  }

  btnDescargarPlantResol(codRadio: any) {
    let params: any = {
      idSolicitudConcesion: this.idPFDM,
      codRadio: codRadio,
      vertices: localStorage.getItem("coordenadasPFDM") != null ? JSON.parse(''+localStorage.getItem("coordenadasPFDM")?.toString()):[]
    };
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.descargarPlantillaResolucionEvaluacionSolictud(params).subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, (error) => this.errorMensaje("Ocurrió un error. Contactar con el administrador del sistema", true));
  }

  actualizarInfoEvaluacionSolicitudConcesion(codRadio: string, codEstado: string) {
    const params: any = {
      idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
      idSolicitudConcesion: this.idPFDM,
      codigoResultadoEvaluacion: codRadio,
      observacion: this.requestEntity.observaciones,
      idUsuarioRegistro: this.usuario.idusuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionEvaluacionService.actualizarInfoEvaluacionSolicitudConcesion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result?.success && result.validateBusiness !== false) {
        this.enviarSolicitud(codEstado);
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  enviarSolicitud(tipoEnvio: any) {
    const params = {
      "idSolicitudConcesion": this.idPFDM,
      "tipoEnvio": tipoEnvio,
      "idUsuarioModificacion": this.usuario.idusuario
    };
    let dialog_1 = this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.enviarSolicitudConcesion(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        dialog_1.close();
        this.toast.ok(resp.message);
        this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  enviarSolicitudConcesionComunicado() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.enviarSolicitudConcesionComunicado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnAprobarSolicitud() {
    this.actualizarInfoEvaluacionSolicitudConcesion(this.codRadio.FAV, "APROBARSC");
    this.registrarResolucionGuardar();
    this.guardarArchivosAdjuntos(1);
  }

  btnEnviarComunicado() {
    this.enviarSolicitudConcesionComunicado();
  }

  btnDenegarSolicitud() {
    if (!this.validarGuardarEval()) return;
    this.actualizarInfoEvaluacionSolicitudConcesion(this.codRadio.DESF, "DENEGARSC");
  }

  isGuardarDatos: boolean = false;
  btnGuardarDenegacion() {
    if (!this.validarGuardarEval()) return;

    this.isGuardarDatos = true;
    var listaReg: any[] = [];
    if (this.evaluacionDetalleRequestEntity.data.length == 0) {
      for (let index = 0; index < this.listaItems.length; index++) {
        let element: any = {
          idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
          idSolicitudConcesion: this.idPFDM,
          codigoSeccion: CodigosPFDM.TAB_8,
          codigoSubSeccion: CodigosPFDM.TAB_8_1,
          lineamiento: this.listaItems[index].check ? true : false,
          codigoLineamiento: this.listaItems[index].codigo,
          codigoMotivDenegacion: "Denegacion",
          idUsuarioRegistro: this.usuario.idusuario
        }
        listaReg.push(element);
      }

      this.solicitudConcesionEvaluacionDetalleService.registrarSolicitudConcesionEvaluacionDetalle(listaReg).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {
          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      }, (error) => this.errorMensaje(error));
      this.registrarResolucionGuardar();
    } else {
      for (let index = 0; index < this.listaItems.length; index++) {
        let element: any = {
          idSolicitudConcesionEvaluacionDetalle: this.listaItems[index].idLineamiento,
          idSolicitudConcesionEvaluacion: this.idEvaluacionSolicitudConcesionParamIn,
          idSolicitudConcesion: this.idPFDM,
          codigoSeccion: CodigosPFDM.TAB_8,
          codigoSubSeccion: CodigosPFDM.TAB_8_1,
          lineamiento: this.listaItems[index].check ? true : false,
          codigoLineamiento: this.listaItems[index].codigo,
          codigoMotivDenegacion: "Denegacion",
          idUsuarioModificacion: this.usuario.idusuario
        }
        listaReg.push(element);
      }

      this.solicitudConcesionEvaluacionDetalleService.actualizarSolicitudConcesionEvaluacionDetalle(listaReg).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {
          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      });
    }
    this.guardarArchivosAdjuntos(5);
  }

  btnEnviarObs() {
    if (!this.validarObservado()) return;
    this.actualizarInfoEvaluacionSolicitudConcesion(this.codRadio.SUB, "EVALUACIONOBS");
    this.guardarArchivosAdjuntos(5);

  }

  validarGuardarEval(): boolean {
    let validado = true;
    let hayTrue = this.listaItems.some((item: any) => item.check === true);
    if (!hayTrue) {
      validado = false;
      this.toast.warn("(*) Debe seleccionar al menos un motivo.");
    }
    if (!this.requestEntity.idArchivo1) {
      this.toast.warn("(*) Debe adjuntar resolución.");
      validado = false;
    }
    if (!this.requestEntity.idArchivo5) {
      this.toast.warn('(*) Debe adjuntar informe de evaluación.');
      validado = false;
    }

    return validado;
  }

  validarObservado(): boolean {
    let validado = true;
    if (!this.requestEntity.observaciones) {
      this.toast.warn("(*) Debe ingresar las observaciones.");
      validado = false;
    }
    if (!this.requestEntity.idArchivo5) {
      this.toast.warn('(*) Debe adjuntar informe de evaluación.');
      validado = false;
    }
    return validado;
  }

  guardarArchivosAdjuntos(file: number) {
    var lstArchivos: any[] = [];

    if (this.requestEntity.idArchivo5 && file == 5) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.requestEntity.idSolicitudConcesionArchivo5 ? this.requestEntity.idSolicitudConcesionArchivo5 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestEntity.idArchivo5,
        codigoSeccion: CodigosPFDM.TAB_8,
        codigoSubSeccion: CodigosPFDM.TAB_8_1,
        tipoArchivo: CodigosPFDM.TAB_8_1 + "_5",
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    if (this.requestEntity.idArchivo1 && file == 1) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.requestEntity.idSolicitudConcesionArchivo1 ? this.requestEntity.idSolicitudConcesionArchivo1 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestEntity.idArchivo1,
        codigoSeccion: CodigosPFDM.TAB_8,
        codigoSubSeccion: CodigosPFDM.TAB_8_1,
        tipoArchivo: CodigosPFDM.TAB_8_1 + "_5",
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    let dialog = this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudConcesionService.registrarInfoAdjuntosSolicitudConcesion(lstArchivos)
      .pipe(finalize(() => dialog.close()))
      .subscribe(resp => {
        if (resp.success) {
          // this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  registrarResolucionGuardar() {
    let auxArchivos: any[] = [];

    if (!this.requestEntity.idArchivo5) {
      this.toast.warn('(*) Debe adjuntar informe de evaluación.');
      return;
    } else if (!this.requestEntity.idArchivo1) {
      this.toast.warn('(*) Debe adjuntar la resolución.');
      return;
    }

    auxArchivos = [{ "idArchivo": this.requestEntity.idArchivo1 }]

    const params = {
      "nroDocumentoGestion": this.idPFDM,
      "tipoDocumentoGestion": 'TRESEVALFAV',
      "estadoResolucion": null,
      "asunto": null,
      "idUsuarioRegistro": this.usuario.idusuario,
      "listaResolucionArchivo": auxArchivos
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.registrarResolucion(params).subscribe(result => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerResolucion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.obtenerResolucion(this.idPFDM).subscribe(result => {
      this.dialog.closeAll();

      if (result.success && result.data.length > 0) {
        const auxData = result.data[0];
        auxData.listaResolucionArchivo.forEach((item: any) => {
          this.requestEntity.idArchivo1 = item.idArchivo;
          
        });
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInfoAdjuntoResultadoEvaluacion() {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_8,
      codigoSubSeccion: CodigosPFDM.TAB_8_1
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          for (let i = 0; i < resp.data.length; i++) {
            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == (CodigosPFDM.TAB_8_1 + "_5")) {
              this.requestEntity.idSolicitudConcesionArchivo5 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestEntity.idArchivo5 = resp.data[i].idArchivo;
            }

            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == (CodigosPFDM.TAB_8_1 + "_1")) {
              this.requestEntity.idSolicitudConcesionArchivo1 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestEntity.idArchivo1 = resp.data[i].idArchivo;
            }
          }
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  registrarArchivo(id: number, condicion: number) {
    switch (condicion) {
      case 1:
        this.requestEntity.idArchivo1 = id;
        break;
      case 5:
        this.requestEntity.idArchivo5 = id;
        break;

      default:
        break;
    }
  }
  eliminarArchivo(ok: boolean, condicion: number) {
    switch (condicion) {
      case 1:
        if (ok) {
          this.requestEntity.idArchivo1 = 0;
          if (this.requestEntity.idSolicitudConcesionArchivo1) {
            this.eliminarAdjuntos(this.requestEntity.idSolicitudConcesionArchivo1, condicion);
            this.requestEntity.idSolicitudConcesionArchivo1 = 0;
          }
        }
        break;
      case 5:
        if (ok) {
          this.requestEntity.idArchivo5 = 0;
          if (this.requestEntity.idSolicitudConcesionArchivo5) {
            this.eliminarAdjuntos(this.requestEntity.idSolicitudConcesionArchivo5, condicion);
            this.requestEntity.idSolicitudConcesionArchivo5 = 0;
          }
        }
        break;
      default:
        break;
    }
  }

  eliminarAdjuntos(id: number, condicion: number) {

    let request = {
      idSolicitudConcesionArchivo: id,
      idUsuarioElimina: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
