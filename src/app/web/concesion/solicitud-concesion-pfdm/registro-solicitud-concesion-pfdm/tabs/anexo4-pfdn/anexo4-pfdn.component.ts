import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, ParametroValorService, PlanManejoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { ContratoConcesionModel, DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from "rxjs/operators";
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { AnexosService } from 'src/app/service/plan-operativo-concesion-maderable/anexos.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { textChangeRangeIsUnchanged } from 'typescript';

@Component({
  selector: 'app-anexo4-pfdn',
  templateUrl: './anexo4-pfdn.component.html',
  styleUrls: ['./anexo4-pfdn.component.scss']
})
export class Anexo4PfdnComponent implements OnInit {
  @Input() data: ContratoConcesionModel = new ContratoConcesionModel();
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  tiposArchivos = TiposArchivos;
  filePanelFoto:any = {};
  comboUbigeo:ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;
  criterioOtros:boolean = false;
  idPGMF = 0;
  codProceso = 'PFDMA4';
  codigoSubSeccion = 'PFDMA4_SS3';

  idSolicitudConcesionAreaParamIn: number = 0;
  formDatoSolicitudConcesion: any = {};
  formDatoAnexoSolicitudConcesion: any = {};
  formDatoMapaSolicitudConcesion: any = {};
  usuarioModel!: UsuarioModel;

  informacionSolicitante: any = {};
  informacionAreaSolicitada: any = {};
  idConcesionArea=0;//cambiar por el valor dado de maycol
  lsDataCriterio: any = {};
  mostrarOtroCriterio=false;

  codigosPFDM = CodigosPFDM;
  constructor(
    public dialog: MatDialog,
    private toast: ToastService,
    private archivoServ: ArchivoService,
    private usuario: UsuarioService,
    private planManejoService: PlanManejoService,
    private solicitudConcesionService: SolicitudConcesionService,
    private parametroValorService: ParametroValorService
  ) {
    this.usuarioModel = this.usuario.usuario;
  }


  ngOnInit(): void {
    //this.listarArchivosAnexo();
    this.obtenerInfoSolicitante();
    this.obtenerInfoAreaSolicitada();
    this.cargarCriterioInicial();

    if(this.idPFDM) {
      this.obtenerInfoAnexoFormatoEstudioTecnico();
      this.obtenerInfoAnexoMapa();
    }


  }

  listarArchivosAnexo() {
    let params = { idPlanManejo: this.idPGMF, subCodigoProceso: CodigosPFDM.TAB_4_3 };

    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.listarPorFiltroPlanManejoArchivo(params).subscribe((result: any) => {
      load.close();
      if (result.success && result.data.length > 0) {
        result.data.forEach((item: any) => {
          this.setIInfoArchivo(item.idTipoDocumento, item);
        });
      }
    }, (error) => {
      this.errorMensaje(error);
      load.close()
    });
  }

  btnRegDeterminacionTamanio(){
    if (!this.validarDetTam()) return;

    if(this.idSolicitudConcesionAreaParamIn == 0){
      let request = {
        idSolicitudConcesion: this.idPFDM,
        superficieHa: 0.0,
        codigoUbigeo: "",
        zonaUTM: 0,
        observacion: "",
        justificacion: this.formDatoSolicitudConcesion.justificacion,
        analisisMapa: this.formDatoSolicitudConcesion.mapa,
        delimitacion: this.formDatoSolicitudConcesion.tamanioConcesion,
        observacionTamanio: this.formDatoSolicitudConcesion.observaciones,
        fuenteBibliografica: "",
        idUsuarioRegistro : this.usuarioModel.idusuario
      };

      this.solicitudConcesionService.registrarInformacionAreaSolicitada(request).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {


          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      });
    }else{
      let request = {
        idSolicitudConcesionArea: this.idSolicitudConcesionAreaParamIn,
        idSolicitudConcesion: this.idPFDM,
        justificacion: this.formDatoSolicitudConcesion.justificacion,
        analisisMapa: this.formDatoSolicitudConcesion.mapa,
        delimitacion: this.formDatoSolicitudConcesion.tamanioConcesion,
        observacionTamanio: this.formDatoSolicitudConcesion.observaciones,
        idUsuarioModificacion : this.usuarioModel.idusuario
      };
      this.solicitudConcesionService.actualizarInformacionAreaSolicitada(request).subscribe((result: any) => {
        if (result?.success && result.validateBusiness !== false) {


          this.toast.ok(result?.message);
        } else {
          this.toast.warn(result?.message);
        }
      });
    }

    this.guardarActualizarCriterioDeterminacion();
  }

  validarDetTam(): boolean {
    let validado = true;

    if (!this.formDatoSolicitudConcesion.justificacion) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la justificación de los criterios utilizados.');
    }
    if (!this.formDatoSolicitudConcesion.mapa) {
      validado = false;
      this.toast.warn('(*) Debe ingresar de mapas temáticos.');
    }
    if(!(this.formDatoSolicitudConcesion.tamanioConcesion)){
      validado = false;
      this.toast.warn('(*) Debe ingresar la delimitación de tamaño de concesión.');
    }
    // if (!this.formDatoSolicitudConcesion.observaciones) {
    //   validado = false;
    //   this.toast.warn('(*) Debe ingresar las observaciones.');
    // }
    return validado;
  }

  registrarArchivoAdjunto(file: any, codigo: string) {
    let params = {
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.usuario.idUsuario,
      codTipo: CodigosPFDM.TAB_4,
      subCodTipo: CodigosPFDM.TAB_4_3,
      codTipoDocumento: codigo,
      idPlanManejoArchivo: file.idPlanManejoArchivo || 0
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.regitrarArchivoPlanManejo(params, file.file).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        this.setIdArchivoRegistrado(codigo, result.data);
      } else {
        this.toast.warn(result.message);
        this.limpiarArchivoFile(codigo);
      }
    }, (error) => {
      this.limpiarArchivoFile(codigo);
      this.errorMensaje(error, true);
    });
  }

  eliminarArchivoAdjunto(file: any, codigo: string) {
    let params = {
      idPlanManejoArchivo: file.idPlanManejoArchivo,
      idUsuarioElimina: this.usuario.idUsuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.eliminarPlanManejoArchivo(params).subscribe((result: any) => {
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
        this.limpiarArchivoFile(codigo);
        this.eliminarArchivo(file.idArchivo);
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarArchivo2(idAdjuntoOut: number) {
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idUsuario).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  setIInfoArchivo(codigoIn: string, datos: any) {
    switch (codigoIn) {
      case TiposArchivos.TDOCEVIMAPGE: this.filePanelFoto = datos; break;
      default: break;
    }
  }

  setIdArchivoRegistrado(codigoIn: string, datos: any) {
    switch (codigoIn) {
     case TiposArchivos.TDOCEVIMAPGE: this.filePanelFoto.idArchivo = datos.idArchivo; this.filePanelFoto.idPlanManejoArchivo = datos.idPGMFArchivo; break;
     default: break;
    }
  }

  limpiarArchivoFile(codigoIn: string) {
    switch (codigoIn) {
      case TiposArchivos.TDOCEVIMAPGE: this.filePanelFoto = {}; break;
      default: break;
    }
  }

  registrarArchivo(idArchivo: number) {
    this.formDatoAnexoSolicitudConcesion.idArchivo = idArchivo;
  }
  registrarMapaArchivo(idArchivo: number) {
    this.formDatoMapaSolicitudConcesion.idArchivo = idArchivo;
  }
  eliminarMapaArchivo(ok: boolean) {
    if(ok){
      if(this.formDatoMapaSolicitudConcesion.idSolicitudConcesionArchivo) {
        this.eliminarInfoAdjuntoEstudioTecnico();
      }
    }

}
  eliminarArchivo(ok: boolean) {
        if(ok){
          if(this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo) {
            this.eliminarInfoAdjuntoEstudioTecnico();
          }
        }

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  //SERVICIOS
  obtenerInfoSolicitante() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.informacionSolicitante = resp.data[0];

      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInfoAreaSolicitada() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAreaSolicitada(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.informacionAreaSolicitada = resp.data[0];
        this.idConcesionArea=this.informacionAreaSolicitada.idSolicitudConcesionArea;
        this.idSolicitudConcesionAreaParamIn =this.informacionAreaSolicitada.idSolicitudConcesionArea;
        this.formDatoSolicitudConcesion.justificacion = this.informacionAreaSolicitada.justificacion;
        this.formDatoSolicitudConcesion.mapa = this.informacionAreaSolicitada.analisisMapa;
        this.formDatoSolicitudConcesion.tamanioConcesion = this.informacionAreaSolicitada.delimitacion;
        this.formDatoSolicitudConcesion.observaciones = this.informacionAreaSolicitada.observacionTamanio;
        this.formDatoSolicitudConcesion.fuenteBibliografica = this.informacionAreaSolicitada.fuenteBibliografica;
      }
    }, (error) => this.errorMensaje(error, true));
  }


 cargarCriterioInicial() {
    this.lsDataCriterio = [];

    var params = { prefijo: "SCCT" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {

        let e : any = {};
        response.data.forEach((element:any) => {
          e = {};
          e.idSolicitudConcesionCriterioArea=0
          e.codigoCriterio = element.codigo;
          e.nombreCriterio = element.valor1;
          e.estadoCriterio = false;

          this.lsDataCriterio.push(e)
       });

       this.listarCriterioAreaSolicitudConcesion();

      });
  }

  listarCriterioAreaSolicitudConcesion() {


    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.listarCriterioAreaSolicitudConcesion(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.success && response.data && response.data.length > 0) {
          //this.lsDataCriterio =  response.data;

          let resp=response.data;

          let objTemp: any =[];

          this.lsDataCriterio.forEach((item: any) => {

            let itemTemp = resp.filter((t: any) => t.codigoCriterio === item.codigoCriterio)[0];
            if(itemTemp!=null){
              objTemp.push(itemTemp);
            }else{
              objTemp.push(item);
            }

          });

          this.lsDataCriterio=objTemp;

          

        }else{
          //this.cargarCriterioInicial();
        }

/*
        this.lsDataCriterio=this.lsDataCriterio.sort((a:any, b:any) => {
          if(a.nombreCriterio < b.nombreCriterio) return 1;
          if(a.nombreCriterio > b.nombreCriterio) return -1;

          return 0;
        })*/

        //this.lsDataCriterio=this.lsDataCriterio.sort((a: any,b: any)=> a.nombreCriterio.localeCompare(b.nombreCriterio) );

      });
  }

  validarEstadoCriterio(item: any){

    if(item.codigoCriterio=='SCCTOT' && item.estadoCriterio){
      this.mostrarOtroCriterio=true;
    }else{
      this.mostrarOtroCriterio=false;
    }

  }

  guardarCriterio(params: any){
    params.idSolicitudConcesionArea= this.idConcesionArea;
    params.idUsuarioRegistro= this.usuario.idUsuario
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarCriterioAreaSolicitudConcesion(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
      }, (error) => {
        this.errorMensaje(error);
        this.dialog.closeAll();
      });
  }
  actualizarCriterio(params: any){
    params.idUsuarioModificacion= this.usuario.idUsuario
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarSolicitudConcesionCriterioArea(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
      }, (error) => {
        this.errorMensaje(error);
        this.dialog.closeAll();
      });
  }

  guardarActualizarCriterioDeterminacion(){

    for (let item of this.lsDataCriterio) {
      if (item.idSolicitudConcesionCriterioArea == 0) {
        this.guardarCriterio(item);
      }else{
        this.actualizarCriterio(item);
      }
  }
  this.listarCriterioAreaSolicitudConcesion();

  }

  obtenerInfoAnexoFormatoEstudioTecnico() {
    let param = {
      idSolicitudConcesion : this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_4,
      codigoSubSeccion: CodigosPFDM.TAB_4_3
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {
        this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
        this.formDatoAnexoSolicitudConcesion.idArchivo = resp.data[0].idArchivo;
        
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }
   obtenerInfoAnexoMapa(){
    let param = {
      idSolicitudConcesion : this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_4,
      codigoSubSeccion: CodigosPFDM.TAB_4_3_2
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {
        this.formDatoMapaSolicitudConcesion.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
        this.formDatoMapaSolicitudConcesion.idArchivo = resp.data[0].idArchivo;
        
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }



  btnRegAnexos() {
    if(!this.validarRegAnexos()) return;

    let data = {
      idSolicitudConcesionArea :this.idSolicitudConcesionAreaParamIn? this.idSolicitudConcesionAreaParamIn : 0,
      fuenteBibliografica: this.formDatoSolicitudConcesion.fuenteBibliografica
    };

    if(this.idPFDM) {
      if(this.idSolicitudConcesionAreaParamIn && this.idSolicitudConcesionAreaParamIn  > 0) this.actualizarAnexoFormatoEstudioTecnico(data);
      else this.registrarAnexoFormatoEstudioTecnico(data);
    }
  }

  registrarAnexoFormatoEstudioTecnico(request: any) {
    request.idSolicitudConcesion = this.idPFDM;
    request.idUsuarioRegistro = this.usuario.idUsuario;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInformacionAreaSolicitada(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        
        this.idSolicitudConcesionAreaParamIn = resp.data.idSolicitudConcesionArea;

        this.obtenerInfoAnexoFormatoEstudioTecnico();
        this.obtenerInfoAnexoMapa();
        this.toast.ok(resp.message);
      } else {
        
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  actualizarAnexoFormatoEstudioTecnico(request: any) {
    var lstArchivos: any[]= [];
    request.idSolicitudConcesion = this.idPFDM;
    request.idUsuarioModificacion = this.usuario.idUsuario;

    if(this.formDatoAnexoSolicitudConcesion.idArchivo){
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo?this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo: 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formDatoAnexoSolicitudConcesion.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_4,
        codigoSubSeccion: CodigosPFDM.TAB_4_3,
        idUsuarioRegistro: this.usuario.idUsuario,
        idUsuarioModificacion: this.usuario.idUsuario
      });
    }
    if(this.formDatoMapaSolicitudConcesion.idArchivo){
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formDatoMapaSolicitudConcesion.idSolicitudConcesionArchivo?this.formDatoMapaSolicitudConcesion.idSolicitudConcesionArchivo: 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formDatoMapaSolicitudConcesion.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_4,
        codigoSubSeccion: CodigosPFDM.TAB_4_3_2,
        idUsuarioRegistro: this.usuario.idUsuario,
        idUsuarioModificacion: this.usuario.idUsuario
      });
    }
    request.lstArchivo = lstArchivos;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarInfoAreaSolicitudConcesion(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {

        this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo=resp.data.idSolicitudConcesionArchivo;

        
        this.toast.ok(resp.message);
        this.obtenerInfoAnexoFormatoEstudioTecnico();
        this.obtenerInfoAnexoMapa();

      } else {
        
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  validarRegAnexos(): boolean {
    let valido = true;
    if (document.getElementsByClassName('anexo4Anexos_required').length > 0) {
      if (!this.formDatoSolicitudConcesion.fuenteBibliografica) {
        this.toast.warn('(*) Debe ingresar las fuentes bibliográficas consultadas.');
        valido = false;
      }
    }
    return valido;

  }

  eliminarInfoAdjuntoEstudioTecnico() {

    let request = {
      idSolicitudConcesionArchivo : this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo,
      idUsuarioElimina : this.usuario.idUsuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.formDatoAnexoSolicitudConcesion.idSolicitudConcesionArchivo=0;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }
  eliminarMapa() {

    let request = {
      idSolicitudConcesionArchivo : this.formDatoMapaSolicitudConcesion.idSolicitudConcesionArchivo,
      idUsuarioElimina : this.usuario.idUsuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {
        this.formDatoMapaSolicitudConcesion.idSolicitudConcesionArchivo=0;
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }
}
