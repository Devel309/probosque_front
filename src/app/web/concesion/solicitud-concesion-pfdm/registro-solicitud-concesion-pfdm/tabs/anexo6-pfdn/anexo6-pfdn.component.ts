import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService, PlanificacionService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { ContratoConcesionModel, DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from "rxjs/operators";
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-anexo6-pfdn',
  templateUrl: './anexo6-pfdn.component.html',
  styleUrls: ['./anexo6-pfdn.component.scss']
})
export class Anexo6PfdnComponent implements OnInit {
  @Input() data: ContratoConcesionModel = new ContratoConcesionModel();
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;

  codProceso = '';
  codigoSubSeccion = '';

  hoy: any = new Date();
  concesion: any = null;
  comboUbigeo: ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;
  act_complementaria1: boolean = false;
  act_complementaria2: boolean = false;
  act_complementaria3: boolean = false;
  act_complementaria4: boolean = false;
  act_complementaria5: boolean = false;

  lstData: any[] = [];
  lstDataComplementaria: any[] = [];

  requestInfoSolicitante: any = {};
  requestInfoSolicitado: any = { vigenciaAnio: 40, flagArchivoR6_4_1: false, flagArchivoR6_4_2: false, flagArchivoR6_4_3: false, flagArchivoR6_6_7: false, flagArchivoR6_6_8: false };
  requestNotifica: any = {};
  requestActividad: any = {};
  requestArea: any = {};
  experienciaSolicitanteRequest: any = {};
  idSolicitudConcesionAreaParamIn: number = 0;

  formEvidenciaFactorAmbiBiologico: any = {};
  formEvidenciaFactorSocioEconoCultural: any = {};
  formEvidenciaManejoConcesion: any = {};
  formDatoMapaUbicacion: any = {};
  formDatoMapaAreaConcesion: any = {};

  departamentos: any = [];
  solicitudConcesion: any = [];
  departamentoSelect: any = null;
  provincias: any = [];
  provinciaSelect: any = null;
  distritos: any = [];
  distritoSelect: any = null;
  superficie: string = '';

  usuario!: UsuarioModel;
  MODALIDAD: { PFDM: string, ECOT: string, CONS: string, } = {
    PFDM: "SCMODPFDM", ECOT: "SCMODECOT", CONS: "SCMODCONS",
  }

  codigosPFDM = CodigosPFDM;

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private parametroValorService: ParametroValorService,
    private planificacionService: PlanificacionService,
    private solicitudConcesionService: SolicitudConcesionService) {
    this.usuario = this.usuarioServ.usuario;

  }

  ngOnInit(): void {
    this.listarInfoSolicita();
    this.listarPorFiltroDepartamento(false);
    this.obtenerInfoAreaSolicitada();
    this.obtenerInfoAdjuntosCaractBiofisica();
    this.obtenerInfoArchivoSustentos();

    if (this.idPFDM) {
      this.obtenerInfoAdjuntoExperienciaSolicitante();
      this.obtenerInfoAEvidenciaAdjuntoOtorgamientoConcesion();
      this.obtenerInfoAnexoFormatoPropuesta();
    }

    let d: any = { idSolicitudConcesion: this.idPFDM };
    this.obtenerInformacionActividadFuenteFinanciamiento(d);
    this.obtenerInformacionMeta(d);
  }

  registrarMeta() {
    if (!this.requestInfoSolicitado.metas) {
      this.toast.warn('(*) Debe ingresar la justificación de los criterios utilizados.');
      return;
    }

    let request = {
      idSolicitudConcesion: this.idPFDM,
      metas: this.requestInfoSolicitado.metas,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.solicitudConcesionService.actualizarSolicitudConcesion(request).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
      } else {
        this.toast.warn(result?.message);
      }
    });
  }

  registrarCaractBiofisica() {
    if (!this.validaCaractBiofisica()) return;

    var lstArchivoEnvidencia: any[] = [];

    if (this.requestInfoSolicitado.idArchivoR6_4_1) {
      lstArchivoEnvidencia.push({
        idSolicitudConcesionArchivo: this.requestInfoSolicitado.flagArchivoR6_4_1 ? this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_1 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestInfoSolicitado.idArchivoR6_4_1,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_4,
        tipoArchivo: "Fisica",
        idUsuarioRegistro: this.usuario.idusuario
      });
    }
    if (this.requestInfoSolicitado.idArchivoR6_4_2) {
      lstArchivoEnvidencia.push({
        idSolicitudConcesionArchivo: this.requestInfoSolicitado.flagArchivoR6_4_2 ? this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_2 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestInfoSolicitado.idArchivoR6_4_2,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_4,
        tipoArchivo: "Biologica",
        idUsuarioRegistro: this.usuario.idusuario
      });
    }
    if (this.requestInfoSolicitado.idArchivoR6_4_3) {
      lstArchivoEnvidencia.push({
        idSolicitudConcesionArchivo: this.requestInfoSolicitado.flagArchivoR6_4_3 ? this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_3 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestInfoSolicitado.idArchivoR6_4_3,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_4,
        tipoArchivo: "Socioeconomica",
        idUsuarioRegistro: this.usuario.idusuario
      });
    }

    let params = {
      idSolicitudConcesionArea: this.idSolicitudConcesionAreaParamIn,
      caractFisica: this.requestInfoSolicitado.caractFisica,
      caractBiologica: this.requestInfoSolicitado.caractBiologica,
      caractSocioeconomica: this.requestInfoSolicitado.caractSocioeconomica,
      lstArchivo: lstArchivoEnvidencia,
      idUsuarioModificacion: this.usuario.idusuario
    };

    this.solicitudConcesionService.actualizarInfoAreaSolicitudConcesion(params).subscribe((result: any) => {
      if (result?.success && result.validateBusiness !== false) {
        this.obtenerInfoAdjuntosCaractBiofisica();
        this.toast.ok(result?.message);
      } else {
        this.toast.warn(result?.message);
      }
    });
  }

  validaCaractBiofisica(): boolean {
    let validado = true;

    if (!this.requestInfoSolicitado.caractFisica) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la caracterización física.');
    }
    if (!this.requestInfoSolicitado.caractBiologica) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la caracterizacio¿ón biológica.');
    }
    if (!(this.requestInfoSolicitado.caractSocioeconomica)) {
      validado = false;
      this.toast.warn('(*) Debe ingresar la caracterización socioeconómica.');
    }

    return validado;
  }

  registrarArchivo(idArchivo: number, condicion: number) {
    switch (condicion) {
      case 1:
        this.requestInfoSolicitado.idArchivoR6_4_1 = idArchivo;
        break;
      case 2:
        this.requestInfoSolicitado.idArchivoR6_4_2 = idArchivo;
        break;
      case 3:
        this.requestInfoSolicitado.idArchivoR6_4_3 = idArchivo;
        break;
      case 4:
        this.formEvidenciaFactorAmbiBiologico.idArchivo = idArchivo;
        break;
      case 5:
        this.formEvidenciaFactorSocioEconoCultural.idArchivo = idArchivo;
        break;
      case 6:
        this.formEvidenciaManejoConcesion.idArchivo = idArchivo;
        break;
      case 7:
        this.requestInfoSolicitado.idArchivoR6_6_7 = idArchivo;
        break;
      case 8:
        this.requestInfoSolicitado.idArchivoR6_6_8 = idArchivo;
        break;
      case 9:
        this.experienciaSolicitanteRequest.idArchivo = idArchivo;
        break;
      case 10:
        this.formDatoMapaUbicacion.idArchivo = idArchivo;
        break;
      case 11:
        this.formDatoMapaAreaConcesion.idArchivo = idArchivo;
        break;
      default:
        break;
    }
  }

  eliminarArchivo(ok: boolean, condicion: number) {
    switch (condicion) {
      case 1:
        if(ok){
          if(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_1) {
            this.eliminarAdjuntos(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_1);
            this.requestInfoSolicitado.flagArchivoR6_4_1 = false;
            this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_1=0;
          }
        }
        break;
      case 2:
        if(ok){
          if(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_2) {
            this.eliminarAdjuntos(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_2);
            this.requestInfoSolicitado.flagArchivoR6_4_2 = false;
            this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_2=0;
          }
        }
        break;
      case 3:
        if(ok){
          if(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_3) {
            this.eliminarAdjuntos(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_3);
            this.requestInfoSolicitado.flagArchivoR6_4_3 = false;
            this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_3=0;
          }
        }
        break;
      case 4:
        if(ok){
          if(this.formEvidenciaFactorAmbiBiologico.idSolicitudConcesionArchivo) {
            this.eliminarAdjuntos(this.formEvidenciaFactorAmbiBiologico.idSolicitudConcesionArchivo);
            this.formEvidenciaFactorAmbiBiologico.idSolicitudConcesionArchivo=0;
          }
        }
        break;
      case 5:
        if(ok){
          if(this.formEvidenciaFactorSocioEconoCultural.idSolicitudConcesionArchivo) {
            this.eliminarAdjuntos(this.formEvidenciaFactorSocioEconoCultural.idSolicitudConcesionArchivo);
            this.formEvidenciaFactorSocioEconoCultural.idSolicitudConcesionArchivo=0;
          }
        }
        break;
      case 6:
        if(ok){
          if(this.formEvidenciaManejoConcesion.idSolicitudConcesionArchivo) {
            this.eliminarAdjuntos(this.formEvidenciaManejoConcesion.idSolicitudConcesionArchivo);
            this.formEvidenciaManejoConcesion.idSolicitudConcesionArchivo=0;
          }
        }
        break;
      case 7:
        if(ok){
          if(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_7) {
            this.eliminarAdjuntos(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_7);
            this.requestInfoSolicitado.idArchivoR6_6_7 = 0;
            this.requestInfoSolicitado.flagArchivoR6_6_7 = false;
          }
        }
        break;
      case 8:
        if(ok){
          if(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_8) {
            this.eliminarAdjuntos(this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_8);
            this.requestInfoSolicitado.idArchivoR6_6_8 = 0;
            this.requestInfoSolicitado.flagArchivoR6_6_8 = false;
          }
        }
        break;
      case 9:
        if(ok){
          if(this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo ) {
            this.eliminarAdjuntos(this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo );
            this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo  = 0;
          }
        }
        break;
      case 10:
        if(ok){
          if(this.formDatoMapaUbicacion.idSolicitudConcesionArchivo ) {
            this.eliminarAdjuntos(this.formDatoMapaUbicacion.idSolicitudConcesionArchivo );
            this.formDatoMapaUbicacion.idSolicitudConcesionArchivo  = 0;
          }
        }
        break;
      case 11:
        if(ok){
          if(this.formDatoMapaAreaConcesion.idSolicitudConcesionArchivo ) {
            this.eliminarAdjuntos(this.formDatoMapaAreaConcesion.idSolicitudConcesionArchivo );
            this.formDatoMapaAreaConcesion.idSolicitudConcesionArchivo  = 0;
          }
        }
        break;
      default:
        break;
    }
  }

  eliminarAdjuntos(id: number) {

    let request = {
      idSolicitudConcesionArchivo : id,
      idUsuarioElimina : this.usuario.idusuario
    };

   this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarInformacionAdjuntoSolicitudConcesion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe(resp => {
      if (resp.success) {

      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  listarInfoSolicita() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        let datos = resp.data[0];
        this.setDataSolicita(datos);

      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInformacionActividadFuenteFinanciamiento(param: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionActividadFuenteFinanciamiento(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {

        if (resp.data && resp.data.length > 0) {

          /* this.lstData = resp.data.filter(
            (eins: any) => eins.complementario ===false
          ); */

          this.lstDataComplementaria = resp.data.filter(
            (eins: any) => eins.complementario
          );

          this.lstDataComplementaria.forEach(element => {
            element.check = null;
            if (element.descripcion !== null && element.descripcion !== undefined && element.descripcion !== "") {
              element.check = true;
            }
          });

        } else {
          this.cargarActividadComplementaria();
        }
      }
    }, (error) => this.errorMensaje(error, true));
  }

  obtenerInformacionMeta(param: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.listarSolicitudConcesion(param).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        this.requestInfoSolicitado.metas = resp.data[0].metas;

      }
    }, (error) => this.errorMensaje(error, true));
  }
  private cargarActividadComplementaria() {
    this.lstDataComplementaria = [];

    var params = { prefijo: "SOLCONACTC" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {

        let e: any = {};
        response.data.forEach((element: any) => {
          e = {};
          e.codigoActividad = element.codigo;
          e.nombre = element.valor1;

          this.lstDataComplementaria.push(e)
        });

      });
  }
  obtenerInfoAreaSolicitada() {
    const params = { "idSolicitudConcesion": this.idPFDM };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAreaSolicitada(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.length > 0) {
        let datosArea = resp.data[0];
        this.idSolicitudConcesionAreaParamIn = datosArea.idSolicitudConcesionArea;
        this.requestInfoSolicitado.caractFisica = datosArea.caractFisica;
        this.requestInfoSolicitado.caractBiologica = datosArea.caractBiologica;
        this.requestInfoSolicitado.caractSocioeconomica = datosArea.caractSocioeconomica;
        this.setInfoAreaSolicitada(datosArea);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  setInfoAreaSolicitada(datos: any) {
    this.idSolicitudConcesionAreaParamIn = datos.idSolicitudConcesionArea;
    this.requestArea.superficieHa = datos.superficieHa;
    this.requestArea.factorAmbientalBiologico = datos.factorAmbientalBiologico;
    this.requestArea.factorSocioEconocultural = datos.factorSocioEconocultural;
    this.requestArea.manejoConcesion = datos.manejoConcesion;
    /* this.requestArea.zonaUTM = datos.zonaUTM;
    this.requestArea.observacion = datos.observacion;

    this.departamentoAreaSelect = datos.idDepartamentoArea;
    this.provinciaAreaSelect = datos.idProvinciaArea;
    this.distritoAreaSelect = datos.codigoUbigeo;
    this.listarPorFilroProvincia(false, 2); */
  }


  setDataSolicita(datos: any) {
    //Info Solicitante
    this.requestInfoSolicitante.idSolicitudConcesion = datos.idSolicitudConcesion;
    this.requestInfoSolicitante.idSolicitante = datos.idSolicitante;
    this.requestInfoSolicitante.nroPartidaRegistral = datos.nroPartidaRegistral;
    this.requestInfoSolicitante.numeroDireccion = datos.numeroDireccion;
    this.requestInfoSolicitante.sectorCaserio = datos.sectorCaserio;
    this.requestInfoSolicitante.telefonoFijo = datos.telefonoFijo;
    this.requestInfoSolicitante.telefonoCelular = datos.telefonoCelular;
    this.requestInfoSolicitante.correoElectronico = datos.correoElectronico;
    this.requestInfoSolicitante.nombreSolicitanteUnico = datos.nombreSolicitanteUnico;
    this.requestInfoSolicitante.direccionSolicitante = datos.direccionSolicitante;
    this.requestInfoSolicitante.idDepartamentosolicitante = datos.idDepartamentosolicitante;
    this.requestInfoSolicitante.idProvinciaSolicitante = datos.idProvinciaSolicitante;
    this.requestInfoSolicitante.idDistritoSolicitante = datos.idDistritoSolicitante;
    this.requestInfoSolicitante.idUsuarioModificacion = this.usuario.idusuario;
    this.requestInfoSolicitante.nombreReprLegal = datos.nombreReprLegal;
    this.requestInfoSolicitante.direccionReprLegal = datos.direccionReprLegal;



    this.requestInfoSolicitante.tipoDocumentoSolicitante = datos.tipoDocumentoSolicitante;
    this.requestInfoSolicitante.soliDni = datos.tipoDocumentoSolicitante === CodigosTiposDoc.DNI
      ? datos.numeroDocumentoSolicitanteUnico : "---";
    this.requestInfoSolicitante.soliRuc = datos.tipoDocumentoSolicitante === CodigosTiposDoc.RUC
      ? datos.numeroDocumentoSolicitanteUnico : "---";

    //Info Solicitado
    this.requestInfoSolicitado.idSolicitudConcesion = datos.idSolicitudConcesion;
    this.requestInfoSolicitado.idSolicitante = datos.idSolicitante;
    this.requestInfoSolicitado.codigoModalidad = datos.codigoModalidad;
    this.requestInfoSolicitado.objetivo = datos.objetivo;
    this.requestInfoSolicitado.recurso = datos.recurso;
    this.requestInfoSolicitado.importancia = datos.importancia;
    this.requestInfoSolicitado.descripcionProyecto = datos.descripcionProyecto;
    this.requestInfoSolicitado.idUsuarioModificacion = this.usuario.idusuario;
    this.data.anexo4.isShow = datos.codigoModalidad === this.MODALIDAD.CONS ? true : false;

    //Notificaciones
    this.requestNotifica.idSolicitudConcesion = datos.idSolicitudConcesion;
    this.requestNotifica.idSolicitante = datos.idSolicitante;
    this.requestNotifica.correoElectronicoNotif = datos.correoElectronicoNotif;
    this.requestNotifica.idUsuarioModificacion = this.usuario.idusuario;

  }


  selectDPD(origen: string) {
    if (origen == 'd') {
      this.listarPorFilroProvincia(true);
    }
    if (origen == 'p') {
      this.listarPorFilroDistrito(true);
    }
  }

  obtenerInfoAdjuntosCaractBiofisica() {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_6,
      codigoSubSeccion: CodigosPFDM.TAB_6_4
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          for (let i = 0; i < resp.data.length; i++) {
            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == "Fisica") {
              this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_1 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestInfoSolicitado.idArchivoR6_4_1 = resp.data[i].idArchivo;
              this.requestInfoSolicitado.flagArchivoR6_4_1 = true;
            }
            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == "Biologica") {
              this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_2 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestInfoSolicitado.idArchivoR6_4_2 = resp.data[i].idArchivo;
              this.requestInfoSolicitado.flagArchivoR6_4_2 = true;
            }
            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == "Socioeconomica") {
              this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_4_3 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestInfoSolicitado.idArchivoR6_4_3 = resp.data[i].idArchivo;
              this.requestInfoSolicitado.flagArchivoR6_4_3 = true;
            }

          }

        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  obtenerInfoArchivoSustentos() {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_6,
      codigoSubSeccion: CodigosPFDM.TAB_6_6
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          for (let i = 0; i < resp.data.length; i++) {
            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == "Presupuesto") {
              this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_7 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestInfoSolicitado.idArchivoR6_6_7 = resp.data[i].idArchivo;
              this.requestInfoSolicitado.flagArchivoR6_6_7 = true;
            }
            if (resp.data[i].tipoArchivo && resp.data[i].tipoArchivo == "Financiero") {
              this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_8 = resp.data[i].idSolicitudConcesionArchivo;
              this.requestInfoSolicitado.idArchivoR6_6_8 = resp.data[i].idArchivo;
              this.requestInfoSolicitado.flagArchivoR6_6_8 = true;
            }

          }

        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  obtenerInfoAdjuntoExperienciaSolicitante() {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_6,
      codigoSubSeccion: CodigosPFDM.TAB_6_7
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success && resp.data && resp.data.length > 0) {
          this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
          this.experienciaSolicitanteRequest.idArchivo = resp.data[0].idArchivo;
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  obtenerInfoAEvidenciaAdjuntoOtorgamientoConcesion() {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_6,
      codigoSubSeccion: CodigosPFDM.TAB_6_5
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {

        let files: any[] = resp.data.filter((file: any) => file.tipoArchivo === 'PFDMA6_SS5_AMBBIO');

        if(files && files.length > 0){
          this.formEvidenciaFactorAmbiBiologico.idSolicitudConcesionArchivo = files[0].idSolicitudConcesionArchivo;
          this.formEvidenciaFactorAmbiBiologico.idArchivo = files[0].idArchivo;
        }

        files = resp.data.filter((file: any) => file.tipoArchivo === 'PFDMA6_SS5_SECOCU');

        if(files && files.length > 0){
          this.formEvidenciaFactorSocioEconoCultural.idSolicitudConcesionArchivo = files[0].idSolicitudConcesionArchivo;
          this.formEvidenciaFactorSocioEconoCultural.idArchivo = files[0].idArchivo;
        }

        files = resp.data.filter((file: any) => file.tipoArchivo === 'PFDMA6_SS5_MJOCON');

        if(files && files.length > 0){
          this.formEvidenciaManejoConcesion.idSolicitudConcesionArchivo = files[0].idSolicitudConcesionArchivo;
          this.formEvidenciaManejoConcesion.idArchivo = files[0].idArchivo;
        }
        
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

   obtenerInfoAnexoFormatoPropuesta() {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      codigoSeccion: CodigosPFDM.TAB_6,
      codigoSubSeccion: CodigosPFDM.TAB_6_8
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {

        let files: any[] = resp.data.filter((file: any) => file.tipoArchivo === 'PFDMA6_SS8_1');

        if(files && files.length > 0){
          this.formDatoMapaUbicacion.idSolicitudConcesionArchivo = files[0].idSolicitudConcesionArchivo;
          this.formDatoMapaUbicacion.idArchivo = files[0].idArchivo;
        }

        files = resp.data.filter((file: any) => file.tipoArchivo === 'PFDMA6_SS8_2');

        if(files && files.length > 0){
          this.formDatoMapaAreaConcesion.idSolicitudConcesionArchivo = files[0].idSolicitudConcesionArchivo;
          this.formDatoMapaAreaConcesion.idArchivo = files[0].idArchivo;
        }
        
       }
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

  listarPorFiltroDepartamento(ischange: boolean = false) {
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };

    this.planificacionService.listarPorFiltroDepartamento_core_central(params).subscribe((result: any) => {
      if (result.success) {

        this.departamentos = result.data;
        this.requestInfoSolicitante.idDepartamentosolicitante = ischange ? result.data[0].idDepartamento : (this.requestInfoSolicitante.idDepartamentosolicitante || result.data[0].idDepartamento);


        this.listarPorFilroProvincia(ischange);
      }
    }, (error) => this.errorMensaje(error));
  }

  listarPorFilroProvincia(ischange: boolean = false) {
    let idDepartamento = null;
    idDepartamento = this.requestInfoSolicitante.idDepartamentosolicitante;

    this.planificacionService.listarPorFilroProvincia({ idDepartamento: idDepartamento, }).subscribe((result: any) => {
      if (result.success) {

        this.provincias = result.data;
        this.requestInfoSolicitante.idProvinciaSolicitante = ischange ? result.data[0].idProvincia : (this.requestInfoSolicitante.idProvinciaSolicitante || result.data[0].idProvincia);

        this.listarPorFilroDistrito(ischange);
      }
    });
  }

  listarPorFilroDistrito(ischange: boolean = false) {

    let idProvincia = null;
    idProvincia = this.requestInfoSolicitante.idProvinciaSolicitante;

    this.planificacionService.listarPorFilroDistrito({ idProvincia: idProvincia }).subscribe((result: any) => {
      if (result.success) {

        this.distritos = result.data;
        this.requestInfoSolicitante.idDistritoSolicitante = ischange ? result.data[0].idDistrito : (this.requestInfoSolicitante.idDistritoSolicitante || result.data[0].idDistrito);
      }
    });
  }

  btnRegSustentoFinanciero() {
    if (!this.validarRegSustentoFinanciero()) return;

    if (this.idPFDM) {
      this.registrarInfoSustentoFinancieroSolicitante();
    } else {
      this.toast.warn('(*) No existe la solicitud de concesión a la que se desea asociar esta evidencia.');
    }
  }

  validarRegSustentoFinanciero(): boolean {
    let validado = true;
    if (!this.requestInfoSolicitado.idArchivoR6_6_7) {
      validado = false;
      this.toast.warn('(*) Debe adjuntar sustento de presupuesto.');
    }
    if (!this.requestInfoSolicitado.idArchivoR6_6_8) {
      validado = false;
      this.toast.warn('(*) Debe adjuntar sustento de análisis financiero.');
    }

    return validado;

  }

  btnRegExperinciaSolicitante() {
    if (!this.validarRegExperienciaSolicitante()) return;

    if (this.idPFDM) {
      this.registrarInfoEvidenciaExperienciaSolicitante();
    } else {
      this.toast.warn('(*) No existe la solicitud de concesión a la que se desea asociar esta evidencia.');
    }
  }

  btnRegOtorgamientoConcesion() {
    if (!this.validarRegOtorgamientoConcesion()) return;

    let data = {
      idSolicitudConcesionArea: this.idSolicitudConcesionAreaParamIn ? this.idSolicitudConcesionAreaParamIn : 0,
      factorAmbientalBiologico: this.requestArea.factorAmbientalBiologico,
      factorSocioEconocultural: this.requestArea.factorSocioEconocultural,
      manejoConcesion: this.requestArea.manejoConcesion
    };

    if(this.idPFDM) {
      if(this.idSolicitudConcesionAreaParamIn && this.idSolicitudConcesionAreaParamIn  > 0) this.actualizarJustificacionOtorgamientoConcesion(data);
      else this.toast.warn('(*) Debe registrar la información básica del área solicitada correspondiente al Anexo 1 previamente.');
    }else{
      this.toast.warn('(*) No existe la solicitud de concesión a la que se desea asociar los datos ingresados.');
    }
  }

  btnRegAnexoElaboracionPropuesta() {
    if (!this.validarRegAnexoElaboracionPropuesta()) return;

    if (this.idPFDM) {
      this.actualizarInfoAnexoFormatoPropuesta();
    } else {
      this.toast.warn('(*) No existe la solicitud de concesión a la que se desea asociar los anexos ingresados..');
    }
  }

  validarRegExperienciaSolicitante(): boolean {
    let valido = true;
    if (document.getElementsByClassName('anexo6ExperienciaLaboral_required').length > 0) {
      if (!this.experienciaSolicitanteRequest.idArchivo) {
        this.toast.warn('(*) Debe adjuntar la evidencia de experiencia laboral.');
        valido = false;
      }
    }
    return valido;

  }

  validarRegOtorgamientoConcesion(): boolean {
    let valido = true;
    if (document.getElementsByClassName('anexo5JustiOtorgamiento_required').length > 0) {
      this.toast.warn("(*) Hay campos obligatorios por ser llenados en la sección.")
      valido = false;
    }
    return valido;

  }

  validarRegAnexoElaboracionPropuesta(): boolean {
    let valido = true;
    if (document.getElementsByClassName('anexo8FormatoPropuesta_required').length > 0) {

      if(!this.formDatoMapaUbicacion.idArchivo){
        this.toast.warn("(*) Seleccione el adjunto para el mapa de ubicación.")
        valido = false;
        return valido;
      }

      if(!this.formDatoMapaAreaConcesion.idArchivo){
        this.toast.warn("(*) Seleccione el adjunto para el mapa base del área a concesionar.")
        valido = false;
        return valido;
      }
    }
    return valido;

  }

  registrarInfoEvidenciaExperienciaSolicitante() {

    var lstArchivos: any[] = [];

    if (this.experienciaSolicitanteRequest.idArchivo) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo ? this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.experienciaSolicitanteRequest.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_7,
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });

      this.dialog.open(LoadingComponent, { disableClose: true });

      this.solicitudConcesionService.registrarInfoAdjuntosSolicitudConcesion(lstArchivos)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(resp => {
          if (resp.success) {

            this.obtenerInfoAdjuntoExperienciaSolicitante();

            this.toast.ok(resp.message);
            // this.experienciaSolicitanteRequest.idSolicitudConcesionArchivo = resp.data[0].idSolicitudConcesionArchivo;
            //this.experienciaSolicitanteRequest.idArchivo = resp.data[0].idArchivo;
          } else {
            this.toast.warn(resp.message);
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

    }

  }

  registrarInfoSustentoFinancieroSolicitante() {

    var lstArchivoSustento: any[] = [];

    if (this.requestInfoSolicitado.idArchivoR6_6_7) {
      lstArchivoSustento.push({
        idSolicitudConcesionArchivo: this.requestInfoSolicitado.flagArchivoR6_6_7 ? this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_7 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestInfoSolicitado.idArchivoR6_6_7,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_6,
        tipoArchivo: "Presupuesto",
        idUsuarioRegistro: this.usuario.idusuario
      });
    }
    if (this.requestInfoSolicitado.idArchivoR6_6_8) {
      lstArchivoSustento.push({
        idSolicitudConcesionArchivo: this.requestInfoSolicitado.flagArchivoR6_6_8 ? this.requestInfoSolicitado.idSolicitudConcesionArchivoR6_6_8 : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.requestInfoSolicitado.idArchivoR6_6_8,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_6,
        tipoArchivo: "Financiero",
        idUsuarioRegistro: this.usuario.idusuario
      });
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudConcesionService.registrarInfoAdjuntosSolicitudConcesion(lstArchivoSustento)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.obtenerInfoArchivoSustentos();
          this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  actualizarJustificacionOtorgamientoConcesion(request: any) {
    var lstArchivos: any[] = [];
    request.idSolicitudConcesion = this.idPFDM;
    request.idUsuarioModificacion = this.usuario.idusuario;

    if (this.formEvidenciaFactorAmbiBiologico.idArchivo) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formEvidenciaFactorAmbiBiologico.idSolicitudConcesionArchivo ? this.formEvidenciaFactorAmbiBiologico.idSolicitudConcesionArchivo : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formEvidenciaFactorAmbiBiologico.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_5,
        tipoArchivo: 'PFDMA6_SS5_AMBBIO',
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    if (this.formEvidenciaFactorSocioEconoCultural.idArchivo) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formEvidenciaFactorSocioEconoCultural.idSolicitudConcesionArchivo ? this.formEvidenciaFactorSocioEconoCultural.idSolicitudConcesionArchivo : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formEvidenciaFactorSocioEconoCultural.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_5,
        tipoArchivo: 'PFDMA6_SS5_SECOCU',
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    if (this.formEvidenciaManejoConcesion.idArchivo) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formEvidenciaManejoConcesion.idSolicitudConcesionArchivo ? this.formEvidenciaManejoConcesion.idSolicitudConcesionArchivo : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formEvidenciaManejoConcesion.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_5,
        tipoArchivo: 'PFDMA6_SS5_MJOCON',
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    request.lstArchivo = lstArchivos;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.actualizarInfoAreaSolicitudConcesion(request)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {

          this.obtenerInfoAEvidenciaAdjuntoOtorgamientoConcesion();

          this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  actualizarInfoAnexoFormatoPropuesta() {
    var lstArchivos: any[] = [];
    
    if (this.formDatoMapaUbicacion.idArchivo) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formDatoMapaUbicacion.idSolicitudConcesionArchivo ? this.formDatoMapaUbicacion.idSolicitudConcesionArchivo : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formDatoMapaUbicacion.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_8,
        tipoArchivo: 'PFDMA6_SS8_1',
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    if (this.formDatoMapaAreaConcesion.idArchivo) {
      lstArchivos.push({
        idSolicitudConcesionArchivo: this.formDatoMapaAreaConcesion.idSolicitudConcesionArchivo ? this.formDatoMapaAreaConcesion.idSolicitudConcesionArchivo : 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: this.formDatoMapaAreaConcesion.idArchivo,
        codigoSeccion: CodigosPFDM.TAB_6,
        codigoSubSeccion: CodigosPFDM.TAB_6_8,
        tipoArchivo: 'PFDMA6_SS8_2',
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario
      });
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.registrarInfoAdjuntosSolicitudConcesion(lstArchivos)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(resp => {
        if (resp.success) {
          this.obtenerInfoAnexoFormatoPropuesta();
          this.toast.ok(resp.message);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  btnDescargarPlantillaManejo(){

    this.solicitudConcesionService.descargarPlantillaManejo().subscribe((result: any) => {
          if (result.success) {
            let p_file = this.base64ToArrayBuffer(result.archivo);
            var blob = new Blob([p_file], {
              type: result.contenTypeArchivo,
            });
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            var fileName = result.nombeArchivo;
            link.download = fileName;
            link.click();

          } else {

            this.toast.warn('Ocurrió un problema, intente nuevamente');
          }
        }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
}


btnDescargarPlantillaPresupuesto(){

  this.solicitudConcesionService.descargarPlantillaPresupuesto().subscribe((result: any) => {
        if (result.success) {
          let p_file = this.base64ToArrayBuffer(result.archivo);
          var blob = new Blob([p_file], {
            type: result.contenTypeArchivo,
          });
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          var fileName = result.nombeArchivo;
          link.download = fileName;
          link.click();

        } else {

          this.toast.warn('Ocurrió un problema, intente nuevamente');
        }
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
}
base64ToArrayBuffer(base64: any) {
  var binaryString = window.atob(base64);
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
    var ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}
}
