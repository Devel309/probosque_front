import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoConcesion } from 'src/app/model/Concesion/CodigoEstadoConcesion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel2 } from 'src/app/model/util/Combo';
import {
  CCAnexoModel,
  ContratoConcesionModel,
  DataDemoMPAFPP,
} from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { ConvertDateToString } from 'src/app/shared/util';

@Component({
  selector: 'app-anexo7-pfdn',
  templateUrl: './anexo7-pfdn.component.html',
  styleUrls: ['./anexo7-pfdn.component.scss'],
})
export class Anexo7PfdnComponent implements OnInit {
  @Input('data') data: ContratoConcesionModel = new ContratoConcesionModel();
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  @Input() isSolicitudHija: boolean = false;

  isChangeCheck: boolean = false;
  disabledEnviar: boolean = true;
  isEstadoSolObs: boolean = false;
  tipoAnexo: number = 1;

  verModalValidacion: boolean = false;
  anexoModalValidacion: CCAnexoModel = new CCAnexoModel();
  resultadoEvaluacionCombo: ComboModel2[] = DataDemoMPAFPP.EstadosH4Erika;
  listaAnexo: any[] = [];
  data6: any = {};
  usuario!: UsuarioModel;
  modalidadSC!: string;

  idArchivoProTec: number = 0;
  idArchivoProTecBD: number | null = null;
  idArchivoProTecAnexo: number | null = null;
  tipoDocProTec: string = 'TDOCSOLPROTEC';

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private solicitudConcesionService: SolicitudConcesionService,
    private messageService: MessageService,
    private usuarioServ: UsuarioService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;

    let d: any = {
      idSolicitudConcesion: this.idPFDM,
      solicitudConcesionArchivo: {
        codigoSubSeccion: 'A7',
      },
    };
    this.listarAnexo(d);
  }

  request: any = {
    anexo1: {
      conforme: false,
      codigoAnexo: 'PFDMA1',
      codigoEstadoAnexo: 'EANBORR',
      descripcionCodigoEstado: 'En Borrador',
      solicitudConcesionArchivo: {
        tipoDocumento: 'TDOCSOLCONIG',
        tipoArchivo: 'PFDMA1',
      },
    },
    anexo2: {
      conforme: false,
      codigoAnexo: 'PFDMA2',
      codigoEstadoAnexo: 'EANBORR',
      descripcionCodigoEstado: 'En Borrador',
      solicitudConcesionArchivo: {
        tipoDocumento: 'TDOCSOLCONCT',
        tipoArchivo: 'PFDMA2',
      },
    },
    anexo3: {
      conforme: false,
      codigoAnexo: 'PFDMA3',
      codigoEstadoAnexo: 'EANBORR',
      descripcionCodigoEstado: 'En Borrador',
      solicitudConcesionArchivo: {
        tipoDocumento: 'TDOCSOLCONDJ',
        tipoArchivo: 'PFDMA3',
      },
    },
    anexo4: {
      conforme: false,
      codigoAnexo: 'PFDMA4',
      codigoEstadoAnexo: 'EANBORR',
      descripcionCodigoEstado: 'En Borrador',
      solicitudConcesionArchivo: {
        tipoDocumento: 'TDOCSOLCONFT',
        tipoArchivo: 'PFDMA4',
      },
    },
    anexo5: {
      conforme: false,
      codigoAnexo: 'PFDMA5',
      codigoEstadoAnexo: 'EANBORR',
      descripcionCodigoEstado: 'En Borrador',
      solicitudConcesionArchivo: {
        tipoDocumento: 'TDOCSOLCONFP',
        tipoArchivo: 'PFDMA5',
      },
    },
    anexo6: {
      conforme: false,
      codigoAnexo: 'PFDMA6',
      codigoEstadoAnexo: 'EANBORR',
      descripcionCodigoEstado: 'En Borrador',
      solicitudConcesionArchivo: {
        tipoDocumento: 'TDOCSOLCONFE',
        tipoArchivo: 'PFDMA6',
      },
    },
  };

  registrarArchivo(idArchivo: number, condicion: number) {
    switch (condicion) {
      case 1:
        this.request.anexo1.solicitudConcesionArchivo.idArchivo = idArchivo;
        break;
      case 2:
        this.request.anexo2.solicitudConcesionArchivo.idArchivo = idArchivo;
        break;
      case 3:
        this.request.anexo3.solicitudConcesionArchivo.idArchivo = idArchivo;
        break;
      case 4:
        this.request.anexo4.solicitudConcesionArchivo.idArchivo = idArchivo;
        break;
      case 5:
        this.request.anexo5.solicitudConcesionArchivo.idArchivo = idArchivo;
        break;
      case 6:
        this.request.anexo6.solicitudConcesionArchivo.idArchivo = idArchivo;
        break;
      default:
        break;
    }
  }

  eliminarArchivo(ok: boolean, condicion: number) {
    if (ok) this.isChangeCheck = true;
    switch (condicion) {
      case 1:
        if (ok) {
          this.request.anexo1.solicitudConcesionArchivo.idArchivo = 0;
          if (
            this.request.anexo1 &&
            this.request.anexo1?.solicitudConcesionArchivo
              ?.idSolicitudConcesionArchivo
          ) {
            let e: any = {
              idSolicitudConcesionArchivo:
                this.request.anexo1.solicitudConcesionArchivo
                  .idSolicitudConcesionArchivo,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.request.anexo1.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
              undefined;
          }
        }
        break;
      case 2:
        if (ok) {
          this.request.anexo2.solicitudConcesionArchivo.idArchivo = 0;
          if (
            this.request.anexo2 &&
            this.request.anexo2?.solicitudConcesionArchivo
              ?.idSolicitudConcesionArchivo
          ) {
            let e: any = {
              idSolicitudConcesionArchivo:
                this.request.anexo2.solicitudConcesionArchivo
                  .idSolicitudConcesionArchivo,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.request.anexo2.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
              undefined;
          }
        }
        break;
      case 3:
        if (ok) {
          this.request.anexo3.solicitudConcesionArchivo.idArchivo = 0;
          if (
            this.request.anexo3 &&
            this.request.anexo3?.solicitudConcesionArchivo
              ?.idSolicitudConcesionArchivo
          ) {
            let e: any = {
              idSolicitudConcesionArchivo:
                this.request.anexo3.solicitudConcesionArchivo
                  .idSolicitudConcesionArchivo,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.request.anexo3.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
              undefined;
          }
        }
        break;
      case 4:
        if (ok) {
          this.request.anexo4.solicitudConcesionArchivo.idArchivo = 0;
          if (
            this.request.anexo4 &&
            this.request.anexo4?.solicitudConcesionArchivo
              ?.idSolicitudConcesionArchivo
          ) {
            let e: any = {
              idSolicitudConcesionArchivo:
                this.request.anexo4.solicitudConcesionArchivo
                  .idSolicitudConcesionArchivo,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.request.anexo4.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
              undefined;
          }
        }
        break;
      case 5:
        if (ok) {
          this.request.anexo5.solicitudConcesionArchivo.idArchivo = 0;
          if (
            this.request.anexo5 &&
            this.request.anexo5?.solicitudConcesionArchivo
              ?.idSolicitudConcesionArchivo
          ) {
            let e: any = {
              idSolicitudConcesionArchivo:
                this.request.anexo5.solicitudConcesionArchivo
                  .idSolicitudConcesionArchivo,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.request.anexo5.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
              undefined;
          }
        }
        break;
      case 6:
        if (ok) {
          this.request.anexo6.solicitudConcesionArchivo.idArchivo = 0;
          if (
            this.request.anexo6 &&
            this.request.anexo5?.solicitudConcesionArchivo
              ?.idSolicitudConcesionArchivo
          ) {
            let e: any = {
              idSolicitudConcesionArchivo:
                this.request.anexo6.solicitudConcesionArchivo
                  .idSolicitudConcesionArchivo,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.request.anexo6.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
              undefined;
          }
        }
        break;
      case 7:
        if (ok) {
          this.idArchivoProTec = 0;
          if (this.idArchivoProTecBD) {
            let e: any = {
              idSolicitudConcesionArchivo: this.idArchivoProTecBD,
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.eliminarSolicitudConcesionArchivo(e);
            this.idArchivoProTecBD = null;
          }
        }
        break;
      default:
        break;
    }
  }

  private eliminarSolicitudConcesionArchivo(request: any) {
    this.solicitudConcesionService
      .eliminarInformacionAdjuntoSolicitudConcesion(request)
      .subscribe((resp) => {
        if (resp.success) {
          this.SuccessMensaje(resp.message);
          let d: any = {
            idSolicitudConcesion: this.idPFDM,
            solicitudConcesionArchivo: {
              codigoSubSeccion: 'A7',
            },
          };
        } else {
          this.toast.warn(resp.message);
        }
      });
  }

  private registrarSolicitudConcesionAnexo(request: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService
      .registrarSolicitudConcesionAnexo(request)
      .subscribe(
        (resp) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.toast.ok(resp.message);

            let anxCompletado = 0;
            request.forEach(function (item: any) {
              if (item.conforme == true) anxCompletado++;
            });

            

            if (this.modalidadSC != 'SCMODCONS') {
              if (anxCompletado == request.length - 1) {
                this.actualizarSolicitud(true);
              } else {
                this.actualizarSolicitud(false);
                this.disabledEnviar = true;
              }
            } else {
              if (anxCompletado == request.length) {
                this.actualizarSolicitud(true);
              } else {
                this.actualizarSolicitud(false);
                this.disabledEnviar = true;
              }
            }

            let d: any = {
              idSolicitudConcesion: this.idPFDM,
              solicitudConcesionArchivo: {
                codigoSubSeccion: 'A7',
              },
            };
            this.listarAnexo(d);
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error: HttpErrorResponse) => {
          this.toast.warn(error.message);
          this.dialog.closeAll();
        }
      );
  }

  listarAnexo(request: any) {
    this.solicitudConcesionService
      .listarSolicitudConcesionAnexo(request)
      .subscribe((resp) => {
        if (resp.success) {
          //Con datos
          this.listaAnexo = resp.data;

          let data = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA1'
          )[0];
          let data2 = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA2'
          )[0];
          let data3 = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA3'
          )[0];
          let data4 = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA4'
          )[0];
          let data5 = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA5'
          )[0];
          let data6 = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA6'
          )[0];
          let data7 = this.listaAnexo.filter(
            (x) => x.codigoAnexo === 'PFDMA7'
          )[0];

          this.setDataRequest(data, data2, data3, data4, data5, data6, data7);
        } else {
          this.toast.warn(resp.message);
        }
      });
  }

  setDataRequest(
    datos: any,
    datos2: any,
    datos3: any,
    datos4: any,
    datos5: any,
    datos6: any,
    datos7: any
  ) {
    //Set data - anexo1
    if (datos != null && datos != undefined) {
      this.request.anexo1.codigoAnexo = datos.codigoAnexo;
      this.request.anexo1.idSolicitudConcesionAnexo =
        datos.idSolicitudConcesionAnexo;
      this.request.anexo1.idSolicitudConcesion = datos.idSolicitudConcesion;
      this.request.anexo1.descripcionCodigoEstado =
        datos.descripcionCodigoEstado;
      this.request.anexo1.codigoEstadoAnexo = datos.codigoEstadoAnexo;
      this.request.anexo1.conforme = datos.conforme;
      //this.request.anexo1.fechaFirma = datos.fechaFirma;
      this.request.anexo1.fechaFirma = datos.fechaFirma
        ? new Date(datos.fechaFirma)
        : null;
      this.request.anexo1.lugar = datos.lugar;
      if (datos.solicitudConcesionArchivo) {
        this.request.anexo1.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
          datos.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
        this.request.anexo1.solicitudConcesionArchivo.idArchivo =
          datos.solicitudConcesionArchivo?.idArchivo;
        this.request.anexo1.solicitudConcesionArchivo.nombreArchivo =
          datos.solicitudConcesionArchivo?.nombreArchivo;
        this.request.anexo1.solicitudConcesionArchivo.tipoDocumento =
          datos.solicitudConcesionArchivo?.tipoDocumento;
      }
    }
    //Set data2 - anexo2
    if (datos2 != null && datos2 != undefined) {
      this.request.anexo2.codigoAnexo = datos2.codigoAnexo;
      this.request.anexo2.idSolicitudConcesionAnexo =
        datos2.idSolicitudConcesionAnexo;
      this.request.anexo2.idSolicitudConcesion = datos2.idSolicitudConcesion;
      this.request.anexo2.descripcionCodigoEstado =
        datos2.descripcionCodigoEstado;
      this.request.anexo2.codigoEstadoAnexo = datos2.codigoEstadoAnexo;
      this.request.anexo2.conforme = datos2.conforme;
      //this.request.anexo2.fechaFirma = datos2.fechaFirma;
      this.request.anexo2.fechaFirma = datos2.fechaFirma
        ? new Date(datos2.fechaFirma)
        : null;
      this.request.anexo2.lugar = datos2.lugar;
      if (datos2.solicitudConcesionArchivo) {
        this.request.anexo2.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
          datos2.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
        this.request.anexo2.solicitudConcesionArchivo.idArchivo =
          datos2.solicitudConcesionArchivo?.idArchivo;
        this.request.anexo2.solicitudConcesionArchivo.nombreArchivo =
          datos2.solicitudConcesionArchivo?.nombreArchivo;
        this.request.anexo2.solicitudConcesionArchivo.tipoDocumento =
          datos2.solicitudConcesionArchivo?.tipoDocumento;
      }
    }
    //Set data3 - anexo3
    if (datos3 != null && datos3 != undefined) {
      this.request.anexo3.codigoAnexo = datos3.codigoAnexo;
      this.request.anexo3.idSolicitudConcesionAnexo =
        datos3.idSolicitudConcesionAnexo;
      this.request.anexo3.idSolicitudConcesion = datos3.idSolicitudConcesion;
      this.request.anexo3.descripcionCodigoEstado =
        datos3.descripcionCodigoEstado;
      this.request.anexo3.codigoEstadoAnexo = datos3.codigoEstadoAnexo;
      this.request.anexo3.conforme = datos3.conforme;
      //this.request.anexo3.fechaFirma = datos3.fechaFirma;
      this.request.anexo3.fechaFirma = datos3.fechaFirma
        ? new Date(datos3.fechaFirma)
        : null;
      this.request.anexo3.lugar = datos3.lugar;
      if (datos3.solicitudConcesionArchivo) {
        this.request.anexo3.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
          datos3.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
        this.request.anexo3.solicitudConcesionArchivo.idArchivo =
          datos3.solicitudConcesionArchivo?.idArchivo;
        this.request.anexo3.solicitudConcesionArchivo.nombreArchivo =
          datos3.solicitudConcesionArchivo?.nombreArchivo;
        this.request.anexo3.solicitudConcesionArchivo.tipoDocumento =
          datos3.solicitudConcesionArchivo?.tipoDocumento;
      }
    }
    //Set data4 - anexo4
    if (datos4 != null && datos4 != undefined) {
      this.request.anexo4.codigoAnexo = datos4.codigoAnexo;
      this.request.anexo4.idSolicitudConcesionAnexo =
        datos4.idSolicitudConcesionAnexo;
      this.request.anexo4.idSolicitudConcesion = datos4.idSolicitudConcesion;
      this.request.anexo4.descripcionCodigoEstado =
        datos4.descripcionCodigoEstado;
      this.request.anexo4.codigoEstadoAnexo = datos4.codigoEstadoAnexo;
      this.request.anexo4.conforme = datos4.conforme;
      //this.request.anexo4.fechaFirma = datos4.fechaFirma;
      this.request.anexo4.fechaFirma = datos4.fechaFirma
        ? new Date(datos4.fechaFirma)
        : null;
      this.request.anexo4.lugar = datos4.lugar;
      if (datos4.solicitudConcesionArchivo) {
        this.request.anexo4.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
          datos4.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
        this.request.anexo4.solicitudConcesionArchivo.idArchivo =
          datos4.solicitudConcesionArchivo?.idArchivo;
        this.request.anexo4.solicitudConcesionArchivo.nombreArchivo =
          datos4.solicitudConcesionArchivo?.nombreArchivo;
        this.request.anexo4.solicitudConcesionArchivo.tipoDocumento =
          datos4.solicitudConcesionArchivo?.tipoDocumento;
      }
    }
    //Set data5 - anexo5
    if (datos5 != null && datos5 != undefined) {
      this.request.anexo5.codigoAnexo = datos5.codigoAnexo;
      this.request.anexo5.idSolicitudConcesionAnexo =
        datos5.idSolicitudConcesionAnexo;
      this.request.anexo5.idSolicitudConcesion = datos5.idSolicitudConcesion;
      this.request.anexo5.descripcionCodigoEstado =
        datos5.descripcionCodigoEstado;
      this.request.anexo5.codigoEstadoAnexo = datos5.codigoEstadoAnexo;
      this.request.anexo5.conforme = datos5.conforme;
      //this.request.anexo5.fechaFirma = datos5.fechaFirma;
      this.request.anexo5.fechaFirma = datos5.fechaFirma
        ? new Date(datos5.fechaFirma)
        : null;
      this.request.anexo5.lugar = datos5.lugar;
      if (datos5.solicitudConcesionArchivo) {
        this.request.anexo5.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
          datos5.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
        this.request.anexo5.solicitudConcesionArchivo.idArchivo =
          datos5.solicitudConcesionArchivo?.idArchivo;
        this.request.anexo5.solicitudConcesionArchivo.nombreArchivo =
          datos5.solicitudConcesionArchivo?.nombreArchivo;
        this.request.anexo5.solicitudConcesionArchivo.tipoDocumento =
          datos5.solicitudConcesionArchivo?.tipoDocumento;
      }
    }
    //Set data6 - anexo6
    if (datos6 != null && datos6 != undefined) {
      this.request.anexo6.codigoAnexo = datos6.codigoAnexo;
      this.request.anexo6.idSolicitudConcesionAnexo =
        datos6.idSolicitudConcesionAnexo;
      this.request.anexo6.idSolicitudConcesion = datos6.idSolicitudConcesion;
      this.request.anexo6.descripcionCodigoEstado =
        datos6.descripcionCodigoEstado;
      this.request.anexo6.codigoEstadoAnexo = datos6.codigoEstadoAnexo;
      this.request.anexo6.conforme = datos6.conforme;
      //this.request.anexo6.fechaFirma = datos6.fechaFirma;
      this.request.anexo6.fechaFirma = datos6.fechaFirma
        ? new Date(datos6.fechaFirma)
        : null;
      this.request.anexo6.lugar = datos6.lugar;
      if (datos6.solicitudConcesionArchivo) {
        this.request.anexo6.solicitudConcesionArchivo.idSolicitudConcesionArchivo =
          datos6.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
        this.request.anexo6.solicitudConcesionArchivo.idArchivo =
          datos6.solicitudConcesionArchivo?.idArchivo;
        this.request.anexo6.solicitudConcesionArchivo.nombreArchivo =
          datos6.solicitudConcesionArchivo?.nombreArchivo;
        this.request.anexo6.solicitudConcesionArchivo.tipoDocumento =
          datos6.solicitudConcesionArchivo?.tipoDocumento;
      }
    }

    //Set data7 - archivo propuesta técnica
    if (datos7) {
      this.idArchivoProTecAnexo = datos7.idSolicitudConcesionAnexo;
      if (datos7.solicitudConcesionArchivo) {
        this.idArchivoProTec = datos7.solicitudConcesionArchivo?.idArchivo;
        this.idArchivoProTecBD =
          datos7.solicitudConcesionArchivo?.idSolicitudConcesionArchivo;
      }
    }

    this.obtenerSolicitudConcesion();
  }

  //BOTONES
  btnGuardar() {
    this.disabledEnviar=true;
    let datos = [];
    for (const key in this.request) {
      const element = this.request[key];
      element.idSolicitudConcesion = this.idPFDM;
      element.solicitudConcesionArchivo.idSolicitudConcesion = this.idPFDM;
      element.idUsuarioRegistro = this.usuario.idusuario;
      element.idUsuarioModificacion = this.usuario.idusuario;

      if (
        element.solicitudConcesionArchivo !== null &&
        element.solicitudConcesionArchivo !== undefined
      ) {
        element.solicitudConcesionArchivo.idUsuarioRegistro =
          this.usuario.idusuario;
        element.solicitudConcesionArchivo.idUsuarioModificacion =
          this.usuario.idusuario;
        element.solicitudConcesionArchivo.codigoSeccion = 'PFDMA7';
        element.solicitudConcesionArchivo.codigoSubSeccion = 'A7';
      }
      datos.push(element);
    }

    if (this.idArchivoProTec) {
      let aux = {
        codigoAnexo: 'PFDMA7',
        conforme: true,
        idSolicitudConcesion: this.idPFDM,
        idSolicitudConcesionAnexo: this.idArchivoProTecAnexo,
        idSolicitudConcesionArchivo: this.idArchivoProTecBD,
        codigoEstadoAnexo: 'EANCOMP',
        descripcionCodigoEstado: 'Completado',
        idUsuarioRegistro: this.usuario.idusuario,
        idUsuarioModificacion: this.usuario.idusuario,
        solicitudConcesionArchivo: {
          codigoSeccion: 'PFDMA7',
          codigoSubSeccion: 'A7',
          idSolicitudConcesion: this.idPFDM,
          idArchivo: this.idArchivoProTec,
          idSolicitudConcesionArchivo: this.idArchivoProTecBD,
          tipoDocumento: this.tipoDocProTec,
          tipoArchivo: 'PFDMA7',
          idUsuarioRegistro: this.usuario.idusuario,
          idUsuarioModificacion: this.usuario.idusuario,
        },
      };
      datos.push(aux);
    }

    let aux = datos.some(
      (item) =>
        item.conforme === true && !item?.solicitudConcesionArchivo?.idArchivo
    );
    if (aux) {
      this.toast.warn('(*) Debe adjuntar los formatos seleccionados.');
      return;
    }
    this.registrarSolicitudConcesionAnexo(datos);
  }

  btnEnviar(event: any) {
    if (this.modalidadSC == null) {
      this.toast.warn('No se ha registrado una modalidad para la solicitud.');
      return;
    }

    if (!this.request.anexo6.conforme) {
      this.toast.warn('(*) Debe dar conformidad al anexo 6.');
      return;
    }

    if (this.isSolicitudHija && this.idArchivoProTec === 0) {
      this.disabledEnviar = true;
      this.toast.warn('(*) Debe de adjuntar documento de propuesta técnica.');
      return;
    }

    this.confirmationService.confirm({
      target: event.target || undefined,
      message:
        'Esta acción enviará la solicitud a Mesa de Partes y ya no podrá editarla. ¿Desea continuar?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.enviarSolicitud();
      },
    });
  }

  btnEnviarEvaluacion(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message:
        'Esta acción enviará la solicitud a Evaluación. ¿Desea continuar?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.enviarSolicitudEval();
      },
    });
  }

  enviarSolicitud() {
    const params = {
      idSolicitudConcesion: this.idPFDM,
      tipoEnvio: 'ENVIARSC',
      idUsuarioModificacion: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.enviarSolicitudConcesion(params).subscribe(
      (resp) => {
        this.dialog.closeAll();
        if (resp.success) {
          this.toast.ok(resp.message);
          this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
        } else {
          this.toast.warn(resp.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  enviarSolicitudEval() {
    const params = {
      idSolicitudConcesion: this.idPFDM,
      tipoEnvio: 'EVALUADOR',
      idUsuarioModificacion: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.enviarSolicitudConcesion(params).subscribe(
      (resp) => {
        this.dialog.closeAll();
        if (resp.success) {
          this.toast.ok(resp.message);
          this.router.navigate(['/concesion/solicitud-concesion-PFDM']);
        } else {
          this.toast.warn(resp.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  /********************************/
  verAnexo(anexo: number) {
    this.data.tabIndex = anexo;
  }

  descargarSolicitud(d: number, l: String, f: Date) {
    let fecha: String = ConvertDateToString(f);

    let params: any = {
      idSolicitudConcesion: this.idPFDM,
      tipoAnexo: d,
      lugar: l,
      fecha: fecha,
      vertices:
        localStorage.getItem('coordenadasPFDM') != null
          ? JSON.parse('' + localStorage.getItem('coordenadasPFDM')?.toString())
          : [],
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService
      .descargarSolicitudConcesion(params)
      .subscribe(
        (data: any) => {
          this.dialog.closeAll();
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
        },
        (error) => this.errorMensaje(error, true)
      );
    
  }

  clickCheck(data: any, event: any) {
    data.codigoEstadoAnexo = 'EANBORR';
    data.descripcionCodigoEstado = 'En Borrador';
    if (data.conforme) {
      data.codigoEstadoAnexo = 'EANCOMP';
      data.descripcionCodigoEstado = 'Completado';
    }

    this.validarChangeCambios();
  }

  validarChangeCambios() {
    if (this.disabledEnviar === false && this.isChangeCheck === false) {
      let datos = [];
      for (const key in this.request) {
        const element = this.request[key];
        datos.push(element);
      }
      // this.isChangeCheck = !datos.every((item) => item.conforme === true);

      let anxCompletado = 0;
      this.request.forEach(function (item: any) {
        if (item.conforme == true) anxCompletado++;
      });

      if (this.modalidadSC != 'SCMODCONS') {
        if (anxCompletado == this.request.length - 1) {
          this.isChangeCheck = false;
        } else {
          this.isChangeCheck = true;
        }
      } else {
        if (anxCompletado == this.request.length) {
          this.isChangeCheck = false;
        } else {
          this.isChangeCheck = true;
        }
      }

      if (!this.isChangeCheck)
        this.isChangeCheck = datos.some(
          (item) =>
            item.conforme === true &&
            !item?.solicitudConcesionArchivo?.idArchivo
        );
    }
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  obtenerSolicitudConcesion() {
    let param = { idSolicitudConcesion: this.idPFDM };
    this.solicitudConcesionService.listarSolicitudConcesion(param).subscribe(
      (resp) => {
        if (resp.success && resp.data) {
          this.isEstadoSolObs =
            resp.data[0].estadoSolicitud ===
            CodigoEstadoConcesion.OBSERVADO_SOL;
          this.modalidadSC = resp.data[0].codigoModalidad;

          if (resp.data[0].anexoCompletado == true) {
            this.disabledEnviar = false;
          } else {
            this.disabledEnviar = true;
          }

          this.isChangeCheck = false;
          this.validarChangeCambios();
        } else {
          this.toast.warn(resp.message);
        }
      },
      (error) => this.errorMensaje(error)
    );
  }

  actualizarSolicitud(estado: any) {
    let param = {
      idSolicitudConcesion: this.idPFDM,
      anexoCompletado: estado,
      idUsuarioMofificacion: 1,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService
      .actualizarSolicitudConcesion(param)
      .subscribe(
        (resp) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.obtenerSolicitudConcesion();
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  registrarArchivoProTec(id: number) {
    this.idArchivoProTec = id;
    this.isChangeCheck = true;
  }
}
