import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { ContratoConcesionModel, DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CodigosPFDM } from 'src/app/model/util/PFDM/CodigosPFDM';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
@Component({
  selector: 'app-anexo5-pfdn',
  templateUrl: './anexo5-pfdn.component.html',
  styleUrls: ['./anexo5-pfdn.component.scss']
})
export class Anexo5PfdnComponent implements OnInit {
  @Input() data: ContratoConcesionModel = new ContratoConcesionModel();
  @Input() idPFDM!: number;
  @Input() isDisbledFormu: boolean = false;
  @Input() isDisbledEval: boolean = false;
  @Input() isShowEval: boolean = false;
  codProceso = '';
  codigoSubSeccion = '';

  comboUbigeo:ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;
  informacionSolicitante: any = {};
  informacionAreaSolicitada: any = {};

  codigosPFDM = CodigosPFDM;

  constructor(
    private dialog: MatDialog,
    private solicitudConcesionService: SolicitudConcesionService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.obtenerInfoSolicitante();
    this.obtenerInfoAreaSolicitada();
  }


    //SERVICIOS
    obtenerInfoSolicitante() {
      const params = { "idSolicitudConcesion": this.idPFDM };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudConcesionService.obtenerInformacionSolicitanteSolicitado(params).subscribe(resp => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.length > 0) {
          this.informacionSolicitante = resp.data[0];
  
        }
      }, (error) => this.errorMensaje(error, true));
    }

    obtenerInfoAreaSolicitada() {
      const params = { "idSolicitudConcesion": this.idPFDM };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudConcesionService.obtenerInformacionAreaSolicitada(params).subscribe(resp => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.length > 0) {
          this.informacionAreaSolicitada = resp.data[0];
        }
      }, (error) => this.errorMensaje(error, true));
    }

    private errorMensaje(error: any, isLoad: boolean = false) {
      if (isLoad) this.dialog.closeAll();
      let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
      this.toast.error(mensajeError);
    }
}
