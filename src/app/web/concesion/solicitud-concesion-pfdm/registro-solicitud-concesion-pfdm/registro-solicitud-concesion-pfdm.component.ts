import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CCAnexoModel, ContratoConcesionModel } from 'src/app/model/util/dataDemoMPAFPP';
import { varLocalStor } from 'src/app/shared/util-local-storage';

@Component({
  selector: 'app-registro-solicitud-concesion-pfdm',
  templateUrl: './registro-solicitud-concesion-pfdm.component.html',
  styleUrls: ['./registro-solicitud-concesion-pfdm.component.scss']
})
export class RegistroSolicitudConcesionPfdmComponent implements OnInit, OnDestroy {

  data: ContratoConcesionModel = new ContratoConcesionModel();

  isDisbledFormu: boolean = false;
  isDisbledEval: boolean = false;
  isShowEval: boolean = false;
  isSolicitudHija: boolean = false;
  idPFDM: number = 0;
  urlBandeja: string = '/concesion/solicitud-concesion-PFDM';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.idPFDM = this.route.snapshot.paramMap.get('idParam') ? Number(this.route.snapshot.paramMap.get('idParam')) : 0;
    if (!this.idPFDM) this.router.navigate([this.urlBandeja]);

    let auxLocalStorageSCPFDMdata = localStorage.getItem(varLocalStor.BJA_SOL_CONCESION);
    if (auxLocalStorageSCPFDMdata) {
      let auxData = JSON.parse("" + auxLocalStorageSCPFDMdata);
      if (auxData.SCidSolicitud === this.idPFDM) {
        this.isDisbledFormu = auxData.SCdisabledForm || false;
        this.isDisbledEval = auxData.SCdisabledEval || false;
        this.isShowEval = auxData.SCshowEval || false;
        this.isSolicitudHija = auxData.isSolicitudHija || false;
      } else {
        this.router.navigate([this.urlBandeja]);
      }
    } else {
      this.router.navigate([this.urlBandeja]);
    }
  }

  ngOnDestroy(): void {
    localStorage.removeItem(varLocalStor.BJA_SOL_CONCESION);
  }

  ngOnInit(): void {
  }

  changeTab(valor: number) {
    this.data.tabIndex = valor;
  }

  siguiente() {
    if (!this.data.anexo4.isShow && this.data.tabIndex === 2) this.data.tabIndex = this.data.tabIndex + 2;
    else this.data.tabIndex++;
  }

  regresar() {
    if (!this.data.anexo4.isShow && this.data.tabIndex === 4) this.data.tabIndex = this.data.tabIndex - 2;
    else this.data.tabIndex--;
  }

  cancelar() {
    this.router.navigate([this.urlBandeja]);
  }
}
