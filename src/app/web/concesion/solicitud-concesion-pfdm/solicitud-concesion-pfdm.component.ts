import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { CodigoEstadoConcesion } from 'src/app/model/Concesion/CodigoEstadoConcesion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { GenericoService } from 'src/app/service/generico.service';
import { IrPlanService } from 'src/app/shared/services/ir-plan.service';
import { varLocalStor } from 'src/app/shared/util-local-storage';
import { ModalResolucionComponent } from '../../planificacion/bandeja-evaluacion-otorgamiento-tu/modal-resolucion/modal-resolucion.component';
import { ModalContratoComponent } from '../../planificacion/generacion-contrato/modal-contrato/modal-contrato.component';
import { ModalRequisitosBpfComponent } from '../../plantacion/bandeja-plantacion-forestal/components/modal-requisitos-bpf/modal-requisitos-bpf.component';
import { ModalEvidenciaComponent } from './modales/modal-evidencia/modal-evidencia.component';
import { ModalNuevaSolicitudComponent } from './modales/modal-nueva-solicitud/modal-nueva-solicitud.component';
import { ModalOpinionComponent } from './modales/modal-opinion/modal-opinion.component';
import { ModalRequisitosComponent } from './modales/modal-requisitos/modal-requisitos.component';

@Component({
  selector: 'app-solicitud-concesion-pfdm',
  templateUrl: './solicitud-concesion-pfdm.component.html',
  styleUrls: ['./solicitud-concesion-pfdm.component.scss']
})
export class SolicitudConcesionPFDMComponent implements OnInit {
  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  usuario!: UsuarioModel;
  requestFiltro: any = {};
  listaBandeja = [];
  comboTipoMod = [];
  comboEstado = [];
  optionPage: any = { pageNum: 1, pageSize: 10, totalRecords: 0 };

  isPerTitularTH: boolean = false;
  isPerARFFS: boolean = false;
  isPerMDP: boolean = false;
  isperOsiSerComp: boolean = false;

  codEstado = CodigoEstadoConcesion;

  constructor(
    private router: Router,
    private toast: ToastService,
    public dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private genericoService: GenericoService,
    private solicitudConcesionService: SolicitudConcesionService,
    private irPlanService: IrPlanService,
    private activaRoute: ActivatedRoute,
  ) {
    this.usuario = this.usuarioServ.usuario;

    this.isPerTitularTH = this.usuario.sirperfil === Perfiles.TITULARTH;
    this.isPerARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    this.isPerMDP = this.usuario.sirperfil === Perfiles.MESA_DE_PARTES;
    this.isperOsiSerComp = (this.usuario.sirperfil === Perfiles.SERFOR || this.usuario.sirperfil === Perfiles.OSINFOR || this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO);

    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    localStorage.removeItem("coordenadasPFDM");
    this.listar();
    this.listarComboModalidad();
    this.listarComboEstados();
  }

  // SERVICIOS
  listarComboModalidad() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "SCMOD" }).subscribe((resp: any) => {
      if (resp.success) this.comboTipoMod = resp.data;
    });
  }

  listarComboEstados() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "SCEST" }).subscribe((resp: any) => {
      if (resp.success) this.comboEstado = resp.data;
    });
  }

  listar() {
    this.listaBandeja = [];
    const params = {
      codigoPerfil: this.usuario.sirperfil
      , idSolicitante: this.usuario.idusuario
      , codigoModalidad: this.requestFiltro.codigoModalidad
      , nombreSolicitanteUnico: this.requestFiltro.solicitante
      , numeroDocumentoSolicitanteUnico: this.requestFiltro.nroDocumento
      , estadoSolicitud: this.requestFiltro.estadoSolicitud
      , idConcesion: this.requestFiltro.idConcesion
      , pageNum: this.optionPage.pageNum
      , pageSize: this.optionPage.pageSize
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.listarSolicitudConcesionPorFiltro(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaBandeja = resp.data.map((item: any) => {
          return { ...item, auxColor: this.getColorEstado(item.estadoSolicitud) }
        });
        this.optionPage.totalRecords = resp.totalRecord || 0;
        if (this.listaBandeja?.length === 0) this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  getColorEstado(estadoIn: string): string {
    let strColor = "";
    switch (estadoIn) {
      case CodigoEstadoConcesion.BORRADOR: strColor = ""; break;
      case CodigoEstadoConcesion.PRESENTADA: strColor = "estAzul"; break;
      case CodigoEstadoConcesion.OBSERVADO_REQ: strColor = "estNaranja"; break;
      case CodigoEstadoConcesion.NO_ADMITIDO: strColor = "estMarron"; break;
      case CodigoEstadoConcesion.EVALUACION: strColor = "estAmarilo"; break;
      case CodigoEstadoConcesion.OBSERVADO_SOL: strColor = "estRojo"; break;
      default: break;
    }
    return strColor;
  }

  eliminarSolicitudConcecion(id: number) {
    const params = { "idSolicitudConcesion": id };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.eliminarSolicitudConcesion(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.limpiarPaginador();
        this.listar();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  clonarSolicitudConcecion(id: number) {

    const params = {
      idSolicitudConcesion: id,
      tipoClonacion: 1,
      idUsuarioRegistro: this.usuario.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.clonarSolicitudConcesion(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.limpiarPaginador();
        this.listar();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  // BOTONES
  btnBuscar() {
    this.limpiarPaginador();
    this.listar();
  }

  btnLimpiar() {
    this.limpiarFiltros();
    this.limpiarPaginador();
    this.listar();
  }

  btnNuevo() {
    const refResp = this.dialogService.open(ModalNuevaSolicitudComponent, {
      header: 'Nueva Solicitud',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        idUser: this.usuario.idusuario,
        perfil: this.usuario.sirperfil
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if(resp) {
        this.irPlanService.irFormSolicitudConcesion_LS(false, '/concesion/solicitud-contrato-concesion', resp?.id, false, true, false, resp?.isAsoc);
      }
    });
  }

  btnVer(id: number, disabledForm: boolean, disabledEval: boolean = true, showEval: boolean = false, idPadre: number) {
    let isSolicitudHija: boolean = !!idPadre;
    this.irPlanService.irFormSolicitudConcesion_LS(false, '/concesion/solicitud-contrato-concesion', id, disabledForm, disabledEval, showEval, isSolicitudHija);
  }

  btnEliminar(event: any, id: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar la solicitud?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarSolicitudConcecion(id);
      },
    });
  }

  btnClonar(event: any, id: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Desea reutilizar los datos de esta solicitud?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.clonarSolicitudConcecion(id);
      },
    });
  }

  btnEvalRequisitos(id: any, disabled: boolean) {
    const refResp = this.dialogService.open(ModalRequisitosComponent, {
      header: 'Documentos Presentados',
      width: '60%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: disabled,
        idSolConcesion: id,
        idUser: this.usuario.idusuario,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listar();
    });
  }

  btnOpinion(id: any, disabled: boolean) {
    const refResp = this.dialogService.open(ModalOpinionComponent, {
      header: 'Ver Solicitudes de Opinión',
      width: '70%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: disabled,
        idSolConcesion: id,
        idUser: this.usuario.idusuario,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listar();
    });
  }

  btnToAnexo6(data : any, isdisabledForm: boolean) {
    this.irPlanService.irFormSolicitudConcesion_LS(false, '/concesion/formato-propuesta', data.idConcesion, isdisabledForm, true, false, false);
  }

  btnEvidencia(item: any) {

    const refResp = this.dialogService.open(ModalEvidenciaComponent, {
      header: 'Acreditar Evidencia Publicación',
      width: '550px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        perfil: this.usuario.sirperfil
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listar();
    });

  }

  btnVerContrato(fila: any) {
    let aux = {
      idContrato: fila.idContrato,
      documentoGestion: fila.idConcesion,
      tipoDocumentoGestionDescripcion: "PFDM por Concesión Directa",
      tipoDocumentoGestion: "TPMPFDM",
      codigoEstadoContrato: "",
      idUsuarioPostulacion: null,
    }
    this.dialogService.open(ModalContratoComponent, {
      header: 'Documentos para Generación de Contrato de Concesión',
      width: '650px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: { item: aux, isNew: false, },
    });
  }

  btnVerResolucion(fila: any) {

    const refResp = this.dialogService.open(ModalResolucionComponent, {
      header: 'Ver Resolución',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        descargaFormato : false,
        isView: true,
      },
    });

    refResp.onClose.subscribe((resp: any) => {
      if (resp) this.listar();
    });
  }

  btnIrOposicion() {
    this.router.navigate(["planificacion/consulta-oposicion"]);
  }

  // FUNCIONES
  loadData(event: any) {
    this.optionPage = { pageNum: event.first + 1, pageSize: event.rows, totalRecords: 0 };
    this.listar();
  }

  limpiarPaginador() {
    this.optionPage = { pageNum: 1, pageSize: 10, totalRecords: 0 };
  }

  limpiarFiltros() {
    this.requestFiltro = { idConcesion: null, codigoModalidad: null, solicitante: null, estadoSolicitud: null, nroDocumento: null };
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
