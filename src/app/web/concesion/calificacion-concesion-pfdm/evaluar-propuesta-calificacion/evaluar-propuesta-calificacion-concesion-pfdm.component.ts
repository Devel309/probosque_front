import { HttpErrorResponse } from '@angular/common/http';
import { DEBUG } from '@angular/compiler-cli/src/ngtsc/logging/src/console_logger';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CCAnexoModel, ContratoConcesionModel } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';

@Component({
  selector: 'app-evaluar-propuesta-calificacion-concesion-pfdm',
  templateUrl: './evaluar-propuesta-calificacion-concesion-pfdm.component.html',
  styleUrls: ['./evaluar-propuesta-calificacion-concesion-pfdm.component.scss']
})
export class EvaluarPropuestaCalificacionConcesionPfdmComponent implements OnInit, OnDestroy {
  listaPropuesta: any[] = [];
  listaEmpate: any[] = []; 
  /*listaPropuesta: any = [{ id: 12, tipoModalidad: 'Ecoturismo', solicitante: 'GESTIÓN DE SERVICIOS COMPARTIDOS S.A.C (Primer Postulante)' ,docSolicitante: 202501827623, estadoSolicitud: 'En Publicación', puntajeObtenido: '95 pts', ordenPuntaje: 'Ganador'},
                          { id: 11, tipoModalidad: 'Ecoturismo', solicitante:'MADERERA SAN JUAN S.A.C', docSolicitante: 20501234624, estadoSolicitud: 'Calificación Pendiente', puntajeObtenido: '85 pts', ordenPuntaje: 'Segundo'},
                          { id: 10, tipoModalidad: 'Ecoturismo', solicitante:'MADERERA PALOS ALTOS S.A.C', docSolicitante: 20768827625, estadoSolicitud: 'Calificación Pendiente', puntajeObtenido: '94 pts', ordenPuntaje: 'Tercero'},
                        ];
                      */
  idPFDM: number = 0;
  primerPosutlante: any;
  isDisabledCalificacion: boolean = false;
  mostrarListaEmpate: boolean = false;
  hayEmpate: boolean = false;
  isCalificacionFinalizada = false;
  calificacionFinalizada: any;
  idConcesionEvaluacionParam : number = 0;
  puntajeFinalDesempate: number = 0;
  usuario!: UsuarioModel;
  idArchivoInformeFinal: number = 0;
  mostrarCargaInforme: boolean = false;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private usuarioServ: UsuarioService,
    private solicitudConcesionService: SolicitudConcesionService,
    
  ) {
    this.idPFDM = this.route.snapshot.paramMap.get('idParam') ? Number(this.route.snapshot.paramMap.get('idParam')) : 0;
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnDestroy(): void {
    localStorage.removeItem("SCdisabledForm");
    localStorage.removeItem("SCidSolicitud");
    localStorage.removeItem("SCdisabledEval");
    localStorage.removeItem("SCshowEval");
  }

  btnEvalPropuesta(id: any, page: any) {
    this.primerPosutlante = '';
    this.calificacionFinalizada = '';
    let SPadre: any=this.idPFDM;
    localStorage.setItem("SCEvalPropid", id);
    localStorage.setItem("SPadre", SPadre);
    localStorage.setItem("page", page);
    if(this.isCalificacionFinalizada){
      this.calificacionFinalizada = 'finalizada';
      localStorage.setItem("calificacionFinalizada", this.calificacionFinalizada);
    }
    if(id === this.idPFDM){
      this.primerPosutlante = 'primero';
    }
    
    localStorage.setItem("SCEvalPrimerPostulante", this.primerPosutlante);
    this.router.navigate(['/concesion/evaluacion-propuesta/', id]);
    
  }
  ngOnInit(): void {

    this.obtenerSolicitudConcesionCalificacionListarPorFiltro();
  }


  validarEmpate(){


    
    let auxLista : any[]=[];

    auxLista = [...this.listaPropuesta];


   
   
    for(let j=0; j<auxLista.length; j++){
      if(this.listaPropuesta[0].idConcesion != auxLista[j].idConcesion){
        if((this.listaPropuesta[0].puntajeObtenido == auxLista[j].puntajeObtenido) && (this.listaPropuesta[0].puntajeObtenido !=0) && (this.listaPropuesta[0].puntajeObtenido !=null) ){
          this.hayEmpate=true

          
            if(this.listaEmpate.length>0){
              let existeId : boolean=false;
              

              this.listaEmpate.forEach(x => {
                
                if(x.idConcesion==auxLista[j].idConcesion){
                // temp.push(x);
                  existeId = true;
                }
              
              });
              if(!existeId){
                this.listaEmpate.push(auxLista[j]);
              }
              
            }else{
              this.listaEmpate.push(this.listaPropuesta[0])
              this.listaEmpate.push(auxLista[j]);
            }

        }
      }

    }
    if(this.isCalificacionFinalizada){
      this.btnEvaluarEmpate()
    }

  }




  btnEvaluarEmpate(){
    
    let idDesempate : any[];
    let auxListaEmpate : any[]=[];
    auxListaEmpate = [...this.listaEmpate];
    let listaEmpateAC: any[]=[];
    let mayorAC: any[]= [];
    let empateAC: boolean = false;
    let listaEmpateCF: any[]=[];
    let mayorCF: any[]= [];
    let empateCF: boolean = false;
    let puntajeFinal = {puntajeFinalObtenido:0}
  
    auxListaEmpate.forEach(x => {
      if(this.listaEmpate[0].idConcesion != x.idConcesion){

        if(this.listaEmpate[0].actividadComplementaria == x.actividadComplementaria){
            
          if(listaEmpateAC.length>0){
            let existeId : boolean=false;
            
            listaEmpateAC.forEach(y => {
              
              if(y.idConcesion==x.idConcesion){
              // temp.push(x);
                existeId = true;
              }
            
            });

            if(!existeId){
              listaEmpateAC.push(x);
            }
            
          }else{
            listaEmpateAC.push(this.listaEmpate[0])
            listaEmpateAC.push(x);
          }

        }
        /***/
        
        if(this.listaEmpate[0].capacidadFinanciera == x.capacidadFinanciera){
            
          if(listaEmpateCF.length>0){
            let existeId : boolean=false;
            
            listaEmpateCF.forEach(y => {
              
              if(y.idConcesion==x.idConcesion){
              // temp.push(x);
                existeId = true;
              }
            
            });

            if(!existeId){
              listaEmpateCF.push(x);
            }
            
          }else{
            listaEmpateCF.push(this.listaEmpate[0])
            listaEmpateCF.push(x);
          }

        }

      }
      

    });


    auxListaEmpate = [...this.listaEmpate];

    auxListaEmpate.forEach(lista =>{

      if(lista.idConcesion != this.listaEmpate[0].idConcesion){
         
        if(lista.actividadComplementaria > this.listaEmpate[0].actividadComplementaria){
          mayorAC.shift();
          mayorAC.push(lista);


        }else{
          mayorAC.shift();
          mayorAC.push(this.listaEmpate[0]);
        }

        if(lista.capacidadFinanciera > this.listaEmpate[0].capacidadFinanciera){
          mayorCF.shift();
          mayorCF.push(lista);


        }else{
          mayorCF.shift();
          mayorCF.push(this.listaEmpate[0]);
        }

      }

    })


    
    if(listaEmpateAC.length>0){
      if(listaEmpateAC[0].actividadComplementaria !=0){
        if(listaEmpateAC[0].actividadComplementaria >= mayorAC[0].actividadComplementaria  ){
            empateAC= true;
        }
      }
    }

    if(listaEmpateCF.length>0){
      if(listaEmpateCF[0].capacidadFinanciera !=0){
        if(listaEmpateCF[0].capacidadFinanciera >= mayorCF[0].capacidadFinanciera ){
            empateCF= true;
        }
      }
    }

    
    if(empateAC){
      this.listaEmpate.forEach( fila => {
        for(let i =0; i<listaEmpateAC.length;i++){
          if(fila.idConcesion == listaEmpateAC[i].idConcesion){
            puntajeFinal.puntajeFinalObtenido =fila.puntajeObtenido +1;

            fila = Object.assign(fila,puntajeFinal);
            puntajeFinal.puntajeFinalObtenido =fila.puntajeObtenido;
          }
        }
        
       // console.log(fila);

      });

      /*********************************** */
      if(empateCF){
        this.listaEmpate.forEach( fila => {
          for(let i =0; i<listaEmpateCF.length;i++){
            if(fila.idConcesion == listaEmpateCF[i].idConcesion){
              fila.puntajeFinalObtenido =fila.puntajeFinalObtenido +1;
            }
          }
          
         // console.log(fila);
  
        });
  
      }else {
  
        this.listaEmpate.forEach( fila => {
          if(fila.idConcesion == mayorCF[0].idConcesion){
  
            fila.puntajeFinalObtenido =fila.puntajeFinalObtenido +1;
          }
          
        });
      }

      /*********************************** */


    }else {

      if(mayorAC[0].actividadComplementaria > 0){
        
        this.listaEmpate.forEach( fila => {

          puntajeFinal.puntajeFinalObtenido = fila.puntajeObtenido;

          if(fila.idConcesion == mayorAC[0].idConcesion){

            puntajeFinal.puntajeFinalObtenido =fila.puntajeObtenido +1;

          }

          fila = Object.assign(fila,puntajeFinal);
          puntajeFinal.puntajeFinalObtenido = fila.puntajeObtenido;
          //console.log(fila);

        });
      }else {
        this.listaEmpate.forEach( fila => {
          puntajeFinal.puntajeFinalObtenido = fila.puntajeObtenido;
          fila = Object.assign(fila,puntajeFinal);
        })
    
        
        if(empateCF){
          this.listaEmpate.forEach( fila => {
            for(let i =0; i<listaEmpateCF.length;i++){
              if(fila.idConcesion == listaEmpateCF[i].idConcesion){
                if(fila.capacidadFinanciera > 0){
                  fila.puntajeFinalObtenido =fila.puntajeFinalObtenido +1;
                }
              }
            }
            
           // console.log(fila);
    
          });
    
        }else {
    
          this.listaEmpate.forEach( fila => {
            if(mayorCF[0].capacidadFinanciera >0){
              if(fila.idConcesion == mayorCF[0].idConcesion){
      
                fila.puntajeFinalObtenido =fila.puntajeFinalObtenido +1;
              }
            }
          });
        }

      }
    }





    
     
     this.mostrarListaEmpate = true;
     if(this.hayEmpate && this.isCalificacionFinalizada){
      this.mostrarListaEmpate = true;
     }

  }
  
  obtenerSolicitudConcesionCalificacionListarPorFiltro() {
    
    const request = {
      idSolicitudConcesion: this.idPFDM,
      idSolicitudConcesionPadre: this.idPFDM
    };
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.obtenerSolicitudConcesionCalificacionPorFiltro(request).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        if (resp.data && resp.data.length > 0) {
            this.listaPropuesta = resp.data;
            

            this.listaPropuesta.forEach(element => {
              if(element.puntajeObtenido == 0){
                this.isDisabledCalificacion = true;
              }
              
              if(element.idConcesion == this.idPFDM){
                this.idConcesionEvaluacionParam = element.idSolicitudConcesionEvaluacion;
                if(element.calificacionFinalizada){
                  this.isCalificacionFinalizada = true;
                }

                this.idArchivoInformeFinal=element.idArchivoInformeFinal;
                if(this.idArchivoInformeFinal>0){
                  this.mostrarCargaInforme=true;
                }
              }
            });
            this.validarEmpate();

        }
      } else {
        this.toast.warn(resp.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  btnFinalizarCalificacion(){
    let contadorMensaje = 0;
    /*   
    concesionEvaluacion = {
      idSolicitudConcesionEvaluacion: this.idSolicitudConcesionEvaluacion,
      puntajeTotal: this.puntajeTotal,
      idUsuarioRegistro: this.usuario.idusuario
    }*/


    let request = {
      idSolicitudConcesionEvaluacion: 0,
      puntajeTotal: 0,
      idUsuarioRegistro: this.usuario.idusuario,
      calificacionFinalizada: true
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    if(this.hayEmpate){
      //debugger
      this.listaPropuesta.forEach(fPropuesta=>{

        request.puntajeTotal = fPropuesta.puntajeObtenido;
        request.idSolicitudConcesionEvaluacion = fPropuesta.idSolicitudConcesionEvaluacion

        this.listaEmpate.forEach(fEmpate=>{
            if(fPropuesta.idConcesion == fEmpate.idConcesion ){
              request.puntajeTotal = fEmpate.puntajeFinalObtenido
            }


            // if(fPropuesta.idSolicitudConcesionPadre == null){
            //   request.calificacionFinalizada = true;
            // }

        });

        
        this.solicitudConcesionService.finalizarCalificacion(request).subscribe((result: any) => {
          contadorMensaje++;
          if (result?.success && result.validateBusiness !== false) {
            if(contadorMensaje<2)
            this.toast.ok(result?.message);
          } else {
            if(contadorMensaje<2)
            this.toast.warn(result?.message);
          }
        }, (error) => this.errorMensaje(error, true));

      });
    
    }else{
      this.listaPropuesta.forEach(fPropuesta=>{

        request.puntajeTotal = fPropuesta.puntajeObtenido;
        request.idSolicitudConcesionEvaluacion = fPropuesta.idSolicitudConcesionEvaluacion

        if(fPropuesta.idSolicitudConcesionPadre == null){
          request.calificacionFinalizada = true;
        }
        
        this.solicitudConcesionService.finalizarCalificacion(request).subscribe((result: any) => {
          contadorMensaje++;
          if (result?.success && result.validateBusiness !== false) {
            
            if(contadorMensaje<2)
            this.toast.ok(result?.message);
          } else {
            if(contadorMensaje<2)
            this.toast.warn(result?.message);
          }
        }, (error) => this.errorMensaje(error, true));


      });

    }
    this.dialog.closeAll();
    this.mostrarCargaInforme=true;
    this.obtenerSolicitudConcesionCalificacionListarPorFiltro();
    /*
    const request = {
      idSolicitudConcesionEvaluacion: this.idConcesionEvaluacionParam,
      calificacionFinalizada: 1
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.finalizarCalificacion(request).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));*/

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  obtenerInfoAdjunto() {
    let param = { 
      idSolicitudConcesion : this.idPFDM,
      codigoSeccion: "TABPRO",
      codigoSubSeccion: "SUBPRO"
    };
  
    this.dialog.open(LoadingComponent, { disableClose: true });
     this.solicitudConcesionService.obtenerInformacionAdjuntosSolicitudConcesion(param)
     .pipe(finalize(() => this.dialog.closeAll()))
     .subscribe(resp => {
       if (resp.success && resp.data && resp.data.length > 0) {

        this.idArchivoInformeFinal = resp.data[0].idArchivo;
        
       } 
     }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
   }

  btnDescargarPlant() { 
    
    let params: any = {};
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.solicitudConcesionService.descargarPlantillaCriteriosCalificacionPropuesta(params).subscribe((data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
      
    

  }

  registrarArchivo(id: number) {
    this.idArchivoInformeFinal = id;
  }
  eliminarArchivo(ok: boolean) {
    if(ok){
      if(this.idArchivoInformeFinal) this.eliminarInfoAdjuntos();
    }
  }

  eliminarInfoAdjuntos() {

    this.dialog.open(LoadingComponent, { disableClose: true });

      this.listaPropuesta.forEach(fPropuesta=>{


        let request = {
          idSolicitudConcesionEvaluacion: fPropuesta.idSolicitudConcesionEvaluacion,
          idArchivoInformeFinal: 0,
          idUsuarioRegistro: this.usuario.idusuario

        }

        this.solicitudConcesionService.actualizarSolicitudConcesionEvaluacion(request).subscribe((result: any) => {
        
          if (result?.success && result.validateBusiness !== false) {
            
          } else {
            this.toast.warn(result?.message);
          }
        }, (error) => this.errorMensaje(error, true));


      });

    this.dialog.closeAll();

    //this.toast.ok("Se eliminó informe final");
    
    this.obtenerSolicitudConcesionCalificacionListarPorFiltro();
  }

  btnGuardarInforme(){



    this.dialog.open(LoadingComponent, { disableClose: true });

      this.listaPropuesta.forEach(fPropuesta=>{


        let request = {
          idSolicitudConcesionEvaluacion: fPropuesta.idSolicitudConcesionEvaluacion,
          idArchivoInformeFinal: this.idArchivoInformeFinal,
          idUsuarioRegistro: this.usuario.idusuario

        }

        this.solicitudConcesionService.actualizarSolicitudConcesionEvaluacion(request).subscribe((result: any) => {
        
          if (result?.success && result.validateBusiness !== false) {
            
          } else {
            this.toast.warn(result?.message);
          }
        }, (error) => this.errorMensaje(error, true));


      });

    this.dialog.closeAll();

    this.toast.ok("Se guardó informe final");
    
    this.obtenerSolicitudConcesionCalificacionListarPorFiltro();


  }
}
