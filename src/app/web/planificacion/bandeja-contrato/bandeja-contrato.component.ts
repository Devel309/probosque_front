import { Component, OnInit } from '@angular/core';
import { ConvertDateToString, ConvertNumberToDate } from '@shared';
import { MessageService, SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ContratoService } from 'src/app/service/contrato.service';
import { ModalExpedienteComponent } from './modal-expediente/modal-expediente.component';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';

@Component({
  selector: 'app-bandeja-contrato',
  templateUrl: './bandeja-contrato.component.html',
})
export class BandejaContratoComponent implements OnInit {
  lstContrato: any[] = [];
  ref!: DynamicDialogRef;
  procesoPostulacionSelect!:String;
  estadoContrato: SelectItem[];
  selectedestadocontrato!: SelectItem;

  constructor(
    public dialogService: DialogService,
    private messageService: MessageService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private contratoService: ContratoService
  ) {
    this.estadoContrato = [
      { label:'-- Seleccione --', value:null},
      { label: 'Firmado', value: true },
      { label: 'Pendiente de firma', value: false },
    ];
  }

  ngOnInit() {
    this.listarContrato();
    this.listarProcesoPostulacion();
  }

  procesoPostulacion: any[] = [];
  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: null,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          idProcesoPostulacion: "-- Seleccione --"
        });
        this.procesoPostulacion = resp.data;
      });
  };


  listarContrato = () => {
    var params = {
      codigoContrato: null,
      firmado: true,
      idContrato: null,
      idProcesoPostulacion: this.procesoPostulacionSelect == null || this.procesoPostulacionSelect == '-- Seleccione --' ? null : this.procesoPostulacionSelect,
    };
    this.contratoService
      .listarContrato(params)
      .subscribe((resp: any) => {
        resp.data.forEach((element: any) => {
          if (element.fechaInicioContrato != null) {
            element.fechaInicioContrato = ConvertDateToString(
              ConvertNumberToDate(element.fechaInicioContrato)
            );
          }
          if (element.fechaFinContrato != null) {
            element.fechaFinContrato = ConvertDateToString(
              ConvertNumberToDate(element.fechaFinContrato)
            );
          }
        });
        this.lstContrato = resp.data;
      });
  };

  verExpediente = (item: any, type: number) => {
    this.ref = this.dialogService.open(ModalExpedienteComponent, {
      header: 'Envío de Expediente',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        typeModal: type,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp.success) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: resp.mensaje,
        });
        this.listarContrato();
      }
    });
  };

}
