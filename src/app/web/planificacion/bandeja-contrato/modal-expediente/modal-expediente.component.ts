import { Component, OnInit } from '@angular/core';
import {
  ConvertDateStringFormat,
  ConvertDateToString,
  ConvertNumberToDate,
  DownloadFile,
} from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { ContratoService } from 'src/app/service/contrato.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';

@Component({
  selector: 'app-modal-expediente',
  templateUrl: './modal-expediente.component.html',
})
export class ModalExpedienteComponent implements OnInit {
  listadoAdjuntos: any[] = [];
  lstContrato: any = {};

  constructor(
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    public ref: DynamicDialogRef,
    private apiGeoforestalService: ApiGeoforestalService,
    private serv: PlanificacionService,
    private contratoService: ContratoService,
    private serviceExternos: ApiForestalService
  ) {}

  ngOnInit() {
    if (this.config.data.typeModal === 2) {
      this.obtenerArchivo();
      this.listarContrato();
    }
  }

  enviarCatastro = () => {
    let objectIdUa = this.config.data.item.objectIdUa.split(',');

    objectIdUa.forEach((element: any) => {
      let params = {
        atributos: {
          ESTUA: 2,
          FECEUA: ConvertDateStringFormat(new Date()),
          OBJECTID: Number(element),
        },
      };

      this.serviceExternos
        .actualizarUnidadAprovechamiento(params)
        .subscribe((result: any) => {
          if (result.dataService.isSuccess) {
            let body = {
              mensaje: result.dataService.message,
              success: true,
            };
            this.ref.close(body);
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: result.dataService.message,
            });
          }
        });
    });
  };

  enviarSerfor = () => {
    let body = {
      mensaje: 'Se guardó correctamente',
      success: true,
    };
    this.ref.close(body);
  };

  obtenerArchivo = () => {
    let params = {
      codigoAnexo: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
      idTipoDocumento: null,
      idUsuarioAdjunta: this.config.data.item.idUsuarioPostulacion,
    };
    this.serv.obtenerAdjuntos(params).subscribe((result: any) => {
      if (result.data.length > 0) {
        this.listadoAdjuntos = result.data;
      }
    });
  };

  listarContrato = () => {
    let params = {
      codigoContrato: null,
      firmado: null,
      idContrato: null,
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
    };
    this.contratoService
      .listarContrato(params)
      .subscribe((resp: any) => {
        resp.data.forEach((element: any) => {
          if (element.fechaInicioContrato != null) {
            element.fechaInicioContrato = ConvertDateToString(
              ConvertNumberToDate(element.fechaInicioContrato)
            );
          }
          if (element.fechaFinContrato != null) {
            element.fechaFinContrato = ConvertDateToString(
              ConvertNumberToDate(element.fechaFinContrato)
            );
          }
        });
        // ;
        this.lstContrato = resp.data[0];
      });
  };

  descargarArchivo = (item: any) => {
    DownloadFile(item.file, item.nombreDocumento, 'application/octet-stream');
  };
}
