import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { SolicitudAccesoService } from 'src/app/service/solicitudAcceso.service';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from '../../../components/loading/loading.component';

@Component({
  selector: 'bandeja-solicitud-acceso',
  templateUrl: './bandeja-solicitud-acceso.component.html',
  styleUrls: ['./bandeja-solicitud-acceso.component.scss'],
})
export class BandejaSolicitudAcceso implements OnInit {
  constructor(
    private serv: ParametroValorService,
    private servSA: SolicitudAccesoService,
    private dialog: MatDialog,
    private router: Router
  ) {}
  solicitudAcceso = {} as SolicitudAccesoModel;

  //Parametro
  lstEstado: any[] = [];
  lstTipoPersona: any[] = [];
  lstTipoActor: any[] = [];
  lstTipoCNCC: any[] = [];
  //bandeja
  lstSolicituAcceso: any[] = [];
  totalRegistrosSA: number = 0;

  revisarSolicitud(data: any): void {
    this.router.navigateByUrl('/planificacion/revision-solicitud-acceso', {
      state: { data: data },
    });
  }

  //Init
  ngOnInit(): void {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.listarEstado();
    this.listarTipoPersona();
    this.listarTipoActor();
    this.listarTipoCNCC();
    //Bandeja Solicitud
    this.listarSolicitudAcceso();
    setTimeout(() => {
      this.dialog.closeAll();
    }, 250);
  }

  /******************************************************/
  /*Métodos**********************************************/
  /******************************************************/
  listarEstado() {
    var params = { prefijo: 'ESAC' };
    //acá debe obtener de tabla estados
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstEstado = result.data;
    });
  }
  listarTipoPersona() {
    var params = { prefijo: 'TPER' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoPersona = result.data;
    });
  }
  listarTipoActor() {
    var params = { prefijo: 'TACT' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoActor = result.data;
    });
  }
  listarTipoCNCC() {
    var params = { prefijo: 'TCNC' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoCNCC = result.data;
    });
  }
  listarSolicitudAcceso() {
    this.servSA
      .listarSolicitudAcceso(this.solicitudAcceso)
      .subscribe((result: any) => {
        this.lstSolicituAcceso = result.data;
        this.totalRegistrosSA = result.data.length;
      });
  }

  registrarSolicitudAcceso() {
    const url = this.router.serializeUrl(
      // this.router.createUrlTree(['/planificacion/registrar-solicitud-acceso'])
      this.router.createUrlTree(['registrar-solicitud-acceso'])
    );
    window.open(url, '_blank');
  }

  limpiar() {
    this.solicitudAcceso.codigoTipoPersona = '';
    this.solicitudAcceso.codigoTipoDocumento = '';
    this.solicitudAcceso.numeroDocumento = '';
    this.solicitudAcceso.nombres = '';
    this.solicitudAcceso.numeroRucEmpresa = '';
    this.solicitudAcceso.codigoTipoActor = '';
    this.solicitudAcceso.codigoTipoCncc = '';
    this.solicitudAcceso.estadoSolicitud = '';
    this.solicitudAcceso.razonSocialEmpresa = '';
  }

  loadData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    setTimeout(() => {
      this.dialog.closeAll();
    }, 250);
  }
}
