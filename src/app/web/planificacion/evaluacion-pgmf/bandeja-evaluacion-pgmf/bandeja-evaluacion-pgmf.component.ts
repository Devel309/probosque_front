import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ListarPlanManejoEvaluacionRequest } from 'src/app/model/plan-manejo-evaluacion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { GenericoService } from 'src/app/service/generico.service';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';

@Component({
  selector: 'app-bandeja-evaluacion-pgmf',
  templateUrl: './bandeja-evaluacion-pgmf.component.html',
  styleUrls: ['./bandeja-evaluacion-pgmf.component.scss']
})
export class BandejaEvaluacionPgmfComponent implements OnInit {

  evaluaciones: any[] = [];
  estados: any[] = [];

  idPGMF: number = 21;

  totalRecords: number = 0;

  listarPlanManejoEvaluacionRequest: ListarPlanManejoEvaluacionRequest = new ListarPlanManejoEvaluacionRequest();

  usuario!: UsuarioModel;

  verCamposExtras: boolean = false;

  verComboEstado: boolean = false;

  displayEspecies: boolean = false;
  listadoEspecies: any[] = [];

  constructor(
    private messageService: MessageService,
    private genericoService: GenericoService,
    private dialog: MatDialog,
    private usuarioSrv: UsuarioService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.usuario = this.usuarioSrv.usuario;
    this.verComboEstado = this.usuario.sirperfil == 'TITULARTH';

    this.listarPlanManejoEval(this.listarPlanManejoEvaluacionRequest);


    if (this.verComboEstado)
      this.listarEstado();

    if (this.usuario.sirperfil == 'OSINFOR' || this.usuario.sirperfil == 'COMESTADIST') {
      this.verCamposExtras = true;
    }

  }


  verDetalleEspecies(data: any) {
    let params = {
      idPlanManejoEval: data.idPlanManejoEval
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoEvaluacionService.listarPlanManejoEvaluacionEspecie(params).subscribe((result: any) => {
      this.dialog.closeAll();
      this.listadoEspecies = result.data;
      this.displayEspecies = true;
    })

  }

  verDetalle(data: any) {

    // this.router.navigateByUrl('/planificacion/consideraciones-pgmf', { state: { data: data } });

    this.router.navigate([
      '/planificacion/consideraciones-pgmf',
      data.idPlanManejoEval,
    ]);

  }

  loadData(event: any) {
    this.listarPlanManejoEvaluacionRequest.pageNum = event.first + 1;
    this.listarPlanManejoEvaluacionRequest.pageSize = event.rows;

    this.listarPlanManejoEval(this.listarPlanManejoEvaluacionRequest);
  }

  limpiar() {
    this.listarPlanManejoEvaluacionRequest = new ListarPlanManejoEvaluacionRequest();
    this.listarPlanManejoEval(this.listarPlanManejoEvaluacionRequest);
  }

  buscar() {
    this.listarPlanManejoEval(this.listarPlanManejoEvaluacionRequest);
  }

  private listarPlanManejoEval(params: any) {
    this.totalRecords = 0;
    this.evaluaciones = [];

  //  if (this.usuario.sirperfil != "TITULARTH") {
      this.listarPlanManejoEvaluacionRequest.perfil = this.usuario.sirperfil;
   // }

    if (this.usuario.sirperfil == "TITULARTH") {
      this.listarPlanManejoEvaluacionRequest.idUsuarioRegistro = this.usuario.idusuario;
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoEvaluacionService.listarPlanManejoEvaluacion(params).subscribe((result: any) => {

      this.dialog.closeAll();
      this.evaluaciones = result.data;
      this.totalRecords = result.totalRecord;
    },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );

  }

  verConsension(data: any) {
    this.router.navigate([
      '/planificacion/concesion-forestal-maderables',
      data.idPlanManejo,
    ]);
  }

  enviarNotificacion(data: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idPlanManejoEval: data.idPlanManejoEval,
      idPlanManejo: data.idPlanManejo,
    }

    this.planManejoEvaluacionService.remitirNotificacion(params).subscribe((result: any) => {

      if (result.success) {
        this.SuccessMensaje(result.message);
        this.dialog.closeAll()
      } else {
        this.ErrorMensaje("Oucrrio un error al enviar la notificacion");
        this.dialog.closeAll()
      }

    }, error => {
      this.ErrorMensaje("Oucrrio un error al enviar la notificacion");
      this.dialog.closeAll()
    })

  }




  private listarEstado() {
    let params: any = { idTipoParametro: 92 };

    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {

      if (result.success) {
        result.data.splice(0, 0, {
          codigo: null,
          valorPrimario: '-- Todos --',
        });
        this.estados = result.data;

        if (this.usuario.sirperfil == "ARFFS") {

        }

      }
    },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
      }
    );
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }


  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

}
