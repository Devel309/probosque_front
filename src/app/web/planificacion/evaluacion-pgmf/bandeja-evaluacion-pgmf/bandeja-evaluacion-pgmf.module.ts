import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEvaluacionPgmfComponent } from './bandeja-evaluacion-pgmf.component';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DialogModule } from 'primeng/dialog';
import { TextColorEstadoModule } from 'src/app/shared/components/text-color-estado/text-color-estado.module';



@NgModule({
  declarations: [
    BandejaEvaluacionPgmfComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DropdownModule,
    FieldsetModule,
    PaginatorModule,
    MatDatepickerModule,
    DialogModule,
    TextColorEstadoModule,
  ],
  exports: [BandejaEvaluacionPgmfComponent]
})
export class BandejaEvaluacionPgmfModule { }
