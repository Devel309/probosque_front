import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { InformacionGeneralPGMFA } from 'src/app/model/ResumenEjecutivoPGMFA';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {CodigoPMFIC} from '../../../../../model/util/PMFIC/CodigoPMFIC';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {UsuarioService} from '@services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab-aspectos-complementarios',
  templateUrl: './tab-aspectos-complementarios.component.html',
})
export class TabAspectosComplementariosComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();
  @Input() isPerfilArffs!: boolean;
  @Input() idPlanManejo!: number;

  accept: string = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  informacionGeneral: InformacionGeneralPGMFA = new InformacionGeneralPGMFA();

  @Input("isDisabled") isDisabled: boolean = false;





  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_14;

  codigoAcordeon_14: string = CodigoPMFIC.TAB_14;

  detEvaluacion_14: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_14,
  });

  evaluacion: any;


  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,private user: UsuarioService,
    private toast: ToastService, private evaluacionService: EvaluacionService,
    private informacionGeneralService: InformacionGeneralService,private router: Router
  ) {}

  ngOnInit(): void {
    this.obtenerResumenEjecutivo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  obtenerResumenEjecutivo() {
    var params = {
      codigoProceso: 'PMFIC',
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.isDisabled = true;
          response.data.forEach((element: any) => {
            this.informacionGeneral = new InformacionGeneralPGMFA(element);
          });
        }else {
          this.isDisabled = false;
        }
      });
  }

  actualizarAspectosComplementarios() {

    if(this.informacionGeneral.detalle == undefined || this.informacionGeneral.detalle ==''){
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: "Debe ingresar el campo Observación.",
      });

      return;
    }


    var params = this.informacionGeneral;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe(
        (result: any) => {
          this.obtenerResumenEjecutivo();
          this.dialog.closeAll();
          this.messageService.add({
            severity: 'success',
            summary: '',
            detail: "Se registró los aspectos complementarios correctamente.",
          });
        },
        (error: any) => {
          this.dialog.closeAll();
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: error.message,
          });
        }
      );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }


  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_14 = Object.assign(this.detEvaluacion_14,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_14));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_14])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_14);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:""
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
