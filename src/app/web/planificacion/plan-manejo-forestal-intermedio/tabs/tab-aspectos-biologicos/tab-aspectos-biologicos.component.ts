import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { InformacionBasicaPGMFIC } from "src/app/model/InfirmacionBasicaModel";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {finalize} from 'rxjs/operators';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';

@Component({
  selector: "app-tab-aspectos-biologicos",
  templateUrl: "./tab-aspectos-biologicos.component.html",
})
export class TabAspectosBiologicosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;

  ref: DynamicDialogRef = new DynamicDialogRef();
  listaBosque: any[] = [];
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  idInfBasica_4: number = 0;
  idInfBasicaTB: number = 0;
  idArchivoTB: number = 0;
  isNewTB: boolean = false;
  listArchivo: any[] = [];
  CodigoPMFIC = CodigoPMFIC;


  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_5;

  codigoAcordeon_5: string = CodigoPMFIC.TAB_5;

  detEvaluacion_5: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_5,
  });

  evaluacion: any;

  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private messageService: MessageService,private evaluacionService: EvaluacionService,
    private confirmationService: ConfirmationService,
    private informacionAreaService: InformacionAreaPmfiService,
    private toast: ToastService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,private router: Router
  ) {}

  ngOnInit() {
    this.listarTiposBosque();
    this.listarGeometria();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarTiposBosque() {
    this.listaBosque = [];
    const params = {
      idInfBasica: CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigoPMFIC.TAB_5_2,
    };

    this.informacionAreaService.listarInformacionBasica(params).subscribe(
      (result: any) => {
        if (result.data && result.data.length != 0) {
          this.idInfBasicaTB = result.data[0].idInfBasica;
          result.data.forEach((element: any) => {
            if (!!element.idInfBasicaDet) {
              const array: any[] = [];
              let objDet = new InformacionBasicaPGMFIC(element);

              array.push(objDet);
              this.listaBosque.push(objDet);
              this.calculateAreaTotalBosques();
            }
          });
        }
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarTipoBosque(items: any) {
    this.isNewTB = true;
    this.listaBosque = [];
    this.listaBosque = items;
    this.calculateAreaTotalBosques();
  }

  calculateAreaTotalBosques() {
    let sum1 = 0;
    this.listaBosque.forEach((t: any) => {
      sum1 += t.areaHa;
    });

    this.listaBosque.forEach((t: any) => {
      t.areaHaPorcentaje = Number(((100 * t.areaHa) / sum1).toFixed(2));
    });
    this.totalAreaBosque = parseFloat(`${sum1.toFixed(2)}`);
    this.totalPorcentajeBosque = parseFloat(`${(100).toFixed(2)} %`);
  }

  registrarTiposBosque() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listBosqueDet: any[] = [];
    this.listaBosque.forEach((t: any) => {
      let obj = new InformacionBasicaPGMFIC();
      obj.codInfBasicaDet = CodigoPMFIC.TAB_5_2;
      obj.codSubInfBasicaDet = CodigoPMFIC.TAB_5_2;
      obj.areaHa = t.areaHa;
      obj.areaHaPorcentaje = t.areaHaPorcentaje;
      obj.nombre = t.nombre;
      obj.descripcion = t.descripcion;
      obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
      listBosqueDet.push(obj);
    });
    let params = [
      {
        idInfBasica: this.idInfBasicaTB ? this.idInfBasicaTB : 0,
        codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
        codSubInfBasica: CodigoPMFIC.TAB_5,
        codNombreInfBasica: CodigoPMFIC.TAB_5_2,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listBosqueDet,
      },
    ];

    this.informacionAreaService
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success) {
          // this.toast.ok(response?.message);
          this.toast.ok("Se registró tipos de bosque correctamente.");
          this.isNewTB = false;
          this.listarTiposBosque();
        } else {
          this.toast.warn(response?.message);
        }
        this.dialog.closeAll();
      });
  }

  eliminarTiposBosque(event: any) {
    if (this.listaBosque.length > 0) {
      let msg = "¿Está seguro de eliminar los registros?."
      if (this.idArchivoTB != 0) {
        msg += "\nSe eliminará archivo shapefile de Tipos de Bosque."
      }
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: msg,
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Sí",
        rejectLabel: "No",
        accept: () => {
          if (this.idInfBasicaTB != 0) {
            let params = {
              idInfBasica: this.idInfBasicaTB,
              idInfBasicaDet: 0,
              codInfBasicaDet: "",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  // this.toast.ok(result.message);
                  this.toast.ok("Se eliminó tipos de bosque correctamente.");
                } else {
                  this.toast.warn(result.message);
                }
                this.eliminarArchivoDetalle(this.idArchivoTB);
                this.listaBosque = [];
                this.calculateAreaTotalBosques();
                this.isNewTB = false;
                this.idArchivoTB = 0;
                this.idInfBasicaTB = 0;
                // this.listarTiposBosque();
              });
          } else {
            this.listaBosque = [];
            this.calculateAreaTotalBosques();
            this.isNewTB = false;
            this.idArchivoTB = 0;
            this.idInfBasicaTB = 0;
            // this.listarTiposBosque();
          }
        },
        reject: () => { },
      });
    }
  }

  listarGeometria() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: CodigoPMFIC.CODIGO_PROCESO,
      codigoSubSeccion: "PMFICINFBATIM",
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe(
        (result: any) => {
          if (result.data.length > 0) {
            result.data.forEach((t: any) => {
              if (t.geometry_wkt !== null) {
                if (!this.listArchivo.includes(t.idArchivo)) {
                  if (t.descripcion == "COD9") {
                    this.idArchivoTB = t.idArchivo;
                    this.listArchivo.push(t.idArchivo);
                  }
                }
              }
            });
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error");
        }
      );
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    if (idArchivo != 0) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.servicePlanManejoGeometria
        .eliminarPlanManejoGeometriaArchivo(idArchivo, this.user.idUsuario)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.toast.ok("Se eliminó el archivo y/o geometría correctamente.");
            } else {
              this.toast.error("Ocurrió un problema, intente nuevamente");
            }
          },
          (error) => {
            this.toast.error("Ocurrió un problema, intente nuevamente");
          }
        );
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_5 = Object.assign(this.detEvaluacion_5,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_5));
            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_5])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_5);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:""
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
