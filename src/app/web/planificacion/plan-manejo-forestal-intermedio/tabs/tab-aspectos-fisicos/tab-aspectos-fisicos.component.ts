import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { InformacionBasicaDetalleModelPMFIC, InformacionBasicaModelPMFIC } from "src/app/model/InformacionSocieconomicaPMFIC";
import { Rios } from "src/app/model/medioTrasporte";
import { InformacionBasicaDetalleModel } from "src/app/model/PlanManejo/InformacionBasica/InformacionBasicaDetalleModel";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';

@Component({
  selector: "app-tab-aspectos-fisicos",
  templateUrl: "./tab-aspectos-fisicos.component.html",
})
export class TabAspectosFisicosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;
  ref: DynamicDialogRef = new DynamicDialogRef();
  CodigoPMFIC = CodigoPMFIC;
  codigoTab: string = CodigoPMFIC.TAB_4;
  codigoAcordeon1: string = CodigoPMFIC.TAB_4_1;
  codigoAcordeon2: string = CodigoPMFIC.TAB_4_2;

  cmbRios: Rios[] = [];
  infoBasica4_1: InformacionBasicaModelPMFIC = new InformacionBasicaModelPMFIC({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
    codSubInfBasica: CodigoPMFIC.TAB_4,
    codNombreInfBasica: CodigoPMFIC.TAB_4_1,
    idUsuarioRegistro: this.user.idUsuario,
  });
  infoBasica4_2: InformacionBasicaModelPMFIC = new InformacionBasicaModelPMFIC({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
    codSubInfBasica: CodigoPMFIC.TAB_4,
    codNombreInfBasica: CodigoPMFIC.TAB_4_2,
    idUsuarioRegistro: this.user.idUsuario,
  });
  listInfoBasicaDet_H: InformacionBasicaDetalleModelPMFIC[] = [];
  listInfoBasicaDet_H2: InformacionBasicaDetalleModelPMFIC[] = [];
  listInfoBasicaDet_F: InformacionBasicaDetalleModelPMFIC[] = [];
  listFisiografiaItems: [] = [];
  listFisiografia: any[] = [];
  listArchivo: any[] = [];
  totalSumaUF!: string;
  totalPorcentajeUF!: string;
  idInfBasicaF: number = 0;
  idInfBasicaH: number = 0;
  idArchivoF : number = 0;
  idArchivoH : number = 0;
  isNewF: boolean = false;
  isSubmitting4_1$ = this.query4_1.selectSubmitting();
  isSubmitting4_2$ = this.query4_2.selectSubmitting();

  hidrografia = {} as InformacionBasicaDetalleModelPMFIC;
  displayBasic: boolean = false;
  tituloModal: string = "Agregar Hidrografía";
  listTipo = [
    { tipoLabel: "Río", tipoValue: "Rio" },
    { tipoLabel: "Quebrada", tipoValue: "Quebradas" },
    { tipoLabel: "Cocha", tipoValue: "Cochas" },
  ];



  codigoAcordeon_4: string = CodigoPMFIC.TAB_4;

  detEvaluacion_4: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: CodigoPMFIC.CODIGO_PROCESO,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_4,
  });

  evaluacion: any;


  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private informacionAreaService: InformacionAreaPmfiService,
    private toast: ToastService,
    private createStore4_1: ButtonsCreateStore,
    private query4_1: ButtonsCreateQuery,
    private createStore4_2: ButtonsCreateStore,
    private query4_2: ButtonsCreateQuery,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,private evaluacionService: EvaluacionService,private router: Router
  ) { }

  ngOnInit() {
    this.listRios();
    this.listarAspectosFisicos4_1();
    this.listarAspectosFisicos4_2();
    this.listarGeometria();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listRios() {
    var params = {
      id: 0,
      tipo: "RIO",
    };
    this.informacionAreaPmfiService
      .obtenerHidrografia(params)
      .subscribe((response: any) => {
        this.cmbRios = [ ...response.data ];
      });
  }

  listarAspectosFisicos4_1() {
    this.listInfoBasicaDet_H = [];
    this.listInfoBasicaDet_H2 = [];

    let params: any = {
      idInfBasica: CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon1,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            this.infoBasica4_1 = response.data[0];
            response.data.forEach((item: any) => {
              this.listInfoBasicaDet_H.push(item);
            });

            this.listInfoBasicaDet_H2 = this.integrateHidrografia(this.listInfoBasicaDet_H);
          }
        }
      });
  }

  integrateHidrografia(listUF: any) {
    let listHidro: any = [];
    let listRio: any = [];
    let listQuebrada: any = [];
    let listLaguna: any = [];

    listUF.forEach((t: any) => {
      if (t.descripcion == 'Rio') {
        listRio.push(t);
      } else if (t.descripcion == 'Quebradas') {
        listQuebrada.push(t);
      } else if (t.descripcion == 'Cochas') {
        listLaguna.push(t);
      }
    });

    if ((listRio.length > listQuebrada.length && listRio.length > listLaguna.length) ||
      (listRio.length == listQuebrada.length && listRio.length > listLaguna.length) ||
      (listRio.length > listQuebrada.length && listRio.length == listLaguna.length) ||
      (listRio.length == listQuebrada.length && listRio.length == listLaguna.length)) {
      listRio.forEach((r: any) => {
        let obj: any = {};
        obj.idRio = r.idInfBasicaDet;
        obj.nombreRio = r.nombreRio;
        obj.indexRio = r.idZona;
        listHidro.push(obj);
      });
      listHidro.forEach((t: any, index: number) => {
        listQuebrada.forEach((q: any, index2: number) => {
          if (index == index2) {
            t.idQuebrada = q.idInfBasicaDet;
            t.nombreQuebrada = q.nombreQuebrada;
            t.indexQuebrada = q.idZona;
          }
        });
      });
      listHidro.forEach((t: any, index: number) => {
        listLaguna.forEach((l: any, index2: number) => {
          if (index == index2) {
            t.idLaguna = l.idInfBasicaDet;
            t.nombreLaguna = l.nombreLaguna;
            t.indexLaguna = l.idZona;
          }
        });
      });
    }

    if ((listQuebrada.length > listRio.length && listQuebrada.length > listLaguna.length) ||
      (listQuebrada.length > listRio.length && listQuebrada.length == listLaguna.length)) {
      listQuebrada.forEach((q: any) => {
        let obj: any = {};
        obj.idQuebrada = q.idInfBasicaDet;
        obj.nombreQuebrada = q.nombreQuebrada;
        obj.indexQuebrada = q.idZona;
        listHidro.push(obj);
      });
      listHidro.forEach((t: any, index: number) => {
        listRio.forEach((r: any, index2: number) => {
          if (index == index2) {
            t.idRio = r.idInfBasicaDet;
            t.nombreRio = r.nombreRio;
            t.indexRio = r.idZona;
          }
        });
      });
      listHidro.forEach((t: any, index: number) => {
        listLaguna.forEach((l: any, index2: number) => {
          if (index == index2) {
            t.idLaguna = l.idInfBasicaDet;
            t.nombreLaguna = l.nombreLaguna;
            t.indexLaguna = l.idZona;
          }
        });
      });
    }

    if (listLaguna.length > listRio.length && listLaguna.length > listQuebrada.length) {
      listLaguna.forEach((l: any) => {
        let obj: any = {};
        obj.idLaguna = l.idInfBasicaDet;
        obj.nombreLaguna = l.nombreLaguna;
        obj.indexLaguna = l.idZona;
        listHidro.push(obj);
      });
      listHidro.forEach((t: any, index: number) => {
        listRio.forEach((r: any, index2: number) => {
          if (index == index2) {
            t.idRio = r.idInfBasicaDet;
            t.nombreRio = r.nombreRio;
            t.indexRio = r.idZona;
          }
        });
      });
      listHidro.forEach((t: any, index: number) => {
        listQuebrada.forEach((q: any, index2: number) => {
          if (index == index2) {
            t.idQuebrada = q.idInfBasicaDet;
            t.nombreQuebrada = q.nombreQuebrada;
            t.indexQuebrada = q.idZona;
          }
        });
      });
    }

    return listHidro;
  }

  listarAspectosFisicos4_2() {
    this.listInfoBasicaDet_F = [];
    let params: any = {
      idInfBasica: CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon2,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            this.infoBasica4_2 = response.data[ 0 ];
            response.data.forEach((item: any) => {
              this.listInfoBasicaDet_F.push(item);
            });
            this.calculateTotalFisiografia();
          }
        }
      });
  }

  agregarDetalle(acordeon: string) {
    if (acordeon == this.codigoAcordeon1) {
      this.listInfoBasicaDet_H.push(
        new InformacionBasicaDetalleModelPMFIC({
          idInfBasicaDet: 0,
          codInfBasicaDet: acordeon,
          codSubInfBasicaDet: acordeon,
          idUsuarioRegistro: this.user.idUsuario,
        })
      );
    }
  }

  openModalHidrografia() {
    this.hidrografia = {} as InformacionBasicaDetalleModelPMFIC;
    this.displayBasic = true;
  }

  agregarHidrografia() {
    if (!this.validarNuevo()) {
      return;
    }

    if (this.hidrografia.descripcion == "Rio") {
      this.hidrografia.nombreRio = this.hidrografia.nombre;
      this.hidrografia.idInfBasicaDet = 0;
    }
    if (this.hidrografia.descripcion == "Quebradas") {
      this.hidrografia.nombreQuebrada = this.hidrografia.nombre;
      this.hidrografia.idInfBasicaDet = 0;
    }
    if (this.hidrografia.descripcion == "Cochas") {
      this.hidrografia.nombreLaguna = this.hidrografia.nombre;
      this.hidrografia.idInfBasicaDet = 0;
    }
    this.hidrografia.idZona = this.listInfoBasicaDet_H.length;
    this.listInfoBasicaDet_H.push(this.hidrografia);
    this.listInfoBasicaDet_H2 = [];
    this.listInfoBasicaDet_H2 = this.integrateHidrografia(this.listInfoBasicaDet_H);
    this.hidrografia = {} as InformacionBasicaDetalleModelPMFIC;
    this.displayBasic = false;
  }

  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.hidrografia.descripcion) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Tipo.\n";
    }
    if (!this.hidrografia.nombre) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre.\n";
    }
    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  cerrarModal() {
    this.hidrografia = {} as InformacionBasicaDetalleModelPMFIC;
    this.displayBasic = false;
  }

  registrarAspectosFisicos4_1() {
    this.createStore4_1.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listaHidrografia: any[] = [];

    this.listInfoBasicaDet_H2.forEach((h: any) => {
      if (h.nombreRio) {
        let obj = new InformacionBasicaDetalleModelPMFIC();
        obj.codInfBasicaDet = CodigoPMFIC.TAB_4_1;
        obj.codSubInfBasicaDet = CodigoPMFIC.TAB_4_1;
        obj.idInfBasicaDet = h.idRio;
        obj.descripcion = 'Rio';
        obj.nombreRio = h.nombreRio;
        listaHidrografia.push(obj);
      }
      if (h.nombreQuebrada) {
        let obj = new InformacionBasicaDetalleModelPMFIC();
        obj.codInfBasicaDet = CodigoPMFIC.TAB_4_1;
        obj.codSubInfBasicaDet = CodigoPMFIC.TAB_4_1;
        obj.idInfBasicaDet = h.idQuebrada;
        obj.descripcion = 'Quebradas';
        obj.nombreQuebrada = h.nombreQuebrada;
        listaHidrografia.push(obj);
      }
      if (h.nombreLaguna) {
        let obj = new InformacionBasicaDetalleModelPMFIC();
        obj.codInfBasicaDet = CodigoPMFIC.TAB_4_1;
        obj.codSubInfBasicaDet = CodigoPMFIC.TAB_4_1;
        obj.idInfBasicaDet = h.idLaguna;
        obj.descripcion = 'Cochas';
        obj.nombreLaguna = h.nombreLaguna;
        listaHidrografia.push(obj);
      }
    });

    const obj = new InformacionBasicaModelPMFIC(this.infoBasica4_1);
    obj.idPlanManejo = this.idPlanManejo;
    obj.codInfBasica = CodigoPMFIC.CODIGO_PROCESO;
    obj.codSubInfBasica = CodigoPMFIC.TAB_4;
    obj.codNombreInfBasica = CodigoPMFIC.TAB_4_1;
    obj.idUsuarioRegistro = this.user.idUsuario;
    obj.listInformacionBasicaDet = [];
    obj.listInformacionBasicaDet = listaHidrografia;

    this.informacionAreaService
      .registrarInformacionBasicaAspectosFisicos([ obj ])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró la hidrografía de la UMF correctamente.");
          this.listarAspectosFisicos4_1();
          this.createStore4_1.submitSuccess();
        } else {
          this.toast.error(response?.message);
          this.createStore4_1.submitError('');
        }
      }, (error) => {
        this.createStore4_1.submitError(error);
      });
  }

  validarH() {
    let validar: boolean = true;
    let mensaje: string = "";

    this.listInfoBasicaDet_H.forEach((item: any) => {
      if (!item.idRios) {
        validar = false;
        mensaje = mensaje += "(*) Debe seleccionar: Río.\n";
      }
      if (!item.nombreQuebrada) {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: Quebrada.\n";
      }
      if (!item.nombreLaguna) {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: Laguna.\n";
      }
    });

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  registrarAspectosFisicosAccordeon4_2() {
    this.createStore4_2.submit();
    this.infoBasica4_2.listInformacionBasicaDet = [];
    const obj = new InformacionBasicaModelPMFIC(this.infoBasica4_2);
    obj.idPlanManejo = this.idPlanManejo;
    obj.codSubInfBasica = CodigoPMFIC.TAB_4;
    obj.listInformacionBasicaDet = this.listInfoBasicaDet_F;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaService
      .registrarInformacionBasicaAspectosFisicos([obj])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró la fisiografía de la UMF correctamente.');
          this.isNewF = false;
          this.idInfBasicaF = 0;
          this.listarAspectosFisicos4_2();
          this.createStore4_2.submitSuccess();
        } else {
          this.toast.error(response?.message);
          this.createStore4_2.submitError('');
        }
      }, (error) => {
        this.createStore4_2.submitError(error);
      });
  }

  mostrarlistaFisiografia(items: any) {
    setTimeout(() => {
      if (this.listInfoBasicaDet_F.length === 0) {
        this.isNewF = true;
        this.listFisiografiaItems = items;

        this.listFisiografiaItems.forEach((element: any) => {
          var objDet = new InformacionBasicaDetalleModelPMFIC();
          objDet.codInfBasicaDet = this.codigoAcordeon2;
          objDet.codSubInfBasicaDet = this.codigoAcordeon2;
          objDet.puntoVertice = element.vertice;
          objDet.areaHa = element.area;
          objDet.descripcion = element.nombreUF;
          objDet.areaHaPorcentaje = element.porcentaje;
          objDet.observaciones = element.observaciones;
          this.listInfoBasicaDet_F.push(objDet);
        });
      }

      this.calculateTotalFisiografia();
    }, 500);
  }

  calculateTotalFisiografia() {
    let sum1 = 0;
    this.listInfoBasicaDet_F.forEach((item) => {
      sum1 += parseFloat(item.areaHa);
      item.areaHaPorcentaje = Number(((100 * parseFloat(item.areaHa)) / sum1).toFixed(2));
    });

    this.totalSumaUF = `${sum1.toFixed(2)}`;
    this.totalPorcentajeUF = this.listInfoBasicaDet_F.length === 0 ? `${0}` : `${(100).toFixed(2)}`;
  }

  eliminarFisiografia(event: any) {
    if (this.listInfoBasicaDet_F.length > 0) {
      let msg = "¿Está seguro de eliminar los registros?.";
      if (this.idArchivoF != 0) {
        msg += "\nSe eliminará archivo shapefile de Fisiografía."
      }
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: msg,
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Sí",
        rejectLabel: "No",
        accept: () => {
          for (let item of this.listInfoBasicaDet_F) {
            this.idInfBasicaF = item.idInfBasica;
          }

          if (this.idInfBasicaF != null && this.idInfBasicaF != undefined) {
            let params = {
              idInfBasica: this.idInfBasicaF,
              idInfBasicaDet: 0,
              codInfBasicaDet: "",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  // this.toast.ok(result.message);
                  this.toast.ok("Se eliminó los Aspectos de Fisiografía correctamente.");
                } else {
                  this.toast.warn(result.message);
                }
                this.eliminarArchivoDetalle(this.idArchivoF);
                this.listInfoBasicaDet_F = [];
                this.calculateTotalFisiografia();
                this.isNewF = false;
                this.idArchivoF = 0;
                this.idInfBasicaF = 0;
                // this.listarAspectosFisicos4_2();
              });
          } else {
            this.listInfoBasicaDet_F = [];
            this.calculateTotalFisiografia();
            this.isNewF = false;
            this.idArchivoF = 0;
            this.idInfBasicaF = 0;
            // this.listarAspectosFisicos4_2();
          }
          this.infoBasica4_2 = new InformacionBasicaModelPMFIC({
            idInfBasica: 0,
            idPlanManejo: this.idPlanManejo,
            codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
            codSubInfBasica: CodigoPMFIC.TAB_4,
            codNombreInfBasica: CodigoPMFIC.TAB_4_2,
            idUsuarioRegistro: this.user.idUsuario,
          });
        },
        reject: () => { },
      });
    }
  }

  eliminarItemInformacionAreaManejo(
    event: any,
    item: InformacionBasicaDetalleModel,
    index: number,
    acordeon: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (acordeon == this.codigoAcordeon1) {
          if (item.idInfBasicaDet == 0) {
            this.toast.ok("Se eliminó el registro correctamente.");
            this.listInfoBasicaDet_H.splice(index, 1);
          } else {
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.informacionAreaService
              .eliminarInformacionBasica({
                idInfBasica: 0,
                codInfBasicaDet: "",
                idInfBasicaDet: item.idInfBasicaDet,
                idUsuarioElimina: this.user.idUsuario,
              })
              .pipe(finalize(() => this.dialog.closeAll()))
              .subscribe((result: any) => {
                this.toast.ok("Se eliminó el registro correctamente.");
                this.listInfoBasicaDet_H.splice(index, 1);
              });
          }
        }
      },
      reject: () => { },
    });
  }

  listarEliminarH(items: any) {
    this.eliminarHidrografiaRegistro(items[0].event, items[0].idInfBasicaDet, items[0].index);
  }

  eliminarHidrografiaRegistro(event: any, idInfBasicaDet: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (idInfBasicaDet == 0) {
          this.toast.ok("Se eliminó el registro correctamente.");
          // this.listarAspectosFisicos4_1();
          this.listInfoBasicaDet_H = this.listInfoBasicaDet_H.filter((item) => item.idZona !== index);
          this.listInfoBasicaDet_H2 = this.integrateHidrografia(this.listInfoBasicaDet_H);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: "",
              idInfBasicaDet: idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              this.toast.ok("Se eliminó el registro correctamente.");
              // this.listarAspectosFisicos4_1();
              this.listInfoBasicaDet_H = this.listInfoBasicaDet_H.filter((item) => item.idInfBasicaDet !== idInfBasicaDet);
              this.listInfoBasicaDet_H2 = this.integrateHidrografia(this.listInfoBasicaDet_H);
            });
        }
      },
      reject: () => { },
    });
  }

  listarGeometria() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: CodigoPMFIC.CODIGO_PROCESO,
      codigoSubSeccion: "PMFICINFBATIM",
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe(
        (result: any) => {
          if (result.data.length > 0) {
            result.data.forEach((t: any) => {
              if (t.geometry_wkt !== null) {
                if (!this.listArchivo.includes(t.idArchivo)) {
                  if (t.descripcion == "COD6") {
                    this.idArchivoH = t.idArchivo;
                    this.listArchivo.push(t.idArchivo);
                  }
                  if (t.descripcion == "COD7") {
                    this.idArchivoF = t.idArchivo;
                    this.listArchivo.push(t.idArchivo);
                  }
                }
              }
            });
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error");
        }
      );
  }

  eliminarHidrografia(event: any) {
    if (this.listInfoBasicaDet_H.length > 0) {
      let msg = "¿Está seguro de eliminar los registros?."
      if (this.idArchivoH != 0) {
        msg += "\nSe eliminará archivo shapefile de Hidrografía."
      }
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: msg,
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Sí",
        rejectLabel: "No",
        accept: () => {
          for (let item of this.listInfoBasicaDet_H) {
            this.idInfBasicaH = item.idInfBasica;
          }

          if (this.idInfBasicaH != null && this.idInfBasicaH != undefined) {
            let params = {
              idInfBasica: this.idInfBasicaH,
              idInfBasicaDet: 0,
              codInfBasicaDet: "",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  // this.toast.ok(result.message);
                  this.toast.ok("Se eliminó los Aspectos de Hidrografía correctamente.");
                } else {
                  this.toast.warn(result.message);
                }
                this.eliminarArchivoDetalle(this.idArchivoH);
                this.listInfoBasicaDet_H = [];
                this.listInfoBasicaDet_H2 = [];
                this.idInfBasicaH = 0;
                this.idArchivoH = 0;
                // this.listarAspectosFisicos4_1();
              });
          } else {
            this.listInfoBasicaDet_H = [];
            this.listInfoBasicaDet_H2 = [];
            this.idInfBasicaH = 0;
            // this.listarAspectosFisicos4_1();
          }
          this.infoBasica4_1 = new InformacionBasicaModelPMFIC({
            idInfBasica: 0,
            idPlanManejo: this.idPlanManejo,
            codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
            codSubInfBasica: CodigoPMFIC.TAB_4,
            codNombreInfBasica: CodigoPMFIC.TAB_4_1,
            idUsuarioRegistro: this.user.idUsuario,
          });
        },
        reject: () => { },
      });
    }
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    if (idArchivo != 0) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.servicePlanManejoGeometria
        .eliminarPlanManejoGeometriaArchivo(idArchivo, this.user.idUsuario)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.toast.ok("Se eliminó el archivo y/o geometría correctamente.");
            } else {
              this.toast.error("Ocurrió un problema, intente nuevamente");
            }
          },
          (error) => {
            this.toast.error("Ocurrió un problema, intente nuevamente");
          }
        );
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }


  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: CodigoPMFIC.CODIGO_PROCESO,
      //codigoEvaluacionDet: CodigoPMFIC.CODIGO_PROCESO,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_4 = Object.assign(this.detEvaluacion_4,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_4));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: CodigoPMFIC.CODIGO_PROCESO,
              codigoEvaluacionDet: CodigoPMFIC.CODIGO_PROCESO,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_4])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_4);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:""
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+CodigoPMFIC.CODIGO_PROCESO);
  }
}
