import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastService } from '@shared';
import { SistemaManejoForestalService, UsuarioService } from '@services';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ManejoBosqueCabecera, ManejoBosqueDetalleModel } from '../../../../../model/ManejoBosque';
import { CodigoPMFIC } from '../../../../../model/util/PMFIC/CodigoPMFIC';
import { ManejoBosqueService } from '../../../../../service/manejoBosque.service';
import { InformacionAreaPmfiService } from '../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab-sistema-manejo-forestal',
  templateUrl: './tab-sistema-manejo-forestal.component.html',
})
export class TabSistemaManejoForestalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  ref: DynamicDialogRef = new DynamicDialogRef();
  @Input() disabled!: boolean;
  @Output()
  public siguiente = new EventEmitter();
  @Input() isPerfilArffs!: boolean;
  @Output()
  public regresar = new EventEmitter();

  CodigoPMFIC = CodigoPMFIC;

  categoriasZonificacion = false;

  item_9_0: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_modal_9_0 = {} as ManejoBosqueDetalleModel;
  lista_9_0: ManejoBosqueDetalleModel[] = [];

  item_9_1: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_9_1_1: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  det_9_1_2: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();

  det_modal_913y923 = {} as ManejoBosqueDetalleModel;
  lista_9_1_3: ManejoBosqueDetalleModel[] = [];

  item_9_2: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_9_2_1: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  det_9_2_2: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  lista_9_2_3: ManejoBosqueDetalleModel[] = [];

  item_9_3: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_modal_931y932 = {} as ManejoBosqueDetalleModel;
  lista_9_3_1: ManejoBosqueDetalleModel[] = [];
  lista_9_3_2: ManejoBosqueDetalleModel[] = [];

  tituloModal: string = '';

  totalVcp: number = 0;

  showModal_9_0: boolean = false;
  showModal_913y923: boolean = false;
  showModal_931y932: boolean = false;
  edit: boolean = false;
  indexUpdate: number = -1;
  tipoTabla: string = '';




  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_9;

  codigoAcordeon_9: string = CodigoPMFIC.TAB_9;

  detEvaluacion_9: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_9,
  });

  evaluacion: any;

  constructor(
    public dialogService: DialogService,
    private api: SistemaManejoForestalService,
    private dialog: MatDialog,
    private msgService: MessageService,
    private confirmationService: ConfirmationService,
    private manejoBosqueService: ManejoBosqueService,
    private user: UsuarioService,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,private evaluacionService: EvaluacionService,private router: Router
  ) {}

  ngOnInit() {
    this.item_9_0.idPlanManejo = this.idPlanManejo;
    this.item_9_0.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_0.codigoManejo = CodigoPMFIC.CODIGO_PROCESO;
    this.item_9_0.subCodigoManejo = CodigoPMFIC.ACORDEON_9_0;

    this.item_9_1.idPlanManejo = this.idPlanManejo;
    this.item_9_1.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_1.codigoManejo = CodigoPMFIC.CODIGO_PROCESO;
    this.item_9_1.subCodigoManejo = CodigoPMFIC.ACORDEON_9_1;

    this.det_9_1_1.idManejoBosqueDet = 0;
    this.det_9_1_1.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_1;
    this.det_9_1_1.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_1_1;
    this.det_9_1_1.idUsuarioRegistro = this.user.idUsuario;

    this.det_9_1_2.idManejoBosqueDet = 0;
    this.det_9_1_2.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_1;
    this.det_9_1_2.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_1_2;
    this.det_9_1_2.idUsuarioRegistro = this.user.idUsuario;

    //ACORDEON  9_2
    this.item_9_2.idPlanManejo = this.idPlanManejo;
    this.item_9_2.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_2.codigoManejo = CodigoPMFIC.CODIGO_PROCESO;
    this.item_9_2.subCodigoManejo = CodigoPMFIC.ACORDEON_9_2;

    this.det_9_2_1.idManejoBosqueDet = 0;
    this.det_9_2_1.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_2;
    this.det_9_2_1.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_2_1;
    this.det_9_2_1.idUsuarioRegistro = this.user.idUsuario;

    this.det_9_2_2.idManejoBosqueDet = 0;
    this.det_9_2_2.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_2;
    this.det_9_2_2.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_2_2;
    this.det_9_2_2.idUsuarioRegistro = this.user.idUsuario;

    this.item_9_3.idPlanManejo = this.idPlanManejo;
    this.item_9_3.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_3.codigoManejo = CodigoPMFIC.CODIGO_PROCESO;
    this.item_9_3.subCodigoManejo = CodigoPMFIC.ACORDEON_9_3;

    this.listarAcordeon_9_0();
    this.listarAcordeon_9_1();
    this.listarAcordeon_9_2();
    this.listarAcordeon_9_3();


    if(this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarAcordeon_9_0() {
    this.lista_9_0 = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPMFIC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPMFIC.ACORDEON_9_0,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .pipe(finalize(() => this.calcularTotal()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];

          if (respuesta.idManejoBosque) {
            this.item_9_0.idManejoBosque = respuesta.idManejoBosque;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              this.lista_9_0.push(new ManejoBosqueDetalleModel(e));
            });
          } else {
            this.item_9_0.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (e.tratamientoSilvicultural != 'Otras (especificar)') {
                e.accion = 1;
              }
              this.lista_9_0.push(new ManejoBosqueDetalleModel(e));
            });
          }
        }
      });
  }

  listarInformacionBasica3_3_zonificacion() {
    const params = {
      idInfBasica: this.CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.CodigoPMFIC.TAB_3_3
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => {
        this.dialog.closeAll();
      }))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.forEach((elemento: any) => {

          });
        }
      });
  }

  listarAcordeon_9_1() {
    this.lista_9_1_3 = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPMFIC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPMFIC.ACORDEON_9_1,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];

          if (respuesta.idManejoBosque) {
            this.item_9_1.idManejoBosque = respuesta.idManejoBosque;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_1) {
                this.det_9_1_1 = new ManejoBosqueDetalleModel(e);
              } else if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_2) {
                this.det_9_1_2 = new ManejoBosqueDetalleModel(e);
              } else if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_3) {
                this.lista_9_1_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          } else {
            this.item_9_1.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_3) {  
                if (e.tratamientoSilvicultural != 'Otros (especificar)') {
                  e.accion = 1;
                }
                this.lista_9_1_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          }
        }
      });
  }

  listarAcordeon_9_2() {
    this.lista_9_2_3 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPMFIC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPMFIC.ACORDEON_9_2,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];
          if (respuesta.idManejoBosque) {
            this.item_9_2.idManejoBosque = respuesta.idManejoBosque;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_1) {
                this.det_9_2_1 = new ManejoBosqueDetalleModel(e);
              } else if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_2) {
                this.det_9_2_2 = new ManejoBosqueDetalleModel(e);
              } else if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_3) {
                this.lista_9_2_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          } else {
            this.item_9_2.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_3) {
                if (e.tratamientoSilvicultural != 'Otros (especificar)') {
                  e.accion = 1;
                }
                this.lista_9_2_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          }
        }
      });
  }

  listarAcordeon_9_3() {
    this.lista_9_3_1 = [];
    this.lista_9_3_2 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPMFIC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPMFIC.ACORDEON_9_3,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];
          if (respuesta.idManejoBosque) {
            this.item_9_3.idManejoBosque = respuesta.idManejoBosque;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              if (
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO
              ) {
                this.lista_9_3_1.push(new ManejoBosqueDetalleModel(e));
              } else if (
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OPCIONALES
              ) {
                this.lista_9_3_2.push(new ManejoBosqueDetalleModel(e));
              }
            });
          } else {
            this.item_9_3.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO) {
                e.accion = 1;
                this.lista_9_3_1.push(new ManejoBosqueDetalleModel(e));
              } else if (e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OPCIONALES) {
                if (e.tratamientoSilvicultural != 'Otros (especificar)') {
                  e.accion = 1;
                }
                this.lista_9_3_2.push(new ManejoBosqueDetalleModel(e));
              }
            });
          }
        }
      });
  }

  registrarAcordeon9_1() {
    this.item_9_1.listManejoBosqueDetalle = this.lista_9_1_3;
    this.item_9_1.listManejoBosqueDetalle.push(this.det_9_1_1);
    this.item_9_1.listManejoBosqueDetalle.push(this.det_9_1_2);
    var params = [this.item_9_1];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok('Se registró Manejo forestal con fines maderables correctamente.');
          this.listarAcordeon_9_1();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarAcordeon9_2() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.item_9_2.listManejoBosqueDetalle = this.lista_9_2_3;
    this.item_9_2.listManejoBosqueDetalle.push(this.det_9_2_1);
    this.item_9_2.listManejoBosqueDetalle.push(this.det_9_2_2);
    var params = [this.item_9_2];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok('Se registró Manejo forestal con fines no maderables correctamente.');
          this.listarAcordeon_9_2();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarAcordeon9_3() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.item_9_3.listManejoBosqueDetalle = this.lista_9_3_1.concat(
      this.lista_9_3_2
    );
    var params = [this.item_9_3];
    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok('Se registró Labores silviculturales correctamente.');
          this.listarAcordeon_9_3();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarAcordeon9_0() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.item_9_0.listManejoBosqueDetalle = this.lista_9_0;
    var params = [this.item_9_0];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok('Se registró Uso potencial y actividades a realizar correctamente.');
          this.listarAcordeon_9_0();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  agregar() {
    if (this.edit) {
      this.editarItem();
    } else {
      this.agregarItem();
    }
  }

  validarFormulario_9_0() {
    let respuest: boolean = true;
    if (
      this.det_modal_9_0.tratamientoSilvicultural == null ||
      this.det_modal_9_0.tratamientoSilvicultural == '' ||
      this.det_modal_9_0.tratamientoSilvicultural == undefined
    ) {
      this.toast.warn('Ingrese una Categoría de zonificación de la UMF.');
      respuest = false;
    }
    return respuest;
  }

  validarFormulario_913_923() {
    let respuest: boolean = true;
    if (
      this.det_modal_913y923.tratamientoSilvicultural == null ||
      this.det_modal_913y923.tratamientoSilvicultural == '' ||
      this.det_modal_913y923.tratamientoSilvicultural == undefined
    ) {
      this.toast.warn('Ingrese una Actividad.');
      respuest = false;
    }
    return respuest;
  }

  validarFormulario_931_932() {
    let respuest: boolean = true;
    if (
      this.det_modal_931y932.tratamientoSilvicultural == null ||
      this.det_modal_931y932.tratamientoSilvicultural == '' ||
      this.det_modal_931y932.tratamientoSilvicultural == undefined
    ) {
      this.toast.warn('Ingrese una Práctica Silvicultural.');
      respuest = false;
    }
    return respuest;
  }

  calcularTotal() {
    this.totalVcp = 0;
    this.lista_9_0.forEach((e: ManejoBosqueDetalleModel) => {
      this.totalVcp = Number(this.totalVcp) + Number(e.nuTotalVcp);
    });
  }

  agregarItem() {
    if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_0) {
      if (this.validarFormulario_9_0()) {
        this.det_modal_9_0.idManejoBosqueDet = 0;
        this.det_modal_9_0.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_0;
        this.det_modal_9_0.nuTotalVcp = this.det_modal_9_0.nuTotalVcp
          ? this.det_modal_9_0.nuTotalVcp
          : 0;
        this.det_modal_9_0.idUsuarioRegistro = this.user.idUsuario;

        this.lista_9_0.push(this.det_modal_9_0);
        this.calcularTotal();

        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_1_3) {
      if (this.validarFormulario_913_923()) {
        this.det_modal_913y923.idManejoBosqueDet = 0;
        this.det_modal_913y923.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_1;
        this.det_modal_913y923.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_1_3;
        this.det_modal_913y923.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_1_3.push(this.det_modal_913y923);
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_2_3) {
      if (this.validarFormulario_913_923()) {
        this.det_modal_913y923.idManejoBosqueDet = 0;
        this.det_modal_913y923.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_2;
        this.det_modal_913y923.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_2_3;
        this.det_modal_913y923.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_2_3.push(this.det_modal_913y923);
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO) {
      if (this.validarFormulario_931_932()) {
        this.det_modal_931y932.idManejoBosqueDet = 0;
        this.det_modal_931y932.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_3;
        this.det_modal_931y932.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO;
        this.det_modal_931y932.actividad = CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO;

        this.det_modal_931y932.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_3_1.push(this.det_modal_931y932);
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OPCIONALES) {
      if (this.validarFormulario_931_932()) {
        this.det_modal_931y932.idManejoBosqueDet = 0;
        this.det_modal_931y932.codtipoManejoDet = CodigoPMFIC.ACORDEON_9_3;
        this.det_modal_931y932.subCodtipoManejoDet = CodigoPMFIC.ACORDEON_9_3_OPCIONALES;
        this.det_modal_931y932.actividad = CodigoPMFIC.ACORDEON_9_3_OPCIONALES;

        this.det_modal_931y932.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_3_2.push(this.det_modal_931y932);
        this.seteoValoresDefecto();
      }
    }
  }

  seteoValoresDefecto() {
    this.tipoTabla = '';
    this.det_modal_913y923 = {} as ManejoBosqueDetalleModel;
    this.showModal_913y923 = false;
    this.det_modal_9_0 = {} as ManejoBosqueDetalleModel;
    this.showModal_9_0 = false;
    this.det_modal_931y932 = {} as ManejoBosqueDetalleModel;
    this.showModal_931y932 = false;

    this.indexUpdate = -1;
    this.edit = false;
  }

  editarItem() {
    if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_0) {
      if (this.validarFormulario_9_0()) {
        if (this.indexUpdate >= 0) {
          this.det_modal_9_0.nuTotalVcp = this.det_modal_9_0.nuTotalVcp
            ? this.det_modal_9_0.nuTotalVcp
            : 0;
          this.lista_9_0[this.indexUpdate] = this.det_modal_9_0;
        }
        this.seteoValoresDefecto();
        this.calcularTotal();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_1_3) {
      if (this.validarFormulario_913_923()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_1_3[this.indexUpdate] = this.det_modal_913y923;
        }
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_2_3) {
      if (this.validarFormulario_913_923()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_2_3[this.indexUpdate] = this.det_modal_913y923;
        }
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO) {
      if (this.validarFormulario_931_932()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_3_1[this.indexUpdate] = this.det_modal_931y932;
        }
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OPCIONALES) {
      if (this.validarFormulario_931_932()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_3_2[this.indexUpdate] = this.det_modal_931y932;
        }
        this.seteoValoresDefecto();
      }
    }

    /* this.tipoTabla = "";
    this.showModal_913y923 = false;
    this.showModal_931y932 = false;
    this.showModal_9_0 = false;
    this.indexUpdate = -1;
    this.det_modal_9_0      = {} as ManejoBosqueDetalleModel;
    this.det_modal_913y923  = {} as ManejoBosqueDetalleModel;
    this.det_modal_931y932  = {} as ManejoBosqueDetalleModel;
    this.edit = false;*/
  }



  openModalNuevo(tipo: string) {
    this.seteoValoresDefecto();
    this.tipoTabla = tipo;
    this.edit = false;

    if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_0) {
      this.tituloModal = 'Actividad a realizar según la categoría de zonificación de la UMF ';
      this.det_modal_9_0.accion = 0;
      this.showModal_9_0 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_1_3) {
      this.tituloModal = 'Actividad de aprovechamiento y equipos a utilizar con fines maderables ';
      this.det_modal_913y923.accion = 0;
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_2_3) {
      this.tituloModal = 'Actividad de aprovechamiento y equipos a utilizar con fines no maderables ';
      this.det_modal_913y923.accion = 0;
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO) {
      this.tituloModal = 'Labores Silviculturales - Actividad Obligatoria ';
      this.showModal_931y932 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OPCIONALES) {
      this.tituloModal = 'Labores Silviculturales - Actividad Opcional ';
      this.det_modal_931y932.accion = 0;
      this.showModal_931y932 = true;
    }
  }

  openModalEditar(
    manejoBosqueDetalleModel: ManejoBosqueDetalleModel,
    index: number,
    tipo: string
  ) {
    this.tipoTabla = tipo;
    this.edit = true;
    this.indexUpdate = index;

    if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_0) {
      this.det_modal_9_0 = { ...manejoBosqueDetalleModel };
      this.tituloModal =
        'Actividad a realizar según la categoría de zonificación de la UMF ';
      this.showModal_9_0 = true;

      const defaultValues = [
        'Áreas de Producción maderable',
        'Áreas de Producción no maderable',
        'Áreas deforestadas y/o degradadas',
        'Áreas de protección',
      ];

      this.categoriasZonificacion= defaultValues.includes(this.det_modal_9_0.tratamientoSilvicultural);

    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_1_3) {
      this.tituloModal =
        'Actividad de aprovechamiento y equipos a utilizar con fines maderables ';
      this.det_modal_913y923 = { ...manejoBosqueDetalleModel };
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_2_3) {
      this.tituloModal =
        'Actividad de aprovechamiento y equipos a utilizar con fines no maderables ';
      this.det_modal_913y923 = { ...manejoBosqueDetalleModel };
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO) {
      this.tituloModal = 'Labores Silviculturales - Actividad Obligatoria ';
      this.det_modal_931y932 = { ...manejoBosqueDetalleModel };
      this.showModal_931y932 = true;
    } else if (this.tipoTabla == CodigoPMFIC.ACORDEON_9_3_OPCIONALES) {
      this.tituloModal = 'Labores Silviculturales - Actividad Opcional ';
      this.det_modal_931y932 = { ...manejoBosqueDetalleModel };
      this.showModal_931y932 = true;
    }
  }

  openEliminar(
    event: Event,
    index: number,
    manejoBosqueDetalleModel: ManejoBosqueDetalleModel,
    tabla: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (manejoBosqueDetalleModel.idManejoBosqueDet != 0) {
          var params = {
            idManejoBosque: 0,
            idManejoBosqueDet: manejoBosqueDetalleModel.idManejoBosqueDet,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.dialog.open(LoadingComponent, { disableClose: true });
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((data: any) => {
              if (tabla == CodigoPMFIC.ACORDEON_9_0)
                this.lista_9_0.splice(index, 1);
              else if (tabla == CodigoPMFIC.ACORDEON_9_1_3)
                this.lista_9_1_3.splice(index, 1);
              else if (tabla == CodigoPMFIC.ACORDEON_9_2_3)
                this.lista_9_2_3.splice(index, 1);
              else if (tabla == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO)
                this.lista_9_3_1.splice(index, 1);
              else if (tabla == CodigoPMFIC.ACORDEON_9_3_OPCIONALES)
                this.lista_9_3_2.splice(index, 1);

              this.toast.ok(data.message);
            });
        } else {
          if (tabla == CodigoPMFIC.ACORDEON_9_0)
            this.lista_9_0.splice(index, 1);
          else if (tabla == CodigoPMFIC.ACORDEON_9_1_3)
            this.lista_9_1_3.splice(index, 1);
          else if (tabla == CodigoPMFIC.ACORDEON_9_2_3)
            this.lista_9_2_3.splice(index, 1);
          else if (tabla == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO)
            this.lista_9_3_1.splice(index, 1);
          else if (tabla == CodigoPMFIC.ACORDEON_9_3_OPCIONALES)
            this.lista_9_3_2.splice(index, 1);

          this.toast.ok('Detalle de Manejo forestal eliminado correctamente.');
        }
      },
      reject: () => {},
    });
  }

  cerrarModal() {
    this.det_modal_9_0 = {} as ManejoBosqueDetalleModel;
    this.showModal_9_0 = false;
    this.det_modal_913y923 = {} as ManejoBosqueDetalleModel;
    this.showModal_913y923 = false;
    this.det_modal_931y932 = {} as ManejoBosqueDetalleModel;
    this.showModal_931y932 = false;
    this.edit = false;
    this.tipoTabla = '';
    this.indexUpdate = -1;
  }

  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_9 = Object.assign(this.detEvaluacion_9,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_9));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }

  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_9])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_9);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6277"
    }));


    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
