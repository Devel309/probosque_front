import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { element } from "protractor";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import {
  InformacionBasicaDetalleModelPMFIC,
  InformacionBasicaModelPMFIC,
  ListInformacionBasicaDetSubPMFIC,
} from "src/app/model/InformacionSocieconomicaPMFIC";
import { InformacionBasicaSOPCMervice } from "src/app/service/plan-operativo-concesion-maderable/informacion-basica.service";
import { ModalFormularioAntecedentesComponent } from "../../modal/modal-formulario-antecedentes/modal-formulario-antecedentes.component";
import { ModalFormularioInfraestructuraComponent } from "../../modal/modal-formulario-infraestructura/modal-formulario-infraestructura.component";
import { ModalFormularioOtrosComponent } from "./modal/modal-formulario-otros/modal-formulario-otros.component";
import { ModalFormularioSocieconomicaComponent } from "./modal/modal-formulario-socieconomica/modal-formulario-socieconomica.component";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigoPMFIC } from "../../../../../model/util/PMFIC/CodigoPMFIC";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-informacion-socioeconomica",
  templateUrl: "./tab-informacion-socioeconomica.component.html",
})
export class TabInformacionSocioeconomicaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;
  ref: DynamicDialogRef = new DynamicDialogRef();
  notData: boolean = false;
  codSubInfBasica: string = "";

  especies = [];
  listCaracterizacionComunidad: InformacionBasicaModelPMFIC[] = [];
  listInfraestructuraServicios: InformacionBasicaModelPMFIC[] = [];
  listAntecentesUsoIdentificacionConflictos: InformacionBasicaModelPMFIC[] = [];

  isSubmitting$ = this.query.selectSubmitting();

  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_6;

  codigoAcordeon_6: string = CodigoPMFIC.TAB_6;

  detEvaluacion_6: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_6,
  });

  evaluacion: any;

  constructor(
    public dialogService: DialogService,
    private informacionBasicaService: InformacionBasicaSOPCMervice,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private toast: ToastService,
    private createStore: ButtonsCreateStore,
    private query: ButtonsCreateQuery,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit() {
    this.listarInfCaracterizacionComunidad();
    this.listarInfAntecentesUsoIdentificacionConflictos();
    this.listarInfInfraestructuraServicios();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  crearOtrasActividadesCaracterizacionComunidad() {
    const obj = new InformacionBasicaDetalleModelPMFIC();

    const array = this.listCaracterizacionComunidad.filter(
      (data: any) => data.comunidad == "Otros (especificar)"
    )[0].listInformacionBasicaDet;

    array.push(obj);

    // listInformacionBasicaDet.push(obj)
  }

  crearOtraInfraestructura() {
    this.toast.ok("Se agregó una fila al final de la tabla correctamente.");
    const obj = new InformacionBasicaDetalleModelPMFIC();

    this.listInfraestructuraServicios[0].listInformacionBasicaDet.push(obj);
  }

  modalFormAntecedentes(
    mensaje: string,
    antecedentes?: any,
    edit?: any,
    index?: any
  ) {
    this.ref = this.dialogService.open(ModalFormularioAntecedentesComponent, {
      header: mensaje,
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        item: antecedentes,
        edit: edit,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (edit == "true") {
          this.listAntecentesUsoIdentificacionConflictos[index] = resp;
          antecedentes.comunidad = resp.comunidad;
        } else {
          resp.codSubInfBasica = "PMFICISAUEC";
          this.listAntecentesUsoIdentificacionConflictos.push(resp);
        }
      }
    });
  }

  eliminarItemCaracterizacion(event: any, item: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasica == 0) {
          this.toast.ok("Se eliminó el registró correctamente.");
          this.listCaracterizacionComunidad.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: item.idInfBasica,
              codInfBasicaDet: "",
              idInfBasicaDet: 0,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.listCaracterizacionComunidad.splice(index, 1);
            });
        }
      },
      reject: () => {},
    });
  }

  eliminarEspecie(check: any, indexActividad: number, indexEspecie: number) {
    this.listAntecentesUsoIdentificacionConflictos[
      indexActividad
    ].listInformacionBasicaDet.splice(indexEspecie, 1);
  }

  eliminarInfraestructura(
    event: any,
    item: ListInformacionBasicaDetSubPMFIC,
    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasicaDet == 0) {
          this.toast.ok("Se eliminó el registró correctamente.");
          this.listInfraestructuraServicios[0].listInformacionBasicaDet.splice(
            index,
            1
          );
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: "",
              idInfBasicaDet: item.idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.listInfraestructuraServicios[0].listInformacionBasicaDet.splice(
                index,
                1
              );
            });
        }
      },
      reject: () => {},
    });
  }

  modalFormInfraestructura(mensaje: string, infraestructura: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioInfraestructuraComponent,
      {
        header: mensaje,
        width: "50%",
        contentStyle: { "max-height": "500px", overflow: "auto" },
        baseZIndex: 10000,
        data: {
          item: infraestructura,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (edit == "true") {
          infraestructura.nombre = resp.nombre;
          infraestructura.este = resp.este;
          infraestructura.norte = resp.norte;
          infraestructura.observaciones = resp.observaciones;
          infraestructura.editarFormInfraestructura = true;
        } else {
          // this.infraestructura.push(resp);
        }
      }
    });
  }

  eliminarAntecedentes(event: any, item: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasica == 0) {
          this.toast.ok("Se eliminó el registró correctamente.");
          this.listAntecentesUsoIdentificacionConflictos.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: item.idInfBasica,
              codInfBasicaDet: "",
              idInfBasicaDet: 0,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.listAntecentesUsoIdentificacionConflictos.splice(index, 1);
            });
        }
      },
      reject: () => {},
    });
  }

  listarInfCaracterizacionComunidad() {
    this.listCaracterizacionComunidad = [];
    this.notData = false;
    var params = {
      codigoTipoInfBasica: "PMFIC",
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: "PMFICISCC",
    };
    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data[0].map((element: any) => {
            const obj = new InformacionBasicaModelPMFIC(element);

            if (element.listInformacionBasicaDet[0].actividad == "") {
              this.notData = true;
            }
            if (element.idPlanManejo == null) {
              obj.idInfBasica = 0;
              obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                (data: any) => {
                  const obj = new InformacionBasicaDetalleModelPMFIC(data);
                  obj.idInfBasica = 0;
                  obj.idInfBasicaDet = 0;
                  obj.solucion = data.solucion == "S" ? true : false;
                  return obj;
                }
              );
            } else {
              obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                (data: any) => {
                  const obj = new InformacionBasicaDetalleModelPMFIC(data);
                  obj.solucion = data.solucion == "S" ? true : false;
                  return obj;
                }
              );
            }
            this.listCaracterizacionComunidad.push(obj);
          });
        }
      });
  }

  listarInfInfraestructuraServicios() {
    this.listInfraestructuraServicios = [];
    var params = {
      codigoTipoInfBasica: "PMFIC",
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: "PMFICISIS",
    };
    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data[0].forEach((element: any) => {
            const obj = new InformacionBasicaModelPMFIC(element);
            if (element.idPlanManejo == null) {
              obj.idInfBasica = 0;

              obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                (data: any) => {
                  const obj = new InformacionBasicaDetalleModelPMFIC(data);
                  obj.idInfBasica = 0;
                  obj.idInfBasicaDet = 0;
                  obj.solucion = data.solucion == "S" ? true : false;
                  return obj;
                }
              );
            } else {
              obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                (data: any) => {
                  const obj = new InformacionBasicaDetalleModelPMFIC(data);
                  obj.solucion = data.solucion == "S" ? true : false;

                  return obj;
                }
              );
            }

            this.listInfraestructuraServicios.push(obj);
          });
        }
      });
  }

  listarInfAntecentesUsoIdentificacionConflictos() {
    this.listAntecentesUsoIdentificacionConflictos = [];
    var params = {
      codigoTipoInfBasica: "PMFIC",
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: "PMFICISAUEC",
    };
    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data[0].map((element: any) => {
            const obj = new InformacionBasicaModelPMFIC(element);

            if (element.idPlanManejo == null) {
              obj.idInfBasica = 0;
            } else {
              if (
                !!element.listInformacionBasicaDet &&
                element.listInformacionBasicaDet.length > 0
              ) {
                obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                  (data: any) => {
                    const item = new InformacionBasicaDetalleModelPMFIC(data);
                    item.solucion = true;

                    return item;
                  }
                );
              }
            }

            this.listAntecentesUsoIdentificacionConflictos.push(obj);
          });
        }
      });
  }

  registratInformacionSocioeconomica() {
    let contentArray: any = [];
    this.createStore.submit();

    if (this.listCaracterizacionComunidad.length > 0) {
      this.listCaracterizacionComunidad.map((element: any) => {
        const obj = new InformacionBasicaModelPMFIC(element);
        obj.idPlanManejo = this.idPlanManejo;
        obj.idUsuarioRegistro = this.user.idUsuario;

        if (!!element.listInformacionBasicaDet) {
          obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
            (element: any) => {
              const obj = new InformacionBasicaDetalleModelPMFIC(element);
              obj.codInfBasicaDet = "PMFICISCC";
              obj.idPlanManejo = this.idPlanManejo;
              obj.idUsuarioRegistro = this.user.idUsuario;
              obj.solucion = element.solucion ? "S" : "N";
              return obj;
            }
          );
          obj.codNombreInfBasica = obj.listInformacionBasicaDet[0]
            .codSubInfBasicaDet
            ? obj.listInformacionBasicaDet[0].codSubInfBasicaDet
            : "OT";

          if (obj.listInformacionBasicaDet[0].codSubInfBasicaDet == "") {
            obj.listInformacionBasicaDet[0].codSubInfBasicaDet = "OT";
          }
        }
        contentArray.push(obj);
      });
    }
    if (this.listInfraestructuraServicios.length > 0) {
      this.listInfraestructuraServicios.map((element: any) => {
        const obj = new InformacionBasicaModelPMFIC(element);
        obj.codInfBasica = "PMFIC";
        obj.idPlanManejo = this.idPlanManejo;
        obj.idUsuarioRegistro = this.user.idUsuario;
        if (!!element.listInformacionBasicaDet) {
          obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
            (element: any) => {
              const obj = new InformacionBasicaDetalleModelPMFIC(element);
              obj.codInfBasicaDet = "PMFICISIS";
              obj.idPlanManejo = this.idPlanManejo;
              obj.idUsuarioRegistro = this.user.idUsuario;
              obj.solucion = element.solucion ? "S" : "N";
              obj.coordenadaEsteIni = parseFloat(element.coordenadaEsteIni);
              obj.coordenadaNorteIni = parseFloat(element.coordenadaNorteIni);
              return obj;
            }
          );
          obj.codNombreInfBasica =
            obj.listInformacionBasicaDet[0].codSubInfBasicaDet;
        }
        contentArray.push(obj);
      });
    }
    if (this.listAntecentesUsoIdentificacionConflictos.length > 0) {
      this.listAntecentesUsoIdentificacionConflictos.map((element: any) => {
        const obj = new InformacionBasicaModelPMFIC(element);

        obj.codInfBasica = "PMFIC";
        obj.codNombreInfBasica = "AIC";
        obj.idPlanManejo = this.idPlanManejo;
        obj.idUsuarioRegistro = this.user.idUsuario;
        if (element.listInformacionBasicaDet.length > 0) {
          obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
            (det: any) => {
              const obj = new InformacionBasicaDetalleModelPMFIC(det);
              obj.codInfBasicaDet = "PMFICISAUEC";
              obj.idPlanManejo = this.idPlanManejo;
              obj.idUsuarioRegistro = this.user.idUsuario;
              return obj;
            }
          );
        }

        contentArray.push(obj);
      });
    }

    if (!!contentArray) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionBasicaService
        .registrarInformacionBasicaDetalle(contentArray)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.createStore.submitSuccess();
            this.toast.ok(
              "Se registró la información socioeconómica correctamente."
            );
            this.listarInfCaracterizacionComunidad();
            this.listarInfAntecentesUsoIdentificacionConflictos();
            this.listarInfInfraestructuraServicios();
          }
        });
    }
  }

  openModal() {
    this.notData = false;
    this.codSubInfBasica = "";
    this.ref = this.dialogService.open(ModalFormularioOtrosComponent, {
      header: "Agregar",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        lista: this.listCaracterizacionComunidad,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        

        if (resp.tipo == "Actividades principales") {
          const obj = new InformacionBasicaDetalleModelPMFIC();
          obj.actividad = resp.actividad;
          if (resp.tipoPoblacion == "Comunidad Nativa") {
            this.codSubInfBasica = "CN";
          } else if (resp.tipoPoblacion == "Comunidad campesina") {
            this.codSubInfBasica = "CC";
          } else if (resp.tipoPoblacion == "Caserio de agricultores") {
            this.codSubInfBasica = "CA";
          } else if (resp.tipoPoblacion == "Parceleros individuales") {
            this.codSubInfBasica = "PI";
          } else if (resp.tipoPoblacion == "Extractores") {
            this.codSubInfBasica = "EX";
          } else {
            this.codSubInfBasica = "OT";
          }
          obj.idInfBasicaDet = 0;
          obj.codSubInfBasicaDet = this.codSubInfBasica;
          const array = this.listCaracterizacionComunidad.filter(
            (data: any) => data.comunidad == resp.tipoPoblacion
          )[0].listInformacionBasicaDet;
          array.push(obj);
        } else {
          const obj = new InformacionBasicaModelPMFIC();
          obj.comunidad = resp.poblacion;
          obj.codInfBasica = "PMFIC";
          obj.codSubInfBasica = "PMFICISCC";
          obj.codNombreInfBasica = "OT";
          this.listCaracterizacionComunidad.push(obj);
        }
      }
    });
  }

  openModalEditar(data: any) {
    this.notData = false;
    this.ref = this.dialogService.open(ModalFormularioSocieconomicaComponent, {
      header: "Editar",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        lista: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
      }
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_6 = Object.assign(
                this.detEvaluacion_6,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_6
                )
              );
            }
          } else {
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.detEvaluacion_6])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_6);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion() {
    localStorage.setItem(
      "EvalResuDet",
      JSON.stringify({
        tab: "RFNM",
        acordeon: "",
      })
    );
    this.router.navigateByUrl(
      "/planificacion/evaluacion/requisitos-previos/" +
        this.idPlanManejo +
        "/" +
        this.codigoProceso
    );
  }

  regresarTab() {
    this.regresar.emit();
  }
}
