import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ListInformacionBasicaDetSubPMFIC } from "src/app/model/InformacionSocieconomicaPMFIC";
import { InformacionBasicaSOPCMervice } from "src/app/service/plan-operativo-concesion-maderable/informacion-basica.service";

@Component({
  selector: "app-modal-formulario-socieconomica",
  templateUrl: "./modal-formulario-socieconomica.component.html",
  styleUrls: ["./modal-formulario-socieconomica.component.scss"],
})
export class ModalFormularioSocieconomicaComponent implements OnInit {
  context: any = {
    actividad: "",
    tipoPoblacion: "",
    tipo:"",
    poblacion:""
  };
  caracterizacion: any = [];

  cmbActividad: any = [ 
    {label:'Tipo de población'},
    {label:'Actividades principales'}
  ]
  ;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private user: UsuarioService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private informacionBasicaService: InformacionBasicaSOPCMervice,
  ) {}

  ngOnInit(): void {
    this.caracterizacion = this.config.data.lista;
  }
  eliminarItemCaracterizacion(
    event: any,
    item: ListInformacionBasicaDetSubPMFIC,
    indexPoblacion: number,
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasicaDet == 0) {
          this.toast.ok("Se eliminó el registró correctamente.");
          this.caracterizacion.listInformacionBasicaDet.splice(indexPoblacion, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: "",
              idInfBasicaDet: item.idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.caracterizacion.listInformacionBasicaDet.splice(indexPoblacion, 1);
            });
        }
      },
      reject: () => {},
    });
  }

  agregar = () => {
    this.ref.close(this.context);
  };

  cerrarModal() {
    this.ref.close();
  }
}
