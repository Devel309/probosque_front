import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { ActividadComponent } from "./modal/actividad/actividad.component";
import { ContingenciaComponent } from "./modal/contingencia/contingencia.component";
import { ImpactoComponent } from "./modal/impacto/impacto.component";
import { EvaluacionAmbientalTabla } from "./evaluacion-ambiental-tabla/evaluacion-ambiental.tabla";
import { ArchivoService, UsuarioService } from "@services";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { descargarArchivo, ToastService } from "@shared";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { FormActividadComponent } from "./modal/form-actividad/form-actividad.component";
import { FormActividadVigilanciaComponent } from "./modal/form-actividad-vigilancia/form-actividad-vigilancia.component";
import {environment} from '@env/environment';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {Router} from '@angular/router';
import { UrlFormatos } from "src/app/model/urlFormatos";
@Component({
  selector: "app-tab-evaluacion-ambiental",
  templateUrl: "./tab-evaluacion-ambiental.component.html",
  styleUrls: ["./tab-evaluacion-ambiental.component.scss"],
})
export class TabEvaluacionAmbientalComponent implements OnInit {
  @Input() idPlanManejo: any;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() isPerfilArffs!: boolean;
  @ViewChild(EvaluacionAmbientalTabla) tablaDinamica!: EvaluacionAmbientalTabla;

  ref!: DynamicDialogRef;

  listaContingencia: any[] = [];
  accept: string = "application/msword,application/pdf";

  listaCatalogoContingencia: any[] = [
    { valor1: "Incendios", codigo: "CONTIINC" },
    {
      valor1: "Derrame de conbustible  y/o lubricantes",
      codigo: "CONTIDER",
    },
    {
      valor1: "Riesgos a la salud",
      codigo: "CONTIRIE",
    },
    { valor1: "Invasiones", codigo: "CONTIINV" },
    { valor1: "Poblaciones no contactadas", codigo: "CONTIPOB" },
    { valor1: "Otros", codigo: "CONTIOTRO" },
  ];

  nombrefile = "";

  factor: any;
  actividades: any[] = [];
  actividades1: any[] = [];
  actividades2: any[] = [];

  selectAnexo: string | null = null;
  nombreArchivoAnexo: string = "";
  idPlanManejoArchivo: number = 0;
  CodigoPMFIC = CodigoPMFIC;

  archivoPGA: any;

  archivo: any;

  codTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";
  subCodTipoAprovechamiento: string = "";

  isPoblacionesNoContactadas: boolean = false;

  totalRecordsACPC: number = 0;
  totalRecordsAVS: number = 0;
  totalRecordsCont: number = 0;

  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_10;

  codigoAcordeon_10: string = CodigoPMFIC.TAB_10;

  detEvaluacion_10: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_10,
  });

  evaluacion: any;

  nombreGenerado: string = "";
  plantillaPMFIC: string = "";
  isLoading: boolean =  false

  constructor(
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private user: UsuarioService,
    private postulacionPFDMService: PostulacionPFDMService,
    private messageService: MessageService,
    private toast: ToastService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private evaluacionService: EvaluacionService,private router: Router,
    private archivoService: ArchivoService
    ) {
    this.archivo = {
      idEvalActividad: 0,
      codTipoActividad: "PMFIC",
      tipoActividad: "ARCHIVO",
      tipoNombreActividad: "",
      nombreActividad: "",
      file: null,
    };
    this.archivoPGA = {
      file: null,
    };
  }

  ngOnInit(): void {
    this.factoresAmbientales();
    this.getContingencia();
    this.getArchivo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();


  }

  getArchivo() {
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad({
        idPlanManejo: this.idPlanManejo,
        tipoActividad: "ARCHIVO",
      })
      .subscribe((res: any) => {
        if (res?.success && res?.data?.length > 0) {
          this.archivo = res.data;
          this.archivo.file = this.archivo.nombreActividad;
        }
      });
  }

  onList(list: any) {
    this.actividades = [];
    this.actividades = [...list];

    this.actividades1 = [];
    this.totalRecordsACPC = 0;
    this.actividades.forEach((item: any) => {
      if (item.tipoAprovechamiento != 'CONTIN') {
        this.totalRecordsACPC += 1;
        this.actividades1.push(item);
      }
    });

    this.actividades2 = [];
    this.totalRecordsAVS = 0;
    this.actividades.forEach((item: any) => {
      if (!!item.impacto  || !!item.medidasControl) {
        this.totalRecordsAVS += 1;
        this.actividades2.push(item);
      }
    });
  }

  openEliminarRegistroActividades(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message:
        "Al eliminar este registro, se eliminara la actividad" +
        " " +
        data.nombreAprovechamiento +
        " " +
        "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.actividades2.splice(index, 1);
                this.toast.ok("Se eliminó la información correctamente.");
                this.tablaDinamica.getData();
              }
            });
        } else {
          this.actividades2.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  modalActividadPreventivo = () => {
    this.ref = this.dialogService.open(FormActividadComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Pre Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "PREAPR";
          this.subCodTipoAprovechamiento = "SUBPREAPR";
          this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
        }

        if (resp.tipo == "Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "APROVE";
          this.subCodTipoAprovechamiento = "SUBAPROVE";
          this.tipoNombreAprovechamiento = "Aprovechamiento";
        }
        if (resp.tipo == "Post Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "POSTAP";
          this.subCodTipoAprovechamiento = "SUBPOSTAP";
          this.tipoNombreAprovechamiento = "Post Aprovechamiento";
        }

        if (resp.tipo == "Otros") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "OTROS";
          this.subCodTipoAprovechamiento = "SUBOTROS";
          this.tipoNombreAprovechamiento = "Otros";
        }
        // console.log("response", resp);
        const params = {
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp.actividad,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .pipe(finalize(() => this.isLoading = true))
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  openEliminarPreventivoCorrector(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.actividades1.splice(index, 1);
                this.toast.ok("Se eliminó la información correctamente.");
                this.tablaDinamica.getData();
              }
            });
        } else {
          this.actividades1.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  factoresAmbientales() {
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad({ tipoActividad: "FACTOR" })
      .subscribe((response: any) => {
        this.factor = response.data;
      });
  }

  modalActividad = (data: any) => {
    this.ref = this.dialogService.open(ActividadComponent, {
      header: "Editar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        // console.log("response", resp);
        const params = {
          idEvalAprovechamiento: data.idEvalAprovechamiento,
          codTipoAprovechamiento: data.codTipoAprovechamiento,
          tipoAprovechamiento: data.tipoAprovechamiento,
          tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
          nombreAprovechamiento: data.nombreAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsable: resp.responsable,
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  modalImpacto = (data: any) => {
    this.ref = this.dialogService.open(ImpactoComponent, {
      header: "Editar Impacto",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        const params = {
          idEvalAprovechamiento: data.idEvalAprovechamiento,
          codTipoAprovechamiento: data.codTipoAprovechamiento,
          tipoAprovechamiento: data.tipoAprovechamiento,
          tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
          nombreAprovechamiento: data.nombreAprovechamiento,
          impacto: data.impacto,
          medidasControl: data.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPlanManejo,
          responsable: resp.responsable,
          idUsuarioRegistro: this.user.idUsuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de vigilancia y seguimiento ambiental correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  openEliminarRegistroContingencia(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.getContingencia();
                this.toast.ok("Se eliminó la Contingencia correctamente.");
              }
            });
        } else {
          this.listaContingencia.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  modalContingencia = (mesaje: string, edit?: any, data?: any) => {
    let idEvalAprovechamiento: number = 0;
    if (edit == "true") {
      idEvalAprovechamiento = data.idEvalAprovechamiento;
    }
    this.ref = this.dialogService.open(ContingenciaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        editar: edit,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        

        const params = {
          idEvalAprovechamiento,
          codTipoAprovechamiento: CodigoPMFIC.CODIGO_PROCESO,
          tipoAprovechamiento: "CONTIN",
          tipoNombreAprovechamiento: !!resp.otra
            ? resp.otra
            : edit == "true"
            ? this.listaCatalogoContingencia.filter(
                (data: any) => data.valor1 == resp.contingencia
              )[0].codigo
            : resp.contingencia,
          nombreAprovechamiento:
            edit == "true"
              ? resp.contingencia
              : this.listaCatalogoContingencia.filter(
                  (data: any) => data.codigo == resp.contingencia
                )[0].valor1,
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: resp.accionesRealizar,
          responsable: "",
          responsableSecundario: resp.responsable,
          contingencia: resp.otra
            ? resp.otra
            : edit == "true"
            ? resp.contingencia
            : this.listaCatalogoContingencia.filter(
                (data: any) => data.codigo == resp.contingencia
              )[0].valor1,
          auxiliar: this.selectAnexo,
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe(() => this.getContingencia(edit));
      }
    });
  };

  modalActividadVigilancia = () => {
    this.ref = this.dialogService.open(FormActividadVigilanciaComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Pre Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "PREAPR";
          this.subCodTipoAprovechamiento = "SUBPREAPR";
          this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
        }

        if (resp.tipo == "Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "APROVE";
          this.subCodTipoAprovechamiento = "SUBAPROVE";
          this.tipoNombreAprovechamiento = "Aprovechamiento";
        }
        if (resp.tipo == "Post Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "POSTAP";
          this.subCodTipoAprovechamiento = "SUBPOSTAP";
          this.tipoNombreAprovechamiento = "Post Aprovechamiento";
        }
        if (resp.tipo == "Otros") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "OTROS";
          this.subCodTipoAprovechamiento = "SUBOTROS";
          this.tipoNombreAprovechamiento = "Otros";
        }

        // console.log("response", resp);
        const params = {
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp.actividad,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsable: resp.responsable,
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  getContingencia(edit?: any) {
    this.isPoblacionesNoContactadas = false;
    this.listaContingencia = [];
    const body = { idPlanManejo: this.idPlanManejo, tipoAprovechamiento: "CONTIN" };
    this.planificacionService
      .getAprovechamiento(body)
      .subscribe((response: any) => {
        if (response.success) {
          this.totalRecordsCont = response.data.length;
          response.data.forEach((element: any) => {
            if (element.tipoNombreAprovechamiento === "CONTIPOB") {
              this.isPoblacionesNoContactadas = true;
            }
            this.selectAnexo = element.auxiliar;
            this.listaContingencia.push(element);
          });
        }
      });
  }

  guardar() {
    if (!this.validarNuevo()) {
      return;
    }
    let item = this.listaContingencia.length;
    let obj = this.listaContingencia[item - 1];
    obj.auxiliar = this.selectAnexo;
    // console.log("se guardar", obj);
    this.planificacionService
      .registrarAprovechamientoEvalAmbiental(obj)
      .subscribe(() => {
        this.toast.ok(
          "Se registró plan de contingencia ambiental correctamente.\n"
        );
        this.getContingencia();
      });
  }

  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectAnexo == "S") {
      if (this.idPlanManejoArchivo == 0) {
        validar = false;
        mensaje = mensaje += "(*) Debe Cargar el archivo requerido.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  eliminarContingencia = (item: any) => {
    this.listaContingencia = this.listaContingencia.filter(
      (x) => x.idContigencia != item.idContigencia
    );
  };

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  /*  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  } */

  varAssets = `${environment.varAssets}`;
  /* btnDescargarFormatoPGA() {

    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla formato Plan de Gestion Ambiental.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla formato Plan de Gestion Ambiental.xlsx';
    }

    window.location.href = urlExcel;

  } */

  btnDescargarFormato() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.nombreGenerado = UrlFormatos.CARGA_PMFIC_EVALUACION;
    this.archivoService
      .descargarPlantilla(this.nombreGenerado)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess == true) {
          this.toast.ok(res?.message);
          this.plantillaPMFIC = res;
          descargarArchivo(this.plantillaPMFIC);
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  btnCargarPGA() {
    if (!(this.archivoPGA.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    let item = {
      nombreHoja: "DATOS",
      numeroFila: 2,
      numeroColumna: 1,
      idPlanManejo: this.idPlanManejo,
      codTipoAprovechamiento: "PMFIC",
      tipoAprovechamiento: "",
      tipoNombreAprovechamiento: "",
      idUsuarioRegistro: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarPlanGestionExcel(
        this.archivoPGA.file,
        item.nombreHoja,
        item.numeroFila,
        item.numeroColumna,
        item.idPlanManejo,
        item.codTipoAprovechamiento,
        item.tipoAprovechamiento,
        item.tipoNombreAprovechamiento,
        item.idUsuarioRegistro
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok(
            "Se registraron los Planes de Gestión Ambiental correctamente.\n"
          );
          this.getArchivo();
          this.getContingencia();
          this.tablaDinamica.getData();
          this.archivoPGA.file = "";
        } else {
          this.toast.error("Ocurrió un error.");
        }
      });
  }

  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_10 = Object.assign(this.detEvaluacion_10,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_10));
            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_10])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_10);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6210"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
