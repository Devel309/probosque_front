import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-impacto",
  templateUrl: "./impacto.component.html",
  styleUrls: ["./impacto.component.scss"],
})
export class ImpactoComponent implements OnInit {
  autoResize: boolean = true;
  impactoObjt: any = {
    descripcionImpacto: "",
    medidasControl: "",
    medidasMonitoreo: "",
    frecuencia: "",
    responsable: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {
    this.impactoObjt.descripcionImpacto = this.config.data.data.impacto;
    this.impactoObjt.medidasControl = this.config.data.data.medidasControl;
    this.impactoObjt.medidasMonitoreo = this.config.data.data.medidasMonitoreo;
    this.impactoObjt.frecuencia = this.config.data.data.frecuencia;
    this.impactoObjt.responsable = this.config.data.data.responsable;
  }

  agregar = () => {
    if (this.impactoObjt.medidasMonitoreo === "") {
      this.toast.warn("(*) Debe ingresar: Medida de Monitoreo.\n");
    } else if (this.impactoObjt.frecuencia === "") {
      this.toast.warn("(*) Debe ingresar: Frecuencia.\n");
    } else if (this.impactoObjt.responsable === "") {
      this.toast.warn("(*) Debe ingresar: Responsable.\n");
    } else this.ref.close(this.impactoObjt);
  };

  cerrarModal() {
    this.ref.close();
  }
}
