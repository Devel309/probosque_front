import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { MessageService, SelectItem, SelectItemGroup } from "primeng/api";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  DetalleObjetivo,
  MaderablesNoMaderablesResponse,
} from "src/app/model/ObjetivoManejoEspecifico";
import { ObjetivosService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/objetivos.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { ObjetivoManejoModel } from "../../../../../model/ObjetivoManejo";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { CodigoPMFIC } from "../../../../../model/util/PMFIC/CodigoPMFIC";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {Router} from '@angular/router';
@Component({
  selector: "app-tab-objetivos-manejo",
  templateUrl: "./tab-objetivos-manejo.component.html",
  styleUrls: ["./tab-objetivos-manejo.component.scss"],
  providers: [MessageService],
})
export class TabObjetivosManejoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled: boolean = false;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  objetoManejo = {} as ObjetivoManejoModel;

  listObjeto: any[] = [];

  pendiente: boolean = false;
  listaEditar: any[] = [];

  listMaderables: MaderablesNoMaderablesResponse[] = [];
  listNoMaderables: MaderablesNoMaderablesResponse[] = [];

  listMaderablesMostrar: SelectItem[] = [];
  listNoMaderablesMostrar: SelectItem[] = [];

  listMaderablesSeleccionados: MaderablesNoMaderablesResponse[] = [];
  listNoMaderablesSeleccionados: MaderablesNoMaderablesResponse[] = [];

  objRegistro: DetalleObjetivo = new DetalleObjetivo();
  listRegistrar: DetalleObjetivo[] = [];

  listMaderableNoMaderable: SelectItemGroup[] = [];

  listaCompleta: DetalleObjetivo[] = [];

  idObjManejo: number = 0;

  groupedOjetivos: SelectItemGroup[] = [];


  justificacion: string = "";
  idPlanManejoArchivo: number = 0;
  selectAnexo: string = "N";

  CodigoPMFIC = CodigoPMFIC;
  isSubmitting$ = this.query.selectSubmitting();

  accept: string = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf';


  // nuevo objetivo

  parametro = {} as MaderablesNoMaderablesResponse;
  displayBasic: boolean = false;
  tituloModal: string = "Agregar Objetivo";
  listParametro = [
    { tipoLabel: "Maderable", tipoValue: "Maderable" },
    { tipoLabel: "No Maderable", tipoValue: "No Maderable" },
  ];

  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_2;

  codigoAcordeon_2: string = CodigoPMFIC.TAB_2;

  detEvaluacion_2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacion: this.codigoProceso,
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_2,
  });

  evaluacion: any;

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private objetivosService: ObjetivosService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private postulacionPFDMService: PostulacionPFDMService,
    private createStore: ButtonsCreateStore,
    private query: ButtonsCreateQuery,private router: Router
  ) {}

  ngOnInit(): void {
    this.listarObjetivosPlanManejo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarObjetivos() {
    this.listMaderables = [];
    this.listNoMaderables = [];
    this.listMaderablesMostrar = [];
    this.listNoMaderablesMostrar = [];
    this.listMaderableNoMaderable = [];
    this.listObjeto = [];
    let params = {
      codigoObjetivo: "PMFIC",
      idPlanManejo: 0,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.objetivosService
      .listarObjetivo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.detalle == "Maderable") {
            this.listMaderables.push({ ...element });
            this.listMaderablesMostrar.push({
              label: element.descripcionDetalle,
              value: element.descripcionDetalle,
            });
          }
          if (element.detalle == "No Maderable") {
            this.listNoMaderables.push(element);
            this.listNoMaderablesMostrar.push({
              label: element.descripcionDetalle,
              value: element.descripcionDetalle,
            });
          }
        });
        this.listMaderableNoMaderable.push({
          label: "Maderable",
          value: "Maderable",
          items: this.listMaderablesMostrar,
        });
        this.listMaderableNoMaderable.push({
          label: "No Maderable",
          value: "No Maderable",
          items: this.listNoMaderablesMostrar,
        });
        this.listas();
      });
  }

  listas() {
    this.groupedOjetivos = [];
    this.listMaderableNoMaderable.forEach((element) => {
      this.groupedOjetivos.push(element);
    });
  }

  marca() {
    this.listObjeto = [];
    this.listNoMaderables.forEach((item) => {
      if (item.activo == "A") {
        this.listObjeto.push(item.descripcionDetalle);
      }
    });

    this.listMaderables.forEach((item) => {
      if (item.activo == "A") {
        this.listObjeto.push(item.descripcionDetalle);
      }
    });
  }

  listarObjetivosPlanManejo() {
    this.listMaderables = [];
    this.listNoMaderables = [];
    this.listMaderablesMostrar = [];
    this.listNoMaderablesMostrar = [];
    this.listMaderableNoMaderable = [];
    this.listObjeto = [];
    let params = {
      codigoObjetivo: "PMFIC",
      idPlanManejo: this.idPlanManejo,
    };

    this.objetivosService.listarObjetivo(params).subscribe((response: any) => {
      if (response.data.length != 0) {
        response.data.forEach((element: any) => {
          this.objetoManejo.general = element.descripcion;
          this.objetoManejo.detalle = element.detalleCabera;
          this.idObjManejo = element.idObjManejo;
          this.selectAnexo = element.observacionCabera;

          if (element.detalle == "Maderable") {
            this.listMaderables.push(element);
            this.listMaderablesMostrar.push({
              label: element.descripcionDetalle,
              value: element.descripcionDetalle,
            });
          }
          if (element.detalle == "No Maderable") {
            this.listNoMaderables.push(element);
            this.listNoMaderablesMostrar.push({
              label: element.descripcionDetalle,
              value: element.descripcionDetalle,
            });
          }
        });
        this.listMaderableNoMaderable.push({
          label: "Maderable",
          value: "Maderable",
          items: this.listMaderablesMostrar,
        });
        this.listMaderableNoMaderable.push({
          label: "No Maderable",
          value: "No Maderable",
          items: this.listNoMaderablesMostrar,
        });
        this.listas();
        this.marca();
      } else {
        this.listarObjetivos();
      }
    });
  }

  listar() {
    this.listObjeto.forEach((elemen) => {
      this.filtrarIgualesMaderables(elemen);
      this.filtrarIgualesNoMaderables(elemen);
    });

    this.crearObjeto();
  }

  filtrarIgualesMaderables(element: string) {
    this.listaEditar = this.listMaderables.filter(
      (x) => x.descripcionDetalle == element
    );

    this.listaEditar
      .map((data) => {
        return {
          ...data,
          activo: "A",
        };
      })
      .forEach((item) => {
        this.listMaderablesSeleccionados.push(item);
      });
  }

  filtrarIgualesNoMaderables(element: string) {
    this.listaEditar = this.listNoMaderables.filter(
      (x) => x.descripcionDetalle == element
    );

    this.listaEditar.forEach((item) => {
      this.listNoMaderablesSeleccionados.push(item);
    });
  }

  generarObjetoListado() {
    this.listRegistrar = [];

    this.listMaderables = this.listMaderables.map((data) => {
      return {
        activo: "I",
        codigoObjetivo: data.codigoObjetivo,
        descripcion: data.descripcion,
        descripcionDetalle: data.descripcionDetalle,
        detalle: data.detalle,
        estado: data.estado,
        fechaElimina: data.fechaElimina,
        fechaModificacion: data.fechaModificacion,
        fechaRegistro: data.fechaRegistro,
        idObjManejo: data.idObjManejo,
        idObjetivoDet: data.idObjetivoDet,
        idPlanManejo: data.idPlanManejo,
        idUsuarioElimina: data.idUsuarioElimina,
        idUsuarioModificacion: data.idUsuarioModificacion,
        idUsuarioRegistro: data.idUsuarioRegistro,
        pageNum: data.pageNum,
        pageSize: data.pageSize,
        search: data.search,
        startIndex: data.startIndex,
        totalPage: data.totalPage,
        totalRecord: data.totalRecord,
      };
    });

    this.listNoMaderables = this.listNoMaderables.map((data) => {
      return {
        activo: "I",
        codigoObjetivo: data.codigoObjetivo,
        descripcion: data.descripcion,
        descripcionDetalle: data.descripcionDetalle,
        detalle: data.detalle,
        estado: data.estado,
        fechaElimina: data.fechaElimina,
        fechaModificacion: data.fechaModificacion,
        fechaRegistro: data.fechaRegistro,
        idObjManejo: data.idObjManejo,
        idObjetivoDet: data.idObjetivoDet,
        idPlanManejo: data.idPlanManejo,
        idUsuarioElimina: data.idUsuarioElimina,
        idUsuarioModificacion: data.idUsuarioModificacion,
        idUsuarioRegistro: data.idUsuarioRegistro,
        pageNum: data.pageNum,
        pageSize: data.pageSize,
        search: data.search,
        startIndex: data.startIndex,
        totalPage: data.totalPage,
        totalRecord: data.totalRecord,
      };
    });

    this.listMaderables.forEach(
      (response: MaderablesNoMaderablesResponse, index: number) => {
        this.listMaderablesSeleccionados.forEach((item) => {
          if (response.descripcionDetalle === item.descripcionDetalle) {
            const data = {
              activo: "A",
              codigoObjetivo: response.codigoObjetivo,
              descripcion: response.descripcion,
              descripcionDetalle: response.descripcionDetalle,
              detalle: response.detalle,
              estado: response.estado,
              fechaElimina: response.fechaElimina,
              fechaModificacion: response.fechaModificacion,
              fechaRegistro: response.fechaRegistro,
              idObjManejo: response.idObjManejo,
              idObjetivoDet: response.idObjetivoDet,
              idPlanManejo: response.idPlanManejo,
              idUsuarioElimina: response.idUsuarioElimina,
              idUsuarioModificacion: response.idUsuarioModificacion,
              idUsuarioRegistro: response.idUsuarioRegistro,
              pageNum: response.pageNum,
              pageSize: response.pageSize,
              search: response.search,
              startIndex: response.startIndex,
              totalPage: response.totalPage,
              totalRecord: response.totalRecord,
            };

            const obj = new DetalleObjetivo(data);
            obj.idUsuarioRegistro = this.user.idUsuario;
            obj.idObjetivo = this.idObjManejo;
            if (this.idObjManejo === 0) {
              obj.idObjEspecificoManejo = 0;
            }
            this.listRegistrar.push(obj);
            this.listMaderables.splice(index, 1, data);
          }
        });
      }
    );

    this.listNoMaderables.forEach((response: any, index: number) => {
      this.listNoMaderablesSeleccionados.forEach((item) => {
        if (response.descripcionDetalle == item.descripcionDetalle) {
          const data = {
            activo: "A",
            codigoObjetivo: response.codigoObjetivo,
            descripcion: response.descripcion,
            descripcionDetalle: response.descripcionDetalle,
            detalle: response.detalle,
            estado: response.estado,
            fechaElimina: response.fechaElimina,
            fechaModificacion: response.fechaModificacion,
            fechaRegistro: response.fechaRegistro,
            idObjManejo: response.idObjManejo,
            idObjetivoDet: response.idObjetivoDet,
            idPlanManejo: response.idPlanManejo,
            idUsuarioElimina: response.idUsuarioElimina,
            idUsuarioModificacion: response.idUsuarioModificacion,
            idUsuarioRegistro: response.idUsuarioRegistro,
            pageNum: response.pageNum,
            pageSize: response.pageSize,
            search: response.search,
            startIndex: response.startIndex,
            totalPage: response.totalPage,
            totalRecord: response.totalRecord,
          };

          const obj = new DetalleObjetivo(data);
          obj.idUsuarioRegistro = this.user.idUsuario;
          obj.idObjetivo = this.idObjManejo;
          if (this.idObjManejo === 0) {
            obj.idObjEspecificoManejo = 0;
          }
          this.listRegistrar.push(obj);
          this.listNoMaderables.splice(index, 1, data);
        }
      });
    });
  }

  crearObjeto() {
    if (this.idObjManejo != 0) {
      this.generarObjetoListado();
    } else {
      this.generarObjetoListado();
    }
  }

  registrarObjetoManejo() {
    this.listar();
    this.ObjetosInactivos();
    this.createStore.submit();

    let params = [
      {
        idObjManejo: this.idObjManejo ? this.idObjManejo : 0,
        idPlanManejo: this.idPlanManejo,
        codigoObjetivo: "PMFIC",
        general: this.objetoManejo.general,
        detalle: this.objetoManejo.detalle,
        observacion: this.selectAnexo,
        idUsuarioRegistro: this.user.idUsuario,
        listDetalle: [...this.listRegistrar],
      },
    ];

    /* this.dialog.open(LoadingComponent, { disableClose: true });
    this.objetivosService
      .registrarObjetivo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          // this.toast.ok(res?.message);
          this.toast.ok('Se registró los objetivos de manejo correctamente.');
          this.pendiente = false;
          this.listarObjetivosPlanManejo();
        } else {
          this.toast.error(res?.message);
        }
      }); */
    return this.objetivosService
      .registrarObjetivo(params)
      .pipe(
        tap(
          (response) => {
            this.createStore.submitSuccess();
            this.toast.ok("Se registró los objetivos de manejo correctamente.");
            this.pendiente = false;
            this.listarObjetivosPlanManejo();
          },
          (error) => {
            this.createStore.submitError(error);
            this.toast.ok(error.message);
          }
        )
      )
      .subscribe();
  }

  ObjetosInactivos() {
    if (this.idObjManejo != 0) {
      this.listMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
      this.listNoMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
    } else {
      this.listMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.idObjEspecificoManejo = 0;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
      this.listNoMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.idObjEspecificoManejo = 0;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
    }
  }

  onChange(event: any) {
    this.pendiente = true;
  }

  GuardarObjetoManejo() {
    if (!this.validarObjetoManejo()) {
      return;
    }
    this.registrarObjetoManejo();
  }

  validarObjetoManejo(): boolean {

    let validar: boolean = true;
    let mensaje: string = "";

    if (this.objetoManejo.general == null || this.objetoManejo.general == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Objetivo General.\n";
    }

    if (this.selectAnexo == 'S' && (this.objetoManejo.detalle == null || this.objetoManejo.detalle == "")) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Justificación.\n";
    }

    if (this.listObjeto == null || this.listObjeto.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Objetivos Específicos.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;

  }



  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  abrirModal() {
    this.parametro = {} as MaderablesNoMaderablesResponse;
    this.displayBasic = true;
  }

  agregarObjetivo() {
    if (!this.validarNuevo()) {
      return;
    }
    if (this.parametro.detalle == "Maderable") {
      this.listMaderables.push(this.parametro);
      this.listMaderablesMostrar.push({
        label: this.parametro.descripcionDetalle,
        value: this.parametro.descripcionDetalle,
      });
    }
    if (this.parametro.detalle == "No Maderable") {
      this.listNoMaderables.push(this.parametro);
      this.listNoMaderablesMostrar.push({
        label: this.parametro.descripcionDetalle,
        value: this.parametro.descripcionDetalle,
      });
    }
    this.listMaderableNoMaderable = [];
    this.listMaderableNoMaderable.push({
      label: "Maderable",
      value: "Maderable",
      items: this.listMaderablesMostrar,
    });
    this.listMaderableNoMaderable.push({
      label: "No Maderable",
      value: "No Maderable",
      items: this.listNoMaderablesMostrar,
    });
    this.listas();
    this.marca();
    this.parametro = {} as MaderablesNoMaderablesResponse;
    this.displayBasic = false;
  }

  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.parametro.detalle == null || this.parametro.detalle == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar Tipo.\n";
    }
    if (
      this.parametro.descripcionDetalle == null ||
      this.parametro.descripcionDetalle == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Objetivo.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  cerrarModal() {
    this.parametro = {} as MaderablesNoMaderablesResponse;
    this.displayBasic = false;
  }




  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_2 = Object.assign(this.detEvaluacion_2, this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_2));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_2])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_2);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }


  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP626"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

}

