import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { InformacionBasicaDetalle } from "src/app/model/InformacionAreaManejo";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { MapApi } from "src/app/shared/mapApi";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, PGMFArchivoTipo, ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { TabInformacionBasicaComponent } from "../../tab-informacion-basica.component";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { ConfirmationService, MessageService } from "primeng/api";
import { concatMap, finalize, tap } from "rxjs/operators";
import { ZonaModel } from "src/app/model/dema/zonificacion/ZonaModel";
import { ModalFormularioZonasComponent } from "src/app/web/planificacion/generacion-declaracion-manejo-dema/tabs/tab-zonificacion-ordenamiento-interno-area/modal/modal-formulario-zonas/modal-formulario-zonas.component";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ZonificacionOrdenamientoInternoAreaService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/zonificacion-ordenamiento-interno-area.service";
import { CustomCapaModel } from "src/app/model/util/CustomCapa";
import { FileModel } from "src/app/model/util/File";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { PlanManejoGeometriaModel } from "src/app/model/PlanManejo/PlanManejoGeometria";
import { forkJoin, from } from "rxjs";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { HttpErrorResponse } from "@angular/common/http";
import {InformacionBasicaDetalleModelPMFIC} from '../../../../../../../model/InformacionSocieconomicaPMFIC';

@Component({
  selector: "tabla-area-zonificacion",
  templateUrl: "./tabla-area-zonificacion.component.html",
  styleUrls: ["./tabla-area-zonificacion.component.scss"],
})
export class TablaAreaZonificacionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoProceso: string =  "PMFIC";
  @Input() codigoSubSeccion: string ="PMFICINFBAZIUMF";
  @Input() tipoGeometria: string ="TPZONA";
  @Input() idInfBasica!: number;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;
  @Input() totalAreaZonificacion!: number;

  CodigoPMFIC = CodigoPMFIC;

  zona: ZonaModel = {} as ZonaModel;
  listZona: ZonaModel[] = {} as ZonaModel[];
  ref!: DynamicDialogRef;
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  totalAreaZona!: number;
  totalPorcentajeZona!: number;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  _filesSHP: any = [];

  listaZonificacion :InformacionBasicaDetalleModelPMFIC [] = [];

  det_modal_3_3 = {} as InformacionBasicaDetalleModelPMFIC;
  showModal_3_3: boolean = false;
  edit: boolean = false;
  indexUpdate: number = -1;
  tituloModal: string = '';
  idInfBasiZonficacionTotal : number = 0;
  _idInfBasica: number = 0;

  constructor(
    //private messageService: MessageService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private parentAreaManejo: TabInformacionBasicaComponent,
    private mapApi: MapApi,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private zonificacionService: ZonificacionOrdenamientoInternoAreaService,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    // this.listarCoordenadas();
   // this.listarZona();
    this.listarInformacionBasica3_3();
  }

  listarInformacionBasica3_3() {
    this.listaZonificacion = [];

    const params = {
      idInfBasica: this.CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.CodigoPMFIC.TAB_3_3
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => {
        this.dialog.closeAll();
        this.obtenerCapas();
        this.calculateAreaTotalZona();
      }))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          let i = 1;
          response.data.forEach((elemento: any) => {
            if (elemento.idPlanManejo == null) {
              elemento.idInfBasicaDet = 0;
              elemento.idZona = i;
              elemento.codigoZona = `Z${elemento.idZona}`;
              elemento.inServer = false;
              i = i + 1;
              this.listaZonificacion.push(new InformacionBasicaDetalleModelPMFIC(elemento));
            } else {
              this._idInfBasica = elemento.idInfBasica;
              //ya esta registrado
              elemento.idZona = Number(elemento.zonaVida);
              // elemento.idLayer = elemento.referencia;
              elemento.inServer = true;
              elemento.codigoZona = `Z${elemento.zonaVida}`

              if (elemento.codSubInfBasicaDet == this.CodigoPMFIC.TAB_3_3) {
                this.listaZonificacion.push(new InformacionBasicaDetalleModelPMFIC(elemento));
              } else if (elemento.codSubInfBasicaDet == this.CodigoPMFIC.TAB_3_3_TOTAL) {
                this.idInfBasiZonficacionTotal = elemento.idInfBasicaDet;
                this.totalAreaZonificacion = elemento.areaHa;
              }
            }
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    //this.CargarComboPadre();
    this.ref = this.dialogService.open(ModalFormularioZonasComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.zona.idZona = 0;
          this.zona.codigoZona = `ZO${this.listZona.length}`;
          this.zona.nombre = resp.nombre;
          this.zona.idZonaPadre = resp.ordenPadre || 0;
          this.zona.total = 0;
          this.zona.porcentaje = 0;
          this.zona.zonaAnexo = []
          this.listZona.push(this.zona);
          //this.guardar();
        } else if (tipo == "E") {
          for (let item of this.listZona) {
            if (item.idZona === data.idZona) {
              item.nombre = data.nombre;
            }
          }
        }
      }
    });
  };

  openEliminar(event: Event, data: InformacionBasicaDetalleModelPMFIC, index: number): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idInfBasicaDet != 0) {
          var params = {
            idInfBasica: 0,
            codInfBasicaDet: "",
            idUsuarioElimina: this.user.idUsuario,
            idInfBasicaDet: data.idInfBasicaDet
          }

          this.informacionAreaPmfiService.eliminarInformacionBasica(params).subscribe((response: any) => {
            if (response.success) {
              this.toast.warn('Se eliminó la Zonificación interna de la UMF.');
              this.listaZonificacion.splice(index, 1);
              // this.listarInformacionBasica3_3();
            } else {
              this.toast.error('Ocurrió un problema, intente nuevamente.');
            }
          });
        } else {
          this.listaZonificacion.splice(index, 1);
          // this.listaZonificacion.splice(
          //   this.listaZonificacion.findIndex((x) => x.idZona == data.idZona),
          //   1
          // );
        }
      },
      reject: () => { },
    });
  }

  onChangeFileSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      idLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      codZona: e.target.dataset.zone,
      codigo: e.target.id,
      codigoProceso:this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }

  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
    });
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = config.idLayer;
      t.groupId = this.parentAreaManejo._id;
      t.idGroupLayer = config.idGroupLayer;
      t.descripcion = config.codigoProceso;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.codigoItem = config.codZona;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.codigoProceso;
      //this._layers.push(layer);
      this.createLayer(t);
      this.calculateArea(t, config);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.codigoProceso;
    this._filesSHP.push(file);
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this.parentAreaManejo._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.codigoItem = item.codigoItem;
    this.parentAreaManejo.viewZona.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.parentAreaManejo.viewZona
        );
        this.parentAreaManejo.viewZona.goTo({ 
          target: data.fullExtent 
        });
      })
      .catch((error: any) => {});
  }

  setProperties(item: any) {
    let popupTemplate: any = {
      title: 'Zonificación UMF',
      content: [{
        type: "fields",
        fieldInfos: [
          {
            fieldName: "title",
            label: "Nombre capa",
          },
          {
            fieldName: "area",
            label: "Área",
          },
          {
            fieldName: "porcentaje",
            label: "Porcentaje",
          }
        ],
      }]
    };
    return popupTemplate;
  }

  calculateArea(data: any, config: any) {
    let controls: any = {};
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      if ((t.geometry.type = "Polygon")) {
        geometry = { spatialReference: { wkid: 4326 } };
        geometry.rings = t.geometry.coordinates;
        let area = this.mapApi.calculateArea(geometry, "hectares");
        sumArea += area;
        controls.success = true;
      } else {
        controls.success = false;
      }
    });

    this.listaZonificacion.forEach((item: any) => {
      if (item.codigoZona == config.codZona) {
        item.areaHa = Number(sumArea.toFixed(2));
        item.archivo = config.file;
        item.inServer = config.inServer;
        item.idLayer = config.idLayer;
        item.idGroupLayer = config.idGroupLayer;
        item.isNew = true;
      }
      this.calculateAreaTotalZona();
    });
  }

  calculateAreaTotalZona() {
    let sum1 = 0;
    let sum2 = 0;

    for (let item of this.listaZonificacion) {
      sum1 += Number(item.areaHa);
    }
    for (let item of this.listaZonificacion) {
      //item.areaHaPorcentaje = Number(((100 * Number(item.areaHa)) / sum1).toFixed(2)) || 0;
      if (this.totalAreaZonificacion == 0) {
        item.areaHaPorcentaje = 0;
      } else {
        item.areaHaPorcentaje = Number(((100 * Number(item.areaHa)) / this.totalAreaZonificacion).toFixed(2)) || 0;
      }

      sum2 += Number(item.areaHaPorcentaje);
    }
    this.totalAreaZona = parseFloat(sum1.toFixed(2));
    this.totalPorcentajeZona = parseFloat(sum2.toFixed(2));
  }

  guardarArchivoZona(codigoZona: any) {
    let fileUpload: any = [];

    this.listaZonificacion.forEach((t: any) => {
      if (t.codigoZona == codigoZona) {
        if (!t.inServer) {
          if (t.archivo !== null && t.archivo !== undefined) {
            let item: any = {
              idUsuario: this.user.idUsuario,
              codigo: '37',
              codigoTipoPGMF: this.codigoProceso,
              file: t.archivo,
              idGroupLayer: t.idGroupLayer,
              descripcion: t.codigoZona,
              codigoItem: t.codigoZona,
              idLayer: t.idLayer,
            };
            fileUpload.push(item);
          }
        }
      }      
    });
    
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.toast.error('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.mapApi.removeLayer(fileUpload[0].idLayer, this.parentAreaManejo.viewZona);
          this.obtenerCapas2(fileUpload[0].codigoItem);
        })
      )
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.toast.ok("El archivo se registró correctamente.");
          } else {
            this.toast.warn(result.message);
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error.");
        }
      );
  }

  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  
  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: this.codigoProceso,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      descripcion: item.descripcion
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }

  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.parentAreaManejo.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.parentAreaManejo.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: this.tipoGeometria,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }

  obtenerCapas2(codigoZona: any) {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.descripcion == codigoZona){
              if (t.geometry_wkt !== null) {
                let idLayer = this.mapApi.Guid2.newGuid;
                this.listaZonificacion.forEach((itemZona: any) => {
                  if (itemZona.codigoZona === t.descripcion) {
                    itemZona.idLayer = idLayer;
                    itemZona.inServer = true;
                    itemZona.idArchivo = t.idArchivo;
                    itemZona.isNew = false;

                    let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
                    let groupId = this.parentAreaManejo._id;
  
                    let item = {
                      color: t.colorCapa,
                      title: t.nombreCapa,
                      jsonGeometry: jsonGeometry,
                      properties: {
                        title: t.nombreCapa,
                        area: itemZona.areaHa,
                        porcentaje: itemZona.areaHaPorcentaje
                      },
                    };
                    let layer: any = {} as CustomCapaModel;
                    layer.codigo = t.idArchivo;
                    layer.idLayer = idLayer;
                    layer.inServer = true;
                    layer.service = false;
                    layer.nombre = t.nombreCapa;
                    layer.groupId = groupId;
                    layer.color = t.colorCapa;
                    layer.annex = false;
  
                    //this._layers.push(layer);
                    let geoJson = this.mapApi.getGeoJson(
                      layer.idLayer,
                      groupId,
                      item
                    );
                    this.createLayer(geoJson);
                  }
                });
              }
            }            
          });
        }
      },
      (error) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }

  onDeleteFileZona(data: any) {
    this.confirmationService.confirm({
      message: '¿Está seguro de eliminar este archivo?',
      icon: 'pi pi-exclamation-triangle',
      key: "deleteFileZona",
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.listaZonificacion.forEach((item: any) => {
          if (item.idZona === data.idZona) {
            // this.mapApi.removeLayer(data.idLayer, this.parentAreaManejo.viewZona);
            data.archivo = null;
            data.areaHa = 0;
            data.areaHaPorcentaje = 0;
            if (data.inServer === true) {
              data.inServer = false;
              // data.idLayer = null;
              this.eliminarArchivoDetalle(data.idArchivo);
              // this.guardarListaZonificacion().subscribe(() =>{
              //   this.listarInformacionBasica3_3()
              // });
            }
            this.mapApi.removeLayer(data.idLayer, this.parentAreaManejo.viewZona);
            data.idLayer = null;
            data.idArchivo = null;
            data.isNew = false;
            this.calculateAreaTotalZona();
          }
        });
      }
    });
  }

  onDownloadFile(e: any, data: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(data.idArchivo)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.file,
            result.data.nombre,
            'application/octet-stream'
          );
        }
      },
        (error: HttpErrorResponse) => {
          this.toast.error(error.message);
          this.dialog.closeAll();
        });
  }

  onChangeTogleLayer(e: any, idLayer: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.parentAreaManejo.viewZona);
  }

  registrar() {
    this.guardarListaZonificacion();
    
    // this.dialog.open(LoadingComponent, { disableClose: true });
    // forkJoin({
    //   //zonaResponse: this.guardarZona(),
    //   zonaResponse: this.guardarListaZonificacion(),
    //   // archivoResponse: this.guardarArchivoZona()
    // })
    //   .pipe(finalize(() => this.dialog.closeAll()))
    //   .subscribe(
    //     (result: any) => {
    //       this.toast.ok(result?.archivoResponse.message);
    //       this.cleanLayers();
    //       this.listarInformacionBasica3_3();
    //       //this.listarZona();
    //     },
    //     (error) => {
    //       this.toast.error('Ocurrió un error.');
    //     }
    //   );
  }

  guardarZona() {
    this.listZona.forEach((item: any, i: any) => {
      item.idPlanManejo = this.idPlanManejo;
      item.idUsuarioRegistro = this.user.idUsuario;
      item.codigoZona = item.codigoZona;
    });
    return this.zonificacionService.registrarZona(this.listZona);
  }

  guardarListaZonificacion() {
    let listMap: InformacionBasicaDetalle[] = [];

    this.listaZonificacion.forEach((obj: any) => {
      let item: any = {};
      // item.idInfBasica = value.idInfBasica || 0;
      item.descripcion = obj.key;
      item.idInfBasicaDet = obj.idInfBasicaDet ? obj.idInfBasicaDet : 0;
      item.idUnidadManejo = this.idPlanManejo;
      item.estado = "A";
      item.idUsuarioRegistro = this.usuario.idusuario;
      item.codInfBasicaDet = CodigoPMFIC.TAB_3_3;
      item.codSubInfBasicaDet = CodigoPMFIC.TAB_3_3;
      item.actividad = obj.actividad;
      item.areaHa = obj.areaHa;
      item.areaHaPorcentaje = obj.areaHaPorcentaje;
      item.observaciones = obj.observaciones;
      item.zonaVida = obj.idZona;//;obj.codigoZona;
      // item.referencia = obj.idLayer;
      listMap.push(item);
    });

    let itemTotal: any = {};
    // item.idInfBasica = value.idInfBasica || 0;
    itemTotal.idInfBasicaDet = this.idInfBasiZonficacionTotal;
    itemTotal.idUnidadManejo = this.idPlanManejo;
    itemTotal.estado = "A";
    itemTotal.idUsuarioRegistro = this.usuario.idusuario;
    itemTotal.codInfBasicaDet = CodigoPMFIC.TAB_3_3;
    itemTotal.codSubInfBasicaDet = CodigoPMFIC.TAB_3_3_TOTAL;
    itemTotal.actividad = "Total Area Zonificación";
    itemTotal.areaHa = this.totalAreaZonificacion;
    listMap.push(itemTotal);

    let params = [
      {
        idInfBasica: this._idInfBasica,
        codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
        codSubInfBasica: this.CodigoPMFIC.TAB_3,
        codNombreInfBasica: CodigoPMFIC.TAB_3_3,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró la zonificación interna de la UMF correctamente');
          // this.cleanLayers();
          this.listarInformacionBasica3_3();
        } else {
          this.toast.error(response?.message);
        }
      });
    // return this.zonificacionService.registrarZona(this.listZona);
  }

  cleanLayers() {
    let layers = this.mapApi.getLayers(this.parentAreaManejo._id, this.parentAreaManejo.viewZona);
    layers.forEach((t: any) => {
      this.parentAreaManejo.viewZona.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    //this._layers = [];
  }

  getLayers() {
    let layers = this.parentAreaManejo.viewZona.map.allLayers.filter(
      (t: any) => t.groupId === this.parentAreaManejo._id
    );
    return layers;
  }

  listarZona() {
    this.listZona = [];
    let params = {
      idPlanManejo: this.idPlanManejo
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.zonificacionService.listarZona(params).subscribe((result: any) => {
      this.listZona = result.data;
      this.listZona.forEach((t: any, i: any) => {
        if (t.idPlanManejo === null) {
          t.codigoZona = `Z${t.idZona}`
        }
      });
      this.dialog.closeAll();
      this.obtenerCapas();
      this.calculateAreaTotalZona();
    });

  }

  obtenerCapas() {
    // this.cleanLayers();
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let idLayer = this.mapApi.Guid2.newGuid;
              this.listaZonificacion.forEach((itemZona: any) => {
                if (itemZona.codigoZona === t.descripcion) {
                  itemZona.idLayer = idLayer;
                  itemZona.inServer = true;
                  itemZona.idArchivo = t.idArchivo;

                  let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
                  let groupId = this.parentAreaManejo._id;

                  let item = {
                    color: t.colorCapa,
                    title: t.nombreCapa,
                    jsonGeometry: jsonGeometry,
                    properties: {
                      title: t.nombreCapa,
                      area: itemZona.areaHa,
                      porcentaje: itemZona.areaHaPorcentaje
                    },
                  };
                  let layer: any = {} as CustomCapaModel;
                  layer.codigo = t.idArchivo;
                  layer.idLayer = idLayer;
                  layer.inServer = true;
                  layer.service = false;
                  layer.nombre = t.nombreCapa;
                  layer.groupId = groupId;
                  layer.color = t.colorCapa;
                  layer.annex = false;

                  //this._layers.push(layer);
                  let geoJson = this.mapApi.getGeoJson(
                    layer.idLayer,
                    groupId,
                    item
                  );
                  this.createLayer(geoJson);
                }
              });
            }
          });
        }
      },
      (error) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    ).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success) {
        this.toast.ok('Se eliminó el archivo y/o geometría correctamente.');
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, (error) => {
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }

  openModalNuevo() {
    //this.tipoTabla = tipo;
    this.edit = false;
    this.showModal_3_3 = true;
    this.tituloModal = "Nueva zonificación interna de la UMF";
  }

  agregar() {
    if (this.edit) {
      this.editarItem();
    } else {
      this.agregarItem();
    }
  }

  agregarItem() {
    if (this.validarFormulario_9_0()) {
      this.det_modal_3_3.codInfBasicaDet = this.codigoSubSeccion;
      this.det_modal_3_3.codSubInfBasicaDet = "A";
      this.det_modal_3_3.idInfBasicaDet = 0;
      this.det_modal_3_3.idPlanManejo = this.idPlanManejo;
      this.det_modal_3_3.idUsuarioRegistro = this.user.idUsuario;

      let maxIdZona = -1;
      this.listaZonificacion.forEach((e: InformacionBasicaDetalleModelPMFIC) => {
        if (e.idZona > maxIdZona) {
          maxIdZona = e.idZona;
        }
      });

      maxIdZona = maxIdZona + 1;
      this.det_modal_3_3.idZona = maxIdZona;
      this.det_modal_3_3.codigoZona = `Z${maxIdZona}`;

      this.listaZonificacion.push(this.det_modal_3_3);
      this.seteoValoresDefecto();
    }
  }

  editarItem() {
    if (this.validarFormulario_9_0()) {
      if (this.indexUpdate >= 0) {
        this.listaZonificacion[this.indexUpdate] = this.det_modal_3_3;
        this.seteoValoresDefecto();
      }
    }
  }

  validarFormulario_9_0() {
    let respuest: boolean = true;
    if (
      this.det_modal_3_3.actividad == null ||
      this.det_modal_3_3.actividad == '' ||
      this.det_modal_3_3.actividad == undefined
    ) {
      this.toast.warn('Ingrese una Categoría de zonificación interna de la UMF.');
      respuest = false;
    }
    return respuest;
  }

  openModalEditar(informacionBasicaDetalleModelPMFIC: InformacionBasicaDetalleModelPMFIC, index: number) {
    this.tituloModal = "Editar zonificación interna de la UMF";
    this.edit = true;
    this.indexUpdate = index;
    this.det_modal_3_3 = { ...informacionBasicaDetalleModelPMFIC };
    this.showModal_3_3 = true;
  }

  seteoValoresDefecto() {
    this.tituloModal = '';
    this.det_modal_3_3 = {} as InformacionBasicaDetalleModelPMFIC;
    this.showModal_3_3 = false;
    this.indexUpdate = -1;
    this.edit = false;
  }
}
