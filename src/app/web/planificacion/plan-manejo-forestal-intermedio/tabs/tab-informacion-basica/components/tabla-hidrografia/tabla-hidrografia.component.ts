import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { InformacionBasicaDetalleModelPMFIC, InformacionBasicaModelPMFIC } from 'src/app/model/InformacionSocieconomicaPMFIC';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';

@Component({
  selector: 'app-tabla-hidrografia',
  templateUrl: './tabla-hidrografia.component.html',
  styleUrls: ['./tabla-hidrografia.component.scss']
})
export class TablaHidrografiaComponent implements OnInit {
  @Input() idPGMF!: number;
  @Input() listInfoBasicaDet_H: InformacionBasicaDetalleModelPMFIC[] = [];
  @Input() listInfoBasicaDet_H2: InformacionBasicaDetalleModelPMFIC[] = [];
  @Input() isModal!: boolean;
  @Input() disabled!: boolean;
  @Output() listEliminar = new EventEmitter<any>();

  codigoAcordeon1: string = CodigoPMFIC.TAB_4_1;

  infoBasica4_1: InformacionBasicaModelPMFIC = new InformacionBasicaModelPMFIC({
    idInfBasica: 0,
    idPlanManejo: this.idPGMF,
    codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
    codSubInfBasica: CodigoPMFIC.TAB_4,
    codNombreInfBasica: CodigoPMFIC.TAB_4_1,
    idUsuarioRegistro: this.user.idUsuario,
  });

  constructor(
    private dialog: MatDialog,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService
  ) { }

  ngOnInit(): void {
    // this.obtenerCapas()
  }

  obtenerCapas() {
    let item = {
      idPlanManejo: this.idPGMF,
      codigoSeccion: 'PMFIC',
      codigoSubSeccion: 'PMFICINFBATIM',
      tipoGeometria:'TPHIDRO'
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe((result: any) => {
        if (result.data.length) {
          let listHidro:any = [];
          result.data.forEach((t:any) =>{
            let propiedad = JSON.parse(t.propiedad);
            listHidro.push({
              descripcion: propiedad.TIPO,
              nombre: propiedad.NOMBRE
            })
          })
          this.listInfoBasicaDet_H = listHidro;
        }
      });
  }

  listarHidrografia4_1() {
    this.listInfoBasicaDet_H = [];
    let params: any = {
      idInfBasica: CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: this.codigoAcordeon1,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            this.infoBasica4_1 = response.data[0];
            response.data.forEach((item: any) => {
              this.listInfoBasicaDet_H.push(item);
            });
          }
        }
      });
  }

  openEliminarRio(event: Event, data: any) {
    let list: any = [];
    let obj: any = {};

    obj.event = event;
    obj.idInfBasicaDet = data.idRio;
    obj.index = data.indexRio;
    list.push(obj);

    this.listEliminar.emit(list);
  }

  openEliminarQuebrada(event: Event, data: any) {
    let list: any = [];
    let obj: any = {};

    obj.event = event;
    obj.idInfBasicaDet = data.idQuebrada;
    obj.index = data.indexQuebrada;
    list.push(obj);

    this.listEliminar.emit(list);
  }

  openEliminarLaguna(event: Event, data: any) {
    let list: any = [];
    let obj: any = {};

    obj.event = event;
    obj.idInfBasicaDet = data.idLaguna;
    obj.index = data.indexLaguna;
    list.push(obj);

    this.listEliminar.emit(list);
  }
}
