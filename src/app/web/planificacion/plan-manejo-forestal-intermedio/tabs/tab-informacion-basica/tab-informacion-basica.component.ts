import { HttpErrorResponse } from "@angular/common/http";
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { AppMapaComponent, DownloadFile, MapApi, PGMFArchivoTipo, ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { from } from "rxjs";
import { concatMap, finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ModalFileComponent } from "src/app/components/modal/modal-file/modal-file.component";
import { ModalObservacionesComponent } from "src/app/components/modal/modal-observaciones/modal-observaciones.component";
import { InformacionUnidadManejo } from "src/app/model/InformacionAreaManejo";
import { InformacionBasicaDetalleModelPMFIC, InformacionBasicaModelPMFIC, ListInformacionBasicaDetSubPMFIC } from "src/app/model/InformacionSocieconomicaPMFIC";
import { PlanManejoGeometriaModel } from "src/app/model/PlanManejo/PlanManejoGeometria";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CustomCapaModel } from "src/app/model/util/CustomCapa";
import { FileModel } from "src/app/model/util/File";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { TablaAreaManejoContinuaComponent } from "./components/tabla-area-manejo-continua/tabla-area-manejo-continua.component";
import { TablaAreaManejoDivididaComponent } from "./components/tabla-area-manejo-dividida/tabla-area-manejo-dividida.component";
import { TablaInformativaMapaComponent } from "./components/tabla-informativa-mapa/tabla-informativa-mapa.component";
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';

@Component({
  selector: "app-tab-informacion-basica",
  styleUrls: ["./tab-informacion-basica.component.scss"],
  templateUrl: "./tab-informacion-basica.component.html",
})
export class TabInformacionBasicaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;
  @ViewChild(TablaAreaManejoDivididaComponent) acordeon_3_3!: TablaAreaManejoDivididaComponent;
  @ViewChild(TablaAreaManejoContinuaComponent) acordeon_3_2!: TablaAreaManejoContinuaComponent;
  @ViewChild(TablaInformativaMapaComponent) tim!: TablaInformativaMapaComponent;

  ref: DynamicDialogRef = new DynamicDialogRef();
  usuario = {} as UsuarioModel;
  view: any = null;
  viewZona:any = null;
  viewTablaInformativa:any = null;

  listCoordinatesAnexo1: any[] = [];
  listCoordinatesAnexo2: any[] = [];
  idInfBasica: number = 0;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  listaBosque: any[] = [];
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  @ViewChild("tblAcreditacionComunal", { static: true })
  private tblAcreditacionComunal!: ElementRef;
  tipoBosquesList: any[] = [];
  totalAreaTipoBosque: String = "";
  totalPorcentajeTipoBosque: String = "";
  unidadManejo = {} as InformacionUnidadManejo;
  listCoordinatesAnexo: any[] = [];
  listAnexoArea: any[] = [];
  totalAnexoArea!: number;
  listAccesibilidad: any[] = [];
  totalAreaZonificacion: number = 0;

  //listado
  listOrdenInterno: any[] = [{}, {}];
  CodigoPMFIC = CodigoPMFIC;

  planificacion: any = [];

  data = [
    {
      ruta: "De (ciudad) al centro de la comunidad",
    },
    {
      ruta:
        "Del centro de la comunidad al área de aprovechamiento del primer año operativo",
    },
  ];
  zonificacion = [
    {
      categoria: "1. Áreas para produccion maderable",
      area: 25,
      porcentaje: 0,
      observaciones: "",
      cargarObservacion: false,
      editarObservaciones: true,
    },
    {
      categoria: "1. Áreas para produccion maderable",
      area: 5,
      porcentaje: 0,
      observaciones: "",
      cargarObservacion: false,
      editarObservaciones: true,
    },
    {
      categoria: "1. Áreas para produccion maderable",
      area: 15,
      porcentaje: 0,
      observaciones: "",
      cargarObservacion: false,
      editarObservaciones: true,
    },
  ];

  areaManejoForestal = [{}];

  transporte = [
    { label: "Terrestre", value: "TERRESTRE" },
    { label: "Fluvial", value: "FLUVIAL" },
    { label: "Mixto", value: "MIXTO" },
  ];

  vehiculo = [
    { label: "Camión / Carreta", value: "Camión / Carreta" },
    { label: "Chata", value: "Chata" },
    { label: "Bote", value: "Bote" },
    { label: "Tráiler", value: "Tráiler" },
    { label: "Semitráiler", value: "Semitráiler" },
    { label: "Tractor", value: "Tractor" },
    { label: "Otros", value: "Otros" },
  ];

  informacionBasica3_4: InformacionBasicaModelPMFIC = {} as InformacionBasicaModelPMFIC;
  informacionBasicaDet3_4: InformacionBasicaDetalleModelPMFIC[] = [];
  isSubDet!: boolean;

  idInfBasicaU: any = 0;
  idInfBasicaS: any = 0;




  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_3;

  codigoAcordeon_3_1: string = CodigoPMFIC.TAB_3_1;
  codigoAcordeon_3_2: string = CodigoPMFIC.TAB_3_2;
  codigoAcordeon_3_3: string = CodigoPMFIC.TAB_3_3;
  codigoAcordeon_3_4: string = CodigoPMFIC.TAB_3_4;

  detEvaluacion_3_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_1,
  });

  detEvaluacion_3_2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_2,
  });

  detEvaluacion_3_3: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_3,
  });

  detEvaluacion_3_4: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_4,
  });

  evaluacion: any;

  constructor(
    public dialogService: DialogService,
    private user: UsuarioService,
    private mapApi: MapApi,
    private dialog: MatDialog,
    private serviceGeoforestal: ApiGeoforestalService,
    private confirmationService: ConfirmationService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private serviceExternos: ApiForestalService,private evaluacionService: EvaluacionService,private router: Router
  ) {}

  ngOnInit() {
    this.usuario = this.user.usuario;
    this.initializeMap();
    this.obtenerCapas();
    this.list3_0();
    // this.listarInfBasica();
    this.listarInformacionBasica3_4();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarInfBasica(){
    let params: any = {
      idInfBasica: this.CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: ""
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        this.idInfBasica = response.data[0]?.idInfBasica || 0;
      });
  }

  listarInformacionBasica3_4() {
    this.informacionBasicaDet3_4 = [];
    const params = {
      idInfBasica: this.CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.CodigoPMFIC.TAB_3_4
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];
          this.informacionBasica3_4 = new InformacionBasicaModelPMFIC(element);
          response.data.forEach((item: any) => {
            this.informacionBasicaDet3_4.push(item);
            if (item.listInformacionBasicaDetSub.length > 0) this.isSubDet = true;
          });
        }
      });
  }

  registrarInformacionBasica3_4() {
    this.informacionBasica3_4.idPlanManejo = this.idPlanManejo;
    this.informacionBasica3_4.idInfBasica = this.informacionBasica3_4.idInfBasica ? this.informacionBasica3_4.idInfBasica : 0;
    this.informacionBasica3_4.codInfBasica = this.CodigoPMFIC.CODIGO_PROCESO;
    this.informacionBasica3_4.codSubInfBasica = this.CodigoPMFIC.TAB_3;
    this.informacionBasica3_4.codNombreInfBasica = this.CodigoPMFIC.TAB_3_4;
    this.informacionBasica3_4.idUsuarioRegistro = this.user.idUsuario;
    this.informacionBasica3_4.listInformacionBasicaDet = [];
    this.informacionBasicaDet3_4.forEach((item) => {
      item.codInfBasicaDet = this.CodigoPMFIC.TAB_3_4;
      item.codSubInfBasicaDet = this.CodigoPMFIC.TAB_3_4;
      item.idInfBasica = item.idInfBasica ? item.idInfBasica : 0;
      item.idInfBasicaDet = item.idInfBasica ? item.idInfBasicaDet : 0;
      item.idUsuarioRegistro = this.user.idUsuario;
      this.informacionBasica3_4.listInformacionBasicaDet.push(item);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica([ this.informacionBasica3_4 ])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró la accesibilidad de la UMF correctamente.");
          this.listarInformacionBasica3_4();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openModalObservacione(mensaje: string, observacion: any, edit: any) {
    this.ref = this.dialogService.open(ModalObservacionesComponent, {
      header: mensaje,
      width: "30%",
      contentStyle: { "max-height": "200px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        item: observacion,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        observacion.observaciones = resp;
        (observacion.cargarObservacion = true),
          (observacion.editarObservaciones = false);
      }
    });
  }

  descaragarFile(data: any) {}
  eliminarFile(data: any) {
    data.shapeFile = "";
    data.cargarDocumento = false;
    data.editarDocumento = true;
  }

  cargarFile = (mensaje: string, data: any) => {
    this.ref = this.dialogService.open(ModalFileComponent, {
      header: mensaje,
      width: "70%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        data.cargarDocumento = true;
        data.editarDocumento = false;
        data.shapeFile = resp.file;
        data.value = "SI";
      }
    });
  };
  cargarObservacione(data: any) {
    (data.cargarObservacion = true), (data.editarObservaciones = false);
  }

  eliminarObservacione(data: any) {}

  //Mapa

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.view = view;
    this.viewZona = view;
    this.viewTablaInformativa = view;
  }

  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    code: any,
    codigoSubSeccion: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      codigoSubTipoPGMF: code,
      withVertice: withVertice,
      codigoSubSeccion: codigoSubSeccion,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (config.codigoSubTipoPGMF === "PGMFAREA") {
        this.calculateArea(data);
      }
    });
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.codigoSubTipoPGMF;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.annex = config.annex;
      layer.descripcion = config.codigoSubSeccion;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.codigoSubSeccion;
    this._filesSHP.push(file);
  }

  calculateArea(data: any) {
    if (data[0].title === "PGMF") {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = data[0].features[0].geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, "hectares");
      let tabla = this.tblAcreditacionComunal.nativeElement;
      let body = tabla.querySelector("tbody");
      let trs = body.querySelectorAll("tr");
      let tdArea = trs[0].querySelectorAll("td")[2];
      tdArea.innerHTML = area.toFixed(3);
    }
    data[0].features.forEach((t: any) => {
      if (t.geometry.type === "Polygon") {
        let item2: any = {
          anexoSector: t.properties.ANEXO,
          detalle: [],
        };
        this.consultarTiposBosque(t.geometry.coordinates).subscribe(
          (result: any) => {
            result.dataService.data.capas.forEach((t2: any) => {
              if (t2.geoJson !== null) {
                let item = {
                  subparcela: "",
                  tipoBosque: t2.nombreCapa,
                  porcentaje: 0,
                  area:
                    t2.geoJson.features[0].properties.SUPAPR ||
                    t2.geoJson.features[0].properties.za_super,
                };
                item2.detalle.push(item);
              }
            });
            this.tipoBosquesList.push(item2);
            this.calculateAreaTotal();
          }
        );
      }
    });
  }

  calculateAreaTotal() {
    let sum1 = 0;
    let sum2 = 0;

    for (let item of this.tipoBosquesList) {
      item.detalle.forEach((t: any) => {
        sum1 += t.area;
      });
    }
    for (let item of this.tipoBosquesList) {
      item.detalle.forEach((t: any) => {
        t.porcentaje = Number(((100 * t.area) / sum1).toFixed(1)) || 0;
        sum2 += t.porcentaje;
      });
    }
    this.totalAreaTipoBosque = `${sum1.toFixed(2)}`;
    this.totalPorcentajeTipoBosque = `${sum2.toFixed(1)} %`;
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }
  consultarTiposBosque(geometry: any) {
    let params = {
      idClasificacion: "",
      geometria: {
        poligono: geometry,
      },
    };
    return this.serviceExternos.identificarTipoBosque(params);
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          "application/octet-stream"
        );
      }
      (error: HttpErrorResponse) => {
        this.toast.error(error.message);
        this.dialog.closeAll();
      };
    });
  }
  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.withVertice === true) {
      popupTemplate.content = [
        {
          type: "fields",
          fieldInfos: [
            {
              fieldName: "anexo",
              label: "Anexo",
            },
            {
              fieldName: "vertice",
              label: "Vertice",
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: "este",
              label: "Este (X)",
            },
            {
              fieldName: "norte",
              label: "Norte (Y)",
            },
          ],
        },
      ];
    }
    return popupTemplate;
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  onGuardarLayer() {
    let fileUpload: any = [];

    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: "37",
          codigoTipoPGMF: this.CodigoPMFIC.CODIGO_PROCESO,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.toast.error("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.cleanLayers();
          this.obtenerCapas();
        })
      )
      .subscribe(
        (result) => {
          this.toast.ok(result.message);
        },
        (error) => {
          this.toast.error("Ocurrió un error.");
        }
      );
  }
  obtenerCapas() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.CodigoPMFIC.TAB_3,
      codigoSubSeccion: "",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              let layer = {} as CustomCapaModel;
              layer.codigo = t.idArchivo;
              layer.idLayer = this.mapApi.Guid2.newGuid;
              layer.inServer = true;
              layer.service = false;
              layer.nombre = t.nombreCapa;
              layer.groupId = groupId;
              layer.color = t.colorCapa;
              layer.descripcion = t.codigoSubSeccion;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layers.push(layer);
              let geoJson = this.mapApi.getGeoJson(
                layer.idLayer,
                groupId,
                item
              );
              this.createLayer(geoJson);
            }
          });
        }
      },
      (error) => {
        this.toast.error("Ocurrió un error");
      }
    );
  }

  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: this.CodigoPMFIC.CODIGO_PROCESO,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      descripcion: item.descripcion
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.CodigoPMFIC.TAB_3,
        codigoSubSeccion: t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      let idInfBasicaDet = 0;
      let codigoInfBasicaDet = '';
      let message = "¿Está seguro de eliminar este grupo de archivo?";
      if(layer.descripcion === CodigoPMFIC.TAB_3_2){
        idInfBasicaDet = this.acordeon_3_3._idInfBasicaDet;
        codigoInfBasicaDet = CodigoPMFIC.TAB_3_2;
        message = "¿Está seguro de eliminar este archivo que pertence a superficie de UMF?";
      }else if(layer.descripcion === CodigoPMFIC.TAB_3_1){
        idInfBasicaDet = this.acordeon_3_2._idInfBasicaDet;
        codigoInfBasicaDet = CodigoPMFIC.TAB_3_1;
        message = "¿Está seguro de eliminar este archivo que pertence a ubicacion de la comunidad?";
      }
      this.confirmationService.confirm({
        message: message,
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHP",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo)
          .pipe(concatMap(()=>this.eliminarInformacionBasica(idInfBasicaDet,codigoInfBasicaDet)))
          .subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                if(layer.descripcion === CodigoPMFIC.TAB_3_2){
                  this.listCoordinatesAnexo2 = [];
                  this.acordeon_3_3.listAnexoArea = [];
                } else if(layer.descripcion === CodigoPMFIC.TAB_3_1){
                  this.listCoordinatesAnexo = [];
                }
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._layers.length === 0) {
                  this.cleanLayers();
                }
                this.toast.ok("El archivo se eliminó correctamente.");
              } else {
                this.toast.error("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.toast.error("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      if(layer.descripcion === CodigoPMFIC.TAB_3_2){
        this.listCoordinatesAnexo2 = [];
        this.acordeon_3_3.listAnexoArea = [];
      }else if(layer.descripcion === CodigoPMFIC.TAB_3_1){
        this.listCoordinatesAnexo = [];
      }
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._layers.length === 0) {
        this.cleanLayers();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }
  eliminarInformacionBasica(idInfBasica:any,codigoInfBasicaDet:string){
    let params = {
      idInfBasica: this.idInfBasica,
      codInfBasicaDet: codigoInfBasicaDet,
      idInfBasicaDet: idInfBasica,
      idUsuarioElimina: this.user.idUsuario
    }
    return this.informacionAreaPmfiService.eliminarInformacionBasica(params);
  }

  // carga de archivo

  list3_0() {
    this.listCoordinatesAnexo = [];
    var params = {
      idInfBasica: CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigoPMFIC.TAB_3_0,
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        this.planificacion = [...response.data];
      });
  }

  get totalArea() {
    return this.listOrdenInterno
      .map((i) => i.areaHA)
      .reduce((sum, x) => sum + x, 0);
  }

  get totalPorcentaje() {
    return this.listOrdenInterno
      .map((i) => i.areaHAPorcentaje)
      .reduce((sum, x) => sum + x, 0);
  }

  calcularPorcentaje() {
    let totalArea: number = this.totalArea;
    this.listOrdenInterno.forEach((e: any) => {
      e.areaHAPorcentaje = Number(e.areaHA / totalArea);
    });
    this.totalPorcentaje;
  }

  openEliminar(event: Event, data: any, index: any): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {},
    });
  }
  download(e: any) {
    let nameFile = String(e.categoria)
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "");
    nameFile = nameFile.replace(/[^a-z0-9\s]/gi, "").replace(/[-\s]/g, "_");
    this.map.downloadGroup(e.actividadesRealizar, nameFile);
  }

  addLayer(planificacion: any, e: any, ) {
    let item: any = {
      idUsuario: this.user.idUsuario,
      codigo: "37",
      file: e.target.files[0],
    };
    this.saveFileItem(item).subscribe( (response: any) => {
      planificacion.numeroPc = response.data.idArchivo
    });

     this.puntoVertice(e);

    //this.puntoAnexos(e);
  }

  puntoVertice(e: any) {
    this.onChangeFileSHP(e, false, "TPPUNTO", "PGMFAIBAMCUAMAMC");
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaAnexo(data[0].features);
    });
    e.target.value = "";
  }
  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo.push({
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: "",
      });
    });
  }

  puntoAnexos(e: any) {
    this.onChangeFileSHP(e, true, "TPPUNTO", "PGMFAIBAMCUAMAMD");
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaConAnexo(data[0].features);
    });
    e.target.value = "";
  }
  createTablaConAnexo(items: any) {
    let listMap = items.map((t: any) => {
      return {
        anexo: t.properties.anexo,
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: "",
      };
    });

    this.listCoordinatesAnexo2 = listMap.groupBy((t: any) => t.anexo);
  }

  saveFileItem(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelationItem(result, item)));
  }
  saveFileRelationItem(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: this.CodigoPMFIC.CODIGO_PROCESO,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe();
  }

  listarUbicacion(items: any) {
    this.listCoordinatesAnexo = [];
    this.listCoordinatesAnexo = items;
  }

  listarSuperficie(items: any) {
    this.listCoordinatesAnexo2 = [];
    this.listCoordinatesAnexo2 = items;
  }

  listarArea(items: any) {
    this.listAnexoArea = [];
    this.listAnexoArea = items;
  }

  listarTotalArea(total: any) {
    this.totalAnexoArea = total;
  }

  listarAccesibilidad(items: any) {
    this.listAccesibilidad = [];
    this.listAccesibilidad = items;

    this.informacionBasicaDet3_4.forEach((element: InformacionBasicaDetalleModelPMFIC, index: number) => {
      if (index == 1) {
        let det: ListInformacionBasicaDetSubPMFIC[] = [];
        let num: number = 0;
        let ant: string = "";
        this.listAccesibilidad.forEach((item: any) => {
          let subDet = new ListInformacionBasicaDetSubPMFIC();
          subDet.descripcion = item.ruta;
          subDet.detalle = item.transporte;
          subDet.codTipoInfBasicaDet = this.CodigoPMFIC.TAB_3_4;;
          subDet.codSubTipoInfBasicaDet = this.CodigoPMFIC.TAB_3_4;
          det.push(subDet);
          this.isSubDet = true;
          if (ant != item.transporte) {
            num++;
          }
          ant = item.transporte;
        });

        if (num == 1) {
          element.descripcion = ant;
        }
        if (num > 1) {
          element.descripcion = "MIXTO";
        }
        element.observaciones = "S";
        element.listInformacionBasicaDetSub = [ ...det];
      }
    });
  }

  listarTotalAreaZonificacion(total: any) {
    this.totalAreaZonificacion = total;
  }

  listarIdInfBasicaU(id: number) {
    this.idInfBasicaU = id;
  }

  listarIdInfBasicaS(id: number) {
    this.idInfBasicaS = id;
  }

  eliminarU(elimina: boolean) {
    if (elimina == true) {
      this.initializeMap();
      this.tim.ngAfterViewInit();
    }
  }

  eliminarS(elimina: boolean) {
    if (elimina == true) {
      this.initializeMap();
      this.tim.ngAfterViewInit();
    }
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }



  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_3_1 = Object.assign(this.detEvaluacion_3_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_1));
              this.detEvaluacion_3_2 = Object.assign(this.detEvaluacion_3_2,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_2));
              this.detEvaluacion_3_3 = Object.assign(this.detEvaluacion_3_3,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_3));
              this.detEvaluacion_3_4 = Object.assign(this.detEvaluacion_3_4,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_4));
            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_3_1,this.detEvaluacion_3_2,this.detEvaluacion_3_3,this.detEvaluacion_3_4,])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_1);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_2);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_3);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_4);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6241"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
