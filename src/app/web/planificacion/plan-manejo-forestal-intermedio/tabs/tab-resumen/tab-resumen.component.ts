import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { data } from "jquery";
import { MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { FileModel } from "src/app/model/util/File";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { NotificacionService } from "src/app/service/notificacion";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { CargaArchivosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/caragaArchivos.service";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service";
import { CodigoPMFIC } from "../../../../../model/util/PMFIC/CodigoPMFIC";
import {Router} from '@angular/router';

@Component({
  selector: "app-tab-resumen",
  templateUrl: "./tab-resumen.component.html",
  styleUrls: ["./tab-resumen.component.scss"],
})
export class TabResumenComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output()
  public regresar = new EventEmitter();

  CodigoPMFIC = CodigoPMFIC;
  @Input() disabled: boolean =  false;
  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICINFG",
      label: "1. Información General",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICOBJ",
      label: "2. Objetivos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICINFBA",
      label: "3. Información básica",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICAF",
      label: "4. Aspectos físicos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICAB",
      label: "5. Aspectos biológicos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICIS",
      label: "6. Información socioeconómica",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICOP",
      label: "7. Ordenamiento y protección de la UMF",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICPPF",
      label: "8. Potencial de producción forestal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICSMFUM",
      label: "9. Sistema de manejo forestal de uso múltiple",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICEA",
      label: "10. Evaluación ambiental",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICORG",
      label: "11. Organización para desarrollo de actividades",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICORGCA",
      label: "12. Cronograma de actividades",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICORGRMF",
      label: "13. Rentabilidad del manejo forestal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICORGAC",
      label: "14. Aspectos complementarios",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PMFICORGA",
      label: "15. Anexos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
  ];

  verPMFIC: boolean = false;
  pendientes: any[] = [];
  verDescargaPMFIC: boolean = true;
  verDescargaAnexoPMFIC: boolean = true;

  files: FileModel[] = [];
  filePMFIC: FileModel = {} as FileModel;

  consolidadoPMFIC: any = null;

  cargarPMFIC: boolean = false;
  eliminarPMFIC: boolean = true;
  idArchivoPMFIC: number = 0;
  verEnviar: boolean = false;

  CodigoProceso = CodigoProceso;

  isSubmittingFormulario$ = this.formularioQuery.selectSubmitting();
  isLoadingFormulario$ = this.formularioQuery.selectLoading();

  isSubmittingGenerar$ = this.generarQuery.selectSubmitting();
  isLoadingGenerar$ = this.generarQuery.selectLoading();

  isSubmittingPlanFormulado$ = this.planFormuladoQuery.selectSubmitting();

  isSubmittingARFFS$ = this.ARFFSQuery.selectSubmitting();

  checkAll: boolean = false;
  enviarARFFS: boolean = false;

  longitudTUPA:number=0

  masterSelected: boolean = false;
  disableGenerarPMFIC: boolean = true;
  disableDescargarPMFIC: boolean = true;
  disableAdjuntarPMFIC: boolean = true;
  disablePlanFormulado: boolean = true;
  disableAgregarTUPA: boolean = true;
  disableEnviarARFFS: boolean = true;


  disabledCodigoEstadoPlanBorrador:boolean=true;
  disabledCodigoEstadoPlanPresentado:boolean=true;

  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private cargaArchivosService: CargaArchivosService,
    private toast: ToastService,
    private user: UsuarioService,
    private evaluacionService: EvaluacionService,
    private generarStore: ButtonsCreateStore,
    private generarQuery: ButtonsCreateQuery,
    private formularioStore: ButtonsCreateStore,
    private formularioQuery: ButtonsCreateQuery,
    private planFormuladoStore: ButtonsCreateStore,
    private planFormuladoQuery: ButtonsCreateQuery,
    private ARFFSStore: ButtonsCreateStore,
    private ARFFSQuery: ButtonsCreateQuery,
    private notificacionService: NotificacionService,
    private informacionGeneralService: InformacionGeneralService,private router: Router
  ) {}

  ngOnInit(): void {
    //this.listarTabs();
    this.listarEstados();
    this.obtenerEstadoPlan();
  }


  obtenerEstadoPlan(){
    const body={
      idPlanManejo:this.idPlanManejo
  }
    this.informacionGeneralService.listarInfGeneralResumido(body).subscribe((_res:any)=>{

      if(_res.data.length>0){
        const estado= _res.data[0];
        if(estado.codigoEstado == 'EPLMPRES' || estado.codigoEstado == 'EMDOBS' || estado.codigoEstado == 'EMDBOR'){
          this.disabledCodigoEstadoPlanPresentado=false;
          this.disabledCodigoEstadoPlanBorrador= true;
          this.disabled = true;
        }
        if(estado.codigoEstado == 'EPLMBOR'){
          this.disabledCodigoEstadoPlanPresentado=true;
          this.disabledCodigoEstadoPlanBorrador= false;
          this.disabled = false;
        }
      }
    })
  }

  regresarTab() {
    this.regresar.emit();
  }

  isTodosPlanesTerminado : boolean = true;

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }

    if (
      this.listProcesos.length ===
      this.listProcesos.filter((data) => data.marcar === true).length
    ) {
      this.verPMFIC = false;
      this.isTodosPlanesTerminado = true;
    }else{
      this.isTodosPlanesTerminado = false;
    }

    // this.isAllSelected(false);
  };

  listarEstados() {
    this.formularioStore.submit();
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      idPlanManejoEstado: null,
    };

    this.cargaEnvioDocumentacionService.listarEstadosPlanManejo(body).subscribe(
      (response: any) => {
        this.formularioStore.submitSuccess();
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == "CPMPEND") {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == "CPMPEND") {
            this.isTodosPlanesTerminado = false;
            this.listProcesos[objIndex].estado = "Pendiente";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == "CPMTERM") {
            this.listProcesos[objIndex].estado = "Terminado";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });
        this.pendiente();
        // this.isAllSelected(true);
      },
      (err) => {
        this.formularioStore.submitError(err);
      }
    );
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPMFIC = true;
    } else {
      this.verPMFIC = false;
    }
  }

  guardarEstados() {
    this.formularioStore.submit();
    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element: any, index) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        observacion: "",
      };
      listaEstadosPlanManejo.push(obje);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success) {
            response.data.forEach((element: any) => {
              if (element.codigoEstado == "CPMPEND") {
                this.pendientes.push(element);
              }
            });
            this.listarEstados();

            this.messageService.add({
              key: "tl",
              severity: "success",
              summary: "",
              detail:
                "Se registró los estados del Plan General de Manejo Forestal Intermedio - PMFI correctamente.",
            });
            this.formularioStore.submitSuccess();
          } else {
            this.messageService.add({

              key: "tl",
              severity: "error",
              summary: "ERROR",
              detail: response.message,
            });
            this.formularioStore.submitError("");
          }
        },
        (error) => {
          this.formularioStore.submitError(error);
        }
      );
  }

  generarPMFIC() {
    this.generarStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaArchivosService
      .consolidadoPMFIC(this.idPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.consolidadoPMFIC = res;
            // this.verDescargaPMFIC = false;
            this.disableDescargarPMFIC = false;
            this.disableAdjuntarPMFIC = false;
            this.generarStore.submitSuccess();
          } else {
            this.toast.error(res?.message);
            this.disableDescargarPMFIC = true;
            this.disableAdjuntarPMFIC = true;
            this.generarStore.submitError("");
          }
        },
        (err) => {
          this.toast.warn(
            "Para generar el consolidado del Plan Operativo para concesiones - PMFIC. Debe tener todos los items completados previamente."
          );
          this.generarStore.submitError(err);
        }
      );
  }

  descargarPMFIC() {
    if (isNullOrEmpty(this.consolidadoPMFIC)) {
      this.toast.warn("Debe generar archivo consolidado");
      return;
    }
    descargarArchivo(this.consolidadoPMFIC);
  }

  registrarArchivoIdPF(event: any) {
    if (event) {
      this.verEnviar = true;
      this.disablePlanFormulado = false;
    }
  }

  registrarArchivoId(event: any) {
    if (event) {
      this.verEnviar = true;
      this.disablePlanFormulado = false;
    }
  }

  enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EMDPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  guardarPlanFormulado() {
    this.planFormuladoStore.submit();
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.planFormuladoStore.submitSuccess();
          this.toast.ok("Esta solicitud ha sido Formulada.");
          this.enviarARFFS = true
          this.disablePlanFormulado = true;
          this.disableAgregarTUPA = false;
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }

  actualizarPlanManejoEstado(param: any) {
    this.ARFFSStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok(response?.message);
          this.enviarARFFS = false
          this.disableEnviarARFFS = true;
          this.registrarNotificacion();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarNotificacion() {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: this.CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      numDocgestion: this.idPlanManejo,
      mensaje: 'Se informa que se ha registrado el plan '
        + this.CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO
        + ' Nro. '
        + this.idPlanManejo
        + ' para evaluación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.user.idUsuario,
      //codigoPerfil: this.user.usuario.sirperfil,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 10,
      url: '/planificacion/evaluacion/bandeja-eval-pmfi-dema',
      idUsuarioRegistro: this.user.idUsuario
    }
    this.notificacionService
      .registrarNotificacion(params)
      .subscribe()
  }

  isAllSelected(isSaved: boolean) {
    this.masterSelected = this.listProcesos.every(function (item: any) {
      return item.marcar == true;
    });

    if (isSaved) {
      if (this.masterSelected === true) {
        this.disableGenerarPMFIC = false;
      } else {
        this.disableGenerarPMFIC = true;
      }
    }
  }

  registrarLongitudTUPA(event: number) {
    if (event > 0) {
      this.disableEnviarARFFS = false;
    } else {
      this.disableEnviarARFFS = true;
    }
  }
}
