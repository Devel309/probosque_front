import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { descargarArchivo, ToastService } from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PmficInformacionGeneral } from 'src/app/model/pmfic-informacion-general';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { AnexosService } from 'src/app/service/plan-operativo-concesion-maderable/anexos.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { ModalDescripcionComponent } from '../../modal/modal-descripcion/modal-descripcion.component';
import { ModalFormularioCategoriaComponent } from '../../modal/modal-formulario-categoria/modal-formulario-categoria.component';
import { ModalFormularioFinesManerablesComponent } from '../../modal/modal-formulario-fines-maderables/modal-formulario-fines-maderables.component';
import {environment} from '@env/environment';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {Router} from '@angular/router';
import { UrlFormatos } from 'src/app/model/urlFormatos';

@Component({
  selector: 'app-tab-potencial-produccion-forestal',
  templateUrl: './tab-potencial-produccion-forestal.component.html',
})
export class TabPotencialProduccionForestalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() isPerfilArffs!: boolean;
  ref: DynamicDialogRef = new DynamicDialogRef();

  actividades = [
    {
      practicaSilvicultural: 'respeto por árboles semilleros',
      descripcion: 'string',
    },
    {
      practicaSilvicultural: 'respeto por árboles semilleros',
      descripcion: '',
    },
  ];

  archivo: any;
  cargarServicios= false;
  CodigoPMFIC = CodigoPMFIC;
  form: PmficInformacionGeneral = new PmficInformacionGeneral();
  idInformacionGeneral: number = 0;
  nroArbolesMaderables!: number | null;
  nroArbolesMaderablesSemilleros!: number | null;
  volumenM3rMaderables: string | number | null = '';
  nroArbolesNoMaderables!: number | null;
  nroArbolesNoMaderablesSemilleros!: number | null;
  volumenM3rNoMaderables: string | number | null = '';
  superficieHaNoMaderables: string | number | null = '';
  tieneCenso: boolean = false;

  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_8;

  codigoAcordeon_8: string = CodigoPMFIC.TAB_8;

  detEvaluacion_8: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_8,
  });

  evaluacion: any;
  nombreGenerado: string = "";
  plantillaPMFIC: string = "";

  constructor(
    public dialogService: DialogService,
    private censoForestalService: CensoForestalService,
    private dialog: MatDialog,
    private toast: ToastService,private evaluacionService: EvaluacionService,
    private informacionGeneralService: InformacionGeneralService,
    private anexosService: AnexosService,
    private usuarioService: UsuarioService,private router: Router,
    private archivoService: ArchivoService
  ) {
    this.archivo = {
      file: null,
    };
  }

  ngOnInit() {

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  openModalCategorias(mensaje: string, observacion: any, edit: any) {
    this.ref = this.dialogService.open(ModalFormularioCategoriaComponent, {
      header: mensaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: observacion,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        observacion.categoria = resp.categoria;
        observacion.usoPotencial = resp.usoPotencial;
        observacion.actividades = resp.actividades;
        observacion.editarForm = false;
      }
    });
  }

  openModalFinesMaderables(mensaje: string, finesMaderables: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioFinesManerablesComponent,
      {
        header: mensaje,
        width: '50%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        baseZIndex: 10000,
        data: {
          item: finesMaderables,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        finesMaderables.actividades = resp.actividades;
        finesMaderables.descripcionDeSistema = resp.descripcionDeSistema;
        finesMaderables.maquinaria = resp.maquinaria;
        finesMaderables.persona = resp.persona;
        finesMaderables.observaciones = resp.observaciones;
        finesMaderables.editarForm = false;
      }
    });
  }
  openModalDescripcion(mensaje: string, descripcion: any, edit: any) {
    this.ref = this.dialogService.open(ModalDescripcionComponent, {
      header: mensaje,
      width: '30%',
      contentStyle: { 'max-height': '200px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: descripcion,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        descripcion = resp;
      }
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  varAssets = `${environment.varAssets}`;

/*   btnDescargarFormato() {


    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }

    window.location.href = urlExcel;
} */

btnDescargarFormato() {
  this.dialog.open(LoadingComponent, { disableClose: true });
  this.nombreGenerado = UrlFormatos.CARGA_MASIVA_PMFIC;
  this.archivoService
    .descargarPlantilla(this.nombreGenerado)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((res: any) => {
      if (res.isSuccess == true) {
        this.toast.ok(res?.message);
        this.plantillaPMFIC = res;
        descargarArchivo(this.plantillaPMFIC);
      } else {
        this.toast.error(res?.message);
      }
    });
}

  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService
      .registrarCargaMasiva(this.archivo.file)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registró el archivo correctamente..\n');
          //  Agregar servicios de consumo para esta pantalla
          this.actualizarInformacionGeneral();
          this.cargarServicios= true;
          this.archivo.file = '';
        } else {
          this.toast.error('Error al procesar archivo, el archivo seleccionado no cumple con el formato de carga masiva.');
        }
      });
  }

  actualizarInformacionGeneral() {
    forkJoin([
      this.listAnexo5(),
      this.listAnexo6(),
      this.listAnexo7(),
      this.listarInformacionGeneral()
    ]).pipe(finalize(() => {
      this.editarInformacionGeneral();
    })).subscribe();
  }

  listAnexo5() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.CodigoPMFIC.CODIGO_PROCESO,
    };
    return this.anexosService
      .Anexo2(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroAM = 0;
          let nroAMS = 0;
          let vM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroAM = nroAM + 1;
            }
            if (element.categoria == 'S') {
              nroAMS = nroAMS + 1;
            }
            if (element.volumen) {
              vM = vM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesMaderables = nroAM;
          this.nroArbolesMaderablesSemilleros = nroAMS;
          this.volumenM3rMaderables = vM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesMaderables = null;
          this.nroArbolesMaderablesSemilleros = null;
          this.volumenM3rMaderables = null;
        }
      }));
  }

  listAnexo6() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.CodigoPMFIC.CODIGO_PROCESO,
    };
    return this.anexosService
      .Anexo6(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let sNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.areaBosque) {
              sNM = sNM + parseFloat(element.areaBosque);
            }
          });
          this.superficieHaNoMaderables = sNM;
          this.tieneCenso = true;
        } else {
          this.superficieHaNoMaderables = null;
        }
      }));
  }

  listAnexo7() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.CodigoPMFIC.CODIGO_PROCESO,
    };
    return this.anexosService
      .Anexo7NoMaderable(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroANM = 0;
          let nroANMS = 0;
          let vNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroANM = nroANM + 1;
            }
            if (element.categoria == 'S') {
              nroANMS = nroANMS + 1;
            }
            if (element.volumen) {
              vNM = vNM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesNoMaderables = nroANM;
          this.nroArbolesNoMaderablesSemilleros = nroANMS;
          this.volumenM3rNoMaderables = vNM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesNoMaderables = null;
          this.nroArbolesNoMaderablesSemilleros = null;
          this.volumenM3rNoMaderables = null;
        }
      }));
  }

  listarInformacionGeneral() {
    var params = {
      codigoProceso: this.CodigoPMFIC.CODIGO_PROCESO,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    return this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.form = new PmficInformacionGeneral(element);
            this.idInformacionGeneral = element.idInformacionGeneralDema;
          });
        }
      }));
  }

  editarInformacionGeneral() {
    

    if (this.tieneCenso) {
      this.form.nroArbolesMaderables = this.nroArbolesMaderables === 0 ? 0 : this.nroArbolesMaderables ? this.nroArbolesMaderables : null;
      this.form.nroArbolesMaderablesSemilleros = this.nroArbolesMaderablesSemilleros === 0 ? 0 : this.nroArbolesMaderablesSemilleros ? this.nroArbolesMaderablesSemilleros : null;
      this.form.volumenM3rMaderables = this.volumenM3rMaderables ? String(this.volumenM3rMaderables) : null;
      this.form.nroArbolesNoMaderables = this.nroArbolesNoMaderables === 0 ? 0 : this.nroArbolesNoMaderables ? this.nroArbolesNoMaderables : null;
      this.form.nroArbolesNoMaderablesSemilleros = this.nroArbolesNoMaderablesSemilleros === 0 ? 0 : this.nroArbolesNoMaderablesSemilleros ? this.nroArbolesNoMaderablesSemilleros : null;
      this.form.volumenM3rNoMaderables = this.volumenM3rNoMaderables ? String(this.volumenM3rNoMaderables) : null;
      this.form.superficieHaNoMaderables = this.superficieHaNoMaderables ? String(this.superficieHaNoMaderables) : null;

      var params = new PmficInformacionGeneral(this.form);
      params.idPlanManejo = this.idPlanManejo;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = this.CodigoPMFIC.CODIGO_PROCESO;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
          }
        });
    }
  }


  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_8 = Object.assign(this.detEvaluacion_8,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_8));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.usuarioService.idUsuario,
            };
          }
        }
      });

  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_8])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_8);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){


    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6278"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
