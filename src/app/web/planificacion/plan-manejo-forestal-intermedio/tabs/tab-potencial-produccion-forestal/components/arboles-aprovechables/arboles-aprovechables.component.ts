import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { CensoForestalService } from "src/app/service/censoForestal";

@Component({
  selector: "app-arboles-aprovechables",
  templateUrl: "./arboles-aprovechables.component.html",
  styleUrls: ["./arboles-aprovechables.component.scss"],
})
export class ArbolesAprovechablesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() set cargarServicio(b: boolean) {
    if (b) {
      this.listArbolesAprovechables();
    }
  }
  colTipoBosque: any[] = [];
  listEspecies: any[] = [];
  liscols: any[] = ["Número de árboles (N)", "Volumen comercial (vc)"];
  lisVr: any[] = ["N°", "%", "m3 (r)", "%"];
  listValores: any[] = [];
  tipoBosqueDtos: any[] = [];
  nHaInventariadas: number = 0;
  totalArboles: number = 0;
  totalVolumen: number = 0;
  totales: any[] = [];
  totalN: number = 0;
  totalP: number = 0;
  totalV: number = 0;
  totalPV: number = 0;
  totalArbolesA: number = 0;
  totalVolumenB: number = 0;
  otraLista: any[] = [];
  listTot: any = [];

  constructor(private censoForestalService: CensoForestalService) {}

  ngOnInit(): void {
    this.listArbolesAprovechables();
  }

  listArbolesAprovechables() {
    var params = {
      //idPlanDeManejo: 1441,
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: "PMFIC",
    };
    this.censoForestalService
      .arbolesAprovechablesMaderables(params)
      .subscribe((response: any) => {
        if (!!response.data.listaTipoBosque) {
          this.colTipoBosque = [...response.data.listaTipoBosque];
          this.listEspecies = [
            ...response.data.tablaArbolesAprovechablesMaderablesDtoList,
          ];
          this.nHaInventariadas = response.data.nHaInventariadas;

          this.colTipoBosque.forEach((item) => {
            this.liscols.push(
              "Número de árboles (N)",
              "Volumen comercial (vc)"
            );
            this.lisVr.push("N°", "%", "m3 (r)", "%");
          });

          this.totalArbolesA = 0;
          this.totalVolumenB = 0;

          //totales especies x tipo de bosque
          this.colTipoBosque.forEach((x: any, index: number) => {
            let totA: number = 0;
            let totV: number = 0;
            this.listEspecies.forEach((element: any) => {
                totA += Number(element.tipoBosqueDtos[index].nArboles);
                totV += Number(element.tipoBosqueDtos[index].volumenComercial);
            });
            this.listTot.push(
              { 
                totalA: totA != 0 ? totA : 1,
                totalV: totV != 0 ? totV : 1,
              }
            )
          });

          this.listEspecies.forEach((element: any) => {
            this.tipoBosqueDtos = element.tipoBosqueDtos;
            this.totalArboles = Number(element.nArbolesTotal);
            this.totalVolumen =
              Number(element.volumenTotal) != 0
                ? Number(element.volumenTotal)
                : 1;

            this.totalArbolesA += Number(element.nArbolesTotal);
            this.totalVolumenB += Number(element.volumenTotal);

            this.listValores = [];
            element.tipoBosqueDtos.forEach((item: any, index: number) => {
              this.listValores.push(
                item.nArboles,
                // ((Number(item.nArboles) * 100) / this.totalArboles).toFixed(2),
                ((Number(item.nArboles) * 100) / this.listTot[index].totalA).toFixed(2),
                item.volumenComercial,
                // ((Number(item.volumenComercial) * 100) / this.totalVolumen).toFixed(2)
                ((Number(item.volumenComercial) * 100) / this.listTot[index].totalV).toFixed(2)
              );
            });
            this.otraLista.push({
              ...element,
              listaEspecies: this.listValores,
            });
          });
        }
        this.calcularTotales();
      });
  }

  calcularTotales() {
    this.colTipoBosque.forEach((x: any, index: number) => {
      this.totalN = 0;
      this.totalP = 0;
      this.totalV = 0;
      this.totalPV = 0;
      this.listEspecies.forEach((element: any) => {
        this.totalArboles = Number(element.nArbolesTotal);
        this.totalVolumen =
          Number(element.volumenTotal) != 0 ? Number(element.volumenTotal) : 1;
        this.totalN += Number(element.tipoBosqueDtos[index].nArboles);
        this.totalP += Number(
          (
            (Number(element.tipoBosqueDtos[index].nArboles) * 100) /
            // this.totalArboles
            this.listTot[index].totalA
          ).toFixed(2)
        );
        this.totalV += Number(Number(element.tipoBosqueDtos[index].volumenComercial).toFixed(4));
        this.totalPV += Number(
          (
            (Number(element.tipoBosqueDtos[index].volumenComercial) * 100) /
            // this.totalVolumen
            this.listTot[index].totalV
          ).toFixed(2)
        );
      });
      this.totalP = Number(this.totalP.toFixed(2));
      this.totalV = Number(this.totalV.toFixed(4));
      this.totalPV = Number(this.totalPV.toFixed(2));

      this.totales.push(this.totalN, this.totalP, this.totalV, this.totalPV);
    });
  }
}
