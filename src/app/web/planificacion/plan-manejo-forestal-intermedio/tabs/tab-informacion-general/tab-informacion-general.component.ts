import { HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Input } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DepartamentoModel, DistritoModel, ProvinciaModel } from '@models';
import { CoreCentralService, UsuarioService } from '@services';
import { isNullOrEmpty, ToastService } from '@shared';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PmficInformacionGeneral, RegenteForestal } from 'src/app/model/pmfic-informacion-general';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { AnexosService } from 'src/app/service/plan-operativo-concesion-maderable/anexos.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { ModalInfoPermisoComponent } from 'src/app/shared/components/modal-info-permiso/modal-info-permiso.component';
import {Perfiles} from '../../../../../model/util/Perfiles';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab-informacion-general',
  templateUrl: './tab-informacion-general.component.html',
  styleUrls: ["./tab-informacion-general.component.scss"],
})
export class TabInformacionGeneralComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();
  @Input() idPlanManejo!: number;

  @Input() disabled!: boolean;

  @Input() isPerfilArffs!: boolean;


  CodigoPMFIC = CodigoPMFIC;
  form: PmficInformacionGeneral = new PmficInformacionGeneral();
  listDepartamento: DepartamentoModel[] = [];
  listProvincia: ProvinciaModel[] = [];
  listDistrito: DistritoModel[] = [];
  departamento = {} as DepartamentoModel;
  provincia = {} as ProvinciaModel;
  distrito = {} as DistritoModel;
  listCuenca: any[] = [];
  listSubcuenca: any[] = [];
  listCuencaSubcuenca: CuencaSubcuencaModel[] = [];
  idCuenca!: number | null;
  idSubcuenca!: number | null;
  edit: boolean = false;
  nombreComunidad: string = '';
  nombreJefeComunidad: string = '';
  documentoJefeComunidad: string = '';
  representanteLegal: string = '';
  selectRegente: RegenteForestal = new RegenteForestal();
  verModalRegente: boolean = false;
  queryRegente: string = '';
  regentes: any[] = [];
  nombreCompleto: string = "";
  domicilioLegal!: string | null;
  ref!: DynamicDialogRef;
  idPlanManejoPadre: number = 0;
  displayModal: boolean = false;
  displayModalSP1: boolean = false;
  displayModalSP2: boolean = false;
  displayModalSP3: boolean = false;
  minDate = moment(new Date()).format('YYYY-MM-DD');
  minDateFinal = moment(new Date()).add('days', 1).format('YYYY-MM-DD');
  disabledDate: boolean = false;
  listRegimenes: any[] = [];
  idInformacionGeneral: number = 0;
  idPermisoForestal: number = 0;
  idPermisoForestalTemp: number = 0;
  areatotal = null;
  nroTituloPropiedadComunidad = null;
  nroArbolesMaderables: number | null = 0;
  nroArbolesMaderablesSemilleros: number | null = 0;
  volumenM3rMaderables: string | number | null = '0';
  nroArbolesNoMaderables: number | null = 0;
  nroArbolesNoMaderablesSemilleros: number | null = 0;
  volumenM3rNoMaderables: string | number | null = '0';
  superficieHaNoMaderables: string | number | null = '0';
  showWarn: boolean = true;
  msgWarn: string = '';
  tieneCenso: boolean = false;


  //EVALUACION
  //Perfiles = Perfiles;

  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_1;

  codigoAcordeon_1_1: string = CodigoPMFIC.TAB_1_1;
  codigoAcordeon_1_2: string = CodigoPMFIC.TAB_1_2;
  codigoAcordeon_1_3: string = CodigoPMFIC.TAB_1_3;
  codigoAcordeon_1_4: string = CodigoPMFIC.TAB_1_4;
  codigoAcordeon_1_5: string = CodigoPMFIC.TAB_1_5;
  codigoAcordeon_1_6: string = CodigoPMFIC.TAB_1_6;
  codigoAcordeon_1_7: string = CodigoPMFIC.TAB_1_7;

  detEvaluacion_1_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_1,
  });

  detEvaluacion_1_2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_2,
  });

  detEvaluacion_1_3: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_3,
  });

  detEvaluacion_1_4: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_4,
  });

  detEvaluacion_1_5: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_5,
  });

  detEvaluacion_1_6: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_6,
  });

  detEvaluacion_1_7: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_1_7,
  });



  evaluacion: any;


  constructor(
    private servCoreCentral: CoreCentralService,
    private messageService: MessageService,
    private informacionGeneralService: InformacionGeneralService,
    private permisoForestalService: PermisoForestalService,
    private dialog: MatDialog,
    private dialogService: DialogService,
    private usuarioService: UsuarioService,
    private apiForestal: ApiForestalService,
    private toast: ToastService,
    private anexosService: AnexosService,private evaluacionService: EvaluacionService,private router: Router
  ) {}

  ngOnInit(): void {
    this.listarPorFiltroDepartamento();
    this.listarPorFilroProvincia();
    this.listarPorFilroDistrito();
    this.listarCuencaSubcuenca();
    this.cargarRegimenes();
    this.listarInformacionGeneral();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarAnexos() {
    forkJoin([
      this.listAnexo5(),
      this.listAnexo6(),
      this.listAnexo7(),
    ]).pipe(finalize(() => {
      if (this.tieneCenso === false) {
        this.toast.warn(this.msgWarn);
      }
    })).subscribe();
  }

  listAnexo5() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'PMFIC',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo2(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroAM = 0;
          let nroAMS = 0;
          let vM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroAM = nroAM + 1;
            }
            if (element.categoria == 'S') {
              nroAMS = nroAMS + 1;
            }
            if (element.volumen) {
              vM = vM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesMaderables = nroAM;
          this.nroArbolesMaderablesSemilleros = nroAMS;
          this.volumenM3rMaderables = vM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesMaderables = 0;
          this.nroArbolesMaderablesSemilleros = 0;
          this.volumenM3rMaderables = '0';
          if (this.showWarn) {
            this.showWarn = false;
            this.msgWarn = response.message;
            // this.toast.warn(response.message);
          }
        }
      }));
  }

  listAnexo6() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'PMFIC',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo6(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let sNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.areaBosque) {
              sNM = sNM + parseFloat(element.areaBosque);
            }
          });
          this.superficieHaNoMaderables = sNM;
          this.tieneCenso = true;
        } else {
          this.superficieHaNoMaderables = 0;
          if (this.showWarn) {
            this.showWarn = false;
            this.msgWarn = response.message;
            // this.toast.warn(response.message);
          }
        }
      }));
  }

  listAnexo7() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'PMFIC',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo7NoMaderable(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroANM = 0;
          let nroANMS = 0;
          let vNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroANM = nroANM + 1;
            }
            if (element.categoria == 'S') {
              nroANMS = nroANMS + 1;
            }
            if (element.volumen) {
              vNM = vNM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesNoMaderables = nroANM;
          this.nroArbolesNoMaderablesSemilleros = nroANMS;
          this.volumenM3rNoMaderables = vNM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesNoMaderables = 0;
          this.nroArbolesNoMaderablesSemilleros = 0;
          this.volumenM3rNoMaderables = '0';
          if (this.showWarn) {
            this.showWarn = false;
            this.msgWarn = response.message;
            // this.toast.warn(response.message);
          }
        }
      }));
  }

  listarPorFiltroDepartamento() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia() {
    this.servCoreCentral.listarPorFilroProvincia({}).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarPorFilroDistrito() {
    this.servCoreCentral.listarPorFilroDistrito({}).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  Distrito(param: any) {
    this.form.idDistritoRepresentante = param.value;
  }

  listarCuencaSubcuenca() {
    this.listCuencaSubcuenca = [];
    this.listCuenca = [];

    this.servCoreCentral.listarCuencaSubcuenca().subscribe(
      (result: any) => {
        this.listCuencaSubcuenca = result.data;

        let listC: any[] = [];
        this.listCuencaSubcuenca.forEach((item) => {
          listC.push(item);
        });

        const setObj = new Map();
        const unicos = listC.reduce((arr, cuenca) => {
          if (!setObj.has(cuenca.idCuenca)) {
            setObj.set(cuenca.idCuenca, cuenca)
            arr.push(cuenca)
          }
          return arr;
        }, []);

        this.listCuenca = unicos;
        this.listCuenca.sort((a, b) => a.cuenca - b.cuenca);

        if (!isNullOrEmpty(this.idCuenca) && !isNullOrEmpty(this.idSubcuenca)) {
          this.onSelectedCuenca({ value: Number(this.idCuenca) });
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  onSelectedCuenca(param: any) {
    this.listSubcuenca = [];

    this.listCuencaSubcuenca.forEach((item) => {
      if (item.idCuenca == param.value) {
        this.listSubcuenca.push(item);
      }
    });
  }

  listarInformacionGeneral() {
    this.edit = false;
    var params = {
      codigoProceso: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.edit = true;
          response.data.forEach((element: any) => {
            this.form = new PmficInformacionGeneral(element);

            this.distrito.idDistrito = element.idDistrito;
            this.provincia.idProvincia = element.idProvincia;
            this.departamento.idDepartamento = element.idDepartamento;
            this.idInformacionGeneral = element.idInformacionGeneralDema;
            this.idPermisoForestal = element.idPermisoForestal;
            this.nroArbolesMaderables = isNullOrEmpty(element.nroArbolesMaderables) ? 0 : element.nroArbolesMaderables;
            this.nroArbolesMaderablesSemilleros = isNullOrEmpty(element.nroArbolesMaderablesSemilleros) ? 0 : element.nroArbolesMaderablesSemilleros;
            this.volumenM3rMaderables = isNullOrEmpty(element.volumenM3rMaderables) ? '0' : element.volumenM3rMaderables;
            this.nroArbolesNoMaderables = isNullOrEmpty(element.nroArbolesNoMaderables) ? 0 : element.nroArbolesNoMaderables;
            this.nroArbolesNoMaderablesSemilleros = isNullOrEmpty(element.nroArbolesNoMaderablesSemilleros) ? 0 : element.nroArbolesNoMaderablesSemilleros;
            this.volumenM3rNoMaderables = isNullOrEmpty(element.volumenM3rNoMaderables) ? '0' : element.volumenM3rNoMaderables;
            this.superficieHaNoMaderables = isNullOrEmpty(element.superficieHaNoMaderables) ? '0' : element.superficieHaNoMaderables;
            this.idCuenca = isNullOrEmpty(element.cuenca) ? null : Number(element.cuenca);
            this.onSelectedCuenca({ value: this.idCuenca });
            this.idSubcuenca = isNullOrEmpty(element.subCuenca) ? null : Number(element.subCuenca);

            this.form.idDistritoRepresentante =  element.idDistrito;
            if (!isNullOrEmpty(element.fechaInicioDema)) {
              this.form.fechaInicioDema = new Date(element.fechaInicioDema);
            }
            if (!isNullOrEmpty(element.fechaFinDema)) {
              this.form.fechaFinDema = new Date(element.fechaFinDema);
            }
            element.nombreElaboraDema = !isNullOrEmpty(element.nombreElaboraDema) ? element.nombreElaboraDema : '';
            element.apellidoPaternoElaboraDema = !isNullOrEmpty(element.apellidoPaternoElaboraDema) ? element.apellidoPaternoElaboraDema : '';
            element.apellidoMaternoElaboraDema = !isNullOrEmpty(element.apellidoMaternoElaboraDema) ? element.apellidoMaternoElaboraDema : '';
            element.nombreRepresentante = !isNullOrEmpty(element.nombreRepresentante) ? element.nombreRepresentante : '';
            element.apellidoPaternoRepresentante = !isNullOrEmpty(element.apellidoPaternoRepresentante) ? element.apellidoPaternoRepresentante : '';
            element.apellidoMaternoRepresentante = !isNullOrEmpty(element.apellidoMaternoRepresentante) ? element.apellidoMaternoRepresentante : '';
            if (element.codTipoPersona === 'TPERJURI') {
              this.nombreComunidad = element.nombreElaboraDema;
              this.nombreJefeComunidad = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;
              this.documentoJefeComunidad = element.documentoRepresentante;
            } else {
              this.nombreJefeComunidad = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaboraDema + ' ' + element.apellidoMaternoElaboraDema;
              this.documentoJefeComunidad = element.dniElaboraDema;
            }
            this.representanteLegal = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;

            if (!isNullOrEmpty(element.regente)) {
              this.nombreCompleto = element.regente.nombres + ' ' + element.regente.apellidos;
              this.domicilioLegal = element.regente.domicilioLegal;
            }
          });
        } else {
          this.listRegimenes.forEach((item) => {
            this.form.lstUmf.push(item);
          });
          this.listarAnexos();
        }
      });
  }

  openModal() {
    this.ref = this.dialogService.open(ModalInfoPermisoComponent, {
      header: "Buscar Solicitud de Permiso Forestal",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        buscar: true,
        nroDocumento: this.usuarioService.nroDocumento,
        idTipoDocumento: this.usuarioService.idtipoDocumento,
        idPlanManejo: this.idPlanManejo,
        idPermisoForestal: this.idPermisoForestal
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.displayModal = true;
        this.idPermisoForestalTemp = resp.idPermisoForestal;
        this.areatotal = resp.areaComunidad;
        this.nroTituloPropiedadComunidad = resp.nroTituloPropiedad;
      }
    });
  }

  closeModal() {
    this.displayModal = false;
    this.listarInformacionGeneralPermisoForestal();
  }

  listarInformacionGeneralPermisoForestal() {
    var params = {
      codTipoInfGeneral: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idPermisoForestal: this.idPermisoForestalTemp,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .listarInformacionGeneralPF(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];

          this.form = new PmficInformacionGeneral();

          this.distrito.idDistrito = element.idDistrito;
          this.provincia.idProvincia = element.idProvincia;
          this.departamento.idDepartamento = element.idDepartamento;

          this.form.idPermisoForestal = element.idPermisoForestal;
          this.form.codTipoPersona =  element.codTipoPersona;
          this.form.esReprLegal = element.esReprLegal;
          this.form.idDistritoRepresentante =  element.idDistrito;
          this.form.areaTotal = this.areatotal;
          this.form.nroTituloPropiedadComunidad = this.nroTituloPropiedadComunidad;
          this.form.superficieHaMaderables = '';
          this.form.vigencia = 0;
          this.form.fechaInicioDema = '';
          this.form.fechaFinDema = '';
          this.form.federacionComunidad = '';
          this.form.nroAnexosComunidad = 0;
          this.form.volumenM3rMaderables = '';

          element.nombreElaborador = !isNullOrEmpty(element.nombreElaborador) ? element.nombreElaborador : '';
          element.apellidoPaternoElaborador = !isNullOrEmpty(element.apellidoPaternoElaborador) ? element.apellidoPaternoElaborador : '';
          element.apellidoMaternoElaboraDema = !isNullOrEmpty(element.apellidoMaternoElaborador) ? element.apellidoMaternoElaborador : '';
          if (element.codTipoPersona === 'TPERJURI') {
            this.form.nombreElaboraDema = element.federacionComunidad;
            this.form.nroRucComunidad = element.rucComunidad;
            this.form.direccionLegalTitular = element.domicilioLegal;
            this.form.codTipoDocumentoRepresentante = element.tipoDocumentoElaborador;
            this.form.documentoRepresentante = element.documentoElaborador;
            this.form.nombreRepresentante = element.nombreElaborador;
            this.form.apellidoPaternoRepresentante = element.apellidoPaternoElaborador;
            this.form.apellidoMaternoRepresentante = element.apellidoMaternoElaborador;
            this.form.dniElaboraDema = element.rucComunidad;

            this.nombreComunidad = element.federacionComunidad;
            this.nombreJefeComunidad = element.nombreElaborador + ' ' + element.apellidoPaternoElaborador + ' ' + element.apellidoMaternoElaborador;
            this.documentoJefeComunidad = element.documentoElaborador;
            this.representanteLegal = element.nombreElaborador + ' ' + element.apellidoPaternoElaborador + ' ' + element.apellidoMaternoElaborador;
          } else {
            this.form.nombreElaboraDema = element.nombreElaborador;
            this.form.apellidoPaternoElaboraDema = element.apellidoPaternoElaborador;
            this.form.apellidoMaternoElaboraDema = element.apellidoMaternoElaborador;
            this.form.dniElaboraDema = element.documentoElaborador;

            this.nombreJefeComunidad = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaborador + ' ' + element.apellidoMaternoElaborador;
            this.documentoJefeComunidad = element.documentoElaborador;
          }

          this.form.regente = new RegenteForestal();

          if (isNullOrEmpty(element.lstUmf)) {
            this.listRegimenes.forEach((item) => {
              this.form.lstUmf.push(item);
            });
          }
        }
      });
  }

  abrirModalRegentes() {
    this.listarRegentes();
    this.selectRegente = new RegenteForestal();
    this.verModalRegente = true;
    this.queryRegente = '';
  }

  listarRegentes() {
    this.apiForestal.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {
        this.regentes.push({
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
          domicilioLegal: element.domicilioLegal
        });
      });
    });
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegentes();
    }
  }

  guardarRegente() {
    this.nombreCompleto = this.selectRegente.nombres + ' ' + this.selectRegente.apellidos;
    this.domicilioLegal = this.selectRegente.domicilioLegal;
    this.form.regente = new RegenteForestal(this.selectRegente);
    this.form.regente.idPlanManejo = this.idPlanManejo;
    this.form.regente.idUsuarioRegistro = this.usuarioService.idUsuario;
    this.verModalRegente = false;
  }

  cargarShapefile1() {
    this.displayModalSP1 = true;
  }

  cargarShapefile2() {
    this.displayModalSP2 = true;
  }

  cargarShapefile3() {
    this.displayModalSP3 = true;
  }

  calcularArea(area: any, tipo: string) {
    if (tipo === CodigoPMFIC.TAB_1_1) {
      this.form.areaTotal = area;
    } else if (tipo === CodigoPMFIC.TAB_1_2) {
      this.form.areaBosqueProduccionForestal = area;
    } else if (tipo === CodigoPMFIC.TAB_1_4) {
      this.form.superficieHaMaderables = area;
    }
  }

  guardarInformacionGeneral() {
    if (this.domicilioLegal) this.form.regente.domicilioLegal = this.domicilioLegal;
    this.form.fechaElaboracionPmfi = new Date();
    this.form.fechaInicioDema = new Date(this.form.fechaInicioDema);
    this.form.fechaFinDema = new Date(this.form.fechaFinDema);
    this.form.nroArbolesMaderables = this.nroArbolesMaderables === 0 ? 0 : this.nroArbolesMaderables ? this.nroArbolesMaderables : null;
    this.form.nroArbolesMaderablesSemilleros = this.nroArbolesMaderablesSemilleros === 0 ? 0 : this.nroArbolesMaderablesSemilleros ? this.nroArbolesMaderablesSemilleros : null;
    this.form.volumenM3rMaderables = this.volumenM3rMaderables ? String(this.volumenM3rMaderables) : null;
    this.form.nroArbolesNoMaderables = this.nroArbolesNoMaderables === 0 ? 0 : this.nroArbolesNoMaderables ? this.nroArbolesNoMaderables : null;
    this.form.nroArbolesNoMaderablesSemilleros = this.nroArbolesNoMaderablesSemilleros === 0 ? 0 : this.nroArbolesNoMaderablesSemilleros ? this.nroArbolesNoMaderablesSemilleros : null;
    this.form.volumenM3rNoMaderables = this.volumenM3rNoMaderables ? String(this.volumenM3rNoMaderables) : null;
    this.form.superficieHaNoMaderables = this.superficieHaNoMaderables ? String(this.superficieHaNoMaderables) : null;
    this.form.cuenca = this.idCuenca ? String(this.idCuenca) : null;
    this.form.subCuenca = this.idSubcuenca ? String(this.idSubcuenca) : null;

    if (this.validationFields(this.form)) {
      var params = new PmficInformacionGeneral(this.form);
      params.idPlanManejo = this.idPlanManejo;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .registrarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            // this.SuccessMensaje(response.message);
            this.registrarPermisoForestalPlanManejo();
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
            // this.ErrorMensaje(response.message)
          };
        });
    }
  }

  validationFields(data: PmficInformacionGeneral) {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!isNullOrEmpty(data.codTipoPersona)) {
      if (data.codTipoPersona === 'TPERJURI') {
        if (!this.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Nombre de la Comunidad.\n";
        }
      }
    } else {
      if (!this.nombreJefeComunidad) {
        if (!this.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Nombre de la Comunidad.\n";
        }
      }
    }
    if (!this.nombreJefeComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre del jefe de la comunidad.\n";
    }
    if (!this.documentoJefeComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nº DNI del jefe de la comunidad.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  registrarPermisoForestalPlanManejo() {
    let listPlanManejo: any[] = [];
    var params = {
      idPermisoForestal: this.idPermisoForestalTemp,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    listPlanManejo.push(params);

    this.permisoForestalService
      .registrarPermisoForestalPlanManejo(listPlanManejo)
      .subscribe();
  }

  editarInformacionGeneral() {
    if (this.domicilioLegal) this.form.regente.domicilioLegal = this.domicilioLegal;
    this.form.fechaElaboracionPmfi = new Date();
    this.form.fechaInicioDema = new Date(this.form.fechaInicioDema);
    this.form.fechaFinDema = new Date(this.form.fechaFinDema);
    this.form.nroArbolesMaderables = this.nroArbolesMaderables === 0 ? 0 : this.nroArbolesMaderables ? this.nroArbolesMaderables : null;
    this.form.nroArbolesMaderablesSemilleros = this.nroArbolesMaderablesSemilleros === 0 ? 0 : this.nroArbolesMaderablesSemilleros ? this.nroArbolesMaderablesSemilleros : null;
    this.form.volumenM3rMaderables = this.volumenM3rMaderables ? String(this.volumenM3rMaderables) : null;
    this.form.nroArbolesNoMaderables = this.nroArbolesNoMaderables === 0 ? 0 : this.nroArbolesNoMaderables ? this.nroArbolesNoMaderables : null;
    this.form.nroArbolesNoMaderablesSemilleros = this.nroArbolesNoMaderablesSemilleros === 0 ? 0 : this.nroArbolesNoMaderablesSemilleros ? this.nroArbolesNoMaderablesSemilleros : null;
    this.form.volumenM3rNoMaderables = this.volumenM3rNoMaderables ? String(this.volumenM3rNoMaderables) : null;
    this.form.superficieHaNoMaderables = this.superficieHaNoMaderables ? String(this.superficieHaNoMaderables) : null;
    this.form.cuenca = this.idCuenca ? String(this.idCuenca) : null;
    this.form.subCuenca = this.idSubcuenca ? String(this.idSubcuenca) : null;

    if (this.validationFields(this.form)) {
      var params = new PmficInformacionGeneral(this.form);
      params.idPlanManejo = this.idPlanManejo;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            // this.SuccessMensaje(response.message);
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
            // this.ErrorMensaje(response.message);
          }
        });
    }
  }

  handleAdmDateChange() {
    this.validarInformacion();

    let fechaInicial = moment(this.form.fechaInicioDema);
    let fechaFinal = moment(this.form.fechaFinDema);

    if (fechaFinal.diff(fechaInicial, 'days') >= 0) {
      this.form.vigencia = fechaFinal.diff(fechaInicial, 'years');
      this.disabledDate = false;
    } else {
      this.toast.error('Fecha Fin debe ser posterior o igual a Fecha Inicio');
      this.disabledDate = true;
    }
  }

  validarInformacion() {
    let fechaActual = moment();
    let fechaInicio = moment(this.form.fechaInicioDema);

    if (!!this.form.fechaInicioDema && !!this.form.fechaFinDema) {
      if (fechaInicio.diff(fechaActual, 'days') < 0) {
        this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
        this.disabledDate = true;
        return;
      }

      if (this.form.fechaFinDema <= this.form.fechaInicioDema) {
        this.disabledDate = true;
        this.toast.error('La Fecha Final debe ser posterior a la Fecha Inicio');
        return;
      }
    } else if (fechaInicio.diff(fechaActual, 'days') < 0) {
      this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
      this.disabledDate = true;
      return;
    }
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ severity: "success", summary: "", detail: mensaje });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  cargarRegimenes = () => {
    this.listRegimenes.push(
      {
        codigo: "001",
        descripcion: 'En la UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR.',
        detalle: 'NO'
      },
      {
        codigo: "002",
        descripcion: 'La UMF se ubica en zona prioritaria para el Estado.',
        detalle: 'NO'
      },
      {
        codigo: "003",
        descripcion: 'Se realizan proyectos integrales de transformación.',
        detalle: 'NO'
      },
      {
        codigo: "004",
        descripcion: 'La UMF cuenta con certificación forestal voluntaria de buenas prácticas.',
        detalle: 'NO'
      },
      {
        codigo: "005",
        descripcion: 'La UMF cuenta con certificación forestal voluntaria de buenas prácticas por más de 5 años.',
        detalle: 'NO'
      },
      {
        codigo: "006",
        descripcion: 'Se emitió el informe de evaluación o "scoping" por la empresa certificadora.',
        detalle: 'NO'
      },
      {
        codigo: "007",
        descripcion: 'Se reporta a la ARFFS y al SERFOR los resultados de las parcelas permanentes de muestreo.',
        detalle: 'NO'
      },
      {
        codigo: "008",
        descripcion: 'Se protege, conserva y/o recupera áreas no destinadas al aprovechamiento forestal.',
        detalle: 'NO'
      },
      {
        codigo: "009",
        descripcion: 'La UMF se encuentra bajo manejo de uso múltiple.',
        detalle: 'NO'
      }
    );
  };

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }


  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_1_1 = Object.assign(this.detEvaluacion_1_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_1));
              this.detEvaluacion_1_2 = Object.assign(this.detEvaluacion_1_2,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_2));
              this.detEvaluacion_1_3 = Object.assign(this.detEvaluacion_1_3,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_3));
              this.detEvaluacion_1_4 = Object.assign(this.detEvaluacion_1_4,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_4));
              this.detEvaluacion_1_5 = Object.assign(this.detEvaluacion_1_5,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_5));
              this.detEvaluacion_1_6 = Object.assign(this.detEvaluacion_1_6,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_6));
              this.detEvaluacion_1_7 = Object.assign(this.detEvaluacion_1_7,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_1_7));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.usuarioService.idUsuario,
            };
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_1_1,this.detEvaluacion_1_2,this.detEvaluacion_1_3,this.detEvaluacion_1_4,
      this.detEvaluacion_1_5,this.detEvaluacion_1_6,this.detEvaluacion_1_7])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_1);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_2);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_3);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_4);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_5);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_6);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_1_7);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6231"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export interface CuencaSubcuencaModel {
  codCuenca: string,
  codSubCuenca: string,
  codigo: string,
  cuenca: string,
  idCuenca: number,
  idSubCuenca: number,
  subCuenca: string,
  valor: string
}
