import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ModalFormularioAprovechamientoForestalComponent } from "../../modal/modal-formulario-aprovechamiento-forestal/modal-formulario-aprovechamiento-forestal.component";
import {ModalActividadesComponent} from '../../../plan-operativo-ccnn-ealta/tabs/tab-resumen-actividades/modal/modal.actividades/modal.actividades.component';
import {ModelParticipacionComunal} from '../../../plan-general-manejo/tabs/tab-participacion-comunal/tab-participacion-comunal.component';
import {UsuarioService} from '@services';
import {OrganizacionActividadModel} from '../../../../../model/OrganizacionActividad';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ToastService} from '@shared';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {MatDialog} from '@angular/material/dialog';
import {ActividadesSilviculturalesService} from '../../../../../service/planificacion/plan-general-manejo-pgmfa/actividadesSilviculturales.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ActividadSilvicultural} from '../../../../../model/ActividadesSilviculturalesModel';
import {CodigoPMFIC} from '../../../../../model/util/PMFIC/CodigoPMFIC';
import {CodigoUtil} from '../../../../../model/util/CodigoUtil';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {finalize} from 'rxjs/operators';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {Router} from '@angular/router';

@Component({
  selector: "app-tab-desarrollo-actividades",
  templateUrl: "./tab-desarrollo-actividades.component.html",
})
export class TabDesarrolloActividadesComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;
  /*maderable = [
    { actividad: "Aprovechamiento forestal maderable", organizacion: "" },
    { actividad: "Planificación", organizacion: "" },
    { actividad: "Reclutamiento de personal", organizacion: "" },
    {actividad: "Construcción o rehabilitacion de campamentos ", organizacion: "",},
    { actividad: "Construcción o rehabilitacion de caminos", organizacion: "" },
    { actividad: "Mantenimiento de máquinas y equipos", organizacion: "" },
    { actividad: "Tumbado y trosado", organizacion: "" },
  ];

  noMaderable = [
    { actividad: "Aprovechamiento forestal no maderable", organizacion: "" },
    { actividad: "Reclutamiento de personal", organizacion: "" },
    {actividad: "Construcción o rehabilitacion de campamentos ", organizacion: "",},
    { actividad: "Construcción o rehabilitacion de caminos", organizacion: "" },
    { actividad: "Mantenimiento de máquinas y equipos", organizacion: "" },
  ];*/

  tituloModal: string = "Registrar Organización para el Desarrollo de la Actividad";

  /*MADERABLE :string = "PFMICORGMAD";
  NO_MADERABLE :string = "PFMICORGNMAD";*/
  idArchivoOrganigrama: number = 0;
  MADERABLE :string = "MAD";
  NO_MADERABLE :string = "NMAD";

  CodigoProceso = CodigoProceso;
  CodigoPMFIC = CodigoPMFIC;

  organizacion = {} as OrganizacionActividadModel;
  organizacionesList = [] as OrganizacionActividadModel[];
  organizacionesList2 = [] as OrganizacionActividadModel[];

  ref: DynamicDialogRef = new DynamicDialogRef();

  indexUpdate:number = -1;
  tipoTabla:string="";

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  displayBasic: boolean = false;
  edit: boolean = false;
  idActSilvicultural: number = 0;

  listDetalle: ActividadSilvicultural[] = [];

  CodigoUtil = CodigoUtil;

  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_11;

  codigoAcordeon_11: string = CodigoPMFIC.TAB_11;

  detEvaluacion_11: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_11,
  });

  evaluacion: any;

  constructor(private user: UsuarioService,
              private messageService: MessageService,
              private dialog: MatDialog,private router: Router,
              private confirmationService: ConfirmationService, private evaluacionService: EvaluacionService,
              private actividadesSilviculturalesService: ActividadesSilviculturalesService,
              private toast: ToastService) {}

  ngOnInit() {

    this.listarOrganizacionDesarrollo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarOrganizacionDesarrollo() {

    this.organizacionesList = [];
    this.organizacionesList2 = [];

    let params = {
      idPlanManejo: this.idPlanManejo,
      idTipo: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (!!response.data) {
          if (response.data.detalle.length != 0) {
            this.idActSilvicultural = response.data.idActividadSilvicultural;
            let listado = response.data.detalle;
            listado.forEach((e:any) => {

                if(e.idActividadSilviculturalDet == null && e.actividad != "Otros (especificar)"){
                  e.equipo = CodigoUtil.VALOR_DEFECTO
                }

               if(e.observacionDetalle == 'MAD'){
                 this.organizacionesList.push(e);
               }else if(e.observacionDetalle == 'NMAD') {
                 this.organizacionesList2.push(e);
               }

            });

          }
        }
      });
  }

  cerrarModal() {
    this.organizacion = {} as OrganizacionActividadModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = "Registrar Organización";
  }

  agregarOrganizacion() {
    if (!this.validarOrganizacion()) {
      return;
    }
    if (this.edit) {
      this.editarOrganizacion();
    } else {
      this.registrarOrganizacion();
    }
  }

  validarOrganizacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (
      this.organizacion.actividad == null ||
      this.organizacion.actividad == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Actividad Realizada.\n";
    }
    if (
      this.organizacion.descripcionDetalle == null ||
      this.organizacion.descripcionDetalle == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Forma de Organización.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  openEditarOrganizacion(organizacion: OrganizacionActividadModel,index:number, tipo:string) {
    this.organizacion = { ...organizacion };
    this.displayBasic = true;
    this.edit = true;
    this.indexUpdate = index;
    this.tituloModal = "Editar Organización para el desarrollo de actividades";
  }

  openEliminarOrganizacion(event: Event, index: number, organizacion: OrganizacionActividadModel, tipo:string) {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {

        this.tipoTabla = tipo;

        if (organizacion.idActividadSilviculturalDet != 0) {
          var params = [
            {
              estado: "I",
              idActividadSilviculturalDet:
              organizacion.idActividadSilviculturalDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesSilviculturalesService.eliminarActividadSilviculturalPFDM(params)
            .subscribe(
              (data: any) => {
                this.SuccessMensaje(data.message);

                if(this.tipoTabla == this.MADERABLE)
                  this.organizacionesList.splice(index, 1);
                else if(this.tipoTabla == this.NO_MADERABLE)
                  this.organizacionesList2.splice(index, 1);

                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          if(this.tipoTabla == this.MADERABLE)
            this.organizacionesList.splice(index, 1);
          else if(this.tipoTabla == this.NO_MADERABLE)
            this.organizacionesList2.splice(index, 1);
          this.dialog.closeAll();
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  validarOrganizacionOrganizacionDesarrollo = (): boolean => {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.organizacionesList.length === 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe de agregar al menos 1 registro";
    }

    if (this.organizacionesList2.length === 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe de agregar al menos 1 registro";
    }

    if (mensaje != "") this.ErrorMensaje(mensaje);
    return validar;
  };

  registrarOrganizacionesDesarrollo() {

    if (!this.validarOrganizacionOrganizacionDesarrollo()) return;

    this.listDetalle = [];

    this.organizacionesList.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    this.organizacionesList2.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    var params = {
      aprovechamiento: null,
      divisionAdministrativa: null,
      laboresSilviculturales: {
        idActSilvicultural: this.idActSilvicultural ? this.idActSilvicultural : 0,
        codigoTipoActSilvicultural: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
        actividad: null,
        descripcion: null,
        observacion: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listActividadSilvicultural: this.listDetalle,
      },
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService.guardarSistemaManejoPFDM(params)
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.dialog.closeAll();
            this.SuccessMensaje("Se registró la organización para el desarrollo de la actividad correctamente.");
            this.listarOrganizacionDesarrollo();
          } else this.ErrorMensaje(result.message);
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  }

  cargarIdArchivo(idArchivo: any) {
    
    this.idArchivoOrganigrama = idArchivo;
    //data.equipo = idArchivo+"";
  }


  registrarOrganizacion() {
    this.organizacion.idActividadSilviculturalDet = 0;
    if(this.tipoTabla == this.MADERABLE){
      this.organizacion.observacionDetalle = this.MADERABLE;
      this.organizacionesList.push(this.organizacion);
    }else if(this.tipoTabla == this.NO_MADERABLE){
      this.organizacion.observacionDetalle = this.NO_MADERABLE;
      this.organizacionesList2.push(this.organizacion);
    }
    this.tipoTabla = "";
    this.organizacion = {} as OrganizacionActividadModel;
    this.displayBasic = false;
  }

  editarOrganizacion() {
    this.displayBasic = false;
    this.tituloModal = "Registrar Organización para el desarrollo de actividades";
    this.edit = false;
    //this.organizacionesList[this.findIndexById(this.organizacion.idActividadSilviculturalDet)] = this.organizacion;
    if(this.indexUpdate>=0){
      if(this.organizacion.observacionDetalle == this.MADERABLE)
        this.organizacionesList[this.indexUpdate] = this.organizacion;
      else if(this.organizacion.observacionDetalle == this.NO_MADERABLE)
        this.organizacionesList2[this.indexUpdate] = this.organizacion;

      this.indexUpdate = -1;
    }

    this.organizacion = {} as OrganizacionActividadModel;
  }


  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.organizacionesList.length; i++) {
      if (this.organizacionesList[i].idActividadSilviculturalDet === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  abrirModal(tipo:string) {
    this.tipoTabla = tipo;
    this.displayBasic = true;
  }


  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {

    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_11 = Object.assign(this.detEvaluacion_11,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_11));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_11])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_11);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }

  }

  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6278"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

}
