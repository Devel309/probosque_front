import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SistemaManejoForestalService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import {
  OrdenamientoProteccionCabeceraModel,
  OrdenamientoProteccionDetalleModel,
} from "src/app/model/OrdenamientoProteccionUMFModel";
import {
  DetalleSistemaManejo,
  SistemaManejoPMFIC,
} from "src/app/model/PMFI/SistemaManejoPMFIC";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { OrdenamientoProteccionService } from "src/app/service/planificacion/plan-manejo-forestal-intermedio/ordenamiento.service";

import { FormMedidaProteccionComponent } from "./modal/form-medida-proteccion/form-medida-proteccion.component";
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';

@Component({
  selector: "tab-ordenamiento-proteccion",
  templateUrl: "./tab-ordenamiento-y-proteccion.component.html",
})
export class TabOrdenamientoProteccionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  ref: DynamicDialogRef = new DynamicDialogRef();
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;
  CodigoPMFIC = CodigoPMFIC;

  newList1: any = [];
  listCoordinatesAnexo1: any[] = [];
  newList2: any = [];
  listCoordinatesAnexo2: any = [];
  newList3: any = [];
  listCoordinatesAnexo3: any = [];

  usuario = {} as UsuarioModel;
  listGeneral: any[] = [];
  listBloquesVetices: any[] = [];
  listPrimerBloque: any[] = [];
  listAnioOperativo: any[] = [];
  listMedidadProteccion: any[] = [];
  cabeceraMedidadProteccion: SistemaManejoPMFIC = {} as SistemaManejoPMFIC;
  disabledBtn: boolean = false;

  objSuperficieUbicacionBloques = new OrdenamientoProteccionCabeceraModel();
  objSuperficieUbicacionPrimerBloque = new OrdenamientoProteccionCabeceraModel();
  objSuperficieUbicacion = new OrdenamientoProteccionCabeceraModel();
  objMedidasProteccion = new OrdenamientoProteccionCabeceraModel();

  isSubmitting$ = this.queryStore.selectSubmitting();
  isSubmittingSistemaManejo$ = this.queryStoreSistemaManejo.selectSubmitting();


  codigoProceso = CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO;
  codigoTab = CodigoPMFIC.TAB_7;

  codigoAcordeon_7: string = CodigoPMFIC.TAB_7;

  detEvaluacion_7: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_7,
  });


  evaluacion: any;

  constructor(
    public dialogService: DialogService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private toast: ToastService,
    private ordenamientoProteccionService: OrdenamientoProteccionService,
    private sistemaManejoForestalService: SistemaManejoForestalService,
    private queryStore: ButtonsCreateQuery,
    private createStore: ButtonsCreateStore,
    private queryStoreSistemaManejo: ButtonsCreateQuery,
    private createStoreSistemaManejo: ButtonsCreateStore,private evaluacionService: EvaluacionService,private router: Router
  ) { }

  ngOnInit() {
    this.usuario = this.user.usuario;
    this.listaDefault();
    this.lista7_4();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  lista7_4() {
    this.listMedidadProteccion = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: "PMFIC",
    };

    this.sistemaManejoForestalService
      .obtener(params.idPlanManejo, params.codigoProceso)
      .subscribe((response: any) => {
        if (response.data && response.data.detalle.length > 0) {
          this.cabeceraMedidadProteccion = new SistemaManejoPMFIC(
            response.data
          );

          response.data.detalle.map((response: any) => {
            const obj = new DetalleSistemaManejo(response);
            obj.idUsuarioRegistro = this.user.idUsuario;
            this.listMedidadProteccion.push(obj);
          });
        }
      });
  }

  registarMedidaProteccion() {
    const obj = new DetalleSistemaManejo();
    obj.idUsuarioRegistro = this.user.idUsuario;
    obj.codigoTipoDetalle = CodigoPMFIC.ACORDEON_7_4;
    obj.editable = true;
    this.listMedidadProteccion.push(obj);
  }

  guardarMedidasProteccion() {
    this.createStoreSistemaManejo.submit();
    const cabecera = new SistemaManejoPMFIC(this.cabeceraMedidadProteccion);
    cabecera.detalle = this.listMedidadProteccion;
    cabecera.idUsuarioRegistro = this.user.idUsuario;

    this.sistemaManejoForestalService
      .guardarMedidasProteccion(cabecera)
      .subscribe(
        (res: any) => {
          if (res.success) {
            this.toast.ok('Se registró ordenamiento y protección de la UMF correctamente.')
            this.lista7_4();
            this.createStoreSistemaManejo.submitSuccess();
          } else {
            this.createStoreSistemaManejo.submitError("");
          }
        },
        (err) => {
          this.createStoreSistemaManejo.submitError(err);
        }
      );
  }

  listaDefault() {
    this.newList1 = [];
    this.newList2 = [];
    this.newList3 = [];
    this.listCoordinatesAnexo1 = [];
    this.listCoordinatesAnexo2 = [];
    this.listCoordinatesAnexo3 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: CodigoPMFIC.CODIGO_PROCESO,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ordenamientoProteccionService
      .listarOrdenamientoInternoDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idOrdenamientoProteccion != null) {
            this.objMedidasProteccion = new OrdenamientoProteccionCabeceraModel(
              element
            );
            this.objMedidasProteccion.idPlanManejo = this.idPlanManejo;
            this.objMedidasProteccion.idUsuarioRegistro = this.user.idUsuario;
            element.listOrdenamientoProteccionDet.forEach((item: any) => {
              if (item.codigoTipoOrdenamientoDet == CodigoPMFIC.ACORDEON_7_1) {
                this.newList1.push({
                  idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                  vertice: item.verticeBloque,
                  referencia: item.observacion,
                  este: item.coordenadaEste,
                  norte: item.coordenadaNorte,
                  anexo: item.descripcion,
                  superficie: item.areaHA,
                });
                this.listCoordinatesAnexo1 = this.newList1.groupBy(
                  (t: any) => t.anexo
                );
                this.listCoordinatesAnexo1.length != 0 ? this.disabledBtn = true : this.disabledBtn = false;
              } else if (item.codigoTipoOrdenamientoDet == CodigoPMFIC.ACORDEON_7_2) {
                this.newList2.push({
                  idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                  vertice: item.verticeBloque,
                  referencia: item.observacion,
                  este: item.coordenadaEste,
                  norte: item.coordenadaNorte,
                  anexo: item.descripcion,
                });
                this.listCoordinatesAnexo2 = this.newList2.groupBy(
                  (t: any) => t.anexo
                );
              } else if (item.codigoTipoOrdenamientoDet == CodigoPMFIC.ACORDEON_7_3) {
                this.newList3.push({
                  idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                  vertice: item.verticeBloque,
                  referencia: item.observacion,
                  este: item.coordenadaEste,
                  norte: item.coordenadaNorte,
                  anexo: item.descripcion,
                });
                this.listCoordinatesAnexo3 = this.newList3.groupBy(
                  (t: any) => t.anexo
                );
              }
            });
          }
        });
        if (this.listCoordinatesAnexo1.length != 0) {
          this.agruparBloques();
        }
        if (this.listCoordinatesAnexo2.length != 0) {
          this.agruparVerticesPrimerBloque();
        }
        if (this.listCoordinatesAnexo3.length != 0) {
          this.agruparVerticesAnioOperativo();
        }
      });
  }

  //7.1
  agruparBloques() {
    this.listCoordinatesAnexo1.forEach((t: any) => {
      t.superficie = t.value[0].superficie;
    });

    this.listBloquesVetices = [];
    this.listCoordinatesAnexo1.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.areaHA = element.superficie;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPMFIC.ACORDEON_7_1;
        obj.actividadesRealizar = String(this.idPlanManejo);
        obj.observacionDetalle = CodigoPMFIC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet;
        this.listBloquesVetices.push(obj);
      });
    });
  }

  listBloques(event: any) {
    this.listBloquesVetices = [];
    event.length != 0 ? this.disabledBtn = true : this.disabledBtn = false
    event.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.areaHA = element.superficie;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPMFIC.ACORDEON_7_1;
        obj.actividadesRealizar = String(this.idPlanManejo);
        obj.observacionDetalle = CodigoPMFIC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listBloquesVetices.push(obj);
      });
    });
  }

  //7.2
  agruparVerticesPrimerBloque() {
    this.listPrimerBloque = [];
    this.listCoordinatesAnexo2.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPMFIC.ACORDEON_7_2;
        obj.actividadesRealizar = String(this.idPlanManejo);
        obj.observacionDetalle = CodigoPMFIC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listPrimerBloque.push(obj);
      });
    });
  }

  listVerticesPrimerBloque(event: any) {
    this.listPrimerBloque = [];
    event.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPMFIC.ACORDEON_7_2;
        obj.actividadesRealizar = String(this.idPlanManejo);
        obj.observacionDetalle = CodigoPMFIC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listPrimerBloque.push(obj);
      });
    });
  }

  //7.3
  agruparVerticesAnioOperativo() {
    this.listAnioOperativo = [];
    this.listCoordinatesAnexo3.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.codigoTipoOrdenamientoDet = CodigoPMFIC.ACORDEON_7_3;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.actividadesRealizar = String(this.idPlanManejo);
        obj.observacionDetalle = CodigoPMFIC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listAnioOperativo.push(obj);
      });
    });
  }

  listVerticesAnioOperativo(event: any) {
    this.listAnioOperativo = [];
    event.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.codigoTipoOrdenamientoDet = CodigoPMFIC.ACORDEON_7_3;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.actividadesRealizar = String(this.idPlanManejo);
        obj.observacionDetalle = CodigoPMFIC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listAnioOperativo.push(obj);
      });
    });
  }

  //
  editarMedidas(type: string, data?: any) {
    let titulo = "";
    if (type == "edit") {
      titulo = "Editar"
    } else {
      titulo = "Agregar"
    }
    this.ref = this.dialogService.open(FormMedidaProteccionComponent, {
      header: titulo + " Medidas de Protección",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        item: data,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (type == "edit") {
          data.actividades = resp.actividades;
          data.descripcionSistema = resp.descripcionSistema;
        } else {
          var objDetalle = new DetalleSistemaManejo();
          objDetalle.idSistemaManejoForestalDetalle = 0;
          objDetalle.codigoTipoDetalle = CodigoPMFIC.ACORDEON_7_4;
          objDetalle.idUsuarioRegistro = this.user.idUsuario;
          objDetalle.actividades = resp.actividades;
          objDetalle.descripcionSistema = resp.descripcionSistema;
          objDetalle.codigoProceso = CodigoPMFIC.CODIGO_PROCESO;
          objDetalle.editable = true;

          this.listMedidadProteccion.push(objDetalle);
        }
      }
    });
  }

  eliminarMedida(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idOrdenamientoProteccionDet != 0) {
          var params = {
            idSistemaManejoForestalDetalle: data.idSistemaManejoForestalDetalle,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.sistemaManejoForestalService
            .eliminarDetalle(
              params.idSistemaManejoForestalDetalle,
              params.idUsuarioElimina
            )
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                this.toast.ok("Se eliminó el registro correctamente.");
                this.listMedidadProteccion.splice(index, 1);
              } else {
                this.toast.error("Ha surgido un error.");
              }
            });
        } else {
          this.listMedidadProteccion.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  guardar() {
    this.createStore.submit();
    this.objMedidasProteccion.listOrdenamientoProteccionDet = [];
    this.objMedidasProteccion.idPlanManejo = this.idPlanManejo;
    this.objMedidasProteccion.idUsuarioRegistro = this.user.idUsuario;
    this.objMedidasProteccion.codTipoOrdenamiento = CodigoPMFIC.CODIGO_PROCESO;
    this.listGeneral = [];
    this.objMedidasProteccion.listOrdenamientoProteccionDet.push(
      ...this.listBloquesVetices,
      ...this.listPrimerBloque,
      ...this.listAnioOperativo
    );

    if (this.objMedidasProteccion.listOrdenamientoProteccionDet.length > 0) {
      this.listGeneral.push(this.objMedidasProteccion);
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.ordenamientoProteccionService
        .registrarOrdenamientoInterno(this.listGeneral)
        .pipe(
          finalize(() => {
            this.dialog.closeAll();
            // this.registarMedidasProteccion();
          })
        )
        .subscribe(
          (response: any) => {
            if (response.success == true) {
              // this.toast.ok(response.message);
              this.listaDefault();
              this.createStore.submitSuccess();
              this.guardarMedidasProteccion();
            } else {
              this.toast.warn(response.message);
              this.createStore.submitError("");
            }
          },
          (err) => {
            this.createStore.submitError(err);
          }
        );
    } else {
      this.createStore.submitSuccess();
      this.guardarMedidasProteccion();
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }



  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet: this.codigoProceso,
      //codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion_7 = Object.assign(this.detEvaluacion_7,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_7));
            }
          }else{
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_7])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_7);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6251"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
