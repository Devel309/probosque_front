import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plan-manejo-forestal-intermedio',
  templateUrl: './plan-manejo-forestal-intermedio.component.html',
})
export class PlanManejoForestalIntermedioComponent implements OnInit {
  listPlanManejoForestalIntermedio: any[] = [];

  constructor(private router: Router) {}

  ngOnInit() {
    localStorage.removeItem('plan-manejo-forestal-intermedio');
    this.cargarPlanManejoForestalIntermedio();
  }

  cargarPlanManejoForestalIntermedio = () => {
    this.listPlanManejoForestalIntermedio.push({
      idProcesoPostulacion: 'idProcesoPostulacion',
      descripcion: 'descripcion',
    });
  };

  verDetalle = (item: any) => {
    localStorage.setItem(
      'plan-manejo-forestal-intermedio',
      JSON.stringify(item)
    );
    this.router.navigate([
      '/planificacion/plan-manejo-forestal-intermedio',
    ]);
  };
}
