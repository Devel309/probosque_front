import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-formulario-fines-maderables",
  templateUrl: "./modal-formulario-fines-maderables.component.html",
})
export class ModalFormularioFinesManerablesComponent implements OnInit {
  finesMaderablesObjt: any = {
    actividades: "",
    descripcionDeSistema: "",
    maquinaria: "",
    persona: "",
    observaciones: "",
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == "true") {
      
      this.finesMaderablesObjt.actividades = this.config.data.item.actividades;
      this.finesMaderablesObjt.descripcionDeSistema = this.config.data.item.descripcionDeSistema;
      this.finesMaderablesObjt.maquinaria = this.config.data.item.maquinaria;
      this.finesMaderablesObjt.persona = this.config.data.item.persona;
      this.finesMaderablesObjt.observaciones = this.config.data.item.observaciones;
    }
  }

  agregar = () => {
    if (this.finesMaderablesObjt.actividades === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Actividedes, es obligatorio",
      });
    } else if (this.finesMaderablesObjt.descripcionDeSistema === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Descripcion de sistema, es obligatorio",
      });
    } else if (this.finesMaderablesObjt.maquinaria === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Maquinaria, es obligatorio",
      });
    } else if (this.finesMaderablesObjt.persona === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "persona, es obligatorio",
      });
    } else if (this.finesMaderablesObjt.observaciones === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Observaciones, es obligatorio",
      });
    } else this.ref.close(this.finesMaderablesObjt);
  };
}
