import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-aprovechamiento-forestal',
  templateUrl: './modal-formulario-aprovechamiento-forestal.component.html',
})
export class ModalFormularioAprovechamientoForestalComponent implements OnInit {
  desarrolloObjt: any = {
    actividad: '',
    organizacion: '',
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      
      this.desarrolloObjt.actividad = this.config.data.item.actividad;
      this.desarrolloObjt.organizacion = this.config.data.item.organizacion;
    
    }
  }

  agregar = () => {
    if (this.desarrolloObjt.actividad === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Actividad, es obligatorio',
      });
    } else if (this.desarrolloObjt.organizacion === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Forma de organización, es obligatorio',
      });
    } else this.ref.close(this.desarrolloObjt);
  };
}
