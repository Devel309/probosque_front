import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { KEY_SMF_DETALLE, SistemaManejoForestal, SistemaManejoForestalDetalle } from '@models';
import { SistemaManejoForestalService } from '@services';
import { handlerWriteResult, isNullOrEmpty } from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-modal-formulario-categoria',
  templateUrl: './modal-formulario-categoria.component.html',
})
export class ModalFormularioCategoriaComponent implements OnInit {

  smf: SistemaManejoForestal = new SistemaManejoForestal();
  uso: SistemaManejoForestalDetalle = new SistemaManejoForestalDetalle();

  TOAST_KEY = 'smf-uso01';

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private dialog: MatDialog,
    private msgService: MessageService,
    private api: SistemaManejoForestalService
  ) {
    this.smf = this.config.data as SistemaManejoForestal;
    if (this.smf.detalle != null) {
      this.uso = this.smf.detalle[0];
      this.uso.idSistemaManejoForestal = this.smf.idSistemaManejoForestal;
    }
  }

  ngOnInit() {

  }

  guardarSMF(body: SistemaManejoForestal) {
    return this.api.guardar(body)
      .pipe(tap(handlerWriteResult(this.msgService, this.TOAST_KEY)));
  }

  guardarSMFDetalle(body: SistemaManejoForestalDetalle) {
    return this.api.guardarDetalle(body)
      .pipe(tap(handlerWriteResult(this.msgService, this.TOAST_KEY)));
  }

  guardar() {
    const keys = ['categoriaZona', 'usoPotencialNumerico', 'actividadRealizar'];
    if (!this.validAndShowError(keys)) return;

    let request = this.guardarSMFDetalle(this.uso);
    if (isNullOrEmpty(this.smf.idSistemaManejoForestal)) {
      request = this.guardarSMF(this.smf);
    }
    request.subscribe();
  }


  validAndShowError(keys: string[]): boolean {
    let isValid = true;
    for (const _key of keys) {
      const key = _key as keyof SistemaManejoForestalDetalle;
      if (isNullOrEmpty(this.uso[key])) {
        isValid = false;
        this.msgService.add({
          key: this.TOAST_KEY,
          severity: 'error',
          detail: `${KEY_SMF_DETALLE[key]}, es obligatorio`
        });
      }
    }
    return isValid;
  }
}

