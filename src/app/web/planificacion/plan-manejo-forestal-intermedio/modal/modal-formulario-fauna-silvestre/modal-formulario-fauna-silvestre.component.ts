import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-fauna-silvestre',
  templateUrl: './modal-formulario-fauna-silvestre.component.html',
})
export class ModalFormularioFaunaSilvestreComponent implements OnInit {
  impactoObjt: any = {
    nombreComun: '',
    nombreNativo: '',
    nombreCientifico: '',
    categoriaDeAmenaza: '',
    apendiceCites: '',
    observaciones: '',
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      
      this.impactoObjt.nombreComun = this.config.data.item.nombreComun;
      this.impactoObjt.nombreNativo = this.config.data.item.nombreNativo;
      this.impactoObjt.nombreCientifico =
        this.config.data.item.nombreCientifico;
      this.impactoObjt.categoriaDeAmenaza =
        this.config.data.item.categoriaDeAmenaza;
      this.impactoObjt.apendiceCites = this.config.data.item.apendiceCites;
      this.impactoObjt.observaciones = this.config.data.item.observaciones;
    }
  }

  agregar = () => {
    if (this.impactoObjt.nombreComun === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre común, es obligatorio',
      });
    } else if (this.impactoObjt.nombreNativo === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre nativo, es obligatorio',
      });
    } else if (this.impactoObjt.nombreCientifico === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre científico, es obligatorio',
      });
    } else if (this.impactoObjt.categoriaDeAmenaza === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Categoía de amenaza es obligatorio',
      });
    } else if (this.impactoObjt.apendiceCites === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Apéndice CITES, es obligatorio',
      });
    } else if (this.impactoObjt.observaciones === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Observaciones, es obligatorio',
      });
    } else this.ref.close(this.impactoObjt);
  };
}
