import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';
import {
  InformacionBasicaDetalleModelPMFIC,
  InformacionBasicaModelPMFIC,
} from 'src/app/model/InformacionSocieconomicaPMFIC';
import { DialogTipoEspecieMultiselectFaunaComponent } from 'src/app/shared/components/dialog-tipo-especie-multiselect-fauna/dialog-tipo-especie-multiselect.component';

@Component({
  selector: 'app-modal-formulario-antecedentes',
  templateUrl: './modal-formulario-antecedentes.component.html',
})
export class ModalFormularioAntecedentesComponent implements OnInit {
  antecedentesObjt = new InformacionBasicaModelPMFIC();
  listInformacionBasicaDet: any = [];

  ref!: DynamicDialogRef;

  constructor(
    public principalRef: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    public dialogService: DialogService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      this.antecedentesObjt = new InformacionBasicaModelPMFIC(
        this.config.data.item
      );
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  validarEspecies(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';



    if (this.antecedentesObjt?.listInformacionBasicaDet == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar  especies\n';
    }
    if (this.antecedentesObjt.departamento == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Debe ingresar propuestas de solución al conflicto.\n';
    }
    if (this.antecedentesObjt.distrito == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar identificación de conflicto.\n';
    }
    if (this.antecedentesObjt.provincia == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Debe ingresar identificación de observaciones.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  agregar() {
    if (!this.validarEspecies()) return;
    this.principalRef.close(this.antecedentesObjt);
  }

  openModalEspecies(mesaje: string) {
    this.antecedentesObjt.comunidad != 'Caza o captura de fauna'
      ? this.modalEspeciesFlora(mesaje)
      : this.modalEspeciesFauna(mesaje);
  }

  modalEspeciesFauna(mesaje: string) {
    this.ref = this.dialogService.open(
      DialogTipoEspecieMultiselectFaunaComponent,
      {
        header: mesaje,
        width: '50%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
      }
    );

    this.ref.onClose.subscribe((resp: any) => {
      if (resp && resp.length > 0) {

        resp.map((element: any) => {
          let obj = new InformacionBasicaDetalleModelPMFIC();
          obj.actividad = element.nombreCientifico;
          obj.solucion= 'S';
          this.listInformacionBasicaDet.push(obj);
        });
        this.antecedentesObjt.listInformacionBasicaDet =
          this.listInformacionBasicaDet;
      }
    });
  }
  modalEspeciesFlora(mesaje: string) {
    this.ref = this.dialogService.open(
      DialogTipoEspecieMultiselectFaunaComponent,
      {
        header: mesaje,
        width: '50%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
      }
    );

    this.ref.onClose.subscribe((resp: any) => {
      if (resp && resp.length > 0) {
        resp.map((element: any) => {
          let obj = new InformacionBasicaDetalleModelPMFIC();
          obj.actividad = element.nombreCientifico;
          obj.solucion= 'S';
          this.listInformacionBasicaDet.push(obj);
        });

        this.antecedentesObjt.listInformacionBasicaDet =
          this.listInformacionBasicaDet;
      }
    });
  }
  cerrarModal() {
    this.principalRef.close();
  }
}
