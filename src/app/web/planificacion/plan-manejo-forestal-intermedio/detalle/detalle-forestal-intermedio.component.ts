import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MenuItem } from 'primeng/api';
import { TabInformacionBasicaComponent } from '../tabs/tab-informacion-basica/tab-informacion-basica.component';
import {CodigoEstadoPlanManejo} from '../../../../model/util/CodigoEstadoPlanManejo';
import {PlanManejoService, UsuarioService} from '@services';
import {UsuarioModel} from '../../../../model/seguridad/usuario';
import {Perfiles} from '../../../../model/util/Perfiles';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';

@Component({
  selector: 'app-detalle-forestal-intermedio',
  templateUrl: './detalle-forestal-intermedio.component.html',
  styleUrls: ['./detalle-forestal-intermedio.component.scss'],
})
export class DetallePlanManejoForestalIntermedioComponent implements OnInit {
  @ViewChild(TabInformacionBasicaComponent)
  TabInformacionBasicaComponent!: TabInformacionBasicaComponent;
  //idPGMF: number= 23;
  idPlanManejo!: number;
  tabIndex = 0;
  disabled: boolean = true;

  isPerfilArffs: boolean = false;

  usuario!: UsuarioModel;

  items!: MenuItem[];
  home!: MenuItem;
  obj: any ={};

  CodigoPMFIC =CodigoPMFIC

  constructor(private router: Router,private route: ActivatedRoute,private planManejoService: PlanManejoService,private usuarioService: UsuarioService,) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Plan General de Manejo Forestal Intermedio',
        routerLink: '/planificacion/bandeja-pmfic',
      },
      { label: 'Detalle' },
    ];

    this.home = { icon: 'pi pi-home', routerLink: '/inicio' };

    //if(this.usuarioService)
    this.usuario = this.usuarioService.usuario;

    if(this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL){
      this.isPerfilArffs = true;
    }

    this.obtenerPlan();
  }

  ngAfterViewInit(){
    if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    }
    //console.log("MAIN ngAfterViewInit");
  }
  obtenerPlan() {


    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.disabled = true;
    } else{
      this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {

        if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
          this.disabled = true;
        }else if(result.data.codigoEstado == CodigoEstadoPlanManejo.BORRADOR){
          this.disabled = false;
        }
      })
    }
  }

  setTabIndex(obj: any){
    var cod = obj.tab.replace(obj.codigoEvaluacionDet,'');

    if(cod === 'INFG') this.tabIndex = 0;
    else if (cod === 'REEVRIIC') {
      this.tabIndex = 0;

      setTimeout (() => {
        
        this.tabIndex = 1;
      }, 1000);}
    else if (cod === 'OBJ') {this.tabIndex = 1;}
    else if (cod === 'INFBA') this.tabIndex = 2;
    else if (cod === 'INFBAAF') this.tabIndex = 3;
    else if (cod === 'AB') this.tabIndex = 4;
    else if (cod === 'IS') this.tabIndex = 5;
    else if (cod === 'OP') this.tabIndex = 6;
    else if (cod === 'PPF') this.tabIndex = 7;
    else if (cod === 'SMFUM') this.tabIndex = 8;
    else if (cod === 'EA') this.tabIndex = 9;
    else if (cod === 'ORG') this.tabIndex = 10;
    else if (cod === 'ORGCA') this.tabIndex = 11;
    else if (cod === 'ORGRMF') this.tabIndex = 12;
    else if (cod === 'ORGAC') this.tabIndex = 13;
    else if (cod === 'ORGA') this.tabIndex = 14;
  }


  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
    if (event === 0) {
      this.TabInformacionBasicaComponent.ngOnInit();
    }
  }

  regresar = () => {
    this.router.navigate(['/planificacion/bandeja-pmfic']);
  };
}
