import {
  Component,
  Input,
  EventEmitter,
  OnInit,
  Output,
  ElementRef,
  ViewChild,
} from "@angular/core";
import {ConfirmationService, MessageService} from "primeng/api";
import {ParametroValorService} from "../../../../service/parametro.service";
import {LoadingComponent} from "src/app/components/loading/loading.component";
import {MatDialog} from "@angular/material/dialog";
import {SolicitudService} from "../../../../service/solicitud.service";
import {DownloadFile} from "@shared";
import {RegenteExternoModel, RegenteForestalModel} from "@models";
import {forkJoin} from "rxjs";
import {finalize} from "rxjs/operators";
import {RegenteService} from "../../../../service/regente.service";
import {ApiForestalService} from "../../../../service/api-forestal.service";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import {SolicitudModel} from "../../../../model/Solicitud";
import {
  SolicitudArchivoAdjuntoModel,
  SolicitudArchivoModel,
  SolicitudCargaArchivoModel
} from "../../../../model/SolicitudArchivo";
import {Router} from "@angular/router";
import {DialogService, DynamicDialogRef} from "primeng/dynamicdialog";
import {AdjuntoModalComponent} from "./modal/adjunto.modal.component";
import {ModalEvaluacionAnexosComponent} from "./modal/modal-evaluacion-anexo/modal-evaluacion-anexo.component";
import {ModalEvaluarAreaComponent} from "./modal/modal-evaluar-area/modal-evaluar-area.component";
import {ModalEvaluarPlanManejoComponent} from "./modal/modal-evaluar-plan-manejo/modal-evaluar-plan-manejo.component";

@Component({
  selector: 'datos-otorgamiento',
  templateUrl: './datos-otorgamiento.component.html',
  styleUrls: ['./datos-otorgamiento.component.scss'],
  providers: [MessageService]
})
export class DatosOtorgamientoComponent implements OnInit {
  @Output() isValidatingAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isValidatingArea: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() isValidatingPlanManejo: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input("dataBase") dataBase: any = {};
  @Input("enabledControlOtorgamiento") enabledControlOtorgamiento: any = {};
  @Input("solicitud") solicitud: any = {} as SolicitudModel;
  @Input("solicitudCargaArchivo")
  solicitudCargaArchivo!: SolicitudCargaArchivoModel;
  @Input('accion') accion: string | null = '';
  solArchivoTitProp: any = {} as SolicitudCargaArchivoModel;
  solArchivoDecJur: any = {} as SolicitudCargaArchivoModel;
  solArchivoLimCol: any = {} as SolicitudCargaArchivoModel;
  solArchivoActRepLeg: any = {} as SolicitudCargaArchivoModel;
  solArchivoAreCom: any = {} as SolicitudCargaArchivoModel;
  solArchivoAsamAcu: any = {} as SolicitudCargaArchivoModel;
  solArchivoRegFore: any = {} as SolicitudCargaArchivoModel;
  solArchivoArrf: any = {} as SolicitudCargaArchivoModel;
  solArchivoComPag: any = {} as SolicitudCargaArchivoModel;
  solArchivoConTer: any = {} as SolicitudCargaArchivoModel;
  solArchivoActAsa: any = {} as SolicitudCargaArchivoModel;
  SolArchivoAdjuntoc: any = {} as SolicitudCargaArchivoModel;// CARGA
  SolArchivoAdjuntoc2: any = {} as SolicitudCargaArchivoModel;// CARGA
  lstSolArchivoAdjunto: SolicitudArchivoAdjuntoModel[] = [];// CARGA
  lstSolArchivoAdjunto2: SolicitudArchivoAdjuntoModel[] = []; // DESCARGA
  @Output('registroSolicitud') registroSolicitud = new EventEmitter<void>();
  enabledControlGeneral: boolean = false;
  enabledControlOperativo: boolean = false;
  enabledControlDema: boolean = false;
  @ViewChild("map", {static: true}) private mapViewEl!: ElementRef;
  @ViewChild("mapOtorgamiento", {static: true})
  enabledHabilitarTercero: boolean = false;
  enabledHabilitarContinuar: boolean = false;
  private mapViewEl2!: ElementRef;
  public view: any = null;
  ref!: DynamicDialogRef;
  isValid: boolean = false;
  isPending: boolean = true;
  isObserved: boolean = false;

  isValidArea: boolean = false;
  isPendingArea: boolean = true;
  isObservedArea: boolean = false;

  isValidPlan: boolean = false;
  isPendingPlan: boolean = true;
  isObservedPlan: boolean = false;

  archivoDeclaracionJurada: string = '';
  archivoTituloPropiedad: string = '';
  archivoLimiteColindancia: string = '';
  archivoActaRepLegal: string = '';
  archivoAreaComunidad: string = '';
  archivoAsambleaAcuerdo: string = '';
  archivoRegenteForestal: string = '';
  archivoComprobantePago: string = '';
  archivoActaAsamblea: string = '';
  archivoContratTerc: string = '';
  archivoArrf: string = '';
  titulo: string = '';
  idArchivoSolDeclaracionJurada: string = '';
  idArchivoSolTituloPropiedad: string = '';
  idArchivoSolLimiteColindancia: string = '';
  idArchivoSolActaRepLegal: string = '';
  idArchivoSolAreaComunidad: string = '';
  idArchivoSolAsambleaAcuerdo: string = '';
  idArchivoSolRegenteForestal: string = '';
  idArchivoSolComprobantePago: string = '';
  idArchivoSolActaAsamblea: string = '';
  idArchivoSolContratTerc: string = '';
  idArchivoSolArrf: string = '';

  validaSunatClass: string = 'btn btn-sm btn-danger2';
  flagvalidaSunat: boolean = true;
  validaSunarpClass: string = 'btn btn-sm btn-danger2';
  flagvalidaSunaarp: boolean = true;
  flagAgregaArchivo: boolean = true;
  idSolicitud: number = 0;
  lstEstado: any[] = [];
  lstAprovechamiento = [
    {id: 1, nAprov: "Por Comunidad"},
    {id: 2, nAprov: "Por Terceros"}
  ];
  terceros: string = '';

  regenteForestal: RegenteForestalModel = new RegenteForestalModel();
  documentoRegente: RegenteExternoModel = new RegenteExternoModel();

  regentesFiltrados: RegenteExternoModel[] = [];
  regentesForestales: RegenteExternoModel[] = [];

  idUsuario: number = 1;

  mapaBase = [
    {label: "-- Seleccione --", value: null},
    {label: "Archivos shapefiles", value: true},
    {label: "PDF", value: false},
  ];

  resultadoBusqueda: boolean | null = null;
  resultadoOpiniones: boolean | null = null;
  resultadoInspeccion: boolean | null = null;
  resultadoInforme: boolean | null = null;

  codigoUnicoAprobado: boolean | null = null;
  estadoSolicitudAprobado: boolean | null = null;
  declaracionJuradaAprobado: boolean | null = null;
  nroRucEmpresaAprobado: boolean | null = null;
  nroPropiedadAprobado: boolean | null = null;
  archivoTituloPropiedadAprobado: boolean | null = null;
  nroPartidaRegistralAprobado: boolean | null = null;
  archivoLimiteColindanciaAprobado: boolean | null = null;
  archivoActaRepLegalAprobado: boolean | null = null;
  archivoAreaComunidadAprobado: boolean | null = null;
  documentoRegenteAprobado: boolean | null = null;
  archivoRegenteForestalAprobado: boolean | null = null;
  nroContratoRegenteAprobado: boolean | null = null;
  archivoArrfAprobado: boolean | null = null;


  constructor(private messageService: MessageService,
              private serv: ParametroValorService,
              private servSol: SolicitudService,
              private dialog: MatDialog,
              private serv_forestal: ApiForestalService,
              private confirmationService: ConfirmationService,
              private reg: RegenteService,
              private router: Router,
              public dialogService: DialogService,) {
  }

  ngOnInit(): void {
    this.listarEstado();
    if (this.solicitud.idSolicitud != null && this.accion == 'editar') {
      this.idSolicitud = this.solicitud.idSolicitud;
      this.obtenerNombresArchivos();

    }
    this.asignarRegente();
    this.initializeMapBase();
    this.initializeMapOtorgamiento();
  }

  initializeMapBase(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "370px";
    container.style.width = "500px";
    const map = new Map({
      basemap: "hybrid",
    });
    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }

  initializeMapOtorgamiento(): void {
    const container = this.mapViewEl2.nativeElement;
    container.style.height = "370px";
    container.style.width = "500px";
    const map = new Map({
      basemap: "hybrid",
    });
    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }

  validaRuc() {
    if (this.solicitud.nroRucEmpresa == null || this.solicitud.nroRucEmpresa == "" || this.solicitud.nroRucEmpresa == undefined) {
      this.toast('warn', 'Se requiere número de ruc.');
      return;
    } else {
      this.serv_forestal.consultarDatoRuc(this.solicitud.nroRucEmpresa).subscribe(
        (data: any) => {
          if (data.dataService) {
            if (data.dataService.respuesta.esHabido) {
              
              this.messageService.add({severity: "success", summary: "", detail: 'Se valido ruc en Sunat.'});
              this.validaSunatClass = 'btn btn-sm btn-secondary';
              this.flagvalidaSunat = false;
            }

          }
        });
    }

  }

  validaSunarp() {
    if (this.solicitud.nroPartidaRegistral == null || this.solicitud.nroPartidaRegistral == "" || this.solicitud.nroPartidaRegistral == undefined) {
      this.toast('warn', 'Se requiere número partida registral.');
      return;
    } else {
      this.serv_forestal.consultarDatoSunarp(this.solicitud.nroPartidaRegistral).subscribe(
        (data: any) => {
          if (data) {
            
            this.messageService.add({severity: "success", summary: "", detail: 'Se valido en Sunarp.'});
            this.validaSunarpClass = 'btn btn-sm btn-secondary';
          }
        });
    }
  }

  continuar() {
    /*if (!this.validarRegistroSolicitud()) {
      return;
    }*/
    this.registroSolicitud.emit();
    this.enabledHabilitarContinuar = true;
  }


  listarEstado() {
    var params = {prefijo: 'ESAC'}
    //acá debe obtener de tabla estados
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstEstado = result.data;
      }
    );
  };

  asignarRegente() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    forkJoin({
      regentesResponse: this.serv_forestal.consultarRegente(),
      regenteResponse: this.reg.obtenerRegentes()
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe({
        next: res => {
          this.regentesForestales = (res.regentesResponse as any)?.dataService;
          this.regenteForestal = res?.regenteResponse?.data;
          const regenteForestal = this.regentesForestales
            .find(x => this.regenteForestal.numeroDocumento == x.numeroDocumento);
          this.documentoRegente = regenteForestal ? regenteForestal :
            new RegenteExternoModel({
              numeroDocumento: this.regenteForestal.numeroDocumento
            });
        },
        error: _err => {
        }
      });
  }


  clearRegente() {
    this.regenteForestal = new RegenteForestalModel();
    this.documentoRegente = new RegenteExternoModel();
  }

  selectDocumentoRegente(value: RegenteExternoModel) {
    this.regenteForestal = {
      ...this.regenteForestal,
      numeroLicencia: value.numeroLicencia,
      numeroDocumento: value.numeroLicencia,
      idUsuarioRegistro: this.idUsuario
    };
    this.solicitud.regenteForestal = value.numeroDocumento;
    this.solicitud.nombreRegenteForestal = value.nombresNroDocumento;
  }

  filtrarRegente(value: string) {
    value = value.toLowerCase();
    this.regentesFiltrados = [];
    this.regentesFiltrados = this.regentesForestales.filter(
      r => (r.nombres.toLowerCase().includes(value) ||
        r.apellidos.toLowerCase().includes(value) ||
        r.numeroDocumento.includes(value))
    ).map(x => {
      return new RegenteExternoModel({...x, nombresNroDocumento: `${x.nombres} ${x.apellidos} - ${x.numeroDocumento}`})
    });
  }


  onFileSelectedTP(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoTitProp.idSolicitud = 0;
    this.solArchivoTitProp.tipoArchivo = 'TITPRO';
    this.solArchivoTitProp.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoTitProp = this.solArchivoTitProp;
    this.archivoTituloPropiedad = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }


  onFileSelectedDC(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoDecJur.idSolicitud = 0;
    this.solArchivoDecJur.tipoArchivo = 'DECJUR';
    this.solArchivoDecJur.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoDecJur = this.solArchivoDecJur;
    this.archivoDeclaracionJurada = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }


  onFileSelectedCP(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoComPag.idSolicitud = 0;
    this.solArchivoComPag.tipoArchivo = 'COMPAG';
    this.solArchivoComPag.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoComPag = this.solArchivoComPag;
    this.archivoComprobantePago = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedCT(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoConTer.idSolicitud = 0;
    this.solArchivoConTer.tipoArchivo = 'CONTTERC';
    this.solArchivoConTer.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoConTer = this.solArchivoConTer;
    this.archivoContratTerc = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedLC(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoLimCol.idSolicitud = 0;
    this.solArchivoLimCol.tipoArchivo = 'LIMCON';
    this.solArchivoLimCol.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoLimCol = this.solArchivoLimCol;
    this.archivoLimiteColindancia = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }


  onFileSelectedActRepLeg(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoActRepLeg.idSolicitud = 0;
    this.solArchivoActRepLeg.tipoArchivo = 'ACTASA';
    this.solArchivoActRepLeg.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoActRepLeg = this.solArchivoActRepLeg;
    this.archivoActaRepLegal = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedAreCom(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoAreCom.idSolicitud = 0;
    this.solArchivoAreCom.tipoArchivo = 'ARECOM';
    this.solArchivoAreCom.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoAreCom = this.solArchivoAreCom;
    this.archivoAreaComunidad = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedAsamAcu(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoAsamAcu.idSolicitud = 0;
    this.solArchivoAsamAcu.tipoArchivo = 'ASAACU';
    this.solArchivoAsamAcu.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoAsamAcu = this.solArchivoAsamAcu;
    this.archivoAsambleaAcuerdo = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedRegFore(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoRegFore.idSolicitud = 0;
    this.solArchivoRegFore.tipoArchivo = 'CONREGFOR';
    this.solArchivoRegFore.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoRegFore = this.solArchivoRegFore;
    this.archivoRegenteForestal = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedArrf(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoArrf.idSolicitud = 0;
    this.solArchivoArrf.tipoArchivo = 'ARRF';
    this.solArchivoArrf.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoArrf = this.solArchivoArrf;
    this.archivoArrf = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedAA(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoActAsa.idSolicitud = 0;
    this.solArchivoActAsa.tipoArchivo = 'ACASAM';
    this.solArchivoActAsa.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoActAsa = this.solArchivoActAsa;
    this.archivoActaAsamblea = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileDowlandDC() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'DECJUR';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolDeclaracionJurada, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }

  onFileDowlandTP() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'TITPRO';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolTituloPropiedad, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }

  onFileDowlandLC() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'LIMCON';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolLimiteColindancia, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }

  onFileDowlandActaRepLegal() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'ACTASA';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolActaRepLegal, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }

  onFileDowlandAreCom() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'ARECOM';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolAreaComunidad, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }


  onFileDowlandAsamAcu() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'ASAACU';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolAsambleaAcuerdo, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }


  onFileDowlandRegFore() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'CONREGFOR';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolRegenteForestal, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }


  onFileDowlandCP() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'COMPAG';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolComprobantePago, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }

  onFileDowlandAA() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'ACASAM';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolActaAsamblea, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }

  onFileDowlandCT() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'CONTTERC';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolContratTerc, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }


  onFileDowlandArrf() {
    this.dialog.open(LoadingComponent, {disableClose: true});
    let tipoDocumento: string = 'ARRF';
    this.servSol.descargarArchivoSolicitud(this.idArchivoSolArrf, this.idSolicitud, tipoDocumento).subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      this.dialog.closeAll();
    });
  }


  obtenerNombresArchivos() {
    this.servSol.obtenerArchivoSolicitud(this.solicitud.idSolicitud).subscribe(
      (response: any) => {
        for (let item of response) {

          if (item.tipoDocumento == 'TITPRO') {
            this.archivoTituloPropiedad = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolTituloPropiedad = item.idArchivoSolicitud;


          }

          if (item.tipoDocumento == 'ACTASA') {
            this.archivoActaRepLegal = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolActaRepLegal = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'LIMCON') {
            this.archivoLimiteColindancia = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolLimiteColindancia = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'COMPAG') {
            this.archivoComprobantePago = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolComprobantePago = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'ARECOM') {
            this.archivoAreaComunidad = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolAreaComunidad = item.idArchivoSolicitud;

          }

          if (item.tipoDocumento == 'ASAACU') {
            this.archivoAsambleaAcuerdo = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolAsambleaAcuerdo = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'CONREGFOR') {
            this.archivoRegenteForestal = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolRegenteForestal = item.idArchivoSolicitud;
          }


          if (item.tipoDocumento == 'DECJUR') {
            this.archivoDeclaracionJurada = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolDeclaracionJurada = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'ARRF') {
            this.archivoArrf = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolArrf = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'CONTTERC') {
            this.archivoContratTerc = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolContratTerc = item.idArchivoSolicitud;
          }

          if (item.tipoDocumento == 'ACASAM') {
            this.archivoActaAsamblea = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolActaAsamblea = item.idArchivoSolicitud;
          }

        }
      });
  }

  habilitarAlta() {
    this.enabledControlGeneral = true;
    this.enabledControlOperativo = false;
    this.enabledControlDema = false;
  }

  habilitaMedia() {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = true;
    this.enabledControlDema = false;
  }

  habilitaBaja() {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = false;
    this.enabledControlDema = true;
  }

  habilitarTercero(event: any) {
    this.enabledHabilitarTercero = event.value == 'Por Terceros';
  }

  redireccionarOperativo() {
    this.router.navigate(['/planificacion/plan-operativo-ccnn-ealta']);
  }

  redireccionarGeneral() {
    this.router.navigate(['/planificacion/plan-general-manejo']);
  }

  redireccionarDema() {
    this.router.navigate(['/planificacion/elaboracion-declaracion-mpafpp']);
    // CAMBIOS
  }

  declaracionJurada() {
    this.messageService.add({severity: "success", summary: "", detail: 'Se genera declaración jurada.'});
  }

  private validarRegistroSolicitud() {
    let boolValidar: boolean = true;

    if (this.solicitud.nroRucEmpresa == null || this.solicitud.nroRucEmpresa == "" || this.solicitud.nroRucEmpresa == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere número de ruc.');
      return boolValidar;
    }

    if (this.flagvalidaSunat) {
      boolValidar = false;
      this.toast('warn', 'Se requiere validar ruc en sunat.');
      return boolValidar;
    }

    if (this.solicitud.nroPropiedad == null || this.solicitud.nroPropiedad == "" || this.solicitud.nroPropiedad == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere número de propiedad.');
      return boolValidar;
    }

    if (this.solArchivoTitProp.archivo == null || this.solArchivoTitProp.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo titulo de propiedad.');
      return boolValidar;
    }

    if (this.solicitud.nroPartidaRegistral == null || this.solicitud.nroPartidaRegistral == "" || this.solicitud.nroPartidaRegistral == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número de partida registral.');
      return boolValidar;
    }

    if (this.solicitud.nroActaAsamblea == null || this.solicitud.nroActaAsamblea == "" || this.solicitud.nroActaAsamblea == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número de acta de asamblea.');
      return boolValidar;
    }

    if (this.solArchivoActRepLeg.archivo == null || this.solArchivoActRepLeg.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo acta de asamblea RRLL.');
      return boolValidar;
    }

    if (this.solArchivoLimCol.archivo == null || this.solArchivoLimCol.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo limites y colindancia.');
      return boolValidar;
    }

    if (this.solicitud.nroComprobantePago == null || this.solicitud.nroComprobantePago == "" || this.solicitud.nroComprobantePago == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número del comprobante de pago.');
      return boolValidar;
    }

    if (this.solArchivoComPag.archivo == null || this.solArchivoComPag.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo comprobante de pago.');
      return boolValidar;
    }

    if (this.solArchivoAreCom.archivo == null || this.solArchivoAreCom.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo area comunidad.');
      return boolValidar;
    }


    if (this.solicitud.nroActaAsambleaAcuerdo == null || this.solicitud.nroActaAsambleaAcuerdo == "" || this.solicitud.nroActaAsambleaAcuerdo == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número de acta de acuerdo asamblea.');
      return boolValidar;
    }

    if (this.solArchivoAsamAcu.archivo == null || this.solArchivoAsamAcu.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo asamblea acuerdo.');
      return boolValidar;
    }

    if (this.solicitud.regenteForestal == null || this.solicitud.regenteForestal == "" || this.solicitud.regenteForestal == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere regente forestal.');
      return boolValidar;
    }

    if (this.solicitud.nroContratoRegente == null || this.solicitud.nroContratoRegente == "" || this.solicitud.nroContratoRegente == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere numero de contrato.');
      return boolValidar;
    }

    if (this.solArchivoRegFore.archivo == null || this.solArchivoRegFore.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo contrato regente forestal.');
      return boolValidar;
    }


    if (this.solicitud.tipoAprovechamiento == null || this.solicitud.tipoAprovechamiento == "" || this.solicitud.tipoAprovechamiento == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere tipo de aprovechamiento.');
      return boolValidar;
    }


    if (this.solArchivoArrf.archivo == null || this.solArchivoArrf.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo Arrf.');
      return boolValidar;
    }

    if (this.solArchivoDecJur.archivo == null || this.solArchivoDecJur.archivo == undefined) {
      boolValidar = false;
      this.toast('warn', 'Se requiere archivo declaracion jurada.');
      return boolValidar;
    }

    if (this.solicitud.tipoAprovechamiento != null || this.solicitud.tipoAprovechamiento != "" || this.solicitud.tipoAprovechamiento != undefined) {
      if (this.solicitud.tipoAprovechamiento == 'Por Terceros') {

        if (this.solicitud.nroContratoTercero == null || this.solicitud.nroContratoTercero == "" || this.solicitud.nroContratoTercero == undefined) {
          boolValidar = false;
          this.toast('warm', 'Se requiere numero de contrato tercero.');
          return boolValidar;
        }

        if (this.solArchivoConTer.archivo == null || this.solArchivoConTer.archivo == undefined) {
          boolValidar = false;
          this.toast('warn', 'Se requiere archivo contrato tercero.');
          return boolValidar;
        }

        if (this.solicitud.nroActaAprovechamiento == null || this.solicitud.nroActaAprovechamiento == "" || this.solicitud.nroActaAprovechamiento == undefined) {
          boolValidar = false;
          this.toast('warm', 'Se requiere numero de acta asamblea.');
          return boolValidar;
        }

        if (this.solArchivoActAsa.archivo == null || this.solArchivoActAsa.archivo == undefined) {
          boolValidar = false;
          this.toast('warn', 'Se requiere archivo acta asamblea.');
          return boolValidar;
        }
      }
    }

    if (this.solicitud.escalaManejo == null || this.solicitud.escalaManejo == "" || this.solicitud.escalaManejo == undefined) {
      boolValidar = false;
      this.toast('warm', 'Se requiere escala de manejo.');
      return boolValidar;
    }


    return boolValidar;

  }

  otroAdjunto() {
  }

  abrirModal() {
    const header = `Adjuntar Declaración Jurada`;
    const ref = this.dialogService.open(AdjuntoModalComponent, {header, width: '50vw', closable: true,});
    /*ref.onClose.subscribe(detalle => {
      if (detalle) {
        if (!CompareObjects(data, detalle)) {
          this.pendiente = true;
          detalle.enviar = true;
        }
      }
    })*/
  }

  toast(severty: string, msg: string) {

    this.messageService.add({severity: severty, summary: "", detail: msg});
  }

  openModalAnexos() {
    this.ref = this.dialogService.open(ModalEvaluacionAnexosComponent, {
      header: "Evaluación Anexo",
      width: "50%",
      contentStyle: {"max-height": "500px", overflow: "auto"},
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObserved = resp;
          this.isValid = false;
          this.isPending = false;
        } else {
          this.isValidatingAnexo.emit(true)
          this.isObserved = false;
          this.isValid = true;
          this.isPending = false;
        }
      }
    });
  }

  openModalArea() {
    this.ref = this.dialogService.open(ModalEvaluarAreaComponent, {
      header: "Validación Superposición",
      width: "80%",
      contentStyle: {"max-height": "500px", overflow: "auto"},
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObservedArea = resp;
          this.isValidArea = false;
          this.isPendingArea = false;
        } else {
          this.isValidatingArea.emit(true)
          this.isObservedArea = false;
          this.isValidArea = true;
          this.isPendingArea = false;
        }
      }
    });
  }

  openModalPlanManejo() {
    this.ref = this.dialogService.open(ModalEvaluarPlanManejoComponent, {
      header: "¿Anexo tiene observaciones?",
      width: "50%",
      contentStyle: {"max-height": "500px", overflow: "auto"},
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObservedPlan = resp;
          this.isValidPlan = false;
          this.isPendingPlan = false;
        } else {
          this.isValidatingPlanManejo.emit(true)
          this.isObservedPlan = false;
          this.isValidPlan = true;
          this.isPendingPlan = false;
        }
      }
    });
  }


  agregarArchivo() {
    var data: SolicitudArchivoAdjuntoModel = {
      nombreArchivo: '',
      idSolicitud: this.solicitud.idSolicitud,
      idArchivoSolicitud: '',
      tipoArchivo: 'OTRARC',
      file: null
    };
    this.lstSolArchivoAdjunto.push(data);
    this.flagAgregaArchivo = true;
  }


  onFileSelectedAdjunto(event: any, index: number) {
    if (this.lstSolArchivoAdjunto[index].nombreArchivo) {
      this.lstSolArchivoAdjunto[index].file = event.target.files[0];
      this.flagAgregaArchivo = false;
    }

  }

  guardarArchivo() {
    //console.log(this.lstSolArchivoAdjunto);
    for (let item of this.lstSolArchivoAdjunto) {
      this.servSol.registrarArchivoSolicitudAdjunto(item).subscribe((data: any) => {
        console.log(data);
      })
    }
  }

  grabar(SolArchivoAdjuntoc: SolicitudArchivoAdjuntoModel) {
    var data: SolicitudArchivoAdjuntoModel = {
      nombreArchivo: SolArchivoAdjuntoc.nombreArchivo,
      idSolicitud: this.solicitud.idSolicitud,
      idArchivoSolicitud: '',
      tipoArchivo: 'OTRARC',
      file: this.SolArchivoAdjuntoc2.file
    };
    this.lstSolArchivoAdjunto.push(data);
    this.flagAgregaArchivo = true;


  }

  onFileSelectedAdjunto2(event: any) {
    this.SolArchivoAdjuntoc2.file = event.target.files[0];
    this.flagAgregaArchivo = false;
  }

  listarOrdenamiento() {
    let lstTempSolArchivoAdjunto: SolicitudArchivoAdjuntoModel[] = [];
    for (let item of this.lstSolArchivoAdjunto) {
      lstTempSolArchivoAdjunto.push(item);
    }
    this.lstSolArchivoAdjunto = [];
    
    for (let item of lstTempSolArchivoAdjunto) {
      
      var data: SolicitudArchivoAdjuntoModel = {
        nombreArchivo: item.nombreArchivo,
        idArchivoSolicitud: '',
        idSolicitud: item.idSolicitud,
        tipoArchivo: item.tipoArchivo,

        file: item.file
      };
      this.lstSolArchivoAdjunto.push(data);
    }

  }

  openEliminarArchivo(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.lstSolArchivoAdjunto.splice(index, 1);
        this.listarOrdenamiento();
      },
      reject: () => {
        //reject action
      }
    });
  }
}

