import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-evaluacion-anexo",
  templateUrl: "./modal-evaluacion-anexo.component.html",
})
export class ModalEvaluacionAnexosComponent implements OnInit {
  isObserved: boolean = false
  obj = {
    sunat: "",
    informacion:""
  };
  
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
    ) {}
    
    ngOnInit() {

  }

  agregar = () => {
    if (this.obj.sunat === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Descripcion de Sunat, es obligatorio",
      });
    } else if (this.obj.informacion === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Descripcion de Informacion, es obligatorio",
      });
    } else this.ref.close(this.obj);
  }

  cancelar(){
    this.isObserved = true;
    this.ref.close(this.isObserved);
  }
}
