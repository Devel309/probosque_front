import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "validar-requisitos-modal",
  templateUrl: "./validar-requisitos.modal.html",
})
export class ValidarRequisitosModal implements OnInit {
  obj = {
    pide: "",
    informacion: ""
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) { }

  ngOnInit() {

  }

  agregar = () => {
    // this.ref.close(this.obj);
    this.ref.close(true);
  };

  cancelar() {
    this.ref.close();
  }
}
