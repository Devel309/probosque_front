import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ParametroValorService} from 'src/app/service/parametro.service';

@Component({
  selector: 'solicitud-permiso-forestal-juridica',
  templateUrl: './solicitud-permiso-forestal-juridica.component.html',
  styleUrls: ['./solicitud-permiso-forestal-juridica.component.scss']
})
export class SolicitudPermisoForestalJuridica implements OnInit {
  constructor(private serv: ParametroValorService, private router: Router) {
  }

  lstTipoDocumento: any[] = [];

  @Input('dataBase') dataBase: any = {
    disabled: false
  };
  @Input('solicitudAcceso') solicitudAcceso: any = {};

  ngOnInit(): void {
    this.listarTipoDocumento();
  }

  listarTipoDocumento() {
    var params = {prefijo: 'TPER'}
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoDocumento = result.data;
      }
    );
  };

}
