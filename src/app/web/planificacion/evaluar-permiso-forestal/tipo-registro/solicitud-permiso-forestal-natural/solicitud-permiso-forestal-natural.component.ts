import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ParametroValorService } from "src/app/service/parametro.service";
import { ModalEvaluacionPersonaComponent } from "../model/modal-evaluacion-persona/modal-evaluacion-persona.component";
//import { MessageService } from 'primeng/api';

@Component({
  selector: "solicitud-permiso-forestal-natural",
  templateUrl: "./solicitud-permiso-forestal-natural.component.html",
  styleUrls: ["./solicitud-permiso-forestal-natural.component.scss"],
})
export class SolicitudPermisoForestalNatural implements OnInit {
  ref!: DynamicDialogRef;
  isValid: boolean = false;
  isPending: boolean = true;
  isObserved: boolean = false;
  @Output() isValidating: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(
    private serv: ParametroValorService,
    private router: Router,
    public dialogService: DialogService
  ) {}

  lstTipoDocumento: any[] = [];

  @Input("dataBase") dataBase: any = {
    disabled: false,
    tipoPersona: "",
  };

  @Input("solicitudAcceso") solicitudAcceso: any = {};
  checkedRepresentante: boolean = false;
  ngOnInit(): void {
    this.listarTipoDocumento();
    this.checkedRepresentante =
      this.solicitudAcceso.codigoTipoPersona == "TPERJURI";
  }

  listarTipoDocumento() {
    var params = { prefijo: 'TDOCI' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoDocumento = result.data;
    });
  }
  onOpenModal() {
    this.ref = this.dialogService.open(ModalEvaluacionPersonaComponent, {
      header: "Evaluación",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObserved = resp;
          this.isValid = false;
          this.isPending = false;
        } else {
          this.isValidating.emit(true);
          this.isObserved = false;
          this.isValid = true;
          this.isPending = false;
        }
      }
    });
  }
}
