import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-evaluacion-persona",
  templateUrl: "./modal-evaluacion-persona.component.html",
})
export class ModalEvaluacionPersonaComponent implements OnInit {
  isObserved: boolean = false
  obj = {
    pide: "",
    informacion:""
  };
  
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
    ) {}
    
    ngOnInit() {

  }

  agregar = () => {
    this.ref.close(this.obj);
  }; 
  cancelar(){
    this.isObserved = true;
    this.ref.close(this.isObserved);
  }
}
