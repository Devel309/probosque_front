import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-evaluacion',
  templateUrl: './modal-evaluacion.component.html',
  styleUrls: ['./modal-evaluacion.component.scss']
})
export class ModalEvaluacionComponent implements OnInit {

  constructor(public ref: DynamicDialogRef) { }

  ngOnInit(): void {
  }
  agregar = () => {
    this.ref.close(true);
  };

  cancelar() {
    this.ref.close();
  }

}
