import { ModalEvaluacionComponent } from './modal-evaluacion/modal-evaluacion.component';
import { ModalPublicacionComponent } from './modal-publicacion/modal-publicacion.component';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-bandeja-contrato-concesion-postulacion',
  templateUrl: './bandeja-contrato-concesion-postulacion.component.html',
  styleUrls: ['./bandeja-contrato-concesion-postulacion.component.scss']
})
export class BandejaContratoConcesionPostulacionComponent implements OnInit {

  @ViewChild('map') private mapViewEl!: ElementRef;

  data:any[] = [];
  hoy:Date = new Date();
  verModalZona:boolean = false;

  initializeMap(): void {
    ;
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const map = new Map({
      basemap: "hybrid"
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6
    });

  }


  constructor(private router: Router 
    ,public dialogService: DialogService
    ,private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
   
    this.data.push({});
    
  }

  postular()
  {
    this.router.navigateByUrl('/planificacion/bandeja-contrato-concesion', { state: { data: {} } });
  }

  pruebasPublicacion()
  {    
    const header = `Pruebas de Publicacion`;
    this.dialogService.open(ModalPublicacionComponent, {
      header,
      width: "50vw",
      closable: true,
    });    
  }

  validacionPublicacion()
  {    
    const header = `Validar Requisitos`;
    this.dialogService.open(ModalEvaluacionComponent, {
      header,
      width: "50vw",
      closable: true,
    });    
  }

  verZona()
  {   
    this.verModalZona = true;    
  }

  demo()
  {
    
    this.changeDetectorRef.detectChanges();
    this.initializeMap();
  }

}
