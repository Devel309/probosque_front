import { HttpErrorResponse } from '@angular/common/http';
import { ConsoleLogger } from '@angular/compiler-cli/src/ngtsc/logging';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { FILTRO1_TIPO_DOC_GESTION } from 'src/app/model/util/CodigoDocGestion';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { GenericoService } from 'src/app/service/generico.service';
import { OposicionService } from 'src/app/service/oposicion/oposicion.service';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { ConvertNumberToDate, DownloadFile, validarEmail } from 'src/app/shared/util';

@Component({
  selector: 'app-modal-nueva-oposicion',
  templateUrl: './modal-nueva-oposicion.component.html',
})

export class ModalNuevaOposicionComponent implements OnInit {
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private oposicionService: OposicionService,
    private parametroValorService: ParametroValorService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private pideService: PideService,
    private toast: ToastService,
    private genericoService: GenericoService,
    private usuarioServ: UsuarioService,
    private solicitudConcesionService: SolicitudConcesionService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  oposicionRequestEntity: any = {};

  idOposicionRespuesta: number = 0;
  objDoc: any = {};
  objDocResp: any = {};
  autoResize: boolean = true;
  tipoSolicitud: any[] = [];

  fileAdjuntarArchivo!: any;
  filePropuestaExploracion!: any;
  fileFormato!: any;
  fileInforme!: any;
  fileAmpliacion!: any;

  usuarioOpositor: any = {
    numeroDocumento: '',
  };

  listOpisicion: any[] = [];

  codigoTipoDocumento: string = '';
  isDni: boolean = false;
  isRuc: boolean = false;
  isTH: boolean = true;
  lstTipoDocumento: any[] = [];

  isFundada!: boolean;
  nombreDocumentoOposicion !:number;
  numeroDocumentoOposicion !:string;
  nombreDocumentoTH !:string;
  tituloHabilitante !:string;
  fechaDocumentoTH !:Date;

  fileArchivo: any = {};

  usuario!: UsuarioModel;
  tiposArchivos = TiposArchivos;
  minDate!: Date;
  minDateTH: Date = new Date();


  comboTipoDocumento: any[] = [];
  fileOposicion: any = {};
  fileResolucion: any = {};
  fileRespuesta: any = {};

  validaPIDEClass: boolean = false;
  isDisabledEdit: boolean = false;
  isShowDataResol: boolean = false;
  isShowDataResp: boolean = false;
  isEnviarArrfs: boolean = false;
  isEnviarTitularTH: boolean = false;

  /*************/
  objDocuemnto = { idFile: 0, nameFile: "", descripcion: "" };

  modelNumDoc: any = null;
  comboTipodoc: any[] = [];
  listaTablaDocumentos: any[] = [];

  numeroDocumento: string = '';
  descripcionDocumento: string = '';

  isShowBtnConformidad: boolean = false;

  txtDocumento : string = 'N° Documento de gestión';

  ngOnInit() {
    this.minDateTH.setDate(this.minDateTH.getDate() - 365);
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear, 0, 1);
    this.listarComboTipoDoc();
    this.listarTipoDocumento();
    this.oposicionRequestEntity.idUsuarioRegistro = this.usuario.idusuario;

    if (this.config.data.isNew) {
      this.isDisabledEdit = false;
    }

    if (this.config.data.isEdit) {

      this.isDisabledEdit = true;

      this.oposicionRequestEntity.idOposicion = this.config.data.item.idOposicion;
      this.oposicionRequestEntity.idProcesoPostulacion = this.config.data.item.documentoGestion;
      this.oposicionRequestEntity.tipoDocumentoGestion = this.config.data.item.tipoDocumentoGestion;
      this.oposicionRequestEntity.documentoGestion = this.config.data.item.documentoGestion;
      this.oposicionRequestEntity.fundada = this.config.data.item.fundada;
      this.oposicionRequestEntity.nombreDocumentoOposicion = this.config.data.item.nombreDocumentoOposicion;
      this.oposicionRequestEntity.numeroDocumentoOposicion = this.config.data.item.numeroDocumentoOposicion;
      this.oposicionRequestEntity.nombreDocumentoTH = this.config.data.item.nombreDocumentoTH;
      this.oposicionRequestEntity.tituloHabilitante = this.config.data.item.tituloHabilitante;
      this.oposicionRequestEntity.fechaDocumentoTH = this.config.data.item.fechaDocumentoTH;
      this.nombreDocumentoTH = this.config.data.item.nombreDocumentoTH;
      this.tituloHabilitante = this.config.data.item.tituloHabilitante;
      this.fechaDocumentoTH = this.config.data.item.fechaDocumentoTH;


      this.listarComboTipoDocumentosArchivo();
      this.obtenerOposicion();
    }
    if (this.config.data?.txtDocumento) {
        this.txtDocumento = this.config.data?.txtDocumento;
    }
  }

  //SERVICIOS
  obtenerOposicion = () => {
    this.seleccionarTipoDocumentoGestion();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.oposicionService.obtenerOposicion(this.config.data.item.idOposicion).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data?.length > 0) {
        let item: any = result.data[0];

        this.codigoTipoDocumento = item.persona.codigo_tipo_documento;
        if (item.persona.codigo_tipo_documento === CodigosTiposDoc.DNI) {
          this.isDni = true;
          this.usuarioOpositor.numeroDocumento = item.persona.numeroDocumento;
          this.usuarioOpositor.nombres = item.persona.nombres;
          this.usuarioOpositor.apellidoPaterno = item.persona.apellidoPaterno;
          this.usuarioOpositor.apellidoMaterno = item.persona.apellidoMaterno;
          this.validaPIDEClass = true;
        } else if (item.persona.codigo_tipo_documento === CodigosTiposDoc.RUC) {
          this.isRuc = true;
          this.usuarioOpositor.ruc = item.persona.numeroDocumento;
          this.usuarioOpositor.razonSocial = item.persona.razonSocialEmpresa;
          this.validaPIDEClass = true;
        }
        this.usuarioOpositor.idPersona = item.persona.idPersona;
        this.usuarioOpositor.telefono = item.persona.telefono;
        this.oposicionRequestEntity.descripcion = item.descripcion;
        this.usuarioOpositor.correo = item.persona.email_empresa;
        this.isFundada = item.fundada;
        this.nombreDocumentoOposicion = item.nombreDocumentoOposicion;
        this.numeroDocumentoOposicion  = item.numeroDocumentoOposicion;
        this.nombreDocumentoTH = item.nombreDocumentoTH;
        this.tituloHabilitante = item.tituloHabilitante;
        this.fechaDocumentoTH = new Date(item.fechaDocumentoTH);

        this.oposicionRequestEntity.respuesta = item.respuesta;

        this.listaTablaDocumentos = item.listaOposicionArchivo.filter((val: any) => val.tipoDocumento === TiposArchivos.OPOSICION);
        

        this.fileResolucion = item.listaOposicionArchivo.find((val: any) => (val.tipoDocumento !== TiposArchivos.OPOSICION && val.tipoDocumento !== TiposArchivos.RESP_OPOSICION)) || {};
        if (this.fileResolucion.nombre) {
          this.oposicionRequestEntity.tipoDocArchivo = this.fileResolucion.tipoDocumento;
          this.oposicionRequestEntity.asunto = this.fileResolucion.asunto;
          this.oposicionRequestEntity.fechaEmision = ConvertNumberToDate(this.fileResolucion.fechaEmision);
        }
        this.fileRespuesta = item.listaOposicionArchivo.find((val: any) => val.tipoDocumento === TiposArchivos.RESP_OPOSICION) || {};
        if (this.fileRespuesta.nombre) {
          this.oposicionRequestEntity.asuntoResp = this.fileRespuesta.asunto;
          this.oposicionRequestEntity.fechaResp = ConvertNumberToDate(this.fileRespuesta.fechaEmision);
        }

        switch (this.usuario.sirperfil) {
          case Perfiles.AUTORIDAD_REGIONAL:
            this.isShowBtnConformidad = true;
            this.isShowDataResol = true;
            if (item.fundada === null) this.isEnviarArrfs = true;
            if (item.respuesta) {
              this.isShowDataResp = true;
              this.isShowBtnConformidad = false;
            }
            break;
          case Perfiles.TITULARTH:
            this.isShowDataResp = true;


            if (item.fundada !== null) this.isShowDataResol = true;
            if(item.fueraFecha){
              this.isEnviarTitularTH = false;
            }else{
              if (!item.respuesta) this.isEnviarTitularTH = true;
            }

            break;
          case Perfiles.POSTULANTE:
            this.isShowDataResp = true;
            if (item.fundada !== null) this.isShowDataResol = true;
            if(item.fueraFecha){
              this.isEnviarTitularTH = false;
            }else{
              if (!item.respuesta) this.isEnviarTitularTH = true;
            }
            break;
          default:
            break;
        }

      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  };

  listarComboTipoDoc() {
    const params = { idTipoParametro: 77 }
    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success && result.data) {
        this.comboTipodoc = result.data.filter((item: any )=> FILTRO1_TIPO_DOC_GESTION.includes(item.codigo));;
      }
    })
  }

  listarTipoDocumento = () => {
    var params = { prefijo: 'TDOCI' };
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      if (result.success) {
        this.lstTipoDocumento = result.data.filter((item: any) => (item.codigo === CodigosTiposDoc.DNI || item.codigo === CodigosTiposDoc.RUC));
      }
    });
  };

  listarComboTipoDocumentosArchivo() {
    const params = { prefijo: 'TDOCGE' }
    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success) {
        result.data.splice(0, 0, { codigo: null, valorPrimario: 'Seleccione' });
        this.comboTipoDocumento = result.data;
      }
    });
  }

  registrarArchivoFilePFDM(file: any) {
    this.objDocuemnto.nameFile = file.nombre;
    this.objDocuemnto.idFile = file.idArchivo;
  }

  registrarOposicion(paramsIn: any, isActEstado: boolean = false) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.oposicionService.registrarOposicion(paramsIn).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result?.success && result.validateBusiness !== false) {
        this.toast.ok(result?.message);
        if (isActEstado) {
          if(this.oposicionRequestEntity.tipoDocumentoGestion=="TPMTHPA"){
            let aux = this.oposicionRequestEntity.fundada ? '27' : '5';
            this.actualizarEstadoProcesoPostulacion(aux);
          }else if(this.oposicionRequestEntity.tipoDocumentoGestion=="TPMPFDM"){
            if(this.oposicionRequestEntity.fundada){
              this.finalizarSolicitudConcesion();
            }

          }



        }

        this.ref.close(true);

      } else {
        this.toast.warn(result?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  };

  actualizarEstadoProcesoPostulacion = (idEstatusIn: any) => {

      const params = {
        "correoPostulante": this.config.data.item?.correoPostulante,
        "idProcesoPostulacion": this.oposicionRequestEntity.idProcesoPostulacion,
        "idStatus": idEstatusIn,
        "idUsuarioModificacion": this.usuario.idusuario,
      };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.ampliacionSolicitudService.actualizarEstadoProcesoPostulacion(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.success) this.ref.close(true);
        else this.toast.warn(result?.message);
      }, (error) => this.errorMensaje(error, true));


  };

  //BOTONES
  btnAgregarDocSustento() {
    if (!this.validarAgregarDoc()) return;
    let d = { idArchivo: this.objDocuemnto.idFile, nombre: this.nombreDocumentoTH, descripcion: this.objDocuemnto.descripcion };
    this.listaTablaDocumentos.push(d);
    this.objDocuemnto = { idFile: 0, nameFile: "", descripcion: "" };
    this.nombreDocumentoTH='';
  }

  btnDescargarDocSustento(fila: any) {
    if (fila.idArchivo) {
      let params = { idArchivo: fila.idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  btnEliminarDocSustento(fila: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(fila.idArchivo, this.usuarioServ.idUsuario).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success) {
        let d = this.listaTablaDocumentos.indexOf(fila);
        this.listaTablaDocumentos.splice(d, 1);
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnGuadarConformidad() {

  let statusConformidad=true;

  this.listaTablaDocumentos.forEach((reg: any, index) => {

      let params={

        idOposicionArchivo: reg.idOposicionArchivo,
        idArchivo: reg.idArchivo,
        conformidad: reg.conformidad,
        idUsuarioRegistro: this.usuario.idusuario

      }

      this.oposicionService.registrarArchivoDetalle(params).subscribe((result: any) => {

      },
      (error: HttpErrorResponse) => {
        statusConformidad=false
      });

      if (index == this.listaTablaDocumentos.length - 1) {
        if (statusConformidad){
          this.toast.ok("Se guardó la conformidad correctamente.");
        }else{
          this.toast.warn(Mensajes.MSJ_ERROR_CATCH);
        }
      }

    });




  }

  btnRegistrarOposicion() {
    if (!this.validarRegistrar()) return;
    const paramsOut = this.getParamsRegOpos();
    this.registrarOposicion(paramsOut);
  }

  btnRegistrarResolucion() {
    if (!this.validarEnviarResolucion()) return;
    const paramsOut = this.getParamsRegResol();
    this.registrarOposicion(paramsOut, true);
  }

  btnRegistrarRespuesta = () => {
    if (!this.validarEnviarRespuesta()) return;
    const paramsOut = this.getParamsRegRespo();
    this.registrarOposicion(paramsOut);
  };

  //FUNCIONES
  getParamsRegOpos() {
    // let listaDoc: any[] = [];
    // this.listaTablaDocumentos.forEach((item) => {
    //   let aux2 = { "idArchivo": item.idArchivo, "asunto": null, "fechaEmision": null };
    //   listaDoc.push(aux2);
    // });

    let item: any = {
      persona: {}
    };
    item.idOposicion = null;
    item.tipoDocumentoGestion = this.oposicionRequestEntity.tipoDocumentoGestion;
    item.documentoGestion = this.oposicionRequestEntity.idProcesoPostulacion;
    item.idProcesoPostulacion = this.oposicionRequestEntity.idProcesoPostulacion;
    item.descripcion = this.oposicionRequestEntity.descripcion;
    item.idUsuarioOpositor = this.usuario.idusuario;
    item.fundada = null;
    item.idUsuarioRegistro = this.usuario.idusuario;
    item.persona.telefono = this.usuarioOpositor.telefono;
    item.persona.correo = this.usuarioOpositor.correo;
    item.persona.tipo_documento = this.codigoTipoDocumento;
    if(this.isTH){
      item.nombreDocumentoTH = this.nombreDocumentoTH;
      item.tituloHabilitante = this.tituloHabilitante;
      item.fechaDocumentoTH = this.fechaDocumentoTH;
    }



    if (this.isRuc) {
      item.persona.numeroDocumento = this.usuarioOpositor.ruc;
      item.persona.razonSocialEmpresa = this.usuarioOpositor.razonSocialEmpresa;
    } else {
      item.persona.numeroDocumento = this.usuarioOpositor.numeroDocumento;
      item.persona.nombres = this.usuarioOpositor.nombres;
      item.persona.apellidoPaterno = this.usuarioOpositor.apellidoPaterno;
      item.persona.apellidoMaterno = this.usuarioOpositor.apellidoMaterno;
    }
    item.listaOposicionArchivo = this.listaTablaDocumentos
    return item;
  }

  validarAgregarDoc(): boolean {
    let valido = true;
    let msj=''
    if (!this.objDocuemnto.idFile) {
      valido = false;
      msj+="(*) Debe adjuntar un documento.\n";
    }
    if (!this.objDocuemnto.descripcion) {
      valido = false;
      msj+="(*) Debe agregar una descripción.\n";
    }

    if (!this.nombreDocumentoTH) {
      valido = false;
      msj+="(*) Debe agregar nombre del documento.\n";
    }

    if(!valido){
      this.toast.warn(msj);
    }

    return valido;
  }

  validarRegistrar(): boolean {
    let validado = true;
    let msj='';

    if (!this.oposicionRequestEntity.tipoDocumentoGestion) {
      validado = false;
     msj+='(*) Debe seleccionar un tipo de documento de gestión.\n';
    }
    if (!this.oposicionRequestEntity.idProcesoPostulacion) {
      validado = false;
     msj+='(*) Debe ingresar un número de documento de gestión.\n';
    }
    if (!this.codigoTipoDocumento) {
      validado = false;
     msj+='(*) Debe seleccionar un tipo documento de identidad.\n';
    }
    if (!this.usuarioOpositor.correo) {
      validado = false;
     msj+='(*) Debe ingresar un correo.\n';
    } else if (!validarEmail(this.usuarioOpositor.correo)) {
      validado = false;
     msj+='(*) Debe ingresar un correo válido.\n';
    }


    if (!this.usuarioOpositor.telefono) {
      validado = false;
     msj+='(*) Debe ingresar un número de celular.\n';
    }
    if (this.usuarioOpositor.telefono && this.usuarioOpositor.telefono.length !== 9) {
      validado = false;
     msj+='(*) Debe de ingresar un número de celular con 9 dígitos.\n';
    }
    if (!this.oposicionRequestEntity.descripcion) {
      validado = false;
     msj+='(*) Debe ingresar el motivo de la oposición.\n';
    }

    if (this.isRuc) {
      if (!this.usuarioOpositor.ruc) {
        validado = false;
       msj+='(*) Debe ingresar el número de RUC.\n';
      }

      if (!this.validaPIDEClass) {
        validado = false;
       msj+='(*) Debe validar RUC en SUNAT.\n';
      }
    }
    if (this.isDni) {
      if (!this.usuarioOpositor.numeroDocumento) {
        validado = false;
       msj+='(*) Debe ingresar el número de documento.\n';
      }

      if (!this.validaPIDEClass) {
        validado = false;
       msj+='(*) Debe validar DNI en RENIEC.\n';
      }
    }

    if (this.listaTablaDocumentos.length === 0) {
      validado = false;
     msj+='(*) Debe adjuntar al menos un documento de sustento.\n';
    }

    if(!validado){
      this.toast.warn(msj);
    }

    return validado;
  }


  /***********/

  getParamsRegResol() {

    const paramsOut = {
      respuesta: this.oposicionRequestEntity.respuesta,
      idOposicion: this.oposicionRequestEntity.idOposicion,
      tipoDocumentoGestion: this.oposicionRequestEntity.tipoDocumentoGestion,
      documentoGestion: this.oposicionRequestEntity.idProcesoPostulacion,
      idProcesoPostulacion: this.oposicionRequestEntity.idProcesoPostulacion,
      persona: {
        idPersona: this.usuarioOpositor.idPersona,
        numeroDocumento: this.isRuc ? this.usuarioOpositor.ruc : this.usuarioOpositor.numeroDocumento,
      },
      fundada: this.oposicionRequestEntity.fundada,
      nombreDocumentoOposicion: this.nombreDocumentoOposicion,
      numeroDocumentoOposicion: this.numeroDocumentoOposicion,
      descripcion: this.oposicionRequestEntity.descripcion,
      nombreDocumentoTH: this.nombreDocumentoTH,
      tituloHabilitante: this.tituloHabilitante,
      fechaDocumentoTH: this.fechaDocumentoTH,

      listaOposicionArchivo: [{
        "idArchivo": this.fileResolucion.idArchivo,
        "asunto": this.oposicionRequestEntity.asunto,
        "fechaEmision": this.oposicionRequestEntity.fechaEmision
      }]
    }



    return paramsOut;
  }

  getParamsRegRespo() {
    const paramsOut = {
      respuesta: true,
      idOposicion: this.oposicionRequestEntity.idOposicion,
      tipoDocumentoGestion: this.oposicionRequestEntity.tipoDocumentoGestion,
      documentoGestion: this.oposicionRequestEntity.idProcesoPostulacion,
      idProcesoPostulacion: this.oposicionRequestEntity.idProcesoPostulacion,
      nombreDocumentoTH: this.nombreDocumentoTH,
      tituloHabilitante: this.tituloHabilitante,
      fechaDocumentoTH: this.fechaDocumentoTH,
      persona: {
        idPersona: this.usuarioOpositor.idPersona,
        numeroDocumento: this.isRuc ? this.usuarioOpositor.ruc : this.usuarioOpositor.numeroDocumento,
      },
      fundada: this.oposicionRequestEntity.fundada,
      descripcion: this.oposicionRequestEntity.descripcion,

      listaOposicionArchivo: [{
        "idArchivo": this.fileRespuesta.idArchivo,
        "asunto": this.oposicionRequestEntity.asuntoResp,
        "fechaEmision": this.oposicionRequestEntity.fechaResp
      }]
    }

    return paramsOut;
  }

  validarEnviarRespuesta(): boolean {
    let validado = true;
    let msj=''

    if (!this.oposicionRequestEntity.fechaResp) {
      validado = false;
      msj+='(*) Debe ingresar una fecha de respuesta.\n';
    }
    if (!this.oposicionRequestEntity.asuntoResp) {
      validado = false;
      msj+='(*) Debe ingresar un asunto de respuesta.\n';
    }
    if (!this.fileRespuesta.nombre) {
      validado = false;
      msj+='(*) Debe adjuntar documento de respuesta.\n';
    }

    if(!validado){
      this.toast.warn(msj);
    }
    return validado;
  }

  validarEnviarResolucion(): boolean {
    let validado = true;
    let msj=''

    if (this.oposicionRequestEntity.fundada === undefined || this.oposicionRequestEntity.fundada === null) {
      validado = false;
      msj+='(*) Debe eligir fundada o infundada.\n';
    }
    if (!this.oposicionRequestEntity.tipoDocArchivo) {
      validado = false;
      msj+='(*) Debe seleccionar un tipo de documento.\n';
    }
    if (!this.fileResolucion.nombre) {
      validado = false;
      msj+='(*) Debe adjuntar documento de resolución.\n';
    }
    if (!this.oposicionRequestEntity.fechaEmision) {
      validado = false;
      msj+='(*) Debe ingresar una fecha.\n';
    }
    if (!this.oposicionRequestEntity.asunto) {
      validado = false;
      msj+='(*) Debe ingresar un asunto.\n';
    }

    if (!this.nombreDocumentoOposicion) {
      validado = false;
      msj+='(*) Debe ingresar nombre del documento.\n';
    }

    if (!this.numeroDocumentoOposicion) {
      validado = false;
      msj+='(*) Debe ingresar número de documento.\n';
    }

    if(!validado){
      this.toast.warn(msj);
    }

    return validado;
  }



  setIdArchivoRegistrado(codigoIn: string, idArchivoIn: number) {
    if (codigoIn === TiposArchivos.OPOSICION) {
      this.fileOposicion.idArchivo = idArchivoIn;
    } else if (codigoIn === TiposArchivos.RESP_OPOSICION) {
      this.fileRespuesta.idArchivo = idArchivoIn;
    } else if (codigoIn === TiposArchivos.OPOS_TIPO_DOC) {
      this.fileResolucion.idArchivo = idArchivoIn;
    }
  }

  limpiarArchivoFile(codigoIn: string) {
    if (codigoIn === TiposArchivos.OPOSICION) {
      this.fileOposicion = {}
    } else if (codigoIn === TiposArchivos.RESP_OPOSICION) {
      this.fileRespuesta = {};
    } else if (codigoIn === TiposArchivos.OPOS_TIPO_DOC) {
      this.fileResolucion = {};
    }
  }

  registrarArchivoAdjunto(file: any, codigo: string) {
    let codigoOut = '';
    // Para adjuntar archivo resolución, primero se valida si selecciono un tipo de documento
    if (codigo === TiposArchivos.OPOS_TIPO_DOC) {
      if (!this.oposicionRequestEntity.tipoDocArchivo) {
        this.fileResolucion = {};
        this.toast.warn('(*) Debe seleccionar un tipo de documento.');
        return;
      }
      codigoOut = this.oposicionRequestEntity.tipoDocArchivo;
    } else {
      codigoOut = codigo;
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuario.idusuario, codigoOut, file.file).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
          this.setIdArchivoRegistrado(codigo, result.data);
        } else {
          this.toast.warn(result.message);
          this.limpiarArchivoFile(codigo);
        }
      },
      () => {
        this.limpiarArchivoFile(codigo);
        this.dialog.closeAll();
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    );
  }

  eliminarArchivoAdjunto(idAdjuntoOut: number, codigo: string) {
    if (!idAdjuntoOut) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idusuario).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
          this.limpiarArchivoFile(codigo);
        } else {
          this.toast.warn(result.message);
        }
      },
      () => {
        this.dialog.closeAll();
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    );
  }

  descargarPlantillaResolucion = () => {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.oposicionService.descargarFormatoResulucion().subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, () => {
      this.dialog.closeAll();
    });
  };

  seleccionarTipoDocumento = () => {
    if (this.codigoTipoDocumento === 'TDOCDNI') {
      this.isDni = true;
      this.isRuc = false;
    } else {
      this.isDni = true;
      this.isRuc = true;
    }
    this.usuarioOpositor = {};
  };

  seleccionarTipoDocumentoGestion(){
    if (this.oposicionRequestEntity.tipoDocumentoGestion === 'TPMTHPA' || this.oposicionRequestEntity.tipoDocumentoGestion === 'TPMTHPA' ) {

      this.isTH=true;
      this.txtDocumento="N° Proceso Postulación";
    }else if (this.oposicionRequestEntity.tipoDocumentoGestion === 'TPMPFDM'){
      this.isTH=false;
      this.txtDocumento="N° Documento de Gestión";
    }
  }

  changeTipoDoc() {
    this.cambiarValidPIDE(false)
  }


  validarPide(tipo:number) {
    if (!this.codigoTipoDocumento) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;
    }

    if (this.codigoTipoDocumento === CodigosTiposDoc.DNI) {
      if (!this.usuarioOpositor.numeroDocumento) {
        this.toast.warn('(*) Debe ingresar un número de DNI.');
        return;
      }
      if (this.usuarioOpositor.numeroDocumento.toString().length !== 8) {
        this.toast.warn('(*) El número de documento de DNI debe tener 8 digitos.');
        return;
      }
      this.consultarDNI({ "numDNIConsulta": this.usuarioOpositor.numeroDocumento });

    } else if (this.codigoTipoDocumento === CodigosTiposDoc.RUC) {
      if (!this.usuarioOpositor.ruc) {
        this.toast.warn('(*) Debe ingresar un número de RUC.');
        return;
      }
      if (this.usuarioOpositor.ruc.toString().length !== 11) {
        this.toast.warn('(*) El número de documento de RUC debe tener 11 digitos.');
        return;
      }

      if (tipo === 2) {
        this.consultarDNI({ "numDNIConsulta": this.usuarioOpositor.numeroDocumento });
        return;
      }

      this.consultarRazonSocial({ "numRUC": this.usuarioOpositor.ruc });

    }
  }
  consultarDNI(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.datosPersona) {
          let persona = result.dataService.datosPersona;
          this.usuarioOpositor.nombres = persona.prenombres;
          this.usuarioOpositor.apellidoPaterno = persona.apPrimer;
          this.usuarioOpositor.apellidoMaterno = persona.apSegundo;
          this.cambiarValidPIDE(true);
          this.toast.ok('Se validó el DNI en RENIEC.');
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(result.dataService.deResultado);
        }
      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
    }
    )
  }

  consultarRazonSocial(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.dataService && result.dataService) {

        if (result.dataService.respuesta.ddp_nombre) {
          let respuesta = result.dataService.respuesta;
          if (result.dataService.respuesta.esHabido) {
            this.cambiarValidPIDE(true);
            this.toast.ok('Se validó existencia de RUC en SUNAT.');
            this.usuarioOpositor.razonSocial = respuesta.ddp_nombre;
          } else {
            this.cambiarValidPIDE(false);
            this.toast.warn(`RUC ingresado en estado: ${result.dataService.respuesta.desc_estado} y ${result.dataService.respuesta.desc_flag22}.`);
          }
        } else {
          this.cambiarValidPIDE(false);
          this.toast.warn(`No se ha encontrado información para el RUC ${params.numRUC}.`);
        }

      } else {
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    }, () => {
      this.dialog.closeAll();
      this.cambiarValidPIDE(false);
      this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
    }
    )
  }

  cambiarValidPIDE(valor: boolean) {
    this.validaPIDEClass = valor;
  }

  cerrarModal() {
    this.ref.close();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  finalizarSolicitudConcesion(){
    const params = {
      "idSolicitudConcesion": this.oposicionRequestEntity.documentoGestion,
      "tipoEnvio": "FINALIZARSC",
      "idUsuarioModificacion": this.usuario.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.solicitudConcesionService.enviarSolicitudConcesion(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);

      }else{
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

}
