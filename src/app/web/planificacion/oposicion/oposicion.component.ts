import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { FILTRO1_TIPO_DOC_GESTION } from 'src/app/model/util/CodigoDocGestion';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { GenericoService } from 'src/app/service/generico.service';
import { OposicionService } from 'src/app/service/oposicion/oposicion.service';
import { ConvertDateToString, ConvertNumberToDate } from 'src/app/shared/util';
import { ModalNuevaOposicionComponent } from './modal-nueva-oposicion/modal-nueva-oposicion.component';

@Component({
  selector: 'app-oposicion',
  templateUrl: './oposicion.component.html',
})
export class OposicionComponent implements OnInit {
  tipoProcesoSelect: string = '';
  ref!: DynamicDialogRef;
  procesoPostulacionSelect!: string | null;
  tipoDocGestion!: string | null;
  selectedestadoOpsosicion!: string | null;
  estadosolicitudes: any[] = [];
  comboDocumentos: any[] = [];
  usuario!: UsuarioModel;
  isPerfilARFFS: boolean = false;
  validoFechaRespuesta: boolean = false;

  constructor(
    private oposicionService: OposicionService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private genericoServ: GenericoService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  procesosOfertaFiltros: any = {
    idProceso: 0,
    razonSocial: '',
    ruc: '',
    estado: '',
  };

  tipoSolicitud: any[] = [];
  listaOposicion: any[] = [];

  optionPage: any = { pageNum: 1, pageSize: 10, totalRecords: 0 };

  ngOnInit() {
    this.isPerfilARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    this.listarOposicion();
    this.listarEstados();
    this.listarComboDocumentos();
  }

  listarEstados() {
    this.estadosolicitudes = [
      { label: 'Seleccione', value: null },
      { label: 'Pendiente de Evaluación', value: 1 },
      { label: 'Fundado', value: 2 },
      { label: 'Infundado', value: 3 },
    ];
  }

  listarComboDocumentos() {
    const params = { idTipoParametro: 77 }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      if(result.success && result.data) {
        this.comboDocumentos = result.data.filter((item: any )=> FILTRO1_TIPO_DOC_GESTION.includes(item.codigo));
      }
    })
  }

  listarOposicion = () => {
    const params = {
      idEstado: this.selectedestadoOpsosicion || null,
      idOposicion: null,
      numDocumentoGestion: this.procesoPostulacionSelect,
      //NOTA: agregar filtro para tipoDocGestion,
      idUsuarioRegistro: this.isPerfilARFFS ? this.usuario.idusuario : null,
      idUsuarioPostulacion: this.isPerfilARFFS ? null : this.usuario.idusuario,
      tipoDocumentoGestion: this.tipoDocGestion,
      pageNum: this.optionPage.pageNum,
      pageSize: this.optionPage.pageSize
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.oposicionService.listarOposicion(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.data && resp.data.length > 0) {
        resp.data.forEach((element: any) => {
          if (element.fechaEnvio != null) {
            element.fechaEnvio = ConvertDateToString(ConvertNumberToDate(element.fechaEnvio));
          }
        });
        this.listaOposicion = resp.data;
        this.optionPage.totalRecords = resp.totalrecord;
      } else {
        this.listaOposicion = [];
        this.optionPage.totalRecords = 0;
        this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      }
    }, (error) => this.errorMensaje(error, true));
  };

  verOposicion = (item: any = null, typeConsult: number = 1) => {
    
    this.ref = this.dialogService.open(ModalNuevaOposicionComponent, {
      header: typeConsult === 1 ? 'Registrar Oposición' : 'Editar Oposición',
      width: '650px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto', minHeight: '350px' },
      data: {
        item: item,
        isNew: typeConsult === 1,
        isEdit: typeConsult === 2,
        txtDocumento : 'N° Proceso Postulación'
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) this.listarOposicion();
    });
  };

  verDetalle = (item: any) => {
    if(!item.validoPlazoRespuesta){
      this.validoFechaRespuesta = true;
      return;
    }
    this.verOposicion(item, 2);
  };

  evalOposicion(item: any) {

  }

  btnBuscar(){
    this.optionPage = { pageNum: 1, pageSize: 10, totalRecords: 0 };
    this.listarOposicion();
  }

  btnLimpiar() {
    this.procesoPostulacionSelect = null;
    this.selectedestadoOpsosicion = null;
    this.tipoDocGestion = null;
    this.optionPage = { pageNum: 1, pageSize: 10, totalRecords: 0 };
    this.listarOposicion();
  }

  loadData(event: any) {
    this.optionPage = { pageNum: event.first + 1, pageSize: event.rows, totalRecords: 0 };
    this.listarOposicion();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
