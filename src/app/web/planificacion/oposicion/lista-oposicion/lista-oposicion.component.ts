import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { OposicionService } from 'src/app/service/oposicion/oposicion.service';
import { ModalNuevaOposicionComponent } from '../modal-nueva-oposicion/modal-nueva-oposicion.component';

@Component({
  selector: 'app-lista-oposicion',
  templateUrl: './lista-oposicion.component.html',
})
export class ListaOposicionComponent implements OnInit {
  ref!: DynamicDialogRef;
  listaOposicion: any[] = [];

  constructor(
    private router: Router,
    public dialogService: DialogService,
    private messageService: MessageService,
    private oposicionService: OposicionService
  ) {}

  ngOnInit() {
    if (!localStorage.getItem('oposicion')) {
      this.retroceder();
    } else this.listarOposicion();
  }

  listarOposicion = () => {
    var obj = {
      fundada: null,
      idOposicion: null,
      idProcesoPostulacion: JSON.parse(' ' + localStorage.getItem('oposicion')),
    };

    this.oposicionService.listarOposicion(obj).subscribe((resp: any) => {
      this.listaOposicion = resp.data;
    });
  };

  retroceder = () => {
    this.router.navigate(['planificacion/bandeja-postulacion']);
  };

  verOposicion = (item: any = null) => {
    this.ref = this.dialogService.open(ModalNuevaOposicionComponent, {
      header: 'Enviar Respuesta',
      width: '70%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        item: item,
        isRespuesta: true,
        isNew: false,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarOposicion();
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se guardó correctamente',
        });
      }
    });
  };
}
