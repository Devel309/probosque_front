import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import {MatTabsModule} from '@angular/material/tabs';
import { SharedModule } from "@shared";
import { RequisitosMdpComponentPgmfa } from "./requisitos-mdp.component";
import { MesaPartesPgmfaModule } from "./tabs/mesa-partes-pgmfa/mesa-partes-pgmfa.module";

@NgModule({
  exports:[RequisitosMdpComponentPgmfa],
  declarations: [RequisitosMdpComponentPgmfa],
  imports: [CommonModule, MatTabsModule, SharedModule,MesaPartesPgmfaModule],
})
export class RequisitosMdpModule {}
