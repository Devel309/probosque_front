import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitosMdpComponentPgmfa } from './requisitos-mdp.component';

describe('RequisitosMdpComponent', () => {
  let component: RequisitosMdpComponentPgmfa;
  let fixture: ComponentFixture<RequisitosMdpComponentPgmfa>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequisitosMdpComponentPgmfa ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitosMdpComponentPgmfa);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
