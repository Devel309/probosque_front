import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesaPartesPgmfaComponent } from './mesa-partes-pgmfa.component';

describe('MesaPartesPgmfaComponent', () => {
  let component: MesaPartesPgmfaComponent;
  let fixture: ComponentFixture<MesaPartesPgmfaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesaPartesPgmfaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesaPartesPgmfaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
