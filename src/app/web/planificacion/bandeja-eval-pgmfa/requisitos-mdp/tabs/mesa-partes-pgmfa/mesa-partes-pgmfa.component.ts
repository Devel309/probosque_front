import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { ButtonsCreateQuery } from 'src/app/features/state/buttons-create.query';
import { ButtonsCreateStore } from 'src/app/features/state/buttons-create.store';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { CodigoEstadoEvaluacion } from 'src/app/model/util/CodigoEstadoEvaluacion';
import { CodigoEstadoMesaPartesEvaluacion } from 'src/app/model/util/CodigoEstadoMesaPartes';
import { MesaPartesService } from 'src/app/service/evaluacion/evaluacion-plan-operativo/mesa-partes.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { TabRequisitosTupaComponent } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.component';

@Component({
  selector: 'mesa-partes-pgmfa',
  templateUrl: './mesa-partes-pgmfa.component.html',
  styleUrls: ['./mesa-partes-pgmfa.component.scss'],
})
export class MesaPartesPgmfaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @ViewChild(TabRequisitosTupaComponent)
  TabRequisitosTupaComponent!: TabRequisitosTupaComponent;

  evaluacion: LineamientoInnerModel = new LineamientoInnerModel();

  codigoEdoMesaPartesEvaluacion = CodigoEstadoMesaPartesEvaluacion;
  codEstadoConforme: string = '';
  pendiente: boolean = false;

  accordeon_3: any = [];

  tabIndex: number = 0;

  evaluacionRequisitos: any = {
    fechaTramite: null,
    detalle: null,
  };

  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;

  tipoPlan: string = '';
  idMesaPartes: number = 0;
  longitudTupa: number = 0;

  isLoadingSubsanar$ = this.buttonQuerySubsanar.selectLoading();
  isLoadingEvaluador$ = this.buttonQueryEvaluador.selectLoading();

  disabledCodigoEstadoPlanPresentado: boolean = false;
  infoMesaPartes: boolean = false;

  constructor(
    private mesaPartesService: MesaPartesService,
    private toast: ToastService,
    private user: UsuarioService,
    private messageService: MessageService,

    private route: ActivatedRoute,
    private buttonQuerySubsanar: ButtonsCreateQuery,
    private buttonStoreSubsanar: ButtonsCreateStore,
    private buttonQueryEvaluador: ButtonsCreateQuery,
    private informacionGeneralService: InformacionGeneralService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.tipoPlan = String(this.route.snapshot.paramMap.get('tipoPlan'));
    this.obtenerEstadoPlan();
  }

  obtenerEstadoPlan() {
    this.disabledCodigoEstadoPlanPresentado = false;
    const body = {
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInfGeneralResumido(body)
      .subscribe((_res: any) => {
        if (_res.data.length > 0) {
          const estado = _res.data[0];

          this.disabledCodigoEstadoPlanPresentado =
            estado.codigoEstado == 'EMDPRES' ? false : true;
        }
      });
  }

  filtroRequisitosTUPA() {
    this.TabRequisitosTupaComponent.listMesaPartesFiltro(
      this.evaluacionRequisitos
    );
  }

  limpiarFiltroRequisitosTUPA() {
    this.evaluacionRequisitos = {
      fechaTramite: null,
      detalle: null,
    };

    this.TabRequisitosTupaComponent.filtro = {
      fechaTramite: null,
      detalle: null,
    };
    this.TabRequisitosTupaComponent.listMesaPartes();
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

  validacionConforme(listadoTupa: any) {
    const allItems =
      listadoTupa.filter((data: any) => data.conforme == 'true').length ==
      listadoTupa.length;

    if (!!allItems) {
      this.evaluacion.conforme = true;
    } else {
      this.evaluacion.conforme = false;
    }
  }

  guardarMesaPartes() {
    if (!this.evaluacion.observacion && !this.evaluacion.conforme) {
      this.errorMensaje('(*) Debe ingresar Observacón.');
      return;
    }

    // this.listMesaPartesDetalle = [];

    // this.listadoTupa.forEach((item: any) => {
    //   var obj = new MesaPartesDetalleModel(item);
    //   this.listMesaPartesDetalle.push(obj);
    // });

    var params = {
      // idMesaPartes: !!this.idMesaPartes ? this.idMesaPartes : 0,
      idPlanManejo: this.idPlanManejo,
      comentario: '',
      codigo: '',
      conforme: !!this.evaluacion.conforme
        ? this.codigoEdoMesaPartesEvaluacion.COMPLETADO
        : this.codigoEdoMesaPartesEvaluacion.OBSERVADO,
      observacion: !this.evaluacion.conforme ? this.evaluacion.observacion : '',
      detalle: '',
      descripcion: '',
      idUsuarioRegistro: this.user.idUsuario,
      // listarMesaPartesDetalle: this.listMesaPartesDetalle,
    };

    this.mesaPartesService.registrarMesaPartes(params).subscribe(
      (response: any) => {
        if (response.success) {
          if (this.evaluacion.conforme == true) {
            this.toast.ok(
              'Mesa de Partes validó los requisitos TUPA correctamente.'
            );
          } else {
            this.toast.ok(
              'Mesa de Partes validó los requisitos TUPA, el cual se encuentra observado.'
            );
          }
          this.pendiente = false;
        } else {
          this.toast.error(response?.message);
        }
      },
      () => {
        this.toast.error('Ha ocurrido un error, intente nuevamente.');
      }
    );
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  changeConforme(value: string) {}

  cambiarEstadoMdp(estado: string) {
    let body;

    if (estado == this.codigoEdoMesaPartesEvaluacion.OBSERVADO) {
      this.buttonStoreSubsanar.submit();
      body = {
        idMesaPartes: this.idMesaPartes,
        idPlanManejo: null,
        conforme: this.codigoEdoMesaPartesEvaluacion.OBSERVADO,
      };
    } else {
      this.buttonStoreSubsanar.submit();
      body = {
        idMesaPartes: this.idMesaPartes,
        idPlanManejo: null,
        conforme: estado,
      };
    }

    this.mesaPartesService
      .registrarMesaPartesMdp(body)
      .subscribe((data: any) => {
        if (data.success) {
          this.TabRequisitosTupaComponent.listMesaPartes();
          this.obtenerEstadoPlan();
          const url = 'planificacion/evaluacion/plan-general-manejo';
          this.router.navigate([url]);
        } else {
          if (estado == this.codigoEdoMesaPartesEvaluacion.OBSERVADO) {
            this.buttonStoreSubsanar.submitError('');
          } else {
            this.buttonStoreSubsanar.submitError('');
          }
        }
      });
  }
}
