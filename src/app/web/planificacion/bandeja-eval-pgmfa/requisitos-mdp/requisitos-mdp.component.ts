import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-requisitos-mdp',
  templateUrl: './requisitos-mdp.component.html',
  styleUrls: ['./requisitos-mdp.component.scss'],
})
export class RequisitosMdpComponentPgmfa implements OnInit {
  idPlanManejo: number=0;

  tabIndex: number = 0;

  disabled: boolean = false;

  constructor(private route: ActivatedRoute) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {}

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }
}
