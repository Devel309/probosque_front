import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '@shared';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { EspeciesCensadasModule } from 'src/app/shared/components/especies-censadas/especies-censadas.module';
import { EvaluacionSimpleModule } from 'src/app/shared/components/evaluacion-simple/evaluacion-simple.module';
import { InformeEvaluacionSharedModule } from 'src/app/shared/components/informe-evaluacion-shared/informe-evaluacion-shared.module';
import { InputButtonsCodigoModule } from 'src/app/shared/components/input-button-codigo/input-buttons.module';
import { InputButtonsModule } from 'src/app/shared/components/input-button/input-buttons.module';
import { RequisitoPgmfSimpleComponent } from 'src/app/shared/components/requisito-pgmf-simple/requisito-pgmf-simple.component';
import { RequisitoPgmfSimpleModule } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.module';
import { ResultadoEvaluacionSharedModule } from 'src/app/shared/components/resultado-evaluacion/resultado-evaluacion-shared.module';
import { SolicitarOpicionEvaluacionModule } from 'src/app/shared/components/solicitar-opicion-evaluacion/solicitar-opicion-evaluacion.module';
import { SolicitudSanSharedModule } from 'src/app/shared/components/solicitud-san-shared/solicitud-san-shared.module';
import { TableHistoricoPlanManejoModule } from 'src/app/shared/components/tabla-historico-plan-manejo/tabla-historico-plan-manejo.module';
import { VerticesGeograficosModule } from 'src/app/shared/components/vertices-geograficos/vertices-geograficos.module';
import { OsinforSharedModule } from '../../../../shared/components/osinfor-shared/osinfor-shared.module';
import { ArbolesAprovechablesComponent } from '../../bandeja-eval-pgmfa/requisitos-previos/tabs/tab-evaluacion-info-doc/components/arboles-aprovechables/arboles-aprovechables.component';
import { RequisitosPreviosPgmfaComponent } from './requisitos-previos-pgmfa.component';
import { anexosComponent } from './tabs/tab-evaluacion-info-doc/components/anexos/tab-anexos.component';
import { CensoComercialNoMaderableComponent } from './tabs/tab-evaluacion-info-doc/components/censo-comercial-no-maderable/censo-comercial-no-maderable.component';
import { CensoForestalMaderableComponent } from './tabs/tab-evaluacion-info-doc/components/censo-forestal-maderable/censo-forestal-maderable.component';
import { ConsultaSUNARPComponent } from './tabs/tab-evaluacion-info-doc/components/consulta-sunarp/consulta-sunarp.component';
import { FustalesComponent } from './tabs/tab-evaluacion-info-doc/components/fustales/fustales.component';
import { InventarioNoMaderableComponent } from './tabs/tab-evaluacion-info-doc/components/inventario-no-maderable/inventario-no-maderable.component';
import { ListaRegenteComponent } from './tabs/tab-evaluacion-info-doc/components/lista-regente/lista-regente.component';
import { TabEvaluacionInfoDoc } from './tabs/tab-evaluacion-info-doc/tab-evaluacion-info-doc';
import { TabEvaluarPgmfaComponent } from './tabs/tab-evaluar-pgmfa/tab-evaluar-pgmfa.component';
import { TabGestionarInspeccionComponent } from './tabs/tab-gestionar-inspeccion/tab-gestionar-inspeccion.component';
import { OpinionesComponent } from './tabs/tab-opiniones/opiniones.component';
import { TabOpinionesComponent } from './tabs/tab-opiniones/tab-opiniones.component';
import { TabPresentacionPgmfaComponent } from './tabs/tab-presentacion-pgmfa/tab-presentacion-pgmfa.component';
import { TabRegistrarInformacionComponent } from './tabs/tab-registrar-informacion/tab-registrar-informacion.component';
import { TabRegistrarResultadosPoac } from './tabs/tab-registrar-resultados/tab-registrar-resultados.component';
import { TabRegistrarSubsanarComponent } from './tabs/tab-registrar-subsanar/tab-registrar-subsanar.component';
import { TabRemitirInformacionComponent } from './tabs/tab-remitir-informacion/tab-remitir-informacion.component';
import { RequisitoPgmfaSimpleComponent } from './tabs/tab-requisitos-pgmf/requisito-pgmf-simple/requisito-pgmfa.component';
import { TabRequisitosPgmfComponent } from './tabs/tab-requisitos-pgmf/tab-requisitos-pgmf.component';
import { TabResolverSolicitudComponent } from './tabs/tab-resolver-solicitud/tab-resolver-solicitud.component';
import { TabSolicitarSubsanarComponent } from './tabs/tab-solicitar-subsanar/tab-solicitar-subsanar.component';
import { TabZonificacionOrdenamientoInternoAreaComponent } from './tabs/tab-verificar-informacion-pmff/625OrdenamientoInterno/dema/tab-zonificacion-ordenamiento-interno-area.component';
import { PmfiOrdenamientoInternoComponent } from './tabs/tab-verificar-informacion-pmff/625OrdenamientoInterno/pmfi/pmfi-ordenamiento-interno.component';
import { TabRecursosForestalesMaderablesComponent } from './tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/dema/maderable/tab-recursos-forestales-maderables.component';
import { TabActividadesAprovechamientoEquiposComponent } from './tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/dema/tab-actividades-aprovechamiento-equipos.component';
import { AprovechamientoFormComponent } from './tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/pmfi/aprovechamiento-form/aprovechamiento-form.component';
import { PmfiManejoAprovechamiento2Component } from './tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/pmfi/pmfi-manejo-aprovechamiento.component';
import { TabSistemaManejoLaboralesSilviculturalesComponent } from './tabs/tab-verificar-informacion-pmff/628LaboresSilviculturales/dema/tab-sistema-manejo-laborales-silviculturales.component';
import { LaboresSilviculturalesFormComponent } from './tabs/tab-verificar-informacion-pmff/628LaboresSilviculturales/pmfi/labores-silviculturales-form/labores-silviculturales-form.component';
import { PmfiManejoAprovechamientoComponent } from './tabs/tab-verificar-informacion-pmff/628LaboresSilviculturales/pmfi/pmfi-manejo-aprovechamiento.component';
import { TabMedidasProteccionUnidadManejoForestalComponent } from './tabs/tab-verificar-informacion-pmff/629MedidasProteccion/dema/tab-medidas-proteccion-unidad-manejo-forestal.component';
import { TabCronogramaActividadesComponent } from './tabs/tab-verificar-informacion-pmff/cronograma/dema/tab-cronograma-actividades.component';
import { PmfiCronogramaComponent } from './tabs/tab-verificar-informacion-pmff/cronograma/pmfi-cronograma.component';
import { TabImpactosAmbientalesNegativosComponent } from './tabs/tab-verificar-informacion-pmff/impactosAmbientales/dema/tab-impactos-ambientales-negativos.component';
import { PmfiImpactoAmbientalNegativoComponent } from './tabs/tab-verificar-informacion-pmff/impactosAmbientales/pmfi/pmfi-impacto-ambiental-negativo.component';
import { TabVerificarInformacionPmffComponent } from './tabs/tab-verificar-informacion-pmff/tab-verificar-informacion-pmff.component';
import { HistoricoTabularComponent } from './tabs/tab-verificar-ubicacion-espacial/components/historico-tabular/historico-tabular.component';
import { TabVerificarUbicacionEspacialComponent } from './tabs/tab-verificar-ubicacion-espacial/tab-verificar-ubicacion-espacial.component';


const COMMON_DECLARATIONS=[
  RequisitosPreviosPgmfaComponent,
  TabEvaluarPgmfaComponent,
  TabEvaluacionInfoDoc,
  anexosComponent,
  ArbolesAprovechablesComponent,
  CensoComercialNoMaderableComponent,
  CensoForestalMaderableComponent,
  ConsultaSUNARPComponent,
  FustalesComponent,
  InventarioNoMaderableComponent,
  ListaRegenteComponent,
  TabGestionarInspeccionComponent,
  OpinionesComponent,
  TabOpinionesComponent,
  TabPresentacionPgmfaComponent,
  TabRegistrarInformacionComponent,
  TabRegistrarResultadosPoac,
  TabRegistrarSubsanarComponent,
  TabRemitirInformacionComponent,
  TabResolverSolicitudComponent,
  TabSolicitarSubsanarComponent,
  TabZonificacionOrdenamientoInternoAreaComponent,
  PmfiOrdenamientoInternoComponent,
  TabRecursosForestalesMaderablesComponent,
  TabActividadesAprovechamientoEquiposComponent,
  AprovechamientoFormComponent,
  PmfiManejoAprovechamiento2Component,
  TabSistemaManejoLaboralesSilviculturalesComponent,
  LaboresSilviculturalesFormComponent,
  PmfiManejoAprovechamientoComponent,
  TabMedidasProteccionUnidadManejoForestalComponent,
  PmfiCronogramaComponent,
  TabCronogramaActividadesComponent,
  TabImpactosAmbientalesNegativosComponent,
  PmfiImpactoAmbientalNegativoComponent,
  TabVerificarInformacionPmffComponent,
  TabVerificarUbicacionEspacialComponent,
  HistoricoTabularComponent,
  TabRequisitosPgmfComponent,
  RequisitoPgmfaSimpleComponent

  
]


@NgModule({
  exports: COMMON_DECLARATIONS,
  declarations:COMMON_DECLARATIONS,
  imports: [
    CommonModule,
    MatTabsModule,
    SharedModule,
    AccordionModule,
    TableModule,
    ButtonModule,
    ReactiveFormsModule,
    FormsModule,
    EspeciesCensadasModule,
    RadioButtonModule,
    VerticesGeograficosModule,
    OsinforSharedModule,
    ToastModule,
    SolicitarOpicionEvaluacionModule,
    MatDatepickerModule,
    SolicitudSanSharedModule,
    PaginatorModule,
    ResultadoEvaluacionSharedModule,
    DialogModule,
    ConfirmPopupModule,
    InformeEvaluacionSharedModule,
    InputButtonsCodigoModule,
    InputButtonsModule,
    CheckboxModule,
    MatTabsModule,
    TableHistoricoPlanManejoModule,
    EvaluacionSimpleModule,
    PanelModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    TableModule,
    PanelModule,
    DropdownModule,
    AccordionModule,
    RequisitoPgmfSimpleModule
  ],
})
export class RequisitoPreviosPgmfaModule {}
