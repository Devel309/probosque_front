import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CodigoEstadoPlanManejo} from '../../../../model/util/CodigoEstadoPlanManejo';
import {PlanManejoService, UsuarioService} from '@services';
import {Perfiles} from '../../../../model/util/Perfiles';

@Component({
  selector: 'app-requisitos-previos-pgmfa',
  templateUrl: './requisitos-previos-pgmfa.component.html',
  styleUrls: ['./requisitos-previos-pgmfa.component.scss']
})
export class RequisitosPreviosPgmfaComponent implements OnInit, AfterViewInit {

  idPlanManejo!: number;
  codigoEpica!: string;
  tabIndex: number = 0;
  disabled: boolean = false;
  perfil:string = "";

  obj: any ={};

    constructor(private route: ActivatedRoute,private planManejoService: PlanManejoService,private user: UsuarioService,) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));


  }

  ngOnInit(): void {
    this.obtenerPlan();
    this.perfil = this.user.usuario.sirperfil;
    if(this.perfil == Perfiles.OSINFOR || this.perfil == Perfiles.COMPONENTE_ESTADISTICO){
      this.disabled = true;
    }


  }


  ngAfterViewInit(){
    if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    }

  }
  setTabIndex(obj: any){

    var cod = obj.tab.replace(obj.codigoEvaluacionDet,'');
    //console.log("MAIN setTabIndex ", cod);

    if(cod === 'COD_0') this.tabIndex = 0;
    else if (cod === 'REEVRIIC') {
      this.tabIndex = 0;

      setTimeout (() => {

        this.tabIndex = 1;
      }, 1000);}
    else if (cod === 'COD_2') {this.tabIndex = 2;}
    else if (cod === 'RFNM') this.tabIndex = 3;
    else if (cod === 'COD_4') this.tabIndex = 4;
    else if (cod === 'INSOCU') this.tabIndex = 5;
    else if (cod === 'RREVAL') this.tabIndex = 6;
    else if (cod === 'COD_7') this.tabIndex = 7;
    else if (cod === 'COD_8') this.tabIndex = 8;
    else if (cod === 'COD_9') this.tabIndex = 9;
    else if (cod === 'COD_10') this.tabIndex = 10;
    else if (cod === 'COD_11') this.tabIndex = 11;
    else if (cod === 'COD_12') this.tabIndex = 12;
    else if (cod === 'COD_13') this.tabIndex = 13;
    else if (cod === 'COD_14') this.tabIndex = 14;
  }

  tab1(){
    this.tabIndex = 1;
  }

  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {
       this.codigoEpica = result.data.descripcion;

     /*  if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
        this.disabled = true;
      } */
    });

  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

}
