import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ResultadosEvaluacionDetalle } from "../../../../../../model/resultadosEvaluacion";
import { LoadingComponent } from "../../../../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { EvaluacionService } from "../../../../../../service/evaluacion/evaluacion.service";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { ParametroValorService, UsuarioService } from "@services";
import { ResultadosEvaluacionService } from "../../../../../../service/evaluacion/resultadosEvaluacion.service";
import { MatDialog } from "@angular/material/dialog";
import { ConfirmationService } from "primeng/api";
import { ResumenService } from "../../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";

@Component({
  selector: "app-tab-resolver-solicitud",
  templateUrl: "./tab-resolver-solicitud.component.html",
  styleUrls: ["./tab-resolver-solicitud.component.scss"],
})
export class TabResolverSolicitudComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  codigoFirmado: string = "";
  codigoLegal: string = "";
  codigoNotificacion: string = "";

  evaluacion: any = "";

  classEstado: string = "";
  archivoRecurso: any = {};
  radioSelect: string = "FAVO";
  listDocumentos: any[] = [];

  listaDetalle: ResultadosEvaluacionDetalle[] = [];

  observadoObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  favorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  desFavorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();

  idEvalResultadoCarga: number = 0;
  idEvalResultadoEnvio: number = 0;

  isSubmittingCreate$ = this.createQuery.selectSubmitting();


  perfiles: any[] = [];
  perfilesAgregados: any[] = [];
  perfilObj: any;

  constructor(
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private user: UsuarioService,
    private createStore: ButtonsCreateStore,
    private createQuery: ButtonsCreateQuery,
    private resultadosEvaluacionService: ResultadosEvaluacionService,
    private dialog: MatDialog,
    private resumenService: ResumenService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.codigoFirmado = this.codigoEpica + "IEFIRM";
    this.codigoLegal = this.codigoEpica + "CDLEGAL";
    this.codigoNotificacion = this.codigoEpica + "EVDNOTI";
    this.listPerfiles();
    this.listadoEnviar();
    this.listTipoDocumento();
    this.listadoCargaInforme();
  }

  listPerfiles() {
    var params = {
      prefijo: 'AUTSAN',
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.perfiles = [...response.data];
      });
  }


  agregarPerfil() {

    let idPerfilAgregar = this.perfilObj.codigo;
    let encontrado:boolean = false;

    this.perfilesAgregados.forEach((item:any)=>{
      if(item.codigo == idPerfilAgregar){
        encontrado = true;
        return;
      }
    })

    if(!encontrado){
      this.perfilesAgregados.push({ ...this.perfilObj, idEvalResultadoDet: 0 });
    }else{
      this.toast.warn("El perfil " +this.perfilObj.valorPrimario+" ya se encuentra agregado.");
    }

  }


  openEliminarPerfil(event: Event, index: number, perfil: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (perfil.idEvalResultadoDet != 0) {
          var params = {
            idEvalResultado: this.idEvalResultadoEnvio,
            idEvalResultadoDet: perfil.idEvalResultadoDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.resultadosEvaluacionService
            .eliminarEvaluacionesultado(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.perfilesAgregados.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.perfilesAgregados.splice(index, 1);
        }
      },
    });
  }

  listaDetalleEnviar: ResultadosEvaluacionDetalle[] = [];
  guardarEnviar() {
    this.perfilesAgregados.forEach((item) => {
      var obj = new ResultadosEvaluacionDetalle();
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.perfil = item.codigo;
      obj.codResultadoDet = 'NOPER';
      obj.idEvalResultadoDet = item.idEvalResultadoDet
        ? item.idEvalResultadoDet
        : 0;
      this.listaDetalleEnviar.push(obj);
    });

    var params = [
      {
        idEvalResultado: this.idEvalResultadoEnvio,
        idPlanManejo: this.idPlanManejo,
        codResultado: this.codigoEpica,
        subCodResultado: 'ENIN',
        descripcionCab: 'Envio de información',
        codInforme: '',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalleEnviar,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoEnviar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }


  listadoEnviar() {
    this.perfilesAgregados = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codResultado: this.codigoEpica,
      subCodResultado: 'ENIN',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoEnvio = element.idEvalResultado;
              this.filtrarPerfil(element);
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }
  datos: any;

  filtrarPerfil(element: any) {
    this.datos = this.perfiles.filter((x) => x.codigo === element.perfil);
    this.datos.forEach((item: any) => {
      this.perfilesAgregados.push({
        ...item,
        idEvalResultadoDet: element.idEvalResultadoDet,
      });
    });
  }


  listadoCargaInforme() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codResultado: this.codigoEpica,
      subCodResultado: "ENINEVA",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoCarga = element.idEvalResultado;
              this.radioSelect = element.codResultadoDet;
              if (element.codResultadoDet == "FAVO") {
                this.favorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == "DESF") {
                this.desFavorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == "OBSE") {
                this.observadoObj = new ResultadosEvaluacionDetalle(element);
              }
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  onChange(event: any) {
    this.listaDetalle = [];
    var obj = new ResultadosEvaluacionDetalle(event.obj);
    obj.codResultadoDet = event.codResultadoDet;
    obj.idUsuarioRegistro = this.user.idUsuario;

    this.listaDetalle.push(obj);

    var params = [
      {
        idEvalResultado: this.idEvalResultadoCarga,
        idPlanManejo: this.idPlanManejo,
        codResultado: this.codigoEpica,
        subCodResultado: "ENINEVA",
        descripcionCab: "Carga de Informe de Evaluación",
        codInforme: "radioSelect",
        idUsuarioRegistro: this.user.idUsuario,
        fechaConsentimiento: obj.fechaConsentimiento,
        listEvaluacionResultado: this.listaDetalle,
      },
    ];


    //return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoCargaInforme();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listTipoDocumento() {
    var params = {
      prefijo: "TDRESULTEVAL",
    };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listDocumentos = [...response.data];
      });
  }

  consolidadoPGMFA: any = null;

  generarDispositivoLegal() {
    this.createStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resumenService
      .generarDispositivoLegal(this.idPlanManejo, this.codigoEpica)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          this.consolidadoPGMFA = res;
          this.consolidadoPGMFA.contenTypeArchivo = "pdf";
          this.createStore.submitSuccess();
          this.descargarPGMFA();
          this.toast.ok("Se generó el archivo correctamente.");
        },
        (err) => {
          this.toast.warn(
            "Para generar el consolidado del Plan General de Manejo Forestal - PGMFA. Debe tener todos los items completados previamente."
          );
        }
      );
  }

  descargarPGMFA() {
    if (isNullOrEmpty(this.consolidadoPGMFA)) {
      this.toast.warn("Debe generar archivo consolidado");
      return;
    }
    descargarArchivo(this.consolidadoPGMFA);
  }
}
