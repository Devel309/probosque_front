import { HttpErrorResponse } from "@angular/common/http";
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, observados, ToastService } from "@shared";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ListarEvaluacionCampoRequest } from "src/app/model/evaluacion-campo";
import { CodigoEstadoEvaluacion } from "src/app/model/util/CodigoEstadoEvaluacion";
import { EvaluacionCampoService } from "src/app/service/evaluacion-campo.service";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { GenericoService } from "src/app/service/generico.service";
import { OpinionesService } from "src/app/service/opiniones/opiniones.service";

@Component({
  selector: "app-tab-registrar-resultados",
  templateUrl: "./tab-registrar-resultados.component.html",
  styleUrls: ["./tab-registrar-resultados.component.scss"],
})
export class TabRegistrarResultadosPoac implements OnInit, AfterViewInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;
  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;

  listarEvaluacionCampoRequest: ListarEvaluacionCampoRequest = new ListarEvaluacionCampoRequest();

  evaluacion: any;
  listLineamiento: any[] = [];
  listOpinion: any[] = [];
  listEvaluacionOcular: any[] = [];
  evaluaciones: any[] = [];
  totalRecords = 0;
  acordion = false;

  constructor(
    private dialog: MatDialog,
    private genericoService: GenericoService,
    private usuarioService: UsuarioService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private opinionesService: OpinionesService,
    private archivoService: ArchivoService,
    private evaluacionServ: EvaluacionCampoService
  ) {}
  ngAfterViewInit() {
    if (localStorage.getItem("EvalResuDet")) {
      this.acordion = true;
      localStorage.removeItem("EvalResuDet");
    }
  }

  ngOnInit(): void {
    this.listarEvaluacion();
    this.listarParametroLineamiento();
    this.listarSolicitudOpinionEvaluacion();
    this.listarEvaluacionCampo();
  }

  listarEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoEpica,
      codigoEvaluacionDetSub: this.codigoEpica + "RREVAL",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .obtenerEvaluacion(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
          } else {
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoEpica,
              codigoEvaluacionDet: this.codigoEpica,
              codigoEvaluacionDetSub: this.codigoEpica + "RREVAL",
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.usuarioService.idUsuario,
            };

            /*this.evaluacion.idEvaluacion = 0;
            this.evaluacion.idPlanManejo = this.idPlanManejo;
            this.evaluacion.codigoEvaluacion = this.codigoEpica;
            this.evaluacion.idUsuarioRegistro = this.usuarioService.idUsuario;
            this.evaluacion.listarEvaluacionDetalle = [];*/
          }
        }
      });
  }

  listarParametroLineamiento() {
    var params = {
      prefijo: "RESULEVALDP",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.genericoService
      .listarParametroLineamiento(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        this.listLineamiento = [...response.data];
      });
  }

  guardarEvaluacion() {
    let evaluacionDet: any = [];

    this.listLineamiento.forEach((item: any) => {
      if (item.listNivel2.length > 0) {
        item.listNivel2.forEach((item2: any) => {
          var objDet: any = {};
          objDet.idEvaluacionDet = item2.idEvaluacionDet
            ? item2.idEvaluacionDet
            : 0;
          objDet.codigoEvaluacionDet = this.codigoEpica;
          objDet.codigoEvaluacionDetSub = this.codigoEpica + "RREVAL";
          objDet.codigoEvaluacionDetPost = item2.codigo;
          objDet.conforme = item2.codigoSniffsDescripcion;
          objDet.observacion = item2.observacion;
          objDet.idUsuarioRegistro = this.usuarioService.idUsuario;
          evaluacionDet.push(objDet);
        });
      }
    });

    this.evaluacion.listarEvaluacionDetalle = evaluacionDet;
    if (!observados(this.evaluacion.listarEvaluacionDetalle, this.toast)) {
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .registrarEvaluacionPlanManejo(this.evaluacion)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response.message);
          this.listarParametroLineamiento();
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  listarSolicitudOpinionEvaluacion() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codSolicitud: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.opinionesService
      .listarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.listOpinion = [...response.data];
        });
        this.dialog.closeAll();
      });
  }

  descargarArchivo(idArchivo: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idArchivo: idArchivo,
    };
    this.archivoService
      .descargarArchivoGeneral(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.archivo,
            result.data.nombeArchivo,
            result.data.contenTypeArchivo
          );
        }
        (error: HttpErrorResponse) => {
          this.toast.warn(error.message);
          this.dialog.closeAll();
        };
      });
  }

  listarEvaluacionCampo() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.listarEvaluacionCampoRequest.documentoGestion = this.idPlanManejo;

    this.evaluacionServ
      .listarEvaluacionCampo(this.listarEvaluacionCampoRequest)
      .subscribe(
        (result: any) => {
          this.evaluaciones = [...result.data];
          this.totalRecords = result.totalrecord;
          this.dialog.closeAll();
        },
        (error) => this.dialog.closeAll()
      );
  }

  loadData(event: any) {
    this.listarEvaluacionCampoRequest.pageNum = event.first + 1;
    this.listarEvaluacionCampoRequest.pageSize = event.rows;

    this.listarEvaluacionCampo();
  }

  generarDescargarReporte() {
    const body = {
      idPlanManejo: this.idPlanManejo,
      tipoProceso: this.codigoEpica,
    };

    this.evaluacionService
      .generarInformeDEMAPMFI(body.idPlanManejo, body.tipoProceso)
      .subscribe((data: any) => {
        this.toast.ok("Se generó el Reporte de Evaluación correctamente.");

        DownloadFile(data.archivo, data.nombeArchivo, "");
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
