import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-tab-evaluar-pgmfa",
  templateUrl: "./tab-evaluar-pgmfa.component.html",
  styleUrls: ["./tab-evaluar-pgmfa.component.scss"],
})
export class TabEvaluarPgmfaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  codigoSubSeccion1: string = '';
  codigoSubSeccion2: string = '';

  constructor() {}

  ngOnInit(): void {
    this.setearCodigo();
  }

  setearCodigo() {
    if (this.codigoEpica == 'PMFIC') {
      this.codigoSubSeccion1 = 'PFMICORGAAnexo1';
      this.codigoSubSeccion2 = 'PFMICORGAAnexo2';
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
