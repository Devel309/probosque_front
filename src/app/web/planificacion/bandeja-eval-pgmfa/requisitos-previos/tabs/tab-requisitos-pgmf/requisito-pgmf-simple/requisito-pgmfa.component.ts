import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from "@angular/core";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";

@Component({
  selector: "requisito-pgmfa",
  templateUrl: "./requisito-pgmfa.component.html",
  styleUrls: ["./requisito-pgmfa.component.scss"],
})
export class RequisitoPgmfaSimpleComponent implements OnInit {
  @ContentChild("conatinerinner") conatinerinnerRef!: TemplateRef<any>;
  @Input("base") base: RequisitoSimpleModel = new RequisitoSimpleModel();
  @Input() default: boolean = true;
  @Output() onChange: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  evaluation(conforme: any) {
    this.onChange.emit(conforme);
  }
}

export class RequisitoSimpleModel {
  constructor(data?: any) {
    if (data) {
      this.titulo = data.titulo;
      this.evaluacion = data.evaluacion;
      this.classEstado = data.classEstado;
      this.icon = data.icon;
      return;
    }
  }
  titulo: string = "";
  icon: string = "";
  classEstado: string = "req_pendiente_class";
  evaluacion: LineamientoInnerModel = new LineamientoInnerModel();
}
