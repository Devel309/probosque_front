import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ToastService } from "@shared";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { Arboles, PotencialForestal } from "src/app/model/ProduccionForestalModel";
import { CensoForestalService } from "src/app/service/censoForestal";

@Component({
  selector: "app-censo-forestal-maderable",
  templateUrl: "./censo-forestal-maderable.component.html",
  styleUrls: ["./censo-forestal-maderable.component.scss"],
})
export class CensoForestalMaderableComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  listAnexo2Especies: any[] = [];
  variables: any[] = [{ var: "N(Arb/ha)" }, { var: "Vc(m3/ha)" }];
  areaTotalCensada: number = 0;
  especiesCensadas: number = 0;
  arbolesCensados: number = 0;
  Dap30a39N: number = 0;
  Dap40a49N: number = 0;
  Dap50a59N: number = 0;
  Dap60a69N: number = 0;
  Dap70a79N: number = 0;
  Dap80a89N: number = 0;
  Dap90aMasN: number = 0;
  TotalTipoBosqueN: number = 0;
  TotalPorAreaN: number = 0;
  porcentajeN: number = 0;

  Dap30a39vc: number = 0;
  Dap40a49vc: number = 0;
  Dap50a59vc: number = 0;
  Dap60a69vc: number = 0;
  Dap70a79vc: number = 0;
  Dap80a89vc: number = 0;
  Dap90aMasvc: number = 0;
  TotalTipoBosquevc: number = 0;
  TotalPorAreavc: number = 0;
  porcentajevc: number = 0;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private censoForestalService: CensoForestalService
  ) {}

  ngOnInit(): void {
    this.listAnexos();
  }
  listAnexos() {
    this.listAnexo2Especies = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService
      .censoComercialAprovechamientoMaderable(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (!!response.data) {
          this.areaTotalCensada = response.data.areaTotalCensada;
          this.especiesCensadas = response.data.contadorEspecies;
          this.arbolesCensados = response.data.numeroArbolesMaderables;

          response.data.resultadosRFMTablas.forEach(
            (element: any, index: number) => {
              var obj = new PotencialForestal();
              obj.nombreComun = element.nombreEspecie;
              this.variables.forEach((item: any, i: number) => {
                var ob1 = new Arboles();
                var ob2 = new Arboles();
                var ob3 = new Arboles();
                if (item.var == "N(Arb/ha)") {
                  ob1.var = "N(Arb/ha)";
                  ob1.Dap30a39 = Number(element.dap30a39);
                  this.Dap30a39N += Number(element.dap30a39);
                  ob1.Dap40a49 = Number(element.dap40a49);
                  this.Dap40a49N += Number(element.dap40a49);
                  ob1.Dap50a59 = Number(element.dap50a59);
                  this.Dap50a59N += Number(element.dap50a59);
                  ob1.Dap60a69 = Number(element.dap60a69);
                  this.Dap60a69N += Number(element.dap60a69);
                  ob1.Dap70a79 = Number(element.dap70a79);
                  this.Dap70a79N += Number(element.dap70a79);
                  ob1.Dap80a89 = Number(element.dap80a89);
                  this.Dap80a89N += Number(element.dap80a89);
                  ob1.Dap90aMas = Number(element.dap90aMas);
                  this.Dap90aMasN += Number(element.dap90aMas);
                  ob1.TotalTipoBosque = Number(element.dapTotalporHa);
                  this.TotalTipoBosqueN += Number(element.dapTotalporHa);
                  ob1.TotalPorArea = Number(element.dapTotalPC);
                  this.TotalPorAreaN += Number(element.dapTotalPC);
                  ob1.porcentaje =
                    Number(element.dapTotalPC) /
                    Number(this.areaTotalCensada) * 100;
                  this.porcentajeN += ob1.porcentaje;
                  obj.array.push(ob1);
                } else if (item.var == "Vc(m3/ha)") {
                  ob2.var = "Vc(m3/ha)";
                  ob2.Dap30a39 = Number(element.vol30a39);
                  this.Dap30a39vc += Number(element.vol30a39);
                  ob2.Dap40a49 = Number(element.vol40a49);
                  this.Dap40a49vc += Number(element.vol40a49);
                  ob2.Dap50a59 = Number(element.vol50a59);
                  this.Dap50a59vc += Number(element.vol50a59);
                  ob2.Dap60a69 = Number(element.vol60a69);
                  this.Dap60a69vc += Number(element.vol60a69);
                  ob2.Dap70a79 = Number(element.vol70a79);
                  this.Dap70a79vc += Number(element.vol70a79);
                  ob2.Dap80a89 = Number(element.vol80a89);
                  this.Dap80a89vc += Number(element.vol80a89);
                  ob2.Dap90aMas = Number(element.vol90aMas);
                  this.Dap90aMasvc += Number(element.vol90aMas);
                  ob2.TotalTipoBosque = Number(element.volTotalporHa);
                  this.TotalTipoBosquevc += Number(element.volTotalporHa);
                  ob2.TotalPorArea = Number(element.volTotalPC);
                  this.TotalPorAreavc += Number(element.volTotalPC);
                  ob2.porcentaje =
                    Number(element.volTotalPC) /
                    Number(this.areaTotalCensada) * 100;
                  this.porcentajevc += ob2.porcentaje;
                  obj.array.push(ob2);
                }
              });
              this.listAnexo2Especies.push(obj);
            }
          );
        } else {
          this.toast.warn(response.message);
        }
      });
  }
}
