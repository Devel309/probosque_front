import { Component, Input, OnInit } from "@angular/core";
import { CensoForestalService } from "src/app/service/censoForestal";

@Component({
  selector: "app-inventario-no-maderable",
  templateUrl: "./inventario-no-maderable.component.html",
  styleUrls: ["./inventario-no-maderable.component.scss"],
})
export class InventarioNoMaderableComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  colTipoBosque: any[] = [];
  listEspecies: any[] = [];
  liscols: any[] = ["Número de individuos (N)", "Unidad (C)"];
  lisVr: any[] = ["N°", "%", "**", "%"];
  listValores: any[] = [];
  tipoBosqueDtos: any[] = [];
  nHaInventariadas: number = 0;
  totalArboles: number = 0;
  totalVolumen: number = 0;
  totales: any[] = [];
  totalN: number = 0;
  totalP: number = 0;
  totalV: number = 0;
  totalPV: number = 0;
  totalNs = 0;
  totalPs = 0;
  totalVs = 0;
  totalPVs = 0;
  totalArbolesA: number = 0;
  totalVolumenB: number = 0;
  otraLista: any[] = [];
  listTot: any = [];

  constructor(private censoForestalService: CensoForestalService) {}

  ngOnInit(): void {
    this.listArbolesAprovechables();
  }

  listArbolesAprovechables() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.censoForestalService
      .aprovechableForestalNoMaderable(params)
      .subscribe((response: any) => {
        if (!!response.data.listaTipoBosque) {
          this.colTipoBosque = [...response.data.listaTipoBosque];
          this.listEspecies = [
            ...response.data.tablaArbolesAprovechablesMaderablesDtoList,
          ];
          this.nHaInventariadas = response.data.nHaInventariadas;

          this.colTipoBosque.forEach((item) => {
            this.liscols.push("Número de individuos (N)", "Unidad (C)");
            this.lisVr.push("N°", "%", "**", "%");
          });


          this.totalArbolesA = 0;
          this.totalVolumenB = 0;

          //totales especies x tipo de bosque
          this.colTipoBosque.forEach((x: any, index: number) => {
            let totA: number = 0;
            let totU: number = 0;
            this.listEspecies.forEach((element: any) => {
              totA += Number(element.tipoBosqueDtos[ index ].nArboles);
              totU += Number(this.removeNonNumeric(element.tipoBosqueDtos[ index ].unidadCantidad)) != 0 ?
                Number(this.removeNonNumeric(element.tipoBosqueDtos[ index ].unidadCantidad)) :
                1;
            });
            this.listTot.push(
              {
                totalA: totA != 0 ? totA : 1,
                totalU: totU != 0 ? totU : 1,
              }
            )
          });

          this.listEspecies.forEach((element: any) => {
            this.tipoBosqueDtos = element.tipoBosqueDtos;
            this.totalArboles = Number(element.nArbolesTotal);
            this.totalVolumen = Number(
              // element.unidadCantidadTotal.slice(0, -2)
              this.removeNonNumeric(element.unidadCantidadTotal)
            ) != 0 ? Number(
              // element.unidadCantidadTotal.slice(0, -2)
              this.removeNonNumeric(element.unidadCantidadTotal)
            ) : 1;
            this.totalArbolesA += Number(element.nArbolesTotal);
            this.totalVolumenB += Number(
              // element.unidadCantidadTotal.slice(0, -2)
              this.removeNonNumeric(element.unidadCantidadTotal)
            );
            this.listValores = [];
            element.tipoBosqueDtos.forEach((item: any, index: number) => {
              this.listValores.push(
                item.nArboles,
                // ((item.nArboles * 100) / this.totalArboles).toFixed(2),
                ((Number(item.nArboles) * 100) / this.listTot[index].totalA).toFixed(2),
                item.unidadCantidad,
                // ((Number(item.unidadCantidad.slice(0, -2)) * 100) / this.totalVolumen).toFixed(2)
                ((Number(this.removeNonNumeric(item.unidadCantidad)) * 100) / this.listTot[index].totalU).toFixed(2)
              );
            });
            this.otraLista.push({
              ...element,
              listaEspecies: this.listValores,
            });
          });
        }
        this.calcularTotales();
      });
  }

  calcularTotales() {
    this.colTipoBosque.forEach((x: any, index: number) => {
      this.totalN = 0;
      this.totalP = 0;
      this.totalV = 0;
      this.totalPV = 0;
      this.listEspecies.forEach((element: any) => {
        this.totalArboles = Number(element.nArbolesTotal);
        this.totalVolumen =
          // Number(element.unidadCantidadTotal.slice(0, -2)) != 0 ? Number(element.unidadCantidadTotal.slice(0, -2)) : 1;
          Number(this.removeNonNumeric(element.unidadCantidadTotal)) != 0 ? Number(this.removeNonNumeric(element.unidadCantidadTotal)) : 1;

       this.totalN += Number(element.tipoBosqueDtos[index].nArboles);
        this.totalP += Number(
          (
            (Number(element.tipoBosqueDtos[index].nArboles) * 100) /
            // this.totalArboles
            this.listTot[index].totalA
          ).toFixed(2)
        );
        // this.totalV += Number(element.tipoBosqueDtos[index].unidadCantidad.slice(0, -2));
        this.totalV += Number(this.removeNonNumeric(element.tipoBosqueDtos[index].unidadCantidad));
        this.totalPV += Number(
          (
            // (Number(element.tipoBosqueDtos[index].unidadCantidad.slice(0, -2)) * 100) /
            // this.totalVolumen
            (Number(this.removeNonNumeric(element.tipoBosqueDtos[index].unidadCantidad)) * 100) /
            this.listTot[index].totalU
          ).toFixed(2)
        );
      });

      this.totales.push(this.totalN, this.totalP, this.totalV, this.totalPV);
    });
  }

  removeNonNumeric(str: string) {
    var NumericString = str.replace(/[^0-9.]/g, '');
    return NumericString;
  }
}
