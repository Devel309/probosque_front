import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import {
  OrdenamientoInternoPmfiService,
  ParametroValorService,
  PlanManejoService,
  UsuarioService,
} from "@services";
import { LoadingComponent } from "../../../../../../components/loading/loading.component";
import { finalize, tap } from "rxjs/operators";
import { Mensajes } from "../../../../../../model/util/Mensajes";
import { CodigosDEMA } from "../../../../../../model/util/DEMA/DEMA";
import { EvaluacionArchivoModel } from "../../../../../../model/Comun/EvaluacionArchivoModel";
import { EvaluacionService } from "../../../../../../service/evaluacion/evaluacion.service";
import { observados, RespuestaTipo, ToastService } from "@shared";
import { MatDialog } from "@angular/material/dialog";
import { GenericoService } from "../../../../../../service/generico.service";
import { CodigoEstadoEvaluacion } from "../../../../../../model/util/CodigoEstadoEvaluacion";
import { PideService } from "../../../../../../service/evaluacion/pide.service";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ValidarCondicionMinimaComponent } from "../../../../../../shared/components/validar-condicion-minima/validar-condicion-minima.component";
import { ApiForestalService } from "../../../../../../service/api-forestal.service";
import { InformacionGeneralService } from "../../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { LaborSilviculturalServiceService } from "../../../../../../service/labor-silvicultural-service.service";
import { PmfiInformacionGeneral } from "../../../../../../model/pmfi-informacion-general";
import { InformacionAreaPmfiService } from "../../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { CodigoPMFIC } from "../../../../../../model/util/PMFIC/CodigoPMFIC";
import { Router } from "@angular/router";
import { MesaPartesService } from "src/app/service/evaluacion/evaluacion-plan-operativo/mesa-partes.service";
import { HttpErrorResponse } from "@angular/common/http";
import { Perfiles } from "src/app/model/util/Perfiles";

@Component({
  selector: "app-tab-verificar-informacion-pmff",
  templateUrl: "./tab-verificar-informacion-pmff.component.html",
  styleUrls: ["./tab-verificar-informacion-pmff.component.scss"],
})
export class TabVerificarInformacionPmffComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() codigoEpica!: string;
  listaEncabezados: any[] = [];

  lista: any[] = [];
  listaCabeceras: Cabeceras[] = [];

  listaOpciones: any[] = [];

  /* EVALUACION */

  //codigoTab = CodigosDEMA.TAB_1;

  codigoAcordeon1: string = CodigosDEMA.ACORDEON_1_1;

  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;

  evaluacion: any;
  obj: any = {};
  accordeon_6: any = [];

  products: any[] = [
    {
      image: "dfdsf",
      orders: [{ id: 0 }],
    },
  ];

  informacionGeneral: PmfiInformacionGeneral = new PmfiInformacionGeneral();
  codigoTab: string = "";
  codigoProceso: string = "";
  dniRucTitular: string = "";
  nombre_o_razonSocialTitular: string = "";
  nombreTitular: string = "";
  apellidoPaternoTitular: string = "";
  apellidoMaternoTitular: string = "";

  dniTitular: string = "";
  rucTitular: string = "";

  direccionTitular: string = "";
  tipoPersonaTitular: string = ""; //N-J
  nroPartidaRegistral: string = "";

  esReprLegal: string = "";
  dniRucRepresentante: string = "";
  nombre_o_razonSocialRepresentante: string = "";
  nombreRepresentante: string = "";
  apellidoPaternoRepresentante: string = "";
  apellidoMaternoRepresentante: string = "";

  direccionRepresentante: string = "";
  tipoPersonaRepresentante: string = ""; //N-J
  tipoDocumentoRepresentante: string = ""; //DNI  RUC

  validaPIDEReniecClass: boolean = false;
  validaPIDESunatClass: boolean = false;
  validaPIDESunarpClass: boolean = false;

  validaPIDEReniecClassRepre: boolean = false;
  validaPIDESunatClassRepre: boolean = false;
  validaPIDESunarpClassRepre: boolean = false;

  validaRegente: boolean = false;

  //LINEADP6251
  listOrdenInterno: any[] = [];
  RespuestaTipo = RespuestaTipo;
  CodigoPMFIC = CodigoPMFIC;

  tab_6 = false;
  tab_6_ = false;

  isActualizarMDP: boolean = false;
  perfil: string = "";
  perfiles = Perfiles;

  constructor(
    private dialog: MatDialog,
    private usuarioService: UsuarioService,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private evaluacionService: EvaluacionService,
    private genericoService: GenericoService,
    private pideService: PideService,
    private informacionGeneralService: InformacionGeneralService,
    private planManejoService: PlanManejoService,
    public dialogService: DialogService,
    private serviceExternos: ApiForestalService,
    private serviceApiForestal: ApiForestalService,
    private apiLavoresSivilculturales: LaborSilviculturalServiceService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private router: Router,
    private mesaPartesService: MesaPartesService
  ) {}

  ngOnInit(): void {
    this.codigoProceso = this.codigoEpica;
    this.codigoTab = this.codigoEpica + "RFNM";
    //this.listTipos();
    this.obtenerCabeceraEvaluacion();
    this.listarTitulos();

    //this.listarPlanes();
    this.listarInfGeneral();

    //LINEADP6251
    this.listarOrdenamiento();

    this.listarCoordenadas();

    this.perfil = this.usuarioService.usuario.sirperfil;

    this.obtenerEstadoPlan();
  }

  setTabAcordion(obj: any) {
    this.accordeon_6[1].array.forEach((e: any) => {
      if (
        obj.acordeon === e.codigo ||
        obj.acordeon.substring(0, obj.acordeon.length - 1) === e.codigo
      ) {
        e.acordion = true;
      }
    });
  }

  /*listarPlanes() {

    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {
      if(result.data){
        let info = result.data.infoGeneral;

        this.ruc = info.dniElaborador;
        this.razonSocial = info.nombreElaborador;

      }
    });
  }*/

  nombreCompleto: string = "";
  numeroDocumentoRegente: string = "";
  numeroLicenciaRegente: string = "";

  listarInfGeneral() {
    //this.informacionGeneral.idPlanManejo = this.idPlanManejo;

    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: this.codigoEpica,
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let info = response.data[0];

          this.informacionGeneral = new PmfiInformacionGeneral(info);

          if (info.regente) {
            this.nombreCompleto = info.regente.nombres + info.regente.apellidos;
            this.numeroDocumentoRegente = info.regente.numeroDocumento;
            this.numeroLicenciaRegente = info.regente.numeroLicencia;
          }

          var anios = this.calcDate(
            this.informacionGeneral.fechaInicioDema,
            this.informacionGeneral.fechaFinDema
          );
          this.informacionGeneral.vigencia = anios;

          this.dniRucTitular = info.dniElaboraDema;
          this.dniTitular = info.documentoRepresentante;
          this.rucTitular = info.dniElaboraDema;
          this.nombre_o_razonSocialTitular = info.nombreElaboraDema;
          this.nombreTitular = info.nombreRepresentante;
          this.apellidoPaternoTitular = info.apellidoPaternoRepresentante;
          this.apellidoMaternoTitular = info.apellidoMaternoRepresentante;

          this.direccionTitular = info.direccionLegalTitular;
          this.tipoPersonaTitular =
            info.codTipoPersona == "TPERJURI" ? "J" : "N";

          this.esReprLegal = info.esReprLegal; //S
          this.nroPartidaRegistral = info.observacion ? info.observacion : "";
          if (info.codTipoDocumentoRepresentante == "TDOCDNI") {
            this.tipoDocumentoRepresentante = "DNI";
            this.nombre_o_razonSocialRepresentante =
              info.nombreRepresentante +
              " " +
              info.apellidoPaternoRepresentante +
              " " +
              info.apellidoMaternoRepresentante;
            this.dniRucRepresentante = info.documentoRepresentante;
            this.direccionRepresentante = info.direccionLegalRepresentante;
          } else {
            this.tipoDocumentoRepresentante = "RUC";
            this.nombre_o_razonSocialRepresentante = info.representanteLegal;
            this.dniRucRepresentante = info.documentoRepresentante;
            this.direccionRepresentante = info.direccionLegalRepresentante;
          }
        }
      });
  }

  listCoordinatesAnexo: any[] = [];
  listarCoordenadas() {
    this.listCoordinatesAnexo = [];

    var params = {
      idInfBasica: this.CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.CodigoPMFIC.TAB_3_2,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            let newList: any = [];
            //this._idInfBasica = response.data[0].idInfBasica;
            //this._idInfBasicaDet = response.data[0].idInfBasicaDet;
            response.data.forEach((t: any) => {
              if (t.codInfBasicaDet !== null) {
                newList.push({
                  idInfBasica: t.idInfBasica,
                  idInfBasicaDet: t.idInfBasicaDet,
                  codInfBasicaDet: t.codInfBasicaDet,
                  anexo: t.descripcion,
                  vertice: t.puntoVertice,
                  este: t.coordenadaEste,
                  norte: t.coordenadaNorte,
                  referencia: t.referencia,
                });
              }
            });

            this.listCoordinatesAnexo = newList;
            //listCoordinatesAnexo = newList.groupBy((t: any) => t.anexo);
            //this.calculatArea();
          }
        } else {
          //this._idInfBasicaDet = 0;
        }
        //this.idInfBasicaS.emit(this._idInfBasica);
      });
  }

  calcDate(value1: Date, value2: Date) {
    if (value1 && value2) {
      const date1 = new Date(value1);
      const date2 = new Date(value2);
      var months;
      months = (date2.getFullYear() - date1.getFullYear()) * 12;
      months -= date1.getMonth();
      months += date2.getMonth();
      months <= 0 ? 0 : months;
      var years = Math.floor(months / 12);
      return years;
    }
    return 0;
  }

  listarTitulos() {
    var params = {
      prefijo: "LINEADP",
      idPlanManejo: this.idPlanManejo,
    };

    this.genericoService
      .listarParametroLineamiento(params)
      .subscribe((response: any) => {
        //this.listaOpciones = [...response.data];

        if (response.data.length > 0) {
          response.data.map((response: any) => {
            const accordeon = new accordeon_6();
            accordeon.title = response.valorPrimario.substr(4);
            accordeon.codigo = response.codigo;
            accordeon.idEvaluacionDet = response.idEvaluacionDet;
            accordeon.observacion = response.observacion;
            accordeon.codigoSniffsDescripcion =
              response.codigoSniffsDescripcion;
            accordeon.codigoSniffs = response.codigoSniffs;

            response.listNivel2.map((value: any) => {
              const body = new accordionDes(value);
              body.codigoSniffs = value.codigoSniffs;
              body.title = value.valorPrimario.substr(6);
              body.codigo = value.codigo;

              const includesT = [":"];
              const isTitle = includesT.includes(value.valorSecundario);

              if (isTitle || body.codigoSniffs == "N") {
                body.parrafo = value.valorSecundario;
                body.titleTabla = "";
              } else {
                body.titleTabla = value.valorSecundario;
                body.parrafo = "";
              }

              if (value.listNivel3.length) {
                value.listNivel3.map((res: any) => {
                  body.array.push(res);
                });
              }
              accordeon.array.push(body);
            });

            this.accordeon_6.push(accordeon);
          });

          if (localStorage.getItem("EvalResuDet")) {
            this.obj = JSON.parse(
              "" + localStorage.getItem("EvalResuDet")?.toString()
            );
            this.tab_6 = true;
            localStorage.removeItem("EvalResuDet");
            this.accordeon_6[1].array.forEach((e: any) => {
              e.acordion = false;
            });
            //console.log("Array",this.accordeon_6[1].array);
            this.setTabAcordion(this.obj);
          }
        }
      });
  }

  listTipos() {
    var params = {
      prefijo: "LINEADP",
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.listaEncabezados = [...response.data];

        let cabecera3: string = "";
        let cabecera_segfundo_level: string = "";

        this.listaEncabezados.forEach((e: any) => {
          if (e.valorTerciario.length == 3) {
            cabecera3 = e.valorTerciario;
            this.listaCabeceras.push(
              new Cabeceras({
                codigo: e.valorTerciario,
                descripcion: e.valorPrimario,
                detalle: e.valorSecundario,
              })
            );
          } else if (e.valorTerciario.length == 5) {
            cabecera_segfundo_level = e.valorTerciario;
            let index = this.listaCabeceras.findIndex(
              (i: any) => i.codigo.substring(0, 3) == cabecera3
            );
            if (index >= 0) {
              this.listaCabeceras[index].titulos.push(
                new Titulos({
                  codigo: e.valorTerciario,
                  descripcion: e.valorPrimario,
                  detalle: e.valorSecundario,
                })
              );
            }
          } else if (e.valorTerciario.length == 7) {
            let index = this.listaCabeceras.findIndex(
              (i: any) => i.codigo.substring(0, 3) == cabecera3
            );
            if (index >= 0) {
              let index2 = this.listaCabeceras[index].titulos.findIndex(
                (i: any) => i.codigo.substring(0, 5) == cabecera_segfundo_level
              );

              if (index2 >= 0) {
                this.listaCabeceras[index].titulos[index2].subtitulos.push(
                  new Subtitulos({
                    codigo: e.valorTerciario,
                    descripcion: e.valorPrimario,
                    detalle: e.valorSecundario,
                  })
                );
              }
            }
          }
        });
      });
  }

  evaluarPlan() {
    this.router.navigateByUrl(
      "/planificacion/plan-general-manejo/" + this.idPlanManejo
    );
  }

  registrarEvaluacion(codigoAcordeon: string) {
    let listaDetalle: any[] = [];

    this.accordeon_6.forEach((e: any) => {
      if (e.codigoSniffs == "S") {
        let evaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
          codigoEvaluacionDet: this.codigoProceso,
          codigoEvaluacionDetSub: this.codigoTab,
          codigoEvaluacionDetPost: e.codigo,
          conforme: e.codigoSniffsDescripcion,
          observacion: e.observacion,
          idEvaluacionDet: e.idEvaluacionDet ? e.idEvaluacionDet : 0,
        });

        listaDetalle.push(evaluacion);
      }

      e.array.forEach((e: any) => {
        if (e.codigo == codigoAcordeon) {
          if (e.codigoSniffs == "S") {
            let evaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel(
              {
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                codigoEvaluacionDetPost: e.codigo,
                conforme: e.codigoSniffsDescripcion,
                observacion: e.observacion,
                idEvaluacionDet: e.idEvaluacionDet ? e.idEvaluacionDet : 0,
              }
            );

            listaDetalle.push(evaluacion);
          }

          e.array.forEach((e: any) => {
            if (e.codigoSniffs == "S") {
              let evaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel(
                {
                  codigoEvaluacionDet: this.codigoProceso,
                  codigoEvaluacionDetSub: this.codigoTab,
                  codigoEvaluacionDetPost: e.codigo,
                  conforme: e.codigoSniffsDescripcion,
                  observacion: e.observacion,
                  idEvaluacionDet: e.idEvaluacionDet ? e.idEvaluacionDet : 0,
                }
              );

              listaDetalle.push(evaluacion);
            }

            /*
            e.listNivel4.forEach((e: any) => {
              if (e.codigoSniffs == 'S') {
                let evaluacion: EvaluacionArchivoModel =
                  new EvaluacionArchivoModel({
                    codigoEvaluacionDet: this.codigoProceso,
                    codigoEvaluacionDetSub: this.codigoTab,
                    codigoEvaluacionDetPost: e.codigo,
                    conforme: e.codigoSniffsDescripcion,
                    observacion: e.observacion,
                    idEvaluacionDet: e.idEvaluacionDet ? e.idEvaluacionDet : 0,
                  });

                listaDetalle.push(evaluacion);
              }

              e.listNivel5.forEach((e: any) => {
                if (e.codigoSniffs == 'S') {
                  let evaluacion: EvaluacionArchivoModel =
                    new EvaluacionArchivoModel({
                      codigoEvaluacionDet: this.codigoProceso,
                      codigoEvaluacionDetSub: this.codigoTab,
                      codigoEvaluacionDetPost: e.codigo,
                      conforme: e.codigoSniffsDescripcion,
                      observacion: e.observacion,
                      idEvaluacionDet: e.idEvaluacionDet ? e.idEvaluacionDet : 0,
                    });

                  listaDetalle.push(evaluacion);
                }
              });
            });
            */
          });
        }
      });
    });

    //console.log('listaEvaluaciones ', listaDetalle);
    // this.evaluacion.listarEvaluacionDetalle = listaDetalle;
    // console.log('evaluacion ', this.evaluacion);
    if (this.evaluacion) {
      this.evaluacion.listarEvaluacionDetalle = listaDetalle;
      if (!observados(this.evaluacion.listarEvaluacionDetalle, this.toast)) {
        return;
      } else {
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion(codigoAcordeon);
          });
      }
    }
  }

  validarMinimoUnObservado(lista: any) {}

  cambiarEstadoEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EEVAEVAL",
      idUsuarioModificacion: this.usuarioService.idUsuario,
    };
    this.planManejoService.actualizarPlanManejoEstado(params).subscribe(
      (result: any) => {},
      (error: HttpErrorResponse) => {}
    );
  }

  obtenerCabeceraEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
          } else {
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.usuarioService.idUsuario,
            };
          }
        }
      });
  }

  obtenerEvaluacion(codigoAcordeon: any) {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            let todasLasevaluaciones = this.evaluacion.listarEvaluacionDetalle;



            this.buscarAcordeon(codigoAcordeon, todasLasevaluaciones);
          }
        }
      });
  }

  buscarAcordeon(codigoAcordeon: string, todasEvaluaciones: any[]) {
    let listaDetalle: any[] = [];

    this.accordeon_6.forEach((e: any) => {
      if (e.codigoSniffs == "S") {
        //console.log("E A BUSCAR 1", e.codigo);
        let item = todasEvaluaciones.find(
          (re: any) => re.codigoEvaluacionDetPost === e.codigo
        );
        // console.log("ITEM " , item);
        if (item != null) {
          e.idEvaluacionDet = item.idEvaluacionDet;
        }
      }

      e.array.forEach((e: any) => {
        if (e.codigoSniffs == "S") {
          //console.log("E A BUSCAR 2", e.codigo);
          let item = todasEvaluaciones.find(
            (re: any) => re.codigoEvaluacionDetPost === e.codigo
          );
          //console.log("ITEM " , item);
          if (item != null) {
            e.idEvaluacionDet = item.idEvaluacionDet;
          }
        }

        e.array.forEach((e: any) => {
          if (e.codigoSniffs == "S") {
            //console.log("E A BUSCAR 3", e.codigo);
            let item = todasEvaluaciones.find(
              (re: any) => re.codigoEvaluacionDetPost === e.codigo
            );
            //console.log("ITEM " , item);
            if (item != null) {
              e.idEvaluacionDet = item.idEvaluacionDet;
            }
          }
        });
      });
    });
  }

  mostrarLista() {

  }

  siguienteTab() {
    this.siguiente.emit();
    //console.log("Listar ",this.listaCabeceras);
  }

  regresarTab() {
    this.regresar.emit();
  }

  validarPideReniec() {
    let params = {
      numDNIConsulta: this.dniTitular,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          if (result.dataService.datosPersona) {
            this.validaPIDEReniecClass = true;
            this.toast.ok("Se validó existencia de DNI en RENIEC.");
          } else {
            this.validaPIDEReniecClass = false;
            this.toast.warn("DNI ingresado no existe en RENIEC.");
          }
        } else {
          this.validaPIDEReniecClass = false;
          this.toast.error(
            "Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema."
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDEReniecClass = false;
        this.toast.error(
          "Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema."
        );
      }
    );
  }

  cambioDetalle(data: any) {
    data.observacion = "";
  }

  validarPideReniecRepresentanteLegal() {
    let params = {
      numDNIConsulta: this.dniRucRepresentante,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          if (result.dataService.datosPersona) {
            this.validaPIDEReniecClassRepre = true;
            this.toast.ok("Se validó existencia de DNI en RENIEC.");
          } else {
            this.validaPIDEReniecClassRepre = false;
            this.toast.warn("DNI ingresado no existe en RENIEC.");
          }
        } else {
          this.validaPIDEReniecClassRepre = false;
          this.toast.error(
            "Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema."
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDEReniecClassRepre = false;
        this.toast.error(
          "Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema."
        );
      }
    );
  }

  validarPideSunat() {
    if (this.rucTitular == "") {
      this.toast.warn("Debe ingresar un número de RUC.");
      return;
    }
    if (this.rucTitular.length != 11) {
      this.toast.warn("RUC ingresado no tiene 11 caracteres.");
      return;
    }

    let params = {
      numRUC: this.rucTitular,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          let respuesta = result.dataService.respuesta;
          if (respuesta.esHabido) {
            this.validaPIDESunatClass = true;
            this.toast.ok("Se validó existencia de RUC en SUNAT.");
          } else {
            this.validaPIDESunatClass = false;
            this.toast.warn("RUC ingresado no existe en SUNAT.");
          }
        } else {
          this.validaPIDESunatClass = false;
          this.toast.error(
            "Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema."
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDESunatClass = false;
        this.toast.error(
          "Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema."
        );
      }
    );
  }

  validarPideSunatRepresentante() {
    if (this.dniRucRepresentante == "") {
      this.toast.warn("Debe ingresar un número de RUC.");
      return;
    }
    if (this.dniRucRepresentante.length != 11) {
      this.toast.warn("RUC ingresado no tiene 11 caracteres.");
      return;
    }

    let params = {
      numRUC: this.dniRucRepresentante,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          let respuesta = result.dataService.respuesta;
          if (respuesta.esHabido) {
            this.validaPIDESunatClassRepre = true;
            this.toast.ok("Se validó existencia de RUC en SUNAT.");
          } else {
            this.validaPIDESunatClassRepre = false;
            this.toast.warn("RUC ingresado no existe en SUNAT.");
          }
        } else {
          this.validaPIDESunatClassRepre = false;
          this.toast.error(
            "Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema."
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDESunatClassRepre = false;
        this.toast.error(
          "Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema."
        );
      }
    );
  }

  ref!: DynamicDialogRef;

  validarCondiciones() {
    let param = {
      nombre: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      tipoDocumento: "",
      numeroDocumento: "",
    };

    param.tipoDocumento = this.tipoPersonaTitular == "N" ? "DNI" : "RUC";
    param.nombre = this.nombre_o_razonSocialTitular;
    param.apellidoPaterno = this.apellidoPaternoTitular;
    param.apellidoMaterno = this.apellidoMaternoTitular;
    param.numeroDocumento = this.dniRucTitular;

    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: "Validar Condiciones Mínimas",
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        idCondicionMinima: null,
        obj: param,
      },
    });
  }

  validarInformacionRegente() {
    if (this.numeroDocumentoRegente == "") {
      this.toast.warn("(*) Debe ingresar un número de documento de regente");
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.serviceApiForestal.consultarRegente().subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.dataService?.length > 0) {
            const regente = resp.dataService.find(
              (x: any) =>
                x.numeroDocumento.toString() === this.numeroDocumentoRegente
            );
            if (regente) {
              this.toast.ok(
                "Número de documento en base de datos de regente encontrado."
              );

              if (regente.numeroLicencia == this.numeroLicenciaRegente) {
                if (regente.descripcionEstado == "VIGENTE") {
                  this.toast.ok("La licencia se encuentra en estado VIGENTE");
                  this.validaRegente = true;
                } else {
                  this.toast.warn(
                    "La licencia no se encuentra en estado VIGENTE"
                  );
                  this.validaRegente = false;
                }
              } else {
                this.toast.warn(
                  "La licencia no concuerda con la registrada en BD."
                );
                this.validaRegente = false;
              }
            } else {
              this.toast.warn(
                "Número de documento en base de datos de regente  no encontrado."
              );
              this.validaRegente = false;
            }
          }
        },
        () => {
          this.dialog.closeAll();
          this.toast.error(Mensajes.MSJ_ERROR_CATCH);
        }
      );
    }
  }

  ngOnDestroy() {
    if (this.ref) {
      this.ref.close();
    }
  }

  validarPideSunarp() {
    /*let param = {
      apellidoMaterno: this.apellidoMaternoTitular,
      apellidoPaterno: this.apellidoPaternoTitular,
      nombres: this.nombreTitular,
      razonSocial: this.nombre_o_razonSocialTitular,
      tipoParticipante: this.tipoPersonaTitular,
    };

    this.consultarTitularidadSUNARP(param);*/

    this.toast.warn("El Servicio de validación no se encuentra disponible.");
  }

  consultarTitularidadSUNARP(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.serviceExternos
      .consultarTitularidadSUNARP(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {

        if (
          response.dataService &&
          response.dataService.respuesta != "" &&
          response.dataService.respuesta.respuestaTitularidad.length > 0
        ) {
          this.toast.ok(
            "Se validó la existencia de la Partida Registral en SUNARP"
          );
          const res = response.dataService.respuesta.respuestaTitularidad[0];
          this.nroPartidaRegistral = res.numeroPartida;
          this.validaPIDESunarpClass = true;
          this.actualizarInfGeneral();
        } else {


          this.validaPIDESunarpClass = false;
          this.validarPideSunarpJ();
        }
      });
  }

  validarPideSunarpJ() {
    let param = {
      tipoParticipante: "J",
      apellidoPaterno: "",
      apellidoMaterno: "",
      nombres: "",
      razonSocial: "CAJA MUNICIPAL AREQUIPA",
    };

    this.consultarTitularidadSUNARP(param);
  }

  actualizarInfGeneral() {
    const obj = new PmfiInformacionGeneral(this.informacionGeneral);
    obj.observacion = this.nroPartidaRegistral;

    this.informacionGeneralService
      .actualizarInformacionGeneralDema(obj)
      .subscribe((response: any) => {
        if (response.success) {
          this.listarInfGeneral();
        }
      });
  }

  obtenerEstadoPlan() {
    const body = {
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInfGeneralResumido(body)
      .subscribe((_res: any) => {
        if (_res.data.length > 0) {
          const estado = _res.data[0];
          this.isActualizarMDP =
            estado == "EMDCOMP" &&
            this.perfil == this.perfiles.AUTORIDAD_REGIONAL;
        }
      });
  }

  //LINEADP6251
  get totalArea() {
    return this.listOrdenInterno
      .map((i) => i.areaHA)
      .reduce((sum, x) => sum + x, 0);
  }

  listarOrdenamiento() {
    this.listOrdenInterno = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: this.codigoEpica, //'PMFI',
    };

    this.apiOrdenamiento
      .listarOrdenamiento(params)
      .subscribe((response: any) => {
        response.data[0].listOrdenamientoProteccionDet.forEach(
          (element: any) => {
            const item = element;
            let detalle = {
              idOrdenamientoProteccionDet: !!response.data[0].anexo
                ? item.idOrdenamientoProteccionDet
                : 0,
              codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
              categoria: item.categoria,
              areaHA: item.areaHA,
              observacion: item.observacion, //campo para saber si tiene asociado un shapefile
              descripcion: item.descripcion, //campo para guardar nombres de las capas
              actividad: item.actividad, //campo para guardar layerId(capa mapa)
              actividadesRealizar: item.actividadesRealizar, //campo para guardar groupId (grupo capa mapa)
              observacionDetalle: item.observacionDetalle, //campo para guardar color capa
            };
            this.listOrdenInterno.push(detalle);
          }
        );
      });
  }

  objDivisionAdministrativa?: any = {
    observacion: "Sin observaciones",
  };
  obtenerSistemaManejo() {
    this.apiLavoresSivilculturales
      .obtenerListaLaborSilviculturalCabecera(this.idPlanManejo)
      .subscribe((res: any) => {
        if (res?.data && res?.data !== undefined) {
          if (res.data.length === 1) {
            this.objDivisionAdministrativa = res.data.find(
              (x: any) => x.codActividad === "DIVICAD"
            );
          }
        }
      });
  }
}

export class Cabeceras {
  constructor(data?: any) {
    if (data) {
      this.codigo = data.codigo ? data.codigo : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.titulos = data.titulos ? data.titulos : [];
    }
  }

  codigo: string = "";
  descripcion: string = "";
  detalle: string = "";
  titulos: Titulos[] = [];
}

export class Titulos {
  constructor(data?: any) {
    if (data) {
      this.codigo = data.codigo ? data.codigo : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.subtitulos = data.subtitulos ? data.subtitulos : [];
    }
  }

  codigo: string = "";
  descripcion: string = "";
  detalle: string = "";
  subtitulos: Subtitulos[] = [];
}

export class Subtitulos {
  constructor(data?: any) {
    if (data) {
      this.codigo = data.codigo ? data.codigo : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.subsubtitulos = data.subsubtitulos ? data.subsubtitulos : [];
      this.idEvaluacionDet = data.idEvaluacionDet ? data.idEvaluacionDet : 0;
      this.codigoSniffsDescripcion = data.codigoSniffsDescripcion
        ? data.codigoSniffsDescripcion
        : "";
      this.codigoSniffs = data.codigoSniffs ? data.codigoSniffs : "";
    }
  }

  codigo: string = "";
  descripcion: string = "";
  detalle: string = "";
  idEvaluacionDet: number = 0;
  codigoSniffsDescripcion: string = "";
  codigoSniffs: string = "";
  subsubtitulos: SubSubtitulos[] = [];
}

export class SubSubtitulos {
  constructor(data?: any) {
    if (data) {
      this.codigo = data.codigo ? data.codigo : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
    }
  }

  codigo: string = "";
  descripcion: string = "";
  detalle: string = "";
}

export class accordeon_6 {
  title: string = "";
  observacion: string = "";
  codigo: string = "";
  codigoSniffsDescripcion: string = "";
  codigoSniffs: string = "";
  idParametro: number = 0;
  idEvaluacionDet: number = 0;
  body: accordionDes[] = [];
  array: accordionDes[] = [];

  constructor(obj?: any) {
    if (obj) {
      this.title = obj.title ? obj.title : "";
      this.observacion = obj.observacion ? obj.observacion : "";
      this.idParametro = obj.idParametro ? obj.idParametro : 0;
      this.idEvaluacionDet = obj.idEvaluacionDet ? obj.idEvaluacionDet : 0;
      this.codigo = obj.codigo ? obj.codigo : "";
      this.codigoSniffsDescripcion = obj.codigoSniffsDescripcion
        ? obj.codigoSniffsDescripcion
        : "";
      this.codigoSniffs = obj.codigoSniffs ? obj.codigoSniffs : "";
      this.array = obj.subAccordeon1 ? obj.subAccordeon1 : [];
    }
  }
}

export class accordionDes {
  title: string = "";
  titleTabla: string = "";
  observacion: string = "";
  idParametro: number = 0;
  idEvaluacionDet: number = 0;
  codigoSniffsDescripcion: string = "";
  codigoSniffs: string = "";
  codigo: string = "";
  lastbody1: any;
  parrafo: any;
  array: any = [];
  body: any = [];

  constructor(obj?: any) {
    if (obj) {
      this.title = obj.title ? obj.title : "";
      this.body = obj.body ? obj.body : [];
      this.array = obj.array ? obj.array : [];
      this.titleTabla = obj.titleTabla ? obj.titleTabla : "";
      this.codigoSniffs = obj.codigoSniffs ? obj.codigoSniffs : "";
      this.codigo = obj.codigo ? obj.codigo : "";
      this.codigoSniffsDescripcion = obj.codigoSniffsDescripcion
        ? obj.codigoSniffsDescripcion
        : "";
      this.observacion = obj.observacion ? obj.observacion : "";
      this.idParametro = obj.idParametro ? obj.idParametro : 0;
      this.idEvaluacionDet = obj.idEvaluacionDet ? obj.idEvaluacionDet : 0;
    }
  }
}

interface plan {
  idTipoProceso: any;
  idTipoPlan: any;
  codigoEstado: string;
  descripcion: string;
}
