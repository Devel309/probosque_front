import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { SistemaManejoForestalService, UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import {DEMATipo, ToastService} from '@shared';
import { SistemaManejoForestal } from "@models";
import { finalize, tap } from "rxjs/operators";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
//import { ModalFormularioCronogramaComponent } from "./modal/modal-formulario-zonas/modal-formulario-cronograma.component";
//import { ModalTipoActividadesComponent } from "./modal/modal-tipo-actividades/modal-tipo-actividades.component";

import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {CodigosDEMA} from '../../../../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../../../../model/util/Mensajes';

@Component({
  selector: "app-tab-cronograma-actividades",
  templateUrl: "./tab-cronograma-actividades.component.html",
  styleUrls: ["./tab-cronograma-actividades.component.scss"],
})
export class TabCronogramaActividadesComponent implements OnInit {
  @Input() idPlanManejo!: number;@Input() isPerfilArffs!: boolean;
  /*@Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;*/
  medidaProteccion: SistemaManejoForestal = new SistemaManejoForestal({
    codigoProceso: DEMATipo.MPUMF,
  });

  ref!: DynamicDialogRef;
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  context: Modelo = new Modelo();
  lstMaderable: Modelo[] = [];
  lstNoMaderable: Modelo[] = [];
  lstOtros: Modelo[] = [];
  cmbTipoActividad: any[] = [
    { code: 0, name: "Seleccionar" },
    { code: "AAEAM", name: "Aprovechamiento Maderable" },
    { code: "AAEANM", name: "Aprovechamiento No Maderable" },
    { code: "AAEOTRO", name: "Otros" },
  ];
  cmbAnios: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [{ label: "Año 2", value: "2-0" }],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [{ label: "Año 3", value: "3-0" }],
    },
  ];



  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_9;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_9_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private api: SistemaManejoForestalService,
    private dialog: MatDialog,
    private user: UsuarioService,private toast: ToastService,
    private messageService: MessageService,private evaluacionService: EvaluacionService
  ) {}

  ngOnInit(): void {
   /* if(this.isPerfilArffs)
      this.obtenerEvaluacion();*/

    this.listarCronogramaActividad();
  }

  obtenerAAE(idPlanManejo: number, codigoProceso: DEMATipo) {
    return this.api.obtener(idPlanManejo, codigoProceso).pipe(
      tap((res) => {
        if (res.data.idSistemaManejoForestal != null) {
          res.data.detalle?.forEach((x: any) => {
            if (
              (x.idSistemaManejoForestalDetalle, x.codigoTipoDetalle == "AAEAM")
            ) {
              this.lstMaderable.push({ ...x, actividad: x.actividades });
              this.guardarTipoActividadPrincipal(x);
            } else if (
              (x.idSistemaManejoForestalDetalle,
              x.codigoTipoDetalle == "AAEANM")
            ) {
              this.lstNoMaderable.push({ ...x, actividad: x.actividades });
              this.guardarTipoActividadPrincipal(x);
            } else if (
              (x.idSistemaManejoForestalDetalle,
              x.codigoTipoDetalle == "AAEOTRO")
            ) {
              this.lstOtros.push({ ...x, actividad: x.actividades });
              this.guardarTipoActividadPrincipal(x);
            }
          });
        }
      })
    );
  }
  // listar todas las actividades
  listarCronogramaActividad() {
    this.lstMaderable = [];
    this.lstNoMaderable = [];
    this.lstOtros = [];
    var params = {
      codigoProceso: "DEMA",
      idCronogramaActividad: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((x: any) => {
            if (
              x.idCronogramaActividad != null &&
              x.codigoActividad == "AAEAM"
            ) {
              this.lstMaderable.push({
                ...x,
                tipoActividad: "Aprovechamiento Maderable",
              });
            } else if (
              x.idCronogramaActividad != null &&
              x.codigoActividad == "AAEANM"
            ) {
              this.lstNoMaderable.push({
                ...x,
                tipoActividad: "Aprovechamiento No Maderable",
              });
            } else if (
              x.idCronogramaActividad != null &&
              x.codigoActividad == "AAEOTRO"
            ) {
              this.lstOtros.push({ ...x, tipoActividad: "Otros" });
            }
          });
          this.marcar();
        } else {
          this.obtenerAAE(this.idPlanManejo, DEMATipo.AAE)
            .pipe(
              finalize(() => {
                if (this.lstMaderable.length != 0) {
                  setTimeout(() => {
                    this.listarCronogramaActividad();
                  }, 2000);
                }
                this.dialog.closeAll();
              })
            )

            .subscribe();
        }
      });
  }

  marcar() {
    var aniosMeses: any[] = [];
    this.lstMaderable.forEach((x: any, index) => {
      aniosMeses = [];
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });
      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstMaderable[index] = this.context;
    });

    this.lstNoMaderable.forEach((x: any, index) => {
      aniosMeses = [];
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });
      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstNoMaderable[index] = this.context;
    });

    this.lstOtros.forEach((x: any, index) => {
      aniosMeses = [];
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });
      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstOtros[index] = this.context;
    });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: resp.tipoActividad,
      codigoProceso: "DEMA",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó el cronograma de actividades.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente",
          });
        }
      });
  }

  guardarTipoActividadPrincipal(resp: any) {
    let params = {
      actividad: resp.actividades,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: resp.codigoTipoDetalle,
      codigoProceso: "DEMA",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };
    

    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó el cronograma de actividades.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente",
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    /*this.ref = this.dialogService.open(ModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbTipoActividad: this.cmbTipoActividad,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });*/
  };

  modalTipoAct = (mensaje: any) => {
   /* this.ref = this.dialogService.open(ModalTipoActividadesComponent, {
      header: mensaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.guardarTipoActividad(resp);
      }
    });*/
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      if (this.context.tipoActividad == "AAEAM") {
        this.lstMaderable.push(this.context);
      } else if (this.context.tipoActividad == "AAEANM") {
        this.lstNoMaderable.push(this.context);
      }
    }
    this.verModalMantenimiento = false;
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail:
                    "Se eliminó  la actividad del cronograma correctamente.",
                });
                this.listarCronogramaActividad();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  summary: "ERROR",
                  detail: "Ocurrió un problema, intente nuevamente",
                });
              }
            });
        } else {
          if (data.tipoActividad == "AAEAM") {
            this.lstMaderable.splice(
              this.lstMaderable.findIndex((x) => x.id == data.id),
              1
            );
          } else if (data.tipoActividad == "AAEANM") {
            this.lstNoMaderable.splice(
              this.lstNoMaderable.findIndex((x) => x.id == data.id),
              1
            );
          }
        }
      },
      reject: () => {},
    });
  }

  /*siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }*/


  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

}

export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.tipoActividad = data.tipoActividad);
      return;
    }
  }
  tipoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m1: boolean = false;
  a1m2: boolean = false;
  a1m3: boolean = false;
  a1m4: boolean = false;
  a1m5: boolean = false;
  a1m6: boolean = false;
  a1m7: boolean = false;
  a1m8: boolean = false;
  a1m9: boolean = false;
  a1m10: boolean = false;
  a1m11: boolean = false;
  a1m12: boolean = false;

  a2m1: boolean = false;

  a3m1: boolean = false;

  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-1") {
        this.a1m1 = x.aniosMeses.indexOf("1-1") > -1;
      } else if (x.aniosMeses == "1-2") {
        this.a1m2 = x.aniosMeses.indexOf("1-2") > -1;
      } else if (x.aniosMeses == "1-3") {
        this.a1m3 = x.aniosMeses.indexOf("1-3") > -1;
      } else if (x.aniosMeses == "1-4") {
        this.a1m4 = x.aniosMeses.indexOf("1-4") > -1;
      } else if (x.aniosMeses == "1-5") {
        this.a1m5 = x.aniosMeses.indexOf("1-5") > -1;
      } else if (x.aniosMeses == "1-6") {
        this.a1m6 = x.aniosMeses.indexOf("1-6") > -1;
      } else if (x.aniosMeses == "1-7") {
        this.a1m7 = x.aniosMeses.indexOf("1-7") > -1;
      } else if (x.aniosMeses == "1-8") {
        this.a1m8 = x.aniosMeses.indexOf("1-8") > -1;
      } else if (x.aniosMeses == "1-9") {
        this.a1m9 = x.aniosMeses.indexOf("1-9") > -1;
      } else if (x.aniosMeses == "1-10") {
        this.a1m10 = x.aniosMeses.indexOf("1-10") > -1;
      } else if (x.aniosMeses == "1-11") {
        this.a1m11 = x.aniosMeses.indexOf("1-11") > -1;
      } else if (x.aniosMeses == "1-12") {
        this.a1m12 = x.aniosMeses.indexOf("1-12") > -1;
      } else if (x.aniosMeses == "2-0") {
        this.a2m1 = x.aniosMeses.indexOf("2-0") > -1;
      } else if (x.aniosMeses == "3-0") {
        this.a3m1 = x.aniosMeses.indexOf("3-0") > -1;
      }
    });
  }


}
