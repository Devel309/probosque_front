import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ApiForestalService } from 'src/app/service/api-forestal.service';

@Component({
  selector: 'historico-tabular',
  templateUrl: './historico-tabular.component.html',
  styleUrls: ['./historico-tabular.component.scss'],
})
export class HistoricoTabularComponent implements OnInit {
  @Input() idPlanManejo!: number;
  showTable:boolean = false;

  historico: any = [];
  
  constructor(private apiForestalService: ApiForestalService, private messageService: MessageService) {}

  ngOnInit(): void {
    this.consultarDivisionAdmParametrica();
  }

  consultarDivisionAdmParametrica() {
    const body = {
      codProceso: '110102',
      filtros: {
        tipoTh: 1,
        numeroTh: '08-CUS-ATA/PER-FDM-2022-0445',
        bloque: 1,
        parcelaCorta: '1-2',
        unidadTrabajo: '',
        id: '',
      },
    };

    this.apiForestalService
      .consultarDivisionAdmParametrica(body)
      .subscribe((res: any) => {
        if (res.data && res.data.length ) {
          
        }else{
          this.errorMensaje(`No se encuentro data tabular para el plan: ${this.idPlanManejo}`)
        }
      });
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}
