import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanzarPostulacionComponent } from './lanzar-postulacion.component';

describe('LanzarPostulacionComponent', () => {
  let component: LanzarPostulacionComponent;
  let fixture: ComponentFixture<LanzarPostulacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanzarPostulacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanzarPostulacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
