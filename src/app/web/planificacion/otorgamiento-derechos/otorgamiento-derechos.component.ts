import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ConfirmationService,MessageService, SelectItem } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { SolicitudOpinionRegistro } from 'src/app/model/solicitud-opinion';
import { EntidadService } from 'src/app/service/entidad.service';
import { GenericoService } from 'src/app/service/generico.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { SolicitudOpinionService } from 'src/app/service/solicitud-opinion.service';
import { EstatusProcesoService } from 'src/app/service/estatusProceso.service';
import { EstadoProcesoModel } from 'src/app/model/EstadoProcesoModel';
import { ModelDemo } from '../evaluacion-aprobacion-pm/evaluacion-aprobacion-pm.component';
import { ModalRegistroSolicitudesComponent } from './modal-registro-solicitudes/modal-registro-solicitudes.component';
import { DialogService } from 'primeng/dynamicdialog';
import { ProcesoOfertaService } from 'src/app/service/proceso-oferta/proceso-oferta.service';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';

@Component({
  selector: 'app-otorgamiento-derechos',
  templateUrl: './otorgamiento-derechos.component.html',
  styleUrls: ['./otorgamiento-derechos.component.scss']
})
export class OtorgamientoDerechosComponent implements OnInit {
  permisos: IPermisoOpcion = {} as IPermisoOpcion;

  procesoOfertaSelect!: String;
  estadoProcesoSelect!: SelectItem;
  usuario!: UsuarioModel;

  constructor(private serv: PlanificacionService, private router: Router,
    public dialog: MatDialog,
    public dialogService: DialogService,
    private estatusProcesoServ: EstatusProcesoService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private procesoOfertaService :ProcesoOfertaService,
    private usuarioServ: UsuarioService,
    private activaRoute: ActivatedRoute,
    ) {
      this.permisos = this.activaRoute.snapshot.data?.permisos || {};
      
    }

  procesosOfertaFiltros: any = { idProceso: 0, razonSocial: '', ruc: '', estado: '' };
  lstProcesosOferta: any[] = [];
  fileList : any[] = [];

  estadoProceso = {} as EstadoProcesoModel;
  listaEstadoProceso: EstadoProcesoModel[] = [];
  listarGrid: any = {pageNum :1,pageSize: 10};
  totalRecords = 0;
  ngOnInit() {
    this.listarProcesoOferta();
    this.listarEstado();
    this.usuario = this.usuarioServ.usuario;
  }

  listarEstado() {
    let params = { tipoStatus: 'PROCESO' }
    this.estatusProcesoServ.listarEstadoEstatusProceso(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        tipoStatus: '',
        idStatusProceso: 0,
        descripcion: '-- Seleccione --'
      });
      this.listaEstadoProceso = result.data;

    })
  }

  listarProcesoOferta(estado?: any) {
    if(estado==undefined){
      this.listarGrid= {pageNum :1,pageSize: 10};
    }

    var params = {
      agrupada: null,
      idProcesoOferta: this.procesoOfertaSelect ? this.procesoOfertaSelect : null, 
      idStatusProceso: this.estadoProceso.idStatusProceso ? this.estadoProceso.idStatusProceso : null,
      idTipoProceso: null,
      pageNum: this.listarGrid.pageNum,
      pageSize: this.listarGrid.pageSize 
    }
    
    this.serv.listarProcesoOferta(params).subscribe(
      (result: any) => {
        this.lstProcesosOferta = result.data;
        this.totalRecords = result.totalRecord;
      }
    );
  }

  limpiarFiltro() {
    this.estadoProceso.idStatusProceso = 0;
    this.procesoOfertaSelect = '';
    this.listarProcesoOferta();
  }

  nuevoOtorgamientoD() {
    this.router.navigate(['/planificacion/otorgamiento-derechos/nuevo']);
  }

  paginate(obj: any) {
    console.log(obj);
  }

  abrirModalSolicitudes(idProOferta: number) {
    let ref = this.dialogService.open(ModalRegistroSolicitudesComponent, {
      header: 'Registro de Solicitudes de Opinión',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: { idProcesoOferta: idProOferta },
    });
    ref.onClose.subscribe((resp: any) => {
      console.log("respModal: ", resp);
    });
  };

  loadData(event: any) {
    this.listarGrid.pageNum = event.first + 1;
    this.listarGrid.pageSize = event.rows;

    this.listarProcesoOferta(true);
  }
  

eliminarConfirm(event: any, idProcesoOferta: number) {
  this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarProcesoOferta(idProcesoOferta);
      },     
  });
}

eliminarProcesoOferta(idProcesoOferta: number ) {
  let params = { 
    "idProcesoOferta": idProcesoOferta,
    "idUsuarioElimina": this.usuario.idusuario 
  
  }

  this.dialog.open(LoadingComponent, { disableClose: true });
  this.procesoOfertaService.eliminarEstatusProcesoOferta(params).subscribe(
      (result: any) => {
          this.dialog.closeAll()
          if (result.success) {
              
             this.SuccessMensaje(result.message);
             this.listarProcesoOferta();
             
          } else {
              this.ErrorMensaje(result.message);
          }
          
      },
      (error) => {
        this.ErrorMensaje('Ocurrió un error');
        this.dialog.closeAll();
      }
      
  );
}

private ErrorMensaje(mensaje: any) {
  this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
}

private SuccessMensaje(mensaje: any) {
  this.messageService.add({
    severity: 'success',
    summary: '',
    detail: mensaje,
  });
}

/*
  private removerAccionOpcion(data:AccionOpcionModel, tipo:string):void{

    if(data.local && tipo == 'D')
    {
      const index = this.lstaDetalleOpcionAccion.indexOf(data, 0);
      this.lstaDetalleOpcionAccion.splice(index,1);
    }else{
      this.dialog.open(LoadingComponent,{ disableClose: true });
      let params = data;
      this.serSeguridad.eliminarOpcionAccion(params).subscribe(
        (result : any)=>{
          if(result.success){
            this.messageService.add({severity:"success", summary: "", detail: result.message});
            if(tipo == 'D'){
              this.obtenerOpcionAccion({value : this.opcionContext});
            }else{
              this.paginator?.changePage(0);              
              this.listarBandeja(this.objbuscar);
            }
           
    
          }else{
            console.log(result);
            this.messageService.add({severity:"warn", summary: "", detail: result.message});
          }
          this.dialog.closeAll();
        },
        (error)=>{
          this.dialog.closeAll();
          console.log(error);
          this.messageService.add({severity:"warn", summary: "", detail: error});
        }
      );

    }
  }
*/
  /*
  openEliminarRegistro(event: any, index: number) {

     this.confirmationService.confirm({
       target: event.target || undefined,
       message: '¿Está seguro de eliminar este registro?',
       icon: 'pi pi-exclamation-triangle',
       acceptLabel: 'Si',
       rejectLabel: 'No',
       accept: () => {
    this.fileList.splice(index, 1);
         alert("En construcción")
       }
     });
  }*/


}
