import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { config } from 'process';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { SolicitudOpinionRegistro } from 'src/app/model/solicitud-opinion';
import { ProcesoOfertaSolicitudOpinion } from 'src/app/model/solicitud-opinion';
import { EntidadService } from 'src/app/service/entidad.service';
import { EstatusProcesoService } from 'src/app/service/estatusProceso.service';
import { GenericoService } from 'src/app/service/generico.service';
import { SolicitudOpinionService } from 'src/app/service/solicitud-opinion.service';
import * as moment from "moment";


@Component({
    selector: 'app-modal-registro-solicitudes',
    templateUrl: './modal-registro-solicitudes.component.html',
    styleUrls: ['./modal-registro-solicitudes.component.scss']
})

export class ModalRegistroSolicitudesComponent implements OnInit {

    listaArchivosRegistrados: any[] = [];
    usuario!: UsuarioModel;
    comboEntidad: any[] = [];
    comboEntidadOriginal: any[] = [];
    comboTipoDocumento: any = [];
    opinion: any = {};
    archivoAdjunto: any = {};
    verModalSolicitud: boolean = false;
    informacion: any = {};
    idProcesoOfertaModal: number = 0;
    idDocumentoAdjunto: number=0;

    isVerModalSolicitud: boolean = false;

    constructor(
        public config: DynamicDialogConfig,
        public dialog: MatDialog,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private genericoServ: GenericoService,
        private entidadServ: EntidadService,
        private usuarioServ: UsuarioService,
        private opinionServ: SolicitudOpinionService,
        private archivoServ: ArchivoService
    ) { }

    ngOnInit() {
        this.usuario = this.usuarioServ.usuario;
        this.listarSolicitudesRegistradas();
        this.listarComboEntidad();
        this.listarComboTipoDocumentos();
    }

    listarComboEntidad() {
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.entidadServ.listaComboEntidad({}).subscribe(
            (result: any) => {
                this.dialog.closeAll();
                this.comboEntidad = result.data;
                //this.listarSolicitudesRegistradas();
            },
            (error) => this.ErrorMensaje('Ocurrió un error')
        )
    }

    listarComboTipoDocumentos() {
        let params = { prefijo: 'TDRESULTEVAL' }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.genericoServ.getParametroPorPrefijo(params).subscribe(
            (result: any) => {
                this.dialog.closeAll();
                this.comboTipoDocumento = result.data;
            },
            (error) => this.ErrorMensaje('Ocurrió un error')
        );
    }

    listarSolicitudesRegistradas() {
        let params = { "idProcesoOferta": this.config.data.idProcesoOferta, "pageNum": 1, "pageSize": 100 }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.opinionServ.listarProcesoOfertaSolicitudOpinion(params).subscribe(
            (result: any) => {
                this.listaArchivosRegistrados = result.data;
                

                //const auxLista = this.listaArchivosRegistrados.map(item => item.siglaEntidad);
                //this.comboEntidad = this.comboEntidadOriginal.filter(item => !auxLista.includes(item.text));
                this.dialog.closeAll();
            },
            (error) => this.ErrorMensaje('Ocurrió un error')
        );
    }

    eliminarProcesoSolicitud(idProOfeSoli: number) {
        let params = { "idProcesoOfertaSolicitudOpinion": idProOfeSoli, "idUsuarioElimina": this.usuario.idusuario }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.opinionServ.eliminarProcesoOfertaSolicitudOpinion(params).subscribe(
            (result: any) => {
                if (result.success) {
                    this.SuccessMensaje(result.message);
                    this.listarSolicitudesRegistradas();
                } else {
                    this.ErrorMensaje(result.message);
                }
            },
            (error) => this.ErrorMensaje('Ocurrió un error')
        );
    }

    registrarProcesoSolicitud(idProcesoOferta: number,idSolicitudOpinion: number) {
        let params = { 
            "idProcesoOferta": idProcesoOferta,
            "idSolicitudOpinion": idSolicitudOpinion,
            "idUsuarioRegistro": this.usuario.idusuario 
        }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.opinionServ.registrarProcesoOfertaSolicitudOpinion(params).subscribe(
            (result: any) => {
                if (result.success) {
                    this.SuccessMensaje(result.message);
                    this.listarSolicitudesRegistradas();
                } else {
                    this.ErrorMensaje(result.message);
                }
            },
            (error) => this.ErrorMensaje('Ocurrió un error')
        );
    }

    eliminarConfirm(event: any, idProOfeSoli: number) {
        this.confirmationService.confirm({
            target: event.target || undefined,
            message: '¿Está seguro de eliminar la solicitud?.',
            icon: 'pi pi-exclamation-triangle',
            acceptLabel: 'Sí',
            rejectLabel: 'No',
            accept: () => {
                this.eliminarProcesoSolicitud(idProOfeSoli);
            },
        });

    }

    abrirModalSolicitudVer(fila: any) {
        
        this.isVerModalSolicitud = true;

        let params = { 
            "idSolicitudOpinion": fila.idSolicitudOpinion
        }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.opinionServ.obtenerSolicitudOpinion(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(
            (result: any) => {
                if (result.success) {

                    this.opinion.siglaEntidad = result.data.siglaEntidad;
                    this.opinion.tipoDocumento = result.data.tipoDocumento;
                    this.opinion.fechaDocGestion = new Date(result.data.fechaDocGestion);
                    this.opinion.asunto = result.data.asunto;
                    this.archivoAdjunto = {idArchivo: result.data.documentoGestion,nombre: result.data.nombreDocumentoArchivo};
                    this.verModalSolicitud = true;

                } else {
                    this.ErrorMensaje(result.message);
                }
            },
            (error) => this.ErrorMensaje('Ocurrió un error')
        );





    }

    abrirModalSolicitud() {
        this.isVerModalSolicitud = false;
        this.idProcesoOfertaModal = this.config.data.idProcesoOferta;
        this.opinion = {};
        this.opinion.siglaEntidad = '';
        this.archivoAdjunto = {};
        this.verModalSolicitud = true;
    }

    cerrarModal() {
        this.verModalSolicitud = false;
    }

    enviarSolcitud() {
        var indice = this.listaArchivosRegistrados.find(x => x.siglaEntidad == this.opinion.siglaEntidad);

        let fecha1 = moment(new Date(2021,1,1))
        .subtract(31, "days")
        .format("YYYY-MM-DD");
        var fechaSeleccionada = new Date(this.opinion.fechaDocGestion)
        var fechaDocumento = moment(fechaSeleccionada).format("YYYY-MM-DD");

        if (indice !== undefined) {
            this.ErrorMensaje("La Entidad " + this.opinion.siglaEntidad + " ya está registrada");
            return;
        }

        if (fechaDocumento < fecha1) {
            this.ErrorMensaje("La Fecha de Documento debe ser posterior a 01/01/2021");
            return;
        }

        if (!this.validarSolicitudOpinion()) return;

        this.dialog.open(LoadingComponent, { disableClose: true });

        let params: SolicitudOpinionRegistro = {
            asunto: this.opinion.asunto,
            estSolicitudOpinion: "ESOPPEND",
            fechaDocGestion: this.opinion.fechaDocGestion,
            //fileDocumentoGestion: this.archivoAdjunto.file,
            idUsuarioRegistro: this.usuario.idusuario,
            nroDocGestion: this.idProcesoOfertaModal,
            numeroDocumento: this.opinion.numeroDocumento,
            siglaEntidad: this.opinion.siglaEntidad,
            tipoDocGestion: "THPA",
            tipoDocumento: this.opinion.tipoDocumento,
            documentoGestion: this.idDocumentoAdjunto
        }

        

        this.opinionServ.registrarSolicitudOpinion(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
            if (res.success) {
                //console.log(res.data);
                let params2: ProcesoOfertaSolicitudOpinion = {
                    idProcesoOferta: this.idProcesoOfertaModal,
                    idSolicitudOpinion: res.data.idSolicitudOpinion,
                    idUsuarioRegistro: this.usuario.idusuario
                }
                this.opinionServ.registrarProcesoOfertaSolicitudOpinion(params2)
                    .pipe(finalize(() => this.dialog.closeAll()))
                    .subscribe((res: any) => {
                        if (res.success) {
                            
                        this.verModalSolicitud = false;
                        this.SuccessMensaje(res.message);
                        this.listarSolicitudesRegistradas();

                        }
                    }, error => {
                        this.ErrorMensaje("Ocurrió un error");
                    });


            //this.verModalSolicitud = false;
            //this.SuccessMensaje(res.message);
            //this.listarSolicitudesRegistradas();
            //this.registrarProcesoSolicitud( this.config.data.idProcesoOferta,res.data.idSolicitudOpinion);

            }
        }, error => {
            this.ErrorMensaje("Ocurrió un error");
        });

    }

    validarSolicitudOpinion() {
        let validar: boolean = true;
        let mensaje: string = '';

        if (!this.opinion.siglaEntidad) {
            validar = false;
            mensaje = mensaje += '(*) Seleccione entidad.\n';
        }

        if (!this.opinion.tipoDocumento) {
            validar = false;
            mensaje = mensaje += '(*) Seleccione tipo documento.\n';
        }

        /*if (!this.opinion.numeroDocumento) {
            validar = false;
            mensaje = mensaje += '(*) Ingrese numero documento.\n';
        }

        if (this.opinion.tipoDocumento && this.opinion.numeroDocumento) {
            if (this.opinion.tipoDocumento == "TDOCDNI") {
                if (this.opinion.numeroDocumento.toString().length < 8 || this.opinion.numeroDocumento.toString().length > 8) {
                    validar = false;
                    mensaje = mensaje += '(*) Numero Documento Ingrese 8 caracteres.\n';
                }
            }

            if (this.opinion.tipoDocumento == "TDOCRUC") {

                if (this.opinion.numeroDocumento.toString().length < 11 || this.opinion.numeroDocumento.toString().length > 11) {
                    validar = false;
                    mensaje = mensaje += '(*) Numero Documento Ingrese 11 caracteres.\n';
                }
            }

            if (this.opinion.tipoDocumento == "TDOCEXTR") {

                if (this.opinion.numeroDocumento.toString().length < 12 || this.opinion.numeroDocumento.toString().length > 12) {
                    validar = false;
                    mensaje = mensaje += '(*) Numero Documento Ingrese 12 caracteres.\n';
                }
            }
        }*/

        if (!this.opinion.fechaDocGestion) {
            validar = false;
            mensaje = mensaje += '(*) Ingrese fecha documento.\n';
        }

        if (!this.opinion.asunto) {
            validar = false;
            mensaje = mensaje += '(*) Ingrese asunto.\n';
        }

        /*if (!this.archivoAdjunto.nombre) {
            validar = false;
            mensaje = mensaje += '(*) Adjunte Archivo.\n';
        }*/

        if (!validar) this.ErrorMensaje(mensaje);

        return validar;
    }

    registrarAdjunto(file: any) {

        this.dialog.open(LoadingComponent, { disableClose: true });
    
        this.archivoServ.cargar(this.usuario.idusuario, "THPA", file.file)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((result: any) => {
    
          if (result.success) {
            this.idDocumentoAdjunto = result.data;
            this.SuccessMensaje("Se registró el archivo correctamente.");

          }
        }, error => {

          this.ErrorMensaje("Ocurrió un error al realizar la operación");
        });
      }
    
    eliminarAdjunto(){

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ.eliminarArchivo(this.idDocumentoAdjunto, this.usuario.idusuario)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any) => {

        if (result.success) {
        
        this.idDocumentoAdjunto = 0;
        this.SuccessMensaje("Se eliminó el archivo correctamente");
        }
    }, error => {
        this.ErrorMensaje("Ocurrió un error al realizar la operación");
    });

    this.archivoAdjunto = {};
    }

    private ErrorMensaje(mensaje: any) {
        this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
    }

    private SuccessMensaje(mensaje: any) {
        this.messageService.add({ severity: 'success', summary: '', detail: mensaje });
    }


}