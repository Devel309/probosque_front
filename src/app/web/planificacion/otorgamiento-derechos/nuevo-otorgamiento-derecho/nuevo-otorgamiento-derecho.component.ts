import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
  Inject,
} from '@angular/core';
import { Router } from '@angular/router';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { MatDialog } from '@angular/material/dialog';
import GraphicsLayer from '@arcgis/core/layers/GraphicsLayer';
import Graphic from '@arcgis/core/Graphic';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { Table } from 'primeng/table';
import { ConfirmationService, MessageService } from 'primeng/api';
import { datosUsuario_parcial } from 'src/app/model/user';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { HttpErrorResponse } from '@angular/common/http';
import { MapApi } from '@shared';
import { ApiForestalService } from 'src/app/service/api-forestal.service';

@Component({
  selector: 'app-nuevo-otorgamiento-derecho',
  templateUrl: './nuevo-otorgamiento-derecho.component.html',
  styleUrls: ['./nuevo-otorgamiento-derecho.component.scss'],
  providers: [MessageService],
})
export class NuevoOtorgamientoDerechoComponent implements OnInit {
  @ViewChild('mapNuevoOtorgamiento', { static: true })
  private mapViewEl!: ElementRef;
  @ViewChild('itemsEstadoUas', { static: true })
  private itemsEstadoUas!: ElementRef;
  @ViewChild('tableUasElement2', { static: false }) private body!: Table;
  @ViewChild('tableUasElement', { static: true })
  private tableUasElement!: ElementRef;
  constructor(
    private router: Router,
    private serv: PlanificacionService,
    public dialog: MatDialog,
    private serviceGeoforestal: ApiGeoforestalService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private serviceExternos: ApiForestalService
  ) { }

  public view: any = null;
  public graphicsLayer: any = null;

  procesoOfertaActual: any = {
    idTipoProceso: 0,
    codigoGrupo: '',
    estado: 'A',
    fechaRegistro: new Date(),
    idProcesoOferta: 0,
    idUnidadAprovechamiento: 1,
    idStatusProceso: 1,
  };

  cols_uas: any[] = [];
  lista_uas: any[] = [];
  totalRecords_uas: any = Number;

  listUA_save: any[] = [];
  listUA_BD: any[] = [];
  strEstado: string ="";
  ngOnInit() {
    this.cols_uas = [
      { field: 'id', header: 'ID' },
      { field: 'lote', header: 'Ua(Lote)' },
      { field: 'estado', header: 'Estado' },
      { field: 'zona', header: 'Ubicación UA' },
      { field: 'superficie', header: 'Superficie' },
    ];
    this.initializeMap();
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '560px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
      this.view = view;
  }


  consultarCatastro() {
    // let tabla = null;
    // if ($.fn.dataTable.isDataTable(this.tableUasElement.nativeElement)) {
    //   tabla = $(this.tableUasElement.nativeElement).DataTable();
    //   tabla.destroy();
    // }
    let itemEstadosUas = this.itemsEstadoUas.nativeElement;
    let chks = itemEstadosUas.querySelectorAll('input[type="checkbox"]');
    if (Array.from(chks).filter((t: any) => t.checked === true).length > 0) {
      let chksActive = Array.from(chks).filter((t: any) => t.checked === true);
      this.listarUnidadAprovechamiento(chksActive);
    }
    else {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: '',
        detail: 'Debe seleccionar un estado de UA',
      });
    }

  }

  listarUnidadAprovechamiento(items: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.serv.listarUnidadAprovechamiento().subscribe(
      (result: any) => {
        //debugger;
        if (result.message == "Ocurrió un error.") {
          this.dialog.closeAll();
          this.messageService.add({
            key: 'toast',
            severity: 'warn',
            summary: '',
            detail: 'Ocurrió un error al consultar el servicio de Catastro. Vuelva a intentar más tarde.',
          });
        }
        else {
          this.listUA_BD = [];
          this.lista_uas = [];
          this.totalRecords_uas = 0;
          this.oncleanLayer();
          this.listUA_BD = result.data;
          this.loadGeojson(items);
          let tabla = this.tableUasElement.nativeElement;
          $(tabla).DataTable().clear().destroy();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }


  loadGeojson(items: any) {
    let idUser = JSON.parse(
      '' + localStorage.getItem('usuario')?.toString()
    ).region;
    
    this.graphicsLayer = new GraphicsLayer();

    items.forEach((t: any) => {
      let item = {
        filtros: {
          arffs: idUser,
          estado: t.id,
          lote: '',
          objectid: '',
        },
      };

      this.strEstado="";
      switch(t.id) { 
        case "4": { 
           this.strEstado= "No otorgada";
           break; 
        } 
        case "1": { 
          this.strEstado= "Revertida";
           break; 
        } 
        case "8": { 
          this.strEstado= "No Concesionada en segundo concurso público ";
           break; 
        } 
     } 

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serviceExternos
        .consultarUnidadAprovechamiento(item)
        .subscribe((result: any) => {
          if (result.dataService.data.capas[0].geoJson === null) {
            this.dialog.closeAll();



            this.messageService.add({
              key: 'toast',
              severity: 'warn',
              summary: '',
              detail: 'No hay UAS Disponibles para el estado '+this.strEstado,
            });
            setTimeout(() => {
              let tabla = this.tableUasElement.nativeElement;
                $(tabla).DataTable({
                  destroy: true,
                  searching: false,
                  language: {
                    zeroRecords: 'No se encontró ningún registro.',
                    infoEmpty: 'No hay registros disponibles'
                  },
                });
            }, 1000);
          } else {
            let items: any = {
              geoJson: {
                crs: result.dataService.data.capas[0].geoJson.crs,
                type: result.dataService.data.capas[0].geoJson.type,
                features: [],
              },
            };
            let newFeatures = [];
            let data1 = result.dataService.data.capas[0].geoJson.features;
            let data2 = this.listUA_BD;
            for (var i = 0; i < data1.length; i++) {
              var igual: any = false;
              for (var j = 0; j < data2.length && !igual; j++) {
                if (data1[i].properties.OBJECTID == data2[j].objectid) {
                  igual = true;
                }
              }
              if (!igual) {
                newFeatures.push(data1[i]);
              }
              
            }
            items.geoJson.features = newFeatures;
            if (items.geoJson.features.length != 0) {
              this.createLayer(items);
              this.createTableLayer(items);
              this.dialog.closeAll();
            } else {
              this.dialog.closeAll();
              /*this.messageService.add({
                key: 'toast',
                severity: 'warn',
                summary: '',
                detail: 'No hay Concesiones No Otorgadas disponibles',
              });*/
              setTimeout(() => {
                let tabla = this.tableUasElement.nativeElement;
                $(tabla).DataTable({
                  destroy: true,
                  searching: false,
                  language: {
                    zeroRecords: 'No se encontró ningún registro.',
                    infoEmpty: 'No hay registros disponibles'
                  },
                })
              }, 1000);
              return;
            }
          }
          if(this.totalRecords_uas > 0){
            setTimeout(() => {
              let tabla = this.tableUasElement.nativeElement;
              $(tabla).DataTable({
                destroy: true,
                searching: false,
                pageLength: 10,
                language: {
                  paginate: {
                    previous: '<',
                    next: '>',
                    first: '<<',
                    last: '>>'
                  },
                  lengthMenu: 'Mostrar _MENU_ registros por página',
                  zeroRecords: 'No se encontró ningún registro.',
                  info: 'Mostrando página _PAGE_ de _PAGES_',
                  infoEmpty: 'No hay registros disponibles'
                },
              });
              this.dialog.closeAll();
            }, 1000);
          }
        },(error)=>{
          this.dialog.closeAll();
          this.messageService.add({
            key: 'toast',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un error.',
          });
        });
    });
  }
  oncleanLayer() {
    if (this.graphicsLayer !== null) this.graphicsLayer.graphics.removeAll();
  }
  verLayerUA(e: any) {
    let id = e.target.id;
    let layer = this.graphicsLayer.graphics.items.filter(
      (t: any) => t.id === id
    );
    if (layer.length > 0) {
      layer.forEach((t: any) => {
        if (e.srcElement.checked === true) {
          t.visible = true;
        } else {
          t.visible = false;
        }
      });
    }
    this.view.goTo(this.graphicsLayer.graphics);
  }
  createLayer(item: any) {
    if (item.geoJson === null) return;
    item.geoJson.crs.type = 'name';
    var features = item.geoJson.features;
    if (features === null) return;
    //this.graphicsLayer = new GraphicsLayer();
    this.view.map.add(this.graphicsLayer);
    var polygon: any = {};
    const fillSymbol = {
      type: 'simple-fill',
      color: [227, 139, 79, 0.8],
      outline: {
        color: [255, 255, 255],
        width: 1,
      },
    };
    let feature: any = null;
    const template = {
      title: '{AUTFOR}',
      content: [
        {
          type: 'fields',
          fieldInfos: [
            {
              fieldName: 'ESTUA',
              label: 'Estado',
            },
            {
              fieldName: 'CONTRA',
              label: 'Contrato',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'FUENTE',
              label: 'Fuente',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'LOTE',
              label: 'Lote',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
          ],
        },
      ],
    };
    features.forEach((element: any, i: any) => {
      if (element.geometry === null) return;
      polygon.type = 'polygon';
      element.geometry.coordinates.forEach((t2: any) => {
        polygon.rings = t2;
        feature = new Graphic({
          geometry: polygon,
          symbol: fillSymbol,
        });
        feature.id = element.id;
        feature.attributes = element.properties;
        feature.popupTemplate = template;
        feature.visible = false;
        this.graphicsLayer.add(feature);
      });
    });
  }
  createTableLayer(data: any) {
    if (data.geoJson === null) return;
    var features = data.geoJson.features;
    var sort: any[] = features.sort(
      (a: any, b: any) =>
        parseInt(a.properties.LOTE) - parseInt(b.properties.LOTE)
    );
    if (sort[0].geometry === null) {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: '',
        detail: 'Hay errores con el servicio de catastro. Contactar con el administrador del sistema.',
      });
    } else {
      sort.forEach((t: any) => {
        this.lista_uas.push({
          lote: t.properties.LOTE,
          estado: t.properties.ESTUA,
          zona: t.properties.ZONUTM,
          superficie: t.properties.SUPSIG,
          id: t.id,
        });
      });
    }

    this.totalRecords_uas = this.lista_uas.length;
  }
  getLayers(){
    let layers = this.graphicsLayer.graphics.items.filter(
      (t: any) => t.visible === true
    );
    return layers;
  }
  getGeometryJson(id: any) {
    let layer = this.graphicsLayer.graphics.items.filter(
      (t: any) => t.id === id
    );
    let rings: any = [];
    if (layer.length > 1) {
      layer.forEach((t2: any) => {
        let ring =
          this.graphicsLayer.graphics.items.find(
            (t: any) => t.uid === t2.uid
          ) || null;
        rings.push(ring.geometry.rings);
      });
      let wkt_str3: any = [];
      rings.forEach((t: any) => {
        let geojsonGeometry2 = {
          type: 'Polygon',
          coordinates: t,
          srid: this.view.spatialReference.wkid,
        };
        var wkt_str2 =
          '(' +
          geojsonGeometry2.coordinates
            .map((ring: any) => {
              return (
                '(' +
                ring
                  .map((p: any) => {
                    return p[0] + ' ' + p[1];
                  })
                  .join(', ') +
                ')'
              );
            })
            .join(', ') +
          ')';
        wkt_str3.push(wkt_str2);
      });
      let ini = 'MULTIPOLYGON(';
      let fin = ')';
      let newValor = ini.concat(wkt_str3.toString(), fin);
      return newValor;
    } else {
      let feature = null;
      feature =
        this.graphicsLayer.graphics.items.find((t: any) => t.id === id) || null;
      if (feature === null) return null;
      let geojsonGeometry = {
        type: 'Polygon',
        coordinates: feature.geometry.rings,
        srid: this.view.spatialReference.wkid,
      };
      var wkt_str =
        'POLYGON(' +
        geojsonGeometry.coordinates
          .map((ring: any) => {
            return (
              '(' +
              ring
                .map((p: any) => {
                  return p[0] + ' ' + p[1];
                })
                .join(', ') +
              ')'
            );
          })
          .join(', ') +
        ')';
      return wkt_str;
    }
  }
  getAttributeLayerUA(item: any) {
    let feature = null;
    feature =
      this.graphicsLayer.graphics.items.find((t: any) => t.id === item.id) ||
      null;
    if (feature === null) return;
    let attributes: any = null;
    attributes = {
      autfor: feature.attributes.AUTFOR,
      concur: feature.attributes.CONCUR,
      contra: feature.attributes.CONTRA,
      docleg: feature.attributes.DOCLEG,
      docreg: feature.attributes.DOCREG,
      estua: feature.attributes.ESTUA,
      feceua: feature.attributes.FECEUA,
      fecleg: feature.attributes.FECLEG,
      fecoto: feature.attributes.FECOTO,
      fecreg: feature.attributes.FECREG,
      fuente: feature.attributes.FUENTE,
      lote: feature.attributes.LOTE,
      modoto: feature.attributes.MODOTO,
      nomdep: feature.attributes.NOMDEP,
      nomrel: feature.attributes.NOMREL,
      nomtit: feature.attributes.NOMTIT,
      objectid: feature.attributes.OBJECTID,
      observ: feature.attributes.OBSERV,
      origen: feature.attributes.ORIGEN,
      supapr: feature.attributes.SUPAPR,
      supsig: feature.attributes.SUPSIG,
      zonutm: feature.attributes.ZONUTM,
      geometry_wkt: this.getGeometryJson(item.id),
      srid: 4326,
      grupo: item.grupo,
    };
    return attributes;
  }
  guardarUnidadAprovechamiento(codigoProceOferta: any) {
    var attributes = null;
    var indicador1 = 1;
    this.listUA_save.forEach((t: any) => {
      
      attributes = this.getAttributeLayerUA(t);
      let item = {
        idUsuarioRegistro: JSON.parse(
          '' + localStorage.getItem('usuario')?.toString()
        ).idusuario,
        idUnidadAprovechamiento: 0,
        idProcesoOferta: codigoProceOferta,
        ua: attributes,
      };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serv.guardarUnidadAprovechamiento(item).subscribe((data: any) => {
        this.dialog.closeAll();

        if (data.success === true) {
          this.guardarProcesoOfertaDetalle(data.codigo, codigoProceOferta, indicador1);
        } else {
          this.messageService.add({
            key: 'toast',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema',
          });
        }
        indicador1 = indicador1 + 1;
      },
        (error) => {
          this.dialog.closeAll();
          this.messageService.add({
            key: 'toast',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema',
          });
        }
      );
    });
  }
  generarListaUA() {
    let table = this.tableUasElement.nativeElement;
    let body = table.querySelector('tbody');
    let chks = body.querySelectorAll('input[type="checkbox"]');
    this.listUA_save = [];
    if (Array.from(chks).filter((t: any) => t.checked === true).length > 0) {
      let chksActive: any = Array.from(chks).filter(
        (t: any) => t.checked === true
      );
      if (chksActive.length === 1) {
        this.listUA_save.push({ grupo: 0, id: chksActive[0].id });
      } else {
        chksActive.forEach((t: any) => {
          this.listUA_save.push({ grupo: 1, id: t.id });
        });
      }
    }
  }
  guardar() {
    this.generarListaUA();
    this.procesoOfertaActual.idTipoProceso = 3;

    if (this.procesoOfertaActual.codigoGrupo != '') {
      this.procesoOfertaActual.agrupada = true;
      this.procesoOfertaActual.asociada = true; // ???
      this.procesoOfertaActual.idUsuarioRegistro = JSON.parse(
        '' + localStorage.getItem('usuario')?.toString()
      ).idusuario;
    }

    let item = {
      idUsuarioRegistro: JSON.parse(
        '' + localStorage.getItem('usuario')?.toString()
      ).idusuario,
      idTipoProceso: 3,
      idStatusProceso: 13,
      idProcesoOferta: 0,
      agrupada: true,
      asociada: true,
      codigoGrupo: '',
    };
    if (this.listUA_save.length === 0) {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: '',
        detail: 'Debe incluir una Unidad de Aprovechamiento',
      });
    } else {
      let isTouched = this.validarColindancia();
      
      if(isTouched === false){
        this.confirmationService.confirm({
          message: 'En las Uas seleccionadas existe No Colindancia. ¿Desea Continuar?.',
          icon: 'pi pi-exclamation-triangle',
          acceptLabel: 'Sí',
          rejectLabel: 'No',
          key:'confirmColindancia',
          accept: () => {
            this.guardarProcesoOferta(item);
          },
    
        });
      }else{
        this.guardarProcesoOferta(item);
      }
    }
  }
  guardarProcesoOferta(item:any){
    this.dialog.open(LoadingComponent, { disableClose: true });
      this.serv.guardarProcesoOferta(item).subscribe((result: any) => {
        this.dialog.closeAll();

        if (result.success == true) {
          this.guardarUnidadAprovechamiento(result.codigo);
        } else {
          this.messageService.add({
            key: 'toast',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema',
          });
        }
      },
        (error) => {
          this.dialog.closeAll();
          this.messageService.add({
            key: 'toast',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema',
          });
        }
      );
  }
  validarColindancia(){
		let layers  = this.getLayers();
    let geometrias:any  = [];
    layers.forEach((layer:any) => {
      geometrias.push(layer?.geometry);
    });
    let isTouches = this.mapApi.validarGeometriasColindancia2(geometrias);
		let item = isTouches.find(
			(e: any) => e == false
		  );
		return item;
	}
  guardarProcesoOfertaDetalle(idUA: any, codigoProceOferta: any, indicador1: any) {
    let item = {
      idProcesoOferta: codigoProceOferta,
      idUnidadAprovechamiento: idUA,
      idUsuarioRegistro: 33,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.guardarProcesoOfertaDetalle(item).subscribe((result: any) => {
    
      if (result.success == true) {
        if(indicador1 === 1) {
          this.messageService.add({
            key: 'toast',
            severity: 'success',
            summary: '',
            detail: 'El registro de proceso en oferta se realizó correctamente.',
          });
        }
        setTimeout(() => {
          this.dialog.closeAll();
          this.router.navigate(['/planificacion/otorgamiento-derechos']);
        }, 3000);

      } else {
        this.dialog.closeAll();
        this.messageService.add({
          key: 'toast',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema',
        });
      }
    
    },
      (error) => {
        this.dialog.closeAll();
        this.messageService.add({
          key: 'toast',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema',
        });
      }
    );
  }
  cancelar() {
    this.router.navigate(['/planificacion/otorgamiento-derechos']);
  }

  // openDialog() {
  //   this.generarListaUA();
  //   if (this.listUA_save.length === 0) {
  //     this.messageService.add({
  //       key: 'toast',
  //       severity: 'warn',
  //       summary: '',
  //       detail: 'Debe incluir una Unidad de Aprovechamiento',
  //     });
  //   } else {
  //     const dialogRef = this.dialog.open(ConsultarEntidadComponent, {
  //       disableClose: true,
  //       height: '70%',
  //       width: '700px',
  //       data: {
  //         id_ua: 'id',
  //       },
  //     });
  //     dialogRef.afterClosed().subscribe(result => {
  //       if (result) {
  //         this.messageService.add({
  //           key: 'toast',
  //           severity: 'success',
  //           summary: 'CORRECTO',
  //           detail: 'Se envió la consulta* (Sprint 3)',
  //         });
  //       }
  //     });
  //   }
  // }
  selectDpUas() { }
  /*
  openDialoga1() {
    this.dialog.open(Anexo1, {
     disableClose: true,
     height: '80%',
     width: '90%',
   });
  }
  openDialoga2() {
    this.dialog.open(Anexo2, {
     disableClose: true,
     height: '80%',
     width: '90%',
   });
  }
  openDialoga3() {
    this.dialog.open(Anexo3, {
     disableClose: true,
     height: '80%',
     width: '90%',
   });
  }
  openDialoga4() {
    this.dialog.open(Anexo4, {
     disableClose: true,
     height: '80%',
     width: '90%',
   });
  }
*/
}
export interface ModelUA {
  lote: string;
  estado: string;
  id: string;
}

@Component({
  selector: 'app-nuevo-otorgamiento-derecho',
  templateUrl: './consultar-entidad.component.html',
  styleUrls: ['./nuevo-otorgamiento-derecho.component.scss'],
})
export class ConsultarEntidadComponent {
  constructor(
    private serv: PlanificacionService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ConsultarEntidadComponent>,
    private messageService: MessageService,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  usarioActual: datosUsuario_parcial = JSON.parse(
    '' + localStorage.getItem('usuario')?.toString()
  );
  ngOnInit() {
    this.listarEntidades();
  }

  lstEntidadConsulta: any;
  entidadConsultaSelect: any = {
    idConsultaEntidad: 0,
    descripcion: 'Elegir una entidad',
  };
  consultaActual: any = {
    asunto: '',
    descripcion: '',
    estado: '',
    fechaRegistro: new Date(),
    idConsultaEntidad: 0,
    idEnviarConsulta: 0,
    idUnidadAprovechamiento: parseInt(this.data.id_ua),
    idUsuarioRegistro: this.usarioActual.idUsuario,
    idUsuarioRemite: this.usarioActual.idUsuario,
  };

  listarEntidades() {
    this.serv.listarEntidades().subscribe((result: any) => {
      this.lstEntidadConsulta = result.data;
      this.lstEntidadConsulta.push(this.entidadConsultaSelect);
    });
  }

  enviarConsulta() {
    this.consultaActual.idConsultaEntidad = this.entidadConsultaSelect.idConsultaEntidad;

    if (this.consultaActual.idConsultaEntidad != 0) {
      //Descomentar en sprint 3
      // this.serv.enviarConsulta(this.consultaActual).subscribe(
      //   (result: any) => {
      //     console.log(result);
      //   }
      // );
      setTimeout(() => {
        this.dialogRef.close(true);
      }, 250);
    } else {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: '',
        detail: 'No se ha elegido una entidad a consultar',
      });
    }
  }
}

//anexos
/*
@Component({
  selector: 'app-anexo1',
  templateUrl: '../anexos/solicitud-cencesiones-forestales.html',
  styleUrls: ['../anexos/stylos-anexos.scss']
})
export class Anexo1{


  ngOnInit() {  }
}
@Component({
  selector: 'app-anexo2',
  templateUrl: '../anexos/acreditacion-capacidad-tecnica.html',
  styleUrls: ['../anexos/stylos-anexos.scss']
})
export class Anexo2{

}
@Component({
  selector: 'app-anexo3',
  templateUrl: '../anexos/anexo3.html',
  styleUrls: ['../anexos/stylos-anexos.scss']
})
export class Anexo3{

}
@Component({
  selector: 'app-anexo4',
  templateUrl: '../anexos/anexo4.html',
  styleUrls: ['../anexos/stylos-anexos.scss']
})
export class Anexo4{

}*/
