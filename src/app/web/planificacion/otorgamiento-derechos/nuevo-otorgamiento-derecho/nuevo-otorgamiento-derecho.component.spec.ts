import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoOtorgamientoDerechoComponent } from './nuevo-otorgamiento-derecho.component';

describe('NuevoOtorgamientoDerechoComponent', () => {
  let component: NuevoOtorgamientoDerechoComponent;
  let fixture: ComponentFixture<NuevoOtorgamientoDerechoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoOtorgamientoDerechoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoOtorgamientoDerechoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
