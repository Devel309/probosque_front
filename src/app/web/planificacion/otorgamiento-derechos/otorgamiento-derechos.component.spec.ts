import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtorgamientoDerechosComponent } from './otorgamiento-derechos.component';

describe('OtorgamientoDerechosComponent', () => {
  let component: OtorgamientoDerechosComponent;
  let fixture: ComponentFixture<OtorgamientoDerechosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtorgamientoDerechosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtorgamientoDerechosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
