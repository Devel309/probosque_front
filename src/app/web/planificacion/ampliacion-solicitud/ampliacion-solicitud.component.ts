import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from '@models';
import { UsuarioService } from '@services';
import { MessageService, SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ImpugnacionDto } from 'src/app/model/Impugnacion/ImpugnacionModel';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { ConvertDateToString, ConvertNumberToDate } from 'src/app/shared/util';
import { ModalNuevaConsultaSolicitudComponent } from './modal/modal-ampliacion-solicitud/modal-ampliacion-solicitud.component';
import { ModalSolicitudPermisoForestalComponent } from './modal/modal-solicitud-permiso-forestal/modal-solicitud-permiso-forestal.component';

@Component({
  selector: 'app-consulta-solicitud',
  templateUrl: './ampliacion-solicitud.component.html',
})
export class ConsultaSolicitudComponent implements OnInit {
  tipoProcesoSelect!: number;
  procesoPostulacionSelect!: String;
  ref!: DynamicDialogRef;
  estadosolicitudes: SelectItem[];
  selectedestadosolicitudes!: SelectItem;
  constructor(
    private procesoPostulaionService: ProcesoPostulaionService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    public dialogService: DialogService,
    private messageService: MessageService,
    private usuariServ: UsuarioService,
    // usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString())
  ) {
    // codigo db
    this.estadosolicitudes = [
      { label: '-- Seleccione --', value: null },
      { label: 'Aprobado', value: true },
      { label: 'Rechazado', value: false },
    ];
  }

  procesosOfertaFiltros: any = {
    idProceso: 0,
    razonSocial: '',
    ruc: '',
    estado: '',
  };
  lstProcesosOferta: ImpugnacionDto[] = [];

  tipoSolicitud: any[] = [];
  procesoPostulacion: any[] = [];

  usuario = {} as UsuarioModel;

  totalRecords: number = 0;

  objParamIni = {
    pageNum: 1,
    pageSize: 10,
    idUsuarioRegistro: this.usuario.idusuario,
    idTipoSolicitud: 0,
    idEtapaImpugnacionTx: "",
    idSolicitud: 0
  }

  esEvaluador: boolean = false;

  ngOnInit() {
//     if(localStorage.getItem("usuario")==null){      
//       sessionStorage.clear();
//       localStorage.clear();
//       window.location.href = './';    
// }

    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.objParamIni.idUsuarioRegistro = this.usuario.idusuario;

    this.listarProcesoPostulacion();
    this.listarTipoSolicitud();
    this.ListarImpugnacion(this.objParamIni);

    this.esEvaluador = this.usuariServ.idPerfilUsuario == 46;

  }

  loadData(event: any) {
    this.objParamIni.pageNum = event.first + 1;
    this.objParamIni.pageSize = event.rows;
    this.ListarImpugnacion(this.objParamIni);
  }

  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: null,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          idProcesoPostulacion: '-- Seleccione --',
        });
        this.procesoPostulacion = resp.data;
      });
  };

  listarTipoSolicitud = () => {
    this.ampliacionSolicitudService
      .listarTipoSolicitud()
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          idTipoSolicitud: null,
          descripcion: '-- Seleccione --',
        });
        this.tipoSolicitud = resp.data;
      });
  };

  ListarImpugnacion = (param: any) => {
    //debugger;

    var obj = {
      pageNum: param.pageNum,
      pageSize: param.pageSize,
      idUsuarioRegistro: param.idUsuarioRegistro,
      idTipoSolicitud: param.idTipoSolicitud,
      idEtapaImpugnacionTx: param.idEtapaImpugnacionTx,
      idSolicitud: param.idSolicitud
    };

    this.ampliacionSolicitudService
      .ListarImpugnacion(obj)
      .subscribe((result: any) => {
        this.lstProcesosOferta = result.data;
        this.totalRecords = result.totalrecord;
      });
  };

  nuevaAmpliacion = () => {
    this.ref = this.dialogService.open(ModalNuevaConsultaSolicitudComponent, {
      header: "Solicitar Recurso Impugnacion",
      width: '50%',
      contentStyle: { overflow: 'auto' },
    });

    this.ref.onClose.subscribe((resp: any) => {

      if (resp) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se guardó correctamente '
        });
        // this.listarAmpliacionSolicitud();
      }
    });
  };

  verDetalle = (item: any) => {
    this.ref = this.dialogService.open(ModalSolicitudPermisoForestalComponent, {
      header: 'Evaluación de Recursos de Impugnación',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        esEvaluador: this.usuariServ.idPerfilUsuario == 46
      },
    });
  };
}
