import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { fdatasync } from 'fs';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ImpugnacionEtapaModel, ImpugnacionModel } from 'src/app/model/Impugnacion/ImpugnacionModel';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { TipoSolicitud } from 'src/app/shared/const';

@Component({
  selector: 'app-modal-nueva-consulta-solicitud',
  templateUrl: './modal-ampliacion-solicitud.component.html',
})
export class ModalNuevaConsultaSolicitudComponent implements OnInit {
  solicitudRequestEntity: any = {
    idTipoSolicitud: 0,
    fechaInicioSolicitud: new Date(),
  };

  idProcesoPostulacion: number | string = '-- Seleccione --';
  //idProcesoPostulacion2: number = .idProcesoPostulacion;data

  autoResize: boolean = true;
  tipoSolicitud: any[] = [];
  procesoPostulacion: any[] = [];
  procesoPostulacionNuevo: any[] = [];
  

  idUsuarioPostulacion!: number;

  aprobada: boolean | null = null;
  rechazada: boolean = false;

  fileDeclaracionJurada!: any;
  filePropuestaExploracion!: any;
  fileFormato!: any;
  fileInforme!: any;
  fileAmpliacion!: any;
  fileImpugnacion: any = {};
  fileImpSAN!: any;

  constTipoSolicitud = TipoSolicitud;

  idSolicitud!: number;
  idDocumentoAdjunto!: number;

  cont: number = 0;
  isFundada!: boolean;

  nombredocumentoImpugnacion: string = '';
  documentoImpugnacion: string = '';

  nombredocumentoImpugnacionRespuesta: string = '';
  documentoImpugnacionRespuesta: string = '';

  nombredocumentoImpSAN: string = '';
  documentoImpSAN: string = '';
  nombredocumentoImpSANRespuesta: string = '';
  documentoImpSANRespuesta: string = '';

  notaTotal!: number;

  nombreUsuarioPostulante: string = '';
  descripcionStatusProceso: string = '';
  idProcesoOferta: string = '';
  impugnacionSAN: boolean = false;
  tipoArc: string = '';
  btnEnviarload:boolean=false;
  usuario = {} as UsuarioModel;
  perfil: any = null;
  
  impugnacionModel : ImpugnacionModel=new ImpugnacionModel();
  impugnacionEtapaModel : ImpugnacionEtapaModel=new ImpugnacionEtapaModel();

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private messageService: MessageService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private serv: PlanificacionService,
    private seguridadService: SeguridadService,
    private usuariServ: UsuarioService
  ) { }

  ngOnInit() {
    
    // if(localStorage.getItem("usuario")==null){      
    //                     sessionStorage.clear();
    //                     localStorage.clear();
    //                     window.location.href = './';    
    // }
    this.usuario  = JSON.parse(''+localStorage.getItem("usuario")?.toString() );
    this.impugnacionModel.idTipoSolicitud = 0;
    this.impugnacionModel.idSolicitud = this.config.data.item.idProcesoPostulacion;
    this.listarProcesoPostulacion();
    this.listarTipoSolicitud();
  }

  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: this.impugnacionModel.idTipoSolicitud === 5 ? 11 : 2,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        this.procesoPostulacion = [];
        resp.data.splice(0, 0, {
          idProcesoPostulacion: '-- SELECCIONE --',
        });
        this.procesoPostulacion = [...resp.data];
      });
  };


  listarComboProcesoPostulacion() {
    this.ampliacionSolicitudService.listarSolicitudesVencidas().subscribe((result: any) => {
      this.procesoPostulacion = result.data;
    })
  }


  listarTipoSolicitud = () => {
    this.ampliacionSolicitudService
      .listarTipoSolicitud()
      .subscribe((resp: any) => {
        this.tipoSolicitud = resp.data;
      });
  };



  registrarSolicitudAmplicacion() {
    
    if (!this.validarRegistrarSolicitudAmplicacion())
      return;
    else{
      this.impugnacionModel.idUsuarioRegistro= this.usuario.idusuario;
      this.impugnacionModel.idTipoEvaluacion = 1;
      this.impugnacionModel.asunto="";
      this.impugnacionModel.calificacionImpugnacion=null;
      this.impugnacionModel.fundadoInfundado=null;


      this.impugnacionEtapaModel.idUsuarioRegistro= this.usuario.idusuario;
      this.impugnacionEtapaModel.idEtapaImpugnacion=0;
      this.impugnacionEtapaModel.idImpugnacionEtapa=0;
      this.impugnacionEtapaModel.idUsuarioResponsable = this.usuario.idusuario;

      let data={
        impugnacion:this.impugnacionModel
        ,impugnacionEtapa:this.impugnacionEtapaModel
        ,"user": {
        "idPerfil":this.usuario.perfiles[0].idPerfil,
        "idusuario": this.usuario.idusuario,
        "region": this.usuario.region
     }
  };
    this.ampliacionSolicitudService.RegistrarImpugnacion(data)
        .subscribe((result: any) => {
          if(result){
            if(result.isSuccess==true){
              this.messageService.add({severity:"success", summary: "Registro", detail: result.message});

              /*
              */
              if( this.impugnacionEtapaModel.idEtapaImpugnacion==0){
                this.tipoArc='5';
              }
              if( this.impugnacionEtapaModel.idEtapaImpugnacion==1  || this.impugnacionEtapaModel.idEtapaImpugnacion==1){
                this.tipoArc='3';
              }
              
              const formData = new FormData();
              formData.append('file', this.fileImpugnacion);
              formData.append('idArchivo', '0');
              formData.append('idImpugnacion', result.codigo.toString());
              formData.append('idImpugnacionArchivo', '0');
              
              formData.append('idTipoArchivo',this.tipoArc );
              formData.append('idUsuarioRegistro', this.usuario.idusuario.toString());

              this.ampliacionSolicitudService.registrarImpugnacionArchivo(data,formData)
              .subscribe((result: any) => {      
                  if(result){
                    if(result.isSuccess==true){
                      this.messageService.add({severity:"success", summary: "Archivo", detail: result.message});
                    }else{
                      this.messageService.add({severity:"warn", summary: "Archivo", detail: result.message});
                    }
                  }
                },
                (error)=>{
                  // this.dialog.closeAll();
                  
                  this.messageService.add({severity:"warn", summary: "Archivo", detail: error});
                }) ;
              }else{
                this.messageService.add({severity:"warn", summary: "Registro", detail: result.message});
              }
          }
          this.procesoPostulacion = result.data;

         
    },
    (error)=>{
      // this.dialog.closeAll();
      
      this.messageService.add({severity:"warn", summary: "Registro", detail: error});
    }
    ) ;
    }
  }

  validarRegistrarSolicitudAmplicacion() {
    let validar: boolean = true;
    let mensaje: string = '';


    if (!this.impugnacionModel.idTipoSolicitud) {
      validar = false;
      mensaje = mensaje += '(*) Seleccione Tipo Solicitud.\n';
    }

    if (!this.impugnacionModel.idSolicitud) {
      validar = false;
      mensaje = mensaje += '(*) Seleccione Proceso postulación.\n';
    }

    if (this.impugnacionModel.idTipoSolicitud != 7) {
      if (!this.impugnacionModel.descripcion && this.impugnacionModel.idTipoSolicitud !==
        this.constTipoSolicitud.impSAN) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese Motivo Solicitud.\n';
      }
    }

    if (this.impugnacionModel.idTipoSolicitud ===
      this.constTipoSolicitud.ampliacionPlazoPublicacion && !this.fileAmpliacion) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar archivo de solicitud.\n';
    }


    if (this.impugnacionModel.idTipoSolicitud ===
      this.constTipoSolicitud.autorizacionExpliracion && !this.fileDeclaracionJurada) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar declaración jurada.\n';
    }

    if (this.impugnacionModel.idTipoSolicitud ===
      this.constTipoSolicitud.autorizacionExpliracion && !this.filePropuestaExploracion) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar propuesta de exploración.\n';
    }

    if (this.impugnacionModel.idTipoSolicitud ===
      this.constTipoSolicitud.impugnacion && !this.fileImpugnacion) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar archivo de impugnación.\n';
    }

    if (this.impugnacionModel.idTipoSolicitud ===
      this.constTipoSolicitud.impSAN) {
      if (!this.fileImpSAN) {
        validar = false;
        mensaje = mensaje += '(*) Adjuntar archivo de impugnación al SAN.\n';
      }

      if (!this.impugnacionModel.descripcion) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese descripción.\n';
      }

      if (!this.impugnacionModel.asunto) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese Asunto.\n';
      }
    }

    if (!this.impugnacionModel.descripcion && this.impugnacionModel.idTipoSolicitud == 7) {
      validar = false;
      mensaje = mensaje += '(*) Ingrese descripción.\n';
    }

    if (this.impugnacionModel.idTipoSolicitud ===
      7 && !this.fileImpugnacion) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar archivo de Recurso Impugnación.\n';
    }


    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({ severity: 'error', summary: 'ERROR', detail: mensaje });
  }



  cargarAchivoImpugnacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileImpugnacion = e.srcElement.files[0];
      }
    }
  }


}
