import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from '@models';
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ArchivoModel } from 'src/app/model/Comun/ArchivoModel';
import { ImpugnacionArchivoModel, ImpugnacionDto, ImpugnacionEtapaModel, ImpugnacionModel } from 'src/app/model/Impugnacion/ImpugnacionModel';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-modal-solicitud-permiso-forestal',
  templateUrl: './modal-solicitud-permiso-forestal.component.html',
  styleUrls: ['./modal-solicitud-permiso-forestal.component.scss']
})
export class ModalSolicitudPermisoForestalComponent implements OnInit {

  solicitudRequestEntity: any = {
    idTipoSolicitud: 0,
    fechaInicioSolicitud: new Date(),
  };

  tipoSolicitud: any[] = [];
  procesoPostulacion: any[] = [];


  calificar: any = [
    { valorSecundario: 1, valorPrimario: "Apelación" },
    { valorSecundario: 2, valorPrimario: "Reconsideración" }
  ]

  nombredocumentoImpugnacionRespuesta: string = '';
  documentoImpugnacionRespuesta: string = '';

  nombredocumentoImpSAN: string = '';
  documentoImpSAN: string = '';

  aprobada: boolean | null = null;

  fileImpugnacion: any = {};
  fileEvaluacion: any = {};

  fileAdjEvaluacion: any = {};
  fileResolucion: any = {};  
  fileResultadosEvaluacion: any = {};

  esEvaluador: boolean = false;
  impugnacionDto : ImpugnacionDto=new ImpugnacionDto();
  impugnacionModel : ImpugnacionModel=new ImpugnacionModel();
  impugnacionEtapaModel : ImpugnacionEtapaModel=new ImpugnacionEtapaModel();
  impugnacionArchivoModel : ImpugnacionArchivoModel[]=[];
  archivoModel : ArchivoModel=new ArchivoModel();

  btnDescargaload:boolean=false;
  btnEnviarload:boolean=false;
  usuario = {} as UsuarioModel;
  tipoArc: string = '';
  disablePerfilArFFs:boolean=true;
  disablePerfilEvaluador:boolean=true;
  disableAprobarPerfilEvaluador:boolean=false;
  disableAprobarPerfilArFFs:boolean=false;
   idPerfil :number|undefined=0;
  disableTipoArchivo={
    resultadoEvaluacion:false,
    resultadoResolucion:false,
    adjuntarEvaluacion:false,
    adjuntarResolucion:false,
    adjuntarResultadoEvaluacion:false,
  };

  constructor(public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private messageService: MessageService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private serv: PlanificacionService,
    private seguridadService: SeguridadService,
    private usuariServ: UsuarioService,
    private  serArchivo:ArchivoService) { 

    }

  ngOnInit(): void {
//  debugger;;
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.esEvaluador = this.config.data.esEvaluador;
    this.impugnacionDto =this.config.data.item;
    
    this.impugnacionModel.idCalificacionImpugnacion =1;
    this.listarProcesoPostulacion();
    this.listarTipoSolicitud();
    this.ObtenerImpugnacion(this.config.data.item);
    
    this.idPerfil = this.usuario.perfiles.find(x=> x.idPerfil==38 || x.idPerfil==46 )?.idPerfil;
    if(this.idPerfil==38){
      this.disablePerfilArFFs=false;
      
      if(this.impugnacionDto.idEtapaImpugnacion==1  ){
          this.disableAprobarPerfilArFFs=true;
      }
    }
    if(this.idPerfil==46){
      this.disablePerfilEvaluador=false;
      
      if(this.impugnacionModel.idCalificacionImpugnacion == 1 ){
          this.disableAprobarPerfilEvaluador=true;
      }
    }
  }

  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: this.impugnacionModel.idTipoSolicitud === 5 ? 11 : 2,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        this.procesoPostulacion = [];
        resp.data.splice(0, 0, {
          idProcesoPostulacion: '-- SELECCIONE --',
        });
        this.procesoPostulacion = [...resp.data];
      });
  };

  listarTipoSolicitud = () => {
    this.ampliacionSolicitudService
      .listarTipoSolicitud()
      .subscribe((resp: any) => {
        this.tipoSolicitud = resp.data;
      });
  };

  ObtenerImpugnacion = (Impugnacion:any) => {
    let data={  
        idImpugnacion: Impugnacion.idImpugnacion
        ,idSolicitud :Impugnacion.idSolicitud
      };
    this.ampliacionSolicitudService
      .ObtenerImpugnacion(data)
      .subscribe((resp: any) => {
        if(resp){
          if(resp.isSuccess==true){
            this.impugnacionModel = resp.data[0];
            this.impugnacionModel.idCalificacionImpugnacion =( this.impugnacionModel.calificacionImpugnacion==true?1:2);
            // if(  this.impugnacionModel.idCalificacionImpugnacion ==1 &&   this.idPerfil == 38 ){
            //   this.disablePerfil=true;
            // }
            this.ObtenerImpugnacionArchivo( this.impugnacionModel );
          }else{
            this.messageService.add({severity:"warn", summary: "Registro", detail: resp.message});
          }
        }    
      },
      (error)=>{
        // this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "Registro", detail: error});
      });
  };

  ObtenerImpugnacionArchivo = (Impugnacion:any) => {
    let data={  
        idImpugnacion: Impugnacion.idImpugnacion
       // ,idSolicitud :Impugnacion.idSolicitud
      };
    this.ampliacionSolicitudService
      .ObtenerImpugnacionArchivo(data)
      .subscribe((resp: any) => {
        if(resp){
          if(resp.isSuccess==true){
           // debugger;;
            this.impugnacionArchivoModel = resp.data;
          this.disableTipoArchivo.resultadoEvaluacion = (this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==3 )==null?false:true) ;
          // this.disableTipoArchivo.resul = (this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==5 )?.idArchivo!=null?false:true) ;
  
    // this.disableTipoArchivo.adjuntarEvaluacion = (this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==3 )==null?false:true) ;
    // this.disableTipoArchivo.adjuntarResolucion = (this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==3 )==null?false:true) ;
    // this.disableTipoArchivo.adjuntarResultadoEvaluacion = (this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==3 )==null?false:true) ;

        }else{
            this.messageService.add({severity:"warn", summary: "Archivo", detail: resp.message});
          }
        }    
      },
      (error)=>{
        // this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "Archivo", detail: error});
      });
  };

enviar(etapaImpugnacion:number){

  //if(fileEvaluacion)

  
  this.impugnacionModel.idUsuarioModificacion= this.usuario.idusuario;
  this.impugnacionModel.calificacionImpugnacion = (this.impugnacionModel.idCalificacionImpugnacion==1?true:false);

  //this.impugnacionModel.idTipoEvaluacion = 1;
  //this.impugnacionModel.asunto="";
  //this.impugnacionModel.calificacionImpugnacion=false;
  //this.impugnacionModel.fundadoInfundado=false;
  this.impugnacionEtapaModel.idUsuarioRegistro= this.usuario.idusuario;
  this.impugnacionEtapaModel.idUsuarioModificacion= this.usuario.idusuario;

  this.impugnacionEtapaModel.idImpugnacion= this.impugnacionModel.idImpugnacion;
  this.impugnacionEtapaModel.idEtapaImpugnacion=etapaImpugnacion;
  this.impugnacionEtapaModel.etapaImpugnacion=(etapaImpugnacion==2?'Aprobado':'Desaprobado');
  //this.impugnacionEtapaModel.idImpugnacionEtapa=0;
  this.impugnacionEtapaModel.idUsuarioResponsable = this.usuario.idusuario;

  let data={
    impugnacion:this.impugnacionModel
    ,impugnacionEtapa:this.impugnacionEtapaModel
    ,"user": {
    "idPerfil":this.usuario.perfiles[0].idPerfil,
    "idusuario": this.usuario.idusuario,
    "region": this.usuario.region
 }
};
this.ampliacionSolicitudService.RegistrarImpugnacion(data)
    .subscribe((result: any) => {
      if(result){
        if(result.isSuccess==true){
          this.messageService.add({severity:"success", summary: "Registro", detail: result.message});

          /*          
          if( this.impugnacionEtapaModel.idEtapaImpugnacion==0){
            this.tipoArc='5';
          }*/
          let file=null;
          let iteracion=1;
            if( (etapaImpugnacion==2  || etapaImpugnacion==3) && this.idPerfil==38){
              this.tipoArc='3';
             file =  this.fileEvaluacion;
            }
            if( (etapaImpugnacion==2  || etapaImpugnacion==3) && this.idPerfil==46){
              this.tipoArc='3';
             file =   this.fileAdjEvaluacion;
             iteracion=3
            }
          
          // debugger;;
          for(var i=1;i<=iteracion;i++){

            if(i=2){file =   this.fileResolucion;}
            if(i=3){file =   this.fileResultadosEvaluacion;}
            const formData = new FormData();
            formData.append('file', file);
            formData.append('idArchivo', '0');
            formData.append('idImpugnacion', result.codigo.toString());
            formData.append('idImpugnacionArchivo', '0');
            
            formData.append('idTipoArchivo',this.tipoArc );
            formData.append('idUsuarioRegistro', this.usuario.idusuario.toString());
  
            this.ampliacionSolicitudService.registrarImpugnacionArchivo(data,formData)
            .subscribe((result: any) => {      
                if(result){
                  if(result.isSuccess==true){
                    this.messageService.add({severity:"success", summary: "Archivo", detail: result.message});
                  }else{
                    this.messageService.add({severity:"warn", summary: "Archivo", detail: result.message});
                  }
                }
              },
              (error)=>{
                // this.dialog.closeAll();
                
                this.messageService.add({severity:"warn", summary: "Archivo", detail: error});
              }) ;

          }
       

          }else{
            this.messageService.add({severity:"warn", summary: "Registro", detail: result.message});
          }
      }
      this.procesoPostulacion = result.data;     
},
(error)=>{
  // this.dialog.closeAll();
  
  this.messageService.add({severity:"warn", summary: "Registro", detail: error});
}
) ;

}


  cargarAchivoImpugnacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileImpugnacion = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoEvaluacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileEvaluacion = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoAdjEvaluacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileAdjEvaluacion = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoResultadosEvaluacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileResultadosEvaluacion = e.srcElement.files[0];
      }
    }
  }


  cargarAchivoResolucion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileResolucion = e.srcElement.files[0];
      }
    }
  }


  descargarInfundada = () => {
    this.btnDescargaload=true;
   let  item = this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==5 );//tipo x modelo
   if(item!=undefined){
     this.serArchivo.obtenerArchivo(item.idArchivo).subscribe(
      (resp: any) => {
        if(resp){
          if(resp.success==true){
           // debugger;;
            this.archivoModel = resp.data;
            this.btnDescargaload=false;
            DownloadFile(
              this.archivoModel.file.toString(),
              this.archivoModel.nombre.toString(),
              'application/octet-stream'
            );

          }else{
            this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: resp.message});
          }
        }    
      },
      (error)=>{
        // this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: error});
      }
     );
       
      };
   }  
  descargarImpSAN = () => {
    this.btnDescargaload=true;
    let  item = this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==5 );//tipo 5 carga inicial
    if(item!=undefined){
      this.serArchivo.obtenerArchivo(item.idArchivo).subscribe(
       (resp: any) => {
         if(resp){
           if(resp.success==true){
            // debugger;;
            this.btnDescargaload=false;
             this.archivoModel = resp.data;
             DownloadFile(
               this.archivoModel.file.toString(),
               this.archivoModel.nombre.toString(),
               'application/octet-stream'
             );
 
           }else{
             this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: resp.message});
           }
         }    
       },
       (error)=>{
         // this.dialog.closeAll();
         
         this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: error});
       }
      );
        
       };
  };

  descargarEvaluacion= () => {
    this.btnDescargaload=true;
    let  item = this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==5 );//tipo 5 carga inicial
    if(item!=undefined){
      this.serArchivo.obtenerArchivo(item.idArchivo).subscribe(
       (resp: any) => {
         if(resp){
           if(resp.success==true){
            // debugger;;
            this.btnDescargaload=false;
             this.archivoModel = resp.data;
             DownloadFile(
               this.archivoModel.file.toString(),
               this.archivoModel.nombre.toString(),
               'application/octet-stream'
             );
 
           }else{
             this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: resp.message});
           }
         }    
       },
       (error)=>{
         // this.dialog.closeAll();
         
         this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: error});
       }
      );
        
       };
  };
  descargarResultadoEvaluacion= () => {
    this.btnDescargaload=true;
    let  item = this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==5 );//tipo 5 carga inicial
    if(item!=undefined){
      this.serArchivo.obtenerArchivo(item.idArchivo).subscribe(
       (resp: any) => {
         if(resp){
           if(resp.success==true){
            // debugger;;
            this.btnDescargaload=false;
             this.archivoModel = resp.data;
             DownloadFile(
               this.archivoModel.file.toString(),
               this.archivoModel.nombre.toString(),
               'application/octet-stream'
             );
 
           }else{
             this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: resp.message});
           }
         }    
       },
       (error)=>{
         // this.dialog.closeAll();
         
         this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: error});
       }
      );
        
       };
  };
  descargarResolucion= () => {
    this.btnDescargaload=true;
    let  item = this.impugnacionArchivoModel.find(x=>x.idTipoArchivo==5 );//tipo 5 carga inicial
    if(item!=undefined){
      this.serArchivo.obtenerArchivo(item.idArchivo).subscribe(
       (resp: any) => {
         if(resp){
           if(resp.success==true){
            // debugger;;
            this.btnDescargaload=false;
             this.archivoModel = resp.data;
             DownloadFile(
               this.archivoModel.file.toString(),
               this.archivoModel.nombre.toString(),
               'application/octet-stream'
             );
 
           }else{
             this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: resp.message});
           }
         }    
       },
       (error)=>{
         // this.dialog.closeAll();
         
         this.messageService.add({severity:"warn", summary: "Descarga Archivo", detail: error});
       }
      );
        
       };
  };
}
