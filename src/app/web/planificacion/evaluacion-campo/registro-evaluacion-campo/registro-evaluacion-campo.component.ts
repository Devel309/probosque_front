import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanManejoService, UsuarioService } from '@services';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EvaluacionCampo, EvaluacionCampoAutoridad, EvaluacionCampoRegistro } from 'src/app/model/evaluacion-campo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { EvaluacionCampoAutoridadService } from 'src/app/service/evaluacion-campo-autoridad.service';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';

@Component({
  selector: 'app-registro-evaluacion-campo',
  templateUrl: './registro-evaluacion-campo.component.html',
  styleUrls: ['./registro-evaluacion-campo.component.scss']
})
export class RegistroEvaluacionCampoComponent implements OnInit {
  /****ESTAS VARIABLES SE ENVIA DESDE BANDEJA FISCALIZACION PLANTACION FORESTAL*****/
  @Input() isPlanFor: boolean = false;
  @Input() idSolPlaCampo: number = 0;
  /***********************************/

  //minDate!: Date;
  minDate: any = moment(new Date()).format("YYYY-MM-DD");

  comboAutoridad: any[] = [];
  selectAutoridad: any[] = [];
  hayListaArchivosPlan: boolean = false;

  idEvaluacionCampo: any;
  evaluacion = {} as EvaluacionCampo;

  listInfracciones: any[] = [];
  listArchivos: any = [];
  listArchivosPlan: any = [];

  usuario!: UsuarioModel;

  disabled: boolean = false;

  archivoComunicacion: any = {};
  archivoSustento: any = {};

  archivoAsamblea: any = {};
  archivoInforme: any = {};

  autoridadRequest: EvaluacionCampoAutoridad[] = [];

  listArchivosImpedimientos: any[] = [];

  listarArchivosInspeccion: any[] = [];

  constructor(
    private evaluacionAutServ: EvaluacionCampoAutoridadService,
    private route: ActivatedRoute,
    private evaluacionServ: EvaluacionCampoService,
    private planServ: PlanManejoService,
    private messageService: MessageService,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private router: Router,

  ) { }

  ngOnInit(): void {
    const currentYear = new Date().getFullYear();
    this.usuario = this.usuarioServ.usuario;

    this.route.paramMap.subscribe(params => {
      this.idEvaluacionCampo = params.get("idEvaluacionCampo");

      if (this.idEvaluacionCampo) {
        this.obtenerEvaluacion();
      }
      // condicion si se envia desde modal de bandeja fiscalización plantacion forestal
      else if (this.idSolPlaCampo) {
        this.idEvaluacionCampo = this.idSolPlaCampo;
        this.disabled = this.isPlanFor;
        this.obtenerEvaluacion();
      }
      /******************************/
    });

  }


  listarComboPorFiltroAutoridad() {

    let params = {
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo
    }

    this.evaluacionAutServ.comboPorFiltroAutoridad(params).subscribe((result: any) => {
      if (result.data) {
        this.comboAutoridad = result.data;
        this.obtenerSelectItemsAutoridad();
      }
    })
  }


  registrarAutoridad() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.autoridadRequest = [];


    this.comboAutoridad.forEach((aut: any) => {
      aut.items.forEach((item: any) => {
        this.selectAutoridad.forEach(value => {
          if (item.value == value) {
            let autoridad: EvaluacionCampoAutoridad = {
              idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
              idUsuarioRegistro: this.usuario.idusuario,
              idEvaluacionCampoAutoridad: item.valor,
              idTipoAutoridad: +item.value,
              tipoAutoridad: item.label
            }
            this.autoridadRequest.push(autoridad);
          }
        })
      });
    })

    this.evaluacionAutServ.registrarEvaluacionCampoAutoridad(this.autoridadRequest).subscribe((result: any) => {
      this.dialog.closeAll();
      this.listarComboPorFiltroAutoridad();
    }, error => {
      this.SuccessMensaje("Ocurrió un Error")
      this.dialog.closeAll();
    })
  }


  obtenerEvaluacion() {
    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionServ.obtenerEvaluacionCampo(params)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any) => {
      if (result.data) {
        this.evaluacion = {
          ...result.data,
          fechaInicioEvaluacion: result.data.fechaInicioEvaluacion ? new Date(result.data.fechaInicioEvaluacion) : result.data.fechaInicioEvaluacion,
          fechaFinEvaluacion: result.data.fechaFinEvaluacion ? new Date(result.data.fechaFinEvaluacion) : result.data.fechaFinEvaluacion,
          fechaInicioImpedimentos: result.data.fechaInicioImpedimentos ? new Date(result.data.fechaInicioImpedimentos) : result.data.fechaInicioImpedimentos,
          fechaFinImpedimentos: result.data.fechaFinImpedimentos ? new Date(result.data.fechaFinImpedimentos) : result.data.fechaFinImpedimentos,
        }

        this.disabled = this.evaluacion.idEvaluacionCampoEstado == 3;

        // condicion si se envia desde modal de bandeja fiscalización plantacion forestal
        if(this.isPlanFor) this.disabled = this.isPlanFor;

        this.listarComboPorFiltroAutoridad();
        this.listarArchivos();
        this.listarArchivosPlan();
        this.listarArchivosImpedimientos();
        this.listarEvaluacionCampoArchivoInspeccion();
      }
    })

  }

  obtenerDetalleEvaluacion() {

  }

  listarArchivos() {

    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo
    }

    this.evaluacionServ.obtenerEvaluacionCampoArchivo(params).subscribe((result: any) => {
      if (result.data) {
        this.asignarArchivos(result.data);
      }
    })
  }

  listarArchivosPlan() {
    let params = {
      idPlanManejo: this.evaluacion.documentoGestion
    }

    this.planServ.listarPorFiltroPlanManejoArchivo(params).subscribe((result: any) => {
      if (result.data) {
        this.listArchivosPlan = result.data;
        if(result.data.length>0)
        this.hayListaArchivosPlan=true;
      }

    })
  }

  listarArchivosImpedimientos() {
    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo,
      idTipoSeccionArchivo: 3
    }

    this.evaluacionServ.listarPorSeccionEvaluacionCampoArchivo(params).subscribe((result: any) => {
      if (result.data) {
        this.listArchivosImpedimientos = result.data
      }
    })

  }

  listarEvaluacionCampoArchivoInspeccion() {
    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo,
    }

    this.evaluacionServ.listarEvaluacionCampoArchivoInspeccion(params).subscribe((result: any) => {
      if (result.data) {
        this.listarArchivosInspeccion = result.data
      }
    })
  }



  adjuntarInforme(file: any) {
    let params = {
      file: file.file,
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo ? file.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: 14,
      idTipoSeccionArchivo: 6,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje("Se registró el informe de verificación de campo correctamente. ");
        this.archivoAsamblea = res.body.data;
        file.idEvaluacionCampoArchivo = this.archivoAsamblea.idEvaluacionCampoArchivo;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }



  eliminarArchivoInforme(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
      idUsuarioRegistro: this.usuario.idusuario,

    }

    this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
      this.SuccessMensaje("Se eliminó el informe de verificación de campo correctamente.");
      this.archivoInforme = {};


      this.dialog.closeAll();
    })


  }



  registrarEvaluacion(idTipo: number) {
    if (!this.validarRegistroEvaluacion(idTipo)) return;

    let params: EvaluacionCampoRegistro = {
      ...this.evaluacion,
      idEvaluacionCampoEstado: idTipo,
      idUsuarioRegistro: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampo(params).subscribe((result: any) => {

      this.evaluacion.idEvaluacionCampoEstado = idTipo;
      if (this.evaluacion.idEvaluacionCampoEstado == 2) {
        this.evaluacion.evaluacionCampoEstado = "En Proceso"
        this.SuccessMensaje(" Se registró la evaluación de campo correctamente.")
      }
      if (this.evaluacion.idEvaluacionCampoEstado == 3) {
        this.evaluacion.evaluacionCampoEstado = "Inpección Concluida"
        this.SuccessMensaje(" Se registró la evaluación de campo correctamente.")
        this.disabled = this.evaluacion.idEvaluacionCampoEstado == 3
      }

      this.dialog.closeAll()
      if (this.selectAutoridad.length > 0) this.registrarAutoridad();

    })

  }

  obtenerSelectItemsAutoridad() {
    this.selectAutoridad = [];
    this.comboAutoridad.forEach((aut: any) => {
      aut.items.forEach((item: any) => {
        if (item.activo) {
          this.selectAutoridad.push(item.value)
        }
      })
    });
  }

  regresar() {
    this.router.navigate([
      '/planificacion/bandeja-evaluacion-campo'
    ]);
  }

  validarRegistroEvaluacion(tipo: number) {
    let validar: boolean = true;
    let mensaje: string = '';


    if (tipo == 3) {
      if (!this.evaluacion.fechaInicioEvaluacion) {
        validar = false;
        mensaje = mensaje +=
          '(*) Ingrese Fecha de Inicio Evaluación.\n';
      }

      if (!this.evaluacion.fechaFinEvaluacion) {
        validar = false;
        mensaje = mensaje +=
          '(*) Ingrese Fecha de final Evaluación.\n';
      }

      // if (!this.evaluacion.fechaInicioImpedimentos) {
      //   validar = false;
      //   mensaje = mensaje +=
      //     '(*) Ingrese Fecha de Inicio Inspección.\n';
      // }

      // if (!this.evaluacion.fechaFinImpedimentos) {
      //   validar = false;
      //   mensaje = mensaje +=
      //     '(*) Ingrese Fecha de final Inspección.\n';
      // }

      if (!this.evaluacion.notificacion) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe notificar la inspección.\n';
      }

      if (!this.archivoComunicacion.nombre) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe Adjuntar archivo sustento comunicación.\n';
      }

      if (!this.archivoSustento.nombre) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe Adjuntar archivo sustento.\n';
      }

      if (this.evaluacion.comunidadNatidadCampesina && !this.archivoAsamblea.nombre) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe Adjuntar archivo acta asamblea.\n';
      }


      if (!this.archivoInforme.nombre) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe Adjuntar informe firmado.\n';
      }

      if (this.selectAutoridad.length == 0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe seleccionar al menos una autoridad.\n';
      }

      // if (this.listarArchivosInspeccion.some((item: any) => item.idArchivo == 0)) {
      //   validar = false;
      //   mensaje = mensaje +=
      //     '(*) Debe cargar un documento de cada tipo sección inspección\n';
      // }

      // if (this.listArchivosImpedimientos.length == 0) {
      //   validar = false;
      //   mensaje = mensaje +=
      //     '(*) Debe cargar al menos un documento sección impedimentos\n';
      // }

    }


    if (
      this.evaluacion.fechaInicioEvaluacion &&
      this.evaluacion.fechaFinEvaluacion
    ) {
      if (
        this.evaluacion.fechaInicioEvaluacion >= this.evaluacion.fechaFinEvaluacion
      ) {
        validar = false;
        mensaje = mensaje +=
          '(*) La Fecha de Inicio Evaluación no puede ser mayor a la Fecha Final Evaluación.\n';
      }
    }

    if (
      this.evaluacion.fechaInicioImpedimentos &&
      this.evaluacion.fechaFinImpedimentos
    ) {
      if (
        this.evaluacion.fechaInicioImpedimentos >= this.evaluacion.fechaFinImpedimentos
      ) {
        validar = false;
        mensaje = mensaje +=
          '(*) La Fecha de Inicio impedimientos no puede ser mayor a la Fecha Final Impedimientos.\n';
      }
    }


    // if (
    //   this.evaluacion.fechaFinEvaluacion &&
    //   this.evaluacion.fechaInicioImpedimentos
    // ) {
    //   if (
    //     this.evaluacion.fechaInicioImpedimentos <= this.evaluacion.fechaFinEvaluacion
    //   ) {
    //     validar = false;
    //     mensaje = mensaje +=
    //       '(*) La Fecha de Inicio Impedimentos no puede ser menor a la Fecha Final Evaluación.\n';
    //   }
    // }


    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  asignarArchivos(archivos: []) {

    archivos.forEach((archivo: any) => {
      if (archivo.idTipoDocumento == 11) {
        this.archivoComunicacion = archivo;
        this.archivoComunicacion.nombre = archivo.nombre ? archivo.nombre : "Archivo Comunicación";
      }

      if (archivo.idTipoDocumento == 12) {
        this.archivoSustento = archivo;
        this.archivoSustento.nombre = archivo.nombre ? archivo.nombre : "Archivo Sustento";
      }

      if (archivo.idTipoDocumento == 13) {
        this.archivoAsamblea = archivo;
        this.archivoAsamblea.nombre = archivo.nombre ? archivo.nombre : "Archivo Acta Asamblea";
      }

      if (archivo.idTipoDocumento == 14) {
        this.archivoInforme = archivo;
        this.archivoInforme.nombre = archivo.nombre ? archivo.nombre : "Informe Firmado";
      }

    })

  }


  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

}
