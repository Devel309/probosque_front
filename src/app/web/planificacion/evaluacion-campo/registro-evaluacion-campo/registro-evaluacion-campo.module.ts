import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RegistroEvaluacionCampoComponent } from './registro-evaluacion-campo.component';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { AccordionModule } from 'primeng/accordion';
import { TablaDocumentosModule } from '../../../../shared/components/tabla-documentos/tabla-documentos.module';
import { FormularioDocumentoGestionModule } from './components/formulario-documento-gestion/formulario-documento-gestion.module';
import { FormularioProgramacionInspeccionModule } from './components/formulario-programacion-inspeccion/formulario-programacion-inspeccion.module';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { InfraccionesModule } from './components/infracciones/infracciones.module';
import { UploadInputButtonsModule } from '../../../../shared/components/upload-input-buttons/upload-input-buttons.module';
import { MatDialogModule } from '@angular/material/dialog';
import { ListboxModule } from 'primeng/listbox';
import { ImpedimentosModule } from './components/impedimentos/impedimentos.module';
import { InspeccionModule } from './components/inspeccion/inspeccion.module';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  }
};



@NgModule({
  declarations: [
    RegistroEvaluacionCampoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DropdownModule,
    FieldsetModule,
    AccordionModule,
    RadioButtonModule,
    FieldsetModule,
    UploadInputButtonsModule,
    MatDialogModule,
    ListboxModule,
    TablaDocumentosModule,
    FormularioDocumentoGestionModule,
    FormularioProgramacionInspeccionModule,
    InfraccionesModule,
    ImpedimentosModule,
    InspeccionModule,
  ],
  exports: [
    RegistroEvaluacionCampoComponent
  ],
  providers: [
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class RegistroEvaluacionCampoModule { }
