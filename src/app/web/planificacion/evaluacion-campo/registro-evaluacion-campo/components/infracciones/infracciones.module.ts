import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfraccionesComponent } from './infracciones.component';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';



@NgModule({
  declarations: [
    InfraccionesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    DialogModule,
    CheckboxModule,
    ButtonModule
  ],
  exports: [InfraccionesComponent]
})
export class InfraccionesModule { }
