import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioModel } from '@models';
import { MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';

@Component({
  selector: 'infracciones',
  templateUrl: './infracciones.component.html',
  styleUrls: ['./infracciones.component.scss']
})
export class InfraccionesComponent implements OnInit {

  @Input() idEvaluacionCampo: any;
  @Input() disabled: boolean = false;

  usuario = {} as UsuarioModel;
  display: boolean = false;
  infracciones: InfraccionModel[] = [];

  infraccionesList: any[] = [];

  constructor(
    private messageService: MessageService,
    private evaluacionServ: EvaluacionCampoService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.listarInfraccionesSeleccionadas();
  }

  listarInfracciones() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo,
    }
    this.evaluacionServ.obtenerEvaluacionCampoInfraccion(params).subscribe((result: any) => {
      this.infracciones = [];
      result.data.forEach((item: any) => {
        this.infracciones.push(new InfraccionModel(item));
      });
      this.dialog.closeAll();
      this.display = true;
    })
  }



  listarInfraccionesSeleccionadas() {
    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo,
    }
    this.evaluacionServ.obtenerEvaluacionCampoInfraccion(params).subscribe((result: any) => {
      this.infraccionesList = result.data.filter((element: any) => element.estado == 'A');
    })
  }


  guardar(): void {

    let params = this.infracciones.filter((data: InfraccionModel) => data.selected == true);

    if (params.length > 0) {
      params.forEach((iten: InfraccionModel) => {
        iten.idUsuarioRegistro = this.usuario.idusuario;
      });

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.evaluacionServ.registroEvaluacionCampoInfraccion(params).subscribe((result: any) => {
        if (result.success == true) {
          this.messageService.add({ severity: 'success', summary: '', detail: "Se registró el cuadro de infracciones y sanciones de materia forestal correctamente" });
          this.listarInfraccionesSeleccionadas();
          this.display = false;
        } else {
          this.messageService.add({ severity: 'warn', summary: '', detail: result.message });
        }
        this.dialog.closeAll();
      });
    } else {
      this.messageService.add({ severity: 'warn', summary: '', detail: "Debe seleccionar mínimo una infracción antes de guardar" });
    }

  }

  abrirModal() {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarInfracciones();
  }

  cerrarModal() {
    this.display = false;

  }

}

export class InfraccionModel {
  constructor(data?: any) {

    if (data) {
      this.calificacion = data.calificacion;
      this.estado = data.estado;
      this.idEvaluacionCampo = data.idEvaluacionCampo;
      this.idEvaluacionCampoInfraccion = data.idEvaluacionCampoInfraccion;
      this.idInfraccion = data.idInfraccion;
      this.sancionMonetaria = data.sancionMonetaria;
      this.sancionNoMonetaria = data.sancionNoMonetaria;
      this.subsanable = data.subsanable;
      this.infraccion = data.infraccion;
      this.selected = data.estado == "A";
      return;
    }

  }

  calificacion: string = "";
  estado: string = "I";
  idEvaluacionCampo: number = 0;
  idEvaluacionCampoInfraccion: number = 0;
  idInfraccion: number = 0;
  sancionMonetaria: string = "";
  sancionNoMonetaria: string = "";
  subsanable: string = "";
  infraccion: string = "";
  idUsuarioRegistro: number = 0;
  selected: boolean = this.estado == "A";

}
