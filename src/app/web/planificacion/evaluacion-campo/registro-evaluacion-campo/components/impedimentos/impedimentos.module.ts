import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImpedimentosComponent } from './impedimentos.component';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ButtonModule } from 'primeng/button';
import { MatDialogModule } from '@angular/material/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { MatDatepickerModule } from '@angular/material/datepicker';



@NgModule({
  declarations: [
    ImpedimentosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DialogModule,
    ConfirmPopupModule,
    ButtonModule,
    MatDialogModule,
    DropdownModule,
    FieldsetModule,
    MatDatepickerModule,
  ],
  exports: [ImpedimentosComponent]
})
export class ImpedimentosModule { }
