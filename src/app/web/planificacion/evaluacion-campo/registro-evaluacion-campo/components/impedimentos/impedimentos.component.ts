import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { DowloadFileLocal, DownloadFile } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EvaluacionCampo } from 'src/app/model/evaluacion-campo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'impedimentos',
  templateUrl: './impedimentos.component.html',
  styleUrls: ['./impedimentos.component.scss']
})
export class ImpedimentosComponent implements OnInit {

  @Input() evaluacion = {} as EvaluacionCampo;

  @Input() usuario!: UsuarioModel;

  @Input() disabled: boolean = false;

  @Input() minDate!: Date;

  @Input() fileList: any[] = [];
  
  @Output() listadoImpedimentos = new EventEmitter<any>();

  file: any = {};
  display: boolean = false;


  listaTipoDocumento: any[] = [];


  constructor(private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService,
    private genericoService: GenericoService,
    private dialog: MatDialog,
    private evaluacionServ: EvaluacionCampoService,
  ) { }

  ngOnInit(): void {
    this.listarTipoDocumento();
  }

  private listarTipoDocumento() {
    let parametro: any = {};
    parametro.idTipoParametro = 79;

    this.genericoService.listarPorFiltroParametro(parametro).subscribe((result: any) => {
      this.listaTipoDocumento = result.data;
    })
  }

  abrirModal() {
    this.file = {};
    this.display = true;
  }

  cerrarModal() {
    this.display = false;

  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.file.url = URL.createObjectURL(event.target.files[0]);
    this.file.file = event.target.files[0];
    this.file.nombre = event.target.files[0].name;

  }

  agregarArchivo() {

    if (!this.validarArchivo()) return;

    let params = {
      file: this.file.file,
      idEvaluacionCampoArchivo: this.file.idEvaluacionCampoArchivo ? this.file.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: this.file.idTipoDocumento,
      idTipoSeccionArchivo: 3,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.display = false;

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje(res.body.message);
        this.fileList.push(res.body.data);
        this.listadoImpedimentos.emit();
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();

  }



  openEliminarRegistro(event: any, index: number, file: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });
        let params = {
          idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
          idUsuarioRegistro: this.usuario.idusuario,

        }
        this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
          this.SuccessMensaje(result.message);
          this.fileList.splice(index, 1);
          this.dialog.closeAll();
        })
      }
    });
  }


  verArchivo(d: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (d.idArchivo) {
      let params = {
        idArchivo: d.idArchivo
      }

      this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);

        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      })

    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }


  validarArchivo() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.file.file) {
      validar = false;
      mensaje = mensaje += '(*) Debe adjuntar el archivo.\n';
    }

    if (!this.file.idTipoDocumento) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar el tipo de documento.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }


  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

}
