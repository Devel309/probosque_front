import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InspeccionComponent } from './inspeccion.component';
import { CheckboxModule } from 'primeng/checkbox';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ButtonModule } from 'primeng/button';
import { MatDialogModule } from '@angular/material/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';



@NgModule({
  declarations: [
    InspeccionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DialogModule,
    ConfirmPopupModule,
    ButtonModule,
    MatDialogModule,
    DropdownModule,
    FieldsetModule,
    UploadInputButtonsModule,
    CheckboxModule,
  ],
  exports:[InspeccionComponent]
})
export class InspeccionModule { }
