import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { DowloadFileLocal, DownloadFile } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EvaluacionCampo } from 'src/app/model/evaluacion-campo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'inspeccion',
  templateUrl: './inspeccion.component.html',
  styleUrls: ['./inspeccion.component.scss']
})
export class InspeccionComponent implements OnInit {


  @Input() isPlanFor: boolean = false;

  @Input() evaluacion = {} as EvaluacionCampo;

  @Input() usuario!: UsuarioModel;

  @Input() archivoAsamblea: any = {};

  @Input() disabled: boolean = false;

  @Input() fileList: any[] = [];

  file: any = {};
  display: boolean = false;


  listaTipoDocumento: any[] = [];


  constructor(private messageService: MessageService,
    private evaluacionServ: EvaluacionCampoService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private confirmationService: ConfirmationService,
    private genericoService: GenericoService) { }

  ngOnInit(): void {
    this.listarTipoDocumento();

  }

  private listarTipoDocumento() {
    let parametro: any = {};
    parametro.idTipoParametro = 83;

    this.genericoService.listarPorFiltroParametro(parametro).subscribe((result: any) => {
      this.listaTipoDocumento = [result.data[5]];
    })
  }

  abrirModal() {
    this.file = {};
    this.display = true;
  }

  cerrarModal() {
    this.display = false;

  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.file.url = URL.createObjectURL(event.target.files[0]);
    this.file.file = event.target.files[0];
    this.file.nombre = event.target.files[0].name;

  }

  updateFile(event: any, data: any, index: number) {
    let params = {
      file: event.target.files[0],
      idEvaluacionCampoArchivo: data.idEvaluacionCampoArchivo ? data.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: data.idTipoDocumento,
      idTipoSeccionArchivo: 41,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje(res.body.message);
        this.fileList[index] = res.body.data;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();

  }


  adjuntarArchivoAsamblea(file: any) {
    let params = {
      file: file.file,
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo ? file.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: 13,
      idTipoSeccionArchivo: 4,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje("Se registró el acta asamblea comunal correctamente.");
        this.archivoAsamblea = res.body.data;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }


  eliminarArchivoAsamblea(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
      idUsuarioRegistro: this.usuario.idusuario,

    }

    this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
      this.SuccessMensaje("Se eliminó el  acta asamblea comunal correctamente.");
      this.archivoAsamblea = {};

      this.dialog.closeAll();
    })
  }




  agregarArchivo() {

    if (!this.validarArchivo()) return;

    let params = {
      file: this.file.file,
      idEvaluacionCampoArchivo: this.file.idEvaluacionCampoArchivo ? this.file.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: this.file.idTipoDocumento,
      idTipoSeccionArchivo: 41,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.display = false;

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje(res.body.message);
        this.fileList.push(res.body.data);
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();

  }


  openEliminarRegistro(event: any, index: number, file: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });
        let params = {
          idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
          idUsuarioRegistro: this.usuario.idusuario,

        }
        this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
          this.SuccessMensaje(result.message);
          this.fileList.splice(index, 1);
          this.dialog.closeAll();
        })
      }
    });
  }


  verArchivo(d: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (d.idArchivo) {
      let params = {
        idArchivo: d.idArchivo
      }

      this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);

        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      })

    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }


  validarArchivo() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.file.file) {
      validar = false;
      mensaje = mensaje += '(*) Debe adjuntar el archivo.\n';
    }

    if (!this.file.idTipoDocumento) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar el tipo de documento.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }


  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }


}
