import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormularioProgramacionInspeccionComponent } from './formulario-programacion-inspeccion.component';
import {RadioButtonModule} from 'primeng/radiobutton';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule } from '@angular/forms';
import { UploadInputButtonsModule } from '../../../../../../shared/components/upload-input-buttons/upload-input-buttons.module';

@NgModule({
  declarations: [
    FormularioProgramacionInspeccionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RadioButtonModule,
    MatDatepickerModule,
    UploadInputButtonsModule,
  ],
  exports: [FormularioProgramacionInspeccionComponent]
})
export class FormularioProgramacionInspeccionModule { }
