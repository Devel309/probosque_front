import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EvaluacionCampo, EvaluacionCampoAutoridad } from 'src/app/model/evaluacion-campo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { EvaluacionCampoAutoridadService } from 'src/app/service/evaluacion-campo-autoridad.service';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';

@Component({
  selector: 'formulario-programacion-inspeccion',
  templateUrl: './formulario-programacion-inspeccion.component.html',
  styleUrls: ['./formulario-programacion-inspeccion.component.scss']
})
export class FormularioProgramacionInspeccionComponent implements OnInit {


  @Input() evaluacion = {} as EvaluacionCampo;

  @Input() usuario!: UsuarioModel;

  @Input() disabled: boolean = false;

  @Input() archivoSustento: any = {};
  @Input() archivoComunicacion: any = {};

  @Input() minDate!: Date;


  constructor(
    private evaluacionServ: EvaluacionCampoService,
    private dialog: MatDialog,
    private messageService: MessageService,
    private toast: ToastService
  ) { }

  ngOnInit(): void {

  }

  notificar() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idUsuarioRegistro: this.usuario.idusuario,
      notificacion: true
    }

    this.evaluacionServ.notificacionEvaluacionCampo(params).subscribe((result: any) => {
      this.SuccessMensaje('Se envió la notificación correctamente.');
      this.evaluacion.notificacion = true;
      this.dialog.closeAll();
    }, error => {
      this.toast.warn("Ocurrió un Error")
      this.dialog.closeAll();

    })

  }

  adjuntarSustentoComunicacion(file: any) {
    let params = {
      file: file.file,
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo ? file.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: 11,
      idTipoSeccionArchivo: 2,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje("Se registró el archivo sustento de comunicación correctamente.");
        this.archivoComunicacion = res.body.data;
        file.idEvaluacionCampoArchivo =this.archivoComunicacion.idEvaluacionCampoArchivo;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe()

  }


  adjuntarSustento(file: any) {
    let params = {
      file: file.file,
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo ? file.idEvaluacionCampoArchivo : 0,
      idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      idTipoDocumento: 12,
      idTipoSeccionArchivo: 2,
      idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje("Se registró el archivo sustento de autoridad correctamente.");
        this.archivoSustento = res.body.data;
        file.idEvaluacionCampoArchivo =this.archivoComunicacion.idEvaluacionCampoArchivo;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe()

  }

  eliminarArchivoComunicacion(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
      idUsuarioRegistro: this.usuario.idusuario,

    }

    this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
      this.SuccessMensaje("Se eliminó el archivo sustento de comunicación correctamente.");
      this.archivoComunicacion = {};
      this.dialog.closeAll();
    })
  }


  eliminarArchivoSustento(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
      idUsuarioRegistro: this.usuario.idusuario,

    }

    this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
      this.SuccessMensaje("Se eliminó el archivo sustento de autoridad correctamente.");
      this.archivoSustento = {};
      this.dialog.closeAll();
    })
  }



  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

}
