import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioDocumentoGestionComponent } from './formulario-documento-gestion.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    FormularioDocumentoGestionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports:[FormularioDocumentoGestionComponent]
})
export class FormularioDocumentoGestionModule { }
