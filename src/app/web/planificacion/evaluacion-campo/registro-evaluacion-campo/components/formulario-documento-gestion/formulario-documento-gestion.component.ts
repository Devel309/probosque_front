import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TipoDocumentoPlan } from '@shared';
import { EvaluacionCampo } from 'src/app/model/evaluacion-campo';
import { TipoPlanManejo } from 'src/app/model/util/codigos/TipoPlanManejo';
import { IrPlanService } from 'src/app/shared/services/ir-plan.service';

@Component({
  selector: 'formulario-documento-gestion',
  templateUrl: './formulario-documento-gestion.component.html',
  styleUrls: ['./formulario-documento-gestion.component.scss']
})
export class FormularioDocumentoGestionComponent implements OnInit {

  @Input() evaluacion = {} as EvaluacionCampo;

  tipoDocumentoPlan = TipoDocumentoPlan;


  constructor(private router: Router, private irPlanService: IrPlanService,) { }

  ngOnInit(): void {
  }

  verPlan() {
    switch (this.evaluacion.tipoDocumentoGestion) {
      case TipoPlanManejo.PGMF_CONCESION_MADERABLE: {
        this.router.navigate([`/planificacion/plan-general-manejo/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.PMFIC: {
        this.router.navigate([`/planificacion/plan-manejo-forestal-intermedio/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.DEMA: {
        this.router.navigate([`/planificacion/generar-declaracion-manejo-dema/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.PMFI: {
        this.router.navigate([`/planificacion/formulacion-PMFI/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.PGMFA: {
        this.router.navigate([`/planificacion/plan-general-manejo/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.POAC: {
        this.router.navigate([`/planificacion/plan-operativo-ccnn-ealta/${this.evaluacion.documentoGestion}`])
        break;
      }


      case TipoPlanManejo.PGMF_PERMISO_APROVE: {
        //statements;
        break;
      }

      case TipoPlanManejo.DEMA_PERMISO_APROVE: {
        this.router.navigate([`/planificacion/generar-declaracion-manejo-dema/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.PMFI_CONCESION_PFDM: {
        this.router.navigate([`/planificacion/formulacion-PMFI/${this.evaluacion.documentoGestion}`])
        break;
      }

      case TipoPlanManejo.PLANTACIONES_FORESTALES: {
        this.irPlanService.irFormPlantacionForestal_LS(true, this.evaluacion.documentoGestion, true, true, true, "", false, false, true, false, "", false);
        break;
      }

      default: {
        //statements;
        break;
      }
    }

  }

}
