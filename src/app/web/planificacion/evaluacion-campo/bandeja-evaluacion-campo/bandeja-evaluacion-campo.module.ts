import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaEvaluacionCampoComponent } from './bandeja-evaluacion-campo.component';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { FormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';



@NgModule({
  declarations: [
    BandejaEvaluacionCampoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DropdownModule,
    FieldsetModule,
    PaginatorModule,
  ]
})
export class BandejaEvaluacionCampoModule { }
