import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Page } from '@models';
import { ToastService } from '@shared';
import { LazyLoadEvent } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ListarEvaluacionCampoRequest } from 'src/app/model/evaluacion-campo';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { EvaluacionCampoService } from 'src/app/service/evaluacion-campo.service';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'app-bandeja-evaluacion-campo',
  templateUrl: './bandeja-evaluacion-campo.component.html',
  styleUrls: ['./bandeja-evaluacion-campo.component.scss']
})
export class BandejaEvaluacionCampoComponent implements OnInit {

  evaluaciones: any[] = [];

  listarEvaluacionCampoRequest: ListarEvaluacionCampoRequest = new ListarEvaluacionCampoRequest();

  comboDocumentos: any[] = [];
  comboEstado: any[] = [];

  loading = false;
  totalRecords = 0;

  constructor(
    private evaluacionServ: EvaluacionCampoService,
    private router: Router,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.listarEvaluacionCampo();
    this.listarComboDocumentos();
    this.listarComboEstado();
  }

  listarEvaluacionCampo() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.evaluacionServ.listarEvaluacionCampo(this.listarEvaluacionCampoRequest).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data.length > 0) {
      this.evaluaciones = [...result.data];
      this.totalRecords = result.totalrecord;
    }else{
      this.evaluaciones = [];
      this.totalRecords = 0;
      this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
    }
    }, (error) => this.dialog.closeAll());
  }

  listarComboDocumentos() {
    const params = { prefijo: "TPM" }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      if(result.success) this.comboDocumentos = result.data;
    })
  }

  listarComboEstado() {
    const params = { prefijo: "EEVALCAMP" }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      if(result.success) this.comboEstado = result.data;
    })
  }


  loadData(event: any) {
    this.listarEvaluacionCampoRequest.pageNum = event.first + 1;
    this.listarEvaluacionCampoRequest.pageSize = event.rows;

    this.listarEvaluacionCampo();
  }

  limpiar() {
    this.listarEvaluacionCampoRequest = new ListarEvaluacionCampoRequest();
    this.listarEvaluacionCampo();
  }

  evaluar(evaluacion: any) {
    this.router.navigate([
      '/planificacion/registro-evaluacion-campo',
      evaluacion.idEvaluacionCampo,
    ]);
  }

}
