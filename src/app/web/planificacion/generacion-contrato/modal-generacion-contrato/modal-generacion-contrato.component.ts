import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ContratoModel } from 'src/app/model/contratoModel';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ArchivoService, ParametroValorService, UsuarioService } from '@services';
import { ContratoService } from 'src/app/service/contrato.service';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoContrato } from 'src/app/model/util/CodigoEstadoContrato';
import { CodigosTiposPersona } from 'src/app/model/util/CodigosTiposPerona';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ToastService } from '@shared';
import { CodigoDocGestion } from 'src/app/model/util/CodigoDocGestion';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { CodigoTipoContrato } from 'src/app/model/util/CodigoTipoContrato';

@Component({
  selector: 'app-modal-generacion-contrato',
  templateUrl: './modal-generacion-contrato.component.html',
})
export class ModalGeneracionContratoComponent implements OnInit {
  lstTipoDocumento: any[] = [];
  contratoRequestEntity = {} as ContratoModel;
  idContrato!: number;

  fileLegal: any = {};
  fileGarantia: any = {};

  usuario!: UsuarioModel;
  minDate!: Date;

  validaPIDEClass: boolean = false;
  isValidPIDE: boolean = false;
  validaRNPClass: boolean = false;
  isValidRNP: boolean = false;
  auxTipoArchivo = { LEGAL: "legal", GARANTIA: "garantia", BIENES: 'bienes', CONTRATO: 'contrato' }

  isTPERJURI = false;
  isTHProcAbrev: boolean = false;
  isPfdmConDirecta: boolean = false;
  codigoTipoContrato: string = "";
  touchedAll: string = "";

  constructor(
    public ref: DynamicDialogRef,
    public ref1: DynamicDialogRef,
    public dialogService: DialogService,
    public config: DynamicDialogConfig,
    private procesoPostulaionService: ProcesoPostulaionService,
    private contratoService: ContratoService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private parametroValorService: ParametroValorService,
    private pideService: PideService,
    private toast: ToastService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.isTHProcAbrev = this.config.data.item.tipoDocumentoGestion === CodigoDocGestion.TH_PROCED_ABREV;
    this.isPfdmConDirecta = this.config.data.item.tipoDocumentoGestion === CodigoDocGestion.PFDM_CONCE_DIRECTA;

    if(this.isTHProcAbrev) {
      this.codigoTipoContrato = CodigoTipoContrato.PROGRAMADO_ABREVIADO;
    }
    if(this.isPfdmConDirecta) {
      this.codigoTipoContrato = CodigoTipoContrato.PFDM_CONC_DIRECTA;
    }

    this.contratoRequestEntity.tipoDocumentoGestion = this.config.data.item.tipoDocumentoGestion;
    this.contratoRequestEntity.tipoDocumentoGestionDescripcion = this.config.data.item.tipoDocumentoGestionDescripcion;
    this.contratoRequestEntity.documentoGestion = this.config.data.item.documentoGestion;
  }

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear, 0, 1);

    this.listarTipoDocumento();
    this.obtenerProcesoPostulacion();
  }

  listarTipoDocumento() {
    var params = { prefijo: 'TDOCI' }
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoDocumento = result.data.filter((item: any) => (item.codigo === CodigosTiposDoc.DNI || item.codigo === CodigosTiposDoc.RUC));
      }
    );
  };

  obtenerProcesoPostulacion() {
    const params = {
      "documentoGestion": this.contratoRequestEntity.documentoGestion,
      "tipoDocumentoGestion": this.contratoRequestEntity.tipoDocumentoGestion,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.obtenerPostulacion(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.isTPERJURI = result.data.codigoTipoPersonaPostulante === CodigosTiposPersona.JURIDICA ? true : false;
          this.contratoRequestEntity.correoElectronico = result.data.correoPostulante;

          if (this.isTPERJURI) {
            //Datos Empresa
            this.contratoRequestEntity.tipoDocumentoTitular = result.data.codigoTipoDocumentoPostulante;
            this.contratoRequestEntity.numeroDocumentoTitular = result.data.numeroDocumentoPostulante;
            this.contratoRequestEntity.nombreTitular = result.data.nombrePostulante;
            this.contratoRequestEntity.tipoPersonaTitular = result.data.codigoTipoPersonaPostulante;
            //Datos Representante
            this.contratoRequestEntity.tipoDocumentoRepresentante = result.data.codigoTipoDocumentoRepresentante;
            this.contratoRequestEntity.dniRucRepresentanteLegal = result.data.numeroDocumentoRepresentante;
            this.contratoRequestEntity.tipoPersonaRepresentante = result.data.codigoTipoPersonaRepresentante;
            if (result.data.tipoPersonaRepresentante === CodigosTiposPersona.JURIDICA) {
              this.contratoRequestEntity.nombreRaSoRepresentanteLegal = result.data.nombreRepresentante;
            } else {
              this.contratoRequestEntity.nombresRepresentante = result.data.nombreRepresentante;
              this.contratoRequestEntity.apePaternoRepresentante = result.data.apellidoPaternoRepresentante;
              this.contratoRequestEntity.apeMAternoRepresentante = result.data.apellidoMaternoRepresentante;
              this.contratoRequestEntity.nombreRaSoRepresentanteLegal =
                `${result.data.apellidoPaternoRepresentante} ${result.data.apellidoMaternoRepresentante} ${result.data.nombreRepresentante}`;
            }
          } else {
            //Datos Representante
            this.contratoRequestEntity.nombreTitular = result.data.nombrePostulante;
            this.contratoRequestEntity.apellidoPaternoTitular = result.data.apellidoPaternoPostulante;
            this.contratoRequestEntity.apellidoMaternoTitular = result.data.apellidoMaternoPostulante;
            this.contratoRequestEntity.tipoDocumentoTitular = result.data.codigoTipoDocumentoPostulante;
            this.contratoRequestEntity.numeroDocumentoTitular = result.data.numeroDocumentoPostulante;
            this.contratoRequestEntity.tipoPersonaTitular = result.data.codigoTipoPersonaPostulante;
          }
        } else {
          this.toast.warn("Ocurrió un problema, intente nuevamente");
        }
      }, (error) => this.errorMensaje(error, true));
  }

  eliminarArchivo(idAdjuntoOut: number, tipo: string) {
    if (!idAdjuntoOut) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idusuario).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);

        if (tipo === this.auxTipoArchivo.LEGAL) {
          this.contratoRequestEntity.archivoLegal = 0;
          this.fileLegal = {};
        } else if (tipo === this.auxTipoArchivo.GARANTIA) {
          this.contratoRequestEntity.archivoGarantia = 0;
          this.fileGarantia = {};
        }

      } else {
        this.toast.warn('Ocurrió un error al realizar la operación.');
      }
    }, (error) => this.errorMensaje(error, true));
  }

  limpiarFileRegFallo(tipo: string) {
    if (tipo === this.auxTipoArchivo.LEGAL) {
      this.fileLegal = {};
    } else if (tipo === this.auxTipoArchivo.GARANTIA) {
      this.fileGarantia = {};
    }
  }

  registrarArchivo(file: any, codigo: string, tipo: string) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ.cargar(this.usuario.idusuario, codigo, file.file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);

        if (tipo === this.auxTipoArchivo.LEGAL) {
          this.contratoRequestEntity.archivoLegal = result.data;
        } else if (tipo === this.auxTipoArchivo.GARANTIA) {
          this.contratoRequestEntity.archivoGarantia = result.data;
        } else if (tipo === this.auxTipoArchivo.BIENES) {
          this.contratoRequestEntity.archivoBienes = result.data;
        } else if (tipo === this.auxTipoArchivo.CONTRATO) {
          this.contratoRequestEntity.archivoContrato = result.data;
        }

      } else {
        this.limpiarFileRegFallo(tipo);
        this.toast.warn(result.message);
      }
    }, (error) => {
      this.limpiarFileRegFallo(tipo);
      this.errorMensaje(error, true);
    }
    );
  }

  validarEnviar(): boolean {
    this.touchedAll = "ng-touched";
    this.contratoRequestEntity.codigoPartidaRegistral = this.contratoRequestEntity.codigoPartidaRegistral?.trim() || "";
    let validado = true;
    if (this.isTPERJURI) {
      if (!this.isValidPIDE) {
        validado = false;
        this.toast.warn('(*) Debe validar el documento en RENIEC.');
      }

      if (!this.isValidRNP) {
        validado = false;
        this.toast.warn('(*) Debe validar el RUC en SUNAT.');
      }

      if (!this.contratoRequestEntity.dniRucRepresentanteLegal) {
        validado = false;
        this.toast.warn('(*) Debe ingresar el número de documento.');
      }

      if (!this.contratoRequestEntity.codigoPartidaRegistral) {
        validado = false;
        this.toast.warn('(*) Debe ingresar el código de partida registral.');
      }

      if (!this.fileLegal.nombre) {
        validado = false;
        this.toast.warn('(*) Debe adjuntar Copia literal de la inscripción registral del poder del representante legal.');
      }
      if (this.isTHProcAbrev && !this.fileGarantia.nombre) {
        validado = false;
        this.toast.warn('(*) Debe adjuntar Documento de garantía de fiel cumplimiento.');
      }

    } else {

      if (!this.isValidPIDE) {
        validado = false;
        this.toast.warn('(*) Debe validar el documento en RENIEC.');
      }

      if (!this.contratoRequestEntity.codigoPartidaRegistral) {
        validado = false;
        this.toast.warn('(*) Debe ingresar el código de partida registral.');
      }

      if (!this.fileLegal.nombre) {
        validado = false;
        this.toast.warn('(*) Debe adjuntar Copia literal de la inscripción registral del poder del representante legal.');
      }
      if (this.isTHProcAbrev && !this.fileGarantia.nombre) {
        validado = false;
        this.toast.warn('(*) Debe adjuntar Documento de garantía de fiel cumplimiento.');
      }
    }

    return validado;
  }

  registrarContrato() {
    if (!this.validarEnviar()) return;

    let auxFiles = [];
    if( this.codigoTipoContrato === CodigoTipoContrato.PFDM_CONC_DIRECTA){
      auxFiles =  [{ "idArchivo": this.contratoRequestEntity.archivoLegal }]
    } else {
      auxFiles = [
        { "idArchivo": this.contratoRequestEntity.archivoLegal },
        { "idArchivo": this.contratoRequestEntity.archivoGarantia }
      ]
    }

    const params = {
      "tipoDocumentoGestion": this.contratoRequestEntity.tipoDocumentoGestion,
      "documentoGestion": this.contratoRequestEntity.documentoGestion,
      "codigoPartidaRegistral": this.contratoRequestEntity.codigoPartidaRegistral,
      "codigoEstadoContrato": CodigoEstadoContrato.BORRADOR,
      "codigoTipoContrato": this.codigoTipoContrato,
      "idUsuarioRegistro": this.usuario.idusuario,
      "listaContratoArchivo": auxFiles
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.registrarContrato(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.idContrato = result.data.idContrato;
        this.actualizarEstadoProcesoPostulacion();
      } else {
        this.toast.warn('Ocurrió un problema, intente nuevamente');
      }
    }, (error) => this.errorMensaje(error, true));
  };

  actualizarEstadoProcesoPostulacion = () => {
    var params = {
      "correoPostulante": this.contratoRequestEntity.correoElectronico,
      "idProcesoPostulacion": this.contratoRequestEntity.documentoGestion,
      "idStatus": 24,
      "idUsuarioModificacion": this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.actualizarEstadoProcesoPostulacion(params).subscribe(
      (resp: any) => {
        this.dialog.closeAll();
        if (resp.success) {
          this.toast.ok('Se guardó correctamente');
          this.ref.close(this.idContrato);
        } else {
          this.toast.warn('Ocurrió un problema, intente nuevamente');
        }
      }, (error) => this.errorMensaje(error, true));
  };

  cerrarModal(){
    this.ref.close();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  // METODOS PARA EL PIDE
  validarPide() {
    if (!this.contratoRequestEntity.tipoDocumentoRepresentante) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;

    } else if (!this.contratoRequestEntity.dniRucRepresentanteLegal) {
      this.toast.warn('(*) Debe ingresar un número de documento.');
      return;
    }

    let params = {
      numDNIConsulta: this.contratoRequestEntity.dniRucRepresentanteLegal
    };

    if (this.contratoRequestEntity.tipoDocumentoRepresentante === CodigosTiposDoc.DNI) {
      if (this.contratoRequestEntity.dniRucRepresentanteLegal.toString().length !== 8) {
        this.toast.warn('(*) El número de documento de DNI debe tener 8 digitos.');
        return;
      }
      this.consultarDNI(params);

    } else if (this.contratoRequestEntity.tipoDocumentoRepresentante === CodigosTiposDoc.RUC) {
      if (this.contratoRequestEntity.dniRucRepresentanteLegal.toString().length !== 11) {
        this.toast.warn('(*) El número de documento de RUC debe tener 11 digitos.');
        return;
      }
      this.consultarRazonSocial({ "numRUC": this.contratoRequestEntity.dniRucRepresentanteLegal }, false);

    }

  }

  // METODOS PARA EL PIDE
  validarTitularPide() {
    if (!this.contratoRequestEntity.tipoDocumentoTitular) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;

    } else if (!this.contratoRequestEntity.numeroDocumentoTitular) {
      this.toast.warn('(*) Debe ingresar un número de documento.');
      return;
    }

    let params = {
      numDNIConsulta: this.contratoRequestEntity.numeroDocumentoTitular
    };


    if (this.contratoRequestEntity.numeroDocumentoTitular.toString().length !== 8) {
      this.toast.warn('(*) El número de documento de DNI debe tener 8 digitos.');
      return;
    }
    this.consultarDNI(params);
  }

  consultarDNI(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success && result.dataService) {

          if (result.dataService.datosPersona) {
            this.cambiarValidPIDE(true);
            this.toast.ok('Se validó el DNI en RENIEC.');
          } else {
            this.cambiarValidPIDE(false);
            this.toast.warn(result.dataService.deResultado);
          }
        } else {
          this.cambiarValidPIDE(false);
          this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.cambiarValidPIDE(false);
        this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
      }
    )
  }

  consultarRazonSocial(params: any, isEmpresa: boolean) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success && result.dataService) {
          if (result.dataService.respuesta.ddp_nombre) {
            if (result.dataService.respuesta.esHabido) {
              if (isEmpresa) {
                this.cambiarValidRUC(true);
              } else {
                this.cambiarValidPIDE(true);
              }
              this.toast.ok('Se validó el RUC en SUNAT.');
            } else {
              if (isEmpresa) {
                this.cambiarValidRUC(false);
              } else {
                this.cambiarValidPIDE(false);
              }
              this.toast.warn(`RUC ingresado en estado: ${result.dataService.respuesta.desc_estado} y ${result.dataService.respuesta.desc_flag22}.`);
            }
          } else {
            if (isEmpresa) {
              this.cambiarValidRUC(false);
            } else {
              this.cambiarValidPIDE(false);
            }
            this.toast.warn(`No se ha encontrado información para el RUC ${params.numRUC}.`);
          }
        } else {
          if (isEmpresa) {
            this.cambiarValidRUC(false);
          } else {
            this.cambiarValidPIDE(false);
          }
          this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
        }
      },
      (error) => {
        this.dialog.closeAll();
        if (isEmpresa) {
          this.cambiarValidRUC(false);
        } else {
          this.cambiarValidPIDE(false);
        }
        this.toast.error('Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.');
      }
    )
  }

  cambiarValidPIDE(valor: boolean) {
    this.validaPIDEClass = valor;
    this.isValidPIDE = valor;
  }

  // VALIDAR RUC.
  validarRuc() {
    if (!this.contratoRequestEntity.tipoDocumentoTitular) {
      this.toast.warn('(*) Debe seleccionar un tipo de documento de la empresa.');
      return;

    } else if (!this.contratoRequestEntity.numeroDocumentoTitular) {
      this.toast.warn('(*) Debe ingresar un número de documento de la empresa.');
      return;
    }

    this.consultarRazonSocial({ "numRUC": this.contratoRequestEntity.numeroDocumentoTitular }, true);
  }

  cambiarValidRUC(valor: boolean) {
    this.validaRNPClass = valor;
    this.isValidRNP = valor;
  }

}
