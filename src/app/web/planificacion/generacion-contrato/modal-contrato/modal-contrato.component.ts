import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as moment from "moment";
import { ArchivoService, ParametroValorService, UsuarioService } from '@services';
import { DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ContratoModel } from 'src/app/model/contratoModel';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigoEstadoContrato } from 'src/app/model/util/CodigoEstadoContrato';
import { CodigosTiposDoc } from 'src/app/model/util/CodigosTiposDoc';
import { CodigosTiposPersona } from 'src/app/model/util/CodigosTiposPerona';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { ContratoService } from 'src/app/service/contrato.service';

@Component({
  selector: 'app-modal-contrato',
  templateUrl: './modal-contrato.component.html',
})
export class ModalContratoComponent implements OnInit {
  lstTipoDocumento: any[] = [];
  contratoRequestEntity = {} as ContratoModel;
  idContrato!: number;

  fileLegal: any = {};
  fileGarantia: any = {};
  fileBienes: any = {};
  fileContrato: any = {};
  fileGenerarContraro: any = {};
  fileContratoTitular: any = {};

  seGenero: boolean = false;

  usuario!: UsuarioModel;
  minDate: any;

  auxTipoArchivo = { LEGAL: "legal", GARANTIA: "garantia", BIENES: 'bienes', CONTRATO: 'contrato', FIRMAPOS: 'FIRMAPOS' }

  contrato: any = {};
  titular: any = {};
  representante: any = {};

  perfil = Perfiles;
  TipoArchivo = TiposArchivos;

  isPerARFFs: boolean = false;

  isEstadoBorrador: boolean = false;
  isEstadoPendFirma: boolean = false
  isEstadoFirmado: boolean = false;
  isEstadoVigente: boolean = false;
  isBtnGuardar: boolean = false;

  isInputArffs: boolean = false;
  isPerJuridica: boolean = false;

  mostrarDocPostulante: boolean = false;
  isPostulanteSesion: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public dialogService: DialogService,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private contratoService: ContratoService,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private parametroValorService: ParametroValorService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.contrato.idContrato = this.config.data.item.idContrato;

    this.contrato.documentoGestion = this.config.data.item.documentoGestion;
    this.contrato.tipoDocumentoGestionDescripcion = this.config.data.item.tipoDocumentoGestionDescripcion;
    this.contrato.tipoDocumentoGestion = this.config.data.item.tipoDocumentoGestion;
    this.idContrato = this.config.data.item.idContrato;

    this.isEstadoBorrador = this.config.data.item.codigoEstadoContrato === CodigoEstadoContrato.BORRADOR ? true : false;
    this.isEstadoPendFirma = this.config.data.item.codigoEstadoContrato === CodigoEstadoContrato.PENDIENTE_FIRMA ? true : false;
    this.isEstadoFirmado = this.config.data.item.codigoEstadoContrato === CodigoEstadoContrato.FIRMADO ? true : false;
    this.isEstadoVigente = this.config.data.item.codigoEstadoContrato === CodigoEstadoContrato.VIGENTE ? true : false;
  }

  ngOnInit() {
    //this.minDate = new Date(2021, 0, 1);
    this.minDate = moment(new Date()).format("YYYY-MM-DD");

    this.isPerARFFs = this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL ? true : false;

    if (this.isPerARFFs && (this.isEstadoBorrador || this.isEstadoFirmado)) {
      this.isBtnGuardar = true;
    }

    if (this.isPerARFFs && this.isEstadoBorrador) {
      this.isInputArffs = true;
    }

    if (!this.isEstadoBorrador) {
      this.mostrarDocPostulante = true;
    }

    if (this.usuario.idusuario === this.config.data.item.idUsuarioPostulacion && this.isEstadoPendFirma) {
      this.isPostulanteSesion = true;
      this.isBtnGuardar = true;
    }

    this.listarTipoDocumento();
    this.obtenerContrato();
  }

  listarTipoDocumento() {
    var params = { prefijo: 'TDOCI' }
    this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoDocumento = result.data.filter((item: any) => (item.codigo === CodigosTiposDoc.DNI || item.codigo === CodigosTiposDoc.RUC));
      }
    );
  };

  obtenerContrato = () => {
    let param: any = { idContrato: this.config.data.item.idContrato, tipoDocumentoGestion: this.contrato.tipoDocumentoGestion };
    this.contratoService.obtenerContratoGeneral(param).subscribe((result: any) => {
      if (result.success) this.convertData(result.data);
      else this.warnMensaje(result.message);
    }, (error) => this.errorMensaje()
    );
  };

  private convertData(data: any) {
    let con = data[0];

    if (con?.fechaInicioContrato != null) {
      this.contrato.fechaInicioContrato = new Date(con.fechaInicioContrato);
    }

    if (con?.fechaFinContrato != null) {
      this.contrato.fechaFinContrato = new Date(con.fechaFinContrato);
    }

    this.contrato.codigoPartidaRegistral = con?.codigoPartidaRegistral;
    this.contrato.idResultadoPP = con?.idResultadoPP;
    this.contrato.idStatusContrato = con?.idStatusContrato;
    this.contrato.codigoTituloH = con?.codigoTituloH;

     if(this.contrato.codigoTituloH !=null && this.contrato.codigoTituloH.length>0){
      this.seGenero=true;
     }
    this.titular = con?.listaContratoPersona.find((i: any) => i.tipoRelacion === 'PERTIRETIT');

    if (con?.listaContratoPersona.length > 1) {
      this.representante = con?.listaContratoPersona.find((i: any) => i.tipoRelacion === 'PERTIRERLE');
      this.representante.nombreCompleto = this.representante.nombres + ' ' + this.representante.apellidoPaterno + ' ' + this.representante.apellidoMaterno;
      if (this.representante.tipoPersona === CodigosTiposPersona.JURIDICA) {
        this.representante.nombreCompleto = this.representante.razonSocial;
      }
    }

    this.titular.nombreCompleto = this.titular.nombres + ' ' + this.titular.apellidoPaterno + ' ' + this.titular.apellidoMaterno;

    if (this.titular.tipoPersona === CodigosTiposPersona.JURIDICA) {
      this.isPerJuridica = true;
      this.titular.nombreCompleto = this.titular.razonSocial;
    }

    let aux1 = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.LEGAL);
    if (aux1) {
      this.fileLegal = aux1;
      this.fileLegal.nombre = this.fileLegal.nombreArchivo;
    }

    con?.listaContratoArchivo.reverse();

    let aux2 = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.GARANTIA);
    if (aux2) {
      this.fileGarantia = aux2;
      this.fileGarantia.nombre = this.fileGarantia.nombreArchivo;
    }

    let aux3 = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.BIENES);
    if (aux3) {
      this.fileBienes = aux3;
      this.fileBienes.nombre = this.fileBienes?.nombreArchivo;
      this.contratoRequestEntity.archivoBienes = aux3.idArchivo;
    }

    let aux4 = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.FIRMA_ARFFS);
    if (aux4) {
      this.fileContrato = aux4;
      this.fileContrato.nombre = this.fileContrato?.nombreArchivo;
      this.contratoRequestEntity.archivoContrato = aux4.idArchivo;
    }

    let aux5 = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.FIRMA_POSTULANTE);
    if (aux5) {
      this.fileContratoTitular = aux5;
      this.fileContratoTitular.nombre = this.fileContratoTitular.nombreArchivo;
      this.contratoRequestEntity.archivoFirmadoTitularTH = aux5.idArchivo;
    }

    // this.fileGenerarContraro = con?.listaContratoArchivo.find((i: any) => i.tipoDocumento === this.TipoArchivo.CONTRATO_PLANTILLA);
    // if (this.fileGenerarContraro) this.fileGenerarContraro.nombre = this.fileGenerarContraro?.nombreArchivo;
  }

  eliminarArchivo(idAdjuntoOut: number, tipo: string) {
    if (!idAdjuntoOut) {
      return;
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idusuario).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(Mensajes.MSJ_ELIMINO_ARCHIVO);

          if (tipo === this.auxTipoArchivo.LEGAL) {
            this.contratoRequestEntity.archivoLegal = 0;
            this.fileLegal = {};
          } else if (tipo === this.auxTipoArchivo.GARANTIA) {
            this.contratoRequestEntity.archivoGarantia = 0;
            this.fileGarantia = {};
          } else if (tipo === this.auxTipoArchivo.BIENES) {
            this.contratoRequestEntity.archivoBienes = 0;
            this.fileBienes = {};
          } else if (tipo === this.auxTipoArchivo.CONTRATO) {
            this.contratoRequestEntity.archivoContrato = 0;
            this.fileContrato = {};
          } else if (tipo === this.auxTipoArchivo.FIRMAPOS) {
            this.contratoRequestEntity.archivoFirmadoTitularTH = 0;
            this.fileContratoTitular = {};
          }

        } else {
          this.warnMensaje('Ocurrió un error al realizar la operación.');
        }
      },
      () => {
        this.dialog.closeAll();
        this.errorMensaje();
      }
    );
  }

  limpiarFileRegFallo(tipo: string) {
    if (tipo === this.auxTipoArchivo.LEGAL) {
      this.fileLegal = {};
    } else if (tipo === this.auxTipoArchivo.GARANTIA) {
      this.fileGarantia = {};
    } else if (tipo === this.auxTipoArchivo.BIENES) {
      this.fileBienes = {};
    } else if (tipo === this.auxTipoArchivo.CONTRATO) {
      this.fileContrato = {};
    } else if (tipo === this.auxTipoArchivo.FIRMAPOS) {
      this.fileContratoTitular = {};
    }
  }

  registrarArchivo(file: any, codigo: string, tipo: string) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ.cargar(this.usuario.idusuario, codigo, file.file).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(Mensajes.MSJ_REGISTRO_ARCHIVO);

          if (tipo === this.auxTipoArchivo.LEGAL) {
            this.contratoRequestEntity.archivoLegal = result.data;
          } else if (tipo === this.auxTipoArchivo.GARANTIA) {
            this.contratoRequestEntity.archivoGarantia = result.data;
          } else if (tipo === this.auxTipoArchivo.BIENES) {
            this.contratoRequestEntity.archivoBienes = result.data;
          } else if (tipo === this.auxTipoArchivo.CONTRATO) {
            this.contratoRequestEntity.archivoContrato = result.data;
          } else if (tipo === this.auxTipoArchivo.FIRMAPOS) {
            this.contratoRequestEntity.archivoFirmadoTitularTH = result.data;
          }

        } else {
          this.warnMensaje(result.message);
          this.limpiarFileRegFallo(tipo);
        }
      },
      () => {
        this.limpiarFileRegFallo(tipo);
        this.dialog.closeAll();
        this.errorMensaje();
      }
    );
  }

  descargarContrato = () => {

    let param: any = {
      idContrato: this.contrato.idContrato,
      tipoDocumentoGestion: this.config.data.item.tipoDocumentoGestion
    }

    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.descargarContratoPersonaJuridica(param).subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, () => {
      this.dialog.closeAll();
      this.errorMensaje()
    });
  };

  generarCodigo = () => {
    /*
      Para la generación de contrato, los valores de derecho y recurso deben tomarse desde el lineamiento RDE N° 0116-2018
      En esta épica 2.3.2.1: derecho = CON Concesión y  Recurso = MAD Maderable
    */
    const params = { "idUsuario": this.usuario.idusuario, "derecho": 'CON', "recurso": 'MAD' };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.generarCodigoContrato(params).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.contrato.codigoTituloH = result.data;

        let auxListaArchivos: any = [];


          auxListaArchivos = [
            {
              "idArchivo": 0,
              "idArchivoContrato": null,
              "idContrato": this.idContrato
            },
            {

              "idArchivo": 0,
              "idArchivoContrato": null,
              "idContrato": this.idContrato
            }
          ]

        const params = {
          "idContrato": this.idContrato,
          "codigoPartidaRegistral": this.contrato.codigoPartidaRegistral,
          "idResultadoPP": this.contrato.idResultadoPP,
          "codigoTituloH": this.contrato.codigoTituloH,
          "fechaInicioContrato": this.contrato.fechaInicioContrato,
          "fechaFinContrato": this.contrato.fechaFinContrato,
          "idUsuarioRegistro": this.usuario.idusuario,
          "idUsuarioModificacion": this.usuario.idusuario,
          "listaContratoArchivo": auxListaArchivos
        };

        this.contratoService.actualizarContrato(params).subscribe((result: any) => {
          if (result.success) {
            this.SuccessMensaje('Se generó el código de TH correctamente.');
            this.seGenero=true;
          }
        }, (error) => {
          this.errorMensaje("Ocurrió un problema, intente nuevamente");
        });


      } else {
        this.warnMensaje(result.message);
      }
    }, () => {
      this.dialog.closeAll();
      this.errorMensaje();
    }
    );
  };

  validarGuardar() {
    let validado = true;
    if (!this.fileBienes.nombre) {
      this.warnMensaje('(*) Debe adjuntar documento de informe favorables.');
    }

    if (!this.contrato.fechaInicioContrato) {
      validado = false;
      this.warnMensaje('(*) Debe ingresar fecha de inicio de vigencia.');
    } else if (!this.contrato.fechaFinContrato) {
      validado = false;
      this.warnMensaje('(*) Debe ingresar fecha de fin vigencia.');
    } else {
      if (this.contrato.fechaInicioContrato >= this.contrato.fechaFinContrato) {
        validado = false;
        this.warnMensaje('(*) La fecha de inicio no puede ser mayor o igual a la fecha final.');
      }
    }

    if (!this.contrato.codigoTituloH) {
      validado = false;
      this.warnMensaje('(*) Debe Generar codigo de TH.');
    }

    if (!this.fileContrato.nombre && this.isPerARFFs) {
      validado = false;
      this.warnMensaje('(*) Debe adjuntar el contrato firmado por la autoridad.');
    }

    if (!this.fileContratoTitular.nombre && this.isPostulanteSesion) {
      validado = false;
      this.warnMensaje('(*) Debe adjuntar el contrato firmado por el postulante.');
    }

    return validado;
  }

  guardarInformacionValida = () => {
    if (!this.validarGuardar()) { return; }


    let auxListaArchivos: any = [];

    if (this.isPerARFFs) {
      auxListaArchivos = [
        {
          "idArchivo": this.contratoRequestEntity.archivoBienes,
          "idArchivoContrato": null,
          "idContrato": this.idContrato
        },
        {

          "idArchivo": this.contratoRequestEntity.archivoContrato,
          "idArchivoContrato": null,
          "idContrato": this.idContrato
        }
      ]

    } else if (this.isPostulanteSesion) {
      auxListaArchivos = [
        {
          "idArchivo": this.contratoRequestEntity.archivoFirmadoTitularTH,
          "idArchivoContrato": null,
          "idContrato": this.idContrato
        }
      ]
    } else {
      return;
    }

    let estadoOut = "";
    if (this.isPostulanteSesion) {
      estadoOut = CodigoEstadoContrato.FIRMADO;
    }

    if (this.isPerARFFs && this.isEstadoBorrador) {
      estadoOut = CodigoEstadoContrato.PENDIENTE_FIRMA;
    }

    if (this.isPerARFFs && this.isEstadoFirmado) {
      estadoOut = CodigoEstadoContrato.VIGENTE;
    }

    const params = {
      "idContrato": this.idContrato,
      "codigoPartidaRegistral": this.contrato.codigoPartidaRegistral,
      "idResultadoPP": this.contrato.idResultadoPP,
      "codigoEstadoContrato": estadoOut,
      "codigoTituloH": this.contrato.codigoTituloH,
      "fechaInicioContrato": this.contrato.fechaInicioContrato,
      "fechaFinContrato": this.contrato.fechaFinContrato,
      "idUsuarioRegistro": this.usuario.idusuario,
      "idUsuarioModificacion": this.usuario.idusuario,
      "listaContratoArchivo": auxListaArchivos,
    };

    this.contratoService.actualizarContrato(params).subscribe((result: any) => {
      if (result.success) {
        this.SuccessMensaje('Se guardó correctamente');
        this.ref.close(this.idContrato);
      } else {
        this.errorMensaje("Ocurrió un problema, intente nuevamente");
      }
    }, (error) => {
      this.errorMensaje("Ocurrió un problema, intente nuevamente");
    });
  };

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'success', summary: '', detail: mensaje });
  }

  warnMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'warn', summary: '', detail: mensaje });
  }

  errorMensaje(mensaje: any = Mensajes.MSJ_ERROR_CATCH) {
    this.messageService.add({ key: 'tr', severity: 'error', summary: '', detail: mensaje });
  }

}
