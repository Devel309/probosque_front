import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { PlanificacionService } from 'src/app/service/planificacion.service';

@Component({
  selector: 'app-modal-informacion',
  templateUrl: './modal-informacion.component.html',
})
export class ModalInformacionComponent implements OnInit {
  numeroDocumento: string = '';
  idConsultaEntidad: number = 0;
  listadoEntidad: any[] = [];
  info: any = {};

  listaValidada: any[] = [];

  constructor(
    private planificacionService: PlanificacionService,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.listarEntidades();
  }

  listarEntidades = () => {
    this.planificacionService.listarEntidades().subscribe((result: any) => {
      result.data.splice(0, 0, {
        descripcion: '-- Seleccione --',
        idConsultaEntidad: 0,
      });
      this.listadoEntidad = result.data;
    });
  };

  agregar = () => {
    if (this.idConsultaEntidad === 0) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe seleccionar Entidad',
      });
    } else if (this.numeroDocumento === '') {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe ingresar numero documento',
      });
    } else {
      this.info.id = this.listaValidada.length + 1;
      this.info.descripcion = this.listadoEntidad.find(
        (x: any) => x.idConsultaEntidad === this.idConsultaEntidad
      ).descripcion;
      this.info.idConsultaEntidad = this.idConsultaEntidad;
      this.info.usuario = 'prueba';
      this.listaValidada.push(this.info);

      this.idConsultaEntidad = 0;
      this.numeroDocumento = '';
      this.info = {};
    }
  };

  validar = () => {
    if (this.listaValidada.length === 0) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe agregar validación',
      });
    } else if (
      this.listaValidada.filter(
        (x) =>
          x.idConsultaEntidad === 5 ||
          x.idConsultaEntidad === 6 ||
          x.idConsultaEntidad === 7
      ).length < 3
    ) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Las validaciones aun no ha sido completada',
      });
    } else if (
      this.listaValidada.filter(
        (x) =>
          x.idConsultaEntidad === 5 ||
          x.idConsultaEntidad === 6 ||
          x.idConsultaEntidad === 7
      ).length === 3
    ) {
      this.ref.close(true);
    } else this.ref.close(false);
  };
}
