import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ContratoService } from 'src/app/service/contrato.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';

@Component({
  selector: 'app-modal-contrato-generado',
  templateUrl: './modal-contrato-generado.component.html',
})
export class ModalContratoGeneradoComponent implements OnInit {


  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private contratoService: ContratoService,
    private serv: PlanificacionService
  ) {}

  ngOnInit() {

  }


}
