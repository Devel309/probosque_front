import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PlanificacionService, UsuarioService } from '@services';
import { MapApi, OgcGeometryType, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { FILTRO1_TIPO_DOC_GESTION } from 'src/app/model/util/CodigoDocGestion';
import { CodigoEstadoContrato } from 'src/app/model/util/CodigoEstadoContrato';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { ContratoService } from 'src/app/service/contrato.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ModalContratoComponent } from './modal-contrato/modal-contrato.component';
import { ModalInformacionEspacialComponent } from './modal-informacion-espacial/modal-informacion-espacial.component';

@Component({
  selector: 'app-generacion-contrato',
  templateUrl: './generacion-contrato.component.html',
})
export class GeneracionContratoComponent implements OnInit {
  ref!: DynamicDialogRef;
  listContrato!: any[];
  selectedIdProceso!: number | null;
  selectTipoDocGestion!: string | null;
  // selectedEstadoContrato!: string | null;
  // listEstadoContrato: any[] = [];

  usuario!: UsuarioModel;
  perfil = Perfiles;
  codContrato = CodigoEstadoContrato;
  totalRecords: number = 0;
  optionPage: any = { pageNum: 1, pageSize: 10 };
  isPerArffs: boolean = false;
  auxPerfiles = [
    this.perfil.AUTORIDAD_REGIONAL,
    this.perfil.OSINFOR,
    this.perfil.SERFOR,
  ];
  comboDocGestion: any[] = [];

  isValidaGeometria: boolean = false;
  constructor(
    public dialogService: DialogService,
    private contratoService: ContratoService,
    private router: Router,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private genericoService: GenericoService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private planificacionService: PlanificacionService,
    private mapApi: MapApi
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.isPerArffs = this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL;
  }

  ngOnInit() {
    this.listarContrato();
    // this.listarEstadoContrato();
    this.listarComboDocGestion();
  }

  listarComboDocGestion() {
    const params = { idTipoParametro: 77 };
    this.genericoService
      .listarPorFiltroParametro(params)
      .subscribe((result: any) => {
        if (result.success && result.data) {
          this.comboDocGestion = result.data.filter((item: any) =>
            FILTRO1_TIPO_DOC_GESTION.includes(item.codigo)
          );
        }
      });
  }

  // listarEstadoContrato() {
  //   let params = { prefijo: 'ECONTR' }
  //   this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
  //     if (result.success) {
  //       this.listEstadoContrato = result.data;
  //     }
  //   })
  // }

  listarContrato = () => {
    const params = {
      documentoGestion: this.selectedIdProceso || null,
      tipoDocumentoGestion: this.selectTipoDocGestion || null,
      // codigoEstadoContrato: this.selectedEstadoContrato || null,
      idUsuarioPostulacion: this.auxPerfiles.includes(this.usuario.sirperfil)
        ? null
        : this.usuario.idusuario,
      perfil: this.usuario.sirperfil,
      pageNum: this.optionPage.pageNum,
      pageSize: this.optionPage.pageSize,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.listarContrato(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success && result.data.length > 0) {
          this.listContrato = result.data;
          this.totalRecords = result.totalRecord;
        } else {
          this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
          this.listContrato = [];
          this.totalRecords = 0;
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  };

  EnviarRemitir(fila: any) {
    const params = {
      idContrato: fila.idContrato,
      contratoRemitido: true,
      idUsuarioModificacion: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.actualizarEstadoRemitido(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok('Se remitió la información a SERFOR/OSINFOR');
          this.listarContrato();
        } else {
          this.toast.warn(Mensajes.MSJ_ERROR_CATCH);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  btnBuscar() {
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.listarContrato();
  }

  btnLmipiar() {
    this.selectTipoDocGestion = null;
    this.selectedIdProceso = null;
    // this.selectedEstadoContrato = null;
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.listarContrato();
  }

  btnVerDetalle(item: any) {
    this.ref = this.dialogService.open(ModalContratoComponent, {
      header: 'Documentos para Generación de Contrato de Concesión',
      width: '650px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: { item: item, isNew: false },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) this.listarContrato();
    });
  }

  btnRemitirInfo(event: any, fila: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: 'Remitir información a SERFOR y Componente estadístico.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.EnviarRemitir(fila);
      },
    });
  }

  obtenerGeometria(idContrato: any, tipoDocumentoGestion: string) {
    let idsContratos = idContrato;
    
    this.planificacionService
      .obtenerContratoPoligono(idsContratos)
      .subscribe((result: any) => {
        let cont: number = 0;

        if (result.data.length > 1 && tipoDocumentoGestion == 'TPMTHPA') {
          this.isValidaGeometria = false;
          this.toast.warn(
            'El contrato seleccionado tiene más de una UA, por lo tanto, no se podrá remitir el multi-polígono al servicio de catastro'
          );
        }

        if (result.data.length > 1 && tipoDocumentoGestion == 'TPMPFDM') {
          this.isValidaGeometria = false;
          this.toast.warn(
            'El contrato seleccionado tiene asociado un multi-polígono, no se podrá remitir al servicio de catastro'
          );
        }
        if(result.data.length <1){
          this.toast.warn(
            'El contrato seleccionado no tiene polígono.'
          );
        }else{
          if (result.data.length == 1) {
            result.data.forEach((t: any) => {
              let geometryJson: any = this.mapApi.wktParse(t.geometry);

              switch (geometryJson.type.toUpperCase()) {
                case OgcGeometryType.POLYGON:
                  this.isValidaGeometria = true;
                  break;
                case OgcGeometryType.MULTIPOINT:
                  if (tipoDocumentoGestion == 'TPMTHPA') {
                    this.isValidaGeometria = false;
                    this.toast.warn(
                      'El contrato seleccionado tiene más de una UA, por lo tanto, no se podrá remitir el multi-polígono al servicio de catastro'
                    );
                  } else {
                    this.isValidaGeometria = false;
                    this.toast.warn(
                      'El contrato seleccionado tiene asociado un multi-polígono, no se podrá remitir al servicio de catastro'
                    );
                  }
                  break;
              }
            });
          }else{
            this.toast.warn(
              'El contrato seleccionado no tiene polígono.'
            );
          }
        }
      });
  }

  btnEviarInfoEspacial(fila: any) {
    

    this.obtenerGeometria(fila.idContrato, fila.tipoDocumentoGestion);

    if (this.isValidaGeometria) {
      this.ref = this.dialogService.open(ModalInformacionEspacialComponent, {
        header: 'Enviar Información Espacial',
        width: '900px',
        style: { margin: '15px' },
        contentStyle: { overflow: 'auto' },
        data: { item: { ...fila }, idUser: this.usuario.idusuario },
      });

      this.ref.onClose.subscribe((resp: any) => {
        if (resp) this.listarContrato();
      });
    }
  }

  validarBtnRemitir(obj: any): boolean {
    let resp = false;
    if (
      this.isPerArffs &&
      obj.codigoEstadoContrato === CodigoEstadoContrato.VIGENTE &&
      !obj.contratoRemitir
    )
      resp = true;
    return resp;
  }

  validarBtnEnviar(obj: any): boolean {
    let resp = false;
    if (
      this.isPerArffs &&
      obj.codigoEstadoContrato === CodigoEstadoContrato.VIGENTE &&
      !obj.idGeometria
    )
      resp = true;
    return resp;
  }

  validarDescripcionRemitir(obj: any): string {
    let resp = '';
    if (obj.codigoEstadoContrato === CodigoEstadoContrato.VIGENTE) {
      // resp = "Información enviada a SERFOR/OSINFOR";
      if (obj.contratoRemitir) resp = 'SI';
      else resp = 'Pendiente';
    }
    return resp;
  }

  validarDescripcionExterno(obj: any): string {
    let resp = '';
    if (obj.codigoEstadoContrato === CodigoEstadoContrato.VIGENTE) {
      if (obj.idGeometria) resp = 'Información enviada.';
      else resp = 'Pendiente';
    }
    return resp;
  }

  verModalGeneracionContrato() {
    this.router.navigate(['planificacion/generacion-contrato-resultado']);
  }

  loadData(event: any) {
    this.optionPage = { pageNum: event.first + 1, pageSize: event.rows };
    this.listarContrato();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
