import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConvertDateToString, ConvertNumberToDate, ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { FILTRO1_TIPO_DOC_GESTION } from 'src/app/model/util/CodigoDocGestion';
import { ContratoService } from 'src/app/service/contrato.service';
import { EstatusProcesoService } from 'src/app/service/estatusProceso.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ModalGeneracionContratoComponent } from '../modal-generacion-contrato/modal-generacion-contrato.component';

@Component({
  selector: 'app-detalle-resultado',
  templateUrl: './detalle-resultado.component.html',
})
export class DetalleResultadoComponent implements OnInit {
  idProcesoOferta!: number;
  listaResultadoPP: any[] = [];
  procesosOfertas: any[] = [];
  listaResultado: any[] = [];
  ref!: DynamicDialogRef;

  selectedIdProceso: number | null = null;
  selectTipoDocGestion: string | null = null;
  // selectedEstadoPostu: string | null = null;
  // listEstadoPostu: any[] = [];

  totalRecords: number = 0;
  optionPage: any = { pageNum: 1, pageSize: 10 };

  comboDocGestion: any[] = [];

  constructor(
    private messageService: MessageService,
    public dialogService: DialogService,
    private router: Router,
    private estatusProcesoServ: EstatusProcesoService,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    private contratoService: ContratoService,
    private toast: ToastService,
  ) { }

  ngOnInit() {
    this.cargarPP();
    // this.listarEstadoPostulacion();
    this.listarComboDocGestion();
  }

  listarComboDocGestion() {
    const params = { idTipoParametro: 77 }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success && result.data) {
        this.comboDocGestion = result.data.filter((item: any )=> FILTRO1_TIPO_DOC_GESTION.includes(item.codigo));
      }
    })
  }

  // listarEstadoPostulacion() {
  //   let params = { tipoStatus: 'PROCESOP' }
  //   this.estatusProcesoServ.listarEstadoEstatusProceso(params).subscribe(
  //     (result: any) => {
  //       if (result.success) this.listEstadoPostu = result.data;
  //     })
  // }

  modalGenerarContrato = (item: any = null) => {
    this.ref = this.dialogService.open(ModalGeneracionContratoComponent, {
      header: 'Documentos para Generación de Contrato de Concesión',
      width: '650px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.SuccessMensaje('Se guardó correctamente');
        this.router.navigate(['planificacion/generacion-contrato']);
      }
    });
  };

  cargarPP() {
    const params = {
      documentoGestion: this.selectedIdProceso,
      tipoDocumentoGestion: this.selectTipoDocGestion,//this.selectTipoDocGestion,
      // idEstadoPostulacion: this.selectedEstadoPostu,
      // ganador: true,
      pageNum: this.optionPage.pageNum,
      pageSize: this.optionPage.pageSize,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.listarGeneracionContrato(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        result.data.forEach((element: any) => {
          if (element.fechaPostulacion != null)
            element.fechaPostulacionString = ConvertDateToString(ConvertNumberToDate(element.fechaPostulacion));
        });
        this.listaResultadoPP = result.data;
        this.totalRecords = result.totalRecord;

        if (result.data.length === 0) {
          this.toast.warn(result.message);
        }
      }
    }, () => {
      this.dialog.closeAll();
      this.errorMensaje();
    });
  };
  btnBuscar() {
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.cargarPP();
  }

  btnLmipiar() {
    this.selectedIdProceso = null;
    // this.selectedEstadoPostu = null;
    this.selectTipoDocGestion = null;
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.cargarPP();
  }

  btnRegresar() {
    this.router.navigate(['planificacion/generacion-contrato']);
  }

  loadData(event: any) {
    this.optionPage.pageNum = event.first + 1;
    this.optionPage.pageSize = event.rows;
    this.cargarPP();
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'success', summary: '', detail: mensaje });
  }

  warnMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'warn', summary: '', detail: mensaje });
  }

  errorMensaje(mensaje: any = 'Ocurrió un problema, intente nuevamente') {
    this.messageService.add({ key: 'tr', severity: 'error', summary: '', detail: mensaje });
  }

}
