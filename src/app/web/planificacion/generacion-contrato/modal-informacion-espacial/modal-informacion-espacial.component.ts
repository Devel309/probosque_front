import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as moment from "moment";
import { PlanificacionService } from '@services';
import { MapApi, OgcGeometryType, ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigosTiposPersona } from 'src/app/model/util/CodigosTiposPerona';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ContratoService } from 'src/app/service/contrato.service';

@Component({
  selector: 'app-modal-informacion-espacial',
  templateUrl: './modal-informacion-espacial.component.html',
  styles: [
  ]
})
export class ModalInformacionEspacialComponent implements OnInit {
  requestEntity: any = {};
  isDisabled: boolean = false;
  isPersonaJuridica: boolean = false;
  coordinates: any = [];
  minDateInicio: any = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private toast: ToastService,
    private dialog: MatDialog,
    private contratoService: ContratoService,
    private apiForestalService: ApiForestalService,
    private mapApi: MapApi,
    private planificacionService: PlanificacionService
  ) {
    this.requestEntity.idProcesoPostulacion = this.config.data.item.documentoGestion;
    this.requestEntity.contra = this.config.data.item.codigoTituloH;
    this.requestEntity.nomdep = this.config.data.item.idDepartamento;
    this.requestEntity.nompro = this.config.data.item.idProvincia;
    this.requestEntity.nomdis = this.config.data.item.idDistrito;

    this.requestEntity.codigoTipoPersonaTitular = this.config.data.item.codigoTipoPersonaTitular;
    if (this.config.data.item.codigoTipoPersonaTitular === CodigosTiposPersona.JURIDICA) {
      this.isPersonaJuridica = true;
      this.requestEntity.numruc = this.config.data.item.numeroDocumentoTitular;
    } else {
      this.requestEntity.nrodoc = this.config.data.item.numeroDocumentoTitular;
    }

    this.requestEntity.nomtit = this.config.data.item.nombreUsuarioPostulante;
    this.requestEntity.razonSocialTitular = this.config.data.item.razonSocialTitular;
    this.requestEntity.idContrato = this.config.data.item.idContrato;

    this.isDisabled = this.config.data.item?.idGeometria || false;
  }

  ngOnInit(): void {
    this.obtenerGeometria();
  }
  obtenerGeometria() {
    let idsContratos = this.requestEntity.idContrato.toString();
    this.coordinates = [];
    this.planificacionService.obtenerContratoPoligono(idsContratos).subscribe(
      (result: any) => {
        let cont: number = 0;
        result.data.forEach((t: any) => {
          let geometryJson: any = this.mapApi.wktParse(t.geometry);
          if (geometryJson.type.toUpperCase() === OgcGeometryType.POLYGON && cont === 0) {
            cont++;
            this.coordinates = geometryJson.coordinates;
          }
        });
      })
  }
  insertDerechoForestal(paramsOut: any) {

    // this.dialog.open(LoadingComponent, { disableClose: true });

    this.apiForestalService
      .insertDerechoForestal(paramsOut)
      .subscribe((response: any) => {

        if (response.dataService != null) {

          if (response.dataService.status == "400") {
            this.toast.warn(response.dataService.message);
          }
          else {
            if (response.dataService.data.mensaje != null) {
              if (response.dataService.data.observacionesCampos != null && response.dataService.data.observacionesCampos.length > 0) {

                let mensaje = "";
                let obs = "";
                mensaje = response.dataService.data.mensaje;
                response.dataService.data.observacionesCampos.forEach((t: any) => {

                  obs = obs + "\n" + t.campo + " : " + t.mensaje;


                });
                mensaje = mensaje + "\n" + obs;

                this.toast.warn(mensaje);
              }
              if (response.dataService.data.idsGeometrias != null && response.dataService.data.idsGeometrias.length > 0) {
                this.actualizarGeometria(parseInt(response.dataService.data.idsGeometrias[0]));
              }
            }
          }

        }

      });
  }

  actualizarGeometria(idOut: number) {
    const params = {
      "idContrato": this.config.data.item.idContrato,
      "idGeometria": idOut,
      "idUsuarioModificacion": this.config.data.idUser
    };

    this.contratoService.actualizarGeometria(params).subscribe(result => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.ref.close(true);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnEnviar() {
    if (!this.validar()) return;
    const paramsIn = this.getParams();
    this.insertDerechoForestal(paramsIn);
  }

  validar(): boolean {
    let validado = true;

    return validado;
  }

  getParams() {
    //[[[-78.16223144531249, -5.307031369598637], [-78.09906005859375, -5.353521355337321], [-78.02215576171875, -5.320705259943899], [-78.01666259765625, -5.271477837928355], [-78.09356689453125, -5.244127581489528], [-78.16223144531249, -5.307031369598637]]]
    const params = {
      "codProceso": this.requestEntity.codProceso,
      "geometria": {
        "poligono": this.coordinates
      },
      "atributos": this.requestEntity,
      // "idClasificacion": "string",
      // "codProceso": "string",
      // "idObjects": [
      //   0
      // ]
      // {
      //   "contra": this.requestEntity.codigoTituloH,
      //   "nomtit": this.requestEntity.nombreUsuarioPostulante,
      //   "nrodoc": "40696616", "numreg": "123", "numruc": "20601934109",
      //   "nomdis": "150103", "nompro": "1501", "nomdep": "15",

      //   "fuente": "PRUEBA2", "docreg": "NINGUNO", "observ": "", "zonutm": 18, "origen": 2, "tipcon": 80204, "adenda": "1",
      //   "cerfor": "1", "finali": "1", "situac": "1", "objcon": "1", "aderef": "1", "tipdoc": "1", "supsig": "120",
      //   "perime": "100", "autfor": "18", "produc": "2", "procot": "1", "docleg": "SS", "fecreg": "02/12/2011",
      //   "fecont": "02/11/2011", "fecini": "02/11/2011", "fecter": "02/11/2011", "fecleg": "02/11/2011"
      // }
    }
    return params;
  }

  btnClose() {
    this.ref.close();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
