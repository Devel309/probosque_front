import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CodigoEstadoPlanManejo} from '../../../model/util/CodigoEstadoPlanManejo';
import {PlanManejoService, UsuarioService} from '@services';
import {UsuarioModel} from '../../../model/seguridad/usuario';
import {Perfiles} from '../../../model/util/Perfiles';

@Component({
  selector: 'app-generacion-declaracion-manejo-dema',
  templateUrl: './generacion-declaracion-manejo-dema.component.html',
  styleUrls: ['./generacion-declaracion-manejo-dema.component.scss']
})
export class GeneracionDeclaracionManejoDemaComponent implements OnInit {
  idPlanManejo!: number;

  tabIndex: number = 0;

  usuario!: UsuarioModel;

  disabled: boolean = true;

  isPerfilArffs:boolean = false;
  obj: any ={};

  constructor(private route: ActivatedRoute,private planManejoService: PlanManejoService,private usuarioService: UsuarioService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {

    this.obtenerPlan();
  }

  obtenerPlan() {
    this.disabled = false;
    this.usuario = this.usuarioService.usuario;
    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL){
      this.disabled = true;
      this.isPerfilArffs = true;
    }else{

      this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {
        
        if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
          this.disabled = true;
        }
        if(result.data.codigoEstado == CodigoEstadoPlanManejo.BORRADOR ){
          this.disabled = false;
        }
      })
    }
  }
  ngAfterViewInit(){
    if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    }
    //console.log("MAIN ngAfterViewInit");
  }

  setTabIndex(obj: any){
    var cod = obj.tab.replace(obj.codigoEvaluacionDet,'');

    if(cod === 'IG') this.tabIndex = 0;
    else if (cod === 'REEVRIIC') {
      this.tabIndex = 0;

      setTimeout (() => {
        
        this.tabIndex = 1;
      }, 1000);}
    else if (cod === 'ZOIA') {this.tabIndex = 1;}
    else if (cod === 'RFM') this.tabIndex = 2;
    else if (cod === 'RFNM') this.tabIndex = 3;
    else if (cod === 'SMLS') this.tabIndex = 4;
    else if (cod === 'MPUMF') this.tabIndex = 5;
    else if (cod === 'AAE') this.tabIndex = 6;
    else if (cod === 'IAN') this.tabIndex = 7;
    else if (cod === 'CA') this.tabIndex = 8;
    else if (cod === 'A') this.tabIndex = 9;
    else if (cod === 'CED') this.tabIndex = 10;
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }
  informChange(event: any) {
    this.tabIndex = event;
  }

}
