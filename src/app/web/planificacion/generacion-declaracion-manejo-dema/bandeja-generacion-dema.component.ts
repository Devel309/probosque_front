import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { Page, PlanManejoMSG as MSG } from "@models";
import { PlanManejoService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { LazyLoadEvent } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { IPermisoOpcion } from "src/app/model/Comun/IPermisoOpcion";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { ResultadoEvaluacionTitularSharedComponent } from "src/app/shared/components/resultado-evaluacion-titular/resultado-evaluacion-titular-shared.component";
import { UsuarioModel } from "../../../model/seguridad/usuario";
import { CodigoEstadoEvaluacion } from "../../../model/util/CodigoEstadoEvaluacion";
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from "../../../model/util/CodigoEstadoPlanManejo";
import { Perfiles } from "../../../model/util/Perfiles";
@Component({
  selector: "bandeja-generacion-dema",
  templateUrl: "./bandeja-generacion-dema.component.html",
  styleUrls: ["./bandeja-generacion-dema.component.scss"],
})
export class BandejaGeneracionDema implements OnInit {
  f!: FormGroup;
  planes: any[] = [];

  loading = false;
  totalRecords = 0;

  first: number = 0;

  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;

  CodigoEstadoMesaPartes = CodigoEstadoMesaPartes;

  usuario!: UsuarioModel;

  Perfiles = Perfiles;
  ref!: DynamicDialogRef;

  DEMA = {
    idTipoProceso: 3, //proceso DEMA
    idTipoEscala: 2,
    idTipoPlan: 4,
    idSolicitud: 1,
    descripcion: "DEMA",
  };

  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialogService: DialogService,
    private evaluacionService: EvaluacionService,
    private activaRoute: ActivatedRoute
  ) {
    this.usuario = this.user.usuario;
    this.f = this.initForm();
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    this.buscar();
  }

  verDetalle(item: any) {
    var params = {
      idPlanManejo: item.idPlanManejo,
      codigoEvaluacion: "EVAL",
    };
    this.evaluacionService
      .listarEvaluacionResumido(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          if (response.data.length != 0) {
            this.ref = this.dialogService.open(
              ResultadoEvaluacionTitularSharedComponent,
              {
                header: "RESULTADO EVALUACIÓN",
                width: "60%",
                contentStyle: { overflow: "auto" },
                data: {
                  item: item,
                },
              }
            );

            this.ref.onClose.subscribe((resp: any) => {
              if (resp) {
                if (resp.tipo == "EVAL") {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );

                  this.router.navigate([
                    "/planificacion/evaluacion/requisitos-previos/" +
                      item.idPlanManejo +
                      "/" +
                      resp.data.codigoEvaluacionDet,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "DEMA"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/generar-declaracion-manejo-dema/" +
                      item.idPlanManejo,
                  ]);
                }
              }
            });
          } else {
            this.toast.warn("El Plan Seleccionado No Tiene Observaciones");
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  initForm() {
    return this.fb.group({
      dniElaborador: [null],
      rucComunidad: [null],
      idPlanManejo: [null],
    });
  }

  /*  buscar() {
    const r = {
      ...this.DEMA,
      ...this.f.value
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.filtrarPlanManejo(r)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(res => this.planes = res.data);
  } */

  buscar() {
    this.listarPlanes().subscribe();
  }

  listarPlanes(page?: Page) {
    let estados = {
      codigoEstado: "",
    };

    if (this.usuario.sirperfil == Perfiles.TITULARTH) {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.BORRADOR +
        "," +
        CodigoEstadoPlanManejo.PRESENTADO +
        "," +
        CodigoEstadoMesaPartes.PRESENTADOMDP +
        "," +
        CodigoEstadoMesaPartes.PRESENTADOSUBSANADOMP +
        "," +
        CodigoEstadoMesaPartes.PRESENTADO_PREREQUISITO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO_MESA_DE_PARTES +
        "," +
        CodigoEstadoMesaPartes.OBSERVADOMP +
        "," +
        CodigoEstadoMesaPartes.OBSERVADO_PREREQUISITO +
        "," +
        CodigoEstadoPlanManejo.COMPLETADO +
        "," +
        CodigoEstadoMesaPartes.COMPLETADOMP +
        "," +
        CodigoEstadoMesaPartes.COMPLETADO_MED_CORREC +
        "," +
        CodigoEstadoMesaPartes.COMPLETADO_PREREQUISITO +
        "," +
        CodigoEstadoPlanManejo.EN_EVALUACION +
        "," +
        CodigoEstadoMesaPartes.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.APROBADO +
        "," +
        CodigoEstadoPlanManejo.APROBADO_NOTIFICADO +
        "," +
        CodigoEstadoPlanManejo.FAVORABLE +
        "," +
        CodigoEstadoPlanManejo.DESFAVORABLE +
        "," +
        CodigoEstadoPlanManejo.IMPUGNADO_SAN +
        "," +
        CodigoEstadoPlanManejo.PROCEDE +
        "," +
        CodigoEstadoPlanManejo.NO_PROCEDE +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_DENEGADO +
        "," +
        CodigoEstadoPlanManejo.REGISTRADO +
        "," +
        CodigoEstadoPlanManejo.RECIBIDO +
        ",";
    } else if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.COMPLETADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO_MESA_DE_PARTES +
        "," +
        CodigoEstadoPlanManejo.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.APROBADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoPlanManejo.APROBADO_NOTIFICADO +
        "," +
        CodigoEstadoPlanManejo.IMPUGNADO_SAN;
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.BORRADOR +
        "," +
        CodigoEstadoPlanManejo.REGISTRADO +
        "," +
        CodigoEstadoPlanManejo.PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.RECIBIDO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_DENEGADO;
    } else {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.COMPLETADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO_MESA_DE_PARTES +
        "," +
        CodigoEstadoPlanManejo.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.APROBADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoPlanManejo.APROBADO_NOTIFICADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR +
        "," +
        CodigoEstadoPlanManejo.REGISTRADO +
        "," +
        CodigoEstadoPlanManejo.PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.RECIBIDO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_DENEGADO +
        "," +
        CodigoEstadoPlanManejo.IMPUGNADO_SAN;
    }

    const r = { ...this.DEMA, ...this.f.value, ...estados };
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(tap((res) => (this.planes = res.data)));
  }

  limpiar() {
    this.f.reset();
    this.first = 0;
    this.listarPlanes().subscribe();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
  }

  nuevoPlan() {
    const body = { ...this.DEMA, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => this.navigate(res.data.idPlanManejo));
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo);
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body).pipe(
      tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE),
      })
    );
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiPlanManejo
      .filtrar(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  /*  filtrarPlanManejo(request: any) {
    return this.apiPlanManejo.filtrar(request)
      .pipe(tap({
        error: () => this.toast.error(MSG.ERR.LIST)
      }));
  } */

  navigate(idPlan: number) {
    this.router.navigate([
      "/planificacion/generar-declaracion-manejo-dema",
      idPlan,
    ]);
  }

  registroImpugnacionEmiter(event: any) {
    this.limpiar();
  }
}
