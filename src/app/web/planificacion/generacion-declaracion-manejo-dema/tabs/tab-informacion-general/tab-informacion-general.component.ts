import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { DecimalPipe } from "@angular/common";
import { DepartamentoModel, DistritoModel, IGD_DETALLE, InformacionGeneralDema, InformacionGeneralDemaDetalle, ProvinciaModel } from "@models";
import { CoreCentralService, InformacionGeneralDemaService, PlanificacionService, SolicitudAccesoService, UsuarioService } from "@services";
import { aprovDEMA, DEMATipo, isNullOrEmpty, ToastService } from "@shared";
import { sortedUniq } from "lodash";
import { forkJoin } from "rxjs";
import { concatMap, finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PideService } from "src/app/service/evaluacion/pide.service";
import {CodigosDEMA} from '../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import { MessageService } from "primeng/api";
import { HttpErrorResponse } from "@angular/common/http";
import { ModalInfoPermisoComponent } from "src/app/shared/components/modal-info-permiso/modal-info-permiso.component";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { PermisoForestalService } from "src/app/service/permisoForestal.service";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import {Router} from '@angular/router';
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";

@Component({
  selector: "app-tab-informacion-general",
  templateUrl: "./tab-informacion-general.component.html",
  styleUrls: ["./tab-informacion-general.component.scss"],
})
export class TabInformacionGeneralComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() siguiente = new EventEmitter();
  @Input() isPerfilArffs!: boolean;

  @Input() disabled!: boolean;
  f: FormGroup;

  //constante Lista InformacionGeneralDetalle
  IGD_DETALLE = IGD_DETALLE;

  departamentos: any[] = [];
  provincias: any[] = [];
  distritos: any[] = [];
  listDepartamento: DepartamentoModel[] = [];
  listProvincia: ProvinciaModel[] = [];
  listDistrito: DistritoModel[] = [];
  listCuenca: any[] = [];
  listSubcuenca: any[] = [];
  listCuencaSubcuenca: CuencaSubcuencaModel[] = [];
  idCuenca!: number | null;
  idSubcuenca!: number | null;
  comunidad: InformacionGeneralDema = new InformacionGeneralDema();
  ref!: DynamicDialogRef;
  idPermisoForestal: number = 0;
  displayModal: boolean = false;
  idPermisoForestalTemp: number = 0;
  areaTotal = null;
  codTipoPersona = null;

  cuencas = [
    { name: "Río Suches", code: "3001" },
    { name: "Río Huancané", code: "3002" },
    { name: "Río Ramis", code: "3003" },
    { name: "Río Coata", code: "3004" },
    { name: "Río Desaguadero", code: "3005" },
  ];
  subCuenca = [
    { name: "Río Madre de Dios", code: "2101" },
    { name: "Río Tigre", code: "2102" },
    { name: "Río Napo", code: "2103" },
    { name: "Río Perumayo", code: "2104" },
    { name: "Río Yavarí", code: "2105" },
  ];

  get detalle(): InformacionGeneralDemaDetalle[] {
    return this.f.controls["lstUmf"].value;
  }



  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_1;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_1_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;

  nroArbolesMaderables: number | null = 0;
  nroArbolesMaderablesSemilleros: number | null = 0;
  volumenM3rMaderables: string | number | null = '0';
  nroArbolesNoMaderables: number | null = 0;
  nroArbolesNoMaderablesSemilleros: number | null = 0;
  volumenM3rNoMaderables: string | number | null = '0';
  superficieHaNoMaderables: string | number | null = '0';
  showWarn: boolean = true;
  msgWarn: string = '';
  tieneCenso: boolean = false;

  constructor(
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private api: InformacionGeneralDemaService,
    private apiSolAcceso: SolicitudAccesoService,
    private apiPlanificacion: PlanificacionService,
    private toast: ToastService,
    private _decimalPipe: DecimalPipe,
    private pideService: PideService,
    private evaluacionService: EvaluacionService,
    private servCoreCentral: CoreCentralService,
    private messageService: MessageService,
    private dialogService: DialogService,
    private permisoForestalService: PermisoForestalService,
    private router: Router,
    private anexosService: AnexosService
  ) {
    this.f = this.formInit();
  }

  ngOnInit(): void {
    this.listarPorFiltroDepartamento();
    this.listarPorFilroProvincia();
    this.listarPorFilroDistrito();
    this.listarCuencaSubcuenca();
    this.init();
    

    this.f.patchValue({
      idPlanManejo: this.idPlanManejo,
      codigoProceso: DEMATipo.DEMA,
      lstUmf: [...IGD_DETALLE],
    });

    

    if(this.isPerfilArffs)
      this.obtenerEvaluacion();

  }

  listarPorFiltroDepartamento() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia() {
    this.servCoreCentral.listarPorFilroProvincia({}).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarPorFilroDistrito() {
    this.servCoreCentral.listarPorFilroDistrito({}).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  Distrito(param: any) {
    this.f.controls.idDistritoRepresentante = param.value;
  }

  listarCuencaSubcuenca() {
    this.listCuencaSubcuenca = [];
    this.listCuenca = [];

    this.servCoreCentral.listarCuencaSubcuenca().subscribe(
      (result: any) => {
        this.listCuencaSubcuenca = result.data;

        let listC: any[] = [];
        this.listCuencaSubcuenca.forEach((item) => {
          listC.push(item);
        });

        const setObj = new Map();
        const unicos = listC.reduce((arr, cuenca) => {
          if (!setObj.has(cuenca.idCuenca)) {
            setObj.set(cuenca.idCuenca, cuenca)
            arr.push(cuenca)
          }
          return arr;
        }, []);

        this.listCuenca = unicos;
        this.listCuenca.sort((a, b) => a.cuenca - b.cuenca);

        if (!isNullOrEmpty(this.idCuenca) && !isNullOrEmpty(this.idSubcuenca)) {
          this.onSelectedCuenca({ value: Number(this.idCuenca) });
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  onSelectedCuenca(param: any) {
    this.listSubcuenca = [];

    this.listCuencaSubcuenca.forEach((item) => {
      if (item.idCuenca == param.value) {
        this.listSubcuenca.push(item);
      }
    });
  }

  openModal() {
    this.ref = this.dialogService.open(ModalInfoPermisoComponent, {
      header: "Buscar Solicitud de Permiso Forestal",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        buscar: true,
        nroDocumento: this.usuarioService.nroDocumento,
        idTipoDocumento: this.usuarioService.idtipoDocumento,
        idPlanManejo: this.idPlanManejo,
        idPermisoForestal: this.idPermisoForestal
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.displayModal = true;
        this.idPermisoForestalTemp = resp.idPermisoForestal;
        this.areaTotal = resp.areaComunidad;
      }
    });
  }

  closeModal() {
    this.displayModal = false;
    this.listarInformacionGeneralPermisoForestal();
  }

  listarInformacionGeneralPermisoForestal() {
    var params = {
      codTipoInfGeneral: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idPermisoForestal: this.idPermisoForestalTemp,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .listarInformacionGeneralPF(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[ 0 ];

          this.codTipoPersona = element.codTipoPersona;
          if (element.codTipoPersona === 'TPERJURI') {
            this.f.patchValue({ nombreComunidad: element.federacionComunidad });
            this.f.patchValue({ nombreElaboraDema: element.federacionComunidad });
            this.f.patchValue({ codTipoDocumentoRepresentante: element.tipoDocumentoElaborador });
            this.f.patchValue({ dniJefeComunidad: element.documentoElaborador });
            this.f.patchValue({ documentoRepresentante: element.documentoElaborador });
            this.f.patchValue({ nombresJefeComunidad: element.nombreElaborador });
            this.f.patchValue({ nombreRepresentante: element.nombreElaborador });
            this.f.patchValue({ apellidoPaternoJefeComunidad: element.apellidoPaternoElaborador });
            this.f.patchValue({ apellidoPaternoRepresentante: element.apellidoPaternoElaborador });
            this.f.patchValue({ apellidoMaternoJefeComunidad: element.apellidoMaternoElaborador });
            this.f.patchValue({ apellidoMaternoRepresentante: element.apellidoMaternoElaborador });
            this.f.patchValue({ nroRucComunidad: element.rucComunidad });
            this.f.patchValue({ dniElaboraDema: element.rucComunidad });
          } else {
            this.f.patchValue({ codTipoDocumentoElaborador: element.tipoDocumentoElaborador });
            this.f.patchValue({ dniJefeComunidad: element.documentoElaborador });
            this.f.patchValue({ dniElaboraDema: element.documentoElaborador });
            this.f.patchValue({ nombresJefeComunidad: element.nombreElaborador });
            this.f.patchValue({ nombreElaboraDema: element.nombreElaborador });
            this.f.patchValue({ apellidoPaternoJefeComunidad: element.apellidoPaternoElaborador });
            this.f.patchValue({ apellidoPaternoElaboraDema: element.apellidoPaternoElaborador });
            this.f.patchValue({ apellidoMaternoJefeComunidad: element.apellidoMaternoElaborador });
            this.f.patchValue({ apellidoMaternoElaboraDema: element.apellidoMaternoElaborador });
          }
          this.f.patchValue({ codTipoPersona: element.codTipoPersona });
          this.f.patchValue({ esReprLegal: element.esReprLegal });
          this.f.patchValue({ idPermisoForestal: element.idPermisoForestal });
          this.f.patchValue({ areaTotal: this.areaTotal });
          this.f.patchValue({ idDistrito: element.idDistrito });
          this.f.patchValue({ idProvincia: element.idProvincia });
          this.f.patchValue({ idDepartamento: element.idDepartamento });
          this.f.patchValue({ idDistritoRepresentante: element.idDistrito });
        }
      });
  }

  onChangeEvent(event: any) {
    var numero = event.target.value;
    this.f.patchValue({
      totalIngresosEstimado: Number(numero).toFixed(2),
      /* totalCostoEstimado: Number(numero).toFixed(2),
      totalUtilidadesEstimado: Number(numero).toFixed(2)    */
    });

    let costo = this.f.value.totalCostoEstimado==null?0:this.f.value.totalCostoEstimado;

    let valor = this.f.value.totalIngresosEstimado - costo;
    this.f.patchValue({
      totalUtilidadesEstimado: Number(valor).toFixed(2),
    });
  }
  onChangeEventCosto(event: any) {
    var numero = event.target.value;
    this.f.patchValue({
      totalCostoEstimado: Number(numero).toFixed(2),
    });
    let valor =
      this.f.value.totalIngresosEstimado - this.f.value.totalCostoEstimado;
    this.f.patchValue({
      totalUtilidadesEstimado: Number(valor).toFixed(2),
    });
  }

  onChangeEventUtilidad(event: any) {
    var numero = event.target.value;
  }

  init() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    // this.obtenerSolAcceso(this.usuarioService.nroDocumento)
    //   .pipe(finalize(() => this.dialog.closeAll()))
    //   .pipe(tap((data: any) => this.setDatosComunidad(data?.data)))
    //   .pipe(
    //     concatMap(() => {
    //       const { idDepartamento, idProvincia } = this.comunidad as any;
    //       return this.getUbigeo(idDepartamento, idProvincia);
    //     })
    //   )
    //   .pipe(tap({ error: () => this.toast.error(MSG.ERR.GET_COMUNIDAD) }))
    //   .subscribe();

    this.obtener(this.idPlanManejo, DEMATipo.DEMA)
      .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.GET_INFO_GENERAL) }))
      .subscribe(
        (res) => {
          if (res?.data && res.data.length > 0) {
            // si existe informacionGeneral
            this.f.patchValue({ ...res.data[0] });

            const element = res.data[0];
            this.codTipoPersona = element.codTipoPersona;
            this.idCuenca = isNullOrEmpty(element.cuenca) ? null : Number(element.cuenca);
            this.onSelectedCuenca({ value: this.idCuenca });
            this.idSubcuenca = isNullOrEmpty(element.subCuenca) ? null : Number(element.subCuenca);
            this.f.patchValue({ cuenca: this.idCuenca });
            this.f.patchValue({ subCuenca: this.idSubcuenca });
            if (element.codTipoPersona === 'TPERJURI') {
              this.f.patchValue({ nombreComunidad: element.nombreElaboraDema });
              this.f.patchValue({ dniJefeComunidad: element.documentoRepresentante });
              this.f.patchValue({ nombresJefeComunidad: element.nombreRepresentante });
              this.f.patchValue({ apellidoPaternoJefeComunidad: element.apellidoPaternoRepresentante });
              this.f.patchValue({ apellidoMaternoJefeComunidad: element.apellidoMaternoRepresentante });
            } else {
              this.f.patchValue({ dniJefeComunidad: element.dniElaboraDema });
              this.f.patchValue({ nombresJefeComunidad: element.nombreElaboraDema });
              this.f.patchValue({ apellidoPaternoJefeComunidad: element.apellidoPaternoElaboraDema });
              this.f.patchValue({ apellidoMaternoJefeComunidad: element.apellidoMaternoElaboraDema });
            }

            // this.obtenerAprovechamiento(this.idPlanManejo, aprovDEMA.MADERABLE)
            //   .pipe(tap((data: any) => this.setAprovechamiento(data?.data)))
            //   .pipe(tap({ error: () => this.toast.error(MSG.ERR.GET_CENSO) }))
            //   .subscribe();
            this.listarAnexos();
            //this.aprovechamientoNoMaderable = false;

            // this.f.patchValue({
            //   aprovechamientoNoMaderable: false,
            // });

            // this.setAprovechamientoNoMaderable();
          } else {
            // this.obtenerAprovechamiento(this.idPlanManejo, aprovDEMA.MADERABLE)
            //   .pipe(tap((data: any) => this.setAprovechamiento(data?.data)))
            //   .pipe(tap({ error: () => this.toast.error(MSG.ERR.GET_CENSO) }))
            //   .subscribe();
            this.listarAnexos();
          }
        },
        (_err) => this.dialog.closeAll()
      );
  }

  listarAnexos() {
    forkJoin([
      this.listAnexo5(),
      this.listAnexo6(),
      this.listAnexo7(),
    ]).pipe(finalize(() => {
      if (this.tieneCenso === false) {
        this.toast.warn(this.msgWarn);
      }
    })).subscribe();
  }

  listAnexo5() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'DEMA',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo2(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroAM = 0;
          let nroAMS = 0;
          let vM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroAM = nroAM + 1;
            }
            if (element.categoria == 'S') {
              nroAMS = nroAMS + 1;
            }
            if (element.volumen) {
              vM = vM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesMaderables = nroAM;
          this.nroArbolesMaderablesSemilleros = nroAMS;
          this.volumenM3rMaderables = vM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesMaderables = 0;
          this.nroArbolesMaderablesSemilleros = 0;
          this.volumenM3rMaderables = '0';
          if (this.showWarn) {
            this.showWarn = false;
            this.msgWarn = response.message;
            // this.toast.warn(response.message);
          }
        }
        this.f.patchValue({ nroArbolesMaderables: this.nroArbolesMaderables });
        this.f.patchValue({ nroArbolesMaderablesSemilleros: this.nroArbolesMaderablesSemilleros });
        this.f.patchValue({ volumenM3rMaderables: this.volumenM3rMaderables });
      }));
  }

  listAnexo6() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'DEMA',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo6(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let sNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.areaBosque) {
              sNM = sNM + parseFloat(element.areaBosque);
            }
          });
          this.superficieHaNoMaderables = sNM;
          this.tieneCenso = true;

          this.f.patchValue({ aprovechamientoNoMaderable: true });
        } else {
          this.superficieHaNoMaderables = 0;
          if (this.showWarn) {
            this.showWarn = false;
            this.msgWarn = response.message;
            // this.toast.warn(response.message);
          }

          this.f.patchValue({ aprovechamientoNoMaderable: this.f.controls.aprovechamientoNoMaderable.value == true ? true : false });
        }
        this.f.patchValue({ superficieHaNoMaderables: this.superficieHaNoMaderables });
      }));
  }

  listAnexo7() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'DEMA',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo7NoMaderable(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroANM = 0;
          let nroANMS = 0;
          let vNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroANM = nroANM + 1;
            }
            if (element.categoria == 'S') {
              nroANMS = nroANMS + 1;
            }
            if (element.volumen) {
              vNM = vNM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesNoMaderables = nroANM;
          this.nroArbolesNoMaderablesSemilleros = nroANMS;
          this.volumenM3rNoMaderables = vNM;
          this.tieneCenso = true;

        

          this.f.patchValue({ aprovechamientoNoMaderable: true });
        } else {
          this.nroArbolesNoMaderables = 0;
          this.nroArbolesNoMaderablesSemilleros = 0;
          this.volumenM3rNoMaderables = '0';
          if (this.showWarn) {
            this.showWarn = false;
            this.msgWarn = response.message;
            // this.toast.warn(response.message);
          }

          this.f.patchValue({ aprovechamientoNoMaderable: this.f.controls.aprovechamientoNoMaderable.value == true ? true : false });
        }
        this.f.patchValue({ nroArbolesNoMaderables: this.nroArbolesNoMaderables });
        this.f.patchValue({ nroArbolesNoMaderablesSemilleros: this.nroArbolesNoMaderablesSemilleros });
        this.f.patchValue({ volumenM3rNoMaderables: this.volumenM3rNoMaderables });
      }));
  }

  formInit() {
    const f = this.fb.group({});
    const info = new InformacionGeneralDema();
    Object.keys(info).forEach((key) =>
      f.addControl(key, new FormControl(null))
    );
    return f;
  }

  validarTitular() {

    console.log("this.codTipoPersona ", this.codTipoPersona);

    let dni;
    if (this.codTipoPersona === 'TPERJURI') {
      dni = this.f.value.documentoRepresentante;
    } else {
      dni = this.f.value.dniElaboraDema;
    }
    var params = {
      numDNIConsulta: dni
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService
      .consultarDNI(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (!!response.dataService.datosPersona) {
            this.toast.ok(response?.dataService.deResultado);

            if (this.codTipoPersona === 'TPERJURI') {
              this.f.patchValue({
                nombreRepresentante: response.dataService.datosPersona.prenombres,
              });
              this.f.patchValue({
                apellidoPaternoRepresentante: response.dataService.datosPersona.apPrimer,
              });
              this.f.patchValue({
                apellidoMaternoRepresentante: response.dataService.datosPersona.apSegundo,
              });
            } else {
              this.f.patchValue({
                nombreElaboraDema: response.dataService.datosPersona.prenombres,
              });
              this.f.patchValue({
                apellidoPaternoElaboraDema: response.dataService.datosPersona.apPrimer,
              });
              this.f.patchValue({
                apellidoMaternoElaboraDema: response.dataService.datosPersona.apSegundo,
              });
            }


          } else {
            this.toast.warn(response?.dataService.deResultado);
            this.f.patchValue({ nombreElaboraDema: "" });
            this.f.patchValue({ apellidoPaternoElaboraDema: "" });
            this.f.patchValue({ apellidoMaternoElaboraDema: "" });
          }
        } else {
          this.toast.warn(response?.dataService.deResultado);
        }
      });
  }

  setDatosComunidad(data: any) {
    if (isNullOrEmpty(data) || data.length == 0) return;
    const item = data[0];
    this.comunidad = {
      ...this.comunidad,
      nombreComunidad: item.razonSocialEmpresa,
      dniJefeComunidad: item.numeroDocumento,
      nombresJefeComunidad: item.nombres,
      apellidoMaternoJefeComunidad: item.apellidoMaterno,
      apellidoPaternoJefeComunidad: item.apellidoPaterno,
      idDepartamento: item.idDepartamentoPersona,
      idProvincia: item.idProvinciaPersona,
      idDistrito: item.idDistritoPersona,
      nroRucComunidad: item.numeroRucEmpresa,
    };
    this.f.patchValue({ nroRucComunidad: item.numeroRucEmpresa });
  }

  setAprovechamiento(data: any) {
    if (isNullOrEmpty(data) || data.length == 0) return;
    //const item = data[0];
    let arrarList: any[] = data;
    let totalnumeroArbolesAprovechables = 0;
    let totalnumeroArbolSemilleros = 0;
    let totalsuperficieComercial = 0;
    let totalvolumenComercial = 0;

    arrarList.forEach((element) => {
      totalnumeroArbolesAprovechables += element.numeroArbolesAprovechables;
      totalnumeroArbolSemilleros += element.numeroArbolSemilleros;
      totalsuperficieComercial += element.superficieComercial;
      totalvolumenComercial += element.volumenComercial;
    });

    this.f.patchValue({
      nroArbolesMaderables: totalnumeroArbolesAprovechables,
      nroArbolesMaderablesSemilleros: totalnumeroArbolSemilleros,
      superficieHaMaderables: this._decimalPipe.transform(
        totalsuperficieComercial,
        "1.2-2"
      ),
      volumenM3rMaderables: this._decimalPipe.transform(
        totalvolumenComercial,
        "1.2-2"
      ),
    });
  }

  //#region requests begin
  obtenerSolAcceso(numeroDocumento: string) {
    return this.apiSolAcceso
      .listarSolicitudAcceso({ numeroDocumento })
      .pipe(tap({ error: (err) => console.log(err) }));
  }
  getUbigeo(idDepartamento: string, idProvincia: string) {
    return forkJoin([
      this.getDepartamentos(),
      this.getProvincias(idDepartamento),
      this.getDistritos(idProvincia),
    ]);
  }
  getDistritos(idProvincia: string) {
    return this.apiPlanificacion.listarPorFilroDistrito({ idProvincia }).pipe(
      tap((res: any) => {
        if (!isNullOrEmpty(res.data)) this.distritos = res.data;
      })
    );
  }

  getProvincias(idDepartamento: string) {
    return this.apiPlanificacion
      .listarPorFilroProvincia({ idDepartamento })
      .pipe(
        tap((res: any) => {
          if (!isNullOrEmpty(res.data)) this.provincias = res.data;
        })
      );
  }

  getDepartamentos() {
    return this.apiPlanificacion
      .listarPorFiltroDepartamento_core_central({})
      .pipe(
        tap((res: any) => {
          if (!isNullOrEmpty(res.data)) this.departamentos = res.data;
        })
      );
  }

  obtener(idPlanManejo: number, codigoProceso: string) {
    return this.api.obtener(idPlanManejo, codigoProceso);
  }
  registrarInfoGeneral(body: any) {
    return this.api.registrar(body);
  }
  actualizarInfoGeneral(body: any) {
    return this.api.actualizar(body);
  }

  obtenerAprovechamiento(idPlanManejo: number, tipoAprovechamiento: number) {
    return this.api.obtenerAprovechamiento(idPlanManejo, tipoAprovechamiento);
  }
  //#endregion requests end
  changeAprovechamiento(checked: boolean) {
    if (checked) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.obtenerAprovechamiento(this.idPlanManejo, aprovDEMA.NO_MADERABLE)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res) => {
          if (isNullOrEmpty(res.data) || res.data.length == 0) return;
          //const item = res.data[0];

          let arrarList: any[] = res.data;
          let totalnumeroArbolesAprovechables = 0;
          let totalnumeroArbolSemilleros = 0;
          let totalsuperficieComercial = 0;
          let totalvolumenComercial = 0;

          arrarList.forEach((element) => {
            totalnumeroArbolesAprovechables +=
              element.numeroArbolesAprovechables;
            totalnumeroArbolSemilleros += element.numeroArbolSemilleros;
            totalsuperficieComercial += element.superficieComercial;
            totalvolumenComercial += element.volumenComercial;
          });

          let nomaderable = {
            numeroArbolesAprovechables: totalnumeroArbolesAprovechables,
            numeroArbolSemilleros: totalnumeroArbolSemilleros,
            superficieComercial: this._decimalPipe.transform(
              totalsuperficieComercial,
              "1.2-2"
            ),
            volumenComercial: this._decimalPipe.transform(
              totalvolumenComercial,
              "1.2-2"
            ),
          };

          this.setAprovechamientoNoMaderable(nomaderable);
        });
    } else {
      this.setAprovechamientoNoMaderable();
    }
  }

  setAprovechamientoNoMaderable(item?: any) {
    this.f.patchValue({
      nroArbolesNoMaderables: item ? item.numeroArbolesAprovechables : null,
      nroArbolesNoMaderablesSemilleros: item
        ? item.numeroArbolSemilleros
        : null,
      superficieHaNoMaderables: item ? item.superficieComercial : null,
      volumenM3rNoMaderables: item ? item.volumenComercial : null,
    });
  }

  guardar() {
    if (this.validationFields(this.f.value)) {
      this.f.patchValue({ idPersonaComunidad: this.usuarioService.idUsuario });
      let fechaActual = new Date();
      fechaActual.setHours(0, 0, 0, 0);
      let fechaInicial = new Date(this.f.value.fechaInicioDema);
      let fechaFinal = new Date(this.f.value.fechaFinDema);

      if (fechaInicial < fechaActual) {
        this.toast.error("La Fecha Inicio debe ser posterior a la Fecha Actual");
        return;
      }

      if (fechaFinal <= fechaInicial) {
        this.toast.error("La Fecha Final debe ser posterior a la Fecha Inicio");
        return;
      }

      let request;
      if (isNullOrEmpty(this.f.controls.idInformacionGeneralDema.value)) {
        this.f.patchValue({ idUsuarioRegistro: this.usuarioService.idUsuario });
        request = this.registrarInfoGeneral(this.f.value);
      } else {
        this.f.patchValue({ idUsuarioModificacion: this.usuarioService.idUsuario });
        request = this.actualizarInfoGeneral(this.f.value);
      }

      this.dialog.open(LoadingComponent, { disableClose: true });
      request
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res) => {
          this.toast.ok(res?.message);
          this.init();
          if (isNullOrEmpty(this.f.controls.idInformacionGeneralDema.value)) {
            this.registrarPermisoForestalPlanManejo();
          }
      });
    }
  }

  validationFields(data: InformacionGeneralDema) {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!isNullOrEmpty(data.codTipoPersona)) {
      if (data.codTipoPersona === 'TPERJURI') {
        if (!data.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Razon Social.\n";
        }
      }
    } else {
      if (!data.dniJefeComunidad) {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: DNI del Jefe de la Comunidad .\n";
      }
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  registrarPermisoForestalPlanManejo() {
    let listPlanManejo: any[] = [];
    var params = {
      idPermisoForestal: this.idPermisoForestalTemp,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    listPlanManejo.push(params);

    this.permisoForestalService
      .registrarPermisoForestalPlanManejo(listPlanManejo)
      .subscribe();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet : this.codigoProceso,
      //codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }else{
          this.evaluacion = {
            idPlanManejo: this.idPlanManejo,
            codigoEvaluacion: this.codigoProceso,
            codigoEvaluacionDet: this.codigoProceso,
            codigoEvaluacionDetSub: this.codigoTab,
            listarEvaluacionDetalle: [],
            idUsuarioRegistro: this.usuarioService.idUsuario,
          };
        }
      }
    })
  }

  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6231"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((res:any) => {
              this.toast.ok(res.message);
              this.obtenerEvaluacion();
            })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }



}

const MSG = {
  OK: {},
  ERR: {
    LIST: "Ocurrió un error al listar los Planes",
    GET_COMUNIDAD: "Ocurrió un error al obtener datos comunidad",
    GET_INFO_GENERAL: "Ocurrió un error al obtener información general",
    GET_CENSO: "Ocurrió un error al obtener información censo",
  },
};

export interface CuencaSubcuencaModel {
  codCuenca: string,
  codSubCuenca: string,
  codigo: string,
  cuenca: string,
  idCuenca: number,
  idSubCuenca: number,
  subCuenca: string,
  valor: string
}
