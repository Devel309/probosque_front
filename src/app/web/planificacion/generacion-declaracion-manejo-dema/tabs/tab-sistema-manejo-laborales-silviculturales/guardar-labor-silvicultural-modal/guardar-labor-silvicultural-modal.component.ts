import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { MatDialog } from '@angular/material/dialog';
import { LaborSilviculturalDto } from 'src/app/model/LaborSilviculturalDto';
import { LaborSilviculturalServiceService } from 'src/app/service/labor-silvicultural-service.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { finalize } from 'rxjs/operators';
import { AccionTipo, ToastService } from '@shared';

@Component({
  selector: 'app-guardar-labor-silvicultural-modal',
  templateUrl: './guardar-labor-silvicultural-modal.component.html',
  styleUrls: ['./guardar-labor-silvicultural-modal.component.scss']
})
export class GuardarLaborSilviculturalModalComponent implements OnInit {
  f: FormGroup;
  labor: LaborSilviculturalDto = new LaborSilviculturalDto();
  labelBtn: string = (this.config.data.accion === AccionTipo.REGISTRAR) ? 'Agregar' : 'Editar';

  idGenerado: string = this.generarId();

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private dialog: MatDialog,
    private toast: ToastService
  ) {
    this.f = this.initForm();
  }
  cmbTipoActividad: any[] = [
    { code: 0, name: 'Seleccionar' },
    { code: 1, name: 'Obligatorias' },
    { code: 2, name: 'Opcionales' }
  ];

  initForm(): FormGroup {
    return this.fb.group({
      tipoActividad: 0,
      actividad: [null, [Validators.required, Validators.minLength(1)]],
      descripcion: [null, [Validators.required, Validators.minLength(1)]],
      equipos: [null, [Validators.required, Validators.minLength(1)]],
      insumos: [null, [Validators.required, Validators.minLength(1)]],
      personal: [null, [Validators.required, Validators.minLength(1)]],
      observaciones: [null, [Validators.required, Validators.minLength(1)]],
    });
  }

  ngOnInit(): void {
    this.labor = this.config.data as LaborSilviculturalDto;
    const { idUsuarioRegistro, tipoActividad, actividad, descripcion, equipos, insumos, personal, observaciones } = this.labor;
    this.f.patchValue({ idUsuarioRegistro, tipoActividad, actividad, descripcion, equipos, insumos, personal, observaciones });
    // this.tipoParticipacion = this.participacion.codTipoPartComunal;
    // this.agregarValidadores(this.tipoParticipacion);
  }

  generarId() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  guardar() {
    if(this.showFormError()) return 
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    const { idUsuarioRegistro, tipoActividad, actividad, descripcion, equipos, insumos, personal, observaciones } = this.f.value;
    
    const id = (this.config.data.accion === AccionTipo.REGISTRAR ) ? this.idGenerado : this.config.data.id;
    if(this.config.data.accion === AccionTipo.EDITAR && this.config.data.id.substr(0,1) === "_"){
      this.config.data.accion = AccionTipo.REGISTRAR;
    }

    this.labor = { ...this.labor, idUsuarioRegistro, tipoActividad, actividad, descripcion, equipos, insumos, personal, observaciones, id };
    this.labor.idUsuarioRegistro = 1;

    this.dialog.closeAll();
    this.ref.close(this.labor);
  }


  cancelar() {
    this.ref.close();
  }

  showFormError() {
    const keys = Object.keys(this.f.value);
    const valid = this.toast.validAndShowError<LaborSilviculturalDto>(keys, this.f.value, ValidacionCampoError);    
    return this.f.invalid || !valid;
  }

}

const ValidacionCampoError: any = {
  tipoActividad: 'Tipo Actividad',
  actividad: 'Actividad',
  descripcion: 'Descripción',
  equipos: 'Equipos',
  insumos: 'Insumos',
  personal: 'Personal',
  observaciones: 'Observaciones'
}
