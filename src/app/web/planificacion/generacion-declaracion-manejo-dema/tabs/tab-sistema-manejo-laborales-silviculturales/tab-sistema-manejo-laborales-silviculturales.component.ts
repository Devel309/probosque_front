import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize, tap } from 'rxjs/operators';
import { LaborSilviculturalDto } from 'src/app/model/LaborSilviculturalDto';
import { LaborSilviculturalServiceService } from 'src/app/service/labor-silvicultural-service.service';
import {AccionTipo, compareObjects, ToastService} from '@shared';
import { GuardarLaborSilviculturalModalComponent } from './guardar-labor-silvicultural-modal/guardar-labor-silvicultural-modal.component';
import { Detalle } from 'src/app/model/ActividadesSilviculturalesModel';
import { forkJoin, Observable, of } from 'rxjs';

import { LoadingComponent } from 'src/app/components/loading/loading.component';
import {CodigosDEMA} from '../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {UsuarioService} from '@services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab-sistema-manejo-laborales-silviculturales',
  templateUrl: './tab-sistema-manejo-laborales-silviculturales.component.html',
  styleUrls: ['./tab-sistema-manejo-laborales-silviculturales.component.scss']
})
export class TabSistemaManejoLaboralesSilviculturalesComponent implements OnInit {

  @Input() idPlanManejo!: number;@Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  AccionTipo = AccionTipo;
  pendiente: Boolean = false;

  lstObligatorio: LaborSilviculturalDto[] = [];
  lstOpcional: LaborSilviculturalDto[] = [];
  lstModificados: LaborSilviculturalDto[] = [];

  idUsuario = 27;



  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_5;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_5_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private api: LaborSilviculturalServiceService,private toast: ToastService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private evaluacionService: EvaluacionService,private user: UsuarioService,private router: Router,
  ) { }

  ngOnInit(): void {
    this.listaLaborSilvicultural(
      this.idPlanManejo).subscribe();

    if(this.isPerfilArffs)
      this.obtenerEvaluacion();

  }


  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
     // codigoEvaluacionDet : this.codigoProceso,
      //codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }else{
          this.evaluacion = {
            idPlanManejo: this.idPlanManejo,
            codigoEvaluacion: this.codigoProceso,
            codigoEvaluacionDet: this.codigoProceso,
            codigoEvaluacionDetSub: this.codigoTab,
            listarEvaluacionDetalle: [],
            idUsuarioRegistro: this.user.idUsuario,
          };
        }
      }
    })
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP628"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }


  guardar(): void {
    this.registrarLaborSilvicultural(this.lstModificados).subscribe();
  }
  obtenerPorId(list: LaborSilviculturalDto[], id: string): any {
    return list.find(x => x.id == id);
  }
  eliminar(list: LaborSilviculturalDto[], id: string) {
    const item = list.find(x => x.id == id);
    if (item) {
      if(item.accion !== AccionTipo.REGISTRAR) {
        item.accion = AccionTipo.ELIMINAR;

        this.addListModificados(item!);
      }else{
        this.lstModificados = this.lstModificados.filter(item => item.id !== id);
      }

      list = list.filter(item => item.id !== id);

      this.pendiente = true;
    }
    return list;
  }
  addListModificados(data: LaborSilviculturalDto) {
    const itemModificado = this.lstModificados.findIndex((item) => item.id == data.id);
    if (itemModificado >= 0) {
      this.lstModificados.splice(itemModificado, 1);
    }

    this.lstModificados.push(data);

  }
  eliminarLaborSilvicultural(data: LaborSilviculturalDto[]){
    var detalle = data.map((x) => (
      {
        accion : false,
        idActividadSilviculturalDet : (x.accion == AccionTipo.REGISTRAR) ? null : parseInt(x.id),
        // idActSilvicultural: null,
        idTipo : "4",
        idTipoTratamiento: x.tipoActividad.toString(),
        actividad: x.actividad,
        descripcionDetalle: x.actividad,
        equipo: x.equipos,
        insumo: x.insumos,
        tratamiento: '',
        justificacion: '',
        observacionDetalle: x.observaciones,
        tipoTratamiento: '',
        idUsuarioRegistro: this.idUsuario,
        personal: x.personal
      }
    ));

    return this.api.eliminiarLaborSilvicultural(detalle)
      .pipe(tap({
        next: res => {
          this.messageService.add({ severity: "success", summary: "", detail: "Se eliminó: Labor Silvicultural" });
          this.listaLaborSilvicultural(this.idPlanManejo);
          this.pendiente = false;
        },
        error: (detail) => {
          
          this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
        }
      }));

  }
  registrarLaborSilvicultural(data: LaborSilviculturalDto[]){
    this.dialog.open(LoadingComponent, { disableClose: true });
    let detalleRegistro = data.filter(x => x.accion=== AccionTipo.REGISTRAR || x.accion === AccionTipo.EDITAR )
      .map((x) => (
      {
        accion : false,
        idActividadSilviculturalDet : (x.accion == AccionTipo.REGISTRAR) ? null : parseInt(x.id),
        idTipo : "4",
        idTipoTratamiento: x.tipoActividad.toString(),
        actividad: x.actividad,
        descripcionDetalle: x.descripcion,
        equipo: x.equipos,
        insumo: x.insumos,
        tratamiento: '',
        justificacion: '',
        observacionDetalle: x.observaciones,
        tipoTratamiento: '',
        idUsuarioRegistro: this.idUsuario,
        personal: x.personal
      }
    ));
    let actividad = {
      'idActSilvicultural': 0,
      'codigoTipoActSilvicultural': 'DEMASMLS',
      'actividad': '',
      'monitoreo': 'N',
      'anexo':'N',
      'descripcion': '',
      'observacion': '',
      'idPlanManejo': this.idPlanManejo,
      'idTipoTratamiento' : 0,
      'idUsuarioRegistro': this.idUsuario,
      'listActividadSilvicultural' : detalleRegistro
    };
    let detalleEliminado = data.filter(x => x.accion === AccionTipo.ELIMINAR )
      .map((x) => (
      {
        accion : false,
        idActividadSilviculturalDet : parseInt(x.id),
        idTipo : "4",
        idTipoTratamiento: x.tipoActividad.toString(),
        actividad: x.actividad,
        descripcionDetalle: x.actividad,
        equipo: x.equipos,
        insumo: x.insumos,
        tratamiento: '',
        justificacion: '',
        observacionDetalle: x.observaciones,
        tipoTratamiento: '',
        personal: x.personal,
        idUsuarioElimina: this.idUsuario
      }
    ));

    return forkJoin( {
      responseGuardar : detalleRegistro.length > 0 ? this.api.guardarLaborSilvicultural(actividad) : of(null),
      responseEliminar : detalleEliminado.length > 0 ? this.api.eliminiarLaborSilvicultural(detalleEliminado) : of(null)
    })
    .pipe(
      finalize(() => {
        this.listaLaborSilvicultural(this.idPlanManejo).subscribe();
        this.dialog.closeAll()
      }),
      tap({
      next: res => {
        this.messageService.add({ severity: "success", summary: "", detail: "Se actualizó: Sistema de Manejo y Labores Silviculturales" });
        this.pendiente = false;
      },
      error: (detail) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
      }
    }));

  }
  openEliminar(event: Event, data: LaborSilviculturalDto): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.tipoActividad == 1) {
          this.lstObligatorio = this.eliminar(this.lstObligatorio, data.id);
        } else {
          this.lstOpcional = this.eliminar(this.lstOpcional, data.id);
        }

        this.messageService.add({
          key: 'tr',
          severity: 'success',
          summary: '',
          detail: 'Se eliminó la actividad del Sistema de Manejo y Labores Silviculturales correctamente.',
        });
      },
      reject: () => {
      }
    });
  }
  abrirModal(titulo: AccionTipo, data?: LaborSilviculturalDto) {
    const header = `${titulo} Labores Silviculturales`;
    data = data ? data : new LaborSilviculturalDto();
    data.accion = titulo;

    const config = { header, data, width: '50vw', closable: true };
    const ref = this.dialogService.open(GuardarLaborSilviculturalModalComponent, config);

    ref.onClose.subscribe((labor: LaborSilviculturalDto) => {
      this.pendiente = !compareObjects(data, labor) ? true : false;

      if (labor) {
        if (titulo === AccionTipo.REGISTRAR) {
          if (labor.tipoActividad == TipoActividad.Obligatoria) {
            this.lstObligatorio.push(labor);
          } else {
            this.lstOpcional.push(labor);
          }
        } else {
          var indexObligatorio = this.lstObligatorio.findIndex(x => x.id === labor.id);
          var indexOpcional = this.lstOpcional.findIndex(x => x.id === labor.id);

          if (indexObligatorio >= 0) {
            if (this.lstObligatorio[indexObligatorio].tipoActividad == labor.tipoActividad) {
              this.lstObligatorio[indexObligatorio] = labor;
            } else {
              this.lstObligatorio = this.eliminar(this.lstObligatorio, labor.id);
              this.lstOpcional.push(labor);
            }
          } else if (indexOpcional >= 0) {
            if (this.lstOpcional[indexOpcional].tipoActividad == labor.tipoActividad) {
              this.lstOpcional[indexOpcional] = labor;
            } else {
              this.lstOpcional = this.eliminar(this.lstOpcional, labor.id);
              this.lstObligatorio.push(labor);
            }
          }
        }

        this.addListModificados(labor);
      }
    });
  }
  listaLaborSilvicultural(idPlanManejo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.api.obtenerListaLaborSilvicultural(idPlanManejo)
      .pipe(
        finalize(() => this.dialog.closeAll()),
        tap({
        next: res => {
          if (res?.data && res?.data !== undefined) {
            

            this.popularLstModificados(res.data.detalle);
          };
        },
        error: (detail) => {
          
          this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
        }
      }));
  }
  popularLstModificados(listado: Detalle[]){
    this.lstObligatorio = [];
    this.lstOpcional = [];
    this.lstModificados = [];

    const listaLaboresSiviculturales: Detalle[] = listado;
    listaLaboresSiviculturales.forEach(labor => {
      const objLabor = new LaborSilviculturalDto(labor);

      objLabor.id = (labor.idActividadSilviculturalDet === null) ? this.generarId() : objLabor.id;
      objLabor.accion = (labor.idActividadSilviculturalDet === null) ? AccionTipo.REGISTRAR : AccionTipo.EDITAR;

      if (objLabor.tipoActividad === TipoActividad.Obligatoria) {
        this.lstObligatorio.push(objLabor);
        if(labor.idActividadSilviculturalDet === null){
          this.lstModificados.push(objLabor);
        }
      } else {
        this.lstOpcional.push(objLabor);
        if(labor.idActividadSilviculturalDet === null){
          this.lstModificados.push(objLabor);
        }
      }
    });
  }
  generarId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }
  siguienteTab() {
    this.siguiente.emit();
  }
  regresarTab() {
    this.regresar.emit();
  }
}

enum TipoActividad {
  Obligatoria = 1,
  Opcionales = 2
}
