import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  ViewChild,
  Output,
} from '@angular/core';
import { FileModel } from 'src/app/model/util/File';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ArchivoService, UsuarioService } from '@services';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { DownloadFile, descargarArchivo, ToastService } from '@shared';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { HttpParams } from '@angular/common/http';
import { Anexos } from 'src/app/model/anexosModel';
import { aprovDEMA } from '@shared';
import { MapApi } from 'src/app/shared/mapApi';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { finalize } from 'rxjs/operators';
import { CodigosDEMA } from '../../../../../model/util/DEMA/DEMA';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-anexos',
  templateUrl: './tab-anexos.component.html',
  styleUrls: ['./tab-anexos.component.scss'],
})
export class TabAnexosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input('dataBase') dataBase: any = {};
  @Input('enabledControlAnexo') enabledControlAnexo: any = {};
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;

  lstAnexo2: Anexos[] = [];
  lstAnexo3: any[] = [];

  filesAnexo1: FileModel[] = [];
  archivoMapPDF: FileModel = {} as FileModel;
  cargarAnexo1: boolean = false;
  eliminarAnexo1: boolean = true;
  idAnexo1: number = 0;
  verEnviar1: boolean = false;

  archivoSHP: FileModel = {} as FileModel;
  totalVolumne: number = 0;

  view: any = null;
  _id = this.mapApi.Guid2.newGuid;

  filesAsamblea: FileModel[] = [];
  fileAsambleaComunal: FileModel = {} as FileModel;
  cargarAnexo4: boolean = false;
  eliminarAnexo4: boolean = true;
  idAnexo4: number = 0;
  verEnviar4 = false;

  filesAnexo3: FileModel[] = [];
  fileAnexo3: FileModel = {} as FileModel;
  cargarAnexo3: boolean = false;
  eliminarAnexo3: boolean = true;
  idAnexo3: number = 0;
  verEnviar3 = false;

  filesAnexo2: FileModel[] = [];
  fileAnexo2: FileModel = {} as FileModel;
  cargarAnexo2: boolean = false;
  eliminarAnexo2: boolean = true;
  idAnexo2: number = 0;
  verEnviar2 = false;

  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_10;
  codigoAcordeon1: string = CodigosDEMA.ACORDEON_10_1;

  evaluacion_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon1,
  });

  evaluacion: any;

  listAnexo2Maderables: any[] = [];
  listAnexo3NoMaderables: any[] = [];
  totalVolumen: number = 0;
  showWarn: boolean = true;

  constructor(
    private messageService: MessageService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private anexosService: AnexosService,
    private dialog: MatDialog,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.isPerfilArffs) this.obtenerEvaluacion();

    // this.initializeMap();
    this.archivoMapPDF.inServer = false;
    this.archivoMapPDF.descripcion = 'PDF';
    this.archivoSHP.inServer = false;
    this.archivoSHP.descripcion = 'SHP';
    this.fileAsambleaComunal.inServer = false;
    this.fileAsambleaComunal.descripcion = 'PDF';
    this.fileAnexo3.inServer = false;
    this.fileAnexo3.descripcion = 'PDF';
    this.fileAnexo2.inServer = false;
    this.fileAnexo2.descripcion = 'PDF';
    // this.obtenerArchivos();
    this.listarAnexo2();
    this.listarAnexo3();
    // this.listarCensoForestalAnexo2();
    // this.listarCensoForestalAnexo3();
    this.obtenerArchivosCapas();
    this.obtenerArchivosZonas();

    this.listarAnexos1();
    this.listarAnexos2();
    this.listarAnexos3();
    this.listarAnexos4();
  }
  static get EXTENSIONSAUTHORIZATION() {
    return ['zip', 'application/x-zip-compressed'];
  }
  static get EXTENSIONSAUTHORIZATION2() {
    return ['.pdf', 'image/png', 'image/jpeg', 'image/jpeg', 'application/pdf'];
  }
  private initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '490px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    let btnView = document.createElement('button');
    btnView.classList.add('btn-primary2');
    btnView.setAttribute('title', 'Mostrar/Ocultar Exportar mapa');
    let i = document.createElement('span');
    i.classList.add('esri-icon-documentation');
    btnView.appendChild(i);
    view.ui.add(btnView, 'bottom-right');
    btnView.addEventListener('click', (e) => {
      let print = container.querySelector('.esri-widget--panel');
      if (print.style.display === 'none') {
        print.style.display = 'block';
      } else {
        print.style.display = 'none';
      }
    });

    view.when(() => {
      let print = this.mapApi.print(view);
      view.ui.add(print, 'top-right');
    });
    this.view = view;
  }

  obtenerArchivosCapas() {
    let item: any = {
      idPlanManejo: this.idPlanManejo,
      codigoTipoPGMF: 'ZOIAREA',
    };
    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF)
      .subscribe((result: any) => {
        result.data.forEach((t: any) => {
          if (t.documento != null) {
            if (t.codigoTipoPGMF === 'ZOIAREA') {
              let blob = this.mapApi.readFileByte(t.documento);
              let config = {
                inServer: true,
              };
              this.mapApi.processFile(blob, config, this._id, this.view);
            }
          }
        });
      });
  }

  obtenerArchivosZonas() {
    let item: any = {
      idPlanManejo: this.idPlanManejo,
      codigoTipoPGMF: 'TZONDEMA',
    };
    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF)
      .subscribe((result: any) => {
        result.data.forEach((t: any) => {
          if (t.documento != null) {
            if (t.codigoTipoPGMF === 'TZONDEMA') {
              let blob = this.mapApi.readFileByte(t.documento);
              let config = {
                inServer: true,
              };
              this.mapApi.processFile(blob, config, this._id, this.view);
            }
          }
        });
      });
  }
  onChangeFileAnexo1(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === 'PDF') {
          include = TabAnexosComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else if (type === 'SHP') {
          include = TabAnexosComponent.EXTENSIONSAUTHORIZATION.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: 'toast',
            severity: 'warn',
            summary: '',
            detail: 'El Tipo de Documento no es V&aacute;lido',
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: 'toast',
            severity: 'warn',
            summary: '',
            detail: 'El Archivo no Debe Tener más de  3MB',
          });
        } else {
          if (type === 'PDF') {
            /* this.archivoMapPDF.nombreFile = e.target.files[0].name;
            this.archivoMapPDF.file = e.target.files[0];
            this.archivoMapPDF.descripcion = type;
            this.archivoMapPDF.inServer = false;
            this.filesAnexo1.push(this.archivoMapPDF); */
          } else {
            this.archivoSHP.nombreFile = e.target.files[0].name;
            this.archivoSHP.file = e.target.files[0];
            this.archivoSHP.descripcion = type;
            this.archivoSHP.inServer = false;
            this.filesAnexo1.push(this.archivoSHP);
          }
        }
      }
    }
  }

  /* onDeleteFile(data: FileModel) {
    this.confirmationService.confirm({
      message: "¿Esta seguro de querer eliminar?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      key: "deleteAnexo1",
      accept: () => {
        data.codigo = 0;
        data.nombreFile = "";
        data.inServer = false;
        data.file = null;
        this.eliminarArchivoDetalle(data.codigo);
      },
    });
  } */

  onDownloadFile(data: FileModel) {
    let file: any = data.file;
    DownloadFile(file, data.nombreFile, '');
  }

  /* guardarArchivoAnexo1() {
    this.filesAnexo1.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          typeCode: t.descripcion,
        };
        this.serviceArchivo
          .cargar(item.id, item.typeCode, t.file)
          .subscribe((result) => {
            let item2 = {
              codigoTipoPGMF: "DEMAANEXO1",
              idArchivo: result.data,
              idPlanManejo: this.idPlanManejo,
              idUsuarioRegistro: this.user.idUsuario,
            };
            this.servicePostulacionPFDM
              .registrarArchivoDetalle(item2)
              .subscribe();
          });
      }
    });
    setTimeout(() => {
      this.obtenerArchivos();
    }, 1000);
  } */

  /* obtenerArchivos() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoTipoPGMF: "DEMAANEXO1",
    };
    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF)
      .subscribe((result: any) => {
        result.data.forEach((t: any) => {
          if (t.tipoDocumento === "PDF") {
            this.archivoMapPDF.nombreFile = t.nombre;
            this.archivoMapPDF.codigo = t.idArchivo;
            this.archivoMapPDF.descripcion = t.tipoDocumento;
            this.archivoMapPDF.inServer = true;
            this.archivoMapPDF.file = t.documento;
          } else {
            this.archivoSHP.nombreFile = t.nombre;
            this.archivoSHP.codigo = t.idArchivo;
            this.archivoSHP.descripcion = t.tipoDocumento;
            this.archivoSHP.inServer = true;
            this.archivoSHP.file = t.documento;
          }
        });
      });
  } */

  /* eliminarArchivoDetalle(idArchivo: Number) {
    this.servicePostulacionPFDM
      .eliminarArchivoDetalle(idArchivo, this.user.idUsuario)
      .subscribe();
  } */

  listarCensoForestalAnexo2() {
    const params = new HttpParams()
      .set('idPlanManejo', String(this.idPlanManejo))
      .set('tipoEspecie', String(aprovDEMA.MADERABLE));

    this.anexosService
      .listarCensoForestalAnexo2(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.totalVolumne += element.volumen;
          this.lstAnexo2.push(element);
        });
      });
  }

  listarCensoForestalAnexo3() {
    const params = new HttpParams()
      .set('idPlanManejo', String(this.idPlanManejo))
      .set('tipoEspecie', String(aprovDEMA.NO_MADERABLE));

    this.anexosService
      .listarCensoForestalAnexo3(params)
      .subscribe((response: any) => {
        if (response.data) {
          response.data.forEach((element: any) => {
            this.lstAnexo3.push(element);
          });
        }
      });
  }

  onFileChange(e: any, typeAnexo: string) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === 'PDF') {
          include = TabAnexosComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'el tipo de documento no es válido',
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El archivo no debe tener más de  3MB',
          });
        } else {
          if (type === 'PDF' && typeAnexo == 'Anexo4') {
            this.fileAsambleaComunal.nombreFile = e.target.files[0].name;
            this.fileAsambleaComunal.file = e.target.files[0];
            this.fileAsambleaComunal.descripcion = type;
            this.fileAsambleaComunal.inServer = false;
            this.filesAsamblea.push(this.fileAsambleaComunal);
            this.verEnviar4 = true;
          } else if (type === 'PDF' && typeAnexo == 'Anexo3') {
            this.fileAnexo3.nombreFile = e.target.files[0].name;
            this.fileAnexo3.file = e.target.files[0];
            this.fileAnexo3.descripcion = type;
            this.fileAnexo3.inServer = false;
            this.verEnviar3 = true;
            this.filesAnexo3.push(this.fileAnexo3);
          } else if (type === 'PDF' && typeAnexo == 'Anexo2') {
            this.fileAnexo2.nombreFile = e.target.files[0].name;
            this.fileAnexo2.file = e.target.files[0];
            this.fileAnexo2.descripcion = type;
            this.fileAnexo2.inServer = false;
            this.verEnviar2 = true;
            this.filesAnexo2.push(this.fileAnexo2);
          } else if (type === 'PDF' && typeAnexo == 'Anexo1') {
            this.archivoMapPDF.nombreFile = e.target.files[0].name;
            this.archivoMapPDF.file = e.target.files[0];
            this.archivoMapPDF.descripcion = type;
            this.archivoMapPDF.inServer = false;
            this.verEnviar1 = true;
            this.filesAnexo1.push(this.archivoMapPDF);
          }
        }
      }
    }
  }

  guardarAnexo1() {
    this.filesAnexo1.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: 1,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            if (result.success == true) {
              this.toast.ok('Se cargó el Anexo 1 correctamente.');
              this.registrarArchivo(result.data);
              this.listarAnexos1();
            } else {
              this.toast.error('Ocurrió un error al realizar la operación.');
            }
          });
      }
    });
  }

  listarAnexos1() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 1,
      codigoProceso: 'DEMA',
    };
    
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarAnexo1 = true;
        this.eliminarAnexo1 = false;
        result.data.forEach((element: any) => {
          this.archivoMapPDF.nombreFile = element.nombreArchivo;
          this.idAnexo1 = element.idArchivo;
          this.archivoMapPDF.inServer=true;
        });
      } else {
        this.eliminarAnexo2 = true;
        this.cargarAnexo2 = false;
      }
    });
  }

  eliminarA1() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idAnexo1))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.cargarAnexo1 = false;
        this.eliminarAnexo1 = true;
        this.archivoMapPDF.nombreFile = '';
        this.toast.ok('Se Eliminó el Anexo 1 correctamente.');
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }

  guardarArchivoAsambleaComunal() {
    this.filesAsamblea.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: 4,
        };
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .subscribe((result: any) => {
            if (result.success == true) {
              this.toast.ok('Se cargó el Anexo 4 correctamente.');
              this.registrarArchivo(result.data);
            } else {
              this.toast.error('Ocurrió un error al realizar la operación');
            }
          });
      }
    });
  }

  guardarAnexo2() {
    this.filesAnexo2.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: 2,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            if (result.success == true) {
              this.toast.ok('Se cargó el Anexo 2 correctamente.');
              this.registrarArchivo(result.data);
            } else {
              this.toast.error('Ocurrió un error al realizar la operación');
            }
          });
      }
    });
  }

  guardarAnexo3() {
    this.filesAnexo3.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: 3,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            if (result.success == true) {
              this.toast.ok('Se cargó el Anexo 3 correctamente.');
              this.registrarArchivo(result.data);
            } else {
              this.toast.error('Ocurrió un error al realizar la operación');
            }
          });
      }
    });
  }

  registrarArchivo(id: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: 'DEMA',
      descripcion: '',
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: '',
    };
    this.anexosService.registrarArchivo(params).subscribe();
  }

  listarAnexos2() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 2,
      codigoProceso: 'DEMA',
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarAnexo2 = true;
        this.eliminarAnexo2 = false;
        result.data.forEach((element: any) => {
          this.fileAnexo2.nombreFile = element.nombreArchivo;
          this.idAnexo2 = element.idArchivo;
        });
      } else {
        this.eliminarAnexo2 = true;
        this.cargarAnexo2 = false;
      }
    });
  }

  listarAnexos3() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 3,
      codigoProceso: 'DEMA',
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarAnexo3 = true;
        this.eliminarAnexo3 = false;
        result.data.forEach((element: any) => {
          this.fileAnexo3.nombreFile = element.nombreArchivo;
          this.idAnexo3 = element.idArchivo;
        });
      } else {
        this.eliminarAnexo3 = true;
        this.cargarAnexo3 = false;
      }
    });
  }

  listarAnexos4() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 4,
      codigoProceso: 'DEMA',
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarAnexo4 = true;
        this.eliminarAnexo4 = false;
        result.data.forEach((element: any) => {
          this.fileAsambleaComunal.nombreFile = element.nombreArchivo;
          this.idAnexo4 = element.idArchivo;
        });
      } else {
        this.eliminarAnexo4 = true;
        this.cargarAnexo4 = false;
      }
    });
  }

  eliminarArchivoLimites() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idAnexo4))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok('Se eliminó Acta Asamblea Comunal correctamente.');
        this.cargarAnexo4 = false;
        this.eliminarAnexo4 = true;
        this.fileAsambleaComunal.nombreFile = '';
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }

  eliminarA2() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idAnexo2))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.cargarAnexo2 = false;
        this.eliminarAnexo2 = true;
        this.fileAnexo2.nombreFile = '';
        this.toast.ok('Se Eliminó el Anexo 2 correctamente.');
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }

  eliminarA3() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idAnexo3))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.cargarAnexo3 = false;
        this.eliminarAnexo3 = true;
        this.fileAnexo3.nombreFile = '';
        this.toast.ok('Se Eliminó el Anexo 2 correctamente.');
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }

  onDownloadAnexo(data: FileModel) {
    if (data.file != undefined) {
      let file: any = data.file;
      DownloadFile(file, data.nombreFile, '');
    }
  }

  generarAnexo2() {
    var params = {
      codigoProceso: 'DEMA',
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 2,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .generarAnexo2Dema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response) => descargarArchivo(response));
  }

  generarAnexo3() {
    var params = {
      codigoProceso: 'DEMA',
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 3,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .generarAnexo3Dema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response) => descargarArchivo(response));
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet : this.codigoProceso,
      //codigoEvaluacionDetSub : this.codigoTab
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion_1 = Object.assign(
                this.evaluacion_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1
                )
              );
            }
          } else {
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion() {
    this.router.navigateByUrl(
      '/planificacion/evaluacion/requisitos-previos/' +
        this.idPlanManejo +
        '/' +
        this.codigoProceso
    );
  }

  listarAnexo2() {
    this.listAnexo2Maderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
      tipoProceso: 1,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let listMap: any = [];

          response.data.forEach((element: any) => {
            this.totalVolumen += Number(element.volumen);
            listMap.push(element);
          });

          this.listAnexo2Maderables = listMap.groupBy(
            (item: any) => item.numeroFaja
          );
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  listarAnexo3() {
    this.listAnexo3NoMaderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
      tipoProceso: 2,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.listAnexo3NoMaderables.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }
}

export class Anexo2Model {
  constructor(data?: any) {}

  titulo: string = 'titutlo demo xxxxx';
  detalle: Anexo2Detalle[] = [
    new Anexo2Detalle(),
    new Anexo2Detalle(),
    new Anexo2Detalle(),
  ];
}

export class Anexo2Detalle {
  constructor(data?: any) {}

  codigo: string = '001';
  especie: string = 'demo demo';
  diametro: number = 0;
  altura: number = 0;
  volumen: number = 0;
  observaciones: string = `Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T`;
}
