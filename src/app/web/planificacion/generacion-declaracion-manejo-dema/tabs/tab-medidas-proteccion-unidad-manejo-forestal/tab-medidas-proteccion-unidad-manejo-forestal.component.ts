import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { concatMap, finalize, tap } from 'rxjs/operators';

import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService } from 'primeng/api';

import { LoadingComponent } from 'src/app/components/loading/loading.component';

import { AccionTipo, compareObjects, DEMATipo, handlerSaveResult, isNullOrEmpty, ToastService } from '@shared';
import { SistemaManejoForestalService, UsuarioService } from '@services';
import { SistemaManejoForestal, SistemaManejoForestalDetalle } from '@models';
import { ModalActividadesMpumf } from './modal-actividades-mpumf/modal-actividades-mpumf';
import {CodigosDEMA} from '../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab-medidas-proteccion-unidad-manejo-forestal',
  templateUrl: './tab-medidas-proteccion-unidad-manejo-forestal.component.html',
  styleUrls: ['./tab-medidas-proteccion-unidad-manejo-forestal.component.scss']
})
export class TabMedidasProteccionUnidadManejoForestalComponent implements OnInit {

  @Input() idPlanManejo!: number;@Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();

  AccionTipo = AccionTipo;
  medidaProteccion: SistemaManejoForestal = new SistemaManejoForestal({ codigoProceso: DEMATipo.MPUMF });
  actividades: SistemaManejoForestalDetalle[] = [];
  pendiente: boolean = false;


  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_6;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_6_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;
  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private confirm: ConfirmationService,
    private api: SistemaManejoForestalService,
    private toast: ToastService,
    private user: UsuarioService,private evaluacionService: EvaluacionService,private router: Router,
  ) { }

  ngOnInit(): void {

    if(this.isPerfilArffs)
      this.obtenerEvaluacion();

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.obtenerMPUMF(this.idPlanManejo, DEMATipo.MPUMF)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();

  }

  //#region REQUESTS BEGIN
  obtenerMPUMF(idPlanManejo: number, codigoProceso: DEMATipo) {
    return this.api.obtener(idPlanManejo, codigoProceso)
      .pipe(tap(res => {
        this.medidaProteccion = res?.data;
        this.actividades = res?.data?.detalle as SistemaManejoForestalDetalle[];
        this.setEnviar(this.actividades);
      }))
  }

  guardarSMF(body: SistemaManejoForestal) {
    return this.api.guardar(body)
      .pipe(tap({
        next: (res: any) => this.toast.ok(res?.message ? res.message : 'Se actualizó: Medidas de Protección de Unidad de Manejo Forestal'),
        error: (err: any) => {
          this.toast.error(err?.message ? err.message : 'Ocurrió un error al realizar la operación')
          console.error(err);
        }
    }));
  }

  eliminarSMFDetalle(idDetalle: number | null, idUsuario: number) {
    return this.api.eliminarDetalle(idDetalle, idUsuario)
      .pipe(tap({
        next: (res: any) => this.toast.ok(res?.message ? res.message : 'Se eliminó la Medida de Protección'),
        error: (err: any) => {
          this.toast.error(err?.message ? err.message : 'Ocurrió un error al realizar la operación')
          console.error(err);
        }
      }));
  }
  //#endregion REQUESTS END

  setEnviar(items: SistemaManejoForestalDetalle[]) {
    if (isNullOrEmpty(items)) return;
    items.forEach(x => x.enviar = isNullOrEmpty(x.idSistemaManejoForestalDetalle))
  }

  abrirModal(tipo: AccionTipo, data?: SistemaManejoForestalDetalle, index?: number) {
    const header = `${tipo} Medida de Protección`;
    data = data ? data : new SistemaManejoForestalDetalle({
      codigoProceso: DEMATipo.MPUMF,
      codigoTipoDetalle: DEMATipo.MPUMF
    });
    const ref = this.dialogService.open(ModalActividadesMpumf, { header, data, width: '50vw', closable: true, });

    ref.onClose.subscribe(detalle => {
      if (detalle) {
        if (!compareObjects(data, detalle)) {
          this.pendiente = true;
          detalle.enviar = true;
        }
        this.agregarDetalle(tipo, detalle, index);
      }
    })
  }

  agregarDetalle(accion: AccionTipo, detalle: SistemaManejoForestalDetalle, index?: number) {
    if (accion == AccionTipo.REGISTRAR) {

      let existe = this.actividades.find( e => e.descripcionSistema == detalle.descripcionSistema);

      if(existe != undefined){
        this.toast.warn('El registro ya existe.');
      }else{
        this.actividades.push(detalle);
      }

    } else {
      if (isNullOrEmpty(detalle.idSistemaManejoForestalDetalle) && index) {
        this.actividades[index] = detalle;
      } else {
        const posicion = this.actividades.findIndex(x =>
          (x.idSistemaManejoForestalDetalle == detalle.idSistemaManejoForestalDetalle));
        this.actividades[posicion] = detalle;
      }
    }
  }

  guardar() {
    this.medidaProteccion.codigoProceso = DEMATipo.MPUMF;
    this.medidaProteccion.idUsuarioRegistro = this.user.idUsuario;
    this.medidaProteccion.detalle = this.actividades.filter(x => x.enviar);

    this.medidaProteccion.detalle.forEach(x => {
      if (x.idSistemaManejoForestalDetalle == null) {
        x.idUsuarioRegistro = this.user.idUsuario;
      } else {
        x.idUsuarioModificacion = this.user.idUsuario;
      }
    })

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarSMF(this.medidaProteccion)
      .pipe(finalize(() => this.dialog.closeAll()),
        concatMap(() => this.obtenerMPUMF(this.idPlanManejo, DEMATipo.MPUMF))
      ).subscribe(() => this.pendiente = false);
  }

  eliminar(target: any, id: number, index: number) {
    this.confirm.confirm({
      target,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => this.eliminarRegistro(id, index)
    });
  }
  eliminarRegistro(id: number, index: number) {
    if (isNullOrEmpty(id)) {
      this.actividades.splice(index, 1);
      this.toast.ok('Se eliminó la Medida de Protección con éxito.');
      this.pendiente = false;
      for (const el of this.actividades) {
        if (el?.enviar) {
          this.pendiente = true;
          break;
        }
      }
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.eliminarSMFDetalle(id, this.user.idUsuario)
        .pipe(finalize(() => this.dialog.closeAll()),
          concatMap(() => this.obtenerMPUMF(this.idPlanManejo, DEMATipo.MPUMF))
        ).subscribe(() => this.pendiente = false);
    }
  }

  loadData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    setTimeout(() => this.dialog.closeAll(), 250);
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet : this.codigoProceso,
      //codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }else{
          this.evaluacion = {
            idPlanManejo: this.idPlanManejo,
            codigoEvaluacion: this.codigoProceso,
            codigoEvaluacionDet: this.codigoProceso,
            codigoEvaluacionDetSub: this.codigoTab,
            listarEvaluacionDetalle: [],
            idUsuarioRegistro: this.user.idUsuario,
          };
        }
      }
    })
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP629"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);

  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }
}
