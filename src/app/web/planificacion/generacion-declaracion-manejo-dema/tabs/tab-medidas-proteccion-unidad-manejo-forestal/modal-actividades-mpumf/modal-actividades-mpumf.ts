import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { removeTagsEditor, ToastService } from '@shared';
import { SistemaManejoForestalDetalle, KEY_DEMA_DETALLE } from '@models';


@Component({
  selector: 'modal-actividades-mpumf',
  templateUrl: './modal-actividades-mpumf.html',
  styleUrls: ['./modal-actividades-mpumf.scss']
})
export class ModalActividadesMpumf {
  f: FormGroup;
  detalle: SistemaManejoForestalDetalle = new SistemaManejoForestalDetalle();
  labelBtn: string = '';

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService,
  ) {
    this.f = this.initForm();
  }

  ngOnInit() {
    this.detalle = this.config.data as SistemaManejoForestalDetalle;
    const { actividades, descripcionSistema } = this.detalle;
    this.f.patchValue({ actividades, descripcionSistema });
    this.labelBtn = this.detalle.idSistemaManejoForestalDetalle == null ? 'Agregar' : 'Editar';
  }

  initForm() {
    return this.fb.group({
      actividades: [null, [Validators.required, Validators.minLength(1)]],
      descripcionSistema: [null, [Validators.required, Validators.minLength(1)]]
    });
  }

  guardar() {
    const keys = ['actividades', 'descripcionSistema'];
    const valid = this.toast.validAndShowOneToastError<SistemaManejoForestalDetalle>(keys, this.f.value, KEY_DEMA_DETALLE);
    if (this.f.invalid && !valid) return;

    let { actividades, descripcionSistema } = this.f.value;
    descripcionSistema = removeTagsEditor(descripcionSistema);
    this.detalle = { ...this.detalle, actividades, descripcionSistema };
    this.ref.close(this.detalle);
  }

  cancelar() {
    this.ref.close();
  }

}