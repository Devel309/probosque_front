import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RecursoForestalService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/recursos-forestales.service';
import {descargarArchivo, ToastService} from '@shared';
import {CodigosDEMA} from '../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {finalize, tap} from 'rxjs/operators';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {MatDialog} from '@angular/material/dialog';
import { environment } from '@env/environment';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import {ArchivoService, UsuarioService} from '@services';
import {Router} from '@angular/router';
import { UrlFormatos } from 'src/app/model/urlFormatos';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { InformacionGeneralDema } from '@models';

@Component({
  selector: 'app-tab-recursos-forestales-maderables',
  templateUrl: './tab-recursos-forestales-maderables.component.html',
  styleUrls: ['./tab-recursos-forestales-maderables.component.scss']
})
export class TabRecursosForestalesMaderablesComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;@Input() isPerfilArffs!: boolean;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();
  lstGrilla:any[] = [];
  lstGrillaResumen:any[] = [];



  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_3;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_3_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;

  varAssets = `${environment.varAssets}`;
  archivo: any;

  listAnexo2Maderables: any[] = [];
  listAnexo2ResumenMaderables: any[] = [];
  totalVolumen: number = 0;
  totalArbolA: number = 0;
  totalArbolS: number = 0;
  totalArbol: number = 0;
  totalRVolumen: number = 0;
  totalRArbolA: number = 0;
  totalRArbolS: number = 0;
  totalRArbol: number = 0;
  showWarn: boolean = true;
  UrlFormatos = UrlFormatos;
  nombreGenerado: string = "";
  plantillaPMFI: string = "";
  idCensoForestal: number = 0;
  form: InformacionGeneralDema = new InformacionGeneralDema();
  idInformacionGeneral: number = 0;
  nroArbolesMaderables!: number | null;
  nroArbolesMaderablesSemilleros!: number | null;
  volumenM3rMaderables: string | number | null = '';
  nroArbolesNoMaderables!: number | null;
  nroArbolesNoMaderablesSemilleros!: number | null;
  volumenM3rNoMaderables: string | number | null = '';
  superficieHaNoMaderables: string | number | null = '';
  tieneCenso: boolean = false;

  constructor(
    private recursoForestalService: RecursoForestalService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private usuarioService: UsuarioService,
    private censoForestalService: CensoForestalService,
    private anexosService: AnexosService,
    private router: Router,
    private archivoService: ArchivoService,
    private informacionGeneralService: InformacionGeneralService
  ) {
    this.archivo = {
      file: null,
    };
  }

  ngOnInit(): void {
    // this.consultarServicio();
    this.listarAnexo2();
    this.listarAnexo2Resumen();

    if(this.isPerfilArffs)
      this.obtenerEvaluacion();

  }

  consultarServicio():void{

    this.lstGrilla = [];
    this.lstGrillaResumen = [];

    const params = new HttpParams().set('idPlanManejo',this.idPlanManejo.toString()).set('tipoCenso','').set('tipoEspecie','1');
    const params2 = new HttpParams().set('idPlanManejo',this.idPlanManejo.toString()).set('tipoCenso','').set('tipoRecurso','1');

    //?idPlanManejo=2&tipoCenso=2&tipoEspecie=1

    this.recursoForestalService.listarRecurosForestales(params2).subscribe( (response: any) => {

      if(response.data.length>0){
          this.toast.ok("Se listó la información de Recursos Forestales Maderables");
          this.lstGrilla = response.data;

         /* response.data.forEach((element: any) => {
            this.lstGrilla.push(element);
          });*/
      }else{
        this.toast.warn("No existe información de Recursos Forestales Maderables");
      }

    })

    this.recursoForestalService.listarRecurosResumen(params).subscribe( (response: any) => {


      this.lstGrillaResumen = response.data;
      /*response.data.forEach((element: any) => {
        this.lstGrillaResumen.push(element);
      });*/
    })


    //
    //for (let index = 0; index < Math.floor(Math.random() * (100 - 1)) + 1; index++) {

     // this.lstGrilla.push({});

    //}
  }




  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet : this.codigoProceso,
      //codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }else{
          this.evaluacion = {
            idPlanManejo: this.idPlanManejo,
            codigoEvaluacion: this.codigoProceso,
            codigoEvaluacionDet: this.codigoProceso,
            codigoEvaluacionDetSub: this.codigoTab,
            listarEvaluacionDetalle: [],
            idUsuarioRegistro: this.usuarioService.idUsuario,
          };
        }
      }
    })
  }


  retornarFlujoEvaluacion(){
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

  btnDescargarFormato() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.nombreGenerado = UrlFormatos.CARGA_MASIVA_DEMA;
    this.archivoService
      .descargarPlantilla(this.nombreGenerado)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess == true) {
          this.toast.ok(res?.message);
          this.plantillaPMFI = res;
          descargarArchivo(this.plantillaPMFI);
        } else {
          this.toast.error(res?.message);
        }
      });
  }

/*   btnDescargarFormato() {
    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }

    window.location.href = urlExcel;
  } */

  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService
      .registrarCargaMasiva(this.archivo.file)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registró el archivo correctamente.\n');
          this.listarAnexo2();
          this.listarAnexo2Resumen();
          this.actualizarInformacionGeneral();
          this.archivo.file = '';
        } else {
          this.toast.error('Error al procesar archivo, el archivo seleccionado no cumple con el formato de carga masiva.');
        }
      });
  }

  listarAnexo2() {
    this.listAnexo2Maderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
      tipoProceso: 1
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.totalVolumen += Number(element.volumen);
            this.totalArbolA += Number(element.nArbolesAprovechables);
            this.totalArbolS += Number(element.nArbolesSemilleros);
            this.totalArbol += Number(element.nTotalArboles);
            this.listAnexo2Maderables.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  listarAnexo2Resumen() {
    this.listAnexo2ResumenMaderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
      tipoProceso: 3
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.totalRVolumen += Number(element.volumen);
            this.totalRArbolA += Number(element.nArbolesAprovechables);
            this.totalRArbolS += Number(element.nArbolesSemilleros);
            this.totalRArbol += Number(element.nTotalArboles);
            this.listAnexo2ResumenMaderables.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  actualizarInformacionGeneral() {
    forkJoin([
      this.listAnexo5(),
      this.listAnexo6(),
      this.listAnexo7(),
      this.listarInformacionGeneral()
    ]).pipe(finalize(() => {
      this.editarInformacionGeneral();
    })).subscribe();
  }

  listAnexo5() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'DEMA',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo5(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroAM = 0;
          let nroAMS = 0;
          let vM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroAM = nroAM + 1;
            }
            if (element.categoria == 'S') {
              nroAMS = nroAMS + 1;
            }
            if (element.volumen) {
              vM = vM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesMaderables = nroAM;
          this.nroArbolesMaderablesSemilleros = nroAMS;
          this.volumenM3rMaderables = vM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesMaderables = 0;
          this.nroArbolesMaderablesSemilleros = 0;
          this.volumenM3rMaderables = '0';
        }
      }));
  }

  listAnexo6() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'DEMA',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo6(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let sNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.areaBosque) {
              sNM = sNM + parseFloat(element.areaBosque);
            }
          });
          this.superficieHaNoMaderables = sNM;
          this.tieneCenso = true;
        } else {
          this.superficieHaNoMaderables = 0;
        }
      }));
  }

  listAnexo7() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'DEMA',
    };
    // this.dialog.open(LoadingComponent, { disableClose: true });
    return this.anexosService
      .Anexo7NoMaderable(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroANM = 0;
          let nroANMS = 0;
          let vNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroANM = nroANM + 1;
            }
            if (element.categoria == 'S') {
              nroANMS = nroANMS + 1;
            }
            if (element.volumen) {
              vNM = vNM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesNoMaderables = nroANM;
          this.nroArbolesNoMaderablesSemilleros = nroANMS;
          this.volumenM3rNoMaderables = vNM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesNoMaderables = 0;
          this.nroArbolesNoMaderablesSemilleros = 0;
          this.volumenM3rNoMaderables = '0';
        }
      }));
  }

  listarInformacionGeneral() {
    var params = {
      codigoProceso: this.codigoProceso,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    return this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.form = new InformacionGeneralDema(element);
            this.idInformacionGeneral = element.idInformacionGeneralDema;
          });
        }
      }));
  }

  editarInformacionGeneral() {
    if (this.tieneCenso) {
      this.form.nroArbolesMaderables = this.nroArbolesMaderables === 0 ? 0 : this.nroArbolesMaderables ? this.nroArbolesMaderables : null;
      this.form.nroArbolesMaderablesSemilleros = this.nroArbolesMaderablesSemilleros === 0 ? 0 : this.nroArbolesMaderablesSemilleros ? this.nroArbolesMaderablesSemilleros : null;
      this.form.volumenM3rMaderables = this.volumenM3rMaderables ? String(this.volumenM3rMaderables) : null;
      this.form.nroArbolesNoMaderables = this.nroArbolesNoMaderables === 0 ? 0 : this.nroArbolesNoMaderables ? this.nroArbolesNoMaderables : null;
      this.form.nroArbolesNoMaderablesSemilleros = this.nroArbolesNoMaderablesSemilleros === 0 ? 0 : this.nroArbolesNoMaderablesSemilleros ? this.nroArbolesNoMaderablesSemilleros : null;
      this.form.volumenM3rNoMaderables = this.volumenM3rNoMaderables ? String(this.volumenM3rNoMaderables) : null;
      this.form.superficieHaNoMaderables = this.superficieHaNoMaderables ? String(this.superficieHaNoMaderables) : null;

      var params = new InformacionGeneralDema(this.form);
      params.idPlanManejo = this.idPlanManejo;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = this.codigoProceso;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
          }
        });
    }
  }
}
