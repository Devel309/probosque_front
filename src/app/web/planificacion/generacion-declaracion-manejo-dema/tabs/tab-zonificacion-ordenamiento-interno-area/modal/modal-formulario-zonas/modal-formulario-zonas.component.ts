import { Component, OnInit } from '@angular/core';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-zonas',
  templateUrl: './modal-formulario-zonas.component.html',
  styleUrls: ['./modal-formulario-zonas.component.scss']
})
export class ModalFormularioZonasComponent implements OnInit {
  context: any = {}
  //cmbZonPadre: any = []

  constructor(
    public ref: DynamicDialogRef, 
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    // console.log(this.config);
    //this.cmbZonPadre = this.config.data.cmbZonPadre
    if(this.config.data.type == 'E'){
      this.context =  this.config.data.data
    }
  }

  agregar = () => {
    if (!this.context.nombre) {
      this.toast.warn("(*) Debe ingresar: Nombre de Zona.\n");
    } else this.ref.close(this.context);
  }

  cerrarModal() {
    this.ref.close()
  }
}
