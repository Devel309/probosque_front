import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/carga-envio-documentacion.service";
import { ArchivoService, PlanManejoService, UsuarioService } from "@services";
import { FileModel } from "src/app/model/util/File";
import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { CargaArchivosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/caragaArchivos.service";
import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { ArchivoDema } from "src/app/model/anexoDema";
import { DownloadFile } from "src/app/shared/util";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { NotificacionService } from "src/app/service/notificacion";

@Component({
  selector: "app-tab-carga-envio-documentos",
  templateUrl: "./tab-carga-envio-documentos.component.html",
  styleUrls: ["./tab-carga-envio-documentos.component.scss"],
})
export class TabCargaEnvioArchivosComponent implements OnInit {

  @Input() disabled!: boolean;@Input() isPerfilArffs!: boolean;
  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: "CPMINFG",
      label: "1. Información General",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMZONO",
      label: "2. Zonificación u Ordenamiento Interno del Área",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMRFM",
      label: "3. Recursos Forestales Maderables",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMRFN",
      label: "4. Recursos Forestales No Maderables",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMSMLS",
      label: "5. Sistema de Manejo y Labores Silviculturales",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMMPU",
      label: "6. Medidas de Protección de la Unidad de Manejo Forestal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMDAAE",
      label: "7. Actividades de Aprovechamiento y Equipos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMDAA2",
      label: "8. Impactos Ambientales Negativos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMCRA",
      label: "9. Cronograma de Actividades",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMA4",
      label: "10. Anexos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "CPMCARDOC",
      label: "11. Carga y Envío de Documentos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
  ];

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  @Input() idPlanManejo!: number;
  @Output() public regresar = new EventEmitter();

  files: FileModel[] = [];
  fileDEMA: FileModel = {} as FileModel;

  consolidadoDEMA: any = null;

  cargarDema: boolean = false;
  eliminarDema: boolean = true;
  idArchivoDema: number = 0;

  pendientes: any[] = [];
  verDema: boolean = false;
  verDescargaDema: boolean = true;
  isDemaGenerado: boolean = false;
  verAdjuntarTupa: boolean = false;

  anexo1!: ArchivoDema;
  anexo2!: ArchivoDema;
  anexo3!: ArchivoDema;
  anexo4!: ArchivoDema;

  anexos: ArchivoDema[] = [];

  verAnexo1: boolean = true;
  verAnexo2: boolean = true;
  verAnexo3: boolean = true;
  verAnexo4: boolean = true;
  verEnviar: boolean = false;


  longitudTUPA: number=0

  isSubmittingARFFS$ = this.ARFFSQuery.selectSubmitting();

  disabledCodigoEstadoPlanBorrador:boolean=true;
  disabledCodigoEstadoPlanPresentado:boolean=true;


  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private cargaArchivosService: CargaArchivosService,
    private dialog: MatDialog,
    private toast: ToastService,
    private anexosService: AnexosService,
    private planManejoService: PlanManejoService,
    private evaluacionService: EvaluacionService,
    private ARFFSStore: ButtonsCreateStore,
    private ARFFSQuery: ButtonsCreateQuery,
    private informacionGeneralService: InformacionGeneralService,
    private notificacionService: NotificacionService
  ) {}

  ngOnInit(): void {
    this.fileDEMA.inServer = false;
    this.fileDEMA.descripcion = "PDF";
    this.listarEstados();
    this.listarAnexos1();
    this.listarAnexos2();
    this.listarAnexos3();
    this.listarAnexos4();
    this.obtenerEstadoPlan();
  }


  obtenerEstadoPlan(){
    const body={
      idPlanManejo:this.idPlanManejo
  }
    this.informacionGeneralService.listarInfGeneralResumido(body).subscribe((_res:any)=>{

      if(_res.data.length>0){
        const estado= _res.data[0];
        if(estado.codigoEstado == 'EPLMPRES' || estado.codigoEstado == 'EMDOBS' || estado.codigoEstado == 'EMDBOR'){
          this.disabledCodigoEstadoPlanPresentado=false;
          this.disabledCodigoEstadoPlanBorrador= true;
          this.disabled = true;
        }
        if(estado.codigoEstado == 'EPLMBOR'){
          this.disabledCodigoEstadoPlanPresentado=true;
          this.disabledCodigoEstadoPlanBorrador= false;
          this.disabled = false;
        }
      }
    })
  }

  listarEstados() {
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: "DEMA",
      idPlanManejoEstado: null,
    };

    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == "CPMPEND") {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == "CPMPEND") {
          //  this.isTodosPlanesTerminado = false;
            this.listProcesos[objIndex].estado = "Pendiente";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == "CPMTERM") {
            this.listProcesos[objIndex].estado = "Terminado";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });
        this.pendiente();
      });
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verDema = true;
      this.isTodosPlanesTerminado = true;
    } else {
      this.verDema = false;
      this.isTodosPlanesTerminado = false;
    }
  }

  isTodosPlanesTerminado : boolean = true;

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }

    if (
      this.listProcesos.length ===
      this.listProcesos.filter((data) => data.marcar === true).length
    ) {
      this.verDema = false;
      this.isTodosPlanesTerminado = true;
    }else{
      this.isTodosPlanesTerminado = false;
    }

  };

  guardarEstados() {
    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: "DEMA",
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        observacion: "",
      };
      listaEstadosPlanManejo.push(obje);
    });

    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == "CPMPEND") {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();

          this.messageService.add({
            key: "tl",
            severity: "success",
            summary: "",
            detail:
              "Se registró los estados de la Declaración del Plan de Manejo correctamente.",
          });
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: response.message,
          });
        }
      });
  }

  regresarTab() {
    this.regresar.emit();
  }


  eliminarArchivoDema() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivoDema))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el DEMA firmado correctamente");
        this.cargarDema = false;
        this.eliminarDema = true;
        this.fileDEMA.nombreFile = "";
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  generarDema() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaArchivosService
      .consolidadoDEMA(this.idPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.consolidadoDEMA = res;
          this.verDescargaDema = false;
          this.isDemaGenerado = false;
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  descargarDEMA() {
    if (isNullOrEmpty(this.consolidadoDEMA)) {
      this.toast.warn("Debe generar archivo consolidado");
      return;
    }
    descargarArchivo(this.consolidadoDEMA);
  }

  validar(){
    this.verAdjuntarTupa = false;
  }

  enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EMDPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstadoARFFS(params);
  }

  actualizarPlanManejoEstadoARFFS(param: any) {
    this.ARFFSStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok(response?.message);
          this.registrarNotificacion();
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarNotificacion() {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: 'DEMA',
      numDocgestion: this.idPlanManejo,
      mensaje: 'Se informa que se ha registrado el plan '
        + 'DEMA'
        + ' Nro. '
        + this.idPlanManejo
        + ' para evaluación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.user.idUsuario,
      //codigoPerfil: this.user.usuario.sirperfil,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 10,
      url: '/planificacion/evaluacion/bandeja-eval-pmfi-dema',
      idUsuarioRegistro: this.user.idUsuario
    }
    this.notificacionService
      .registrarNotificacion(params)
      .subscribe()
  }

  private actualizarPlanManejoEstado(param: any) {
    this.ARFFSStore.submit();
    // this.planManejoService.actualizarPlanManejoEstado(param).subscribe(
    //   (result: any) => {},
    //   (error: HttpErrorResponse) => {}
    // );

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok("Esta solicitud ha sido Formulada.")
          // this.toast.ok(response?.message);
          this.verEnviar = true;
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }

  listarAnexos1() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 1,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.verAnexo1 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo1 = element;
        });
      }
    });
  }

  listarAnexos2() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 2,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.verAnexo2 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo2 = element;
        });
      }
    });
  }

  listarAnexos3() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 3,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.verAnexo3 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo3 = element;
        });
      }
    });
  }

  listarAnexos4() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 4,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.verAnexo4 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo4 = element;
        });
      }
    });
  }
  descargaAnexo1() {
    DownloadFile(
      this.anexo1.documento,
      this.anexo1.nombreArchivo,
      "application/octet-stream"
    );
  }

  descargaAnexo2() {
    DownloadFile(
      this.anexo2.documento,
      this.anexo2.nombreArchivo,
      "application/octet-stream"
    );
  }

  descargaAnexo3() {
    DownloadFile(
      this.anexo3.documento,
      this.anexo3.nombreArchivo,
      "application/octet-stream"
    );
  }
  descargaAnexo4() {
    DownloadFile(
      this.anexo4.documento,
      this.anexo4.nombreArchivo,
      "application/octet-stream"
    );
  }

  guardarPlanFormulado() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }
  registrarArchivoId(event: any,){
    console.log("event", event);

  }
}
