import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-tipo-actividades",
  templateUrl: "./modal-tipo-actividades.component.html",
})
export class ModalTipoActividadesComponent implements OnInit {
  actividad: string = "";

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  agregar = () => {
    if (this.actividad === "") {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        summary: "Alerta",
        detail: "Tipo de Actividad, es obligatorio",
      });
    } else {
      this.ref.close(this.actividad);
    }
  };

  cerrarModal() {
    this.ref.close();
  }
}
