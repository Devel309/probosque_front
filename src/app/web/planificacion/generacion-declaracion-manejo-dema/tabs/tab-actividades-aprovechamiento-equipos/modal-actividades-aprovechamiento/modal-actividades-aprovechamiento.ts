import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { removeTagsEditor, ToastService } from '@shared';
import { SistemaManejoForestalDetalle, KEY_DEMA_DETALLE, ParametroValor } from '@models';


@Component({
  selector: 'modal-actividades-aprovechamiento',
  templateUrl: './modal-actividades-aprovechamiento.html',
  styleUrls: ['./modal-actividades-aprovechamiento.scss']
})
export class ModalActividadesAprovechamiento {
  f: FormGroup;
  detalle: SistemaManejoForestalDetalle = new SistemaManejoForestalDetalle();
  labelBtn: string = '';
  tipos: ParametroValor[] = []

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService,
  ) {
    this.f = this.initForm();
  }

  ngOnInit() {
    this.detalle = this.config.data?.item as SistemaManejoForestalDetalle;
    this.tipos = this.config.data?.tipos as ParametroValor[];
    const {
      codigoTipoDetalle,
      actividades,
      descripcionSistema,
      maquinariasInsumos,
      personalRequerido,
      observacion
    } = this.detalle;
    this.f.patchValue({
      codigoTipoDetalle,
      actividades,
      descripcionSistema,
      maquinariasInsumos,
      personalRequerido,
      observacion
    });
    this.labelBtn = this.detalle.idSistemaManejoForestalDetalle == null ? 'Agregar' : 'Editar';
  }

  initForm() {
    return this.fb.group({
      codigoTipoDetalle: [null, [Validators.required, Validators.minLength(1)]],
      actividades: [null, [Validators.required, Validators.minLength(1)]],
      descripcionSistema: [null, [Validators.required, Validators.minLength(1)]],
      maquinariasInsumos: [null, [Validators.required, Validators.minLength(1)]],
      personalRequerido: [null, [Validators.required, Validators.minLength(1)]],
      observacion: [null, [Validators.required, Validators.minLength(1)]]
    });
  }

  guardar() {
    const keys = Object.keys(this.f.value);
    const valid = this.toast.validAndShowError<SistemaManejoForestalDetalle>(keys, this.f.value, KEY_DEMA_DETALLE);
    if (this.f.invalid && !valid) return;

    this.detalle = { ...this.detalle, ... this.f.value };
    this.ref.close(this.detalle);
  }

  cancelar() {
    this.ref.close();
  }

}