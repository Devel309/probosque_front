import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ImpactosAmbientalesNegativosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/impactos-ambientales-negativos.service';

@Component({
  selector: 'app-modal-formulario-impactos-negativos',
  templateUrl: './modal-formulario-impactos-negativos.component.html',
  styleUrls: ['./modal-formulario-impactos-negativos.component.scss'],
})
export class ModalFormularioImpactosNegativosComponent implements OnInit {
  impactoAmbiental: ImpactoAmbiental = {} as ImpactoAmbiental;

  medida: string = '';

  arrayDetalle: ImpactoAmbientalDetalle[] = [];
  arrayEliminaDetalles: any[] = [];
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private user: UsuarioService,
    private impactosAmbientalesNegativosService: ImpactosAmbientalesNegativosService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    if (this.config.data.type == 'E') {
      this.impactoAmbiental = new ImpactoAmbiental(this.config.data.data);
      this.impactoAmbiental.idImpactoambiental =
        this.config.data.data.idImpactoambiental;
      this.impactoAmbiental.detalle = [];
      this.arrayDetalle = [...this.config.data.data.detalle];
    } else this.arrayDetalle = [];
  }

  agregar() {
    const obj = new ImpactoAmbiental(this.impactoAmbiental);
    obj.detalle = this.arrayDetalle;

    if(this.arrayEliminaDetalles.length>0){
      this.eliminaDetalles();
    }
    this.ref.close(obj);
  }

  agregarMedida() {
    if (this.medida != '') {
      const obj = new ImpactoAmbientalDetalle();
      obj.idUsuarioModificacion = this.user.idUsuario;
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.medidasprevencion = this.medida;
      this.arrayDetalle.push(obj);
    }
    this.medida = '';
  }

  eliminar(event: any, data: any, id: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (id != 0) {
          var params = {
            idImpactoambientaldetalle: id,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.arrayEliminaDetalles.push(params);

          this.arrayDetalle = this.arrayDetalle.filter(
            (x: any) => x.medidasprevencion != data
          );
        } else {
          this.arrayDetalle = this.arrayDetalle.filter(
            (x: any) => x.medidasprevencion != data
          );
        }
      },
      reject: () => {},
    });
  }

  eliminaDetalles() {
    this.arrayEliminaDetalles.map((resp: any) => {
      this.impactosAmbientalesNegativosService
        .eliminarImpactoAmbientalDetalle(resp)
        .subscribe();
    });
  }

  cerrarModal() {
    this.ref.close();
  }
}

export class ImpactoAmbiental {
  idUsuarioRegistro?: any;
  idUsuarioModificacion?: any;
  idImpactoambiental: number = 0;
  idPlanManejo: number = 0;
  actividad: string = '';
  descripcion: string = '';
  detalle: ImpactoAmbientalDetalle[] = [];

  constructor(data?: any) {
    if (data) {
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion
        ? data.idUsuarioModificacion
        : 0;
      this.idImpactoambiental = data.idImpactoambiental
        ? data.idImpactoambiental
        : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.actividad = data.actividad ? data.actividad : '';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.detalle = data.detalle ? data.detalle : [];
    }
  }
}

export class ImpactoAmbientalDetalle {
  idUsuarioRegistro?: any = 0;
  idUsuarioModificacion?: any = 0;
  idImpactoambientaldetalle: number = 0;
  idImpactoambiental: number = 0;
  medidasprevencion: string = '';

  constructor(data?: any) {
    if (data) {
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion
        ? data.idUsuarioModificacion
        : 0;
      this.idImpactoambientaldetalle = data.idImpactoambientaldetalle
        ? data.idImpactoambientaldetalle
        : 0;
      this.idImpactoambiental = data.idImpactoambiental
        ? data.idImpactoambiental
        : 0;
      this.medidasprevencion = data.medidasprevencion
        ? data.medidasprevencion
        : '';
    }
  }
}
