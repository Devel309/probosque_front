import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { RecursoForestalService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/recursos-forestales.service";
import { HttpParams } from "@angular/common/http";
import { ToastService } from "@shared";
import { CodigosDEMA } from "../../../../../model/util/DEMA/DEMA";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { MatDialog } from "@angular/material/dialog";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { UsuarioService } from "@services";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-recursos-forestales-no-maderables",
  templateUrl: "./tab-recursos-forestales-no-maderables.component.html",
  styleUrls: ["./tab-recursos-forestales-no-maderables.component.scss"],
})
export class TabRecursosForestalesNoMaderablesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  lstGrilla: any[] = [];
  lstGrillaResumen: any[] = [];

  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_4;
  codigoAcordeon1: string = CodigosDEMA.ACORDEON_4_1;

  evaluacion_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon1,
  });

  evaluacion: any;

  listAnexo2NoMaderables: any[] = [];
  listAnexo2ResumenNoMaderables: any[] = [];
  totalCantidad: number = 0;
  totalArbolA: number = 0;
  totalArbolS: number = 0;
  totalArbol: number = 0;
  totalRProducto: number = 0;
  totalRCantidad: number = 0;
  totalRArbolA: number = 0;
  totalRArbolS: number = 0;
  totalRArbol: number = 0;
  showWarn: boolean = true;

  constructor(
    private recursoForestalService: RecursoForestalService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private router: Router
  ) {}

  consultarServicio(): void {
    this.lstGrilla = [];
    this.lstGrillaResumen = [];

    const params = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("tipoCenso", "")
      .set("tipoEspecie", "2");
    const params2 = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("tipoCenso", "")
      .set("tipoRecurso", "2");

    //?idPlanManejo=2&tipoCenso=2&tipoEspecie=1

    this.recursoForestalService
      .listarRecurosForestales(params2)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          this.toast.ok(
            "Se listó la información de Recursos Forestales No Maderables"
          );
          this.lstGrilla = response.data;
        } else {
          this.toast.warn(
            "No existe información de Recursos Forestales No Maderables"
          );
        }
      });

    this.recursoForestalService
      .listarRecurosResumen(params)
      .subscribe((response: any) => {
        this.lstGrillaResumen = response.data;

        /*response.data.forEach((element: any) => {
        this.lstGrillaResumen.push(element);
      });*/
      });
  }

  ngOnInit(): void {
    if (this.isPerfilArffs) this.obtenerEvaluacion();

    // this.consultarServicio();
    this.listarAnexo2();
    this.listarAprovechamientoRecursoForestalNoMaderableResumen();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
      //codigoEvaluacionDet : this.codigoProceso,
      //codigoEvaluacionDetSub : this.codigoTab
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion_1 = Object.assign(
                this.evaluacion_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1
                )
              );
            }
          } else {
            this.evaluacion = {
              idPlanManejo: this.idPlanManejo,
              codigoEvaluacion: this.codigoProceso,
              codigoEvaluacionDet: this.codigoProceso,
              codigoEvaluacionDetSub: this.codigoTab,
              listarEvaluacionDetalle: [],
              idUsuarioRegistro: this.user.idUsuario,
            };
          }
        }
      });
  }

  retornarFlujoEvaluacion() {
    this.router.navigateByUrl(
      "/planificacion/evaluacion/requisitos-previos/" +
        this.idPlanManejo +
        "/" +
        this.codigoProceso
    );
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  listarAnexo2() {
    this.listAnexo2NoMaderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
      tipoProceso: 2,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.totalCantidad += Number(element.cantidadProducto);
            this.totalArbolA += Number(element.nArbolesAprovechables);
            this.totalArbolS += Number(element.nArbolesSemilleros);
            this.totalArbol += Number(element.nTotalArboles);
            this.listAnexo2NoMaderables.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  listarAprovechamientoRecursoForestalNoMaderableResumen() {
    this.listAnexo2ResumenNoMaderables = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
      tipoProceso: 4,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .AprovechamientoRecursoForestalNoMaderableResumen(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.listAnexo2ResumenNoMaderables.push(element);
            element.listaAnexo2Dto.forEach((item: any) => {
              this.totalRProducto += Number(item.producto);
              this.totalRCantidad += Number(item.cantidadProducto);
              this.totalRArbolA += Number(item.nArbolesAprovechables);
              this.totalRArbolS += Number(item.nArbolesSemilleros);
              this.totalRArbol += Number(item.nTotalArboles);
            });
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }
}
