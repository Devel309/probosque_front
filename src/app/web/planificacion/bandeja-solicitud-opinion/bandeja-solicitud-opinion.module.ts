import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaSolicitudOpinionPmfiDemaComponent } from './bandeja-solicitud-opinion.component';
import { BandejaSolicitudOpinionpmfiDemaPModule } from 'src/app/shared/components/bandeja-solicitud-opinion/bandeja-solicitud-opinion.module';



@NgModule({
  declarations: [
    BandejaSolicitudOpinionPmfiDemaComponent
    
  ],
  imports: [
    CommonModule,
    BandejaSolicitudOpinionpmfiDemaPModule
  ],
  exports:[BandejaSolicitudOpinionPmfiDemaComponent]
})
export class BandejaSolicitudOpinionModule { }
