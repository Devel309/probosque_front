import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ParametroValorService, UsuarioService } from "@services";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ListarSolicitudOpinionRequest } from "src/app/model/solicitud-opinion";
import { GenericoService } from "src/app/service/generico.service";
import { SolicitudOpinionService } from "src/app/service/solicitud-opinion.service";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { finalize } from "rxjs/operators";
import { LazyLoadEvent } from "primeng/api";
import { Page } from "@models";

@Component({
  selector: "app-bandeja-solicitud-opinion",
  templateUrl: "./bandeja-solicitud-opinion.component.html",
  styleUrls: ["./bandeja-solicitud-opinion.component.scss"],
})
export class BandejaSolicitudOpinionPmfiDemaComponent implements OnInit {
  solicitudes: any[] = [];

  totalRecords = 0;

  listarSolicitudOpinionRequest: ListarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();

  comboDocumentos: any[] = [];
  comboEstado: any[] = [];
  comboTipoDocumento: any[] = [];

  ref!: DynamicDialogRef;

  usuario!: UsuarioModel;
  perfil!: string;

  comboEntidad: any = [
    { valor1: "SERNANP", codigo: "SERNANP" },
    { valor1: "ANA", codigo: "ANA" },
    { valor1: "MINCUL", codigo: "MINCUL" },
  ];

  constructor(
    private solicitudOpinionServ: SolicitudOpinionService,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    public dialogService: DialogService,
    private usuarioServ: UsuarioService,
    private parametroValorService: ParametroValorService,
    private genericoService: GenericoService
  ) {}

  ngOnInit(): void {
    this.listarSolicitudOpinionRequest.siglaEntidad = this.usuarioServ.usuario.sirperfil;

    this.listarSolicitudOpinionRequest.idUsuarioRegistro = this.usuarioServ.usuario.idusuario;

    this.listarsolicitudes();
    this.listarComboDocumentos();
    this.listarComboEstado();
    this.listarTipoDocumento();
  }

  listarsolicitudes() {
    this.perfil = this.usuarioServ.usuario.sirperfil;
    if (
      this.perfil == "ANA" ||
      this.perfil == "MINCUL" ||
      this.perfil == "SERNANP"
    ) {
      this.listarSolicitudOpinionRequest.idUsuarioRegistro = null;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudOpinionServ
      .listarSolicitudOpinion(this.listarSolicitudOpinionRequest)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data) {
          this.solicitudes = [...result.data];
          this.totalRecords = result.totalRecord;
        }
      });
  }

  listarComboDocumentos() {
    var params = { prefijo: "TDOCGE" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        
        this.comboDocumentos = [...response.data];
      });
  }

  listarTipoDocumento() {
    let parametro: any = {};
    parametro.idTipoParametro = 77;
    this.genericoService
      .listarPorFiltroParametro(parametro)
      .subscribe((result: any) => {
        this.comboTipoDocumento = result.data;
      });
  }

  listarComboEstado() {
    var params = { idTipoParametro: 82 };

    this.genericoServ
      .listarPorFiltroParametro(params)
      .subscribe((result: any) => {
        this.comboEstado = result.data;
      });
  }

  buscar() {
    this.listarsolicitudes();
  }

  limpiar() {
    this.listarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();
    this.listarSolicitudOpinionRequest.siglaEntidad = this.usuarioServ.usuario.sirperfil;
    this.listarsolicitudes();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarSolicitudOpinionRequest.pageSize = page.pageSize;
    this.listarSolicitudOpinionRequest.pageNum = page.pageNumber;
    this.listarsolicitudes();
  }
}
