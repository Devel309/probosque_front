import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { SolicitudAccesoService } from 'src/app/service/solicitudAcceso.service';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolicitudService } from '../../../service/solicitud.service';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-solicitud-permiso-forestal',
  templateUrl: './solicitud-permiso-forestal.component.html',
  styleUrls: ['./solicitud-permiso-forestal.component.scss'],
})
export class SolicitudPermisoForestalComponent implements OnInit {
  solicitudAcceso = {} as SolicitudAccesoModel;

  //Parametro
  lstEstado: any[] = [];
  lstTipoPersona: any[] = [];
  lstTipoActor: any[] = [];
  lstTipoCNCC: any[] = [];
  //bandeja
  lstSolicituAcceso: any[] = [];
  totalRegistrosSA: number = 0;
  visibleTipoCnnc = false;
  objbuscar = {
    dato: '',
  };
  totalRecords: number = 0;
  idSolicitudAcceso: number = 0;

  estadosolicitudes: SelectItem[];

  constructor(
    private serv: ParametroValorService,
    private servSA: SolicitudAccesoService,
    private serPerFores: PermisoForestalService,
    private servPf: PermisoForestalService,
    private serSol: SolicitudService,
    private router: Router
  ) {
    this.estadosolicitudes = [
      { label: '-- Seleccione --', value: null },
      { label: 'Fundado', value: true },
      { label: 'Infundado', value: false },
    ];
  }

  //Init
  ngOnInit(): void {
    this.listarEstado();
    this.obtenerSolicitudAccesoPorUsuario();
    //this.listarTipoPersona();
    //this.listarTipoActor();
    //this.listarTipoCNCC();
    //Bandeja Solicitud
  }

  loadData() {
    this.listarSolicitudAcceso();
  }

  /******************************************************/
  /*Métodos**********************************************/

  /******************************************************/
  listarEstado() {
    var params = { prefijo: 'ESAC' };
    //acá debe obtener de tabla estados
    this.serPerFores
      .ListaComboSolicitudEstado(params)
      .subscribe((result: any) => {
        this.lstEstado = result.data;
        this.solicitudAcceso.idestadoSolicitud = 0;
        this.listarSolicitudAcceso();
      });
  }

  listarTipoPersona() {
    var params = { prefijo: 'TPER' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoPersona = result.data;
    });
  }

  listarTipoActor() {
    var params = { prefijo: 'TACT' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
     
      this.lstTipoActor = result.data;
      this.lstTipoActor = this.lstTipoActor.slice(0, 2);
    });
  }

  listarTipoCNCC() {
    var params = { prefijo: 'TCNC' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoCNCC = result.data;
    });
  }

  listarSolicitudAcceso() {
    this.lstSolicituAcceso.push(
      {
        idSolicitud: 1,
        estadoSolicitudEntity: {
          tx_NOMBRE: 'aa',
        },
        fechaRegistro: new Date(),
        persona: {
          tipoPersona: '1',
          nombres: 'a',
          apellidoPaterno: 'aa',
          apellidoMaterno: 'aaa',
        },
        razonSocialEmpresa: 'razonSocialEmpresa',
        tipoActor: 'tipoActor',
        tipoCncc: 'tipoCncc',
        evaluar: 'Evaluar',
        idEstado: 1,
      },
      {
        idSolicitud: 2,
        estadoSolicitudEntity: {
          tx_NOMBRE: 'b',
        },
        fechaRegistro: new Date(),
        persona: {
          tipoPersona: '2',
          nombres: 'bb',
          apellidoPaterno: 'bbb',
          apellidoMaterno: 'bbbb',
        },
        razonSocialEmpresa: 'razonSocialEmpresa',
        tipoActor: 'tipoActor',
        tipoCncc: 'tipoCncc',
        evaluar: 'Observacion',
        idEstado: 2,
      },
      {
        idSolicitud: 3,
        estadoSolicitudEntity: {
          tx_NOMBRE: 'c',
        },
        fechaRegistro: new Date(),
        persona: {
          tipoPersona: '3',
          nombres: 'cc',
          apellidoPaterno: 'ccc',
          apellidoMaterno: 'cccc',
        },
        razonSocialEmpresa: 'razonSocialEmpresa',
        tipoActor: 'tipoActor',
        tipoCncc: 'tipoCncc',
        evaluar: 'Finalizado',
        idEstado: 3,
      }
    );
    let usuario = JSON.parse(localStorage.getItem('usuario') as any);
    this.solicitudAcceso.codigoUsuario = usuario.codigoUsuario;
    let dato = {
      idUsuarioRegistro: usuario.idusuario,
      estadoSolicitudEntity: {
        nu_ID_ESTADOSOLICITUD:
          this.solicitudAcceso.idestadoSolicitud == undefined
            ? 0
            : this.solicitudAcceso.idestadoSolicitud,
      },
      search: this.objbuscar.dato,
    };

    // this.serPerFores.ListarPermisosForestales(dato).subscribe((result: any) => {
    //   this.lstSolicituAcceso = result.data;
    // });
  }

  validar(value: any) {
    this.visibleTipoCnnc = value === 'TACTTCNC';
  }

  limpiar() {
    this.solicitudAcceso.codigoTipoPersona = '';
    this.solicitudAcceso.codigoTipoDocumento = '';
    this.solicitudAcceso.numeroDocumento = '';
    this.solicitudAcceso.nombres = '';
    this.solicitudAcceso.numeroRucEmpresa = '';
    this.solicitudAcceso.codigoTipoActor = '';
    this.solicitudAcceso.codigoTipoCncc = '';
    this.solicitudAcceso.estadoSolicitud = '';
    this.solicitudAcceso.razonSocialEmpresa = '';
    this.objbuscar.dato = '';
    this.solicitudAcceso.idestadoSolicitud = 0;
    this.listarSolicitudAcceso();
  }

  revisarSolicitud(data: any): void {
    this.router.navigate([
      '/planificacion/evaluar-permiso-forestal',
      'editar',
      this.idSolicitudAcceso,
      data.idSolicitud,
    ]);
  }

  nuevaSolicitud(): void {
    this.router.navigate([
      '/planificacion/evaluar-permiso-forestal',
      'nuevo',
      this.idSolicitudAcceso,
    ]);
  }

  obtenerSolicitudAccesoPorUsuario() {
    let usuario = JSON.parse(localStorage.getItem('usuario') as any);
    this.servPf
      .obtenerSolicitudAccesoPersonaPorUsuario(usuario.idusuario)
      .subscribe((result: any) => {
        
        this.idSolicitudAcceso = result.data.idSolicitudAcceso;
      });
  }
}
