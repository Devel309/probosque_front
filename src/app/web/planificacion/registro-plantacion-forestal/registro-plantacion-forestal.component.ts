import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { SolicitudPlantacionForestalModel, solPlantacionForestalAreaModel, solPlantacionForestalAreaPlantadaModel, solPlantacionForestalDetCoordModel, solPlantacionForestalDetModel, solPlantacionForestalModel, DetCoordModel, PersonaSolPlantacion, EstadoSolPlantacion } from 'src/app/model/Solicitud';
import { AreaPFModel, AreaPlantadaModel, InformacionSolicitantePFModel, PlantacionForestalModel, DetallePlantacion1Model, DetallePlantacion2Model, DetalleAreaPlantadaModel, DetalleDetallePlantacion1, DetalleDetallePlanteacion2model, DetalleVerticesModel } from 'src/app/model/util/dataDemoMPAFPP';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ValidarRequisitosModal } from '../evaluar-permiso-forestal-ccnn/modal/validar-requisitos-modal/validar-requisitos.modal';

@Component({
  selector: 'app-registro-plantacion-forestal',
  templateUrl: './registro-plantacion-forestal.component.html',
  styleUrls: ['./registro-plantacion-forestal.component.scss'],
  providers: [MessageService]
})
export class RegistroPlantacionForestalComponent implements OnInit {

  usuario = {} as UsuarioModel;
  solPermisoForestales = [];

  data: PlantacionForestalModel = new PlantacionForestalModel();
  _data :SolicitudPlantacionForestalModel =new  SolicitudPlantacionForestalModel();

  



  idSolicitante: number = 1;
  cmbSolicitante = [
    {
      text: "Titular de predio",
      value: 1
    },
    {
      text: "Titular de TH",
      value: 2
    },
    {
      text: "Titular de plantación Costa y Sierra",
      value: 3
    },
    {
      text: "ARFFS",
      value: 4

    },
    {
      text: "EVALUADOR DE ARFFS",
      value: 5

    },
    {
      text: "Titular de Plantacion",
      value: 1
    },
  ]

  ListaDepartamento = [];
  ListaDistritoInformacion = [];
  ListaProvinciaInformacion = [];

  ListaDistritoRepr= [];
  ListaProvinciaRepr = [];


  ListaDistritoArea = [];
  ListaProvinciaArea = [];

  id_solplantforest: any = null;
  idPersona: any = null;

  file: any = {};

  fileList: any[] = [
    {
      descripcion: "Certificado de registro de plantaciones.pdf",
      url: "fake",
      date: new Date(),
    }
  ];

  constructor(private router: Router,
    private servPf: PermisoForestalService,
    private messageService: MessageService,
    private core: CoreCentralService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
  ) {

    this._data.solPlantacionForestal = new solPlantacionForestalModel();
    this._data.solPlantacionForestal.persona = new PersonaSolPlantacion();
    this._data.solPlantacionForestal.estadoSolPlanta = new EstadoSolPlantacion();
    
    this._data.solPlantacionForestalArea = new solPlantacionForestalAreaModel();
  
   // this.data.area = new AreaPFModel();
    this.data.areaPlantada = new AreaPlantadaModel();
    this.data.detalleAreaPlanteada1 = new DetallePlantacion1Model();
    this.data.detalleAreaPlanteada2 = new DetallePlantacion2Model();
  }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    let id = this.route.snapshot.paramMap.get('id');
      // ;;
      /*
      35	000035	Titular de predio         ==1
      36	000036	Titular de TH             ==2
      37	000037	Titular de Costa y Sierra ==3
      38	000038	ARFFS                     ==4
      39	000039	Evaluador de ARFFS        ==5
      40	000040	Titular de Plantación     ==6
      */
let idperfil = this.usuario.perfiles[0].idPerfil;
if(idperfil==35)
    this.idSolicitante =1 ;
if(idperfil==36)
    this.idSolicitante =2 ;
if(idperfil==37)
    this.idSolicitante =3 ;
if(idperfil==38)
    this.idSolicitante =4 ;
if(idperfil==39)
    this.idSolicitante =5 ;
if(idperfil==40)
    this.idSolicitante =6 ;

    
   if (id) {
      this.id_solplantforest = id;
    } else {
      this.id_solplantforest = 0;    
    }


    //this.listarPorFiltroDepartamento();

    if (this.id_solplantforest == 0) {  //nuevo
      this.ObtenerPersona(null, this.usuario.idusuario, true); //solicictante    

    } else {                 //editar
      this.obtener(this.id_solplantforest);
    }

  }



  eliminarItemDetCoord(dato: any) {
    let dto = {
      "id_solplantforest": 0,
      "id_solplantforest_coord_det": 0,
      "idUsuarioElimina": 0,
    };
    this.servPf.EliminarSistemaPlantacionForestalDetCoord(dto).subscribe(
      (result: any) => {

        //console.log(result);
        if (result.isSuccess) {
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      }, (error: any) => {
        //console.log(error.message);
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }

  obtener(value: any) {
    let dto =    { "id_solplantforest": value };
    this.servPf.ObtenerPermisosForestales(dto).subscribe(
      (result: any) => {
        //console.log(result);
        if (result.isSuccess) {
  
          this._data.solPlantacionForestal = result.data[0];
          if( this._data.solPlantacionForestal.id_sol_persona<1){
              this.ObtenerPersona(this._data.solPlantacionForestal.id_sol_persona, null, true); 
          }
     
          //this.ObtenerPersona(this._data.solPlantacionForestal.id_sol_representante, null, false);
          this.listarInformacionUbicacion(1);
          this.listarInformacionUbicacion(2);
       
          this.obtenerArea(this._data.solPlantacionForestal.id_solplantforest);  
          this.listarAreaUbicacion();
          this.obtenerAreaPlantada(this._data.solPlantacionForestal.id_solplantforest);
          this.obtenerDetallePlantacionForestal(this._data.solPlantacionForestal.id_solplantforest);
          this.obtenerDetalleAreaCordenada(this._data.solPlantacionForestal.id_solplantforest);
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
         }
      }, (error: any) => {    
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }
  ObtenerPersona(persona: any, idUsuario: any, pbooelan: boolean) {
    let dto = { "idPersona": persona, "idUsuario": idUsuario };
    /**/
    this.servPf.listarPorFiltroPersona(dto).subscribe(
      (result: any) => {

        if (result.isSuccess) {
          if (pbooelan) {
            this._data.solPlantacionForestal.persona = {
              ...result.data[0],
              nombres: (result.data[0].apellidoPaterno + " " + result.data[0].apellidoMaterno + " " + result.data[0].nombres)
            }
            this.idPersona = result.data[0].idPersona;
           } else {
            this._data.solPlantacionForestal.persona.nombres = result.data[0].razon_social_empresa;
            this._data.solPlantacionForestal.persona.email_empresa = result.data[0].email_empresa;
            this._data.solPlantacionForestal.persona.direccion_empresa = result.data[0].direccion_empresa;
            this._data.solPlantacionForestal.persona.razon_social_empresa = result.data[0].numero_ruc_empresa;
          }
          this.listarInformacionUbicacion(1);
          //this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }
  guardar(): void {
    let datoStr: any = localStorage.getItem('plantacionesForestales');
    let datos: any[] = [];
    if (!datoStr) {
      datos = [];
    } else {
      datos = JSON.parse(datoStr);
    }
    datos.push(this.data);
    localStorage.setItem('plantacionesForestales', JSON.stringify(datos));

    this._data.solPlantacionForestal.id_solplantforest = this.id_solplantforest;
    if(this.idPersona!=null){
      this._data.solPlantacionForestal.id_sol_persona = this.idPersona;
    }

    this._data.solPlantacionForestal.idUsuarioRegistro = this.usuario.idusuario;
    this._data.solPlantacionForestal.estado = "A";

    if( this._data.solPlantacionForestal.persona.idTipoPersona==2
        &&( this._data.solPlantacionForestal.persona.ruc==null
        || this._data.solPlantacionForestal.persona.ruc =="")
      ){
      this.messageService.add({ severity: "warn", summary: "", detail: "Ingrese el Numero de R.U.C"});
      return;
    }


    this.servPf.RegistroSolicitudPermisosForestales(this._data.solPlantacionForestal).subscribe(
      (result: any) => {
        //;       
       
        if (result.isSuccess) {
          //capturar el result.codigo y enviarlo en cada metodo result.codigo => *id_solplantforest*
          this.registrarSolicitudPlantacacionArea(result.codigo);
          this.registrarSolicitudPlantacacionAreaPlant(result.codigo)
          this.registrarSolicitudPlantacacionDet(result.codigo)
          this.registrarSolicitudPlantacacionDetCoord(result.codigo);
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          //this.ListarCronogramaActividades(this.objbuscar);
          // this.router.navigateByUrl('/planificacion/bandeja-plantacion-forestal');
        } else {
          this.messageService.add({ severity: "warn", summary: "1. Informacón del Solicitante", detail: result.message });
        }

      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "1. Informacón del Solicitante", detail: error.message });
      });

  }


  registrarSolicitudPlantacacionArea(id_solplantforest: any) {
    
    // Area
    this._data.solPlantacionForestalArea.id_solplantforest = id_solplantforest;
    this._data.solPlantacionForestalArea.id_doc_autoriza_plantacion = 0;
    this._data.solPlantacionForestalArea.doc_autoriza_plantacion = "";
    this._data.solPlantacionForestalArea.num_cesion_agroforestal = (this.data.area.tipo == "P" ? this.data.area.tipoNumero : "");
    this._data.solPlantacionForestalArea.num_concesion_agroforestal = (this.data.area.tipo == "I" ? this.data.area.tipoNumero : "")
    this._data.solPlantacionForestalArea.idUsuarioRegistro = this.usuario.idusuario;

    this.servPf.registrarSolicitudPlantacacionArea(this._data.solPlantacionForestalArea).subscribe(
      (result: any) => {
       
        if (result.isSuccess) {
          
          // this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "2. Del Área", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "2. Del Área", detail: error.message });
      });


  }
  registrarSolicitudPlantacacionAreaPlant(id_solplantforest: any) {

    var ar_plan: solPlantacionForestalAreaPlantadaModel[] = [];
    var datav;
    this.data.areaPlantada.lstDetalle.forEach(element => {
      datav = {} as solPlantacionForestalAreaPlantadaModel;
      datav.id_solplantforest = id_solplantforest;
      datav.id_solplantforest_areaplantada = element.id_solplantforest_areaplantada;
      datav.area_total_plantacion = this.data.areaPlantada.areaTotal;
      datav.mes_anio_plantacion = new Date();
      datav.idUsuarioRegistro = this.usuario.idusuario;
      datav.id_sistemaplantacion = element.id_sistemaplantacion;
      datav.superficie_medida = element.superficie_cantidad;
      datav.superficie_cantidad = element.superficie_cantidad;
      datav.fines = element.fines;
      datav.especies_establecidas = element.especies_establecidas;
      datav.estado = "A";
      ar_plan.push(datav);
    });

  if(ar_plan.length!=0){
    this.servPf.registrarSolicitudPlantacacionAreaPlant(ar_plan).subscribe(
      (result: any) => {
       
        if (result.isSuccess) {
          
          // this.messageService.add({ severity: "success", summary: "", detail: result.message });ce.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "3. Informaciñon Área Plantada", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "3. Informaciñon Área Plantada", detail: error.message });
      });
  }


  }
  registrarSolicitudPlantacacionDet(id_solplantforest: any) {

    var ar_det: solPlantacionForestalDetModel[] = [];
    this.data.detalleAreaPlanteada1.lstDetalle.forEach(element => {
      let datav = {} as solPlantacionForestalDetModel;
      datav.id_solplantforest_det = element.id_solplantforest_det;
      datav.id_solplantforest = id_solplantforest;
      datav.idUsuarioRegistro = this.usuario.idusuario;
      datav.espcs_nom_comun = element.espcs_nom_comun;
      datav.espcs_nom_cientifico = element.espcs_nom_cientifico;
      datav.total_arbol_matas_espcs_existentes = element.produccion_estimada;
      datav.produccion_estimada = element.produccion_estimada;
      datav.coord_este = element.coord_este;
      datav.coord_norte = element.coord_este;
      datav.estado = "A";
      ar_det.push(datav);
    });

    if(ar_det.length!=0){
      this.servPf.registrarSolicitudPlantacacionDet(ar_det).subscribe(
        (result: any) => {
         
          if (result.isSuccess) {  
            
            // this.messageService.add({ severity: "success", summary: "", detail: result.message });e.add({ severity: "success", summary: "", detail: result.message });  
          } else {
            this.messageService.add({ severity: "warn", summary: "4. Detalle de Plantación", detail: result.message });
          }  
        }, (error: any) => {
          this.messageService.add({ severity: "warn", summary: "4. Detalle de Plantación", detail: error.message });
        });
    }

  }
  registrarSolicitudPlantacacionDetCoord(id_solplantforest: any) {

    let datav = {} as solPlantacionForestalDetCoordModel;
    let detalle = {} as DetCoordModel;

    var ar_coord: solPlantacionForestalDetCoordModel[] = [];
    this.data.detalleAreaPlanteada2.lstDetalle.forEach(element => {
      datav = {} as solPlantacionForestalDetCoordModel;

      datav.id_solplantforest = id_solplantforest;
      datav.id_solplantforest_coord_det = element.id_solplantforest_coord_det;
      datav.areabloque = element.areabloque
      datav.areabloque_unidad = element.areabloque_unidad
      datav.estado = "A"
      datav.idUsuarioRegistro = this.usuario.idusuario;
      datav.bloquesector = element.bloquesector;
      datav.detalle = [];

      element.detalle.forEach((element, i) => {
        detalle = {} as DetCoordModel;
        //detalle.id_bloquesector = element.id_bloquesector;
        detalle.id_solplantforest_coord_det = datav.id_solplantforest_coord_det;
        detalle.observaciones = element.observaciones;
        detalle.vertice = element.vertice;
        detalle.zonaeste = element.zonaeste;
        detalle.zonanorte = element.zonanorte;
        datav.detalle.push(detalle);
      })
      ar_coord.push(datav);
    });

    if(ar_coord.length!=0){
      this.servPf.registrarSolicitudPlantacacionDetCoord(ar_coord).subscribe(
        (result: any) => {  
         
          if (result.isSuccess) {  
            
          // this.messageService.add({ severity: "success", summary: "", detail: result.message });Service.add({ severity: "success", summary: "", detail: result.message });  
          } else {
            this.messageService.add({ severity: "warn", summary: "4. Detalle Plantación", detail: result.message });  
          }  
        }, (error: any) => {  
          this.messageService.add({ severity: "warn", summary: "4. Detalle Plantación", detail: error.message });
        });
  
    }
    
  }


  obtenerArea(id_solplantforest: number) {
    
    let params = {
      id_solplantforest
    }
    this.servPf.ObtenerPermisosForestalesArea(params).subscribe((result: any) => {
      this._data.solPlantacionForestalArea = result.data[0];
      this.listarPorFilroProvinciaArea();
      this.listarPorFilroDistritoArea();
      /*if (result.data) {
        this.data.areaPlantada.areaTotal = result.data[0].area_total_plantacion;
        this.data.areaPlantada.mesAnhio = result.data[0].mes_anio_plantacion;
        this.data.areaPlantada.lstDetalle = result.data;
      }*/
/**
 this.data.area.tipoNumero = (
            this._data.solPlantacionForestalArea.num_cesion_agroforestal == null ?
              this._data.solPlantacionForestalArea.num_concesion_agroforestal :
              this._data.solPlantacionForestalArea.num_cesion_agroforestal
          );
          this.data.area.tipo = (
            this._data.solPlantacionForestalArea.num_cesion_agroforestal == null ? "P" : "I"
          );
 */

    })
  }

  obtenerAreaPlantada(id_solplantforest: number) {
    let params = {
      id_solplantforest
    }
    this.servPf.obtenerSolicitudPlantacacionAreaPant(params).subscribe((result: any) => {
      if (result.data) {
        this.data.areaPlantada.areaTotal = result.data[0].area_total_plantacion;
        this.data.areaPlantada.mesAnhio = result.data[0].mes_anio_plantacion;
        this.data.areaPlantada.lstDetalle = result.data;
      }
    })
  }

  obtenerDetallePlantacionForestal(id_solplantforest: number) {
    let params = {
      id_solplantforest
    }

    this.servPf.obtenerSolicitudPlantacacionDet(params).subscribe((result: any) => {
      var datav;

      var ar_det: DetalleDetallePlantacion1[] = [];
      result.data.forEach((element: any) => {
        datav = {} as DetalleDetallePlantacion1;

        datav.id_solplantforest_det = element.id_solplantforest_det;
        datav.espcs_nom_comun = element.espcs_nom_comun;
        datav.espcs_nom_cientifico = element.espcs_nom_cientifico;
        datav.total_arbol_matas_espcs_existentes = element.total_arbol_matas_espcs_existentes;
        datav.produccion_estimada = element.produccion_estimada;
        datav.coord_este = element.coord_este;
        datav.coord_norte = element.coord_norte;
        ar_det.push(datav);

      });
      this.data.detalleAreaPlanteada1.lstDetalle = ar_det;

    })

  }

  obtenerDetalleAreaCordenada(id_solplantforest: number) {
    let params = {
      id_solplantforest
    }

    this.servPf.obtenerSolicitudPlantacacionDetCoord(params).subscribe((result: any) => {
      this._data.solPlantacionForestalDetCoord = result.data;

      var datav = {} as DetalleVerticesModel;
      var datac = {} as DetalleDetallePlanteacion2model;
      var ar_coord: DetalleDetallePlanteacion2model[] = [];


      this._data.solPlantacionForestalDetCoord.forEach(element => {
        datac = {} as DetalleDetallePlanteacion2model;

        datac.id_solplantforest_coord_det = element.id_solplantforest_coord_det;
        datac.areabloque = element.areabloque;
        datac.areabloque_unidad = element.areabloque_unidad;
        datac.bloquesector = element.bloquesector;
        datac.detalle = [];
        element.detalle.forEach(element => {
          datav = {} as DetalleVerticesModel;
          datav.id_bloquesector = element.id_bloquesector;
          datav.zonaeste = element.zonaeste;
          datav.zonanorte = element.zonanorte;
          datav.observaciones = element.observaciones;
          datav.vertice = element.vertice;
          datac.detalle.push(datav)
        })
        ar_coord.push(datac);
      })
      //console.log(ar_coord);

      this.data.detalleAreaPlanteada2.lstDetalle = ar_coord;


    })

  }

  listarPorFiltroDepartamento() {
    let depart = {};
    this.core.listarPorFiltroDepartamento(depart).subscribe(
      (result: any) => {
        if (result.success) {
          this.ListaDepartamento = result.data;
        }
      });
  }

  listarInformacionUbicacion(tipo:any) {
   // this.listarPorFiltroDepartamento();
    this.listarPorFilroProvincia(tipo);
    this.listarPorFilroDistrito(tipo);
  }

  listarPorFilroProvincia(tipo:any) {
    var provincia;
      if(tipo==1){
        provincia = {
            "idDepartamento": this._data.solPlantacionForestal.persona.id_departamento
          };
        }
      if(tipo==2){
          provincia = {
          "idDepartamento": this._data.solPlantacionForestal.persona.id_departamento_empresa
        };
      }
      
    if(
        this._data.solPlantacionForestal.persona.id_departamento > 0 
        || this._data.solPlantacionForestal.persona.id_departamento_empresa > 0)
        {
      this.core.listarPorFilroProvincia(provincia).subscribe(
        (result: any) => {
          if (result.isSuccess) {
            if(tipo==1){
              this.ListaProvinciaInformacion = result.data;
            }
            else if(tipo==2){
              this.ListaProvinciaRepr = result.data;
            }
          }
        });
    }
     
     
 
  }

  listarPorFilroDistrito(tipo:any) {
    let provincia;
    if(tipo==1){
      provincia = {
        "idProvincia": this._data.solPlantacionForestal.persona.id_provincia
      };
   }
    if(tipo==2){
        provincia = {
          "idProvincia": this._data.solPlantacionForestal.persona.id_provincia_empresa
        };
    }
    if( this._data.solPlantacionForestal.persona.id_departamento > 0 
      || this._data.solPlantacionForestal.persona.id_provincia_empresa > 0)
      {
        this.core.listarPorFilroDistrito(provincia).subscribe(
          (result: any) => {
            if (result.isSuccess) {
            

               if(tipo==1){
                this.ListaDistritoInformacion = result.data;
              }
              else if(tipo==2){
                this.ListaDistritoRepr = result.data;
              }
            }
            
          });
    }
  }


  listarAreaUbicacion() {
    this.listarPorFilroProvinciaArea();
    this.listarPorFilroDistritoArea();
  }

  listarPorFilroProvinciaArea() {
    let provincia = {
      "idDepartamento": this._data.solPlantacionForestalArea.id_departamento
    };
    this.core.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
        if (result.isSuccess) {
          this.ListaProvinciaArea = result.data;
        }
      });
  }

  listarPorFilroDistritoArea() {
    let provincia = {
      "idProvincia": this._data.solPlantacionForestalArea.id_provincia
    };
    this.core.listarPorFilroDistrito(provincia).subscribe(
      (result: any) => {
        if (result.isSuccess) {
          this.ListaDistritoArea = result.data;
        }
      });
  }
  
  generarRegistro(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de generar el registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.messageService.add({ severity: "success", summary: "", detail: "Registro generado con éxito" });

      },
      reject: () => {
        //reject action
      }
    });
  }

  enviarSerfor(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de Enviarlo SERFOR?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.messageService.add({ severity: "success", summary: "", detail: "Enviado realizado con éxito" });
      },
      reject: () => {
        //reject action
      }
    });
  }

  enviarSolicitante(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de Enviarlo al solicitante?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.messageService.add({ severity: "success", summary: "", detail: "Enviado realizado con éxito" });

      },
      reject: () => {
        //reject action
      }
    });
  }

  onFileChange(e: any) {
    this.file.url = URL.createObjectURL(e.target.files[0]);
    this.file.file = e.srcElement.files[0];
    this.file.descripcion = e.srcElement.files[0].name;
    e.preventDefault();
    e.stopPropagation();

  }

  verArchivo() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', this.file.url);
    link.setAttribute('download', this.file.descripcion);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  eliminarArchivo() {
    this.file = {};
  }

  notificar(detail: string) {
    this.messageService.add({ severity: 'success', detail });
  }

}

