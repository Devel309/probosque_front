import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DetalleAnexo, SolDetalleAnexo } from 'src/app/model/util/dataDemoMPAFPP';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';

@Component({
  selector: 'app-rpf-anexo',
  templateUrl: './rpf-anexo.component.html',
  styleUrls: ['./rpf-anexo.component.scss']
})
export class RpfAnexoComponent implements OnInit {

  @Input('SolDetalleAnexo') data: SolDetalleAnexo = new SolDetalleAnexo();
  @Input() usuario: any = {};
  @Input() id_solplantforest: any = null;

  uploadedFiles: any[] = [];
  file: any = {};

  accion: any = null;

  tipoDocumentoCombo: any[] = [
    { index:1,valor: "Mapa/Croquis de la ubicación" },
    { index:2,valor: "Copia simple del titulo de propiedad" },
    { index:3,valor: "Carta Poder" }
  ];

  verModalArchivos: boolean = false;
  tituloArchivo: string = '';

  constructor(private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private servPf: PermisoForestalService,) { }

  ngOnInit(): void {
   this.obtener(this.data.id_solplantforest);
  }

  openModal(data: any, tipo: string) {

    this.accion = tipo;
    if (tipo == "C") {
      this.file = {};

      this.tituloArchivo = "Nuevo Registro"
    } else if (tipo == "E") {
      this.file = data;
      this.tituloArchivo = "Editar Registro"
    }

    this.verModalArchivos = true;
  }



  obtener(data: any) { 
    let params={
      "idUsuarioRegistro": 0,
      "id_archivo_solplantacionforest": 0,
      "id_solplantforest": data.id_solplantforest,
      "id_tipodetalleanexo": 0,
      "id_tipodetalleseccion": 0,
    
      "pageNum": 1,
      "pageSize": 100

    };
    this.servPf.obtenerSolicitudPlantacacionAnexo(params).subscribe(
      (result : any)=>{
        if(result.success){

          this.messageService.add({severity:"success", summary: "", detail: result.message});
        }else{
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
      },
      (error)=>{
        //console.log(error);
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );

   }

  guardar(data: DetalleAnexo): void {
    if (this.accion == 'C') {
      this.data.lsDetalleAnexo.push(data);

      let p_archivo =  this.file.file;
      let params = new HttpParams()     
      .set("idUsuario", this.usuario.idusuario.toString())
      .set("idUsuarioEdicion", this.usuario.idusuario.toString())
      .set("id_archivo", "0")     
      .set("id_archivo_solplantacionforest", "0")
      .set("id_solplantforest", this.id_solplantforest.toString()  )
      .set("id_tipodetalleanexo", `${this.file.tipodetalleanexo}`) 
      .set("id_tipodetalleseccion", `${1}`)    ;
  const formData = new FormData();
  formData.append("file", p_archivo);

      
      this.servPf.registrarSolicitudPlantacacionAnexo(params,formData).subscribe(
        (result : any)=>{
          if(result.isSuccess){
            this.messageService.add({severity:"success", summary: "5. Anexos", detail: result.message});
          }else{
            this.messageService.add({severity:"warn", summary: "5. Anexos", detail: result.message});
          }
        },
        (error)=>{
          
          this.messageService.add({severity:"warn", summary: "5. Anexos", detail: error});
        }
      );

    } else {
      // let index = this.data.findIndex(x => x.id_solplantforest == data.id_solplantforest);
      // this.lstDetalle[index] = data;
    }

    this.verModalArchivos = false;
  }

  openEliminarRegistro(event: Event, index: number): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.data.lsDetalleAnexo.splice(index, 1);
        //this.eliminar();
      },
      reject: () => {
        //reject action
      }
    });
  }

  onFileChange(e: any) {
    this.file.url = URL.createObjectURL(e.target.files[0]);
    this.file.file = e.srcElement.files[0];
    this.file.descripcion = e.srcElement.files[0].name;
    e.preventDefault();
    e.stopPropagation();

  }

  verArchivo(d: any) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', d.url);
    link.setAttribute('download', d.descripcion);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  eliminar(data: any) {  
      let dto = {
        "id_solplantforest": this.id_solplantforest,
        "id_archivo_solplantacionforest": data.id_solplantforest_det,
        "idUsuarioElimina": this.usuario.idusuario,
      };
      this.servPf.EliminarSistemaPlantacionForestalAnexo(dto).subscribe(
        (result: any) => {
          if (result.isSuccess) {
            //this.data.lstDetalle.splice(index, 1);
            this.messageService.add({ severity: "success", summary: "", detail: result.message });
          } else {
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
        }, (error: any) => {
          this.messageService.add({ severity: "warn", summary: "", detail: error.message });
        });
   
  }

}
