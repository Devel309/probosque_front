import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer";
import Graphic from '@arcgis/core/Graphic';
import Color from '@arcgis/core/Color';
import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import SimpleLineSymbol from '@arcgis/core/symbols/SimpleLineSymbol';
import SimpleFillSymbol from '@arcgis/core/symbols/SimpleFillSymbol';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import Point from "@arcgis/core/geometry/Point";
import { MessageService } from 'primeng/api';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { AreaPFModel, DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { MapApi } from "src/app/shared/mapApi";
import { solPlantacionForestalAreaModel } from 'src/app/model/Solicitud';
import { ApiForestalService } from 'src/app/service/api-forestal.service';

@Component({
  selector: 'app-rpf-area',
  templateUrl: './rpf-area.component.html',
  styleUrls: ['./rpf-area.component.scss'],
  providers: [MessageService]
})
export class RpfAreaComponent implements OnInit {

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  public view: any = null;
  public filFile: any = null;
  public _id = this.mapApi.Guid2.newGuid;


  @Input('area') data: solPlantacionForestalAreaModel = new solPlantacionForestalAreaModel();


  comboUbigeo: ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;

  @Input() ListaDistrito = [];
  @Input() ListaProvincia = [];
  @Input() ListaDepartamento = [];
  @Input() idSolicitante: number = 0;

  constructor(private servPf: PermisoForestalService,
    private core: CoreCentralService,
    private messageService: MessageService,
    private serviceGeoforestal: ApiGeoforestalService,
    private mapApi: MapApi,
    private serviceExternos: ApiForestalService) { }

  ngOnInit(): void {
    this.initializeMap();
    this.listarPorFiltroDepartamento();
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const map = new Map({
      basemap: "hybrid"
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6
    });
    this.view = view;
  }
  processFile(file: any) {
    let controls = this.validateFileInput();
    if (controls.success === false) {
      this.messageService.add({ key: 'toast', severity: 'warn', summary: "", detail: controls.message });
    } else {
      let item = file;
      try {
        let promise = Promise.resolve([]);
        promise = this.mapApi.loadShapeFile(item);
        promise.then(data => {
          this.createLayers(data, file);
          this.calculateArea(data);

        }).catch(error => {
          throw error;
        });
      } catch (error) {
        console.log(error);
      }
    }
  }
  calculateArea(data: any) {
    let geometry: any = null;
    geometry = { spatialReference: { wkid: 4326 } };
    geometry.rings = data[0].features[0].geometry.coordinates;
    let area = this.mapApi.calculateArea(geometry, "hectares");
    this.data.area = Number(area.toFixed(3));
    this.superposicionArea(data[0].features[0].geometry.coordinates);
    this.obtenerCentroid(data[0].features[0].geometry.coordinates);
  }
  createLayers(layers: any, file: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.4;
      t.color = this.mapApi.random();
      t.file = file;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`
        }
      };
      this.createLayer(t);
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let geoJsonLayer:any = new GeoJSONLayer({
      url: url
    });
    const template = {
      title: item.title
    }
    geoJsonLayer.visible = true;
    geoJsonLayer.popupTemplate = template;
    geoJsonLayer.id = this.mapApi.Guid2.newGuid;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer.when((data: any) => {
      URL.revokeObjectURL(url);
      this.changeLayerStyle(geoJsonLayer.id,geoJsonLayer.color);
      this.view.goTo({ target: data.fullExtent });
    }).catch((error: any) => {
      console.log(error);
    });
    this.view.map.add(geoJsonLayer);

  }
  changeLayerStyle(id: any, color: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer === null) return;
    layer.color = color;
    let type = layer.geometryType;
    let style: any = null;
    if (type === "point" || type === "multipoint") {
      style = new SimpleMarkerSymbol();
      style.size = 10;
    }
    else if (type === "polyline") {
      style = new SimpleLineSymbol();
      style.width = 2;
    }
    else if (type === "polygon" || type === "extent") {
      style = new SimpleFillSymbol();
      style.outline = new SimpleLineSymbol();
      style.outline.width = 2;
    }
    style.color = Color.fromHex(color);
    layer.renderer = new SimpleRenderer({ symbol: style });

  }
  validateFileInput() {
    let controls = {
      message: '',
      success: true
    }
    if (this.filFile[0] === undefined || this.filFile[0].length === 0) {
      controls.message = "Seleccione un archivo";
      controls.success = false;
    }
    else if (this.filFile[0].name.endsWith(".zip") === false) {
      controls.message = "Seleccione un archivo .zip";
      controls.success = false;
    }
    return controls;
  }
  obtenerCentroid(geometry: any) {
    let graphicsLayer:any = new GraphicsLayer();
    this.view.map.add(graphicsLayer);
    this.mapApi.proj4j();
    //let coordinates = this.mapApi.transform(geometry,4326,32718);
    //console.log(coordinates);
    /* Crear Geometria para obtener el centroide */
    const fillSymbol = {
      type: "simple-fill",
      color: [227, 139, 79, 0.4],
      outline: {
        color: [255, 255, 255],
        width: 1
      }
    };
    const polygon: any = {
      type: "polygon",
      rings: geometry
    };
    const polygonGraphic: any = new Graphic({
      geometry: polygon,
      symbol: fillSymbol
    });
    /* Obtener el Centroide */
    let centroide = polygonGraphic.geometry.centroid
    
    let point: any = {
      type: 'point',
      longitude: centroide.x,
      latitude: centroide.y
    };
    let markerSymbol = {
      type: "simple-marker",
      color: [226, 119, 40]
    };
    /*Crear graphic punto para pintar en el mapa */
    const template = {
      title: "Centroide"
    }
    const pointGraphic: any = new Graphic({
      geometry: point,
      symbol: markerSymbol
    });
    pointGraphic.popupTemplate = template;
    this.data.ubi_geografica = `X: ${centroide.x} Y: ${centroide.y} Zona: 18 Sur, Datum: WGS-84`
    graphicsLayer.add(pointGraphic);
  }
  superposicionArea(geometry: any) {
    let params = {
      codProceso: "110102",
      geometria: {
        poligono: geometry,
      },
    };
    this.serviceExternos.validarSuperposicionPlanificacion(params).subscribe((result: any) => {
      
      result.dataService.data.noSuperpuesta.forEach((t: any) => {
        if (t.seSuperpone === "Si") {
          t.geoJson.opacity = 0.8;
          t.geoJson.color = this.mapApi.random();
          t.geoJson.title = t.nombreCapa;
          t.geoJson.crs.type = "name"
          t.geoJson.file = null;
          this.createLayer(t.geoJson)
        }
      });
    });
  }
  onFileInversionistaChange(e: any) {
    this.data.fileNameInversionista = e.srcElement.files[0].name;
    e.preventDefault();
    e.stopPropagation();

  }

  onFilePropietarioChange(e: any) {
    this.data.fileNamePropietario = e.srcElement.files[0].name;
    e.preventDefault();
    e.stopPropagation();

  }

  onFileShapeChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.target.files;
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          this.processFile(e.target.files[i])
          i++;
        }
      }
    }
  }

  
  listarPorFiltroDepartamento() {
    let depart = {};
    this.core.listarPorFiltroDepartamento(depart).subscribe(
      (result: any) => {
        if (result.success) {
          this.ListaDepartamento = result.data;
        }
      });
  }


  listarPorFilroDistrito(dato: any) {
    let provincia = {
      "idProvincia": dato.value//3  
    };
    this.core.listarPorFilroDistrito(provincia).subscribe(
      (result: any) => {
        //;       
        // console.log(result);
        if (result.isSuccess) {
          this.ListaDistrito = result.data;
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }

  listarPorFilroProvincia(dato: any) {
    
    let provincia = {
      "idDepartamento": dato.value//3  
    };
    this.core.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
        //;       
        //console.log(result);
        if (result.isSuccess) {
          this.ListaProvincia = result.data;
          this.listarPorFilroDistrito({ value: result.data[0].idProvincia });

          this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }


}
