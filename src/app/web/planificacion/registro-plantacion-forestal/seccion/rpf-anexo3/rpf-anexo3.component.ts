import { Component, Input, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/model/Usuario';
import { PlantacionForestalModel } from 'src/app/model/util/dataDemoMPAFPP';

@Component({
  selector: 'app-rpf-anexo3',
  templateUrl: './rpf-anexo3.component.html',
  styleUrls: ['./rpf-anexo3.component.scss']
})
export class RpfAnexo3Component implements OnInit {

  idSolicitante: number = 0;
  usuario = {} as UsuarioModel
  id_solplantforest: any = null;


  @Input() data: PlantacionForestalModel = new PlantacionForestalModel();
  @Input() ListaDepartamento = [];

  @Input() ListaDistritoInformacion = [];
  @Input() ListaProvinciaInformacion = [];

  @Input() ListaDistritoArea = [];
  @Input() ListaProvinciaArea = [];

  constructor() { }

  ngOnInit(): void {
    
  }

}
