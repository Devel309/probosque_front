import { Component, Input, OnInit } from '@angular/core';
import { AnyNaptrRecord } from 'dns';
import { MessageService } from 'primeng/api';
import { solPlantacionForestalModel } from 'src/app/model/Solicitud';
import { ComboModel, ComboModel2 } from 'src/app/model/util/Combo';
import { DataDemoMPAFPP, InformacionSolicitantePFModel } from 'src/app/model/util/dataDemoMPAFPP';
import { CoreCentralService } from 'src/app/service/coreCentral.service';

@Component({
  selector: 'app-rpf-informacion-solicitante',
  templateUrl: './rpf-informacion-solicitante.component.html',
  styleUrls: ['./rpf-informacion-solicitante.component.scss'],
  providers: [MessageService]
})
export class RpfInformacionSolicitanteComponent implements OnInit {

  @Input('solPlantacionForestal') data: solPlantacionForestalModel = new solPlantacionForestalModel();

  comboTipoPersona: ComboModel2[] = DataDemoMPAFPP.TipoPersona;
  comboUbigeo: ComboModel2[] = DataDemoMPAFPP.ubigeoCombo;

  @Input() ListaDistrito = [];
  @Input() ListaProvincia = [];
  @Input() ListaDepartamento = [];

  @Input() ListaDistritoRepr = [];
  @Input() ListaProvinciaRepr = [];
 ListaDepartamentoRepr = [];

  @Input() idSolicitante:number=0;

  // ListaDepartamentoRepr = [];
  // ListaProvinciaRepr = [];
  // ListaDistritoRepr = [];

  constructor(
    private core: CoreCentralService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.listarPorFiltroDepartamento();
/*
if(this.idSolicitante==1){
this.data.readonly=false;
} 
if(this.idSolicitante==2){
  this.data.readonly=false;
}
if(this.idSolicitante==3){
  this.data.readonly=false;
}
if(this.idSolicitante==4){
  this.data.readonly=true;
}
if(this.idSolicitante==5){
  this.data.readonly=true;
}
if(this.idSolicitante==6){
  this.data.readonly=false;
}
*/

}

  
  listarPorFiltroDepartamento() {
    let depart = {};
    this.core.listarPorFiltroDepartamento(depart).subscribe(
      (result: any) => {
        if (result.success) {
          this.ListaDepartamento = result.data;
          this.ListaDepartamentoRepr = result.data;
        }
      });
  }


  listarPorFilroDistrito(dato: any,tipo:any) {

    let provincia = {
      "idProvincia": dato.value//3  
    };
    this.core.listarPorFilroDistrito(provincia).subscribe(
      (result: any) => {

        if (result.isSuccess) {

          if(tipo==1){
            this.ListaDistrito = result.data;
          }
          else if(tipo==2){
            this.ListaDistritoRepr = result.data;
          }
         
          //this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }

  listarPorFilroProvincia(dato: any,tipo:any) {

    let provincia = {
      "idDepartamento": dato.value//3  
    };
    this.core.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
   
        if (result.isSuccess) {
          if(tipo==1){
            this.ListaProvincia = result.data;
            this.listarPorFilroDistrito({ value: result.data[0].idProvincia },tipo);
          }
          else if(tipo==2){
            this.ListaProvinciaRepr = result.data;
            this.listarPorFilroDistrito({ value: result.data[0].idProvincia },tipo);
          }        

          //this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      }, (error: any) => {
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }


}
