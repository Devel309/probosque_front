import { Component, Input,ViewChild, ElementRef, OnInit } from '@angular/core';
import { DetalleAreaPlantadaModel, DetalleDetallePlanteacion2model, DetallePlantacion2Model, DetalleVerticesModel } from 'src/app/model/util/dataDemoMPAFPP';
import { MapApi } from "src/app/shared/mapApi";
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-rpf-detalle-plantacion2',
  templateUrl: './rpf-detalle-plantacion2.component.html',
  styleUrls: ['./rpf-detalle-plantacion2.component.scss'],
  providers: [MessageService]
})
export class RpfDetallePlantacion2Component implements OnInit {
  @ViewChild('tblVertices', { static: true }) private tblVertices!: ElementRef;
  @Input('detalleAreaPlanteada2') data: DetallePlantacion2Model = new DetallePlantacion2Model();
  @Input() idSolicitante: number = 0;
  _filFile: any = null;

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accion: string = "";
  detalleAreaPlanteada2Context: DetalleDetallePlanteacion2model = new DetalleDetallePlanteacion2model();


  openModal(data: any, tipo: string) {
    this.accion = tipo;
    if (tipo == "C") {
      this.detalleAreaPlanteada2Context = new DetalleDetallePlanteacion2model();
      this.tituloModalMantenimiento = "Nuevo Registro"
    } else if (tipo == "E") {
      this.detalleAreaPlanteada2Context = new DetalleDetallePlanteacion2model(data);
      this.tituloModalMantenimiento = "Editar Registro"
    }

    this.verModalMantenimiento = true;
  }

  guardar(data: DetalleDetallePlanteacion2model): void {
    ;

    if (this.accion == 'C') {
      this.data.lstDetalle.push(data);
    } else {
      let index = this.data.lstDetalle.findIndex(x => x.id_solplantforest_coord_det == data.id_solplantforest_coord_det);
      this.data.lstDetalle[index] = data;
    }

    this.verModalMantenimiento = false;
  }

  eliminar(data: DetalleDetallePlanteacion2model) {
    let index = this.data.lstDetalle.findIndex(x => x.id_solplantforest_coord_det == data.id_solplantforest_coord_det);
    this.data.lstDetalle.splice(index, 1);
  }

  /********************************************************************* */

  nuevoDetalle() {
    this.detalleAreaPlanteada2Context.detalle.push(new DetalleVerticesModel());
  }

  eliminarDetalle(data: DetalleVerticesModel) {
    let index = this.detalleAreaPlanteada2Context.detalle.findIndex(x => x.id_bloquesector == data.id_bloquesector);
    this.detalleAreaPlanteada2Context.detalle.splice(index, 1);
  }


  constructor(private messageService: MessageService,private mapApi: MapApi) { }

  ngOnInit(): void {
  }
  onFileShapeChange(e:any){
    e.preventDefault();
    e.stopPropagation();
    this._filFile = e.target.files;
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          this.processFile(e.target.files[i])
          i++;
        }
      }
    }
  }
  processFile(file: any) {
    let controls = this.validateFileInput();
    if (controls.success === false) {
      this.messageService.add({ key: 'toast', severity: 'warn', summary: "", detail: controls.message });
    } else {
      let item = file;
      try {
        let promise = Promise.resolve([]);
        promise = this.mapApi.loadShapeFile(item);
        promise.then(data => {
          this.createTableVertices(data);
        }).catch(error => {
          throw error;
        });
      } catch (error) {
        console.log(error);
      }
    }
  }
  validateFileInput() {
    let controls = {
      message: '',
      success: true
    }
    if (this._filFile[0] === undefined || this._filFile[0].length === 0) {
      controls.message = "Seleccione un archivo";
      controls.success = false;
    }
    else if (this._filFile[0].name.endsWith(".zip") === false) {
      controls.message = "Seleccione un archivo .zip";
      controls.success = false;
    }
    return controls;
  }
  createTableVertices(data: any) {
    let items = data[0].features
    let table = this.tblVertices.nativeElement;
    let thead = table.querySelector('thead');
    let tbody = table.querySelector('tbody');
    tbody.empty();
    let groups = items.groupBy((t: any) => t.properties.anexo);
    let k = 0;
    groups.forEach((t: any, i: any) => {
      let tr = tbody.insertRow(k);
      let td = tr.insertCell(0);
      td.textContent = t.key;
      td.rowSpan = t.value.length;
      td.classList.add('align-middle');
      tr.insertCell(1).textContent = t.value[0].properties.vertice;
      tr.insertCell(2).textContent = t.value[0].properties.este;
      tr.insertCell(3).textContent = t.value[0].properties.norte;
      tr.insertCell(4).textContent = t.value[0].properties.observ;
      td = tr.insertCell(5);
      td.rowSpan = t.value.length;
      td.textContent = t.value[0].properties.area;
      td.classList.add('align-middle');
      t.value = t.value.slice(1);
      k++;
      t.value.forEach((t2: any, j: any) => {
        let tr = tbody.insertRow(k);
        tr.insertCell(0).textContent = t2.properties.vertice;
        tr.insertCell(1).textContent = t2.properties.este;
        tr.insertCell(2).textContent = t2.properties.norte;
        tr.insertCell(3).textContent = t2.properties.observ;
        k++;
      });
    });
    this.calculateAreaTotal();
  }
  calculateAreaTotal(){
    let sumTotal = 0;
    let table = this.tblVertices.nativeElement;
    let body = table.querySelector('tbody');
    let foot = table.querySelector('tfoot');
    let trs = body.querySelectorAll("tr");
    let tds = foot.querySelectorAll('td');
    trs.forEach((t: any) => {
      let td5 = t.querySelectorAll("td")[5];
      if (td5 !== undefined) sumTotal += parseFloat(td5.innerHTML || 0);
    });
    let total = tds[1];
    total.innerHTML = `${sumTotal.toFixed(3)}`;
  }
}
