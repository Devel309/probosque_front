import { Component, Input, OnInit ,ViewChild,ElementRef} from '@angular/core';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';
import * as geometryEngine from '@arcgis/core/geometry/geometryEngine';
import Color from '@arcgis/core/Color';
import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import SimpleLineSymbol from '@arcgis/core/symbols/SimpleLineSymbol';
import SimpleFillSymbol from '@arcgis/core/symbols/SimpleFillSymbol';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import { MessageService } from 'primeng/api';
import { UsuarioModel } from 'src/app/model/Usuario';
import { AreaPlantadaModel, DetalleAreaPlantadaModel } from 'src/app/model/util/dataDemoMPAFPP';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { ApiForestalService } from 'src/app/service/api-forestal.service';
declare const shp: any;

@Component({
  selector: 'app-rpf-area-plantada',
  templateUrl: './rpf-area-plantada.component.html',
  styleUrls: ['./rpf-area-plantada.component.scss']
})
export class RpfAreaPlantadaComponent implements OnInit {
  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  public view: any = null;
  public filFile: any = null;
  public geoJsonLayer: any = null;
  public _id = RpfAreaPlantadaComponent.Guid2.newGuid;
  @Input('areaPlantada') data: AreaPlantadaModel = new AreaPlantadaModel();
  @Input() usuario: any = {};
  @Input() id_solplantforest: any = null;
  @Input() idSolicitante: number = 0;


  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accion: string = "";
  areaPlantadaContext: DetalleAreaPlantadaModel = new DetalleAreaPlantadaModel();
  comboTipoSistema = [];
  comboUnidadMedida = [{ valor: "ha" }, { valor: "m2" }, { valor: "otros" }]
  medida: string = "";

  constructor(
    private servPf: PermisoForestalService,
    private messageService: MessageService,
    private serviceGeoforestal: ApiGeoforestalService,
    private serviceExternos: ApiForestalService
  ) { }

  ngOnInit(): void {
    this.listarTipoComboSistemaPlantacionForestal();
    this.initializeMap();
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const map = new Map({
      basemap: "hybrid"
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6
    });
    this.view = view;
  }
  static get Guid2() {
    return class Guid2 {
      constructor() { }
      static get newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
          var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });

      }
    }
  }
  random() {
    var length = 6;
    var chars = '0123456789ABCDEF';
    var hex = '#';
    while (length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }
  processFile(file: any) {
    let controls = this.validateFileInput();
    if (controls.success === false) {
      this.messageService.add({ key: 'toast', severity: 'warn', summary: "", detail: controls.message });
    } else {
      let item = file;
      try {
        let promise = Promise.resolve([]);
        promise = this.loadShapeFile(item);
        promise.then(data => {
          this.createLayers(data, file);
          this.calculateArea(data);

        }).catch(error => {
          throw error;
        });
      } catch (error) {
        console.log(error);
      }
    }
  }
  calculateArea(data: any) {
    let geometry: any = null;
    geometry = { spatialReference: { wkid: 4326 } };
    geometry.rings = data[0].features[0].geometry.coordinates;
    let area = geometryEngine.geodesicArea(geometry, "hectares");
    this.data.areaTotal = area.toFixed(3);
    this.superposicionArea(data[0].features[0].geometry.coordinates);
  }
  createLayers(layers: any, file: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.random();
      t.file = file;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`
        }
      };
      this.createLayer(t);
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    this.geoJsonLayer = new GeoJSONLayer({
      url: url
    });
    const template = {
      title: item.title
    }
    this.geoJsonLayer.visible = true;
    this.geoJsonLayer.popupTemplate = template;
    this.geoJsonLayer.id = RpfAreaPlantadaComponent.Guid2.newGuid;
    this.geoJsonLayer.ID = this.geoJsonLayer.id;
    this.geoJsonLayer.title = item.title;
    this.geoJsonLayer.layerType = 'vector';
    this.geoJsonLayer.groupId = this._id;
    this.geoJsonLayer.color = item.color;
    this.geoJsonLayer.opacity = item.opacity;
    this.geoJsonLayer.attributes = item.features;
    this.geoJsonLayer.file = item.file;
    this.geoJsonLayer.when((data: any) => {
      URL.revokeObjectURL(url);
      this.changeLayerStyle(this.geoJsonLayer.id, this.geoJsonLayer.color);
      this.view.goTo({ target: data.fullExtent });
    }).catch((error: any) => {
      console.log(error);
    });
    this.view.map.add(this.geoJsonLayer);

  }
  changeLayerStyle(id: any, color: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer === null) return;
    layer.color = color;
    let type = layer.geometryType;
    let style: any = null;
    if (type === "point" || type === "multipoint") {
      style = new SimpleMarkerSymbol();
      style.size = 10;
    }
    else if (type === "polyline") {
      style = new SimpleLineSymbol();
      style.width = 2;
    }
    else if (type === "polygon" || type === "extent") {
      style = new SimpleFillSymbol();
      style.outline = new SimpleLineSymbol();
      style.outline.width = 2;
    }
    style.color = Color.fromHex(color);
    layer.renderer = new SimpleRenderer({ symbol: style });

  }
  validateFileInput() {
    let controls = {
      message: '',
      success: true
    }
    if (this.filFile[0] === undefined || this.filFile[0].length === 0) {
      controls.message = "Seleccione un archivo";
      controls.success = false;
    }
    else if (this.filFile[0].name.endsWith(".zip") === false) {
      controls.message = "Seleccione un archivo .zip";
      controls.success = false;
    }
    return controls;
  }
  superposicionArea(geometry:any){
    let params = {
      codProceso:"110102",
      geometria:{poligono: geometry,
      },
    };
    this.serviceExternos.validarSuperposicionPlanificacion(params).subscribe((result:any) =>{
        console.log(result);
        result.dataService.data.noSuperpuesta.forEach((t:any) => {
            if(t.seSuperpone === "Si"){
              t.geoJson.opacity = 0.8;
              t.geoJson.color = this.random();
              t.geoJson.title = t.nombreCapa;
              t.geoJson.crs.type = "name"
              t.geoJson.file = null;
              this.createLayer(t.geoJson)
            }
        });
    });
  }
  loadShapeFile(item: any) {
    return this.getItem(item).then(data => {
      return new Promise((resolve, reject) => {
        let fileReader = new FileReader();
        fileReader.onload = (e: any) => {
          resolve(e.target.result);
        };
        fileReader.onerror = fileReader.onabort = reject;
        fileReader.readAsArrayBuffer(data);
      });
    }).then(data => {
      return shp(data);
    }).then(data => {
      if (Array.isArray(data) === false)
        data = [data];
      data.forEach((t: any) => t.title = t.fileName);
      return data;
    }).catch(error => {
      throw error;
    });
  }
  getItem(item: any) {
    let promise = Promise.resolve(item);
    if (typeof item === "string")
      promise = fetch(item).then(data => { return data.blob(); });
    return promise;
  }
  openModal(data: any, tipo: string) {
    this.accion = tipo;
    if (tipo == "C") {
      this.areaPlantadaContext = new DetalleAreaPlantadaModel();
      this.medida = "";
      this.tituloModalMantenimiento = "Nuevo Registro"
    } else if (tipo == "E") {
      this.areaPlantadaContext = new DetalleAreaPlantadaModel(data);
      this.tituloModalMantenimiento = "Editar Registro"
    }

    this.verModalMantenimiento = true;
  }

  guardar(data: DetalleAreaPlantadaModel): void {
    if (this.accion == 'C') {
      if (this.medida != 'otros')
        data.superficie_medida = this.medida;

      this.data.lstDetalle.push(data);
    } else {
      let index = this.data.lstDetalle.findIndex(x => x.id_solplantforest_areaplantada == data.id_solplantforest_areaplantada);
      this.data.lstDetalle[index] = data;
    }

    this.verModalMantenimiento = false;
  }

  eliminar(data: DetalleAreaPlantadaModel) {
    let index = this.data.lstDetalle.findIndex(x => x.id_solplantforest_areaplantada == data.id_solplantforest_areaplantada);
    this.data.lstDetalle.splice(index, 1);

    if (data.id_solplantforest_areaplantada) {
      let dto = {
        id_solplantforest: this.id_solplantforest,
        id_solplantforest_areaplantada: data.id_solplantforest_areaplantada,
        idUsuarioElimina: this.usuario.idusuario,
      };

      this.servPf.EliminarSistemaPlantacionForestalAreaPlant(dto).subscribe((result: any) => {
        this.data.lstDetalle.splice(index, 1);
        this.messageService.add({ severity: "success", summary: "", detail: result.message });

      })

    } else {
      this.data.lstDetalle.splice(index, 1);

    }
  }


  listarTipoComboSistemaPlantacionForestal() {

    this.servPf.TipoSistemaForestales(null).subscribe(
      (result: any) => {
        
        if (result.isSuccess) {
          this.comboTipoSistema = result.data;
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          //console.log(result.message);
        }
      }, (error: any) => {
        //console.log(error.message);
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });

  }
  onFileShapeChange(e:any){
    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.srcElement.files;
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        let i = 0;
        while (i < e.srcElement.files.length) {
          this.processFile(e.srcElement.files[i])
          i++;
        }
      }
    }
  }



}
