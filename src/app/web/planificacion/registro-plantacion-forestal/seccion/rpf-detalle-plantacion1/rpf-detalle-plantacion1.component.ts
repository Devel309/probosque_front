import { Component, Input, OnInit } from '@angular/core';
import { DetallePlantacion1Model, DetalleDetallePlantacion1 } from 'src/app/model/util/dataDemoMPAFPP';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { MessageService } from 'primeng/api';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import {AutoCompleteModule} from 'primeng/autocomplete';

@Component({
  selector: 'app-rpf-detalle-plantacion1',
  templateUrl: './rpf-detalle-plantacion1.component.html',
  styleUrls: ['./rpf-detalle-plantacion1.component.scss']
})
export class RpfDetallePlantacion1Component implements OnInit {

  @Input('detalleAreaPlanteada1') data: DetallePlantacion1Model = new DetallePlantacion1Model();
  @Input() usuario: any = {};
  @Input() id_solplantforest: any = null;

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accion: string = "";
  detalleAreaPlanteada1Context: DetalleDetallePlantacion1 = new DetalleDetallePlantacion1();

  ListaEspecies: any[] = [];
  ResultListaEspecies: any[] = [];

  tipoProduccion: string = "";

  comboTipoArbol: any[] = [{ valor: "Matas" }, { valor: "Cepas existentes" }];

  comboProduccionEstimada: any[] = [{ valor: "M3" }, { valor: "Kg" }, { valor: "Cañas" }, { valor: "Unidades" }, { valor: "Otros" }];

  constructor(private servPf: PermisoForestalService,
    private messageService: MessageService,
    private core: CoreCentralService) { }


  ngOnInit(): void {
    this.ListaPorFiltroEspecieForestal();
  
  }

  ListaPorFiltroEspecieForestal() {
    
    let esps = {
      "autor": "",
      "familia": "",
      "idespecie": 0,
      "nombre_cientifico": "",
      "nombre_comun": ""
    };
    this.core.ListaPorFiltroEspecieForestal(esps).subscribe(
      (result: any) => {
        if (result.isSuccess) {
          this.ListaEspecies = result.data;
        }
      });
  }

searchEspecies(event:any,tipo :any){
  let filtered : any[] = [];
  // filtered = this.ListaEspecies.find(x  => x.nombreComun == event.query );

  for(let i = 0; i < this.ListaEspecies.length; i++) {
    let especie = this.ListaEspecies[i];
    if(tipo==1){
        if (especie.nombreCientifico.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
          filtered.push(especie);
        }
    }
    else{
      if (especie.nombreComun.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
        filtered.push(especie);
      }
    }
    
}

  this.ResultListaEspecies =filtered;

}

  openModal(data: any, tipo: string) {
    this.accion = tipo;
    if (tipo == "C") {


      this.detalleAreaPlanteada1Context = new DetalleDetallePlantacion1();
      this.tituloModalMantenimiento = "Nuevo Registro"
      this.tipoProduccion = "";
    } else if (tipo == "E") {
      this.detalleAreaPlanteada1Context = new DetalleDetallePlantacion1(data);
      this.tituloModalMantenimiento = "Editar Registro"
    }

    this.verModalMantenimiento = true;
  }

  guardar(data: DetalleDetallePlantacion1): void {
    if (this.accion == 'C') {
      if (this.tipoProduccion != 'Otros')
        data.producion_estimada_und_medida = this.tipoProduccion;

        data.espcs_nom_comun = data.especie.nombreComun;
        data.espcs_nom_cientifico = data.especie_cient.nombreCientifico;


      this.data.lstDetalle.push(data);
    } else {
      let index = this.data.lstDetalle.findIndex(x => x.id_solplantforest_det == data.id_solplantforest_det);
      this.data.lstDetalle[index] = data;
    }

    this.verModalMantenimiento = false;
  }

  eliminar(data: DetalleDetallePlantacion1) {
    let index = this.data.lstDetalle.findIndex(x => x.id_solplantforest_det == data.id_solplantforest_det);

    if (data.id_solplantforest_det) {
      let dto = {
        "id_solplantforest": this.id_solplantforest,
        "id_solplantforest_det": data.id_solplantforest_det,
        "idUsuarioElimina": this.usuario.idusuario,
      };

      this.servPf.EliminarSistemaPlantacionForestalDet(dto).subscribe(
        (result: any) => {
          if (result.isSuccess) {
            this.data.lstDetalle.splice(index, 1);
            //this.messageService.add({ severity: "success", summary: "", detail: result.message });
          } else {
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
        }, (error: any) => {
          this.messageService.add({ severity: "warn", summary: "", detail: error.message });
        });

    } else {
      this.data.lstDetalle.splice(index, 1);
    }
  }


}
