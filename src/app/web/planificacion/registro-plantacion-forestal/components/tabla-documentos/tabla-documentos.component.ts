import { Component, Input, OnInit } from '@angular/core';

import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'tabla-documentos',
  templateUrl: './tabla-documentos.component.html',
  styleUrls: ['./tabla-documentos.component.scss']
})
export class TablaDocumentosComponent implements OnInit {

  @Input() fileList: any[] = [];
  file: any = {};


  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {

  }

  ngOnInit(): void {

  }

  notificar(detail: string) {
    this.messageService.add({ severity: 'success', detail });
  }

  onFileChange(e: any) {
    this.file = {};


    e.preventDefault();
    e.stopPropagation();

  }

  verArchivo(d: any) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', d.url);
    link.setAttribute('download', d.descripcion);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  openEliminarRegistro(event: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.fileList.splice(index, 1);
      },
      reject: () => {
        //reject action
      }
    });
  }

}


