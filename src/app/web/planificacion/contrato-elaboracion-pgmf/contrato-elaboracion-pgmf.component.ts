import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { concatMap, finalize, tap } from "rxjs/operators";

import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import { DownloadFile} from "../../../shared/util";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CoreCentralService, BandejaPlanOperativoService, PlanificacionService, PlanManejoService, UsuarioService, ArchivoService, ParametroValorService } from "@services";
import { ArchivoTipo, handlerSaveResult, MapApi, PGMFArchivoSubTipo, PGMFArchivoTipo, ToastService } from "@shared";
import { PlanManejoContrato } from "@models";
import { CodigoEstadoContrato } from 'src/app/model/util/CodigoEstadoContrato';
import { CodigosTiposPersona } from 'src/app/model/util/CodigosTiposPerona';
import { CodigoTipoContrato } from 'src/app/model/util/CodigoTipoContrato';
import { ConfirmationService } from "primeng/api";
import { ConsoleLogger } from "@angular/compiler-cli/src/ngtsc/logging";
import { ConsultaSolicitudesComponent } from "../ampliacion-solicitudes/ampliacion-solicitudes.component";
import { CodigoProceso } from "../../../model/util/CodigoProceso";
import { environment } from "@env/environment";
@Component({
	selector: "app-contrato-elaboracion-pgmf",
	templateUrl: "./contrato-elaboracion-pgmf.component.html",
	styleUrls: ["./contrato-elaboracion-pgmf.component.scss"],
})
export class ContratoElaboracionPgmfComponent implements OnInit {
	@ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
	_view: any = null;
	_id = this.mapApi.Guid2.newGuid;

	idPlanManejo: number = 21;

	distritos = [];
	provincias = [];
	departamentos = [];

	isValid: boolean = false;
	display: boolean = false;

	contratos: PlanManejoContrato[] = [];
	contratosSeleccionados: PlanManejoContrato[] = [];
	contrato!: PlanManejoContrato;
	contratosPrincipales : PlanManejoContrato[] = [];
	contratosPrincipalesSeleccionados: PlanManejoContrato[] = [];

	codigoEstadoContrato = CodigoEstadoContrato;
	codigoTipoContrato = CodigoTipoContrato;
	codigoProceso = CodigoProceso;

	f: FormGroup = this.fb.group({
		idPlanManejo: [null],
		descripcion: [this.codigoProceso.PLAN_GENERAL_MANEJO_FORESTAL],
		idContrato: [0, Validators.required],
		idSolicitud: [1],
		idPlanManejoPadre: [null, Validators.required],
		idTipoPlan: [1],
		idTipoProceso: [1],
		consolidado: [null],
		idUsuarioRegistro: [this.user.idUsuario],
		listaPlanManejoContrato: [[], Validators.minLength(1)],
		listaContrato: [[], Validators.minLength(1)],
		contratoPrincipal: [null, Validators.nullValidator],
		infoGeneral: this.fb.group({
			codigoTipoPersonaTitular: [{ value: null, disabled: true }, Validators.nullValidator],
			descripcionTipoPersonaTitular: [{ value: "", disabled: true },],
			codigoTipoDocIdentidadTitular: [{ value: null, disabled: true }, Validators.required],
			descripcionTipoDocIdentidadTitular: [{ value: null, disabled: true }, Validators.required],
			nombreTitular: [{ value: null, disabled: true }, Validators.required],
			apePaternoTitular: [{ value: null, disabled: true }, Validators.required],
			apeMaternoTitular: [{ value: null, disabled: true }, Validators.required],
			numeroDocumentoTitular: [{ value: null, disabled: true }, Validators.required],
			rucTitular: [{ value: null, disabled: true }, Validators.required],
			correoTitular: [{ value: null, disabled: true }, Validators.required],
			razonSocialTitular: [{ value: null, disabled: true }, Validators.required],
			domicilioLegalTitular: [{ value: null, disabled: true }, Validators.required],
			departamento: [{ value: null, disabled: true }, Validators.required],
			provincia: [{ value: null, disabled: true }, Validators.required],
			distrito: [{ value: null, disabled: true }, Validators.required],
			codigoTipoDocIdentidadRepLegal: [{ value: null, disabled: true }, Validators.required],
			descripcionTipoDocIdentidadRepLegal: [{ value: null, disabled: true }, Validators.required],
			numeroDocumentoRepLegal: [{ value: null, disabled: true }, Validators.required],
			nombreRepresentanteLegal: [{ value: null, disabled: true }, Validators.required],
			apePaternoRepLegal: [{ value: null, disabled: true }, Validators.required],
			apeMaternoRepLegal: [{ value: null, disabled: true }, Validators.required],
			correoRepLegal: [{ value: null, disabled: true }, Validators.required],
			rucRepLegal: [null, Validators.required],
			constanciaRepLegal: this.fb.group({
				idPlanManejo: [null],
				idArchivo: [null],
				archivo: [null, Validators.required],
				codigoTipoPGMF: [PGMFArchivoTipo.PGMFIG],
				codigoSubTipoPGMF: [PGMFArchivoSubTipo.ARCHIVO],
				fileName: [{value: "", disabled: true}],
				descripcion: [''],
				observacion: [''],
				idUsuarioRegistro: [this.user.idUsuario],
			}),
			tipoPersonaTitular: ['TPERNATU'], //agregado
			tipoPersonaRepre: ['TPERNATU'], //agregado
		}),
	});
	get archivo() {
		return this.f.get('infoGeneral.constanciaRepLegal')?.value
	}
	set archivo({ idArchivo, descripcion, codigoTipoPGMF, codigoSubTipoPGMF, idUsuarioRegistro }) {
		this.f.get('infoGeneral.constanciaRepLegal')?.patchValue({ idArchivo, descripcion, codigoTipoPGMF, codigoSubTipoPGMF, idUsuarioRegistro });
	}
	
	get formConsolidado() { return this.f.get('consolidado') }
	get formListaPlanManejoContrato() { return this.f.get('listaPlanManejoContrato') }

	idPGMF!: number;
	isDisabled: boolean = false;
	isContratoDisabled: boolean = false;
	isPerNaturalTitular: boolean = true;
	isPerNaturalRepre: boolean = true;
	lstTipoPersona: any[] = [];
	fileRepLegal: any;
	fileNombre: string = "";

	constructor(
		private fb: FormBuilder,
		private router: Router,
		private dialog: MatDialog,
		private toast: ToastService,
		private user: UsuarioService,
		private apiCore: CoreCentralService,
		private apiPlanificacion: PlanificacionService,
		private apiBandejaPO: BandejaPlanOperativoService,
		private apiPlanManejo : PlanManejoService,
		private apiArchivo: ArchivoService,
		private routeActive: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private parametroValorService: ParametroValorService,
		private changeDetectorRef: ChangeDetectorRef,
		private mapApi: MapApi
	) {
		this.idPGMF = Number(this.routeActive.snapshot.paramMap.get('idPlan'));
		if (this.idPGMF) { this.isDisabled = true; this.isContratoDisabled = true; }
	}

	ngOnInit(): void {
		

		this.initializeMap();
		this.f.controls['consolidado'].setValue(false);
		if(!this.idPGMF) this.listarContratos();
		//this.listarContratosVigentes();
		this.listarTipoPersona();
		if(this.idPGMF) {
			this.obtenerContratosPlanTrabajo(this.idPGMF);
			this.obtenerAdjuntosRepLegal(this.idPGMF);
		}
	}

	initializeMap(): void {
		const container = this.mapViewEl.nativeElement;
		container.style.height = "390px";
		container.style.width = "100%";
		let view = this.mapApi.initializeMap(container);
		this._view = view;
	}

	listarTipoPersona() {
		var params = { prefijo: 'TPER' }
		this.parametroValorService.listarPorCodigoParametroValor(params).subscribe(
			(result: any) => { this.lstTipoPersona = result.data; }
		);
	};

	clickRadio() {
		this.limpiarDatos();

		this.mapApi.cleanLayers(this._id, this._view);
		if (!this.idPGMF) this.isContratoDisabled = false;
		this.formListaPlanManejoContrato?.setValue([]);
		this.contratosSeleccionados = [];
		
	}

	changeContratos(contratos: PlanManejoContrato[], tipoSelect: boolean) {
		if(tipoSelect) this.limpiarDatos();
		
		if(contratos.length > 0){
			var idsContratos: string = "";
			this.contratosSeleccionados = [];
			this.contratosSeleccionados = [...contratos];
			this.contratosSeleccionados.forEach((x: PlanManejoContrato) => {
				x.contratoPrincipal = false;
				if (idsContratos.length > 0)
					idsContratos = idsContratos.concat(',');
				idsContratos = idsContratos.concat(x.idContrato.toString());

			});
			this.f.patchValue({ listaPlanManejoContrato: this.contratosSeleccionados });
			//this.f.patchValue({ listaContrato: this.contratosSeleccionados });

			this.obtenerContratosPoligonos(idsContratos);
		}else{
			this.mapApi.cleanLayers(this._id, this._view);
		}
	}

	seleccionarContrato(event: any, c: PlanManejoContrato, chk: any) {
		this.limpiarDatos();
		if(event.checked) {

		this.f.controls['infoGeneral'].patchValue({
			nombreTitular: c.nombreTitular,
			numeroDocumentoTitular: c.numeroDocumentoTitular,
			rucTitular: c.rucTitular,
			domicilioLegalTitular: c.domicilioLegalTitular,
			departamento: c.departamentoTitular,
			provincia: c.provinciaTitular,
			distrito: c.distritoTitular,
			nombreRepresentanteLegal: c.nombreRepresentanteLegal,
			rucRepLegal: c.dniRepresentanteLegal,
		});

		this.contratosPrincipalesSeleccionados = [];
		this.contratosPrincipalesSeleccionados.push(c);

		this.formListaPlanManejoContrato?.value.forEach((x: PlanManejoContrato) => {
			x.contratoPrincipal = false;
			if(x.idContrato == c.idContrato) x.contratoPrincipal = event.checked;
		});

		this.obtenerContrato(c.idContrato);

		}
	}

	limpiarDatos() {
		this.f.controls['infoGeneral'].reset();
		this.contratosPrincipalesSeleccionados = [];
		this.isPerNaturalTitular = true;
		this.isValid = false;
	}

	getDepartamento(idDepartamento: number) {
    
		this.apiCore.listarPorFiltroDepartamento({ idDepartamento })
			.subscribe((result: any) => this.departamentos = result.data);
	}
	getProvincia(idProvincia: number) {
		this.apiCore.listarPorFilroProvincia({ idProvincia })
			.subscribe((result: any) => this.provincias = result.data);
	}
	getDistrito(idDistrito: number) {
		this.apiCore.listarPorFilroDistrito({ idDistrito })
			.subscribe((result: any) => this.distritos = result.data);
	}

	validar() {
		if(!this.validarContratoPrincipal()) {
			this.isValid = false;
			this.toast.warn("(*)Debe seleccionar un contrato como principal.");
			return;
		}

		this.toast.ok("Se Validó Correctamente");
		this.isValid = true;
	}

	validarContratoPrincipal(){
		let valido = false;

		this.formListaPlanManejoContrato?.value.forEach((x: PlanManejoContrato) => {

			if(x.contratoPrincipal) valido = true;
		});

		return valido;
	}

	listarContratos() {
		let params = {
			codigoEstadoContrato: this.codigoEstadoContrato.VIGENTE,
			codigoTipoContrato: this.codigoTipoContrato.PROGRAMADO_ABREVIADO,
			idUsuarioTitularTH: this.user.idUsuario
		}

		this.apiPlanificacion.listarComboContrato(params).subscribe((result: any) => {
			this.contratos = result.data;
		});
	}

	obtenerContratosPlanTrabajo(idPlanTrabajo: number){
		let params = {
			idPlanManejoContrato: null,
			idPlanManejo: idPlanTrabajo,
			idContrato: null
		  }
		let load = this.dialog.open(LoadingComponent, { disableClose: true });
		this.apiPlanificacion.obtenerPlanTrabajoContrato(params).subscribe(
			(result: any) => {
				load.close();

				if (result.success) {
					this.contratosSeleccionados = [...result?.data];
					let idsContratos: string = "";
					this.contratosSeleccionados.forEach((x: PlanManejoContrato) => {
						if (idsContratos.length > 0)
							idsContratos = idsContratos.concat(',');
						idsContratos = idsContratos.concat(x.idContrato.toString());

					});


					this.obtenerContratosPoligonos(idsContratos);

					this.contratosPrincipales = this.contratosSeleccionados.filter((x: PlanManejoContrato) => x.contratoPrincipal === true);

					if(this.contratosPrincipales && this.contratosPrincipales.length > 0) {
						this.contratosPrincipalesSeleccionados.push(this.contratosPrincipales[0]);
						this.obtenerContrato(this.contratosPrincipales[0].idContrato);
					}

				}
		  	},
			(error: HttpErrorResponse) => {
				load.close();
			  this.toast.error(error.message);
			}
		  )
	}

	obtenerContrato(idContrato: number) {
		// var params = new HttpParams().set("idContrato", String(0));

		let params = {
			idContrato: idContrato,
			idUsuarioTitularTH: null,
			codigoEstadoContrato: this.codigoEstadoContrato.VIGENTE,
			codigoTipoContrato: this.codigoTipoContrato.PROGRAMADO_ABREVIADO
		}

		this.apiPlanificacion.obtenerContrato(params).subscribe(
			(result: any) => {
				if (result.success) {
					if (result.data && result.data.length > 0) {
						this.f.controls['infoGeneral'].reset();
						this.contrato = result.data[0];
						this.isPerNaturalTitular = result.data[0].codigoTipoPersonaTitular === CodigosTiposPersona.NATURAL;
						this.f.controls['infoGeneral'].patchValue({
							...this.contrato
						});
					}
				} else {
					this.f.controls['infoGeneral'].reset();
					this.toast.warn(result.message);
				}
			},
			(error) => {
				this.f.controls['infoGeneral'].reset();
				this.toast.error('Ocurrió un error al traer los datos del contrato seleccionado');
			}
		);
	}

	listarContratosVigentes() {
		this.contratos = [];
		this.apiPlanificacion.listarContratosVigentes(0).subscribe((res) => this.contratos = [...res.data]);
	}

	obtenerContratosPoligonos(idsContratos: string) {
		this.apiPlanificacion.obtenerContratoPoligono(idsContratos).subscribe(
			(result: any) => {
				if (result.success) {
					//this.contratosSeleccionados = [];
					this.mapApi.cleanLayers(this._id, this._view);
					this.visualizarGeometria(result.data);
					//this.contratosSeleccionados = [...result.data];
				}
			},
			(error: HttpErrorResponse) => {
				this.toast.error(error.message);
			}
		)
	}
	visualizarGeometria(data: any) {
		data.forEach((t: any) => {
			if (t.geometry !== null) {
				//let geometry2 =
				//	"POLYGON ((-73.751046211999949 -8.90109354699996, -73.751199184999962 -8.8558786099999338, -73.751295108999955 -8.8274027349999642, -73.751347504999956 -8.8274069189999409, -73.765734878999979 -8.8274545109999281, -73.765773680999985 -8.8157736699999418, -73.751334350999969 -8.81572596899997, -73.751351346999968 -8.8106635629999346, -73.751502699999946 -8.7654484039999261, -73.796953448999943 -8.7655958389999569, -73.796807602999934 -8.81081176999993, -73.796660975999941 -8.8560275909999291, -73.796544736999977 -8.8917027939999684, -73.795301972999937 -8.8932089979999773, -73.794557891999943 -8.895331748999979, -73.794603946999985 -8.8975836599999525, -73.793962389999933 -8.8987662449999334, -73.793384964999973 -8.8991080169999464, -73.791577960999973 -8.8990026819999457, -73.789469155999939 -8.8982090739999649, -73.788473459999977 -8.89698500399993, -73.787602456999934 -8.8946580669999662, -73.786675847999959 -8.8936874289999537, -73.784453332999988 -8.89326419799994, -73.783319869999957 -8.8933238009999513, -73.78292248799994 -8.893530496999972, -73.782434083999988 -8.89427948499997, -73.782361609999953 -8.8954006029999277, -73.782942480999964 -8.8960807399999453, -73.785293585999966 -8.8978427939999278, -73.785513394999953 -8.8989196509999715, -73.785010837999948 -8.8996505099999581, -73.782598768999947 -8.9011980589999666, -73.782528712999977 -8.9012430459999337, -73.780910542999948 -8.9028112629999328, -73.780558968999969 -8.90411232799994, -73.780623945999935 -8.9081729259999634, -73.780068080999968 -8.9098893059999682, -73.77961970299998 -8.9103852089999691, -73.778624298999944 -8.9104452439999591, -73.777300105999984 -8.9092200659999321, -73.776060522999956 -8.9044321589999527, -73.775271245999988 -8.9040407069999787, -73.774370144999978 -8.90411912899998, -73.771014524999941 -8.90550070699993, -73.768303619999983 -8.90715568099995, -73.767307395999978 -8.90915995599994, -73.766427151999949 -8.9133530479999763, -73.765589742999964 -8.9140918049999414, -73.764163921999966 -8.914367403999961, -73.763576183999987 -8.913705302999972, -73.763426710999966 -8.9103226809999683, -73.763079819999973 -8.90953477599993, -73.762214512999947 -8.909613285999967, -73.760020330999964 -8.9112246929999515, -73.758576755999968 -8.91188002399997, -73.757037441999955 -8.9122727779999309, -73.755680300999984 -8.9122411099999681, -73.754194784999981 -8.91168450799995, -73.752960790999964 -8.9107037189999687, -73.75205410999996 -8.9094075169999769, -73.751612771999987 -8.9076516789999687, -73.751235555999983 -8.9070987859999491, -73.75102626599994 -8.9069714799999815, -73.751046211999949 -8.90109354699996))";
				let geoJsonGeometry: any = this.mapApi.wktParse(t.geometry);

				let item = {
					color: this.mapApi.random(),
					title: 'Unidad',
					jsonGeometry: geoJsonGeometry,
					properties: {
						title: 'Unidad'
					}
				}
				let idLayer = this.mapApi.Guid2.newGuid;
				let groupId = this._id;
				let geoJson = this.mapApi.getGeoJson(idLayer, groupId, item);
				this.mapApi.createLayer(geoJson, this._view);
			}
		});
	}

	obtenerAdjuntosRepLegal(idPlanTrabajo: number) {

		let params = {
		  idPlanManejo: idPlanTrabajo
		}
		let load = this.dialog.open(LoadingComponent, { disableClose: true });
		this.apiPlanManejo.listarPlanManejoAnexoArchivo(params)
		.subscribe((response: any) => {
			load.close();

		  if (response.data && response.data.length != 0) {

			let archivos = response.data.find((r: any) => (r.codigoTipo && r.codigoTipo.toUpperCase() === PGMFArchivoTipo.PGMFIG) )

			if (archivos){
				this.fileNombre = archivos.nombreArchivo;
				this.fileRepLegal = archivos;
			}

		  }
		  (error: HttpErrorResponse) => {
			this.toast.error(error.message);
			load.close();
		  };
		});
	  }

	onDescargarArchivo(): void{
		this.dialog.open(LoadingComponent,{ disableClose: true });
		DownloadFile(this.fileRepLegal.documento, this.fileRepLegal.nombreArchivo, 'application/octet-stream');
		this.dialog.closeAll();
	}

	btnCrear(event: any) {

		this.confirmationService.confirm({
			message: 'Este proceso creará el PGMF y no podrá ser eliminado. ¿Desea Continuar?.',
			icon: 'pi pi-exclamation-triangle',
			acceptLabel: 'Sí',
			rejectLabel: 'No',
			key:'confirmGuardado',
			accept: () => {
				this.guardar();
			},

		});
	}
	validarColindancia(){
		let layers:any = this.mapApi.getLayers(this._id,this._view);
		let geometrias:any = [];
		layers.items.forEach((t:any) => {
			geometrias.push(t.attributes[0].geometry)
		});
		let isTouches = this.mapApi.validarGeometriasColindancia(geometrias);
		let item = isTouches.find(
			(e: any) => e == false
		  );
		return item;
	}
	guardar() {
		if(!this.validarContratoPrincipal()) {
			this.toast.warn("(*)Debe seleccionar un contrato como principal.");
			return;
		}

		if (!this.isPerNaturalTitular && !(this.archivo?.archivo instanceof File)) {
			this.toast.warn('(*)Debe Seleccionar un Archivo.');
			return;
		}

		this.dialog.open(LoadingComponent, { disableClose: true });

		if(!this.isPerNaturalTitular){
			this.f.get('infoGeneral.constanciaRepLegal')?.patchValue({
				codigoTipoPGMF: PGMFArchivoTipo.PGMFIG, 
				codigoSubTipoPGMF: PGMFArchivoSubTipo.ARCHIVO,
				idUsuarioRegistro: this.user.idUsuario
			});
			this.guardarArchivoConstancia(this.archivo)
				.pipe(concatMap(() => this.guardarPlanManejo(this.f.getRawValue())))
				.pipe(finalize(() => this.dialog.closeAll()))
				.subscribe(() => this.router.navigateByUrl(`/planificacion/bandeja-PGMF`));
		}else{
			this.archivo = {};
			this.guardarPlanManejo(this.f.getRawValue())
			.pipe(finalize(() => this.dialog.closeAll()))
			.subscribe(() => this.router.navigateByUrl(`/planificacion/bandeja-PGMF`));

		}

	}

	guardarArchivoConstancia(archivo: any) {
		return this.apiArchivo.cargar(this.user.idUsuario, ArchivoTipo.INFO_GENERAL, archivo?.archivo)
			.pipe(tap(res => this.archivo = { idArchivo: res?.data, descripcion: archivo?.archivo?.name, codigoTipoPGMF: PGMFArchivoTipo.PGMFIG, codigoSubTipoPGMF: PGMFArchivoSubTipo.ARCHIVO, idUsuarioRegistro: this.user.idUsuario }));
	}

	guardarPlanManejo(body: any) {
		return this.apiBandejaPO.registrarPlanManejo(body)
			.pipe(tap(handlerSaveResult(this.toast)));
	}

	/*  */
	btnCancelar() {
		const uri = 'planificacion/bandeja-PGMF';
		this.router.navigate([uri])
	}

	verPlan() {
		this.navigate(this.idPGMF);
	  }

	navigate(idPlan: number) {
		const uri = 'planificacion/concesion-forestal-maderables';
		this.router.navigate([uri, idPlan])
	  }

	changeTipoPersonaTitular(tipo: any) {
		if (tipo === "TPERNATU") this.isPerNaturalTitular = true;
		if (tipo === "TPERJURI") this.isPerNaturalTitular = false;
	}

}
