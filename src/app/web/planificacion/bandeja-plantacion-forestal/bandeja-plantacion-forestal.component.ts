import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { includes } from 'lodash';
import { ConfirmationService, MessageService } from 'primeng/api';
import { UsuarioModel } from 'src/app/model/Usuario';

import { SolBandejaPlantacionForestalModel } from 'src/app/model/util/dataDemoMPAFPP';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';

@Component({
  selector: 'app-bandeja-plantacion-forestal',
  templateUrl: './bandeja-plantacion-forestal.component.html',
  styleUrls: ['./bandeja-plantacion-forestal.component.scss'],
  providers: [MessageService]
})
export class BandejaPlantacionForestalComponent implements OnInit {

  ListaBandeja: SolBandejaPlantacionForestalModel[] = [];


  usuario = {} as UsuarioModel;
  dataTable: any[] = [];
  totalRecords: number = 0;
  idSolicitante: number = 0;
  objbuscar = {

    "estado": "",
    "pageNum": 1,
    "pageSize": 10,
    "search": "",
    "idUsuarioRegistro":0,
    "estadoSolPlanta":{
      "id_solplantforestal_estado": (this.idSolicitante==4 || this.idSolicitante==5 ? 1 : 0)
     }
  }

  comboEstados: any[] = [{
    valor: "ENVIADO"
  },
  {
    valor: "APROBADO"
  },
  {
    valor: "OBSERVADO",
  },
  {
    valor: "RECHAZADO"
  }]

  cmbSolicitante = [
    {
      text: "Titular de predio",
      value: 1
    },
    {
      text: "Titular de TH",
      value: 2
    },
    {
      text: "Titular de plantación Costa y Sierra",
      value: 3

    },
    {
      text: "ARFFS",
      value: 4

    },
    {
      text: "EVALUADOR DE ARFFS",
      value: 5

    }
  ]

  verModalCheck: boolean = false;
  titulocheck = 'Registrar'

  tabList: any[] = [{
    valor: "Informacion del solicitante"
  }, {
    valor: "Del area"
  }, {
    valor: "Informacion General del area planteada"
  }, {
    valor: "Detalle de plantacion forestal"
  },
  {
    valor: "Anexo"
  }];


  revisar(data: any) {
    this.router.navigateByUrl('/planificacion/revision-plantacion-forestal', { state: { data: data, accion: 0 } });
  }

  editar(data: any) {
    this.router.navigateByUrl(`/planificacion/registro-plantacion-forestal/${data.id_solplantforest}`);
  }

  loadData(event: any) {
    ;
    this.objbuscar.pageNum = event.first + 1;
    this.objbuscar.pageSize = event.rows;
    this.BandejaSolicitudPermisosForestales(this.objbuscar);
  }

  constructor(private router: Router,
    private confirmationService: ConfirmationService,
    private servPf: PermisoForestalService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    let datoStr: any = localStorage.getItem('plantacionesForestales');
 // ;
 
/*
35	000035	Titular de predio         ==1
36	000036	Titular de TH             ==2
37	000037	Titular de Costa y Sierra ==3
38	000038	ARFFS                     ==4
39	000039	Evaluador de ARFFS        ==5
40	000040	Titular de Plantación     ==6
*/
let idperfil = this.usuario.perfiles[0].idPerfil;
if(idperfil==35)
    this.idSolicitante =1 ;
if(idperfil==36)
    this.idSolicitante =2 ;
if(idperfil==37)
    this.idSolicitante =3 ;
if(idperfil==38)
    this.idSolicitante =4 ;
if(idperfil==39)
    this.idSolicitante =5 ;
if(idperfil==40)
    this.idSolicitante =6 ;

this.objbuscar.idUsuarioRegistro =  (this.idSolicitante==4 || this.idSolicitante==5 ? 0 : this.usuario.idusuario) ;

    this.BandejaSolicitudPermisosForestales(this.objbuscar);
  }

  Limpiar() {
    this.objbuscar = {
      "estado": "",
      "pageNum": 1,
      "pageSize": 10,
      "search": "",
      "idUsuarioRegistro": (this.idSolicitante==4 || this.idSolicitante==5 ? 0 : this.usuario.idusuario) ,
      "estadoSolPlanta":{
        "id_solplantforestal_estado": (this.idSolicitante==4 || this.idSolicitante==5 ? 1 : 0)
       }
    };
  


  }

  BandejaSolicitudPermisosForestales(dato: any) {
  /*  let dto = {
      "estado": dato.estado,
      "pageNum": dato.pageNum,
      "pageSize": dato.pageSize,
      "search": dato.search
    };

    this.objbuscar.estado = dato.estado;


    ;;*/
    this.servPf.BandejaSolicitudPermisosForestales(dato).subscribe(
      (result: any) => {
        //console.log(result);
        if (result.isSuccess) {
          this.ListaBandeja = result.data;
          this.totalRecords = result.totalrecord;
          //this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      }, (error: any) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }

  eliminar(dato: any) {
    let dto = {
      "id_solplantforest": dato.id_solplantforest,
      "idUsuarioElimina": this.usuario.idusuario
    };
    
    this.servPf.EliminarSistemaPlantacionForestal(dto).subscribe(
      (result: any) => {
        //console.log(result);
        if (result.isSuccess) {
          this.BandejaSolicitudPermisosForestales(this.objbuscar);
          //this.messageService.add({ severity: "success", summary: "", detail: result.message });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      }, (error: any) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error.message });
      });
  }


  openEliminarRegistro(event: Event, usuario: any): void {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.eliminar(usuario);

      },
      reject: () => {
        //reject action
      }
    });
  }

  openModalCheck() {
    this.verModalCheck = true;
  }

  guardar() {
    this.verModalCheck = false;
  }

}
