import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PlanificacionService } from '@services';
import { AccionTipo, ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';

@Component({
  selector: 'app-registro-actividad',
  templateUrl: './registro-actividad.component.html',
  styleUrls: ['./registro-actividad.component.scss']
})
export class RegistroActividadComponent implements OnInit {

  actividad: any;
  idGenerado: string = this.generarId();
  MantenedorFormulario: FormGroup;

  constructor(private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private toast: ToastService,
    private dialog: MatDialog,
    private planificacionService: PlanificacionService,
  ) {
    this.MantenedorFormulario = this.initForm();

  }


  ngOnInit(): void {
    this.actividad = this.config.data;
    const { idEvalAprovechamiento, impacto, nombreAprovechamiento, medidasControl } = this.actividad;

    if (this.actividad.accion === AccionTipo.EDITAR) {
      this.MantenedorFormulario.patchValue({ idEvalAprovechamiento, impacto, nombreAprovechamiento, medidasControl });
    }
  }

  initForm(): FormGroup {
    let form: FormGroup;

    form = this.fb.group({
      // idEvalAprovechamiento: null,
      impacto: [null, [Validators.required, Validators.minLength(1)]],
      nombreAprovechamiento: [null, [Validators.required, Validators.minLength(1)]],
      medidasControl: [null, [Validators.required, Validators.minLength(1)]]
    });

    return form;
  }

  generarId() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  guardar() {
    if (this.MantenedorFormulario.invalid){
      this.showFormError();
      return;
    } 
    
    const id = (this.config.data.accion === AccionTipo.REGISTRAR) ? this.idGenerado : this.config.data.idEvalAprovechamiento;
    if (this.config.data.accion === AccionTipo.EDITAR && this.config.data.idEvalAprovechamiento.toString().substr(0, 1) === "_") {
      this.config.data.accion = AccionTipo.REGISTRAR;
    }

    // if (this.tipoModal == TipoModal.Aprovechamiento) {
    const { impacto, nombreAprovechamiento, medidasControl } = this.MantenedorFormulario.value;

    this.actividad = { ...this.actividad, impacto, nombreAprovechamiento, medidasControl, idEvalAprovechamiento: id };
    // this.dialog.closeAll();
    this.ref.close(this.actividad);
  }

  showFormError() {
    const keys = Object.keys(this.MantenedorFormulario.value);
    const valid = this.toast.validAndShowOneToastError<any>(keys, this.MantenedorFormulario.value, ValidacionActividades);
    if (this.MantenedorFormulario.invalid && !valid) return;
  }

  cancelar() {
    this.ref.close();
  }
}



const ValidacionActividades: any = {
  nombreAprovechamiento: 'Actividad',
  impacto: 'Descripción',
  medidasControl: 'Medidas de prevención y/o mitigación, incluido el manejo de residuos sólidos'
}
