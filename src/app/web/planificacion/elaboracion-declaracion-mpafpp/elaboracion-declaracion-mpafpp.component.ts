import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { PlanManejoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
import { CodigosDEMAC } from 'src/app/model/util/DEMAC/CodigosDEMAC';
import { CodigosDEMAP } from 'src/app/model/util/DEMAP/CodigosDEMAP';
import { CodigoEstadoPlanManejo } from 'src/app/model/util/CodigoEstadoPlanManejo';
import { CodigosPMFIP } from 'src/app/model/util/PMFIP/CodigosPMFIP';
import { CodigosPOPMIFP } from 'src/app/model/util/POPMIFP/CodigosPOPMIFP';

@Component({
  selector: 'app-elaboracion-declaracion-mpafpp',
  templateUrl: './elaboracion-declaracion-mpafpp.component.html',
  styleUrls: ['./elaboracion-declaracion-mpafpp.component.scss']
})
export class ElaboracionDeclaracionMpafppComponent implements OnInit {
  idPlan: number = 0;
  tabIndex: number = 0;
  urlBandeja = "/inicio";
  codMedidaGeneral: string = "MPUMFGRAL";
  codigoProceso: string = "";
  tipoProceso!: ICodigoGeneral;
  usuario!: UsuarioModel;

  isValidEnviar: boolean = false;

  tabChange(event: any) {
    this.tabIndex = event;
  }

  tabSiguiente(): void {
    this.tabIndex = this.tabIndex + 1;
  }

  tabAnterior(): void {
    this.tabIndex = this.tabIndex - 1;
  }

  cancelar(): void {
    this.router.navigate([this.urlBandeja]);
  }

  private getUrlBandeja(): void {
    if (this.router.url.indexOf("demap") > -1) {
      this.urlBandeja = '/planificacion/bandeja-pmp-dema';
      this.codigoProceso = "DEMAP";
      this.tipoProceso = CodigosDEMAP;
    } else if (this.router.url.indexOf("pmfip") > -1) {
      this.urlBandeja = '/planificacion/bandeja-pmp-pmfi';
      this.codigoProceso = "PMFIP";
      this.tipoProceso = CodigosPMFIP;
    } else if (this.router.url.indexOf("popmifp") > -1) {
      this.urlBandeja = '/planificacion/bandeja-pmp-po-pmfi';
      this.codigoProceso = "POPMIFP";
      this.tipoProceso = CodigosPOPMIFP;
    } else if (this.router.url.indexOf("demac") > -1) {
      this.urlBandeja = '/planificacion/bandeja-pmc-dema';
      this.tipoProceso = CodigosDEMAC;
      this.codigoProceso = "DEMAC";
    }
  }

  constructor(private router: Router, private activaRoute: ActivatedRoute,
    private usuarioServ: UsuarioService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private planManejoService: PlanManejoService
    ) {
    this.getUrlBandeja();
    let param = window.history.state;
    
    this.idPlan = this.activaRoute.snapshot.paramMap.get('idPlan') ? Number(this.activaRoute.snapshot.paramMap.get('idPlan')) : 0;
    
    if (!this.idPlan) this.router.navigate([this.urlBandeja]);
  }

  remitirInformacion(event: Event){

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "Una vez enviado el plan ya no se podrá editar. ¿Desea Continuar?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        let params =
          {
            idPlanManejo: this.idPlan,
            codigoEstado: (this.codigoProceso == "DEMAP" || this.codigoProceso == "DEMAC") ? CodigoEstadoPlanManejo.FORMULADO : CodigoEstadoPlanManejo.PRESENTADO,
            idUsuarioModificacion: this.usuario.idusuario
          }
    
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planManejoService.actualizarPlanManejoEstado(params).subscribe((result: any) => {
            this.dialog.closeAll();
            if (result.success) {
              this.toastService.ok("Se Remitió Información Correctamente");
              this.router.navigate([this.urlBandeja]);
            } else {
              this.toastService.error("Error al Remitir Información");
            }
          }, (error) => {
            this.dialog.closeAll();
            this.toastService.error("Error al Remitir Información");
          });

      },
    });

  }

  validarEnviar(isValid: boolean) {
    this.isValidEnviar = isValid;
  }


  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
  }

}
