import { Component, OnInit, ViewChild, ElementRef, Renderer2, Input, Output, EventEmitter } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import * as geometryEngine from '@arcgis/core/geometry/geometryEngine';
import { MapApi } from 'src/app/shared/mapApi';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Base64toBlob, DownloadFile, OgcGeometryType, ToastService } from '@shared';
import { HttpErrorResponse } from '@angular/common/http';
import { ArchivoService, UsuarioService } from '@services';
import { MapCustomMPAFPPComponent } from 'src/app/shared/components/map-custom-mpafpp/map-custom-mpafpp.component';
import { CodigosDEMAC } from 'src/app/model/util/DEMAC/CodigosDEMAC';
import { InformacionBasicaDetalle } from 'src/app/model/InformacionAreaManejo';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
declare const shp: any;

@Component({
  selector: 'app-mpafpp-informacion-umf',
  templateUrl: './mpafpp-informacion-umf.component.html',
  styleUrls: ['./mpafpp-informacion-umf.component.scss']
})
export class MpafppInformacionUmfComponent implements OnInit {
  @Input() tipoProceso!: ICodigoGeneral;
  @ViewChild('map') mapCustom!: MapCustomMPAFPPComponent;
  @Input() idDEMAC!: number;
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();
  isPerfilArffs:boolean = false;
  codigoProceso: string = "";
  codigoSubSeccion: string = "";
  public titleUMF = "PGMF";
  public titleVertice = "VERTICES vert_pgmf";
  public filFile: any = null;
  listVertices: any = [];
  txtAreaTotal:any = null;
 // codigoSubSeccion:string = CodigosDEMAC.TAB_2_1;
  constructor(private renderer: Renderer2,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private dialog: MatDialog,
    private serviceArchivo: ArchivoService,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService)
    { }

  ngOnInit(): void {
    this.codigoProceso = this.tipoProceso.CODIGO_PROCESO;
    this.codigoSubSeccion = this.tipoProceso.TAB_2_1;

    this.listarCoordenadas();
    //this.listarVertices();
  }


  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  
  calcularAreaTotal(total:any){
    this.txtAreaTotal  = total.toFixed(2);;
  }
  listarVertices(items:any){
    this.listVertices = [];

    this.listVertices = items;
  }
  guardar(){
    let layers = this.mapCustom._layers;
    let item:any = layers.find(
      (e: any) => e.descripcion == 'TPPUNTO'
    );
    if(item !== undefined){
      if(item.codigo >0){
        this.registrarCoordenadas();
      }else{
        this.toast.warn('Primero guarde el archivo shp de Vértices.')
        return;
      }
    }else if(this.listVertices.length <= 0){
      this.toast.warn('Cargue un archivo shp para listar los vértices.')
    }
  }
  registrarCoordenadas() {
    let listMap: InformacionBasicaDetalle[] = this.listVertices.map((coordenada: any) => {
      return {
        idInfBasica: coordenada.idInfBasica || 0,
        idInfBasicaDet: coordenada.idInfBasicaDet ? coordenada.idInfBasicaDet : 0,
        idUnidadManejo: this.idDEMAC,
        estado: 'A',
        idUsuarioRegistro: this.user.idUsuario,
        codInfBasicaDet: this.codigoSubSeccion,
        codSubInfBasicaDet:  this.codigoProceso,
        coordenadaEsteIni: coordenada.este,
        coordenadaNorteIni: coordenada.norte,
        idUsuarioModificacion: null,
        referencia: coordenada.referencia,
        puntoVertice: coordenada.vertice,
        descripcion: '',
      };
    }
    );
    let params = [
      {
        idInfBasica: this.listVertices[0]?.idInfBasica ? this.listVertices[0]?.idInfBasica : 0,
        codInfBasica:  this.codigoProceso,
        codNombreInfBasica:  this.tipoProceso.TAB_2,
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idDEMAC,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService.registrarInformacionBasica(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success == true) {
        this.toast.ok("Se registró la Información de la Unidad de Manejo Forestal.");
        this.listarCoordenadas();
      } else {
        this.toast.error("Falló el registro de la Información de la Unidad de Manejo Forestal.");
      }
    }, () => {
      this.dialog.closeAll();
    });
  }





  eliminarDetalleVertice(item:any){
    let auxListVertices: any[] = [...this.listVertices];


    if(auxListVertices.length>1){

      let params = {
          idInfBasica: auxListVertices[0].idInfBasica,
          idInfBasicaDet: auxListVertices[0].idInfBasicaDet,
          codInfBasicaDet: this.codigoSubSeccion,
          idUsuarioElimina: this.user.idUsuario
        }
      


      this.informacionAreaPmfiService.eliminarInformacionBasica(params).subscribe((response: any) => {

        if (response.success == true) {
          this.listarCoordenadas();
          // this.toast.ok(response?.message);
          // this.listarCoordenadas();
        } else {
          // this.toast.error(response?.message);
        }
      }, () => {

      });

    }
    this.listVertices = [];


    
  }
  listarCoordenadas(){
    this.listVertices = [];
    var params = {
      idInfBasica:  this.codigoProceso,
      idPlanManejo: this.idDEMAC,
      codCabecera: this.codigoSubSeccion,
    };

    this.informacionAreaPmfiService.listarInformacionBasica(params).subscribe((response: any) => {
      if (response.success && response.data) {
        if(response.data[0].idInfBasicaDet!=null){
          this.listVertices = response.data.map((item: any) => {
            return {
              vertice: item.puntoVertice,
              referencia: item.referencia,
              idInfBasica: item.idInfBasica,
              idInfBasicaDet: item.idInfBasicaDet,
              codInfBasicaDet: item.codInfBasicaDet,
              este: item.coordenadaEste,
              norte: item.coordenadaNorte,
            }
          });
       }
        

      }
    });
  }
}
