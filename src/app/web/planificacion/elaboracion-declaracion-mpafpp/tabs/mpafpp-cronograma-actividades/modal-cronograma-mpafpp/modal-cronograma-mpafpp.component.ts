import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CronogrmaActividadesService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service';

@Component({
  selector: 'app-modal-cronograma-mpafpp',
  templateUrl: './modal-cronograma-mpafpp.component.html',
  styleUrls: ['./modal-cronograma-mpafpp.component.scss']
})
export class ModalCronogramaMpafppComponent implements OnInit {
  isEdit: boolean = false;
  tiempo: any[] = [];
  context: any = {};
  listaFase: any = [
    { label: "Aprovechamiento Maderable", value: "1" },
    { label: "Aprovechamiento Productos diferentes a madera", value: "2" }
  ];

  comboAnios: any[] = [];

  constructor(
    private ref: DynamicDialogRef,
    private dialog: MatDialog,
    private config: DynamicDialogConfig,
    private toast: ToastService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
  ) { }

  ngOnInit(): void {
    this.isEdit = this.config.data.isEdit;

    if (this.config.data.datos) {
      this.context.grupo = this.config.data.datos.grupo;
      this.context.actividad = this.config.data.datos.actividad;
      this.context.idCronogramaActividad = this.config.data.datos.idCronogramaActividad;
      this.context.aniosMeses = this.config.data.datos.aniosMeses;
    }
    
    if (this.isEdit) {
      this.comboAnios = this.config.data.comboAnios;
      this.config.data.datos.aniosMeses.forEach((element: any) => {
        this.tiempo.push(element.aniosMeses);
      });
    }
  }

  guardarTipoActividad() {
    const params = {
      idPlanManejo: this.config.data.idPlan,
      codigoActividad: this.context.grupo === "1" ? "CRONACTDEMACOTROP" : "CRONACTDEMACOTROA",
      actividad: this.context.actividad,
      codigoProceso: this.config.data.codProceso,
      idUsuarioRegistro: this.config.data.idUser,
      anio: null,
      importe: null,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService.registrarCronogramaActividades(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.isSuccess) {
        this.toast.ok(response.message);
        this.ref.close(true);
      } else {
        this.toast.warn(response.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  guardarTipoActividadDetalle() {
    let anosMes = this.context.aniosMeses.toString();
    let params = {
      idCronogramaActividad: this.context.idCronogramaActividad || 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.config.data.idUser,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService.registrarMarcaCronogramaActividadDetalle(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.isSuccess) {
        this.toast.ok(response.message);
        this.ref.close(true);
      } else {
        this.toast.warn(response.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnAgregar() {
    if (this.isEdit) {
      if (this.context.aniosMeses.length == 0) {
        this.context.aniosMeses = this.tiempo;
        this.guardarTipoActividadDetalle();
      } else if (this.context.aniosMeses.length != 0) {
        this.context.aniosMeses.forEach((element: any) => {
          this.tiempo.forEach((x: any, index: number) => {
            if (element.aniosMeses == x) {
              this.tiempo.splice(index, 1);
            }
          });
        });
        this.context.aniosMeses = this.tiempo;
        if (this.tiempo.length != 0) {
          this.guardarTipoActividadDetalle();
        } else {
          this.ref.close(this.isChangeEliminar);
        }
      }

    } else {
      if (!this.validarNuevo()) return;
      this.guardarTipoActividad();
    }
  };

  btnCancelar() {
    this.ref.close();
  }

  validarNuevo(): boolean {
    let valido = true;
    if (!this.context.grupo) {
      valido = false;
      this.toast.warn("(*) Debe seleccionar un aprovechamiento.");
    }

    this.context.actividad = this.context.actividad?.trim();
    if (!this.context.actividad) {
      valido = false;
      this.toast.warn("(*) Debe ingresar la actividad.");
    }

    return valido;
  }

  isChangeEliminar: boolean = false;
  onChange(event: any) {
    if (event.itemValue != undefined) {
      this.context.aniosMeses.forEach((element: any, index: number) => {
        if (event.itemValue != undefined && element.aniosMeses == event.itemValue) {
          let params = {
            idCronogramaActividadDetalle: element.id,
            idUsuarioElimina: this.config.data.idUser,
          };
          this.cronogrmaActividadesService.eliminarCronogramaActividadDetalle(params).subscribe((response: any) => {
            if(response.success){
              this.isChangeEliminar = true;
            }
          });
          this.context.aniosMeses.splice(index, 1);
        }
      });
    } else if (event.itemValue == undefined) {
      let array = [];
      for (var i = 0; i < this.context.aniosMeses.length; i++) {
        var igual = false;
        for (var j = 0; j < event.value.length && !igual; j++) {
          if (this.context.aniosMeses[i].aniosMeses == event.value[j])
            igual = true;
        }
        if (!igual) array.push(this.context.aniosMeses[i]);
      }

      array.forEach((response: any, index: number) => {
        var params = {
          idCronogramaActividadDetalle: response.id,
          idUsuarioElimina: this.config.data.idUser,
        };

        this.cronogrmaActividadesService.eliminarCronogramaActividadDetalle(params).subscribe((response: any) => {
          if(response.success){
            this.isChangeEliminar = true;
          }
        });
      });
    }
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
