import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { CronogrmaActividadesService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service';
import { ModalCronogramaMpafppComponent } from './modal-cronograma-mpafpp/modal-cronograma-mpafpp.component';
import { CodigosDEMAC } from 'src/app/model/util/DEMAC/CodigosDEMAC';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';

@Component({
  selector: 'app-mpafpp-cronograma-actividades',
  templateUrl: './mpafpp-cronograma-actividades.component.html',
  styleUrls: ['./mpafpp-cronograma-actividades.component.scss']
})
export class MpafppCronogramaActividadesComponent implements OnInit {
  @Input() idPlan: number = 0;
  @Input() tipoProceso!: ICodigoGeneral;

  codProceso: string = "";
  usuario!: UsuarioModel;
  listaAproMad: any[] = [];
  listaAproNoMad: any[] = [];
  comboAnios: any [] = [];
  numAnios = Array(3).fill(1);

  MAX_ANIOS = 20;

  constructor(
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }
  
  ngOnInit(): void {
    this.codProceso = this.tipoProceso.CODIGO_PROCESO
    this.listarCronogramaActividad();
    this.armarComboAnios();
  }

  //SERVICIOS
  listarCronogramaActividad() {
    this.listaAproMad = [];
    this.listaAproNoMad = [];
    var params = {
      codigoProceso: this.codProceso,
      idCronogramaActividad: null,
      idPlanManejo: this.idPlan,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService.listarPorFiltroCronogramaActividad(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success && response.data) {
        if (response.data.length > 0) {
          let auxnUM = (response.data[0].cantidadAnio) || 3;
          let auxnUM2 = auxnUM < 3 ? 3 : auxnUM;
          this.numAnios = Array(auxnUM2).fill(1);
          this.armarComboAnios();
          let aux1 = response.data.filter((item: any) => item.grupo === "1");
          let aux2 = response.data.filter((item: any) => item.grupo === "2");
          this.listaAproMad = this.marcar(aux1);
          this.listaAproNoMad = this.marcar(aux2);
        } else {
          this.registrarActividadesInit();
        }
      }
    }, () => this.dialog.closeAll());
  }

  registrarActividadesInit() {
    const params = {
      idPlanManejo: this.idPlan,
      codigoProceso: this.codProceso,
      prefijo: "CRONACTDEMAC",
      idUsuarioRegistro: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService.registrarConfigCronoActi(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        // this.toast.ok(resp.message);
        this.listarCronogramaActividad();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarActividad(id: number) {
    const params = {
      idCronogramaActividad: id,
      idUsuarioElimina: this.usuario.idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService.eliminarCronogramaActividad(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.isSuccess) {
        this.toast.ok(response.message);
        this.listarCronogramaActividad();
      } else {
        this.toast.warn(response.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnAgregar(msj: string, data: any, isEdit: boolean) {
    const ref = this.dialogService.open(ModalCronogramaMpafppComponent, {
      header: msj,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        datos: data,
        isEdit: isEdit,
        idUser: this.usuario.idusuario,
        idPlan: this.idPlan,
        codProceso: this.codProceso,
        comboAnios: this.comboAnios,
      },
    });

    ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarCronogramaActividad();
      }
    });
  }

  openEliminar(event: Event, id: number): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarActividad(id);
      }
    });
  }

  btnAgregarColum() {
    if(this.numAnios.length === this.MAX_ANIOS) {
      this.toast.warn("No se puede agregar mas columnas.")
      return;
    }

    this.numAnios.push(1);

    this.listaAproMad = this.marcar([...this.listaAproMad]);
    this.listaAproNoMad = this.marcar([...this.listaAproNoMad]);
    this.armarComboAnios();
  }

  //FUNCIONES
  marcar(datos: any[]): any[] {

    let dataFinal = datos.map((item: any) => {
      let aux = item.detalle.map((element: any) => {
        return {
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        }
      });

      let aux2 = this.numAnios.map(() => {
        return { mes1: "", mes2: "", mes3: "", mes4: "", mes5: "", mes6: "", mes7: "", mes8: "", mes9: "", mes10: "", mes11: "", mes12: "" }
      });

      return { ...item, aniosMeses: [...aux], listaValues: [...aux2] }
    });

    dataFinal.forEach((item: any, index) => {
      item.aniosMeses.forEach((x: any) => {
        let aux = x.aniosMeses.split("-");
        let fila = Number(aux[0]) - 1;
        let columna = "mes" + aux[1];

        if(item.listaValues[fila]){
          item.listaValues[fila][columna] = "X";
        }
      });
    });

    return dataFinal;
  }

  armarComboAnios() {
    this.comboAnios = this.numAnios.map((item:any, index: number) => {
      let anio = index+1;
      return {
        label: "AÑO " + anio,
        value: anio,
        items: [
          { label: `Año ${anio} - Mes 1`, value:  `${anio}-1`},
          { label: `Año ${anio} - Mes 2`, value:  `${anio}-2`},
          { label: `Año ${anio} - Mes 3`, value:  `${anio}-3`},
          { label: `Año ${anio} - Mes 4`, value:  `${anio}-4`},
          { label: `Año ${anio} - Mes 5`, value:  `${anio}-5`},
          { label: `Año ${anio} - Mes 6`, value:  `${anio}-6`},
          { label: `Año ${anio} - Mes 7`, value:  `${anio}-7`},
          { label: `Año ${anio} - Mes 8`, value:  `${anio}-8`},
          { label: `Año ${anio} - Mes 9`, value:  `${anio}-9`},
          { label: `Año ${anio} - Mes 10`, value:  `${anio}-10`},
          { label: `Año ${anio} - Mes 11`, value:  `${anio}-11`},
          { label: `Año ${anio} - Mes 12`, value:  `${anio}-12`},
        ],
      }
    });
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
