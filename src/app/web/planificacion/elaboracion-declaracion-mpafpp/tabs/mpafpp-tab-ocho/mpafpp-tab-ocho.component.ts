import {HttpErrorResponse} from '@angular/common/http';
import { Component,EventEmitter,Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {UsuarioService} from '@services';
import { ConfirmationService } from 'primeng/api';
import {ToastService} from '@shared';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {MatDialog} from '@angular/material/dialog';
import {OrganizacionActividadModel} from '../../../../../model/OrganizacionActividad';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from "rxjs/operators";
import { ComboModel2 } from 'src/app/model/util/Combo';
import { DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';
import {ActividadesSilviculturalesService} from '../../../../../service/planificacion/plan-general-manejo-pgmfa/actividadesSilviculturales.service';
import {ActividadSilvicultural} from '../../../../../model/ActividadesSilviculturalesModel';
import { validateHorizontalPosition } from '@angular/cdk/overlay';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
@Component({
  selector: 'app-mpafpp-tab-ocho',
  templateUrl: './mpafpp-tab-ocho.component.html',
  styleUrls: ['./mpafpp-tab-ocho.component.scss']
})
export class MpafppTabOchoComponent implements OnInit {

  @Input() idPlan!: number;
  @Input() tipoProceso!: ICodigoGeneral;
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();
  tituloModalMantenimiento:string = "";
  verModalMantenimiento:boolean = false;
  tipoAccion:string = "";

  MantenedorFormulario!: FormGroup;

  MADERABLE :string = "MAD";
  NO_MADERABLE :string = "NMAD";

  CodigoProceso = CodigoProceso;

  idActSilvicultural: number = 0;
  indexUpdate:number = -1;
  tipoTabla:string="";

  listDetalle: ActividadSilvicultural[] = [];

  showOtroActividad:boolean = false;
  comboAprovechamiento:ComboModel2[] = [];
  comboActividad:ComboModel2[] = [];
  actividadAprovechamiento = {} as DataModel;
  lstData1:DataModel[] = [];
  lstData2:DataModel[] = [];

  lstData1_1:DataModel[] = [];
  lstData2_1:DataModel[] = [];

  private cargarCombo(tipo:number)
  {
    let data:any[] = [];
    if(tipo == 1) {
      let dataBase = DataDemoMPAFPP.Actividades08_A;
      data = dataBase.filter(x => this.lstData1.filter(y => y.idActividad == x.value).length == 0);
      data.push({
        text:'Otros',
        value:-1
      });
    }else if(tipo == 2){
      let dataBase = DataDemoMPAFPP.Actividades08_B;
      data = dataBase.filter(x => this.lstData2.filter(y => y.idActividad == x.value).length == 0);

      data.push({
        text:'Otros',
        value:-1
      });
    }
    this.comboActividad = data;

  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  openEliminar(event: Event,index: number,data:DataModel):void{

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Si',
      rejectLabel:'No',
      accept: () => {
         this.removerRegistro(data);
      },
      reject: () => {
      }
   });
  }

  openModal(data:any,index: number,accion: string,tipo:string):void{
    this.tipoTabla = tipo;
    this.tipoAccion = accion;
    //this.cargarCombo();
    this.MantenedorFormulario.reset();
    if(accion =='C')
    {
      this.MantenedorFormulario.patchValue(new DataModel());
      this.tituloModalMantenimiento = "Registrar Actividad de aprovechamiento";
      if(this.tipoTabla===this.MADERABLE)
        this.tituloModalMantenimiento = "Registrar Actividad de aprovechamiento maderable";
      else if(this.tipoTabla===this.NO_MADERABLE)
        this.tituloModalMantenimiento = "Registrar Actividad de aprovechamiento de productos diferentes a la madera";
    }
    else if(accion == 'E')
    {
      this.MantenedorFormulario.patchValue(new DataModel(data));
      //if(data.idActividad > 0) {
      //  this.comboActividad.push({
       //   text:data.actividad,
       //   value:data.idActividad
       // });
      //}
      this.actividadAprovechamiento = new DataModel(data);
      this.indexUpdate = index;

      this.tituloModalMantenimiento = "Editar Actividad de aprovechamiento";
      if(data.observacionDetalle === this.MADERABLE)
        this.tituloModalMantenimiento = "Editar Actividad de aprovechamiento maderable";
      else if(data.observacionDetalle===this.NO_MADERABLE)
        this.tituloModalMantenimiento = "Editar Actividad de aprovechamiento de productos diferentes a la madera";
    }

    this.verModalMantenimiento=true;
  }

  guardarRegistro():void{
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);

    if(this.MantenedorFormulario.valid){

      if(this.tipoAccion == 'C')
      {
        this.registrarActividadAprovechamiento();
      }else{
      this.editarActividadAprovechamiento();
      }

    }


  }

  guardarRegistro2():void{
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);

    if(this.MantenedorFormulario.valid){

        let obj = this.MantenedorFormulario.value;
        if(obj.observaciones == 1){

          if(obj.idActividad > 0){
            obj.actividad = DataDemoMPAFPP.Actividades08_A.filter(x => x.value == obj.idActividad)[0].text;
          }

          if(this.tipoAccion == 'C')
          {
            this.lstData1.push(obj);
          }else{
            let index = this.lstData1.findIndex(x => x.id == obj.id);
            this.lstData1[index] = obj;
          }

        }else if(obj.idTipoAprovecamiento == 2){

          if(obj.idActividad > 0){
            obj.actividad = DataDemoMPAFPP.Actividades08_B.filter(x => x.value == obj.idActividad)[0].text;
          }

          if(this.tipoAccion == 'C')
          {
            this.lstData2.push(obj);
          }else{
            let index = this.lstData2.findIndex(x => x.id == obj.id);
            this.lstData2[index] = obj;
          }

        }



      this.verModalMantenimiento=false;

    }


  }

  validarActividadDuplicado(actAprovechamiento: DataModel, tipo: string, accion: string): boolean{
    let valido: boolean = true;
    let mensaje: string = "";
    let lst: DataModel[] = [];
    let indice: number = -1;

    if(accion == 'C'){
      
      if(tipo == this.MADERABLE){
        if(this.lstData1 != null && this.lstData1.length > 0){

          lst = this.lstData1.filter((item: DataModel) => item.actividad.trim() === actAprovechamiento.actividad.trim());
          if(lst && lst.length > 0) {
            valido = false;
            mensaje = "La actividad de aprovechamiento maderable ingresada ya existe.";
            this.toast.warn(mensaje);
          }
        }
      }else{
        if(this.lstData2 != null && this.lstData2.length > 0){
          lst = this.lstData2.filter((item: DataModel) => item.actividad.trim() === actAprovechamiento.actividad.trim());
          if(lst && lst.length > 0) {
            valido = false;
            mensaje = "La actividad de aprovechamiento no maderable ingresada ya existe.";
            this.toast.warn(mensaje);
          }
        }
      }
    }else if(accion == 'E'){
     
      if(this.indexUpdate>=0){
       
        if(tipo == this.MADERABLE){
          if(this.lstData1 != null && this.lstData1.length > 0){
            indice = this.lstData1.findIndex((item: DataModel) => item.actividad.trim() === actAprovechamiento.actividad.trim());

           
            if(indice > -1 && this.indexUpdate != indice){
              valido = false;
              mensaje = "La actividad de aprovechamiento maderable ingresada ya existe.";
              this.toast.warn(mensaje);
            }
          }
        }else{
          if(this.lstData2 != null && this.lstData2.length > 0){
            indice = this.lstData2.findIndex((item: DataModel) => item.actividad.trim() === actAprovechamiento.actividad.trim());
           
            if(indice > -1 && this.indexUpdate != indice){
              valido = false;
              mensaje = "La actividad de aprovechamiento no maderable ingresada ya existe.";
              this.toast.warn(mensaje);
            }
          }
        }
      }
    }

    return valido;
  }

  registrarActividadAprovechamiento() {

    this.actividadAprovechamiento = new DataModel( this.MantenedorFormulario.value);
    this.actividadAprovechamiento.idActividadSilviculturalDet = 0;
    if(!this.validarActividadDuplicado(this.actividadAprovechamiento,this.tipoTabla,'C')) return;
    if(this.tipoTabla == this.MADERABLE){
      this.actividadAprovechamiento.observacionDetalle = this.MADERABLE;
      this.lstData1.push(this.actividadAprovechamiento);
    }else if(this.tipoTabla == this.NO_MADERABLE){
      this.actividadAprovechamiento.observacionDetalle = this.NO_MADERABLE;
      this.lstData2.push(this.actividadAprovechamiento);
    }
    this.tipoTabla = "";
    this.actividadAprovechamiento = {} as DataModel;
    this.verModalMantenimiento = false;
  }

  editarActividadAprovechamiento() {

    if(this.indexUpdate>=0){
      this.actividadAprovechamiento = new DataModel( this.MantenedorFormulario.value);

      if(!this.validarActividadDuplicado(this.actividadAprovechamiento,this.tipoTabla,'E')) return;

      if(this.actividadAprovechamiento.observacionDetalle == this.MADERABLE){
        this.lstData1[this.indexUpdate] = this.actividadAprovechamiento;
      }
      else if(this.actividadAprovechamiento.observacionDetalle == this.NO_MADERABLE){
        this.lstData2[this.indexUpdate] = this.actividadAprovechamiento;
      }
      this.indexUpdate = -1;
    }

    this.verModalMantenimiento = false;
    this.actividadAprovechamiento = {} as DataModel;
  }

  openEliminarActividadAprovechamiento(event: Event, index: number, data: DataModel, tipo:string) {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {

        this.tipoTabla = tipo;

        if (data.idActividadSilviculturalDet != 0) {
          var params = [
            {
              estado: "I",
              idActividadSilviculturalDet:
              data.idActividadSilviculturalDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesSilviculturalesService.eliminarActividadSilviculturalPFDM(params)
            .subscribe(
              (data: any) => {
                this.toast.ok(data.message);

                if(this.tipoTabla == this.MADERABLE)
                  this.lstData1.splice(index, 1);
                else if(this.tipoTabla == this.NO_MADERABLE)
                  this.lstData2.splice(index, 1);

                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          if(this.tipoTabla == this.MADERABLE)
            this.lstData1.splice(index, 1);
          else if(this.tipoTabla == this.NO_MADERABLE)
            this.lstData2.splice(index, 1);
          this.dialog.closeAll();
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  private removerRegistro(data:DataModel):void{

    if(data.idTipoAprovecamiento == 1){
      const index = this.lstData1.indexOf(data, 0);
      this.lstData1.splice(index,1);
    }else if(data.idTipoAprovecamiento == 2){
      const index = this.lstData2.indexOf(data, 0);
      this.lstData2.splice(index,1);
    }
  }

  constructor(private fb: FormBuilder
    ,private utilSev:UtilitariosService
    ,private dialog: MatDialog
    ,private confirmationService: ConfirmationService
    ,private user: UsuarioService
    ,private actividadesSilviculturalesService: ActividadesSilviculturalesService
    ,private toast: ToastService) {

      this.MantenedorFormulario = this.fb.group({
        id:'',
        idActividadSilviculturalDet: 0,
        //idTipoAprovecamiento: new FormControl('',ValidatorsExtend.comboRequired()),
        idTipoAprovecamiento: new FormControl(''),
        //idActividad: new FormControl('',ValidatorsExtend.comboRequired()),
        idActividad: new FormControl(''),
        actividad: new FormControl('',Validators.compose([Validators.required])),
        descripcion:'',
        descripcionDetalle: new FormControl('',Validators.compose([Validators.required])) ,
        equipo: new FormControl('',Validators.compose([Validators.required])),
        personal: new FormControl('',Validators.compose([Validators.required])),
        observacion: '',
        observacionDetalle: '',
        detalle: '',
      });
      /*
      this.MantenedorFormulario.get('idTipoAprovecamiento')?.valueChanges.subscribe(value =>{
        this.cargarCombo(value);
      });

      this.MantenedorFormulario.get('idActividad')?.valueChanges.subscribe(value =>{
        this.MantenedorFormulario.get('actividad')?.setValue('');
        if(value == -1)
        {
          this.showOtroActividad = true;
        }
        else
        {
          this.showOtroActividad = false;
        }
      });*/

     }

  ngOnInit(): void {
    this.comboAprovechamiento = DataDemoMPAFPP.TipoAprovechamiento08;

    if(this.idPlan) this.listarActividadesAprovechamiento();

    let id = 0;
    DataDemoMPAFPP.Actividades08_A.forEach((item:ComboModel2) => {
      if(item.value > 0){
        id = id +1;
        this.lstData1_1.push(new DataModel({
          id: id.toString()+ 'A',
          idTipoAprovecamiento:1,
          idActividad:item.value,
          actividad:item.text,
          descripcionDetalle: 'descripcion ' + id,
          equipo: 'equipo ' + id,
          personal: 'personal ' + id,
          observacion: 'observacion ' + id,
          detalle: 'detalle ' + id
        }));
      }


    });

    DataDemoMPAFPP.Actividades08_B.forEach((item:ComboModel2) => {
      if(item.value > 0){
        id = id +1;
        this.lstData2_1.push(new DataModel({
          id: id.toString() + 'B',
          idTipoAprovecamiento:2,
          idActividad:item.value,
          actividad:item.text,
          descripcionDetalle: 'descripcion ' + id,
          equipo: 'equipo ' + id,
          personal: 'personal ' + id,
          observacion: 'observacion ' + id,
          detalle: 'detalle ' + id
        }));
      }
    });


  }

  listarActividadesAprovechamiento() {

    this.lstData1 = [];
    this.lstData2 = [];

    let params = {
      idPlanManejo: this.idPlan,
      idTipo: this.tipoProceso.CODIGO_PROCESO
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (!!response.data) {
          if (response.data.detalle.length != 0) {
            this.idActSilvicultural = response.data.idActividadSilvicultural;
            let listado = response.data.detalle;
            listado.forEach((e:any) => {

                //if(e.idActividadSilviculturalDet == null && e.actividad != "Otros (especificar)"){
                //  e.equipo = CodigoUtil.VALOR_DEFECTO
                //}

               if(e.observacionDetalle == 'MAD'){
                 this.lstData1.push(e);
               }else if(e.observacionDetalle == 'NMAD') {
                 this.lstData2.push(e);
               }

            });

          }
        }
      });
  }

  guardarActividadAprovechamiento() {
    if(!this.validarActividadAprovechamiento()) return;

    this.listDetalle = [];

    this.lstData1.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = this.tipoProceso.CODIGO_PROCESO;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    this.lstData2.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = this.tipoProceso.CODIGO_PROCESO;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    var request = {
      aprovechamiento: null,
      divisionAdministrativa: null,
      laboresSilviculturales: {
        idActSilvicultural: this.idActSilvicultural ? this.idActSilvicultural : 0,
        codigoTipoActSilvicultural: this.tipoProceso.CODIGO_PROCESO,
        actividad: null,
        descripcion: null,
        observacion: null,
        idPlanManejo: this.idPlan,
        idUsuarioRegistro: this.user.idUsuario,
        listActividadSilvicultural: this.listDetalle,
      },
    };


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService.guardarSistemaManejoPFDM(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any )=> {
      if (result.success) {
        this.listarActividadesAprovechamiento();
        this.toast.ok('Se registraron las actividades de aprovechamiento correctamente.');
      } else {
        this.toast.warn('Ocurrió un error. No se pudo registrar las actividades de aprovechamiento.');
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  validarActividadAprovechamiento= (): boolean => {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.lstData1.length === 0) {
      validar = false;
      mensaje = "(*) Debe de agregar al menos 1 actividad de aprovechamiento maderable.";
      this.toast.warn(mensaje);
    }

    if (this.lstData2.length === 0) {
      validar = false;
      mensaje = "(*) Debe de agregar al menos 1 actividad de aprovechamiento no maderable";
      this.toast.warn(mensaje);
    }

    return validar;
  };

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
export class DataModel {
  constructor(data?:any) {
    if(data){
      this.id = data.id;
      this.idActividadSilviculturalDet = data.idActividadSilviculturalDet;
      this.idTipoAprovecamiento = data.idTipoAprovecamiento;
      this.idActividad = data.idActividad;
      this.actividad = data.actividad;
      this.descripcion = data.descripcion;
      this.descripcionDetalle = data.descripcionDetalle;
      this.equipo = data.equipo;
      this.personal = data.personal;
      this.observacion = data.observacion;
      this.observacionDetalle = data.observacionDetalle;
      this.detalle = data.detalle;
      return;
    }else{
      this.id =  new Date().toISOString();
    }
  }

  id:string = '';
  idActividadSilviculturalDet: number= 0;
  idTipoAprovecamiento:number = 0;
  idActividad:number = 0;
  actividad:string = "";
  descripcion:string = "";
  descripcionDetalle= "";
  equipo:string = "";
  personal:string = "";
  observacion:string = "";
  observacionDetalle:string = "";
  detalle:string = "";
}
