import {HttpErrorResponse} from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastService } from '@shared';
import { LoadingComponent } from '../../../../../components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ConfirmationService } from 'primeng/api';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { finalize, tap } from 'rxjs/operators';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
import { CodigosDEMAC } from 'src/app/model/util/DEMAC/CodigosDEMAC';

@Component({
  selector: 'app-mpafpp-aprobechamiento-drmf',
  templateUrl: './mpafpp-aprobechamiento-drmf.component.html',
  styleUrls: ['./mpafpp-aprobechamiento-drmf.component.scss']
})
export class MpafppAprobechamientoDrmfComponent implements OnInit {

  tituloModalMantenimiento:string = "";
  verModalMantenimiento:boolean = false;
  tipoAccion:string = "";

  @Input() idPlan!: number;
  @Input() tipoProceso!: ICodigoGeneral;

  MantenedorFormulario!: FormGroup;
  lstData: DataModel[] = [];

  areaTotal: number = 0;
  individuosTotal: number = 0;
  productosTotal: number = 0;

  msjListaDemac: string = "";

  openModal(data:any,tipo:string):void{
    this.tipoAccion = tipo;
    this.MantenedorFormulario.reset();
    if(tipo =='C')
    {      
      this.MantenedorFormulario.patchValue(new DataModel()); 
      this.tituloModalMantenimiento = "Nuevo Registro";
    }
    else if(tipo == 'E')
    {  
      this.MantenedorFormulario.patchValue(new DataModel(data)); 
      this.tituloModalMantenimiento = "Editar Registro";
    }
    this.verModalMantenimiento=true;
  }
  openEliminar(event: Event,data:DataModel):void{
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Si',
      rejectLabel:'No',
      accept: () => {
        this.removerRegistro(data);
      },
      reject: () => {
      }
   });
  }

  guardarRegistro():void{
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
    
    if(this.MantenedorFormulario.valid){ 
      let obj =  this.MantenedorFormulario.value;
      if(this.tipoAccion == 'C')
      {
        this.lstData.push(obj);
      }else{
        let index = this.lstData.findIndex(x => x.id == obj.id);
        this.lstData[index] = obj;
      }
      
      this.verModalMantenimiento=false;
    }
  }

  private removerRegistro(data:DataModel):void{
    const index = this.lstData.indexOf(data, 0);
    this.lstData.splice(index,1);
  }

  constructor(private fb: FormBuilder
    ,private dialog: MatDialog
    ,private utilSev:UtilitariosService
    ,private toast: ToastService
    ,private confirmationService: ConfirmationService
    ,private censoForestalService: CensoForestalService) { }

  ngOnInit(): void {
    this.MantenedorFormulario = this.fb.group({
      id:'',
      nombreComun: new FormControl('',Validators.compose([Validators.required])),
      nombreCientifico: new FormControl('',Validators.compose([Validators.required])),
      nroIndividuo: new FormControl('',Validators.compose([Validators.required])),
      areaProductiva: new FormControl('',Validators.compose([Validators.required])),
      productoExtraer: new FormControl('',Validators.compose([Validators.required])),
      unidadMedida: new FormControl('',Validators.compose([Validators.required])),
      cantidadProducto: new FormControl('',Validators.compose([Validators.required]))
    });

    if(this.tipoProceso.CODIGO_PROCESO !== CodigosDEMAC.CODIGO_PROCESO) {
      this.obtenerInfoAprovechamientoRecursoForestalNoMaderable();
      this.msjListaDemac = ""
    } else {
      this.msjListaDemac = "Esta información no está disponible para planes del tipo DEMA de CONCESIONES.";
    }
  }

  obtenerInfoAprovechamientoRecursoForestalNoMaderable(){

    let request = {
      idPlanManejo: this.idPlan
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService.listarAprovechamientoRecursoForestalNoMaderable(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any )=> {
      if (result.success) {
        this.lstData = result.data;
        this.calcularTotalIndicadores(result.data);
      } else {
        this.toast.warn(result.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  calcularTotalIndicadores(lista: any[]) {

    if(lista && lista.length > 0){
   
      lista.forEach((element: any) => {
        this.areaTotal += Number(element.areaProductiva | 0);
        this.individuosTotal += Number(element.nroIndividuo);
        this.productosTotal += Number(element.cantidadProducto);
      });

    }
  
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
    
  }
}

export class DataModel {
  constructor(data?:any) {
    if(data){
      this.id = data.id;
      this.nombreComun = data.nombreComun;
      this.nombreCientifico = data.nombreCientifico;
      this.nroIndividuo = data.nroIndividuo;
      this.areaProductiva = data.areaProductiva;
      this.productoExtraer = data.productoExtraer;
      this.unidadMedida = data.unidadMedida;
      this.cantidadProducto = data.cantidadProducto;
      return;
    }else{
      this.id =  new Date().toISOString();
    }
  }
  id:string = '';
  nombreComun:string = "";  
  nombreCientifico:string = "";
  nroIndividuo:any = null;
  areaProductiva:any = null;
  productoExtraer:string = "";
  unidadMedida:any = null;
  cantidadProducto:any = null;

}
