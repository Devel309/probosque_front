import { HttpErrorResponse } from "@angular/common/http";
import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { PlanManejoService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { map } from "jquery";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { RecursoForestalService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/recursos-forestales.service";
import { ModalDetalleVolumenesComponent } from "./modal/modal-detalle-volumenes/modal-detalle-volumenes.component";
import { Mensajes } from "src/app/model/util/Mensajes";
import { CensoForestalService } from "src/app/service/censoForestal";

@Component({
  selector: "app-anexo-formato-dema",
  templateUrl: "./anexo-formato-dema.component.html",
  styleUrls: ["./anexo-formato-dema.component.scss"],
})
export class AnexoFormatoDemaComponent implements OnInit {
  @Input() idDEMAC!:number;
  ref!: DynamicDialogRef;
  codigoProceso:string = '';
  codigoSubSeccion:string = '';
  lstAnexoC: DataModel[] = [];
  listaResultados: any[]= [];
  listaEspecie: any[]= [{especie: "especie"},{especie: "especie"}];
  
  @Input() idPGMF!: number;

  usuario!: UsuarioModel;
  idPlanManejo!: number;

  constructor(public dialogService: DialogService,
    private recursoForestalService: RecursoForestalService,
    private dialog: MatDialog,
    private toast: ToastService,
    private route: ActivatedRoute,
    private planManejoService: PlanManejoService,
    private censoForestalService: CensoForestalService,
    private usuarioServ: UsuarioService) {

    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    }

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;

    
    this.listarResultadoCensoForestalEspeciesMaderables();
    this.listarAnexoC();
  }


  listarResultadoCensoForestalEspeciesMaderables(){

    this.recursoForestalService.obtenerInfoAnexoCensoForestalDetalle(this.idPlanManejo)
      .subscribe((response: any )=> {
        
      if(response.success){

        // this.listaResultados = [...response.data]
        
        let auxNombresEspecies =response.data.map((nombre:any)=>nombre.nombreComun);
        
       
        

        const dataArr = new Set(auxNombresEspecies);

        let result = [...dataArr];

        

        let arrayFinal:any =[];
        
        result.forEach((x)=>{
          let aux =  response.data.filter((nombre:any)=>nombre.nombreComun === x);
          let aux2 = {nombreComun: x, lista: aux, row: aux.length+1};
          
          if(aux){
            arrayFinal.push(aux2);
          }

        });
        
        this.listaResultados = [...arrayFinal];

      }else{
        this.dialog.closeAll();
        this.toast.error("Error al información de resultados del censo forestal")
      }
      
     

    }, (error: HttpErrorResponse) => {
      this.toast.error("Error al guardar información en Aprovechamiento de Revursos Forestales Maderables")
  
  
    });

  }

  // remitirInformacion(){
  //   let params =
  //   {
  //     idPlanManejo: this.idPlanManejo,
  //     codigoEstado: "EPLMPRES",
  //     idUsuarioModificacion: this.usuario.idusuario
  //   }
    
  //   let load = this.dialog.open(LoadingComponent, { disableClose: true });
  //   this.planManejoService.actualizarPlanManejoEstado(params).subscribe((result: any) => {
  //     load.close();
  //     if (result.success) {
  //       this.toast.ok("Se Remitió Información Correctamente");
  //     }
  //   }, (error) => {
  //     this.toast.error("Error al Remitir Información");
  //     load.close()
  //   });


  // }

  openModalDetalle(data: any) {

    if(data) {
      data.nroIndividuos = data.idPlanManejo;
      data.idPlan = this.idDEMAC;
    }
    let params = { data: data };
    this.ref = this.dialogService.open(ModalDetalleVolumenesComponent, {
      header: "Actualizar Detalle de Volúmenes",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: params
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarAnexoC();
      }
    });
  }

  listarAnexoC(){

    let request = {
      idPlanManejo: this.idDEMAC
    };


    this.censoForestalService.listarAnexoC(request)
    .subscribe((result: any )=> {
      if (result.success) {
        this.lstAnexoC = result.data;
      } else {
        this.toast.warn(result.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
    
    
  }
}

export class DataModel {
  constructor(data?:any) {
    if(data){
      this.id = data.id;
      this.nombreComun = data.nombreComun;
      this.nombreCientifico = data.nombreCientifico;
      this.nroArbolesCensados = data.nroArbolesCensados;
      this.nroArboles = data.nroArboles;
      this.volumenComercial = data.volumenComercial;
      this.nroArbolesSemillero = data.nroArbolesSemillero;
      return;
    }else{
      this.id =  new Date().toISOString();
    }
  }
  id:string = '';
  nombreComun:string = "";  
  nombreCientifico:string = "";
  nroArbolesCensados:any = null;
  nroArboles:any = null;
  volumenComercial:any = null;
  nroArbolesSemillero:any = null;

}
