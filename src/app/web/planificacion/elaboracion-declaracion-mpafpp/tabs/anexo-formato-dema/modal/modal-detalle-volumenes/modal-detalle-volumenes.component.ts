import {HttpErrorResponse} from '@angular/common/http';
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import {ToastService} from '@shared';
import { finalize } from "rxjs/operators";
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from 'src/app/service/base/usuario.service';
import { PlanManejoService } from 'src/app/service/plan-manejo.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from 'src/app/model/util/Mensajes';
@Component({
  selector: "app-modal-detalle-volumenes",
  templateUrl: "./modal-detalle-volumenes.component.html",
})
export class ModalDetalleVolumenesComponent implements OnInit {
  params_insetar_detalle: any = {};
  usuario!: UsuarioModel;

  f!: FormGroup;

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    private dialog: MatDialog,
    public config: DynamicDialogConfig,
    private user: UsuarioService,
    private toast: ToastService,
    private planManejoService: PlanManejoService
  ) {
    this.usuario = this.user.usuario;
    this.f = this.initForm();
  }

  ngOnInit() {
    this.setInfoDetalleVolumen(this.config.data);
  }

  initForm(): FormGroup {
   

    return this.fb.group({
      idPlanManejoEspecieGrupo: [null, Validators.nullValidator],
      idPlanManejo: [null],
      idCodigoEspecie: [null],
      nroIndividuos: [{ value: null, disabled: true }, Validators.nullValidator],
      nombreComun: new FormControl({ value: '', disabled: true }, Validators.nullValidator),
      nombreCientifico: new FormControl({ value: '', disabled: true }, Validators.nullValidator),
      arbolesArea: new FormControl('',Validators.compose([Validators.required,  Validators.minLength(1)])),
      producto: new FormControl('',Validators.compose([Validators.required,  Validators.minLength(1)])),
      observaciones: new FormControl('',Validators.compose([Validators.required,  Validators.minLength(1)])),
      productoAprovechamiento: new FormControl('',Validators.nullValidator),
      idUsuarioRegistro:  [{ value: null }, Validators.nullValidator]
    });

   
  }

  setInfoDetalleVolumen(dato: any){
    /*this.f.patchValue({
      nombreComun: dato.nombreComun,
      arbolesArea: dato.nroArbolesArea,
      producto: dato.producto,
      observaciones: dato.observaciones
    });
    */
    this.f.patchValue(dato.data);
    this.f.controls["idCodigoEspecie"].setValue(dato.data.nroIndividuos);
    this.f.controls["idPlanManejo"].setValue(dato.data.idPlan);
    this.f.controls["arbolesArea"].setValue(dato.data.nroArbolesArea);
    this.f.controls["productoAprovechamiento"].setValue(dato.data.productoAprov);
    this.f.controls["idUsuarioRegistro"].setValue(this.user.idUsuario);
  }

  guardar = () => {
    /*
    if (this.f.invalid) {
      this.showFormError();
      return;
    }
    */
   if(!this.validarCampos()) return;

    
    this.registrarVolumen();
  };

  validarCampos(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.f.controls["arbolesArea"].value == null || this.f.controls["arbolesArea"].value == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar el N° de Árboles.\n';
    }

    if (this.f.controls["producto"].value == null || this.f.controls["producto"].value == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar el producto.\n';
    }

    if (this.f.controls["observaciones"].value == null || this.f.controls["observaciones"].value == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar las observaciones.\n';
    }

    if(mensaje != "") this.toast.warn(mensaje);

    return validar;
  }

  registrarVolumen(){
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.registrarGrupoEspeciePlanManejo(this.f.value)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any )=> {
      if (result.success) {
        this.ref.close(result.data);
      } else {
        this.toast.warn(result.message);
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));
  }

  showFormError() {
    const keys = Object.keys(this.f.value);
    const valid = this.toast.validAndShowOneToastError<any>(keys, this.f.value, ValidacionCampoError);
    if (this.f.invalid && !valid) return;
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close();
    }
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
    
  }
}

const ValidacionCampoError: any = {
  arbolesArea:"N° de Árboles",
  producto: "producto",
  observaciones: "observaciones"
};
