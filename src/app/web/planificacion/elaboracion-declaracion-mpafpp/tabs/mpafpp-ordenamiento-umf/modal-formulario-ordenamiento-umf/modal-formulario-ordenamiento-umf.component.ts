import { Component, OnInit } from '@angular/core';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-ordenamiento-umf',
  templateUrl: './modal-formulario-ordenamiento-umf.component.html',
  styleUrls: ['./modal-formulario-ordenamiento-umf.component.scss'],
})
export class ModalFormularioOrdenamientoUmfComponent implements OnInit {
  context: any = {};

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  categoriTmp: string = '';
  coberturaTmp: string = '';

  ngOnInit(): void {
    if (this.config.data.type == 'E') {
      this.context = { ...this.config.data.data };
      this.categoriTmp = this.config.data.data.categoria;
      this.coberturaTmp = this.config.data.data.zonaUTM;
    }
  }

  agregar = () => {
    this.context.categoria = this.context.categoria?.trim();
    if (!this.context.categoria) {
      this.toast.warn('El campo Denominación o espacios es obligatorio.');
      return;
    }

    this.ref.close(this.context);
  };

  cerrarModal() {
    if (this.config.data.type == 'E') {
      this.context.categoria = this.categoriTmp;
      this.context.zonaUTM = this.coberturaTmp;
    }

    this.ref.close();
  }
}
