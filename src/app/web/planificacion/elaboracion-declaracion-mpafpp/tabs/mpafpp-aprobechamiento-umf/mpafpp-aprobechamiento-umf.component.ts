import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ArchivoService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import * as moment from "moment";
import { ConfirmationService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { RecursoForestalService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/recursos-forestales.service';
import {environment} from '@env/environment';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { PlantillaConst } from 'src/app/shared/plantilla.const';

@Component({
  selector: 'app-mpafpp-aprobechamiento-umf',
  templateUrl: './mpafpp-aprobechamiento-umf.component.html',
  styleUrls: ['./mpafpp-aprobechamiento-umf.component.scss']
})
export class MpafppAprobechamientoUmfComponent implements OnInit {
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();


  tituloModalMantenimiento:string = "";
  verModalMantenimiento:boolean = false;
  tipoAccion:string = "";
  usuario!: UsuarioModel;
  idPlanManejo!: number;

  MantenedorFormulario!: FormGroup;
  fechaInicio!: Date;
  fechaFin!: Date;
  nroArbolesCensadosTotal: number = 0;
  nroArbolesTotal: number = 0;
  volumenComercialTotal: number = 0;
  nroArbolesSemillerosTotal: number = 0;
  minDate = moment(new Date()).format("YYYY-MM-DD");

  lstData:DataModel[]=[

  ];
  


  // openModal(data:any,tipo:string):void{
  //   this.tipoAccion = tipo;
  //   this.MantenedorFormulario.reset();
  //   if(tipo =='C')
  //   {      
  //     this.MantenedorFormulario.patchValue(new DataModel()); 
  //     this.tituloModalMantenimiento = "Nuevo Registro";
  //   }
  //   else if(tipo == 'E')
  //   {  
  //     this.MantenedorFormulario.patchValue(new DataModel(data)); 
  //     this.tituloModalMantenimiento = "Editar Registro";
  //   }
  //   this.verModalMantenimiento=true;
  // }

  // openEliminar(event: Event,data:DataModel):void{
  //   this.confirmationService.confirm({
  //     target: event.target || undefined,
  //     message: '¿Está seguro de eliminar este registro?',
  //     icon: 'pi pi-exclamation-triangle',
  //     acceptLabel:'Si',
  //     rejectLabel:'No',
  //     accept: () => {
  //       this.removerRegistro(data);
  //     },
  //     reject: () => {
  //     }
  //  });
  // }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  openTab(e: any) {
    if (e.index == 0) {

    } else if (e.index == 1) {
      this.listarRecursosForestalesMaderables();
    }
  }
  listarRecursosForestalesMaderables(){
    let params = {
      idPlanManejo: this.idPlanManejo,
    };
    this.lstData  = [];
    this.nroArbolesTotal =  0;
    this.nroArbolesCensadosTotal = 0;
    this.nroArbolesSemillerosTotal = 0;
    this.volumenComercialTotal = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.recursoForestalService.listarRecursosMaderablesCensoForestalDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any )=> {
      if(response.success){
        let date1 = response.data.strFechaInicio
        let date2 = response.data.strFechaFin
        let datearray1 = date1.split("/");
        let datearray2 = date2.split("/");
        var newdateInicio = datearray1[2] +'/'+ datearray1[1] + '/' + datearray1[0] ;
        var newdateFin = datearray2[2] +'/'+ datearray2[1] + '/' + datearray2[0] ;
        this.fechaInicio = new Date(newdateInicio);
        this.fechaFin = new Date(newdateFin);

        let dataResponse: any[]= [...response.data.listCensoForestalDetalle];
        this.calcularTotalIndicadores([...response.data.listCensoForestalDetalle])
        dataResponse.forEach((dt)=>{
        
        let data = new DataModel();
        data.id =dt.idCensoForestalDetalle;
        data.nombreCientifico =dt.nombreCientifico;
        data.nombreComun =dt.nombreComun;
        data.nroArboles =  dt.nroArboles; 
        data.nroArbolesCensados = dt.nroArbolesCensados;
        data.nroArbolesSemillero = dt.nroArbolesSemilleros;
        data.volumenComercial = dt.volumen;

        this.lstData.push(data);
        });
        //this.toast.ok("Se guardó información en Aprovechamiento de Revursos Forestales Maderables correctamente")
      }else{
        this.dialog.closeAll();
        this.toast.error("Error al guardar información en Aprovechamiento de Revursos Forestales Maderables")
      }
    }, (error: HttpErrorResponse) => {
        this.toast.error("Error al guardar información en Aprovechamiento de Revursos Forestales Maderables")
    });
  }

  calcularTotalIndicadores(lista: any[]) {
    if(lista && lista.length > 0){
      lista.forEach((element: any) => {
        this.nroArbolesTotal += Number(element.nroArboles | 0);
        this.nroArbolesCensadosTotal += Number(element.nroArbolesCensados);
        this.nroArbolesSemillerosTotal += Number(element.nroArbolesSemilleros);
        this.volumenComercialTotal += Number(element.volumen);
      });
    }
  }

  guardar(){
    // if(!this.fechaInicio || !this.fechaFin) {
    //   this.toast.warn("()")
    //   return;
    // }

    if(this.fechaInicio > this.fechaFin) {
      this.toast.warn("La Fecha Fin debe ser mayor o igual a la Fecha Inicio")
      return;
    }

    if(this.lstData.length<=0){
      this.toast.warn("Debe ingresar por lo menos un registro de especies")
      return;
    }

    // this.toast.ok("OK");
    // return;
    let params = {
      idPlanManejo: this.idPlanManejo,
      fechaInicio: this.fechaInicio,
      fechaFin: this.fechaFin,
      idUsuarioModificacion: this.usuario.idusuario
    };
    
    this.dialog.open(LoadingComponent, { disableClose: true });
     this.recursoForestalService.actualizarCensoForestal(params).subscribe( (response: any) => {
      
       if(response.success){

        this.toast.ok("Se registró el aprovechamiento de recursos maderables correctamente");

        this.dialog.closeAll();
      }else{
        this.toast.error("Falló el registro de aprovechamiento de recursos maderables correctamente");
        this.dialog.closeAll();
      }
    });
  }

  guardarRegistro():void{
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
    if(this.MantenedorFormulario.valid){  
      let obj =  this.MantenedorFormulario.value;
      if(this.tipoAccion == 'C'){
        this.lstData.push(obj);
      }else{
        let index = this.lstData.findIndex(x => x.id == obj.id);
        this.lstData[index] = obj;
      }  
      
      this.verModalMantenimiento=false;
    }
  }

  archivoCarga: any = { file: null };
  btnDescargarPlantilla() {
    const NombreGenerado: string = PlantillaConst.N314_TAB_4_0;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
	    if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
	    this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }

  btnCargarPlantilla() {
    if (!(this.archivoCarga.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService.registrarCargaMasivaTipoPlan(this.archivoCarga.file, this.idPlanManejo).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response?.success) {
        this.toast.ok("Se registró el archivo correctamente.");
        this.archivoCarga.file = "";
      } else {
        this.toast.warn(response?.message || Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error("Ocurrió un error.");
    });
  }

  private removerRegistro(data:DataModel):void{
    const index = this.lstData.indexOf(data, 0);
    this.lstData.splice(index,1);
  }

  constructor( private fb: FormBuilder ,
    private recursoForestalService: RecursoForestalService,
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private utilSev:UtilitariosService,
    private confirmationService: ConfirmationService,
    private censoForestalService: CensoForestalService,
    private archivoService: ArchivoService,
  ) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.MantenedorFormulario = this.fb.group({
      id:'',
      nombreComun: new FormControl('',Validators.compose([Validators.required])),
      nombreCientifico: new FormControl('',Validators.compose([Validators.required])),
      nroArbolesCesados: new FormControl('',Validators.compose([Validators.required])),
      nroArboles: new FormControl('',Validators.compose([Validators.required])),
      volumenComercial: new FormControl('',Validators.compose([Validators.required])),
      nroArbolesSemillero: new FormControl('',Validators.compose([Validators.required]))
    });

   }




  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    
    //this.listarRecursosForestalesMaderables();
    
  }

}

export class DataModel {
  constructor(data?:any) {
    if(data){
      this.id = data.id;
      this.nombreComun = data.nombreComun;
      this.nombreCientifico = data.nombreCientifico;
      this.nroArbolesCensados = data.nroArbolesCensados;
      this.nroArboles = data.nroArboles;
      this.volumenComercial = data.volumenComercial;
      this.nroArbolesSemillero = data.nroArbolesSemillero;
      return;
    }else{
      this.id =  new Date().toISOString();
    }
  }
  id:string = '';
  nombreComun:string = "";  
  nombreCientifico:string = "";
  nroArbolesCensados:any = null;
  nroArboles:any = null;
  volumenComercial:any = null;
  nroArbolesSemillero:any = null;

}
