import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { KEY_DEMA_DETALLE, SistemaManejoForestalDetalle } from "@models";
import { removeTagsEditor, ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "form-medida-proteccion.component",
  templateUrl: "./form-medida-proteccion.component.html",
  styleUrls: ["./form-medida-proteccion.component.scss"],
})
export class FormMedidaProteccionComponent implements OnInit {
  f: FormGroup;
  detalle: SistemaManejoForestalDetalle = new SistemaManejoForestalDetalle();
  labelBtn: string = '';

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService,
  ) {
    this.f = this.initForm();
  }

  ngOnInit() {
    this.detalle = this.config.data as SistemaManejoForestalDetalle;
    const { actividades, descripcionSistema } = this.detalle;
    this.f.patchValue({ actividades, descripcionSistema });
    this.labelBtn = this.detalle.idSistemaManejoForestalDetalle == null ? 'Agregar' : 'Editar';
  }

  initForm() {
    return this.fb.group({
      actividades: [null, [Validators.required, Validators.minLength(1)]],
      descripcionSistema: [null, [Validators.required, Validators.minLength(1)]]
    });
  }

  guardar() {
    this.f.controls.actividades.setValue(this.f.controls.actividades.value?.trim() || "");
    this.f.controls.descripcionSistema.setValue(this.f.controls.descripcionSistema.value?.trim() || "");
    if(!this.f.controls.actividades.value) this.toast.warn("(*) Debe ingresar la medida de Protección.");
    if(!this.f.controls.descripcionSistema.value) this.toast.warn("(*) Debe ingresar la descripción.");
    
    if (this.f.invalid) {
      this.f.markAllAsTouched();
      return;
    }

    let { actividades, descripcionSistema } = this.f.value;
    descripcionSistema = removeTagsEditor(descripcionSistema);
    this.detalle = { ...this.detalle, actividades, descripcionSistema };
    this.ref.close(this.detalle);
  }

  cancelar() {
    this.ref.close();
  }
}
