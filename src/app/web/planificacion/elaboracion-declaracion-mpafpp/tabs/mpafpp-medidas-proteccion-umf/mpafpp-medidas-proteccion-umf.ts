import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { concatMap, finalize, tap } from "rxjs/operators";
import { AccionTipo, isNullOrEmpty, ToastService, compareObjects } from '@shared';
import { SistemaManejoForestalService, UsuarioService } from '@services';
import { FormMedidaProteccionComponent } from './form-medida-proteccion/form-medida-proteccion.component';
import { SistemaManejoForestal, SistemaManejoForestalDetalle } from '@models';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-mpafpp-medidas-proteccion',
  templateUrl: './mpafpp-medidas-proteccion-umf.html'
})
export class MpafppMedidasProteccionComponent implements OnInit {
  @Input() idPlan: number = 0;
  @Input() tipoProceso!: ICodigoGeneral;
  @Input() codMedidaGeneral: string = "";
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();
  idPlanManejo: number = 0;

  AccionTipo = AccionTipo;
  medidaProteccion: SistemaManejoForestal = new SistemaManejoForestal({ codigoProceso: this.codMedidaGeneral });
  actividades: SistemaManejoForestalDetalle[] = [];
  pendiente: boolean = false;
  msjGuardarDataLocal: string = Mensajes.GUADAR_DATA_LOCAL;

  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private confirm: ConfirmationService,
    private api: SistemaManejoForestalService,
    private toast: ToastService,
    private user: UsuarioService,
  ) { }

  ngOnInit(): void {
    this.idPlanManejo = this.idPlan;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.obtenerMPUMF(this.idPlanManejo, this.codMedidaGeneral).pipe(finalize(() => this.dialog.closeAll())).subscribe();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  //#region REQUESTS BEGIN
  obtenerMPUMF(idPlanManejo: number, codigoProceso: string) {
    return this.api.obtener(idPlanManejo, codigoProceso)
      .pipe(tap(res => {
        this.medidaProteccion = res?.data;
        this.actividades = res?.data?.detalle as SistemaManejoForestalDetalle[];
        this.setEnviar(this.actividades);
      }))
  }

  guardarSMF(body: SistemaManejoForestal) {
    return this.api.guardar(body)
      .pipe(tap({
        next: (res: any) => this.toast.ok(res?.message ? res.message : 'Se actualizó: Medidas de Protección de Unidad de Manejo Forestal'),
        error: (err: any) => {
          this.toast.error(err?.message ? err.message : 'Ocurrió un error al realizar la operación')
          console.error(err);
        }
      }));
  }

  eliminarSMFDetalle(idDetalle: number | null, idUsuario: number) {
    return this.api.eliminarDetalle(idDetalle, idUsuario)
      .pipe(tap({
        next: (res: any) => this.toast.ok(res?.message ? res.message : 'Se eliminó la Medida de Protección'),
        error: (err: any) => {
          this.toast.error(err?.message ? err.message : 'Ocurrió un error al realizar la operación')
          console.error(err);
        }
      }));
  }
  //#endregion REQUESTS END

  setEnviar(items: SistemaManejoForestalDetalle[]) {
    if (isNullOrEmpty(items)) return;
    items.forEach(x => x.enviar = isNullOrEmpty(x.idSistemaManejoForestalDetalle))
  }

  abrirModal(tipo: AccionTipo, data?: SistemaManejoForestalDetalle, index?: number) {
    const header = `${tipo} Medida de Protección`;
    data = data ? data : new SistemaManejoForestalDetalle({
      codigoProceso: this.codMedidaGeneral,
      codigoTipoDetalle: this.codMedidaGeneral
    });
    const ref = this.dialogService.open(FormMedidaProteccionComponent, { header, data, width: '50vw', closable: true, });

    ref.onClose.subscribe(detalle => {
      if (detalle) {
        if (!compareObjects(data, detalle)) {
          this.pendiente = true;
          detalle.enviar = true;
        }
        this.agregarDetalle(tipo, detalle, index);
      }
    })
  }

  agregarDetalle(accion: AccionTipo, detalle: SistemaManejoForestalDetalle, index?: number) {
    if (accion == AccionTipo.REGISTRAR) {

      let existe = this.actividades.find(e => e.descripcionSistema == detalle.descripcionSistema);

      if (existe != undefined) {
        this.toast.warn('El registro ya existe.');
      } else {
        this.actividades.push(detalle);
      }

    } else {
      if (isNullOrEmpty(detalle.idSistemaManejoForestalDetalle) && index) {
        this.actividades[index] = detalle;
      } else {
        const posicion = this.actividades.findIndex(x =>
          (x.idSistemaManejoForestalDetalle == detalle.idSistemaManejoForestalDetalle));
        this.actividades[posicion] = detalle;
      }
    }
  }

  guardar() {
    
    if(this.actividades.length<=0){
      this.toast.warn("Debe ingresar por lo menos un registro de medidas de Protección de la Unidad de Manejo Forestal")
      return;
    }
    this.medidaProteccion.codigoProceso = this.codMedidaGeneral;
    this.medidaProteccion.seccion = this.tipoProceso.TAB_7;
    this.medidaProteccion.subSeccion = this.tipoProceso.TAB_7_1;
    this.medidaProteccion.idUsuarioRegistro = this.user.idUsuario;
    this.medidaProteccion.detalle = this.actividades.filter(x => x.enviar);

    this.medidaProteccion.detalle.forEach(x => {
      if (x.idSistemaManejoForestalDetalle == null) {
        x.idUsuarioRegistro = this.user.idUsuario;
      } else {
        x.idUsuarioModificacion = this.user.idUsuario;
      }
    })

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarSMF(this.medidaProteccion)
      .pipe(finalize(() => this.dialog.closeAll()),
        concatMap(() => this.obtenerMPUMF(this.idPlanManejo, this.codMedidaGeneral))
      ).subscribe(() => this.pendiente = false);
  }

  eliminar(target: any, id: number, index: number) {
    this.confirm.confirm({
      target,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => this.eliminarRegistro(id, index)
    });
  }

  eliminarRegistro(id: number, index: number) {
    if (isNullOrEmpty(id)) {
      this.actividades.splice(index, 1);
      this.toast.ok('Se eliminó la Medida de Protección con éxito.');
      this.pendiente = false;
      for (const el of this.actividades) {
        if (el?.enviar) {
          this.pendiente = true;
          break;
        }
      }
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.eliminarSMFDetalle(id, this.user.idUsuario)
        .pipe(finalize(() => this.dialog.closeAll()),
          concatMap(() => this.obtenerMPUMF(this.idPlanManejo, this.codMedidaGeneral))
        ).subscribe(() => this.pendiente = false);
    }
  }

}


