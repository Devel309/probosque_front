import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { PlanManejoService, UsuarioService } from "@services";
import { DownloadFile, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { ICodigoGeneral } from "src/app/model/util/codigos/ICodigoGeneral";
import { LeyendaCargaDescargaArchivos } from "src/app/model/util/LeyendaCargaDescargaArchivos";
import { Mensajes } from "src/app/model/util/Mensajes";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service";

@Component({
  selector: "app-tab-carga-descarga-archivos",
  templateUrl: "./tab-carga-descarga-archivos.component.html",
  styleUrls: ["./tab-carga-descarga-archivos.component.scss"],
  providers: [MessageService]
})
export class TabCargaDescargaArchivosComponent implements OnInit {

  @Input('codigoProceso') codigoProceso:string = '';
  @Input() isDisbledObjFormu!: boolean;
  @Input() idPGMF!: number;
  @Input() codigoEstado!: string;
  @Input() codigo!: string;
  @Input() tipoProceso!: ICodigoGeneral;
  @Input() codMedidaGeneral: string = "";
  @Output() EventValidarEnviar: EventEmitter<boolean> = new EventEmitter();

  archivoPDF: any={};

  pendientes: any[] = [];
  verPGMF: boolean = false;
  idPlanManejo!: number;
  usuario!: UsuarioModel;

  isOkFormulario: boolean = false;
  isOkGenerar: boolean = false;
  isOkAdjunto:boolean = false;
  documentoFirmado!: string;
  nombreBtnGenerar : string = '';
  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCIG',
      label: '1. Información General',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCRARPOA',
      label: '2. Información de la Unidad de Manejo Forestal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCOBJ',
      label: '3. Ordenamiento Interno de la Unidad de Manejo Forestal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCIBPC',
      label: '4. Aprovechamiento de Recursos Forestales Maderables',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCOPCPUMF',
      label:
        '5. Aprovechamiento de Recursos Forestales Diferentes a la Madera',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCAAPRO',
      label: '6. Sistema de Manejo y Labores Silviculturales',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCASILV',
      label: '7. Medidas de Protección de la Unidad de Manejo Forestal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCEIA',
      label: '8. Descripción de las Actividades Aprovechamiento',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCMON',
      label: '9. Identificación de Impactos Ambientales Negativos y Establecimiento de Medidas de Prevención y Mitigación',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCPC',
      label: '10. Cronograma de Actividades',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POCCCAPA',
      label: '11. Anexos del Formato de la Dema',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    }
  ];

  validarEnviar() {
    if(this.isOkFormulario && this.isOkAdjunto) this.EventValidarEnviar.emit(true);
    else this.EventValidarEnviar.emit(false);
  }

  registrarArchivoId(event: any){
    if(event) this.isOkAdjunto = true;
    else this.isOkAdjunto = false;
    this.validarEnviar();
  }

  eliminarArchivoId() {
    this.isOkAdjunto = false;
    this.validarEnviar();
  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }
  };

  CodigoProceso = CodigoProceso;
  leyendaCargaDescargaArchivos=LeyendaCargaDescargaArchivos
  constructor(
    private router: Router,
    private messageService: MessageService,
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private usuarioServ: UsuarioService,
    private route: ActivatedRoute,
    private planServ: PlanManejoService,
    private confirmationService: ConfirmationService,
    private toastService: ToastService,
    private planManejoService: PlanManejoService,
    private dialog: MatDialog,
    ) {
      this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    this.documentoFirmado = this.tipoProceso.CODIGO_PROCESO + 'FIRM';
    this.listarEstados();

    const nombre =  this.codigoProceso.substring(0, this.codigoProceso.length-1);
    this.nombreBtnGenerar = 'Generar '+ nombre+' de concesión de PFDM, '+nombre + ' de predio privado';

    sessionStorage.getItem('generoPFDM');
    let d : number = Number(sessionStorage.getItem('generoPFDM')?.toString());

    if (d === this.idPGMF  ) {
      this.isOkGenerar = true;
    }
  }

  generarPDF(){
    let params: any ={
      idPlanManejo: this.idPGMF,
      codigo: this.tipoProceso.CODIGO_PROCESO,
      subCodigoGeneral:this.tipoProceso.TAB_6_1,
      codigoProceso: this.codMedidaGeneral,
      codCabecera:this.tipoProceso.TAB_2_1
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.descargarDeclaracionManejoProductosForestales_PDF(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if(result.success){
          this.archivoPDF={
            archivo : result.archivo,
            nombeArchivo: result.nombeArchivo,
            contenTypeArchivo: result.contenTypeArchivo
          }
          this.isOkGenerar = true;
          sessionStorage.setItem('generoPFDM',''+this.idPGMF);

        }
      },
      (error: HttpErrorResponse) => {
        this.errorMensaje(error.message);
        this.dialog.closeAll();
      }
    );

  }

  descargarPDF(){
    if(this.archivoPDF.archivo) DownloadFile(this.archivoPDF.archivo, this.archivoPDF.nombeArchivo, this.archivoPDF.contenTypeArchivo);
    else this.toastService.warn("Antes debe generar la versión Imprimible " + this.codigoProceso);
  }

  adjuntarPDF(){

  }

  enviarARFFS(){
    this.messageService.add({severity:'success', detail: 'El plan se formuló correctamente'});
  }
  toast(severty: string, msg: string) {
    this.messageService.add({severity: severty, summary: "", detail: msg});
  }

  guardarEstadosEnvio() {

    let idPlanManejoAct = this.idPGMF;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    let codigoP = this.codigoProceso;

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      let marca:any;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
         marca:true;
      }

      let obje = {
        codigo: element.codigo,
        marcar:marca,
        codigoEstado: element.codigoEstado,
        codigoProceso: codigoP, //
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        observacion: "",
      };
      listaEstadosPlanManejo.push(obje);
    });

// CAMBIAR POR EL SERVICIO QUE CORRESPONDA
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == "CPMPEND") {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();
          this.toastService.ok("Se guardó los estados correctamente");
        } else {
          this.toastService.error("Error al guardar los estados");
        }
      }, () => {
        this.dialog.closeAll();
        this.toastService.error("Error al guardar los estados");
      });

  }

  listarEstados() {
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPGMF,
      codigoProceso: this.codigoProceso, //
      idPlanManejoEstado: null,
    };
// CAMBIAR POR EL SERVICIO QUE CORRESPONDA
    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == "CPMPEND") {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;
 //debugger
          if (element.codigoEstado == "CPMPEND") {
            this.listProcesos[objIndex].estado = "Pendiente";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == "CPMTERM") {
            this.listProcesos[objIndex].estado = "Terminado";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });

        this.validarFormularioCompleto();
        this.pendiente();
      });
  }

  validarFormularioCompleto() {
    this.isOkFormulario = this.listProcesos.every(x => x.marcar === true);
    this.validarEnviar();
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPGMF = true;
    } else if (this.pendientes.length == 0) {
      this.verPGMF = false;
    }
  }

  // remitirInformacion(event: Event){

  //   this.confirmationService.confirm({
  //     target: event.target || undefined,
  //     message: "Una vez enviado el plan ya no se podrá editar. ¿Desea Continuar?.",
  //     icon: "pi pi-exclamation-triangle",
  //     acceptLabel: "Sí",
  //     rejectLabel: "No",
  //     accept: () => {
  //       let params =
  //         {
  //           idPlanManejo: this.idPlanManejo,
  //           codigoEstado: "EPLMPRES",
  //           idUsuarioModificacion: this.usuario.idusuario
  //         }

  //         let load = this.dialog.open(LoadingComponent, { disableClose: true });
  //         this.planManejoService.actualizarPlanManejoEstado(params).subscribe((result: any) => {
  //           load.close();
  //           if (result.success) {

  //             this.toastService.ok("Se Remitió Información Correctamente");
  //             this.router.navigate(['/planificacion/bandeja-pmc-dema']);
  //           }
  //         }, (error) => {
  //           this.toastService.error("Error al Remitir Información");
  //           load.close()
  //         });

  //     },
  //     reject: () => {
  //       //reject action
  //     },
  //   });

  // }


  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toastService.error(mensajeError);
  }

}
