import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from '@services';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ManejoBosqueCabecera, ManejoBosqueDetalleModel } from 'src/app/model/ManejoBosque';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { ManejoBosqueService } from 'src/app/service/manejoBosque.service';
import { ToastService } from "@shared";
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';

@Component({
  selector: 'app-mpafpp-sistena-manejo-silvicul',
  templateUrl: './mpafpp-sistena-manejo-silvicul.component.html',
  styleUrls: ['./mpafpp-sistena-manejo-silvicul.component.scss']
})
export class MpafppSistenaManejoSilviculComponent implements OnInit {
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();

  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  tipoAccion: string = "";

  MantenedorFormulario!: FormGroup;

  lstData: DataModel[] = [];

  disabledActividad : boolean = false;

  @Input() idPlan!: number;
  @Input() tipoProceso!: ICodigoGeneral;

  idPlanManejo: number = 0;
  codProceso: string = "";

  tratamientosSilviculturales!: ManejoBosqueCabecera;

  constructor(private fb: FormBuilder
    , private utilSev: UtilitariosService
    , private confirmationService: ConfirmationService
    , private usuarioServ: UsuarioService
    , private manejoBosqueService: ManejoBosqueService
    , private toast: ToastService
    , private messageService: MessageService) {

    this.MantenedorFormulario = this.fb.group({
      id: new FormControl('', Validators.compose([Validators.required])),
      idActividad: new FormControl('', Validators.compose([Validators.required])),
      actividad: new FormControl('', Validators.compose([Validators.required])),
      descripcion: new FormControl('', Validators.compose([Validators.required])),
      equipos: new FormControl('', Validators.compose([Validators.required])),
      insumos: new FormControl('', Validators.compose([Validators.required])),
      personal: new FormControl('', Validators.compose([Validators.required])),
      observacion: new FormControl(''),
      accion: new FormControl(false),
    });
  }

  ngOnInit(): void {
    this.idPlanManejo = this.idPlan;
    
    this.tratamientosSilviculturales = new ManejoBosqueCabecera();
    this.tratamientosSilviculturales.idPlanManejo = this.idPlanManejo;
    this.tratamientosSilviculturales.idUsuarioRegistro = this.usuarioServ.idUsuario;
    this.tratamientosSilviculturales.subCodigoManejo = this.tipoProceso.TAB_6_1;
    this.tratamientosSilviculturales.codigoManejo = this.tipoProceso.CODIGO_PROCESO;

    this.listadoActividades();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  /**
   * Obtener actividades desde back
   */
  public listadoActividades() {

    this.lstData = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: this.tipoProceso.CODIGO_PROCESO,
      subCodigoGeneral: this.tipoProceso.TAB_6_1
    };

    this.manejoBosqueService.listarManejoBosque(params)
      .subscribe((r: any) => {

        r.data.forEach((e: any) => {

          this.tratamientosSilviculturales.idManejoBosque = e.idManejoBosque;
          let id = 0;
          e.listManejoBosqueDetalle.forEach((i: any) => {

            this.lstData.push(new DataModel({
              id: id++,
              idActividad: e.idPlanManejo == null ? 0 : i.idManejoBosqueDet,
              actividad: i.tratamientoSilvicultural,
              descripcion: i.descripcionTratamiento,
              equipos: i.equipo,
              insumos: i.insumo,
              personal: i.personal,
              observacion: i.observacion,
              band: e.idPlanManejo == null,
              accion: i.accion
            }));
          })
        })
      });
  }

  /**
   * Método para persistir registros
   */
  registrar() {

    this.tratamientosSilviculturales.listManejoBosqueDetalle = [];

    let lstDatafinal = this.lstData.filter(r => r.band);

    lstDatafinal.forEach(i => {

      this.tratamientosSilviculturales.listManejoBosqueDetalle.push(new ManejoBosqueDetalleModel({
        idManejoBosqueDet: i.idActividad,
        codtipoManejoDet: this.tipoProceso.TAB_6_1,
        tratamientoSilvicultural: i.actividad,
        descripcionTratamiento: i.descripcion,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        equipo: i.equipos,
        insumo: i.insumos,
        personal: i.personal,
        observacion: i.observacion,
        accion: i.accion,
      }));
    });

    var params = [this.tratamientosSilviculturales];
    this.manejoBosqueService
      .registrarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró el sistema de manejo y labores silviculturales.");
          this.listadoActividades();
        } else {
          this.toast.error(response?.message);
        }
      });

  }

  /**
   * Método para eliminar registro
   * @param event
   * @param data
   */
  openEliminar(event: Event, data: DataModel): void {

    let params = {
      idManejoBosque: this.tratamientosSilviculturales.idManejoBosque,
      idUsuarioElimina: this.usuarioServ.idUsuario,
      idManejoBosqueDet: data.idActividad,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActividad > 0) {
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.lstData.splice(data.id, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.lstData.splice(data.id, 1);
        }
      },
      reject: () => {
      }
    });
  }

  /**
   * Método para la edición del registro
   * @param data
   * @param tipo
   */
  openModal(data: any, tipo: string): void {
    this.tipoAccion = tipo;

    this.MantenedorFormulario.reset();

    if (tipo == 'C') {
      this.MantenedorFormulario.patchValue(new DataModel());
      this.tituloModalMantenimiento = "Nuevo Registro";
      this.MantenedorFormulario.controls['actividad'].enable();
    }
    else if (tipo == 'E') {
      
      
      this.MantenedorFormulario.patchValue(new DataModel(data));
      this.tituloModalMantenimiento = "Editar Registro";
      
      if(data.accion) {
        this.MantenedorFormulario.controls['actividad'].disable();
      }
      
    }

    this.verModalMantenimiento = true;
  }

  /**
   * Método para guardar registro local
   */
  guardarRegistro(): void {
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
    
    if (this.MantenedorFormulario.valid) {
      this.MantenedorFormulario.controls['actividad'].enable();

      let obj = this.MantenedorFormulario.value;

      if (this.tipoAccion == 'C') {

        this.lstData.push(new DataModel({
          id: this.lstData.length,
          idActividad: 0,
          actividad: obj.actividad,
          descripcion: obj.descripcion,
          equipos: obj.equipos,
          insumos: obj.insumos,
          personal: obj.personal,
          observacion: obj.observacion,
          band: true,
          accion: false,
        }));

      } else {
        let index: any = this.lstData.findIndex(x => x.id == obj.id);
        

        this.lstData[index] = new DataModel({
          id: obj.id,
          idActividad: obj.idActividad,
          actividad: obj.actividad,
          descripcion: obj.descripcion,
          equipos: obj.equipos,
          insumos: obj.insumos,
          personal: obj.personal,
          observacion: obj.observacion,
          band: true,
          accion: obj.accion,
        });
      }

      this.verModalMantenimiento = false;

    }
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

}

export class DataModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.idActividad = data.idActividad;
      this.actividad = data.actividad;
      this.descripcion = data.descripcion;
      this.equipos = data.equipos;
      this.insumos = data.insumos;
      this.personal = data.personal;
      this.observacion = data.observacion;
      this.band = data.band;
      this.accion = data.accion;
      return;
    }
  }
  id: number = 0;
  idActividad: number = 0;
  actividad: string = "";
  descripcion: string = "";
  equipos: string = "";
  insumos: string = "";
  personal: string = "";
  observacion: string = "";
  band: boolean = true;
  accion: boolean | null = false;

}
