import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { DepartamentoModel, DistritoModel, ProvinciaModel } from '@models';
import {
  CoreCentralService,
  PlanManejoService,
  UsuarioService,
} from '@services';
import { MapApi, ToastService } from '@shared';
import { MapCustomMPAFPPComponent } from 'src/app/shared/components/map-custom-mpafpp/map-custom-mpafpp.component';
import { InformacionBasicaDetalle } from 'src/app/model/InformacionAreaManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
import { GenericoService } from 'src/app/service/generico.service';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ColindanciaLimitePredio } from 'src/app/model/PlanManejo/permiso-concesion/colindancia-limite-predio';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { PropietarioPredioConPer } from 'src/app/model/PlanManejo/permiso-concesion/propietario-predio-con-per';
import { UbicacionPredioConPer } from 'src/app/model/PlanManejo/permiso-concesion/ubicacion-predio-con-per';

@Component({
  selector: 'app-mpafpp-informacion-general',
  templateUrl: './mpafpp-informacion-general.component.html',
  styleUrls: ['./mpafpp-informacion-general.component.scss'],
})
export class MpafppInformacionGeneralComponent implements OnInit {
  @Input() idPlan: number = 0;
  @Input() tipoProceso!: ICodigoGeneral;

  @ViewChild('map') mapCustom!: MapCustomMPAFPPComponent;
  listVertices: any = [];
  tipoGeometria!: string;
  codigoProceso: string = '';
  codigoSubSeccion: string = '';

  usuarioM = {} as UsuarioModel;
  listDepartamento: DepartamentoModel[] = [];
  listProvincia: ProvinciaModel[] = [];
  listDistrito: DistritoModel[] = [];
  requestPropietario = new PropietarioPredioConPer();
  requestUbicacion = new UbicacionPredioConPer();
  listaColindancia: ColindanciaLimitePredio[] = [];
  txtAreaTotal: any = null;

  disabledUbigeo: boolean = false;

  constructor(
    private servCoreCentral: CoreCentralService,
    private mapApi: MapApi,
    private toast: ToastService,
    private dialog: MatDialog,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService,
    private planManejoService: PlanManejoService,
    private genericoService: GenericoService,
    private informacionGeneralService: InformacionGeneralService
  ) {
    this.usuarioM = this.user.usuario;
  }

  ngOnInit(): void {
    this.codigoProceso = this.tipoProceso.CODIGO_PROCESO;
    this.codigoSubSeccion = this.tipoProceso.TAB_1_3;

    this.listarInfoBasica();
    this.listarColindanciaPredio();
    this.listarInfoGeneral(0);
  }

  

  /************SERVICIOS*********************/
  listarInfoBasica() {
    const params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlan,
      codCabecera: this.tipoProceso.TAB_1_3,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success && response.data) {
          this.listVertices = response.data.map((item: any) => {
            return {
              vertice: item.puntoVertice,
              referencia: item.referencia,
              idInfBasica: item.idInfBasica,
              idInfBasicaDet: item.idInfBasicaDet,
              codInfBasicaDet: item.codInfBasicaDet,
              este: item.coordenadaEste,
              norte: item.coordenadaNorte,
            };
          });
        }
      });
  }

  //BLOQUE 1.1 y 1.2
  listarInfoGeneral(tab: number) {
    const params = { idPlanManejo: this.idPlan };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.isSuccess && resp.data) {
            if (tab === 1 || tab === 0) this.setDataPropietario(resp.data);
            if (tab === 2 || tab === 0) this.setDataUbicacion(resp.data);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  registrarInfoGeneral(params: any, tab: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .registrarInformacionGeneralDema(params)
      .subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.toast.ok(
              'Se registró los puntos de Ubicación en Coordenadas UTM del Predio.'
            );
            this.listarInfoGeneral(tab);
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }
  registrarInfoGeneral1(params: any, tab: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .registrarInformacionGeneralDema(params)
      .subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.toast.ok(
              'Se registró la Ubicación  del Predio.'
            );
            this.listarInfoGeneral(tab);
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }
  actualizararInfoGeneral(params: any, tab: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.toast.ok(
              'Se actualizó los puntos de Ubicación en Coordenadas UTM del Predio.'
            );
            this.listarInfoGeneral(tab);
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }
  actualizararInfoGeneral1(params: any, tab: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.toast.ok(
              'Se actualizó la Ubicación  del Predio.'
            );
            this.listarInfoGeneral(tab);
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  //BLOQUE 1.2
  listarPorFiltroDepartamento() {
    this.servCoreCentral
      .listarPorFiltroDepartamento({})
      .subscribe((result: any) => {
        this.listDepartamento = result.data;
        this.requestUbicacion.idDepartamentoTitular =
          this.requestUbicacion.idDepartamentoTitular;
        this.listarPorFilroProvincia();
      });
  }

  listarPorFilroProvincia() {
    const params = {
      idDepartamento: this.requestUbicacion.idDepartamentoTitular,
    };
    this.servCoreCentral
      .listarPorFilroProvincia(params)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
        this.requestUbicacion.idProvinciaTitular =
          this.requestUbicacion.idProvinciaTitular;
        this.listarPorFilroDistrito();
      });
  }

  listarPorFilroDistrito() {
    const params = { idProvincia: this.requestUbicacion.idProvinciaTitular };
    this.servCoreCentral
      .listarPorFilroDistrito(params)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
        this.requestUbicacion.idDistritoTitular =
          this.requestUbicacion.idDistritoTitular;
      });
  }

  //BLOQUE 1.3

  calcularAreaTotal(total: any) {
    this.txtAreaTotal = total.toFixed(2);
  }
  //BLOQUE 1.4
  listarColindanciaPredio() {
    const params = { idPlanManejo: this.idPlan };
    this.planManejoService.listarColindanciaPredio(params).subscribe((resp) => {
      if (resp.success && resp.data) {
        if (resp.data.length > 0) {
          this.listaColindancia = resp.data;
        } else {
          this.listarColindanciaParametros();
        }
      }
    });
  }

  listarColindanciaParametros() {
    this.genericoService
      .listarPorFiltroParametro({ prefijo: 'TCOLIM' })
      .subscribe((resp: any) => {
        if (resp.success) {
          this.listaColindancia = resp.data.map((x: any) => {
            let aux = new ColindanciaLimitePredio();
            aux.idPlanManejo = this.idPlan;
            aux.codTipoColindancia = x.codigo;
            aux.tipoColindancia = x.valorPrimario;
            aux.idUsuarioRegistro = this.usuarioM.idusuario;
            return aux;
          });
        }
      });
  }

  registrarColindanciaPredio() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService
      .registrarColindanciaPredio(this.listaColindancia)
      .subscribe(
        (resp) => {
          this.dialog.closeAll();
          if (resp.success) {
            this.toast.ok(
              'Se actualizó la colindancia y límites del predio correctamente.'
            );
            this.listarColindanciaPredio();
          } else {
            this.toast.warn(resp.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  /******************************************/
  /************BOTONES*********************/
  btnGuardarPropietario() {
    if (!this.requestPropietario.nombreCompleto) {
      this.toast.warn('No se pudo obtener el nombre del propietario.');
      return;
    }
    if (this.requestPropietario.idInformacionGeneralDema) {
      this.requestPropietario.idUsuarioModificacion = this.usuarioM.idusuario;
      this.actualizararInfoGeneral(this.requestPropietario, 1);
    } else {
      this.requestPropietario.idUsuarioRegistro = this.usuarioM.idusuario;
      this.registrarInfoGeneral(this.requestPropietario, 1);
    }
  }

  btnGuardarUbicacion() {
    // if (!this.requestUbicacion.cuenca) {
    //   this.toast.warn("(*) Debe de agregar el caserío.");
    //   return;
    // }
    if (!this.requestUbicacion.idDepartamentoTitular) {
      this.toast.warn('(*) Debe seleccionar el departamento.');
      return;
    }
    if (!this.requestUbicacion.idProvinciaTitular) {
      this.toast.warn('(*)  Debe seleccionar la provincia.');
      return;
    }
    if (!this.requestUbicacion.idDistritoTitular) {
      this.toast.warn('(*)  Debe seleccionar el distrito.');
      return;
    }
    /* this.requestUbicacion.zona;
    this.requestUbicacion.datum;
    this.requestUbicacion.horizontal; */
    if (this.requestUbicacion.idInformacionGeneralDema) {
      this.requestUbicacion.idUsuarioModificacion = this.usuarioM.idusuario;
      this.requestUbicacion.idPlanManejo = this.idPlan;
      this.actualizararInfoGeneral1(this.requestUbicacion, 2);
    } else {
      this.requestUbicacion.idUsuarioRegistro = this.usuarioM.idusuario;
      this.requestUbicacion.idPlanManejo = this.idPlan;
      this.registrarInfoGeneral1(this.requestUbicacion, 2);
    }
  }

  btnGuardarUbicacionUTM() {
    
    this.btnGuardarCoordenadas(false);
    this.guardarPuntosUbicacion();
  }

  guardarPuntosUbicacion() {
    let param: any = {
      idInformacionGeneralDema: this.requestUbicacion.idInformacionGeneralDema,
      zona: this.requestUbicacion.zona,
      datum: this.requestUbicacion.datum,
      horizontal: this.requestUbicacion.horizontal,
      areaTotal: this.txtAreaTotal,
    };

    if (param.idInformacionGeneralDema) {
      param.idUsuarioRegistro = this.usuarioM.idusuario;
      param.idPlanManejo = this.idPlan;
      this.actualizararInfoGeneral(param, 2);
    } else {
      param.idUsuarioRegistro = this.usuarioM.idusuario;
      param.idPlanManejo = this.idPlan;
      this.registrarInfoGeneral(param, 2);
    }
  }

  btnGuardarCoordenadas(mostrarMensaje?:boolean) {
    let layers = this.mapCustom._layers;
    let item: any = layers.find((e: any) => e.descripcion == 'TPPUNTO');
    if (item !== undefined) {
      if (item.codigo > 0) {
        this.registrarCoordenadas(mostrarMensaje);
      } else {
        this.toast.warn('Primero guarde el archivo shp de Vértices.');
        return;
      }
    } else if (this.listVertices.length <= 0) {
      this.toast.warn('Cargue un archivo shp para listar los vértices.');
    }
  }

  btnGuardarColindancia() {
    let valido = true;
    this.listaColindancia.forEach((item) => {
      if ((!item.descripcionColindante || !item.descripcionLimite) && valido) {
        valido = false;
        this.toast.warn('(*) Debe ingresar todos los campos.');
      }
    });
    if (valido) this.registrarColindanciaPredio();
  }
  /******************************************/
  /************FUNCIONES*********************/
  setDataPropietario(datos: any[]) {
    if (datos.length > 0) {
      let auxData = datos[0];
      this.requestPropietario.setData(auxData);

    }
    
  }

  setDataUbicacion(datos: any[]) {
    if (datos.length > 0) {
      let auxData = datos[0];
      this.requestUbicacion.setData(auxData);
      if (this.requestUbicacion.idDepartamentoTitular)
        this.disabledUbigeo = true;
    }
    this.listarPorFiltroDepartamento();
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    this.requestUbicacion.idDepartamentoTitular = param.value;
    this.requestUbicacion.idProvinciaTitular = null;
    this.requestUbicacion.idDistritoTitular = null;
    this.listarPorFilroProvincia();
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    this.requestUbicacion.idProvinciaTitular = param.value;
    this.requestUbicacion.idDistritoTitular = null;
    this.listarPorFilroDistrito();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  /******************************************/
  listarVertices(items: any) {
    this.listVertices = items;
  }
  onChangeFileSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (this.listVertices.length >= 1) {
      this.toast.warn('Ya se encuentra registros para la tabla.');
      return;
    }
    let controls = this.mapApi.validateFileInputSHP(e.target.files);
    if (controls.success == false) {
      this.toast.warn('Archivo no válido!');
      return;
    }

    this.mapCustom.onChangeFile(e, 'TPPUNTO', 'TPPUNTO');
  }
  createTablaVertices(items: any) {
    this.listVertices = [];
    items.forEach((t: any) => {
      this.listVertices.push({
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
      });
    });
  }

  eliminarDetalleVertice(item: any) {
    this.listVertices = [];
    
  }
  registrarCoordenadas(mostrarMensaje?:boolean) {
    let listMap: InformacionBasicaDetalle[] = this.listVertices.map(
      (coordenada: any) => {
        return {
          idInfBasica: coordenada.idInfBasica || 0,
          idInfBasicaDet: coordenada.idInfBasicaDet
            ? coordenada.idInfBasicaDet
            : 0,
          idUnidadManejo: this.idPlan,
          estado: 'A',
          idUsuarioRegistro: this.usuarioM.idusuario,
          codInfBasicaDet: this.tipoProceso.TAB_1_3,
          codSubInfBasicaDet: this.tipoProceso.TAB_1,
          coordenadaEsteIni: coordenada.este,
          coordenadaNorteIni: coordenada.norte,
          idUsuarioModificacion: null,
          referencia: coordenada.referencia,
          puntoVertice: coordenada.vertice,
          descripcion: '',
        };
      }
    );
    let params = [
      {
        idInfBasica: this.listVertices[0]?.idInfBasica
          ? this.listVertices[0]?.idInfBasica
          : 0,
        codInfBasica: this.codigoProceso,
        codNombreInfBasica: this.tipoProceso.TAB_1,
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idPlan,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .subscribe(
        (response: any) => {
          this.dialog.closeAll();
          if (response.success == true) {
            if(mostrarMensaje)
            this.toast.ok(response?.message);
            this.listarCoordenadas();
          } else {
            if(mostrarMensaje)
            this.toast.error(response?.message);
          }
        },
        () => {
          this.dialog.closeAll();
        }
      );
  }

  listarCoordenadas() {
    this.listVertices = [];
    var params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlan,
      codCabecera: this.tipoProceso.TAB_1_3,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success && response.data) {
          this.listVertices = response.data.map((item: any) => {
            return {
              vertice: item.puntoVertice,
              referencia: item.referencia,
              idInfBasica: item.idInfBasica,
              idInfBasicaDet: item.idInfBasicaDet,
              codInfBasicaDet: item.codInfBasicaDet,
              este: item.coordenadaEste,
              norte: item.coordenadaNorte,
            };
          });
        }
      });
  }
}
