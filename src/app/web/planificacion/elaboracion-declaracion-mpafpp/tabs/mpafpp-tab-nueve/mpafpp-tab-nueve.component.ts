import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PlanificacionService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ICodigoGeneral } from 'src/app/model/util/codigos/ICodigoGeneral';
import { ComboModel2 } from 'src/app/model/util/Combo';
import { DataDemoMPAFPP } from 'src/app/model/util/dataDemoMPAFPP';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';
import { AccionTipo, PGMFArchivoTipo } from 'src/app/shared/enums';
import { RegistroActividadComponent } from '../../modal/registro-actividad/registro-actividad.component';

@Component({
  selector: 'app-mpafpp-tab-nueve',
  templateUrl: './mpafpp-tab-nueve.component.html',
  styleUrls: ['./mpafpp-tab-nueve.component.scss']
})
export class MpafppTabNueveComponent implements OnInit {
  @Input() idPlan: number = 0;
  @Input() tipoProceso!: ICodigoGeneral;
  @Input() codMedidaGeneral: string = "";
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();
  
  idPlanManejo: number = 0;
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  tipoAccion: string = "";
  AccionTipo = AccionTipo;
  MantenedorFormulario!: FormGroup;

  showOtroActiviad: boolean = false;
  // comboActividades:ComboModel2[] = [];
  lstData: any = [];
  totalRecords: number = 0;
  msjGuardarDataLocal: string = Mensajes.GUADAR_DATA_LOCAL;

  constructor(private fb: FormBuilder
    , private utilSev: UtilitariosService
    , private confirmationService: ConfirmationService
    , public dialogService: DialogService
    , private planificacionService: PlanificacionService
    , private dialog: MatDialog
    , private user: UsuarioService
    , private messageService: MessageService
    , private toast: ToastService
  ) {
    this.MantenedorFormulario = this.fb.group({
      id: '',
      idActividad: new FormControl('', ValidatorsExtend.comboRequired()),
      actividad: new FormControl(''),
      descipcion: new FormControl('', Validators.compose([Validators.required])),
      medidas: new FormControl('', Validators.compose([Validators.required]))
    });

    this.MantenedorFormulario.get('idActividad')?.valueChanges.subscribe(value => {
      this.MantenedorFormulario.get('actividad')?.setValue('');
      if (value == -1) {
        this.showOtroActiviad = true;
      }
      else {
        this.showOtroActiviad = false;
      }
    });
  }

  ngOnInit(): void {
    this.idPlanManejo = this.idPlan;
    this.getData();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  generarId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  getData() {
    this.listarActividades().subscribe((res: any) => {
      if (res?.data && res?.data !== undefined) {
        res.data.forEach((e: any) => {
          e.accion = (e.idEvalAprovechamiento === null) ? AccionTipo.REGISTRAR : AccionTipo.EDITAR;
          e.idEvalAprovechamiento = (e.idEvalAprovechamiento === null) ? this.generarId() : e.idEvalAprovechamiento;

          this.lstData.push(e);
        });
        this.totalRecords = this.lstData.length;
      }
    });
  }

  listarActividades() {
    this.lstData = [];

    const body = { idPlanManejo: this.idPlanManejo, tipoAprovechamiento: this.tipoProceso.CODIGO_PROCESO };
    return this.planificacionService.getActividades(body)
      .pipe(
        finalize(() => this.dialog.closeAll()),
      );
  }

  openEliminar(event: Event, data: any): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.eliminar(data);
      },
      reject: () => {
      }
    });
  }

  eliminar(data: any) {
    if (data) {
      if (data.accion !== AccionTipo.REGISTRAR) {
        this.dialog.open(LoadingComponent, { disableClose: true });
        data.idUsuarioRegistro = this.user.idUsuario;
        this.planificacionService.eliminarEvaluacionAmbientalAprovechamiento(data)
          .pipe(finalize(() => this.dialog.closeAll())
            , tap({
              next: (res: any) => {
                this.toast.ok("Se eliminó la actividad que genera impacto correctamente");
                this.lstData = this.lstData.filter((item: any) => item.idEvalAprovechamiento !== data.idEvalAprovechamiento);
              },
              error: (detail) => {
                this.toast.error("Error al eliminar la actividad que genera impacto");
              }
            })
          )
          .subscribe();
      } else {
        this.toast.ok("Se eliminó el registro con éxito.");
        this.lstData = this.lstData.filter((item: any) => item.idEvalAprovechamiento !== data.idEvalAprovechamiento);
      }
    }
  }

  openModal(tipo: AccionTipo, data?: any): void {
    const header = `${tipo} Registro`;
    let dataSend = { ...data, accion: tipo }

    const config = { header, data: dataSend, width: '50vw', closable: true };
    const ref = this.dialogService.open(RegistroActividadComponent, config);

    ref.onClose.subscribe((actividad: any) => {
      if (actividad) {
        if (tipo === AccionTipo.REGISTRAR) {
          actividad.idPlanManejo = this.idPlanManejo;
          actividad.idUsuarioRegistro = this.user.idUsuario;

          this.lstData.push(actividad);
        }
        else {
          let index = this.lstData.findIndex((x: any) => x.idEvalAprovechamiento == actividad.idEvalAprovechamiento);
          if (index >= 0) {
            this.lstData[index] = actividad;
          }
        }
      }
    });
  }

  grabarActividades(): void {
    let data1: EvaluacionAmbientalAprovechamiento[] = [];
    
    if (this.lstData !== undefined) {
      this.lstData.forEach((e: any) => {
        if (e.accion == AccionTipo.REGISTRAR) e.idEvalAprovechamiento = null;
        if (e.accion === AccionTipo.REGISTRAR) {
          e.idPlanManejo = this.idPlanManejo;
          e.idUsuarioRegistro = this.user.idUsuario;
          e.codTipoAprovechamiento = this.tipoProceso.CODIGO_PROCESO;
        }
        data1.push(new EvaluacionAmbientalAprovechamiento(e));
      });
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService.registrarListEvaluacionAmbiental(data1)
      .pipe(

        finalize(() => this.dialog.closeAll()),
      )
      .subscribe((res: any) => {
        this.toast.ok("Se guardó la identificación de impactos ambientales");
        this.getData();
      });
  }
}

export class EvaluacionAmbientalAprovechamiento {
  constructor(data?: any) {
    if (data) {
      this.idEvalAprovechamiento = data.idEvalAprovechamiento;
      this.codTipoAprovechamiento = data.codTipoAprovechamiento;
      this.tipoAprovechamiento = data.tipoAprovechamiento;
      this.tipoNombreAprovechamiento = data.tipoNombreAprovechamiento;
      this.nombreAprovechamiento = data.nombreAprovechamiento;
      this.impacto = data.impacto;
      this.medidasControl = data.medidasControl;
      this.medidasMonitoreo = data.medidasMonitoreo;
      this.frecuencia = data.frecuencia;
      this.acciones = data.acciones;
      this.responsable = data.responsable;
      this.responsableSecundario = data.responsableSecundario;
      this.contingencia = data.contingencia;
      this.auxiliar = data.auxiliar;
      this.idPlanManejo = data.idPlanManejo;
      this.accion = data.accion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;

      return;
    }
  }
  idEvalAprovechamiento?: number;
  codTipoAprovechamiento: string = "";
  subCodTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";
  nombreAprovechamiento: string = "";
  impacto: string = "";
  medidasControl: string = "";
  medidasMonitoreo: string = "";
  frecuencia: string = "";
  acciones: string = "";
  responsable: string = "";
  responsableSecundario: string = "";
  contingencia: string = "";
  auxiliar: string = "";
  idPlanManejo: number = 0;
  accion: string = AccionTipo.REGISTRAR;
  idUsuarioRegistro: number = 0;
}
