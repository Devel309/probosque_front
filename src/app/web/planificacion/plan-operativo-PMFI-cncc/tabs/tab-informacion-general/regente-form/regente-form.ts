import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";


import { RegenteExternoModel, RegenteForestalModel } from "@models";
import { MessageService } from "primeng/api";
import { forkJoin } from "rxjs";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { RegenteService } from "src/app/service/regente.service";

@Component({
  selector: "regente-form",
  templateUrl: "./regente-form.html",
})
export class RegenteForm implements OnInit {
  @Input() idPgmf: number = 0;
  idUsuario: number = 1;

  regenteForestal: RegenteForestalModel = new RegenteForestalModel();
  documentoRegente: RegenteExternoModel = new RegenteExternoModel();

  regentesFiltrados: RegenteExternoModel[] = [];
  regentesForestales: RegenteExternoModel[] = [];

  constructor(
    private reg: RegenteService,
    private messageService: MessageService,
    private serv_forestal: ApiForestalService,
    private dialog: MatDialog
  ) {

  }

  ngOnInit(): void {
    this.asignarRegente(this.idPgmf);
  }

  asignarRegente(idPlanManejo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      regentesResponse: this.serv_forestal.consultarRegente(),
      regenteResponse: this.reg.obtenerRegente(idPlanManejo)
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe({
        next: res => {
          this.regentesForestales = (res.regentesResponse as any)?.dataService;
          this.regenteForestal = res?.regenteResponse?.data;
          const regenteForestal = this.regentesForestales
            .find(x => this.regenteForestal.numeroDocumento == x.numeroDocumento);
          this.documentoRegente = regenteForestal ? regenteForestal :
            new RegenteExternoModel({
              numeroDocumento: this.regenteForestal.numeroDocumento
            });
        },
        error: _err => { }
      });
  }

  filtrarRegente(value: string) {
    value = value.toLowerCase();
    this.regentesFiltrados = [];
    this.regentesFiltrados = this.regentesForestales.filter(
      r => (r.nombres.toLowerCase().includes(value) ||
        r.apellidos.toLowerCase().includes(value) ||
        r.numeroDocumento.includes(value))
    ).map(x => {
      return new RegenteExternoModel({ ...x, nombresNroDocumento: `${x.nombres} ${x.apellidos} - ${x.numeroDocumento}` })
    });
  }

  clearRegente() {
    this.regenteForestal = new RegenteForestalModel();
    this.documentoRegente = new RegenteExternoModel();
  }
  selectDocumentoRegente(value: RegenteExternoModel) {
    this.regenteForestal = {
      ...this.regenteForestal,
      numeroLicencia: value.numeroLicencia,
      numeroDocumento: value.numeroLicencia,
      idUsuarioRegistro: this.idUsuario
    };
  }

  /*Funciones de guardado */

  guardar() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.reg.guardarRegente(this.regenteForestal)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe({
        next: (res) => this.messageService.add({ severity: "success", detail: res?.message }),
        error: (_err) => {
          this.messageService.add({ severity: "warn", detail: _err?.message });
          console.error(_err);
        },
      });
  }
}
