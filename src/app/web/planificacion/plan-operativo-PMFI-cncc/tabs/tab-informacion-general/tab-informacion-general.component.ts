import { HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Input } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DepartamentoModel, DistritoModel, ProvinciaModel, RegenteForestalModel } from '@models';
import { CoreCentralService, PlanManejoService, UsuarioService } from '@services';
import { isNullOrEmpty, ToastService } from '@shared';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PopacInformacionGeneral, RegenteForestal } from 'src/app/model/popac-informacion-general';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { ModalInfoPlanComponent } from 'src/app/shared/components/modal-info-plan/modal-info-plan.component';

@Component({
  selector: 'app-tab-informacion-general',
  templateUrl: './tab-informacion-general.component.html',
  styleUrls: ["./tab-informacion-general.component.scss"],
})
export class TabInformacionGeneralComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();

  @Input() disabled!: boolean;

  @Output()
  public regresar = new EventEmitter();
  @Input() idPGMF!: number;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC;
  form: PopacInformacionGeneral = new PopacInformacionGeneral();
  listDepartamento: DepartamentoModel[] = [];
  listProvincia: ProvinciaModel[] = [];
  listDistrito: DistritoModel[] = [];
  departamento = {} as DepartamentoModel;
  provincia = {} as ProvinciaModel;
  distrito = {} as DistritoModel;
  listCuenca: any[] = [];
  listSubcuenca: any[] = [];
  listCuencaSubcuenca: CuencaSubcuencaModel[] = [];
  idCuenca!: number | null;
  idSubcuenca!: number | null;

  edit: boolean = false;
  nombreComunidad: string = '';
  nombreJefeComunidad: string = '';
  documentoJefeComunidad: string = '';
  representanteLegal: string = '';
  selectRegente: RegenteForestal = new RegenteForestal();
  verModalRegente: boolean = false;
  queryRegente: string = '';
  regentes: any[] = [];
  nombreCompleto: string = "";
  domicilioLegal!: string | null;
  ref!: DynamicDialogRef;
  idPlanManejoPadre: number = 0;
  idPlanManejoPadreTemp: number = 0;
  displayModal: boolean = false;
  minDate = moment(new Date()).format('YYYY-MM-DD');
  minDateFinal = moment(new Date()).add('days', 1).format('YYYY-MM-DD');
  disabledDate: boolean = false;
  listRegimenes: any[] = [];
  idInformacionGeneral: number = 0;
  editar: string = "";
  editContrato: boolean = false;
  validaSunatClass: string = 'btn btn-sm btn-danger2';
  validaPIDEClass: boolean = false;

  constructor(
    private servCoreCentral: CoreCentralService,
    private messageService: MessageService,
    private informacionGeneralService: InformacionGeneralService,
    private dialog: MatDialog,
    private dialogService: DialogService,
    private usuarioService: UsuarioService,
    private apiForestal: ApiForestalService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private planManejoService: PlanManejoService,
    private serv_forestal: ApiForestalService,
    private pideService: PideService
  ) {}

  ngOnInit(): void {
    this.obtenerPlanManejo();
    this.listarPorFiltroDepartamento();
    this.listarPorFilroProvincia();
    this.listarPorFilroDistrito();
    this.listarCuencaSubcuenca();
    this.cargarRegimenes();
    this.listarInformacionGeneral();
  }

  obtenerPlanManejo() {
    var params = {
      idPlanManejo: this.idPGMF,
    };
    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((response: any) => {
        this.idPlanManejoPadre = response.data.idPlanManejoPadre ? response.data.idPlanManejoPadre : 0;
      });
  }

  listarPorFiltroDepartamento() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia() {
    this.servCoreCentral.listarPorFilroProvincia({}).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarPorFilroDistrito() {
    this.servCoreCentral.listarPorFilroDistrito({}).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  Distrito(param: any) {
    this.form.idDistritoRepresentante = param.value;
  }

  listarCuencaSubcuenca() {
    this.listCuencaSubcuenca = [];
    this.listCuenca = [];

    this.servCoreCentral.listarCuencaSubcuenca().subscribe(
      (result: any) => {
        this.listCuencaSubcuenca = result.data;

        let listC: any[] = [];
        this.listCuencaSubcuenca.forEach((item) => {
          listC.push(item);
        });

        const setObj = new Map();
        const unicos = listC.reduce((arr, cuenca) => {
          if (!setObj.has(cuenca.idCuenca)) {
            setObj.set(cuenca.idCuenca, cuenca)
            arr.push(cuenca)
          }
          return arr;
        }, []);

        this.listCuenca = unicos;
        this.listCuenca.sort((a, b) => a.cuenca - b.cuenca);

        if (!isNullOrEmpty(this.idCuenca) && !isNullOrEmpty(this.idSubcuenca)) {
          this.onSelectedCuenca({ value: Number(this.idCuenca) });
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  onSelectedCuenca(param: any) {
    this.listSubcuenca = [];

    this.listCuencaSubcuenca.forEach((item) => {
      if (item.idCuenca == param.value) {
        this.listSubcuenca.push(item);
      }
    });
  }

  listarInformacionGeneral() {
    this.edit = false;
    var params = {
      codigoProceso: CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPGMF,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.edit = true;
          response.data.forEach((element: any) => {
            this.form = new PopacInformacionGeneral(element);

            this.distrito.idDistrito = element.idDistrito;
            this.provincia.idProvincia = element.idProvincia;
            this.departamento.idDepartamento = element.idDepartamento;
            this.idInformacionGeneral = element.idInformacionGeneralDema;
            this.idCuenca = isNullOrEmpty(element.cuenca) ? null : Number(element.cuenca);
            this.onSelectedCuenca({ value: this.idCuenca });
            this.idSubcuenca = isNullOrEmpty(element.subCuenca) ? null : Number(element.subCuenca);

            this.form.idDistritoRepresentante =  element.idDistrito;
            if (!isNullOrEmpty(element.fechaInicioDema)) {
              this.form.fechaInicioDema = new Date(element.fechaInicioDema);
            }
            if (!isNullOrEmpty(element.fechaFinDema)) {
              this.form.fechaFinDema = new Date(element.fechaFinDema);
            }
            this.form.nroArbolesMaderables = isNullOrEmpty(element.nroArbolesMaderables) ? 0 : element.nroArbolesMaderables;
            this.form.nroArbolesMaderablesSemilleros = isNullOrEmpty(element.nroArbolesMaderablesSemilleros) ? 0 : element.nroArbolesMaderablesSemilleros;
            this.form.volumenM3rMaderables = isNullOrEmpty(element.volumenM3rMaderables) ? '0' : element.volumenM3rMaderables;
            this.form.nroArbolesNoMaderables = isNullOrEmpty(element.nroArbolesNoMaderables) ? 0 : element.nroArbolesNoMaderables;
            this.form.nroArbolesNoMaderablesSemilleros = isNullOrEmpty(element.nroArbolesNoMaderablesSemilleros) ? 0 : element.nroArbolesNoMaderablesSemilleros;
            this.form.volumenM3rNoMaderables = isNullOrEmpty(element.volumenM3rNoMaderables) ? '0' : element.volumenM3rNoMaderables;
            this.form.superficieHaNoMaderables = isNullOrEmpty(element.superficieHaNoMaderables) ? '0' : element.superficieHaNoMaderables;

            element.nombreElaboraDema = !isNullOrEmpty(element.nombreElaboraDema) ? element.nombreElaboraDema : '';
            element.apellidoPaternoElaboraDema = !isNullOrEmpty(element.apellidoPaternoElaboraDema) ? element.apellidoPaternoElaboraDema : '';
            element.apellidoMaternoElaboraDema = !isNullOrEmpty(element.apellidoMaternoElaboraDema) ? element.apellidoMaternoElaboraDema : '';
            element.nombreRepresentante = !isNullOrEmpty(element.nombreRepresentante) ? element.nombreRepresentante : '';
            element.apellidoPaternoRepresentante = !isNullOrEmpty(element.apellidoPaternoRepresentante) ? element.apellidoPaternoRepresentante : '';
            element.apellidoMaternoRepresentante = !isNullOrEmpty(element.apellidoMaternoRepresentante) ? element.apellidoMaternoRepresentante : '';
            // this.form.volumenM3rMaderables = element.volumenM3rMaderables != 'undefined' ? element.volumenM3rMaderables : '';
            // this.form.volumenM3rNoMaderables = element.volumenM3rNoMaderables != 'undefined' ? element.volumenM3rNoMaderables : '';
            // this.form.superficieHaNoMaderables = element.superficieHaNoMaderables != 'undefined' ? element.superficieHaNoMaderables : '';
            if (element.codTipoPersona === 'TPERJURI') {
              this.nombreComunidad = element.nombreElaboraDema;
              this.nombreJefeComunidad = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;
              this.documentoJefeComunidad = element.documentoRepresentante;
            } else {
              this.nombreJefeComunidad = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaboraDema + ' ' + element.apellidoMaternoElaboraDema;
              this.documentoJefeComunidad = element.dniElaboraDema;
            }
            this.representanteLegal = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;

            if (!isNullOrEmpty(element.regente)) {
              this.nombreCompleto = element.regente.nombres + ' ' + element.regente.apellidos;
              this.domicilioLegal = element.regente.domicilioLegal;
            }
          });
        } else {
          this.listRegimenes.forEach((item) => {
            this.form.lstUmf.push(item);
          });
        }
      });
  }

  openModal() {
    this.ref = this.dialogService.open(ModalInfoPlanComponent, {
      header: "Buscar Plan Manejo " + CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        buscar: true,
        codTipoPlan: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
        nroDocumento: this.usuarioService.nroDocumento,
        idTipoDocumento: this.usuarioService.idtipoDocumento,
        idPlanManejo: this.idPGMF,
        idPlanManejoPadre: this.idPlanManejoPadre,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.displayModal = true;
        this.idPlanManejoPadreTemp = resp.idPlanManejo;
      }
    });
  }

  closeModal() {
    this.displayModal = false;
    this.listarInformacionGeneralPrevio();
  }

  listarInformacionGeneralPrevio() {
    var params = {
      codigoProceso: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejoPadreTemp,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.form = new PopacInformacionGeneral(element);

            this.distrito.idDistrito = element.idDistrito;
            this.provincia.idProvincia = element.idProvincia;
            this.departamento.idDepartamento = element.idDepartamento;
            this.idCuenca = isNullOrEmpty(element.cuenca) ? null : Number(element.cuenca);
            this.idSubcuenca = isNullOrEmpty(element.subCuenca) ? null : Number(element.subCuenca);
            this.onSelectedCuenca({ value: Number(element.cuenca) });

            this.form.idDistritoRepresentante =  element.idDistrito;
            this.form.nroArbolesMaderables = element.nroArbolesMaderables ? element.nroArbolesMaderables : 0;
            this.form.nroArbolesMaderablesSemilleros = element.nroArbolesMaderablesSemilleros ? element.nroArbolesMaderablesSemilleros : 0;
            this.form.volumenM3rMaderables = element.volumenM3rMaderables ? element.volumenM3rMaderables : '0';
            this.form.nroArbolesNoMaderables = element.nroArbolesNoMaderables ? element.nroArbolesNoMaderables : 0;
            this.form.nroArbolesNoMaderablesSemilleros = element.nroArbolesNoMaderablesSemilleros ? element.nroArbolesNoMaderablesSemilleros : 0;
            this.form.volumenM3rNoMaderables = element.volumenM3rNoMaderables ? element.volumenM3rNoMaderables : '0';
            this.form.superficieHaNoMaderables = element.superficieHaNoMaderables ? element.superficieHaNoMaderables : '0';

            element.nombreElaboraDema = !isNullOrEmpty(element.nombreElaboraDema) ? element.nombreElaboraDema : '';
            element.apellidoPaternoElaboraDema = !isNullOrEmpty(element.apellidoPaternoElaboraDema) ? element.apellidoPaternoElaboraDema : '';
            element.apellidoMaternoElaboraDema = !isNullOrEmpty(element.apellidoMaternoElaboraDema) ? element.apellidoMaternoElaboraDema : '';
            element.nombreRepresentante = !isNullOrEmpty(element.nombreRepresentante) ? element.nombreRepresentante : '';
            element.apellidoPaternoRepresentante = !isNullOrEmpty(element.apellidoPaternoRepresentante) ? element.apellidoPaternoRepresentante : '';
            element.apellidoMaternoRepresentante = !isNullOrEmpty(element.apellidoMaternoRepresentante) ? element.apellidoMaternoRepresentante : '';
            if (element.codTipoPersona === 'TPERJURI') {
              this.nombreComunidad = element.nombreElaboraDema;
              this.nombreJefeComunidad = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;
              this.documentoJefeComunidad = element.documentoRepresentante;
            } else {
              this.nombreJefeComunidad = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaboraDema + ' ' + element.apellidoMaternoElaboraDema;
              this.documentoJefeComunidad = element.dniElaboraDema;
            }
            this.representanteLegal = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;

            this.form.regente.idPlanManejo = this.idPGMF;
            this.form.regente.idPlanManejoRegente = 0;
            this.form.regente.codigoProceso = CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC;
            if (!isNullOrEmpty(element.regente)) {
              this.nombreCompleto = element.regente.nombres + ' ' + element.regente.apellidos;
              this.domicilioLegal = element.regente.domicilioLegal;
            }

            if (isNullOrEmpty(element.lstUmf)) {
              this.listRegimenes.forEach((item) => {
                this.form.lstUmf.push(item);
              });
            }
          });
        }
      });
  }

  abrirModalRegentes() {
    this.listarRegentes();
    this.selectRegente = new RegenteForestal();
    this.verModalRegente = true;
    this.queryRegente = '';
  }

  listarRegentes() {
    this.apiForestal.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {
        this.regentes.push({
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
          domicilioLegal: element.domicilioLegal
        });
      });
    });
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegentes();
    }
  }

  guardarRegente() {
    this.nombreCompleto = this.selectRegente.nombres + ' ' + this.selectRegente.apellidos;
    this.domicilioLegal = this.selectRegente.domicilioLegal;
    this.form.regente = new RegenteForestal(this.selectRegente);
    this.form.regente.idPlanManejo = this.idPGMF;
    this.form.regente.idUsuarioRegistro = this.usuarioService.idUsuario;
    this.verModalRegente = false;
  }

  guardarInformacionGeneral() {
    if (this.domicilioLegal) this.form.regente.domicilioLegal = this.domicilioLegal;
    this.form.fechaElaboracionPmfi = new Date();
    this.form.fechaInicioDema = new Date(this.form.fechaInicioDema);
    this.form.fechaFinDema = new Date(this.form.fechaFinDema);
    this.form.cuenca = this.idCuenca ? String(this.idCuenca) : null;
    this.form.subCuenca = this.idSubcuenca ? String(this.idSubcuenca) : null;

    if (this.validationFields(this.form)) {
      var params = new PopacInformacionGeneral(this.form);
      params.idPlanManejo = this.idPGMF;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .registrarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            // this.SuccessMensaje(response.message);
            this.guardarPlanManejo();
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
            // this.ErrorMensaje(response.message)
          };
        });
    }
  }

  validationFields(data: PopacInformacionGeneral) {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!isNullOrEmpty(data.codTipoPersona)) {
      if (data.codTipoPersona === 'TPERJURI') {
        if (!this.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Nombre de la Comunidad.\n";
        }
      }
    } else {
      if (!this.nombreJefeComunidad) {
        if (!this.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Nombre de la Comunidad.\n";
        }
      }
    }
    if (!this.nombreJefeComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre del jefe de la comunidad.\n";
    }
    if (!this.documentoJefeComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nº DNI del jefe de la comunidad.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  guardarPlanManejo() {
    var params = {
      idPlanManejo: this.idPGMF,
      idUsuarioModificacion: this.usuarioService.idUsuario,
      idPlanManejoPadre: this.idPlanManejoPadreTemp,
    };
    this.evaluacionService
      .actualizarPlanManejo(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  editarInformacionGeneral() {
    if (this.domicilioLegal) this.form.regente.domicilioLegal = this.domicilioLegal;
    this.form.fechaElaboracionPmfi = new Date();
    this.form.fechaInicioDema = new Date(this.form.fechaInicioDema);
    this.form.fechaFinDema = new Date(this.form.fechaFinDema);
    this.form.cuenca = this.idCuenca ? String(this.idCuenca) : null;
    this.form.subCuenca = this.idSubcuenca ? String(this.idSubcuenca) : null;

    if (this.validationFields(this.form)) {
      var params = new PopacInformacionGeneral(this.form);
      params.idPlanManejo = this.idPGMF;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            // this.SuccessMensaje(response.message);
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
            // this.ErrorMensaje(response.message)
          };
        });
    }
  }

  handleAdmDateChange() {
    this.validarInformacion();

    let fechaInicial = moment(this.form.fechaInicioDema);
    let fechaFinal = moment(this.form.fechaFinDema);

    if (fechaFinal.diff(fechaInicial, 'days') >= 0) {
      this.form.vigencia = fechaFinal.diff(fechaInicial, 'years');
      this.disabledDate = false;
    } else {
      this.toast.error('Fecha Fin debe ser posterior o igual a Fecha Inicio');
      this.disabledDate = true;
    }
  }

  validarInformacion() {
    let fechaActual = moment();
    let fechaInicio = moment(this.form.fechaInicioDema);

    if (!!this.form.fechaInicioDema && !!this.form.fechaFinDema) {
      if (fechaInicio.diff(fechaActual, 'days') < 0) {
        this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
        this.disabledDate = true;
        return;
      }

      if (this.form.fechaFinDema <= this.form.fechaInicioDema) {
        this.disabledDate = true;
        this.toast.error('La Fecha Final debe ser posterior a la Fecha Inicio');
        return;
      }
    } else if (fechaInicio.diff(fechaActual, 'days') < 0) {
      this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
      this.disabledDate = true;
      return;
    }
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ severity: "success", summary: "", detail: mensaje });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  cargarRegimenes = () => {
    this.listRegimenes.push(
      {
        codigo: "001",
        descripcion: 'En la UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR.',
        detalle: 'NO'
      },
      {
        codigo: "002",
        descripcion: 'La UMF se ubica en zona prioritaria para el Estado.',
        detalle: 'NO'
      },
      {
        codigo: "003",
        descripcion: 'Se realizan proyectos integrales de transformación.',
        detalle: 'NO'
      },
      {
        codigo: "004",
        descripcion: 'La UMF cuenta con certificación forestal voluntaria de buenas prácticas.',
        detalle: 'NO'
      },
      {
        codigo: "005",
        descripcion: 'La UMF cuenta con certificación forestal voluntaria de buenas prácticas por más de 5 años.',
        detalle: 'NO'
      },
      {
        codigo: "006",
        descripcion: 'Se emitió el informe de evaluación o "scoping" por la empresa certificadora.',
        detalle: 'NO'
      },
      {
        codigo: "007",
        descripcion: 'Se reporta a la ARFFS y al SERFOR los resultados de las parcelas permanentes de muestreo.',
        detalle: 'NO'
      },
      {
        codigo: "008",
        descripcion: 'Se protege, conserva y/o recupera áreas no destinadas al aprovechamiento forestal.',
        detalle: 'NO'
      },
      {
        codigo: "009",
        descripcion: 'La UMF se encuentra bajo manejo de uso múltiple.',
        detalle: 'NO'
      }
    );
  };

  editarContrato(){
    this.editContrato = true
    this.representanteLegal = "";
    this.form.direccionLegalRepresentante = "";
    this.nombreComunidad  = "";
    this.form.direccionLegalTitular = "";
    this.form.nroRucComunidad = "";
     this.form.documentoRepresentante = "";
  }

  validaRuc() {
    if (this.form.nroRucComunidad == null || this.form.nroRucComunidad == "" || this.form.nroRucComunidad == undefined) {
      this.toast.warn('Se requiere número de ruc.');
      return;
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serv_forestal.consultarDatoRuc(this.form.nroRucComunidad)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
        if(result.dataService){

          let persona = result.dataService.respuesta;
          if(result.dataService.respuesta.esHabido){
            this.validaSunatClass = 'btn btn-sm btn-secondary';

            this.nombreComunidad  = persona.ddp_nombre;
            this.form.direccionLegalTitular = persona.ddp_nomvia + '-'+ persona.ddp_refer1
            this.form.nombreElaboraDema = persona.ddp_nombre;

            this.toast.ok('Se validó RUC en Sunat.');
          }else{
            this.toast.warn('RUC ingresado no existe en Sunat.');
          }
        }else{
          this.toast.error('Hay errores con el servicio de validación del Ruc. Contactar con el administrador del sistema.');
        }

        },
        (error) => {
          this.toast.error('Hay errores con el servicio de validación de Ruc. Contactar con el administrador del sistema.');
        });
    }

  }
  consultarDNI(params: any) {
    if (this.form.documentoRepresentante == null || this.form.documentoRepresentante == "" || this.form.documentoRepresentante == undefined) {
      this.toast.warn('Se requiere número DNI.');
      return;
    } else{
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.pideService.consultarDNI(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if(result.dataService){

            let persona = result.dataService.datosPersona;
            if(result.dataService.datosPersona){
              this.validaPIDEClass = true;

              this.representanteLegal = persona.prenombres + ' ' + persona.apPrimer + ' ' + persona.apSegundo;
              this.form.direccionLegalRepresentante = persona.direccion
              this.form.nombreRepresentante = persona.prenombres;
              this.form.apellidoPaternoRepresentante = persona.apPrimer;
              this.form.apellidoMaternoRepresentante = persona.apSegundo;


              this.toast.ok('Se validó existencia de DNI en RENIEC.');
            }else{
              this.validaPIDEClass = false;
              this.toast.warn('DNI ingresado no existe en RENIEC.');
            }
          }else{
            this.validaPIDEClass = false;
            this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
          }
        },
        (error) => {
          this.toast.error('Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.');
        }
      )

    }
  }

  validarPide() {
    this.consultarDNI({"numDNIConsulta":this.form.documentoRepresentante});
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}

export interface CuencaSubcuencaModel {
  codCuenca: string,
  codSubCuenca: string,
  codigo: string,
  cuenca: string,
  idCuenca: number,
  idSubCuenca: number,
  subCuenca: string,
  valor: string
}
