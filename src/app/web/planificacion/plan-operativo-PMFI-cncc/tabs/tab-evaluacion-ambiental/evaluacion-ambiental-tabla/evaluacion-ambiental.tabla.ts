import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { concatMap, finalize, tap } from "rxjs/operators";

import { AprovechamientoTipo, RespuestaTipo, ToastService } from "@shared";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AprovechamientoResponse } from "src/app/model/evaluacionImpacto";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { ActividadComponent } from "../modal/actividad/actividad.component";
import { AprovechamientoComponent } from "../modal/aprovechamiento/aprovechamiento.component";
import { FactorAmbientalComponent } from "../modal/factor-ambiental/factor-ambiental.component";
import { FactorActividadModel, FactorModel } from "@models";
import { ConfirmationService, MessageService } from "primeng/api";
import { BehaviorSubject } from "rxjs";
import { mergeMap } from "rxjs-compat/operator/mergeMap";
import { merge } from "rxjs-compat/operator/merge";
import { ModalConfirmationEvaluacionComponent } from "../modal/modal-confirmation-guardar/modal-confirmation-guardar.component";
import { UsuarioService } from "@services";
import { TabEvaluacionAmbientalComponent } from "../tab-evaluacion-ambiental.component";

@Component({
  selector: "evaluacion-ambiental-tabla",
  templateUrl: "./evaluacion-ambiental.tabla.html",
  styleUrls: ["./evaluacion-ambiental.tabla.scss"],
})
export class EvaluacionAmbientalTabla implements OnInit {
  @Input() idPlanManejo: number = 0;
  @Input() disabled!: boolean;
  idUsuario: number = 1;

  @Output() updateContingencia = new EventEmitter();

  ref!: DynamicDialogRef;

  params: any;

  preAprovechamiento: AprovechamientoResponse[] = [];
  aprovechamiento: AprovechamientoResponse[] = [];
  postAprovechamiento: AprovechamientoResponse[] = [];

  @Output() list = new EventEmitter<any>();

  factores: FactorModel[] = [];

  codTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";

  actividades: any[] = [];
  emit: any[] = [];

  sizeColActivities = 0;

  factoresEditar: any[] = [];
  factoresEliminar: any[] = [];
  actividad: string = "";

  isEmptyPMFIC: boolean = false;
  isValuesPMFIC: boolean = false;
  isEmptyActAprovPMFIC: boolean = false;
  isEmptyEvaluacionAmbientalPMFIC: boolean = false;

  isLoadedButton$ = new BehaviorSubject(true);
  
  @Input() isLoading!: boolean;
  constructor(
    private messageService: MessageService,
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private user: UsuarioService,
    private toast: ToastService
  ) {
    this.idUsuario = JSON.parse("" + localStorage.getItem("usuario")).idusuario;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.preAprovechamiento = [];
    this.aprovechamiento = [];
    this.postAprovechamiento = [];
    this.isValuesPMFIC = false;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.listar()
      .pipe(finalize(() => {
        this.dialog.closeAll()
        if(this.isLoading){
          this.guardar()
        }
      }))
      .subscribe();
  }

  listar() {
    this.sizeColActivities = 0;
    this.preAprovechamiento = [];
    this.aprovechamiento = [];
    this.postAprovechamiento = [];
    this.factores = [];
    this.actividades = [];

    return this.getActividades().pipe(
      concatMap(() => this.getFactores()),
      concatMap(() => this.getRespuestas())
    );
  }

  getFactores() {
    const body = { idPlanManejo: this.idPlanManejo, tipoActividad: "FACTOR" };
    return this.planificacionService
      .listarFiltrosEvalAmbientalActividad(body)
      .pipe(
        tap({
          next: (res: any) => {
            if (res.success) {
              if (res.data[0].codTipoActividad == "PMFIC") {
                this.isValuesPMFIC = true;
                this.openModalConfirmationGuardar();
              }
              res?.data.map((response: any) => {
                if (
                  response.tipoNombreActividad ===
                  "NO HAY DATA EN EL PLAN PADRE"
                ) {
                  this.isEmptyPMFIC = true;
                }
              });

              if (!this.isEmptyPMFIC) {
                const factores = this.setDatosTabla(res?.data);
                this.factores = factores;
                this.isLoadedButton$.next(false);
              }
            }
          },
          error: (_err) => console.error(_err),
        })
      );
  }

  getActividades() {
    const body = { idPlanManejo: this.idPlanManejo, tipoAprovechamiento: "" };
    return this.planificacionService.getActividades(body).pipe(
      finalize(() => this.activitiesColSize()),
      tap({
        next: (res) => {
          if (res.data[0].codTipoAprovechamiento == "PMFIC") {
            this.isValuesPMFIC = true;
          }
          res.data.map((response: any) => {
            if (
              response.tipoNombreAprovechamiento ===
              "NO HAY DATA EN EL PLAN PADRE"
            ) {
              this.isEmptyActAprovPMFIC = true;
            }
          });

          if (!this.isEmptyActAprovPMFIC) {
            return this.filtrarTipoAprovechamiento(res.data);
          }
        },
        error: (_err) => console.error(_err),
      })
    );
  }

  getRespuestas() {
    return this.planificacionService
      .listarFactoresActividades(this.idPlanManejo)
      .pipe(
        tap({
          next: (res) => {
            res.data.map((response: any) => {
              if (response.descripcion === "NO HAY DATA EN EL PLAN PADRE") {
                this.isEmptyEvaluacionAmbientalPMFIC = true;
              }
            });

            if (!this.isEmptyEvaluacionAmbientalPMFIC) {
              return this.responseToBoolean(res?.data);
            }
          },
          error: (_err) => console.error(_err),
        })
      );
  }

  responseToBoolean(data: FactorActividadModel[]) {
    if (data) {
      for (const respuesta of data) {
        const factor = this.factores.find(
          (x) => x.idEvalActividad == respuesta.idEvalActividad
        );

        if (factor == undefined) return;

        const preAprovechamiento = this.existFactor(
          factor,
          "preAprovechamiento"
        );
        const aprovechamiento = this.existFactor(factor, "aprovechamiento");
        const postAprovechamiento = this.existFactor(
          factor,
          "postAprovechamiento"
        );
        const actividades = preAprovechamiento
          .concat(aprovechamiento)
          .concat(postAprovechamiento);
        const item = actividades.find(
          (y) => y.idEvalAprovechamiento == respuesta.idEvalAprovechamiento
        ) as AprovechamientoResponse;
        if (item) {
          item.valorEvaluacion = respuesta.valorEvaluacion == RespuestaTipo.SI;
          item.idEvaluacionAmbiental = respuesta.idEvaluacionAmbiental;
        }
      }
    }
  }

  existFactor(
    factor: FactorModel | undefined,
    attr: "preAprovechamiento" | "aprovechamiento" | "postAprovechamiento"
  ): AprovechamientoResponse[] {
    if (factor == undefined || factor[attr] == undefined) {
      return [];
    }
    return factor[attr] as AprovechamientoResponse[];
  }

  activitiesColSize() {
    this.sizeColActivities =
      this.preAprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.preAprovechamiento.length;
    this.sizeColActivities =
      this.aprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.aprovechamiento.length;
    this.sizeColActivities =
      this.postAprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.postAprovechamiento.length;
  }

  filtrarTipoAprovechamiento(data: AprovechamientoResponse[]) {
    let actividades: AprovechamientoResponse[] = [];
    if (data == null || data == undefined) return;
    data.forEach((x) => {
      if (x.valorEvaluacion == undefined || x.valorEvaluacion == null)
        x.valorEvaluacion = false;

      this.actividades.push(x);
      actividades.push({ ...x });
      switch (x.tipoAprovechamiento) {
        case AprovechamientoTipo.PRE_APROVECHAMIENTO:
          this.preAprovechamiento.push(x);
          break;
        case AprovechamientoTipo.APROVECHAMIENTO:
          this.aprovechamiento.push(x);
          break;
        case AprovechamientoTipo.POST_APROVECHAMIENTO:
          this.postAprovechamiento.push(x);
          break;
        default:
          break;
      }
    });
    this.addList(actividades);
  }

  addList(value: any) {
    this.list.emit(value);
  }

  setDatosTabla(data: any[]) {
    if (data == null || data == undefined) return [];

    data.forEach((x) => {
      x["preAprovechamiento"] = this.preAprovechamiento.map((o) => ({ ...o }));
      x["aprovechamiento"] = this.aprovechamiento.map((o) => ({ ...o }));
      x["postAprovechamiento"] = this.postAprovechamiento.map((o) => ({
        ...o,
      }));
    });
    return data;
  }

  guardar() {
    const body = this.requestToString(this.idPlanManejo, this.idUsuario);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.registrarRespuestas(body)
      .pipe(concatMap(() => this.listar()))
      .pipe(finalize(() => {
        this.dialog.closeAll()
        this.isLoading = false
      }))
      .subscribe();
  }

  requestToString(idPlanManejo: number, idUsuario: number) {
    let body: FactorActividadModel[] = [];
    this.factores.forEach((factor) => {
      const preAprovechamiento = this.existFactor(factor, "preAprovechamiento");
      const aprovechamiento = this.existFactor(factor, "aprovechamiento");
      const postAprovechamiento = this.existFactor(
        factor,
        "postAprovechamiento"
      );
      const actividades = factor
        ? preAprovechamiento.concat(aprovechamiento).concat(postAprovechamiento)
        : [];

      actividades.forEach((actividad) => {
        const item = new FactorActividadModel({
          responsableSecundario: factor.nombreActividad,
          auxiliar: factor.medidasControl,
          contingencia: factor.codTipoActividad,
          descripcion: actividad.nombreAprovechamiento,
          idEvaluacionAmbiental: actividad?.idEvaluacionAmbiental
            ? actividad.idEvaluacionAmbiental
            : 0,
          idEvalAprovechamiento: actividad.idEvalAprovechamiento,
          idEvalActividad: factor.idEvalActividad,
          idPlanManejo,
          valorEvaluacion: actividad.valorEvaluacion
            ? RespuestaTipo.SI
            : RespuestaTipo.NO,
          medidasMitigacion: factor?.medidasMonitoreo
            ? factor.medidasMonitoreo
            : "",
          idUsuarioRegistro: idUsuario,
        });

        body.push(item);
      });

      const medidasMitigacion = new FactorActividadModel({
        idEvaluacionAmbiental: 0,
        idEvalAprovechamiento: 0,
        idEvalActividad: factor.idEvalActividad,
        idPlanManejo,
        valorEvaluacion: "",
        descripcion: "",
        medidasMitigacion: factor?.medidasMonitoreo
          ? factor.medidasMonitoreo
          : "",
        adjunto: "",
        idUsuarioRegistro: idUsuario,
      });
      body.push(medidasMitigacion);
    });
    return body;
  }

  registrarRespuestas(body: FactorActividadModel[]) {
    return this.planificacionService
      .registrarFactoresActividades(body)
      .pipe(tap(this.handlerResult()));
  }

  modalAprovechamiento = (mensaje: string, item: any, tipo: string) => {
    if (tipo == "Pre Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "PREAPR";
      this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
    }

    if (tipo == "Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "APROVE";
      this.tipoNombreAprovechamiento = "Aprovechamiento";
    }
    if (tipo == "Post Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "POSTAP";
      this.tipoNombreAprovechamiento = "Post Aprovechamiento";
    }

    this.ref = this.dialogService.open(AprovechamientoComponent, {
      header: mensaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.params = {
          idPlanManejo: this.idPlanManejo,
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
        };
        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe(() => this.getData());
      }
    });
  };

  guardarInformacionPMFI() {
    const paramsActividad = {
      idEvalActividad: 0,
      codTipoActividad: "POPAC",
      tipoActividad: "FACTOR",
      tipoNombreActividad: null,
      nombreActividad: null,
      medidasControl: null,
      medidasMonitoreo: null,
      frecuencia: null,
      acciones: null,
      responsable: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
        .idusuario,
    };
    this.planificacionService
      .registrarActividadEvalAmbiental(paramsActividad)
      .pipe(tap(this.handlerResult()))
      .subscribe();

    const paramsAprovechamiento = {
      idEvalAprovechamiento: 0,
      idPlanManejo: this.idPlanManejo,
      codTipoAprovechamiento: "POPAC",
      tipoAprovechamiento: "",
      tipoNombreAprovechamiento: null,
      nombreAprovechamiento: null,
      idUsuarioRegistro: this.user.idUsuario,
      impacto: null,
      medidasControl: null,
      medidasMonitoreo: null,
      frecuencia: null,
      acciones: null,
      responsable: null,
      responsableSecundario: null,
      contingencia: null,
      auxiliar: null,
    };
    this.planificacionService
      .registrarAprovechamientoEvalAmbiental(paramsAprovechamiento)
      .pipe(tap(this.handlerResult()))
      .subscribe(() => {
        this.updateContingencia.emit(true);
      });
  }

  modalFactorAmbiental = (mensaje: string, editar: boolean, data?: any) => {
    this.factoresEditar = [];
    this.actividad = "";
    if (!!data) {
      this.factoresEditar = this.factores.filter(
        (x) => x.nombreActividad == data.nombreActividad
      );
      this.actividad = data.nombreActividad;
    }

    this.ref = this.dialogService.open(FactorAmbientalComponent, {
      header: mensaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        factor: this.actividad,
        editar: editar,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (!editar) {
          this.params = {
            idEvalActividad: 0,
            codTipoActividad: "POAPRO",
            tipoActividad: "FACTOR",
            tipoNombreActividad: "Factores Ambientales",
            nombreActividad: resp,
            medidasControl: "",
            medidasMonitoreo: "",
            frecuencia: "",
            acciones: "",
            responsable: "",
            idPlanManejo: this.idPlanManejo,
            idUsuarioRegistro: this.user.idUsuario,
          };
          this.planificacionService
            .registrarActividadEvalAmbiental(this.params)
            .pipe(tap(this.handlerResult()))
            .subscribe();
        } else {
          this.factoresEditar.forEach((response: any, index: number) => {
            this.params = {
              idEvalActividad: response.idEvalActividad,
              codTipoActividad: "POAPRO",
              tipoActividad: "FACTOR",
              tipoNombreActividad: "Factores Ambientales",
              nombreActividad: resp,
              medidasControl: response.medidasControl,
              medidasMonitoreo: "",
              frecuencia: "",
              acciones: "",
              responsable: "",
              idPlanManejo: this.idPlanManejo,
              idUsuarioRegistro: this.user.idUsuario,
            };
            

            this.planificacionService
              .registrarActividadEvalAmbiental(this.params)
              .pipe(tap(this.handlerResult()))
              .pipe(
                finalize(() => {
                  if (index == 2) {
                    this.toast.ok(
                      "Se actualizó el Factor Ambiental correctamente"
                    );
                    this.getData();
                  }
                })
              )
              .subscribe();
          });
        }
      }
    });
  };

  openEliminarFactor(event: Event, data: any) {
    this.factoresEliminar = [];
    if (!!data) {
      this.factoresEliminar = this.factores.filter(
        (x) => x.nombreActividad == data.nombreActividad
      );
    }
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        this.factoresEliminar.forEach((response: any, index: number) => {
          if (response.idEvalActividad != 0) {
            var params = {
              idUsuarioElimina: this.user.idUsuario,
              idEvalActividad: response.idEvalActividad,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.planificacionService
              .eliminarEvaluacionAmbientalActividad(params)
              .pipe(
                finalize(() => {
                  this.dialog.closeAll();
                  if (index == 2) {
                    this.toast.ok(
                      "Se eliminó el Factor Ambiental correctamente"
                    );
                    this.getData();
                  }
                })
              )
              .subscribe();
          }
        });
      },
      reject: () => {},
    });
  }

  modalActividad = (data: any) => {
    this.ref = this.dialogService.open(ActividadComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.params = {
          idEvalActividad: 0,
          codTipoActividad: "POAPRO",
          tipoActividad: "INPAC",
          tipoNombreActividad: "IMPACTO",
          nombreActividad: data.nombreAprovechamiento,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };
        this.planificacionService
          .registrarActividadEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe();
        this.impactoAmbientales();
      }
    });
  };
  impactoAmbientales() {
    this.params = {
      tipoActividad: "INPAC",
    };
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad(this.params)
      .subscribe((response: any) => (this.factores = response.data));
  }

  handlerResult() {
    return {
      next: (res: any) =>
        this.messageService.add({
          severity: "success",
          detail: res?.message,
        }),
      error: (_err: any) => {
        this.messageService.add({
          severity: "warn",
          detail: _err?.message,
        });
        console.error(_err);
      },
    };
  }

  openModalConfirmationGuardar() {
    this.ref = this.dialogService.open(ModalConfirmationEvaluacionComponent, {
      header: "",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp == "YES") {
        this.guardarInformacionPMFI();
        this.registrarEvaluacionAmbiental();
      }
    });
  }

  registrarEvaluacionAmbiental() {
    var params = [
      {
        idEvaluacionAmbiental: 0,
        idEvalAprovechamiento: 9999,
        idEvalActividad: null,
        idPlanManejo: this.idPlanManejo,
      },
    ];
    this.planificacionService
      .registrarEvaluacionAmbiental(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.getData();
        }
      });
  }
}
