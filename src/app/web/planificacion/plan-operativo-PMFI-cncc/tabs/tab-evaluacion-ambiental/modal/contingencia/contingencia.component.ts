import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-contingencia",
  templateUrl: "./contingencia.component.html",
  styleUrls: ["./contingencia.component.scss"],
})
export class ContingenciaComponent implements OnInit {
  listaContingencia: any[] = [
    { valor1: "Incendios", codigo: "CONTIINC" },
    {
      valor1: "Derrame de conbustible  y/o lubricantes",
      codigo: "CONTIDER",
    },
    {
      valor1: "Riesgos a la salud",
      codigo: "CONTIRIE",
    },
    { valor1: "Invasiones", codigo: "CONTIINV" },
    { valor1: "Poblaciones no contactadas", codigo: "CONTIPOB" },
    { valor1: "Otros", codigo: "CONTIOTRO" },
  ];

  autoResize: boolean = true;
  isDisabled: boolean = false;
  isDisabledOtros: boolean = false;
  contingenciaObjt: any = {
    contingencia: "",
    accionesRealizar: "",
    responsable: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {
    if (this.config.data.editar == "true") {
      this.contingenciaObjt.contingencia = this.config.data.data.contingencia;
      this.contingenciaObjt.accionesRealizar = this.config.data.data.acciones;
      this.contingenciaObjt.responsable = this.config.data.data.responsableSecundario;
      if (this.config.data.data.nombreAprovechamiento == "Otros") {
        this.contingenciaObjt.otra = this.config.data.data.contingencia;
        this.contingenciaObjt.contingencia = this.config.data.data.nombreAprovechamiento;
        this.isDisabledOtros = true;
      }
      this.isDisabled = true;
    }
  }

  agregar = () => {
    if (!this.validarEnvio()) return;

    this.ref.close(this.contingenciaObjt);
  };

  validarEnvio() {
    let validar: boolean = true;
    let mensaje: string = "";
    if (this.contingenciaObjt.contingencia === "") {
      mensaje = mensaje += "(*) Contingencia, es obligatorio.\n";
      validar = false;
    }
    if (this.contingenciaObjt.accionesRealizar === "") {
      mensaje = mensaje += "(*) Acciones a realizar, es obligatorio.\n";
      validar = false;
    }
    if (this.contingenciaObjt.responsable === "") {
      mensaje = mensaje += "(*) Responsable, es obligatorio.\n";
      validar = false;
    }
    if (!validar) this.toast.warn(mensaje);
    return validar;
  }

  cerrarModal() {
    this.ref.close();
  }
}
