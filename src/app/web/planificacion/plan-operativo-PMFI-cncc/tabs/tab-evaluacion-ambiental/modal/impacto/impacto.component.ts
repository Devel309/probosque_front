import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-impacto",
  templateUrl: "./impacto.component.html",
  styleUrls: ["./impacto.component.scss"],
})
export class ImpactoComponent implements OnInit {
  autoResize: boolean = true;
  impactoObjt: any = {
    descripcionImpacto: "",
    medidasControl: "",
    medidasMonitoreo: "",
    frecuencia: "",
    responsable: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit() {
    this.impactoObjt.descripcionImpacto = this.config.data.data.impacto;
    this.impactoObjt.medidasControl = this.config.data.data.medidasControl;
    this.impactoObjt.medidasMonitoreo = this.config.data.data.medidasMonitoreo;
    this.impactoObjt.frecuencia = this.config.data.data.frecuencia;
    this.impactoObjt.responsable = this.config.data.data.responsable;
  }

  agregar = () => {
    if (this.impactoObjt.medidasMonitoreo === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Medidas Monitoreo, es obligatorio",
      });
    } else if (this.impactoObjt.frecuencia === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Frecuencia, es obligatorio",
      });
    } else if (this.impactoObjt.responsable === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Responsable, es obligatorio",
      });
    } else this.ref.close(this.impactoObjt);
  };
  
  cerrarModal() {
    this.ref.close();
  }
}
