import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-evaluacion-ambiental",
  templateUrl: "./evaluacion-ambiental.component.html",
  styleUrls: ["./evaluacion-ambiental.component.scss"],
})
export class EvaluacionAmbientaloComponent implements OnInit {
  factor: any;
  actividades: any;

  autoResize: boolean = true;

  carImpacto = [
    { label: "Alto", value: 1 },
    { label: "Medio", value: 2 },
    { label: "Bajo", value: 3 },
  ];

  impactoObjt: any = {
    idEvalAprovechamiento: "",
    idEvalActividad: "",
    mitigacion: "",
    idCarImpacto: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.factor = this.config.data.factor;
    this.actividades = this.config.data.actividades;
  }

  agregar = () => {
    if (this.impactoObjt.idEvalAprovechamiento === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Factor, es obligatorio",
      });
    } else if (this.impactoObjt.idEvalActividad === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Actividad, es obligatorio",
      });
    } else if (this.impactoObjt.mitigacion === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Medidas de Mitigación, es obligatorio",
      });
    } else if (this.impactoObjt.idCarImpacto === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Caracterización de Impacto, es obligatorio",
      });
    } else this.ref.close(this.impactoObjt);
  };
  cerrarModal() {
    this.ref.close();
  }
}
