import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-form-actividad-vigilancia",
  templateUrl: "./form-actividad-vigilancia.component.html",
  styleUrls: ["./form-actividad-vigilancia.component.scss"],
})
export class FormActividadVigilanciaComponent implements OnInit {
  actividad: any;

  autoResize: boolean = true;
  lista: any[] = [
    { tipo: "Pre Aprovechamiento" },
    {
      tipo: "Aprovechamiento",
    },
    {
      tipo: "Post Aprovechamiento",
    },
    {
      tipo: "Otros",
    },
  ];
  impactoObjt: any = {
    actividad: "",
    descripcionImpacto: "",
    medidasControl: "",
    medidasMonitoreo: "",
    frecuencia: "",
    responsable: "",
    tipo: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {}

  agregar = () => {
    if (!this.impactoObjt.tipo) {
      this.toast.warn("(*) Debe seleccionar: Tipo de Actividad.\n");
    } else if (this.impactoObjt.actividad === "") {
      this.toast.warn("(*) Debe ingresar: Actividad.\n");
    } else this.ref.close(this.impactoObjt);
  };
  cerrarModal() {
    this.ref.close();
  }
}
