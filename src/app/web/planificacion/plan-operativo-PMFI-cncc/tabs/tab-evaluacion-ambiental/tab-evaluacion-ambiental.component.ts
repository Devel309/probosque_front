import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { EvaluacionAmbientalTabla } from "./evaluacion-ambiental-tabla/evaluacion-ambiental.tabla";
import { ActividadComponent } from "./modal/actividad/actividad.component";
import { ContingenciaComponent } from "./modal/contingencia/contingencia.component";
import { FormActividadVigilanciaComponent } from "./modal/form-actividad-vigilancia/form-actividad-vigilancia.component";
import { FormActividadComponent } from "./modal/form-actividad/form-actividad.component";
import { ImpactoComponent } from "./modal/impacto/impacto.component";

@Component({
  selector: "app-tab-evaluacion-ambiental",
  templateUrl: "./tab-evaluacion-ambiental.component.html",
  styleUrls: ["./tab-evaluacion-ambiental.component.scss"],
})
export class TabEvaluacionAmbientalComponent implements OnInit {
  @Input() idPGMF: any;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @ViewChild(EvaluacionAmbientalTabla) tablaDinamica!: EvaluacionAmbientalTabla;

  ref!: DynamicDialogRef;

  listaContingencia: any[] = [];

  listaCatalogoContingencia: any[] = [
    { valor1: "Incendios", codigo: "CONTIINC" },
    {
      valor1: "Derrame de conbustible  y/o lubricantes",
      codigo: "CONTIDER",
    },
    {
      valor1: "Riesgos a la salud",
      codigo: "CONTIRIE",
    },
    { valor1: "Invasiones", codigo: "CONTIINV" },
    { valor1: "Poblaciones no contactadas", codigo: "CONTIPOB" },
    { valor1: "Otros", codigo: "CONTIOTRO" },
  ];

  nombrefile = "";

  archivoPGA: any;

  archivo: any;

  factor: any;
  actividades: any[] = [];
  actividades1: any[] = [];
  actividades2: any[] = [];

  selectAnexo: string | null = null;
  nombreArchivoAnexo: string = "";
  idPlanManejoArchivo: number = 0;
  CodigoPOPAC = CodigoPOPAC;
  codTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";
  subCodTipoAprovechamiento: string = "";

  isPoblacionesNoContactadas: boolean = false;
  accept: string = "application/msword,application/pdf";

  totalRecordsACPC: number = 0;
  totalRecordsAVS: number = 0;
  totalRecordsCont: number = 0;
  codigoProceso = CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC;

  @Output() listado = new EventEmitter();
  isLoading: boolean = false; 

  constructor(
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private user: UsuarioService,
    private postulacionPFDMService: PostulacionPFDMService,
    private messageService: MessageService,
    private toast: ToastService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService
  ) {
    this.archivo = {
      idEvalActividad: 0,
      codTipoActividad: "POPAC",
      tipoActividad: "ARCHIVO",
      tipoNombreActividad: "",
      nombreActividad: "",
      file: null,
    };
    this.archivoPGA = {
      file: null,
    };
  }

  ngOnInit(): void {
    this.factoresAmbientales();
    this.getContingencia();
    this.getArchivo();
  }

  getArchivo() {
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad({
        idPlanManejo: this.idPGMF,
        tipoActividad: "ARCHIVO",
      })
      .subscribe((res: any) => {
        if (res?.success && res?.data?.length > 0) {
          this.archivo = res.data;
          this.archivo.file = this.archivo.nombreActividad;
        }
      });
  }

  onList(list: any) {
    this.actividades = [];
    this.actividades = [...list];

    this.actividades1 = [];
    this.totalRecordsACPC = 0;
    this.actividades.forEach((item: any) => {
      if (item.tipoAprovechamiento != 'CONTIN') {
        this.totalRecordsACPC += 1;
        this.actividades1.push(item);
      }
    });

    this.actividades2 = [];
    this.totalRecordsAVS = 0;
    this.actividades.forEach((item: any) => {
      if (!!item.impacto  || !!item.medidasControl) {
        this.totalRecordsAVS += 1;
        this.actividades2.push(item);
      }
    });
  }

  openEliminarRegistroActividades(event: Event, data: any, index: number) {
    

    this.confirmationService.confirm({
      target: event.target || undefined,
      message:
        "Al eliminar este registro, se eliminara la actividad" +
        " " +
        data.nombreAprovechamiento +
        " " +
        "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.actividades2.splice(index, 1);
                this.toast.ok("Se eliminó la información correctamente.");
                this.tablaDinamica.getData();
              }
            });
        } else {
          this.actividades2.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  openEliminarPreventivoCorrector(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.actividades1.splice(index, 1);
                this.toast.ok("Se eliminó la información correctamente.");
                this.tablaDinamica.getData();
              }
            });
        } else {
          this.actividades1.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  factoresAmbientales() {
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad({ tipoActividad: "FACTOR" })
      .subscribe((response: any) => {
        this.factor = response.data;
      });
  }

  modalActividadPreventivo = () => {
    this.ref = this.dialogService.open(FormActividadComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Pre Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "PREAPR";
          this.subCodTipoAprovechamiento = "SUBPREAPR";
          this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
        }

        if (resp.tipo == "Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "APROVE";
          this.subCodTipoAprovechamiento = "SUBAPROVE";
          this.tipoNombreAprovechamiento = "Aprovechamiento";
        }
        if (resp.tipo == "Post Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "POSTAP";
          this.subCodTipoAprovechamiento = "SUBPOSTAP";
          this.tipoNombreAprovechamiento = "Post Aprovechamiento";
        }

        if (resp.tipo == "Otros") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "OTROS";
          this.subCodTipoAprovechamiento = "SUBOTROS";
          this.tipoNombreAprovechamiento = "Otros";
        }

        const params = {
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp.actividad,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .pipe(finalize(()=> this.isLoading = true))
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  modalActividadVigilancia = () => {
    this.ref = this.dialogService.open(FormActividadVigilanciaComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Pre Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "PREAPR";
          this.subCodTipoAprovechamiento = "SUBPREAPR";
          this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
        }

        if (resp.tipo == "Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "APROVE";
          this.subCodTipoAprovechamiento = "SUBAPROVE";
          this.tipoNombreAprovechamiento = "Aprovechamiento";
        }
        if (resp.tipo == "Post Aprovechamiento") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "POSTAP";
          this.subCodTipoAprovechamiento = "SUBPOSTAP";
          this.tipoNombreAprovechamiento = "Post Aprovechamiento";
        }
        if (resp.tipo == "Otros") {
          this.codTipoAprovechamiento = this.codigoProceso;
          this.tipoAprovechamiento = "OTROS";
          this.subCodTipoAprovechamiento = "SUBOTROS";
          this.tipoNombreAprovechamiento = "Otros";
        }

        const params = {
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp.actividad,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsable: resp.responsable,
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  modalActividad = (data: any) => {
    this.ref = this.dialogService.open(ActividadComponent, {
      header: "Editar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        const params = {
          idEvalAprovechamiento: data.idEvalAprovechamiento,
          codTipoAprovechamiento: data.codTipoAprovechamiento,
          tipoAprovechamiento: data.tipoAprovechamiento,
          tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
          nombreAprovechamiento: data.nombreAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsable: resp.responsable,
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: this.user.idUsuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
            
          });
      }
    });
  };

  modalImpacto = (data: any) => {
    this.ref = this.dialogService.open(ImpactoComponent, {
      header: "Editar Impacto",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        const params = {
          idEvalAprovechamiento: data.idEvalAprovechamiento,
          codTipoAprovechamiento: data.codTipoAprovechamiento,
          tipoAprovechamiento: data.tipoAprovechamiento,
          tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
          nombreAprovechamiento: data.nombreAprovechamiento,
          impacto: data.impacto,
          medidasControl: data.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          responsable: resp.responsable,
          idUsuarioRegistro: this.user.idUsuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de vigilancia y seguimiento ambiental correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  openEliminarRegistroContingencia(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.getContingencia();
                this.toast.ok("Se eliminó la Contingencia correctamente.");
              }
            });
        } else {
          this.listaContingencia.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  modalContingencia = (mesaje: string, edit?: any, data?: any) => {
    let idEvalAprovechamiento: number = 0;
    if (edit == "true") {
      idEvalAprovechamiento = data.idEvalAprovechamiento;
    }
    this.ref = this.dialogService.open(ContingenciaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        editar: edit,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        const params = {
          idEvalAprovechamiento,
          codTipoAprovechamiento: CodigoPOPAC.CODIGO_PROCESO,
          tipoAprovechamiento: "CONTIN",
          tipoNombreAprovechamiento: !!resp.otra
            ? resp.otra
            : edit == "true"
            ? this.listaCatalogoContingencia.filter(
                (data: any) => data.valor1 == resp.contingencia
              )[0].codigo
            : resp.contingencia,
          nombreAprovechamiento:
            edit == "true"
              ? resp.contingencia
              : this.listaCatalogoContingencia.filter(
                  (data: any) => data.codigo == resp.contingencia
                )[0].valor1,
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: resp.accionesRealizar,
          responsable: "",
          responsableSecundario: resp.responsable,
          contingencia: resp.otra
            ? resp.otra
            : edit == "true"
            ? resp.contingencia
            : this.listaCatalogoContingencia.filter(
                (data: any) => data.codigo == resp.contingencia
              )[0].valor1,
          auxiliar: this.selectAnexo,
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe(() => this.getContingencia(edit));
      }
    });
  };

  updateContin(e: any) {
    this.getContingencia();
  }

  getContingencia(edit?: any) {
    this.isPoblacionesNoContactadas = false;
    this.listaContingencia = [];
    const body = { idPlanManejo: this.idPGMF, tipoAprovechamiento: "CONTIN" };
    this.planificacionService
      .getAprovechamiento(body)
      .subscribe((response: any) => {
        if (response.success) {
          this.totalRecordsCont = response.data.length;
          response.data.forEach((element: any) => {
            if (element.tipoNombreAprovechamiento === "CONTIPOB") {
              this.isPoblacionesNoContactadas = true;
            }
            this.selectAnexo = element.auxiliar;

            if (
              element.tipoNombreAprovechamiento !==
              "NO HAY DATA EN EL PLAN PADRE"
            ) {
              if (element.codTipoAprovechamiento != "PMFIC") {
                this.listaContingencia.push(element);
              }
            }
          });
        }
      });
  }

  guardar() {
    if (!this.validarNuevo()) {
      return;
    }
    let item = this.listaContingencia.length;
    let obj = this.listaContingencia[item - 1];
    obj.auxiliar = this.selectAnexo;

    this.planificacionService
      .registrarAprovechamientoEvalAmbiental(obj)
      .subscribe(() => {
        this.toast.ok(
          "Se registró plan de contingencia ambiental correctamente.\n"
        );
        this.getContingencia();
      });
  }

  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectAnexo == "S") {
      if (this.idPlanManejoArchivo == 0) {
        validar = false;
        mensaje = mensaje += "(*) Debe Cargar el archivo requerido.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  eliminarContingencia = (item: any) => {
    this.listaContingencia = this.listaContingencia.filter(
      (x) => x.idContigencia != item.idContigencia
    );
  };

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  btnDescargarFormatoPGA() {
    window.location.href =
      "/assets/evaluacionAmbiental/Plantilla formato Plan de Gestion Ambiental.xlsx";
  }

  /*  btnCargarPGA() {
    if (!(this.archivoPGA.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    let item = {
      nombreHoja: "DATOS",
      numeroFila: 2,
      numeroColumna: 1,
      idPlanManejo: this.idPGMF,
      codTipoAprovechamiento: "POPAC",
      tipoAprovechamiento: "",
      tipoNombreAprovechamiento: "",
      idUsuarioRegistro: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarPlanGestionExcel(this.archivoPGA.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.idPlanManejo, item.codTipoAprovechamiento, item.tipoAprovechamiento, item.tipoNombreAprovechamiento, item.idUsuarioRegistro)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registraron los Planes de Gestión Ambiental correctamente.\n');
          this.getArchivo();
          this.getContingencia();
          this.tablaDinamica.getData();
          this.archivoPGA.file = "";
        }else {
          this.toast.error('Ocurrió un error.');
        }
      });
  } */
}
