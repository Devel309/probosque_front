import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { descargarArchivo, isNullOrEmpty, ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ButtonsCreateQuery } from 'src/app/features/state/buttons-create.query';
import { ButtonsCreateStore } from 'src/app/features/state/buttons-create.store';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { FileModel } from 'src/app/model/util/File';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { CargaArchivosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/caragaArchivos.service';
import { CargaEnvioDocumentacionService } from 'src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service';

@Component({
  selector: 'app-tab-resumen',
  templateUrl: './tab-final.component.html',
  styleUrls: ['./tab-final.component.scss'],
})
export class TabFinalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output()
  public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  isTodosPlanesTerminado: boolean = true;
  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACINFG',
      label: '1. Información General',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACOBJ',
      label: '2. Objetivos',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACINFBA',
      label: '3. Información básica',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACAF	',
      label: '4. Aspectos físicos',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACAB',
      label: '5. Aspectos biológicos',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACIS',
      label: '6. Información socioeconómica',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACOP',
      label: '7. Ordenamiento y protección de la UMF',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACPPF',
      label: '8. Potencial de producción forestal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACSMFUM',
      label: '9. Sistema de manejo forestal de uso múltiple',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACEA	',
      label: '10. Evaluación ambiental',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACORG',
      label: '11. Organización para el desarrollo de actividades',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACORGCA',
      label: '12. Cronograma de Actividades',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACORGRMF',
      label: '13. Rentabilidad del manejo forestal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACORGAC',
      label: '14. Aspectos complementarios',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POPACORGA',
      label: '15. Anexos',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
  ];

  verPOPAC: boolean = false;
  pendientes: any[] = [];
  verDescargaPOPAC: boolean = true;
  verDescargaAnexoPOPAC: boolean = true;

  files: FileModel[] = [];
  filePOPAC: FileModel = {} as FileModel;

  consolidadoPOPAC: any = null;

  cargarPOPAC: boolean = false;
  eliminarPOPAC: boolean = true;
  idArchivoPOPAC: number = 0;
  verEnviar: boolean = false;

  checkAll: boolean = false;
  enviarARFFS: boolean = false;

  isSubmittingFormulario$ = this.formularioQuery.selectSubmitting();
  isLoadingFormulario$ = this.formularioQuery.selectLoading();

  isSubmittingGenerar$ = this.generarQuery.selectSubmitting();
  isLoadingGenerar$ = this.generarQuery.selectLoading();

  isSubmittingPlanFormulado$ = this.planFormuladoQuery.selectSubmitting();

  isSubmittingARFFS$ = this.ARFFSQuery.selectSubmitting();

  CodigoProceso = CodigoProceso;

  accept: string = 'image/jpeg,image/png,image/jpeg,application/pdf';

  masterSelected: boolean = false;

  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private cargaArchivosService: CargaArchivosService,
    private toast: ToastService,
    private user: UsuarioService,
    private generarStore: ButtonsCreateStore,
    private generarQuery: ButtonsCreateQuery,
    private formularioStore: ButtonsCreateStore,
    private formularioQuery: ButtonsCreateQuery,
    private evaluacionService: EvaluacionService,
    private planFormuladoStore: ButtonsCreateStore,
    private planFormuladoQuery: ButtonsCreateQuery,
    private ARFFSStore: ButtonsCreateStore,
    private ARFFSQuery: ButtonsCreateQuery,
    private informacionGeneralService: InformacionGeneralService
  ) {}

  ngOnInit(): void {
    this.listarEstados();
    this.obtenerEstadoPlan();
  }

  obtenerEstadoPlan() {
    const body = {
      idPlanManejo: this.idPlanManejo
    }
    this.informacionGeneralService.listarInfGeneralResumido(body).subscribe((_res: any) => {

      if (_res.data.length > 0) {
        const estado = _res.data[ 0 ];
        if (estado.codigoEstado == 'EPLMPRES' || estado.codigoEstado == 'EMDOBS' || estado.codigoEstado == 'EMDBOR') {
          this.disabled = true;
        }
        if (estado.codigoEstado == 'EPLMBOR') {
         this.disabled = false;
        }
      }
    })
  }

  regresarTab() {
    this.regresar.emit();
  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = 'CPMTERM';
      this.listProcesos[objIndex].estado = 'Terminado';
    } else {
      this.listProcesos[objIndex].codigoEstado = 'CPMPEND';
      this.listProcesos[objIndex].estado = 'Pendiente';
    }

    if (
      this.listProcesos.length ===
      this.listProcesos.filter((data) => data.marcar === true).length
    ) {
      // this.verPOPAC = false;
      this.isTodosPlanesTerminado = true;
    } else {
      this.isTodosPlanesTerminado = false;
    }

    this.isAllSelected();
  };

  listarEstados() {
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: 'POPAC',
      idPlanManejoEstado: null,
    };

    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == 'CPMPEND') {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == 'CPMPEND') {
            this.isTodosPlanesTerminado = false;
            this.listProcesos[objIndex].estado = 'Pendiente';
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == 'CPMTERM') {
            this.listProcesos[objIndex].estado = 'Terminado';
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
          this.isAllSelected();
        });
        this.pendiente();
      });
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPOPAC = true;
    } else {
      this.verPOPAC = false;
    }
  }

  guardarEstados() {
    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC,
        descripcion: '',
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        observacion: '',
      };
      listaEstadosPlanManejo.push(obje);
    });

    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == 'CPMPEND') {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();

          this.messageService.add({
            key: 'tl',
            severity: 'success',
            summary: '',
            detail:
              'Se registró los estados del Plan Operativo - PO correctamente.',
          });
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: response.message,
          });
        }
      });
  }

  generarPOPAC() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaArchivosService
      .consolidadoPOPAC(this.idPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.consolidadoPOPAC = res;
            this.verDescargaPOPAC = false;
          } else {
            this.toast.error(res?.message);
          }
        },
        (err) => {
          this.toast.warn(
            'Para generar el consolidado del Plan Operativo para concesiones - POPAC. Debe tener todos los items completados previamente.'
          );
        }
      );
  }

  descargarPOPAC() {
    if (isNullOrEmpty(this.consolidadoPOPAC)) {
      this.toast.warn('Debe generar archivo consolidado');
      return;
    }
    descargarArchivo(this.consolidadoPOPAC);
  }

  registrarArchivoId(event: any) {
    if (event) {
      this.verEnviar = true;
    }
  }

  enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: 'EPLMPRES',
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  guardarPlanFormulado() {
    this.planFormuladoStore.submit();
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: 'EPLMPRES',
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.evaluacionService
      .actualizarPlanManejo(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.planFormuladoStore.submitSuccess();
          this.toast.ok('Esta solicitud ha sido Formulada.');
          this.enviarARFFS = true;
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }

  actualizarPlanManejoEstado(param: any) {
    this.ARFFSStore.submit();
    this.evaluacionService
      .actualizarPlanManejo(param)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok(response?.message);
          this.enviarARFFS = false;
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  isAllSelected() {
    this.masterSelected = this.listProcesos.every(function (item: any) {
      return item.marcar == true;
    });
  }
}
