import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {InformacionGeneralPGMFA} from '../../../../../model/ResumenEjecutivoPGMFA';
import {MessageService} from 'primeng/api';
import {MatDialog} from '@angular/material/dialog';
import {ToastService} from '@shared';
import {InformacionGeneralService} from '../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';

@Component({
    selector: 'app-tab-aspectos-complementarios',
    templateUrl: './tab-aspectos-complementarios.component.html'
  })
export class TabAspectosComplementariosComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();

  accept: string = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  @Input() idPlanManejo!: number;

  @Input("isDisabled") isDisabled: boolean = false;

  informacionGeneral: InformacionGeneralPGMFA = new InformacionGeneralPGMFA();

  constructor(private messageService: MessageService,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionGeneralService: InformacionGeneralService) { }

  ngOnInit(): void {
    this.obtenerResumenEjecutivo();
  }

  obtenerResumenEjecutivo() {
    var params = {
      codigoProceso: 'POPAC',
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.isDisabled = true;
          response.data.forEach((element: any) => {
            this.informacionGeneral = new InformacionGeneralPGMFA(element);
          });
        }else {
          this.isDisabled = false;
        }
      });
  }

  actualizarAspectosComplementarios() {

    if(this.informacionGeneral.detalle == undefined || this.informacionGeneral.detalle ==''){
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: "Debe ingresar el campo Observación.",
      });

      return;
    }


    var params = this.informacionGeneral;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe(
        (result: any) => {
          this.obtenerResumenEjecutivo();
          this.dialog.closeAll();
          this.messageService.add({
            severity: 'success',
            summary: '',
            detail: "Se registró los aspectos complementarios correctamente.",
          });
        },
        (error: any) => {
          this.dialog.closeAll();
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: error.message,
          });
        }
      );
  }

    siguienteTab(){
      this.siguiente.emit();
    }

    regresarTab(){
      this.regresar.emit();
    }

  }
