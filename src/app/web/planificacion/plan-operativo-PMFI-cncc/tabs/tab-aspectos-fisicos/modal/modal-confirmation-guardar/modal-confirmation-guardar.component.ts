import { Component, OnInit } from "@angular/core";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-confirmation-guardar",
  templateUrl: "./modal-confirmation-guardar.component.html",
  styleUrls: ["./modal-confirmation-guardar.component.scss"],
})
export class ModalConfirmationEvaluacionComponentAF implements OnInit {
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit(): void {}

  agregar() {
    this.ref.close("YES");
  }

  cerrarModal() {
    this.ref.close();
  }
}
