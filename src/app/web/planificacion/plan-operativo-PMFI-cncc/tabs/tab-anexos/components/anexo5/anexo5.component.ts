import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import autoTable from 'jspdf-autotable';
import jsPDF from "jspdf";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { ModalAnexo5Component } from "../../modal/modal-anexo5/modal-anexo5.component";
import { environment } from "@env/environment";

@Component({
  selector: "anexo5",
  templateUrl: "./anexo5.component.html",
  styleUrls: ["./anexo5.component.scss"],
})
export class Anexo5ComponentPOPAC implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  
  varAssets = `${environment.varAssets}`;
  listAnexo5Maderables: any[] = [];
  ref!: DynamicDialogRef;
  codigoUnidadMedida:any;

  constructor(
    private user: UsuarioService,
    private toast: ToastService,
    private anexosService: AnexosService,
    private dialog: MatDialog,
    public dialogService: DialogService,
  ) {}

  ngOnInit(): void {
    this.listAnexo5();
  }


  calidadFuste = [
    { codigo: "ARCALMUB", valor1: "Muy Bueno" },
    { codigo: "ARCALBUE", valor1: "Bueno" },
    { codigo: "ARCALREG", valor1: "Regular" },
    { codigo: "ARCALMAL", valor1: "Malo" },
  ];

  listAnexo5() {
    this.listAnexo5Maderables = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
     //idPlanManejo: 1846,
      tipoPlan: "POPAC",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .anexo2POPAC(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo5Maderables.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }


  openModalEditarArbol(item: any){
    this.ref = this.dialogService.open(ModalAnexo5Component, {
      header:
        "Editar resultados del censo forestal comercial con fines maderables.",
      width: "40%",
      data: {
        idPlanManejo: this.idPlanManejo,
        form: item,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if(resp){
        if (resp.c == "Métro Cúbico") {
          this.codigoUnidadMedida = "UNDMEMET";
        } else if (resp.c == "Kilogramo") {
          this.codigoUnidadMedida = "UNDMEKIL ";
        } else if (resp.c == "Litro") {
          this.codigoUnidadMedida = "UNDMELIT";
        }
        const calidadF=  this.calidadFuste.filter(value=> value.valor1 == resp.calidadFuste)

        const obj = {
          idTipoArbol: resp.categoria,
          codigoArbolCalidad: calidadF.length? calidadF[0].codigo: '',
          codigoUnidadMedida:  this.codigoUnidadMedida,
          faja: resp.numeroFaja,
          dap: resp.dap,
          alturaComercial: resp.alturaComercial,
          este: resp.este,
          norte: resp.norte,
          volumen: resp.volumen,
          nombreCientifico: resp.nombreEspecies,
          numeroCorrelativoArbol: resp.nArbol,
        };
        const params = {
          idCensoForestalDetalle: resp.idCensoForestalDetalle,
          idUsuarioRegistro: this.user.idUsuario,
        };

        this.anexosService
          .actualizarCensoForestal(params, obj)
          .subscribe((data: any) => {
            if (data.success) {
              // this.toast.ok(data.message);
              this.toast.ok('Se actualizó el registro correctamente.');
              this.listAnexo5();
            } else {
              this.toast.warn(data.message);
            }
          });

      }
    })
  }

  descargarListadoAnexo5() {
    if (!this.listAnexo5Maderables.length) {
      this.toast.warn("No se encontró ningún registro")

    } else {
      const doc = new jsPDF({
        orientation: 'landscape'
      });
      const head = [ [
        { content: 'N°', rowSpan: 2 },
        { content: 'N° faja', rowSpan: 2 },
        { content: 'N° Árbol (Código)', rowSpan: 2 },
        { content: 'Especie', rowSpan: 2 },
        { content: 'DAP (cm)', rowSpan: 2 },
        { content: 'Altura comercial (m)', rowSpan: 2 },
        { content: 'Calidad fuste', rowSpan: 2 },
        { content: 'Coordenadas UTM', colSpan: 2 },
        { content: 'Vc (m3)', rowSpan: 2 },
        { content: 'Árbol Aprovechable/ Semillero', rowSpan: 2 } ],
      [ 'E', 'N' ] ];
      let data: any = [];

      this.dialog.open(LoadingComponent, { disableClose: true });

      this.listAnexo5Maderables.forEach((t: any, i: any) => {
        data.push([ i + 1, t.numeroFaja, t.nArbol, t.nombreEspecies, t.dap, t.alturaComercial, t.calidadFuste, t.este, t.norte, t.volumen, t.categoria ])
      });

      let urlImagen: string = "";

      if (this.varAssets && this.varAssets != "") {
        urlImagen = '/' + this.varAssets + '/assets/img/ministerio-agricultura-serfor.png';
      } else {
        urlImagen = 'assets/img/ministerio-agricultura-serfor.png';
      }

      var img = new Image();
      img.src = urlImagen;
      doc.addImage(img, 2, 2, 70, 8);
      doc.setFontSize(14);
      doc.setTextColor(0, 0, 0);
      doc.text(`Ep3.1.8: Plan Operativo PMFI en Comunidades Campesinas y Nativas `, 15, 20);
      doc.setTextColor(76, 175, 80);
      doc.text(` N° ${this.idPlanManejo} `, 170, 20);
      doc.setFontSize(12);
      doc.setTextColor(0, 0, 0);
      doc.text(`Anexo 5 - Resultados del censo forestal comercial con fines maderables `, 15, 30);

      autoTable(doc, {
        head: head,
        margin: { top: 35 },
        body: data,
        didDrawCell: (data: any) => {
        },
        styles: { fontSize: 10, },
      });
      this.dialog.closeAll();
      doc.save(`Ep3.1.8 - Plan Operativo PMFI - Anexo 5 - Resultados del censo forestal comercial con fines maderables - Plan N° ${this.idPlanManejo}.pdf`);
    }
  }
}
