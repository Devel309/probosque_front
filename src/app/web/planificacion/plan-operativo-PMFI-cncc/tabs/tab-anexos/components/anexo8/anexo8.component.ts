import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { environment } from "@env/environment";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import autoTable from 'jspdf-autotable';
import jsPDF from "jspdf";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";

@Component({
  selector: "anexo8",
  templateUrl: "./anexo8.component.html",
  styleUrls: ["./anexo8.component.scss"],
})
export class Anexo8ComponentPOPAC implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;

  varAssets = `${environment.varAssets}`;
  listAnexo8Especies: any[] = [];

  constructor(
    private user: UsuarioService,
    private toast: ToastService,
    private anexosService: AnexosService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.listAnexo8();
  }

  listAnexo8() {
    this.listAnexo8Especies = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
    // idPlanManejo: 1846,
      tipoPlan: "POPAC",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .listaEspeciesInventariadasMaderablesObtener(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo8Especies.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  descargarListadoAnexo8() {

    if (!this.listAnexo8Especies.length) {
      this.toast.warn("No se encontró ningún registro")

    } else {
      const doc = new jsPDF({
        orientation: 'landscape'
      });
      const head = [ [ 'N°', 'Nombre Común', 'Nombre Nativo', 'Nombre Científico' ] ];
      let data: any = [];

      this.dialog.open(LoadingComponent, { disableClose: true });

      this.listAnexo8Especies.forEach((t: any, i: any) => {
        data.push([ i + 1, t.textNombreComun, t.nombreNativo, t.textNombreCientifico ])
      });

      let urlImagen: string = "";

      if (this.varAssets && this.varAssets != "") {
        urlImagen = '/' + this.varAssets + '/assets/img/ministerio-agricultura-serfor.png';
      } else {
        urlImagen = 'assets/img/ministerio-agricultura-serfor.png';
      }

      var img = new Image();
      img.src = urlImagen;
      doc.addImage(img, 2, 2, 70, 8);
      doc.setFontSize(14);
      doc.setTextColor(0, 0, 0);
      doc.text(`Ep3.1.8: Plan Operativo PMFI en Comunidades Campesinas y Nativas `, 15, 20);
      doc.setTextColor(76, 175, 80);
      doc.text(` N° ${this.idPlanManejo} `, 170, 20);
      doc.setFontSize(12);
      doc.setTextColor(0, 0, 0);
      doc.text(`Anexo 8 - Lista de especies mencionadas `, 15, 30);

      autoTable(doc, {
        head: head,
        margin: { top: 35 },
        body: data,
        didDrawCell: (data: any) => {
        },
        styles: { fontSize: 10, },
      });
      this.dialog.closeAll();
      doc.save(`Ep3.1.8 - Plan Operativo PMFI - Anexo 8 - Lista de especies mencionadas - Plan N° ${this.idPlanManejo}.pdf`);
    }
  }
}
