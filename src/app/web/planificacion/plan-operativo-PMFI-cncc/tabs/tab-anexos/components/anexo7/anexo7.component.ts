import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import autoTable from 'jspdf-autotable';
import jsPDF from "jspdf";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { ModalAnexo7Component } from "../../modal/modal.actividades/modal.actividades.component";
import { environment } from "@env/environment";

@Component({
  selector: "anexo7",
  templateUrl: "./anexo7.component.html",
  styleUrls: ["./anexo7.component.scss"],
})
export class Anexo7ComponentPOPAC implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  
  varAssets = `${environment.varAssets}`;
  listAnexo7NoMaderables: any[] = [];
  ref!: DynamicDialogRef;
  codigoUnidadMedida: string = "";
  f!: FormGroup;

  constructor(
    private user: UsuarioService,
    private toast: ToastService,
    private anexosService: AnexosService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    public fb: FormBuilder
  ) {

    this.f= fb.group({
      nombreComun:[''],
      codigo:[''],
      coordenadaN:[''],
      coordenadaE:[''],
    })
  }

  ngOnInit(): void {
    this.listAnexo7();
  }

  listAnexo7() {
    this.listAnexo7NoMaderables = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
     // idPlanManejo: 1846,
      tipoPlan: "POPAC",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .listaAnexo7NoMaderable(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo7NoMaderables.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }


  buscarAnexo7NoMaderable() {
    this.listAnexo7NoMaderables = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
     // idPlanManejo: 1846,
      tipoPlan: "POPAC",
    };

    const {nombreComun,codigo,coordenadaE, coordenadaN }= this.f.value


    if( nombreComun!='' || codigo!=''|| coordenadaE!='' || coordenadaN!=''){

      const body={
        c: null,
        categoria: null,
        codigo: codigo,
        este: coordenadaE,
        nArbol: null,
        nombreEspecies: nombreComun,
        norte: coordenadaN,
        numeroFaja: null
      }

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.anexosService
        .buscarAnexo7NoMaderable(params, body)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((response: any) => {
          if (response.data.length != 0) {
            response.data.forEach((element: any, index: number) => {
              this.listAnexo7NoMaderables.push(element);
            });
          } else {
            this.toast.warn(response.message);
          }
        });
    }
  }


  limpiar(){
    this.f= this.fb.group({
      nombreComun:[''],
      codigo:[''],
      coordenadaN:[''],
      coordenadaE:[''],
    })

    this.listAnexo7();
  }

  openModalConfirmationGuardar(item: any) {
    this.ref = this.dialogService.open(ModalAnexo7Component, {
      header:
        "Editar Resultados del censo forestal comercial con fines no maderables.",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        idPlanManejo: this.idPlanManejo,
        form: item,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if (resp) {
        if (resp.c == "Métro Cúbico") {
          this.codigoUnidadMedida = "UNDMEMET";
        } else if (resp.c == "Kilogramo") {
          this.codigoUnidadMedida = "UNDMEKIL ";
        } else if (resp.c == "Litro") {
          this.codigoUnidadMedida = "UNDMELIT";
        }

        const obj = {
          idTipoArbol: "",
          codigoUnidadMedida: this.codigoUnidadMedida,
          faja: resp.numeroFaja,
          este: resp.este,
          norte: resp.norte,
          volumen: resp.volumen,
          nombreCientifico: resp.nombreEspecies,
          numeroCorrelativoArbol: resp.nArbol,
        };
        const params = {
          idCensoForestalDetalle: resp.idCensoForestalDetalle,
          idUsuarioRegistro: this.user.idUsuario,
        };

        this.anexosService
          .actualizarCensoForestal(params, obj)
          .subscribe((data: any) => {
            if (data.success) {
              // this.toast.ok(data.message);
              this.toast.ok('Se actualizó el registro correctamente.');
              this.listAnexo7();
            } else {
              this.toast.warn(data.message);
            }
          });
      }
    });
  }

  descargarListadoAnexo7() {
    if (!this.listAnexo7NoMaderables.length) {
      this.toast.warn("No se encontró ningún registro")

    } else {
      const doc = new jsPDF({
        orientation: 'landscape'
      });
      const head = [ [
        { content: 'N°', rowSpan: 2 },
        { content: 'N° faja', rowSpan: 2 },
        { content: 'N° Árbol o individuo', rowSpan: 2 },
        { content: 'Especie', rowSpan: 2 },
        { content: 'Coordenadas UTM', colSpan: 2 },
        { content: 'C*', rowSpan: 2 },
        { content: 'Árbol o individuo semillero', rowSpan: 2 } ],
      [ 'E', 'N' ] ];
      let data: any = [];

      this.dialog.open(LoadingComponent, { disableClose: true });

      this.listAnexo7NoMaderables.forEach((t: any, i: any) => {
        data.push([ i + 1, t.numeroFaja, t.nArbol, t.nombreEspecies, t.este, t.norte, t.c, t.categoria ])
      });

      let urlImagen: string = "";

      if (this.varAssets && this.varAssets != "") {
        urlImagen = '/' + this.varAssets + '/assets/img/ministerio-agricultura-serfor.png';
      } else {
        urlImagen = 'assets/img/ministerio-agricultura-serfor.png';
      }

      var img = new Image();
      img.src = urlImagen;
      doc.addImage(img, 2, 2, 70, 8);
      doc.setFontSize(14);
      doc.setTextColor(0, 0, 0);
      doc.text(`Ep3.1.8: Plan Operativo PMFI en Comunidades Campesinas y Nativas `, 15, 20);
      doc.setTextColor(76, 175, 80);
      doc.text(` N° ${this.idPlanManejo} `, 170, 20);
      doc.setFontSize(12);
      doc.setTextColor(0, 0, 0);
      doc.text(`Anexo 7 - Resultados del censo forestal comercial con fines no maderables `, 15, 30);

      autoTable(doc, {
        head: head,
        margin: { top: 35 },
        body: data,
        didDrawCell: (data: any) => {
        },
        styles: { fontSize: 10, },
      });
      this.dialog.closeAll();
      doc.save(`Ep3.1.8 - Plan Operativo PMFI - Anexo 7 - Resultados del censo forestal comercial con fines no maderables - Plan N° ${this.idPlanManejo}.pdf`);
    }
  }
}
