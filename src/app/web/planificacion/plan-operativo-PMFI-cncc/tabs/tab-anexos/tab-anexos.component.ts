import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
@Component({
  selector: "app-tab-anexos",
  templateUrl: "./tab-anexos.component.html",
})
export class TabAnexosComponent implements OnInit {
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @Input() disabled!: boolean;
  @ViewChild("mapOrdenamiento", { static: true })
  public view: any = null;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input() idPGMF!: number;

  mapaBase = [
    { label: "-- Seleccione --", value: null },
    { label: "Archivos shapefiles", value: true },
    { label: "PDF", value: false },
  ];

  constructor() {}

  ngOnInit(): void {}

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
