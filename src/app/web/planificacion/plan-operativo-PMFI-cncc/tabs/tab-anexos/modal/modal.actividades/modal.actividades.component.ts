import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { PlanOPResumenActDetModel } from "src/app/model/planOperativo";
import { ResumenActDetModelPOAC } from "src/app/model/poac-resumen-actividades";
import { UtilitariosService } from "src/app/model/util/util.service";

@Component({
  selector: "app-modal.actividades",
  templateUrl: "./modal.actividades.component.html",
  styleUrls: ["./modal.actividades.component.scss"],
})
export class ModalAnexo7Component implements OnInit {
  form: any;

  arbolesSemilleros = [
    { codigo: "ARBTAPRO", valor1: "A" },
    { codigo: "ARBTSEMI", valor1: "S" },
    { codigo: "ARBTFUTC", valor1: "FC" },
  ];

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.form = this.config.data.form;
  }

  validar() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.form.numeroFaja === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: N° de faja.\n";
    }

    if (this.form.nArbol === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Número de árbol.\n";
    }

    if (this.form.nombreEspecies === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Especie.\n";
    }

    if (this.form.c === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: C.\n";
    }

    if (this.form.este === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Este.\n";
    }

    if (this.form.norte === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Norte.\n";
    }

    if (this.form.categoria === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Árbol o individuo semillero.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  editar() {
    if (!this.validar()) {
      return;
    }
    return this.ref.close(this.form);
  }

  cerrarModal() {
    this.ref.close();
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
