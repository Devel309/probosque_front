import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAnexo5Component } from './modal-anexo5.component';

describe('ModalAnexo5Component', () => {
  let component: ModalAnexo5Component;
  let fixture: ComponentFixture<ModalAnexo5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAnexo5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAnexo5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
