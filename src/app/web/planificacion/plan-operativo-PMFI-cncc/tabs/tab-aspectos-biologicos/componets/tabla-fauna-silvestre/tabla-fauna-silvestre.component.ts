import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CoreCentralService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EspeciesFauna } from "src/app/model/medioTrasporte";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { InformacionBasicaModel } from "../../../../../../../model/PlanManejo/InformacionBasica/InformacionBasicaModel";
import { finalize, tap } from "rxjs/operators";
import { AccesibilidadVias } from "src/app/model/FaunaModel";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ConfirmationService } from "primeng/api";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "tabla-fauna-silvestre",
  templateUrl: "./tabla-fauna-silvestre.component.html",
  styleUrls: [ "./tabla-fauna-silvestre.component.scss" ],
})
export class TablaFaunaSilvestreComponentPOPAC implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;

  CodigoPOPAC = CodigoPOPAC;
  listFauna: any[] = [];
  selectFauna: any[] = [];
  comboListEspeciesFauna: EspeciesFauna[] = [];
  selectedValue: AccesibilidadVias[] = [];
  listGuardarFauna: AccesibilidadVias[] = [];
  deleteFauna: any[] = [];
  selecEspeciesFauna!: AccesibilidadVias;
  verModalFauna: boolean = false;
  pendiente: boolean = false;
  queryFauna: string = "";
  queryFaunaNombre: string = "";
  totalRecords: number = 0;
  objbuscar = {
    dato: "",
    nombreCientifico: "",
    nombreComun:'',
    pageNum: 1,
    pageSize: 10,
  };
  infoBasica: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.CodigoPOPAC.CODIGO_PROCESO,
    codSubInfBasica: this.CodigoPOPAC.TAB_5,
    codNombreInfBasica: this.CodigoPOPAC.TAB_5_1,
    idUsuarioRegistro: this.user.idUsuario,
  });
  isSubmitting$ = this.query.selectSubmitting();

  constructor(
    private coreCentralService: CoreCentralService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private createStore: ButtonsCreateStore,
    private query: ButtonsCreateQuery,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit(): void {
    this.listarInfBasicaFauna();
  }

  // ---------- Aspectos Biológicos Fauna------------------- //

  listarInfBasicaFauna() {
    this.selectedValue = [];
    this.selectFauna = [];
    var params = {
      idInfBasica: this.CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.CodigoPOPAC.TAB_5_1,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          if (response.data[ 0 ].comunidad == "NO HAY DATA EN EL PLAN PADRE") {
            this.pendiente = true;
          }else {
            response.data.forEach((element: any) => {
              element.idEspecie = element.idFauna;
              element.estado = element.descripcion;
              element.nombrecomercial = element.nombreLaguna;
              element.amenaza = element.zonaVida;
              element.cities = element.epoca;
              element.edit = false;
              this.selectFauna.push(element);
              this.listFauna.push(element);
            });
          }

          let list_InfoBasicDetalle = response.data.filter(
            (x: any) => (x.codInfBasicaDet = this.CodigoPOPAC.TAB_5_1)
          );
          if (list_InfoBasicDetalle && list_InfoBasicDetalle.length > 0) {
            let infoBasic = list_InfoBasicDetalle[ 0 ];
            Object.assign(this.infoBasica, infoBasic);
          }
        
        }
      });
  }

  abrirModalEspecieFauna() {
    this.queryFauna = "";
    this.queryFaunaNombre = "";
    this.objbuscar.nombreCientifico = "";
    this.objbuscar.nombreComun = "";
    
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.comboListEspeciesFauna = [];
    this.listaPorFiltroEspecieFauna();
    this.selecEspeciesFauna = {} as AccesibilidadVias;
    this.verModalFauna = true;
  }

  listaPorFiltroEspecieFauna() {
    let params = {
      idEspecie: null,
      nombreCientifico: this.objbuscar.nombreCientifico,
      nombreComun: this.objbuscar.nombreComun,
      autor: null,
      familia: null,
      pageNum: this.objbuscar.pageNum,
      pageSize: this.objbuscar.pageSize,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.coreCentralService
      .listaEspecieFaunaPorFiltro(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.totalRecords = result.totalrecord;
        this.comboListEspeciesFauna = [ ...result.data ];
        this.filtrarFaunaPorId();
      });
  }

  filtrarFaunaPorId() {
 //   this.selectedValue = [];
    var fauna: any[] = [];
    this.selectFauna.forEach((item) => {
      fauna = this.comboListEspeciesFauna.filter(
        (x) => x.idEspecie === item.idFauna
      );
      fauna.forEach((item) => {
        this.selectedValue.push(item);
      })
    });
  }

  onChangeFauna(event: any, data: any) {
    
    if (!event.checked) {
      this.deleteFauna.push(data);
    } else {
      this.deleteFauna.forEach((item, index) => {
        if (data.idEspecie === item.idEspecie) {
          this.deleteFauna.splice(index, 1);
        }
      })
    }

    
  }

  guardarFauna() {
    var tmpValues: any[] = [];
    tmpValues = this.selectedValue;

    if (this.deleteFauna.length > 0) {
      this.deleteFauna.forEach((item) => {
        this.selectFauna.forEach((data, index) => {
          if (data.idEspecie === item.idEspecie) {
            this.selectFauna.splice(index, 1);
          }
        });
        this.eliminarFauna(item);
      });
    }

    this.selectFauna.forEach((item: any) => {
      tmpValues.forEach((element: any, index) => {
        if (item.idEspecie == element.idEspecie) {
          tmpValues.splice(index, 1);
        }
      });
    });

    tmpValues.forEach((item: any) => {
      item.idInfBasicaDet = 0;
      item.idFauna = item.idEspecie;
      item.descripcion = item.estado;
      item.nombreLaguna = item.nombrecomercial;
      item.zonaVida = item.amenaza;
      item.epoca = item.cities;
      item.edit = true;
      this.selectFauna.push(item);
    });

    tmpValues = [];
    this.selectedValue = [];
    this.deleteFauna = [];
    this.verModalFauna = false;
  }

  eliminarFauna(data: any) {
    this.listFauna.forEach((item) => {
      if (data.idEspecie === item.idEspecie) {
        let params = {
          idInfBasica: 0,
          idInfBasicaDet: item.idInfBasicaDet,
          codInfBasicaDet: this.CodigoPOPAC.TAB_5_1,
          idUsuarioElimina: this.user.idUsuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((response: any) => {
            this.dialog.closeAll();
          });
      }
    });
  }

  openEliminarFauna(event: Event, index: number, fauna: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (fauna.idInfBasicaDet > 0) {
          let params = {
            idInfBasica: 0,
            idInfBasicaDet: fauna.idInfBasicaDet,
            codInfBasicaDet: this.CodigoPOPAC.TAB_5_1,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService
            .eliminarInformacionBasica(params)
            .subscribe(
              (data: any) => {
                this.toast.ok(data.message);
                this.selectFauna.splice(index, 1);
                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.selectFauna.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  loadData(e: any) {
    const pageSize = Number(e.rows);
    this.objbuscar.pageNum = Number(e.first) / pageSize + 1;
    this.objbuscar.pageSize = pageSize;
    this.listaPorFiltroEspecieFauna();
  }

  filtrarFauna() {
    if (this.queryFauna || this.queryFaunaNombre) {
      this.objbuscar.nombreCientifico = this.queryFauna;
      this.objbuscar.nombreComun = this.queryFaunaNombre;
    } else {
      this.objbuscar.nombreCientifico = "";
      this.objbuscar.nombreComun = "";
    }
    this.listaPorFiltroEspecieFauna();
  }

  guardarAspectosBiologicos() {
    this.listGuardarFauna = [];
    this.createStore.submit();

    this.selectFauna.forEach((item: any) => {
      this.selecEspeciesFauna = new AccesibilidadVias();
      this.selecEspeciesFauna.idFauna = item.idEspecie;
      this.selecEspeciesFauna.nombreComun = item.nombreComun;
      this.selecEspeciesFauna.nombreCientifico = item.nombreCientifico;
      this.selecEspeciesFauna.nombreLaguna = item.nombrecomercial;
      this.selecEspeciesFauna.familia = item.familia;
      this.selecEspeciesFauna.zonaVida = item.amenaza;
      this.selecEspeciesFauna.epoca = item.cities;
      this.selecEspeciesFauna.descripcion = item.estado;
      this.selecEspeciesFauna.codInfBasicaDet = this.CodigoPOPAC.TAB_5_1;
      this.selecEspeciesFauna.codSubInfBasicaDet = this.CodigoPOPAC.TAB_5_1;
      this.selecEspeciesFauna.idUsuarioRegistro = this.user.idUsuario;
      this.selecEspeciesFauna.idInfBasicaDet = item.idInfBasicaDet ? item.idInfBasicaDet : 0;
      this.listGuardarFauna.push(new AccesibilidadVias(this.selecEspeciesFauna));
    });

    var params = [
      {
        idInfBasica: this.infoBasica.idInfBasica,
        codInfBasica: this.CodigoPOPAC.CODIGO_PROCESO,
        codSubInfBasica: this.CodigoPOPAC.TAB_5,
        codNombreInfBasica: this.CodigoPOPAC.TAB_5_1, //aspectos biologicos
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: this.listGuardarFauna,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(
        finalize(() => this.dialog.closeAll()),
        tap(
          (response: any) => {
            if (response.success == true) {
              this.createStore.submitSuccess();
              this.toast.ok("Se registró los datos de fauna silvestre correctamente.");
              this.listarInfBasicaFauna();
            } else {
              this.createStore.submitError("");
              this.toast.error(response?.message);
            }
          },
          (error) => {
            this.createStore.submitError(error);
            this.toast.error(error.message);
          }
        )
      )
      .subscribe();
  }
}
