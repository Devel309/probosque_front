import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { InformacionBasicaPGMFIC } from "src/app/model/InfirmacionBasicaModel";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";

@Component({
  selector: "app-tab-aspectos-biologicos",
  templateUrl: "./tab-aspectos-biologicos.component.html",
})
export class TabAspectosBiologicosComponent implements OnInit {
  @Input() idPGMF!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  ref: DynamicDialogRef = new DynamicDialogRef();
  listaBosque: any[] = [];
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  idInfBasica_4: number = 0;
  idInfBasicaTB: number = 0;
  idArchivoTB: number = 0;
  isNewTB: boolean = false;
  listArchivo: any[] = [];
  CodigoPOPAC = CodigoPOPAC;
  pendiente: boolean = false;

  isSubmitting$ = this.query.selectSubmitting();

  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private informacionAreaService: InformacionAreaPmfiService,
    private createStore: ButtonsCreateStore,
    private query: ButtonsCreateQuery,
    private toast: ToastService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService
  ) {}

  ngOnInit() {
    this.listarTiposBosque();
    this.listarGeometria();
  }

  listarTiposBosque() {
    this.listaBosque = [];
    const params = {
      idInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: CodigoPOPAC.TAB_5_2,
    };

    this.informacionAreaService.listarInformacionBasica(params).subscribe(
      (result: any) => {
        if (result.data) {
          if (result.data[0].comunidad == "NO HAY DATA EN EL PLAN PADRE") {
            this.pendiente = true;
          } else {
            if (result.data && result.data.length != 0) {
              this.idInfBasicaTB = result.data[0].idInfBasica;
              result.data.forEach((element: any) => {
                const array: any[] = [];
                let objDet = new InformacionBasicaPGMFIC(element);

                array.push(objDet);
                this.listaBosque.push(objDet);
                this.calculateAreaTotalBosques();
              });
            }
          }
        }
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarTipoBosque(items: any) {
    this.isNewTB = true;
    this.listaBosque = [];
    this.listaBosque = items;
    this.calculateAreaTotalBosques();
  }

  calculateAreaTotalBosques() {
    let sum1 = 0;
    this.listaBosque.forEach((t: any) => {
      sum1 += t.areaHa;
    });

    this.listaBosque.forEach((t: any) => {
      t.areaHaPorcentaje = Number(((100 * t.areaHa) / sum1).toFixed(2));
    });
    this.totalAreaBosque = parseFloat(`${sum1.toFixed(2)}`);
    this.totalPorcentajeBosque = parseFloat(`${(100).toFixed(2)} %`);
  }

  registrarTiposBosque() {
    this.createStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listBosqueDet: any[] = [];
    this.listaBosque.forEach((t: any) => {
      let obj = new InformacionBasicaPGMFIC();
      obj.codInfBasicaDet = CodigoPOPAC.TAB_5_2;
      obj.codSubInfBasicaDet = CodigoPOPAC.TAB_5_2;
      obj.areaHa = t.areaHa;
      obj.areaHaPorcentaje = t.areaHaPorcentaje;
      obj.nombre = t.nombre;
      obj.descripcion = t.descripcion;
      obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
      listBosqueDet.push(obj);
    });
    let params = [
      {
        idInfBasica: this.idInfBasicaTB ? this.idInfBasicaTB : 0,
        codInfBasica: CodigoPOPAC.CODIGO_PROCESO,
        codSubInfBasica: CodigoPOPAC.TAB_5,
        codNombreInfBasica: CodigoPOPAC.TAB_5_2,
        idPlanManejo: this.idPGMF,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listBosqueDet,
      },
    ];

    return this.informacionAreaService
      .registrarInformacionBasica(params)
      .pipe(
        tap(
          (response) => {
            this.createStore.submitSuccess();
            this.toast.ok("Se registró tipos de bosque correctamente.");
            this.listarTiposBosque();
          },
          (error) => {
            this.createStore.submitError(error);
            this.toast.error(error.message);
          }
        )
      )
      .subscribe();
  }

  eliminarTiposBosque(event: any) {
    if (this.listaBosque.length > 0) {
      let msg = "¿Está seguro de eliminar los registros?."
      if (this.idArchivoTB != 0) {
        msg += "\nSe eliminará archivo shapefile de Tipos de Bosque."
      }
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: msg,
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Sí",
        rejectLabel: "No",
        accept: () => {
          if (this.idInfBasicaTB != 0) {
            let params = {
              idInfBasica: this.idInfBasicaTB,
              idInfBasicaDet: 0,
              codInfBasicaDet: "",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  // this.SuccessMensaje(result.message);
                  this.toast.ok("Se eliminó tipos de bosque correctamente.");
                } else {
                  this.toast.warn(result.message);
                }
                this.eliminarArchivoDetalle(this.idArchivoTB);
                this.listaBosque = [];
                this.calculateAreaTotalBosques();
                this.isNewTB = false;
                this.idArchivoTB = 0;
                this.idInfBasicaTB = 0;
                // this.listarTiposBosque();
              });
          } else {
            this.listaBosque = [];
            this.calculateAreaTotalBosques();
            this.isNewTB = false;
            this.idArchivoTB = 0;
            this.idInfBasicaTB = 0;
            // this.listarTiposBosque();
          }
        },
        reject: () => { },
      });
    }
  }

  listarGeometria() {
    let item = {
      idPlanManejo: this.idPGMF,
      codigoSeccion: CodigoPOPAC.CODIGO_PROCESO,
      codigoSubSeccion: "POPACINFBATIM",
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe(
        (result: any) => {
          if (result.data.length > 0) {
            result.data.forEach((t: any) => {
              if (t.geometry_wkt !== null) {
                if (!this.listArchivo.includes(t.idArchivo)) {
                  if (t.descripcion == "COD9" && t.tipoGeometria == "TPAREA") {
                    this.idArchivoTB = t.idArchivo;
                    this.listArchivo.push(t.idArchivo);
                  }
                }
              }
            });
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error");
        }
      );
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    if (idArchivo != 0) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.servicePlanManejoGeometria
        .eliminarPlanManejoGeometriaArchivo(idArchivo, this.user.idUsuario)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.toast.ok("Se eliminó el archivo y/o geometría correctamente.");
            } else {
              this.toast.error("Ocurrió un problema, intente nuevamente");
            }
          },
          (error) => {
            this.toast.error("Ocurrió un problema, intente nuevamente");
          }
        );
    }
  }

  registroArchivo(idArchivo: number) {
    this.idArchivoTB = idArchivo;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
