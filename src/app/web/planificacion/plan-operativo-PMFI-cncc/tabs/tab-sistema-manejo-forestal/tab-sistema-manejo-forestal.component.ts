import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ToastService } from '@shared';
import { SistemaManejoForestalService, UsuarioService } from '@services';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ButtonsCreateQuery } from 'src/app/features/state/buttons-create.query';
import { ButtonsCreateStore } from 'src/app/features/state/buttons-create.store';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';
import { CodigoPOPAC } from 'src/app/model/util/POPAC/POPAC';
import { ManejoBosqueCabecera, ManejoBosqueDetalleModel } from '../../../../../model/ManejoBosque';
import { ManejoBosqueService } from '../../../../../service/manejoBosque.service';

@Component({
  selector: 'app-tab-sistema-manejo-forestal',
  templateUrl: './tab-sistema-manejo-forestal.component.html',
})
export class TabSistemaManejoForestalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  ref: DynamicDialogRef = new DynamicDialogRef();
  @Input() disabled!: boolean;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  CodigoPOPAC = CodigoPOPAC;

  item_9_0: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_modal_9_0 = {} as ManejoBosqueDetalleModel;
  lista_9_0: ManejoBosqueDetalleModel[] = [];

  item_9_1: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_9_1_1: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  det_9_1_2: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();

  det_modal_913y923 = {} as ManejoBosqueDetalleModel;
  lista_9_1_3: ManejoBosqueDetalleModel[] = [];

  item_9_2: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_9_2_1: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  det_9_2_2: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  lista_9_2_3: ManejoBosqueDetalleModel[] = [];

  item_9_3: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  det_modal_931y932 = {} as ManejoBosqueDetalleModel;
  lista_9_3_1: ManejoBosqueDetalleModel[] = [];
  lista_9_3_2: ManejoBosqueDetalleModel[] = [];

  tituloModal: string = '';

  totalVcp: number = 0;

  showModal_9_0: boolean = false;
  showModal_913y923: boolean = false;
  showModal_931y932: boolean = false;
  edit: boolean = false;
  indexUpdate: number = -1;
  tipoTabla: string = '';

  isSubmittingUsoPotencial$ = this.saveUsoPotencialQuery.selectSubmitting();
  isSubmittingManejoForestalMaderable$ =
    this.saveManejoForestalMaderableQuery.selectSubmitting();
  isSubmittingManejoForestalNoMaderable$ =
    this.saveManejoForestalNoMaderableQuery.selectSubmitting();
  isSubmittingLaboresSilv$ = this.saveLaboresSilvQuery.selectSubmitting();

  isEmptyPMFIC9_0 = false;
  isEmptyPMFIC9_1 = false;
  isEmptyPMFIC9_2 = false;
  isEmptyPMFIC9_3 = false;
  isValuesPMFIC9_0 = false;
  isValuesPMFIC9_1 = false;
  isValuesPMFIC9_2 = false;
  isValuesPMFIC9_3 = false;

  constructor(
    public dialogService: DialogService,
    private api: SistemaManejoForestalService,
    private dialog: MatDialog,
    private msgService: MessageService,
    private confirmationService: ConfirmationService,
    private manejoBosqueService: ManejoBosqueService,
    private user: UsuarioService,
    private toast: ToastService,
    private saveUsoPotencialStore: ButtonsCreateStore,
    private saveUsoPotencialQuery: ButtonsCreateQuery,
    private saveManejoForestalMaderableStore: ButtonsCreateStore,
    private saveManejoForestalMaderableQuery: ButtonsCreateQuery,
    private saveManejoForestalNoMaderableStore: ButtonsCreateStore,
    private saveManejoForestalNoMaderableQuery: ButtonsCreateQuery,
    private saveLaboresSilvStore: ButtonsCreateStore,
    private saveLaboresSilvQuery: ButtonsCreateQuery
  ) {}

  ngOnInit() {
    this.item_9_0.idPlanManejo = this.idPlanManejo;
    this.item_9_0.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_0.codigoManejo = CodigoPOPAC.CODIGO_PROCESO;
    this.item_9_0.subCodigoManejo = CodigoPOPAC.ACORDEON_9_0;

    this.item_9_1.idPlanManejo = this.idPlanManejo;
    this.item_9_1.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_1.codigoManejo = CodigoPOPAC.CODIGO_PROCESO;
    this.item_9_1.subCodigoManejo = CodigoPOPAC.ACORDEON_9_1;

    this.det_9_1_1.idManejoBosqueDet = 0;
    this.det_9_1_1.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_1;
    this.det_9_1_1.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_1_1;
    this.det_9_1_1.idUsuarioRegistro = this.user.idUsuario;

    this.det_9_1_2.idManejoBosqueDet = 0;
    this.det_9_1_2.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_1;
    this.det_9_1_2.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_1_2;
    this.det_9_1_2.idUsuarioRegistro = this.user.idUsuario;

    //ACORDEON  9_2
    this.item_9_2.idPlanManejo = this.idPlanManejo;
    this.item_9_2.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_2.codigoManejo = CodigoPOPAC.CODIGO_PROCESO;
    this.item_9_2.subCodigoManejo = CodigoPOPAC.ACORDEON_9_2;

    this.det_9_2_1.idManejoBosqueDet = 0;
    this.det_9_2_1.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_2;
    this.det_9_2_1.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_2_1;
    this.det_9_2_1.idUsuarioRegistro = this.user.idUsuario;

    this.det_9_2_2.idManejoBosqueDet = 0;
    this.det_9_2_2.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_2;
    this.det_9_2_2.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_2_2;
    this.det_9_2_2.idUsuarioRegistro = this.user.idUsuario;

    this.item_9_3.idPlanManejo = this.idPlanManejo;
    this.item_9_3.idUsuarioRegistro = this.user.idUsuario;
    this.item_9_3.codigoManejo = CodigoPOPAC.CODIGO_PROCESO;
    this.item_9_3.subCodigoManejo = CodigoPOPAC.ACORDEON_9_3;

    this.listarAcordeon_9_0();
    this.listarAcordeon_9_1();
    this.listarAcordeon_9_2();
    this.listarAcordeon_9_3();
  }

  listarAcordeon_9_0() {
  this.isEmptyPMFIC9_0= false;
  this.isValuesPMFIC9_0= false;
    this.lista_9_0 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPOPAC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPOPAC.ACORDEON_9_0,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .pipe(finalize(() => this.calcularTotal()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];

          if (respuesta.idManejoBosque) {
            if (respuesta.idPlanManejo === this.idPlanManejo) {
              this.item_9_0.idManejoBosque = respuesta.idManejoBosque;
            } else {
              this.item_9_0.idManejoBosque = 0;
              this.isValuesPMFIC9_0 = true;
            }

            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              this.lista_9_0.push(new ManejoBosqueDetalleModel(e));
            });
          } else {
            this.item_9_0.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              this.lista_9_0.push(new ManejoBosqueDetalleModel(e));
            });
          }
        } else {
          this.isEmptyPMFIC9_0 = true;
        }
      });
  }

  listarAcordeon_9_1() {
    this.isEmptyPMFIC9_1 = false;
    this.isValuesPMFIC9_1 = false;
    this.lista_9_1_3 = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPOPAC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPOPAC.ACORDEON_9_1,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];

          if (respuesta.idManejoBosque) {
            if (respuesta.idPlanManejo === this.idPlanManejo) {
              this.item_9_1.idManejoBosque = respuesta.idManejoBosque;
            } else {
              this.item_9_1.idManejoBosque = 0;
              this.isValuesPMFIC9_1= true;
            }

            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_1_1 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_1
              ) {
                this.det_9_1_1 = new ManejoBosqueDetalleModel(e);
              } else if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_1_2 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_2
              ) {
                this.det_9_1_2 = new ManejoBosqueDetalleModel(e);
              } else if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_1_3 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_1_3
              ) {
                this.lista_9_1_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          } else {
            this.item_9_1.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_1_3) {
                this.lista_9_1_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          }
        } else {
          this.isEmptyPMFIC9_1 = true;
        }
      });
  }

  listarAcordeon_9_2() {
    this.isEmptyPMFIC9_2= false;
    this.isValuesPMFIC9_2= false;
    this.lista_9_2_3 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPOPAC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPOPAC.ACORDEON_9_2,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];
          if (respuesta.idManejoBosque) {
            if (respuesta.idPlanManejo === this.idPlanManejo) {
              this.item_9_2.idManejoBosque = respuesta.idManejoBosque;
            } else {
              this.item_9_2.idManejoBosque = 0;
              this.isValuesPMFIC9_2= true;
            }

            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_2_1 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_1
              ) {
                this.det_9_2_1 = new ManejoBosqueDetalleModel(e);
              } else if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_2_2 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_2
              ) {
                this.det_9_2_2 = new ManejoBosqueDetalleModel(e);
              } else if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_2_3 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_3
              ) {
                this.lista_9_2_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          } else {
            this.item_9_2.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_2_3 ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_2_3
              ) {
                this.lista_9_2_3.push(new ManejoBosqueDetalleModel(e));
              }
            });
          }
        }else{
          this.isEmptyPMFIC9_2= true;
        }
      });
  }

  listarAcordeon_9_3() {
    this.isEmptyPMFIC9_3= false;
    this.isValuesPMFIC9_3= false;

    this.lista_9_3_1 = [];
    this.lista_9_3_2 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: CodigoPOPAC.CODIGO_PROCESO,
      subCodigoGeneral: CodigoPOPAC.ACORDEON_9_3,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let respuesta = response.data[0];
          if (respuesta.idManejoBosque) {
            if (respuesta.idPlanManejo === this.idPlanManejo) {
              this.item_9_3.idManejoBosque = respuesta.idManejoBosque;
            } else {
              this.item_9_3.idManejoBosque = 0;
              this.isValuesPMFIC9_3= true;
            }

            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO
              ) {
                this.lista_9_3_1.push(new ManejoBosqueDetalleModel(e));
              } else if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_3_OPCIONALES ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OPCIONALES
              ) {
                this.lista_9_3_2.push(new ManejoBosqueDetalleModel(e));
              }
            });
          } else {
            this.item_9_3.idManejoBosque = 0;
            respuesta.listManejoBosqueDetalle.forEach((e: any) => {
              e.idManejoBosqueDet = 0;
              if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OBLIGATORIO
              ) {
                this.lista_9_3_1.push(new ManejoBosqueDetalleModel(e));
              } else if (
                e.subCodtipoManejoDet == CodigoPOPAC.ACORDEON_9_3_OPCIONALES ||
                e.subCodtipoManejoDet == CodigoPMFIC.ACORDEON_9_3_OPCIONALES
              ) {
                this.lista_9_3_2.push(new ManejoBosqueDetalleModel(e));
              }
            });
          }
        }else{
          this.isEmptyPMFIC9_3= true;
        }
      });
  }

  registrarAcordeon9_1() {
    this.saveManejoForestalMaderableStore.submit();
    this.item_9_1.listManejoBosqueDetalle = this.lista_9_1_3;
    this.item_9_1.listManejoBosqueDetalle.push(this.det_9_1_1);
    this.item_9_1.listManejoBosqueDetalle.push(this.det_9_1_2);
    var params = [this.item_9_1];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            // this.toast.ok(response?.message);
            this.toast.ok('Se registró Manejo forestal con fines maderables correctamente.');
            this.listarAcordeon_9_1();
            this.saveManejoForestalMaderableStore.submitSuccess();
          } else {
            this.toast.error(response?.message);
            this.saveManejoForestalMaderableStore.submitError('');
          }
        },
        (err) => {
          this.saveManejoForestalMaderableStore.submitError(err);
        }
      );
  }

  registrarAcordeon9_2() {
    this.saveManejoForestalNoMaderableStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.item_9_2.listManejoBosqueDetalle = this.lista_9_2_3;
    this.item_9_2.listManejoBosqueDetalle.push(this.det_9_2_1);
    this.item_9_2.listManejoBosqueDetalle.push(this.det_9_2_2);
    var params = [this.item_9_2];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            // this.toast.ok(response?.message);
            this.toast.ok('Se registró Manejo forestal con fines no maderables correctamente.');
            this.listarAcordeon_9_2();
            this.saveManejoForestalNoMaderableStore.submitSuccess();
          } else {
            this.toast.error(response?.message);
            this.saveManejoForestalMaderableStore.submitError('');
          }
        },
        (err) => {
          this.saveManejoForestalMaderableStore.submitError(err);
        }
      );
  }

  registrarAcordeon9_3() {
    this.saveLaboresSilvStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.item_9_3.listManejoBosqueDetalle = this.lista_9_3_1.concat(
      this.lista_9_3_2
    );
    var params = [this.item_9_3];
    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            // this.toast.ok(response?.message);
            this.toast.ok('Se registró Labores silviculturales correctamente.');
            this.listarAcordeon_9_3();
            this.saveLaboresSilvStore.submitSuccess();
          } else {
            this.toast.error(response?.message);
            this.saveLaboresSilvStore.submitError('');
          }
        },
        (err) => {
          this.saveLaboresSilvStore.submitError(err);
        }
      );
  }

  registrarAcordeon9_0() {
    this.saveUsoPotencialStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.item_9_0.listManejoBosqueDetalle = this.lista_9_0;
    var params = [this.item_9_0];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            // this.toast.ok(response?.message);
            this.toast.ok('Se registró Uso potencial y actividades a realizar correctamente.');
            this.listarAcordeon_9_0();
            this.saveUsoPotencialStore.submitSuccess();
          } else {
            this.toast.error(response?.message);
            this.saveUsoPotencialStore.submitError('');
          }
        },
        (err) => {
          this.saveUsoPotencialStore.submitError('');
        }
      );
  }


  guardarInformacionPMFI(){
    this.registrarAcordeon9_0();
    this.registrarAcordeon9_1();
    this.registrarAcordeon9_2();
    this.registrarAcordeon9_3();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  agregar() {
    if (this.edit) {
      this.editarItem();
    } else {
      this.agregarItem();
    }
  }

  validarFormulario_9_0() {
    let respuest: boolean = true;
    if (
      this.det_modal_9_0.tratamientoSilvicultural == null ||
      this.det_modal_9_0.tratamientoSilvicultural == '' ||
      this.det_modal_9_0.tratamientoSilvicultural == undefined
    ) {
      this.toast.warn('Ingrese una Categoría de zonificación de la UMF.');
      respuest = false;
    }
    return respuest;
  }

  validarFormulario_913_923() {
    let respuest: boolean = true;
    if (
      this.det_modal_913y923.tratamientoSilvicultural == null ||
      this.det_modal_913y923.tratamientoSilvicultural == '' ||
      this.det_modal_913y923.tratamientoSilvicultural == undefined
    ) {
      this.toast.warn('Ingrese una Actividad.');
      respuest = false;
    }
    return respuest;
  }

  validarFormulario_931_932() {
    let respuest: boolean = true;
    if (
      this.det_modal_931y932.tratamientoSilvicultural == null ||
      this.det_modal_931y932.tratamientoSilvicultural == '' ||
      this.det_modal_931y932.tratamientoSilvicultural == undefined
    ) {
      this.toast.warn('Ingrese una Práctica Silvicultural.');
      respuest = false;
    }
    return respuest;
  }

  calcularTotal() {
    this.totalVcp = 0;
    this.lista_9_0.forEach((e: ManejoBosqueDetalleModel) => {
      this.totalVcp = Number(this.totalVcp) + Number(e.nuTotalVcp);
    });
  }

  agregarItem() {
    if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_0) {
      if (this.validarFormulario_9_0()) {
        this.det_modal_9_0.idManejoBosqueDet = 0;
        this.det_modal_9_0.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_0;
        this.det_modal_9_0.nuTotalVcp = this.det_modal_9_0.nuTotalVcp
          ? this.det_modal_9_0.nuTotalVcp
          : 0;
        this.det_modal_9_0.idUsuarioRegistro = this.user.idUsuario;

        this.lista_9_0.push(this.det_modal_9_0);
        this.calcularTotal();

        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_1_3) {
      if (this.validarFormulario_913_923()) {
        this.det_modal_913y923.idManejoBosqueDet = 0;
        this.det_modal_913y923.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_1;
        this.det_modal_913y923.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_1_3;
        this.det_modal_913y923.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_1_3.push(this.det_modal_913y923);
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_2_3) {
      if (this.validarFormulario_913_923()) {
        this.det_modal_913y923.idManejoBosqueDet = 0;
        this.det_modal_913y923.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_2;
        this.det_modal_913y923.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_2_3;
        this.det_modal_913y923.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_2_3.push(this.det_modal_913y923);
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO) {
      if (this.validarFormulario_931_932()) {
        this.det_modal_931y932.idManejoBosqueDet = 0;
        this.det_modal_931y932.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_3;
        this.det_modal_931y932.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO;

        this.det_modal_931y932.actividad = CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO;
        this.det_modal_931y932.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_3_1.push(this.det_modal_931y932);
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OPCIONALES) {
      if (this.validarFormulario_931_932()) {
        this.det_modal_931y932.idManejoBosqueDet = 0;
        this.det_modal_931y932.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_3;
        this.det_modal_931y932.subCodtipoManejoDet = CodigoPOPAC.ACORDEON_9_3_OPCIONALES;
        this.det_modal_931y932.actividad = CodigoPOPAC.ACORDEON_9_3_OPCIONALES;
        this.det_modal_931y932.idUsuarioRegistro = this.user.idUsuario;
        this.lista_9_3_2.push(this.det_modal_931y932);
        this.seteoValoresDefecto();
      }
    }
  }

  seteoValoresDefecto() {
    this.tipoTabla = '';
    this.det_modal_913y923 = {} as ManejoBosqueDetalleModel;
    this.showModal_913y923 = false;
    this.det_modal_9_0 = {} as ManejoBosqueDetalleModel;
    this.showModal_9_0 = false;
    this.det_modal_931y932 = {} as ManejoBosqueDetalleModel;
    this.showModal_931y932 = false;

    this.indexUpdate = -1;
    this.edit = false;
  }

  editarItem() {
    if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_0) {
      if (this.validarFormulario_9_0()) {
        if (this.indexUpdate >= 0) {
          this.det_modal_9_0.nuTotalVcp = this.det_modal_9_0.nuTotalVcp
            ? this.det_modal_9_0.nuTotalVcp
            : 0;
          this.lista_9_0[this.indexUpdate] = this.det_modal_9_0;
        }
        this.seteoValoresDefecto();
        this.calcularTotal();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_1_3) {
      if (this.validarFormulario_913_923()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_1_3[this.indexUpdate] = this.det_modal_913y923;
        }
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_2_3) {
      if (this.validarFormulario_913_923()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_2_3[this.indexUpdate] = this.det_modal_913y923;
        }
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO) {
      if (this.validarFormulario_931_932()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_3_1[this.indexUpdate] = this.det_modal_931y932;
        }
        this.seteoValoresDefecto();
      }
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OPCIONALES) {
      if (this.validarFormulario_931_932()) {
        if (this.indexUpdate >= 0) {
          this.lista_9_3_2[this.indexUpdate] = this.det_modal_931y932;
        }
        this.seteoValoresDefecto();
      }
    }

    /* this.tipoTabla = "";
    this.showModal_913y923 = false;
    this.showModal_931y932 = false;
    this.showModal_9_0 = false;
    this.indexUpdate = -1;
    this.det_modal_9_0      = {} as ManejoBosqueDetalleModel;
    this.det_modal_913y923  = {} as ManejoBosqueDetalleModel;
    this.det_modal_931y932  = {} as ManejoBosqueDetalleModel;
    this.edit = false;*/
  }

  openModalNuevo(tipo: string) {
    this.tipoTabla = tipo;
    this.edit = false;

    if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_0) {
      this.tituloModal =
        'Actividad a realizar según la categoría de zonificación de la UMF ';
      this.showModal_9_0 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_1_3) {
      this.tituloModal =
        'Actividad de aprovechamiento y equipos a utilizar con fines maderables ';
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_2_3) {
      this.tituloModal =
        'Actividad de aprovechamiento y equipos a utilizar con fines no maderables ';
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO) {
      this.tituloModal = 'Labores Silviculturales - Actividad Obligatoria ';
      this.showModal_931y932 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OPCIONALES) {
      this.tituloModal = 'Labores Silviculturales - Actividad Opcional ';
      this.showModal_931y932 = true;
    }
  }

  openModalEditar(
    manejoBosqueDetalleModel: ManejoBosqueDetalleModel,
    index: number,
    tipo: string
  ) {
    this.tipoTabla = tipo;
    this.edit = true;
    this.indexUpdate = index;

    if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_0) {
      this.det_modal_9_0 = { ...manejoBosqueDetalleModel };
      this.tituloModal =
        'Actividad a realizar según la categoría de zonificación de la UMF ';
      this.showModal_9_0 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_1_3) {
      this.tituloModal =
        'Actividad de aprovechamiento y equipos a utilizar con fines maderables ';
      this.det_modal_913y923 = { ...manejoBosqueDetalleModel };
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_2_3) {
      this.tituloModal =
        'Actividad de aprovechamiento y equipos a utilizar con fines no maderables ';
      this.det_modal_913y923 = { ...manejoBosqueDetalleModel };
      this.showModal_913y923 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO) {
      this.tituloModal = 'Labores Silviculturales - Actividad Obligatoria ';
      this.det_modal_931y932 = { ...manejoBosqueDetalleModel };
      this.showModal_931y932 = true;
    } else if (this.tipoTabla == CodigoPOPAC.ACORDEON_9_3_OPCIONALES) {
      this.tituloModal = 'Labores Silviculturales - Actividad Opcional';
      this.det_modal_931y932 = { ...manejoBosqueDetalleModel };
      this.showModal_931y932 = true;
    }
  }

  openEliminar(
    event: Event,
    index: number,
    manejoBosqueDetalleModel: ManejoBosqueDetalleModel,
    tabla: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (manejoBosqueDetalleModel.idManejoBosqueDet != 0) {
          var params = {
            idManejoBosque: 0,
            idManejoBosqueDet: manejoBosqueDetalleModel.idManejoBosqueDet,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.dialog.open(LoadingComponent, { disableClose: true });
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((data: any) => {
              if (tabla == CodigoPOPAC.ACORDEON_9_0){
                this.lista_9_0.splice(index, 1);
                this.calcularTotal();
              }
              else if (tabla == CodigoPOPAC.ACORDEON_9_1_3)
                this.lista_9_1_3.splice(index, 1);
              else if (tabla == CodigoPOPAC.ACORDEON_9_2_3)
                this.lista_9_2_3.splice(index, 1);
              else if (tabla == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO)
                this.lista_9_3_1.splice(index, 1);
              else if (tabla == CodigoPOPAC.ACORDEON_9_3_OPCIONALES)
                this.lista_9_3_2.splice(index, 1);

              this.toast.ok(data.message);
            });
        } else {
          if (tabla == CodigoPOPAC.ACORDEON_9_0){
            this.lista_9_0.splice(index, 1);
            this.calcularTotal();
          }
          else if (tabla == CodigoPOPAC.ACORDEON_9_1_3)
            this.lista_9_1_3.splice(index, 1);
          else if (tabla == CodigoPOPAC.ACORDEON_9_2_3)
            this.lista_9_2_3.splice(index, 1);
          else if (tabla == CodigoPOPAC.ACORDEON_9_3_OBLIGATORIO)
            this.lista_9_3_1.splice(index, 1);
          else if (tabla == CodigoPOPAC.ACORDEON_9_3_OPCIONALES)
            this.lista_9_3_2.splice(index, 1);

          this.toast.ok('Detalle de Manejo forestal eliminado correctamente.');
        }
      },
      reject: () => {},
    });
  }

  cerrarModal() {
    this.det_modal_9_0 = {} as ManejoBosqueDetalleModel;
    this.showModal_9_0 = false;
    this.det_modal_913y923 = {} as ManejoBosqueDetalleModel;
    this.showModal_913y923 = false;
    this.det_modal_931y932 = {} as ManejoBosqueDetalleModel;
    this.showModal_931y932 = false;
    this.edit = false;
    this.tipoTabla = '';
    this.indexUpdate = -1;
  }
}
