import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { InformacionBasicaDetalle } from "src/app/model/InformacionAreaManejo";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { MapApi } from "src/app/shared/mapApi";
import { TabInformacionBasicaComponent } from "../../tab-informacion-basica.component";

@Component({
  selector: "tabla-area-manejo-dividida",
  templateUrl: "./tabla-area-manejo-dividida.component.html",
  styleUrls: ["./tabla-area-manejo-dividida.component.scss"],
})
export class TablaAreaManejoDivididaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() listAnexoArea: any = [];
  @Input() totalAnexoArea!: number;
  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;
  @Input() idInfBasica!: number;
  @Output() idInfBasicaS = new EventEmitter<number>();
  @Output() eliminaS = new EventEmitter<boolean>();

  CodigoPOPAC = CodigoPOPAC;
  _idInfBasica: number = 0;
  _idInfBasicaDet: number = 0;
  idArchivoS: number = 0;
  listArchivo: any[] = [];

  constructor(
    private messageService: MessageService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private parentAreManejo: TabInformacionBasicaComponent,
    private mapApi: MapApi,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.listarCoordenadas();
    this.listarGeometria();
  }

  listarCoordenadas() {
    this.listCoordinatesAnexo = [];
    var params = {
      idInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigoPOPAC.TAB_3_2,
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[ 0 ].idInfBasica != null) {
            let newList: any = [];
            this._idInfBasica = response.data[ 0 ].idInfBasica;
            this._idInfBasicaDet = response.data[ 0 ].idInfBasicaDet;
            response.data.forEach((t: any) => {
              if (t.codInfBasicaDet !== null) {
                newList.push({
                  idInfBasica: t.idInfBasica,
                  idInfBasicaDet: t.idInfBasicaDet,
                  codInfBasicaDet: t.codInfBasicaDet,
                  anexo: t.descripcion,
                  vertice: t.puntoVertice,
                  este: t.coordenadaEste,
                  norte: t.coordenadaNorte,
                  referencia: t.referencia,
                });
              }
            });
            this.listCoordinatesAnexo = newList.groupBy((t: any) => t.anexo);
            this.calculatArea();
          }
        } else {
          this._idInfBasicaDet = 0;
        }
        this.idInfBasicaS.emit(this._idInfBasica);
      });
  }

  registrar() {
    if (!this.validarRegistro()) return;

    let listMap: InformacionBasicaDetalle[] = [];

    this.listCoordinatesAnexo.forEach((group: any) => {
      group.value.forEach((value: any) => {
        let coordenada = {} as InformacionBasicaDetalle;
        coordenada.idInfBasica = value.idInfBasica || 0;
        coordenada.idInfBasicaDet = value.idInfBasicaDet
          ? value.idInfBasicaDet
          : 0;
        coordenada.idUnidadManejo = this.idPlanManejo;
        coordenada.idUsuarioRegistro = this.usuario.idusuario;
        coordenada.codInfBasicaDet = CodigoPOPAC.TAB_3_2;
        coordenada.codSubInfBasicaDet = CodigoPOPAC.TAB_3_2;
        coordenada.estado = "A";
        coordenada.descripcion = group.key;
        coordenada.puntoVertice = value.vertice;
        coordenada.coordenadaEsteIni = value.este;
        coordenada.coordenadaNorteIni = value.norte;
        coordenada.referencia = value.referencia;
        coordenada.idUsuarioModificacion = null;
        listMap.push(coordenada);
      });
    });
    let params = [
      {
        idInfBasica: this.idInfBasica,
        codInfBasica: CodigoPOPAC.CODIGO_PROCESO,
        codSubInfBasica: this.CodigoPOPAC.TAB_3,
        codNombreInfBasica: CodigoPOPAC.TAB_3_2,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró la superficie y ubicación de la UMF correctamente.');
          this.listarCoordenadas();
          this.listarGeometria();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  onChangeFileSHP(e: any) {
    let item: any = this.parentAreManejo._layers.find(
      (e: any) => e.descripcion === CodigoPOPAC.TAB_3_2
    );
    if (item !== undefined) {
      this.toast.warn("Ya se encuentra un achivo para esta sección.");
      return;
    }

    this.parentAreManejo.onChangeFileSHP(
      e,
      true,
      "TPPUNTO",
      CodigoPOPAC.TAB_3_2
    );
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaConAnexo(data[0].features);
    });
    e.target.value = "";
  }

  createTablaConAnexo(items: any) {
    let listMap = items.map((t: any) => {
      return {
        anexo: t.properties.anexo,
        vertice: t.properties.vertice,
        este: t.geometry.coordinates[0],
        norte: t.geometry.coordinates[1],
        referencia: "",
      };
    });

    this.listCoordinatesAnexo = listMap.groupBy((t: any) => t.anexo);
    this.calculatArea();
  }

  calculatArea() {
    this.listAnexoArea = [];
    this.listCoordinatesAnexo.forEach((t) => {
      let rings: number[][] = [];
      t.value.forEach((t2: any) => {
        rings.push([t2.este, t2.norte]);
      });
      let polygon = this.mapApi.createPolygon(rings);
      let area = this.mapApi.calculateArea(polygon, 'hectares');
      this.listAnexoArea.push({ anexo: t.key, area: Math.abs(parseFloat(area.toFixed(2))) })
    });
    let suma = 0;
    for (let j = 0; j < this.listAnexoArea.length; j++) {
      suma += parseFloat(this.listAnexoArea[j].area)
    }
    this.totalAnexoArea = suma;
  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje +=
        "(*) Agregue archivo VERTICES PARCELAS DE CORTA ANEXO \n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  eliminarLista(event: any) {
    if (this.listCoordinatesAnexo.length > 0) {
      let msg = "¿Está seguro de eliminar los registros?."
      if (this.idArchivoS != 0) {
        msg += "\nSe eliminará archivo shapefile de vértices de área de la UMF."
      }
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: msg,
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          if (this._idInfBasica != null && this._idInfBasica != undefined) {
            let params = {
              idInfBasica: this._idInfBasica,
              idInfBasicaDet: 0,
              codInfBasicaDet: '',
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaPmfiService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  this.toast.ok('Se eliminó superficie y ubicación de la UMF correctamente.');
                } else {
                  this.toast.warn(result.message);
                }
                this.eliminarArchivoDetalle(this.idArchivoS);
                this.listCoordinatesAnexo = [];
                this.listAnexoArea = [];
                this._idInfBasica = 0;
                this.listarCoordenadas();
              });
          } else {
            this.listCoordinatesAnexo = [];
            this.listAnexoArea = [];
            this._idInfBasica = 0;
            this.listarCoordenadas();
          }
        },
        reject: () => { },
      });
    }
  }

  listarGeometria() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: CodigoPOPAC.CODIGO_PROCESO,
      codigoSubSeccion: "POPACINFBATIM",
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe(
        (result: any) => {
          if (result.data.length > 0) {
            result.data.forEach((t: any) => {
              if (t.geometry_wkt !== null) {
                if (!this.listArchivo.includes(t.idArchivo)) {
                  if (t.descripcion == "COD3" && t.tipoGeometria == "TPPUNTO") {
                    this.idArchivoS = t.idArchivo;
                    this.listArchivo.push(t.idArchivo);
                  }
                }
              }
            });
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error");
        }
      );
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    if (idArchivo != 0) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.servicePlanManejoGeometria
        .eliminarPlanManejoGeometriaArchivo(idArchivo, this.user.idUsuario)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.toast.ok("Se eliminó el archivo y/o geometría correctamente.");
              this.eliminaS.emit(true);
            } else {
              this.toast.error("Ocurrió un problema, intente nuevamente");
            }
          },
          (error) => {
            this.toast.error("Ocurrió un problema, intente nuevamente");
          }
        );
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ severity: "success", summary: "", detail: mensaje });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({ severity: "error", summary: "ERROR", detail: mensaje });
  }
}
