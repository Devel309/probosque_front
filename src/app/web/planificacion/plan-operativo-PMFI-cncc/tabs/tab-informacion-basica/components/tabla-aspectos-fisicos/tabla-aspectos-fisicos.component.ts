import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { InformacionBasicaDetalleModelPMFIC, InformacionBasicaModelPMFIC } from 'src/app/model/InformacionSocieconomicaPMFIC';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';

@Component({
  selector: 'tabla-aspectos-fisicos',
  templateUrl: './tabla-aspectos-fisicos.component.html',
})
export class TablaAspectosFisicosComponent implements OnInit {
  @Input() idPGMF!: number;

  codigoAcordeon2: string = CodigoPMFIC.TAB_4_2;

  totalSumaUF!: string;
  totalPorcentajeUF!: string;

  @Input() listInfoBasicaDet_F: InformacionBasicaDetalleModelPMFIC[] = [];
  @Input() set totalValues(data:InformacionBasicaDetalleModelPMFIC[]){    
    if(data.length>0){
     this.calculateTotalFisiografia();
    }
  }

 


  infoBasica4_2: InformacionBasicaModelPMFIC = new InformacionBasicaModelPMFIC({
    idInfBasica: 0,
    idPlanManejo: this.idPGMF,
    codInfBasica: CodigoPMFIC.CODIGO_PROCESO,
    codSubInfBasica: CodigoPMFIC.TAB_4,
    codNombreInfBasica: CodigoPMFIC.TAB_4_2,
    idUsuarioRegistro: this.user.idUsuario,
  });


  constructor(
    private dialog: MatDialog,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService
  ) {}

  ngOnInit() {
    // this. listarAspectosFisicos4_2();
  }

  listarAspectosFisicos4_2() {
    this.listInfoBasicaDet_F = [];
    let params: any = {
      idInfBasica: CodigoPMFIC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: this.codigoAcordeon2,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            this.infoBasica4_2 = response.data[0];
            response.data.forEach((item: any) => {
              this.listInfoBasicaDet_F.push(item);
            });
            this.calculateTotalFisiografia();
          }
        }
      });
  }

  calculateTotalFisiografia() {
    let sum1 = 0;
    this.listInfoBasicaDet_F.forEach((item) => {
      sum1 += parseFloat(item.areaHa);
      item.areaHaPorcentaje = Number(((100 * parseFloat(item.areaHa)) / sum1).toFixed(2));
    });

    this.totalSumaUF = `${sum1.toFixed(2)}`;
    this.totalPorcentajeUF = this.listInfoBasicaDet_F.length === 0 ? `${0}` : `${(100).toFixed(2)}`;
  }
}
