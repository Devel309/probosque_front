import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import Polygon from '@arcgis/core/geometry/Polygon';
import { ArchivoService, UsuarioService } from '@services';
import { DownloadFile, EsriGeometryType, isNullOrEmpty, MapApi, ToastService, UnitMetric } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin, from, Observable } from 'rxjs';
import { concatMap, finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { InformacionBasicaPGMFIC } from 'src/app/model/InfirmacionBasicaModel';
import { InformacionBasicaDetalleModelPMFIC } from 'src/app/model/InformacionSocieconomicaPMFIC';
import { ManejoBosqueDetalleModel } from 'src/app/model/ManejoBosque';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { CodigoPOPAC } from 'src/app/model/util/POPAC/POPAC';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { ManejoBosqueService } from 'src/app/service/manejoBosque.service';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { ModalRegistrarComponent } from '../../modal-registrar/modal-registrar.component';
import { TabInformacionBasicaComponent } from '../../tab-informacion-basica.component';

@Component({
  selector: 'app-tabla-informativa-mapa',
  templateUrl: './tabla-informativa-mapa.component.html',
  styleUrls: ['./tabla-informativa-mapa.component.scss']
})
export class TablaInformativaMapaComponent implements OnInit {

  constructor(
    private confirmationService: ConfirmationService,
    private user: UsuarioService,
    private mapApi: MapApi,
    private dialog: MatDialog,
    private toast: ToastService,
    private componenteInfBasica: TabInformacionBasicaComponent,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private serviceArchivo: ArchivoService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private servicioGeoforestal: ApiGeoforestalService,
    public dialogService: DialogService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private manejoBosqueService: ManejoBosqueService,
    private serviceExternos: ApiForestalService
  ) { }

  @Input() disabled!: boolean;
  @Input() idInfBasica: number = 0;
  @Input() idPGMF!: number;
  @Input() idPlanManejoPadre!: number;
  @Input() usuario = {} as UsuarioModel;
  @Input() codigoProceso: string = "POPAC";
  @Input() codigoSubSeccion: string = "POPACINFBATIM";
  @Input() codigoProcesoPadre: string = "PMFIC";
  @Input() codigoSubSeccionPadre: string = "PMFICINFBATIM";
  @Input() tipoGeometria: string = "TPTBLINFO";
  @Input() idInfBasicaU: number = 0;
  @Input() idInfBasicaS: number = 0;
  @Output() listUbicacion = new EventEmitter<any>();
  @Output() listSuperficie = new EventEmitter<any>();
  @Output() listArea = new EventEmitter<any>();
  @Output() totalArea = new EventEmitter<any>();
  @Output() listAccesibilidad = new EventEmitter<any>();
  @Output() totalAreaZonitificacion = new EventEmitter<number>();

  planificacion: any = [];
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  _filesSHP: any = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  ref!: DynamicDialogRef;

  idManejoBosqueZ: number = 0;
  listaZonificacionUMF: any = [];

  idInfBasicaH: number = 0;
  listaHidrografiaUMF: any = [];
  listaHidrografiaUMF2: any = [];

  idInfBasicaF: number = 0;
  listaFisiografiaUMF: any = [];
  isNewF: boolean = true;
  codeF: string = "";
  codigoAcordeonF: string = CodigoPOPAC.TAB_4_2;

  idInfBasicaTB: number = 0;
  listTipoBosque: any = [];
  isNewTB: boolean = false;
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  code: string = "";

  listArchivo: any[] = [];

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.listarTablaInformativa();
    this.listarTiposBosque();
    this.listarFisiografia();
  }

  listarTablaInformativa() {
    let param = {
      idInfBasica: this.codigoProcesoPadre,
      idPlanManejo: this.idPGMF,
      codCabecera: this.codigoSubSeccionPadre
    }
    this.informacionAreaPmfiService
      .listarInformacionBasica(param)
      .subscribe((result: any) => {
        let newList: any = [];
        result.data.forEach((t: any, i: number) => {
          newList.push({
            idInfBasica: t.idInfBasica,
            idInfBasicaDet: t.idInfBasicaDet,
            codInfBasicaDet: t.codInfBasicaDet.replace(this.codigoProcesoPadre, this.codigoProceso),
            label: t.actividad,
            value: 'NO',
            mapa: t.descripcion,
            codigo: `COD${i + 1}`,
            codigotim: t.justificacion.replace(this.codigoProcesoPadre, this.codigoProceso),
            area: 0,
            archivoArea: null,
            inServerArea: null,
            idLayerArea: null,
            idGroupLayerArea: null,
            isNewArea: false,
            tipoArea: null,
            validate: false,
            archivoVertice: null,
            inServerVertice: null,
            idLayerVertice: null,
            idGroupLayerVertice: null,
            isNewVertice: false,
            tipoVertice: null,
          });
        });

        let items = newList.groupBy((t: any) => t.mapa)
        this.planificacion = items;
        this.obtenerCapas();
      });
  }

  obtenerCapas() {
    this.cleanLayers();
    let item = {
      idPlanManejo: this.idPGMF,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe((result) => {
        if (result.data.length > 0) {
          //COD1
          const filtered1 = result.data.filter((t: any) => {
            return t.descripcion === "COD1" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered1, "TPAREA");
          const filteredPunto1 = result.data.filter((t: any) => {
            return t.descripcion === "COD1" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto1, "TPPUNTO");
          //COD2
          const filtered2 = result.data.filter((t: any) => {
            return t.descripcion === "COD2" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered2, "TPAREA");
          const filteredPunto2 = result.data.filter((t: any) => {
            return t.descripcion === "COD2" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto2, "TPPUNTO");
          //COD3
          const filtered3 = result.data.filter((t: any) => {
            return t.descripcion === "COD3" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered3, "TPAREA");
          const filteredPunto3 = result.data.filter((t: any) => {
            return t.descripcion === "COD3" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto3, "TPPUNTO");
          //COD4
          const filtered4 = result.data.filter((t: any) => {
            return t.descripcion === "COD4" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered4, "TPAREA");
          const filteredPunto4 = result.data.filter((t: any) => {
            return t.descripcion === "COD4" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto4, "TPPUNTO");
          //COD5
          const filtered5 = result.data.filter((t: any) => {
            return t.descripcion === "COD5" && t.tipoGeometria === "TPRUTA";
          });
          this.createFeature(filtered5, "TPRUTA");
          const filteredPunto5 = result.data.filter((t: any) => {
            return t.descripcion === "COD5" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto5, "TPPUNTO");
          //COD6
          const filtered6 = result.data.filter((t: any) => {
            return t.descripcion === "COD6" && t.tipoGeometria === "TPHIDRO";
          });
          this.createFeature(filtered6, "TPHIDRO");
          const filteredPunto6 = result.data.filter((t: any) => {
            return t.descripcion === "COD6" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto6, "TPPUNTO");
          //COD7
          const filtered7 = result.data.filter((t: any) => {
            return t.descripcion === "COD7" && t.tipoGeometria === "TPFISIO";
          });
          this.createFeature(filtered7, "TPFISIO");
          const filteredPunto7 = result.data.filter((t: any) => {
            return t.descripcion === "COD7" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto7, "TPPUNTO");
          //COD8
          const filtered8 = result.data.filter((t: any) => {
            return t.descripcion === "COD8" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered8, "TPAREA");
          const filteredPunto8 = result.data.filter((t: any) => {
            return t.descripcion === "COD8" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto8, "TPPUNTO");
          //COD9
          const filtered9 = result.data.filter((t: any) => {
            return t.descripcion === "COD9" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered9, "TPAREA");
          const filteredPunto9 = result.data.filter((t: any) => {
            return t.descripcion === "COD9" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto9, "TPPUNTO");
          //COD10
          const filtered10 = result.data.filter((t: any) => {
            return t.descripcion === "COD10" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered10, "TPAREA");
          const filteredPunto10 = result.data.filter((t: any) => {
            return t.descripcion === "COD10" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto10, "TPPUNTO");
          //COD11
          const filtered11 = result.data.filter((t: any) => {
            return t.descripcion === "COD11" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered11, "TPAREA");
          const filteredPunto11 = result.data.filter((t: any) => {
            return t.descripcion === "COD11" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto11, "TPPUNTO");
          //COD12
          const filtered12 = result.data.filter((t: any) => {
            return t.descripcion === "COD12" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered12, "TPAREA");
          const filteredPunto12 = result.data.filter((t: any) => {
            return t.descripcion === "COD12" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto12, "TPPUNTO");
          //COD13
          const filtered13 = result.data.filter((t: any) => {
            return t.descripcion === "COD13" && t.tipoGeometria === "TPAREA";
          });
          this.createFeature(filtered13, "TPAREA");
          const filteredPunto13 = result.data.filter((t: any) => {
            return t.descripcion === "COD13" && t.tipoGeometria === "TPPUNTO";
          });
          this.createFeature(filteredPunto13, "TPPUNTO");
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.obtenerCapasPadre()
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe();
        }
      },
      (error) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }

  cleanLayers() {
    let layers = this.mapApi.getLayers(
      this.componenteInfBasica._id,
      this.componenteInfBasica.viewTablaInformativa
    );
    layers.forEach((t: any) => {
      this.componenteInfBasica.viewTablaInformativa.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    //this._layers = [];
  }

  createFeature(filtered: any, tipo: any) {
    if (filtered.length) {
      let idLayer = this.mapApi.Guid2.newGuid;
      let groupId = this.componenteInfBasica._id;
      let features1: any = [];
      let element = filtered[0];
      filtered.forEach((t: any) => {
        if (t.geometry_wkt !== null) {
          let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
          let area = 0;
          if (jsonGeometry.type === "Polygon") {
            let geometry: any = null;
            geometry = { spatialReference: { wkid: 4326 } };
            geometry.rings = jsonGeometry.coordinates;
            area = this.mapApi.calculateArea(geometry, "hectares");
            features1.push({
              type: "Feature",
              properties: {
                title: t.nombreCapa,
                area: area.toFixed(2),
                typeGeometry: jsonGeometry.type,
              },
              geometry: jsonGeometry,
            });
          } else {
            features1.push({
              type: "Feature",
              properties: JSON.parse(t.propiedad),
              geometry: jsonGeometry,
            });
          }
        }
      });
      this.planificacion.forEach((item: any) => {
        item.value.forEach((t2: any) => {
          if (t2.codigo === filtered[0].descripcion) {
            t2.value =
              element.geometry_wkt === null ||
              element.geometry_wkt === undefined
                ? "NO"
                : "SI";
            if (
              tipo === "TPAREA" ||
              tipo === "TPRUTA" ||
              tipo === "TPHIDRO" ||
              tipo === "TPFISIO"
            ) {
              t2.idLayerArea = idLayer;
              t2.inServerArea = true;
              t2.idArchivoArea = element.idArchivo;
              t2.isNewArea = false;
            
              this._layers = [];
              if (filtered[0].detalle) {
                this._layers = JSON.parse(filtered[0].detalle);
                this._layers.forEach((l: any) => {
                  l.service = true;
                  l.view = false;
                })
              }
              t2.layers = this._layers;
              t2.validate = filtered[0].conforme ? filtered[0].conforme : t2.validate;
              this._layers = [];
            } else if (tipo === "TPPUNTO") {
              t2.idLayerVertice = idLayer;
              t2.inServerVertice = true;
              t2.idArchivoVertice = element.idArchivo;
              t2.isNewVertice = false;
            }

            let item = {
              color: element.colorCapa,
              title: element.nombreCapa,
              features: features1,
            };
            let geoJson = this.mapApi.getGeoJson2(idLayer, groupId, item);
            this.createLayer(geoJson);
          }
        });
      });
    }
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      if (t.properties !== null) {
        t.properties.OBJECTID = i + 1;
      }
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this.componenteInfBasica._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.tipo = item.tipo;
    geoJsonLayer.codigotim = item.codigotim;
    geoJsonLayer.codigoItem = item.codigoItem;
    geoJsonLayer.validate = item.validate;
    this.componenteInfBasica.viewTablaInformativa.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.componenteInfBasica.viewTablaInformativa
        );
        this.componenteInfBasica.viewTablaInformativa.goTo({
          target: data.fullExtent,
        });
      })
      .catch((error: any) => {});
  }

  setProperties(item: any) {
    let popupTemplate: any = {
      title: 'Tabla Informativa',
      content: [{
        type: "fields",
        fieldInfos: [
          {
            fieldName: "title",
            label: "Nombre capa",
          },
          {
            fieldName: "area",
            label: "Área",
          }
          ,
          {
            fieldName: "typeGeometry",
            label: "Tipo de Geometría",
          },
        ],
      }]
    };
    return popupTemplate;
  }

  obtenerCapasPadre() {
    this.cleanLayers();
    let item = {
      idPlanManejo: this.idPlanManejoPadre,
      codigoSeccion: this.codigoProcesoPadre,
      codigoSubSeccion: this.codigoSubSeccionPadre,
    };
    return this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .pipe(tap((result) => {
        if (result.data.length > 0) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              if (!this.listArchivo.includes(t.idArchivo)) {
                this.listArchivo.push(t.idArchivo);
                this.crearFile(t);
              }
            }
          });
        }
      },
        (error) => {
          this.toast.error('Ocurrió un error');
        }
      ));
  }

  crearFile(item: any) {
    this.serviceArchivo
      .obtenerArchivo(item.idArchivo)
      .subscribe((result: any) => {
        if (result.data !== null && result.data !== undefined) {
          let config = {
            idGroupLayer: this.mapApi.Guid2.newGuid,
            idLayer: this.mapApi.Guid2.newGuid,
            inServer: false,
            service: false,
            validate: false,
            codigo: item.descripcion,
            tipo: item.tipoGeometria,
            codigoProceso: this.codigoProceso,
            codigoSubSeccion: this.codigoSubSeccion,
          };

          const binary_string = window.atob(result.data.file);
          const len = binary_string.length;
          const bytes = new Uint8Array(len);
          for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
          }
          const file: File = new File([bytes.buffer], result.data.nombre, { type: 'application/x-zip-compressed' });
          
          this.processFile(file, config, true);

          this.planificacion.forEach((p: any) => {
            p.value.forEach((t2: any) => {
              if (t2.codigo === item.descripcion) {
                t2.validate = item.conforme ? item.conforme : false;

                this._layers = [];
                if (item.detalle) {
                  this._layers = JSON.parse(item.detalle);
                  this._layers.forEach((l: any) => {
                    l.service = true;
                    l.view = false;
                  })
                }
                t2.layers = this._layers;
                let det: any = {};
                let obj: any = [];
                this._layers.forEach((l: any) => {
                  det = {};
                  det.color =  l.color;
                  det.nombre = l.nombre;
                  det.overlap = l.overlap;
                  // det.service = l.service;
                  obj.push(det);
                })
                t2.detalle = obj;
                t2.isPadre = true;
                this._layers = [];
              }
            });
          });
        }
      },
      (error: HttpErrorResponse) => {
        this.toast.error(error.message);
      }
    );
  }

  cargarArchivo(e: any) {
    let valida: boolean = false;
    if (e.target.dataset.tipo === "TPAREA") {
      if (e.target.dataset.validate === "true") {
        valida = true;
      }
    }
    if (
      e.target.dataset.codigo === "COD5" &&
      e.target.dataset.tipo === "TPAREA"
    ) {
      e.target.dataset.tipo = "TPRUTA";
    } else if (
      e.target.dataset.codigo === "COD6" &&
      e.target.dataset.tipo === "TPAREA"
    ) {
      e.target.dataset.tipo = "TPHIDRO";
    } else if (
      e.target.dataset.codigo === "COD7" &&
      e.target.dataset.tipo === "TPAREA"
    ) {
      e.target.dataset.tipo = "TPFISIO";
    }

    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      idLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: valida,
      codigo: e.target.dataset.codigo,
      tipo: e.target.dataset.tipo,
      codigotim: e.target.dataset.codigotim,
      codigoProceso: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config, false);
          }
          i++;
        }
      }
      e.target.value = "";
    }
  }

  processFile(file: any, config: any, isPadre: boolean) {
    this.codeF = '';
    config.file = file;
    if (!isPadre) {
      this.dialog.open(LoadingComponent, { disableClose: true });
    }
    this.mapApi.processFileSHP(file).then((data: any) => {
      if (!isPadre) {
        this.dialog.closeAll();
      }
      this.createLayers(data, config);
      //Componente Catastro
      if (config.validate === true) {
        this.dialog.open(LoadingComponent, { disableClose: true });
        forkJoin([
          this.validateOverlap(data[0].features[0].geometry.coordinates),
          this.obtenerTiposBosques(data[0].features[0].geometry.coordinates)
        ]).pipe(finalize(() => {
          this.valoresCatastro(config.codigo);
          this.dialog.closeAll();
        })).subscribe();
      }
      //
      if (!isPadre) {
        if (config.codigo === "COD1" && config.tipo === "TPPUNTO") {
          this.createTablaUbicacion(data[0].features);
        } else if (config.codigo === "COD3" && config.tipo === "TPPUNTO") {
          this.createTablaSuperficie(data[0].features);
        } else if (config.codigo === "COD4" && config.tipo === "TPAREA") {
          this.createTablaZonificacion(data[0].features);
        } else if (config.codigo === "COD5" && config.tipo === "TPRUTA") {
          this.createTablaAccesibilidad(data[0].features);
        } else if (config.codigo === "COD6" && config.tipo === "TPHIDRO") {
          this.createTablaHidrografia(data[0].features);
        } else if (config.codigo === "COD7" && config.tipo === "TPFISIO") {
          this.codeF = "COD7";
          this.createTablaFisiografia(data[0].features);
        } else if (config.codigo === "COD9" && config.tipo === "TPAREA") {
          this.code = "COD9";
          this.createTablaTipoBosque(data[0]);
        }
      }
    });
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = config.idLayer;
      t.groupId = this.componenteInfBasica._id;
      t.idGroupLayer = config.idGroupLayer;
      t.descripcion = config.codigoProceso;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.tipo = config.tipo;
      t.codigotim = config.codigotim;
      t.codigoItem = config.codigo;
      t.validate = config.validate;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.codigoProceso;
      //this._layers.push(layer);
      this.createLayer(t);
      this.calcularArea(t, config);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.codigoProceso;
    file.tipoArchivo = config.tipo;
    file.color = config.codigo;
    this._filesSHP.push(file);
  }

  calcularArea(data: any, config: any) {
    let sumArea: any = 0;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      if (t.geometry.type === "Polygon") {
        geometry = { spatialReference: { wkid: 4326 } };
        geometry.rings = t.geometry.coordinates;
        let area = this.mapApi.calculateArea(geometry, "hectares");
        sumArea += area;
      }
    });

    if (config.codigo == "COD4") {
      this.toast.ok(
        "Cargó un shape file para Zonificación de la UMF, se setea el área total en el acordeón 3.3"
      );
      this.totalAreaZonitificacion.emit(Number(sumArea.toFixed(3)));
    }

    this.planificacion.forEach((item: any) => {
      item.value.forEach((t: any) => {
        if (
          t.codigo === config.codigo &&
          (config.tipo === "TPAREA" ||
            config.tipo === "TPRUTA" ||
            config.tipo === "TPHIDRO" ||
            config.tipo === "TPFISIO")
        ) {
          t.area = Number(sumArea.toFixed(2));
          t.archivoArea = config.file;
          t.inServerArea = config.inServer;
          t.idLayerArea = config.idLayer;
          t.idGroupLayerArea = config.idGroupLayer;
          t.tipoArea = config.tipo;
          t.isNewArea = true;
          t.value = "SI";
        } else if (t.codigo === config.codigo && config.tipo === "TPPUNTO") {
          t.area = Number(sumArea.toFixed(2));
          t.archivoVertice = config.file;
          t.inServerVertice = config.inServer;
          t.idLayerVertice = config.idLayer;
          t.idGroupLayerVertice = config.idGroupLayer;
          t.tipoVertice = config.tipo;
          t.isNewVertice = true;
          t.value = "SI";
        }
      });
    });
  }

  validateOverlap(coordinates: any): Observable<any> {
    let item = {
      geometria: {
        poligono: coordinates,
      },
    };

    return this.serviceExternos
      .validarSuperposicionAprovechamiento(item)
      .pipe(tap((result: any) => {
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this.componenteInfBasica._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.view = true;
                this._layer = {} as CustomCapaModel;
                this._layer.codigo = 0;
                this._layer.idLayer = t.geoJson.idLayer;
                this._layer.inServer = true;
                this._layer.nombre = t.geoJson.title;
                this._layer.groupId = t.geoJson.groupId;
                this._layer.color = t.geoJson.color;
                this._layer.overlap = t.geoJson.overlap;
                this._layer.service = t.geoJson.service;
                this._layer.view = t.geoJson.view;
                this._layers.push(this._layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this.componenteInfBasica._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                t.geoJson.view = true;
                let _layer = {} as CustomCapaModel;
                this._layer.codigo = 0;
                this._layer.idLayer = t.geoJson.idLayer;
                this._layer.inServer = true;
                this._layer.nombre = t.geoJson.title;
                this._layer.groupId = t.geoJson.groupId;
                this._layer.color = t.geoJson.color;
                this._layer.overlap = t.geoJson.overlap;
                this._layer.service = t.geoJson.service;
                this._layer.view = t.geoJson.view;
                this._layers.push(this._layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
      }));
  }

  obtenerTiposBosques(coordinates: any): Observable<any> {
    let parametros = {
      idClasificacion: "",
      geometria: {
        poligono: coordinates,
      },
    };
    return this.serviceExternos
      .identificarTipoBosque(parametros)
      .pipe(tap((result) => {
        if (result.dataService.data.capas.length > 0) {
          result.dataService.data.capas.forEach((t: any) => {
            if (t.geoJson !== null) {
              t.geoJson.crs.type = "name";
              t.geoJson.opacity = 0.4;
              t.geoJson.groupId = this.componenteInfBasica._id;
              t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
              t.geoJson.color = this.mapApi.random();
              t.geoJson.title = t.nombreCapa;
              t.geoJson.service = true;
              t.geoJson.view = true;
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = 0;
              this._layer.idLayer = t.geoJson.idLayer;
              this._layer.nombre = t.geoJson.title;
              this._layer.groupId = t.geoJson.groupId;
              this._layer.color = t.geoJson.color;
              this._layer.service = t.geoJson.service;
              this._layer.view = t.geoJson.view;
              this._layers.push(this._layer);
              this.createLayer(t.geoJson);
            }
          });
        }
      }));
  }

  checkCatastro(e: any, cod: any) {
    this.planificacion.forEach((item: any) => {
      item.value.forEach((t: any) => {
        if (t.codigo == cod) {
          let val: boolean = e.target.checked;
          t.validate = val;
        }
      });
    });
  }

  valoresCatastro(cod: any) {
    this.planificacion.forEach((item: any) => {
      item.value.forEach((t: any) => {
        if (t.codigo == cod) {
          t.layers = this._layers;
          let det: any = {};
          let obj: any = [];
          this._layers.forEach((l: any) => {
            det = {};
            det.color =  l.color;
            det.nombre = l.nombre;
            det.overlap = l.overlap;
            // det.service = l.service;
            obj.push(det);
          })
          t.detalle = obj;
        }
      });
    });
    this._layers = [];
  }

  removeCatastroLayers(cod: any, idArchivo: number, isPadre: boolean) {
    this.planificacion.forEach((item: any) => {
      item.value.forEach((t: any) => {
        if (t.codigo == cod) {
          if (t.layers && t.layers.length > 0) {
            if (idArchivo == 0 && !isPadre) {
              t.layers.forEach((l: any) => {
                this.mapApi.removeLayer(l.idLayer, this.componenteInfBasica.viewTablaInformativa);
              });
            } else if (isNullOrEmpty(idArchivo) && !isPadre) {
              t.layers.forEach((l: any) => {
                this.mapApi.removeLayer(l.idLayer, this.componenteInfBasica.viewTablaInformativa);
              });
            }
            t.layers = [];
          }
          t.validate = false;
        }
      });
    });
  }

  registrar(cod: any, tipo: any) {
    this.guardarArchivo(cod, tipo);
    //this.registrarTablaInformativa();
  }

  guardarArchivo(cod: any, tipo: any) {
    let fileUpload: any = [];

    this.planificacion.forEach((item: any) => {
      item.value.forEach((t: any) => {
        if (t.codigo === cod) {
          if (cod === "COD5" && tipo === "TPAREA") {
            tipo = "TPRUTA";
          } else if (cod === "COD6" && tipo === "TPAREA") {
            tipo = "TPHIDRO";
          } else if (cod === "COD7" && tipo === "TPAREA") {
            tipo = "TPFISIO";
          }

          if (t.tipoArea === tipo) {
            if (!t.inServerArea) {
              if (t.archivoArea !== null && t.archivoArea !== undefined) {
                let item: any = {
                  idUsuario: this.user.idUsuario,
                  codigo: "37",
                  codigoTipoPGMF: this.codigoProceso,
                  file: t.archivoArea,
                  idGroupLayer: t.idGroupLayerArea,
                  descripcion: t.codigo,
                  tipo: t.tipoArea,
                  codigotim: t.codigotim,
                  codigoItem: t.codigo,
                  idLayer: t.idLayerArea,
                  conforme: t.validate,
                  detalle: t.detalle,
                  isPadre: t.isPadre ? t.isPadre : false,
                };
                fileUpload.push(item);
              }
            }
          } else if (t.tipoVertice === tipo) {
            if (!t.inServerVertice) {
              if (t.archivoVertice !== null && t.archivoVertice !== undefined) {
                let item: any = {
                  idUsuario: this.user.idUsuario,
                  codigo: "37",
                  codigoTipoPGMF: this.codigoProceso,
                  file: t.archivoVertice,
                  idGroupLayer: t.idGroupLayerVertice,
                  descripcion: t.codigo,
                  tipo: t.tipoVertice,
                  codigotim: t.codigotim,
                  codigoItem: t.codigo,
                  idLayer: t.idLayerVertice,
                };
                fileUpload.push(item);
              }
            }
          }
        }
      });
    });

    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.toast.error("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.mapApi.removeLayer(fileUpload[0].idLayer, this.componenteInfBasica.viewTablaInformativa);
          if (tipo == "TPAREA" || tipo == "TPFISIO") {
            this.removeCatastroLayers(fileUpload[0].codigoItem, 0, fileUpload[0].isPadre);
          }
          this.obtenerCapas2(fileUpload[0].codigoItem, fileUpload[0].tipo);
          if (cod == "COD4" && tipo == "TPAREA") {
            this.registrarSistemaManejo(this.listaZonificacionUMF);
          }
          if (cod == "COD6" && tipo == "TPHIDRO") {
            this.registrarHidrografia(this.listaHidrografiaUMF);
          }
          if (cod == "COD7" && tipo == "TPFISIO") {
            this.isNewF= false
            this.registrarFisiografia(this.listaFisiografiaUMF);
          }
          if (cod == "COD9" && tipo == "TPAREA") {
            this.registrarTiposBosque(this.listTipoBosque);
          }
        })
      )
      .subscribe(
        (result: any) => {
          // this.toast.ok(result?.message);
          if (result.success) {
            this.toast.ok("El archivo se registró correctamente.");
          } else {
            this.toast.warn(result.message);
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error.");
        }
      );
  }

  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }

  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: this.codigoProceso,
      codigoSubTipoPGMF: this.codigoSubSeccion,
      idArchivo: result.data,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      descripcion: item.descripcion,
      observacion: item.codigotim,
      asunto: item.tipo,
      conforme: item.conforme,
      detalle: JSON.stringify(item.detalle),
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }

  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.componenteInfBasica.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      t3.attributes.forEach((feature: any) => {
        let geometryWKT: any = this.mapApi.getFeature(
          feature,
          this.componenteInfBasica.view.spatialReference.wkid
        );
        let item: any = {
          idPlanManejoGeometria: 0,
          idPlanManejo: this.idPGMF,
          idArchivo: idArchivo,
          tipoGeometria: t3.tipo,
          codigoGeometria: feature.geometry.type,
          codigoSeccion: this.codigoProceso,
          codigoSubSeccion: this.codigoSubSeccion,
          geometry_wkt: geometryWKT,
          srid: 4326,
          nombreCapa: `${t3.title}`,
          propiedad: JSON.stringify(feature.properties),
          colorCapa: t3.color,
          idUsuarioRegistro: this.user.idUsuario,
        };
        this.planManejoGeometria.push(item);
      });
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }

  obtenerCapas2(codigo: any, tipo: any) {
    let item = {
      idPlanManejo: this.idPGMF,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result: any) => {
        if (result.data.length) {
          if (tipo === "TPAREA" || tipo === "TPRUTA" || tipo === "TPHIDRO" || tipo === "TPFISIO") {
            const filtered = result.data.filter((t: any) => {
              return t.descripcion === codigo && t.tipoGeometria === tipo;
            });
            this.createFeature(filtered, "TPAREA");
          }
          if (tipo === "TPPUNTO") {
            const filteredPunto = result.data.filter((t: any) => {
              return t.descripcion === codigo && t.tipoGeometria === tipo;
            });
            this.createFeature(filteredPunto, "TPPUNTO");
          }
        }
      },
      (error) => {
        this.toast.error("Ocurrió un error");
      }
    );
  }

  descargarArchivo(data: any, tipoGeometria: any) {
    let idArchivo: any;
    this.planificacion.forEach((item: any) => {
      item.value.forEach((t: any) => {
        if (t.codigo === data.codigo) {
          if (tipoGeometria === "TPAREA") {
            idArchivo = t.idArchivoArea;
          } else if (tipoGeometria === "TPPUNTO") {
            idArchivo = t.idArchivoVertice;
          }
        }
      });
    });
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(idArchivo)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.file,
            result.data.nombre,
            "application/octet-stream"
          );
        }
      },
        (error: HttpErrorResponse) => {
          this.toast.error(error.message);
          this.dialog.closeAll();
        }
      );
  }

  eliminarArchivo(data: any, tipoGeometria: any) {
    let msg = "¿Está seguro de eliminar este archivo?\n"
    // if (data.codigo == "COD1" && tipoGeometria === "TPPUNTO" && this.idInfBasicaU != 0) {
    if (data.codigo == "COD1" && tipoGeometria === "TPPUNTO") {
      msg += "\nSe eliminará información de acordeón 3.1."
    // } else if (data.codigo == "COD3" && tipoGeometria === "TPPUNTO" && this.idInfBasicaS != 0) {
    } else if (data.codigo == "COD3" && tipoGeometria === "TPPUNTO") {
      msg += "\nSe eliminará información de acordeón 3.2."
    // } else if (data.codigo == "COD6" && tipoGeometria === "TPAREA" && this.idInfBasicaH != 0) {
    } else if (data.codigo == "COD6" && tipoGeometria === "TPAREA") {
      msg += "\nSe eliminará información de Hidrografía."
    // } else if (data.codigo == "COD7" && tipoGeometria === "TPAREA" && this.idInfBasicaF != 0) {
    } else if (data.codigo == "COD7" && tipoGeometria === "TPAREA") {
      msg += "\nSe eliminará información de Fisiografía."
    // } else if (data.codigo == "COD9" && tipoGeometria === "TPAREA" && this.idInfBasicaTB != 0) {
    } else if (data.codigo == "COD9" && tipoGeometria === "TPAREA") {
      msg += "\nSe eliminará información de Tipos de Bosque."
    }
    this.confirmationService.confirm({
      message: msg,
      icon: "pi pi-exclamation-triangle",
      key: "deleteFileTIM",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        this.planificacion.forEach((item: any) => {
          item.value.forEach((t: any) => {
            if (t.codigo === data.codigo) {
              if (tipoGeometria === "TPAREA") {
                this.removeCatastroLayers(t.codigo, data.idArchivoArea, t.isPadre);
                t.archivo = null;
                t.area = 0;
                t.value = t.idArchivoVertice == null ? "NO" : t.value;
                if (t.inServerArea === true) {
                  t.inServerArea = false;
                  this.eliminarArchivoDetalle(data.idArchivoArea);
                }
                this.mapApi.removeLayer(data.idLayerArea, this.componenteInfBasica.viewTablaInformativa);
                t.idLayerArea = null;
                t.idArchivoArea = null;
                t.isNewArea = false;
              } else if (tipoGeometria === "TPPUNTO") {
                t.archivo = null;
                t.area = 0;
                t.value = t.idArchivoArea == null ? "NO" : t.value;
                if (t.inServerVertice === true) {
                  t.inServerVertice = false;
                  this.eliminarArchivoDetalle(data.idArchivoVertice);
                }
                this.mapApi.removeLayer(data.idLayerVertice, this.componenteInfBasica.viewTablaInformativa);
                t.idLayerVertice = null;
                t.idArchivoVertice = null;
                t.isNewVertice = false;
              }
              t.isPadre = false;              
              if (t.codigo == "COD1" && tipoGeometria === "TPPUNTO") {
                this.eliminarTablaUbicacion();
              } else if (t.codigo == "COD3" && tipoGeometria === "TPPUNTO") {
                this.eliminarTablaSuperficie();
              } else if (t.codigo == "COD6" && tipoGeometria === "TPAREA") {
                this.eliminarHidrografia();
              } else if (t.codigo == "COD7" && tipoGeometria === "TPAREA") {
                this.eliminarFisiografia();
              } else if (t.codigo == "COD9" && tipoGeometria === "TPAREA") {
                this.eliminarTiposBosque();
              }
            }
          });
        });
      },
    });
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servicePlanManejoGeometria
      .eliminarPlanManejoGeometriaArchivo(idArchivo, this.user.idUsuario)
      .subscribe(
        (response: any) => {
          this.dialog.closeAll();
          if (response.success) {
            this.toast.ok("Se eliminó el archivo y/o geometría correctamente.");
          } else {
            this.toast.error("Ocurrió un problema, intente nuevamente");
          }
        },
        (error) => {
          this.toast.error("Ocurrió un problema, intente nuevamente");
        }
      );
  }

  visualizarCapa(e: any, idLayer: any) {
    this.mapApi.toggleLayer(
      idLayer,
      e.currentTarget.checked,
      this.componenteInfBasica.viewTablaInformativa
    );
  }

  //3.1
  createTablaUbicacion(items: any) {
    let listCoordinates: any = [];
    items.forEach((t: any) => {
      listCoordinates.push({
        vertice: t.properties.vertices,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: t.properties.observacio,
      });
    });

    this.listUbicacion.emit(listCoordinates);
  }

  eliminarTablaUbicacion() {
    if (this.idInfBasicaU != 0) {
      let params = {
        idInfBasica: this.idInfBasicaU,
        idInfBasicaDet: 0,
        codInfBasicaDet: '',
        idUsuarioElimina: this.user.idUsuario,
      };
      this.informacionAreaPmfiService
        .eliminarInformacionBasica(params)
        .subscribe((result: any) => {
          if (result.success) {
            this.toast.ok("Se eliminó información de ubicación de la comunidad.");
          } else {
            this.toast.warn(result.message);
          }
          this.listUbicacion.emit([]);
          this.idInfBasicaU = 0;
        });
    } else {
      this.listUbicacion.emit([]);
    }
  }

  //3.2
  createTablaSuperficie(items: any) {
    let listCoordinates: any = [];
    let listMap = items.map((t: any) => {
      return {
        anexo: t.properties.anexo,
        vertice: t.properties.punto,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: t.properties.observacio,
      };
    });

    listCoordinates = listMap.groupBy((t: any) => t.anexo);
    this.listSuperficie.emit(listCoordinates);
    this.createTablaArea(listCoordinates);
  }

  createTablaArea(list: any) {
    let listArea: any = [];
    list.forEach((t: any) => {
      let rings: number[][] = [];
      t.value.forEach((t2: any) => {
        rings.push([t2.este, t2.norte]);
      });
      let polygon = this.mapApi.createPolygon(rings);
      let area = this.mapApi.calculateArea(polygon, "hectares");
      listArea.push({
        anexo: t.key,
        area: Math.abs(parseFloat(area.toFixed(2))),
      });
    });
    let suma = 0;
    for (let j = 0; j < listArea.length; j++) {
      suma += parseFloat(listArea[j].area);
    }

    this.listArea.emit(listArea);
    this.totalArea.emit(suma);
  }

  eliminarTablaSuperficie() {
    if (this.idInfBasicaS != 0) {
      let params = {
        idInfBasica: this.idInfBasicaS,
        idInfBasicaDet: 0,
        codInfBasicaDet: '',
        idUsuarioElimina: this.user.idUsuario,
      };
      this.informacionAreaPmfiService
        .eliminarInformacionBasica(params)
        .subscribe((result: any) => {
          if (result.success) {
            this.toast.ok("Se eliminó información de superficie y ubicación de la UMF.");
          } else {
            this.toast.warn(result.message);
          }
          this.listSuperficie.emit([]);
          this.createTablaArea([]);
          this.idInfBasicaS = 0;
        });
    } else {
      this.listSuperficie.emit([]);
      this.createTablaArea([]);
    }
  }

  //3.4
  createTablaAccesibilidad(items: any) {
    let listCoordinates: any = [];
    items.forEach((t: any) => {
      listCoordinates.push({
        ruta: t.properties.ruta,
        transporte: t.properties.transporte,
      });
    });

    this.listAccesibilidad.emit(listCoordinates);
  }

  //modal
  openModal(data: any, code: string) {
    let values: any = [];
    let values2: any = [];
    let sectionCode = ''
    if (data.codigo == "COD6") {
      // values = this.obtenerHidrografia(data.idLayerArea);
      values = this.listaHidrografiaUMF;
      values2 = this.listaHidrografiaUMF2;
      sectionCode = 'COD6'
    } else if (data.codigo == "COD7") {
      values = this.listaFisiografiaUMF;
      sectionCode = 'COD7'
    } else if (data.codigo == "COD9") {
      values = this.listTipoBosque;
      sectionCode = 'COD9'
    }
    this.ref = this.dialogService.open(ModalRegistrarComponent, {
      header: "Registar Datos",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        codigo: data.codigo,
        code: sectionCode,
        data: values,
        data2: values2,
      },
    });

    this.ref.onClose.subscribe((resp: any) => { });
  }

  //Sistema de Manejo Forestal
  createTablaZonificacion(items: any) {
    let listZonificacion: any = [];
    items.forEach((t: any) => {
      if (listZonificacion.length > 0) {
        listZonificacion.forEach((z: any) => {
          if (z.tratamientoSilvicultural !== t.properties.categoria) {
            listZonificacion.push({
              tratamientoSilvicultural: t.properties.categoria,
            });
          }
        });
      } else {
        listZonificacion.push({
          tratamientoSilvicultural: t.properties.categoria,
        });
      }
    });

    this.listaZonificacionUMF = listZonificacion;
  }

  registrarSistemaManejo(listaZ: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listaSistemaManejo: any[] = [];

    listaZ.forEach((z: any) => {
      let obj = new ManejoBosqueDetalleModel();
      obj.idManejoBosqueDet = 0;
      obj.codtipoManejoDet = CodigoPOPAC.ACORDEON_9_0;
      obj.subCodtipoManejoDet = "A";
      obj.descripcionOrd = CodigoPOPAC.CODIGO_PROCESO;
      obj.tratamientoSilvicultural = z.tratamientoSilvicultural;
      obj.idUsuarioRegistro = this.user.idUsuario;
      listaSistemaManejo.push(obj);
    });

    let params = [
      {
        idManejoBosque: this.idManejoBosqueZ ? this.idManejoBosqueZ : 0,
        codigoManejo: CodigoPOPAC.CODIGO_PROCESO,
        subCodigoManejo: CodigoPOPAC.ACORDEON_9_0,
        idPlanManejo: this.idPGMF,
        idUsuarioRegistro: this.user.idUsuario,
        listManejoBosqueDetalle: listaSistemaManejo,
      },
    ];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok('Se registró categoría de la zonificación de la UMF correctamente.');
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  //Aspectos Biológicos

  //-Tipos de Bosque-
  listarTiposBosque() {
    this.listTipoBosque = [];
    const params = {
      idInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: CodigoPOPAC.TAB_5_2,
    };

    this.informacionAreaPmfiService.listarInformacionBasica(params).subscribe(
      (result: any) => {
        if (result.data && result.data.length !=0) {
          this.idInfBasicaTB = result.data[0].idInfBasica;
          result.data.forEach((element: any) => {
            const array: any[] = [];
            let objDet = new InformacionBasicaPGMFIC(element);

            array.push(objDet);
            this.listTipoBosque.push(objDet);
            this.calculateAreaTotalBosques();
          });
        }
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  calculateAreaTotalBosques() {
    let sum1 = 0;
    this.listTipoBosque.forEach((t: any) => {
      sum1 += t.areaHa;
    });

    this.listTipoBosque.forEach((t: any) => {
      if(t.areaHaPorcentaje == 0){
        t.areaHaPorcentaje = Number(((100 * t.areaHa) / sum1).toFixed(2));
      }
    });
    this.totalAreaBosque = parseFloat(`${sum1.toFixed(2)}`);
    this.totalPorcentajeBosque = parseFloat(`${(100).toFixed(2)} %`);
  }

  createTablaTipoBosque(items: any) {
    items.features.forEach((t: any, index: any) => {
      this.listTipoBosque.push({
        vertice: index + 1,
        nombre: t.properties.CobVeg2013
          ? t.properties.CobVeg2013
          : t.properties.tipo,
        areaHa: t.properties.areaha ? t.properties.areaha: this.calculateArea(t),
        areaHaPorcentaje: t.propertiesporcentaje ? t.properties.porcentaje: 0,
      });
    });
    this.calculateAreaTotalBosques();
  }

  calculateArea(feature: any) {
    if (feature.geometry.type == EsriGeometryType.POLYGON) {
      let polygon: any = new Polygon();
      polygon = { spatialReference: { wkid: 4326 } };
      polygon.rings = feature.geometry.coordinates;
      return this.mapApi.calculateArea(polygon, UnitMetric.HA);
    }
    if (feature.geometry.type == EsriGeometryType.MULTIPOLYGON) {
      const features = feature?.geometry?.coordinates;
      let totalArea = 0;
      features.forEach((coordinates: any) => {
        let polygon: any = new Polygon();
        polygon = { spatialReference: { wkid: 4326 } };
        polygon.rings = coordinates;
        let area = this.mapApi.calculateArea(polygon, UnitMetric.HA);
        totalArea += area;
      });
      return totalArea;
    }

    return 0;
  }

  registrarTiposBosque(listaBosque: any) {
    if (this.isNewTB) {
      this.toast.warn("Debe guardar Archivo shape file.");
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      let listBosqueDet: any[] = [];
      listaBosque.forEach((t: any) => {
        let obj = new InformacionBasicaPGMFIC();
        obj.codInfBasicaDet = CodigoPOPAC.TAB_5_2;
        obj.codSubInfBasicaDet = CodigoPOPAC.TAB_5_2;
        obj.areaHa = t.areaHa;
        obj.areaHaPorcentaje = t.areaHaPorcentaje;
        obj.nombre = t.nombre;
        obj.descripcion = t.descripcion;
        obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
        listBosqueDet.push(obj);
      });

      let params = [
        {
          idInfBasica: this.idInfBasicaTB ? this.idInfBasicaTB : 0,
          codInfBasica: CodigoPOPAC.CODIGO_PROCESO,
          codSubInfBasica: CodigoPOPAC.TAB_5,
          codNombreInfBasica: CodigoPOPAC.TAB_5_2,
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: this.user.idUsuario,
          listInformacionBasicaDet: listBosqueDet,
        },
      ];

      this.informacionAreaPmfiService
        .registrarInformacionBasica(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((response: any) => {
          if (response.success) {
            // this.toast.ok(response?.message);
            this.toast.ok("Se registró tipos de bosque correctamente.")
            this.listarTiposBosque();
          } else {
            this.toast.warn(response?.message);
          }
        });
    }
  }

  eliminarTiposBosque() {
    if (this.listTipoBosque.length > 0) {
      if (this.idInfBasicaTB != 0) {
        let params = {
          idInfBasica: this.idInfBasicaTB,
          idInfBasicaDet: 0,
          codInfBasicaDet: "",
          idUsuarioElimina: this.user.idUsuario,
        };
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((result: any) => {
            if (result.success) {
              this.toast.ok("Se eliminó tipos de bosque correctamente.");
            } else {
              this.toast.warn(result.message);
            }
            this.listTipoBosque = [];
            this.idInfBasicaTB = 0;
          });
      } else {
        this.listTipoBosque = [];
      }
    }
  }

  //Aspectos Físicos

  //-Hidrografía-
  createTablaHidrografia(data: any) {
    this.listaHidrografiaUMF = [];
    this.listaHidrografiaUMF2 = [];
    let listUF: any = [];

    data.forEach((t: any) => {
      listUF.push({
        descripcion: t.properties.TIPO,
        nombre: t.properties.NOMBRE
      });
    });

    listUF.forEach((t: any) => {
      if (t.descripcion == 'Rio') {
        t.nombreRio = t.nombre;
      } else if (t.descripcion == 'Quebradas') {
        t.nombreQuebrada = t.nombre;
      } else if (t.descripcion == 'Cochas') {
        t.nombreLaguna = t.nombre;
      }
    });

    this.listaHidrografiaUMF = listUF;
    this.listaHidrografiaUMF2 = this.integrateHidrografia(listUF);
  }

  integrateHidrografia(listUF: any) {
    let listHidro: any = [];
    let listRio: any = [];
    let listQuebrada: any = [];
    let listLaguna: any = [];

    listUF.forEach((t: any) => {
      if (t.descripcion == 'Rio') {
        listRio.push(t);
      } else if (t.descripcion == 'Quebradas') {
        listQuebrada.push(t);
      } else if (t.descripcion == 'Cochas') {
        listLaguna.push(t);
      }
    });

    if (listRio.length > listQuebrada.length && listRio.length > listLaguna.length) {
      listRio.forEach((t: any) => {
        let obj: any = {};
        obj.idRio = t.idInfBasicaDet;
        obj.nombreRio = t.nombreRio;
        listHidro.push(obj);
      });
      listHidro.forEach((t: any, index: number) => {
        listQuebrada.forEach((q: any, index2: number) => {
          if (index == index2) {
            t.idQuebrada = q.idInfBasicaDet;
            t.nombreQuebrada = q.nombreQuebrada;
          }
        });
      });
      listHidro.forEach((t: any, index: number) => {
        listLaguna.forEach((l: any, index2: number) => {
          if (index == index2) {
            t.idLaguna = l.idInfBasicaDet;
            t.nombreLaguna = l.nombreLaguna;
          }
        });
      });
    }

    if (listQuebrada.length > listRio.length && listQuebrada.length > listLaguna.length) {
      listQuebrada.forEach((t: any) => {
        let obj: any = {};
        obj.idQuebrada = t.idInfBasicaDet;
        obj.nombreQuebrada = t.nombreQuebrada;
        listHidro.push(obj);
      });
      listHidro.forEach((t: any, index: number) => {
        listRio.forEach((r: any, index2: number) => {
          if (index == index2) {
            t.idRio = r.idInfBasicaDet;
            t.nombreRio = r.nombreRio;
          }
        });
      });
      listHidro.forEach((t: any, index: number) => {
        listLaguna.forEach((l: any, index2: number) => {
          if (index == index2) {
            t.idLaguna = l.idInfBasicaDet;
            t.nombreLaguna = l.nombreLaguna;
          }
        });
      });
    }

    if (listLaguna.length > listRio.length && listLaguna.length > listQuebrada.length) {
      listLaguna.forEach((t: any) => {
        let obj: any = {};
        obj.idLaguna = t.idInfBasicaDet;
        obj.nombreLaguna = t.nombreLaguna;
        listHidro.push(obj);
      });
      listHidro.forEach((t: any, index: number) => {
        listRio.forEach((r: any, index2: number) => {
          if (index == index2) {
            t.idRio = r.idInfBasicaDet;
            t.nombreRio = r.nombreRio;
          }
        });
      });
      listHidro.forEach((t: any, index: number) => {
        listQuebrada.forEach((q: any, index2: number) => {
          if (index == index2) {
            t.idQuebrada = q.idInfBasicaDet;
            t.nombreQuebrada = q.nombreQuebrada;
          }
        });
      });
    }

    return listHidro;
  }

  registrarHidrografia(listaH: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listaHidrografia: any[] = [];

    listaH.forEach((t: any) => {
      let obj = new InformacionBasicaDetalleModelPMFIC();
      obj.codInfBasicaDet = CodigoPOPAC.TAB_4_1;
      obj.codSubInfBasicaDet = CodigoPOPAC.TAB_4_1;
      obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
      obj.descripcion = t.descripcion;
      if (t.descripcion == 'Rio') {
        obj.nombreRio = t.nombre;
      } else if (t.descripcion == 'Quebradas') {
        obj.nombreQuebrada = t.nombre;
      } else if (t.descripcion == 'Cochas') {
        obj.nombreLaguna = t.nombre;
      }
      listaHidrografia.push(obj);
    });

    let params = [
      {
        idInfBasica: this.idInfBasicaH ? this.idInfBasicaH : 0,
        codInfBasica: CodigoPOPAC.CODIGO_PROCESO,
        codSubInfBasica: CodigoPOPAC.TAB_4,
        codNombreInfBasica: CodigoPOPAC.TAB_4_1,
        idPlanManejo: this.idPGMF,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listaHidrografia,
      },
    ];

    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok("Se registró aspectos de hidrografía correctamente.")
          this.listarHidrografia();
        } else {
          this.toast.warn(response?.message);
        }
      });
  }

  listarHidrografia() {
    this.listaHidrografiaUMF = [];
    this.listaHidrografiaUMF2 = [];

    const params = {
      idInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: CodigoPOPAC.TAB_4_1,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            this.idInfBasicaH = response.data[0].idInfBasica;
            response.data.forEach((element: any) => {
              this.listaHidrografiaUMF.push(element);
            });
            this.listaHidrografiaUMF2 = this.integrateHidrografia(this.listaHidrografiaUMF);
          }
        }
        this.dialog.closeAll();
      },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  }

  eliminarHidrografia() {
    if (this.listaHidrografiaUMF.length > 0) {
      if (this.idInfBasicaH != 0) {
        let params = {
          idInfBasica: this.idInfBasicaH,
          idInfBasicaDet: 0,
          codInfBasicaDet: "",
          idUsuarioElimina: this.user.idUsuario,
        };
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((result: any) => {
            if (result.success) {
              this.toast.ok("Se eliminó aspectos de hidrografía correctamente.");
            } else {
              this.toast.warn(result.message);
            }
            this.listaHidrografiaUMF = [];
            this.listaHidrografiaUMF2 = [];
            this.idInfBasicaH = 0;
          });
      } else {
        this.listaHidrografiaUMF = [];
        this.listaHidrografiaUMF2 = [];
      }
    }
  }

  obtenerHidrografia(id: any) {
    let items = this.mapApi.getLayerById(id, this.componenteInfBasica.viewTablaInformativa);
    let newItems: any = [];
    items.attributes.forEach((t: any) => {
      newItems.push({
        descripcion: t.properties.TIPO,
        nombre: t.properties.NOMBRE
      });
    });

    return newItems;
  }

  //-Fisiografía-
  createTablaFisiografia(data: any) {
    this.listaFisiografiaUMF = [];
    let listUF: any = [];
    data.forEach((t: any, index: any) => {
      listUF.push({
        vertice: index + 1,
        descripcion:
          t.properties.CobVeg2013
            ? t.properties.CobVeg2013
            : t.properties.unidad,
        areaHa: this.calculateArea(t),
        areaHaPorcentaje: 0,
      });
    });
    this.listaFisiografiaUMF = listUF;
  }

  listarFisiografia() {
    this.listaFisiografiaUMF = [];
    this.idInfBasicaF = 0;
    let params: any = {
      idInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: this.codigoAcordeonF,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            const element = response.data[0];
            this.idInfBasicaF = element.idInfBasica;
            this.isNewF = false;
            response.data.forEach((item: any) => {
              this.listaFisiografiaUMF.push(item);
            });
            // this.calculateTotalFisiografia();
          }
        }
      });
  }

  registrarFisiografia(listaAF: any) {
    if (this.isNewF) {
      this.toast.warn("Debe guardar Archivo shape file.");
    } else {
      // this.createStore4_2.submit()
      let listaFisiografia: any[] = [];

      listaAF.map((t: any) => {
        let obj = new InformacionBasicaPGMFIC();
        obj.codInfBasicaDet = CodigoPOPAC.TAB_4_2;
        obj.codSubInfBasicaDet = CodigoPOPAC.TAB_4_2;
        obj.areaHa = t.areaHa;
        obj.areaHaPorcentaje = t.areaHaPorcentaje;
        obj.nombre = t.nombre;
        obj.descripcion = t.descripcion;
        obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
        listaFisiografia.push(obj);
      })

      let params = [
        {
          idInfBasica: this.idInfBasicaF ? this.idInfBasicaF : 0,
          idPlanManejo: this.idPGMF,
          codInfBasica: CodigoPOPAC.CODIGO_PROCESO,
          codSubInfBasica: CodigoPOPAC.TAB_4,
          codNombreInfBasica: CodigoPOPAC.TAB_4_2,
          idUsuarioRegistro: this.user.idUsuario,
          listInformacionBasicaDet: listaFisiografia,
        },
      ];

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionAreaPmfiService
        .registrarInformacionBasica(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(
          (response: any) => {
            if (response.success == true) {
              this.toast.ok("Se registró aspectos de fisiografía correctamente.");
              this.listarFisiografia();
              // this.createStore4_2.submitSuccess();
            } else {
              this.toast.error(response?.message);
              // this.createStore4_2.submitError('');
            }
          },
          (error) => {
            // this.createStore4_2.submitError(error);
          }
        );
    }
  }

  eliminarFisiografia() {
    if (this.listaFisiografiaUMF.length > 0) {
      if (this.idInfBasicaF != 0) {
        let params = {
          idInfBasica: this.idInfBasicaF,
          idInfBasicaDet: 0,
          codInfBasicaDet: "",
          idUsuarioElimina: this.user.idUsuario,
        };
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((result: any) => {
            if (result.success) {
              this.toast.ok("Se eliminó aspectos de fisiografía correctamente.");
            } else {
              this.toast.warn(result.message);
            }
            this.listaFisiografiaUMF = [];
            this.idInfBasicaF = 0;
          });
      } else {
        this.listaFisiografiaUMF = [];
      }
    }
  }
}
