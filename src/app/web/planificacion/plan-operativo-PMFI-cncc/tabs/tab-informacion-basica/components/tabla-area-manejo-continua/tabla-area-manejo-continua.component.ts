import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { InformacionBasicaDetalle } from "src/app/model/InformacionAreaManejo";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { MapApi } from "src/app/shared/mapApi";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { TabInformacionBasicaComponent } from "../../tab-informacion-basica.component";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";

@Component({
  selector: "tabla-area-manejo-continua",
  templateUrl: "./tabla-area-manejo-continua.component.html",
  styleUrls: ["./tabla-area-manejo-continua.component.scss"],
})
export class TablaAreaManejoContinuaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() idInfBasica!: number;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;
  @Output() idInfBasicaU = new EventEmitter<any>();
  @Output() eliminaU = new EventEmitter<boolean>();

  CodigoPOPAC = CodigoPOPAC
  _idInfBasica: number = 0;
  _idInfBasicaDet: number = 0;
  idArchivoU: number = 0;
  listArchivo: any[] = []; 

  constructor(
    private messageService: MessageService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private parentAreManejo: TabInformacionBasicaComponent,
    private mapApi: MapApi,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.listarCoordenadas();
    this.listarGeometria();
  }

  listarCoordenadas() {
    this.listCoordinatesAnexo = [];
    var params = {
      idInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigoPOPAC.TAB_3_1,
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            this._idInfBasica = response.data[0].idInfBasica;
            this._idInfBasicaDet = response.data[0].idInfBasicaDet;
            response.data.forEach((t: any) => {
              if (t.codInfBasicaDet === this.CodigoPOPAC.TAB_3_1) {
                this.listCoordinatesAnexo.push({
                  idInfBasica: t.idInfBasica,
                  idInfBasicaDet: t.idInfBasicaDet,
                  codInfBasicaDet: t.codInfBasicaDet,
                  vertice: t.puntoVertice,
                  este: t.coordenadaEste,
                  norte: t.coordenadaNorte,
                  referencia: t.referencia,
                });
              }
            });
          }
        } else {
          this._idInfBasicaDet = 0;
        }
        this.idInfBasicaU.emit(this._idInfBasica);
      });
  }

  registrar() {
    if (!this.validarRegistro()) return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listMap: InformacionBasicaDetalle[] = this.listCoordinatesAnexo.map(
      (coordenada) => {
        return {
          idInfBasica: coordenada.idInfBasica || 0,
          idInfBasicaDet: coordenada.idInfBasicaDet
            ? coordenada.idInfBasicaDet
            : 0,
          idUnidadManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario,
          codInfBasicaDet: CodigoPOPAC.TAB_3_1,
          codSubInfBasicaDet: CodigoPOPAC.TAB_3_1,
          estado: "A",
          puntoVertice: coordenada.vertice,
          coordenadaEsteIni: coordenada.este,
          coordenadaNorteIni: coordenada.norte,
          referencia: coordenada.referencia,
          idUsuarioModificacion: null,
          descripcion: "",
        };
      }
    );
    let params = [
      {
        idInfBasica: this.idInfBasica,
        codInfBasica: CodigoPOPAC.CODIGO_PROCESO,
        codSubInfBasica: this.CodigoPOPAC.TAB_3,
        codNombreInfBasica: this.CodigoPOPAC.TAB_3_1,
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.success == true) {
          this.toast.ok('Se registró la ubicación de la comunidad correctamente.');
          this.listarCoordenadas();
          this.listarGeometria();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Agregue archivo VERTICES PGMF \n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  onChangeFileSHP(e: any) {
    let item: any = this.parentAreManejo._layers.find(
      (e: any) => e.descripcion === CodigoPOPAC.TAB_3_1
    );
    if (item !== undefined) {
      this.toast.warn("Ya se encuentra un achivo para esta sección.");
      return;
    }

    this.parentAreManejo.onChangeFileSHP(
      e,
      true,
      "TPPUNTO",
      CodigoPOPAC.TAB_3_1
    );
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaAnexo(data[0].features);
    });
    e.target.value = "";
  }

  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo.push({
        vertice: t.properties.vertice,
        este: t.geometry.coordinates[0],
        norte: t.geometry.coordinates[1],
        referencia: "",
      });
    });
  }

  eliminarLista(event: any) {
    if (this.listCoordinatesAnexo.length > 0) {
      let msg = "¿Está seguro de eliminar los registros?."
      if (this.idArchivoU != 0) {
        msg += "\nSe eliminará archivo shapefile de vértices de área de territorio comunal."
      }
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: msg,
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          if (this._idInfBasica != null && this._idInfBasica != undefined) {
            let params = {
              idInfBasica: this._idInfBasica,
              idInfBasicaDet: 0,
              codInfBasicaDet: '',
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaPmfiService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  this.toast.ok('Se eliminó ubicación de la comunidad correctamente.');
                } else {
                  this.toast.warn(result.message);
                }
                this.eliminarArchivoDetalle(this.idArchivoU);
                this.listCoordinatesAnexo = [];
                this._idInfBasica = 0;
                this.listarCoordenadas();
              });
          } else {
            this.listCoordinatesAnexo = [];
            this._idInfBasica = 0;
            this.listarCoordenadas();
          }
        },
        reject: () => { },
      });
    }
  }
  listarGeometria() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: CodigoPOPAC.CODIGO_PROCESO,
      codigoSubSeccion: "PMFICINFBATIM",
    };
    this.servicePlanManejoGeometria
      .listarPlanManejoGeometria(item)
      .subscribe(
        (result: any) => {
          if (result.data.length > 0) {
            result.data.forEach((t: any) => {
              if (t.geometry_wkt !== null) {
                if (!this.listArchivo.includes(t.idArchivo)) {
                  if (t.descripcion == "COD1" && t.tipoGeometria == "TPPUNTO") {
                    this.idArchivoU = t.idArchivo;
                    this.listArchivo.push(t.idArchivo);
                  }
                }
              }
            });
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error");
        }
      );
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    if (idArchivo != 0) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.servicePlanManejoGeometria
        .eliminarPlanManejoGeometriaArchivo(idArchivo, this.user.idUsuario)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.toast.ok("Se eliminó el archivo y/o geometría correctamente.");
              this.eliminaU.emit(true);
            } else {
              this.toast.error("Ocurrió un problema, intente nuevamente");
            }
          },
          (error) => {
            this.toast.error("Ocurrió un problema, intente nuevamente");
          }
        );
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ severity: "success", summary: "", detail: mensaje });
  }

  WarningMessage(mensaje: any) { 
    this.messageService.add({ severity: "error", summary: "ERROR", detail: mensaje });
  }
}
