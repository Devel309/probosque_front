import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PlanManejoService, UsuarioService } from '@services';
import { MapApi, ToastService } from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { from } from 'rxjs';
import { concatMap } from 'rxjs-compat/operator/concatMap';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ModalFileComponent } from 'src/app/components/modal/modal-file/modal-file.component';
import { ModalObservacionesComponent } from 'src/app/components/modal/modal-observaciones/modal-observaciones.component';
import { InformacionUnidadManejo } from 'src/app/model/InformacionAreaManejo';
import { InformacionBasicaDetalleModelPMFIC, InformacionBasicaModelPMFIC, ListInformacionBasicaDetSubPMFIC } from 'src/app/model/InformacionSocieconomicaPMFIC';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { CodigoPOPAC } from 'src/app/model/util/POPAC/POPAC';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { MapCustomComponent } from 'src/app/shared/components/map-custom/map-custom.component';
import { TablaAreaManejoContinuaComponent } from './components/tabla-area-manejo-continua/tabla-area-manejo-continua.component';
import { TablaInformativaMapaComponent } from './components/tabla-informativa-mapa/tabla-informativa-mapa.component';

@Component({
  selector: 'app-tab-informacion-basica',
  styleUrls: ['./tab-informacion-basica.component.scss'],
  templateUrl: './tab-informacion-basica.component.html',
})
export class TabInformacionBasicaComponent implements OnInit {
  @ViewChild('map') mapCustom!: MapCustomComponent;
  @ViewChild(TablaInformativaMapaComponent) tim!: TablaInformativaMapaComponent;
  @Input() idPGMF!: number;
  @Input() disabled!: boolean;
  @Input() idPlanManejoPadre!: number;

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;

  total: number = 0;

  suma: number = 0;

  totalPorcentaje: number = 0;

  // @ViewChild(TablaAreaManejoDivididaComponent) acordeon_3_3!: TablaAreaManejoDivididaComponent;
  @ViewChild(TablaAreaManejoContinuaComponent)
  acordeon_3_2!: TablaAreaManejoContinuaComponent;

  ref: DynamicDialogRef = new DynamicDialogRef();
  usuario = {} as UsuarioModel;
  view: any = null;
  viewZona: any = null;
  viewTablaInformativa: any = null;

  listCoordinatesAnexo1: any[] = [];
  listCoordinatesAnexo2: any[] = [];
  idInfBasica: number = 0;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  listaBosque: any[] = [];
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  @ViewChild('tblAcreditacionComunal', { static: true })
  private tblAcreditacionComunal!: ElementRef;
  tipoBosquesList: any[] = [];
  totalAreaTipoBosque: String = '';
  totalPorcentajeTipoBosque: String = '';
  unidadManejo = {} as InformacionUnidadManejo;
  listCoordinatesAnexo: any[] = [];
  listAnexoArea: any[] = [];
  totalAnexoArea!: number;
  listAccesibilidad: any[] = [];
  totalAreaZonificacion: number = 0;

  //listado
  listOrdenInterno: any[] = [{}, {}];
  CodigoPOPAC = CodigoPOPAC;

  planificacion: any = [];

  data = [
    {
      ruta: 'De (ciudad) al centro de la comunidad',
    },
    {
      ruta: 'Del centro de la comunidad al área de aprovechamiento del primer año operativo',
    },
  ];
  zonificacion = [
    {
      categoria: '1. Áreas para produccion maderable',
      area: 25,
      porcentaje: 0,
      observaciones: '',
      cargarObservacion: false,
      editarObservaciones: true,
    },
    {
      categoria: '1. Áreas para produccion maderable',
      area: 5,
      porcentaje: 0,
      observaciones: '',
      cargarObservacion: false,
      editarObservaciones: true,
    },
    {
      categoria: '1. Áreas para produccion maderable',
      area: 15,
      porcentaje: 0,
      observaciones: '',
      cargarObservacion: false,
      editarObservaciones: true,
    },
  ];

  areaManejoForestal = [{}];

  transporte = [
    { label: "Terrestre", value: "TERRESTRE" },
    { label: "Fluvial", value: "FLUVIAL" },
    { label: "Mixto", value: "MIXTO" },
  ];

  vehiculo = [
    { label: "Camión / Carreta", value: "Camión / Carreta" },
    { label: "Chata", value: "Chata" },
    { label: "Bote", value: "Bote" },
    { label: "Tráiler", value: "Tráiler" },
    { label: "Semitráiler", value: "Semitráiler" },
    { label: "Tractor", value: "Tractor" },
    { label: "Otros", value: "Otros" },
  ];

  informacionBasica3_4: InformacionBasicaModelPMFIC = {} as InformacionBasicaModelPMFIC;
  informacionBasicaDet3_4: InformacionBasicaDetalleModelPMFIC[] = [];
  isSubDet!: boolean;

  idInfBasicaU: any = 0;
  idInfBasicaS: any = 0;

  constructor(
    public dialogService: DialogService,
    private planManejoService: PlanManejoService,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private mapApi: MapApi,
    private serviceGeoforestal: ApiGeoforestalService,
    private serviceExternos: ApiForestalService
  ) {}

  ngOnInit() {
    this.obtenerPlanManejo();
    this.initializeMap();
    // this.listarInfBasica();
    this.listarInformacionBasica3_4();
  }

  obtenerPlanManejo() {
    var params = {
      idPlanManejo: this.idPGMF,
    };
    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((response: any) => {
        this.idPlanManejoPadre = response.data.idPlanManejoPadre
          ? response.data.idPlanManejoPadre
          : 0;
      });
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.view = view;
    this.viewZona = view;
    this.viewTablaInformativa = view;
  }

  listarInformacionBasica3_4() {
    this.informacionBasicaDet3_4 = [];
    const params = {
      idInfBasica: this.CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: this.CodigoPOPAC.TAB_3_4
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];
          this.informacionBasica3_4 = new InformacionBasicaModelPMFIC(element);
          response.data.forEach((item: any) => {
            this.informacionBasicaDet3_4.push(item);
            if (item.listInformacionBasicaDetSub.length > 0) this.isSubDet = true;
          });
        }
      });
  }

  registrarInformacionBasica3_4() {
    this.informacionBasica3_4.idPlanManejo = this.idPGMF;
    this.informacionBasica3_4.idInfBasica = this.informacionBasica3_4.idInfBasica ? this.informacionBasica3_4.idInfBasica : 0;
    this.informacionBasica3_4.codInfBasica = this.CodigoPOPAC.CODIGO_PROCESO;
    this.informacionBasica3_4.codSubInfBasica = this.CodigoPOPAC.TAB_3;
    this.informacionBasica3_4.codNombreInfBasica = this.CodigoPOPAC.TAB_3_4;
    this.informacionBasica3_4.idUsuarioRegistro = this.user.idUsuario;
    this.informacionBasica3_4.listInformacionBasicaDet = [];
    this.informacionBasicaDet3_4.forEach((item) => {
      item.codInfBasicaDet = this.CodigoPOPAC.TAB_3_4;
      item.codSubInfBasicaDet = this.CodigoPOPAC.TAB_3_4;
      item.idInfBasica = item.idInfBasica ? item.idInfBasica : 0;
      item.idInfBasicaDet = item.idInfBasica ? item.idInfBasicaDet : 0;
      item.idUsuarioRegistro = this.user.idUsuario;
      this.informacionBasica3_4.listInformacionBasicaDet.push(item);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica([ this.informacionBasica3_4 ])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró la accesibilidad de la UMF correctamente.");
          this.listarInformacionBasica3_4();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }


  listarInfBasica(){
    let params: any = {
      idInfBasica: this.CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPGMF,
      codCabecera: ""
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        this.idInfBasica = response.data[0]?.idInfBasica || 0;
      });
  }

  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    code: any,
    codigoSubSeccion: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      codigoSubTipoPGMF: code,
      withVertice: withVertice,
      codigoSubSeccion: codigoSubSeccion,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }

  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (config.codigoSubTipoPGMF === "PGMFAREA") {
        this.calculateArea(data);
      }
    });
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.codigoSubTipoPGMF;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.annex = config.annex;
      layer.descripcion = config.codigoSubSeccion;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.codigoSubSeccion;
    this._filesSHP.push(file);
  }

  calculateArea(data: any) {
    if (data[0].title === "PGMF") {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = data[0].features[0].geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, "hectares");
      let tabla = this.tblAcreditacionComunal.nativeElement;
      let body = tabla.querySelector("tbody");
      let trs = body.querySelectorAll("tr");
      let tdArea = trs[0].querySelectorAll("td")[2];
      tdArea.innerHTML = area.toFixed(3);
    }
    data[0].features.forEach((t: any) => {
      if (t.geometry.type === "Polygon") {
        let item2: any = {
          anexoSector: t.properties.ANEXO,
          detalle: [],
        };
        this.consultarTiposBosque(t.geometry.coordinates).subscribe(
          (result: any) => {
            result.dataService.data.capas.forEach((t2: any) => {
              if (t2.geoJson !== null) {
                let item = {
                  subparcela: "",
                  tipoBosque: t2.nombreCapa,
                  porcentaje: 0,
                  area:
                    t2.geoJson.features[0].properties.SUPAPR ||
                    t2.geoJson.features[0].properties.za_super,
                };
                item2.detalle.push(item);
              }
            });
            this.tipoBosquesList.push(item2);
            this.calculateAreaTotal();
          }
        );
      }
    });
  }

  calculateAreaTotal() {
    let sum1 = 0;
    let sum2 = 0;

    for (let item of this.tipoBosquesList) {
      item.detalle.forEach((t: any) => {
        sum1 += t.area;
      });
    }
    for (let item of this.tipoBosquesList) {
      item.detalle.forEach((t: any) => {
        t.porcentaje = Number(((100 * t.area) / sum1).toFixed(1)) || 0;
        sum2 += t.porcentaje;
      });
    }
    this.totalAreaTipoBosque = `${sum1.toFixed(2)}`;
    this.totalPorcentajeTipoBosque = `${sum2.toFixed(1)} %`;
  }


  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }

  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.withVertice === true) {
      popupTemplate.content = [
        {
          type: "fields",
          fieldInfos: [
            {
              fieldName: "anexo",
              label: "Anexo",
            },
            {
              fieldName: "vertice",
              label: "Vertice",
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: "este",
              label: "Este (X)",
            },
            {
              fieldName: "norte",
              label: "Norte (Y)",
            },
          ],
        },
      ];
    }
    return popupTemplate;
  }

  consultarTiposBosque(geometry: any) {
    let params = {
      idClasificacion: "",
      geometria: {
        poligono: geometry,
      },
    };
    return this.serviceExternos.identificarTipoBosque(params);
  }

  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }

  eliminarInformacionBasica(idInfBasica:any,codigoInfBasicaDet:string){
    let params = {
      idInfBasica: this.idInfBasica,
      codInfBasicaDet: codigoInfBasicaDet,
      idInfBasicaDet: idInfBasica,
      idUsuarioElimina: this.user.idUsuario
    }
    return this.informacionAreaPmfiService.eliminarInformacionBasica(params);
  }

  listarUbicacion(items: any) {
    this.listCoordinatesAnexo = [];
    this.listCoordinatesAnexo = items;
  }

  listarSuperficie(items: any) {
    this.listCoordinatesAnexo2 = [];
    this.listCoordinatesAnexo2 = items;
  }

  listarArea(items: any) {
    this.listAnexoArea = [];
    this.listAnexoArea = items;
  }

  listarTotalArea(total: any) {
    this.totalAnexoArea = total;
  }

  listarAccesibilidad(items: any) {
    this.listAccesibilidad = [];
    this.listAccesibilidad = items;

    this.informacionBasicaDet3_4.forEach((element: InformacionBasicaDetalleModelPMFIC, index: number) => {
      if (index == 1) {
        let det: ListInformacionBasicaDetSubPMFIC[] = [];
        let num: number = 0;
        let ant: string = "";
        this.listAccesibilidad.forEach((item: any) => {
          let subDet = new ListInformacionBasicaDetSubPMFIC();
          subDet.descripcion = item.ruta;
          subDet.detalle = item.transporte;
          subDet.codTipoInfBasicaDet = this.CodigoPOPAC.TAB_3_4;;
          subDet.codSubTipoInfBasicaDet = this.CodigoPOPAC.TAB_3_4;
          det.push(subDet);
          this.isSubDet = true;
          if (ant != item.transporte) {
            num++;
          }
          ant = item.transporte;
        });

        if (num == 1) {
          element.descripcion = ant;
        }
        if (num > 1) {
          element.descripcion = "MIXTO";
        }
        element.observaciones = "S";
        element.listInformacionBasicaDetSub = [ ...det];
      }
    });
  }

  listarTotalAreaZonificacion(total: any) {
    this.totalAreaZonificacion = total;
  }

  listarIdInfBasicaU(id: number) {
    this.idInfBasicaU = id;
  }

  listarIdInfBasicaS(id: number) {
    this.idInfBasicaS = id;
  }

  eliminarU(elimina: boolean) {
    if (elimina == true) {
      this.initializeMap();
      this.tim.ngAfterViewInit();
    }
  }

  eliminarS(elimina: boolean) {
    if (elimina == true) {
      this.initializeMap();
      this.tim.ngAfterViewInit();
    }
  }
}
