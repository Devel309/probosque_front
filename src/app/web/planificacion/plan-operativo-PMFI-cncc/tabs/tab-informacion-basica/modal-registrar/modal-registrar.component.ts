import { Component, OnInit } from "@angular/core";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";

@Component({
  selector: "modal-registrar",
  templateUrl: "./modal-registrar.component.html",
  styleUrls: ["./modal-registrar.component.scss"],
})
export class ModalRegistrarComponent implements OnInit {
  CodigoPMFIC = CodigoPMFIC;

  listaBosque: any[] = [];
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  idArchivoTB: number = 0;
  idInfBasicaTB: number = 0;
  codigo: string = "";
  idPGMF: number = 0;

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef
  ) {}

  ngOnInit() {
    this.listaBosque = this.config.data.data;
    this.codigo = this.config.data.codigo;
    this.calculateAreaTotalBosques();
  }

  // aspectos biologicos - tipo de bosque
  calculateAreaTotalBosques() {
    let sum1 = 0;
    this.listaBosque.forEach((t: any) => {
      sum1 += t.areaHa;
    });

    this.listaBosque.forEach((t: any) => {
      t.areaHaPorcentaje = Number(((100 * t.areaHa) / sum1).toFixed(2));
    });
    this.totalAreaBosque = parseFloat(`${sum1.toFixed(2)}`);
    this.totalPorcentajeBosque = parseFloat(`${(100).toFixed(2)} %`);
  }

  cerrarModal() {
    this.ref.close();
  }
}
