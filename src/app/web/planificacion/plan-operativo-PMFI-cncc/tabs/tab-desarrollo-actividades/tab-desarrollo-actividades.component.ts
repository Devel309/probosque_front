import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { CodigoPOPAC } from 'src/app/model/util/POPAC/POPAC';
import { LoadingComponent } from '../../../../../components/loading/loading.component';
import { ActividadSilvicultural } from '../../../../../model/ActividadesSilviculturalesModel';
import { OrganizacionActividadModel } from '../../../../../model/OrganizacionActividad';
import { CodigoPMFIC } from '../../../../../model/util/PMFIC/CodigoPMFIC';
import { InformacionGeneralService } from '../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { ActividadesSilviculturalesService } from '../../../../../service/planificacion/plan-general-manejo-pgmfa/actividadesSilviculturales.service';
import {CodigoUtil} from '../../../../../model/util/CodigoUtil';

@Component({
  selector: 'app-tab-desarrollo-actividades',
  templateUrl: './tab-desarrollo-actividades.component.html',
})
export class TabDesarrolloActividadesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  tituloModal: string =
    'Registrar Organización para el Desarrollo de la Actividad';

  MADERABLE: string = 'MAD';
  NO_MADERABLE: string = 'NMAD';

  organizacion = {} as OrganizacionActividadModel;
  CodigoUtil = CodigoUtil;
  organizacionesList = [] as OrganizacionActividadModel[];
  organizacionesList2 = [] as OrganizacionActividadModel[];

  ref: DynamicDialogRef = new DynamicDialogRef();

  indexUpdate: number = -1;
  tipoTabla: string = '';

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  displayBasic: boolean = false;

  edit: boolean = false;
  idActSilvicultural: number = 0;

  listDetalle: ActividadSilvicultural[] = [];

  CodigoPOPAC = CodigoPOPAC;
  CodigoPMFIC = CodigoPMFIC;

  tipoDocumento: string = '';
  nroDocumento: string = '';

  constructor(
    private user: UsuarioService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private actividadesSilviculturalesService: ActividadesSilviculturalesService,
    private toast: ToastService,
    private informacionGeneralService: InformacionGeneralService
  ) {}

  ngOnInit() {
    this.listarOrganizacionDesarrollo();

  }

  listarOrganizacionDesarrollo() {
    this.organizacionesList = [];
    this.organizacionesList2 = [];

    let params = {
      idTipo: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (!!response.data) {
          if (response.data.detalle.length != 0) {
            let proviene = response.data.codActividad;

            if (proviene == CodigoPMFIC.CODIGO_PROCESO) {
              this.idActSilvicultural = response.data.idActividadSilvicultural; //0

              //setear valores a 0
              let listado = response.data.detalle;
              listado.forEach((e: any) => {
                if(e.idActividadSilviculturalDet == null && e.actividad != "Otros (especificar)"){
                  e.equipo = CodigoUtil.VALOR_DEFECTO;
                  e.idActividadSilviculturalDet = 0;
                }
                if (e.observacionDetalle == 'MAD') {
                  this.organizacionesList.push(e);
                } else if (e.observacionDetalle == 'NMAD') {
                  this.organizacionesList2.push(e);
                }
              });
            } else if (proviene == CodigoPOPAC.CODIGO_PROCESO) {
              this.idActSilvicultural = response.data.idActividadSilvicultural;
              let listado = response.data.detalle;
              listado.forEach((e: any) => {
                //e.idTipo = CodigoPOPAC.CODIGO_PROCESO;
                //e.idActividadSilviculturalDet = 0;
                if (e.observacionDetalle == 'MAD') {
                  this.organizacionesList.push(e);
                } else if (e.observacionDetalle == 'NMAD') {
                  this.organizacionesList2.push(e);
                }
              });
            }
          }
        }
      });
  }

  cerrarModal() {
    this.organizacion = {} as OrganizacionActividadModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = 'Registrar Organización';
  }

  agregarOrganizacion() {
    if (!this.validarOrganizacion()) {
      return;
    }
    if (this.edit) {
      this.editarOrganizacion();
    } else {
      this.registrarOrganizacion();
    }
  }

  validarOrganizacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.organizacion.actividad == null ||
      this.organizacion.actividad == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Actividad Realizada.\n';
    }
    if (
      this.organizacion.descripcionDetalle == null ||
      this.organizacion.descripcionDetalle == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Forma de Organización.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  openEditarOrganizacion(
    organizacion: OrganizacionActividadModel,
    index: number,
    tipo: string
  ) {
    this.organizacion = { ...organizacion };
    this.displayBasic = true;
    this.edit = true;
    this.indexUpdate = index;
    this.tituloModal = 'Editar Organización';
  }

  openEliminarOrganizacion(
    event: Event,
    index: number,
    organizacion: OrganizacionActividadModel,
    tipo: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.tipoTabla = tipo;

        if (organizacion.idActividadSilviculturalDet != 0) {
          var params = [
            {
              estado: 'I',
              idActividadSilviculturalDet:
                organizacion.idActividadSilviculturalDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesSilviculturalesService
            .eliminarActividadSilviculturalPFDM(params)
            .subscribe(
              (data: any) => {
                this.SuccessMensaje(data.message);

                if (this.tipoTabla == this.MADERABLE)
                  this.organizacionesList.splice(index, 1);
                else if (this.tipoTabla == this.NO_MADERABLE)
                  this.organizacionesList2.splice(index, 1);

                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          if (this.tipoTabla == this.MADERABLE)
            this.organizacionesList.splice(index, 1);
          else if (this.tipoTabla == this.NO_MADERABLE)
            this.organizacionesList2.splice(index, 1);
          this.dialog.closeAll();
        }
      },
      reject: () => {

      },
    });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  validarOrganizacionOrganizacionDesarrollo = (): boolean => {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.organizacionesList.length === 0) {
      validar = false;
      mensaje = mensaje += '(*) Debe de agregar al menos 1 registro';
    }

    if (this.organizacionesList2.length === 0) {
      validar = false;
      mensaje = mensaje += '(*) Debe de agregar al menos 1 registro';
    }

    if (mensaje != '') this.ErrorMensaje(mensaje);
    return validar;
  };

  cargarIdArchivo(idArchivo: any) {

  }

  registrarOrganizacionesDesarrollo() {
    if (!this.validarOrganizacionOrganizacionDesarrollo()) return;

    this.listDetalle = [];

    this.organizacionesList.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = CodigoPOPAC.CODIGO_PROCESO;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    this.organizacionesList2.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = CodigoPOPAC.CODIGO_PROCESO;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    var params = {
      aprovechamiento: null,
      divisionAdministrativa: null,
      laboresSilviculturales: {
        idActSilvicultural: this.idActSilvicultural
          ? this.idActSilvicultural
          : 0,
        codigoTipoActSilvicultural: CodigoPOPAC.CODIGO_PROCESO,
        actividad: null,
        descripcion: null,
        observacion: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listActividadSilvicultural: this.listDetalle,
      },
    };

    //return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .guardarSistemaManejoPFDM(params)
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.dialog.closeAll();
            this.SuccessMensaje(
              'Se registró la organización para el desarrollo de la actividad correctamente.'
            );
            this.listarOrganizacionDesarrollo();
          } else this.ErrorMensaje(result.message);
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  }

  registrarOrganizacion() {
    this.organizacion.idActividadSilviculturalDet = 0;
    if (this.tipoTabla == this.MADERABLE) {
      this.organizacion.observacionDetalle = this.MADERABLE;
      this.organizacionesList.push(this.organizacion);
    } else if (this.tipoTabla == this.NO_MADERABLE) {
      this.organizacion.observacionDetalle = this.NO_MADERABLE;
      this.organizacionesList2.push(this.organizacion);
    }
    this.tipoTabla = '';
    this.organizacion = {} as OrganizacionActividadModel;
    this.displayBasic = false;
  }

  editarOrganizacion() {
    this.displayBasic = false;
    this.tituloModal = 'Registrar Capacitación';
    this.edit = false;
    //this.organizacionesList[this.findIndexById(this.organizacion.idActividadSilviculturalDet)] = this.organizacion;
    if (this.indexUpdate >= 0) {
      if (this.organizacion.observacionDetalle == this.MADERABLE)
        this.organizacionesList[this.indexUpdate] = this.organizacion;
      else if (this.organizacion.observacionDetalle == this.NO_MADERABLE)
        this.organizacionesList2[this.indexUpdate] = this.organizacion;

      this.indexUpdate = -1;
    }

    this.organizacion = {} as OrganizacionActividadModel;
  }

  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.organizacionesList.length; i++) {
      if (this.organizacionesList[i].idActividadSilviculturalDet === id) {
        index = i;
        break;
      }
    }
    return index;
  }

  abrirModal(tipo: string) {
    this.tipoTabla = tipo;
    this.displayBasic = true;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
