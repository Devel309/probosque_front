import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SistemaManejoForestalService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  OrdenamientoProteccionCabeceraModel,
  OrdenamientoProteccionDetalleModel,
  SistemaOrdenamientoDetalle,
  SistemaOrdenamientoodel,
} from "src/app/model/OrdenamientoProteccionUMFModel";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { OrdenamientoProteccionService } from "src/app/service/planificacion/plan-manejo-forestal-intermedio/ordenamiento.service";

import { FormMedidaProteccionComponent } from "./modal/form-medida-proteccion/form-medida-proteccion.component";

@Component({
  selector: "tab-ordenamiento-proteccion",
  templateUrl: "./tab-ordenamiento-y-proteccion.component.html",
})
export class TabOrdenamientoProteccionComponent implements OnInit {
  @Input() idPGMF!: number;
  @Input() idPlanManejoPadre!: number;
  @Input() disabled!: boolean ;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  ref: DynamicDialogRef = new DynamicDialogRef();

  CodigoPOPAC = CodigoPOPAC;

  newList1: any = [];
  listCoordinatesAnexo1: any[] = [];
  newList2: any = [];
  listCoordinatesAnexo2: any = [];
  newList3: any = [];
  listCoordinatesAnexo3: any = [];

  usuario = {} as UsuarioModel;
  listGeneral: any[] = [];
  listBloquesVetices: any[] = [];
  listPrimerBloque: any[] = [];
  listAnioOperativo: any[] = [];
  pendiente: boolean = false;
  pendienteSistema: boolean = false;
  isPadre: boolean = false;
  listMedidadProteccion: any[] = [];
  idSistema: number = 0;
  disabledBtn: boolean = false;

  objSuperficieUbicacionBloques = new OrdenamientoProteccionCabeceraModel();
  objSuperficieUbicacionPrimerBloque = new OrdenamientoProteccionCabeceraModel();
  objSuperficieUbicacion = new OrdenamientoProteccionCabeceraModel();
  objMedidasProteccion = new OrdenamientoProteccionCabeceraModel();

  constructor(
    public dialogService: DialogService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private toast: ToastService,
    private ordenamientoProteccionService: OrdenamientoProteccionService,
    private sistemaManejoForestalService: SistemaManejoForestalService
  ) { }

  ngOnInit() {
    this.usuario = this.user.usuario;
   // this.isPadre = true
    this.listaDefault();
    this.getSistemaManejo();
  }

  getSistemaManejo() {
    this.listMedidadProteccion = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codigoProceso: "POPAC",
    };
    this.sistemaManejoForestalService
      .sistemaManejoForestal(params)
      .subscribe((response) => {
        if (
          response.data.descripcionCicloCorta == "NO HAY DATA EN EL PLAN PADRE"
        ) {
          this.pendienteSistema = true;
        } else if (
          !!response.data.idSistemaManejoForestal ||
          response.data.idSistemaManejoForestal == 0
        ) {
          this.idSistema = response.data.idSistemaManejoForestal;
          response.data.detalle?.forEach((response) => {
            var obj = new SistemaOrdenamientoDetalle(response);
            obj.codigoTipoDetalle = CodigoPOPAC.ACORDEON_7_4;
            obj.idUsuarioRegistro = this.user.idUsuario;
            obj.codigoProceso = CodigoPOPAC.CODIGO_PROCESO;
            this.listMedidadProteccion.push(obj);
          });
        }
      });
  }

  listaDefault() {
    this.newList1 = [];
    this.newList2 = [];
    this.newList3 = [];
    this.listCoordinatesAnexo1 = [];
    this.listCoordinatesAnexo2 = [];
    this.listCoordinatesAnexo3 = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codTipoOrdenamiento: CodigoPOPAC.CODIGO_PROCESO,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ordenamientoProteccionService
      .listarOrdenamientoInternoDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data[0].justificacion == "NO HAY DATA EN EL PLAN PADRE") {
          this.pendiente = true;
        } else {
          response.data.forEach((element: any) => {
            if (element.idOrdenamientoProteccion != null) {
              this.objMedidasProteccion = new OrdenamientoProteccionCabeceraModel(
                element
              );
              if (element.idOrdenamientoProteccion == 0) this.isPadre = true;
              
              this.objMedidasProteccion.idPlanManejo = this.idPGMF;
              this.objMedidasProteccion.codTipoOrdenamiento = CodigoPOPAC.CODIGO_PROCESO;
              this.objMedidasProteccion.idUsuarioRegistro = this.user.idUsuario;
              element.listOrdenamientoProteccionDet.forEach((item: any) => {
                if (item.codigoTipoOrdenamientoDet == CodigoPOPAC.ACORDEON_7_1) {
                  this.newList1.push({
                    idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                    vertice: item.verticeBloque,
                    referencia: item.observacion,
                    este: item.coordenadaEste,
                    norte: item.coordenadaNorte,
                    anexo: item.descripcion,
                    superficie: item.areaHA,
                  });
                  this.listCoordinatesAnexo1 = this.newList1.groupBy(
                    (t: any) => t.anexo
                  );
                  this.listCoordinatesAnexo1.length != 0 ? this.disabledBtn = true : this.disabledBtn = false;
                } else if (item.codigoTipoOrdenamientoDet == CodigoPOPAC.ACORDEON_7_2) {
                  this.newList2.push({
                    idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                    vertice: item.verticeBloque,
                    referencia: item.observacion,
                    este: item.coordenadaEste,
                    norte: item.coordenadaNorte,
                    anexo: item.descripcion,
                  });
                  this.listCoordinatesAnexo2 = this.newList2.groupBy(
                    (t: any) => t.anexo
                  );
                } else if (item.codigoTipoOrdenamientoDet == CodigoPOPAC.ACORDEON_7_3) {
                  this.newList3.push({
                    idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                    vertice: item.verticeBloque,
                    referencia: item.observacion,
                    este: item.coordenadaEste,
                    norte: item.coordenadaNorte,
                    anexo: item.descripcion,
                  });
                  this.listCoordinatesAnexo3 = this.newList3.groupBy(
                    (t: any) => t.anexo
                  );
                }
              });
            }
          });
          if (this.listCoordinatesAnexo1.length != 0) {
            this.agruparBloques();
          }
          if (this.listCoordinatesAnexo2.length != 0) {
            this.agruparVerticesPrimerBloque();
          }
          if (this.listCoordinatesAnexo3.length != 0) {
            this.agruparVerticesAnioOperativo();
          }
        }
      });
  }

  //7.1
  agruparBloques() {
    this.listCoordinatesAnexo1.forEach((t: any) => {
      t.superficie = t.value[0].superficie;
    });

    this.listBloquesVetices = [];
    this.listCoordinatesAnexo1.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.areaHA = element.superficie;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPOPAC.ACORDEON_7_1;
        obj.actividadesRealizar = String(this.idPGMF);
        obj.observacionDetalle = CodigoPOPAC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet;
        this.listBloquesVetices.push(obj);
      });
    });
  }

  listBloques(event: any) {
    this.listBloquesVetices = [];
    event.length != 0 ? this.disabledBtn = true : this.disabledBtn = false;
    event.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.areaHA = element.superficie;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPOPAC.ACORDEON_7_1;
        obj.actividadesRealizar = String(this.idPGMF);
        obj.observacionDetalle = CodigoPOPAC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listBloquesVetices.push(obj);
      });
    });
  }

  //7.2
  agruparVerticesPrimerBloque() {
    this.listPrimerBloque = [];
    this.listCoordinatesAnexo2.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPOPAC.ACORDEON_7_2;
        obj.actividadesRealizar = String(this.idPGMF);
        obj.observacionDetalle = CodigoPOPAC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listPrimerBloque.push(obj);
      });
    });
  }

  listVerticesPrimerBloque(event: any) {
    this.listPrimerBloque = [];
    event.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.codigoTipoOrdenamientoDet = CodigoPOPAC.ACORDEON_7_2;
        obj.actividadesRealizar = String(this.idPGMF);
        obj.observacionDetalle = CodigoPOPAC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listPrimerBloque.push(obj);
      });
    });
  }

  //7.3
  agruparVerticesAnioOperativo() {
    this.listAnioOperativo = [];
    this.listCoordinatesAnexo3.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.codigoTipoOrdenamientoDet = CodigoPOPAC.ACORDEON_7_3;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.actividadesRealizar = String(this.idPGMF);
        obj.observacionDetalle = CodigoPOPAC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listAnioOperativo.push(obj);
      });
    });
  }

  listVerticesAnioOperativo(event: any) {
    this.listAnioOperativo = [];
    event.forEach((element: any) => {
      element.value.forEach((value: any) => {
        var obj = new OrdenamientoProteccionDetalleModel();
        obj.descripcion = element.key;
        obj.coordenadaEste = value.este;
        obj.coordenadaNorte = value.norte;
        obj.verticeBloque = value.vertice;
        obj.observacion = value.referencia;
        obj.codigoTipoOrdenamientoDet = CodigoPOPAC.ACORDEON_7_3;
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj.actividadesRealizar = String(this.idPGMF);
        obj.observacionDetalle = CodigoPOPAC.CODIGO_PROCESO;
        obj.idOrdenamientoProteccionDet = value.idOrdenamientoProteccionDet ? value.idOrdenamientoProteccionDet : 0;
        this.listAnioOperativo.push(obj);
      });
    });
  }

  //
  editarMedidas(type: string, data?: any) {
    let titulo = "";
    if (type == "edit") {
      titulo = "Editar"
    } else {
      titulo = "Agregar"
    }
    this.ref = this.dialogService.open(FormMedidaProteccionComponent, {
      header: titulo + " Medidas de Protección",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        item: data,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (type == "edit") {
          data.actividades = resp.actividades;
          data.descripcionSistema = resp.descripcionSistema;
        } else {
          var objDetalle = new SistemaOrdenamientoDetalle();
          objDetalle.idSistemaManejoForestalDetalle = 0;
          objDetalle.codigoTipoDetalle = CodigoPOPAC.ACORDEON_7_4;
          objDetalle.idUsuarioRegistro = this.user.idUsuario;
          objDetalle.actividades = resp.actividades;
          objDetalle.descripcionSistema = resp.descripcionSistema;
          objDetalle.codigoProceso = CodigoPOPAC.CODIGO_PROCESO;
          objDetalle.editable = true;

          this.listMedidadProteccion.push(objDetalle);
        }
      }
    });
  }

  eliminarMedida(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idSistemaManejoForestalDetalle != 0) {
          var params = {
            idOrdenamientoProteccionDet: data.idSistemaManejoForestalDetalle,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.sistemaManejoForestalService
            .eliminarDetalle(
              params.idOrdenamientoProteccionDet,
              params.idUsuarioElimina
            )
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                this.toast.ok(response?.message);
                this.listMedidadProteccion.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listMedidadProteccion.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  guardar() {
    this.objMedidasProteccion.listOrdenamientoProteccionDet = [];
    this.objMedidasProteccion.idPlanManejo = this.idPGMF;
    this.objMedidasProteccion.idUsuarioRegistro = this.user.idUsuario;
    this.objMedidasProteccion.codTipoOrdenamiento = CodigoPOPAC.CODIGO_PROCESO;
    this.listGeneral = [];
    this.objMedidasProteccion.listOrdenamientoProteccionDet.push(
      ...this.listBloquesVetices,
      ...this.listPrimerBloque,
      ...this.listAnioOperativo
    );

    if (this.objMedidasProteccion.listOrdenamientoProteccionDet.length > 0) {
      this.listGeneral.push(this.objMedidasProteccion);
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.ordenamientoProteccionService
        .registrarOrdenamientoInterno(this.listGeneral)
        .pipe(
          finalize(() => {
            this.dialog.closeAll();
            // this.guardarMedidasProteccion();
          })
        )
        .subscribe((response: any) => {
          if (response.success == true) {
            // this.toast.ok(response.message);
            this.listaDefault();
            this.guardarMedidasProteccion();
          } else {
            this.toast.warn(response.message);
          }
        });
    } else {
      this.guardarMedidasProteccion();
    }
  }

  guardarMedidasProteccion() {
    var obj = new SistemaOrdenamientoodel();
    obj.idSistemaManejoForestal = this.idSistema;
    obj.idPlanManejo = this.idPGMF;
    obj.codigoProceso = CodigoPOPAC.CODIGO_PROCESO;
    obj.idUsuarioRegistro = this.user.idUsuario;
    obj.detalle = this.listMedidadProteccion;

    this.sistemaManejoForestalService
      .guardarSistemaManejoForestal(obj)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró ordenamiento y protección de la UMF correctamente.')
          this.getSistemaManejo();
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
