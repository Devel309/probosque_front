import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "form-medida-proteccion.component",
  templateUrl: "./form-medida-proteccion.component.html",
  styleUrls: [ "./form-medida-proteccion.component.scss" ],
})
export class FormMedidaProteccionComponent implements OnInit {
  context: any = {};

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) { }

  ngOnInit() {
    this.context = { ...this.config.data.item };
  }

  agregar() {
    if (this.validar()) {
      this.ref.close(this.context);
    }
  }

  cerrarModal() {
    this.ref.close();
  }

  validar() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.context.actividades) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Medida.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }
}
