import { HttpErrorResponse } from "@angular/common/http";
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { from } from "rxjs";
import { concatMap, finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { PlanManejoGeometriaModel } from "src/app/model/PlanManejo/PlanManejoGeometria";
import { CustomCapaModel } from "src/app/model/util/CustomCapa";
import { FileModel } from "src/app/model/util/File";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { MapApi } from "src/app/shared/mapApi";

@Component({
  selector: "tabla-superficie",
  templateUrl: "./tabla-superficie.component.html",
  styleUrls: ["./tabla-superficie.component.scss"],
})
export class TablaSuperficieComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;
  /*  @Input() isPadre!: boolean; */

  @Input() set isPadre(data: any) {
    if (!!data) {
      if (!data) {
        this.obtenerCapas();
      } else {
        this.obtenerCapasPadre();
      }
    }
  }

  @Input() idPlanManejoPadre!: number;

  @Output() listBloques: EventEmitter<any> = new EventEmitter();

  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;

  CodigoPOPAC = CodigoPOPAC;
  CodigoPMFIC = CodigoPMFIC;

  //inicio map
  view: any = null;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  //fin map
  listCoordinatesSuperficie: any[] = [];

  constructor(
    private messageService: MessageService,
    private mapApi: MapApi,
    private dialog: MatDialog,
    private usuarioService: UsuarioService,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.initializeMap();
    /*     if (!this.isPadre) {
      console.log("entra al if");
      
      this.obtenerCapas();
    } else {
      console.log("entra al else");
      this.obtenerCapasPadre();
    } */
  }

  ngAfterViewInit(): void {}

  /* onChangeFileSHPBloque(e: any) {
     this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      
       this.createTablaConAnexo(data[0].features);
     });
     e.target.value = "";
   } */

  // onChangeFileSHP(e: any) {
  //   this.mapApi.processFileSHP(e.target.files[ 0 ]).then((data: any) => {
  //     this.createTablaConAnexo(data[ 0 ].features);
  //   });
  //   e.target.value = "";
  // }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    codigoProceso: any,
    codigoSubSeccion: any,
    tipoGeometria: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      withVertice: withVertice,
      codigoProceso: codigoProceso,
      codigoSubSeccion: codigoSubSeccion,
      tipoGeometria: tipoGeometria,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }

  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);

      if (!this.isPadre) {
        if (!config.withVertice) {
          this.createTablaSuperficie(data[0].features);
        }
        if (config.withVertice) {
          this.createTablaConAnexo(data[0].features);
        }
      }
    });
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.tipoGeometria;
      t.codigoProceso = config.codigoProceso;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.codigoSubSeccionPadre = config.codigoSubSeccionPadre;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layer.annex = config.annex;
      this._layer.descripcion = config.tipoGeometria;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    this.file.idGroupLayer = config.idGroupLayer;
    this.file.descripcion = config.tipoGeometria;
    this._filesSHP.push(this.file);
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoProceso = item.codigoProceso;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.codigoSubSeccionPadre = item.codigoSubSeccionPadre;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }

  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.withVertice === true) {
      popupTemplate.content = [
        {
          type: "fields",
          fieldInfos: [
            {
              fieldName: "anexo",
              label: "Anexo",
            },
            {
              fieldName: "vertice",
              label: "Vertice",
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: "este",
              label: "Este (X)",
            },
            {
              fieldName: "norte",
              label: "Norte (Y)",
            },
          ],
        },
      ];
    }
    return popupTemplate;
  }

  createTablaSuperficie(items: any) {
    this.listCoordinatesSuperficie = [];
    items.forEach((t: any) => {
      let existe: boolean = false;

      if (this.listCoordinatesSuperficie.length > 0) {
        this.listCoordinatesSuperficie.forEach((z: any) => {
          if (z.bloque === t.properties.bloque) {
            z.superficie = Number(z.superficie) + Number(t.properties.areaha);
            existe = true;
          }
        });
        if (!existe) {
          this.listCoordinatesSuperficie.push({
            bloque: t.properties.bloque,
            superficie: t.properties.areaha,
          });
        }
      } else {
        this.listCoordinatesSuperficie.push({
          bloque: t.properties.bloque,
          superficie: t.properties.areaha,
        });
      }
    });
  }

  createTablaConAnexo(items: any) {
    this.listCoordinatesAnexo = [];
    let listMap = items.map((t: any) => {
      return {
        anexo: t.properties.bloque,
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: "",
      };
    });

    listMap.forEach((t: any) => {
      this.listCoordinatesSuperficie.forEach((s: any) => {
        if (t.anexo == s.bloque) {
          t.superficie = s.superficie;
        }
      });
    });

    this.listCoordinatesAnexo = listMap.groupBy((t: any) => t.anexo);

    this.listCoordinatesAnexo.forEach((t: any) => {
      this.listCoordinatesSuperficie.forEach((s: any) => {
        if (t.key == s.bloque) {
          t.superficie = s.superficie;
        }
      });
    });

    this.listBloques.emit(this.listCoordinatesAnexo);
  }

  onGuardarLayer() {
    let fileUpload: any = [];

    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.usuarioService.idUsuario,
          codigo: "37",
          codigoTipoPGMF: this.CodigoPOPAC.CODIGO_PROCESO,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.cleanLayers();
          this.obtenerCapas();
        })
      )
      .subscribe(
        (result) => {
          this.SuccessMensaje(result.message);
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error.");
        }
      );
  }

  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }

  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: this.CodigoPOPAC.CODIGO_PROCESO,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }

  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let property: any = null;
      if (t3.codigoSubSeccionPadre) {
        property = t3.codigoSubSeccionPadre;
      } else {
        if (t3.attributes.length == 1) {
          property = t3.attributes[0].properties.ET_ID;
        }
      }
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: t3.geometryType,
        codigoSeccion: t3.codigoProceso,
        codigoSubSeccion: t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        propiedad: property,
        idUsuarioRegistro: this.usuarioService.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }

  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }

  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }

  obtenerCapas() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.CodigoPOPAC.CODIGO_PROCESO,
      codigoSubSeccion: this.CodigoPOPAC.CODIGO_PROCESO + "OPUMFSUB",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = t.idArchivo;
              this._layer.idLayer = this.mapApi.Guid2.newGuid;
              this._layer.inServer = true;
              this._layer.service = false;
              this._layer.nombre = t.nombreCapa;
              this._layer.groupId = groupId;
              this._layer.color = t.colorCapa;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layers.push(this._layer);
              let geoJson = this.mapApi.getGeoJson(
                this._layer.idLayer,
                groupId,
                item
              );
              this.createLayer(geoJson);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  obtenerCapasPadre() {
    let item = {
      idPlanManejo: this.idPlanManejoPadre,
      codigoSeccion: this.CodigoPMFIC.CODIGO_PROCESO,
      codigoSubSeccion: this.CodigoPMFIC.CODIGO_PROCESO + "OPUMFSUB",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              this.crearFile(t.idArchivo, t.tipoGeometria, t.codigoSubSeccion);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  crearFile(id: any, tipoGeometria: any, codigoSubSeccionPadre: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          let config = {
            idGroupLayer: this.mapApi.Guid2.newGuid,
            inServer: false,
            service: false,
            validate: false,
            annex: false,
            withVertice: false,
            codigoProceso: this.CodigoPOPAC.CODIGO_PROCESO,
            codigoSubSeccion: this.CodigoPOPAC.CODIGO_PROCESO + "OPUMFSUB",
            tipoGeometria: tipoGeometria,
            codigoSubSeccionPadre: codigoSubSeccionPadre,
          };

          const binary_string = window.atob(result.data.file);
          const len = binary_string.length;
          const bytes = new Uint8Array(len);
          for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
          }
          const file: File = new File([bytes.buffer], result.data.nombre, {
            type: "application/x-zip-compressed",
          });

          this.processFile(file, config);
        }
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }

  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          "application/octet-stream"
        );
      }
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      };
    });
  }

  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: "¿Está seguro de eliminar este archivo?",
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHP",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                this.SuccessMensaje("El archivo se eliminó correctamente.");
              } else {
                this.ErrorMensaje("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.ErrorMensaje("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
      }
    }
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.usuarioService.idUsuario
    );
  }

  onChangeObservacion() {
    this.listBloques.emit(this.listCoordinatesAnexo);
  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje +=
        "(*) Agregue archivo VERTICES PARCELAS DE CORTA ANEXO \n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
