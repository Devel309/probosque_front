import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";

@Component({
  selector: "app-modal-confirmation-guardar",
  templateUrl: "./modal-confirmation-guardar.component.html",
  styleUrls: ["./modal-confirmation-guardar.component.scss"],
})
export class ModalConfirmationGuardarComponent implements OnInit {
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  agregar() {
    this.ref.close("YES");
  }

  cerrarModal() {
    this.ref.close();
  }
}
