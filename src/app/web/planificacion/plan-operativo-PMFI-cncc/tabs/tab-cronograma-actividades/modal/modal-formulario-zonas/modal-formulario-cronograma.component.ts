import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ButtonsCreateQuery } from 'src/app/features/state/buttons-create.query';
import { ButtonsCreateStore } from 'src/app/features/state/buttons-create.store';
import { CronogrmaActividadesService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service';

@Component({
  selector: 'app-modal-formulario-cronograma',
  templateUrl: './modal-formulario-cronograma.component.html',
  styleUrls: ['./modal-formulario-cronograma.component.scss'],
})
export class ModalFormularioCronogramaComponent implements OnInit {
  context: any = {
    actividad: '',
    tipoActividad: '',
  };
  cmbTipoActividad: any = [];
  cmbAnios: any = [];
  isDisabled = false;
  fecha: any[] = [];
  edit: any[] = [];
  editar: boolean = false
  isDisabledActividad: boolean =  false;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private toast: ToastService,
  ) {}

  ngOnInit(): void {

    this.cmbTipoActividad = this.config.data.cmbTipoActividad;
    if (this.config.data.type == 'E') {
      this.editar = true;
      this.cmbTipoActividad = this.config.data.cmbTipoActividad;
      this.cmbAnios = this.config.data.cmbAnios;
      if(this.config.data.data.actividad == "Otros (especificar)"){
        this.isDisabledActividad = true;
      }
      this.context = this.config.data.data;
      this.config.data.data.aniosMeses.forEach((element: any) => {
        this.fecha.push(element.aniosMeses);
      });
    } else {
      this.isDisabledActividad = true;
      this.editar = false;
      this.isDisabled = true;
    }
  }

  onChange(event: any) {
    if (event.itemValue != undefined) {
      this.context.aniosMeses.forEach((element: any, index: number) => {
        if (
          event.itemValue != undefined &&
          element.aniosMeses == event.itemValue
        ) {
          var params = {
            idCronogramaActividadDetalle: element.id,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividadDetalle(params)
            .subscribe((response) => {
              console.log(response);
            });

          this.context.aniosMeses.splice(index, 1);
        }
      });
    } else if (event.itemValue == undefined) {
      var array = [];
      for (var i = 0; i < this.context.aniosMeses.length; i++) {
        var igual = false;
        for (var j = 0; j < event.value.length && !igual; j++) {
          if (this.context.aniosMeses[i].aniosMeses == event.value[j])
            igual = true;
        }
        if (!igual) array.push(this.context.aniosMeses[i]);
      }

      array.forEach((response: any, index: number) => {
        var params = {
          idCronogramaActividadDetalle: response.id,
          idUsuarioElimina: this.user.idUsuario,
        };

        this.cronogrmaActividadesService
          .eliminarCronogramaActividadDetalle(params)
          .subscribe((response) => {
            console.log(response);
          });
      });
    }
  }

  agregar = () => {
    if (this.config.data.type == 'C') {
      if (this.context.tipoActividad === '') {
        this.toast.warn('Tipo de Actividad, es obligatorio.');
      } else if (this.context.actividad === '') {
        this.toast.warn('Actividad, es obligatorio.');
      } else this.ref.close(this.context);
    } else if (this.config.data.type == 'E') {
      if (this.context.aniosMeses.length == 0) {
        this.context.aniosMeses = this.fecha;
        this.ref.close(this.context);
      } else if (this.context.aniosMeses.length != 0) {
        this.context.aniosMeses.forEach((element: any) => {
          this.fecha.forEach((x: any, index: number) => {
            if (element.aniosMeses == x) {
              this.fecha.splice(index, 1);
            }
          });
        });
        this.context.aniosMeses = this.fecha;
        if (this.fecha.length != 0) {
          this.ref.close(this.context);
        } else {
          this.ref.close((this.context = null));
        }
      }
    }
  };

  cerrarModal() {
    this.ref.close();
  }
}
