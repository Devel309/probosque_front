import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { SistemaManejoForestalService, UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { ToastService } from "@shared";
import { finalize } from "rxjs/operators";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { ModalFormularioCronogramaComponent } from "./modal/modal-formulario-zonas/modal-formulario-cronograma.component";

import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoPOPAC } from "src/app/model/util/POPAC/POPAC";
import { ActividadesSilviculturalesService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/actividadesSilviculturales.service";
import { ModalConfirmationGuardarComponent } from "./modal/modal-confirmation-guardar/modal-confirmation-guardar.component";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";

@Component({
  selector: "app-tab-cronograma-actividades",
  templateUrl: "./tab-cronograma-actividades.component.html",
  styleUrls: ["./tab-cronograma-actividades.component.scss"],
})
export class TabCronogramaActividadesComponent implements OnInit {
  @Input() idPGMF!: number;
  @Output() public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output() public regresar = new EventEmitter();

  CodigoPOPAC = CodigoPOPAC;

  ref!: DynamicDialogRef;
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  context: Modelo = new Modelo();
  lstMaderable: Modelo[] = [];
  lstNoMaderable: Modelo[] = [];
  lstOtros: Modelo[] = [];
  pendiente: boolean = false;
  cmbTipoActividad: any[] = [
    { code: 0, name: "Seleccionar" },
    { code: "MAD", name: "Aprovechamiento Maderable" },
    { code: "NMAD", name: "Aprovechamiento No Maderable" },
    { code: "AAEOTRO", name: "Otros" },
  ];
  cmbAnios: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [
        { label: "Año 2 - Mes 1", value: "2-1" },
        { label: "Año 2 - Mes 2", value: "2-2" },
        { label: "Año 2 - Mes 3", value: "2-3" },
        { label: "Año 2 - Mes 4", value: "2-4" },
        { label: "Año 2 - Mes 5", value: "2-5" },
        { label: "Año 2 - Mes 6", value: "2-6" },
        { label: "Año 2 - Mes 7", value: "2-7" },
        { label: "Año 2 - Mes 8", value: "2-8" },
        { label: "Año 2 - Mes 9", value: "2-9" },
        { label: "Año 2 - Mes 10", value: "2-10" },
        { label: "Año 2 - Mes 11", value: "2-11" },
        { label: "Año 2 - Mes 12", value: "2-12" },
      ],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [
        { label: "Año 3 - Mes 1", value: "3-1" },
        { label: "Año 3 - Mes 2", value: "3-2" },
        { label: "Año 3 - Mes 3", value: "3-3" },
        { label: "Año 3 - Mes 4", value: "3-4" },
        { label: "Año 3 - Mes 5", value: "3-5" },
        { label: "Año 3 - Mes 6", value: "3-6" },
        { label: "Año 3 - Mes 7", value: "3-7" },
        { label: "Año 3 - Mes 8", value: "3-8" },
        { label: "Año 3 - Mes 9", value: "3-9" },
        { label: "Año 3 - Mes 10", value: "3-10" },
        { label: "Año 3 - Mes 11", value: "3-11" },
        { label: "Año 3 - Mes 12", value: "3-12" },
      ],
    },
  ];
  verAnio1: boolean = false;
  verAnio2: boolean = false;
  verAnio3: boolean = false;

  isSubmittingGenerar$ = this.generarQuery.selectSubmitting();
  isLoadingGenerar$ = this.generarQuery.selectLoading();

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private actividadesSilviculturalesService: ActividadesSilviculturalesService,
    private toast: ToastService,
    private generarStore: ButtonsCreateStore,
    private generarQuery: ButtonsCreateQuery
  ) {}

  ngOnInit() {
    this.listarCronogramaActividad();
  }

  obtenerActividades() {
    let params = {
      idPlanManejo: this.idPGMF,
      idTipo: CodigoPOPAC.CODIGO_PROCESO,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (!!response.data) {
          if (response.data.codActividad != null) {
            if (response.data.detalle.length != 0) {
              let listado = response.data.detalle;
              listado.forEach((x: any, index: number) => {
                if (x.observacionDetalle == "MAD") {
                  this.lstMaderable.push({ ...x, actividad: x.actividad });
                  this.guardarTipoActividadPrincipal(x);
                } else if (x.observacionDetalle == "NMAD") {
                  this.lstNoMaderable.push({ ...x, actividad: x.actividad });
                  this.guardarTipoActividadPrincipal(x);
                } else if (x.observacionDetalle == "AAEOTRO") {
                  this.lstOtros.push({ ...x, actividad: x.actividad });
                  this.guardarTipoActividadPrincipal(x);
                } else if (index === listado.length - 1) {
                  this.listarCronogramaActividad();
                }
              });
            }
          }
        }
      });
  }
  // listar todas las actividades
  listarCronogramaActividad() {
    this.generarStore.submit();
    this.lstMaderable = [];
    this.lstNoMaderable = [];
    this.lstOtros = [];
    var params = {
      codigoProceso: "POPAC",
      idCronogramaActividad: null,
      idPlanManejo: this.idPGMF,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.isSuccess == true) {
          this.generarStore.submitSuccess();
          if (response.data.length != 0) {
            if (response.data[0].codigoProceso == "PMFIC") {
              this.openModalConfirmationGuardar();
            }
            response.data.forEach((x: any) => {
              if (x.actividad == "NO HAY DATA EN EL PLAN PADRE") {
                this.pendiente = true;
              }

              if (x.codigoProceso == "POPAC" && x.idPlanManejo == this.idPGMF) {
                if (
                  x.idCronogramaActividad != null &&
                  x.codigoActividad == "MAD"
                ) {
                  this.lstMaderable.push({
                    ...x,
                    tipoActividad: "Aprovechamiento Maderable",
                  });
                } else if (
                  x.idCronogramaActividad != null &&
                  x.codigoActividad == "NMAD"
                ) {
                  this.lstNoMaderable.push({
                    ...x,
                    tipoActividad: "Aprovechamiento No Maderable",
                  });
                } else if (
                  x.idCronogramaActividad != null &&
                  x.codigoActividad == "AAEOTRO"
                ) {
                  this.lstOtros.push({ ...x, tipoActividad: "Otros" });
                }
              }
            });
            this.marcar();
          }
        } else {
          this.generarStore.submitError("");
        }
      });
  }

  marcar() {
    var aniosMeses: any[] = [];
    this.lstMaderable.forEach((x: any, index) => {
      aniosMeses = [];
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });
      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstMaderable[index] = this.context;
    });

    this.lstNoMaderable.forEach((x: any, index) => {
      aniosMeses = [];
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });
      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstNoMaderable[index] = this.context;
    });

    this.lstOtros.forEach((x: any, index) => {
      aniosMeses = [];
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });
      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstOtros[index] = this.context;
    });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: resp.tipoActividad,
      codigoProceso: "POPAC",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.toast.ok("Se actualizó el cronograma de actividades.");
          this.listarCronogramaActividad();
        } else {
          this.toast.warn("Ocurrió un problema, intente nuevamente");
        }
      });
  }

  guardarTipoActividadPrincipal(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: resp.observacionDetalle,
      codigoProceso: "POPAC",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        console.log(response);
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.toast.ok("Se actualizó el cronograma de actividades.");
          this.listarCronogramaActividad();
        } else {
          this.toast.warn("Ocurrió un problema, intente nuevamente");
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    this.ref = this.dialogService.open(ModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbTipoActividad: this.cmbTipoActividad,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      if (this.context.tipoActividad == "MAD") {
        this.lstMaderable.push(this.context);
      } else if (this.context.tipoActividad == "NMAD") {
        this.lstNoMaderable.push(this.context);
      }
    }
    this.verModalMantenimiento = false;
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.dialog.open(LoadingComponent, { disableClose: true });
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.toast.ok(
                  "Se eliminó  la actividad del cronograma correctamente."
                );
                this.listarCronogramaActividad();
              } else {
                this.toast.warn("Ocurrió un problema, intente nuevamente");
              }
            });
        } else {
          if (data.tipoActividad == "MAD") {
            this.lstMaderable.splice(
              this.lstMaderable.findIndex((x) => x.id == data.id),
              1
            );
          } else if (data.tipoActividad == "NMAD") {
            this.lstNoMaderable.splice(
              this.lstNoMaderable.findIndex((x) => x.id == data.id),
              1
            );
          }
        }
      },
      reject: () => {},
    });
  }

  openModalConfirmationGuardar() {
    this.ref = this.dialogService.open(ModalConfirmationGuardarComponent, {
      header: "",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: this.idPGMF,
        type: "POPAC",
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp == "YES") {
        
        let params = {
          actividad: "",
          anio: null,
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: this.user.idUsuario,
          importe: null,
          codigoActividad: null,
          codigoProceso: "POPAC",
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cronogrmaActividadesService
          .registrarCronogramaActividades(params)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((response: any) => {
            if (response.isSuccess) {
              this.toast.ok("Se actualizó el cronograma de actividades.");
              this.listarCronogramaActividad();
            } else {
              this.toast.warn("Ocurrió un problema, intente nuevamente");
            }
          });
      }
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}

export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.tipoActividad = data.tipoActividad);
      return;
    }
  }
  tipoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m1: boolean = false;
  a1m2: boolean = false;
  a1m3: boolean = false;
  a1m4: boolean = false;
  a1m5: boolean = false;
  a1m6: boolean = false;
  a1m7: boolean = false;
  a1m8: boolean = false;
  a1m9: boolean = false;
  a1m10: boolean = false;
  a1m11: boolean = false;
  a1m12: boolean = false;

  a2m1: boolean = false;
  a2m2: boolean = false;
  a2m3: boolean = false;
  a2m4: boolean = false;
  a2m5: boolean = false;
  a2m6: boolean = false;
  a2m7: boolean = false;
  a2m8: boolean = false;
  a2m9: boolean = false;
  a2m10: boolean = false;
  a2m11: boolean = false;
  a2m12: boolean = false;

  a3m1: boolean = false;
  a3m2: boolean = false;
  a3m3: boolean = false;
  a3m4: boolean = false;
  a3m5: boolean = false;
  a3m6: boolean = false;
  a3m7: boolean = false;
  a3m8: boolean = false;
  a3m9: boolean = false;
  a3m10: boolean = false;
  a3m11: boolean = false;
  a3m12: boolean = false;

  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-1") {
        this.a1m1 = x.aniosMeses.indexOf("1-1") > -1;
      } else if (x.aniosMeses == "1-2") {
        this.a1m2 = x.aniosMeses.indexOf("1-2") > -1;
      } else if (x.aniosMeses == "1-3") {
        this.a1m3 = x.aniosMeses.indexOf("1-3") > -1;
      } else if (x.aniosMeses == "1-4") {
        this.a1m4 = x.aniosMeses.indexOf("1-4") > -1;
      } else if (x.aniosMeses == "1-5") {
        this.a1m5 = x.aniosMeses.indexOf("1-5") > -1;
      } else if (x.aniosMeses == "1-6") {
        this.a1m6 = x.aniosMeses.indexOf("1-6") > -1;
      } else if (x.aniosMeses == "1-7") {
        this.a1m7 = x.aniosMeses.indexOf("1-7") > -1;
      } else if (x.aniosMeses == "1-8") {
        this.a1m8 = x.aniosMeses.indexOf("1-8") > -1;
      } else if (x.aniosMeses == "1-9") {
        this.a1m9 = x.aniosMeses.indexOf("1-9") > -1;
      } else if (x.aniosMeses == "1-10") {
        this.a1m10 = x.aniosMeses.indexOf("1-10") > -1;
      } else if (x.aniosMeses == "1-11") {
        this.a1m11 = x.aniosMeses.indexOf("1-11") > -1;
      } else if (x.aniosMeses == "1-12") {
        this.a1m12 = x.aniosMeses.indexOf("1-12") > -1;
      } else if (x.aniosMeses == "2-1") {
        this.a2m1 = x.aniosMeses.indexOf("2-1") > -1;
      } else if (x.aniosMeses == "2-2") {
        this.a2m2 = x.aniosMeses.indexOf("2-2") > -1;
      } else if (x.aniosMeses == "2-3") {
        this.a2m3 = x.aniosMeses.indexOf("2-3") > -1;
      } else if (x.aniosMeses == "2-4") {
        this.a2m4 = x.aniosMeses.indexOf("2-4") > -1;
      } else if (x.aniosMeses == "2-5") {
        this.a2m5 = x.aniosMeses.indexOf("2-5") > -1;
      } else if (x.aniosMeses == "2-6") {
        this.a2m6 = x.aniosMeses.indexOf("2-6") > -1;
      } else if (x.aniosMeses == "2-7") {
        this.a2m7 = x.aniosMeses.indexOf("2-7") > -1;
      } else if (x.aniosMeses == "2-8") {
        this.a2m8 = x.aniosMeses.indexOf("2-8") > -1;
      } else if (x.aniosMeses == "2-9") {
        this.a2m9 = x.aniosMeses.indexOf("2-9") > -1;
      } else if (x.aniosMeses == "2-10") {
        this.a2m10 = x.aniosMeses.indexOf("2-10") > -1;
      } else if (x.aniosMeses == "2-11") {
        this.a2m11 = x.aniosMeses.indexOf("2-11") > -1;
      } else if (x.aniosMeses == "2-12") {
        this.a2m12 = x.aniosMeses.indexOf("2-12") > -1;
      } else if (x.aniosMeses == "3-1") {
        this.a3m1 = x.aniosMeses.indexOf("3-1") > -1;
      } else if (x.aniosMeses == "3-2") {
        this.a3m2 = x.aniosMeses.indexOf("3-2") > -1;
      } else if (x.aniosMeses == "3-3") {
        this.a3m3 = x.aniosMeses.indexOf("3-3") > -1;
      } else if (x.aniosMeses == "3-4") {
        this.a3m4 = x.aniosMeses.indexOf("3-4") > -1;
      } else if (x.aniosMeses == "3-5") {
        this.a3m5 = x.aniosMeses.indexOf("3-5") > -1;
      } else if (x.aniosMeses == "3-6") {
        this.a3m6 = x.aniosMeses.indexOf("3-6") > -1;
      } else if (x.aniosMeses == "3-7") {
        this.a3m7 = x.aniosMeses.indexOf("3-7") > -1;
      } else if (x.aniosMeses == "3-8") {
        this.a3m8 = x.aniosMeses.indexOf("3-8") > -1;
      } else if (x.aniosMeses == "3-9") {
        this.a3m9 = x.aniosMeses.indexOf("3-9") > -1;
      } else if (x.aniosMeses == "3-10") {
        this.a3m10 = x.aniosMeses.indexOf("3-10") > -1;
      } else if (x.aniosMeses == "3-11") {
        this.a3m11 = x.aniosMeses.indexOf("3-11") > -1;
      } else if (x.aniosMeses == "3-12") {
        this.a3m12 = x.aniosMeses.indexOf("3-12") > -1;
      }
    });
  }
}
