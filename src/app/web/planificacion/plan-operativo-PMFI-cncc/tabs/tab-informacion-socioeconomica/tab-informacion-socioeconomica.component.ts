import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { element } from "protractor";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  InformacionBasicaDetalleModelPMFIC,
  InformacionBasicaModelPMFIC,
  ListInformacionBasicaDetSubPMFIC,
} from "src/app/model/InformacionSocieconomicaPMFIC";
import { InformacionBasicaSOPCMervice } from "src/app/service/plan-operativo-concesion-maderable/informacion-basica.service";
import { ModalFormularioAntecedentesComponent } from "../../modal/modal-formulario-antecedentes/modal-formulario-antecedentes.component";
import { ModalFormularioInfraestructuraComponent } from "../../modal/modal-formulario-infraestructura/modal-formulario-infraestructura.component";
import { CodigoPOPAC } from "../../../../../model/util/POPAC/POPAC";
import { ModalConfirmationGuardarSocioeconomicoComponent } from "./modal-confirmation-guardar/modal-confirmation-guardar.component";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ModalFormularioOtrosComponent } from "./modal-formulario-otros/modal-formulario-otros.component";
import { ModalFormularioSocieconomicaComponent } from "./modal-formulario-socieconomica/modal-formulario-socieconomica.component";

@Component({
  selector: "app-tab-informacion-socioeconomica",
  templateUrl: "./tab-informacion-socioeconomica.component.html",
})
export class TabInformacionSocioeconomicaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  ref: DynamicDialogRef = new DynamicDialogRef();
  pendienteCaracterizacion: boolean = false;
  pendienteInfraestructura: boolean = false;
  pendienteAntecedentes: boolean = false;
  notData: boolean = false;

  especies = [];
  listCaracterizacionComunidad: InformacionBasicaModelPMFIC[] = [];
  listInfraestructuraServicios: InformacionBasicaModelPMFIC[] = [];
  listAntecentesUsoIdentificacionConflictos: InformacionBasicaModelPMFIC[] = [];
  cont: number = 0;

  CodigoPOPAC = CodigoPOPAC;
  conCodigo: boolean = false;
  isSubmitting$ = this.query.selectSubmitting();

  constructor(
    public dialogService: DialogService,
    private informacionBasicaService: InformacionBasicaSOPCMervice,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private toast: ToastService,
    private createStore: ButtonsCreateStore,
    private query: ButtonsCreateQuery
  ) {}

  ngOnInit() {
    this.listarInfCaracterizacionComunidad();
    this.listarInfInfraestructuraServicios();
    this.listarInfAntecentesUsoIdentificacionConflictos();
  }

  crearOtrasActividadesCaracterizacionComunidad() {
    const obj = new InformacionBasicaDetalleModelPMFIC();

    const array = this.listCaracterizacionComunidad.filter(
      (data: any) => data.comunidad == "Otros (especificar)"
    )[0].listInformacionBasicaDet;

    array.push(obj);

    // listInformacionBasicaDet.push(obj)
  }

  crearOtraInfraestructura() {
    this.toast.ok("Se agregó una fila al final de la tabla correctamente.");
    const obj = new InformacionBasicaDetalleModelPMFIC();

    this.listInfraestructuraServicios[0].listInformacionBasicaDet.push(obj);
  }

  modalFormAntecedentes(
    mensaje: string,
    antecedentes?: any,
    edit?: any,
    index?: any
  ) {
    this.ref = this.dialogService.open(ModalFormularioAntecedentesComponent, {
      header: mensaje,
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        item: antecedentes,
        edit: edit,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (edit == "true") {
          this.listAntecentesUsoIdentificacionConflictos[index] = resp;
          antecedentes.comunidad = resp.comunidad;
        } else {
          resp.codSubInfBasica = "PMFICISAUEC";
          this.listAntecentesUsoIdentificacionConflictos.push(resp);
        }
      }
    });
  }

  eliminarItemCaracterizacion(event: any, item: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasica == 0) {
          this.toast.ok("Se eliminó el registró correctamente.");
          this.listCaracterizacionComunidad.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: item.idInfBasica,
              codInfBasicaDet: "",
              idInfBasicaDet: 0,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.listCaracterizacionComunidad.splice(index, 1);
            });
        }
      },
      reject: () => {},
    });
  }
  eliminarEspecie(check: any, indexActividad: number, indexEspecie: number) {
    this.listAntecentesUsoIdentificacionConflictos[
      indexActividad
    ].listInformacionBasicaDet.splice(indexEspecie, 1);
  }

  eliminarInfraestructura(
    event: any,
    item: ListInformacionBasicaDetSubPMFIC,
    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasicaDet == 0) {
          this.toast.ok("Se eliminó el registro correctamente.");
          this.listInfraestructuraServicios[0].listInformacionBasicaDet.splice(
            index,
            1
          );
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: "",
              idInfBasicaDet: item.idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.listInfraestructuraServicios[0].listInformacionBasicaDet.splice(
                index,
                1
              );
            });
        }
      },
      reject: () => {},
    });
  }

  modalFormInfraestructura(mensaje: string, infraestructura: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioInfraestructuraComponent,
      {
        header: mensaje,
        width: "50%",
        contentStyle: { "max-height": "500px", overflow: "auto" },
        baseZIndex: 10000,
        data: {
          item: infraestructura,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (edit == "true") {
          infraestructura.nombre = resp.nombre;
          infraestructura.este = resp.este;
          infraestructura.norte = resp.norte;
          infraestructura.observaciones = resp.observaciones;
          infraestructura.editarFormInfraestructura = true;
        } else {
          // this.infraestructura.push(resp);
        }
      }
    });
  }

  eliminarAntecedentes(event: any, item: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (item.idInfBasica == 0) {
          this.toast.ok("Se eliminó el registro correctamente.");
          this.listAntecentesUsoIdentificacionConflictos.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionBasicaService
            .eliminarInformacionBasica({
              idInfBasica: item.idInfBasica,
              codInfBasicaDet: "",
              idInfBasicaDet: 0,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              this.toast.ok(response.message);
              this.listAntecentesUsoIdentificacionConflictos.splice(index, 1);
            });
        }
      },
      reject: () => {},
    });
  }

  listarInfCaracterizacionComunidad() {
    this.conCodigo = false;
    this.listCaracterizacionComunidad = [];
    this.notData = false;
    var params = {
      codigoTipoInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: CodigoPOPAC.ACORDEON_6_1,
    };

    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length) {
          response.data[0].map((element: any) => {
            if (element.nombreHojaCatastral == "NO HAY DATA EN EL PLAN PADRE") {
              this.pendienteCaracterizacion = true;
            }
          });
          response.data[0].forEach((item: any, index: number) => {
            this.conCodigo = false;
            if (item.codInfBasica == "PMFIC") {
              this.conCodigo = true;
            }
          });
          if (this.conCodigo == true) {
            this.openModalConfirmationGuardar();
          }
          if (this.pendienteCaracterizacion == false) {
            response.data[0].map((element: any) => {
              if (element.codInfBasica == CodigoPOPAC.CODIGO_PROCESO) {
                this.conCodigo = true;
                if (element.listInformacionBasicaDet[0].actividad == "") {
                  this.notData = true;
                }
                const obj = new InformacionBasicaModelPMFIC(element);
                obj.codInfBasica = CodigoPOPAC.CODIGO_PROCESO;
                obj.codNombreInfBasica = CodigoPOPAC.ACORDEON_6_1;
                obj.codSubInfBasica = CodigoPOPAC.ACORDEON_6_1;
                obj.idPlanManejo = this.idPlanManejo;
                obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                  (data: any) => {
                    const obj = new InformacionBasicaDetalleModelPMFIC(data);
                    obj.codInfBasicaDet = CodigoPOPAC.ACORDEON_6_1;
                    obj.solucion = data.solucion == "S";
                    return obj;
                  }
                );
                this.listCaracterizacionComunidad.push(obj);
              }
            });
          }
          //  this.listarInfCaracterizacionComunidadPOPAC();
        }
      });
  }

  /* listarInfCaracterizacionComunidadPOPAC() {

    this.listCaracterizacionComunidad = [];
    var params = {
      codigoTipoInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: CodigoPOPAC.ACORDEON_6_1,
    };

    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data[0].map((element: any) => {
            if(element.codInfBasica == CodigoPOPAC.CODIGO_PROCESO){
              const obj2 = new InformacionBasicaModelPMFIC(element);
              obj2.listInformacionBasicaDet =
                element.listInformacionBasicaDet.map((data: any) => {
                  const obj = new InformacionBasicaDetalleModelPMFIC(data);
                  obj.solucion = data.solucion == 'S';
                  return obj;
                });
              this.listCaracterizacionComunidad.push(obj2);
            }
          });
        }
      });
  } */

  listarInfInfraestructuraServicios() {
    this.listInfraestructuraServicios = [];
    var params = {
      codigoTipoInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: CodigoPOPAC.ACORDEON_6_3,
    };
    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length) {
          response.data[0].map((element: any) => {
            if (element.nombreHojaCatastral == "NO HAY DATA EN EL PLAN PADRE") {
              this.pendienteInfraestructura = true;
            }
          });
          if (this.pendienteCaracterizacion == false) {
            response.data[0].forEach((element: any) => {
              if (element.codInfBasica == CodigoPOPAC.CODIGO_PROCESO) {
                const obj = new InformacionBasicaModelPMFIC(element);
                obj.codInfBasica = CodigoPOPAC.CODIGO_PROCESO;
                obj.codNombreInfBasica = CodigoPOPAC.ACORDEON_6_3;
                obj.codSubInfBasica = CodigoPOPAC.ACORDEON_6_3;
                obj.idPlanManejo = this.idPlanManejo;
                obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                  (data: any) => {
                    const obj = new InformacionBasicaDetalleModelPMFIC(data);
                    obj.codInfBasicaDet = CodigoPOPAC.ACORDEON_6_3;
                    obj.solucion = data.solucion == "S";
                    return obj;
                  }
                );
                this.listInfraestructuraServicios.push(obj);
              }
            });
          }
        }
      });
  }

  /*  listarInfInfraestructuraServiciosPOPAC() {
    this.listInfraestructuraServicios = [];
    var params = {
      codigoTipoInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: CodigoPOPAC.ACORDEON_6_3,
    };
    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data[0].forEach((element: any) => {

            if(element.codInfBasica == CodigoPOPAC.CODIGO_PROCESO){
              const obj2 = new InformacionBasicaModelPMFIC(element);
              obj2.listInformacionBasicaDet =
                element.listInformacionBasicaDet.map((data: any) => {
                  const obj = new InformacionBasicaDetalleModelPMFIC(data);
                  obj.solucion = data.solucion == 'S';
                  return obj;
                });
              this.listInfraestructuraServicios.push(obj2);
            }
          });
        }
      });
  } */

  listarInfAntecentesUsoIdentificacionConflictos() {
    this.listAntecentesUsoIdentificacionConflictos = [];
    var params = {
      codigoTipoInfBasica: CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo: this.idPlanManejo,
      subCodigoTipoInfBasica: CodigoPOPAC.ACORDEON_6_2,
    };
    this.informacionBasicaService
      .listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length) {
          response.data[0].map((element: any) => {
            if (element.nombreHojaCatastral == "NO HAY DATA EN EL PLAN PADRE") {
              this.pendienteAntecedentes = true;
            }
          });
          if (this.pendienteCaracterizacion == false) {
            response.data[0].map((element: any) => {
              if (element.codInfBasica == CodigoPOPAC.CODIGO_PROCESO) {
                const obj = new InformacionBasicaModelPMFIC(element);
                obj.codInfBasica = CodigoPOPAC.CODIGO_PROCESO;
                obj.codNombreInfBasica = CodigoPOPAC.ACORDEON_6_2;
                obj.codSubInfBasica = CodigoPOPAC.ACORDEON_6_2;
                obj.idPlanManejo = this.idPlanManejo;
                if (
                  !!element.listInformacionBasicaDet &&
                  element.listInformacionBasicaDet.length > 0
                ) {
                  obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
                    (data: any) => {
                      const item = new InformacionBasicaDetalleModelPMFIC(data);
                      item.codInfBasicaDet = CodigoPOPAC.ACORDEON_6_2;
                      item.solucion = true;
                      return item;
                    }
                  );
                }
                this.listAntecentesUsoIdentificacionConflictos.push(obj);
              }
            });
          }
        }
      });
  }

  /*   listarInfAntecentesUsoIdentificacionConflictosPOPAC() {
    this.listAntecentesUsoIdentificacionConflictos = [];
    var params = {
      codigoTipoInfBasica    : CodigoPOPAC.CODIGO_PROCESO,
      idPlanManejo           : this.idPlanManejo,
      subCodigoTipoInfBasica : CodigoPOPAC.ACORDEON_6_2
    };
    this.informacionBasicaService.listarInformacionSocioeconomica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data[0].map((element: any) => {
            if(element.codInfBasica == CodigoPOPAC.CODIGO_PROCESO) {
              const obj2 = new InformacionBasicaModelPMFIC(element);
              if (!!element.listInformacionBasicaDet && element.listInformacionBasicaDet.length > 0) {
                obj2.listInformacionBasicaDet = element.listInformacionBasicaDet.map((data: any) => {
                  const item = new InformacionBasicaDetalleModelPMFIC(data);
                  item.solucion = true;
                  return item;
                });
              }
              this.listAntecentesUsoIdentificacionConflictos.push(obj2);
            }
          });
        }
      });
  }
 */
  registratInformacionSocioeconomica() {
    let contentArray: any = [];
    this.createStore.submit();

    if (this.listCaracterizacionComunidad.length > 0) {
      this.listCaracterizacionComunidad.map((element: any) => {
        const obj = new InformacionBasicaModelPMFIC(element);
        obj.codNombreInfBasica = CodigoPOPAC.ACORDEON_6_1;
        obj.idPlanManejo = this.idPlanManejo;
        obj.idUsuarioRegistro = this.user.idUsuario;
        if (!!element.listInformacionBasicaDet) {
          obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
            (element: any) => {
              const obj = new InformacionBasicaDetalleModelPMFIC(element);
              obj.codInfBasicaDet = CodigoPOPAC.ACORDEON_6_1;
              obj.idPlanManejo = this.idPlanManejo;
              obj.idUsuarioRegistro = this.user.idUsuario;
              obj.solucion = element.solucion ? "S" : "N";
              return obj;
            }
          );
          obj.codNombreInfBasica = obj.listInformacionBasicaDet[0]
            .codSubInfBasicaDet
            ? obj.listInformacionBasicaDet[0].codSubInfBasicaDet
            : "OT";
          if (obj.listInformacionBasicaDet[0].codSubInfBasicaDet == "") {
            obj.listInformacionBasicaDet[0].codSubInfBasicaDet = "OT";
          }
        }
        contentArray.push(obj);
      });
    }

    if (this.listInfraestructuraServicios.length > 0) {
      this.listInfraestructuraServicios.map((element: any) => {
        const obj = new InformacionBasicaModelPMFIC(element);
        obj.codInfBasica = CodigoPOPAC.CODIGO_PROCESO;
        obj.codNombreInfBasica = CodigoPOPAC.ACORDEON_6_3;
        obj.idPlanManejo = this.idPlanManejo;
        obj.idUsuarioRegistro = this.user.idUsuario;
        if (!!element.listInformacionBasicaDet) {
          obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
            (element: any) => {
              const obj = new InformacionBasicaDetalleModelPMFIC(element);
              obj.codInfBasicaDet = CodigoPOPAC.ACORDEON_6_3;
              obj.idPlanManejo = this.idPlanManejo;
              obj.idUsuarioRegistro = this.user.idUsuario;
              obj.solucion = element.solucion ? "S" : "N";
              obj.coordenadaEsteIni = parseFloat(element.coordenadaEsteIni);
              obj.coordenadaNorteIni = parseFloat(element.coordenadaNorteIni);
              return obj;
            }
          );
        }
        contentArray.push(obj);
      });
    }

    if (this.listAntecentesUsoIdentificacionConflictos.length > 0) {
      this.listAntecentesUsoIdentificacionConflictos.map((element: any) => {
        const obj = new InformacionBasicaModelPMFIC(element);
        obj.codInfBasica = CodigoPOPAC.CODIGO_PROCESO;
        obj.codNombreInfBasica = CodigoPOPAC.ACORDEON_6_2;
        obj.idPlanManejo = this.idPlanManejo;
        obj.idUsuarioRegistro = this.user.idUsuario;
        if (element.listInformacionBasicaDet.length > 0) {
          obj.listInformacionBasicaDet = element.listInformacionBasicaDet.map(
            (element: any) => {
              const obj = new InformacionBasicaDetalleModelPMFIC(element);
              obj.codInfBasicaDet = CodigoPOPAC.ACORDEON_6_2;
              obj.idPlanManejo = this.idPlanManejo;
              obj.idUsuarioRegistro = this.user.idUsuario;
              return obj;
            }
          );
        }
        contentArray.push(obj);
      });
    }

    if (!!contentArray) {
      this.informacionBasicaService
        .registrarInformacionBasicaDetalle(contentArray)
        .subscribe((response: any) => {
          if (response.success) {
            this.createStore.submitSuccess();
            this.toast.ok(
              "Se registró la información socioeconómica correctamente."
            );
            this.listarInfCaracterizacionComunidad();
            this.listarInfInfraestructuraServicios();
            this.listarInfAntecentesUsoIdentificacionConflictos();
          }
        });
    }
  }

  openModalConfirmationGuardar() {
    this.ref = this.dialogService.open(
      ModalConfirmationGuardarSocioeconomicoComponent,
      {
        header: "",
        width: "40%",
        contentStyle: { "max-height": "500px", overflow: "auto" },
      }
    );

    this.ref.onClose.subscribe((resp: any) => {
      if (resp == "YES") {
        var params = [
          {
            idInfBasica: 0,
            codInfBasica: "POPAC",
            codSubInfBasica: "SOCIOECONOMICO",
            idPlanManejo: this.idPlanManejo,
            idUsuarioRegistro: this.user.idUsuario,
            listInformacionBasicaDet: [],
          },
        ];
        this.informacionBasicaService
          .registrarInformacionBasicaDetalle(params)
          .subscribe((response: any) => {
            if (response.success) {
              this.toast.ok(response.message);
              this.listarInfCaracterizacionComunidad();
              this.listarInfInfraestructuraServicios();
              this.listarInfAntecentesUsoIdentificacionConflictos();
            }
          });
      }
    });
  }
  openModal() {
    this.notData = false;
    this.ref = this.dialogService.open(ModalFormularioOtrosComponent, {
      header: "Agregar",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        lista: this.listCaracterizacionComunidad,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Actividades principales") {
          const obj = new InformacionBasicaDetalleModelPMFIC();
          obj.actividad = resp.actividad;
          obj.idInfBasicaDet = 0;
          const array = this.listCaracterizacionComunidad.filter(
            (data: any) => data.comunidad == resp.tipoPoblacion
          )[0].listInformacionBasicaDet;
          array.push(obj);
        } else {
          const obj = new InformacionBasicaModelPMFIC();
          obj.comunidad = resp.poblacion;
          obj.codInfBasica = "POPAC";
          obj.codSubInfBasica = "POPACISCC";
          obj.codNombreInfBasica = "OT";
          this.listCaracterizacionComunidad.push(obj);
        }
      }
    });
  }
  openModalEditar(data: any) {
    this.notData = false;
    this.ref = this.dialogService.open(ModalFormularioSocieconomicaComponent, {
      header: "Editar",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        lista: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
      }
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
