import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-formulario-otros",
  templateUrl: "./modal-formulario-otros.component.html",
  styleUrls: ["./modal-formulario-otros.component.scss"],
})
export class ModalFormularioOtrosComponent implements OnInit {
  context: any = {
    actividad: "",
    tipoPoblacion: "",
    tipo:"",
    poblacion:""
  };
  cmbTipoActividad: any = [];

  cmbActividad: any = [ 
    {label:'Tipo de población'},
    {label:'Actividades principales'}
  ]
  ;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private user: UsuarioService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.cmbTipoActividad = this.config.data.lista;
  }

  agregar = () => {
    this.ref.close(this.context);
  };

  cerrarModal() {
    this.ref.close();
  }
}
