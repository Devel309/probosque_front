import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-rentabilidad",
  templateUrl: "./modal-rentabilidad.component.html",
  styleUrls: ["./modal-rentabilidad.component.scss"],
})
export class ModalRentabilidadComponent implements OnInit {
  autoResize: boolean = true;
  impactoObjt: any = {
    rubro: "",
    mes1: "",
    mes2: "",
    mes3: "",
    mes4: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit() {}

  agregar = () => {
    this.ref.close(this.impactoObjt);
  };
}
