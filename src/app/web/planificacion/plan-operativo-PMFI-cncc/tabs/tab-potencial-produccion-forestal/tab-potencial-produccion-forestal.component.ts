import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { descargarArchivo, ToastService } from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PopacInformacionGeneral } from 'src/app/model/popac-informacion-general';
import { CodigoPOPAC } from 'src/app/model/util/POPAC/POPAC';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { AnexosService } from 'src/app/service/plan-operativo-concesion-maderable/anexos.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { ModalDescripcionComponent } from '../../modal/modal-descripcion/modal-descripcion.component';
import { ModalFormularioCategoriaComponent } from '../../modal/modal-formulario-categoria/modal-formulario-categoria.component';
import { ModalFormularioFinesManerablesComponent } from '../../modal/modal-formulario-fines-maderables/modal-formulario-fines-maderables.component';
import {environment} from '@env/environment';
import { UrlFormatos } from 'src/app/model/urlFormatos';

@Component({
  selector: 'app-tab-potencial-produccion-forestal',
  templateUrl: './tab-potencial-produccion-forestal.component.html',
})
export class TabPotencialProduccionForestalComponent implements OnInit {
  @Input() idPGMF!: number;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  ref: DynamicDialogRef = new DynamicDialogRef();

  actividades = [
    {
      practicaSilvicultural: 'respeto por árboles semilleros',
      descripcion: 'string',
    },
    {
      practicaSilvicultural: 'respeto por árboles semilleros',
      descripcion: '',
    },
  ];

  archivo: any;
  cargarServicios= false;
  CodigoPOPAC = CodigoPOPAC;
  form: PopacInformacionGeneral = new PopacInformacionGeneral();
  idInformacionGeneral: number = 0;
  nroArbolesMaderables!: number | null;
  nroArbolesMaderablesSemilleros!: number | null;
  volumenM3rMaderables: string | number | null = '';
  nroArbolesNoMaderables!: number | null;
  nroArbolesNoMaderablesSemilleros!: number | null;
  volumenM3rNoMaderables: string | number | null = '';
  superficieHaNoMaderables: string | number | null = '';
  tieneCenso: boolean = false;
  
  nombreGenerado: string = "";
  plantillaPOPAC: string = "";

  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private censoForestalService: CensoForestalService,
    private informacionGeneralService: InformacionGeneralService,
    private anexosService: AnexosService,
    private usuarioService: UsuarioService,
    private archivoService: ArchivoService
  ) {
    this.archivo = {
      file: null,
    };
  }

  ngOnInit() {}

  openModalCategorias(mensaje: string, observacion: any, edit: any) {
    this.ref = this.dialogService.open(ModalFormularioCategoriaComponent, {
      header: mensaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: observacion,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        observacion.categoria = resp.categoria;
        observacion.usoPotencial = resp.usoPotencial;
        observacion.actividades = resp.actividades;
        observacion.editarForm = false;
      }
    });
  }

  openModalFinesMaderables(mensaje: string, finesMaderables: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioFinesManerablesComponent,
      {
        header: mensaje,
        width: '50%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        baseZIndex: 10000,
        data: {
          item: finesMaderables,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        finesMaderables.actividades = resp.actividades;
        finesMaderables.descripcionDeSistema = resp.descripcionDeSistema;
        finesMaderables.maquinaria = resp.maquinaria;
        finesMaderables.persona = resp.persona;
        finesMaderables.observaciones = resp.observaciones;
        finesMaderables.editarForm = false;
      }
    });
  }
  openModalDescripcion(mensaje: string, descripcion: any, edit: any) {
    this.ref = this.dialogService.open(ModalDescripcionComponent, {
      header: mensaje,
      width: '30%',
      contentStyle: { 'max-height': '200px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: descripcion,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        descripcion = resp;
      }
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  varAssets = `${environment.varAssets}`;
/*   btnDescargarFormato() {
    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }

    window.location.href = urlExcel;
} */

btnDescargarFormato() {
  this.dialog.open(LoadingComponent, { disableClose: true });
  this.nombreGenerado = UrlFormatos.CARGA_MASIVA_PMFIC;
  this.archivoService
    .descargarPlantilla(this.nombreGenerado)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((res: any) => {
      if (res.isSuccess == true) {
        this.toast.ok(res?.message);
        this.plantillaPOPAC = res;
        descargarArchivo(this.plantillaPOPAC);
      } else {
        this.toast.error(res?.message);
      }
    });
}


  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService.registrarCargaMasiva(
        this.archivo.file
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok("Se registró el archivo correctamente.\n");
          this.actualizarInformacionGeneral();
          this.cargarServicios= true;
          this.archivo.file = "";
        } else {
          this.toast.error('Error al procesar archivo, el archivo seleccionado no cumple con el formato de carga masiva.');
        }
      });
  }

  actualizarInformacionGeneral() {
    forkJoin([
      this.listAnexo5(),
      this.listAnexo6(),
      this.listAnexo7(),
      this.listarInformacionGeneral()
    ]).pipe(finalize(() => {
      this.editarInformacionGeneral();
    })).subscribe();
  }

  listAnexo5() {
    var params = {
      idPlanDeManejo: this.idPGMF,
      tipoPlan: this.CodigoPOPAC.CODIGO_PROCESO,
    };
    return this.anexosService
      .Anexo2(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroAM = 0;
          let nroAMS = 0;
          let vM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroAM = nroAM + 1;
            }
            if (element.categoria == 'S') {
              nroAMS = nroAMS + 1;
            }
            if (element.volumen) {
              vM = vM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesMaderables = nroAM;
          this.nroArbolesMaderablesSemilleros = nroAMS;
          this.volumenM3rMaderables = vM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesMaderables = null;
          this.nroArbolesMaderablesSemilleros = null;
          this.volumenM3rMaderables = null;
        }
      }));
  }

  listAnexo6() {
    var params = {
      idPlanDeManejo: this.idPGMF,
      tipoPlan: this.CodigoPOPAC.CODIGO_PROCESO,
    };
    return this.anexosService
      .Anexo6(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let sNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.areaBosque) {
              sNM = sNM + parseFloat(element.areaBosque);
            }
          });
          this.superficieHaNoMaderables = sNM;
          this.tieneCenso = true;
        } else {
          this.superficieHaNoMaderables = null;
        }
      }));
  }

  listAnexo7() {
    var params = {
      idPlanDeManejo: this.idPGMF,
      tipoPlan: this.CodigoPOPAC.CODIGO_PROCESO,
    };
    return this.anexosService
      .Anexo7NoMaderable(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          let nroANM = 0;
          let nroANMS = 0;
          let vNM = 0;
          response.data.forEach((element: any, index: number) => {
            if (element.categoria == 'A') {
              nroANM = nroANM + 1;
            }
            if (element.categoria == 'S') {
              nroANMS = nroANMS + 1;
            }
            if (element.volumen) {
              vNM = vNM + parseFloat(element.volumen);
            }
          });
          this.nroArbolesNoMaderables = nroANM;
          this.nroArbolesNoMaderablesSemilleros = nroANMS;
          this.volumenM3rNoMaderables = vNM;
          this.tieneCenso = true;
        } else {
          this.nroArbolesNoMaderables = null;
          this.nroArbolesNoMaderablesSemilleros = null;
          this.volumenM3rNoMaderables = null;
        }
      }));
  }

  listarInformacionGeneral() {
    var params = {
      codigoProceso: this.CodigoPOPAC.CODIGO_PROCESO,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPGMF,
    };
    return this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(tap((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.form = new PopacInformacionGeneral(element);
            this.idInformacionGeneral = element.idInformacionGeneralDema;
          });
        }
      }));
  }

  editarInformacionGeneral() {
    if (this.tieneCenso) {
      this.form.nroArbolesMaderables = this.nroArbolesMaderables === 0 ? 0 : this.nroArbolesMaderables ? this.nroArbolesMaderables : null;
      this.form.nroArbolesMaderablesSemilleros = this.nroArbolesMaderablesSemilleros === 0 ? 0 : this.nroArbolesMaderablesSemilleros ? this.nroArbolesMaderablesSemilleros : null;
      this.form.volumenM3rMaderables = this.volumenM3rMaderables ? String(this.volumenM3rMaderables) : null;
      this.form.nroArbolesNoMaderables = this.nroArbolesNoMaderables === 0 ? 0 : this.nroArbolesNoMaderables ? this.nroArbolesNoMaderables : null;
      this.form.nroArbolesNoMaderablesSemilleros = this.nroArbolesNoMaderablesSemilleros === 0 ? 0 : this.nroArbolesNoMaderablesSemilleros ? this.nroArbolesNoMaderablesSemilleros : null;
      this.form.volumenM3rNoMaderables = this.volumenM3rNoMaderables ? String(this.volumenM3rNoMaderables) : null;
      this.form.superficieHaNoMaderables = this.superficieHaNoMaderables ? String(this.superficieHaNoMaderables) : null;

      var params = new PopacInformacionGeneral(this.form);
      params.idPlanManejo = this.idPGMF;
      params.idUsuarioRegistro = this.usuarioService.idUsuario;
      params.codigoProceso = this.CodigoPOPAC.CODIGO_PROCESO;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.toast.ok(response.message);
            this.listarInformacionGeneral();
          } else {
            this.toast.warn(response.message);
          }
        });
    }
  }
}
