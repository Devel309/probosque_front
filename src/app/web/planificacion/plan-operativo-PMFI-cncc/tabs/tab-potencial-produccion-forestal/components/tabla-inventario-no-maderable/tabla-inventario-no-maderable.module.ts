import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaInventarioNoMaderableComponent } from './tabla-inventario-no-maderable.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    TablaInventarioNoMaderableComponent
  ],
  exports: [
    TablaInventarioNoMaderableComponent
  ],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class TablaInventarioNoMaderableModule { }
