import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaFustalesComponent } from './tabla-fustales.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    TablaFustalesComponent
  ],
  exports:[TablaFustalesComponent],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class TablaFustalesModule { }
