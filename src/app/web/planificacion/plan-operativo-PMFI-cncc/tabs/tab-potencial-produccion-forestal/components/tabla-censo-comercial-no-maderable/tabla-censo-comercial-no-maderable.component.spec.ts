import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaCensoComercialNoMaderableComponent } from './tabla-censo-comercial-no-maderable.component';

describe('TablaCensoForestalNoMaderableComponent', () => {
  let component: TablaCensoComercialNoMaderableComponent;
  let fixture: ComponentFixture<TablaCensoComercialNoMaderableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaCensoComercialNoMaderableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaCensoComercialNoMaderableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
