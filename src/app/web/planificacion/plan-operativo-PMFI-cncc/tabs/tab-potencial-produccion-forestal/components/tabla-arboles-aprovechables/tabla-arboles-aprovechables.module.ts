import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaArbolesAprovechablesComponent } from './tabla-arboles-aprovechables.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    TablaArbolesAprovechablesComponent
  ],
  exports:[TablaArbolesAprovechablesComponent ],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class TablaArbolesAprovechablesModule { }
