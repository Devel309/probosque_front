import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaFustalesComponent } from './tabla-fustales.component';

describe('TablaFustalesComponent', () => {
  let component: TablaFustalesComponent;
  let fixture: ComponentFixture<TablaFustalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaFustalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaFustalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
