import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaCensoForestalMaderableComponent } from './tabla-censo-forestal-maderable.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    TablaCensoForestalMaderableComponent
  ],
  exports: [
    TablaCensoForestalMaderableComponent
  ],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class TablaCensoForestalMaderableModule { }
