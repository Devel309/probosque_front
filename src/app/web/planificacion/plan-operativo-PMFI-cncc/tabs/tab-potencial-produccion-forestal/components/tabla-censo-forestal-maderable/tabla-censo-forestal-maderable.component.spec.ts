import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaCensoForestalMaderableComponent } from './tabla-censo-forestal-maderable.component';

describe('TablaCensoForestalMaderableComponent', () => {
  let component: TablaCensoForestalMaderableComponent;
  let fixture: ComponentFixture<TablaCensoForestalMaderableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaCensoForestalMaderableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaCensoForestalMaderableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
