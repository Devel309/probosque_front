import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaArbolesAprovechablesComponent } from './tabla-arboles-aprovechables.component';

describe('TablaArbolesAprovechablesComponent', () => {
  let component: TablaArbolesAprovechablesComponent;
  let fixture: ComponentFixture<TablaArbolesAprovechablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaArbolesAprovechablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaArbolesAprovechablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
