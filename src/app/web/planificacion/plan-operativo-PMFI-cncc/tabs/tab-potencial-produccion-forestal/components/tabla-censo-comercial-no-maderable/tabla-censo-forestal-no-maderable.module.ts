import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaCensoComercialNoMaderableComponent } from './tabla-censo-comercial-no-maderable.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    TablaCensoComercialNoMaderableComponent
  ],
  exports: [
    TablaCensoComercialNoMaderableComponent
  ],
  imports: [
    CommonModule,
    TableModule
  ]
})
export class TablaCensoComercialNoMaderableModule { }
