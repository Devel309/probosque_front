import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { Arboles, PotencialForestal } from 'src/app/model/ProduccionForestalModel';
import { CensoForestalService } from 'src/app/service/censoForestal';

@Component({
  selector: 'tabla-censo-comercial-no-maderable',
  templateUrl: './tabla-censo-comercial-no-maderable.component.html',
  styleUrls: ['./tabla-censo-comercial-no-maderable.component.scss']
})
export class TablaCensoComercialNoMaderableComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() set cargarServicio(b:boolean){
    if(b){
      this.listAprovechamientoNoMaderable();
    }
  }
  listAnexo2Especies: any[] = [];
  variables: any[] = [{ var: "N(Individuos/ha)" }, { var: "C(**)" }];
  areaTotalCensada: number = 0;
  especiesCensadas: number = 0;
  arbolesCensados: number = 0;

  TotalTipoBosqueN: number = 0;
  TotalPorAreaN: number = 0;
  porcentajeN: number = 0;

  TotalTipoBosquevc: number = 0;
  TotalPorAreavc: number = 0;
  porcentajevc: number = 0;
  unidad: string = "";
  constructor(
    private censoForestalService: CensoForestalService,
    private toast: ToastService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.listAprovechamientoNoMaderable();
  }

  listAprovechamientoNoMaderable() {
    var params = {

      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: "POPAC",
    };
    this.censoForestalService
      .aprovechamientoNoMaderable(params)
      .subscribe((response: any) => {
        if (!!response.data) {
          this.areaTotalCensada = response.data.areaTotalCensada;
          this.arbolesCensados = response.data.numeroArbolesNoMaderables;

          response.data.resultadosRFMTablas.forEach(
            (element: any, index: number) => {
              var obj = new PotencialForestal();
              obj.nombreComun = element.nombreEspecie;
              this.variables.forEach((item: any, i: number) => {
                var ob1 = new Arboles();
                var ob2 = new Arboles();
                this.unidad = element.unidadC;
                if (item.var == "N(Individuos/ha)") {
                  ob1.var = "N(Individuos/ha)";
                  ob1.TotalTipoBosque = Number(element.dapTotalporHa);
                  this.TotalTipoBosqueN += Number(element.dapTotalporHa);
                  ob1.TotalPorArea = Number(element.dapTotalPC);
                  this.TotalPorAreaN += Number(element.dapTotalPC);
                  ob1.porcentaje =
                    Number(element.dapTotalPC) /
                    Number(this.areaTotalCensada) * 100;
                  this.porcentajeN += ob1.porcentaje;
                  obj.array.push(ob1);
                } else if (item.var == "C(**)") {
                  ob2.var = "C(" + element.unidadC + ")";
                  ob2.TotalTipoBosque = Number(element.volTotalporHa);
                  this.TotalTipoBosquevc += Number(element.volTotalporHa);
                  ob2.TotalPorArea = Number(element.volTotalPC);
                  this.TotalPorAreavc += Number(element.volTotalPC);
                  ob2.porcentaje = Number(
                    element.volTotalPC / Number(this.areaTotalCensada) * 100
                  );
                  this.porcentajevc += ob2.porcentaje;
                  obj.array.push(ob2);
                }
              });
              this.listAnexo2Especies.push(obj);
            }
          );
        } else {
          this.toast.warn(response.message);
        }
      });
  }

}
