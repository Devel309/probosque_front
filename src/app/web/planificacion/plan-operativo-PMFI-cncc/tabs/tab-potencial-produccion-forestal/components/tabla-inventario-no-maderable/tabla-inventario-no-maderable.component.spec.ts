import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaInventarioNoMaderableComponent } from './tabla-inventario-no-maderable.component';

describe('TablaInventarioNoMaderableComponent', () => {
  let component: TablaInventarioNoMaderableComponent;
  let fixture: ComponentFixture<TablaInventarioNoMaderableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaInventarioNoMaderableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaInventarioNoMaderableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
