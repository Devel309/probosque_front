import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-antecedentes',
  templateUrl: './modal-formulario-antecedentes.component.html',
})
export class ModalFormularioAntecedentesComponent implements OnInit {
  antecedentesObjt: any = {
    actividades: '',
    especies: '',
    conflictos: '',
    propuestas: '',
    observaciones: '',
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      
      this.antecedentesObjt.actividades = this.config.data.item.actividades;
      this.antecedentesObjt.especies = this.config.data.item.especies;
      this.antecedentesObjt.conflictos =
        this.config.data.item.conflictos;
      this.antecedentesObjt.propuestas =
        this.config.data.item.propuestas;
      this.antecedentesObjt.observaciones = this.config.data.item.observaciones;
    }
  }

  agregar = () => {
    if (this.antecedentesObjt.actividades === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre común, es obligatorio',
      });
    } else if (this.antecedentesObjt.especies === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre nativo, es obligatorio',
      });
    } else if (this.antecedentesObjt.conflictos === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre científico, es obligatorio',
      });
    } else if (this.antecedentesObjt.propuestas === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Categoía de amenaza es obligatorio',
      });
    } else if (this.antecedentesObjt.observaciones === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Observaciones, es obligatorio',
      });
    } else this.ref.close(this.antecedentesObjt);
  };
}
