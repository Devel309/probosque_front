import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-categoria',
  templateUrl: './modal-formulario-categoria.component.html',
})
export class ModalFormularioCategoriaComponent implements OnInit {
  categoriaZonificacionObjt: any = {
    categoria: '',
    usoPotencial: '',
    actividades: '',
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      
      this.categoriaZonificacionObjt.categoria = this.config.data.item.categoria;
      this.categoriaZonificacionObjt.usoPotencial = this.config.data.item.usoPotencial;
      this.categoriaZonificacionObjt.actividades =
        this.config.data.item.actividades;
    }
  }

  agregar = () => {
    if (this.categoriaZonificacionObjt.categoria === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Categoria, es obligatorio',
      });
    } else if (this.categoriaZonificacionObjt.usoPotencial === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Uso potencial, es obligatorio',
      });
    } else if (this.categoriaZonificacionObjt.actividades === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Actividades, es obligatorio',
      });
    } else this.ref.close(this.categoriaZonificacionObjt);
  };
}
