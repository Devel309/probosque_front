import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-hidrografia',
  templateUrl: './modal-formulario-hidrografia.component.html',
})
export class ModalFormularioHidrografiaComponent implements OnInit {
  descripcion: string = '';
  selectedRio: string = '';
  quebrada: string = '';
  cocha: string = '';
  obj: any = null;

  rios = [
    { rio: 'Río Zarumilla' },
    { rio: 'Río Tumbes' },
    { rio: 'Río Piura' },
    { rio: 'Río Chira' },
    { rio: 'Río Olmos' },
    { rio: 'Río Motupe' },
  ];
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef
  ) {}

  ngOnInit() {}

  enviar = () => {
    this.obj = {
      rio: this.selectedRio,
      quebrada: this.quebrada,
      cocha: this.cocha,
    };

    this.ref.close(this.obj);
  };
}
