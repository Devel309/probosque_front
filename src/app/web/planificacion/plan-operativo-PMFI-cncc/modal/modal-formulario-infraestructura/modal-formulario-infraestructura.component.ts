import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-formulario-infraestructura",
  templateUrl: "./modal-formulario-infraestructura.component.html",
})
export class ModalFormularioInfraestructuraComponent implements OnInit {
  isDisabled: boolean = false;
  InfraestructuraObjt: any = {
    label:"",
    nombre: "",
    este: "",
    norte: "",
    observaciones: "",
    editarFormInfraestructura: true,
  };

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == "true") {
      this.isDisabled = true;
      
      this.InfraestructuraObjt.label = this.config.data.item.label;
      this.InfraestructuraObjt.nombre = this.config.data.item.nombre;
      this.InfraestructuraObjt.este = this.config.data.item.este;
      this.InfraestructuraObjt.norte = this.config.data.item.norte;
      this.InfraestructuraObjt.observaciones = this.config.data.item.observaciones;
    }
  }

  agregar = () => {
    if (this.InfraestructuraObjt.nombre === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Nombre, es obligatorio",
      });
    } else if (this.InfraestructuraObjt.este === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Este, es obligatorio",
      });
    } else if (this.InfraestructuraObjt.norte === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Norte, es obligatorio",
      });

    } else if (this.InfraestructuraObjt.observaciones === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Observaciones, es obligatorio",
      });
    } else this.ref.close(this.InfraestructuraObjt);
  };
}
