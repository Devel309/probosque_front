import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plan-operativo-PMFI-cncc',
  templateUrl: './plan-operativo-PMFI-cncc.component.html',
})
export class PlanOperativoPMFICnccComponent implements OnInit {
  listPlanManejoForestalIntermedio: any[] = [];

  constructor(private router: Router) {}

  ngOnInit() {
    localStorage.removeItem('plan-operativo-PMFI-cncc');
    this.cargarPlanManejoForestalIntermedio();
  }

  cargarPlanManejoForestalIntermedio = () => {
    this.listPlanManejoForestalIntermedio.push({
      idProcesoPostulacion: 'idProcesoPostulacion',
      descripcion: 'descripcion',
    });
  };

  verDetalle = (item: any) => {
    localStorage.setItem(
      'plan-operativo-PMFI-cncc',
      JSON.stringify(item)
    );
    this.router.navigate([
      '/planificacion/detalle-plan-operativo-PMFI-cncc',
    ]);
  };
}
