import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlanManejoService } from '@services';
import { MenuItem } from 'primeng/api';
import { TabInformacionBasicaComponent } from '../tabs/tab-informacion-basica/tab-informacion-basica.component';
import {CodigoEstadoPlanManejo} from '../../../../model/util/CodigoEstadoPlanManejo';

@Component({
  selector: 'app-plan-operativo-PMFI-cncc',
  templateUrl: './detalle-plan-operativo-PMFI-cncc.component.html',
  styleUrls: [ './detalle-plan-operativo-PMFI-cncc.component.scss' ],
})
export class DetallePlanOperativoPMFICnccComponent implements OnInit {
  @ViewChild(TabInformacionBasicaComponent)
  TabInformacionBasicaComponent!: TabInformacionBasicaComponent;
  idPlanManejo!: number;
  idPlanManejoPadre!: number;
  tabIndex = 0;
  disabled: boolean = false;

  items!: MenuItem[];
  home!: MenuItem;

  constructor(private router: Router, private route: ActivatedRoute, private planManejoService: PlanManejoService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Plan Operativo PMFI en Comunidades Nativas y Campesinas',
        routerLink: '/planificacion/bandeja-popac',
      },
      { label: 'Detalle' },
    ];

    this.home = { icon: 'pi pi-home', routerLink: '/inicio' };

    this.obtenerPlan();
  }

  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {
      this.idPlanManejoPadre = result.data.idPlanManejoPadre;
      if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
        this.disabled = true;
      }
    })
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
    if (event === 0) {
      this.TabInformacionBasicaComponent.ngOnInit();
    }
  }

  regresar = () => {
    this.router.navigate([ '/planificacion/bandeja-popac' ]);
  };
}
