import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {PlanManejoService, UsuarioService} from '@services';
import {ToastService} from '@shared';
import {LazyLoadEvent} from 'primeng/api';
import {Page, PlanManejoMSG as MSG} from '@models';
import {finalize, tap} from 'rxjs/operators';
import {UsuarioModel} from '../../../model/seguridad/usuario';
import {EvaluacionListarRequest} from '../../../model/EvaluacionListarRequest';
import {LoadingComponent} from '../../../components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';

@Component({
  selector: 'bandeja-POCC',
  templateUrl: './bandeja-POCC.component.html',
})
export class BandejaPOCCComponent implements OnInit {
  f!: FormGroup;
  planes: any[] = [];
  usuario!: UsuarioModel;
  evaluacionRequest : EvaluacionListarRequest

  PLAN = {
    idTipoProceso: 1,
    idTipoEscala: 2,
    idTipoPlan: 6,
    idSolicitud: 1,
    descripcion: 'POCC',
  }

  loading = false;
  totalRecords = 0;
  permisos: IPermisoOpcion = {} as IPermisoOpcion;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    private activaRoute: ActivatedRoute

  ) {
    this.f = this.initForm();
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    this.buscar();
  }

  initForm() {
    return this.fb.group({
      dniElaborador: [null],
      rucComunidad: [null],
      nombreElaborador: [null],
      idPlanManejo: [null],
    });
  }

  buscar() {
    this.listarPlanes().subscribe();
  }

  limpiar() {
    this.f.reset();
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.buscar();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = (Number(e.first) / pageSize) + 1;
    const page = new Page({ pageNumber, pageSize })
    this.listarPlanes(page).subscribe(res => { this.totalRecords = res.totalRecords });
  }

  nuevoPlan() {
    const body = { ...this.PLAN, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(res => this.navigate(res.data.idPlanManejo))
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo)
  }

  listarPlanes(page?: Page) {
    if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }
    const r = { ...this.PLAN, ...this.evaluacionRequest }
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => this.loading = false))
      .pipe(tap(res => {
        if (res.success && res.data.length > 0) {
          this.planes = res.data;
          this.totalRecords = res.totalRecords;
        } else {
          this.planes = [];
          this.totalRecords = 0;
          this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
        }
      }))
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body)
      .pipe(tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE)
      }));
  }


  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiPlanManejo.filtrar(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  navigate(idPlan: number) {
    const uri = 'planificacion/plan-operativo-concesiones-maderables';
    this.router.navigate([uri, idPlan])
  }
}
