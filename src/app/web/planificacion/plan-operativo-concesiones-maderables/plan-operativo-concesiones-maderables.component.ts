import { Component, OnInit, ViewChild } from '@angular/core';
import { TabInformacionGeneralComponent } from './tabs/tab-informacion-general/tab-informacion-general.component'
import {ActivatedRoute} from '@angular/router';
import {CodigoEstadoPlanManejo} from '../../../model/util/CodigoEstadoPlanManejo';
import {PlanManejoService} from '@services';
@Component({
  selector: 'plan-operativo-concesiones-maderables',
  templateUrl: './plan-operativo-concesiones-maderables.component.html',
  styleUrls: ['./plan-operativo-concesiones-maderables.component.scss']
})
export class PlanOperativoConcesionesMaderablesComponent implements OnInit {

  @ViewChild(TabInformacionGeneralComponent) TabInformacionGeneralComponent!: TabInformacionGeneralComponent;
  tabIndex = 0;
  idPlanManejo!: number;

  disabled: boolean = false;

  constructor(private route: ActivatedRoute,private planManejoService: PlanManejoService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.obtenerPlan();

  }

  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    };

    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((result: any) => {
        if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
          this.disabled = true;
        }
      });
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
    if (event === 0) {
      this.TabInformacionGeneralComponent.ngOnInit();
    }
  }

}
