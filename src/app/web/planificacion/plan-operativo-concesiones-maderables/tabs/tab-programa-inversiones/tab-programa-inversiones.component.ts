import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService, SelectItemGroup, } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PoccInformacionGeneral } from "src/app/model/pocc-informacion-general";
import { ListRentabilidadManejoForestalDetalle, RentabilidadPGMFA, } from "src/app/model/rentabilidadManejo";
import { RentabilidadManejoForestalModel } from "src/app/model/RentabilidadManejoForestal";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { InformacionGeneralPOCMService } from "src/app/service/plan-operativo-concesion-maderable/informacion-general.service";
import { RentabilidadManejoForestalService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/rentavilidad-manejo-forestal.service";

@Component({
  selector: "app-tab-programa-inversiones",
  templateUrl: "./tab-programa-inversiones.component.html",
  styleUrls: ["./tab-programa-inversiones.component.scss"],
})
export class TabProgramaInversionesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();

  codigoProceso = CodigoProceso.PLAN_OPERATIVO_CONCESIONES;
  UrlFormatos = UrlFormatos

  form: PoccInformacionGeneral = new PoccInformacionGeneral();
  vigenciaIncial: number = 0;
  listRentabilidadManejo: RentabilidadManejoForestalModel[] = [];
  listRentabilidad: RentabilidadPGMFA[] = [];
  listaNecesidades: any[] = [];
  colsAnioN: any[] = [];
  colsMesN: any[] = [];
  ingresoN: any[] = [];
  egresoN: any[] = [];
  totalIngresoN: any[] = [];
  totalEgresoN: any[] = [];
  edit: boolean = false;
  nuevoIngresoEgresoNecesidades: boolean = false;
  necesidad: string = "";

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private rentabilidadManejoForestalService: RentabilidadManejoForestalService,
    private user: UsuarioService,
    private toast: ToastService,
    private informacionGeneralPOCMService: InformacionGeneralPOCMService
  ) {}

  ngOnInit(): void {
    this.listarInformacionGeneralN();
    this.listarRentabilidadN();
  }

  listarInformacionGeneralN() {
    const body = {
      codigoProceso: this.codigoProceso,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralPOCMService
      .listarInformacionGeneral(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length) {
          const last = response.data.length - 1;
          this.form = new PoccInformacionGeneral(response.data[last]);
          this.form.lstUmf.forEach((item: any) => {
            if (item.detalle == "SI") {
              this.vigenciaIncial = item.vigenciaDet;
            }
          });
          this.crearColumnasN();
        } else {
          this.toast.warn("Se debe registrar duración de PO.")
        }
      });
  }

  crearColumnasN() {
    this.colsAnioN = [];
    this.colsMesN = [];
    if (this.vigenciaIncial > 0) {
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        this.colsAnioN.push({
            header: 'Año ' + i
        })
      }
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          this.colsMesN.push({
              header: 'Mes ' + j,
              field: 'monto' + i + '-' + j
          })
        }
      }
    }
  }

  listarRentabilidadN() {
    this.listRentabilidadManejo = [];
    this.listaNecesidades = [];
    var params = {
      codigoParametro: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.rentabilidadManejoForestalService
      .listarRentabilidadManejoForestal(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idTipoRubro == 3) {
            let item = {
              codigoRentabilidad: element.codigoRentabilidad,
              observacion: element.observacion,
              descripcion: element.descripcion,
              estado: "A",
              idPlanManejo: this.idPlanManejo,
              idRentManejoForestal: element.idRentManejoForestal,
              idTipoRubro: element.idTipoRubro,
              idUsuarioRegistro: this.user.idUsuario,
              listRentabilidadManejoForestalDetalle:
                element.listRentabilidadManejoForestalDetalle,
              rubro: element.rubro,
            };
            this.listaNecesidades.push(item);
          } else {
            element.estado = 'A';
            this.listRentabilidadManejo.push(element);
          }
        });
        this.crearFilasN();
      });
  }

  crearFilasN() {
    this.ingresoN = [];
    this.egresoN = [];

    this.listRentabilidadManejo.forEach((rentab) => {
      let obj: any = {};
      obj = {
        ...rentab,
      };
      rentab.listRentabilidadManejoForestalDetalle.forEach((item) => {
        obj.edit = false;
        obj.estado = "A";
        obj.idUsuarioRegistro = this.user.idUsuario;
        obj['monto' + item.anio + '-' + item.mes] = item.monto.toFixed(2);
        obj['idRentManejoForestalDet' + item.anio + '-' + item.mes] = item.idRentManejoForestalDet;
      });
      if (rentab.idTipoRubro == 1) {
        this.ingresoN.push(obj);
      } else {
        this.egresoN.push(obj);
      }
    });

    this.calcularTotalIngresoN();
    this.calcularTotalEgresoN();
  }

  calcularTotalIngresoN() {
    this.totalIngresoN = [];
    let obj: any = {};
    let arr: any[] = [];
    this.ingresoN.forEach((item) => {
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          let monto1: number;
          let monto2: number;
          if (obj['total' + i + '-' + j] === undefined) {
            monto1 = 0;
          } else {
            monto1 = Number(obj['total' + i + '-' + j]);
          }
          if (item['monto' + i + '-' + j] === undefined) {
            monto2 = 0;
          } else {
            monto2 = Number(item['monto' + i + '-' + j]);
          }
          obj['total' + i + '-' + j] = (parseFloat(monto1.toFixed(2)) + parseFloat(monto2.toFixed(2))).toFixed(2);
        }
      }
    });
    arr.push(obj);
    arr.forEach((a) => {
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          this.totalIngresoN.push({
            total: a['total' + i + '-' + j]
          });
        }
      }
    });
  }

  calcularTotalEgresoN() {
    this.totalEgresoN = [];
    let obj: any = {};
    let arr: any[] = [];
    this.egresoN.forEach((item) => {
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          let monto1: number;
          let monto2: number;
          if (obj['total' + i + '-' + j] === undefined) {
            monto1 = 0;
          } else {
            monto1 = Number(obj['total' + i + '-' + j]);
          }
          if (item['monto' + i + '-' + j] === undefined) {
            monto2 = 0;
          } else {
            monto2 = Number(item['monto' + i + '-' + j]);
          }
          obj['total' + i + '-' + j] = (parseFloat(monto1.toFixed(2)) + parseFloat(monto2.toFixed(2))).toFixed(2);
        }
      }
    });
    arr.push(obj);
    arr.forEach((a) => {
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          this.totalEgresoN.push({
            total: a['total' + i + '-' + j]
          });
        }
      }
    });
  }

  agregarIngreso() {
    let obj: any = {};
    obj = {
      codigoRentabilidad: this.codigoProceso,
      edit: true,
      estado: "A",
      idPlanManejo: this.idPlanManejo,
      idRentManejoForestal: 0,
      idRentManejoForestalDet: 0,
      idTipoRubro: 1,
      idUsuarioRegistro: this.user.idUsuario,
      rubro: ""
    }
    for (let i = 1; i <= this.vigenciaIncial; i++) {
      for (let j = 1; j <= 12; j++) {
        obj['idRentManejoForestalDet' + i + '-' + j] = 0;
        obj['monto' + i + '-' + j] = "";
      }
    }
    this.ingresoN.push(obj);
  }

  agregarEgreso() {
    let obj: any = {};
    obj = {
      codigoRentabilidad: this.codigoProceso,
      edit: true,
      estado: "A",
      idPlanManejo: this.idPlanManejo,
      idRentManejoForestal: 0,
      idRentManejoForestalDet: 0,
      idTipoRubro: 2,
      idUsuarioRegistro: this.user.idUsuario,
      rubro: ""
    }
    for (let i = 1; i <= this.vigenciaIncial; i++) {
      for (let j = 1; j <= 12; j++) {
        obj['idRentManejoForestalDet' + i + '-' + j] = 0;
        obj['monto' + i + '-' + j] = "";
      }
    }
    this.egresoN.push(obj);
  }

  registrarManejoRentabilidadN() {
    if (!this.validarIngresoEgresoN()) return;

    this.listRentabilidad = [];

    this.ingresoN.forEach((item) => {
      var obj = new RentabilidadPGMFA();
      obj.codigoRentabilidad = item.codigoRentabilidad;
      obj.estado = item.estado;
      obj.idPlanManejo = item.idPlanManejo;
      obj.idRentManejoForestal = item.idRentManejoForestal;
      obj.idTipoRubro = item.idTipoRubro;
      obj.idUsuarioRegistro = item.idUsuarioRegistro;
      obj.rubro = item.rubro;
      obj.listRentabilidadManejoForestalDetalle = [];
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          if (item['monto' + i + '-' + j] === undefined || item['monto' + i + '-' + j] === null || item['monto' + i + '-' + j] === "") {

          } else {
            var objDet = new ListRentabilidadManejoForestalDetalle();
            objDet.anio = i;
            objDet.estado = item.estado;
            objDet.mes = j;
            objDet.monto = item['monto' + i + '-' + j];
            if (item['idRentManejoForestalDet' + i + '-' + j] === undefined) {
              objDet.idRentManejoForestalDet = 0;
            } else {
              objDet.idRentManejoForestalDet = item['idRentManejoForestalDet' + i + '-' + j];
            }
            objDet.idUsuarioRegistro = item.idUsuarioRegistro;
            obj.listRentabilidadManejoForestalDetalle.push(objDet);
          }
        }
      }
      this.listRentabilidad.push(obj);
    });

    this.egresoN.forEach((item) => {
      var obj = new RentabilidadPGMFA();
      obj.codigoRentabilidad = item.codigoRentabilidad;
      obj.estado = item.estado;
      obj.idPlanManejo = item.idPlanManejo;
      obj.idRentManejoForestal = item.idRentManejoForestal;
      obj.idTipoRubro = item.idTipoRubro;
      obj.idUsuarioRegistro = item.idUsuarioRegistro;
      obj.rubro = item.rubro;
      obj.listRentabilidadManejoForestalDetalle = [];
      for (let i = 1; i <= this.vigenciaIncial; i++) {
        for (let j = 1; j <= 12; j++) {
          if (item['monto' + i + '-' + j] === undefined || item['monto' + i + '-' + j] === null || item['monto' + i + '-' + j] === "") {

          } else {
            var objDet = new ListRentabilidadManejoForestalDetalle();
            objDet.anio = i;
            objDet.estado = item.estado;
            objDet.mes = j;
            objDet.monto = item['monto' + i + '-' + j];
            if (item['idRentManejoForestalDet' + i + '-' + j] === undefined) {
              objDet.idRentManejoForestalDet = 0;
            } else {
              objDet.idRentManejoForestalDet = item['idRentManejoForestalDet' + i + '-' + j];
            }
            objDet.idUsuarioRegistro = item.idUsuarioRegistro;
            obj.listRentabilidadManejoForestalDetalle.push(objDet);
          }
        }
      }
      this.listRentabilidad.push(obj);
    });


    this.listaNecesidades.forEach((item) => {
      this.listRentabilidad.push(item);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.rentabilidadManejoForestalService
      .registrarRentabilidadManejoForestal(this.listRentabilidad)
      .subscribe(
        (res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.listarRentabilidadN();
            this.dialog.closeAll();
          } else {
            this.toast.error(res?.message);
            this.dialog.closeAll();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  }

  validarIngresoEgresoN(): boolean {
    let validar: boolean = true;
    let validarI: boolean = true;
    let validarE: boolean = true;
    let mensaje: string = "";

    this.ingresoN.forEach((item) => {
      if (item.rubro === "") {
        validarI = false;
      }
    });

    this.egresoN.forEach((item) => {
      if (item.rubro === "") {
        validarE = false;
      }
    });

    if (!validarI) mensaje += "(*) Debe ingresar: Rubro - Ingresos\n";
    if (!validarE) mensaje += "(*) Debe ingresar: Rubro - Egresos\n";

    if (!validarI || !validarE) {
      this.ErrorMensaje(mensaje);
      validar = false;
    }

    return validar;
  }

  eliminarRentabilidadN(event: Event, index: number, rentabilidad: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (rentabilidad.idRentManejoForestal > 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idRentManejoForestal: rentabilidad.idRentManejoForestal,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService
            .eliminarRentabilidadManejoForestal(params)
            .subscribe(
              (data: any) => {
                this.SuccessMensaje(data.message);
                if (rentabilidad.idTipoRubro == 1) {
                  this.ingresoN.splice(index, 1);
                } else {
                  this.egresoN.splice(index, 1);
                }
                this.listarRentabilidadN();
                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          if (rentabilidad.idTipoRubro == 1) {
            this.ingresoN.splice(index, 1);
          } else {
            this.egresoN.splice(index, 1);
          }
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  guardarNecesidad() {
    var params = {
      codigoRentabilidad: this.codigoProceso,
      observacion: "",
      descripcion: this.necesidad,
      estado: "A",
      idPlanManejo: this.idPlanManejo,
      idRentManejoForestal: 0,
      idTipoRubro: 3,
      idUsuarioRegistro: this.user.idUsuario,
      listRentabilidadManejoForestalDetalle: [],
      rubro: "",
    };
    this.listaNecesidades.push(params);
    this.necesidad = "";
  }

  eliminarNecesidad(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idRentManejoForestal > 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idRentManejoForestal: data.idRentManejoForestal,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService
            .eliminarRentabilidadManejoForestal(params)
            .subscribe(
              (data: any) => {
                this.SuccessMensaje(data.message);
                this.listaNecesidades.splice(index, 1);
                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.listaNecesidades.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  cargarFormato(files:any) {
    if (this.vigenciaIncial > 0) {
      if (files.length > 0) {
        files.forEach((t: any) => {
          let item = {
            nombreHoja: "Hoja1",
            numeroFila: 3,
            numeroColumna: 1,
            codigoRentabilidad: this.codigoProceso,
            idPlanManejo: this.idPlanManejo,
            idUsuarioRegistro: this.user.idUsuario,
            vigencia: this.vigenciaIncial
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService
            .registrarRentabilidadManejoForestalExcel(t.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.codigoRentabilidad, item.idPlanManejo, item.idUsuarioRegistro, item.vigencia)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((res: any) => {
              if (res.success == true) {
                this.toast.ok(res?.message);
                this.listarRentabilidadN();
              } else {
                this.toast.error(res?.message);
              }
            },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              });
        });
      } else {
        this.toast.warn('Debe seleccionar archivo.')
      }
    } else {
      this.toast.warn("Se debe registrar duración de PO.")
    }
  }
}
