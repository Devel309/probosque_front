import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";

import {
  ArchivoTipo,
  isNull,
  isNullOrEmpty,
  AppMapaComponent,
  LayerView,
  onlySemicolons,
  PGMFArchivoTipo,
  RespuestaTipo,
  setOneSemicolon,
  ToastService,
  DownloadFile,
} from "@shared";
import { ObjetivosService } from "../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/objetivos.service";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { OrdenamientoProteccionDet } from "../../../../../model/PlanManejo/Pocc/OrdenamientoProteccionDet";
import { HttpErrorResponse } from "@angular/common/http";
import { OrdenamientoProteccionService } from "../../../../../service/planificacion/plan-operativo-concesiones-maderables/ordenamiento-proteccion.service";
import { PostulacionPFDMService } from "../../../../../service/postulacionPFDM/postulacion-pfdm.service";
import { ArchivoService, PlanManejoService, UsuarioService } from "@services";
import {
  CodigoPOCCTabs,
  CodigoPOOC,
} from "../../../../../model/util/POCC/CodigoPOOC";
import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MapPorAcordeonComponent } from "../tab-ordenamiento-parcela-proteccion/components/map-por-acordeon/map-por-acordeon.component";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { AnexosCargaExcelService } from "src/app/service/anexos";
import { finalize } from "rxjs/operators";
import { UrlFormatos } from "src/app/model/urlFormatos";
@Component({
  selector: "app-tab-anexos",
  templateUrl: "./tab-anexos.component.html",
  styleUrls: ["./tab-anexos.component.scss"],
})
export class TabAnexosComponent implements OnInit {
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @Input() disabled!: boolean;
  public view: any = null;
  listAnexos2: any [] = [];
  totalRecords: number = 0;

  @Output()
  public regresar = new EventEmitter();

  @Output()
  public siguiente = new EventEmitter();

  @ViewChild(AppMapaComponent) map!: AppMapaComponent;
  @Input() idPlanManejo!: number;
  UrlFormatos = UrlFormatos

  constructor(
    private objetivosService: ObjetivosService,
    private ordenamientoProteccionService: OrdenamientoProteccionService,
    private planManejoService: PlanManejoService,
    private postulacionPfdmService: PostulacionPFDMService,
    private dialog: MatDialog,
    private archivoService: ArchivoService,
    private toast: ToastService,
    private user: UsuarioService,
    private anexosService: AnexosService,
    private anexosCargaExcelService: AnexosCargaExcelService
  ) {}

  isEditedMap = false;
  listOrdenInterno: any[] = [];
  listTabsToAnexo3: any[] = [];
  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;

  ngOnInit(): void {
    // this.initializeMap();
    this.obtenerCheckAnexos3();
    this.listAnexo2();

    this.planManejoService
      .listarPorFiltroPlanManejoArchivo({ idPlanManejo: this.idPlanManejo })
      .subscribe((response: any) => {});

    this.postulacionPfdmService
      .obtenerArchivoDetalle(this.idPlanManejo, "POCC")
      .subscribe((response: any) => {});
  }

  listAnexo2() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
     //idPlanDeManejo: 998,
      tipoPlan: "POCC",
    };

    this.anexosService.Anexo2(params).subscribe((response: any) => {
      
      this.listAnexos2 = response.data
      this.totalRecords = response.data.length
    });
  }

  obtenerCheckAnexos3() {
    this.listTabsToAnexo3 = [];
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoTipo: this.CodigoPOCC.CODIGO_PROCESO,
      tipoDocumento:
        this.CodigoPOCC.ACORDEON_3_1 +
        "," +
        this.CodigoPOCC.ACORDEON_5_1 +
        "," +
        this.CodigoPOCC.ACORDEON_6_1_6 +
        "," +
        this.CodigoPOCC.ACORDEON_6_1_7 +
        "," +
        this.CodigoPOCC.ACORDEON_6_3 +
        "," +
        this.CodigoPOCC.ACORDEON_7_1 +
        "," +
        this.CodigoPOCC.ACORDEON_7_2 +
        "," +
        this.CodigoPOCC.ACORDEON_7_3 +
        "," +
        this.CodigoPOCC.ACORDEON_8_4 +
        "," +
        this.CodigoPOCC.TAB_9 +
        "," +
        this.CodigoPOCC.TAB_11,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService
      .listarPlanManejoAnexoArchivo(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.data && response.data.length != 0) {
          response.data.forEach((element: any) =>
            this.listTabsToAnexo3.push(element)
          );
        }
        (error: HttpErrorResponse) => {
          this.toast.error(error.message);
          this.dialog.closeAll();
        };
      });
  }

  descargarArchivo(idArchivo: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idArchivo: idArchivo,
    };
    this.archivoService
      .descargarArchivoGeneral(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.archivo,
            result.data.nombeArchivo,
            result.data.contenTypeArchivo
          );
        }
        (error: HttpErrorResponse) => {
          this.toast.error(error.message);
          this.dialog.closeAll();
        };
      });
  }

  editar(data: any) {
    if (!data.isEdit) {
      
      var params = {
        idPGMFArchivo: data.idPlanManejoArchivo,
        codigoTipoPGMF: data.codigoTipo,
        descripcion: data.descripcion,
        idPlanManejo: data.idPlanManejo,
        idArchivo: data.idArchivo,
        idUsuarioRegistro: this.user.idUsuario,
        asunto: data.asunto,
        acapite: data.acapite,
      };
      this.postulacionPfdmService
        .registrarArchivoDetalle(params)
        .subscribe((response: any) => {
          this.dialog.closeAll();
          if (response.success == true) {
            this.toast.ok("Se actualizó el registro correctamente.");
            this.obtenerCheckAnexos3();
          } else {
            this.toast.error(response?.message);
          }
        });
    }

    // this.listTabsToAnexo3.forEach((item) => {
    //   var params = {
    //     idPlanManejoArchivo: item.idPlanManejoArchivo,
    //     codigoTipoPGMF: item.codigoTipo,
    //     descripcion: item.descripcion,
    //     idPlanManejo: item.idPlanManejo,
    //     idArchivo: item.idArchivo,
    //     idUsuarioRegistro: this.user.idUsuario,
    //     asunto: item.asunto,
    //     acapite: item.acapite
    //   };
    //   this.postulacionPfdmService
    //     .registrarArchivoDetalle(params)
    //     .subscribe((response: any) => {
    //       if (response.success == true) {
    //       } else {
    //         this.toast.error(response?.message);
    //       }
    //     });
    // });
    // this.toast.ok('Se actualizó la información correctamente.');
    // this.obtenerCheckAnexos3();
  }

  regresarTab() {
    this.regresar.emit();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  deleteLayer(l: LayerView) {
    let item = this.listOrdenInterno.find(
      (x) => x.actividadesRealizar == l.groupId
    );
    if (!isNull(item)) {
      item.areaHA = item.areaHA - l.area;
      item.areaHA = item.areaHA > 0 ? item.areaHA : 0;
      let capas = String(item.descripcion).replace(
        this.map.joinTitle(l.title),
        ""
      );
      capas = setOneSemicolon(capas);
      capas = onlySemicolons(capas) ? "" : capas;
      item.descripcion = capas;
      item.observacion = isNullOrEmpty(capas)
        ? RespuestaTipo.NO
        : RespuestaTipo.SI;
      let layerId = String(item.actividad).replace(String(l.layerId), "");
      layerId = setOneSemicolon(layerId);
      layerId = onlySemicolons(layerId) ? "" : layerId;
      item.actividad = layerId;
      item.observacionDetalle = isNullOrEmpty(capas)
        ? ""
        : item.observacionDetalle;
    }
  }

  deleteAllLayers() {
    this.listOrdenInterno.forEach((item) => {
      item.areaHA = 0;
      item.observacion = RespuestaTipo.NO; //tiene shapefile
      item.descripcion = "";
      item.actividad = "";
      item.actividadesRealizar = "";
      item.observacionDetalle = "";
    });
  }

  cargarFormato(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          tipoPlan: "POCC",
          idPlanManejo: this.idPlanManejo,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosCargaExcelService
          .FormatoPGMFFormuladoAnexo2Excel(t.file, item.tipoPlan,  item.idPlanManejo, )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listAnexo2();
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }
}
