import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { PGMFArchivoDto, PlanManejoArchivo } from '@models';
import {
  ArchivoService,
  OrdenamientoInternoPmfiService,
  UsuarioService,
} from '@services';
import {
  AppMapaComponent,
  isNull,
  isNullOrEmpty,
  LayerView,
  onlySemicolons,
  RespuestaTipo,
  setOneSemicolon,
  ToastService,
  ArchivoTipoCodigo,
} from '@shared';
import { forkJoin, Observable, of } from 'rxjs';
import { concatMap, finalize, map, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';

@Component({
  selector: 'mapa-por-accordeon',
  templateUrl: './mapa-por-accordeon.component.html',
  styleUrls: ['./mapa-por-accordeon.component.scss'],
})
export class MapaPorAccordeonComponent implements OnInit {
  @Input() codigoProceso!: string;
  @Input() isShow?: boolean = true;

  listOrdenInterno: any[] = [];
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;
  RespuestaTipo = RespuestaTipo;
  idPlanManejo!: number;

  isEditedMap = false;
  relacionArchivo!: { idArchivo: number; idPlanManejoArchivo: number };

  constructor(
    private apiArchivo: ArchivoService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private toast: ToastService,
    private route: ActivatedRoute,
    private anexosService: AnexosService
  ) {}

  ngOnInit(): void {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngAfterViewInit(): void {
    this.getInitData();
  }

  deleteLayer(l: LayerView) {
    let item = this.listOrdenInterno.find(
      (x) => x.actividadesRealizar == l.groupId
    );

    if (!isNull(item)) {
      item.areaHA = item.areaHA - l.area;
      item.areaHA = item.areaHA > 0 ? item.areaHA : 0;
      let capas = String(item.descripcion).replace(
        this.map.joinTitle(l.title),
        ''
      );
      capas = setOneSemicolon(capas);
      capas = onlySemicolons(capas) ? '' : capas;
      item.descripcion = capas;
      item.observacion = isNullOrEmpty(capas)
        ? RespuestaTipo.NO
        : RespuestaTipo.SI;
      let layerId = String(item.actividad).replace(String(l.layerId), '');
      layerId = setOneSemicolon(layerId);
      layerId = onlySemicolons(layerId) ? '' : layerId;
      item.actividad = layerId;
      item.observacionDetalle = isNullOrEmpty(capas)
        ? ''
        : item.observacionDetalle;
    }
  }
  deleteAllLayers() {
    this.listOrdenInterno.forEach((item) => {
      item.areaHA = 0;
      item.observacion = RespuestaTipo.NO; //tiene shapefile
      item.descripcion = '';
      item.actividad = '';
      item.actividadesRealizar = '';
      item.observacionDetalle = '';
    });
  }

  guardar() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.saveFileFlow()
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  saveFileFlow() {
    if (!this.isEditedMap) return of(null);

    const idArchivo = this.relacionArchivo?.idArchivo;
    const idPlanManejoArchivo = this.relacionArchivo?.idPlanManejoArchivo;

    const deleteFile = isNull(this.relacionArchivo)
      ? of(null)
      : this.deleteFile(idArchivo, idPlanManejoArchivo).pipe(
          tap(() => (this.isEditedMap = false))
        );

    if (this.map.isEmpty)
      return deleteFile.pipe(
        tap(() => (this.relacionArchivo = undefined as any))
      );

    return this.saveFile()
      .pipe(
        tap((res: any) => {
          this.relacionArchivo = {
            idArchivo: res?.data?.idArchivo,
            idPlanManejoArchivo: res?.data?.idPGMFArchivo,
          };
          this.isEditedMap = false;
        })
      )
      .pipe(concatMap(() => deleteFile));
  }

  saveFile() {
    return this.map
      .getZipFile()
      .pipe(tap({ error: (err) => this.toast.warn(err) }))
      .pipe(concatMap((blob) => this.guardarArchivo(blob as Blob)))
      .pipe(concatMap((idArchivo) => this.guardarRelacionArchivo(idArchivo)));
  }

  guardarArchivo(blob: Blob): Observable<number> {
    const file = new File([blob], `POCC-OI-${this.idPlanManejo}.zip`);
    return this.apiArchivo
      .cargar(this.user.idUsuario, ArchivoTipoCodigo.SHAPEFILE, file)
      .pipe(map((res) => res?.data));
  }

  guardarRelacionArchivo(idArchivo: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: this.codigoProceso,
      descripcion: '',
      idArchivo: idArchivo,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: '',
    };

    return this.anexosService.registrarArchivo(params);
  }

  deleteFile(idArchivo: number, idPlanManejoArchivo: number) {
    return forkJoin([
      this.eliminarArchivo(idArchivo, this.user.idUsuario),
      this.eliminarRelacionArchivo(idPlanManejoArchivo, this.user.idUsuario),
    ]).pipe(map(() => null));
  }

  eliminarRelacionArchivo(
    idPlanManejoArchivo: number,
    idUsuarioElimina: number
  ) {
    const item = new PlanManejoArchivo({
      idPlanManejoArchivo,
      idUsuarioElimina,
    });
    return this.apiOrdenamiento.eliminarArchivo(item);
  }

  eliminarArchivo(idArchivo: number, idUsuarioElimina: number) {
    return this.apiArchivo.eliminarArchivo(idArchivo, idUsuarioElimina);
  }

  obtenerArchivoMapa(): Observable<string> {
    const item = {
      codigoProceso: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: ArchivoTipoCodigo.SHAPEFILE,
    };
    return this.obtenerRelacionArchivo(item)
      .pipe(
        tap((res) => {
          if (!isNull(res)) {
            const { idArchivo, idPlanManejoArchivo } = res;
            this.relacionArchivo = { idArchivo, idPlanManejoArchivo };
          }
        })
      )
      .pipe(map((res) => (res?.documento ? res.documento : '')));
  }

  obtenerRelacionArchivo(item: any) {
    return this.anexosService.listarPlanManejoListar(item);
  }

  getInitData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      base64: this.obtenerArchivoMapa(),
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => {
        const layers = this.setLayer(this.listOrdenInterno);
        if (!isNullOrEmpty(res.base64)) {
          this.map.addBase64FileWithConfig(res.base64, layers);
        }
      });
  }

  setLayer(list: any): LayerView[] {
    let layers: LayerView[] = [];
    for (const item of list) {
      if (!isNullOrEmpty(item.actividad) && !isNullOrEmpty(item.descripcion)) {
        const layersId = item.actividad.split(';');
        const layersName = item.descripcion.split(';');
        for (let index = 0; index < layersId.length; index++) {
          const layerId = layersId[index];
          const title = layersName[index];
          const color = item.observacionDetalle;
          const groupId = item.actividadesRealizar;
          const layer: LayerView = {
            color,
            groupId,
            layerId,
            title,
            area: 0,
            features: [],
          };
          layers.push(layer);
        }
      }
    }
    return layers;
  }
}
