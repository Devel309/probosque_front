import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaPorAccordeonComponent } from './mapa-por-accordeon.component';

describe('MapaPorAccordeonComponent', () => {
  let component: MapaPorAccordeonComponent;
  let fixture: ComponentFixture<MapaPorAccordeonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapaPorAccordeonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaPorAccordeonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
