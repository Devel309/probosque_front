import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  Input,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { OrganizacionDesarrolloModel } from 'src/app/model/OrganizacionDesarrolloActividad';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { FileModel } from 'src/app/model/util/File';
import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';
import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import SimpleLineSymbol from '@arcgis/core/symbols/SimpleLineSymbol';
import SimpleFillSymbol from '@arcgis/core/symbols/SimpleFillSymbol';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import Color from '@arcgis/core/Color';
import * as geometryEngine from '@arcgis/core/geometry/geometryEngine';
import { Table } from 'primeng/table';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ArchivoService,
  OrdenamientoInternoPmfiService,
  UsuarioService,
} from '@services';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { OrdenamientoProteccionService } from '../../../../../service/planificacion/plan-operativo-concesiones-maderables/ordenamiento-proteccion.service';
import { concatMap, finalize, map, tap } from 'rxjs/operators';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { OrdenamientoProteccion } from '../../../../../model/PlanManejo/Pocc/OrdenamientoProteccion';
import {
  CodigoPOCCTabs,
  CodigoPOOC,
} from '../../../../../model/util/POCC/CodigoPOOC';
import { OrdenamientoProteccionDet } from '../../../../../model/PlanManejo/Pocc/OrdenamientoProteccionDet';
import { ResumenActividadDet } from '../../../../../model/PlanManejo/ResumenActividades/ResumenActividadDet';
import {
  AppMapaComponent,
  ArchivoTipo,
  isNull,
  isNullOrEmpty,
  LayerView,
  onlySemicolons,
  PGMFArchivoTipo,
  RespuestaTipo,
  setOneSemicolon,
  ToastService,
} from '@shared';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { forkJoin, Observable, of } from 'rxjs';
import { PGMFArchivoDto, PlanManejoArchivo } from '@models';
import { MapaPorAccordeonComponent } from './components/mapa-por-accordeon/mapa-por-accordeon.component';
import { MapPorAcordeonComponent } from './components/map-por-acordeon/map-por-acordeon.component';
declare const shp: any;

@Component({
  selector: 'app-tab-ordenamiento-parcela-proteccion',
  templateUrl: './tab-ordenamiento-parcela-proteccion.component.html',
  styleUrls: ['./tab-ordenamiento-parcela-proteccion.component.scss'],
})
export class TabOrdenamientoParcelaProteccionComponent implements OnInit {
  tituloModal: string = 'Registrar Organizacion Desarrollo Actividad';
  displayBasic: boolean = false;
  @Input() disabled!: boolean;
  public filFile: any = null;
  public geoJsonLayer: any = null;
  totalArea: number = 0;
  totalPorcentaje: number = 0;
  objBosque: any = {};
  paramsNuevo: any;
  paramsEdicion: any;

  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;

  isEditedMap = false;

  provigId: number = 0;
  catordId: number = 0;
  idPlanManejoArchivo: number = 0;

  anexo: boolean = false;
  frenteCorta: boolean = true;
  categoriaOrdenamiento: boolean = true;

  public _id = TabOrdenamientoParcelaProteccionComponent.Guid2.newGuid;
  organizacion = {} as OrganizacionDesarrolloModel;
  organizaciones = [] as OrganizacionDesarrolloModel[];
  categoriaOrdenamientoCoverturBoscosa: any[] = [];
  coverturBoscosaNuevos: any[] = [];
  coverturBoscosaEdicion: any[] = [];
  listProteccionVigilanciaNuevos: any[] = [];
  listProteccionVigilanciaEdicion: any[] = [];
  proteccionVigilancia: any[] = [];
  filFiles: FileModel[] = [];
  selectAnexo: string = '';

  title1 = [
    {
      modificar: false,
      descripcion: 'Con Cobertura Boscosa',
    },
  ];
  title2 = [
    {
      modificar: false,
      descripcion: 'Sin Cobertura Boscosa',
    },
  ];

  codigoAmpliacionAnexo3: string = 'AMPL_ANEX_O3';

  edit: boolean = false;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();
  public view: any = null;

  @Input() idPlanManejo!: number;

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild('map2', { static: true }) private mapViewElement!: ElementRef;
  @ViewChild('ulResult', { static: true }) private ulResult!: ElementRef;
  @ViewChild('tablaCatOrdenamiento', { static: false })
  private tablaCatOrdenamiento!: Table;

  idUsuarioRegistro: any;

  ordenamientoCabecera: OrdenamientoProteccion = new OrdenamientoProteccion({
    codTipoOrdenamiento: CodigoProceso.PLAN_OPERATIVO_CONCESIONES,
    subCodTipoOrdenamiento: CodigoPOOC.TAB_5,
    idUsuarioRegistro: this.usuarioService.idUsuario,
    idPlanManejo: this.idPlanManejo,
    anexo: this.anexo ? 'S' : 'N',
    idOrdenamientoProteccion: 0
  });

  listOrdenamientoProdDet_2_1: OrdenamientoProteccionDet[] = [];

  ampliacionAnexo: OrdenamientoProteccionDet = new OrdenamientoProteccionDet({
    idOrdenamientoProteccionDet: 0,
    codigoTipoOrdenamientoDet: this.codigoAmpliacionAnexo3,
    idUsuarioRegistro: this.usuarioService.idUsuario,
  });

  listOrdenamientoProdDet_2_3: OrdenamientoProteccionDet[] = [];

  relacionArchivo!: { idArchivo: number; idPlanManejoArchivo: number };
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;

  @ViewChild('map1', { static: true }) map1!: MapaPorAccordeonComponent;
  //@ViewChild('map2', { static: true }) map2!: MapPorAcordeonComponent;

  listOrdenInterno: any[] = [];
  RespuestaTipo = RespuestaTipo;

  constructor(
    private dialog: MatDialog,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private planificacionService: PlanificacionService,
    private usuarioService: UsuarioService,
    private ordenamientoProteccionService: OrdenamientoProteccionService,
    private toast: ToastService,
    private postulacionPFDMService: PostulacionPFDMService,
    private apiArchivo: ArchivoService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService
  ) {}

  ngOnInit(): void {
    this.idUsuarioRegistro = this.usuarioService.idUsuario;
    this.listarOrdenamientoInternoDetalle();
    //this.listarOrdenamientoProteccion();
  }

  listarOrdenamientoInternoDetalle() {
    this.listOrdenamientoProdDet_2_1 = [];
    this.listOrdenamientoProdDet_2_3 = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: CodigoProceso.PLAN_OPERATIVO_CONCESIONES, //"POCC"
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ordenamientoProteccionService
      .listarOrdenamientoInternoDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.data && response.data.length >= 0) {
            let res = response.data[0];

            this.anexo = res.anexo == 'S' ? true : false;
            //this.ordenamiento_2_1.codTipoOrdenamiento = res.codTipoOrdenamiento;
            this.ordenamientoCabecera.idPlanManejo = res.idPlanManejo
              ? res.idPlanManejo
              : this.idPlanManejo;
            this.ordenamientoCabecera.idOrdenamientoProteccion =
              res.idOrdenamientoProteccion ? res.idOrdenamientoProteccion : 0;
            this.ordenamientoCabecera.idUsuarioRegistro = res.idUsuarioRegistro
              ? res.idUsuarioRegistro
              : this.usuarioService.idUsuario;

            let list_1 = res.listOrdenamientoProteccionDet.filter(
              (e: any) => e.codigoTipoOrdenamientoDet == CodigoPOOC.ACORDEON_5_1
            );
            let list_anexo_3 = res.listOrdenamientoProteccionDet.filter(
              (e: any) =>
                e.codigoTipoOrdenamientoDet == this.codigoAmpliacionAnexo3
            );
            let list_3 = res.listOrdenamientoProteccionDet.filter(
              (e: any) => e.codigoTipoOrdenamientoDet == CodigoPOOC.ACORDEON_5_3
            );

            list_1.forEach((item: any) => {
              if (item.idOrdenamientoProteccionDet == null)
                item.idOrdenamientoProteccionDet = 0;
              if (item.idUsuarioRegistro == null)
                item.idUsuarioRegistro = this.usuarioService.idUsuario;

              this.listOrdenamientoProdDet_2_1.push(
                new OrdenamientoProteccionDet({
                  idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                  codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
                  categoria: item.categoria,
                  areaHA: item.areaHA,
                  areaHAPorcentaje: item.areaHAPorcentaje,
                  idUsuarioRegistro: item.idUsuarioRegistro,
                  accion: item.accion ? item.accion :false
                })
              );
            });

            if (list_anexo_3) {
              if (list_anexo_3.length == 1) {
                this.ampliacionAnexo.idOrdenamientoProteccionDet =
                  list_anexo_3[0].idOrdenamientoProteccionDet;
                this.ampliacionAnexo.categoria = list_anexo_3[0].categoria;
              }
            }

            this.calcularTotal_5_1();

            list_3.forEach((item: any) => {
              if (item.idOrdenamientoProteccionDet == null)
                item.idOrdenamientoProteccionDet = 0;
              if (item.idUsuarioRegistro == null)
                item.idUsuarioRegistro = this.usuarioService.idUsuario;

              this.listOrdenamientoProdDet_2_3.push(
                new OrdenamientoProteccionDet({
                  idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                  codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
                  categoria: item.categoria,
                  descripcion: item.descripcion,
                  idUsuarioRegistro: item.idUsuarioRegistro,
                })
              );
            });
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  calcularTotal_5_1() {
    let totalArea = 0;
    let totalPorcentaje = 0;

    this.listOrdenamientoProdDet_2_1.forEach(
      (item: OrdenamientoProteccionDet) => {
        let area = item.areaHA ? item.areaHA : 0;
        let porcentaje = item.areaHAPorcentaje ? item.areaHAPorcentaje : 0;

        totalArea = totalArea + area;
        totalPorcentaje = totalPorcentaje + porcentaje;
      }
    );

    this.totalArea = totalArea;
    this.totalPorcentaje = totalPorcentaje;
  }

  eliminar(
    event: any,
    index: number,
    resumenDet: OrdenamientoProteccionDet,
    codigoAcordeon: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
      //

        if (codigoAcordeon == CodigoPOOC.ACORDEON_5_1) {
          if (resumenDet.idOrdenamientoProteccionDet == 0) {
            this.listOrdenamientoProdDet_2_1.splice(index, 1);
            this.calcularTotal_5_1();
          } else {
            let params = {
              idOrdenamientoProtecccion: 0,
              idOrdenamientoProteccionDet:
                resumenDet.idOrdenamientoProteccionDet,
              idUsuarioElimina: this.usuarioService.idUsuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.ordenamientoProteccionService
              .eliminarOrdenamientoInternoDetalle(params)
              .pipe(finalize(() => this.dialog.closeAll()))
              .subscribe((result: any) => {
                this.toast.ok(result.message);
                this.listOrdenamientoProdDet_2_1.splice(index, 1);
                this.calcularTotal_5_1();
              });
          }
        } else if (codigoAcordeon == CodigoPOOC.ACORDEON_5_3) {
          if (resumenDet.idOrdenamientoProteccionDet == 0) {
            this.listOrdenamientoProdDet_2_3.splice(index, 1);
          } else {
            let params = {
              idOrdenamientoProtecccion: 0,
              idOrdenamientoProteccionDet:
                resumenDet.idOrdenamientoProteccionDet,
              idUsuarioElimina: this.usuarioService.idUsuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.ordenamientoProteccionService
              .eliminarOrdenamientoInternoDetalle(params)
              .pipe(finalize(() => this.dialog.closeAll()))
              .subscribe((result: any) => {
                this.toast.ok(result.message);
                this.listOrdenamientoProdDet_2_3.splice(index, 1);
              });
          }
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  listarOrdenamientoProteccion() {
    this.categoriaOrdenamientoCoverturBoscosa = [];
    this.proteccionVigilancia = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planificacionService.listarOrdenamientoProteccion(params).subscribe(
      (response: any) => {
       
        /*   this.title1.forEach(res =>{
          this.categoriaOrdenamientoCoverturBoscosa.push(res)
        }) */

        response.data.forEach((element: any) => {
          if (
            element?.codTipoOrdenamiento?.toUpperCase() ===
            'PROVIG'.toUpperCase()
          ) {
            this.provigId = element.idOrdenamientoProteccion;
          } else if (
            element?.codTipoOrdenamiento?.toUpperCase() ===
            'CATORD'.toUpperCase()
          ) {
            this.catordId = element.idOrdenamientoProteccion;
            this.selectAnexo = element.anexo;
          }

          element.listOrdenamientoProteccionDet.forEach((x: any) => {
            x.esActualizar = x.idOrdenamientoProtecccion != null;

            if (
              x.codigoTipoOrdenamientoDet.toUpperCase() ===
              'COCOBO'.toUpperCase()
            ) {
              this.categoriaOrdenamientoCoverturBoscosa.push(x);
            } else if (
              x.codigoTipoOrdenamientoDet.toUpperCase() ===
              'SNCOBO'.toUpperCase()
            ) {
              /* this.title2.forEach(res =>{
                  this.categoriaOrdenamientoCoverturBoscosa.push(res)
                }) */
              this.categoriaOrdenamientoCoverturBoscosa.push(x);
            } else if (
              x.codigoTipoOrdenamientoDet.toUpperCase() ===
              'PROVIG'.toUpperCase()
            ) {
              this.proteccionVigilancia.push(x);
            }
          });
        });

        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const map = new Map({
      basemap: 'hybrid',
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }
  static get Guid2() {
    return class Guid2 {
      constructor() {}
      static get newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
          /[xy]/g,
          function (c) {
            var r = (Math.random() * 16) | 0,
              v = c == 'x' ? r : (r & 0x3) | 0x8;
            return v.toString(16);
          }
        );
      }
    };
  }

  initializeMap2(): void {
    const container = this.mapViewElement.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const map2 = new Map({
      basemap: 'hybrid',
    });

    const view = new MapView({
      container,
      map: map2,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }
  static get Guid() {
    return class Guid {
      constructor() {}
      static get newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
          /[xy]/g,
          function (c) {
            var r = (Math.random() * 16) | 0,
              v = c == 'x' ? r : (r & 0x3) | 0x8;
            return v.toString(16);
          }
        );
      }
    };
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'warn',
      key: 'tl',
      summary: '',
      detail: mensaje,
    });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      key: 'tl',
      summary: '',
      detail: mensaje,
    });
  }

  cerrarModal() {
    this.organizacion = {} as OrganizacionDesarrolloModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = 'Registrar Organizacion';
  }

  // cargar archivo //

  loadShapeFile(item: any) {
    return this.getItem(item)
      .then((data) => {
        return new Promise((resolve, reject) => {
          let fileReader = new FileReader();
          fileReader.onload = (e: any) => {
            resolve(e.target.result);
          };
          fileReader.onerror = fileReader.onabort = reject;
          fileReader.readAsArrayBuffer(data);
        });
      })
      .then((data) => {
        return shp(data);
      })
      .then((data) => {
        if (Array.isArray(data) === false) data = [data];
        data.forEach((t: any) => (t.title = t.fileName));
        return data;
      })
      .catch((error) => {
        throw error;
      });
  }
  getItem(item: any) {
    let promise = Promise.resolve(item);
    if (typeof item === 'string')
      promise = fetch(item).then((data) => {
        return data.blob();
      });
    return promise;
  }

  onFileChange(e: any, catOrd: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.target.files[0];

    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          this.processFile(e.target.files[i], e, catOrd, true);
          i++;
        }
      }
    }
  }
  processFile(file: any, e: any, catOrd: any, modificar: Boolean) {
    let itemFile = file;
    try {
      const file_ = {} as FileModel;
      let val = 0;
      for (let item of this.filFiles) {
        if (item.descripcion == catOrd) {
          item.descripcion = catOrd;
          item.file = file;
          // item.nombreFile=this.file.name;
        }
      }
      if (val == 0) {
        file_.descripcion = catOrd;
        file_.file = file;
        // file.nombreFile=this.filFile.name;
        this.filFiles.push(file_);
      }

      let promise = Promise.resolve([]);
      promise = this.loadShapeFile(itemFile);
      promise
        .then((data) => {
          this.createLayers(data, file);
          this.calculateArea(data, e, catOrd, modificar);
        })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      console.log(error);
    }
  }

  calculateArea(data: any, e: any, catOrd: any, modificar: Boolean) {
    let geometry: any = null;
    geometry = { spatialReference: { wkid: 4326 } };
    geometry.rings = data[0].features[0].geometry.coordinates;
    let area = geometryEngine.geodesicArea(geometry, 'hectares');

    if (e == null) return;

    let txtArea = e.path[3].cells[4];
    let sum1 = 0;
    let sum2 = 0;
    let table = this.tablaCatOrdenamiento.tableViewChild.nativeElement;
    let body = table.querySelector('tbody');
    let footer = table.querySelector('tfoot');
    let tds = footer.querySelectorAll('td');
    let trs = body.querySelectorAll('tr');
    let total = tds[3];
    let totalPorcentaje = tds[4];
    let calculo = 0;
    let sum = 0;
    
    for (let item of this.categoriaOrdenamientoCoverturBoscosa) {
      if (item.idOrdenamientoProteccionDet == catOrd) {
        item.areaHA = Number(area.toFixed(2));
        item.modificar = modificar;
        //  item.porcentaje=Number((area/sum1).toFixed(2));
        item.file = true;
      }
      sum1 += item.areaHA;
    }

    for (let item of this.categoriaOrdenamientoCoverturBoscosa) {
      item.areaHAPorcentaje = Number(((100 * item.areaHA) / sum1).toFixed(2));
    }
    //this.totalArea = `${sum1.toFixed(2)}`;
    //this.totalPorcentaje = `${(100).toFixed(2)} %`;
  }

  createLayers(layers: any, file: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.random();
      t.file = file;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      this.createLayer(t);
    });
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: 'Es una demo',
    };
    this.geoJsonLayer = new GeoJSONLayer({
      url: url,
      outFields: ['*'],
    });
    this.geoJsonLayer.visible = true;
    this.geoJsonLayer.id =
      TabOrdenamientoParcelaProteccionComponent.Guid2.newGuid;
    this.geoJsonLayer.ID = this.geoJsonLayer.id;
    this.geoJsonLayer.title = item.title;
    this.geoJsonLayer.layerType = 'vector';
    this.geoJsonLayer.groupId = this._id;
    this.geoJsonLayer.color = item.color;
    this.geoJsonLayer.opacity = item.opacity;
    this.geoJsonLayer.attributes = item.features;
    this.geoJsonLayer.file = item.file;
    this.geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.changeLayerStyle(this.geoJsonLayer.id, this.geoJsonLayer.color);
        this.view.goTo({ target: data.fullExtent });
        //this.createTreeLayers();
      })
      .catch((error: any) => {
        console.log(error);
      });
    this.view.map.add(this.geoJsonLayer);
  }

  random() {
    var length = 6;
    var chars = '0123456789ABCDEF';
    var hex = '#';
    while (length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }

  changeLayerStyle(id: any, color: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer === null) return;
    layer.color = color;
    let type = layer.geometryType;
    let style: any = null;
    if (type === 'point' || type === 'multipoint') {
      style = new SimpleMarkerSymbol();
      style.size = 10;
    } else if (type === 'polyline') {
      style = new SimpleLineSymbol();
      style.width = 2;
    } else if (type === 'polygon' || type === 'extent') {
      style = new SimpleFillSymbol();
      style.outline = new SimpleLineSymbol();
      style.outline.width = 2;
    }
    style.color = Color.fromHex(color);
    layer.renderer = new SimpleRenderer({ symbol: style });
  }

  parameters() {
    this.coverturBoscosaNuevos = [];
    this.listProteccionVigilanciaNuevos = [];
    this.coverturBoscosaEdicion = [];
    this.listProteccionVigilanciaEdicion = [];

    if (this.catordId != 0 && this.provigId != 0) {
      this.categoriaOrdenamientoCoverturBoscosa.forEach((x: any) => {
        this.coverturBoscosaEdicion.push({
          idOrdenamientoProteccionDet: x.idOrdenamientoProteccionDet,
          codigoTipoOrdenamientoDet: x.codigoTipoOrdenamientoDet,
          categoria: x.categoria,
          actividad: '',
          meta: '',
          areaHA: x.areaHA,
          areaHAPorcentaje: x.areaHAPorcentaje,
          estado: 'A',
          idUsuarioModificacion: this.idUsuarioRegistro,
        });
      });

      this.proteccionVigilancia.forEach((x: any) => {
        this.listProteccionVigilanciaEdicion.push({
          idOrdenamientoProteccionDet: x.idOrdenamientoProteccionDet,
          codigoTipoOrdenamientoDet: x.codigoTipoOrdenamientoDet,
          categoria: '',
          actividad: x.actividad,
          meta: x.meta,
          areaHA: 0,
          areaHAPorcentaje: 0,
          estado: 'A',
          idUsuarioModificacion: this.idUsuarioRegistro,
        });
      });

      this.paramsEdicion = [
        {
          idOrdenamientoProteccion: this.catordId,
          idPlanManejo: this.idPlanManejo,
          codTipoOrdenamiento: 'CATORD',
          anexo: this.selectAnexo,
          estado: 'A',
          idUsuarioModificacion: this.idUsuarioRegistro,
          listOrdenamientoProteccionDet: this.coverturBoscosaEdicion,
        },
        {
          idOrdenamientoProteccion: this.provigId,
          idPlanManejo: this.idPlanManejo,
          codTipoOrdenamiento: 'PROVIG',
          anexo: '',
          estado: 'A',
          idUsuarioModificacion: this.idUsuarioRegistro,
          listOrdenamientoProteccionDet: this.listProteccionVigilanciaEdicion,
        },
      ];
    } else {
      this.categoriaOrdenamientoCoverturBoscosa
        .filter((x: any) => x.idOrdenamientoProteccion === null)
        .forEach((x: any) => {
          this.coverturBoscosaNuevos.push({
            codigoTipoOrdenamientoDet: x.codigoTipoOrdenamientoDet,
            categoria: x.categoria,
            actividad: '',
            meta: '',
            areaHA: x.areaHA,
            areaHAPorcentaje: x.areaHAPorcentaje,
            idUsuarioRegistro: this.idUsuarioRegistro,
          });
        });

      this.proteccionVigilancia
        .filter((x: any) => x.idOrdenamientoProteccion === null)
        .forEach((x: any) => {
          this.listProteccionVigilanciaNuevos.push({
            codigoTipoOrdenamientoDet: x.codigoTipoOrdenamientoDet,
            categoria: '',
            actividad: x.actividad,
            meta: x.meta,
            areaHA: 0,
            areaHAPorcentaje: 0,
            idUsuarioRegistro: this.idUsuarioRegistro,
          });
        });

      this.paramsNuevo = [
        {
          idPlanManejo: this.idPlanManejo,
          codTipoOrdenamiento: 'CATORD',
          anexo: this.selectAnexo,
          idUsuarioRegistro: this.idUsuarioRegistro,
          listOrdenamientoProteccionDet: this.coverturBoscosaNuevos,
        },
        {
          idPlanManejo: this.idPlanManejo,
          codTipoOrdenamiento: 'PROVIG',
          anexo: '',
          idUsuarioRegistro: this.idUsuarioRegistro,
          listOrdenamientoProteccionDet: this.listProteccionVigilanciaNuevos,
        },
      ];
    }
  }

  validar = (): boolean => {
    let esValido = true;
    let mensaje = '';
    if (
      this.categoriaOrdenamientoCoverturBoscosa.filter(
        (x: any) =>
          (x.areaHA != null && x.areaHAPorcentaje != null) ||
          (x.areaHA != '' && x.areaHAPorcentaje != '') ||
          (x.areaHA != 0 && x.areaHAPorcentaje != 0)
      ).length === 0
    ) {
      mensaje += '(*) Debe ingresar: al menos un Área y porcentaje.\n';
      esValido = false;
    } else if (
      this.categoriaOrdenamientoCoverturBoscosa.filter(
        (x: any) =>
          x.areaHA != null &&
          x.areaHAPorcentaje != null &&
          x.areaHAPorcentaje != 0 &&
          x.areaHAPorcentaje != ''
      ).length === 0
    ) {
      mensaje += '(*) Debe ingresar: Porcentaje.\n';
      esValido = false;
    }

    if (this.selectAnexo === null || this.selectAnexo === '') {
      mensaje += '(*) Debe seleccionar: Ampliación información Anexo 3\n';
      esValido = false;
    }

    if (
      this.proteccionVigilancia.filter(
        (x: any) =>
          x.meta != null && x.meta != '' && x.meta != undefined && x.meta != 0
      ).length === 0
    ) {
      mensaje += '(*) Debe ingresar: al menos unas metas\n';
      esValido = false;
    }

    if (mensaje != '') this.ErrorMensaje(mensaje);

    return esValido;
  };
  seleccionar(event: any, data: any, index: number){
        console.log("event", event);

  }

  guardar() {
    const cabecera = new OrdenamientoProteccion({
      codTipoOrdenamiento: CodigoProceso.PLAN_OPERATIVO_CONCESIONES,
      subCodTipoOrdenamiento: CodigoPOOC.TAB_5,
      idUsuarioRegistro: this.usuarioService.idUsuario,
      idPlanManejo: this.idPlanManejo,
      anexo: this.anexo ? 'S' : 'N',
      idOrdenamientoProteccion: this.ordenamientoCabecera.idOrdenamientoProteccion
    });

    let param = [];

    cabecera.listOrdenamientoProteccionDet =
      this.listOrdenamientoProdDet_2_1.concat(
        this.listOrdenamientoProdDet_2_3,
        [this.ampliacionAnexo]
      );
    param.push(cabecera);

    //this.map1.guardar();
    //this.map2.guardar();

    this.ordenamientoProteccionService
      .registrarOrdenamientoInterno(param)

      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok(response.message);
          this.listarOrdenamientoInternoDetalle();
        } else {
          this.toast.error(response.message);
        }
      });



    /*
    if (!this.validar()) return;
    this.parameters();

    this.dialog.open(LoadingComponent, { disableClose: true });


    if (this.catordId != 0 && this.provigId != 0) {
      this.planificacionService
        .actualizarOrdenamientoProteccion(this.paramsEdicion)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.SuccessMensaje(response.message);
              this.listarOrdenamientoProteccion();
            } else this.ErrorMensaje(response.message);
          },
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          }
        );
    } else {
      this.planificacionService
        .registrarOrdenamientoProteccion(this.paramsNuevo)
        .subscribe(
          (response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.SuccessMensaje(response.message);
              this.listarOrdenamientoProteccion();
            } else this.ErrorMensaje(response.message);
          },
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          }
        );
    }
    */
  }

  abrirModal() {
    this.displayBasic = true;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.usuarioService.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }
}
