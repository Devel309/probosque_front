import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ModalFormularioCronogramaComponent } from "./modal/modal-formulario-zonas/modal-formulario-cronograma.component";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastService } from "@shared";

@Component({
  selector: "app-tab-cronograma",
  templateUrl: "./tab-cronograma.component.html",
  styleUrls: ["./tab-cronograma.component.scss"],
})
export class TabCronogramaComponent implements OnInit {
  @Input() idPGMF!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  ref!: DynamicDialogRef;
  context: Modelo = new Modelo();

  verAnio1: boolean = false;
  verAnio2: boolean = false;
  verAnio3: boolean = false;

  lstDetalle: Modelo[] = [];
  lstOtrosAnos: any[] = [];
  params: any;
  vigencia: Number = 0;

  UrlFormatos = UrlFormatos


  cmbAnios: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [{ label: "Año 2", value: "2-0" }],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [{ label: "Año 3", value: "3-0" }],
    },
  ];

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private toast: ToastService,
  ) {}

  ngOnInit(): void {
    this.listarCronogramaActividad();
  }

  listarCronogramaActividad() {
    this.lstDetalle = [];

    var params = {
      codigoProceso: "POCC",
      idCronogramaActividad: null,
      idPlanManejo: this.idPGMF,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((x: any) => {
            this.lstDetalle.push(x);
          });
          this.marcar();
        }
      });
  }

  marcar() {
    var aniosMeses: any[] = [];

    this.lstDetalle.forEach((x: any, index) => {
      

      aniosMeses = [];

      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });

      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstDetalle[index] = this.context;
    });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "POCC",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó el cronograma de actividades correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó la actividad del cronograma correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    this.ref = this.dialogService.open(ModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      this.lstDetalle.push(this.context);
    }
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail:
                    "Se eliminó la actividad del cronograma correctamente.",
                });
                this.listarCronogramaActividad();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  detail: "Ocurrió un problema, intente nuevamente.",
                });
              }
            });
        } else {
          this.lstDetalle.splice(
            this.lstDetalle.findIndex((x) => x.id == data.id),
            1
          );
        }
      },
      reject: () => {},
    });
  }
  cargarFormato(files: any){
    

    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Hoja1",
          numeroFila: 3,
          numeroColumna: 1,
          codigoProceso: "POCC",
          codigoActividad: "AAEAM",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: this.user.idUsuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cronogrmaActividadesService
          .registrarCronogramaActividadExcel(t.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.codigoProceso, item.codigoActividad, item.idPlanManejo, item.idUsuarioRegistro)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listarCronogramaActividad();
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }

  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.tipoActividad = data.tipoActividad);
      return;
    }
  }

  showRemove: boolean = true;
  tipoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m1: boolean = false;
  a1m2: boolean = false;
  a1m3: boolean = false;
  a1m4: boolean = false;
  a1m5: boolean = false;
  a1m6: boolean = false;
  a1m7: boolean = false;
  a1m8: boolean = false;
  a1m9: boolean = false;
  a1m10: boolean = false;
  a1m11: boolean = false;
  a1m12: boolean = false;

  a2m1: boolean = false;

  a3m1: boolean = false;

  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-1") {
        this.a1m1 = x.aniosMeses.indexOf("1-1") > -1;
      } else if (x.aniosMeses == "1-2") {
        this.a1m2 = x.aniosMeses.indexOf("1-2") > -1;
      } else if (x.aniosMeses == "1-3") {
        this.a1m3 = x.aniosMeses.indexOf("1-3") > -1;
      } else if (x.aniosMeses == "1-4") {
        this.a1m4 = x.aniosMeses.indexOf("1-4") > -1;
      } else if (x.aniosMeses == "1-5") {
        this.a1m5 = x.aniosMeses.indexOf("1-5") > -1;
      } else if (x.aniosMeses == "1-6") {
        this.a1m6 = x.aniosMeses.indexOf("1-6") > -1;
      } else if (x.aniosMeses == "1-7") {
        this.a1m7 = x.aniosMeses.indexOf("1-7") > -1;
      } else if (x.aniosMeses == "1-8") {
        this.a1m8 = x.aniosMeses.indexOf("1-8") > -1;
      } else if (x.aniosMeses == "1-9") {
        this.a1m9 = x.aniosMeses.indexOf("1-9") > -1;
      } else if (x.aniosMeses == "1-10") {
        this.a1m10 = x.aniosMeses.indexOf("1-10") > -1;
      } else if (x.aniosMeses == "1-11") {
        this.a1m11 = x.aniosMeses.indexOf("1-11") > -1;
      } else if (x.aniosMeses == "1-12") {
        this.a1m12 = x.aniosMeses.indexOf("1-12") > -1;
      } else if (x.aniosMeses == "2-0") {
        this.a2m1 = x.aniosMeses.indexOf("2-0") > -1;
      } else if (x.aniosMeses == "3-0") {
        this.a3m1 = x.aniosMeses.indexOf("3-0") > -1;
      }
    });
  }

}
