import { Component, OnInit } from "@angular/core";
import { PlanManejoService } from "@services";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-info-contrato",
  templateUrl: "./modal-info-contrato.component.html",
})
export class ModalInfoContratoComponent implements OnInit {
  queryContratos: string = "";
  comboListContratos: any[] = [];
  selectContrato: any;
  selectedValues: any[] = [];
  queryIdContratos: string = "";
  contrato: any[] = [];

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private planManejoService: PlanManejoService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.listContratos();
    this.obtenerPlanManejo();
  }

  onChangeContratos(event: any, data: any){
    if (event.checked === true && this.selectedValues.length != 0) {
      this.selectedValues = [data];
      this.selectedValues.forEach(response => {
        this.selectContrato = response
      })
    }
  }
  obtenerPlanManejo() {
    var params = {
      idPlanManejo: this.config.data.idPlanManejo,
    };
    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((response: any) => {
        if (!!response.data.idPlanManejoPadre) {
          this.filtrarIdPlanPadre(response.data.idPlanManejoPadre);
        }
      });
  }

  filtrarIdPlanPadre(idPlanManejoPadre: number) {
    this.selectedValues = []
    this.contrato = this.comboListContratos.filter(
      (x) => idPlanManejoPadre == x.idPlanManejo
    );
    this.contrato.forEach((response) => {
      this.selectContrato = response;
      this.selectedValues.push(this.selectContrato)
    });
  }

  filtrarContrato() {
    if (this.queryContratos) {
      this.comboListContratos = this.comboListContratos.filter(
        (r) => r.idPlanManejo == this.queryContratos
      );
    } else if (this.queryIdContratos) {
      this.filtrarId();
    } else {
      this.listContratos();
    }
  }

  filtrarId() {
    this.selectedValues = []
    if (this.queryIdContratos) {
      this.comboListContratos = this.comboListContratos.filter(
        (r) => r.idContrato == this.queryIdContratos
      );
      this.contrato.forEach((response) => {
        this.selectContrato = response;
        this.selectedValues.push(this.selectContrato)
      });
    } else {
      this.listContratos();
    }
  }

  listContratos() {
    var params = {
      nroDocumento: this.config.data.nroDocumento,
    };
    this.planManejoService.listarPorFiltroPlanManejoContrato(params).subscribe(
      (response: any) => {
        if (response.success == true) {
          this.comboListContratos = [...response.data];
          if (!!this.config.data.idPlanManejoPadre) {
            this.filtrarIdPlanPadre(this.config.data.idPlanManejoPadre);
          }
        }
      },
      (error) => {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    );
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectContrato == null || this.selectContrato == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un contrato.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  agregar = () => {
    this.ref.close(this.selectContrato);
  };

  cerrarModal() {
    this.ref.close();
  }
}
