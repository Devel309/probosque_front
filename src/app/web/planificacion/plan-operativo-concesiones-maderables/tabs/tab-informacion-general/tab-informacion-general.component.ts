import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  DepartamentoModel,
  DistritoModel,
  ProvinciaModel,
  RegenteForestalModel,
} from "@models";

import { MessageService } from "primeng/api";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import * as moment from "moment";
import {
  CoreCentralService,
  PlanManejoService,
  UsuarioService,
} from "@services";
import { HttpParams } from "@angular/common/http";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { ToastService } from "@shared";
import { FileModel } from "src/app/model/util/File";
import {
  infoGenralDetalle,
  PoccInformacionGeneral,
  RegenteForestal,
} from "src/app/model/pocc-informacion-general";
import { InformacionGeneralPOCMService } from "src/app/service/plan-operativo-concesion-maderable/informacion-general.service";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { finalize } from "rxjs/operators";
import { DecimalPipe } from "@angular/common";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ModalInfoContratoComponent } from "./modal-info-contrato/modal-info-contrato.component";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { ActividadesAprovechamientoService } from "src/app/service/actividades-aprovechamiento-pocc.service";

@Component({
  selector: "app-tab-informacion-general",
  templateUrl: "./tab-informacion-general.component.html",
  styleUrls: ["./tab-informacion-general.component.scss"],
})
export class TabInformacionGeneralComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  @Input() idPlanManejo!: number;

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  form: PoccInformacionGeneral = new PoccInformacionGeneral();
  listDepartamento: DepartamentoModel[] = [];
  departamento = {} as DepartamentoModel;

  listProvincia: ProvinciaModel[] = [];
  provincia = {} as ProvinciaModel;

  listDistrito: DistritoModel[] = [];
  distrito = {} as DistritoModel;

  disabledSave: boolean = false;

  editForm: boolean = false;
  contratoSuscrito: string | null = "";
  numeroInscripcion: string = "";

  selectRegente: RegenteForestalModel = new RegenteForestalModel();
  verModalRegente: boolean = false;
  queryRegente: string = "";
  regentes: any[] = [];
  nombreCompleto: string = "";

  minDate = moment(new Date()).format("YYYY-MM-DD");
  maxDateFinal: any;
  minDateFinal = moment(new Date()).add("days", 1).format("YYYY-MM-DD");

  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  idArchivo: number = 0;
  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;

  verEnviar1: boolean = false;
  autoCompletado: boolean = false;
  vigenciaIncial: number = 0;
  vigenciaFinal: number = 0;
  areaTotal: number = 0;
  idPlanManejoPadre: number = 0;
  showWarn: boolean = true;

  ref!: DynamicDialogRef;
  constructor(
    private informacionGeneralPOCMService: InformacionGeneralPOCMService,
    private messageService: MessageService,
    private serv_forestal: ApiForestalService,
    private user: UsuarioService,
    private anexosService: AnexosService,
    private toastService: ToastService,
    private servCoreCentral: CoreCentralService,
    private dialog: MatDialog,
    private _decimalPipe: DecimalPipe,
    public dialogService: DialogService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private actividadesAprovechamientoService: ActividadesAprovechamientoService
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";
    // this.obtenerInformacionProcesoPostulacion();
    this.listarPorFiltroDepartamentoIni();
    this.listarInformacionGeneral();
    this.getVolumen();
    this.obtenerTotalAprovechamientoNoMaderable();
  }

  getVolumen() {
    var params = {
      codActvAprove: "POCC",
      idPlanManejo: this.idPlanManejo,
    };
    this.actividadesAprovechamientoService
      .obtenerTotalVolumenCorta(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          if (!!response.data.total) {
            // this.form.totalCostoEstimado = response.data.total;
            this.form.volumenM3rNoMaderables = response.data.total;
          } else {
            if (this.showWarn) {
              this.showWarn = false;
              this.toast.warn("No existen datos sincronizados para el Plan de Manejo " + this.idPlanManejo + " Tipo plan POCC");
            }
          }
        }
      });
  }

  obtenerTotalAprovechamientoNoMaderable() {
    var params = {
      codActvAprove: "POCC",
      idPlanManejo: this.idPlanManejo,
    };
    this.actividadesAprovechamientoService
      .obtenerTotalAprovechamientoNoMaderable(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          if (!!response.data.total) {
            this.form.totalCostoEstimado = response.data.total;
          } else {
            if (this.showWarn) {
              this.showWarn = false;
              this.toast.warn("No existen datos sincronizados para el Plan de Manejo " + this.idPlanManejo + " Tipo plan POCC");
            }
          }
        }
      });
  }

  openModal() {
    this.ref = this.dialogService.open(ModalInfoContratoComponent, {
      header: "Buscar Contrato",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        nroDocumento: this.user.nroDocumento,
        idPlanManejo: this.idPlanManejo,
        idPlanManejoPadre: this.idPlanManejoPadre,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.codigoTipoPersonaTitular == "TPERJURI") {
          this.form.nombreElaboraDema = resp.razonSocialTitular;
          this.form.nombreCompletoTitular = resp.razonSocialTitular;
        } else {
          this.form.nombreCompletoTitular =
            resp.nombreTitular +
            " " +
            resp.apePaternoTitular +
            " " +
            resp.apeMaternoTitular;
          this.form.nombreElaboraDema = resp.nombreTitular;
          this.form.apellidoPaternoElaboraDema = resp.apePaternoTitular;
          this.form.apellidoMaternoElaboraDema = resp.apeMaternoTitular;
        }
        this.form.nombreCompletoRepresentante =
          resp.nombreRepresentanteLegal +
          " " +
          resp.apePaternoRepLegal +
          " " +
          resp.apeMaternoRepLegal;
        this.form.nombreRepresentante = resp.nombreRepresentanteLegal;
        this.form.apellidoPaternoRepresentante = resp.apePaternoRepLegal;
        this.form.apellidoMaternoRepresentante = resp.apeMaternoRepLegal;
        this.form.dniElaboraDema = resp.numeroDocumentoTitular;
        this.form.nroResolucionComunidad = resp.idContrato;
        // this.form.nroRucComunidad = resp.numeroDocumentoRepLegal;
        this.form.departamentoRepresentante = resp.departamento;
        this.form.provinciaRepresentante = resp.provincia;
        this.form.distritoRepresentante = resp.distrito;
        this.idPlanManejoPadre = resp.idPlanManejo;
        this.form.direccionLegalTitular = resp.domicilioLegalTitular;
        this.form.idDistritoTitular = resp.idDistritoContrato;
        this.form.codTipoPersona = resp.codigoTipoPersonaTitular;
        this.form.tipoPersona = resp.descripcionTipoPersonaTitular;
        this.form.codTipoDocumentoElaborador = resp.codigoTipoDocumentoTitular;
        this.form.tipoDocumentoElaborador =
          resp.descripcionTipoDocIdentidadTitular;
        this.form.codTipoDocumentoRepresentante =
          resp.codigoTipoDocIdentidadRepLegal;
        this.form.tipoDocumentoRepresentante =
          resp.descripcionTipoDocIdentidadRepLegal;
        this.form.documentoRepresentante = resp.numeroDocumentoRepLegal;
        this.informacionGeneralPOCM(this.form.dniElaboraDema);
      }
    });
  }

  guardarPlanManejo() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      idUsuarioModificacion: this.user.idUsuario,
      idPlanManejoPadre: this.idPlanManejoPadre,
    };
    this.evaluacionService
      .actualizarPlanManejo(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  informacionGeneralPOCM(dni: string) {
    var params = {
      dniElaboraDema: dni,
      codigoProceso: "POCC",
    };
    this.informacionGeneralPOCMService
      .obtenerInformacionPlanOperativo(params)
      .subscribe((response: any) => {
        if (response.data) {
          response.data.forEach((element: any) => {
            this.form.observacion = element.cantidadProceso;
          });
        }
      });
  }

  listarInformacionGeneral() {
    const body = {
      codigoProceso: "POCC",
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralPOCMService
      .listarInformacionGeneral(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length) {
          this.editForm = true;
          const last = response.data.length - 1;

          this.form = new PoccInformacionGeneral(response.data[last]);
          this.contratoSuscrito = this.form.regente.contratoSuscrito;
          this.numeroInscripcion = this.form.regente.numeroInscripcion;
          this.form.departamentoRepresentante =
            response.data[last].departamentoRepresentante;
          this.form.provinciaRepresentante =
            response.data[last].provinciaRepresentante;
          this.form.distritoRepresentante =
            response.data[last].distritoRepresentante;
          this.form.idDistritoTitular = response.data[last].idDistritoTitular;
          if (!!this.form.dniElaboraDema) {
            this.informacionGeneralPOCM(this.form.dniElaboraDema);
          }
          this.form.lstUmf.forEach((item: any) => {
            if (item.detalle == "SI") {
              this.vigenciaIncial = item.vigenciaDet;
              this.vigenciaFinal = item.vigenciaFinalDet;
            }
          });

          this.form.fechaFinDema = new Date(response.data[last].fechaFinDema);
          this.form.fechaInicioDema = new Date(
            response.data[last].fechaInicioDema
          );

          this.nombreCompleto = `${response.data[last].regente.nombres} ${response.data[last].regente.apellidos} `;
          if (response.data[last].codTipoPersona == "TPERJURI") {
            this.form.nombreElaboraDema = response.data[last].nombreElaboraDema;
            this.form.nombreCompletoTitular = `${response.data[last].nombreElaboraDema} ${response.data[last].apellidoPaternoElaboraDema} ${response.data[last].apellidoMaternoElaboraDema}`;
          } else {
            this.form.nombreCompletoTitular = `${response.data[last].nombreElaboraDema} ${response.data[last].apellidoPaternoElaboraDema} ${response.data[last].apellidoMaternoElaboraDema}`;
          }
          this.form.nombreCompletoRepresentante = `${response.data[last].nombreRepresentante} ${response.data[last].apellidoPaternoRepresentante} ${response.data[last].apellidoMaternoRepresentante}`;
        }
      });
  }

  listarPorFiltroDepartamentoIni() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: any) => {
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        this.listarPorFilroProvincia(this.provincia);
      },
      (error: any) => {
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia(provincia: ProvinciaModel) {
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      });
  }
  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }
  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  validarInformacion() {
    let fechaActual = moment();
    let fechaInicio = moment(this.form.fechaInicioDema);
    this.minDateFinal = moment(fechaInicio).add("days", 1).format("YYYY-MM-DD");
    this.maxDateFinal = moment(fechaInicio).add("year", 3).format("YYYY-MM-DD");
    this.disabledSave = false;
    if (!!this.form.fechaInicioDema && !!this.form.fechaFinDema) {
      if (fechaInicio.diff(fechaActual, "days") < 0) {
        this.errorMensaje(
          "La Fecha Inicio debe ser posterior a la Fecha Actual"
        );
        this.disabledSave = true;
        return;
      }

      if (this.form.fechaFinDema <= this.form.fechaInicioDema) {
        this.disabledSave = true;
        this.errorMensaje(
          "La Fecha Final debe ser posterior a la Fecha Inicio"
        );
        return;
      }
    } else if (fechaInicio.diff(fechaActual, "days") < 0) {
      this.errorMensaje("La Fecha Inicio debe ser posterior a la Fecha Actual");
      this.disabledSave = true;
      return;
    }
  }

  handleAdmDateChange() {
    this.validarInformacion();
    let fechaInicial = moment(this.form.fechaInicioDema);
    let fechaFinal = moment(this.form.fechaFinDema);
    if (!!this.form.fechaInicioDema) {
      if (fechaFinal.diff(fechaInicial, "days") >= 0) {
        this.disabledSave = false;
      } else {
        this.toast.warn("Fecha Fin debe ser posterior o igual a Fecha Inicio");
        this.disabledSave = true;
      }
    } else {
      this.toast.warn("Debe ingresar la fecha de inicio ");
      this.disabledSave = true;
    }
  }
  numeroPc(event: any) {
    this.form.superficieHaMaderables = "";
    if (this.form.lstUmf.length != 0) {
      if (this.form.nroAnexosComunidad == 1) {
        this.form.lstUmf.forEach((item) => {
          this.vigenciaIncial = 1;
          this.vigenciaFinal = 3;
          this.form.lstUmf[0].detalle = "SI";
          this.form.lstUmf[0].area = 0;
          this.form.lstUmf[0].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[0].vigenciaFinalDet = this.vigenciaFinal;
          this.form.lstUmf[1].detalle = "NO";
          this.form.lstUmf[1].area = 0;
          this.form.lstUmf[1].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[1].vigenciaFinalDet = this.vigenciaFinal;
          this.form.lstUmf[2].detalle = "NO";
          this.form.lstUmf[2].area = 0;
          this.form.lstUmf[2].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[2].vigenciaFinalDet = this.vigenciaFinal;
        });
      } else if (this.form.nroAnexosComunidad == 2) {
        this.form.lstUmf.forEach((item) => {
          this.vigenciaIncial = 2;
          this.vigenciaFinal = 3;
          this.form.lstUmf[0].detalle = "SI";
          this.form.lstUmf[0].area = 0;
          this.form.lstUmf[0].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[0].vigenciaFinalDet = this.vigenciaFinal;
          this.form.lstUmf[1].detalle = "SI";
          this.form.lstUmf[1].area = 0;
          this.form.lstUmf[1].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[1].vigenciaFinalDet = this.vigenciaFinal;
          this.form.lstUmf[2].detalle = "NO";
          this.form.lstUmf[2].area = 0;
          this.form.lstUmf[2].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[2].vigenciaFinalDet = this.vigenciaFinal;
        });
      } else if (this.form.nroAnexosComunidad == 3) {
        this.form.lstUmf.forEach((item) => {
          this.vigenciaIncial = 3;
          this.vigenciaFinal = 3;
          this.form.lstUmf[0].detalle = "SI";
          this.form.lstUmf[0].area = 0;
          this.form.lstUmf[0].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[0].vigenciaFinalDet = this.vigenciaFinal;
          this.form.lstUmf[1].detalle = "SI";
          this.form.lstUmf[1].area = 0;
          this.form.lstUmf[1].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[1].vigenciaFinalDet = this.vigenciaFinal;
          this.form.lstUmf[2].detalle = "SI";
          this.form.lstUmf[2].area = 0;
          this.form.lstUmf[2].vigenciaDet = this.vigenciaIncial;
          this.form.lstUmf[2].vigenciaFinalDet = this.vigenciaFinal;
        });
      }
    } else {
      if (this.form.nroAnexosComunidad == 1) {
        this.form.lstUmf = [];
        this.vigenciaIncial = 1;
        this.vigenciaFinal = 3;
        let obj = new infoGenralDetalle();
        obj.vigenciaDet = this.vigenciaIncial;
        obj.vigenciaFinalDet = this.vigenciaFinal;
        obj.codigoInformacionDet = "POCCPOA1";
        obj.detalle = "SI";
        obj.codigo = "001";
        obj.descripcion =
          "La UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR.";

        let obj2 = new infoGenralDetalle();
        obj2.vigenciaDet = this.vigenciaIncial;
        obj2.vigenciaFinalDet = this.vigenciaFinal;
        obj2.codigoInformacionDet = "POCCPOA2";
        obj2.codigo = "002";
        obj2.detalle = "NO";
        obj2.descripcion =
          "La UMF se ubica en la zona prioritaria para el Estado.";

        let obj3 = new infoGenralDetalle();
        obj3.vigenciaDet = this.vigenciaIncial;
        obj3.vigenciaFinalDet = this.vigenciaFinal;
        obj3.codigoInformacionDet = "POCCPOA3";
        obj3.codigo = "003";
        obj3.descripcion =
          "La UMF se realizan proyectos integrales de transformación.";
        obj3.detalle = "NO";
        this.form.lstUmf.push(obj, obj2, obj3);
      } else if (this.form.nroAnexosComunidad == 2) {
        this.form.lstUmf = [];
        this.vigenciaIncial = 2;
        this.vigenciaFinal = 3;
        let obj = new infoGenralDetalle();
        obj.vigenciaDet = this.vigenciaIncial;
        obj.vigenciaFinalDet = this.vigenciaFinal;
        obj.codigoInformacionDet = "POCCPOA1";
        obj.detalle = "SI";
        obj.codigo = "001";
        obj.descripcion =
          "La UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR.";

        let obj2 = new infoGenralDetalle();
        obj2.vigenciaDet = this.vigenciaIncial;
        obj2.vigenciaFinalDet = this.vigenciaFinal;
        obj2.codigoInformacionDet = "POCCPOA2";
        obj2.codigo = "002";
        obj2.detalle = "SI";
        obj2.descripcion =
          "La UMF se ubica en la zona prioritaria para el Estado.";

        let obj3 = new infoGenralDetalle();
        obj3.vigenciaDet = this.vigenciaIncial;
        obj3.vigenciaFinalDet = this.vigenciaFinal;
        obj3.codigoInformacionDet = "POCCPOA3";
        obj3.codigo = "003";
        obj3.descripcion =
          "La UMF se realizan proyectos integrales de transformación.";
        obj3.detalle = "NO";
        this.form.lstUmf.push(obj, obj2, obj3);
      } else if (this.form.nroAnexosComunidad == 3) {
        this.form.lstUmf = [];
        this.vigenciaIncial = 3;
        this.vigenciaFinal = 3;
        let obj = new infoGenralDetalle();
        obj.vigenciaDet = this.vigenciaIncial;
        obj.vigenciaFinalDet = this.vigenciaFinal;
        obj.codigoInformacionDet = "POCCPOA1";
        obj.detalle = "SI";
        obj.codigo = "001";
        obj.descripcion =
          "La UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR.";

        let obj2 = new infoGenralDetalle();
        obj2.vigenciaDet = this.vigenciaIncial;
        obj2.vigenciaFinalDet = this.vigenciaFinal;
        obj2.codigoInformacionDet = "POCCPOA2";
        obj2.codigo = "002";
        obj2.detalle = "SI";
        obj2.descripcion =
          "La UMF se ubica en la zona prioritaria para el Estado.";

        let obj3 = new infoGenralDetalle();
        obj3.vigenciaDet = this.vigenciaIncial;
        obj3.vigenciaFinalDet = this.vigenciaFinal;
        obj3.codigoInformacionDet = "POCCPOA3";
        obj3.codigo = "003";
        obj3.descripcion =
          "La UMF se realizan proyectos integrales de transformación.";
        obj3.detalle = "SI";
        this.form.lstUmf.push(obj, obj2, obj3);
      }
    }
  }

  /* ngOnChanges() {
    this.tokenSerfor();
  } */
  totalPc(event: any, data: any, index: number) {
    let decimal = this._decimalPipe.transform(data.area, "1.2-2");
    // this.form.lstUmf[index].area = decimal;
    this.areaTotal = 0;
    this.form.superficieHaMaderables = "";
    this.form.lstUmf.forEach((item) => {
      if (item.detalle == "SI") {
        let area = item.area;

        this.areaTotal += area;
      }
    });
    let sumatoria = this.areaTotal;
    this.form.superficieHaMaderables = Number(sumatoria).toFixed(2);
  }

  /*   tokenSerfor() {
    console.log("--token--");
    this.serv_forestal.login().subscribe((result: any) => {});
  } */

  onRegenteForestal(event: RegenteForestal) {
    this.form.regente = event;
  }

  actualizarInformacionGeneral() {
    if (!this.validationFields(this.form)) {
      this.disabledSave = false;
      return;
    }

    this.form.fechaElaboracionPmfi = new Date(this.form.fechaElaboracionPmfi);
    this.form.fechaFinDema = new Date(this.form.fechaFinDema);
    this.form.fechaInicioDema = new Date(this.form.fechaInicioDema);

    this.form.idPlanManejo = this.idPlanManejo;
    this.form.idUsuarioRegistro = this.user.idUsuario;
    this.form.regente.codigoProceso = "POCC";
    this.form.regente.contratoSuscrito = this.contratoSuscrito;
    this.form.regente.numeroInscripcion = this.numeroInscripcion;
    this.form.areaTotal = this.form.superficieHaMaderables ? Number(this.form.superficieHaMaderables) : null;

    this.informacionGeneralPOCMService
      .actualizarInformacionGeneral(this.form)
      .subscribe(
        (result: any) => {
          if (result.success == true) {
            this.SuccessMensaje(result.message);
            this.disabledSave = false;
            this.guardarPlanManejo();
            this.listarInformacionGeneral();
          } else {
            this.errorMensaje(result.message);
            this.disabledSave = false;
          }
        },
        () => {
          this.errorMensaje("Ocurrió un problema");
          this.disabledSave = false;
        }
      );
  }

  guardar() {
    if (!this.validationFields(this.form)) {
      this.disabledSave = false;
      return;
    }

    this.form.fechaElaboracionPmfi = new Date(this.form.fechaElaboracionPmfi);
    this.form.fechaFinDema = new Date(this.form.fechaFinDema);
    this.form.fechaInicioDema = new Date(this.form.fechaInicioDema);
    this.form.regente.contratoSuscrito = this.contratoSuscrito;
    this.form.regente.numeroInscripcion = this.numeroInscripcion;
    this.form.idPlanManejo = this.idPlanManejo;
    this.form.idUsuarioRegistro = this.user.idUsuario;
    this.form.areaTotal = this.form.superficieHaMaderables ? Number(this.form.superficieHaMaderables) : null;

    this.informacionGeneralPOCMService
      .registarInformacionGeneralDema(this.form)
      .subscribe(
        (result: any) => {
          if (result.success == true) {
            this.SuccessMensaje(result.message);
            this.disabledSave = false;
            this.guardarPlanManejo();
            this.listarInformacionGeneral();
          } else {
            this.errorMensaje(result.message);
            this.disabledSave = false;
          }
        },
        () => {
          this.errorMensaje("Ocurrió un problema");
          this.disabledSave = false;
        }
      );
  }

  validationFields(data: PoccInformacionGeneral) {
    let validar: boolean = true;
    let mensaje: string = "";

    this.disabledSave = true;

    if (!data.tipoPersona) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Tipo de Persona.\n";
    }
    if (!data.tipoDocumentoElaborador) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Tipo Documento.\n";
    }
    if (!data.dniElaboraDema) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Documento.\n";
    }
    if (!data.nombreElaboraDema) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre o Razón Social.\n";
    }
    if (!data.nroResolucionComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: No. de Contrato de la Concesión.\n";
    }
    if (!data.direccionLegalTitular) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Dirección Empresa.\n";
    }
    if (!data.distritoRepresentante) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Domicilio Legal.\n";
    }
    if (data.codTipoPersona == "TPERJURI") {
      if (!data.tipoDocumentoRepresentante) {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: Tipo Documento del Representante Legal.\n";
      }
      if (!data.documentoRepresentante) {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: Documento del Representante Legal.\n";
      }
      if (!data.nombreCompletoRepresentante) {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: Nombre del Representante Legal.\n";
      }
    }
    /* if (!data.representanteLegal) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar nombre de representante legal.\n";
    } */
    /* if (!data.nroRucComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar RUG.\n";
    } */
    // if (!data.detalle) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Objetivo General del PO.\n";
    // }
    /* el numero de PO puede ser nulo
      if (!data.observacion) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Número de PO.\n";
    } */
    // if (!data.fechaElaboracionPmfi) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Fecha de inicio.\n";
    // }
    // if (!data.fechaInicioDema) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Fecha de finalización.\n";
    // }
    /*if (!data.observacion) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Número de PO.\n";
    }*/
    // if (!data.detalle) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Objetivo general de PO.\n";
    // }
    // if (!data.superficieHaMaderables) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     "(*) Debe ingresar: Área total de la PC(s) a aprovechar (ha)3.\n";
    // }
    // if (!data.superficieHaNoMaderables) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     "(*) Debe ingresar: Área de bosque de producción forestal(ha).\n";
    // }
    // if (!data.volumenM3rMaderables) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Área deprotección.\n";
    // }
    // if (!data.volumenM3rNoMaderables) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     "(*) Debe ingresar: Volumen total solicitado (m3).\n";
    // }
    // if (!data.totalCostoEstimado) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Volumen, cantidad, peso(kg), total de productos forestales no maderables solicitado.\n";
    // }
    // if (!data.regente?.numeroLicencia) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: N° de Licencia del regente.\n";
    // }
    // if (!data.regente?.numeroDocumento) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: N° DNI.\n";
    // }
    // if (!this.nombreCompleto) {
    //   validar = false;
    //   mensaje = mensaje += "(*) Debe ingresar: Nombre de regente que formulo el PMF.\n";
    // }

    if (!validar) this.errorMensaje(mensaje);

    return validar;
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  onChangeEventSuperficieHaMaderables(event: any) {
    var numero = event.target.value;
    this.form.superficieHaMaderables = Number(numero).toFixed(2);
  }

  onChangeEventSuperficieHaNoMaderables(event: any) {
    var numero = event.target.value;
    this.form.superficieHaNoMaderables = Number(numero).toFixed(2);
  }

  onChangeEventVolumenM3rMaderables(event: any) {
    var numero = event.target.value;
    this.form.volumenM3rMaderables = Number(numero).toFixed(2);
  }

  onChangeEventVolumenM3rNoMaderables(event: any) {
    var numero = event.target.value;
    this.form.volumenM3rNoMaderables = Number(numero).toFixed(2);
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  abrirModalRegente() {
    this.listarRegente();
    this.selectRegente = new RegenteForestalModel();
    this.verModalRegente = true;
    this.queryRegente = "";
  }

  listarRegente() {
    this.serv_forestal.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {
        this.regentes.push({
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
        });
      });
    });
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegente();
    }
  }

  guardarRegente() {
    this.form.regente = new RegenteForestal(this.selectRegente);
    this.form.regente.idPlanManejo = this.idPlanManejo;
    this.nombreCompleto = `${this.selectRegente.nombres} ${this.selectRegente.apellidos}`;
    this.selectRegente.nombres + this.selectRegente.apellidos;
    this.form.regente.idUsuarioRegistro = this.user.idUsuario;
    // this.form.regente.estado = this.selectRegente.estado;
    this.verModalRegente = false;
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === "PDF") {
          include = TabInformacionGeneralComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El tipo de Documento no es Válido.",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB.",
          });
        } else {
          if (type === "PDF") {
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.fileInfGenreal);
          }
        }
      }
    }
  }

  eliminarArchivoContrato() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivo))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toastService.ok("Se eliminó Contrato Regente correctamente");
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = "";
      } else {
        this.toastService.error("Ocurrió un error al realizar la operación");
      }
    });
  }
}
