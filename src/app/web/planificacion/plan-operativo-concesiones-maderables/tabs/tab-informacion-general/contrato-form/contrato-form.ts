import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";


import { InformacionGeneralDto } from "@models";
import { MessageService } from "primeng/api";
import { forkJoin } from "rxjs";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { RegenteService } from "src/app/service/regente.service";

@Component({
  selector: "contrato-form",
  templateUrl: "./contrato-form.html",
})
export class ContratoForm implements OnInit {

  private _infoGeneral!: InformacionGeneralDto;
  @Input()
  set infoGeneral(i: InformacionGeneralDto) {
    this._infoGeneral = i;
  }
  get infoGeneral() {
    return this._infoGeneral;
  }

  private _f!: FormGroup;
  @Input() set f(f: FormGroup) {
    this._f = f;
  }
  get f() {
    return this._f;
  }


  departamentos: any[] = [];
  distritos: any[] = [];
  provincias: any[] = [];

  obj1: any;
  distrito: string = "";
  provincia: string = "";
  departamento: string = "";

  constructor(
    private reg: RegenteService,
    private messageService: MessageService,
    private serv_forestal: ApiForestalService,
    private dialog: MatDialog
  ) {

  }

  ngOnInit(): void {

  }



}
