import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { MonitoreoService } from "src/app/service/planificacion/monitoreo.service";
import {
  lstDetalleModel,
  MonitoreoPOCCModel,
} from "src/app/model/monitoreoPOCC";
import { finalize } from "rxjs/operators";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ModalMonitoreoComponent } from "./modal/modal-formulario-impactos-negativos/modal-formulario-impactos-negativos.component";
import { CodigoPOCCTabs, CodigoPOOC } from "src/app/model/util/POCC/CodigoPOOC";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { HttpParams } from "@angular/common/http";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";

@Component({
  selector: "app-tab-monitoreo",
  templateUrl: "./tab-monitoreo.component.html",
  styleUrls: ["./tab-monitoreo.component.scss"],
  providers: [MessageService, ConfirmDialogModule],
})
export class TabMonitoreoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  listMonitoreo: any[] = [];
  listDetalle: any[] = [];
  actualizar: boolean = false;
  ref!: DynamicDialogRef;
  pendiente: boolean = false;
  context: MonitoreoPOCCModel = new MonitoreoPOCCModel();
  selectAnexo: string = "";
  justificacion: string = "";
  idArchivo!: number;
  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;
  idPlanManejoArchivo: number = 0;

  constructor(
    private dialog: MatDialog,
    private user: UsuarioService,
    private toast: ToastService,
    private monitoreoService: MonitoreoService,
    public dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private anexosService: AnexosService,
    private postulacionPFDMService: PostulacionPFDMService
  ) {}

  ngOnInit(): void {
    this.listaMonitoreo();
  }

  listaMonitoreo() {
    this.listMonitoreo = [];
    var params = {
      codigoMonitoreo: "POCC",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.monitoreoService
      .listarMonitoreoPOCC(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            if (element.idPlanManejo != null) {
              this.actualizar = true;
              this.listDetalle = [];
              let obj = new MonitoreoPOCCModel(element);
              obj.idUsuarioModificacion = this.user.idUsuario;
              // this.selectAnexo = element.descripcion;
              // this.justificacion = element.observacion;
              this.selectAnexo = element.observacion;
              obj.lstDetalle.forEach((item) => {
                let objDetalle = new lstDetalleModel(item);
                objDetalle.idUsuarioModificacion = this.user.idUsuario;
                this.listDetalle.push(objDetalle);
              });
              obj.lstDetalle = this.listDetalle;
              this.listMonitoreo.push(obj);
            } else {
              this.listDetalle = [];
              let obj = new MonitoreoPOCCModel(element);
              obj.idMonitoreo = 0;
              obj.idUsuarioRegistro = this.user.idUsuario;
              obj.idPlanManejo = this.idPlanManejo;
              obj.lstDetalle.forEach((item) => {
                let objDetalle = new lstDetalleModel(item);
                objDetalle.idMonitoreoDetalle = 0;
                objDetalle.idUsuarioRegistro = this.user.idUsuario;
                this.listDetalle.push(objDetalle);
              });
              obj.lstDetalle = this.listDetalle;
              this.listMonitoreo.push(obj);
            }
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any, index: any) => {
    this.ref = this.dialogService.open(ModalMonitoreoComponent, {
      header: mesaje,
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          resp.codigoMonitoreo = 'POCC';
          resp.idMonitoreo = 0;
          resp.idMonitoreoDet = 0;
          resp.idPlanManejo = this.idPlanManejo;
          resp.idUsuarioRegistro = this.user.idUsuario;
          this.listMonitoreo.push(resp);
          this.pendiente = true;
        } else if (tipo == "E") {
          this.listMonitoreo[index] = resp;
        }
      }
    });
  };

  openEliminar(event: any, data: MonitoreoPOCCModel, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idMonitoreo != 0) {
          var params = {
            idMonitoreo: data.idMonitoreo,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.monitoreoService
            .eliminarMonitoreoCabecera(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                this.listMonitoreo.splice(index, 1);
                this.toast.ok(response?.message);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listMonitoreo.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  guardarMonitoreo() {
    this.listMonitoreo.forEach((item) => {
      // item.descripcion = this.selectAnexo;
      // item.observacion = this.justificacion;
      item.observacion = this.selectAnexo;
    });
    // if (this.actualizar == true) {
    //   this.monitoreoService
    //     .actualizarMonitoreoPOCC(this.listMonitoreo)
    //     .subscribe((response: any) => {
    //       if (response.success == true) {
    //         this.toast.ok(response?.message);
    //         this.actualizar == false;
    //         this.listaMonitoreo();
    //       } else {
    //         this.toast.error(response?.message);
    //       }
    //     });
    // } else {
      this.monitoreoService
        .registrarMonitoreoPOCC(this.listMonitoreo)
        .subscribe((response: any) => {
          if (response.success == true) {
            // this.toast.ok(response?.message);
            this.toast.ok('Se registró el monitoreo correctamente.');
            this.listaMonitoreo();
          } else {
            this.toast.error(response?.message);
          }
        });
    // }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }
}
