import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { finalize, tap } from 'rxjs/operators';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { ParticipacionCiudadanaService, UsuarioService } from '@services';
import { AccionTipo, ParticipacionTipo, ToastService } from '@shared';
import { ParticipacionComunalDetalleModel } from '@models';
import { GuardarPariticipacionCiudadanaModal } from './modal/guardar-participacion-ciudadana.modal';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { ParticipacionComunalService } from 'src/app/service/planificacion/plan-general-manejo-pgmfa/participacion-comunal.service';

@Component({
  selector: 'app-tab-participacion-ciudadana',
  templateUrl: './tab-participacion-ciudadana.component.html',
  styleUrls: ['./tab-participacion-ciudadana.component.scss'],
})
export class TabParticipacionCiudadanaComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();

  @Input() idPlanManejo!: number;

  AccionTipo = AccionTipo;
  ParticipacionTipo = ParticipacionTipo;

  implementaciones: ParticipacionComunalDetalleModel[] = [];
  comites: ParticipacionComunalDetalleModel[] = [];
  planes: ParticipacionComunalDetalleModel[] = [];

  list: any[] = [];
  idPartComunalPO: number = 0;
  idPartComunalGF: number = 0;
  idPartComunalPC: number = 0;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private api: ParticipacionCiudadanaService,
    private user: UsuarioService,
    private participacionComunalService: ParticipacionComunalService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.listarPOCCPO();
    this.listarPOCCGF();
    this.listarPOCCPC();
  }

  listarPOCCPO() {
    this.implementaciones = [];
    var params = {
      codTipoPartComunal: 'POCCPO',
      codTipoPartComunalDet: null,
      idPartComunal: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService
      .listarParticipacionComunalCaberaDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (!!response.data) {
          response.data.forEach((element: any) => {
            this.idPartComunalPO = element.idPartComunal;
            element.lstDetalle.forEach((element: any) => {
              let obj = new ParticipacionComunalDetalleModel(element);
              this.list.push(obj);
              this.filtrarTipoParticipacion(this.list);
            });
          });
        }
      });
  }

  listarPOCCGF() {
    this.comites = [];
    var params = {
      codTipoPartComunal: 'POCCGF',
      codTipoPartComunalDet: null,
      idPartComunal: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService
      .listarParticipacionComunalCaberaDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (!!response.data) {
          response.data.forEach((element: any) => {
            this.idPartComunalGF = element.idPartComunal;
            element.lstDetalle.forEach((element: any) => {
              let obj = new ParticipacionComunalDetalleModel(element);
              this.list.push(obj);
              this.filtrarTipoParticipacion(this.list);
            });
          });
        }
      });
  }

  listarPOCCPC() {
    this.planes = [];
    var params = {
      codTipoPartComunal: 'POCCPC',
      codTipoPartComunalDet: null,
      idPartComunal: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService
      .listarParticipacionComunalCaberaDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (!!response.data) {
          response.data.forEach((element: any) => {
            this.idPartComunalPC = element.idPartComunal;
            element.lstDetalle.forEach((element: any) => {
              let obj = new ParticipacionComunalDetalleModel(element);
              this.list.push(obj);
              this.filtrarTipoParticipacion(this.list);
            });
          });
        }
      });
  }

  filtrarTipoParticipacion(data: ParticipacionComunalDetalleModel[]) {
    this.comites = [];
    this.implementaciones = [];
    this.planes = [];
    data.forEach((x) => {
      switch (x.codigoPartComunal) {
        case ParticipacionTipo.IMPLEMENTACION:
          let objCI = new ParticipacionComunalDetalleModel(x);
          objCI.codPartComunalDet = 'POCC1';
          objCI.idUsuarioRegistro = this.user.idUsuario;
          this.implementaciones.push(objCI);
          break;
        case ParticipacionTipo.COMITE:
          let objC2 = new ParticipacionComunalDetalleModel(x);
          objC2.codPartComunalDet = 'POCC2';
          objC2.idUsuarioRegistro = this.user.idUsuario;
          this.comites.push(objC2);
          break;
        case ParticipacionTipo.PLAN_OPERATIVO:
          let objC3 = new ParticipacionComunalDetalleModel(x);
          objC3.codPartComunalDet = 'POCC3';
          objC3.idUsuarioRegistro = this.user.idUsuario;
          this.planes.push(objC3);
          break;
        default:
          break;
      }
    });
  }

  abrirModal(
    titulo: AccionTipo,
    codigoPartComunal: ParticipacionTipo,
    data?: ParticipacionComunalDetalleModel,
    index?: number,
  ) {
    const header = `${titulo} Participación Ciudadana`;
    const planManejo = { idPlanManejo: this.idPlanManejo } as PlanManejoModel;
    data = data
      ? data
      : new ParticipacionComunalDetalleModel({ codigoPartComunal, planManejo });
    const config = { header, data, width: '50vw', closable: true };
    const ref = this.dialogService.open(
      GuardarPariticipacionCiudadanaModal,
      config
    );
    ref.onClose.subscribe((value) => {
      if (value) {
        if (!this.verificarDuplicidad(value)) {
          if (
            index != undefined &&
            data?.codigoPartComunal == ParticipacionTipo.IMPLEMENTACION
          ) {
            this.implementaciones[index] = value;
          } else if (
            index != undefined &&
            data?.codigoPartComunal == ParticipacionTipo.COMITE
          ) {
            this.comites[index] = value;
          } else if (
            index != undefined &&
            data?.codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO
          ) {
            this.planes[index] = value;
          } else {
            this.list.push(value);
            
            this.filtrarTipoParticipacion(this.list);
          }
        } else {
          if (value.codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO) {
            this.toast.warn(
              'El mecanismo de participación ' +
                value.actividad +
                ' ya se encuentra registrado.'
            );
          } else {
            this.toast.warn(
              'La actividad ' + value.actividad + ' ya se encuentra registrada.'
            );
          }
        }
      }
    });
  }
  abrirNuevoModal(
    titulo: AccionTipo,
    codigoPartComunal: ParticipacionTipo,
    data?: ParticipacionComunalDetalleModel,
    index?: number,
  ) {
    const header = `${titulo} Participación Ciudadana`;
    const planManejo = { idPlanManejo: this.idPlanManejo } as PlanManejoModel;
    data = data
      ? data
      : new ParticipacionComunalDetalleModel({ codigoPartComunal, planManejo });
    const config = { header, data, width: '50vw', closable: true };
    const ref = this.dialogService.open(
      GuardarPariticipacionCiudadanaModal,
      config
    );
    ref.onClose.subscribe((value) => {
      if (value) {

        

        if (!this.verificarDuplicidad(value)) {
          if (

            data?.codigoPartComunal == ParticipacionTipo.IMPLEMENTACION
          ) {
            this.implementaciones.push(value);
          } else if (

            data?.codigoPartComunal == ParticipacionTipo.COMITE
          ) {
            this.comites.push(value);
          } else if (

            data?.codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO
          ) {
            this.planes.push(value);
          } else {
            this.list.push(value);
            this.filtrarTipoParticipacion(this.list);
          }
        } else {
          if (value.codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO) {
            this.toast.warn(
              'El mecanismo de participación ' +
                value.actividad +
                ' ya se encuentra registrado.'
            );
          } else {
            this.toast.warn(
              'La actividad ' + value.actividad + ' ya se encuentra registrada.'
            );
          }
        }
      }
    });
  }

  verificarDuplicidad(value: any) {
    let flag: boolean = false;

    if (value.codigoPartComunal == ParticipacionTipo.IMPLEMENTACION) {
      this.implementaciones.forEach((e: any) => {
        if (e.actividad == value.actividad) {
          flag = true;
        }
      });
    } else if (value.codigoPartComunal == ParticipacionTipo.COMITE) {
      this.comites.forEach((e: any) => {
        if (e.actividad == value.actividad) {
          flag = true;
        }
      });
    } else if (value.codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO) {
      this.planes.forEach((e: any) => {
        if (e.actividad == value.actividad) {
          flag = true;
        }
      });
    }

    return flag;
  }

  eliminar(
    event: Event,
    codigoPartComunal: ParticipacionTipo,
    data: any,
    idPartComunalDet: number,

    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar esta acción?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (codigoPartComunal == ParticipacionTipo.IMPLEMENTACION) {

       


          if(idPartComunalDet==0){
            this.dialog.closeAll();
            this.implementaciones.splice(index, 1);
          }else{
            this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
              .pipe(
                finalize(() => {
                  this.dialog.closeAll();
                  this.implementaciones.splice(index, 1);

                  let indexListaGeneral = this.list.findIndex(
                    (e: any) =>
                      e.actividad == data.actividad &&
                      e.codigoPartComunal == codigoPartComunal
                  );
                  if (indexListaGeneral && indexListaGeneral > 0) {
                    this.list.splice(index, 1);
                  }
                })
              )
              .subscribe();
          }
        } else if (codigoPartComunal == ParticipacionTipo.COMITE) {
          if(idPartComunalDet==0){
            this.comites.splice(index, 1);
            this.dialog.closeAll();
          }else{
          this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
            .pipe(
              finalize(() => {
                this.dialog.closeAll();
                this.comites.splice(index, 1);
                let indexListaGeneral = this.list.findIndex(
                  (e: any) =>
                    e.actividad == data.actividad &&
                    e.codigoPartComunal == codigoPartComunal
                );
                if (indexListaGeneral && indexListaGeneral > 0) {
                  this.list.splice(index, 1);
                }
              })
            )
            .subscribe();}
        } else if (codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO) {
          if(idPartComunalDet==0){
            this.planes.splice(index, 1);
            this.dialog.closeAll();
          }else{
          this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
            .pipe(
              finalize(() => {
                this.dialog.closeAll();
                this.planes.splice(index, 1);
                let indexListaGeneral = this.list.findIndex(
                  (e: any) =>
                    e.actividad == data.actividad &&
                    e.codigoPartComunal == codigoPartComunal
                );
                if (indexListaGeneral && indexListaGeneral > 0) {
                  this.list.splice(index, 1);
                }
              })
            )
            .subscribe();}
        }
      },
    });
  }

  eliminarParticipacion(idPartComunalDet: number, idUsuarioElimina: number) {
    var params = {
      idPartComunalDet: idPartComunalDet,
      idUsuarioElimina: idUsuarioElimina,
    };
    return this.participacionComunalService
      .eliminarParticipacionComunalCaberaDetalle(params)
      .pipe(
        tap({
          next: (res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
            } else {
              this.toast.error(res?.message);
            }
          },
        })
      );
  }

  guardarParticipacionComunal() {
    var params = [
      {
        actividad: '',
        codTipoPartComunal: 'POCCPO',
        fecha: '',
        idPartComunal: this.idPartComunalPO,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: '',
        idPlanManejo: this.idPlanManejo,
        responsable: '',
        seguimientoActividad: '',
        lstDetalle: this.implementaciones,
      },
      {
        actividad: '',
        codTipoPartComunal: 'POCCGF',
        fecha: '',
        idPartComunal: this.idPartComunalGF,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: '',
        idPlanManejo: this.idPlanManejo,
        responsable: '',
        seguimientoActividad: '',
        lstDetalle: this.comites,
      },
      {
        actividad: '',
        codTipoPartComunal: 'POCCPC',
        fecha: '',
        idPartComunal: this.idPartComunalPC,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: '',
        idPlanManejo: this.idPlanManejo,
        responsable: '',
        seguimientoActividad: '',
        lstDetalle: this.planes,
      },
    ];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService
      .registrarParticipacionComunalCaberaDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok(
            'Se registró la participación ciudadana correctamente.'
          );
          this.list = [];
          this.listarPOCCGF();
          this.listarPOCCPC();
          this.listarPOCCPO();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
