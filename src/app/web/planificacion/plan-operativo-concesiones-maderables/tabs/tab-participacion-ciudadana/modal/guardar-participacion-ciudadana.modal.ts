import { Component } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ParticipacionComunalDetalleModel } from "@models";
import { ParticipacionCiudadanaService, UsuarioService } from "@services";
import { ParticipacionTipo } from "@shared";
import * as moment from "moment";

import { MessageService } from "primeng/api";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/dynamicdialog";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";

@Component({
  selector: "guardar-participacion-ciudadana-modal",
  templateUrl: "./guardar-participacion-ciudadana.modal.html",
  styleUrls: ["./guardar-participacion-ciudadana.modal.scss"],
})
export class GuardarPariticipacionCiudadanaModal {
  labelBtn: string = "Guardar";
  ParticipacionTipo = ParticipacionTipo;

  f: FormGroup;
  participacion: ParticipacionComunalDetalleModel = new ParticipacionComunalDetalleModel();
  codigoPartComunal: string | ParticipacionTipo =
    ParticipacionTipo.IMPLEMENTACION;

    minDate = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private dialog: MatDialog,
    private api: ParticipacionCiudadanaService,
    private user: UsuarioService
  ) {
    this.f = this.initForm();
  }

  ngOnInit() {
    this.participacion = this.config.data as ParticipacionComunalDetalleModel;
    const {
      idUsuarioRegistro,
      actividad,
      metodologia,
      lugar,
      fecha,
    } = this.participacion;
    this.f.patchValue({
      idUsuarioRegistro,
      actividad,
      metodologia,
      lugar,
      fecha,
    });
    this.codigoPartComunal = this.participacion.codigoPartComunal;
    this.agregarValidadores(this.codigoPartComunal);
  }

  agregarValidadores(tipo: ParticipacionTipo | string) {
    if (tipo == ParticipacionTipo.PLAN_OPERATIVO) {
      const validators = [Validators.required, Validators.minLength(1)];
      this.f.controls.metodologia.setValidators(validators);

      this.f.controls.lugar.setValidators(validators);
      this.f.controls.fecha.setValidators(validators);
    }
  }

  initForm() {
    return this.fb.group({
      actividad: [null, [Validators.required, Validators.minLength(1)]],
      metodologia: [null],
      idUsuarioRegistro: this.user.idUsuario,
      lugar: [null],
      fecha: [null],
    });
  }

  guardar() {
    this.showFormError();
    if (this.f.invalid) return;

    const {
      idUsuarioRegistro,
      actividad,
      metodologia,
      lugar,
      fecha,
    } = this.f.value;
    this.participacion = {
      ...this.participacion,
      idUsuarioRegistro,
      actividad,
      metodologia,
      lugar,
      fecha,
    };
    this.participacion.idUsuarioRegistro = this.user.idUsuario;
    if (!!this.participacion.fecha) {
      this.participacion.fecha = new Date(this.participacion.fecha);
    }

    this.ref.close(this.participacion);
  }

  cancelar() {
    this.ref.close();
  }

  showFormError() {
    Object.keys(this.f.controls).forEach((key) => {
      const controlErrors: ValidationErrors | null | undefined = this.f.get(key)
        ?.errors;
      if (controlErrors != null) {
        let fechaActual = new Date();
        let fecha = new Date(this.f.controls[key].value);
        if (key == "fecha") {
          if (fecha < fechaActual) {
            let detail = 'La fecha debe ser mayor o igual a la fecha actual.';
            this.messageService.add({ severity: "warn", detail, key: "gpc_02" });
            return;
          }
        }
        key =
          key == "actividad" &&
          this.codigoPartComunal == ParticipacionTipo.PLAN_OPERATIVO
            ? "mecanismo"
            : key;
        const detail = `(*) Debe ingresar ${ValidacionCampoError[key]}.\n`;
        this.messageService.add({ severity: "warn", detail, key: "gpc_02" });
      }
    });
  }
}

const ValidacionCampoError: any = {
  mecanismo: "mecanismo",
  actividad: "actividad",
  metodologia: "metodología",
  lugar: "lugar",
  fecha: "fecha",
};
