import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { toString } from 'lodash';
import { MessageService } from 'primeng/api';
import { InformacionBasicaParcelaCortaPOCCModel } from 'src/app/model/InformacionBasicaParcelaCortaPOCCModel';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { MapApi } from 'src/app/shared/mapApi';

@Component({
  selector: 'tabla-coordenadas',
  templateUrl: './tabla-coordenadas.component.html',
  styleUrls: ['./tabla-coordenadas.component.scss'],
})
export class TablaCoordenadasComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() idInfBasica!: number;
  listCoordinatesAnexo!: any[];
  @Input() set listCoordinates(data: any) {
    if (data) {
      this.listCoordinatesAnexo = [];
      data.forEach((t: any) => {
        this.listCoordinatesAnexo.push({
          vertice: t.puntoVertice,
          este: t.coordenadaEsteIni,
          norte: t.coordenadaNorteIni,
          referencia: t.referencia,
          idInfBasica: t.idInfBasica,
          idInfBasicaDet: t.idInfBasicaDet,
          codInfBasicaDet: t.codInfBasicaDet,
          codSubInfBasicaDet: t.codSubInfBasicaDet,
          descripcion: t.descripcion,
          areaHa: t.areaHa,
          idUsuarioRegistro: t.idUsuarioRegistro
        });
      });

      this.registrar();
    }
  }


  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;
  @Input() codePC!: string;
  @Input() descripcion!: string;
  @Input() area!: number;

  @Output() guardarCoordenada = new EventEmitter();
  @Output() updateReference = new EventEmitter();
  @Output() cargaVertice = new EventEmitter();
  @Input() pcV!: string;

  listMap: any[] = [];
  eventV: any;

  constructor(private messageService: MessageService, private mapApi: MapApi) {}

  ngOnInit(): void {}

  registrar() {
    this.listMap = this.listCoordinatesAnexo.map((coordenada) => {
      const obj = new InformacionBasicaParcelaCortaPOCCModel(coordenada);

      obj.idUsuarioRegistro = this.usuario.idusuario? this.usuario.idusuario: coordenada.idUsuarioRegistro;
      obj.coordenadaEsteIni = !!coordenada.este? coordenada.este.toString():'';
      obj.coordenadaNorteIni = !!coordenada.norte? coordenada.norte.toString(): '';
      obj.puntoVertice = !!coordenada.vertice? coordenada.vertice.toString(): '';
      obj.codInfBasicaDet = 'POCCIBPCUEPC';
      obj.codSubInfBasicaDet = this.codePC? this.codePC : coordenada.codSubInfBasicaDet;
      obj.descripcion = this.descripcion? this.descripcion: coordenada.descripcion;
      obj.areaHa = this.area ? parseInt(this.area.toString()) : coordenada.areaHa;
      obj.referencia = !!coordenada.referencia? coordenada.referencia:null;
      obj.idInfBasica = coordenada.idInfBasica;
      obj.idInfBasicaDet = coordenada.idInfBasicaDet;
      //obj.idInfBasicaDet = 0;

      return obj;
    });

    this.guardarCoordenada.emit(this.listMap);
  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje += '(*) Agregue archivo VERTICES PGMF \n';
    }

    if (this.listCoordinatesAnexo.some((cord: any) => cord.referencia == '')) {
      validar = false;
      mensaje = mensaje +=
        '(*) Todas las coordenadas deben llevar referencia \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  onChangeFileSHP(e: any) {
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaAnexo(data[0].features);
    });
    this.eventV = e.target.files[0];
    let item: any = {
      file: this.eventV,
      pc: this.pcV
    }
    this.cargaVertice.emit(item);
    e.target.value = '';
  }

  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo.push({
        vertice: t.properties.VERTICES,
        este: t.properties.ESTE,
        norte: t.properties.NORTE,
        referencia: null,
      });
    });

    this.registrar();
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: mensaje,
    });
  }

  onChangeReference(data: any, index: number) {
    const obj = new InformacionBasicaParcelaCortaPOCCModel(this.listMap[index]);
    obj.referencia = data.referencia;

    this.listMap[index] = obj;
    this.updateReference.emit(this.listMap);
  }
}
