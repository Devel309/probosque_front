import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { TablaCoordenadasComponent } from './tabla-coordenadas.component';

@NgModule({
  declarations: [TablaCoordenadasComponent],
  imports: [FormsModule, ReactiveFormsModule, TableModule],
  exports: [TablaCoordenadasComponent],
})
export class TablaCoordenadasComponentModule {}
