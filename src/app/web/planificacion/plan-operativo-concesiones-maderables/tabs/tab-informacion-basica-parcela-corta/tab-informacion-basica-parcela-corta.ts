import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ArchivoService, CoreCentralService, InformacionBasicaService, OrdenamientoInternoPmfiService, UsuarioService, } from '@services';
import { AppMapaComponent, MapApi, RespuestaTipo, ToastService, HidrografiaTipo, isNullOrEmpty, } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { from } from 'rxjs';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { InformacionBasicaModel, InformacionBasicaParcelaCortaPOCCModel } from 'src/app/model/InformacionBasicaParcelaCortaPOCCModel';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { FileModel } from 'src/app/model/util/File';
import { concatMap, finalize } from 'rxjs/operators';
import { InformacionBasicaSOPCMervice } from 'src/app/service/plan-operativo-concesion-maderable/informacion-basica.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { DialogTipoBosqueComponent } from 'src/app/shared/components/dialog-tipo-bosque/dialog-tipo-bosque.component';
import { DepartamentoModel, DistritoModel, ProvinciaModel, } from '@models';
import { MapaPorAccordeonComponent } from '../tab-ordenamiento-parcela-proteccion/components/mapa-por-accordeon/mapa-por-accordeon.component';
import { InformacionGeneralPOCMService } from 'src/app/service/plan-operativo-concesion-maderable/informacion-general.service';
import { TablaCoordenadasComponent } from './components/tabla-coordenadas/tabla-coordenadas.component';
import { CodigoPOOC } from 'src/app/model/util/POCC/CodigoPOOC';
@Component({
  selector: 'tab-informacion-basica-parcela-corta',
  templateUrl: './tab-informacion-basica-parcela-corta.html',
  styleUrls: ['./tab-informacion-basica-parcela-corta.scss'],
  providers: [MessageService],
})
export class TabInformacionBasicaParcelaCortaComponent
  implements OnInit, OnDestroy
{
  @Output()
  public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();

  @Input() idPGMF!: number;

  mapa1: boolean = false;
  mapa2: boolean = false;

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;

  @ViewChild('map1', { static: true }) map1!: MapaPorAccordeonComponent;
  @ViewChild('map2', { static: true }) map2!: MapaPorAccordeonComponent;
  @ViewChild('tabla1', { static: false }) tabla1!: TablaCoordenadasComponent;
  @ViewChild('tabla2', { static: false }) tabla2!: TablaCoordenadasComponent;
  @ViewChild('tabla3', { static: false }) tabla3!: TablaCoordenadasComponent;
  @ViewChild('tabla4', { static: false }) tabla4!: TablaCoordenadasComponent;

  public _files: any[] = [];
  file: FileModel = {} as FileModel;
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
  view: any = null;

  public listaDistritos: any[] = [];
  distritoSeleccinado: any[] = [];

  hidrografia = [{ rio: '', quebrada: '', cocha: '' }];

  ubicacion: any[] = [];

  infoPc1: any = {};
  infoPc2: any = {};
  infoPc3: any = {};
  infoPc4: any = {};

  listPc1: any = [];
  listPc2: any = [];
  listPc3: any = [];
  listPc4: any = [];

  pc0: any[] = [];
  pc1: any[] = [];
  pc2: any[] = [];
  pc3: any[] = [];
  pc4: any[] = [];

  listado: boolean = false;

  countAreaTotal = 0;
  countAreaHa = 0;
  countPorcentaje = 0;

  informacionBasica: InformacionBasicaModel = {} as InformacionBasicaModel;
  anexo: boolean = false;
  anexo2: boolean = false;
  listCoordinatesAnexo1: any[] = [];
  usuario = {} as UsuarioModel;
  idPlanManejo!: number;
  idInfBasica: number = 0;
  idInfBasicaBosq: number = 0;
  idInfBasicaAccesibilidad: number = 0;
  ref!: DynamicDialogRef;

  listBosques: any[] = [];
  subCodigo: string = '';
  totalRecords: number = 0;

  showModal: boolean = false;

  accesibility: any = {};
  indexAccesibility: number = 0;
  isEditAccesibility: boolean = false;

  isEditedMap = false;
  relacionArchivo!: { idArchivo: number; idPlanManejoArchivo: number };

  listOrdenInterno: any[] = [];
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;
  RespuestaTipo = RespuestaTipo;

  listDepartamento: DepartamentoModel[] = [];
  departamento = {} as DepartamentoModel;

  listProvincia: ProvinciaModel[] = [];
  provincia = {} as ProvinciaModel;

  listDistrito: DistritoModel[] = [];
  distrito = {} as DistritoModel;

  departamentoSeleccionado: any;
  provinciaSeleccionado: any;
  distritoSeleccionado: any;
  noData: boolean = false;

  HidrografiaTipo = HidrografiaTipo;
  rios: any[] = [];
  cambioV: boolean = false;
  eventV: any;
  pcV!: string;

  CodigoPOCC = CodigoPOOC;

  constructor(
    private messageService: MessageService,
    private informacionBasicaSOPCMervice: InformacionBasicaSOPCMervice,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    public dialogService: DialogService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private confirmationService: ConfirmationService,
    private user: UsuarioService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private toast: ToastService,
    private apiArchivo: ArchivoService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private servCoreCentral: CoreCentralService,
    private informacionGeneralPOCMService: InformacionGeneralPOCMService,
    private api: InformacionBasicaService
  ) {}

  ngOnInit(): void {
    this.usuario.idusuario = JSON.parse(
      '' + localStorage.getItem('usuario')
    ).idusuario;
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.listaDistritos = [
      { name: 'Santiago de Surco', code: 'departamento' },
      { name: 'Ate Vitarte', code: 'RM' },
      { name: 'Lima', code: 'LDN' },
      { name: 'Miraflores', code: 'IST' },
      { name: 'Breña', code: 'PRS' },
    ];
    this.listarInformacionBasica();
    this.listarTiposBosque();
    this.listarAccesibilidad();
    this.initializeMap();
    this.obtenerArchivoCapas();
    this.listarPorFiltroDepartamentoIni();
    this.listaInfoGeneral();
    this.buscarHidrografia(HidrografiaTipo.RIO);
  }
  calcularAreaParcela(area: any) {
    this.informacionBasica.areaTotal = area;
  }
  listarPorFiltroDepartamentoIni() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;

        this.listarPorFilroProvincia({});
      },
      (error: any) => {
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.message,
        });
      }
    );
  }

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        this.listarPorFilroProvincia(this.provincia);
      },
      (error: any) => {
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia(provincia: ProvinciaModel | any) {
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      });
  }
  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }
  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  listaInfoGeneral() {
    var params = {
      codigoProceso: 'POCC',
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.informacionGeneralPOCMService
      .listarInformacionGeneral(params)
      .subscribe((response: any) => {
        if (response.data) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              this.informacionBasica.numeroPc = element.nroAnexosComunidad;
              this.informacionBasica.masPc =
                element.nroAnexosComunidad >= 2 ? true : false;
              this.informacionBasica.detalle = element.superficieHaMaderables;
              element.lstUmf.forEach((element: any) => {});
              this.infoPc1.descripcion = 1;
              this.infoPc1.area = element.lstUmf[0].area;

              this.infoPc2.descripcion = 2;
              this.infoPc2.area = element.lstUmf[1].area;

              this.infoPc3.descripcion = 3;
              this.infoPc3.area = element.lstUmf[2].area;
            });
          } else {
          }
        }
      });
  }

  listarInformacionBasica() {
    this.pc0 = [];
    this.pc1 = [];
    this.listPc1 = [];
    this.pc2 = [];
    this.listPc2 = [];
    this.pc3 = [];
    this.listPc3 = [];
    this.pc4 = [];
    this.listPc4 = [];

      const params = {
        idInfBasica: 'POCC',
        idPlanManejo: this.idPlanManejo,
        codCabecera: 'POCCIBPCUEPC',
      };

      this.informacionBasicaSOPCMervice.listarInformacionBasica(params).subscribe(
        (result: any) => {
          this.dialog.closeAll();

          if (result.data) {
            this.listado = true;

            result.data.forEach((element: any) => {
              //console.log("listarInformacionBasica ", element);

              this.idInfBasica = element.idInfBasica;
              this.anexo = element.anexo == 'S' ? true : false;
              this.departamentoSeleccionado = Number(element.departamento);
              this.provinciaSeleccionado = Number(element.provincia);
              this.distritoSeleccionado = Number(element.distrito);
              this.mapa1 = element.codNombreInfBasica == 'S' ? true : false;
              this.informacionBasica.nroHojaCatastral = element.nroHojaCatastral;
              this.informacionBasica.nombreHojaCatastral =
                element.nombreHojaCatastral;
              this.informacionBasica.areaTotal = element.areaTotal;
              this.informacionBasica.codSubInfBasica = element.cuenca;
              this.informacionBasica.numeroPc = element.numeroPc;

              this.informacionBasica.comunidad = element.comunidad;
              if(this.informacionBasica.comunidad == 'Mapa1'){
                this.mapa1 = true;
              }

              this.distritoSeleccinado = element.distrito
                ? element.distrito.split(',')
                : [];
              this.anexo = element?.anexo?.toUpperCase() === 'S'.toUpperCase();
              this.updatePc();

              if (element.codSubInfBasicaDet == 'PCONE') {
                ///console.log("listar PCONE", element );
                const info = {
                  descripcion: element.descripcion,
                  area: element.areaHa,
                };
                this.infoPc1 = info;

                this.infoPc1.area = element.areaHa;

                const obj = new InformacionBasicaParcelaCortaPOCCModel(element);
                obj.coordenadaEsteIni = element.coordenadaEste;
                obj.coordenadaNorteIni = element.coordenadaNorte;
                obj.codInfBasicaDet = 'POCCIBPCUEPC';
                obj.codSubInfBasicaDet = 'PCONE';
                obj.idInfBasica = element.idInfBasica;
                obj.idInfBasicaDet = element.idInfBasicaDet;
                obj.referencia = element.referencia;
                obj.descripcion = element.descripcion;
                obj.areaHa = element.areaHa;
                obj.idUsuarioRegistro = this.usuario.idusuario;
                this.listPc1.push(obj);
                this.pc1.push(obj);
                //console.log("AGREGO",obj);
              }
              if (element.codSubInfBasicaDet == 'PCTWO') {
                const info = {
                  descripcion: element.descripcion,
                  area: element.areaHa,
                };
                this.infoPc2 = info;

                const obj = new InformacionBasicaParcelaCortaPOCCModel(element);
                obj.coordenadaEsteIni = element.coordenadaEste;
                obj.coordenadaNorteIni = element.coordenadaNorte;
                obj.codInfBasicaDet = 'POCCIBPCUEPC';
                obj.codSubInfBasicaDet = 'PCTWO';
                obj.idInfBasica = element.idInfBasica;
                obj.idInfBasicaDet = element.idInfBasicaDet;
                obj.referencia = element.referencia;
                obj.descripcion = element.descripcion;
                obj.areaHa = element.areaHa;
                obj.idUsuarioRegistro = this.usuario.idusuario;
                this.listPc2.push(obj);
                this.pc2.push(obj);
              }
              if (element.codSubInfBasicaDet == 'PCTHREE') {
                const info = {
                  descripcion: element.descripcion,
                  area: element.areaHa,
                };
                this.infoPc3 = info;

                const obj = new InformacionBasicaParcelaCortaPOCCModel(element);
                obj.coordenadaEsteIni = parseInt(
                  element.coordenadaEste.toString()
                );
                obj.coordenadaNorteIni = parseInt(element.coordenadaNorte);
                obj.codInfBasicaDet = 'POCCIBPCUEPC';
                obj.codSubInfBasicaDet = 'PCTHREE';
                obj.idInfBasica = element.idInfBasica;
                obj.idInfBasicaDet = element.idInfBasicaDet;
                obj.referencia = element.referencia;
                obj.descripcion = element.descripcion;
                obj.areaHa = element.areaHa;
                obj.idUsuarioRegistro = this.usuario.idusuario;
                this.listPc3.push(obj);
                this.pc3.push(obj);
              }
              if (element.codSubInfBasicaDet == 'PCFOUR') {
                const info = {
                  descripcion: element.descripcion,
                  area: element.areaHa,
                };
                this.infoPc4 = info;

                const obj = new InformacionBasicaParcelaCortaPOCCModel(element);
                obj.coordenadaEsteIni = element.coordenadaEste;
                obj.coordenadaNorteIni = element.coordenadaNorte;
                obj.codInfBasicaDet = 'POCCIBPCUEPC';
                obj.codSubInfBasicaDet = 'PCFOUR';
                obj.idInfBasica = element.idInfBasica;
                obj.idInfBasicaDet = element.idInfBasicaDet;
                obj.referencia = element.referencia;
                obj.descripcion = element.descripcion;
                obj.areaHa = element.areaHa;
                obj.idUsuarioRegistro = this.usuario.idusuario;
                this.listPc4.push(obj);
                this.pc4.push(obj);
              }
              if (isNullOrEmpty(element.codSubInfBasicaDet)) {
                const obj = new InformacionBasicaParcelaCortaPOCCModel();
                obj.codInfBasicaDet = element.codInfBasicaDet;
                obj.idInfBasica = element.idInfBasica;
                obj.idInfBasicaDet = element.idInfBasicaDet;
                obj.idUsuarioRegistro = this.usuario.idusuario;
                this.pc0.push(obj);
              }
            });

            setTimeout(() => this.reseteo(), 2000);
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  }

  leerData(){


  }

  ngOnDestroy(): void {
    console.log('');
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  changeDistrito(e: any) {}

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  SuccessMensaje = (message: string) => {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: message,
    });
  };

  guardarCoordenadas(payload: any, nuPc: number) {
    switch (nuPc) {
      case 2:
        this.pc2 = payload;
        break;
      case 3:
        this.pc3 = payload;
        break;
      case 4:
        this.pc4 = payload;
        break;
      default:
        this.pc1 = payload;
        break;
    }
  }

  updateReference(payload: any, nuPc: number) {
    switch (nuPc) {
      case 2:
        this.pc2 = payload;
        break;
      case 3:
        this.pc3 = payload;
        break;
      case 4:
        this.pc4 = payload;
        break;
      default:
        this.pc1 = payload;
        break;
    }
  }

  changeDescripctionPC(nuPc: number) {
    switch (nuPc) {
      case 2:
        this.pc2.forEach((data) => {
          const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
          obj.areaHa = this.infoPc2.area;
          obj.idUsuarioRegistro = this.usuario.idusuario;
          obj.descripcion = this.infoPc2.descripcion;
          obj.codSubInfBasicaDet = 'PCTWO';
          return obj;
        });
        break;
      case 3:
        this.pc3.forEach((data) => {
          const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
          obj.areaHa = this.infoPc3.area;
          obj.idUsuarioRegistro = this.usuario.idusuario;
          obj.descripcion = this.infoPc3.descripcion;
          obj.codSubInfBasicaDet = 'PCTHREE';

          return obj;
        });
        break;
      case 4:
        this.pc4.forEach((data) => {
          const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
          obj.areaHa = this.infoPc4.area;
          obj.idUsuarioRegistro = this.usuario.idusuario;
          obj.descripcion = this.infoPc4.descripcion;
          obj.codSubInfBasicaDet = 'PCFOUR';

          return obj;
        });
        break;
      default:
        this.pc1.forEach((data) => {
          const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
          obj.areaHa = this.infoPc1.area;
          obj.idUsuarioRegistro = this.usuario.idusuario;
          obj.descripcion = this.infoPc1.descripcion;
          obj.codSubInfBasicaDet = 'PCONE';

          return obj;
        });
        break;
    }
  }
  changeAreaPC(nuPc: number) {
    const area1 = !!this.infoPc1.area ? this.infoPc1.area : 0;
    const area2 = !!this.infoPc2.area ? this.infoPc2.area : 0;
    const area3 = !!this.infoPc3.area ? this.infoPc3.area : 0;
    const area4 = !!this.infoPc4.area ? this.infoPc4.area : 0;

    this.informacionBasica.areaTotal =
      parseInt(area1) + parseInt(area2) + parseInt(area3) + parseInt(area4);

    switch (nuPc) {
      case 2:
        if (this.pc2.length) {
          const newArray: any[] = [];
          this.pc2.forEach((data) => {
            const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
            obj.areaHa = this.infoPc2.area;
            obj.idUsuarioRegistro = this.usuario.idusuario;
            obj.descripcion = this.infoPc2.descripcion;
            obj.codSubInfBasicaDet = 'PCTWO';

            newArray.push(obj);
          });
          this.pc2 = newArray;
        }

        break;
      case 3:
        if (this.pc3.length) {
          const newArray: any[] = [];
          this.pc3.forEach((data) => {
            const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
            obj.areaHa = this.infoPc3.area;
            obj.idUsuarioRegistro = this.usuario.idusuario;
            obj.descripcion = this.infoPc3.descripcion;
            obj.codSubInfBasicaDet = 'PCTHREE';

            newArray.push(obj);
          });
          this.pc3 = newArray;
        }
        break;
      case 4:
        if (this.pc4.length) {
          const newArray: any[] = [];
          this.pc4.forEach((data) => {
            const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
            obj.areaHa = this.infoPc4.area;
            obj.idUsuarioRegistro = this.usuario.idusuario;
            obj.descripcion = this.infoPc4.descripcion;
            obj.codSubInfBasicaDet = 'PCFOUR';

            newArray.push(obj);
            this.pc4 = newArray;
          });
        }

        break;
      default:
        if (this.pc1.length) {
          const newArray: any[] = [];
          this.pc1.forEach((data) => {
            const obj = new InformacionBasicaParcelaCortaPOCCModel(data);
            obj.areaHa = this.infoPc1.area;
            obj.idUsuarioRegistro = this.usuario.idusuario;
            obj.descripcion = this.infoPc1.descripcion;
            obj.codSubInfBasicaDet = 'PCONE';

            newArray.push(obj);
          });

          this.pc1 = newArray;
        }
        break;
    }
  }
  guardarShape1() {
    this.map1.guardar();
  }

  registrarCoordenadas() {
    if (this.pc0.length == 0) {
      const obj = new InformacionBasicaParcelaCortaPOCCModel();
      obj.codInfBasicaDet = 'POCCIBPCUEPC';
      obj.idUsuarioRegistro = this.usuario.idusuario;
      this.pc0.push(obj);
    }

    this.informacionBasica.comunidad = "";
    if (this.mapa1) {
      this.informacionBasica.comunidad = "Mapa1"
    } else {
      this.informacionBasica.comunidad = ""
    }

    //if (!this.validarRegistro()) return;
    this.dialog.open(LoadingComponent, { disableClose: true });

    const array = this.pc0.concat(this.pc1, this.pc2, this.pc3, this.pc4);

    let params = [
      {
        idInfBasica: this.idInfBasica ? this.idInfBasica : 0,

        codInfBasica: this.CodigoPOCC.CODIGO_PROCESO,
        codSubInfBasica: this.CodigoPOCC.TAB_4,
        codNombreInfBasica: this.mapa1 ? 'S' : 'N',
        departamento: this.departamentoSeleccionado,
        provincia: this.provinciaSeleccionado,
        distrito: this.distritoSeleccionado,
        comunidad: this.informacionBasica.comunidad,
        cuenca: this.informacionBasica.codSubInfBasica,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuario.idusuario,
        anexo: this.anexo ? 'S' : 'N',
        numeroPc: this.informacionBasica.numeroPc ? parseInt(this.informacionBasica.numeroPc.toString()):0,
        nroPc: this.informacionBasica.numeroPc ? String(this.informacionBasica.numeroPc) : "",
        areaTotal: this.informacionBasica.areaTotal,
        nroHojaCatastral: this.informacionBasica.nroHojaCatastral,
        nombreHojaCatastral: this.informacionBasica.nombreHojaCatastral,
        listInformacionBasicaDet: array,
      },
    ];

    this.informacionBasicaSOPCMervice
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.SuccessMensaje(response?.message);
          this.listarInformacionBasica();
          /*this.tabla1.listCoordinates = this.listPc1;
          this.tabla2.listCoordinates = this.listPc2;
          this.tabla3.listCoordinates = this.listPc3;
          this.tabla4.listCoordinates = this.listPc4;*/


        } else {
          this.ErrorMensaje(response?.message);
        }
      });
  }

  reseteo(){
    if(this.listPc1.length>0){
      this.tabla1.listCoordinates = this.listPc1;
    }
    if(this.listPc2.length>0){
      this.tabla2.listCoordinates = this.listPc2;
    }
    if(this.listPc3.length>0){
      this.tabla3.listCoordinates = this.listPc3;
    }
    if(this.listPc4.length>0){
      this.tabla4.listCoordinates = this.listPc4;
    }




  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    const array = this.pc1.concat(this.pc2, this.pc3, this.pc4);

    const snRef = array.filter((cord: any) => cord.referencia == null);

    if (!this.informacionBasica.numeroPc) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Número de la(s) PC \n';
    }

    if (!this.informacionBasica.nroHojaCatastral) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar N° Hoja Catastral \n';
    }
    if (!this.informacionBasica.nombreHojaCatastral) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Nombre Hoja Catastral \n';
    }
    if (!this.distritoSeleccinado) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar al menos un Distrito \n';
    }
    if (!this.informacionBasica.codSubInfBasica) {
      validar = false;
      mensaje = mensaje +=
        '(*) Debe seleccionar al menos una Cuenca/Subcuenca \n';
    }
    if (
      this.informacionBasica.codSubInfBasica == 'Otros' &&
      !this.informacionBasica.codNombreInfBasica
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Número de la(s) PC \n';
    }

    if (
      !!this.informacionBasica.numeroPc &&
      this.informacionBasica.numeroPc == 1
    ) {
      if (this.pc1.length==0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC1\n';
      }
      if (!this.infoPc1.descripcion) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar PC1\n';
      }
      if (!this.infoPc1.area) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar Área(ha) para  PC1\n';
      }
    }

    if (
      !!this.informacionBasica.numeroPc &&
      this.informacionBasica.numeroPc == 2
    ) {
      if (this.pc1.length==0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC1\n';
      }
      if (this.pc2.length==0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC2\n';
      }
      if (!this.infoPc2.descripcion) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar PC2\n';
      }
      if (!this.infoPc2.area) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar Área(ha) para  PC2\n';
      }
    }

    if (
      !!this.informacionBasica.numeroPc &&
      this.informacionBasica.numeroPc == 3
    ) {
      if (this.pc1.length==0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC1\n';
      }
      if (this.pc2.length==0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC2\n';
      }
      if (this.pc3.length==0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC3\n';
      }
      if (!this.infoPc3.descripcion) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar PC3\n';
      }
      if (!this.infoPc3.area) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar Área(ha) para  PC3\n';
      }
    }

    if (
      !!this.informacionBasica.numeroPc &&
      this.informacionBasica.numeroPc == 4
    ) {
      if (!this.pc4.length) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar el archivo VERTICES POCC para PC4\n';
      }
      if (!this.infoPc4.descripcion) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar PC4\n';
      }
      if (!this.infoPc4.area) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar Área(ha) para  PC4\n';
      }
    }

    if (!this.listado && snRef.length > 0) {
      validar = false;
      mensaje = mensaje +=
        '(*) Debe ingresar todas las coordenadas con referencia \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  updatePc() {
    if (!!this.informacionBasica.numeroPc) {
      this.informacionBasica.masPc =
        this.informacionBasica.numeroPc >= 2 ? true : false;
    } else {
      this.informacionBasica.masPc = false;
    }
  }

  openDialog() {
    this.ref = this.dialogService.open(DialogTipoBosqueComponent, {
      header: 'Tipo de Bosque',
      width: '60%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });
    this.ref.onClose.subscribe((resp: any) => {
      resp.forEach((element: any) => {
        const obj = new InformacionBasicaParcelaCortaPOCCModel(element);
        obj.idUsuarioRegistro = this.usuario.idusuario;
        obj.idTipoBosque = element.idTipoBosque;
        obj.nombreComun = element.descripcion;
        obj.familia = element.region;
        obj.codInfBasicaDet = 'POCCIBPCTIBO';
        this.listBosques.push(obj);

        this.totalRecords = this.listBosques.length;
      });
    });
  }

  updateAreaHa() {
    this.countAreaHa = 0;

    this.listBosques.map((data) => {
      this.countAreaHa += parseFloat(data.areaHa);
    });
  }
  updatePorcentaje() {
    this.countPorcentaje = 0;

    this.listBosques.map((data) => {
      this.countPorcentaje += parseFloat(data.areaHaPorcentaje);
    });
  }

  validarTiposBosque(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    const snRef = this.listBosques.filter(
      (data: any) => data.areaHa == 0 && data.areaHaPorcentaje == 0
    );

    if (this.listBosques.length) {
      if (snRef.length > 0) {
        validar = false;
        mensaje = mensaje +=
          '(*) Debe ingresar todas las áreas y porcentajes para tipos de bosque \n';
      }
    } else {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar tipos de bosque \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  guardarShape2() {
    this.map2.guardar();
  }

  registrarTiposBosque() {
    // if (!this.validarTiposBosque()) return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listBosqueDet: any[] = [];
    for (let item of this.listBosques) {
      this.idInfBasicaBosq = item.idInfBasica,
      item.tipoBosque.forEach((t: any) => {
        let obj = {
          idInfBasicaDet: t.idInfBasicaDet,
          codInfBasicaDet: 'POCCIBPCTIBO',
          areaHa: t.areaHA,
          areaHaPorcentaje: t.areaHAPorcentaje,
          nombre: t.descripcion,
          idTipoBosque: t.idBosque
        }
        listBosqueDet.push(obj);
      });
    }
    let params = [
      {
        idInfBasica: this.idInfBasicaBosq ? this.idInfBasicaBosq : 0,
        codInfBasica: 'POCC',
        codSubInfBasica: 'POCCIBPC',
        codNombreInfBasica: 'POCCIBPCTIBO',
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuario.idusuario,
        listInformacionBasicaDet: listBosqueDet,
      },
    ];

    this.informacionBasicaSOPCMervice
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.SuccessMensaje(response?.message);
          this.listarTiposBosque();
        } else {
          this.ErrorMensaje(response?.message);
        }
        this.dialog.closeAll();
      });
  }

  listarTiposBosque = () => {
    this.listBosques = [];
    const params = {
      idInfBasica: 'POCC',
      idPlanManejo: this.idPlanManejo,
      codCabecera: 'POCCIBPCTIBO',
    };

    this.informacionBasicaSOPCMervice.listarInformacionBasica(params).subscribe(
      (result: any) => {
        if (result.data) {
          result.data.forEach((element: any) => {
            const array: any[] = [];
            let objDet = {
              idInfBasicaDet: element.idInfBasicaDet,
              codInfBasicaDet: element.codInfBasicaDet,
              areaHA: element.areaHa,
              areaHAPorcentaje: element.areaHaPorcentaje,
              descripcion: element.nombre,
              idBosque: element.idTipoBosque,
              des: element.descripcion,
              subPc: element.observaciones
            }
            array.push(objDet);
            let item = {
              idInfBasica: element.idInfBasica,
              tipoBosque: array
            }
            this.listBosques.push(item);
          });
          this.calculateAreaTotalBosques();
        }
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  };

  eliminarTiposBosque(event: any) {
    if (this.listBosques.length > 0) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: '¿Está seguro de eliminar los registros?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          for (let item of this.listBosques) {
            this.idInfBasicaBosq = item.idInfBasica;
          }
          if (this.idInfBasicaBosq != null && this.idInfBasicaBosq != undefined) {
            let params = {
              idInfBasica: this.idInfBasicaBosq,
              idInfBasicaDet: 0,
              codInfBasicaDet: '',
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.informacionBasicaSOPCMervice
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                } else {
                  this.ErrorMensaje(result.message);
                }
                this.listarTiposBosque();
              });
          } else {
            this.listarTiposBosque();
          }

        },
        reject: () => {},
      });
    }
  }

  buscarHidrografia(tipo: HidrografiaTipo) {
    this.informacionBasicaSOPCMervice
      .obtenerHidrografia(0, tipo)
      .subscribe((response: any) => {
        this.rios = [...response.data];
      });
  }

  /* Carga de SHP (track) */
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
    });
  }

  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.file = config.file;
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      this.file = {} as FileModel;
      this.file.codigo = config.idArchivo;
      this.file.file = config.file;
      this.file.idLayer = t.idLayer;
      this.file.inServer = config.inServer;
      this.file.nombreFile = t.title || config.fileName;
      this.file.groupId = t.groupId;
      this.file.color = t.color;
      this._filesSHP.push(this.file);
      this.createLayer(t);
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }
  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    popupTemplate.content = [
      {
        type: 'fields',
        fieldInfos: [
          {
            fieldName: 'TrackName',
            label: 'Nombre',
          },
          {
            fieldName: 'Position',
            label: 'Posicion',
            format: {
              digitSeparator: true,
              places: 0,
            },
          },
          {
            fieldName: 'Time',
            label: 'Tiempo',
          },
          {
            fieldName: 'Altitude',
            label: 'Altitud',
          },
        ],
      },
    ];
    return popupTemplate;
  }
  onChangeFileSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      inServer: false,
      service: false,
      annex: false,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  onRemoveFileSHP(file: FileModel) {
    if (file.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          let index = this._filesSHP.findIndex(
            (t: any) => t.idLayer === file.idLayer
          );
          this._filesSHP.splice(index, 1);
          this.mapApi.removeLayer(file.idLayer, this.view);
          this.eliminarArchivoDetalle(file.codigo);
        },
      });
    } else {
      let index = this._filesSHP.findIndex(
        (t: any) => t.idLayer === file.idLayer
      );
      this._filesSHP.splice(index, 1);
      this.mapApi.removeLayer(file.idLayer, this.view);
    }
  }
  onDownloadFileSHP(file: FileModel) {
    if (file.file !== null) {
      let url = window.URL.createObjectURL(file.file);
      let name = file.nombreFile;
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.href = url;
      a.download = `${name}.zip`;
      a.click();
    }
  }
  onGuardarLayer() {
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: 'POCCIBPCACCE',
          file: t.file,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.savelayer(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (data) => {
          this.obtenerArchivoCapas();
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un error.');
        }
      );
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._filesSHP = [];
  }
  savelayer(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveLayerRelation(result, item)));
  }
  saveLayerRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: 'POCCIBPCACCE',
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
    };
    return this.servicePostulacionPFDM.registrarArchivoDetalle(item2);
  }
  obtenerArchivoCapas() {
    this.cleanLayers();
    this._filesSHP = [];
    let item: any = {
      idPlanManejo: this.idPlanManejo,
      codigoTipoPGMF: 'POCCIBPCACCE',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.isSuccess) {
            result.data.forEach((t: any) => {
              if (t.documento != null) {
                let blob = this.mapApi.readFileByte(t.documento);
                let config = {
                  inServer: true,
                  service: false,
                  fileName: t.nombre,
                  idArchivo: t.idArchivo,
                };
                this.processFile(blob, config);
              }
            });
          } else {
            this.ErrorMensaje(result.message);
          }
        },
        (error) => {
          this.dialog.closeAll();
          this.ErrorMensaje('Ocurrió un problema.');
        }
      );
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    this.servicePostulacionPFDM
      .eliminarArchivoDetalle(idArchivo, this.user.idUsuario)
      .subscribe(
        (response: any) => {
          if (response.success) {
            this.SuccessMensaje('Se eliminó correctamente.');
          } else {
            this.ErrorMensaje('No se pudo eliminar, vuelve a intertarlo.');
          }
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un problema.');
        }
      );
  }

  validarAccesibilidad(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.accesibility.referencia) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Puntos de referencia  \n';
    }

    if (!this.accesibility.coordenadaEsteIni) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar:Coordenadas UTM Este (E)  \n';
    }
    if (!this.accesibility.coordenadaNorteIni) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Coordenadas UTM Norte (N)  \n';
    }
    if (!this.accesibility.distanciaKm) {
      validar = false;
      mensaje = mensaje +=
        '(*) Debe ingresar: Distancia desde el punto de referencia (Km)  \n';
    }
    if (!this.accesibility.tiempo) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Tiempo (horas) \n';
    }
    if (!this.accesibility.medioTransporte) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Medio de transporte \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  agregarAccesibilidad() {
    this.showModal = true;
    this.accesibility = {};
    this.indexAccesibility = 0;
    this.isEditAccesibility = false;
    this.accesibility = {};
  }

  guardarAcessibilidad() {
    if (!this.validarAccesibilidad()) return;
    this.showModal = false;

    if (!this.isEditAccesibility) {
      const obj = new InformacionBasicaParcelaCortaPOCCModel();
      obj.referencia = this.accesibility.referencia;
      obj.coordenadaEsteIni = this.accesibility.coordenadaEsteIni;
      obj.coordenadaNorteIni = this.accesibility.coordenadaNorteIni;
      obj.distanciaKm = this.accesibility.distanciaKm;
      obj.tiempo = this.accesibility.tiempo;
      obj.medioTransporte = this.accesibility.medioTransporte;
      obj.codInfBasicaDet = 'POCCIBPCACCE';
      this.ubicacion.push(obj);
    } else {
      const obj = new InformacionBasicaParcelaCortaPOCCModel();
      obj.referencia = this.accesibility.referencia;
      obj.coordenadaEsteIni = this.accesibility.coordenadaEsteIni;
      obj.coordenadaNorteIni = this.accesibility.coordenadaNorteIni;
      obj.distanciaKm = this.accesibility.distanciaKm;
      obj.tiempo = this.accesibility.tiempo;
      obj.medioTransporte = this.accesibility.medioTransporte;
      obj.codInfBasicaDet = 'POCCIBPCACCE';

      this.ubicacion[this.indexAccesibility] = obj;
    }
  }

  editarAcessibilidad(data: any, index: number) {
    this.showModal = true;
    this.indexAccesibility = index;
    this.isEditAccesibility = true;
    this.accesibility = { ...data };
  }

  registrarAccesibilidad() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = [
      {
        idInfBasica: this.idInfBasicaAccesibilidad
          ? this.idInfBasicaAccesibilidad
          : 0,
        codInfBasica: 'POCC',
        codNombreInfBasica: '',
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuario.idusuario,
        codSubInfBasica: null,
        anexo: null,
        numeroPc: null,
        areaTotal: null,
        nroHojaCatastral: null,
        nombreHojaCatastral: null,
        listInformacionBasicaDet: this.ubicacion,
      },
    ];

    this.informacionBasicaSOPCMervice
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.SuccessMensaje(response?.message);
          this.listarAccesibilidad();
        } else {
          this.ErrorMensaje(response?.message);
        }
        this.dialog.closeAll();
      });
  }

  listarAccesibilidad = () => {
    const params = {
      idInfBasica: 'POCC',
      idPlanManejo: this.idPlanManejo,
      codCabecera: 'POCCIBPCACCE',
    };

    this.informacionBasicaSOPCMervice.listarInformacionBasica(params).subscribe(
      (result: any) => {
        if (result.data) {
          this.listado = true;

          const array: any[] = [];

          result.data.forEach((element: any) => {
            const obj = new InformacionBasicaParcelaCortaPOCCModel(element);
            obj.coordenadaEsteIni = element.coordenadaEste;
            obj.coordenadaNorteIni = element.coordenadaNorte;
            array.push(obj);
            this.idInfBasicaAccesibilidad = element.idInfBasica;
          });

          this.ubicacion = array;
        }
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  };

  eliminarAccesibilidad(event: any, index: number, idInfBasicaDet: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (idInfBasicaDet > 0) {
          let params = {
            codInfBasicaDet: 'POCCIBPCACCE',
            idInfBasica: this.idInfBasicaAccesibilidad,
            idInfBasicaDet: idInfBasicaDet,
            idUsuarioElimina: this.usuario.idusuario,
          };

          this.informacionBasicaSOPCMervice
            .eliminarInformacionBasica(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);

                this.ubicacion.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.ubicacion.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }
  listarTipoBosque(items: any) {
    this.listBosques = items;
    this.calculateAreaTotalBosques();
  }
  calculateAreaTotalBosques() {
    let sum1 = 0;
    for (let item of this.listBosques) {
      item.tipoBosque.forEach((t: any) => {
        sum1 += t.areaHA;
      });
    }

    for (let item of this.listBosques) {
      item.tipoBosque.forEach((t: any) => {
        t.areaHAPorcentaje = Number(((100 * t.areaHA) / sum1).toFixed(2));
      });
    }
    this.countAreaHa = parseFloat(`${sum1.toFixed(2)}`);
    this.countPorcentaje = parseFloat(`${(100).toFixed(2)} %`);
  }

  // mapa Ubicacion y extenciones

  /* vértices inicio */
  cargaVertice(e: any) {
    this.cambioV = true;
    this.eventV = e.file;
    this.pcV = e.pc;
  }

  deleteLayer(layer: any) {
    switch (layer.descripcion) {
      case 'PC1':
        this.pc1 = [];
        this.listPc1 = [];
        break;
      case 'PC2':
        this.pc2 = [];
        this.listPc2 = [];
        break;
      case 'PC3':
        this.pc3 = [];
        this.listPc3 = [];
        break;
      case 'PC4':
        this.pc4 = [];
        this.listPc4 = [];
        break;
    }
  }
  /* vértices fin */
}
