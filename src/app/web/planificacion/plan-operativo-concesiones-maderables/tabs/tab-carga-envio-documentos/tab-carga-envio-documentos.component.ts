import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/carga-envio-documentacion.service";
import { ArchivoService, PlanManejoService, UsuarioService } from "@services";
import { FileModel } from "src/app/model/util/File";
import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { CargaArchivosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/caragaArchivos.service";
import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { ArchivoDema } from "src/app/model/anexoDema";
import { DownloadFile } from "src/app/shared/util";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";

@Component({
  selector: "app-tab-carga-envio-documentos",
  templateUrl: "./tab-carga-envio-documentos.component.html",
  styleUrls: ["./tab-carga-envio-documentos.component.scss"],
})
export class TabCargaEnvioArchivosComponent implements OnInit {
  @Input() disabled!: boolean;
  isTodosPlanesTerminado : boolean = true;
  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: "POCCIG",
      label: "1. Información General",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCRARPOA",
      label: "2. Resumen Actividades y Recomendaciones del PO Anterior",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCOBJ",
      label: "3. Objetivos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCIBPC",
      label: "4. Información básica de la(s) parcela de corta (PC)",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCOPCPUMF",
      label:
        "5. Ordenamiento de la(s) parcela(s) de corta y protección de la UMF",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCAAPRO",
      label: "6. Actividades de Aprovechamiento",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCASILV",
      label: "7. Actividades Silviculturales",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCEIA",
      label: "8. Evaluación de Impactos Ambientales",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCMON",
      label: "9. Monitoreo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCPC",
      label: "10. Participación Ciudadana",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCCAPA",
      label: "11. Capacitación",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCPINV",
      label: "12 .Programa de Inversiones",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCCACT",
      label: "13. Cronograma de Actividades",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "POCCANEX",
      label: "14. Anexos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
  ];


  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  @Input() idPlanManejo!: number;
  @Output() public regresar = new EventEmitter();

  files: FileModel[] = [];
  filePOCC: FileModel = {} as FileModel;

  consolidadoPOCC: any = null;

  cargarPocc: boolean = false;
  eliminarPocc: boolean = true;
  idArchivoPocc: number = 0;

  pendientes: any[] = [];
  verPOCC: boolean = false;
  verDescargaPOCC: boolean = true;

  anexo1!: ArchivoDema;
  anexo2!: ArchivoDema;
  anexo3!: ArchivoDema;
  anexo4!: ArchivoDema;

  anexos: ArchivoDema[] = [];

  verAnexo1: boolean = true;
  verAnexo2: boolean = true;
  verAnexo3: boolean = true;
  verAnexo4: boolean = true;
  verEnviar: boolean = false;
  CodigoProceso = CodigoProceso;
  checkAll:boolean = false;
  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private cargaArchivosService: CargaArchivosService,
    private dialog: MatDialog,
    private toast: ToastService,
    private anexosService: AnexosService,
    private planManejoService: PlanManejoService,
    private evaluacionService: EvaluacionService
  ) {}

  ngOnInit(): void {
    this.filePOCC.inServer = false;
    this.filePOCC.descripcion = "PDF";
    this.listarEstados();
  }



  listarEstados() {
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: CodigoProceso.PLAN_OPERATIVO_CONCESIONES, //"DEMA",
      idPlanManejoEstado: null,
    };

    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == "CPMPEND") {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == "CPMPEND") {
            this.listProcesos[objIndex].estado = "Pendiente";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;

            this.isTodosPlanesTerminado = false;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == "CPMTERM") {
            this.listProcesos[objIndex].estado = "Terminado";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });
        this.pendiente();
      });
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPOCC = true;
    } else {
      this.verPOCC = false;
    }
  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }

    if (
      this.listProcesos.length ===
      this.listProcesos.filter((data) => data.marcar === true).length
    ) {
      this.verPOCC = false;
      this.isTodosPlanesTerminado = true;
    }else{
      this.isTodosPlanesTerminado = false;
    }

    
  };

  guardarEstados() {
    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: CodigoProceso.PLAN_OPERATIVO_CONCESIONES, //"POCC",
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        observacion: "",
      };
      listaEstadosPlanManejo.push(obje);
    });

    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == "CPMPEND") {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();

          this.messageService.add({
            key: "tl",
            severity: "success",
            summary: "",
            detail:
              "Se registró los estados de la Declaración del Plan de Manejo correctamente.",
          });
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: response.message,
          });
        }
      });
  }

  regresarTab() {
    this.regresar.emit();
  }

  generarPocc() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaArchivosService
      .consolidadoPOCC(this.idPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.consolidadoPOCC = res;
          this.verDescargaPOCC = false;
        } else  {
          this.toast.error(res?.message);
        }
      }, err => {
        this.toast.warn("Para generar el consolidado del Plan Operativo para concesiones - POCC. Debe tener todos los items completados previamente.");
    });
  }

  descargarPOCC() {
    if (isNullOrEmpty(this.consolidadoPOCC)) {
      this.toast.warn("Debe generar archivo consolidado");
      return;
    }
    descargarArchivo(this.consolidadoPOCC);
  }

  registrarArchivoId(event: any){
    if(event){
      this.verEnviar = true;
    }
  }

  enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  guardarBorrador() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMBOR",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  actualizarPlanManejoEstado(param: any) {
    this.evaluacionService
      .actualizarPlanManejo(param)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
        } else {
          this.toast.error(response?.message);
        }
      });
  }
}
