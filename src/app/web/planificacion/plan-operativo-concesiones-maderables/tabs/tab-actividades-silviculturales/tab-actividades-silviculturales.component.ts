
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { CheckboxModule } from "primeng/checkbox";
import { CodigoPOCCTabs, CodigoPOOC } from "src/app/model/util/POCC/CodigoPOOC";
import {
  PccActividadesSilviculturales,
  PccActividadesSilviculturalesCabecera,
} from "src/app/module/pcc-actividades-silviculturales";
import { actividadesSilviculturalService } from "src/app/service/actividad-silvicultural";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";

@Component({
  selector: "app-tab-actividades-silviculturales",
  templateUrl: "./tab-actividades-silviculturales.component.html",
  styleUrls: ["./tab-actividades-silviculturales.component.scss"],
})
export class TabActividadesSilviculturalesComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();

  @Input() idPGMF!: number;

  CheckboxModule = new CheckboxModule();
  selectedValues: string[] = ["1", "0"];
  selectMonitoreo: string | null = null;
  selectAnexo: string | null = null;
  selectAnexo2: string | null = null;
  selectAnexo3: string | null = null;
  idActSilvicultural: number = 0;
  idActividadSilviculturalDet: number = 0;
  idUsuarioRegistro: any = JSON.parse("" + localStorage.getItem("usuario"))
    .idusuario;

  params: any[] = [];
  idPlanManejo!: number;

  actividades: [] = [];
  listaGeneral: any[] = [];
  listActividades: any[] = [];
  tratamientos: PccActividadesSilviculturales[] = [];
  enriquecimientos: PccActividadesSilviculturales[] = [];

  intervencionesSilviculturales: boolean = true;

  justificacion: string = "";
  nombreArchivo: string = "";

  nombreArchivoAnexo2: string = "";
  nombreArchivoAnexo3: string = "";
  tabModal: number = 2;

  isShowModal: boolean = false;
  indexModal: number = -1;
  resultadosModal: PccActividadesSilviculturales = {} as PccActividadesSilviculturales;

  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;
  idPlanManejoArchivo: number = 0;

  list1: any[] = [];

  dataDefault: boolean = false;
  listaGeneralCabecera: PccActividadesSilviculturalesCabecera[] = [];

  idActSilviculturalA: number = 0;
  idActSilviculturalB: number = 0;
  idActSilviculturalC: number = 0;

  constructor(
    private actividadesService: actividadesSilviculturalService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private postulacionPFDMService: PostulacionPFDMService,
    private user: UsuarioService
  ) {}

  ngOnInit(): void {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get("idPlan"));
    this.listaActividades(this.idPlanManejo);
  }

  listaActividades(idPlanManejo: number) {
    this.tratamientos = [];
    this.enriquecimientos = [];
    const params = {
      codigoTipoActSilvicultural: "POCC",
      idPlanManejo: idPlanManejo,
    };

    this.getActivitiesService(params, 1);
  }

  getActivitiesService(params: any, type: number) {
    this.tratamientos = [];
    this.enriquecimientos = [];
    this.listaGeneralCabecera = [];
    this.actividadesService
      .listarActividadSilvicultural(params)
      .subscribe((response: any) => {
        if (!!response.data) {
          if (type == 1) {
            this.intervencionesSilviculturales = true;
          }
          if (response.data[0].idPlanManejo != null) {
            response.data.forEach((element: any) => {
              if (element.codigoTipoActSilvicultural == "POCC") {
                let obj = new PccActividadesSilviculturalesCabecera(element);
                obj.listActividadSilvicultural.forEach((item: any) => {
                  if (item.idTipo == "POCCASILVT" && obj.monitoreo == "B") {
                    this.selectAnexo2 = obj.anexo;
                    this.idActSilviculturalB = obj.idActSilvicultural
                    let objDetalle = new PccActividadesSilviculturales(item);
                    this.tratamientos.push(objDetalle);
                    obj.listActividadSilvicultural = [...this.tratamientos];
                  } else if (
                    item.idTipo == "POCCASILVE" &&
                    obj.monitoreo == "C"
                  ) {
                    this.selectAnexo3 = obj.anexo;
                    this.idActSilviculturalC = obj.idActSilvicultural
                    let objDetalle = new PccActividadesSilviculturales(item);
                    this.enriquecimientos.push(objDetalle);
                    obj.listActividadSilvicultural = [...this.enriquecimientos];
                  } else if (item.idTipo == "1" && obj.monitoreo == "A") {
                    this.selectMonitoreo = obj.actividad;
                    this.selectAnexo = obj.anexo;
                    this.idActSilviculturalA = obj.idActSilvicultural
                    this.list1.push(item);
                  }
                });
                this.listaGeneralCabecera.push(obj);
              }
            });
          } else {
            this.getInitialActivitiesService();
          }
        } else {
          this.getInitialActivitiesService();
        }
      });
  }

  getInitialActivitiesService() {
    const initialParams = {
      codigoTipoActSilvicultural: "POCC",
    };
    this.dataDefault = true;
    this.actividadesService
      .listarActividadSilvicultural(initialParams)
      .subscribe((response: any) => {
        if (!!response.data) {
          

          response.data[0].listActividadSilvicultural.forEach((item: any) => {
            if (item.idTipo == "POCCASILVT") {
              let obj = new PccActividadesSilviculturales(item);
              obj.idActividadSilviculturalDet = 0;
              obj.idUsuarioRegistro = this.user.idUsuario;
              this.tratamientos.push(obj);
            } else if (item.idTipo == "POCCASILVE") {
              let obj = new PccActividadesSilviculturales(item);
              obj.idActividadSilviculturalDet = 0;
              obj.idUsuarioRegistro = this.user.idUsuario;
              this.enriquecimientos.push(obj);
            } else if (item.idTipo == "1") {
              this.idActSilvicultural = 0;
              this.justificacion = item.descripcionDetalle;
              this.idActividadSilviculturalDet =
                item.idActividadSilviculturalDet;
            }
          });
        }
      });
  }

  guardar() {
    if (this.dataDefault) {
      this.crearJson();
    } else {
      this.listaGeneralCabecera.forEach((element: any) => {
        if (element.codigoTipoActSilvicultural == "POCC") {
          let obj = new PccActividadesSilviculturalesCabecera(element);
          obj.listActividadSilvicultural = [];
          if (obj.monitoreo == "B") {
            if (this.selectAnexo2 == "null") {
              obj.anexo = null;
            } else {
              obj.anexo = this.selectAnexo2;
            }
            this.tratamientos.forEach((item: any) => {
              if (item.idTipo == "POCCASILVT") {
                let objDetalle = new PccActividadesSilviculturales(item);
                obj.listActividadSilvicultural.push(objDetalle);
              }
            });
          } else if (obj.monitoreo == "C") {
            if (this.selectAnexo3 == "null") {
              obj.anexo = null;
            } else {
              obj.anexo = this.selectAnexo3;
            }
            this.enriquecimientos.forEach((item) => {
              if (item.idTipo == "POCCASILVE") {
                let objDetalle = new PccActividadesSilviculturales(item);
                obj.listActividadSilvicultural.push(objDetalle);
              }
            });
          } else if (obj.monitoreo == "A") {
            obj.actividad = this.selectMonitoreo;
            if (this.selectAnexo == "null") {
              obj.anexo = null;
            } else {
              obj.anexo = this.selectAnexo;
            }
            obj.listActividadSilvicultural = [...this.list1];
          }
          this.params.push(obj);
        }
      });
    }

    this.actividadesService
      .registrarActividadSilvicultural(this.params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.listaActividades(this.idPlanManejo);
          this.toast("success", response.message);
        } else {
          this.toast("warn", response.message);
        }
      });
  }

  toast(severity: String, datail: String) {
    this.messageService.add({
      severity: severity.toString(),
      summary: '',
      detail: datail.toString(),
    });
  }

  crearJson() {
    var cabecera = {
      idActSilvicultural: 0,
      codigoTipoActSilvicultural: "POCC",
      actividad: null,
      monitoreo: null,
      anexo: null,
      descripcion: null,
      observacion: null,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
    };
    this.params = [
      {
        ...cabecera,
        idActSilvicultural: this.idActSilvicultural,
        anexo: this.selectAnexo ? this.selectAnexo : null,
        monitoreo: "A",
        listActividadSilvicultural: [
          {
            idActividadSilviculturalDet: this.idActividadSilviculturalDet,
            idTipo: "1",
            idTipoTratamiento: "1",
            tipoTratamiento: "Necesidad de intervenciones silviculturales",
            descripcionDetalle: this.justificacion ? this.justificacion : "",
            tratamiento: "",
            justificacion: "",
            idUsuarioRegistro: this.user.idUsuario,
            accionSilvicultural: this.selectAnexo == "S" ? 1 : 0,
          },
        ],
      },
      {
        ...cabecera,
        idActSilvicultural: this.idActSilviculturalB,
        anexo: this.selectAnexo2 ? this.selectAnexo2 : null,
        monitoreo: "B",
        listActividadSilvicultural: this.tratamientos.map((data) => {
          const obj = new PccActividadesSilviculturales(data);
          obj.tipoTratamiento = "";
          obj.tratamiento = data.tratamiento;
          obj.idActividadSilviculturalDet = 0;
          return obj;
        }),
      },
      {
        ...cabecera,
        idActSilvicultural: this.idActSilviculturalC,
        anexo: this.selectAnexo3 ? this.selectAnexo3 : null,
        monitoreo: "C",
        listActividadSilvicultural: this.enriquecimientos.map((data) => {
          const obj = new PccActividadesSilviculturales(data);
          obj.tipoTratamiento = "";
          obj.tratamiento = data.tratamiento;
          obj.idActividadSilviculturalDet = 0;
          return obj;
        }),
      },
    ];
  }

  agregarActividad() {}

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  onFileChange(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        let i = 0;
        this.nombreArchivo = e.srcElement.files[0].name;
        while (i < e.srcElement.files.length) {
          i++;
        }
      }
    }
  }

  onFileChangeAnexo2(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        let i = 0;
        this.nombreArchivoAnexo2 = e.srcElement.files[0].name;
        while (i < e.srcElement.files.length) {
          i++;
        }
      }
    }
  }

  onFileChangeAnexo3(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        let i = 0;
        this.nombreArchivoAnexo3 = e.srcElement.files[0].name;
        while (i < e.srcElement.files.length) {
          i++;
        }
      }
    }
  }

  eliminar(
    event: any,
    index: number,
    idActividadSilviculturalDet: any,
    tab: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (idActividadSilviculturalDet > 0) {
          let params = [
            {
              idActividadSilviculturalDet: idActividadSilviculturalDet,
              idUsuarioElimina: this.idUsuarioRegistro,
            },
          ];

          this.actividadesService
            .eliminarActividadSilviculturalPFDM(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.toast("success", result.message);
                if (tab == 2) {
                  this.tratamientos.splice(index, 1);
                } else {
                  this.enriquecimientos.splice(index, 1);
                }
              } else {
                this.toast("error", result.message);
              }
            });
        } else {
          if (tab == 2) {
            this.tratamientos.splice(index, 1);
          } else {
            this.enriquecimientos.splice(index, 1);
          }
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  editarModal(index: number, data: any, tab: number) {
    this.isShowModal = true;
    this.indexModal = index;
    this.resultadosModal = new PccActividadesSilviculturales(data);
    this.tabModal = tab;
  }

  agregarTratamiento() {
    this.isShowModal = false;
    if (this.tabModal == 2) {
      this.tratamientos.splice(this.indexModal, 1, this.resultadosModal);
    } else {
      this.enriquecimientos.splice(this.indexModal, 1, this.resultadosModal);
    }
  }
  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }
}
