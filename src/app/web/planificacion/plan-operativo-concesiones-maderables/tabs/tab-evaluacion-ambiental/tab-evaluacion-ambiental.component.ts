import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { ActividadComponent } from "./modal/actividad/actividad.component";
import { ContingenciaComponent } from "./modal/contingencia/contingencia.component";
import { ImpactoComponent } from "./modal/impacto/impacto.component";
import { EvaluacionAmbientalTabla } from "./evaluacion-ambiental-tabla/evaluacion-ambiental.tabla";
import { UsuarioService } from "@services";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { CodigoPOCCTabs, CodigoPOOC } from "src/app/model/util/POCC/CodigoPOOC";
import { ConfirmationService, MessageService } from "primeng/api";
import { Toast } from "primeng/toast";
import { ToastService } from "@shared";
import { FormActividadComponent } from "./modal/form-actividad/form-actividad.component";
import { FormActividadVigilanciaComponent } from "./modal/form-actividad-vigilancia/form-actividad-vigilancia.component";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-tab-evaluacion-ambiental",
  templateUrl: "./tab-evaluacion-ambiental.component.html",
  styleUrls: ["./tab-evaluacion-ambiental.component.scss"],
})
export class TabEvaluacionAmbientalComponent implements OnInit {
  @Input() idPGMF: any;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @ViewChild(EvaluacionAmbientalTabla) tablaDinamica!: EvaluacionAmbientalTabla;

  ref!: DynamicDialogRef;

  listaCatalogoContingencia: any[] = [
    { valor1: "Incendios", codigo: "CONTIINC" },
    {
      valor1: "Derrame de conbustible  y/o lubricantes",
      codigo: "CONTIDER",
    },
    {
      valor1: "Riesgos a la salud",
      codigo: "CONTIRIE",
    },
    { valor1: "Invasiones", codigo: "CONTIINV" },
    { valor1: 'Otros', codigo:'CONTIOTROS' },
  ];

  listaContingencia: any[] = [];
  nombrefile = "";

  factor: any;
  actividades: any[] = [];

  selectAnexo: string | null = null;
  nombreArchivoAnexo: string = "";
  idPlanManejoArchivo: number = 0;
  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;
  codTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";
  subCodTipoAprovechamiento: string = "";

  constructor(
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private user: UsuarioService,
    private postulacionPFDMService: PostulacionPFDMService,
    private messageService: MessageService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.factoresAmbientales();
    this.getContingencia();
  }
  onList(list: any) {
    this.actividades = [];
    this.actividades = [...list];
  }

  factoresAmbientales() {
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad({ tipoActividad: "FACTOR" })
      .subscribe((response: any) => {
        this.factor = response.data;
      });
  }

  modalActividadPreventivo = () => {
    this.ref = this.dialogService.open(FormActividadComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Pre Aprovechamiento") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "PREAPR";
          this.subCodTipoAprovechamiento = "SUBPREAPR";
          this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
        }

        if (resp.tipo == "Aprovechamiento") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "APROVE";
          this.subCodTipoAprovechamiento = "SUBAPROVE";
          this.tipoNombreAprovechamiento = "Aprovechamiento";
        }
        if (resp.tipo == "Post Aprovechamiento") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "POSTAP";
          this.subCodTipoAprovechamiento = "SUBPOSTAP";
          this.tipoNombreAprovechamiento = "Post Aprovechamiento";
        }

        if (resp.tipo == "Otros") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "OTROS";
          this.subCodTipoAprovechamiento = "SUBOTROS";
          this.tipoNombreAprovechamiento = "Otros";
        }
        // console.log("response", resp);
        const params = {
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp.actividad,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };
  openEliminarPreventivoCorrector(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.actividades.splice(index, 1);
                this.toast.ok("Se eliminó la información correctamente.");
              }
            });
        } else {
          this.actividades.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }


  openEliminarRegistroActividades(event: Event, data: any, index: number) {
    

    this.confirmationService.confirm({
      target: event.target || undefined,
      message:
        "Al eliminar este registro, se eliminara la actividad" +
        " " +
        data.nombreAprovechamiento +
        " " +
        "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.actividades.splice(index, 1);
                this.toast.ok("Se eliminó la información correctamente.");
              }
            });
        } else {
          this.actividades.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  openEliminarRegistroContingencia(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idEvalAprovechamiento: data.idEvalAprovechamiento,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamientoList(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.getContingencia();
                this.toast.ok("Se eliminó la Contingencia correctamente.");
              }
            });
        } else {
          this.listaContingencia.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }


  modalActividadVigilancia = () => {
    this.ref = this.dialogService.open(FormActividadVigilanciaComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.tipo == "Pre Aprovechamiento") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "PREAPR";
          this.subCodTipoAprovechamiento = "SUBPREAPR";
          this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
        }

        if (resp.tipo == "Aprovechamiento") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "APROVE";
          this.subCodTipoAprovechamiento = "SUBAPROVE";
          this.tipoNombreAprovechamiento = "Aprovechamiento";
        }
        if (resp.tipo == "Post Aprovechamiento") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "POSTAP";
          this.subCodTipoAprovechamiento = "SUBPOSTAP";
          this.tipoNombreAprovechamiento = "Post Aprovechamiento";
        }
        if (resp.tipo == "Otros") {
          this.codTipoAprovechamiento = "POAPRO";
          this.tipoAprovechamiento = "OTROS";
          this.subCodTipoAprovechamiento = "SUBOTROS";
          this.tipoNombreAprovechamiento = "Otros";
        }

        // console.log("response", resp);
        const params = {
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp.actividad,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsable: resp.responsable,
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de acción preventivo-corrector correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  modalActividad = (data: any) => {
    this.ref = this.dialogService.open(ActividadComponent, {
      header: "Editar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        // console.log("response", resp);
        const params = {
          idEvalAprovechamiento: data.idEvalAprovechamiento,
          codTipoAprovechamiento: data.codTipoAprovechamiento,
          tipoAprovechamiento: data.tipoAprovechamiento,
          tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
          nombreAprovechamiento: data.nombreAprovechamiento,
          impacto: resp.descripcionImpacto,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok('Se registró plan de acción preventivo-corrector correctamente.\n');
            this.tablaDinamica.getData()
          });
      }
    });
  };

  modalImpacto = (data: any) => {
    this.ref = this.dialogService.open(ImpactoComponent, {
      header: "Editar Impacto",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        const params = {
          idEvalAprovechamiento: data.idEvalAprovechamiento,
          codTipoAprovechamiento: data.codTipoAprovechamiento,
          tipoAprovechamiento: data.tipoAprovechamiento,
          tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
          nombreAprovechamiento: data.nombreAprovechamiento,
          impacto: data.impacto,
          medidasControl: data.medidasControl,
          medidasMonitoreo: resp.medidasMonitoreo,
          frecuencia: resp.frecuencia,
          acciones: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
          idPlanManejo: this.idPGMF,
          responsable: resp.responsable,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };
        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe((_res) => {
            this.toast.ok(
              "Se registró plan de vigilancia y seguimiento ambiental correctamente.\n"
            );
            this.tablaDinamica.getData();
          });
      }
    });
  };

  modalContingencia = (mesaje: string, edit?: any, data?: any) => {
    let idEvalAprovechamiento: number = 0;
    if (edit == "true") {
      idEvalAprovechamiento = data.idEvalAprovechamiento;
    }
    this.ref = this.dialogService.open(ContingenciaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        editar: edit,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {

      

      if (resp) {
        let nombreAprov = "";
        let tipoConti = "";
        if(resp.contingencia == "CONTIOTROS"){
          nombreAprov = resp.otraContingencia;
          tipoConti = edit == "true" ? this.listaCatalogoContingencia.filter((data: any) => data.valor1 == resp.contingencia)[0].codigo : resp.contingencia
        } else {

          nombreAprov = edit == "true" ? resp.contingencia :  this.listaCatalogoContingencia.filter((data: any) => data.codigo == resp.contingencia)[0].valor1;

          let codigoEncontra = "";
          let buscar = this.listaCatalogoContingencia.filter((data: any) => data.valor1 == resp.contingencia);
          
          if(buscar.length>0){
            codigoEncontra = buscar[0].codigo;
          }else {
            codigoEncontra = "CONTIOTROS";
          }

          tipoConti = edit == "true" ? codigoEncontra : resp.contingencia
        }

        const params = {

          idEvalAprovechamiento,
          codTipoAprovechamiento: "POEAPR",
          tipoAprovechamiento: "CONTIN",
          ipoNombreAprovechamiento: tipoConti,
          nombreAprovechamiento: nombreAprov,//edit == "true" ? resp.contingencia : this.listaCatalogoContingencia.filter((data: any) => data.codigo == resp.contingencia)[0].valor1,
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: resp.accionesRealizar,
          responsable: "",
          responsableSecundario: resp.responsable,
          contingencia: nombreAprov,
          auxiliar: this.selectAnexo,
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario")).idusuario

        };

        this.planificacionService.registrarAprovechamientoEvalAmbiental(params)
          .subscribe(() => this.getContingencia(edit));
      }
    });
  };

  getContingencia(edit?: any) {
    this.listaContingencia = [];
    const body = { idPlanManejo: this.idPGMF, tipoAprovechamiento: "CONTIN" };
    this.planificacionService
      .getAprovechamiento(body)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.selectAnexo = element.auxiliar;
          this.listaContingencia.push(element);
        });
      });
  }
  guardar() {
    if (!this.validarNuevo()) {
      return;
    }
    let item = this.listaContingencia.length;
    let obj = this.listaContingencia[item - 1];
    obj.auxiliar = this.selectAnexo;
    // console.log("se guardar", obj);
    this.planificacionService
      .registrarAprovechamientoEvalAmbiental(obj)
      .subscribe(() => {
        this.toast.ok(
          "Se registró plan de contingencia ambiental correctamente.\n"
        );
        this.getContingencia();
      });
  }

  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectAnexo == "S") {
      if (this.idPlanManejoArchivo == 0) {
        validar = false;
        mensaje = mensaje += "(*) Debe Cargar el archivo requerido.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  eliminarContingencia = (item: any) => {
    this.listaContingencia = this.listaContingencia.filter(
      (x) => x.idContigencia != item.idContigencia
    );
  };

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }
}
