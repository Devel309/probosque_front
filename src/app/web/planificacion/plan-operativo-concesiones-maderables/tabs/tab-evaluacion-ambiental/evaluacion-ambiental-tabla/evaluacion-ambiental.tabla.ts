import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { concatMap, finalize, tap } from "rxjs/operators";

import { AprovechamientoTipo, RespuestaTipo, ToastService } from "@shared";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AprovechamientoResponse } from "src/app/model/evaluacionImpacto";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { ActividadComponent } from "../modal/actividad/actividad.component";
import { AprovechamientoComponent } from "../modal/aprovechamiento/aprovechamiento.component";
import { FactorAmbientalComponent } from "../modal/factor-ambiental/factor-ambiental.component";
import { FactorActividadModel, FactorModel } from "@models";
import { ConfirmationService, MessageService } from "primeng/api";
import { UsuarioService } from "@services";

@Component({
  selector: "evaluacion-ambiental-tabla",
  templateUrl: "./evaluacion-ambiental.tabla.html",
  styleUrls: ["./evaluacion-ambiental.tabla.scss"],
})
export class EvaluacionAmbientalTabla implements OnInit {
  @Input() idPlanManejo: number = 0;
  idUsuario: number = 1;

  ref!: DynamicDialogRef;

  params: any;

  preAprovechamiento: AprovechamientoResponse[] = [];
  aprovechamiento: AprovechamientoResponse[] = [];
  postAprovechamiento: AprovechamientoResponse[] = [];

  @Output() list = new EventEmitter<any>();

  factores: FactorModel[] = [];

  codTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";

  actividades: any[] = [];
  emit: any[] = [];

  factoresEditar: any[] = [];
  factoresEliminar: any[] = [];
  actividad: string = "";

  sizeColActivities = 0;
  subCodTipoAprovechamiento: string = "";

  constructor(
    private messageService: MessageService,
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private user: UsuarioService,
    private toast: ToastService
  ) {
    this.idUsuario = JSON.parse("" + localStorage.getItem("usuario")).idusuario;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.listar()
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  listar() {
    this.sizeColActivities = 0;
    this.preAprovechamiento = [];
    this.aprovechamiento = [];
    this.postAprovechamiento = [];
    this.factores = [];
    this.actividades = [];

    return this.getActividades().pipe(
      concatMap(() => this.getFactores()),
      concatMap(() => this.getRespuestas())
    );
  }

  getFactores() {
    const body = { idPlanManejo: this.idPlanManejo, tipoActividad: "FACTOR" };
    return this.planificacionService
      .listarFiltrosEvalAmbientalActividad(body)
      .pipe(
        tap({
          next: (res: any) => {
            const factores = this.setDatosTabla(res?.data);
            this.factores = factores;
          },
          error: (_err) => console.error(_err),
        })
      );
  }

  getActividades() {
    const body = { idPlanManejo: this.idPlanManejo, tipoAprovechamiento: "" };
    return this.planificacionService.getActividades(body).pipe(
      finalize(() => this.activitiesColSize()),
      tap({
        next: (res) => this.filtrarTipoAprovechamiento(res.data),
        error: (_err) => console.error(_err),
      })
    );
  }

  getRespuestas() {
    return this.planificacionService
      .listarFactoresActividades(this.idPlanManejo)
      .pipe(
        tap({
          next: (res) => this.responseToBoolean(res?.data),
          error: (_err) => console.error(_err),
        })
      );
  }
  responseToBoolean(data: FactorActividadModel[]) {
    if (data) {
      for (const respuesta of data) {
        const factor = this.factores.find(
          (x) => x.idEvalActividad == respuesta.idEvalActividad
        );

        if (factor == undefined) return;

        const preAprovechamiento = this.existFactor(
          factor,
          "preAprovechamiento"
        );
        const aprovechamiento = this.existFactor(factor, "aprovechamiento");
        const postAprovechamiento = this.existFactor(
          factor,
          "postAprovechamiento"
        );
        const actividades = preAprovechamiento
          .concat(aprovechamiento)
          .concat(postAprovechamiento);
        const item = actividades.find(
          (y) => y.idEvalAprovechamiento == respuesta.idEvalAprovechamiento
        ) as AprovechamientoResponse;
        if (item) {
          item.valorEvaluacion = respuesta.valorEvaluacion == RespuestaTipo.SI;
          item.idEvaluacionAmbiental = respuesta.idEvaluacionAmbiental;
        }
      }
    }
  }

  existFactor(
    factor: FactorModel | undefined,
    attr: "preAprovechamiento" | "aprovechamiento" | "postAprovechamiento"
  ): AprovechamientoResponse[] {
    if (factor == undefined || factor[attr] == undefined) {
      return [];
    }
    return factor[attr] as AprovechamientoResponse[];
  }

  activitiesColSize() {
    this.sizeColActivities =
      this.preAprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.preAprovechamiento.length;
    this.sizeColActivities =
      this.aprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.aprovechamiento.length;
    this.sizeColActivities =
      this.postAprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.postAprovechamiento.length;
  }

  filtrarTipoAprovechamiento(data: AprovechamientoResponse[]) {
    let actividades: AprovechamientoResponse[] = [];
    if (data == null || data == undefined) return;

    data.forEach((x) => {
      if (x.valorEvaluacion == undefined || x.valorEvaluacion == null)
        x.valorEvaluacion = false;

      this.actividades.push(x);
      actividades.push({ ...x });

      switch (x.tipoAprovechamiento) {
        case AprovechamientoTipo.PRE_APROVECHAMIENTO:
          this.preAprovechamiento.push(x);
          break;
        case AprovechamientoTipo.APROVECHAMIENTO:
          this.aprovechamiento.push(x);
          break;
        case AprovechamientoTipo.POST_APROVECHAMIENTO:
          this.postAprovechamiento.push(x);
          break;
        default:
          break;
      }
    });

    this.addList(actividades);
  }

  addList(value: any) {
    this.list.emit(value);
  }

  setDatosTabla(data: any[]) {
    if (data == null || data == undefined) return [];

    data.forEach((x) => {
      x["preAprovechamiento"] = this.preAprovechamiento.map((o) => ({ ...o }));
      x["aprovechamiento"] = this.aprovechamiento.map((o) => ({ ...o }));
      x["postAprovechamiento"] = this.postAprovechamiento.map((o) => ({
        ...o,
      }));
    });
    return data;
  }

  guardar() {
    const body = this.requestToString(this.idPlanManejo, this.idUsuario);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.registrarRespuestas(body)
      .pipe(concatMap(() => this.listar()))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  requestToString(idPlanManejo: number, idUsuario: number) {
    let body: FactorActividadModel[] = [];
    this.factores.forEach((factor) => {
      const preAprovechamiento = this.existFactor(factor, "preAprovechamiento");
      const aprovechamiento = this.existFactor(factor, "aprovechamiento");
      const postAprovechamiento = this.existFactor(
        factor,
        "postAprovechamiento"
      );
      const actividades = factor
        ? preAprovechamiento.concat(aprovechamiento).concat(postAprovechamiento)
        : [];

      actividades.forEach((actividad) => {
        const item = new FactorActividadModel({
          responsableSecundario: factor.nombreActividad,
          auxiliar: factor.medidasControl,
          idEvaluacionAmbiental: actividad?.idEvaluacionAmbiental
            ? actividad.idEvaluacionAmbiental
            : 0,
          idEvalAprovechamiento: actividad.idEvalAprovechamiento,
          idEvalActividad: factor.idEvalActividad,
          idPlanManejo,
          valorEvaluacion: actividad.valorEvaluacion
            ? RespuestaTipo.SI
            : RespuestaTipo.NO,
          descripcion: "",
          medidasMitigacion: factor?.medidasMonitoreo
            ? factor.medidasMonitoreo
            : "",
          adjunto: "",
          idUsuarioRegistro: idUsuario,
        });
        body.push(item);
      });

      const medidasMitigacion = new FactorActividadModel({
        idEvaluacionAmbiental: 0,
        idEvalAprovechamiento: 0,
        idEvalActividad: factor.idEvalActividad,
        idPlanManejo,
        valorEvaluacion: "",
        descripcion: "",
        medidasMitigacion: factor?.medidasMonitoreo
          ? factor.medidasMonitoreo
          : "",
        adjunto: "",
        idUsuarioRegistro: idUsuario,
      });
      body.push(medidasMitigacion);
    });

    return body;
  }

  registrarRespuestas(body: FactorActividadModel[]) {
    return this.planificacionService
      .registrarFactoresActividades(body)
      .pipe(tap(this.handlerResult()));
  }

  modalAprovechamiento = (mensaje: string, item: any, tipo: string) => {
    if (tipo == "Pre Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "PREAPR";
      this.subCodTipoAprovechamiento = "SUBPREAPR";
      this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
    }

    if (tipo == "Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "APROVE";
      this.subCodTipoAprovechamiento = "SUBAPROVE";
      this.tipoNombreAprovechamiento = "Aprovechamiento";
    }
    if (tipo == "Post Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "POSTAP";
      this.subCodTipoAprovechamiento = "SUBPOSTAP";
      this.tipoNombreAprovechamiento = "Post Aprovechamiento";
    }

    this.ref = this.dialogService.open(AprovechamientoComponent, {
      header: mensaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.params = {
          idPlanManejo: this.idPlanManejo,
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp,
          subCodTipoAprovechamiento: this.subCodTipoAprovechamiento,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
        };
        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe(() => this.getData());
      }
    });
  };

  modalFactorAmbiental = (mensaje: string, editar: boolean, data?: any) => {
    this.factoresEditar = [];
    this.actividad = "";
    if (!!data) {
      this.factoresEditar = this.factores.filter(
        (x) => x.nombreActividad == data.nombreActividad
      );
      this.actividad = data.nombreActividad;
    }
    this.ref = this.dialogService.open(FactorAmbientalComponent, {
      header: mensaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        factor: this.actividad,
        editar: editar,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (!editar) {
          this.params = {
            idEvalActividad: 0,
            codTipoActividad: "POAPRO",
            tipoActividad: "FACTOR",
            tipoNombreActividad: "Factores Ambientales",
            subCodTipoActividad: "SUBPOAPRO",
            nombreActividad: resp,
            medidasControl: "",
            medidasMonitoreo: "",
            frecuencia: "",
            acciones: "",
            responsable: "",
            idPlanManejo: this.idPlanManejo,
            idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
              .idusuario,
          };
          this.planificacionService
            .registrarActividadEvalAmbiental(this.params)
            .pipe(tap(this.handlerResult()))
            .subscribe(() => this.getData());
        } else {
          this.factoresEditar.forEach((response: any, index: number) => {
            this.params = {
              idEvalActividad: response.idEvalActividad,
              codTipoActividad: "POAPRO",
              tipoActividad: "FACTOR",
              tipoNombreActividad: "Factores Ambientales",
              subCodTipoActividad: "SUBPOAPRO",
              nombreActividad: resp,
              medidasControl: response.medidasControl,
              medidasMonitoreo: "",
              frecuencia: "",
              acciones: "",
              responsable: "",
              idPlanManejo: this.idPlanManejo,
              idUsuarioRegistro: JSON.parse(
                "" + localStorage.getItem("usuario")
              ).idusuario,
            };
            this.planificacionService
              .registrarActividadEvalAmbiental(this.params)
              .pipe(tap(this.handlerResult()))
              .pipe(
                finalize(() => {
                  if (index == 2) {
                    this.toast.ok(
                      "Se actualizó el Factor Ambiental correctamente"
                    );
                    this.getData();
                  }
                })
              )
              .subscribe();
          });
        }
      }
    });
  };

  openEliminarFactor(event: Event, data: any) {
    this.factoresEliminar = [];
    if (!!data) {
      this.factoresEliminar = this.factores.filter(
        (x) => x.nombreActividad == data.nombreActividad
      );
    }
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        this.factoresEliminar.forEach((response: any, index: number) => {
          if (response.idEvalActividad != 0) {
            var params = {
              idUsuarioElimina: this.user.idUsuario,
              idEvalActividad: response.idEvalActividad,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.planificacionService
              .eliminarEvaluacionAmbientalActividad(params)
              .pipe(
                finalize(() => {
                  this.dialog.closeAll();
                  if (index == 2) {
                    this.toast.ok(
                      "Se eliminó el Factor Ambiental correctamente"
                    );
                    this.getData();
                  }
                })
              )
              .subscribe();
          }
        });
      },
      reject: () => {},
    });
  }

  modalActividad = (data: any) => {
    this.ref = this.dialogService.open(ActividadComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        
        this.params = {
          idEvalActividad: 0,
          codTipoActividad: "POAPRO",
          tipoActividad: "INPAC",
          tipoNombreActividad: "IMPACTO",
          nombreActividad: data.nombreAprovechamiento,
          medidasControl: resp.medidasControl,
          subCodTipoEvaluacionAmbiental: "SUBIMPACTO",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };
        this.planificacionService
          .registrarActividadEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe();
        this.impactoAmbientales();
      }
    });
  };
  impactoAmbientales() {
    this.params = {
      tipoActividad: "INPAC",
    };
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad(this.params)
      .subscribe((response: any) => (this.factores = response.data));
  }

  handlerResult() {
    return {
      next: (res: any) =>
        this.messageService.add({
          severity: "success",
          detail: res?.message,
        }),
      error: (_err: any) => {
        this.messageService.add({
          severity: "warn",
          detail: _err?.message,
        });
        console.error(_err);
      },
    };
  }
}
