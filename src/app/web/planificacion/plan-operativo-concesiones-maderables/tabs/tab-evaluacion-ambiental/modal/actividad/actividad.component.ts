import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-actividad",
  templateUrl: "./actividad.component.html",
  styleUrls: ["./actividad.component.scss"],
})
export class ActividadComponent implements OnInit {
  actividad: any;

  autoResize: boolean = true;
  actividadObjt: any = {
    nombreActividad: "",
    descripcionImpacto: "",
    medidasControl: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {
    this.actividad = this.config.data.data.nombreAprovechamiento;
    this.actividadObjt.descripcionImpacto = this.config.data.data.impacto;
    this.actividadObjt.medidasControl = this.config.data.data.medidasControl;
  }

  agregar = () => {
    if (this.actividadObjt.descripcionImpacto === "") {
      this.toast.warn('(*) Debe ingresar: Descripción de impacto.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Descripción de impacto, es obligatorio",
      // });
    } else if (this.actividadObjt.medidasControl === "") {
      this.toast.warn('(*) Debe ingresar: Medida de control ambiental.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Media de control ambiental, es obligatorio",
      // });
    } else this.ref.close(this.actividadObjt);
  };
  cerrarModal() {
    this.ref.close();
  }
}
