import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-aprovechamiento",
  templateUrl: "./aprovechamiento.component.html",
  styleUrls: ["./aprovechamiento.component.scss"],
})
export class AprovechamientoComponent implements OnInit {
  nombreAprovechamiento: string = "";

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private toast: ToastService
  ) {}

  ngOnInit() {}

  agregar = () => {
    if (this.nombreAprovechamiento === "") {
      // console.log("entra ????");
      this.toast.warn('(*) Debe ingresar: Nombre Aprovechamiento.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Nombre de Aprovechamiento, es obligatorio",
      // });
    } else this.ref.close(this.nombreAprovechamiento);
  };
  cerrarModal() {
    this.ref.close();
  }
}
