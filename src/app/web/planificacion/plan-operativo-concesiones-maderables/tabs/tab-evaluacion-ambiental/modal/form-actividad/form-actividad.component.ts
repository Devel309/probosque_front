import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-form-actividad",
  templateUrl: "./form-actividad.component.html",
  styleUrls: ["./form-actividad.component.scss"],
})
export class FormActividadComponent implements OnInit {
  actividad: any;

  autoResize: boolean = true;
  lista: any[] = [
    { tipo: "Pre Aprovechamiento" },
    {
      tipo: "Aprovechamiento",
    },
    {
      tipo: "Post Aprovechamiento",
    },
    {
      tipo: "Otros",
    },
  ];
  
  actividadObjt: any = {
    actividad: "",
    nombreActividad: "",
    descripcionImpacto: "",
    medidasControl: "",
    tipo:"Pre Aprovechamiento"
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {}

  agregar = () => {
    if (this.actividadObjt.actividad === "") {
      this.toast.warn("(*) Debe ingresar: Actividad.\n");
    } else this.ref.close(this.actividadObjt);
  };
  cerrarModal() {
    this.ref.close();
  }
}
