import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-contingencia",
  templateUrl: "./contingencia.component.html",
  styleUrls: ["./contingencia.component.scss"],
})
export class ContingenciaComponent implements OnInit {
  listaContingencia: any[] = [
    { valor1: 'Incendios', codigo:'CONTIINC' },
    {
      valor1: 'Derrame de conbustible  y/o lubricantes',codigo:'CONTIDER'
    },
    {
      valor1: 'Riesgos a la salud',codigo:'CONTIRIE'
    },
    { valor1: 'Invasiones', codigo:'CONTIINV' },
    { valor1: 'Otros', codigo:'CONTIOTROS' },
  ];

  autoResize: boolean = true;
  isDisabled: boolean = false;
  contingenciaObjt: any = {
    contingencia: "",
    otraContingencia:"",
    accionesRealizar: "",
    responsable: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {
    if (this.config.data.editar == "true") {
      this.contingenciaObjt.contingencia = this.config.data.data.contingencia;
      this.contingenciaObjt.otraContingencia = this.config.data.data.contingencia;
      this.contingenciaObjt.accionesRealizar = this.config.data.data.acciones;
      this.contingenciaObjt.responsable = this.config.data.data.responsableSecundario;
      this.isDisabled = true;
    }
  }

  agregar = () => {
    if (this.contingenciaObjt.contingencia === "") {
      this.toast.warn('(*) Debe seleccionar: Contingencia.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Contingencia, es obligatorio",
      // });
    } else if (this.contingenciaObjt.accionesRealizar === "") {
      this.toast.warn('(*) Debe ingresar: Acciones a realizar.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Acciones a realizar, es obligatorio",
      // });
    } else if (this.contingenciaObjt.contingencia === "CONTIOTROS" && this.contingenciaObjt.otraContingencia === "") {
        this.toast.warn('(*) Debe ingresar: Otra contingencia.\n');

    } else if (this.contingenciaObjt.responsable === "") {
      this.toast.warn('(*) Debe ingresar: Responsable.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Responsable, es obligatorio",
      // });
    } else this.ref.close(this.contingenciaObjt);
  };
  cerrarModal() {
    this.ref.close();
  }
}
