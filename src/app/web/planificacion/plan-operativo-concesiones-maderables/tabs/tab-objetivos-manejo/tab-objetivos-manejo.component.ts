import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  Input,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { HttpParams } from "@angular/common/http";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { MessageService, SelectItem, SelectItemGroup } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ObjetivoManejoModel } from "../../../../../model/ObjetivoManejo";
import {
  DetalleObjetivo,
  MaderablesNoMaderablesResponse,
} from "../../../../../model/ObjetivoManejoEspecifico";
import { ObjetivosService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/objetivos.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { InputButtonsComponent } from "src/app/shared/components/input-button/input-buttons.component";
import { CodigoPOCCTabs, CodigoPOOC } from "../../../../../model/util/POCC/CodigoPOOC";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: "app-tab-objetivos-manejo",
  templateUrl: "./tab-objetivos-manejo.component.html",
  styleUrls: ["./tab-objetivos-manejo.component.scss"],
  providers: [MessageService],
})
export class TabObjetivosManejoComponent implements OnInit {
  @Input() idPGMF!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @ViewChild(InputButtonsComponent)
  inputButtonsComponent!: InputButtonsComponent;
  @Input() disabled!: boolean;
  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;

  objetoManejo = {} as ObjetivoManejoModel;
  idObjManejo: number = 0;
  objRegistro: DetalleObjetivo = new DetalleObjetivo();
  selectAnexo: string = "";
  idArchivo!: number;

  listObjeto: any[] = [];
  lstObjetoEspecifico: any[] = [];
  listMaderables: MaderablesNoMaderablesResponse[] = [];
  listNoMaderables: MaderablesNoMaderablesResponse[] = [];
  listMaderablesMostrar: SelectItem[] = [];
  listNoMaderablesMostrar: SelectItem[] = [];
  listMaderablesSeleccionados: MaderablesNoMaderablesResponse[] = [];
  listNoMaderablesSeleccionados: MaderablesNoMaderablesResponse[] = [];
  listMaderableNoMaderable: SelectItemGroup[] = [];
  groupedOjetivos: SelectItemGroup[] = [];
  listaEditar: any[] = [];
  listRegistrar: DetalleObjetivo[] = [];

  justificacion: string = "";
  idPlanManejoArchivo: number = 0;

  displayBasic: boolean = false;
  tituloModal: string = "Agregar Objetivo";
  listParametro = [
    { tipoLabel: "Maderable", tipoValue: "Maderable" },
    { tipoLabel: "No Maderable", tipoValue: "No Maderable" },
  ];
  parametro = {} as MaderablesNoMaderablesResponse;

  constructor(
    private objetivosService: ObjetivosService,
    private user: UsuarioService,
    private dialog: MatDialog,
    private toast: ToastService,
    private messageService: MessageService,
    private anexosService: AnexosService,
    private postulacionPFDMService: PostulacionPFDMService
  ) {}

  ngOnInit(): void {
    this.listarObjetivosPlanManejo();
  }

  listarObjetivosPlanManejo() {
    this.listMaderables = [];
    this.listNoMaderables = [];
    this.listMaderablesMostrar = [];
    this.listNoMaderablesMostrar = [];
    this.listMaderableNoMaderable = [];
    this.listObjeto = [];
    let params = {
      codigoObjetivo: "POCC",
      idPlanManejo: this.idPGMF,
    };

    this.objetivosService.listarObjetivo(params).subscribe((response: any) => {
      if (response.data.length != 0) {
        response.data.forEach((element: any) => {
          this.objetoManejo.general = element.descripcion;
          this.idObjManejo = element.idObjManejo;
          this.selectAnexo = element.descripcion;

          if (element.detalle == "Maderable") {
            this.listMaderables.push(element);
            this.listMaderablesMostrar.push({
              label: element.descripcionDetalle,
              value: element.descripcionDetalle,
            });
          }
          if (element.detalle == "No Maderable") {
            this.listNoMaderables.push(element);
            this.listNoMaderablesMostrar.push({
              label: element.descripcionDetalle,
              value: element.descripcionDetalle,
            });
          }
        });
        this.listMaderableNoMaderable.push({
          label: "Maderable",
          value: "Maderable",
          items: this.listMaderablesMostrar,
        });
        this.listMaderableNoMaderable.push({
          label: "No Maderable",
          value: "No Maderable",
          items: this.listNoMaderablesMostrar,
        });
        this.listas();
        this.marca();
      } else {
        this.listarObjetivos();
      }
    });
  }

  listas() {
    this.groupedOjetivos = [];
    this.listMaderableNoMaderable.forEach((element) => {
      this.groupedOjetivos.push(element);
    });
  }

  marca() {
    this.listObjeto = [];
    this.listNoMaderables.forEach((item) => {
      if (item.activo == "A") {
        this.listObjeto.push(item.descripcionDetalle);
      }
    });

    this.listMaderables.forEach((item) => {
      if (item.activo == "A") {
        this.listObjeto.push(item.descripcionDetalle);
      }
    });
  }

  listarObjetivos() {
    this.listMaderables = [];
    this.listNoMaderables = [];
    this.listMaderablesMostrar = [];
    this.listNoMaderablesMostrar = [];
    this.listMaderableNoMaderable = [];
    this.listObjeto = [];
    let params = {
      codigoObjetivo: "POCC",
      idPlanManejo: 0,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.objetivosService.listarObjetivo(params)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((response: any) => {
      response.data.forEach((element: any) => {
        if (element.detalle == "Maderable") {
          this.listMaderables.push({ ...element });
          this.listMaderablesMostrar.push({
            label: element.descripcionDetalle,
            value: element.descripcionDetalle,
          });
        }
        if (element.detalle == "No Maderable") {
          this.listNoMaderables.push(element);
          this.listNoMaderablesMostrar.push({
            label: element.descripcionDetalle,
            value: element.descripcionDetalle,
          });
        }
      });
      this.listMaderableNoMaderable.push({
        label: "Maderable",
        value: "Maderable",
        items: this.listMaderablesMostrar,
      });
      this.listMaderableNoMaderable.push({
        label: "No Maderable",
        value: "No Maderable",
        items: this.listNoMaderablesMostrar,
      });
      this.listas();
    });
  }

  GuardarObjetoManejo() {
    if (!this.validarObjetoManejo()) {
      return;
    }

    this.registrarObjetoManejo();
  }

  validarObjetoManejo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.listObjeto == null || this.listObjeto.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Objetivo Específico.\n";
    }

    if (this.selectAnexo == "S") {
      if (this.idPlanManejoArchivo == 0) {
        validar = false;
        mensaje = mensaje += "(*) Debe Cargar el archivo requerido.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  }

  registrarObjetoManejo() {
    this.listar();
    this.ObjetosInactivos();

    if (this.selectAnexo == "S".toUpperCase()) {
      this.objetoManejo.observacion = "amplia anexo 3";
    } else {
      this.objetoManejo.general = "";
      this.objetoManejo.observacion = "";
    }

    let params = [
      {
        idObjManejo: this.idObjManejo ? this.idObjManejo : 0,
        idPlanManejo: this.idPGMF,
        codigoObjetivo: "POCC",
        general: this.selectAnexo,
        observacion: this.objetoManejo.observacion,
        idUsuarioRegistro: this.user.idUsuario,
        listDetalle: [...this.listRegistrar],
      },
    ];
    

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.objetivosService
      .registrarObjetivo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.listarObjetivosPlanManejo();
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  listar() {
    this.listObjeto.forEach((elemen) => {
      this.filtrarIgualesMaderables(elemen);
      this.filtrarIgualesNoMaderables(elemen);
    });

    this.crearObjeto();
  }

  filtrarIgualesMaderables(element: string) {
    this.listaEditar = this.listMaderables.filter(
      (x) => x.descripcionDetalle == element
    );

    this.listaEditar
      .map((data) => {
        return {
          ...data,
          activo: "A",
        };
      })
      .forEach((item) => {
        this.listMaderablesSeleccionados.push(item);
      });
  }

  filtrarIgualesNoMaderables(element: string) {
    this.listaEditar = this.listNoMaderables.filter(
      (x) => x.descripcionDetalle == element
    );

    this.listaEditar.forEach((item) => {
      this.listNoMaderablesSeleccionados.push(item);
    });
  }

  crearObjeto() {
    if (this.idObjManejo != 0) {
      this.generarObjetoListado();
    } else {
      this.generarObjetoListado();
    }
  }

  generarObjetoListado() {
    this.listRegistrar = [];

    this.listMaderables = this.listMaderables.map((data) => {
      return {
        activo: "I",
        codigoObjetivo: data.codigoObjetivo,
        descripcion: data.descripcion,
        descripcionDetalle: data.descripcionDetalle,
        detalle: data.detalle,
        estado: data.estado,
        fechaElimina: data.fechaElimina,
        fechaModificacion: data.fechaModificacion,
        fechaRegistro: data.fechaRegistro,
        idObjManejo: data.idObjManejo,
        idObjetivoDet: data.idObjetivoDet,
        idPlanManejo: data.idPlanManejo,
        idUsuarioElimina: data.idUsuarioElimina,
        idUsuarioModificacion: data.idUsuarioModificacion,
        idUsuarioRegistro: data.idUsuarioRegistro,
        pageNum: data.pageNum,
        pageSize: data.pageSize,
        search: data.search,
        startIndex: data.startIndex,
        totalPage: data.totalPage,
        totalRecord: data.totalRecord,
      };
    });

    this.listNoMaderables = this.listNoMaderables.map((data) => {
      return {
        activo: "I",
        codigoObjetivo: data.codigoObjetivo,
        descripcion: data.descripcion,
        descripcionDetalle: data.descripcionDetalle,
        detalle: data.detalle,
        estado: data.estado,
        fechaElimina: data.fechaElimina,
        fechaModificacion: data.fechaModificacion,
        fechaRegistro: data.fechaRegistro,
        idObjManejo: data.idObjManejo,
        idObjetivoDet: data.idObjetivoDet,
        idPlanManejo: data.idPlanManejo,
        idUsuarioElimina: data.idUsuarioElimina,
        idUsuarioModificacion: data.idUsuarioModificacion,
        idUsuarioRegistro: data.idUsuarioRegistro,
        pageNum: data.pageNum,
        pageSize: data.pageSize,
        search: data.search,
        startIndex: data.startIndex,
        totalPage: data.totalPage,
        totalRecord: data.totalRecord,
      };
    });

    this.listMaderables.forEach(
      (response: MaderablesNoMaderablesResponse, index: number) => {
        this.listMaderablesSeleccionados.forEach((item) => {
          if (response.descripcionDetalle === item.descripcionDetalle) {
            const data = {
              activo: "A",
              codigoObjetivo: response.codigoObjetivo,
              descripcion: response.descripcion,
              descripcionDetalle: response.descripcionDetalle,
              detalle: response.detalle,
              estado: response.estado,
              fechaElimina: response.fechaElimina,
              fechaModificacion: response.fechaModificacion,
              fechaRegistro: response.fechaRegistro,
              idObjManejo: response.idObjManejo,
              idObjetivoDet: response.idObjetivoDet,
              idPlanManejo: response.idPlanManejo,
              idUsuarioElimina: response.idUsuarioElimina,
              idUsuarioModificacion: response.idUsuarioModificacion,
              idUsuarioRegistro: response.idUsuarioRegistro,
              pageNum: response.pageNum,
              pageSize: response.pageSize,
              search: response.search,
              startIndex: response.startIndex,
              totalPage: response.totalPage,
              totalRecord: response.totalRecord,
            };

            const obj = new DetalleObjetivo(data);
            obj.idUsuarioRegistro = this.user.idUsuario;
            obj.idObjetivo = this.idObjManejo;
            if (this.idObjManejo === 0) {
              obj.idObjEspecificoManejo = 0;
            }
            this.listRegistrar.push(obj);
            this.listMaderables.splice(index, 1, data);
          }
        });
      }
    );

    this.listNoMaderables.forEach((response: any, index: number) => {
      this.listNoMaderablesSeleccionados.forEach((item) => {
        if (response.descripcionDetalle == item.descripcionDetalle) {
          const data = {
            activo: "A",
            codigoObjetivo: response.codigoObjetivo,
            descripcion: response.descripcion,
            descripcionDetalle: response.descripcionDetalle,
            detalle: response.detalle,
            estado: response.estado,
            fechaElimina: response.fechaElimina,
            fechaModificacion: response.fechaModificacion,
            fechaRegistro: response.fechaRegistro,
            idObjManejo: response.idObjManejo,
            idObjetivoDet: response.idObjetivoDet,
            idPlanManejo: response.idPlanManejo,
            idUsuarioElimina: response.idUsuarioElimina,
            idUsuarioModificacion: response.idUsuarioModificacion,
            idUsuarioRegistro: response.idUsuarioRegistro,
            pageNum: response.pageNum,
            pageSize: response.pageSize,
            search: response.search,
            startIndex: response.startIndex,
            totalPage: response.totalPage,
            totalRecord: response.totalRecord,
          };

          const obj = new DetalleObjetivo(data);
          obj.idUsuarioRegistro = this.user.idUsuario;
          obj.idObjetivo = this.idObjManejo;
          if (this.idObjManejo === 0) {
            obj.idObjEspecificoManejo = 0;
          }
          this.listRegistrar.push(obj);
          this.listNoMaderables.splice(index, 1, data);
        }
      });
    });
  }

  ObjetosInactivos() {
    if (this.idObjManejo != 0) {
      this.listMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
      this.listNoMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
    } else {
      this.listMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.idObjEspecificoManejo = 0;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
      this.listNoMaderables
        .filter((data) => data.activo !== "A")
        .forEach((response: any) => {
          this.objRegistro = new DetalleObjetivo(response);
          this.objRegistro.idUsuarioRegistro = this.user.idUsuario;
          this.objRegistro.idObjetivo = this.idObjManejo;
          this.objRegistro.idObjEspecificoManejo = 0;
          this.objRegistro.activo = "I";
          this.listRegistrar.push(this.objRegistro);
        });
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  // eliminarArchivo(idArchivo: any) {
  //   const params = new HttpParams()
  //     .set("idArchivo", String(idArchivo))
  //     .set("idUsuarioElimina", String(this.user.idUsuario));

  //   this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
  //     if (response.success == true) {
  //       this.toast.ok("Se eliminó  el Archivo correctamente");
  //     } else {
  //       this.toast.error("Ocurrió un error al realizar la operación");
  //     }
  //   });
  // }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }

  abrirModal() {
    this.parametro = {} as MaderablesNoMaderablesResponse;
    this.displayBasic = true;
  }

  agregarObjetivo() {
    if (!this.validarNuevo()) {
      return;
    }
    if (this.parametro.detalle == "Maderable") {
      this.listMaderables.push(this.parametro);
      this.listMaderablesMostrar.push({
        label: this.parametro.descripcionDetalle,
        value: this.parametro.descripcionDetalle,
      });
    }
    if (this.parametro.detalle == "No Maderable") {
      this.listNoMaderables.push(this.parametro);
      this.listNoMaderablesMostrar.push({
        label: this.parametro.descripcionDetalle,
        value: this.parametro.descripcionDetalle,
      });
    }
    this.listMaderableNoMaderable = [];
    this.listMaderableNoMaderable.push({
      label: "Maderable",
      value: "Maderable",
      items: this.listMaderablesMostrar,
    });
    this.listMaderableNoMaderable.push({
      label: "No Maderable",
      value: "No Maderable",
      items: this.listNoMaderablesMostrar,
    });
    this.listas();
    this.marca();
    this.parametro = {} as MaderablesNoMaderablesResponse;
    this.displayBasic = false;
  }

  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.parametro.detalle == null || this.parametro.detalle == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar Tipo.\n";
    }
    if (
      this.parametro.descripcionDetalle == null ||
      this.parametro.descripcionDetalle == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Objetivo.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  cerrarModal() {
    this.parametro = {} as MaderablesNoMaderablesResponse;
    this.displayBasic = false;
  }
}
