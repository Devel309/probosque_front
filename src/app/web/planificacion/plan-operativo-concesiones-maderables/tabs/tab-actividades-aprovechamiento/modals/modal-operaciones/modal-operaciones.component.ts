import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-operaciones",
  templateUrl: "./modal-operaciones.component.html",
  styleUrls: ["./modal-operaciones.component.scss"],
})
export class ModalOperacionesComponent implements OnInit {
  context: any = {};
  operacion: string = "";
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    if (this.config.data.type == "E") {
      this.context = this.config.data.data;
    }
  }

  agregar() {
    if (!this.validar()) {
      return;
    }
    this.ref.close(this.context);
  }

  cerrarModal() {
    this.ref.close();
  }
  
  validar(): boolean {
    let validar = true;
    let mensaje = "";

    if (this.context.operaciones == null || this.context.operaciones == "") {
      mensaje = mensaje += "(*) Debe ingresar: Operaciones.\n";
      validar = false;
    }
    if (this.context.unidad == null || this.context.unidad == "") {
      mensaje = mensaje += "(*) Debe ingresar: Unidad.\n";
      validar = false;
    }
    if (
      this.context.carcteristicaTec == null ||
      this.context.carcteristicaTec == ""
    ) {
      mensaje = mensaje += "(*) Debe ingresar: Características técnicas y planificación.\n";
      validar = false;
    }
    if (this.context.personal == null || this.context.personal == "") {
      mensaje = mensaje += "(*) Debe ingresar: Personal.\n";
      validar = false;
    }
    if (
      this.context.maquinariaMaterial == null ||
      this.context.maquinariaMaterial == ""
    ) {
      mensaje = mensaje +=
        "(*) Debe ingresar: Maquinaria, equipos y materiales.\n";
      validar = false;
    }

    // if (!validar) this.ErrorMensaje(mensaje);
    if (!validar) this.toast.warn(mensaje);

    return validar;
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
