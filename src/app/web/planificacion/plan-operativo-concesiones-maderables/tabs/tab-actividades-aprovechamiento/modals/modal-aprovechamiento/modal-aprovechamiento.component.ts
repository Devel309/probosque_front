import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-aprovechamiento",
  templateUrl: "./modal-aprovechamiento.component.html",
  styleUrls: ["./modal-aprovechamiento.component.scss"],
})
export class ModalAprovechamientoComponent implements OnInit {
  context: any = {};
  operacion: string = "";
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService, 
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    if (this.config.data.type == "E") {
      this.context = this.config.data.data;
    }
  }

  agregar() {
    if (!this.validar()) {
      return;
    }
    this.ref.close(this.context);
  }
  cerrarModal() {
    this.ref.close();
  }

  validar(): boolean {
    let validar = true;
    let mensaje = "";

    if (this.context.sector == null || this.context.sector == "") {
      mensaje = mensaje += "(*) Debe ingresar: Sector.\n";
      validar = false;
    }
    if (this.context.area == null || this.context.area == "") {
      mensaje = mensaje += "(*) Debe ingresar: Área (ha).\n";
      validar = false;
    }
    if (
      this.context.totalIndividuos == null ||
      this.context.totalIndividuos == ""
    ) {
      mensaje = mensaje += "(*) Debe ingresar: N° total individuos.\n";
      validar = false;
    }
    if (this.context.individuosHA == null || this.context.individuosHA == "") {
      mensaje = mensaje += "(*) Debe ingresar: Individuos./ ha\n";
      validar = false;
    }
    if (this.context.volumen == null || this.context.volumen == "") {
      mensaje = mensaje += "(*) Debe ingresar: Volumen, peso o número aprovechable.\n";
      validar = false;
    }

    // if (!validar) this.ErrorMensaje(mensaje);
    if (!validar) this.toast.warn(mensaje);

    return validar;
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
