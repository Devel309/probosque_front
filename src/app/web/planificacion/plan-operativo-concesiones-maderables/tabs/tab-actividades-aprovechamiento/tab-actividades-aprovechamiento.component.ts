import { HttpErrorResponse } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import {
  AppMapaComponent,
  CustomFeature,
  CustomLayerView,
  descargarArchivo,
  LayerView,
  POCCTipo,
  setOneSemicolon,
  ToastService,
} from '@shared';
import { param } from 'jquery';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { prependListener } from 'process';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import {
  ActividadesAprovechamientoPOCCModel,
  CodigosActividadesAprovechamiento,
  lstactividadDet,
  lstactividadDetSub,
} from 'src/app/model/ActividadesAprovechamientoPOCCModel';
import { UrlFormatos } from 'src/app/model/urlFormatos';
import { CodigoPOCCTabs, CodigoPOOC } from 'src/app/model/util/POCC/CodigoPOOC';
import { ActividadesAprovechamientoService } from 'src/app/service/actividades-aprovechamiento-pocc.service';
import { AnexosCargaExcelService } from 'src/app/service/anexos';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { DialogTipoEspecieMultiselectComponent } from 'src/app/shared/components/dialog-tipo-especie-multiselect/dialog-tipo-especie-multiselect.component';
import {
  FieldMap,
  InfoListaModel,
  UnidadFisiograficaMap,
} from '../../../formulacion-pmfi-concesion-pfdm/tabs/pmfi-informacion-area/models';
import { ModalAprovechamientoComponent } from './modals/modal-aprovechamiento/modal-aprovechamiento.component';
import { ModalOperacionesComponent } from './modals/modal-operaciones/modal-operaciones.component';
import {environment} from '@env/environment';

@Component({
  selector: 'app-tab-actividades-aprovechamiento',
  templateUrl: './tab-actividades-aprovechamiento.component.html',
  styleUrls: ['./tab-actividades-aprovechamiento.component.scss'],
})
export class TabActividadesAprovechamientoComponent implements OnInit {
  operacionesAprovechamiento: lstactividadDet[] = [];

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;

  displayBasic: boolean = false;
  edit: boolean = false;

  actividades: [] = [];

  listActividades: any[] = [];
  selectSubdivision: string = '';
  selectRes: string = '';
  selectOpe: string = '';
  isEditedMap = false;

  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;
  idPlanManejoArchivo: number = 0;
  idPlanManejoArchivo6_1_6: number = 0;
  idPlanManejoArchivo6_1_7: number = 0;

  ActividadesAprovechamientoPOCCModel!: ActividadesAprovechamientoPOCCModel;
  CodigosActividadesAprovechamiento = CodigosActividadesAprovechamiento;

  censoComercial: ActividadesAprovechamientoPOCCModel =
    new ActividadesAprovechamientoPOCCModel();
  censoComercialVObj: ActividadesAprovechamientoPOCCModel =
    new ActividadesAprovechamientoPOCCModel();

  censoComercialOperaciones: ActividadesAprovechamientoPOCCModel =
    new ActividadesAprovechamientoPOCCModel();
  // censoComercialOObj: ActividadesAprovechamientoPOCCModel = new ActividadesAprovechamientoPOCCModel();

  list: ActividadesAprovechamientoPOCCModel[] = [];
  ref!: DynamicDialogRef;

  listEspecies: any[] = [];
  listResultados: lstactividadDet[] = [];
  listAprovechamiento: lstactividadDet[] = [];

  listEspeciesVolumen: any = [];
  pcListEspeciesVolumen: any[] = [{}];
  totalListEspeciesVolumen = {
    totalsPc1: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc2: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc3: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc4: { numArbolesPC: 0, volumenPC: 0 },
    totalArboles: 0,
    totalVolumen: 0,
  };

  areaTotalPc: number = 0;

  listEspeciesResultados: any[] = [];

  pendiente: boolean = true;

  fieldMap: FieldMap = new FieldMap();
  lstAspectoFisicoFisiografia: any[] = [];
  lstUbicacionGeografica: any[] = [];
  lstUbicacionAreaManejo: any[] = [];

  totalListEspeciesResultados: any = {
    total30a39: 0,
    total40a49: 0,
    total50a59: 0,
    total60a69: 0,
    total70a79: 0,
    total80a89: 0,
    total90aMas: 0,
    totalPc: 0,
    totalHa: 0,
  };
  UrlFormatos = UrlFormatos;

  archivo: any;

  justificacion: string = "";
  justificacion6_1_6: string = "";
  justificacion6_1_7: string = "";

  nombreGenerado: string = "";
  plantillaPOCC: string = "";

  constructor(
    private actividadesAprovechamientoService: ActividadesAprovechamientoService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private postulacionPFDMService: PostulacionPFDMService,
    private anexosCargaExcelService: AnexosCargaExcelService,
    private censoForestalService: CensoForestalService,
    private messageService: MessageService,
    private archivoService: ArchivoService
  ) {
    this.archivo = {
      file: null,
    };
  }

  ngOnInit(): void {
    this.listar();
    // this.listarResultadosForestalesMaderables();
  }

  listar() {
    this.listEspecies = [];
    this.listEspeciesVolumen = [];
    this.listEspeciesResultados = [];
    this.listAprovechamiento = [];
    this.operacionesAprovechamiento = [];

    var parasm = {
      codActvAprove: 'POCC',
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .listarActvidadAprovechamiento(parasm)
      .pipe(finalize(() => {
        this.listarAprovechamientoRFNM();
        this.dialog.closeAll()
        }))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            if (
              element.codSubActvAprove ==
              CodigosActividadesAprovechamiento.ACORDEON1
            ) {
              this.censoComercial = new ActividadesAprovechamientoPOCCModel(
                element
              );
              this.censoComercial.lstactividadDet.forEach((item) => {
                if (
                  item.codActvAproveDet ==
                  CodigosActividadesAprovechamiento.SUBACORDEON1_3
                ) {
                  this.listEspecies.push(item);
                } else if (
                  item.codActvAproveDet ==
                  CodigosActividadesAprovechamiento.SUBACORDEON1_6
                ) {
                  this.listEspeciesResultados.push(item);
                }
                if (
                  item.codActvAproveDet ==
                  CodigosActividadesAprovechamiento.SUBACORDEON1_7
                ) {
                  this.listAprovechamiento.push(item);
                }
              });

              if (
                this.listEspecies.length == 0 ||
                this.listEspeciesResultados.length == 0
              ) {
                this.listarResultadosForestalesMaderables();
              }
            } else if (
              element.codSubActvAprove ==
              CodigosActividadesAprovechamiento.ACORDEON2
            ) {
              this.censoComercialVObj = new ActividadesAprovechamientoPOCCModel(
                element
              );
              this.censoComercialVObj.lstactividadDet.forEach((item) => {
                this.listEspeciesVolumen.push(item);
              });

              if (this.listEspeciesVolumen.length == 0) {
                this.listarResultadosForestalesMaderables();
              } else {
                this.pcListEspeciesVolumen =
                  this.listEspeciesVolumen[0].lstactividadDetSub;

                this.areaTotalPc = this.listEspeciesVolumen[0].totalIndividuos;

                this.calcularTotalVolumenCorta();
              }
            } else if (
              element.codSubActvAprove ==
              CodigosActividadesAprovechamiento.ACORDEON3
            ) {
              this.censoComercialOperaciones =
                new ActividadesAprovechamientoPOCCModel(element);
              this.censoComercialOperaciones.lstactividadDet.forEach((item) => {
                this.operacionesAprovechamiento.push(item);
              });
            }
          });
        } else {
          this.listaDefault();
          this.listarResultadosForestalesMaderables();
        }
      });
  }

  listarAprovechamientoRFNM() {
    if (!this.listAprovechamiento.length) {
      var params = {
        idPlanDeManejo: this.idPlanManejo,
        tipoPlan: "POCC"
      };
      this.censoForestalService
        .AprovechamientoRFNM(params)
        .subscribe((response: any) => {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              if (element.resultadosAprovechamientoRFNMDtos.length > 0) {
                element.resultadosAprovechamientoRFNMDtos.forEach((item: any) => {
                  var objDet = new lstactividadDet();
                  objDet.sector = item.sector;
                  objDet.area = item.area;
                  objDet.totalIndividuos = item.nTotalIndividuos;
                  objDet.individuosHA = item.individuosHa;
                  objDet.volumen = item.volumen;
                  objDet.idActvAproveDet = 0;
                  objDet.codActvAproveDet = "POCCAAPROCCAR";
                  objDet.idUsuarioRegistro = this.user.idUsuario;
                  this.listAprovechamiento.push(objDet);
                });
              }
            });
          }
        });
    }
  }

  listarResultadosForestalesMaderables() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'POCC',
    };

    this.actividadesAprovechamientoService
      .listarResultadosForestalesMaderables(params)
      .subscribe((response: any) => {
        if (
          !!response.data &&
          !!response.data.censoComercialDto
            .resultadosRecursosForestalesMaderablesDto &&
          !!response.data.censoComercialDto
            .resultadosRecursosForestalesMaderablesDto.resultadosRFMTablas
        ) {
          this.listEspeciesResultados =
            response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto.resultadosRFMTablas;

          this.calculoTotalesEspeciesResultado();
        }

        if (
          this.listEspecies.length == 0 &&
          !!response.data &&
          !!response.data.censoComercialDto.listaEspecieDtos
        ) {

          response.data.censoComercialDto.listaEspecieDtos.map((data: any) => {
            var params = new lstactividadDet();
            params.codActvAproveDet =
              CodigosActividadesAprovechamiento.SUBACORDEON1_3;
            params.idUsuarioRegistro = this.user.idUsuario;
            params.nombreCientifica = data.textNombreCientifico;
            params.nombreComun = data.textNombreComun;
            params.familia = data.familia;
            params.dcm= data.dmc;
            params.idUsuarioRegistro = this.user.idUsuario;
            this.listEspecies.push(params);
          });
        }

        if (
          this.listEspeciesVolumen.length == 0 &&
          !!response.data &&
          !!response.data.censoComercialDto.volumenCortaDto &&
          !!response.data.censoComercialDto.volumenCortaDto
            .resultadosVolumenDeCortaTablas
        ) {
          response.data.censoComercialDto.volumenCortaDto.resultadosVolumenDeCortaTablas.map(
            (data: any) => {
              const newArray: lstactividadDetSub[] = [];

              var params = new lstactividadDet(data);
              params.codActvAproveDet =
                CodigosActividadesAprovechamiento.SUBACORDEON2_1;
              params.nombreCientifica = data.nombreCientifico;
              params.nombreComun = data.nombreComun;

              data.parcelaCorteDtos.map((data: any) => {
                var params = new lstactividadDetSub();
                params.nroArbol = data.numArbolesPC;
                params.volM = data.volumenPC;
                newArray.push(params);
              });

              params.lstactividadDetSub = newArray;
              params.volumen = data.volumenTotal;
              params.unidad = data.numArbolesTotales;
              params.totalIndividuos =
                response.data.censoComercialDto.volumenCortaDto.areaTotalPc;

              this.areaTotalPc =
                response.data.censoComercialDto.volumenCortaDto.areaTotalPc;
              this.listEspeciesVolumen.push(params);
            }
          );

          this.pcListEspeciesVolumen =
            this.listEspeciesVolumen[0].lstactividadDetSub;

          this.calcularTotalVolumenCorta();
        }
      });
  }

  calcularTotalVolumenCorta() {
    this.listEspeciesVolumen.map((response: any, index:number) => {

      if (response.lstactividadDetSub[0]) {
        if (!!response.lstactividadDetSub[0].volM) {
          this.totalListEspeciesVolumen.totalsPc1.volumenPC += parseFloat(
            response.lstactividadDetSub[0].volM
          );
        }

        if (!!response.lstactividadDetSub[0].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc1.numArbolesPC += parseFloat(
            response.lstactividadDetSub[0].nroArbol
          );
        }
      }

      if (response.lstactividadDetSub[1]) {
        if (!!response.lstactividadDetSub[1].volM) {
          this.totalListEspeciesVolumen.totalsPc2.volumenPC += parseFloat(
            response.lstactividadDetSub[1].volM
          );
        }

        if (!!response.lstactividadDetSub[1].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc2.numArbolesPC += parseFloat(
            response.lstactividadDetSub[1].nroArbol
          );
        }
      }

      if (response.lstactividadDetSub[2]) {
        if (!!response.lstactividadDetSub[2].volM) {
          this.totalListEspeciesVolumen.totalsPc3.volumenPC += parseFloat(
            response.lstactividadDetSub[2].volM
          );
        }

        if (!!response.lstactividadDetSub[2].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc3.numArbolesPC += parseFloat(
            response.lstactividadDetSub[2].nroArbol
          );
        }
      }

      if (response.lstactividadDetSub[3]) {
        if (!!response.lstactividadDetSub[3].volM) {
          this.totalListEspeciesVolumen.totalsPc4.volumenPC += parseFloat(
            response.lstactividadDetSub[3].volM
          );
        }

        if (!!response.lstactividadDetSub[3].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc4.numArbolesPC += parseFloat(
            response.lstactividadDetSub[3].nroArbol
          );
        }
      }

      this.totalListEspeciesVolumen.totalArboles += parseFloat(response.unidad);

      this.totalListEspeciesVolumen.totalVolumen += parseFloat(
        response.volumen
      );
    });
  }

  calculoTotalesEspeciesResultado() {
    this.listEspeciesResultados.map((data) => {
      this.totalListEspeciesResultados.total30a39 += parseFloat(
        data.dap30a39 + data.vol30a39
      );

      this.totalListEspeciesResultados.total40a49 += parseFloat(
        data.dap40a49 + data.vol40a49
      );

      this.totalListEspeciesResultados.total50a59 += parseFloat(
        data.dap50a59 + data.vol50a59
      );

      this.totalListEspeciesResultados.total60a69 += parseFloat(
        data.dap60a69 + data.vol60a69
      );

      this.totalListEspeciesResultados.total70a79 += parseFloat(
        data.dap70a79 + data.vol70a79
      );

      this.totalListEspeciesResultados.total80a89 += parseFloat(
        data.dap80a89 + data.vol80a89
      );

      this.totalListEspeciesResultados.total90aMas += parseFloat(
        data.dap90aMas + data.vol90aMas
      );

      this.totalListEspeciesResultados.totalPC += parseFloat(
        data.dapTotalPc + data.volTotalPC
      );
      this.totalListEspeciesResultados.totalHa += parseFloat(
        data.dapTotalporHa + data.volTotalporHa
      );
    });


  }

  listaDefault() {
    var params = {
      codActvAprove: 'POCC',
      codSubActvAprove: 'POCCAAPROOA',
      idPlanManejo: null,
    };
    this.actividadesAprovechamientoService
      .listarActvidadAprovechamiento(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idPlanManejo == null)
            this.censoComercialOperaciones =
              new ActividadesAprovechamientoPOCCModel(element);
          this.censoComercialOperaciones.idActvAprove = 0;
          this.censoComercialOperaciones.idUsuarioRegistro =
            this.user.idUsuario;
          this.censoComercialOperaciones.idPlanManejo = this.user.idUsuario;
          this.censoComercialOperaciones.lstactividadDet.forEach((item) => {
            let obj = new lstactividadDet(item);
            obj.idActvAproveDet = 0;
            obj.idUsuarioRegistro = this.user.idUsuario;
            this.operacionesAprovechamiento.push(obj);
          });
        });
      });
  }

  //*********************** 6.1.3 *************************** */

  openModalEspecies = (mesaje: string, data: any, tipo: any, index: any) => {
    this.ref = this.dialogService.open(DialogTipoEspecieMultiselectComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == 'C') {
          resp.forEach((element: any) => {
            var params = new lstactividadDet(element);
            params.codActvAproveDet =
              CodigosActividadesAprovechamiento.SUBACORDEON1_3;
            params.idUsuarioRegistro = this.user.idUsuario;
            this.listEspecies.push(params);
          });
        } else if (tipo == 'E') {
          this.listEspecies[index] = resp;
        }
      }
    });
  };

  openModalEspeciesVolumen = (
    mesaje: string,
    data: any,
    tipo: any,
    index: any
  ) => {
    this.ref = this.dialogService.open(DialogTipoEspecieMultiselectComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == 'C') {
          resp.forEach((element: any) => {
            var params = new lstactividadDet(element);
            params.codActvAproveDet =
              CodigosActividadesAprovechamiento.SUBACORDEON2_1;
            this.listEspeciesVolumen.push(params);
          });
        }
        this.listEspeciesVolumen.forEach((item: any) => {
          if (item.lstactividadDetSub.length == 0) {
            let obj1 = new lstactividadDetSub();
            obj1.idUsuarioRegistro = this.user.idUsuario;
            let obj2 = new lstactividadDetSub();
            obj2.idUsuarioRegistro = this.user.idUsuario;
            let obj3 = new lstactividadDetSub();
            obj3.idUsuarioRegistro = this.user.idUsuario;
            let obj4 = new lstactividadDetSub();
            obj4.idUsuarioRegistro = this.user.idUsuario;
            let obj5 = new lstactividadDetSub();
            obj5.idUsuarioRegistro = this.user.idUsuario;
            item.lstactividadDetSub.push(obj1, obj2, obj3, obj4, obj5);
          }
        });
      }
    });
  };

  openEliminar(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActvAproveDet != 0) {
          var params = {
            idActvAproveDet: data.idActvAproveDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesAprovechamientoService
            .eliminarActvidadAprovechamientoDet(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó el registro correctamente.');
                this.listEspecies.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listEspecies.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  //*********************** 6.1.6 *************************** */
  openModalResultados(mesaje: string, data: any, tipo: any, index: any) {
    this.ref = this.dialogService.open(DialogTipoEspecieMultiselectComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          var params = new lstactividadDet(element);
          params.codActvAproveDet =
            CodigosActividadesAprovechamiento.SUBACORDEON1_6;
          params.idUsuarioRegistro = this.user.idUsuario;
          this.listEspeciesResultados.push(params);
        });

        this.listEspeciesResultados.forEach((item) => {
          if (item.lstactividadDetSub.length == 0) {
            var obj = new lstactividadDetSub();
            obj.var = 'N';
            obj.idUsuarioRegistro = this.user.idUsuario;
            item.lstactividadDetSub.push(obj);

            var obj2 = new lstactividadDetSub();
            obj2.var = 'Vc (m3)';
            obj2.idUsuarioRegistro = this.user.idUsuario;
            item.lstactividadDetSub.push(obj2);
          }
        });
      }
    });
  }

  openEliminarResultado(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActvAproveDet != 0) {
          var params = {
            idActvAproveDet: data.idActvAproveDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesAprovechamientoService
            .eliminarActvidadAprovechamientoDet(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó el registro correctamente.');
                this.listEspeciesResultados.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listEspeciesResultados.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  //*********************** 6.1.7 *************************** */
  openModalAprovechamiento(mesaje: string, data: any, tipo: any, index: any) {
    this.ref = this.dialogService.open(ModalAprovechamientoComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == 'C') {
          let params = new lstactividadDet(resp);
          params.codActvAproveDet =
            CodigosActividadesAprovechamiento.SUBACORDEON1_7;
          params.idUsuarioRegistro = this.user.idUsuario;
          this.listAprovechamiento.push(params);
        } else if (tipo == 'E') {
          this.listAprovechamiento[index] = resp;
        }
      }
    });
  }
  openEliminarAprovechamiento(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActvAproveDet != 0) {
          var params = {
            idActvAproveDet: data.idActvAproveDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesAprovechamientoService
            .eliminarActvidadAprovechamientoDet(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó el registro correctamente.');
                this.listAprovechamiento.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listAprovechamiento.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  openEliminarEspeciesVolumen(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActvAproveDet != 0) {
          var params = {
            idActvAproveDet: data.idActvAproveDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesAprovechamientoService
            .eliminarActvidadAprovechamientoDet(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó el registro correctamente.');
                this.listEspeciesVolumen.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listEspeciesVolumen.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  // ************************* 6.3 ************************** //
  openModalOperaciones = (mesaje: string, data: any, tipo: any, index: any) => {
    this.ref = this.dialogService.open(ModalOperacionesComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == 'C') {
          let params = new lstactividadDet(resp);
          params.codActvAproveDet =
            CodigosActividadesAprovechamiento.SUBACORDEON3_1;
          params.idUsuarioRegistro = this.user.idUsuario;
          this.operacionesAprovechamiento.push(params);
        } else if (tipo == 'E') {
          this.operacionesAprovechamiento[index] = resp;
        }
      }
    });
  };

  openEliminarOperaciones(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActvAproveDet != 0) {
          var params = {
            idActvAproveDet: data.idActvAproveDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesAprovechamientoService
            .eliminarActvidadAprovechamientoDet(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok(
                  'Se eliminó operación de aprovechamiento correctamente.'
                );
                this.operacionesAprovechamiento.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.operacionesAprovechamiento.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  validarInformacionAnexos(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.censoComercialOperaciones.anexo == 'S') {
      if(this.justificacion==''){
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3 de Operaciones de aprovechamiento, debe ingresar una justificación.\n";
      }

      if (
        this.idPlanManejoArchivo == 0
      ) {
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3, debe adjuntar una evidencia.\n";
      }
    }
    if (this.censoComercial.detalle == 'S') {
      if(this.justificacion6_1_7==''){
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3 de Aprovechamiento de recursos forestales no maderables o fauna silvestre, debe ingresar una justificación.\n";
      }

      if (
        this.idPlanManejoArchivo6_1_7 == 0
      ) {
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3, debe adjuntar una evidencia.\n";
      }
    }

    if (this.censoComercial.descripcion== 'S') {
      if(this.justificacion6_1_6==''){
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3 de Resultados de recursos forestales no maderables, debe ingresar una justificación.\n";
      }

      if (
        this.idPlanManejoArchivo6_1_6 == 0
      ) {
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3, debe adjuntar una evidencia.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);



    return validar;
  }



  // ************** guardar ***************************//
  guardar() {
    if(!this.validarInformacionAnexos()) return;


    this.list = [];
    // 3.1
    let censoComercialMObj = new ActividadesAprovechamientoPOCCModel(
      this.censoComercial
    );
    censoComercialMObj.codActvAprove =
      CodigosActividadesAprovechamiento.ACTIVIDAD;
    censoComercialMObj.codSubActvAprove =
      CodigosActividadesAprovechamiento.ACORDEON1;
    censoComercialMObj.idPlanManejo = this.idPlanManejo;
    censoComercialMObj.idUsuarioRegistro = this.user.idUsuario;
    this.listEspecies.forEach((item) => {
      censoComercialMObj.lstactividadDet.push(item);
    });
    // this.listEspeciesResultados.forEach((item) => {
    //   censoComercialMObj.lstactividadDet.push(item);
    // });
    this.listAprovechamiento.forEach((item) => {
      censoComercialMObj.lstactividadDet.push(item);
    });

    //3.2
    // this.censoComercialVObj = new ActividadesAprovechamientoPOCCModel();
    this.censoComercialVObj.codActvAprove =
      CodigosActividadesAprovechamiento.ACTIVIDAD;
    this.censoComercialVObj.codSubActvAprove =
      CodigosActividadesAprovechamiento.ACORDEON2;
    this.censoComercialVObj.idPlanManejo = this.idPlanManejo;
    this.censoComercialVObj.idUsuarioRegistro = this.user.idUsuario;
    this.censoComercialVObj.lstactividadDet = [...this.listEspeciesVolumen];

    //3.3
    let censoComercialOObj = new ActividadesAprovechamientoPOCCModel(
      this.censoComercialOperaciones
    );
    censoComercialOObj.codActvAprove =
      CodigosActividadesAprovechamiento.ACTIVIDAD;
    censoComercialOObj.codSubActvAprove =
      CodigosActividadesAprovechamiento.ACORDEON3;
    censoComercialOObj.idPlanManejo = this.idPlanManejo;
    censoComercialOObj.idUsuarioRegistro = this.user.idUsuario;
    censoComercialOObj.lstactividadDet = [...this.operacionesAprovechamiento];

    this.list.push(
      censoComercialMObj,
      this.censoComercialVObj,
      censoComercialOObj
    );

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .registrarActvidadAprovechamiento(this.list)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok(
            'Se registró actividad de aprovechamiento correctamente.'
          );
          this.listar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  // ********************************************************************* //
  abrirModalActividades() {
    this.displayBasic = true;
  }

  agregarActividad() {}

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  loadShape(layers: CustomLayerView<UnidadFisiograficaMap>[] | LayerView[]) {
    if (layers?.length > 2) {
      this.toast.warn('Archivo no válido');
      this.map.deleteGroupLayer(String(layers[0].groupId));
      return;
    }
    const layer = layers[0];
    const items = this.setUnidadFisiografica(layer);
    this.lstAspectoFisicoFisiografia = [...items];
    this.fieldMap.isEditedMap = true;
    const title = this.map.joinTitle(`${layer.title}`);
    this.fieldMap.relacionArchivoTabla += `${POCCTipo.POCC_UF}:${title};`;
  }

  setUnidadFisiografica(layer: LayerView) {
    let items: InfoListaModel[] = [];
    let areaTotal = 0;
    let porcentaje = 0;
    layer.features.forEach((feature: any) => {
      const f = feature as CustomFeature<UnidadFisiograficaMap>;
      const areaHa = this.map.calculateAreaHaPolygon(f);
      const item: InfoListaModel = new InfoListaModel({
        descripcion: `${f?.properties?.CV_Label}-${f?.properties?.CobVeg2013}-${f?.properties?.Fisiogr}`,
        areaHa,
        areaHaPorcentaje: (areaHa * 100) / layer.area,
        codInfBasicaDet: POCCTipo.POCC_UF, //cambiar
        idUsuarioRegistro: this.user.idUsuario,
      });
      items.push(item);
      areaTotal += areaHa;
      porcentaje += item.areaHaPorcentaje;
    });
    this.fieldMap.totalAreaFisiografia = areaTotal;
    this.fieldMap.totalPorcentajeFisiografia = porcentaje;
    return items;
  }

  deleteLayer(l: LayerView) {
    let relaciones = setOneSemicolon(this.fieldMap.relacionArchivoTabla).split(
      ';'
    );
    let titulo = this.map.joinTitle(l.title);
    const index = relaciones.findIndex((x) => x.endsWith(titulo));
    if (index > -1) {
      const item = relaciones[index].split(':'); // PMFIUG:nombreArchivo
      const nameList = item[0];
      relaciones.splice(index, 1);
      switch (nameList) {
        case POCCTipo.POCC_AP:
          this.lstUbicacionGeografica = [];
          this.fieldMap.totalAreaPredio = 0;
          break;
        case POCCTipo.POCC_AM:
          this.lstUbicacionAreaManejo = [];
          this.fieldMap.totalAreaManejo = 0;
          break;
        case POCCTipo.POCC_UF:
          this.lstAspectoFisicoFisiografia = [];
          this.fieldMap.totalAreaFisiografia = 0;
          this.fieldMap.totalPorcentajeFisiografia = 0;
      }
      this.fieldMap.relacionArchivoTabla = relaciones.join(';');
    }
  }
  deleteAllLayers() {
    this.fieldMap.relacionArchivoTabla = '';
    this.lstUbicacionGeografica = [];
    this.lstUbicacionAreaManejo = [];
    this.lstAspectoFisicoFisiografia = [];
    this.fieldMap.totalAreaManejo = 0;
    this.fieldMap.totalAreaPredio = 0;
    this.fieldMap.totalAreaFisiografia = 0;
    this.fieldMap.totalPorcentajeFisiografia = 0;
  }
  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }
  cargarIdArchivo6_1_6(data: any) {
    this.idPlanManejoArchivo6_1_6 = data.idPGMFArchivo;
  }
  cargarIdArchivo6_1_7(data: any) {
    this.idPlanManejoArchivo6_1_7 = data.idPGMFArchivo;
  }

  justificacionEmiter(data: any) {
    this.justificacion = data;
  }
  justificacionEmiter6_1_6(data: any) {
    this.justificacion6_1_6 = data;
  }
  justificacionEmiter6_1_7(data: any) {
    this.justificacion6_1_7 = data;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }

  valCheck(event: any) {
    if (event.checked == false) {
      this.anexoN(event);
    }
  }

  cargarFormato(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Hoja1",
          numeroFila: 2,
          numeroColumna: 1,
          codActvAprove: CodigosActividadesAprovechamiento.ACTIVIDAD,
          codSubActvAprove: CodigosActividadesAprovechamiento.ACORDEON1,
          codActvAproveDet: CodigosActividadesAprovechamiento.SUBACORDEON1_3,
          idActvAprove: this.censoComercial.idActvAprove === undefined ? 0 : this.censoComercial.idActvAprove,
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.actividadesAprovechamientoService
          .registrarCensoComercialEspecieExcel(t.file, item.nombreHoja, item.numeroFila, item.numeroColumna,
            item.codActvAprove, item.codSubActvAprove, item.codActvAproveDet,
            item.idActvAprove, item.idPlanManejo, item.idUsuarioRegistro)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listar();
              
              files = null;
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }

  varAssets = `${environment.varAssets}`;
 /*  btnDescargarFormato() {
    let urlExcel:string= "";
    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }

    window.location.href = urlExcel;
} */

btnDescargarFormato() {
  this.dialog.open(LoadingComponent, { disableClose: true });
  this.nombreGenerado = UrlFormatos.CARGA_MASIVA_POCC;
  this.archivoService
    .descargarPlantilla(this.nombreGenerado)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((res: any) => {
      if (res.isSuccess == true) {
        this.toast.ok(res?.message);
        this.plantillaPOCC = res;
        descargarArchivo(this.plantillaPOCC);
      } else {
        this.toast.error(res?.message);
      }
    });
}


  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService.registrarCargaMasiva(
        this.archivo.file
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok(
            "Se registró el archivo correctamente.\n"
          );

          this.listar();

          this.archivo.file = "";
        } else {
          this.toast.error("Ocurrió un error.");
        }
      });
  }

}
