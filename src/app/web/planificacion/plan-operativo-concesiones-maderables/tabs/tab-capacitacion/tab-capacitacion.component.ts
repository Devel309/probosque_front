import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { CapacitacionModel } from "src/app/model/Capacitacion";
import { ParametroModel } from "src/app/model/Parametro";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CapacitacionDetalleModel } from "src/app/model/CapacitacionDetalle";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { HttpErrorResponse } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import {
  CapacitacionPGMFA,
  ListCapacitacionDetalle,
} from "src/app/model/capacitacionPGMFA";
import { UsuarioService } from "@services";
import { CapacitacionService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/capacitacion.service";
import { ToastService } from "@shared";
import { CodigoPOCCTabs, CodigoPOOC } from "src/app/model/util/POCC/CodigoPOOC";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
@Component({
  selector: "app-tab-capacitacion",
  templateUrl: "./tab-capacitacion.component.html",
  styleUrls: ["./tab-capacitacion.component.scss"],
})
export class TabCapacitacionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  tituloModal: string = "Registrar Capacitación";
  displayBasic: boolean = false;

  capacitacion = {} as CapacitacionModel;
  capacitaciones: any[] = [];
  edit: boolean = false;
  parametro = {} as ParametroModel;
  tema = [] as any;
  temas = [] as any;
  usuario = {} as UsuarioModel;

  capacitacionObj: CapacitacionPGMFA = new CapacitacionPGMFA();
  listCapacitacion: CapacitacionPGMFA[] = [];
  detalleLista: ListCapacitacionDetalle[] = [];

  listParametro = [
    { valorPrimario: "Presencial", valorSecundario: "1" },
    { valorPrimario: "Virtual", valorSecundario: "2" },
  ];

  CodigoPOCC = CodigoPOOC;
  CodigoPOCCTabs = CodigoPOCCTabs;
  idPlanManejoArchivo: number = 0;
  selectAnexo: string = "";
  rowIndex: number = 0;

  justificacion: string = "";

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private capacitacionService: CapacitacionService,
    private toast: ToastService,
    private postulacionPFDMService: PostulacionPFDMService
  ) {}

  ngOnInit(): void {
    this.listaCapacitacion();
  }

  agregarParticipacion() {
    if (!this.validarCapacitacion()) {
      return;
    }
    if (this.edit) {
      this.editarCapacitacion();
    } else {
      this.registrarCapacitacion();
    }
  }

  listaCapacitacion() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoCapacitacion: "POCC",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.capacitacionService
      .listarCapacitacion(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.selectAnexo = element.adjunto;
        });
        this.capacitaciones = response.data;
        this.dialog.closeAll();
      });
  }

  validarCapacitacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.temas == null || this.temas.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Tema o Actividad.\n";
    }
    if (
      this.capacitacion.personaCapacitar == null ||
      this.capacitacion.personaCapacitar == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Personal.\n";
    }
    if (
      this.parametro.valorSecundario == null ||
      this.parametro.valorSecundario == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Modalidad.\n";
    }
    if (this.capacitacion.lugar == null || this.capacitacion.lugar == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Lugar.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  abrirModal() {
    this.temas = [];
    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = true;
  }

  cerrarModal() {
    this.temas = [];
    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = "Registrar Capacitación";
  }

  registrarCapacitacion() {
    const obj = {} as any;
    obj.idCapacitacion = 0;
    obj.estado = "A";
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.lugar = this.capacitacion.lugar;
    obj.personaCapacitar = this.capacitacion.personaCapacitar;
    obj.periodo = this.capacitacion.periodo;
    obj.responsable = this.capacitacion.responsable;

    obj.idTipoModalidad = this.parametro.valorSecundario;
    for (let val of this.listParametro) {
      if (val.valorSecundario == this.parametro.valorSecundario) {
        obj.idTipoModalidad = val.valorPrimario;
      }
    }
    const listCapDet: CapacitacionDetalleModel[] = [];
    for (let item of this.temas) {
      const cap = {} as CapacitacionDetalleModel;
      cap.actividad = item.actividad;
      cap.idCapacitacionDet = 0;
      listCapDet.push(cap);
    }

    obj.listCapacitacionDetalle = listCapDet;
    obj.idPlanManejo = this.idPlanManejo;
    this.capacitaciones.push(obj);

    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;
  }

  editarCapacitacion() {
    const obj = {} as any;
    obj.idCapacitacion = this.capacitacion.idCapacitacion;
    obj.estado = "A";
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.lugar = this.capacitacion.lugar;
    obj.personaCapacitar = this.capacitacion.personaCapacitar;
    obj.periodo = this.capacitacion.periodo;
    obj.responsable = this.capacitacion.responsable;

    obj.idTipoModalidad = this.parametro.valorSecundario;
    for (let val of this.listParametro) {
      if (val.valorSecundario == this.parametro.valorSecundario) {
        obj.idTipoModalidad = val.valorPrimario;
      }
    }
    const listCapDet: CapacitacionDetalleModel[] = [];
    for (let item of this.temas) {
      const cap = {} as CapacitacionDetalleModel;
      cap.actividad = item.actividad;
      cap.idCapacitacionDet = item.idCapacitacionDet;
      listCapDet.push(cap);
    }

    obj.listCapacitacionDetalle = listCapDet;
    obj.idPlanManejo = this.idPlanManejo;

    this.capacitaciones[this.rowIndex] = obj;

    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;

    this.displayBasic = false;
    this.tituloModal = "Registrar Capacitación";
    this.edit = false;
  }

  agregarActividad() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.tema == null || this.tema == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar el Tema o Actividad.\n";
    }
    if (!validar) {
      this.ErrorMensaje(mensaje);
    } else {
      const obj = {} as any;
      obj.actividad = this.tema;
      obj.idCapacitacionDet = 0;
      this.temas.push(obj);
      this.tema = "";
    }
  }

  justificacionEmiter(data: any) {
    this.justificacion = data;
  }

  validarInformacionAnexos(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectAnexo == 'S') {

      if(this.justificacion==''){
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3, debe ingresar una justificación.\n";

      }

      if (
        this.idPlanManejoArchivo == 0
      ) {
        validar = false;
        mensaje = mensaje += "(*) Al ampliar información en el anexo 3, debe adjuntar una evidencia.\n";
      }


    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  guardarCapacitacion() {
    if(!this.validarInformacionAnexos()) return

    let validar: boolean = true;
    let mensaje: string = "";




    this.listCapacitacion = [];

    this.capacitaciones.forEach((response: any) => {
      this.capacitacionObj = new CapacitacionPGMFA(response);
      this.capacitacionObj.idCapacitacion = response.idCapacitacion;
      this.capacitacionObj.codTipoCapacitacion = "POCC";
      this.capacitacionObj.idUsuarioRegistro = this.user.idUsuario;
      this.capacitacionObj.adjunto = this.selectAnexo;
      this.capacitacionObj.idPlanManejo = this.idPlanManejo;
      for (let val of this.listParametro) {
        if (response.idTipoModalidad == val.valorPrimario) {
          this.capacitacionObj.idTipoModalidad = val.valorSecundario;
        }
      }
      this.detalleLista = [];
      response.listCapacitacionDetalle.forEach((item: any) => {
        let capacitacionDetalle = new ListCapacitacionDetalle(item);
        capacitacionDetalle.idUsuarioRegistro = this.user.idUsuario;
        //capacitacionDetalle
        this.detalleLista.push(capacitacionDetalle);
      });
      this.capacitacionObj.listCapacitacionDetalle = this.detalleLista;
      this.listCapacitacion.push(this.capacitacionObj);
    });

    if (this.capacitaciones == null || this.capacitaciones.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) No hay Capacitaciones registradas.\n";
    }

    if (!validar) {
      this.ErrorMensaje(mensaje);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.capacitacionService
        .registrarCapacitacionDetalle(this.listCapacitacion)
        .subscribe(
          (result: any) => {
            this.dialog.closeAll();
            this.listaCapacitacion();
            this.messageService.add({
              severity: "success",
              summary: "",
              detail: result.message,
            });
          },
          (error: HttpErrorResponse) => {
            this.dialog.closeAll();
          }
        );
    }
  }

  eliminarTema(event: Event, index: number, params: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (params.idCapacitacionDet != 0) {
          var parm = {
            idCapacitacionDet: params.idCapacitacionDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.capacitacionService
            .eliminarCapacitacionDetalle(parm)
            .subscribe((res: any) => {
              if (res.success == true) {
                this.temas.splice(index, 1);
                // this.toast.ok(res?.message);
                this.toast.ok(
                  "Se eliminó la actividad de la capacitación con éxito."
                );
              } else {
                this.toast.error(res?.message);
              }
            });
        } else {
          this.temas.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  openEditarCapacitacion(obj: any, rowIndex: number) {
    this.capacitacion.idCapacitacion = obj.idCapacitacion;
    this.capacitacion.personaCapacitar = obj.personaCapacitar;
    this.capacitacion.lugar = obj.lugar;
    this.parametro.valorSecundario = obj.idTipoModalidad;
    this.temas = obj.listCapacitacionDetalle;
    this.capacitacion.idUsuarioRegistro = this.usuario.idusuario;
    this.capacitacion.estado = "A";
    this.capacitacion.periodo = obj.periodo
    this.capacitacion.responsable = obj.responsable
    if (obj.idTipoModalidad === 1) {
      this.parametro.valorSecundario = "1"
    } else {
      this.parametro.valorSecundario = "2"
    }
    this.rowIndex = rowIndex;

    this.displayBasic = true;
    this.edit = true;
    this.tituloModal = "Editar Capacitación";
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  openEliminarCapacitacion(event: Event, index: number, obj: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (obj.idCapacitacion != 0) {
          var parm = {
            idCapacitacion: obj.idCapacitacion,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.capacitacionService
            .eliminarCapacitacion(parm)
            .subscribe((res: any) => {
              if (res.success == true) {
                this.temas.splice(index, 1);
                this.toast.ok(res?.message);
                this.listaCapacitacion();
                this.dialog.closeAll();
              } else {
                this.toast.error(res?.message);
                this.dialog.closeAll();
              }
            });
        } else {
          this.capacitaciones.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }
  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {});
  }
}
