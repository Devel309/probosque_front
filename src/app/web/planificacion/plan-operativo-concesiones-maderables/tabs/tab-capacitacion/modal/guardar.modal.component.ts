import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { CapacitacionDetalleMaderableModel } from 'src/app/model/CapacitacionMaderableModel';

@Component({
  selector: 'guardar-modal',
  templateUrl: './guardar.modal.component.html',
  styleUrls: ['./guardar.modal.component.scss']
})
export class GuardarModalComponent {
  f: FormGroup;
  detalle: CapacitacionDetalleMaderableModel = new CapacitacionDetalleMaderableModel();
  labelBtn: string = '';
  

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
  ) {
    this.f = this.initForm();
  }

  ngOnInit() {
    this.detalle = this.config.data as CapacitacionDetalleMaderableModel;
    const { actividad, personal, modalidad, lugar, periodo, responsable } = this.detalle;
    this.f.patchValue({ actividad, personal, modalidad, lugar, periodo, responsable });
    this.labelBtn = this.detalle.idCapacitacionDet == null ? 'Agregar' : 'Editar';
  }

  initForm() {
    return this.fb.group({
      actividad: [null, [Validators.required, Validators.minLength(1)]],
      personal: [null, [Validators.required, Validators.minLength(1)]],
      modalidad: [null, [Validators.required, Validators.minLength(1)]],
      lugar: [null, [Validators.required, Validators.minLength(1)]],
      periodo: [null, [Validators.required, Validators.minLength(1)]],
      responsable: [null, [Validators.required, Validators.minLength(1)]],
    });
  }

  guardar() {
    if (this.f.invalid) {
      this.messageService.add({ severity: "warn", detail: "Todos los campos son obligatorios.", key: 'gm_01' });
      return;
    }
    
    this.f.value.periodo = this.f.value.periodo.replace('_', '');
    const { actividad, personal, modalidad, lugar, periodo, responsable } = this.f.value;
    this.detalle = { ...this.detalle, actividad, personal, modalidad, lugar, periodo, responsable };
    this.ref.close(this.detalle);

  }

  cancelar() {
    this.ref.close();
  }

}