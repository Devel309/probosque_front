import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { type } from 'os';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import {
  PoccResumenActModel,
  PoccResumenActRecModel,
} from 'src/app/model/poccResumenActividades';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ResumenComponent } from './resumen/resumen.component';
import {UsuarioService} from '@services';
import {ResumenActividad} from '../../../../../model/PlanManejo/ResumenActividades/ResumenActividad';
import {ResumenActividadDet} from '../../../../../model/PlanManejo/ResumenActividades/ResumenActividadDet';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {CodigoPOOC} from '../../../../../model/util/POCC/CodigoPOOC';
import {InformacionBasicaDetalleModel} from '../../../../../model/PlanManejo/InformacionBasica/InformacionBasicaDetalleModel';
import {finalize} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {LoadingComponent} from '../../../../../components/loading/loading.component';

@Component({
  selector: 'app-resumen-actividades',
  templateUrl: './resumen-actividades.component.html',
  styleUrls: ['./resumen-actividades.component.scss'],
})
export class TabResumenActividadesComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();

  @Input() idPlanManejo!: number;

  displayBasic: boolean = false;
  editar: boolean = false;

  resultadosModal : string|null = null;
  index_modal_2_1 : number = -1;
  index_modal_2_2 : number = -1;
  acciones_2_2 :string = "";

  isShowModal2_2:boolean = false;
  isShowModal2_1:boolean = false;

  listado_2 :any[]=[];


  actividadModal:string = "";
  listResumenActividadDetModal: any[]= [];

  resumeActividad_2_1: ResumenActividad = new ResumenActividad({
    codTipoResumen:CodigoProceso.PLAN_OPERATIVO_CONCESIONES,
    codigoResumen:CodigoPOOC.TAB_2,
    codigoSubResumen:CodigoPOOC.ACORDEON_2_1,
    idUsuarioRegistro : this.user.idUsuario
  });


  resumeActividad_2_2: ResumenActividad = new ResumenActividad({
    codTipoResumen:CodigoProceso.PLAN_OPERATIVO_CONCESIONES,
    codigoResumen:CodigoPOOC.TAB_2,
    codigoSubResumen:CodigoPOOC.ACORDEON_2_2,
    idUsuarioRegistro : this.user.idUsuario
  });

  listResumenActividad_2_2: ResumenActividadDet[] = [];

  resumenPO = {} as PoccResumenActModel;
  codTipoResumen: string = 'POCC';
 // idPlanManejo!: number;
  idUsuarioRegistro: number =0; //JSON.parse('' + localStorage.getItem('usuario')).idusuario;

  cabecera: any;
  listResumenEvaluacionAux: any = [];
  listaGuardar: any[] = [];
  params: any;

  listResumenEvaluacion: any[] = [
  ];
  censoIndicador: any[] = [];
  infraIndicador: any[] = [];
  aprovIndicador: any[] = [];
  silvIndicador: any[] = [];
  protIndicador: any[] = [];
  monitIndicador: any[] = [];
  captIndicador: any[] = [];
  listResponse: any[] = [];
  ref!: DynamicDialogRef;

  idResumenAct_2_1 = 0;
  idResumenAct_2_2 = 0;
  constructor(
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private user: UsuarioService,
    private dialog: MatDialog,
  ) {
  }

  listResumenResultPOEjecutado: any[] = [];

  ngOnInit(): void {
    this.idUsuarioRegistro = this.user.idUsuario;
    //this.getListaResumenDetalle();
    this.getResumenActividades();

    /*
    this.listado_2.push({
      actividad :"Censo Comercial",
      listResumenEvaluacion : [
        {indicador:"Superficie Censada",programado:"", realizado:""},
        {indicador:"Numero de Especies Censadas",programado:"", realizado:""},
        {indicador:"n° de arboles censados",programado:"", realizado:""},
      ]
    },
      {
        actividad :"Construcción de Infraestructura",
        listResumenEvaluacion : [
          {indicador:"Número y tamaño de patios de acopio",programado:"", realizado:""},
          {indicador:"Longitud de caminos primarios",programado:"", realizado:""},
          {indicador:"Longitud de caminos secundarios",programado:"", realizado:""},
          {indicador:"Longitud de caminos empalizados",programado:"", realizado:""},
        ]
      }
    );*/
  }

  validacionCampos() {
    let esValido = true;
    let mensaje = '';

    if (!this.resumenPO.nroResolucion) {
      mensaje += '(*) Debe ingresar N° Resolución que aprueba el PO.\n';
      esValido = false;
    }
    if (!this.resumenPO.nroPcs) {
      mensaje += '(*) Debe ingresar número de PC(s).\n';
      esValido = false;
    }
    if (!this.resumenPO.areaHa) {
      mensaje += '(*) Debe ingresar área (ha).\n';
      esValido = false;
    }

    if (!esValido) this.toast('error', mensaje);

    return esValido;
  }

  getResumenActividades(){

    this.listado_2 = [];
    this.listResumenActividad_2_2=[];

    this.planificacionService
      .listarResumen_Detalle({
        idPlanManejo:this.idPlanManejo,
        codTipoResumen:CodigoProceso.PLAN_OPERATIVO_CONCESIONES
      })
      .subscribe((response: any) => {
        if (response.success) {

          if(response.data && response.data.length>0){

          if(response.data && response.data[0].idPlanManejo != null){
            let res = response.data.find((e:any)=>e.codigoSubResumen == CodigoPOOC.ACORDEON_2_1);
            if(res){
              let idPlanBd  = res.idPlanManejo;
              this.resumeActividad_2_1.idResumenAct = res.idResumenAct? res.idResumenAct :0;
              this.resumeActividad_2_1.idPlanManejo = this.idPlanManejo;
              this.resumeActividad_2_1.nroPcs = res.nroPcs;
              this.resumeActividad_2_1.nroResolucion = res.nroResolucion;
              this.resumeActividad_2_1.areaHa = res.areaHa;

              let censoComercial = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'CC');
              if(censoComercial && censoComercial.length>0) {
                let listado_1:any={};
                listado_1.actividad = censoComercial[0].actividad;
                listado_1.listResumenEvaluacion = [];
                censoComercial.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen:'CC'
                  });
                });
                this.listado_2.push(listado_1);
              }


              let construcInfra = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'CI');
              if(construcInfra && construcInfra.length>0) {
                let listado_1:any={};
                listado_1.actividad = construcInfra[0].actividad;
                listado_1.listResumenEvaluacion = [];
                construcInfra.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen:'CI'
                  });
                });
                this.listado_2.push(listado_1);
              }

              let aprovechamient = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'APROV');
              if(aprovechamient && aprovechamient.length>0) {
                let listado_1:any={};
                listado_1.actividad = aprovechamient[0].actividad;
                listado_1.listResumenEvaluacion = [];
                aprovechamient.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen: 'APROV'
                  });
                });
                this.listado_2.push(listado_1);
              }

              let silvicultural = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'SIL');
              if(silvicultural && silvicultural.length>0) {
                let listado_1:any={};
                listado_1.actividad = silvicultural[0].actividad;
                listado_1.listResumenEvaluacion = [];
                silvicultural.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen: 'SIL'
                  });
                });
                this.listado_2.push(listado_1);
              }



              let proteccion = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'PROT');
              if(proteccion && proteccion.length>0) {
                let listado_1:any={};
                listado_1.actividad = proteccion[0].actividad;
                listado_1.listResumenEvaluacion = [];
                proteccion.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen: 'PROT'
                  });
                });
                this.listado_2.push(listado_1);
              }

              let monitoreo = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'MON');
              if(monitoreo && monitoreo.length>0) {
                let listado_1:any={};
                listado_1.actividad = monitoreo[0].actividad;
                listado_1.listResumenEvaluacion = [];
                monitoreo.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen: 'MON'
                  });
                });
                this.listado_2.push(listado_1);
              }

              let capa = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'CAP');
              if(capa && capa.length>0) {
                let listado_1:any={};
                listado_1.actividad = capa[0].actividad;
                listado_1.listResumenEvaluacion = [];
                capa.forEach((item:any) => {
                  listado_1.listResumenEvaluacion.push({
                    idResumenActDet :idPlanBd? item.idResumenActDet:0,
                    indicador:item.indicador,
                    programado:item.programado,
                    realizado:item.realizado,
                    resumen: 'CAP'
                  });
                });
                this.listado_2.push(listado_1);
              }
            }


            let response2 = response.data.find((e:any)=>e.codigoSubResumen == CodigoPOOC.ACORDEON_2_2);
            //let response2 = response.data[1];
            if (response2) {
              this.resumeActividad_2_2.idResumenAct = response2.idResumenAct? response2.idResumenAct :0;
              this.resumeActividad_2_2.idPlanManejo = response2.idPlanManejo;

              response2.listResumenActividadDetalle.forEach((e:any)=>{
                this.listResumenActividad_2_2.push(new ResumenActividadDet({
                  idResumenActDet:e.idResumenActDet,
                  resumen:e.resumen
                }));

              })
            }



          } else {
            let res = response.data.find((e:any)=>e.codigoSubResumen == null);
            let censoComercial = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'CC');
            if(censoComercial && censoComercial.length>0) {
              let listado_1:any={};
              listado_1.actividad = censoComercial[0].actividad;
              listado_1.listResumenEvaluacion = [];
              censoComercial.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet : 0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen:'CC'
                });
              });
              this.listado_2.push(listado_1);
            }


            let construcInfra = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'CI');
            if(construcInfra && construcInfra.length>0) {
              let listado_1:any={};
              listado_1.actividad = construcInfra[0].actividad;
              listado_1.listResumenEvaluacion = [];
              construcInfra.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet : 0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen:'CI'
                });
              });
              this.listado_2.push(listado_1);
            }

            let aprovechamient = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'APROV');
            if(aprovechamient && aprovechamient.length>0) {
              let listado_1:any={};
              listado_1.actividad = aprovechamient[0].actividad;
              listado_1.listResumenEvaluacion = [];
              aprovechamient.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet :0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen: 'APROV'
                });
              });
              this.listado_2.push(listado_1);
            }

            let silvicultural = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'SIL');
            if(silvicultural && silvicultural.length>0) {
              let listado_1:any={};
              listado_1.actividad = silvicultural[0].actividad;
              listado_1.listResumenEvaluacion = [];
              silvicultural.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet : 0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen: 'SIL'
                });
              });
              this.listado_2.push(listado_1);
            }



            let proteccion = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'PROT');
            if(proteccion && proteccion.length>0) {
              let listado_1:any={};
              listado_1.actividad = proteccion[0].actividad;
              listado_1.listResumenEvaluacion = [];
              proteccion.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet : 0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen: 'PROT'
                });
              });
              this.listado_2.push(listado_1);
            }

            let monitoreo = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'MON');
            if(monitoreo && monitoreo.length>0) {
              let listado_1:any={};
              listado_1.actividad = monitoreo[0].actividad;
              listado_1.listResumenEvaluacion = [];
              monitoreo.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet : 0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen: 'MON'
                });
              });
              this.listado_2.push(listado_1);
            }

            let capa = res.listResumenActividadDetalle.filter( (x:any) => x.resumen == 'CAP');
            if(capa && capa.length>0) {
              let listado_1:any={};
              listado_1.actividad = capa[0].actividad;
              listado_1.listResumenEvaluacion = [];
              capa.forEach((item:any) => {
                listado_1.listResumenEvaluacion.push({
                  idResumenActDet :0,
                  indicador:item.indicador,
                  programado:item.programado,
                  realizado:item.realizado,
                  resumen: 'CAP'
                });
              });
              this.listado_2.push(listado_1);
            }



          }
          }

        } else {
          this.toast('error', response.message);
        }
      });

  }


  registrarResumenActividades() {


    this.dialog.open(LoadingComponent, { disableClose: true });
    let param = [];
    this.resumeActividad_2_1.listResumenEvaluacion = [];
    this.listado_2.forEach((e:any)=>{

      e.listResumenEvaluacion.forEach((ite:any)=>{
        this.resumeActividad_2_1.listResumenEvaluacion.push(new ResumenActividadDet({
          idResumenActDet : ite.idResumenActDet,
          tipoResumen:1,
          actividad : e.actividad,
          programado : ite.programado,
          indicador : ite.indicador,
          realizado: ite.realizado,
          idUsuarioRegistro : this.user.idUsuario,
          estadoVar:"A",
          resumen: ite.resumen
        }))
      });

    });
    this.resumeActividad_2_1.idPlanManejo = this.idPlanManejo;
    param.push(this.resumeActividad_2_1);
    this.resumeActividad_2_2.idPlanManejo = this.idPlanManejo;
    this.resumeActividad_2_2.listResumenEvaluacion = this.listResumenActividad_2_2;
    param.push(this.resumeActividad_2_2);

    this.planificacionService
      .registrarResumenDetalle(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          // this.toast('success', response.message);
          this.toast('success', 'Se registró el resumen correctamente.');
          this.getResumenActividades();
        } else {
          this.toast('error', response.message);
        }
      });

  }




  getListaResumenDetalle() {
    this.listResumenResultPOEjecutado = [];
    this.params = {
      idPlanManejo: this.idPlanManejo,
      codTipoResumen: 'POCC',
    };
    this.planificacionService
      .listarResumenDetalle(this.params)
      .subscribe((response: any) => {
        if (response.data&& response.data[0].idResumenAct !=null) {
          // if (response.data[0].listResumenActividadDetalle) {
          this.editar = true;
          let cabecera = response.data[0];
          //if(cabecera.codigoResumen == 'POCCRARPOARARPO'){}

          this.resumenPO.nroResolucion = cabecera.nroResolucion;
          this.resumenPO.nroPcs = cabecera.nroPcs;
          this.resumenPO.areaHa = cabecera.areaHa;

          response.data.forEach((x: any) => {
            /*this.resumenPO.nroResolucion = x.nroResolucion;
            this.resumenPO.nroPcs = x.nroPcs;
            this.resumenPO.areaHa = x.areaHa;*/

            if(x.codigoResumen == 'POCCRARPOARARPO'){
              this.idResumenAct_2_1 = x.idResumenAct;

              if (x.actividad == 'Censo comercial') {
                const obj = new PoccResumenActRecModel(x);
                obj.idResumenActDet = 0
                this.censoIndicador.push(obj);
              } else if (x.actividad == 'Construcción de infraestructura') {
                const obj = new PoccResumenActRecModel(x);
                this.infraIndicador.push(obj);
              } else if (x.actividad == 'Aprovechamiento') {
                const obj = new PoccResumenActRecModel(x);
                this.aprovIndicador.push(obj);
              } else if (x.actividad == 'Silvicultural') {
                const obj = new PoccResumenActRecModel(x);
                this.silvIndicador.push(obj);
              } else if (x.actividad == 'Protección') {
                const obj = new PoccResumenActRecModel(x);
                this.protIndicador.push(obj);
              } else if (x.actividad == 'Monitoreo') {
                const obj = new PoccResumenActRecModel(x);
                this.monitIndicador.push(obj);
              } else if (x.actividad == 'Capacitación') {
                const obj = new PoccResumenActRecModel(x);
                this.captIndicador.push(obj);
              }
            }else if(x.codigoResumen == 'POCCRARPOARRPOE'){
                this.idResumenAct_2_2 = x.idResumenAct;
                const obj = new PoccResumenActRecModel(x);
                this.listResumenResultPOEjecutado.push(obj);
            }


          });
          // }
        } else {
          this.getInitialListResumenDetalle();
        }
      });
  }

  getInitialListResumenDetalle() {
    this.listResumenResultPOEjecutado = [];
    this.params = {
      codTipoResumen: 'POCC',
    };
    this.planificacionService
      .listarResumenDetalle(this.params)
      .subscribe((response: any) => {
        if (response.data) {
          this.editar = false;
          response.data.forEach((data: any) => {
            if (data.actividad == 'Censo comercial') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.censoIndicador.push(obj);
            } else if (data.actividad == 'Construcción de infraestructura') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.infraIndicador.push(obj);
            } else if (data.actividad == 'Aprovechamiento') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.aprovIndicador.push(obj);
            } else if (data.actividad == 'Silvicultural') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.silvIndicador.push(obj);
            } else if (data.actividad == 'Protección') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.protIndicador.push(obj);
            } else if (data.actividad == 'Monitoreo') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.monitIndicador.push(obj);
            } else if (data.actividad == 'Capacitación') {
              const obj = new PoccResumenActRecModel(data);
              obj.idResumenActDet = 0;
              obj.tipoResumen = 1;
              this.captIndicador.push(obj);
            }
            /*
            if (data.tipoResumen === 2) {
              this.listResumenResultPOEjecutado.push(data);
            }*/
          });
        } else {
        }
      });
  }

  guardarCabecera() {
    this.cabecera = {
      idResumenAct: this.idResumenAct_2_1,
      codTipoResumen: this.codTipoResumen,
      nroResolucion: this.resumenPO.nroResolucion,
      nroPcs: this.resumenPO.nroPcs,
      areaHa: this.resumenPO.areaHa,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.idUsuarioRegistro,
    };
  }

  guardarLista() {
    this.listResumenEvaluacionAux = [];
    this.listaGuardar = [];

    this.censoIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });
    this.infraIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });
    this.aprovIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });
    this.silvIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });
    this.protIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });
    this.monitIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });
    this.captIndicador.forEach((x) => {
      this.listResumenEvaluacionAux.push(x);
    });

    this.listResumenEvaluacionAux.forEach((element: any) => {
      this.listaGuardar.push(element);
    });
  }

  modalAprovechamiento = () => {
    this.ref = this.dialogService.open(ResumenComponent, {
      header: 'Resumen de los resultados del PO ejecutado',
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.estadoVar = 'A';
        //resp.tipoResumen = 2;
        this.listResumenActividad_2_2.push( new ResumenActividadDet({
          idResumenActDet :0,
          resumen:resp.resumen,
          idUsuarioRegistro : resp.idUsuario
        }));
        this.listResumenResultPOEjecutado.push(resp);
      }
    });
  };


  abrirModal(){
    this.isShowModal2_2 = true;
    this.acciones_2_2 = "Agregar";
    this.index_modal_2_2 = -1;
  }

  editarModal(index:number,data:ResumenActividadDet ){
    this.acciones_2_2 = "Editar";
    this.isShowModal2_2 = true;
    this.index_modal_2_2 = index;
    this.resultadosModal = data.resumen;
  }

  editarModal2_1(index:number,data:any){

    this.listResumenActividadDetModal = [];
    this.isShowModal2_1 = true;
    this.index_modal_2_1 = index;
    this.actividadModal = data.actividad;

    data.listResumenEvaluacion.forEach((item:any)=>{

      this.listResumenActividadDetModal.push(item);

      /*this.listResumenActividadDetModal.push({
        idResumenActDet:item.idResumenActDet,
        indicador:item.indicador,
        programado:item.programado,
        realizado:item.realizado
      });*/
    })
    //this.listResumenActividadDetModal = [...data.listResumenEvaluacion];

  }

  agregarResumen_2_1() {

    if(this.index_modal_2_1 >=0){
      this.listado_2[this.index_modal_2_1].actividad = this.actividadModal;
      //this.listado_2[this.index_modal_2_1].listResumenEvaluacion=[];
      this.listado_2[this.index_modal_2_1].listResumenEvaluacion = this.listResumenActividadDetModal.slice();
    }

    if(this.listado_2[this.index_modal_2_1].listResumenEvaluacion.length==0) {
      this.listado_2.splice(this.index_modal_2_1, 1);
    }

    this.listResumenActividadDetModal = [];
    this.actividadModal = "";
    this.index_modal_2_1 = -1;
    this.isShowModal2_1 = false;

  }

  cerrarmodal2_1(){
    this.isShowModal2_1 = false;

  }

  agregarResumen() {
    let resultado = this.resultadosModal;
    if(this.acciones_2_2=='Agregar'){
      this.listResumenActividad_2_2.push( new ResumenActividadDet({
        idResumenActDet :0,
        resumen:resultado,
        idUsuarioRegistro : this.user.idUsuario
      }));

    }else{
      if(this.index_modal_2_2 >=0){
        this.listResumenActividad_2_2[this.index_modal_2_2].resumen =  resultado;
      }
    }

    this.resultadosModal = null;
    this.index_modal_2_2 = -1;
    this.isShowModal2_2 = false;
  }

  registarFormulario() {
    if (!this.validacionCampos()) return;
    this.guardarCabecera();
    this.guardarLista();

    const body = [
      {
        idResumenAct: this.editar ? this.idResumenAct_2_1 : 0,
        codTipoResumen: this.codTipoResumen,
        codigoResumen: 'POCCRARPOARARPO',
        codigoSubResumen: '',
        nroResolucion: this.cabecera.nroResolucion,
        nroPcs: this.cabecera.nroPcs,
        areaHa: this.cabecera.areaHa,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.idUsuarioRegistro,
        listResumenEvaluacion: this.listaGuardar,
      },
      {
        idResumenAct: this.editar ? this.idResumenAct_2_2 : 0,
        codTipoResumen: this.codTipoResumen,
        //codigoResumen: '',
        codigoResumen: 'POCCRARPOARRPOE',
        //codigoSubResumen: 'POCCRARPOARRPOE',
        codigoSubResumen: '',
        nroResolucion:0,// this.cabecera.nroResolucion,
        nroPcs: 0,//this.cabecera.nroPcs,
        areaHa: 0,//this.cabecera.areaHa,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.idUsuarioRegistro,
        listResumenEvaluacion: this.listResumenResultPOEjecutado,
      },
    ];

    this.planificacionService
      .registrarResumenDetalle(body)
      .subscribe((response: any) => {
        if (response.success) {
          this.toast('success', response.message);
        } else {
          this.toast('error', response.message);
        }
      });
  }

  toast(saverty: String, datail: String) {
    this.messageService.add({
      key: 'toast',
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  eliminar(event: any, index: number, resumenDet: ResumenActividadDet, type: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });
        if(resumenDet.idResumenActDet == 0){
          this.listResumenActividad_2_2.splice(index, 1);
        }else{
          let params = {
            idResumenActDet: resumenDet.idResumenActDet,
            idUsuarioElimina: this.idUsuarioRegistro,
          };

          this.planificacionService
            .eliminarResumenDetalle(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              // this.toast('success', result.message);
              this.toast('success', 'Se eliminó el resumen de los resultados del PO correctamente.');
              this.listResumenActividad_2_2.splice(index, 1);
              /*if (result.success) {
                this.toast('success', result.message);
                this.eliminacionTipo(type, index);
              } else {
                this.toast('error', result.message);
              }*/
            });
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  eliminarIndicador(event: any, index: number, resumenDet: any, type: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });
        let params = {
          idResumenActDet: resumenDet.idResumenActDet,
          idUsuarioElimina: this.idUsuarioRegistro,
        };

        this.planificacionService
          .eliminarResumenDetalle(params)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            // this.toast('success', result.message);
            this.toast('success', 'Se eliminó el resumen de actividad operativa  en el PO');
            this.listResumenActividadDetModal.splice(index, 1);

            this.listado_2[this.index_modal_2_1].listResumenEvaluacion.splice(index, 1);

          });
      },
      reject: () => {
        //reject action
      },
    });
  }

  eliminacionTipo(type: number, index: number) {
    switch (type) {
      case 1:
        this.censoIndicador.splice(index, 1);
        break;
      case 2:
        this.infraIndicador.splice(index, 1);
        break;
      case 3:
        this.aprovIndicador.splice(index, 1);
        break;
      case 4:
        this.silvIndicador.splice(index, 1);
        break;
      case 5:
        this.protIndicador.splice(index, 1);
        break;
      case 6:
        this.monitIndicador.splice(index, 1);
        break;
      case 7:
        this.captIndicador.splice(index, 1);
        break;
      default:
        this.listResumenResultPOEjecutado.splice(index, 1);
        break;
    }
  }
}
