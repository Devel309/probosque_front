import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import {UsuarioService} from '@services';

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.scss'],
})
export class ResumenComponent implements OnInit {
  autoResize: boolean = true;
  actividadObjt: any = {
    idResumenActDet: 0,
    tipoResumen: 2,
    actividad: '',
    indicador: '',
    programado: '',
    realizado: '',
    resumen: '',
    ///idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idUsuarioRegistro: this.user.idUsuario,
    aspectoRecomendacion: '',
    aspectoPositivo: '',
    aspectoNegativo: '',
    estadoVar: '',
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private user: UsuarioService
  ) {}

  ngOnInit() {}

  agregar = () => {
    if (this.actividadObjt.resumen === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Resumen, es obligatorio',
      });
    } else this.ref.close(this.actividadObjt);
  };

  cancelar() {
    this.ref.close(null);
  }
}
