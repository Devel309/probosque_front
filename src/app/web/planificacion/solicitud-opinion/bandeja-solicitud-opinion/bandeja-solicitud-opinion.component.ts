import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ListarSolicitudOpinionRequest } from 'src/app/model/solicitud-opinion';
import { GenericoService } from 'src/app/service/generico.service';
import { SolicitudOpinionService } from 'src/app/service/solicitud-opinion.service';
import { ModalSolicitudOpinionComponent } from '../modals/modal-solicitud-opinion/modal-solicitud-opinion.component';

@Component({
  selector: 'app-bandeja-solicitud-opinion',
  templateUrl: './bandeja-solicitud-opinion.component.html',
  styleUrls: ['./bandeja-solicitud-opinion.component.scss']
})
export class BandejaSolicitudOpinionComponent implements OnInit {

  solicitudes: any[] = [];

  totalRecords = 0;

  listarSolicitudOpinionRequest: ListarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();

  comboDocumentos: any[] = [];
  comboEstado: any[] = [];

  ref!: DynamicDialogRef;

  usuario!: UsuarioModel;


  constructor(private solicitudOpinionServ: SolicitudOpinionService, private router: Router,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    public dialogService: DialogService,
    private usuarioServ: UsuarioService,
  ) { }

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    this.listarsolicitudes();
    this.listarComboDocumentos();
    this.listarComboEstado();
  }

  listarsolicitudes() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.listarSolicitudOpinionRequest.siglaEntidad = this.usuario.sirperfil;
    this.solicitudOpinionServ.listarSolicitudOpinion(this.listarSolicitudOpinionRequest).subscribe((result: any) => {
      if (result.data) {
        this.solicitudes = [...result.data];
        this.totalRecords = result.totalRecord;
      }

      this.dialog.closeAll();
    })
  }


  listarComboDocumentos() {
    var params = { idTipoParametro: 77 }

    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboDocumentos = result.data;
    })
  }

  listarComboEstado() {
    var params = { idTipoParametro: 82 }

    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboEstado = result.data;
    })
  }

  limpiar() {
    this.listarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();
    this.listarsolicitudes();
  }


  loadData(event: any) {
    this.listarSolicitudOpinionRequest.pageNum = event.first + 1;
    this.listarSolicitudOpinionRequest.pageSize = event.rows;

    this.listarsolicitudes();
  }

  verDetalle(data: any) {
    this.ref = this.dialogService.open(ModalSolicitudOpinionComponent, {
      header: 'Respuesta de Solicitud de Opinión',
      width: '600px',
      contentStyle: { 'max-height': '600px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        idSolicitudOpinion: data.idSolicitudOpinion,
        disabled: data.estadoSolicitudOpinion == 'ESOPA' || this.usuario.sirperfil == 'ARFFS',
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarsolicitudes();
      }
    });
  }
}
