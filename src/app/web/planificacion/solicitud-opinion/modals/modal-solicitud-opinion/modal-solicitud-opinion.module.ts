import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSolicitudOpinionComponent } from './modal-solicitud-opinion.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';



@NgModule({
  declarations: [
    ModalSolicitudOpinionComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    FormsModule,
    RadioButtonModule,
    InputTextareaModule,
    DropdownModule,
    MatDatepickerModule,
    ButtonModule,
    DialogModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
  ],
  exports: [ModalSolicitudOpinionComponent],
  entryComponents: [ModalSolicitudOpinionComponent],
})
export class ModalSolicitudOpinionModule { }
