import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-evaluacion-aprobacion-pm',
  templateUrl: './evaluacion-aprobacion-pm.component.html',
  styleUrls: ['./evaluacion-aprobacion-pm.component.scss']
})
export class EvaluacionAprobacionPmComponent implements OnInit {

dataDemo:ModelDemo[]=[];

totalRecords:number = 1000;

  constructor(private _formBuilder: FormBuilder) {

   
   }

  ngOnInit(): void {

    for (let index = 0; index < 3; index++) {
      this.dataDemo.push({
        tipoContrato:'string',
        nroContrato:'string',
        titular:'string',
        representanteLegal:'string',
        fechaContrato:'string',
        area:'string',
        arffs:'string'
       });
      
    }
  

  }

}

export interface ModelDemo
{
  tipoContrato:string;
  nroContrato:string;
  titular:string;
  representanteLegal:string;
  fechaContrato:string;
  area:string;
  arffs:string;
}
