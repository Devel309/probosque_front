import { Component, OnInit, Output, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AnexoAdjuntoService } from 'src/app/service/AnexoAdjunto.service';
import { PlanManejoEvaluacionService } from "src/app/service/plan-manejo-evaluacion.service";
import { AnexoAdjuntoModel } from '../../../model/AnexoAdjunto';
import { TanInformacionBasicaComponent } from "./tabs/tab-informacion-basica/tab-informacion-basica.component";

import { TabInformacionGeneralComponent } from './tabs/tab-informacion-general/tab-informacion-general.component';
import { TabObjetivosManejoComponent } from './tabs/tab-objetivos-manejo/tab-objetivos-manejo.component';
import { TabOrdenamientoProteccionComponent } from './tabs/ordenamiento-proteccion/tab-ordenamiento-proteccion.component';

import { TabManejoForestalComponent } from "./tabs/tab-manejo-forestal/tab-manejo-forestal.component";

import { TabMonitoreoComponent } from "./tabs/tab-monitoreo/tab-monitoreo.component";
import { TabParticipacionCiudadanaComponent } from "./tabs/tab-participacion-ciudadana/tab-participacion-ciudadana.component";
import { TabCapacitacionComponent } from "./tabs/tab-capacitacion/tab-capacitacion.component";
import { TabOrganizacionManejoComponent } from "./tabs/tab-organizacion-manejo/tab-organizacion-manejo.component";
import { TabProgramaInversionComponent } from "./tabs/tab-programa-inversion/tab-programa-inversion.component";
import { TabCronogramaActividadesComponent } from "./tabs/tab-cronograma-actividades/tab-cronograma-actividades.component";
import { PlanManejoService, UsuarioService } from "@services";
import { HttpErrorResponse } from "@angular/common/http";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { Perfiles } from "src/app/model/util/Perfiles";
import { CodigoEstadoPlanManejo } from "src/app/model/util/CodigoEstadoPlanManejo";
import { TabProdRecursoComponent } from "./tabs/tab-prod-recurso/tab-prod-recurso.component";
import { TabEvaluacionImpactoComponent } from "./tabs/tab-evaluacion-impacto/tab-evaluacion-impacto.component";

@Component({
  selector: "app-concesion-forestal-maderables",
  templateUrl: "./concesion-forestal-maderables.component.html",
})
export class ConcesionForestalMaderablesComponent implements OnInit {
  @ViewChild(TabInformacionGeneralComponent) tabInformacionGeneralComponent!: TabInformacionGeneralComponent;
  @ViewChild(TabObjetivosManejoComponent) tabObjetivosManejoComponent!: TabObjetivosManejoComponent;
  @ViewChild(TanInformacionBasicaComponent) tanInformacionBasicaComponent!: TanInformacionBasicaComponent;
  @ViewChild(TabOrdenamientoProteccionComponent) tabOrdenamientoProteccionComponent!: TabOrdenamientoProteccionComponent;
  @ViewChild(TabProdRecursoComponent) tabProdRecursoComponent!: TabProdRecursoComponent;
  @ViewChild(TabEvaluacionImpactoComponent) tabEvaluacionImpactoComponent!: TabEvaluacionImpactoComponent;
  
  @ViewChild(TabManejoForestalComponent) tabManejoForestalComponent!: TabManejoForestalComponent;
  @ViewChild(TabMonitoreoComponent) tabMonitoreoComponent!: TabMonitoreoComponent;
  @ViewChild(TabParticipacionCiudadanaComponent) tabParticipacionCiudadanaComponent!: TabParticipacionCiudadanaComponent;
  @ViewChild(TabCapacitacionComponent) tabCapacitacionComponent!: TabCapacitacionComponent;
  @ViewChild(TabOrganizacionManejoComponent) tabOrganizacionManejoComponent!: TabOrganizacionManejoComponent;
  @ViewChild(TabProgramaInversionComponent) tabProgramaInversionComponent!: TabProgramaInversionComponent;
  @ViewChild(TabCronogramaActividadesComponent) tabCronogramaActividadesComponent!: TabCronogramaActividadesComponent;
  tabIndex = 0;
  city: string = "";
  idPGMF!: number;

  disabled: boolean = true;
  usuario!: UsuarioModel;

  listaAnexoAdjunto: any[] = [];
  anexoAdjunto = {} as AnexoAdjuntoModel;

  codigoEstado: string = "";

  perfil = Perfiles;
  estadosPlanManejo = CodigoEstadoPlanManejo;

  constructor(
    private servAnexoAdjunto: AnexoAdjuntoService,
    private route: ActivatedRoute,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private planServ: PlanManejoService,
    private usuarioServ: UsuarioService,
    private router: Router,
  ) {
    this.idPGMF = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
  }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;

    this.obtenerPlan();

    this.servAnexoAdjunto.listarAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {
      this.listaAnexoAdjunto = result.data;
    });
  }

  obtenerPlan() {
    let params = { idPlanManejo: this.idPGMF };
    this.planServ.obtenerPlanManejo(params).subscribe(
      (result: any) => {
        if (result.data) {
          this.codigoEstado = result.data.codigoEstado;

          if (this.usuario.sirperfil == 'TITULARTH' &&
            (this.codigoEstado == 'EPLMCOMP' || this.codigoEstado == 'EPLMOBSM' || 
              this.codigoEstado == 'EPLMOBSG' || this.codigoEstado == 'EPLMBOR')) // || this.codigoEstado == 'EPLMBOR'
          {
            this.disabled = false;
          }

          this.generarValidaciones();
        }
      },
      (error) => { }
    );
  }

  isDisbledObjFormu = true;
  isShowObjEval = false;
  isDisabledObjEval = true;
  isRequiredObsEval = true;

  generarValidaciones() {
    if (this.usuario.sirperfil === this.perfil.TITULARTH) {
      this.isShowObjEval = false
      // falatria: borrador y borrador-denegado
      if (
        this.codigoEstado === this.estadosPlanManejo.COMPLETADO || 
        this.codigoEstado === this.estadosPlanManejo.OBSERVADO_MESA_DE_PARTES || 
        this.codigoEstado === this.estadosPlanManejo.OBSERVADO ||
        this.codigoEstado === this.estadosPlanManejo.BORRADOR
      ) {

        
        this.isDisbledObjFormu = false;
    
      }
      if (this.codigoEstado === this.estadosPlanManejo.OBSERVADO) {
        this.isShowObjEval = true;
        this.isDisabledObjEval = true;
      }

    } else if (this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL) {
      this.isDisbledObjFormu = true;

      if (this.codigoEstado === this.estadosPlanManejo.EN_EVALUACION) {
        this.isShowObjEval = true;
        this.isDisabledObjEval = false;
      }

      if (this.codigoEstado === this.estadosPlanManejo.OBSERVADO) {
        this.isShowObjEval = true;
        this.isDisabledObjEval = true;
      }

    }

  }


  ngAfterViewInit() {
    this.obtenerUltimaEvaluacion();
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
    this.obtenerUltimaEvaluacion();
    // if (event === 0) {
    //   this.tabInformacionGeneralComponent.ngOnInit();
    // }
  }

  private obtenerUltimaEvaluacion() {
    let params = { idPlanManejo: this.idPGMF }
    this.planManejoEvaluacionService.obtenerUltimaEvaluacion(params).subscribe((result: any) => {
      if (result.codigo != null) {
        this.listarEvaluacionDetalle(result.codigo);
        if (this.tabInformacionGeneralComponent !== undefined) {
          this.tabInformacionGeneralComponent.dataLineamientos(result.codigo);
        }
        if (this.tabObjetivosManejoComponent !== undefined) {
          this.tabObjetivosManejoComponent.dataLineamientos(result.codigo);
        }
        if (this.tanInformacionBasicaComponent !== undefined) {
          this.tanInformacionBasicaComponent.dataLineamientos(result.codigo);
        }
        if (this.tabOrdenamientoProteccionComponent !== undefined) {
          this.tabOrdenamientoProteccionComponent.dataLineamientos(result.codigo);
        }
        if (this.tabProdRecursoComponent!==undefined) {
          this.tabProdRecursoComponent.dataLineamientos(result.codigo);
        }
        if (this.tabManejoForestalComponent !== undefined) {
          this.tabManejoForestalComponent.dataLineamientos(result.codigo);
        }
        if (this.tabEvaluacionImpactoComponent!==undefined) {
          this.tabEvaluacionImpactoComponent.dataLineamientos(result.codigo);
        }
        if (this.tabMonitoreoComponent !== undefined) {
          this.tabMonitoreoComponent.dataLineamientos(result.codigo);
        }
        if (this.tabParticipacionCiudadanaComponent !== undefined) {
          this.tabParticipacionCiudadanaComponent.dataLineamientos(result.codigo);
        }
        if (this.tabCapacitacionComponent !== undefined) {
          this.tabCapacitacionComponent.dataLineamientos(result.codigo);
        }
        if (this.tabOrganizacionManejoComponent !== undefined) {
          this.tabOrganizacionManejoComponent.dataLineamientos(result.codigo);
        }
        if (this.tabProgramaInversionComponent !== undefined) {
          this.tabProgramaInversionComponent.dataLineamientos(result.codigo);
        }
        if (this.tabCronogramaActividadesComponent !== undefined) {
          this.tabCronogramaActividadesComponent.dataLineamientos(result.codigo);
        }

      }

    });
  }

  private listarEvaluacionDetalle(id: any) {
    let params =
    {
      idPlanManejoEval: id
    };

    this.planManejoEvaluacionService.listarPlanManejoEvaluacionDetalle(params).subscribe((result: any) => {
      if (result.isSuccess) {

        if (this.tabInformacionGeneralComponent !== undefined) {
          this.tabInformacionGeneralComponent.listarEvaluaciones(result.data);
        }
        if (this.tabObjetivosManejoComponent !== undefined) {
          this.tabObjetivosManejoComponent.listarEvaluaciones(result.data);
        }
        if (this.tanInformacionBasicaComponent !== undefined) {
          this.tanInformacionBasicaComponent.listarEvaluaciones(result.data);
        }
        if (this.tabOrdenamientoProteccionComponent !== undefined) {
          this.tabOrdenamientoProteccionComponent.listarEvaluaciones(result.data);
        }
        if (this.tabProdRecursoComponent!==undefined) {
          this.tabProdRecursoComponent.listarEvaluaciones(result.data);
        }
        if (this.tabManejoForestalComponent !== undefined) {
          this.tabManejoForestalComponent.listarEvaluaciones(result.data);
        }
        if (this.tabEvaluacionImpactoComponent!==undefined) {
          this.tabEvaluacionImpactoComponent.listarEvaluaciones(result.data);
        }
        if (this.tabMonitoreoComponent !== undefined) {
          this.tabMonitoreoComponent.listarEvaluaciones(result.data);
        }
        if (this.tabParticipacionCiudadanaComponent !== undefined) {
          this.tabParticipacionCiudadanaComponent.listarEvaluaciones(result.data);
        }
        if (this.tabCapacitacionComponent !== undefined) {
          this.tabCapacitacionComponent.listarEvaluaciones(result.data);
        }
        if (this.tabOrganizacionManejoComponent !== undefined) {
          this.tabOrganizacionManejoComponent.listarEvaluaciones(result.data);
        }
        if (this.tabProgramaInversionComponent !== undefined) {
          this.tabProgramaInversionComponent.listarEvaluaciones(result.data);
        }
        if (this.tabCronogramaActividadesComponent !== undefined) {
          this.tabCronogramaActividadesComponent.listarEvaluaciones(result.data);
        }
      }
    })
  }

  btnCancelar(){
		const uri = 'planificacion/bandeja-PGMF';
    	this.router.navigate([uri])
	}
}
