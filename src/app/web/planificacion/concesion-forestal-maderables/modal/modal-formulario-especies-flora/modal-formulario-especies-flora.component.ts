import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-formulario-especies-flora",
  templateUrl: "./modal-formulario-especies-flora.component.html",
})
export class ModalFormularioEspeciesFloraComponent implements OnInit {
  nombreCient : string =  "";
  nombreCom : string = "";
  obj = {
    justificacion: "",
    nombreCientifico : "",
    nombreComun : ""
  };

  nombreCientifico = [{ label: "nombre científico1" },{ label: "nombre científico" }];
  nombreComun = [{ label: "nombre común2" },{ label: "nombre común" }];

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == "true") {
      
      this.obj.justificacion = this.config.data.item.justificacion;
      this.nombreCient = this.config.data.item.nombreCientifico;
      this.nombreCom = this.config.data.item.nombreComun;
    }
  }

  nombreCientificoSelect(){
    this.obj.nombreCientifico = this.nombreCient
  }
  nombreComunSelect(){
    this.obj.nombreComun = this.nombreCom
  }

  agregar = () => {
    if (this.obj.justificacion === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Justificación, es obligatorio",
      });
    } else this.ref.close(this.obj);
  };
  cerrarModal(){
    this.ref.close()
  }
}
