import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-formulario-especies",
  templateUrl: "./modal-formulario-especies.component.html",
})
export class ModalFormularioEspeciesComponent implements OnInit {
  nombreCient : string =  "";
  nombreCom : string = "";

  obj = {
    prod: "",
    dmcNorm: "",
    dmcProp: "",
    nombreCientifico : "",
    nombreComun : "",
  };
  nombreCientifico = [{ label: "nombre científico1" },{ label: "nombre científico" }];
  nombreComun = [{ label: "nombre común2" },{ label: "nombre común" }];

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == "true") {
      
      this.obj.prod = this.config.data.item.prod;
      this.obj.dmcNorm = this.config.data.item.dmcNorm;
      this.obj.dmcProp = this.config.data.item.dmcProp;
      this.nombreCient = this.config.data.item.nombreCientifico;
      this.nombreCom = this.config.data.item.nombreComun;
    }
  }
  nombreCientificoSelect(){
    this.obj.nombreCientifico = this.nombreCient
  }
  nombreComunSelect(){
    this.obj.nombreComun = this.nombreCom
  }

  agregar = () => {
    if (this.obj.prod === "") {
      this.messageService.add({
        key: "toast",
        severity: "warn",
        summary: "Alerta",
        detail: "Línea de produccion, es obligatorio",
      });
    } else if (this.obj.dmcNorm === "") {
      this.messageService.add({
        key: "toast",
        severity: "warn",
        summary: "Alerta",
        detail: "DMC normado, es obligatorio",
      });
    } else if (this.obj.dmcProp === "") {
      this.messageService.add({
        key: "toast",
        severity: "warn",
        summary: "Alerta",
        detail: "DCM propuesto, es obligatorio",
      });
    } else this.ref.close(this.obj);
  };
  cerrarModal(){
    this.ref.close()
  }
}
