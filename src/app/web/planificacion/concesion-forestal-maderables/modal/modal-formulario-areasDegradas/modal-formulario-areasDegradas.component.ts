import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ManejoForestalDetalleModel } from 'src/app/model/ManejoForestal';

@Component({
  selector: 'app-modal-formulario-areasDegradas',
  templateUrl: './modal-formulario-areasDegradadas.component.html',
})
export class ModalFormularioAreasDegradadasComponent implements OnInit {
  anydata = new ManejoForestalDetalleModel();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}
 
  ngOnInit() {
  }

  agregar = () => {
    if (this.anydata.tratamientoSilvicultural === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        detail: 'Debe ingresar descripción de objetivo',
      });
    } else if (this.anydata.descripcionOtro === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        detail: 'Debe ingresar Acciones',
      });
    
    }else { this.ref.close(this.anydata);
    
 }
  };

  cerrarModal() {
    this.ref.close();
  }
}
