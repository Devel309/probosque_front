import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { ManejoForestalDetalleModel } from "src/app/model/ManejoForestal";

@Component({
  selector: "app-modal-formulario-especies-flora-proteger",
  templateUrl: "./modal-formulario-especies-flora-proteger.component.html",
})
export class ModalFormularioEspeciesFloraPortegerComponent implements OnInit {
  obj1 = new ManejoForestalDetalleModel();
  nombreCient : string =  "";
  descripcionOtro : string = "";
  
  obj = {
    justificacion: "",
    nombreCientifico : "",
    nombreComun : ""
  };
    valor: any=[];
  nombreCientifico : any[]= [];
  //nombreComun = [{ label: "nombre común2" },{ label: "nombre común" }];

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == "true") {
      console.log(this.config.data.edit);
      this.obj.justificacion = this.config.data.item.justificacion;
      this.nombreCient = this.config.data.item.nombreCientifico;
      this.descripcionOtro = this.config.data.item.nombreComun;
    }
    
    this.nombreCientifico = this.config.data.item;
    
  }

  nombreCientificoSelect(){

    this.valor = this.nombreCientifico.find(item => item.idManejoBosqueDet === this.nombreCient);
     
    if (this.valor) {
      this.obj1.especie = this.valor.especie;
      this.obj1.descripcionOtro = this.valor.descripcionOtro;
      this.obj1.idManejoBosqueDet=this.valor.idManejoBosqueDet;
    }

    
  }

 

  agregar = () => {
    if (this.nombreCient === "" || this.nombreCient === null) {
      this.messageService.add({
        key: "toast",
        severity: "warn",
        detail: "Debe seleccionar la Especie.",
      });
    }else if (this.obj1.descripcionOtros === "") {
      this.messageService.add({
        key: "toast",
        severity: "warn",
        detail: "Debe ingresar la Justificación",
      });
    }  else{
      this.ref.close(this.obj1);
      console.log("Salida",this.obj1);
    }
     
  };
  cerrarModal(){
    this.ref.close()
  }
}
