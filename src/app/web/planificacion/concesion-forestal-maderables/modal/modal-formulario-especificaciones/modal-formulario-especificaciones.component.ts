import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ManejoForestalDetalleModel } from 'src/app/model/ManejoForestal';

@Component({
  selector: 'app-modal-formulario-especificaciones',
  templateUrl: './modal-formulario-especificaciones.component.html',
})
export class ModalFormularioEspecificacionesComponent implements OnInit {
  especificacionesObjt = new ManejoForestalDetalleModel();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == 'true') {
      this.especificacionesObjt.tratamientoSilvicultural =
        this.config.data.item.tratamientoSilvicultural;
      this.especificacionesObjt.metodoConstruccion =
        this.config.data.item.metodoConstruccion;
      this.especificacionesObjt.manoObra = this.config.data.item.manoObra;
      this.especificacionesObjt.maquina = this.config.data.item.maquina;
    }
  }

  agregar = () => {

    let mensaje="";
    if (this.especificacionesObjt.tratamientoSilvicultural === '') {
      mensaje= 'Operaciones, es obligatorio';
    }if (this.especificacionesObjt.metodoConstruccion === '') {
      mensaje=mensaje+'\n Método y diseño, es obligatorio';
    } if (this.especificacionesObjt.manoObra === '') {
      mensaje=mensaje+'\n Personal, es obligatorio';
    }
    if(mensaje!=""){
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: '',
        detail: mensaje
      });
    }
    else{
      this.ref.close(this.especificacionesObjt);
    }


  };
  cerrarModal() {
    this.ref.close();
  }
}
