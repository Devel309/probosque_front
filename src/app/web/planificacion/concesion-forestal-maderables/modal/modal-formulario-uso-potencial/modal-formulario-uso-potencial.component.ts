import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ManejoForestalDetalleModel } from 'src/app/model/ManejoForestal';

@Component({
  selector: 'app-modal-formulario-uso-potencial',
  templateUrl: './modal-formulario-uso-potencial.component.html',
})
export class ModalFormularioUsoPotencialComponent implements OnInit {
  categoriaZonificacionObjt = new ManejoForestalDetalleModel();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}
  anydata: any = {};
  categoria: string = '';
  usoPotencial: string = '';
  actRealizar: string = '';
  ngOnInit() {
    if (this.config.data.edit == 'E') {
      this.anydata = { ...this.config.data.item };
      
      this.categoria = this.config.data.item.categoria;
      this.usoPotencial = this.config.data.item.usoPotencial;
      this.actRealizar = this.config.data.item.actRealizar;
    }
  }

  agregar = () => {
    if (this.anydata.categoria === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: 'Alerta',
        detail: 'Categoria, es obligatorio',
      });
    } else if (this.anydata.usoPotencial === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: 'Alerta',
        detail: 'Uso potencial, es obligatorio',
      });
    } else if (this.anydata.actRealizar === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'warn',
        summary: 'Alerta',
        detail: 'Actividades, es obligatorio',
      });
    }
    this.ref.close(this.anydata);
    
  };

  cerrarModal() {
    this.ref.close();
  }
}
