import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-formulario-especies-importantes",
  templateUrl: "./modal-formulario-especies-importantes.component.html",
})
export class ModalFormularioEspeciesImportantesComponent implements OnInit {
  nombreCient : string =  "";
  nombreCom : string = "";
  obj = {
    nombreCientifico : "",
    nombreComun : ""
  };

  nombreCientifico = [{ label: "nombre científico1" },{ label: "nombre científico" }];
  nombreComun = [{ label: "nombre común2" },{ label: "nombre común" }];

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == "true") {
      
      this.nombreCient = this.config.data.item.nombreCientifico;
      this.nombreCom = this.config.data.item.nombreComun;
    }
  }

  nombreCientificoSelect(){
    this.obj.nombreCientifico = this.nombreCient
  }
  nombreComunSelect(){
    this.obj.nombreComun = this.nombreCom
  }

  agregar = () => {
    if (this.nombreCient === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Nombre Científico, es obligatorio",
      });
    } else this.ref.close(this.obj);
  };
  cerrarModal(){
    this.ref.close()
  }
}
