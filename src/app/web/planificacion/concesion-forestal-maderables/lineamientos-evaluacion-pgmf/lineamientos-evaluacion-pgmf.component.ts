import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ArchivoService, UsuarioService } from "@services";
import {
  descargarArchivo,
  DownloadFile,
  isNullOrEmpty,
  ToastService,
} from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ListarPlanManejoEvaluacionRequest } from "src/app/model/plan-manejo-evaluacion";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { SolicitudOpinionRegistro } from "src/app/model/solicitud-opinion";
import { TipoPlanManejo } from "src/app/model/util/codigos/TipoPlanManejo";
import { FileModel } from "src/app/model/util/File";
import { EntidadService } from "src/app/service/entidad.service";
import { EvaluacionCampoService } from "src/app/service/evaluacion-campo.service";
import { GenericoService } from "src/app/service/generico.service";
import { PlanManejoEvaluacionService } from "src/app/service/plan-manejo-evaluacion.service";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service";
import { SolicitudOpinionService } from "src/app/service/solicitud-opinion.service";

@Component({
  selector: "app-lineamientos-evaluacion-pgmf",
  templateUrl: "./lineamientos-evaluacion-pgmf.component.html",
  styleUrls: ["./lineamientos-evaluacion-pgmf.component.scss"],
})
export class LineamientosEvaluacionPgmfComponent implements OnInit {
  tituloSolicitud: string = "";
  verModalSolicitud: boolean = false;
  estado: string = "";
  evaluado: boolean = false;

  comboEntidad: any[] = [];
  comboTipoDocumento: any = [];

  lstVerficarPGFM: any[] = [];
  lstUbicacionEspecial: any[] = [];

  compiladoPGMF: ModelArchivo = new ModelArchivo();
  compiladoFirmadoPGMF: ModelArchivo = new ModelArchivo();
  compiladoFirmadoUmfPGMF: ModelArchivo = new ModelArchivo();
  idPlanManejo!: number; //22
  filesFirmados: FileModel[] = [];
  fileFirmadoPGMF: FileModel = {} as FileModel;
  fileFirmadoEspacialUmf: FileModel = {} as FileModel;
  fileContingencia: any = {};
  verEnviar: boolean = false;
  eliminarFirmadoPGMF: boolean = false;
  eliminarFirmadoUmfPGMF: boolean = false;
  firmadoPGMFcargadoInputFile: boolean = false;
  firmadoUmfPGMFcargadoInputFile: boolean = false;

  fileAnexoPdfPGMF: any;

  idPlanManejoEvaluacion!: number;

  disabled: boolean = true;
  disabledNotificacion: boolean = true;
  fileResolucion: FileModel = {} as FileModel;
  listaFileResolucion: FileModel[] = [];
  fileResolucionModelArchivo: ModelArchivo = new ModelArchivo();
  descargarResolucionCargada: boolean = false;
  eliminarResolucionCargada: boolean = false;

  fileNotificacion: FileModel = {} as FileModel;
  listaFileNotificacion: FileModel[] = [];
  fileNotificacionModelArchivo: ModelArchivo = new ModelArchivo();
  descargarNotificacionCargada: boolean = false;
  eliminarNotificacionCargada: boolean = false;
  idDocumentoAdjunto!: number;
  desCodTipoFaltante!: string;

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  static get EXTENSIONSAUTHORIZATION3() {
    return [".pdf", "application/pdf"];
  }

  usuario!: UsuarioModel;
  opinion: any = {};
  archivoAdjunto: any = {};

  listarPlanManejoEvaluacionRequest: ListarPlanManejoEvaluacionRequest = new ListarPlanManejoEvaluacionRequest();
  informacion: any = {};
  lista: any = [
    { codigo: "Aprobado" },
    { codigo: "Desaprobado" },
    { codigo: "No Presentado" },
  ];

  displayOk: boolean = false;
  displayConfirm: boolean = false;

  minDate!: Date;

  conformeConsideraciones: boolean = false;
  mensajeConfirmEnviar: string = "¿Esta seguro de enviar la evaluación?";

  constructor(
    private entidadServ: EntidadService,
    private dialog: MatDialog,
    private toast: ToastService,
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private usuarioServ: UsuarioService,
    private confirmationService: ConfirmationService,
    private genericoServ: GenericoService,
    private opinionServ: SolicitudOpinionService,
    private router: Router,
    private route: ActivatedRoute,
    private planManejoEvServ: PlanManejoEvaluacionService,
    private archivoServ: ArchivoService,
    private evalCamposerv: EvaluacionCampoService
  ) {}

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    const auxHoy: Date = new Date();
    //this.minDate = new Date(auxHoy.getFullYear()-1, auxHoy.getMonth(), auxHoy.getDate());
    this.minDate = auxHoy;

    this.route.paramMap.subscribe((params) => {
      if (params.get("idEval")) {
        this.idPlanManejoEvaluacion = Number(params.get("idEval"));
      } else {
        this.idPlanManejoEvaluacion = 0;
      }
    });

    this.obtenerInformacionEvaluacion(this.idPlanManejoEvaluacion);

    this.listarComboEntidad();
    this.listarComboTipoDocumentos();
    this.listarVerficarPGFM();
    this.listarUbicacionEspecial();
    this.fileFirmadoPGMF.inServer = false;
    this.fileFirmadoEspacialUmf.inServer = false;
  }

  clickIr() {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([
        `/planificacion/concesion-forestal-maderables/${this.informacion.idPlanManejo}`,
      ])
    );
    window.open(url, "_blank");
  }

  solicitarInspeccion() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampo: 0,
      documentoGestion: this.informacion.idPlanManejo,
      tipoDocumentoGestion: TipoPlanManejo.PGMF_CONCESION_MADERABLE,
      idUsuarioRegistro: this.usuario.idusuario,
    };

    this.evalCamposerv.registroEvaluacionCampo(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.actualizarEvaluacionCampoPlanManejoEvaluacion(result.codigo);
        } else {
          this.ErrorMensaje("Ocurrió un error");
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  actualizarEvaluacionCampoPlanManejoEvaluacion(idEvaluacionCampo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idEvaluacionCampo: idEvaluacionCampo,
      idPlanManejoEvaluacion: this.informacion.idPlanManejoEval,
      idUsuarioModificacion: this.usuario.idusuario,
    };

    this.planManejoEvServ
      .actualizarEvaluacionCampoPlanManejoEvaluacion(params)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje("Se registró la solicitud de evaluación correctamente.");
            this.informacion.idEvaluacionCampo = idEvaluacionCampo;
          } else {
            this.ErrorMensaje("Ocurrió un error");
          }
        },
        (error) => {
          this.dialog.closeAll();
          this.ErrorMensaje("Ocurrió un error");
        }
      );
  }

  regresar() {
    this.router.navigate([
      "/planificacion/consideraciones-pgmf",
      this.idPlanManejoEvaluacion,
    ]);
  }

  abrirModalSolicitud(entidad: string) {
    this.opinion = {};
    this.opinion.siglaEntidad = entidad;
    this.archivoAdjunto = {};

    this.verModalSolicitud = true;

    // this.listarArchivosAsociados();

    // this.fileFirmadoPGMF.inServer = false;
    // this.fileFirmadoPGMF.descripcion = "PDF";
  }

  obtenerInformacionEvaluacion(idPlanManejoEval: number) {
    this.listarPlanManejoEvaluacionRequest.idPlanManejoEval = idPlanManejoEval;

    this.planManejoEvServ
      .listarPlanManejoEvaluacion(this.listarPlanManejoEvaluacionRequest)
      .subscribe(
        (result: any) => {
          if (result.data[0]) {
            this.informacion = {
              ...result.data[0],
              fechaNotificacion: result.data[0].fechaNotificacion
                ? new Date(result.data[0].fechaNotificacion)
                : result.data[0].fechaNotificacion,
            };

            this.listarArchivosAsociados();
          }

          if (
            this.informacion.estadoPlanManejo == "EPLMAPROB" ||
            this.informacion.estadoPlanManejo == "EPLMBORD" ||
            this.informacion.estadoPlanManejo == "EPLMBORN"
          ) {
            this.estado = this.informacion.estadoPlanManejo;
          }
          if (
            this.usuario.sirperfil == "ARFFS" &&
            this.informacion.estadoPlanManejoEvaluacion == "EEVAEVAL"
          ) {
            this.disabled = false;
          }
          if (this.usuario.sirperfil == "ARFFS" && (this.informacion.estadoPlanManejo && this.informacion.estadoPlanManejo == "EPLMAPROB")) {
            this.disabledNotificacion = false;
          }

          this.listarArchivosAsociados();
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  listarComboEntidad() {
    this.entidadServ.listaComboEntidad({}).subscribe((result: any) => {
      this.comboEntidad = result.data;
    });
  }

  listarComboTipoDocumentos() {
    let params = { prefijo: "TDOCGE" };


    this.genericoServ
      .listarPorFiltroParametro(params)
      .subscribe((result: any) => {
        this.comboTipoDocumento = result.data;
      });
  }

  listarVerficarPGFM() {
    let params = {
      idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
      idTipoParametro: 90,
    };

    this.planManejoEvServ
      .listarPlanManejoEvaluacionLineamiento(params)
      .subscribe((result: any) => {
        this.lstVerficarPGFM = result.data;
      });
  }

  listarUbicacionEspecial() {
    let params = {
      idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
      idTipoParametro: 91,
    };

    this.planManejoEvServ
      .listarPlanManejoEvaluacionLineamiento(params)
      .subscribe((result: any) => {
        this.lstUbicacionEspecial = result.data;
      });
  }

  listarArchivosAsociados() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService
      .listarArchivosPGMF(this.informacion.idPlanManejo, "PGMF")
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess) {
          if (res.data.length > 0) {
            let objetoEvaluacion = res.data.find(
              (x: any) => x.codigoSubTipoPGMF == "PGMF-EVAL"
            );
            let objetoPdf = res.data.find(
              (x: any) => x.codigoSubTipoPGMF == "PGMF-PDF"
            );
            let objetoUmfPdf = res.data.find(
              (x: any) => x.codigoSubTipoPGMF == "PGMF-UMF-PDF"
            );
            let objetoPlanContingencia = res.data.find(
              (x: any) => x.tipoDocumento == "PGMFDOCCONTIN"  // PGMFDOCCONTIN   PGMF-CONTIN
            );

            let objetoResolucion = res.data.find(
              (x: any) => x.codigoSubTipoPGMF == "PGMF-RES-PDF"
            );
            let objetoNotificacion = res.data.find(
              (x: any) => x.codigoSubTipoPGMF == "PGMF-NOT-PDF"
            );

            if (objetoEvaluacion) {
              this.compiladoPGMF.archivo = objetoEvaluacion.documento;
              this.compiladoPGMF.nombeArchivo = objetoEvaluacion.nombre;
              this.compiladoPGMF.idArchivo = objetoEvaluacion.idArchivo;
              this.compiladoPGMF.idPGMFArchivo = objetoEvaluacion.idPGMFArchivo;
              this.compiladoPGMF.contenTypeArchivo = "WORD";
            }

            if (objetoPdf) {
              this.compiladoFirmadoPGMF.archivo = objetoPdf.documento;
              this.compiladoFirmadoPGMF.nombeArchivo = objetoPdf.nombre;
              this.compiladoFirmadoPGMF.idArchivo = objetoPdf.idArchivo;
              this.compiladoFirmadoPGMF.idPGMFArchivo = objetoPdf.idPGMFArchivo;
              this.compiladoFirmadoPGMF.contenTypeArchivo = "PDF";
              //archivo fileUpload
              this.fileFirmadoPGMF.inServer = true;
              this.fileFirmadoPGMF.descripcion = "PDF";
              this.fileFirmadoPGMF.nombreFile = objetoPdf.nombre;
              this.firmadoPGMFcargadoInputFile = true;

              this.eliminarFirmadoPGMF = true;
            }

            if (objetoUmfPdf) {
              this.compiladoFirmadoUmfPGMF.archivo = objetoUmfPdf.documento;
              this.compiladoFirmadoUmfPGMF.nombeArchivo = objetoUmfPdf.nombre;
              this.compiladoFirmadoUmfPGMF.idArchivo = objetoUmfPdf.idArchivo;
              this.compiladoFirmadoUmfPGMF.idPGMFArchivo =
                objetoUmfPdf.idPGMFArchivo;
              this.compiladoFirmadoUmfPGMF.contenTypeArchivo = "PDF";
              //archivo fileUpload
              this.fileFirmadoEspacialUmf.inServer = true;
              this.fileFirmadoEspacialUmf.descripcion = "PDF";
              this.fileFirmadoEspacialUmf.nombreFile = objetoUmfPdf.nombre;
              this.firmadoUmfPGMFcargadoInputFile = true;

              this.eliminarFirmadoUmfPGMF = true;
            }

            if (objetoPlanContingencia) {
              this.fileContingencia = objetoPlanContingencia;
            }
            if (objetoResolucion) {
              this.fileResolucionModelArchivo.archivo =
                objetoResolucion.documento;
              this.fileResolucionModelArchivo.nombeArchivo =
                objetoResolucion.nombre;
              this.fileResolucionModelArchivo.idArchivo =
                objetoResolucion.idArchivo;
              this.fileResolucionModelArchivo.idPGMFArchivo =
                objetoResolucion.idPGMFArchivo;
              this.fileResolucionModelArchivo.contenTypeArchivo = "PDF";

              this.fileResolucion.inServer = true;
              this.fileResolucion.descripcion = "PDF";
              this.fileResolucion.nombreFile = objetoResolucion.nombre;
              this.descargarResolucionCargada = true;
              this.eliminarResolucionCargada = true;
            }
            if (objetoNotificacion) {
              this.fileNotificacionModelArchivo.archivo =
                objetoNotificacion.documento;
              this.fileNotificacionModelArchivo.nombeArchivo =
                objetoNotificacion.nombre;
              this.fileNotificacionModelArchivo.idArchivo =
                objetoNotificacion.idArchivo;
              this.fileNotificacionModelArchivo.idPGMFArchivo =
                objetoNotificacion.idPGMFArchivo;
              this.fileNotificacionModelArchivo.contenTypeArchivo = "PDF";

              this.fileNotificacion.inServer = true;
              this.fileNotificacion.descripcion = "PDF";
              this.fileNotificacion.nombreFile = objetoNotificacion.nombre;
              this.descargarNotificacionCargada = true;
              this.eliminarNotificacionCargada = true;
            }
          }
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  compilarPGMF() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idPlanManejo: this.informacion.idPlanManejo,
      idPlanManejoEvaluacion: this.informacion.idPlanManejoEval,
      idUsuarioRegistro: this.usuario.idusuario,
      tipoDocumento: "PGMF",
    };

    this.cargaEnvioDocumentacionService
      .compilarPGMF(params)
      //.pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.compiladoPGMF = res;

            res.data.forEach((element: any) => {
              if (element.archivo) {
                let archivo = new ModelArchivo();
                archivo.archivo = element.archivo;
                archivo.contenTypeArchivo = element.nombeArchivo.split(".")[1];
                archivo.nombeArchivo = element.nombeArchivo;
                descargarArchivo(archivo);
              }
            });
            this.dialog.closeAll();
          } else {
            this.toast.error(res?.message);
            this.dialog.closeAll();
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error");
          this.dialog.closeAll();
        }
      );
  }

  descargarPGMF() {
    if (isNullOrEmpty(this.compiladoPGMF)) {
      this.toast.warn("Debe generar archivo compilado PGMF");
      return;
    }

    descargarArchivo(this.compiladoPGMF);
    if (this.fileAnexoPdfPGMF) {
      descargarArchivo(this.fileAnexoPdfPGMF);
    }
  }

  descargarPlanContingencia() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.fileContingencia.idArchivo) {
      let params = {
        idArchivo: this.fileContingencia.idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.ErrorMensaje("No se encontro el archivo");
    }
  }

  notificar() {
    if (!this.informacion.fechaNotificacion) {
      this.ErrorMensaje("Ingrese Fecha Notificación");
      return;
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    let param: any = {
      idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
      idUsuarioRegistro: this.usuario.idusuario,
      fechaNotificacion: this.informacion.fechaNotificacion,
      idPlanManejo: this.informacion.idPlanManejo,
    };

    this.planManejoEvServ.notificarTitular(param).subscribe(
      (result: any) => {
        this.dialog.closeAll();

        if (result.success) {
          this.SuccessMensaje("Se envió notificación correctamente");
          this.informacion.notificacion = true;
          if(this.informacion.estadoPlanManejo && this.informacion.estadoPlanManejo == "EPLMAPROB") {
            this.disabledNotificacion = true;
          }
          
        } else {
          this.ErrorMensaje("Ocurrió un error");
        }
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  cargarEspacialUmfFile(auxFile: any) {
    this.filesFirmados = [];
    if (this.compiladoFirmadoUmfPGMF.idArchivo) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.cargaEnvioDocumentacionService
        .eliminarPdf(
          this.compiladoFirmadoUmfPGMF.idArchivo,
          this.usuarioServ.idUsuario
        )
        .subscribe(
          (response: any) => {
            if (response.success) {
              this.fileFirmadoEspacialUmf.file = auxFile;
              this.fileFirmadoEspacialUmf.descripcion = "PDF";
              this.fileFirmadoEspacialUmf.inServer = false;
              this.filesFirmados.push(this.fileFirmadoEspacialUmf);
              this.cargarArchivoCompiladoFirmadoEspacialUmf();
            } else {
              this.toast.error("Ocurrió un error al realizar la operación");
              this.dialog.closeAll();
            }
          },
          (error) => {
            this.toast.error("Ocurrió un error al realizar la operación");
            this.dialog.closeAll();
          }
        );
    } else {
      this.fileFirmadoEspacialUmf.file = auxFile;
      this.fileFirmadoEspacialUmf.descripcion = "PDF";
      this.fileFirmadoEspacialUmf.inServer = false;
      this.filesFirmados.push(this.fileFirmadoEspacialUmf);
      this.cargarArchivoCompiladoFirmadoEspacialUmf();
    }
  }

  cargarArchivoCompiladoFirmadoEspacialUmf() {
    this.filesFirmados.forEach((t: any) => {
      if (!t.inServer) {
        let item = {
          id: this.usuarioServ.idUsuario,
          tipoDocumento: 36,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cargaEnvioDocumentacionService
          .cargarCompiladoFirmadoPGMF(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            let params = {
              codigoTipoPGMF: "PGMF",
              codigoSubTipoPGMF: "PGMF-UMF-PDF",
              idArchivo: result.data,
              idPlanManejo: this.informacion.idPlanManejo,
            };

            this.cargaEnvioDocumentacionService.registrarArchivo(params).subscribe((result: any) => {
              if (result.codigo) this.listarArchivosAsociados();
            });
          });
      }
    });
  }

  cargarCompiladoFirmadoFile(auxFile: any) {
    this.filesFirmados = [];
    if (this.compiladoFirmadoPGMF.idArchivo) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.cargaEnvioDocumentacionService
        .eliminarPdf(
          this.compiladoFirmadoPGMF.idArchivo,
          this.usuarioServ.idUsuario
        )
        .subscribe(
          (response: any) => {
            if (response.success) {
              this.fileFirmadoPGMF.file = auxFile;
              this.fileFirmadoPGMF.descripcion = "PDF";
              this.fileFirmadoPGMF.inServer = false;
              this.filesFirmados.push(this.fileFirmadoPGMF);
              this.cargarArchivoCompiladoFirmado();
            } else {
              this.toast.error("Ocurrió un error al realizar la operación");
              this.dialog.closeAll();
            }
          },
          (error) => {
            this.toast.error("Ocurrió un error al realizar la operación");
            this.dialog.closeAll();
          }
        );
    } else {
      this.fileFirmadoPGMF.file = auxFile;
      this.fileFirmadoPGMF.descripcion = "PDF";
      this.fileFirmadoPGMF.inServer = false;
      this.filesFirmados.push(this.fileFirmadoPGMF);
      this.cargarArchivoCompiladoFirmado();
    }
  }

  cargarArchivoCompiladoFirmado() {
    this.filesFirmados.forEach((t: any) => {
      if (!t.inServer) {
        let item = {
          id: this.usuarioServ.idUsuario,
          tipoDocumento: 36,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cargaEnvioDocumentacionService
          .cargarCompiladoFirmadoPGMF(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            let params = {
              codigoTipoPGMF: "PGMF",
              codigoSubTipoPGMF: "PGMF-PDF",
              idArchivo: result.data,
              idPlanManejo: this.informacion.idPlanManejo,
            };

            this.cargaEnvioDocumentacionService.registrarArchivo(params).subscribe((result: any) => {
              if (result.codigo) this.listarArchivosAsociados();
            });
          });
      }
    });
  }

  cargarResolucionFile(auxFile: any) {
    this.listaFileResolucion = [];
    if (this.fileResolucionModelArchivo.idArchivo) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.cargaEnvioDocumentacionService
        .eliminarPdf(
          this.fileResolucionModelArchivo.idArchivo,
          this.usuarioServ.idUsuario
        )
        .subscribe(
          (response: any) => {
            if (response.success) {
              this.fileResolucion.file = auxFile;
              this.fileResolucion.descripcion = "PDF";
              this.fileResolucion.inServer = false;
              this.listaFileResolucion.push(this.fileResolucion);
              this.cargarArchivoResolucion();
            } else {
              this.toast.error("Ocurrió un error al realizar la operación");
              this.dialog.closeAll();
            }
          },
          (error) => {
            this.toast.error("Ocurrió un error al realizar la operación");
            this.dialog.closeAll();
          }
        );
    } else {
      this.fileResolucion.file = auxFile;
      this.fileResolucion.descripcion = "PDF";
      this.fileResolucion.inServer = false;
      this.listaFileResolucion.push(this.fileResolucion);
      this.cargarArchivoResolucion();
    }
  }

  cargarArchivoResolucion() {
    this.listaFileResolucion.forEach((t: any) => {
      if (!t.inServer) {
        let item = {
          id: this.usuarioServ.idUsuario,
          tipoDocumento: 36,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cargaEnvioDocumentacionService
          .cargarCompiladoFirmadoPGMF(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            let params = {
              codigoTipoPGMF: "PGMF",
              codigoSubTipoPGMF: "PGMF-RES-PDF",
              idArchivo: result.data,
              idPlanManejo: this.informacion.idPlanManejo,
            };

            this.cargaEnvioDocumentacionService.registrarArchivo(params).subscribe((result: any) => {
              if (result.codigo) this.listarArchivosAsociados();
            });
          });
      }
    });
  }

  cargarNorificacionfile(auxFile: any) {
    this.listaFileNotificacion = [];
    if (this.fileNotificacionModelArchivo.idArchivo) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.cargaEnvioDocumentacionService
        .eliminarPdf(
          this.fileNotificacionModelArchivo.idArchivo,
          this.usuarioServ.idUsuario
        )
        .subscribe(
          (response: any) => {
            if (response.success) {
              this.fileNotificacion.file = auxFile;
              this.fileNotificacion.descripcion = "PDF";
              this.fileNotificacion.inServer = false;
              this.listaFileNotificacion.push(this.fileNotificacion);
              this.cargarArchivoNotificacion();
            } else {
              this.toast.error(
                "Ocurrió un error al realizar la operación"
              );
              this.dialog.closeAll();
            }
          },
          (error) => {
            this.toast.error(
              "Ocurrió un error al realizar la operación"
            );
            this.dialog.closeAll();
          }
        );
    } else {
      this.fileNotificacion.file = auxFile;
      this.fileNotificacion.descripcion = "PDF";
      this.fileNotificacion.inServer = false;
      this.listaFileNotificacion.push(this.fileNotificacion);
      this.cargarArchivoNotificacion();
    }
  }

  cargarArchivoNotificacion() {
    this.listaFileNotificacion.forEach((t: any) => {
      if (!t.inServer) {
        let item = {
          id: this.usuarioServ.idUsuario,
          tipoDocumento: 36,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cargaEnvioDocumentacionService
          .cargarCompiladoFirmadoPGMF(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            let params = {
              codigoTipoPGMF: "PGMF",
              codigoSubTipoPGMF: "PGMF-NOT-PDF",
              idArchivo: result.data,
              idPlanManejo: this.informacion.idPlanManejo,
            };

            this.cargaEnvioDocumentacionService
              .registrarArchivo(params)
              .subscribe((result: any) => {
                if (result.codigo) {
                  this.listarArchivosAsociados();
                }
              });
          });
      }
    });
  }

  registrarArchivoFile(auxFile: any, tipo: number) {
    switch (tipo) {
      case 1:
        this.cargarEspacialUmfFile(auxFile.file);
        break;
      case 2:
        this.cargarCompiladoFirmadoFile(auxFile.file);
        break;
      case 3:
        this.cargarResolucionFile(auxFile.file);
        break;
      case 4:
        this.cargarNorificacionfile(auxFile.file);
        break;
      default:
        break;
    }

  }

  getBase64(file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  guardar() {
    if (!this.validarGuardar()) return;

    let concat = this.lstVerficarPGFM.concat(this.lstUbicacionEspecial);

    let registroEvaluacionLineal: any[] = concat.map((item: any) => {
      return {
        idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
        descripcion: item.descripcion,
        codigoTipo: item.codigoTipo,
        conforme: item.conforme,
        observacion: item.conforme ? "" : item.observacion,
        idUsuarioRegistro: this.usuario.idusuario,
      };
    });

    this.planManejoEvServ
      .registrarPlanManejoEvaluacionLineamiento(registroEvaluacionLineal)
      .subscribe((result: any) => {
        this.SuccessMensaje(result.message);
      });
  }

  enviarSolcitud() {
    if (!this.validarSolicitudOpinion()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });

    let params: SolicitudOpinionRegistro = {
      asunto: this.opinion.asunto,
      estSolicitudOpinion: "ESOPPEND",
      fechaDocGestion: this.opinion.fechaDocGestion,
      idUsuarioRegistro: this.usuario.idusuario,
      nroDocGestion: this.informacion.idPlanManejo,
      numeroDocumento: this.opinion.numeroDocumento,
      siglaEntidad: this.opinion.siglaEntidad,
      tipoDocGestion: TipoPlanManejo.PGMF_CONCESION_MADERABLE,
      tipoDocumento: this.opinion.tipoDocumento,
      documentoGestion: this.idDocumentoAdjunto,
    };

    this.opinionServ
      .registrarSolicitudOpinion(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
        //  debugger;
          if (res.success) {
            this.SuccessMensaje(res.message);
            this.verModalSolicitud = false;
            this.actualizarIdSolicitudOpinion(
              res.data.idSolicitudOpinion,
              res.data.siglaEntidad
            );
          }
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error");
        }
      );
  }

  actualizarIdSolicitudOpinion(
    idSolicitudOpinion: number,
    tipoSolicitud: string
  ) {
    let params = {
      idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
      idSolicitudOpinion: idSolicitudOpinion,
      tipoSolicitud: tipoSolicitud,
    };

    this.planManejoEvServ
      .actualizarIdSolicitudOpinionPlanManejoEvaluacion(params)
      .subscribe(
        (result: any) => {
          if (result.success) {
            if (tipoSolicitud == "MINCUL") {
              this.informacion.idMincul = 1;
            }

            if (tipoSolicitud == "ANA") {
              this.informacion.idAna = 1;
            }

            if (tipoSolicitud == "SERNANP") {
              this.informacion.idSernap = 1;
            }
          } else {
            this.ErrorMensaje("Ocurrió un Error al actualizar la solicitud");
          }
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error");
        }
      );
  }

  abrirModalConfirmacion() {
    this.displayConfirm = true;
  }

  enviar() {
    if (!this.validarGuardar()) return;

    if (this.estado === "" || this.estado === null) {
      this.ErrorMensaje("(*) Debe seleccionar el estado final");
      return;
    }

    let params = { idPlanManejoEvaluacion: this.idPlanManejoEvaluacion };

    this.planManejoEvServ
      .validarLineamiento(params)
      .subscribe((result: any) => {
        if (result.data.completado) {
          let concat = this.lstVerficarPGFM.concat(this.lstUbicacionEspecial);
          this.conformeConsideraciones = concat.every(
            (item) => item.conforme == true
          );

          if (!result.data.conforme) {
            this.conformeConsideraciones = false;
          }

          if (
            this.conformeConsideraciones === false &&
            this.estado === "EPLMAPROB"
          ) {
            this.mensajeConfirmEnviar =
              "Existen observaciones. ¿Desea continuar?";
          }

          this.abrirModalConfirmacion();
        } else {
          this.desCodTipoFaltante = result.data.desCodTipoFaltante;
          this.ErrorMensaje(
            "Está pendiente finalizar la evaluación de lineamiento 3-18 del PGMF " + this.desCodTipoFaltante +' '+
              this.informacion.idPlanManejo.toString()
          );
        }
      });
  }

  enviarEvaluacion() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let concat = this.lstVerficarPGFM.concat(this.lstUbicacionEspecial);

    let registroEvaluacionLineal: any[] = concat.map((item: any) => {
      return {
        idPlanManejoEvaluacion: this.idPlanManejoEvaluacion,
        descripcion: item.descripcion,
        codigoTipo: item.codigoTipo,
        conforme: item.conforme,
        observacion: item.conforme ? "" : item.observacion,
        idUsuarioRegistro: this.usuario.idusuario,
      };
    });

    this.planManejoEvServ
      .registrarPlanManejoEvaluacionLineamiento(registroEvaluacionLineal)
      .subscribe(
        (result: any) => {
          if (result.success) {
            let observacionRestrictiva: boolean = concat.every(
              (item) => item.conforme
            );

            if (
              this.estado == "EPLMAPROB" ||
              this.estado == "EPLMBORD" ||
              this.estado == "EPLMBORN"
            ) {
              this.evaluado = true;
            }

            let param: any = {
              idPlanManejoEval: this.informacion.idPlanManejoEval,
              observacionRestrictiva: null,
              conformeConsideraciones: this.conformeConsideraciones,
              idPlanManejo: this.informacion.idPlanManejo,
              idUsuarioModificacion: this.usuario.idusuario,
              evaluado: this.evaluado,
              estadoEvaluacion: this.estado,
            };

            this.planManejoEvServ.enviarEvaluacion(param).subscribe(
              (result: any) => {
                if (result.success) {
                  this.informacion.estadoPlanManejoEvaluacion = "";
                  this.disabled = true;
                  this.displayConfirm = false;
                  this.SuccessMensaje(result.message);
                  this.dialog.closeAll();

                  this.router.navigate([
                    '/planificacion/bandeja-evaluacion-pgmf',
                  ]);
                }
              },
              (error: HttpErrorResponse) => {
                this.ErrorMensaje(error.message);
                this.dialog.closeAll();
              }
            );
          } else {
            this.ErrorMensaje("Ocurrió un error al enviar");
            this.dialog.closeAll();
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      );
  }

  cerrarModal() {
    this.verModalSolicitud = false;
  }

  validarGuardar() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.lstVerficarPGFM.some((item) => item.conforme == null)) {
      validar = false;
      mensaje = mensaje +=
        "(*) Error En la sección 1 seleccione conforme u observado. \n";
    } else {
      if (
        this.lstVerficarPGFM.some(
          (item) => item.conforme == false && !item.observacion
        )
      ) {
        validar = false;
        mensaje = mensaje +=
          "(*) Error En la sección 1  falta agregar una observación. \n";
      }
    }

    if (this.lstUbicacionEspecial.some((item) => item.conforme == null)) {
      validar = false;
      mensaje = mensaje +=
        "(*) Error En la sección 2 seleccione conforme u observado. \n";
    } else {
      if (
        this.lstUbicacionEspecial.some(
          (item) => item.conforme == false && !item.observacion
        )
      ) {
        validar = false;
        mensaje = mensaje +=
          "(*) Error En la sección 2  falta agregar una observación. \n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  validarSolicitudOpinion() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.opinion.siglaEntidad) {
      validar = false;
      mensaje = mensaje += "(*) Seleccione entidad.\n";
    }

    if (!this.opinion.tipoDocumento) {
      validar = false;
      mensaje = mensaje += "(*) Seleccione tipo de documento.\n";
    }

    if (!this.opinion.numeroDocumento) {
      validar = false;
      mensaje = mensaje += "(*) Ingrese número de documento.\n";
    }

    /*if (!this.opinion.numeroDocumento) {
      validar = false;
      mensaje = mensaje +=
        '(*) Ingrese numero documento.\n';
    }



    if (this.opinion.tipoDocumento && this.opinion.numeroDocumento) {
      if (this.opinion.tipoDocumento == "TDOCDNI") {
        if (this.opinion.numeroDocumento.toString().length < 8 || this.opinion.numeroDocumento.toString().length > 8) {
          validar = false;
          mensaje = mensaje +=
            '(*) Numero Documento Ingrese 8 caracteres.\n';
        }
      }

      if (this.opinion.tipoDocumento == "TDOCRUC") {

        if (this.opinion.numeroDocumento.toString().length < 11 || this.opinion.numeroDocumento.toString().length > 11) {
          validar = false;
          mensaje = mensaje +=
            '(*) Numero Documento Ingrese 11 caracteres.\n';
        }
      }

      if (this.opinion.tipoDocumento == "TDOCEXTR") {

        if (this.opinion.numeroDocumento.toString().length < 12 || this.opinion.numeroDocumento.toString().length > 12) {
          validar = false;
          mensaje = mensaje +=
            '(*) Numero Documento Ingrese 12 caracteres.\n';

        }
      }

    }
    */
    if (!this.opinion.fechaDocGestion) {
      validar = false;
      mensaje = mensaje += "(*) Ingrese fecha de documento.\n";
    }

    if (!this.opinion.asunto) {
      validar = false;
      mensaje = mensaje += "(*) Ingrese asunto.\n";
    }

    /*if (!this.archivoAdjunto.nombre) {
      validar = false;
      mensaje = mensaje +=
        '(*) Adjunte Archivo.\n';
    }*/

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  private obtenerEvaluacionPlanManejo(params: any) {
    this.planManejoEvServ.obtenerEvaluacionPlanManejo(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          if (
            result.data !== undefined &&
            result.data !== null &&
            result.data > 0
          ) {
            this.notificarObservacionesAlTitular(result.data[0]);
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  private notificarObservacionesAlTitular(params: any) {
    this.planManejoEvServ.notificarObservacionesAlTitular(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(result.message);
        }
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  registrarAdjunto(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ
      .cargar(
        this.usuario.idusuario,
        TipoPlanManejo.PGMF_CONCESION_MADERABLE,
        file.file
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.idDocumentoAdjunto = result.data;
            this.toast.ok("Se registró el archivo correctamente.");
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      );
  }

  eliminarAdjunto() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.archivoServ
      .eliminarArchivo(this.idDocumentoAdjunto, this.usuario.idusuario)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.idDocumentoAdjunto = 0;
            this.toast.ok("Se eliminó el archivo correctamente");
          }
        },
        (error) => {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      );

    this.archivoAdjunto = {};
  }

  eliminarArchivoUmfPmfi() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService.eliminarPdf(this.compiladoFirmadoUmfPGMF.idArchivo,this.usuarioServ.idUsuario)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok("Se eliminó el archivo compilado firmado correctamente");
          this.compiladoFirmadoUmfPGMF = new ModelArchivo();
          this.firmadoUmfPGMFcargadoInputFile = false;
          this.eliminarFirmadoUmfPGMF = false;
          this.fileFirmadoEspacialUmf.nombreFile = "";
          this.fileFirmadoEspacialUmf.inServer = false;
          this.fileFirmadoEspacialUmf.descripcion = "PDF";
        } else {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      });
  }

  eliminarArchivoPmfi() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService.eliminarPdf(this.compiladoFirmadoPGMF.idArchivo, this.usuarioServ.idUsuario)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok(
            "Se eliminó el archivo compilado firmado correctamente"
          );
          this.compiladoFirmadoPGMF = new ModelArchivo();
          this.firmadoPGMFcargadoInputFile = false;
          this.eliminarFirmadoPGMF = false;
          this.fileFirmadoPGMF.nombreFile = "";
          this.fileFirmadoPGMF.inServer = false;
          this.fileFirmadoPGMF.descripcion = "PDF";
        } else {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      });
  }

  eliminarArchivoResolucion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService.eliminarPdf(this.fileResolucionModelArchivo.idArchivo, this.usuarioServ.idUsuario)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok("Se eliminó el archivo correctamente");
          this.fileResolucionModelArchivo = new ModelArchivo();
          this.descargarResolucionCargada = false;
          this.eliminarResolucionCargada = false;
          this.fileResolucion.nombreFile = "";
          this.fileResolucion.inServer = false;
          this.fileResolucion.descripcion = "PDF";
        } else {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      });
  }

  eliminarArchivoNotificacion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService.eliminarPdf(this.fileNotificacionModelArchivo.idArchivo, this.usuarioServ.idUsuario)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok("Se eliminó el archivo correctamente");
          this.fileNotificacionModelArchivo = new ModelArchivo();
          this.descargarNotificacionCargada = false;
          this.eliminarNotificacionCargada = false;
          this.fileNotificacion.nombreFile = "";
          this.fileNotificacion.inServer = false;
          this.fileNotificacion.descripcion = "PDF";
        } else {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      });
  }

  eliminarArchivo(ok: boolean, tipo: number) {
    if (!ok) return;
    switch (tipo) {
      case 1:
        this.eliminarArchivoUmfPmfi();
        break;
      case 2:
        this.eliminarArchivoPmfi();
        break;
      case 3:
        this.eliminarArchivoResolucion();
        break;
      case 4:
        this.eliminarArchivoNotificacion();
        break;

      default:
        break;
    }


  }
}

export class ModelArchivo {
  constructor(data?: any) {
    if (data) {
      this.idPGMFArchivo = data.idPGMFArchivo;
      this.archivo = data.archivo;
      this.idArchivo = data.idArchivo;
      this.idPlanManejo = data.idPlanManejo;
      this.nombeArchivo = data.nombeArchivo;
      this.contenTypeArchivo = data.contenTypeArchivo;
      return;
    }
  }
  idPGMFArchivo: number = 0;
  idArchivo: number = 0;
  idPlanManejo: number = 0;
  archivo: string = "";
  nombeArchivo: string = "";
  contenTypeArchivo: string = "";
}
