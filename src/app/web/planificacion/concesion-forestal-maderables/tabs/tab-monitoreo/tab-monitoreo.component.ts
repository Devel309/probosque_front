import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoPGMF } from 'src/app/model/CodigoPGMF';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { UsuarioModel } from 'src/app/model/Usuario';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { MonitoreoService } from 'src/app/service/planificacion/monitoreo.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { ModalFormularioComponent } from './modal-formulario/modal-formulario.component';

@Component({
  selector: 'app-tab-monitoreo',
  templateUrl: './tab-monitoreo.component.html',
})
export class TabMonitoreoComponent implements OnInit {


  @Input() disabled: boolean = false;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  btnGuardar: boolean = false;
  mdlNuevo: boolean = false;
  listaMonitoreo: any[] = [];
  descripcion_sistema_monitoreo: string | null = "";
  id_descripcion_sistema_monitoreo: number = 0;
  id_descripcion_cabecera!: number;

  selectAnexo: string = "N";
  codigo_PGMF: string = 'PGMF';
  idPlanManejoArchivo: number = 0;

  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();

  objUser: UsuarioModel = JSON.parse('' + localStorage.getItem("usuario")?.toString());

  params_insetar_detalle = {
    codigoSeccion: "SECMONI",
    codigoSubTipoDetalle: "",
    codigoTipoDetalle: "",
    descripcion: "",
    fecha: new Date(), idPGMF: 0,
    idUsuarioRegistro: this.objUser.idusuario,
    lugar: "", mecanismoParticipacion: "",
    metodologia: "", modalidadCapacitar: "",
    personalCapacitar: "", operacion: ""
  }


  @Input() idPGMF!: number;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  /* ******* */
  ref!: DynamicDialogRef;
  idPlanManejo!: number;
  listaTableMonitoreo: any = [];
  listaTableMonitoreoCabecera: any = [];

  CodigoPGMF8_2 = CodigoPGMF.TAB_8;
  CodigoPGMF = CodigoPGMF.CODIGO_PROCESO;

  fileAnexo4: any = {};

  constructor(
    private serv: PlanificacionService,
    private user: UsuarioService,
    private messageService: MessageService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private monitoreoService: MonitoreoService,
    private dialog: MatDialog,
    private miToast: ToastService,
    public dialogService: DialogService,
    private postulacionPFDMService: PostulacionPFDMService
  ) { }

  ngOnInit() {
    // this.cargarOperaciones();

    this.idPlanManejo = this.idPGMF; //para pruebas
    this.listarMonitoreo();
  }


  listarMonitoreo() {
    // this.listMonitoreo = [];
    var params = {
      codigoMonitoreo: "PGMF",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.monitoreoService.listarMonitoreoPOCC(params).subscribe((response: any) => {
      this.dialog.closeAll();

      if (response.success) {
        this.listaTableMonitoreo = response.data;
        this.listaTableMonitoreoCabecera = this.listaTableMonitoreo.filter((x: any) => x.monitoreo == "TXTCabecera");
        this.listaTableMonitoreo = this.listaTableMonitoreo.filter((x: any) => x.monitoreo != "TXTCabecera");

        this.selectAnexo = this.listaTableMonitoreo[0].observacion || "N";
        this.id_descripcion_cabecera = this.listaTableMonitoreoCabecera[0].idMonitoreo;
        this.descripcion_sistema_monitoreo = this.listaTableMonitoreoCabecera[0].descripcion;



      } else {
        this.miToast.warn(response.message);
      }

    }, () => {
      this.dialog.closeAll();
    });
  }

  actualizarMonitoreo() {
    if(!this.validarGuardar()) return;
    

    this.listaTableMonitoreoCabecera[0].descripcion = this.descripcion_sistema_monitoreo;
    this.listaTableMonitoreo = this.listaTableMonitoreo.filter((x: any) => x.idPlanManejo = this.idPGMF);
    this.listaTableMonitoreo.forEach((x: any) => {
      if (this.selectAnexo != null) {
        x.observacion = this.selectAnexo
      }
    });

    this.listaTableMonitoreoCabecera = this.listaTableMonitoreoCabecera.filter((x: any) => x.idPlanManejo = this.idPGMF);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.monitoreoService.registrarMonitoreoPOCC(this.listaTableMonitoreo).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success == true) {
        this.monitoreoService.registrarMonitoreoPOCC(this.listaTableMonitoreoCabecera).subscribe((response: any) => {
          this.dialog.closeAll();
          if (response.success == true) {
            //this.miToast.ok(response?.message);
            //this.listarMonitoreo();
          } else {
            // this.miToast.error(response?.message);
          }
        }, () => {
          //this.dialog.closeAll();
        });

        this.miToast.ok('Se registró el monitoreo correctamente');
        this.listarMonitoreo();
      } else {
        this.miToast.error(response?.message);
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    this.ref = this.dialogService.open(ModalFormularioComponent, {
      header: mesaje,
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {

        //console.log(resp);
        //console.log(resp.idMonitoreo);
        this.listaTableMonitoreo.forEach((x: any) => {
          if (x.monitoreo === resp.monitoreo) {
            x.descripcion = resp.descripcion;
            x.lstDetalle = resp.lstDetalle;

          }


        });
        
        //this.listaTableMonitoreo.descr
      }
    });
  };




  /* ********************************* */

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN13', this.lineamiento);
  }

  dataLineamientos(data: any) {

    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN13' }
    );
  }

  listaActualizar: any[] = [];
  cListaActualizar(a: any, b: any) {
    let temp = { id: a, check: b }
    let existe = this.listaActualizar.find(e => e.id == temp.id);

    if (existe != undefined) {
      
      existe.check = b;
    } else {
      this.listaActualizar.push(temp);
    }
    this.btnGuardar = true;
  }

  actualizar() {
    let temp = { id: this.id_descripcion_sistema_monitoreo, check: false, descripcion: this.descripcion_sistema_monitoreo }

    this.listaActualizar.push(temp);
    
    this.serv.monitoreoActualizar(this.listaActualizar).subscribe(
      (result: any) => {
        if (result.success == true) {
          this.toast('success', 'Actualizado correctamente');
          this.btnGuardar = false;
        } else {
          this.toast('error', 'Ocurrió un problema');
        }
      }
    );
  }



  cargarOperaciones = () => {
    this.listaMonitoreo = [];
    this.serv.monitoreo({ idPGMF: this.idPGMF }).subscribe(
      (result: any) => {
        // console.log(result.data);
        if (result.success == true) {

          result.data.forEach((element: any) => {
            if (element.tipo == 'DESCMONI') {
              // console.log('element');
              // console.log(element);
              //this.descripcion_sistema_monitoreo = element.descripcion;
              //this.id_descripcion_sistema_monitoreo = element.id;
            }
            else if (element.operacion != null) {
              this.listaMonitoreo.push(element);
            }
          });
          this.listaSubTipos();
        }
      }
    );
  };

  listaTipos: any[] = [];
  listaSubTipos() {
    let items: any[] = [];
    this.listaTipos = [];

    if (!this.listaMonitoreo[0]) return;

    this.listaTipos.push(this.listaMonitoreo[0]);
    let coutnTipo = 0;
    let i = 0;

    //console.log(this.listaActividades);

    this.listaMonitoreo.forEach(element => {
      i++;
      if (element.subTipo != this.listaTipos[coutnTipo].subTipo) {
        this.listaTipos[coutnTipo].items = items;
        this.listaTipos.push(element);
        coutnTipo++;
        items = [];
      }


      items.push({ d: element.descripcion, i: element.id, o: element.operacion, ch: element.check });
      if (i == this.listaMonitoreo.length) {
        this.listaTipos[coutnTipo].items = items;
      }
    });

  }

  padre: any;
  nuevo(padre: any) {
    this.mdlNuevo = true;
    this.padre = padre;

  }

  texto_nueva_operacion: string = "";
  texto_nueva_descripcion: string = "";
  equal: boolean = false;
  guardar() {

    for (let item of this.padre.items) {
      if (item.o == this.texto_nueva_operacion && item.d == this.texto_nueva_descripcion) {
        this.toast('warn', 'La operación ya existe');
        this.texto_nueva_operacion = ""
        this.texto_nueva_operacion = ""
      }
    }

    if (this.texto_nueva_operacion != "") {
      this.params_insetar_detalle.codigoTipoDetalle = "DESCACC";
      this.params_insetar_detalle.codigoSubTipoDetalle = this.padre.subTipo;
      this.params_insetar_detalle.descripcion = this.texto_nueva_descripcion;
      this.params_insetar_detalle.operacion = this.texto_nueva_operacion;
      this.params_insetar_detalle.idPGMF = this.idPGMF;

      //console.log(this.padre);
      //console.log(this.params_insetar_detalle);
      this.serv.detalleRegistrar(this.params_insetar_detalle).subscribe(
        (result: any) => {
          if (result.success == true) {
            this.toast('success', 'Guardado correctamente');
            this.texto_nueva_operacion = "";
            this.texto_nueva_descripcion = "";
            this.mdlNuevo = false;
            this.cargarOperaciones();
          } else {
            this.toast('error', 'Ocurrió un problema');
            this.mdlNuevo = false;
          }
        }
      );
    } else {
      this.toast('warn', 'Debe escribir una operación');
    }
  }

  eliminarDetalle(id: Number) {
    //console.log(id);
    this.serv.organizacionManejoDetalleEliminar({ idOrganizacionManejo: id, idUsuarioElimina: this.objUser.idusuario }).subscribe(
      (result: any) => {
        //console.log(result);
        if (result.success == true) {
          this.cargarOperaciones();
          this.toast('success', 'Eliminado correctamente');
        } else {
          this.toast('error', 'Ocurrió un problema');
        }
      }
    );
  }

  toast(saverty: String, datail: String) {

    this.messageService.add({
      key: 'tl',
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.selectAnexo = "S";
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }

  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      if(response.success) this.limpiarFile();
    });
  }

  limpiarFile() {
    this.idPlanManejoArchivo = 0;
    this.fileAnexo4.nombreFile = "";
    this.fileAnexo4.justificacion = "";
  }

  validarGuardar(): boolean{
    let valido = true;
    if (this.selectAnexo === "S") {
      if (!this.fileAnexo4.justificacion) {
        this.miToast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4.nombreFile) {
        this.miToast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }





}
