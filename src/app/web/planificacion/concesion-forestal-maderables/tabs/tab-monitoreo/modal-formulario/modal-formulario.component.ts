import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { lstDetalleModel } from 'src/app/model/monitoreoPOCC';
import { MonitoreoService } from 'src/app/service/planificacion/monitoreo.service';

@Component({
  selector: 'app-modal-formulario',
  templateUrl: './modal-formulario.component.html',
  styles: [
  ]
})
export class ModalFormularioComponent implements OnInit {

  context: any = {lstDetalle:[]};
  operacion: string = "";

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private monitoreoService: MonitoreoService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if (this.config.data.type == "E") {
 
      this.context = {...this.config.data.data}
      this.context.lstDetalle = [...this.config.data.data.lstDetalle];
 
    } else this.context.lstDetalle = [];


  }

  agregar = () => {
    if (!this.validarMonitoreo()) {
      return;
    }
    this.ref.close(this.context);
  };

  agregarOperacion() {
    if (this.operacion != "") {
      let detalle = new lstDetalleModel();
      
     detalle.operacion = this.operacion;
     this.context.lstDetalle.push(detalle);

    }
    this.operacion = "";
  }

  eliminar(event: any, idMonitoreoDetalle: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (idMonitoreoDetalle != 0) {
          var params = {
            idMonitoreoDetalle: idMonitoreoDetalle,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.monitoreoService.EliminarDetalleMonitoreo(params).subscribe((response: any) => {
            this.dialog.closeAll();
            if (response.success) {
              this.toast.ok(response?.message);
              this.context.lstDetalle.splice(index, 1);
            } else {
              this.toast.error(response?.message);
            }
          }, () => {
            this.dialog.closeAll();
          });

        } else {
          this.context.lstDetalle.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  validarMonitoreo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.context.descripcion == null || this.context.descripcion == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Descripción.\n";
    }

    if (this.context.lstDetalle.length == null || this.context.lstDetalle.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Operaciones.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  cerrarModal() {
    this.ref.close();
  }

}
