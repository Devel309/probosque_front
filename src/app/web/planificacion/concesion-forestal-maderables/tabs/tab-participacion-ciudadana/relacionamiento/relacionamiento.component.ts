
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ParticipacionComunalDetalleModel } from '@models';
import { ParticipacionTipoPGMF, ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import * as moment from "moment";
import { UsuarioService } from '@services';

@Component({
  selector: 'app-relacionamiento',
  templateUrl: './relacionamiento.component.html',
})
export class ModalRelacionamientoComponent implements OnInit {

  labelBtn: string = "Guardar";
  ParticipacionTipoPGMF = ParticipacionTipoPGMF;

  f: FormGroup;
  participacion: ParticipacionComunalDetalleModel = new ParticipacionComunalDetalleModel();
  codigoPartComunal: string | ParticipacionTipoPGMF = ParticipacionTipoPGMF.IMPLEMENTACION;

  minDate = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private mitoast: ToastService,
    private user: UsuarioService
  ) {
    this.f = this.initForm();
  }

  ngOnInit() {
    this.participacion = this.config.data as ParticipacionComunalDetalleModel;
    const { idUsuarioRegistro, actividad, metodologia, lugar, fecha, } = this.participacion;
    this.f.patchValue({
      idUsuarioRegistro,
      actividad,
      metodologia,
      lugar,
      fecha,
    });
    this.codigoPartComunal = this.participacion.codigoPartComunal;
    this.agregarValidadores(this.codigoPartComunal);
  }

  agregarValidadores(tipo: ParticipacionTipoPGMF | string) {
    if (tipo == ParticipacionTipoPGMF.PLAN) {
      const validators = [Validators.required, Validators.minLength(1)];
      this.f.controls.metodologia.setValidators(validators);

      this.f.controls.lugar.setValidators(validators);
      this.f.controls.fecha.setValidators(validators);
    }
  }

  initForm() {
    return this.fb.group({
      actividad: [null, [Validators.required, Validators.minLength(1)]],
      metodologia: [null],
      idUsuarioRegistro: this.user.idUsuario,
      lugar: [null],
      fecha: [null],
    });
  }

  guardar() {
    this.showFormError();
    if (this.f.invalid) return;

    const { idUsuarioRegistro, actividad, metodologia, lugar, fecha, } = this.f.value;

    this.participacion = {
      ...this.participacion,
      idUsuarioRegistro,
      actividad,
      metodologia,
      lugar,
      fecha,
    };
    this.participacion.idUsuarioRegistro = this.user.idUsuario;
    if (!!this.participacion.fecha) {
      this.participacion.fecha = new Date(this.participacion.fecha);
    }

    this.ref.close(this.participacion);
  }

  cancelar() {
    this.ref.close();
  }

  showFormError() {
    Object.keys(this.f.controls).forEach((key) => {
      const controlErrors: ValidationErrors | null | undefined = this.f.get(key)
        ?.errors;
      if (controlErrors != null) {
        let fechaActual = new Date();
        let fecha = new Date(this.f.controls[key].value);
        if (key == "fecha") {
          if (fecha < fechaActual) {
            this.mitoast.warn('La fecha debe ser mayor o igual a la fecha actual.');
            return;
          }
        }
        key = key == "actividad" && this.codigoPartComunal == ParticipacionTipoPGMF.PLAN ? "mecanismo" : key;
        const detail = `(*) Debe ingresar ${ValidacionCampoError[key]}.\n`;
        this.mitoast.warn(detail);
      }
    });
  }
}

const ValidacionCampoError: any = {
  mecanismo: "mecanismo",
  actividad: "actividad",
  metodologia: "metodología",
  lugar: "lugar",
  fecha: "fecha",
};
