import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalRelacionamientoComponent } from './relacionamiento/relacionamiento.component';
import { ConfirmationService } from 'primeng/api';
import { MatDialog } from '@angular/material/dialog';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ParticipacionComunalService } from 'src/app/service/planificacion/plan-general-manejo-pgmfa/participacion-comunal.service';
import { ParticipacionComunalDetalleModel, PlanManejoModel } from '@models';
import { ParticipacionTipoPGMF, ToastService } from '@shared';
import { UsuarioService } from '@services';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-tab-participacion-ciudadana',
  templateUrl: './tab-participacion-ciudadana.component.html',
  providers: [DatePipe]
})
export class TabParticipacionCiudadanaComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();

  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento4: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento5: LineamientoInnerModel = new LineamientoInnerModel();

  ref!: DynamicDialogRef;

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  /* ********** */
  ParticipacionTipoPGMF = ParticipacionTipoPGMF;
  
  list: any[] = [];

  formulaciones: ParticipacionComunalDetalleModel[] = [];
  implementaciones: ParticipacionComunalDetalleModel[] = [];
  comites: ParticipacionComunalDetalleModel[] = [];
  planes: ParticipacionComunalDetalleModel[] = [];

  idPartComunalForm: number = 0;
  idPartComunalImpl: number = 0;
  idPartComunalComi: number = 0;
  idPartComunalPlan: number = 0;

  constructor(
    public dialogService: DialogService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private mitoast: ToastService,
    private user: UsuarioService,
    private participacionComunalService: ParticipacionComunalService,
  ) { }

  ngOnInit() {
    this.listarPGMFFO();
    this.listarPGMFIM();
    this.listarPGMFCO();
    this.listarPGMFRE();
  }

  /* ******************** */
  listarPGMFFO() {
    this.formulaciones = [];
    this.listarParticipacionComunalCaberaDetalle("PGMFFO");
  }

  listarPGMFIM() {
    this.implementaciones = [];
    this.listarParticipacionComunalCaberaDetalle("PGMFIM");
  }

  listarPGMFCO() {
    this.comites = [];
    this.listarParticipacionComunalCaberaDetalle("PGMFCO");
  }

  listarPGMFRE() {
    this.planes = [];
    this.listarParticipacionComunalCaberaDetalle("PGMFRE");
  }

  listarParticipacionComunalCaberaDetalle(codTipoPartCom: string) {
    var params = {
      codTipoPartComunal: codTipoPartCom,
      codTipoPartComunalDet: null,
      idPartComunal: null,
      idPlanManejo: this.idPGMF
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService.listarParticipacionComunalCaberaDetalle(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (!!response.data) {
        response.data.forEach((element: any) => {

          this.filtrarIdPartComunal(element);

          element.lstDetalle.forEach((element: any) => {
            let obj = new ParticipacionComunalDetalleModel(element);
            this.list.push(obj);
            this.filtrarTipoParticipacion(this.list);
          });

        });
      }
    }, () => {
      this.dialog.closeAll()
    });
  }

  filtrarIdPartComunal(datos: any) {
    if (datos.codTipoPartComunal === 'PGMFFO') {
      this.idPartComunalForm = datos.idPartComunal;
    } else if (datos.codTipoPartComunal === 'PGMFIM') {
      this.idPartComunalImpl = datos.idPartComunal;
    } else if (datos.codTipoPartComunal === 'PGMFCO') {
      this.idPartComunalComi = datos.idPartComunal;
    } else if (datos.codTipoPartComunal === 'PGMFRE') {
      this.idPartComunalPlan = datos.idPartComunal;
    }

  }

  filtrarTipoParticipacion(data: ParticipacionComunalDetalleModel[]) {
    this.implementaciones = [];
    this.formulaciones = [];
    this.comites = [];
    this.planes = [];
    data.forEach((x) => {
      switch (x.codigoPartComunal) {
        case ParticipacionTipoPGMF.FORMULACION:
          let objC0 = new ParticipacionComunalDetalleModel(x);
          objC0.codPartComunalDet = "PGMF1";
          objC0.idUsuarioRegistro = this.user.idUsuario;
          this.formulaciones.push(objC0);
          break;
        case ParticipacionTipoPGMF.IMPLEMENTACION:
          let objCI = new ParticipacionComunalDetalleModel(x);
          objCI.codPartComunalDet = "PGMF2";
          objCI.idUsuarioRegistro = this.user.idUsuario;
          this.implementaciones.push(objCI);
          break;
        case ParticipacionTipoPGMF.COMITE:
          let objC2 = new ParticipacionComunalDetalleModel(x);
          objC2.codPartComunalDet = "PGMF3";
          objC2.idUsuarioRegistro = this.user.idUsuario;
          this.comites.push(objC2);
          break;
        case ParticipacionTipoPGMF.PLAN:
          let objC3 = new ParticipacionComunalDetalleModel(x);
          objC3.codPartComunalDet = "PGMF4";
          objC3.idUsuarioRegistro = this.user.idUsuario;
          this.planes.push(objC3);
          break;
        default:
          break;
      }
    });
  }

  guardarParticipacionComunal() {
    var params = [
      {
        actividad: "",
        codTipoPartComunal: "PGMFFO",
        fecha: "",
        idPartComunal: this.idPartComunalForm,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: "",
        idPlanManejo: this.idPGMF,
        responsable: "",
        seguimientoActividad: "",
        lstDetalle: this.formulaciones,
      },
      {
        actividad: "",
        codTipoPartComunal: "PGMFIM",
        fecha: "",
        idPartComunal: this.idPartComunalImpl,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: "",
        idPlanManejo: this.idPGMF,
        responsable: "",
        seguimientoActividad: "",
        lstDetalle: this.implementaciones,
      },
      {
        actividad: "",
        codTipoPartComunal: "PGMFCO",
        fecha: "",
        idPartComunal: this.idPartComunalComi,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: "",
        idPlanManejo: this.idPGMF,
        responsable: "",
        seguimientoActividad: "",
        lstDetalle: this.comites,
      },
      {
        actividad: "",
        codTipoPartComunal: "PGMFRE",
        fecha: "",
        idPartComunal: this.idPartComunalPlan,
        idUsuarioRegistro: this.user.idUsuario,
        lugar: "",
        idPlanManejo: this.idPGMF,
        responsable: "",
        seguimientoActividad: "",
        lstDetalle: this.planes,
      },
    ];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService.registrarParticipacionComunalCaberaDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.mitoast.ok('Se registró la participación ciudadana correctamente.');
          this.list = [];
          this.listarPGMFFO();
          this.listarPGMFIM();
          this.listarPGMFCO();
          this.listarPGMFRE();
        } else {
          this.mitoast.error(response?.message);
        }
      });
  }

  eliminar(event: Event, codigoPartComunal: ParticipacionTipoPGMF, idPartComunalDet: number, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar esta acción?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });
        if (codigoPartComunal == ParticipacionTipoPGMF.FORMULACION) {
          this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
            .pipe(
              finalize(() => {
                this.dialog.closeAll();
                this.formulaciones.splice(index, 1);
              })
            )
            .subscribe();
        } else if (codigoPartComunal == ParticipacionTipoPGMF.IMPLEMENTACION) {
          this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
            .pipe(
              finalize(() => {
                this.dialog.closeAll();
                this.implementaciones.splice(index, 1);
              })
            )
            .subscribe();
        } else if (codigoPartComunal == ParticipacionTipoPGMF.COMITE) {
          this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
            .pipe(
              finalize(() => {
                this.dialog.closeAll();
                this.comites.splice(index, 1);
              })
            )
            .subscribe();
        } else if (codigoPartComunal == ParticipacionTipoPGMF.PLAN) {
          this.eliminarParticipacion(idPartComunalDet, this.user.idUsuario)
            .pipe(
              finalize(() => {
                this.dialog.closeAll();
                this.planes.splice(index, 1);
              })
            )
            .subscribe();
        }
      },
    });
  }

  eliminarParticipacion(idPartComunalDet: number, idUsuarioElimina: number) {
    var params = { idPartComunalDet: idPartComunalDet, idUsuarioElimina: idUsuarioElimina, };
    return this.participacionComunalService.eliminarParticipacionComunalCaberaDetalle(params)
      .pipe(
        tap({
          next: (res: any) => {
            if (res.success == true) {
              this.mitoast.ok(res?.message);
            } else {
              this.mitoast.error(res?.message);
            }
          },
        })
      );
  }

  // MODAL
  abrirModal(titulo: String, codigoPartComunal: ParticipacionTipoPGMF, data?: ParticipacionComunalDetalleModel, index?: number) {
    const header = `${titulo} Participación Ciudadana`;
    const planManejo = { idPlanManejo: this.idPGMF } as PlanManejoModel;
    data = data ? data : new ParticipacionComunalDetalleModel({ codigoPartComunal, planManejo });
    const config = { header, data, width: "50vw", closable: true };

    const ref = this.dialogService.open(ModalRelacionamientoComponent, config);

    ref.onClose.subscribe((value) => {
      if (value) {
        switch (data?.codigoPartComunal) {
          case ParticipacionTipoPGMF.FORMULACION:
            if (index != undefined) this.formulaciones[index] = value;
            else this.formulaciones.push(value);
            break;
          case ParticipacionTipoPGMF.IMPLEMENTACION:
            if (index != undefined) this.implementaciones[index] = value;
            else this.implementaciones.push(value);
            break;
          case ParticipacionTipoPGMF.COMITE:
            if (index != undefined) this.comites[index] = value;
            else this.comites.push(value);
            break;
          case ParticipacionTipoPGMF.PLAN:
            if (index != undefined) this.planes[index] = value;
            else this.planes.push(value);
            break;
          default:
            break;
        }
      }
    });
  }

  /* ******************** */

  listarEvaluaciones(data: any) {
    // this.ordenarLista(data, 'PGMFEVALLIN14.1', this.lineamiento);
    // this.ordenarLista(data, 'PGMFEVALLIN14.2', this.lineamiento2);
    // this.ordenarLista(data, 'PGMFEVALLIN14.3', this.lineamiento3);
    // this.ordenarLista(data, 'PGMFEVALLIN14.4', this.lineamiento4);
    this.ordenarLista(data, 'PGMFEVALLIN14', this.lineamiento5);
  }

  dataLineamientos(data: any) {
    // this.lineamiento = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN14.1' }
    // );
    // this.lineamiento2 = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN14.2' }
    // );
    // this.lineamiento3 = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN14.3' }
    // );
    // this.lineamiento4 = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN14.4' }
    // );
    this.lineamiento5 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN14' }
    );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }
}
