import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";

import { concatMap, finalize, tap } from "rxjs/operators";
import { MessageService } from "primeng/api";

import { ArchivoService, InformacionGeneralService, UsuarioService } from "@services";
import { AcceptType, ArchivoTipo, handlerWriteResult, PGMFArchivoSubTipo, PGMFArchivoTipo, ToastService } from "@shared";
import { InformacionGeneralDto, PGMFArchivoDto, ResponseModel } from "@models";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { forkJoin, Observable, of } from "rxjs";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { PlanManejoEvaluacionService } from "src/app/service/plan-manejo-evaluacion.service";
import { LineamientoSharedComponent } from "src/app/shared/components/lineamiento-shared/lineamiento-shared.component";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { Perfiles } from "src/app/model/util/Perfiles";
import { Mensajes } from "src/app/model/util/Mensajes";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { HttpParams } from "@angular/common/http";


@Component({
	selector: "app-tab-informacion-general",
	templateUrl: "./tab-informacion-general.component.html",
})
export class TabInformacionGeneralComponent implements OnInit {
	@ViewChild(LineamientoSharedComponent) lineamientoSharedComponent!: LineamientoSharedComponent;

	lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
	lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
	lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();

	@Output()
	public siguiente = new EventEmitter();
	@Output()
	public regresar = new EventEmitter();

	@Input() idPgmf: any;
	@Input() disabled!: boolean;
	@Input() codigoEstado!: string;

	@Input() isDisbledObjFormu!: boolean;
	@Input() isShowObjEval!: boolean;
	@Input() isDisabledObjEval!: boolean;
	@Input() isRequiredObsEval!: boolean; 



	accept = AcceptType.PDF_WORD;

	infoGeneral: InformacionGeneralDto = new InformacionGeneralDto();
	contrato: PGMFArchivoDto;

	minDate: Date = new Date();
	distrito: string = "";
	provincia: string = "";
	departamento: string = "";
	isDisableImput: boolean = false;
	isTitular: boolean = false;
	usuario!: UsuarioModel;
	perfil = Perfiles;
	codigo_PGMF_CONTRATO = 'PGMF01C'
	codigo_PGMF_CERTIFICADO = 'PGMF01CR'
	idPlanManejoArchivo: number = 0;
	idPlanManejo: number =0;
	archivoAdjuntoContrato: any = {};
	idPGMFArchivo: number = 0;
  disableSubir: boolean = false;

	comboContratos: any[] = [];

	constructor(
		private serv: PlanificacionService,
		private msgService: MessageService,
		private dialog: MatDialog,
		private api: InformacionGeneralService,
		private apiArchivo: ArchivoService,
		private user: UsuarioService,
		private planManejoEvaluacionService: PlanManejoEvaluacionService,
		private usuarioServ: UsuarioService,
		private toast: ToastService,
		private postulacionPFDMService: PostulacionPFDMService

	) {
		this.contrato = new PGMFArchivoDto({
			codigoTipoPGMF: PGMFArchivoTipo.PGMFIG,
			codigoSubTipoPGMF: PGMFArchivoSubTipo.CONTRATO,
			idPlanManejo: this.infoGeneral.idPlanManejo,
			tipoDocumento: ArchivoTipo.INFO_GENERAL
		});
	}

	ngOnInit(): void {
		this.minDate.setDate(this.minDate.getDate() - 365);
		this.idPlanManejo = this.idPgmf;

		this.usuario = this.usuarioServ.usuario;
		this.isTitular = this.usuario.sirperfil === this.perfil.TITULARTH ? true : false;

		this.isDisableImput = this.isTitular;
		this.obtenerInfoGeneral(this.idPgmf);
		this.listarComboContratos();

    this.listarArchivoPMFI()
	}

	listarComboContratos(){
		this.comboContratos = [
			{codigo: null, valorPrimario: '25-UCA-ATA/CON-PFDM-2018-001'},
			{codigo: 1, valorPrimario: '26-UCA-ATA/CON-PFDM-2018-002'},
			{codigo: 2, valorPrimario: '27-UCA-ATA/CON-PFDM-2019-005'},
		]
	}

	listarEvaluaciones(data: any) {
		this.ordenarLista(data, 'PGMFEVALLIN3.1', this.lineamiento);
		this.ordenarLista(data, 'PGMFEVALLIN3.2', this.lineamiento2);
		this.ordenarLista(data, 'PGMFEVALLIN3.3', this.lineamiento3);
	}

	dataLineamientos(data: any) {
		this.lineamiento = new LineamientoInnerModel(
			{ idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN3.1' }
		);
		this.lineamiento2 = new LineamientoInnerModel(
			{ idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN3.2' }
		);
		this.lineamiento3 = new LineamientoInnerModel(
			{ idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN3.3' }
		);
	}

	obtenerInfoGeneral(idPlanManejo: number) {
		this.api.obtener(idPlanManejo)
			.subscribe((result: any) => {

				if (result.success) {
					if (result.data != null) {

						this.infoGeneral = result.data;
						this.infoGeneral.idInformacionGeneral = result.data.idInformacionGeneral;

            this.infoGeneral.idPlanManejo=this.idPgmf;

						this.setContrato();
						this.getDistrito(result.data.distrito);
						this.getProvincia(result.data.provincia);
						this.getDepartamento(result.data.departamento);
					}
				}
			});
	};


	calculaAnios(){
   // debugger;
		var date1 = new Date(this.infoGeneral.fechaInicio);
		var date2 = new Date(this.infoGeneral.fechaFin);
/* 		let day:any;
		const date1utc = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
		const date2utc = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
		day = 1000*60*60*24;
		let days:any;
		let año=0;
		if(date1utc > 0 && date2utc > 0){

			days=(date2utc - date1utc)/day

			while(days >= 365){
				año++;
				days=days-365;
			}

			this.infoGeneral.duracion=año;

		} */
    this.infoGeneral.duracion = this.getDifferenceInDays(date1.getTime(),  date2.getTime());
	}

  private getDifferenceInDays(date1: any, date2 : any) : any {
    const diffInMs = Math.abs(date2 - date1);
    if (date1>date2) {
      return 0;
    }
    return  Math.floor((diffInMs / (1000 * 60 * 60 * 24))/365);
  }

	setContrato() {
		if (this.infoGeneral?.archivos) {
			const contrato = this.infoGeneral.archivos.find(x => x.codigoSubTipoPGMF == PGMFArchivoSubTipo.CONTRATO);
			this.contrato = contrato ? contrato : new PGMFArchivoDto({
				codigoTipoPGMF: PGMFArchivoTipo.PGMFIG,
				codigoSubTipoPGMF: PGMFArchivoSubTipo.CONTRATO,
				idPlanManejo: this.infoGeneral.idPlanManejo,
				tipoDocumento: ArchivoTipo.INFO_GENERAL
			});
			this.contrato.archivo = this.contrato.nombre;
		}
	}

	getDistrito(idDistrito: string) {
		this.serv.listarPorFilroDistrito({ idDistrito })
			.subscribe((result: any) => {
				if (result.isSuccess)
					console.log(result.data)
					//this.distrito = result.data[0].nombreDistrito;
			});
	}

	getProvincia(idProvincia: string) {
		this.serv
			.listarPorFilroProvincia({ idProvincia })
			.subscribe((result: any) => {
				if (result.isSuccess && result.data[0] != null)
					this.provincia = result.data[0].nombreProvincia;
			});
	}

	getDepartamento(idDepartamento: string) {
		this.serv
			.listarPorFiltroDepartamento_core_central({ idDepartamento })
			.subscribe((result: any) => {
				if (result.success && result.data[0] != null)
					this.departamento = result.data[0].nombreDepartamento;
			});
	}

	guardar() {

		this.infoGeneral.idUsuarioRegistro = this.user.idUsuario;

		this.dialog.open(LoadingComponent, { disableClose: true });
		forkJoin([this.guardarContrato(), this.guardarCertificado()])
			.pipe(concatMap(() => this.guardarInfoGeneral(this.infoGeneral)))
			.pipe(finalize(() => this.dialog.closeAll()))
			.subscribe();

      this.obtenerInfoGeneral(this.idPgmf);
	}

   validarCampo(param:string){
     let dat=null;
     if(param=="-"||param==null){
      return '';
     }
     else{
      return param;
     }
   }

	guardarInfoGeneral(body: InformacionGeneralDto) {


    body.tipoDocElaborador=body.tipoDocumentoTitularTH;
    body.dniElaborador= body.numeroDocumentoTitularTH;
    let nombreElaborador= this.validarCampo(body.nombreTitularTH)+this.validarCampo(body.razonSocialTitularTH);
    body.nombreElaborador=nombreElaborador;
    body.apellidoPaternoElaborador= this.validarCampo(body.apellidoPaternoTitularTH);
    body.apellidoMaternoElaborador= this.validarCampo(body.apellidoMaternoTitularTH);


    body.tipoDocRepresentante= body.tipoDocumentoRepLegal;
    body.docRepresentante= body.numeroDocumentoRepLegal;
    body.nombreRepresentante= body.nombreRepresentanteLegal;
    body.apellidoPaternoRepresentante= body.apellidoPaternoRepLegal;
    body.apellidoMaternoRepresentante= body.apellidoMaternoRepLegal;



		body.archivos = body.archivos.filter(x => x.archivo instanceof File)
		this.toast.ok("Se guardó la información del Plan General correctamente");
		return this.api.guardar(body)
			//.pipe(tap(handlerWriteResult(this.msgService).next));
	}

	guardarContrato(): Observable<ResponseModel<number>> {
		const contrato = this.infoGeneral.archivos.find(a => a.codigoSubTipoPGMF == PGMFArchivoSubTipo.CONTRATO);
		return this.guardarArchivos(contrato);
	}
	guardarCertificado(): Observable<ResponseModel<number>> {
		const certificado = this.infoGeneral.archivos.find(a => a.codigoSubTipoPGMF == PGMFArchivoSubTipo.REGENTE);
		return this.guardarArchivos(certificado);
	}
	guardarArchivos(item?: PGMFArchivoDto): Observable<ResponseModel<number>> {
		if (item?.archivo instanceof File) {
			return this.apiArchivo.cargar(this.user.idUsuario, ArchivoTipo.INFO_GENERAL, item.archivo)
				.pipe(tap(res => item.idArchivo = res?.data));
		}
		return of({ data: 0 });
	}

	changeContrato() {
		if (this.contrato.archivo instanceof File) {
			if (this.infoGeneral?.archivos) {
				this.infoGeneral.archivos = this.infoGeneral.archivos
					.filter(x => x.codigoSubTipoPGMF != PGMFArchivoSubTipo.CONTRATO);
				this.infoGeneral.archivos.push(this.contrato)
			} else {
				this.infoGeneral.archivos = [this.contrato]
			}
		}
	}
	cargarIdArchivo(data: any) {
		this.idPlanManejoArchivo = data.idPGMFArchivo;
	}

	registrarArchivoAdjunto(file: any) {
		this.dialog.open(LoadingComponent, { disableClose: true });

		this.apiArchivo.cargar(this.usuario.idusuario, this.codigo_PGMF_CONTRATO, file.file).subscribe(
		  (result) => {
			this.dialog.closeAll();
			if (result.success) {

			  this.archivoAdjuntoContrato.idArchivo = result.data;
			  this.registrarArchivo(result.data);
			} else {
			  this.toast.warn(result.message);
			  this.archivoAdjuntoContrato = {};
			  //this.limpiarFileRegFallo(tipo);
			}
		  },
		  () => {
			//this.limpiarFileRegFallo(tipo);
			this.dialog.closeAll();
			this.toast.error();
			this.archivoAdjuntoContrato = {};
		  }
		);
	}

	registrarArchivo(id: number) {
		var params = {
		  idPlanManejoArchivo: this.idPGMFArchivo,
		  codigoTipoPGMF: this.codigo_PGMF_CONTRATO,
		  codigoSubTipoPGMF: null,
		  descripcion: null,
		  observacion: null,
		  idPlanManejo: this.idPlanManejo,
		  idArchivo: id,
		  idUsuarioRegistro: this.user.idUsuario,
		};


		this.postulacionPFDMService
		  .registrarArchivoDetalle(params)
		  .subscribe((response: any) => {
			if (response.success == true) {
			   this.disableSubir = true;
				this.toast.ok('Se adjuntó el Contrato correctamente.');

			  //this.listarArchivoPMFI();
			} else {
			  this.toast.error(response?.message);
			}
		  });
		//this.deshabilita = false;
	}


	listarArchivoPMFI() {
		let load = this.dialog.open(LoadingComponent, { disableClose: true });

		this.postulacionPFDMService
			.obtenerArchivoDetalle(this.idPlanManejo, this.codigo_PGMF_CONTRATO)
			.pipe(finalize(() => load.close()))
			.subscribe((response: any) => {
				 if (response.data && response.data.length != 0) {
           this.disableSubir = true;
          this.archivoAdjuntoContrato = response.data[response.data.length-1];

				}
			});
	}

  eliminarArchivoGeneral() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    // this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
    this.postulacionPFDMService.eliminarArchivoDetalle(this.archivoAdjuntoContrato.idArchivo, this.user.idUsuario).subscribe((response: any) => {
      this.dialog.closeAll()
      if (response.success == true) {

          this.toast.ok("Se eliminó el Contrato correctamente.");
          this.disableSubir = false;
          this.archivoAdjuntoContrato = {}
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }

    },()=>{
      this.dialog.closeAll()
    });
  }



	siguienteTab() {
		this.siguiente.emit();
	}

	regresarTab() {
		this.regresar.emit();
	}

	private ordenarLista(lista: any[], codigo: string, object: any) {
		lista.forEach(element => {
			if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
				object.codigoTipo = element.codigoTipo;
				object.conforme = element.conforme;
				object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
				object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
				object.observacion = element.observacion;
				object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
			}
		});
	}
}
