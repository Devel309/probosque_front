import { Component, OnInit, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";

import { InformacionGeneralDto, PGMFArchivoDto, RegenteExternoModel, RegenteForestalModel } from "@models";
import { AcceptType, ArchivoTipo, PGMFArchivoSubTipo, PGMFArchivoTipo, ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { ArchivoService, UsuarioService } from "@services";
import { Perfiles } from "src/app/model/util/Perfiles";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";

@Component({
  selector: "regente-form",
  templateUrl: "./regente-form.html",
})
export class RegenteForm implements OnInit {

  private _infoGeneral!: InformacionGeneralDto;
  @Input()
  set infoGeneral(i: InformacionGeneralDto) {
    this._infoGeneral = i;
    this.regenteForestal = new RegenteForestalModel({
      idRegente: i.idRegenteForestal,
      idPersona: i.idRegenteForestal,
      certificadoHabilitacion: i.certificadoHabilitacionProfesionalRegenteForestal,
      contratoSuscrito: i.contratoSuscritoTitularTituloHabilitante,
      domicilioLegal: i.domicilioLegalRegente,
      numeroInscripcion: i.numeroInscripcionRegistroRegente
      
    });
    this.asignarRegente();
    this.setCertificado();
  }
  get infoGeneral() {
    return this._infoGeneral;
  }

  @Input() disabled!: boolean;
  @Input() isDisbledObjFormu!: boolean;
  @Input() idPgmf: any;

  regenteForestal: RegenteForestalModel = new RegenteForestalModel();
  documentoRegente: RegenteExternoModel = new RegenteExternoModel();

  regentesFiltrados: RegenteExternoModel[] = [];
  regentesForestales: RegenteExternoModel[] = [];

  certificado!: PGMFArchivoDto;

  archivoAdjuntoContrato: any = {};
  archivoAdjuntoCertificado: any = {};
  codigo_PGMF_CERTIFICADO = 'PGMF01CR'
  codigo_PGMF_CONTRATO = 'PGMF01C'
  idPlanManejo: number =0;
  idPGMFArchivo: number = 0;
  idPlanManejoArchivo: number = 0;
  isDisableImput: boolean = false;
	isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;
  disableSubirContrato: boolean = false;
  disableSubirCertificado: boolean = false;

  accept = AcceptType.PDF_WORD;

  constructor(
    private messageService: MessageService,
    private serv_forestal: ApiForestalService,
    private dialog: MatDialog,
    private toast: ToastService,
    private postulacionPFDMService: PostulacionPFDMService,
    private apiArchivo: ArchivoService,
    private usuarioServ: UsuarioService,
  ) {
  }

  ngOnInit(): void {

    this.usuario = this.usuarioServ.usuario;
		this.isTitular = this.usuario.sirperfil === this.perfil.TITULARTH ? true : false;

		this.isDisableImput = this.isTitular;
    this.idPlanManejo = this.idPgmf;
    this.listarArchivoPMFI(this.codigo_PGMF_CERTIFICADO);
    this.listarArchivoPMFI(this.codigo_PGMF_CONTRATO);
  }

  setCertificado() {
    if (this.infoGeneral?.archivos) {
      const certificado = this.infoGeneral.archivos.find(x => x.codigoSubTipoPGMF == PGMFArchivoSubTipo.REGENTE);
      this.certificado = certificado ? certificado : new PGMFArchivoDto({
        codigoTipoPGMF: PGMFArchivoTipo.PGMFIG,
        codigoSubTipoPGMF: PGMFArchivoSubTipo.REGENTE,
        idPlanManejo: this.infoGeneral.idPlanManejo,
        tipoDocumento: ArchivoTipo.INFO_GENERAL
      });
      this.certificado.archivo = this.certificado.nombre;
    }
  }

  asignarRegente() {
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv_forestal.consultarRegente()
      .pipe(finalize(() => load.close()))
      .subscribe({
        next: res => {

          this.regentesForestales = (res as any)?.dataService;
          const regenteForestal = this.regentesForestales
            .find(x => this.regenteForestal.idRegente == x.id);
          this.documentoRegente = new RegenteExternoModel({ ...regenteForestal });
        },
        error: _err => this.messageService.add({ severity: "warn", detail: _err?.message })
      });
  }

  filtrarRegente(value: string) {
    value = value.toLowerCase();
    this.regentesFiltrados = [];
    //console.log(this.regentesForestales);
    this.regentesFiltrados = this.regentesForestales.filter(
      r => (r.nombres.toLowerCase().includes(value) ||
        r.apellidos.toLowerCase().includes(value) ||
        r.numeroDocumento.includes(value))
    ).map(x => {
      //console.log(x)
      return new RegenteExternoModel({ ...x, nombresNroDocumento: `${x.nombres} ${x.apellidos} - ${x.numeroDocumento}` })
    });
  }

  clearRegente() {
    
    this.regenteForestal.idPersona = null;
    this.regenteForestal.idRegente = null;
    this.documentoRegente = new RegenteExternoModel();
    this.documentoRegente.apellidos = '';
    this.documentoRegente.nombres = '';
    this.infoGeneral.idRegenteForestal = null;
    this.infoGeneral.domicilioLegalRegente = "";
    this.infoGeneral.contratoSuscritoTitularTituloHabilitante = "";
    this.infoGeneral.numeroInscripcionRegistroRegente="";
    this.infoGeneral.certificadoHabilitacionProfesionalRegenteForestal="";
  }
  selectDocumentoRegente(value: RegenteExternoModel) {
    this.regenteForestal.idPersona = value.id;
    this.regenteForestal.idRegente = value.id;
    this.infoGeneral.idRegenteForestal = value.id;
  }
  cargarIdArchivo(data: any) {
		this.idPlanManejoArchivo = data.idPGMFArchivo;
	}

  registrarArchivoAdjunto(file: any ) {
		this.dialog.open(LoadingComponent, { disableClose: true });
	
		this.apiArchivo.cargar(this.usuario.idusuario, this.codigo_PGMF_CERTIFICADO, file.file).subscribe(
		  (result) => {
			this.dialog.closeAll();
			if (result.success) {
			  
			  this.archivoAdjuntoCertificado.idArchivo = result.data;
			  this.registrarArchivo(result.data,this.codigo_PGMF_CERTIFICADO);
			} else {
			  this.toast.warn(result.message);
			  this.archivoAdjuntoCertificado = {};
			  //this.limpiarFileRegFallo(tipo);
			}
		  },
		  () => {
			//this.limpiarFileRegFallo(tipo);
			this.dialog.closeAll();
			this.toast.error();
			this.archivoAdjuntoCertificado = {};
		  }
		);
	}

  registrarArchivoAdjuntoContrato(file: any ) {
		this.dialog.open(LoadingComponent, { disableClose: true });
	
		this.apiArchivo.cargar(this.usuario.idusuario, this.codigo_PGMF_CONTRATO, file.file).subscribe(
		  (result) => {
			this.dialog.closeAll();
			if (result.success) {
			  
			  this.archivoAdjuntoContrato.idArchivo = result.data;
			  this.registrarArchivo(result.data, this.codigo_PGMF_CONTRATO);
			} else {
			  this.toast.warn(result.message);
			  this.archivoAdjuntoContrato = {};
			  //this.limpiarFileRegFallo(tipo);
			}
		  },
		  () => {
			//this.limpiarFileRegFallo(tipo);
			this.dialog.closeAll();
			this.toast.error();
			this.archivoAdjuntoContrato = {};
		  }
		);
	}

  registrarArchivo(id: number, codigo_PGMF: string) {
		var params = {
		  idPlanManejoArchivo: this.idPGMFArchivo,
		  codigoTipoPGMF: codigo_PGMF,
		  codigoSubTipoPGMF: null,
		  descripcion: null,
		  observacion: null,
		  idPlanManejo: this.idPlanManejo,
		  idArchivo: id,
		  idUsuarioRegistro: this.usuarioServ.idUsuario,
		};
	
		
		this.postulacionPFDMService
		  .registrarArchivoDetalle(params)
		  .subscribe((response: any) => {
			if (response.success == true) {
          if (codigo_PGMF === this.codigo_PGMF_CERTIFICADO){
            this.disableSubirCertificado = true
          }else if (codigo_PGMF === this.codigo_PGMF_CONTRATO){
			      this.disableSubirContrato = true;
          }
				this.toast.ok('Se adjuntó el contrato suscrito con el Titular.');
        
			  //this.listarArchivoPMFI();
			} else {
			  this.toast.error(response?.message);
			}
		  });
		//this.deshabilita = false;
	}

  eliminarArchivoGeneral() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    // this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
    this.postulacionPFDMService.eliminarArchivoDetalle(this.archivoAdjuntoContrato.idArchivo, this.usuarioServ.idUsuario).subscribe((response: any) => {
      this.dialog.closeAll()
      if (response.success == true) {
        
          this.toast.ok("Se eliminó el Contrato Suscrito con el Titular.");
          this.disableSubirContrato = false;
          this.archivoAdjuntoContrato = {}
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }

    },()=>{
      this.dialog.closeAll()
    });
  }

  eliminarArchivoGeneralCertificado() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    // this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
    this.postulacionPFDMService.eliminarArchivoDetalle(this.archivoAdjuntoCertificado.idArchivo, this.usuarioServ.idUsuario).subscribe((response: any) => {
      this.dialog.closeAll()
      if (response.success == true) {
        
          this.toast.ok("Se eliminó el Certificado de Habilitación Profesional correctamente.");
          this.disableSubirCertificado = false;
          this.archivoAdjuntoCertificado = {}
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }

    },()=>{
      this.dialog.closeAll()
    });
  }

  listarArchivoPMFI(codigo_PGMF:string) {
		let load = this.dialog.open(LoadingComponent, { disableClose: true });

      if(codigo_PGMF === this.codigo_PGMF_CERTIFICADO){
        this.postulacionPFDMService
          .obtenerArchivoDetalle(this.idPlanManejo, codigo_PGMF)
          .pipe(finalize(() => load.close()))
          .subscribe((response: any) => {
            
            if (response.data && response.data.length != 0) {
              this.disableSubirCertificado = true;
              this.archivoAdjuntoCertificado = response.data[response.data.length-1];

          } 
        });
      } else if(codigo_PGMF === this.codigo_PGMF_CONTRATO){
        this.postulacionPFDMService
          .obtenerArchivoDetalle(this.idPlanManejo, codigo_PGMF)
          .pipe(finalize(() => load.close()))
          .subscribe((response: any) => {
            
            if (response.data && response.data.length != 0) {
              this.disableSubirContrato = true;
              this.archivoAdjuntoContrato = response.data[response.data.length-1];

          } 
        });



      }
	}



  changeCertificado() {
    if (this.certificado.archivo instanceof File) {
      if (this.infoGeneral?.archivos) {
        this.infoGeneral.archivos = this.infoGeneral.archivos
          .filter(x => x.codigoSubTipoPGMF != PGMFArchivoSubTipo.REGENTE);
        this.infoGeneral.archivos.push(this.certificado)
      } else {
        this.infoGeneral.archivos = [this.certificado]
      }
    }
  }
}
