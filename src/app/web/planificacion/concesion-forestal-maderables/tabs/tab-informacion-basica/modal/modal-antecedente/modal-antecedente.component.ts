import { HttpParams } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { DynamicDialogRef } from "primeng/dynamicdialog";
import { PlanificacionService } from "src/app/service/planificacion.service";

@Component({
  selector: "app-modal-antecedente",
  templateUrl: "./modal-antecedente.component.html",
})
export class ModalAntecedenteComponent implements OnInit {
  params_insetar_detalle: any = {};

  constructor(
    public ref: DynamicDialogRef,
  ) {}
  
  ngOnInit() {
  }

  agregar = () => {
    this.ref.close(this.params_insetar_detalle);
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }
}
