import { Component, OnInit } from "@angular/core";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
@Component({
  selector: "app-modal-caracterizacion",
  templateUrl: "./modal-caracterizacion.component.html",
  styleUrls:["./modal-caracterizacion.component.scss"]
})
export class ModalCaracterizacionComponent implements OnInit {
  params_insetar_detalle: any = {};
  isDisabled: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
  ) {}
  ngOnInit() {
    if(this.config.data.poblacionAledania != undefined){
      this.params_insetar_detalle.poblacionAledania = this.config.data.poblacionAledania;
      this.isDisabled = true;
    }
     
  }

  agregar = () => {
    this.ref.close(this.params_insetar_detalle);
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }
}
