import { HttpParams } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { DynamicDialogRef } from "primeng/dynamicdialog";
import { PlanificacionService } from "src/app/service/planificacion.service";

@Component({
  selector: "app-modal-infraestructura",
  templateUrl: "./modal-infraestructura.component.html",
})
export class ModalInfraestructuraComponent implements OnInit {
  params_insetar_detalle: any = {};

  constructor(
    public ref: DynamicDialogRef,
  ) {}
  ngOnInit() {
  }

  agregar = () => {
    this.ref.close(this.params_insetar_detalle);
  }
  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }
}
