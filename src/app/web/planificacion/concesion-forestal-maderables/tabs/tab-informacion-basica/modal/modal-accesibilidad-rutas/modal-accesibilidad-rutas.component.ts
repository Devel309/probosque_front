import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-accesibilidad-rutas",
  templateUrl: "./modal-accesibilidad-rutas.component.html",
})
export class ModalAccesibilidadRutasComponent implements OnInit {
  params_insetar_detalle: any = {};

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService,
  ) {}

  ngOnInit() {
    if (this.config.data.data != undefined) {
      this.params_insetar_detalle.referencia = this.config.data.data.referencia;
      this.params_insetar_detalle.coordenadaEsteIni = this.config.data.data.coordenadaEsteIni;
      this.params_insetar_detalle.coordenadaNorteIni = this.config.data.data.coordenadaNorteIni;
      this.params_insetar_detalle.coordenadaEsteFin = this.config.data.data.coordenadaEsteFin;
      this.params_insetar_detalle.coordenadaNorteFin = this.config.data.data.coordenadaNorteFin;
      this.params_insetar_detalle.distanciaKm = this.config.data.data.distanciaKm;
      this.params_insetar_detalle.tiempo = this.config.data.data.tiempo;
      this.params_insetar_detalle.medioTransporte = this.config.data.data.medioTransporte;
    }
  }

  agregar = () => {
    if(!this.validarAgregar()) return;
    this.ref.close(this.params_insetar_detalle);
  };

  validarAgregar(): boolean {
    let valido = true;
    if(!this.params_insetar_detalle.referencia) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar la Referencia.");
    }
    if(!this.params_insetar_detalle.coordenadaEsteIni) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar Punto de Inicio - Este.");
    }
    if(!this.params_insetar_detalle.coordenadaNorteIni) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar Punto de Inicio - Norte.");
    }
    if(!this.params_insetar_detalle.coordenadaEsteFin) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar Punto de Llegada - Este.");
    }
    if(!this.params_insetar_detalle.coordenadaNorteFin) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar Punto de Llegada - Norte.");
    }
    if(!this.params_insetar_detalle.distanciaKm) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar la Distancia.");
    }
    if(!this.params_insetar_detalle.tiempo) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar el Tiempo.");
    }
    if(!this.params_insetar_detalle.medioTransporte) {
      valido = false;
      this.toast.warn("(*) Debe de ingresar el Medio de Transporte.");
    }
    return valido;
  }

  cerrarModal() {
    this.ref.close()
  }
}
