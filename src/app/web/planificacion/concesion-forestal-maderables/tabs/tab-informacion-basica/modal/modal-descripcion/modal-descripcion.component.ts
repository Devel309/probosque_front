import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-modal-descripcion",
  templateUrl: "./modal-descripcion.component.html",
})
export class ModalDescripcionComponent implements OnInit {
  descripcion: string = "";
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toast: ToastService,
  ) {}

  ngOnInit() {
    if (this.config.data.item != undefined) {
      this.descripcion = this.config.data.item.descripcion;
    }
  }

  enviar = () => {
    if (!this.validarFechaCumplimiento()) {
      return;
    }
    else this.ref.close(this.descripcion);
  };

  cerrarModal() {
    if (this.ref) {
        this.ref.close();
    }
  }

  validarFechaCumplimiento(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";
    if (
     this.descripcion == null ||
     this.descripcion == ""
    ) {
      validar = false;
      mensaje = mensaje += this.config.data?.mensajeValid;
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }
  ErrorMensaje(mensaje: any) {
    this.toast.warn(mensaje);
  }
}
