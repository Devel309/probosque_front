import { Component, OnInit } from "@angular/core";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
@Component({
  selector: "app-modal-conflicto",
  templateUrl: "./modal-conflicto.component.html",
})
export class ModalConflictoComponent implements OnInit {
  params_insetar_detalle: any = {};
  isDisabled: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
  ) {}
  ngOnInit() {}

  agregar = () => {
    this.ref.close(this.params_insetar_detalle);
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }
}
