import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { DynamicDialogRef } from 'primeng/dynamicdialog';

import { HidrografiaTipo } from '@shared';
import { InformacionBasicaService } from "src/app/service/planificacion/plan-operativo-concesiones-maderables/informacion-basica.service";
import { HidrografiaModel } from '@models';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-modal-aspecto-fisico',
  styleUrls: ['./modal-aspecto-fisico.scss'],
  templateUrl: './modal-aspecto-fisico.component.html',
})
export class ModalAspectoFisicoComponent implements OnInit {


  HidrografiaTipo = HidrografiaTipo;

  rio = new FormControl(null, [Validators.required, Validators.minLength(2)]);
  quebrada = new FormControl(null, [Validators.required, Validators.minLength(2)]);
  laguna = new FormControl(null, [Validators.required, Validators.minLength(2)]);

  rios: HidrografiaModel[] = [];
  quebradas: HidrografiaModel[] = [];
  lagunas: HidrografiaModel[] = [];

  buscandoRios: boolean = false;
  buscandoQuebradas: boolean = false;
  buscandoLagunas: boolean = false;

  constructor(public ref: DynamicDialogRef, private api: InformacionBasicaService) { }

  ngOnInit() { }

  agregar() {
    this.ref.close();
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }

  clearData() {
    this.rios = [];
    this.quebradas = [];
    this.lagunas = [];
  }

  buscarHidrografia(tipo: HidrografiaTipo, value: string) {

    this.clearData();

    this.buscandoRios = tipo == HidrografiaTipo.RIO;
    this.buscandoQuebradas = tipo == HidrografiaTipo.QUEBRADA;
    this.buscandoLagunas = tipo == HidrografiaTipo.LAGUNA;

    this.api.obtenerHidrografia(0, tipo, value)
      .pipe(finalize(() => {
        this.buscandoRios = false;
        this.buscandoQuebradas = false;
        this.buscandoLagunas = false;
      }))
      .subscribe({
        next: res => this.filtrartipoHidrología(tipo, res?.data),
        error: _err => console.log(_err?.message)
      })
  }

  filtrartipoHidrología(tipo: HidrografiaTipo, data?: HidrografiaModel[]) {
    if (data == null || data == undefined) return;

    switch (tipo) {
      case HidrografiaTipo.RIO:
        this.rios = data;
        break;
      case HidrografiaTipo.QUEBRADA:
        this.quebradas = data;
        break;
      case HidrografiaTipo.LAGUNA:
        this.lagunas = data;
        break;
      default:
        break;
    }
  }

  validarRegistro() {

  }
}
