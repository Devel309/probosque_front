import { Component, OnInit } from '@angular/core';
import { CoreCentralService } from '@services';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { PlanificacionService } from 'src/app/service/planificacion.service';

@Component({
  selector: 'app-modal-ubicacion',
  templateUrl: './modal-ubicacion.component.html',
})
export class ModalUbicacionComponent implements OnInit {
  params_insetar_detalle: any = {};
  departamentos: any = [];
  provincias: any = [];
  distritos: any = [];
  cuencas: any = [];

  isEdit: boolean = false;

  constructor(
    private planificacionService: PlanificacionService,
    private coreCentralService: CoreCentralService,
    public ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private messageService: MessageService,
  ) {
    this.isEdit = this.config.data?.isEdit;
  }

  ngOnInit() {
    if(this.isEdit) {
      this.params_insetar_detalle = Object.assign({}, this.config.data.data);
    } else {
      let _departamentoSelect = {codDepartamento: '', nombreDepartamento: ''};
      let _provinciaSelect = {codProvincia: '', nombreProvincia: ''};
      let _distritoSelect = {codDistrito: '', nombreDistrito: ''};
      this.params_insetar_detalle = {departamentoSelect: _departamentoSelect, provinciaSelect: _provinciaSelect, distritoSelect: _distritoSelect};
    }

    this.listarCuencas();
    this.listarPorFiltroDepartamento();
  }

  listarPorFiltroDepartamento() {
    this.departamentos = [];
    this.provincias = [];
    this.distritos = [];
    this.planificacionService.listarPorFiltroDepartamento_core_central({}).subscribe((result: any) => {
        if (result.success) {
          this.departamentos = result.data.map((x: any)=> {
            return {codDepartamento: x.codDepartamento, nombreDepartamento: x.nombreDepartamento};
          });
          if(this.isEdit) {
            this.listarPorFilroProvincia(false);
          }
        }
      });
  };

  listarPorFilroProvincia(ischange: boolean) {
    this.provincias = [];
    this.distritos = [];
    const params = {codDepartamento: this.params_insetar_detalle.departamentoSelect.codDepartamento};
    this.planificacionService.listarPorFilroProvincia(params).subscribe((result: any) => {
        if (result.success) {
          this.provincias = result.data.map((x: any)=> {
            return {codProvincia: x.codProvincia, nombreProvincia: x.nombreProvincia};
          });
          if(this.isEdit && !ischange) {
            this.listarPorFilroDistrito(ischange);
          }
        }
      });
  };

  listarPorFilroDistrito = (ischange: boolean) => {
    this.distritos = [];
    const params = {codProvincia: this.params_insetar_detalle.provinciaSelect.codProvincia};
    this.planificacionService.listarPorFilroDistrito(params).subscribe((result: any) => {
        if (result.success) {
          this.distritos = result.data.map((x: any)=> {
            return {codDistrito: x.codDistrito, nombreDistrito: x.nombreDistrito};
          });
        }
      });
  };

  listarCuencas = () => {
    this.cuencas=[];
    this.coreCentralService.listarComboCuenca().subscribe((result: any) => {
        if (result.success) {
          this.cuencas = result.data.map((x: any)=> {
            return {codigo: x.codigo, valor: x.valor};
          });
        }
      });
  };

  selectDPD = (obj: any, origen: string) => {
    if (origen == 'd') {
      this.params_insetar_detalle.departamentoSelect = obj;
      this.params_insetar_detalle.provinciaSelect.codProvincia = "";
      this.params_insetar_detalle.provinciaSelect.nombreProvincia = "";
      this.params_insetar_detalle.distritoSelect.codDistrito = "";
      this.params_insetar_detalle.distritoSelect.nombreDistrito = "";
      this.listarPorFilroProvincia(true);
    }
    if (origen == 'p') {
      this.params_insetar_detalle.provinciaSelect = obj;
      this.params_insetar_detalle.distritoSelect.codDistrito = "";
      this.params_insetar_detalle.distritoSelect.nombreDistrito = "";
      this.listarPorFilroDistrito(true);
    }
    if (origen == 'dd') {
      this.params_insetar_detalle.distritoSelect = obj;
    }
  };

  agregar = () => {
    if (this.validarDatos()) {
      this.ref.close(this.params_insetar_detalle);
    }
  }

  private  validarDatos() : boolean{
    let b : boolean = true;
    if (!this.params_insetar_detalle.departamentoSelect.codDepartamento) {
      this.messageService.add({
        key: "toast",
        severity: "warn",
        summary: "",
        detail: "(*) Debe seleccionar un Departamento.",
      });
      b = false;
      return b;
    }
    if (!this.params_insetar_detalle.provinciaSelect.codProvincia) {
    this.messageService.add({
      key: "toast",
      severity: "warn",
      summary: "",
      detail: "(*) Debe seleccionar una Provincia.",
    });
    b = false;
    return b;
    }

    if (!this.params_insetar_detalle.distritoSelect.codDistrito) {
    this.messageService.add({
      key: "toast",
      severity: "warn",
      summary: "",
      detail: "(*) Debe seleccionar un Distrito.",
    });
      b = false;
      return b;
    }

    if (this.params_insetar_detalle.cuencaSelect===null || this.params_insetar_detalle.cuencaSelect===undefined ||
      this.params_insetar_detalle.cuencaSelect.codigo===null || this.params_insetar_detalle.cuencaSelect.codigo===undefined) {
    this.messageService.add({
      key: "toast",
      severity: "warn",
      summary: "",
      detail: "Seleccione la cuenca",
    });
      b = false;
      return b;
    }

    return b;
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close(undefined);
    }
  }
}
