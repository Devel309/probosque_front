import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-tipo-bosque',
  templateUrl: './modal-tipo-bosque.component.html',
})
export class ModalTipoBosqueComponent implements OnInit {
  params_insetar_detalle: any = {};
  listaTipoBosque: any[] = [];

  visible: boolean = false;

  constructor(public ref: DynamicDialogRef) {}

  ngOnInit() {
    this.listarFaunas();
  }

  listarFaunas = () => {
    this.listaTipoBosque.push(
      {
        idBosque: 1,
        descripcion: 'Bosque 1',
      },
      {
        idBosque: 2,
        descripcion: 'Bosque 2',
      }
    );
  };

  seleccionarFauna = () => {
    this.visible = !this.visible;
    this.params_insetar_detalle.tipoBosque = this.listaTipoBosque.find(
      (x) => x.idBosque === this.params_insetar_detalle.idBosque
    ).descripcion;
  };

  agregar = () => {
    this.ref.close(this.params_insetar_detalle);
  }
  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }
}
