import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoPGMF } from "src/app/model/CodigoPGMF";
import { EspeciesFauna } from "src/app/model/medioTrasporte";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { Mensajes } from "src/app/model/util/Mensajes";
import { InformacionBasicaService } from "src/app/service/informacion-basica.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { AccesibilidadVias } from "src/app/web/planificacion/plan-general-manejo/tabs/tab-informacion-area-manejo/components/tabla-hidrografia/models";

@Component({
  selector: "app-modal-fauna",
  templateUrl: "./modal-fauna.component.html",
})
export class ModalFaunaComponent implements OnInit {
  queryFauna: string = "";
  comboListEspeciesFauna: EspeciesFauna[] = [];
  optionPage = { pageNum: 1, pageSize: 10, };
  totalRecords: any = 0;
  listaFauna: any[] = [];
  selectedValues: AccesibilidadVias[] = [];
  valorRespModal: boolean = false;

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private informacionBasicaService: InformacionBasicaService,
  ) { }

  ngOnInit() {
    this.listaPorFiltroEspecieFauna();
  }

  listaPorFiltroEspecieFauna() {
    let params = {
      idPlanManejo: this.config.data.idPlanManejo,
      codInfBasica: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      codSubInfBasicaDet: CodigoPGMF.TAB_3,
      codNombreInfBasica: CodigoPGMF.ACORDEON_3_4_TAB_1,
      idEspecie: null,
      nombreComun: null,
      autor: null,
      familia: null,
      nombreCientifico: this.queryFauna || null,
      pageNum: this.optionPage.pageNum,
      pageSize: this.optionPage.pageSize,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaService.listarFaunaInformacionBasicaDetalle(params).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.totalRecords = result.totalRecord;
        this.comboListEspeciesFauna = result.data.map((item: any) => {
          return {
            ...item,
            check: item.idInfBasicaDet ? true : false,
            idInfBasica: this.config.data.idInfBasica
          }
        });
      } else {
        this.totalRecords = 0;
        this.comboListEspeciesFauna = [];
        this.toast.warn("No se encontraron coincidencias.");
      }
    }, () => {
      this.totalRecords = 0;
      this.comboListEspeciesFauna = [];
      this.dialog.closeAll();
      this.toast.warn("No se encontraron coincidencias.");
    });
  }

  guardarAspectosBiologicos(lista: any) {
    const params = [{
      idInfBasica: this.config.data.idInfBasica,
      codInfBasica: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      codSubInfBasica: CodigoPGMF.TAB_3,
      codNombreInfBasica: CodigoPGMF.ACORDEON_3_4_TAB_1,
      idPlanManejo: this.config.data.idPlanManejo,
      idUsuarioRegistro: this.config.data.idUser,
      listInformacionBasicaDet: lista,
    }];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService.registrarInformacionBasica(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success) {
        this.toast.ok("Se registró los datos de fauna silvestre correctamente.");
        this.cerrarModal(true);
      } else {
        this.toast.warn(response?.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnAceptar() {
    let listaEnviar = this.comboListEspeciesFauna.filter((item: any) => item.check);
    if (listaEnviar.length === 0) {
      this.toast.warn("(*) Debe Selecionar registros en la tabla.");
      return;
    }
    this.guardarAspectosBiologicos(listaEnviar);
  }

  btnBuscar() {
    this.optionPage = { pageNum: 1, pageSize: 10, };
    this.listaPorFiltroEspecieFauna();
  }

  btnCancelar() {
    this.cerrarModal(this.valorRespModal);
  }

  onChangeFauna(event: any, data: any) {
    if (event.checked === false && data.idInfBasicaDet !== 0) {
      let params = {
        idInfBasica: 0,
        idInfBasicaDet: data.idInfBasicaDet,
        codInfBasicaDet: "",//"PGMFAFAU",
        idUsuarioElimina: this.config.data.idUser,
      };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionAreaPmfiService.eliminarInformacionBasica(params).subscribe((response: any) => {
        this.dialog.closeAll();
        data.idInfBasicaDet = 0;
        this.valorRespModal = true;
      }, (error) => this.errorMensaje(error, true));
    }
  }

  loadData(e: any) {
    this.optionPage = { pageNum: e.first + 1, pageSize: e.rows, };
    this.listaPorFiltroEspecieFauna();
  }

  cerrarModal(valor?: any) {
    this.ref.close(valor);
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
