import { HttpParams } from "@angular/common/http";
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, PlanManejoService, UsuarioService } from '@services';
import { ToastService } from "@shared";
import { ConfirmationService, MessageService, SelectItem } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { Table } from "primeng/table";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoPGMF } from "src/app/model/CodigoPGMF";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { DepartamentoModel } from "src/app/model/Departamento";
import { DistritoModel } from "src/app/model/Distrito";
import { InformacionBasicaDetalle } from "src/app/model/InformacionAreaManejo";
import { Rios } from "src/app/model/medioTrasporte";
import { InformacionBasicaDetalleModel } from "src/app/model/PlanManejo/InformacionBasica/InformacionBasicaDetalleModel";
import { InformacionBasicaModel } from "src/app/model/PlanManejo/InformacionBasica/InformacionBasicaModel";
import { ProvinciaModel } from "src/app/model/Provincia";
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { SolicitudModel } from "src/app/model/Solicitud";
import { UnidadFisiograficaModel } from "src/app/model/UnidadFisiografica";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { FileModel } from "src/app/model/util/File";
import { Mensajes } from "src/app/model/util/Mensajes";
import { Perfiles } from "src/app/model/util/Perfiles";
import { TiposArchivos } from "src/app/model/util/TiposArchivos";
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { CoreCentralService } from "src/app/service/coreCentral.service";
import { PlanificacionService } from "src/app/service/planificacion.service";

import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { InformacionBasicaService } from "src/app/service/planificacion/plan-operativo-concesiones-maderables/informacion-basica.service";
import { UnidadFisiograficaService } from "src/app/service/planificacion/plan-operativo-concesiones-maderables/unidad-fisiografica.service";
import { PlanManejoCcnnService } from "src/app/service/planManejoCcnn.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";

import { MapCustomComponent } from "src/app/shared/components/map-custom/map-custom.component";
import { MapApi } from "src/app/shared/mapApi";
import { ActividadesComponent } from "./actividades/actividades.component";
import { ModalAccesibilidadRutasComponent } from "./modal/modal-accesibilidad-rutas/modal-accesibilidad-rutas.component";
import { ModalAntecedenteComponent } from "./modal/modal-antecedente/modal-antecedente.component";
import { ModalCaracterizacionComponent } from "./modal/modal-caracterizacion/modal-caracterizacion.component";
import { ModalDescripcionComponent } from "./modal/modal-descripcion/modal-descripcion.component";
import { ModalFaunaComponent } from "./modal/modal-fauna/modal-fauna.component";
import { ModalInfraestructuraComponent } from "./modal/modal-infraestructura/modal-infraestructura.component";
import { ModalUbicacionComponent } from "./modal/modal-ubicacion/modal-ubicacion.component";

@Component({
  selector: "app-tab-informacion-basica",
  templateUrl: "./tab-informacion-basica.component.html",
  providers: [
    ConfirmationService,
  ]
})
export class TanInformacionBasicaComponent implements OnInit {
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @ViewChild(MapCustomComponent) map!: MapCustomComponent;
  @ViewChild("ulResult", { static: true }) private ulResult!: ElementRef;
  @ViewChild("fileInput", { static: true }) private fileInput!: ElementRef;
  @ViewChild("btnAdjuntarGDB", { static: true })
  private btnAdjuntarGDB!: ElementRef;
  @ViewChild("tblAreaManejoDividido", { static: true })
  private tblAreaManejoDividido!: ElementRef;
  @ViewChild("tblTipoBosque", { static: true }) private tblTipoBosque!: Table;
  @ViewChild("tblUnidadFisiografica", { static: false })
  private tblUnidadFisiografica!: Table;
  usuario: number = 0;
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento4: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento5: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento6: LineamientoInnerModel = new LineamientoInnerModel();
  cultivos: any;
  extraccion: any;

  cultivosCaserio: any;
  extraccionCaserio: any;

  cultivosCentrosPoblados: any;
  extraccionCentrosPoblados: any;

  cultivosColonos: any;
  extraccionColonos: any;

  comunidadNativaNombre: string = "";
  comunidadNativaUbicacion: string = "";
  comunidadNativaArea: string = "";
  comunidadNativaFamilia: string = "";

  caserioNombre: string = "";
  caserioUbicacion: string = "";
  caserioArea: string = "";
  caserioFamilia: string = "";

  centrosPobladosNombre: string = "";
  centrosPobladosUbicacion: string = "";
  centrosPobladosArea: string = "";
  centrosPobladosFamilia: string = "";

  colonosNombre: string = "";
  colonosUbicacion: string = "";
  colonosArea: string = "";
  colonosFamilia: string = "";

  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();

  @Input("dataBase") dataBase: any = {};
  @Input("enabledControlInfBasica") enabledControlInfBasica: any = {};
  @Input('accion') accion: string | null = '';

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  public _files: any[] = [];
  public _filesUF: any[] = [];
  public _fileGDB: any = [];
  public view: any = null;
  public graphicsLayerDepartment: any = null;
  public graphicsLayerProvince: any = null;
  public graphicsLayerDistrict: any = null;
  public graphicsLayer: any = null;
  public geoJsonLayerTipoBosque: any = null;
  public _id = this.mapApi.Guid2.newGuid;
  totalArea: string = "";
  totalPorcentaje: string = "";

  public filFile: any = null;
  public titleVertice = "VERTICES vert_pgmf";
  filFiles: FileModel[] = [];
  archivoGDB: FileModel = {} as FileModel;
  archivoTrack: FileModel = {} as FileModel;
  filesAccesibilidad: FileModel[] = [];
  listUnidadGeografica: UnidadFisiograficaModel[] = [];
  listVertices: any = [];
  //  department: SelectItem[];
  departmentSelect!: SelectItem;
  //  province: SelectItem[];
  provinceSelect!: SelectItem;
  // district: SelectItem[];
  districtSelect!: SelectItem;

  city: string = "";
  autoResize: boolean = true;

  listaUbicacion: any[] = [];
  ref!: DynamicDialogRef;

  listaCoordenadas: any[] = [];
  listaRutas: any[] = [];
  data1: any[] = [];
  principalesLineamientos: any[] = [];
  informacionTipos: any[] = [];

  listaAspectoFisico: any[] = [];
  existeCatastroAspectoFisico: boolean = false;

  listaFisiograficas: any[] = [];

  listaFauna: any[] = [];
  listaBosque: any[] = [];
  listaPoblacion: any[] = [];
  subListaPoblacion: any[] = [];
  listCoordinatesAnexo1: any = [];
  idPlanManejo: number = this.idPGMF;

  /****************************************/
  editarCampo: boolean = false;

  codigoProceso = CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL;
  codigoTab: string = "PGMFIBAM";
  listInfoBasicaDet_3_5_1: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_3_5_2: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_3_6_1: InformacionBasicaDetalleModel[] = [];
  codigoAcordeonAspectosSocioeconomico: string = "PGMFIBAMAS"
  codigoAcordeonAntecedentesUsoIdentificacionConflictos: string = "PGMFAUIC"
  codigoAcordeonUbicacionExtension: string = "PGMFIBAMUE"
  codigoTablaCabecera = 'TAB_CAB';
  archivoAdjuntoContrato: any = {};
  archivoAdjuntoAnexo_3_6_1: any = {};

  disableSubir: boolean = false;
  disableSubir_3_6_1: boolean = false;
  lstTipoAcceso: any = [{ valor1: 'Fluvial Todo el año' }, { valor1: 'Fluvial Limitado' },
  { valor1: 'Terrestre Todo el año' }, { valor1: 'Terrestre Limitado' }];
  infoBasica_3_5: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPGMF,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAspectosSocioeconomico,
    idUsuarioRegistro: this.user.idUsuario,
  });
  infoBasica_3_6: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPGMF,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAntecedentesUsoIdentificacionConflictos,
    idUsuarioRegistro: this.user.idUsuario,
  });

  infoBasicaDet_3_5_cab: InformacionBasicaDetalleModel =
    new InformacionBasicaDetalleModel({
      idInfBasicaDet: 0,
      codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
      codSubInfBasicaDet: this.codigoTablaCabecera,
      idFlora: 0,
      idFauna: 0,
      ampliarAnexo: null,
      justificacion: null,
      idUsuarioRegistro: this.user.idUsuario,
    });

  infoBasicaDet_3_6_cab: InformacionBasicaDetalleModel =
    new InformacionBasicaDetalleModel({
      idInfBasicaDet: 0,
      codInfBasicaDet: this.codigoAcordeonAntecedentesUsoIdentificacionConflictos,
      codSubInfBasicaDet: this.codigoTablaCabecera,
      idFlora: 0,
      idFauna: 0,
      ampliarAnexo: null,
      justificacion: null,
      idUsuarioRegistro: this.user.idUsuario,
    });


  isArffs: boolean = false;
  isTitular: boolean = false;
  usuarioM = {} as UsuarioModel;
  perfil = Perfiles;

  /****************************************/

  caracterizacion: any = [];
  listInformacionBasicaDet: any = [];

  departamento = {} as DepartamentoModel;
  listDepartamento: DepartamentoModel[] = [];
  provincia = {} as ProvinciaModel;
  listProvincia: ProvinciaModel[] = [];
  distrito = {} as DistritoModel;
  listDistrito: DistritoModel[] = [];
  listHidroFisio: any[] = [];

  infraestructura = [];
  antecedentes: any = [];

  comunidadNativa: any[] = [];
  caserio: any[] = [];
  centrosPoblados: any[] = [];
  colonos: any[] = [];

  conflictos: any[] = [];

  isLoadInt: boolean = true;
  cmbRios: Rios[] = [];
  idInfBasica_3_3_1: number = 0;
  idInfBasica_3_3_3: number = 0;
  idInfBasica_3_3_4: number = 0;
  idInfBasica_3_4_1: number = 0;
  idInfBasica_3_4_2: number = 0;
  idInfBasica_3_6_2: number = 0;

  infoBasica_3_1: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: undefined,
    idPlanManejo: this.idPGMF,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonUbicacionExtension,
    idUsuarioRegistro: this.user.idUsuario,
  });

  cuencas: any = [];

  estadoInputArea: boolean = true;
  manejoConflictos: any = [];
  totalAreaTipoBosque!:string;
  totalPorcentaTipoBosque!:string;

  fileAnexo4_3_2: any = {};
  fileAnexo4_3_3: any = {};
  fileAnexo4_3_4: any = {};
  fileAnexo4_4_1: any = {};
  fileAnexo4_4_2: any = {};
  fileAnexo4_5_2: any = {};
  fileAnexo4_6_1: any = {};
  fileAnexo4_6_2: any = {};

  constructor(
    private usuarioServ: UsuarioService,
    public dialogService: DialogService,
    private messageService: MessageService,
    private serviceGeoforestal: ApiGeoforestalService,
    private servCoreCentral: CoreCentralService,
    private servPlanManejoCcnn: PlanManejoCcnnService,
    private planificacionService: PlanificacionService,
    private informacionBasicaService: InformacionBasicaService,
    private confirmationService: ConfirmationService,
    private serviceUnidFisiografica: UnidadFisiograficaService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private coreCentralService: CoreCentralService,
    private planManejoService: PlanManejoService,

  ) {
    this.listaFisiograficas = [
      {
        unidad: "LLanura aluvial inundable permanente",
        area: "",
        porcentaje: "",
        id: 1,
      },
      {
        unidad: "LLanura aluvial inundable temporalmente",
        area: "",
        porcentaje: "",
        id: 2,
      },
      { unidad: "Terrazas inundables", area: "", porcentaje: "", id: 3 },
      { unidad: "Terrazas no inundables", area: "", porcentaje: "", id: 4 },
      { unidad: "Colinas bajas", area: "", porcentaje: "", id: 5 },
      { unidad: "Colinas Medias", area: "", porcentaje: "", id: 6 },
      { unidad: "Colinas Altas", area: "", porcentaje: "", id: 7 },
    ];
  }

  ngOnInit() {
    this.idPlanManejo = this.idPGMF
    this.usuarioM = this.usuarioServ.usuario;
    this.usuario = this.user.idUsuario;
    this.isTitular = this.usuarioM.sirperfil === this.perfil.TITULARTH ? true : false;
    this.isArffs = this.usuarioM.sirperfil === this.perfil.AUTORIDAD_REGIONAL ? true : false;

    this.archivoGDB.descripcion = 'GDB';
    this.archivoGDB.inServer = false;
    this.archivoTrack.descripcion = 'SHP';
    this.archivoTrack.inServer = false;

    this.listarCuencas(); //listar 3.1.1
    this.listarCoordenadas(); // listar 3.1.2
    this.obtenerSolicitudAprovechamientoCcnn();
    this.listaCracteizacion();
    this.listarUnidadFisiografica();
    this.obtenerArchivos();
    this.obtenerFaunas();
    this.listRios();
    this.listarHidrografia();
    this.listarPrincipalesLimitaciones();
    this.listarInformaciontipoSuelo();

    this.listarConflicto();
  }

  showAnexoAcor2: boolean = false;
  showAnexoAcor3: boolean = false;
  showAnexoAcor4: boolean = false;
  showAnexoAcor5: boolean = false;
  showAnexoAcor6: boolean = false;
  onTabOpen = (event: any) => {
    if (event.index === 1) {
      this.listarAccesibilidad();
      this.listarArchivoGDB();
      this.showAnexoAcor2 = true;
    } else if (event.index === 2) {
      this.showAnexoAcor3 = true;
    } else if (event.index === 3) {
      this.showAnexoAcor4 = true;
      this.listarAspectoBioFaunaSilvestre();
      this.listarAspectoBioTiobosque();
    } else if (event.index === 4) {
      this.showAnexoAcor5 = true;
      this.listarAspectosSocieconomicos();
      this.listarArchivoPMFI()
    } else if (event.index === 5) {
      this.showAnexoAcor6 = true;
      this.listarAntecedentesUsoIdentificacion()
      this.listarArchivoPMFI_3_6_1()
    }
  };

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN5.1', this.lineamiento);
    this.ordenarLista(data, 'PGMFEVALLIN5.2', this.lineamiento2);
    this.ordenarLista(data, 'PGMFEVALLIN5.3', this.lineamiento3);
    this.ordenarLista(data, 'PGMFEVALLIN5.4', this.lineamiento4);
    this.ordenarLista(data, 'PGMFEVALLIN5.5', this.lineamiento5);
    this.ordenarLista(data, 'PGMFEVALLIN5.6', this.lineamiento6);
  }

  dataLineamientos(data: any) {
    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN5.1' }
    );
    this.lineamiento2 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN5.2' }
    );
    this.lineamiento3 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN5.3' }
    );
    this.lineamiento4 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN5.4' }
    );
    this.lineamiento5 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN5.5' }
    );
    this.lineamiento6 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN5.6' }
    );
  }

  onChangeDepartment(id: any) {
    if (this.graphicsLayerDepartment !== null)
      this.graphicsLayerDepartment.removeAll();
    if (this.graphicsLayerProvince !== null)
      this.graphicsLayerProvince.removeAll();
    if (this.graphicsLayerDistrict !== null)
      this.graphicsLayerDistrict.removeAll();
    if (id !== null) {
      this.searchDepartment(id);
    }
  }
  onChangeProvince(id: any) {
    if (this.graphicsLayerProvince !== null)
      this.graphicsLayerProvince.removeAll();
    if (this.graphicsLayerDistrict !== null)
      this.graphicsLayerDistrict.removeAll();
    if (id !== null) {
      this.searchProvince(id);
    }
  }
  onChangeDistrict(id: any) {
    if (id !== null) {
      this.searchDistrict(id);
    }
    if (this.graphicsLayerDistrict !== null)
      this.graphicsLayerDistrict.removeAll();
  }
  searchDepartment(id: any) {
    let options = {
      layerId: "14",
      where: `codigo=${id}`,
    };

    if (this.graphicsLayerDepartment !== null)
      this.graphicsLayerDepartment.removeAll();
    this.graphicsLayerDepartment = this.mapApi.graphicsLayer();
    this.view.map.add(this.graphicsLayerDepartment);
    this.mapApi.getDatosbyAGS(options).then((data: any) => {
      const fillSymbol = {
        type: "simple-fill",
        color: [227, 139, 79, 0.4],
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        t.geometry.type = "polygon";
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.properties;
        this.graphicsLayerDepartment.add(feature);
      });
    });
  }
  searchProvince(id: any) {
    let options = {
      layerId: "13",
      where: `codigo=${id}`,
    };

    this.graphicsLayerProvince = this.mapApi.graphicsLayer();
    this.view.map.add(this.graphicsLayerProvince);
    this.mapApi.getDatosbyAGS(options).then((data: any) => {
      const fillSymbol = {
        type: "simple-fill",
        color: [227, 139, 79, 0.4],
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        t.geometry.type = "polygon";
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.properties;
        this.graphicsLayerProvince.add(feature);
      });
    });
  }
  searchDistrict(id: any) {
    let options = {
      layerId: "12",
      where: `codigo=${id}`,
    };
    this.graphicsLayerDistrict = this.mapApi.graphicsLayer();
    this.view.map.add(this.graphicsLayerDistrict);
    this.mapApi.getDatosbyAGS(options).then((data: any) => {
      const fillSymbol = {
        type: "simple-fill",
        color: [227, 139, 79, 0.4],
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        t.geometry.type = "polygon";
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.properties;
        this.graphicsLayerDistrict.add(feature);
        /* this.graphicsLayerDistrict.when(()=>{
          this.view.goTo(this.graphicsLayerDistrict.graphics);
        }) */
      });
    });
  }
  searchRiosPrincipales(geometry: any) {
    let options = {
      url:
        "https://geoservidorperu.minam.gob.pe/arcgis/rest/services/ServicioBase/MapServer",
      layerId: "8",
      where: `OBJECTID=${1}`,
      SRID: 4326,
      SRIDOutput: 4326,
      geometry: geometry,
    };
    this.mapApi.getDatosbyAGSIntersect(options).then((data: any) => {
      let geoJson: any = {};
      geoJson.title = "Rios Principales";
      geoJson.opacity = 0.8;
      geoJson.color = "#1e90ff";
      geoJson.file = null;
      geoJson.service = true;
      geoJson.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      let features = [];
      features.push({
        properties: data.features[0].attributes,
        geometry: {
          type: "MultiLineString",
          coordinates: data.features[0].geometry.paths,
        },
        id: 23,
        type: "Feature",
      });
      geoJson.features = features;
      geoJson.type = "FeatureCollection";
      geoJson.groupId = this._id;
      geoJson.idLayer = this.mapApi.Guid2.newGuid;
      setTimeout(() => {
        //this.createLayer(geoJson);
      }, 1000);
    });
  }
  searchRiosSecundarios(geometry: any) {
    let options = {
      url:
        "https://geoservidorperu.minam.gob.pe/arcgis/rest/services/ServicioBase/MapServer",
      layerId: "9",
      where: `OBJECTID=${1}`,
      SRID: 4326,
      SRIDOutput: 4326,
      geometry: geometry,
    };
    this.mapApi.getDatosbyAGSIntersect(options).then((data: any) => {
      let geoJson: any = {};
      geoJson.title = "Rios Secundarios";
      geoJson.opacity = 0.4;
      geoJson.color = "#409caf";
      geoJson.file = null;
      geoJson.service = true;
      geoJson.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      let features = [];
      features.push({
        properties: data.features[0].attributes,
        geometry: {
          type: "MultiLineString",
          coordinates: data.features[0].geometry.paths,
        },
        id: 23,
        type: "Feature",
      });
      geoJson.features = features;
      geoJson.type = "FeatureCollection";
      geoJson.groupId = this._id;
      geoJson.idLayer = this.mapApi.Guid2.newGuid;
      setTimeout(() => {
        //this.createLayer(geoJson);
      }, 5000);
    });
  }
  searchRedvial(geometry: any) {
    let options = {
      url:
        "https://geoservidorperu.minam.gob.pe/arcgis/rest/services/ServicioBase/MapServer",
      layerId: "11",
      where: `OBJECTID=${1}`,
      SRID: 4326,
      SRIDOutput: 4326,
      geometry: geometry,
    };
    let geoJsonLayer: any = this.mapApi.graphicsLayer();
    this.view.map.add(geoJsonLayer);
    this.mapApi.getDatosbyAGSIntersect(options).then((data: any) => {
      const fillSymbol = {
        type: "simple-line",
        color: "dodgerblue",
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
        width: "3px",
      };
      data.features.forEach((t: any) => {
        t.geometry.type = "polyline";
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.attributes;
        let data = {
          rio: `${t.attributes.DIN99} - ${t.attributes.DN99}`,
        };
        //this.listaAspectoFisico.push(data);
        geoJsonLayer.add(feature);
      });
    });
  }
  searchCochas(geometry: any) {
    let options = {
      url:
        "http://geo.ana.gob.pe/arcgis/rest/services/GeoHidroV2/HIDROGRAFIA_NATURAL/MapServer",
      layerId: "0",
      where: `OBJECTID=${1}`,
      SRID: 4326,
      SRIDOutput: 4326,
      geometry: geometry,
    };
    let geoJsonLayer: any = this.mapApi.graphicsLayer();
    this.view.map.add(geoJsonLayer);
    this.mapApi.getDatosbyAGSIntersect(options).then((data: any) => {
      const fillSymbol = {
        type: "simple-fill",
        color: [38, 237, 10, 0.4],
        outline: {
          color: [255, 255, 255, 0.4],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        let template = {
          title: `Cocha: ${t.attributes.CODIGO}`,
        };
        t.geometry.type = "polygon";
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.attributes;
        feature.popupTemplate = template;
        let data = {
          laguna: `${t.attributes.CODIGO} - ${t.attributes.COD_GDB}`,
        };
        //this.listaAspectoFisico.push(data);
        geoJsonLayer.add(feature);
      });
    });
  }
  searchUAbyIdPlanManejo() { }
  listarTipoBosque(items: any) {
    this.listaBosque = items;
    this.calculateAreaTotalTipoBosque();
  }
  calculateAreaTotalTipoBosque() {
    let sum1 = 0;
    for (let item of this.listaBosque) {
      sum1 += parseFloat(item.areaHa);
    }
    for (let item of this.listaBosque) {
      item.areaHaPorcentaje = Number(((100 * parseFloat(item.areaHa)) / sum1).toFixed(2));
    }
    this.totalAreaTipoBosque = `${sum1.toFixed(2)}`;
    this.totalPorcentaTipoBosque = `${(100).toFixed(2)} %`;
  }
  onChangeTipoBosque(){
    this.calculateAreaTotalTipoBosque();
  }
  ngOnDestroy(): void {
    if (this.view) {
      // destroy the map view
      this.view.destroy();
    }
  }
  cargarCoordenadas = () => {
    this.listaCoordenadas.push({
      punto: 1,
      este: "este",
      norte: "norte",
      referencia: "",
    });
  };

  cargarAspectoFisico = () => {
    //llamar al servicio catastro y si no hay data llenarla, si hay data existeCatastroAspectoFisico debe ser true
    this.listaAspectoFisico = [];
    this.existeCatastroAspectoFisico = false;
  };

  cargarFisiograficas = () => {
    this.listaFisiograficas.push(
      {
        idUnidad: 1,
        unidadFisiografica: "Llanura aluvial inundable permanente",
        seleccionar: null,
        area: "",
        porcentaje: "",
      },
      {
        idUnidad: 2,
        unidadFisiografica: "Llanura aluvial inundable temporalmente",
        seleccionar: null,
        area: "",
        porcentaje: "",
      }
    );
  };

  cargarBosque = () => {
    this.listaBosque.push({
      idBosque: 1,
      tipoBosque: "demo",
      area: "20",
      porcentaje: "",
    });
  };

  /*cargarPoblacion = () => {
    this.listaPoblacion.push({
      idPoblacion: 1,
      poblacion: "Comunidad nativa o campesina",
      nombre: "",
      ubicacion: "",
      area: "",
      numeroFamilia: "",
      listActividades: [
        {
          idActividadGrupo: 1,
          descripcion: "Cultivos",
          listaGrupo: [
            {
              idGrupo: 1,
              descripcion: "Subsitencia",
            },
            {
              idGrupo: 2,
              descripcion: "Perennes",
            },
          ],
        },
        {
          idActividadGrupo: 2,
          descripcion: "Extracción",
          listaGrupo: [
            {
              idGrupo: 1,
              descripcion: "Caza",
            },
            {
              idGrupo: 2,
              descripcion: "Pesca",
            },
          ],
        },
      ],
    });
  };*/

  agregarRutas = (message: string, data?: any) => {
    this.ref = this.dialogService.open(ModalAccesibilidadRutasComponent, {
      header: message,
      width: '550px',
      style: { margin: '15px' },
      contentStyle: { overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp && message == 'Agregar Rutas') {
        resp.idRuta = this.listInfoBasicaDet_3_2_1.length + 1;
        resp.idInfBasicaDet = 0;
        resp.codInfBasicaDet = this.codigoAcordeonAccesibilidad;
        resp.codSubInfBasicaDet = this.codigoTabla1;
        resp.idUsuarioRegistro = this.user.idUsuario;
        this.listInfoBasicaDet_3_2_1.push(resp);
      } else if (resp) {
        data.referencia = resp.referencia
        data.nombre = resp.nombre
        data.acceso = resp.acceso
        data.coordenadaEsteIni = resp.coordenadaEsteIni
        data.coordenadaNorteIni = resp.coordenadaNorteIni
        data.coordenadaEsteFin = resp.coordenadaEsteFin
        data.coordenadaNorteFin = resp.coordenadaNorteFin
        data.distanciaKm = resp.distanciaKm
        data.tiempo = resp.tiempo
        data.medioTransporte = resp.medioTransporte
      }
    });


  };

  eliminarRutas = (idRuta: any) => {
    this.listaRutas = this.listaRutas.filter(
      (x) => x.puntoReferencia != idRuta.puntoReferencia
    );
  };

  modalPrincipalesLineamientos = (item?: any) => {
    this.ref = this.dialogService.open(ModalDescripcionComponent, {
      header: "Principales Limitaciones",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        item: item, mensajeValid: "(*) Debe ingresar la Descripción de Principales Limitaciones."
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {

        let reg = {
          idInfBasicaDet: 0,
          codInfBasicaDet: "PGMFIBAMAF",
          codSubInfBasicaDet: "TAB_3",
          descripcion: resp
        }



        this.principalesLineamientos.push(reg);
      }
    });
  };

  eliminarPrincipalesLineamientos(
    event: any,
    item: InformacionBasicaDetalleModel,
    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',

      accept: () => {

        if (item.idInfBasicaDet == 0) {
          this.toast.ok('Se eliminó el registró correctamente.');
          this.principalesLineamientos.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: '',
              idInfBasicaDet: item.idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.principalesLineamientos.splice(index, 1);
            });
        }

      },
      reject: () => { },
    });
  }




  modalInformacion = (item?: any) => {
    this.ref = this.dialogService.open(ModalDescripcionComponent, {
      header: " Información de Tipos de Suelo",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        item: item, mensajeValid: "(*) Debe ingresar la Descripción de Tipo de Suelo."
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {

        let reg = {
          idInfBasicaDet: 0,
          codInfBasicaDet: "PGMFIBAMAF",
          codSubInfBasicaDet: "TAB_4",
          descripcion: resp
        }



        this.informacionTipos.push(reg);
      }
    });
  };

  eliminarInformacion(
    event: any,
    item: InformacionBasicaDetalleModel,
    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',

      accept: () => {

        if (item.idInfBasicaDet == 0) {
          this.toast.ok('Se eliminó el registró correctamente.');
          this.informacionTipos.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: '',
              idInfBasicaDet: item.idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.informacionTipos.splice(index, 1);
            });
        }

      },
      reject: () => { },
    });
  }

  agregarAspectoFisico = () => {

    let reg = {
      idInfBasicaDet: 0,
      codInfBasicaDet: "PGMFIBAMAF",
      codSubInfBasicaDet: "TAB_1",
      idUsuarioRegistro: this.user.idUsuario,
      idAspectoFisico: this.listaAspectoFisico.length + 1
    }

    this.listaAspectoFisico.push(reg);
  };


  eliminarAspectoFisico(
    event: any,
    item: InformacionBasicaDetalleModel,
    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {

        if (item.idInfBasicaDet == 0) {
          this.toast.ok('Se eliminó el registró correctamente.');
          this.listaAspectoFisico.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService
            .eliminarInformacionBasica({
              idInfBasica: 0,
              codInfBasicaDet: '',
              idInfBasicaDet: item.idInfBasicaDet,
              idUsuarioElimina: this.user.idUsuario,
            })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listaAspectoFisico.splice(index, 1);
            });
        }

      },
      reject: () => { },
    });
  }


  seleccionarUnidad = (item: any) => {
    this.listaFisiograficas.find((x) => {
      if (item.idUnidad === x.idUnidad) {
        x.seleccionar = true;
      }
    });
  };

  calcularPorcentajeFisiografica = (item: any) => {
    this.listaFisiograficas.find((x) => {
      if (item.idUnidad === x.idUnidad) {
        item.porcentaje = (item.area * 2) / 100; //cambiar el 2 por el % real;
      }
    });
  };

  eliminarUnidad = (idUnidad: number) => {
    this.listaFisiograficas = this.listaFisiograficas.filter(
      (x) => x.idUnidad != idUnidad
    );
  };

  agregarFauna = () => {
    this.ref = this.dialogService.open(ModalFaunaComponent, {
      header: "Fauna",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp != 'ok') {
        var params: any = {};
        params.idPlanManejo = this.idPGMF;
        params.idUsuarioRegistro = this.user.idUsuario;
        params.codigoTipo = "PGMF";
        params.nombreCientifico = resp.nombreCientifico;
        //params.familia = resp.familia;
        params.nombre = resp.nombre;
        //params.adjunto = resp.adjunto;
        params.estatus = resp.estado;
        params.estadoSolicitud = "Solicitado";
        //params.idFauna = resp.idFauna ? resp.idFauna : 0;
        //params.idArchivo = resp.idArchivo;
        this.informacionAreaPmfiService
          .registrarSolicitudFauna(params)
          .subscribe((response) => {
            this.obtenerFaunas();
          });

        //resp.idFauna = this.listaFauna.length + 1;
        //this.listaFauna.push(resp);
      }
    });
  };

  eliminarFaunas = (event: any, data: any) => {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        var params = {
          idFauna: data.idFauna,
          codInfBasica: 'PGMF',
          idUsuarioElimina: this.user.idUsuario,
        };
        this.informacionAreaPmfiService
          .eliminarFauna(params)
          .subscribe((response: any) => {
            if (response.success == true) {
              this.toast.ok(response?.message);
              this.obtenerFaunas();
            } else {
              this.toast.error(response?.message);
            }
          });
      },
      reject: () => { }
    });
  }

  calcularPorcentajeBosque = (item: any) => {
    this.listaBosque.find((x) => {
      if (item.idBosque === x.idBosque) {
        item.porcentaje = (item.area * 2) / 100; //cambiar el 2 por el % real;
      }
    });
  };

  agregarCaracterizacion = (poblacionAledania?: string) => {
    this.ref = this.dialogService.open(ModalCaracterizacionComponent, {
      header: "Caracterización",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        poblacionAledania: poblacionAledania,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.poblacionAledania == "Comunidad nativa o campesina") {
          this.comunidadNativaNombre = resp.nombre;
          this.comunidadNativaUbicacion = resp.ubicacion;
          this.comunidadNativaArea = resp.area;
          this.comunidadNativaFamilia = resp.NoFamilias;
        } else if (resp.poblacionAledania == "Caserio") {
          this.caserioNombre = resp.nombre;
          this.caserioUbicacion = resp.ubicacion;
          this.caserioArea = resp.area;
          this.caserioFamilia = resp.NoFamilias;
        } else if (resp.poblacionAledania == "Centros poblados") {
          this.centrosPobladosNombre = resp.nombre;
          this.centrosPobladosUbicacion = resp.ubicacion;
          this.centrosPobladosArea = resp.area;
          this.centrosPobladosFamilia = resp.NoFamilias;
        } else if (resp.poblacionAledania == "Colonos") {
          this.colonosNombre = resp.nombre;
          this.colonosUbicacion = resp.ubicacion;
          this.colonosArea = resp.area;
          this.colonosFamilia = resp.NoFamilias;
        } else {
          this.subListaPoblacion.push(resp);
        }
      }
    });
  };

  delete(item: any, event: any) {
    for (let i = 0; i < this.listInformacionBasicaDet.length; i++) {
      if (
        this.listInformacionBasicaDet[i].actividad == item.poblacionAledania &&
        this.listInformacionBasicaDet[i].descripcion == item.actividades
      ) {
        this.listInformacionBasicaDet.splice(i, 1);
      }
    }

  }

  checkValue(item: any, event?: any) {

    var params: any = {};
    this.delete(item, event.checked);

    if (
      item.poblacionAledania == "Comunidad nativa o campesina" &&
      event.checked == true
    ) {
      params.actividad = item.poblacionAledania;
      params.idInfBasicaDe = 0;
      (params.codInfBasicaDet = "PGMF35"),
        (params.codSubInfBasicaDet = "PGMF351");
      params.nombre = this.comunidadNativaNombre;
      params.ubicacion = this.comunidadNativaUbicacion;
      params.areaHa = this.comunidadNativaArea;
      params.nroFamlias = this.comunidadNativaFamilia;
      params.descripcion = item.actividades;
      if (item.codigo == "INFBAC") {
        params.observaciones = "Cultivos";
      } else {
        params.observaciones = "Extracción";
      }
      params.valor = "S";
      params.idInfBasica = 1;
      this.listInformacionBasicaDet.push(params);
    } else if (item.poblacionAledania == "Caserio" && event.checked == true) {
      params.actividad = item.poblacionAledania;
      params.idInfBasicaDe = 0;
      (params.codInfBasicaDet = "PGMF35"),
        (params.codSubInfBasicaDet = "PGMF351");
      params.nombre = this.caserioNombre;
      params.ubicacion = this.caserioUbicacion;
      params.areaHa = this.caserioArea;
      params.nroFamlias = this.caserioFamilia;
      params.descripcion = item.actividades;
      if (item.codigo == "INFBAC") {
        params.observaciones = "Cultivos";
      } else {
        params.observaciones = "Extracción";
      }
      params.valor = "S";
      params.idInfBasica = 1;
      this.listInformacionBasicaDet.push(params);
    } else if (
      item.poblacionAledania == "Centros poblados" &&
      event.checked == true
    ) {
      params.actividad = item.poblacionAledania;
      params.idInfBasicaDe = 0;
      (params.codInfBasicaDet = "PGMF35"),
        (params.codSubInfBasicaDet = "PGMF351");
      params.nombre = this.centrosPobladosNombre;
      params.ubicacion = this.centrosPobladosUbicacion;
      params.areaHa = this.centrosPobladosArea;
      params.nroFamlias = this.centrosPobladosFamilia;
      params.descripcion = item.actividades;
      if (item.codigo == "INFBAC") {
        params.observaciones = "Cultivos";
      } else {
        params.observaciones = "Extracción";
      }
      params.valor = "S";
      params.idInfBasica = 1;
      this.listInformacionBasicaDet.push(params);
    } else if (item.poblacionAledania == "Colonos" && event.checked == true) {
      params.actividad = item.poblacionAledania;
      params.idInfBasicaDe = 0;
      (params.codInfBasicaDet = "PGMF35"),
        (params.codSubInfBasicaDet = "PGMF351");
      params.nombre = this.colonosNombre;
      params.ubicacion = this.colonosUbicacion;
      params.areaHa = this.colonosArea;
      params.nroFamlias = this.colonosFamilia;
      params.descripcion = item.actividades;
      if (item.codigo == "INFBAC") {
        params.observaciones = "Cultivos";
      } else {
        params.observaciones = "Extracción";
      }
      params.valor = "S";
      params.idInfBasica = 1;
      this.listInformacionBasicaDet.push(params);
    }
  }

  guardarCaracterizacion() {
    this.caracterizacion = [
      {
        idInfBasica: 0,
        codInfBasica: "PGMF",
        codSubInfBasica: "",
        codNombreInfBasica: "",
        departamento: "",
        provincia: "",
        distrito: "",
        cuenca: "",
        anexo: "",
        idPlanManejo: this.idPGMF,
        listInformacionBasicaDet: this.listInformacionBasicaDet,
      },
    ];
  }

  agregarInfraestructura = () => {
    this.ref = this.dialogService.open(ModalInfraestructuraComponent, {
      header: "Infraestructura de Servicios",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => { });
  };

  agregarAntecedente = (data?: any) => {
    this.ref = this.dialogService.open(ModalAntecedenteComponent, {
      header:
        "Antecedentes de uso de recursos anterior al otorgamiento de la concesión",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      this.antecedentes.push(resp)
    });
  };
  agregarConflicto = () => {
    /*this.ref = this.dialogService.open(ModalConflictoComponent, {
      header:
        "Identificación y manejo de conflictos de uso de la tierra y los recursos forestales",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.manejoConflictos.push(resp);
      }
     });*/

    let reg = {
      idInfBasicaDet: 0,
      codInfBasicaDet: "PGMFIBAMAIC",
      codSubInfBasicaDet: "TAB_2",
      idUsuarioRegistro: this.user.idUsuario
    }

    this.manejoConflictos.push(reg);
  };

  eliminarConflicto(
    event: any,
    item: any,
    index: number
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {

        if (item.idInfBasicaDet == 0) {
          this.toast.ok('Se eliminó el registró correctamente.');
          this.manejoConflictos.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService.eliminarInformacionBasica({
            idInfBasica: 0,
            codInfBasicaDet: '',
            idInfBasicaDet: item.idInfBasicaDet,
            idUsuarioElimina: this.user.idUsuario,
          })
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((result: any) => {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.manejoConflictos.splice(index, 1);
            });
        }

      },
      reject: () => { },
    });
  }

  listarConflicto() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPGMF,
      codCabecera: "PGMFIBAMAIC"
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data) {
          // this.toast.ok("Se listó los Aspectos Físicos correctamente.");
          let list_InfoBasicDetalleTemp = response.data.filter(
            (x: any) => (x.codInfBasicaDet = "PGMFIBAMAIC")
          );
          if (list_InfoBasicDetalleTemp && list_InfoBasicDetalleTemp.length > 0) {

            this.manejoConflictos = list_InfoBasicDetalleTemp.filter(
              (x: any) => x.codSubInfBasicaDet == "TAB_2"
            );

            this.idInfBasica_3_6_2 = this.manejoConflictos[0]?.idInfBasica;
            this.selectAnexo4_6_2 = this.manejoConflictos[0]?.anexo || "N";

          }
        }
      });
  }

  validarGuardar3_6_2(): boolean{
    let valido = true;
    if (this.selectAnexo4_6_2 === "S") {
      if (!this.fileAnexo4_6_2.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_6_2.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  guardarConflicto() {
    if (!this.validarGuardar3_6_2()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idInfBasica: this.idInfBasica_3_6_2,
      idPlanManejo: this.idPGMF,
      codInfBasica: "PGMF",
      codSubInfBasica: "PGMFIBAM",
      codNombreInfBasica: "PGMFIBAMAIC",
      anexo: this.selectAnexo4_6_2,
      idUsuarioRegistro: this.user.idUsuario,
      listInformacionBasicaDet: this.manejoConflictos
    };

    this.informacionAreaPmfiService
      .registrarInformacionBasica([params])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok("Se resgitró el conflicto y Propuesta correctamente.");
        this.listarConflicto();
      });
  }

  eliminarCaracterizacion = (idBosque: number) => {
    this.listaPoblacion = this.listaPoblacion.filter(
      (x) => x.idBosque != idBosque
    );
  };

  listaCracteizacion() {
    this.planificacionService.obtenerPoblacion().subscribe((resp: any) => {
      resp.data.forEach((element: any) => {
        this.listaPoblacion.push(element);
        //comunidad nativa
        this.comunidadNativa = this.listaPoblacion.filter(function (e) {
          return e.poblacionAledania == "Comunidad nativa o campesina";
        });
        this.cultivos = this.comunidadNativa.filter(function (e) {
          return e.codigo == "INFBAC";
        });
        this.extraccion = this.comunidadNativa.filter(function (e) {
          return e.codigo == "INFBAE";
        });
        // caserio
        this.caserio = this.listaPoblacion.filter(function (e) {
          return e.poblacionAledania == "Caserio";
        });
        this.cultivosCaserio = this.caserio.filter(function (e) {
          return e.codigo == "INFBAC";
        });
        this.extraccionCaserio = this.caserio.filter(function (e) {
          return e.codigo == "INFBAE";
        });
        //Centros poblados
        this.centrosPoblados = this.listaPoblacion.filter(function (e) {
          return e.poblacionAledania == "Centros poblados";
        });
        this.cultivosCentrosPoblados = this.centrosPoblados.filter(function (
          e
        ) {
          return e.codigo == "INFBAC";
        });
        this.extraccionCentrosPoblados = this.centrosPoblados.filter(function (
          e
        ) {
          return e.codigo == "INFBAE";
        });
        //Colonos
        this.colonos = this.listaPoblacion.filter(function (e) {
          return e.poblacionAledania == "Colonos";
        });
        this.cultivosColonos = this.colonos.filter(function (e) {
          return e.codigo == "INFBAC";
        });
        this.extraccionColonos = this.colonos.filter(function (e) {
          return e.codigo == "INFBAE";
        });
      });
    });
  }

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        this.listarPorFilroProvincia(this.provincia);
      },
      (error: any) => {
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }
  listarPorFilroProvincia(provincia: ProvinciaModel) {
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      });
  }
  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }
  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }
  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  obtenerSolicitudAprovechamientoCcnn() {
    const solicitud = {} as SolicitudModel;
    solicitud.idSolicitud = 70;

    this.servPlanManejoCcnn
      .obtenerSolicitudAprovechamientoCcnn(solicitud)
      .subscribe((result: any) => {
        let resultado = result.data.data;

        this.departamento.idDepartamento = resultado.distrito.idDepartamento;
        this.provincia.idProvincia = resultado.distrito.idProvincia;
        this.distrito.idDistrito = resultado.distrito.idDistrito;

        this.listarPorFiltroDepartamento(this.departamento);
        if (resultado.distrito.idDepartamento !== null) {
          this.searchDepartment(`0${resultado.distrito.idDepartamento}`);
        }
        if (resultado.distrito.idProvincia !== null) {
          this.searchProvince(`0${resultado.distrito.idProvincia}`);
        }
        if (resultado.distrito.idDistrito !== null) {
          this.searchDistrict(`0${resultado.distrito.idDistrito}`);
        }
      });
  }
  
  /* Implementación  para Unidad Fisiográfica */
  onFileChangeUF(e: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.target.files[0];
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        let config = {
          idLayer: this.mapApi.Guid2.newGuid,
          inServer: false,
          id: e.target.id,
          modificar: true,
        };
        while (i < e.target.files.length) {
          this.processFileUF(e.target.files[i], config);
          i++;
        }
      }
    }
  }
  processFileUF(file: any, config: any) {
    config.file = file;
    try {
      let promise = Promise.resolve([]);
      promise = this.mapApi.loadShapeFile(file);
      promise
        .then((data) => {
          let controls = this.calculateAreaUF(data, config);
          if (controls.success === true) {
            this.calculateAreaTotalUF();
            this.createLayersUF(data, file, config);
          }
        })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      console.log(error);
    }
  }
  createLayersUF(layers: any, file: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.5;
      t.color = this.mapApi.random();
      t.file = file;
      t.groupId = this._id;
      t.inServer = config.inServer;
      t.id = this.mapApi.Guid2.newGuid;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this.mapApi.createLayer(t, this.view);
      this.createButton(t, config);
    });
  }
  calculateAreaUF(data: any, config: any) {
    let controls: any = {};
    let sumArea: any = null;
    data[0].features.forEach((t: any) => {
      let geometry: any = null;
      if ((t.geometry.type = "Polygon")) {
        geometry = { spatialReference: { wkid: 4326 } };
        geometry.rings = t.geometry.coordinates;
        let area = this.mapApi.calculateArea(geometry, "hectares");
        sumArea += area;
        controls.success = true;
      } else {
        controls.success = false;
      }
    });
    for (let item of this.listUnidadGeografica) {
      if (item.idUnidadFisiografica == config.id) {
        item.area = Number(sumArea.toFixed(2));
        item.modificar = config.modificar;
        item.idLayer = config.idLayer;
        item.inServer = config.inServer;
        item.file = true;
        item.archivo.archivo = config.file;
      }
    }
    return controls;
  }
  createButton(data: any, config: any) {
    let table = this.tblUnidadFisiografica.tableViewChild.nativeElement;
    let body = table.querySelector("tbody");
    let trs = body.querySelectorAll("tr");
    trs.forEach((t: any) => {
      if (t.id !== "") {
        if (Number(t.id) === Number(config.id)) {
          let layerId = data.id;
          let td = t.querySelectorAll("td")[1];
          let btn: any = null;
          let btnDownload: any = null;
          if (config.inServer === true) {
            btn = td.querySelector("button");
            btn.style.display = "none";
            let url = URL.createObjectURL(data.file);
            btnDownload = td.appendHTML(
              `<button type="button" data-url= "${url}"  data-name = "${data.title}"  class="btn-secondary" style="min-width: 40px;"><i class="pi pi-download" data-url= "${url}"  data-name = "${data.title}"></i></button>`
            );
            btnDownload.addEventListener("click", (e: any) => {
              this.onDownloadFileUF(e);
            });
          } else {
            btn = td.querySelector("button");
            btn.setAttribute("disabled", true);

            let span = td.querySelector("span");
            if (span !== null) span.remove();
          }
          let btnDelete = td.appendHTML(
            `<span class="float-right" data-id ="${config.id}" ><i class="pi pi-trash"  data-in-server="${config.inServer}" id="${layerId}" data-guid="${layerId}"></i></span>`
          );
          btnDelete.addEventListener("click", (e: any) => {
            let idUnidadFisiografica = e.target.parentNode.dataset.id;
            if (config.inServer === true) {
              this.confirmationService.confirm({
                target: e.target || undefined,
                message: "¿Está seguro de eliminar el Archivo?",
                icon: "pi pi-exclamation-triangle",
                acceptLabel: "Sí",
                rejectLabel: "No",
                key: "deleteUnidadFisiografica",
                accept: () => {
                  this.eliminarUnidadFisiograficaArchivo(idUnidadFisiografica);
                  this.mapApi.removeLayer(e.target.id, this.view);
                  let input = td.querySelector("input");
                  btnDownload.remove();
                  btn.style.display = "block";
                  btn.removeAttribute("disabled");
                  input.value = "";
                  btnDelete.remove();
                }
              })
            } else if (config.inServer === false) {
              for (let item of this.listUnidadGeografica) {
                if (item.idUnidadFisiografica === Number(idUnidadFisiografica)) {
                  item.file = false;
                  item.accion = true;
                  item.area = 0;
                  item.porcentaje = 0;
                }
              }
              this.calculateAreaTotalUF();
              this.mapApi.removeLayer(e.target.id, this.view);
              let input = td.querySelector("input");
              if (config.inServer === true) {
                btnDownload.remove();
                btn.style.display = "block";
              } else {
                btn.removeAttribute("disabled");
              }
              input.value = "";
              btnDelete.remove();
            }
          });
        }
      }
    });
  }
  calculateAreaTotalUF() {
    let sum1 = 0;
    let sum2 = 0;

    for (let item of this.listUnidadGeografica) {
      sum1 += Number(item.area);
    }
    for (let item of this.listUnidadGeografica) {
      item.porcentaje = Number(((100 * item.area) / sum1).toFixed(2)) || 0;
      sum2 += item.porcentaje;
    }
    this.totalArea = `${sum1.toFixed(2)}`;
    this.totalPorcentaje = `${sum2.toFixed(2)} %`;
  }
  onDownloadFileUF(e: any) {
    let url = e.target.dataset.url;
    let name = e.target.dataset.name;
    const link = document.createElement("a");
    link.href = url;
    link.download = `${name}.zip`;
    link.click();
  }
  listarUnidadFisiografica() {
    let id = 1;
    this.serviceUnidFisiografica.listarUnidadFisiografica(id).subscribe((result: any) => {
      this.listUnidadGeografica = result.data;
      /*for (let item of this.listUnidadGeografica) {
        if (item.archivo != null && item.archivo != undefined) {
          let blob = this.mapApi.readFileByte(item.archivo.archivo);
          let config = {
            idLayer: this.mapApi.Guid2.newGuid,
            inServer: true,
            id: item.idUnidadFisiografica,
            modificar: false,
          };
          this.processFileUF(blob, config);
        }
      }*/
      this.calculateAreaTotalUF();
    });
  }
  guardarUnidadFisiograficaArchivo() {
    let count = 0;
    let recorridos = 0;
    this.listUnidadGeografica.forEach((item: any) => {
      
      let add = 0;
      //if (item.inServer === true || item.inServer === undefined) return;
      item.idUsuarioRegistro = this.usuario;
      item.informacionBasica.idInfBasica = 1;
      let param = new HttpParams()
        .set('idInfBasica', item.informacionBasica.idInfBasica.toString())
        .set('idUnidadFisiografica', item.idUnidadFisiografica.toString())
        .set('area', item.area.toString())
        .set('porcentaje', item.porcentaje.toString())
        .set('accion', item.accion.toString())
        //.set('file', item.file.toString())
        .set('idUsuarioRegistro', item.idUsuarioRegistro.toString())

      //const formData = new FormData();
      //let filFile: any = item.archivo.archivo;
      //formData.append('files', filFile);
      this.serviceUnidFisiografica.registrarUnidadFisiograficaArchivo(param).subscribe((result: any) => {
        recorridos++;
        add++;
        if(recorridos === this.listUnidadGeografica.length) {
          this.toast.ok("Se Registró la Fisiografía correctamente.");
        }
        if (add == count) {
          this.listarUnidadFisiografica();
        }
      });
    });
  }
  eliminarUnidadFisiograficaArchivo(idUnidadFisiografica: any) {
    for (let item of this.listUnidadGeografica) {
      if (item.idUnidadFisiografica === Number(idUnidadFisiografica)) {
        item.file = false;
        item.accion = true;
        item.area = 0;
        item.porcentaje = 0;
        item.inServer = false;
        let param = {
          idInfBasica: 1,
          idUnidadFisiografica: item.idUnidadFisiografica,
          idUsuario: this.usuario
        }
        this.serviceUnidFisiografica.eliminarUnidadFisiograficaArchivo(param).subscribe((result: any) => {
          
        });
      }
    }
    this.calculateAreaTotalUF();

  }
  listarInfBasicaArchivo() {
    let idInfBasica = 1;
    this.informacionBasicaService.listarInfBasicaArchivo(idInfBasica).subscribe((result: any) => {
      if (result.data.length) {
        result.data.forEach((t: any) => {
          if (t.archivo !== null) {
            let config = {
              inServer: true,
              service: false,
              fileName: t.nombeArchivo,
              idArchivo: t.codigo,
              idLayer: this.mapApi.Guid2.newGuid,
            }
            let blob = this.mapApi.readFileByte(t.archivo);
            //this.processFile(blob, config);
          }
        });
      }
    });
  }
  deleteInfBasicaArchivo(idArchivo: any) {
    let item = {
      id: 1,
      idArchivo: idArchivo,
      idUsuario: this.usuario
    };
    this.informacionBasicaService.eliminarInfBasicaArchivo(item).subscribe((result: any) => {
      
    });
  }
  
  obtenerArchivos() {
    let item = {
      idPlanManejo: this.idPGMF,
      codigoTipoPGMF: 'INFBASICA'
    }
    this.servicePostulacionPFDM.obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF).subscribe((result: any) => {
      
      result.data.forEach((t: any) => {
        if (t.tipoDocumento === 'GDB') {
          this.archivoGDB.nombreFile = t.nombre;
          this.archivoGDB.codigo = t.idArchivo;
          this.archivoGDB.descripcion = t.tipoDocumento;
          this.archivoGDB.inServer = true;
          this.archivoGDB.file = t.documento;
        } else {
          if (t.documento != null) {
            this.archivoTrack.nombreFile = t.nombre;
            this.archivoTrack.codigo = t.idArchivo;
            this.archivoTrack.descripcion = t.tipoDocumento;
            this.archivoTrack.inServer = true;
            this.archivoTrack.file = t.documento;
            this.archivoTrack.idLayer = this.mapApi.Guid2.newGuid;
            let blob = this.mapApi.readFileByte(t.documento);
            let config = {
              idLayer: this.archivoTrack.idLayer
            }
            //this.processFile(blob, config);
          }
        }
      });
    });
  }

  obtenerFaunas() {
    var params = {
      idPlanManejo: this.idPGMF,
      codigoTipo: "PGMF",
    };
    this.informacionAreaPmfiService
      .obtenerFauna(params)
      .subscribe((response: any) => {
        this.listaFauna = [];
        response.data.forEach((element: any, index: number) => {
          
          this.listaFauna.push({
            ...element
          });
        });
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }
  regresarTab() {
    this.regresar.emit();
  }
  onDownloadFileGDB(data: FileModel) {
    let blob = this.mapApi.readFileByte(data.file);
    let url = URL.createObjectURL(blob);
    let name = data.nombreFile;
    const link = document.createElement("a");
    link.href = url;
    link.download = name;
    link.click();
  }
  onDeleteFileGDB(data: FileModel) {
    this.confirmationService.confirm({
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      key: "deleteAccesibilidad",
      accept: () => {
        if (data.descripcion === "GDB") {
          this.archivoGDB.codigo = 0;
          this.archivoGDB.nombreFile = '';
          this.archivoGDB.inServer = false;
        } else if (data.descripcion === "SHP") {
          this.archivoTrack.codigo = 0;
          this.archivoTrack.nombreFile = '';
          this.archivoTrack.inServer = false;
        }
      }
    });
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }

  modalResumen = (item?: any) => {
    this.ref = this.dialogService.open(ModalDescripcionComponent, {
      header: "Necesidades de Caminos ",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        item: item, mensajeValid: "(*) Debe ingresar la Descripción de Necesidades de Caminos."
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listInfoBasicaDet_3_2_2.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            descripcion: resp,
            codInfBasicaDet: this.codigoAcordeonAccesibilidad,
            codSubInfBasicaDet: this.codigoTabla2,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      }
    });
  };

  eliminarResumen = (data: any) => {
    this.data1 = this.data1.filter((x) => x != data);
  };

  /* 3.1.1 */
  listarCuencas() {
    this.cuencas = [];
    this.coreCentralService.listarComboCuenca().subscribe((result: any) => {
      if (result.success) {
        this.cuencas = result.data;
        this.cuencas.unshift({ codigo: undefined, valor: "Seleccione" })
        this.listarUbigeo();
      }
    });
  };

  listarUbigeo() {
    let params4_4: any = {
      idPlanManejo: this.idPGMF,
      codInfBasica: this.codigoProceso,
      codSubInfBasica: this.codigoTab,
    };
    this.listaUbicacion = [];

    this.informacionAreaPmfiService.listarInformacionBasicaUbigeo(params4_4).subscribe((response: any) => {
      if (response.data) {
        this.ordenarUbigeo(response.data);
      }
    });
  }

  private ordenarUbigeo(lista: any[]) {
    this.listaUbicacion = [];
    let d: any = {};
    let c: any = {};
    lista.forEach(element => {
      d = { cuencaSelect: {}, departamentoSelect: {}, provinciaSelect: {}, distritoSelect: {} };
      c = {};
      c = this.cuencas.filter(
        (x: any) => (x.codigo === element.cuenca)
      )[0];
      d.idInfBasica = element.idInfBasica;
      d.idInfBasicaUbigeo = element.idInfBasicaUbigeo;
      d.cuencaSelect.codigo = element.cuenca;
      d.cuencaSelect.valor = c.valor;
      d.departamentoSelect.codDepartamento = element.codigoDepartamento;
      d.provinciaSelect.codProvincia = element.codigoProvincia;
      d.distritoSelect.codDistrito = element.codigoDistrito;
      d.departamentoSelect.nombreDepartamento = element.nombreDepartamento;
      d.provinciaSelect.nombreProvincia = element.nombreProvincia;
      d.distritoSelect.nombreDistrito = element.nombreDistrito;

      this.listaUbicacion.push(d);
    });
  }

  agregarUbicacion = (data: any, index: number) => {
    const isEdit: boolean = !!data;

    this.ref = this.dialogService.open(ModalUbicacionComponent, {
      header: "Ubicación",
      width: "40%",
      contentStyle: { "height": "400px", overflow: "auto" },
      data: {
        data: data, isEdit: isEdit
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if(resp){
        if (isEdit) {
          this.listaUbicacion[index] = resp;
        } else {
          resp.idUbicacion = this.listaUbicacion.length + 1;
          this.listaUbicacion.push(resp);
        }
      }
    });
  };

  registrarUbicacion() {
    this.infoBasica_3_1.idPlanManejo = this.idPGMF;
    this.infoBasica_3_1.idInfBasica = this.listaUbicacion[0]?.idInfBasica;
    this.infoBasica_3_1.listInformacionBasicaDistrito = this.convertListUbicacion(this.listaUbicacion);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService.registrarInformacionBasicaUbigeo(this.infoBasica_3_1)
      .pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
        this.toast.ok('Se registró la ubicación correctamente.');
        this.listarUbigeo();
      });
  }

  private convertListUbicacion(lista: any[]) {
    let d: any = {};
    let l: any[] = [];

    lista.forEach(element => {
      d = {};
      d.idInfBasica = element.idInfBasica;
      d.idInfBasicaUbigeo = element.idInfBasicaUbigeo;
      d.codigoDepartamento = element.departamentoSelect.codDepartamento;
      d.codigoProvincia = element.provinciaSelect.codProvincia;
      d.codigoDistrito = element.distritoSelect.codDistrito;
      d.cuenca = element.cuencaSelect.codigo;
      l.push(d);
    });

    return l;
  }

  eliminarUbicacion(event: any, item: any, index: number, acordeon: string, tipo: string) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (item.idInfBasicaUbigeo === 0 || item.idInfBasicaUbigeo === null || item.idInfBasicaUbigeo === undefined) {
          this.toast.ok('Se eliminó el registro correctamente.');
          this.listaUbicacion.splice(index, 1);
        } else {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService.eliminarInformacionBasicaUbigeo({
            idInfBasicaUbigeo: item.idInfBasicaUbigeo,
            idUsuarioElimina: this.user.idUsuario,
          }).pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
            this.toast.ok('Se eliminó el registro correctamente.');
            this.listaUbicacion.splice(index, 1);
          });
        }
      },
      reject: () => { },
    });
  };

  /* 3.1.2 */
  listarCoordenadas() {
    this.listVertices = [];
    var params = {
      idInfBasica: 'PGMF',
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigoPGMF.ACORDEON_3_1_TAB_2,
    };

    this.informacionAreaPmfiService.listarInformacionBasica(params).subscribe((response: any) => {
      if (response.success && response.data) {
        this.listVertices = response.data.map((item: any) => {
          return {
            vertice: item.puntoVertice,
            referencia: item.referencia,
            idInfBasica: item.idInfBasica,
            idInfBasicaDet: item.idInfBasicaDet,
            codInfBasicaDet: item.codInfBasicaDet,
            este: item.coordenadaEste,
            norte: item.coordenadaNorte,
          }
        });
      }
    });
  }

  registrarCoordenadas() {
    let listMap: InformacionBasicaDetalle[] = this.listVertices.map((coordenada: any) => {
      return {
        idInfBasica: coordenada.idInfBasica || 0,
        idInfBasicaDet: coordenada.idInfBasicaDet ? coordenada.idInfBasicaDet : 0,
        idUnidadManejo: this.idPlanManejo,
        estado: 'A',
        idUsuarioRegistro: this.usuarioM.idusuario,
        codInfBasicaDet: CodigoPGMF.ACORDEON_3_1_TAB_2,
        codSubInfBasicaDet: CodigoPGMF.TAB_3,
        coordenadaEsteIni: coordenada.este,
        coordenadaNorteIni: coordenada.norte,
        idUsuarioModificacion: null,
        referencia: coordenada.referencia,
        puntoVertice: coordenada.vertice,
        descripcion: '',
      };
    }
    );
    let params = [
      {
        idInfBasica: this.listVertices[0]?.idInfBasica ? this.listVertices[0]?.idInfBasica : 0,
        codInfBasica: 'PGMF',
        codNombreInfBasica: CodigoPGMF.TAB_3,
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService.registrarInformacionBasica(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success == true) {
        this.toast.ok(response?.message);
        this.listarCoordenadas();
      } else {
        this.toast.error(response?.message);
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  // 3.2.1 - 3.2.1
  fileArchivoGDB: any = {};
  codigoAcordeonAccesibilidad: string = "PGMFIBAMA";
  codigoTabla1 = 'TAB_1';
  codigoTabla2 = 'TAB_2';
  infoBasica_3_2: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPGMF,
    codInfBasica: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAccesibilidad,
    idUsuarioRegistro: this.user.idUsuario,
  });
  listInfoBasicaDet_3_2_1: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_3_2_2: InformacionBasicaDetalleModel[] = [];

  selectAnexo4_3_2: string = "N";
  selectAnexo4_3_3: string = "N";
  selectAnexo4_3_4: string = "N";
  selectAnexo4_4_1: string = "N";
  selectAnexo4_4_2: string = "N";
  selectAnexo4_5_2: string = "N";
  selectAnexo4_6_1: string = "N";
  selectAnexo4_6_2: string = "N";
  idFile_PGMF_3_2_2: number = 0;
  idFile_PGMF_3_3_3: number = 0;
  idFile_PGMF_3_3_4: number = 0;
  idFile_PGMF_3_4_1: number = 0;
  idFile_PGMF_3_4_2: number = 0;
  idFile_PGMF_3_5_2: number = 0;
  idFile_PGMF_3_6_1: number = 0;
  idFile_PGMF_3_6_2: number = 0;
  codigosFile = {
    PGMF_TAB3_2_2: CodigoPGMF.ACORDEON_3_2_TAB_2,
    PGMF_TAB3_3_3: CodigoPGMF.ACORDEON_3_3_TAB_3, PGMF_TAB3_3_4: CodigoPGMF.ACORDEON_3_3_TAB_4,
    PGMF_TAB3_4_1: CodigoPGMF.ACORDEON_3_4_TAB_1, PGMF_TAB3_4_2: CodigoPGMF.ACORDEON_3_4_TAB_2,
    PGMF_TAB3_5_2: CodigoPGMF.ACORDEON_3_5_TAB_2,
    PGMF_TAB3_6_1: CodigoPGMF.ACORDEON_3_6_TAB_1, PGMF_TAB3_6_2: CodigoPGMF.ACORDEON_3_6_TAB_2,
  };

  listarArchivoGDB() {
    let params = { idPlanManejo: this.idPGMF, subCodigoProceso: CodigoPGMF.ACORDEON_3_2_TAB_1 };
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.listarPorFiltroPlanManejoArchivo(params).subscribe((result: any) => {
      load.close();
      if (result.success && result.data.length > 0) {
        result.data.forEach((item: any) => {
          if(item.idTipoDocumento === TiposArchivos.TDOCGDB) this.fileArchivoGDB = item;
        });
      }
    }, () => {
      load.close()
    });
  }

  registrarArchivoGDB(file: any) {
    let params = {
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.usuarioM.idusuario,
      codTipo: CodigoPGMF.TAB_3,
      subCodTipo: CodigoPGMF.ACORDEON_3_2_TAB_1,
      codTipoDocumento: TiposArchivos.TDOCGDB,
      idPlanManejoArchivo: file.idPlanManejoArchivo || 0
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.regitrarArchivoPlanManejo(params, file.file).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        this.fileArchivoGDB.idArchivo = result.data.idArchivo;
        this.fileArchivoGDB.idPlanManejoArchivo = result.data.idPGMFArchivo;
      } else {
        this.toast.warn(result.message);
        this.fileArchivoGDB = {};
      }
    }, (error) => {
      this.fileArchivoGDB = {};
      this.errorMensaje(error, true);
    });
  }

  eliminarArchivoGDB(file: any) {
    let params = {
      idPlanManejoArchivo: file.idPlanManejoArchivo,
      idUsuarioElimina: this.usuarioM.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.eliminarPlanManejoArchivo(params).subscribe((result: any) => {
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
        this.eliminarArchivo(file.idArchivo);
        this.fileArchivoGDB = {};
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarArchivo(idAdjuntoOut: number) {
    this.serviceArchivo.eliminarArchivo(idAdjuntoOut, this.usuarioM.idusuario).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarAccesibilidad() {
    let params4_4: any = {
      idInfBasica: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPGMF,
      codCabecera: this.codigoAcordeonAccesibilidad,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService.listarInformacionBasica(params4_4)
      .pipe(finalize(() => this.dialog.closeAll())).subscribe((response: any) => {
        if (response.data) {
          let list_InfoBasicDetalle3_2 = response.data.filter(
            (x: any) => (x.codInfBasicaDet = this.codigoAcordeonAccesibilidad)
          );
          if (list_InfoBasicDetalle3_2 && list_InfoBasicDetalle3_2.length > 0) {
            let infoBasic4_4 = list_InfoBasicDetalle3_2[0];
            Object.assign(this.infoBasica_3_2, infoBasic4_4); //SE TOMA EL PRIMERO PARA LA CABECERA
            this.selectAnexo4_3_2 = infoBasic4_4.anexo || "N";
            this.listInfoBasicaDet_3_2_1 = list_InfoBasicDetalle3_2.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla1
            );
            this.listInfoBasicaDet_3_2_2 = list_InfoBasicDetalle3_2.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla2
            );
            this.ordenarCoordenadas();
          }
        }
      });
  }

  private ordenarCoordenadas() {
    this.listInfoBasicaDet_3_2_1.forEach(element => {
      element.coordenadaEsteIni = element.coordenadaEste;
      element.coordenadaNorteIni = element.coordenadaNorte;
      element.coordenadaEsteFin = element.coordenadaEsteFin;
      element.coordenadaNorteFin = element.coordenadaNorteFin;
    });
  }

  validarRegistar(): boolean{
    let valido = true;
    if (this.selectAnexo4_3_2 === "S") {
      if (!this.fileAnexo4_3_2.justificacion) {
        this.toast.warn("Debe ingresar la justificación de la sección 3.2.2.");
        valido = false;
      }
      if (!this.fileAnexo4_3_2.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia de la sección 3.2.2.");
        valido = false;
      }
    }
    if(this.listInfoBasicaDet_3_2_2.length === 0) {
      this.toast.warn("(*) Debe de agregar al menos un registro.");
      valido = false;
    }
    return valido;
  }


  registrarAccesibilidad(seccion: number) {
    if(seccion === 1) {
      if(this.listInfoBasicaDet_3_2_1.length === 0) {
        this.toast.warn("(*) Debe de agregar al menos un registro.");
        return;
      }
    } else if(seccion === 2) {
      if(!this.validarRegistar()) return;
    }

    this.infoBasica_3_2.idPlanManejo = this.idPGMF;
    this.infoBasica_3_2.anexo = this.selectAnexo4_3_2;
    this.infoBasica_3_2.listInformacionBasicaDet = [];
    this.infoBasica_3_2.listInformacionBasicaDet = this.listInfoBasicaDet_3_2_1.concat(this.listInfoBasicaDet_3_2_2);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService.registrarInformacionBasica([this.infoBasica_3_2])
      .pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
        if (result.success) {
          this.toast.ok('Se registró la Accesibilidad correctamente.');
          this.listarAccesibilidad();
        }

      });
  }

  eliminarItemInformacionAreaManejo(
    event: any,
    item: InformacionBasicaDetalleModel,
    index: number,
    acordeon: string,
    tipo: string) {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (acordeon == this.codigoAcordeonAccesibilidad) {
          if (tipo == this.codigoTabla2) {
            if (item.idInfBasicaDet === 0) {
              this.toast.ok('Se eliminó el registro correctamente.');
              this.listInfoBasicaDet_3_2_2.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaPmfiService.eliminarInformacionBasica({
                idInfBasica: 0,
                codInfBasicaDet: '',
                idInfBasicaDet: item.idInfBasicaDet,
                idUsuarioElimina: this.user.idUsuario,
              }).pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
                this.toast.ok('Se eliminó el registro correctamente.');
                this.listInfoBasicaDet_3_2_2.splice(index, 1);
              });
            }
          }
        } else if (acordeon == this.codigoAcordeonAspectosSocioeconomico) {
          if (tipo == this.codigoTabla1) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_3_5_1.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaPmfiService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuarioM.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_3_5_1.splice(index, 1);
                });
            }
          } else if (tipo == this.codigoTabla2) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_3_5_2.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaPmfiService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuarioM.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_3_5_2.splice(index, 1);
                });
            }
          }
        } else if (acordeon == this.codigoAcordeonAntecedentesUsoIdentificacionConflictos) {
          
          this.listInfoBasicaDet_3_6_1[index]
          if (tipo == this.codigoTabla1) {
            if (index < 4) {
              item.marcar = null;
              item.especieExtraida = '';
              item.observaciones = '';
            } else if (index == 4) {
              item.actividad = 'Otra(Especificar)'
              item.especieExtraida = '';
              item.observaciones = '';
              item.marcar = null;
            }
            else if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_3_6_1.splice(index - 1, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaPmfiService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuarioM.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_3_6_1.splice(index - 1, 1);
                });
            }
          }
        }
      },
      reject: () => { },
    });
  }

  eliminarItemInformacionAreaManejo2(event: any, item: InformacionBasicaDetalleModel, index: number, acordeon: string, tipo: string) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (acordeon == this.codigoAcordeonAccesibilidad) {
          if (tipo == this.codigoTabla2) {
            if (item.idInfBasicaDet === 0) {
              this.toast.ok('Se eliminó el registro correctamente.');
              this.listInfoBasicaDet_3_2_1.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaPmfiService.eliminarInformacionBasica({
                idInfBasica: 0,
                codInfBasicaDet: '',
                idInfBasicaDet: item.idInfBasicaDet,
                idUsuarioElimina: this.user.idUsuario,
              }).pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
                this.toast.ok('Se eliminó el registro correctamente.');
                this.listInfoBasicaDet_3_2_1.splice(index, 1);
              });
            }
          }
        }
      },
      reject: () => { },
    });
  }

  cargarIdArchivo(data: any, codigoArchivo: string) {
    switch (codigoArchivo) {
      case this.codigosFile.PGMF_TAB3_2_2:
        this.idFile_PGMF_3_2_2 = data.idPGMFArchivo;
        this.selectAnexo4_3_2 = "S";
        break;
      case this.codigosFile.PGMF_TAB3_3_3:
        this.idFile_PGMF_3_3_3 = data.idPGMFArchivo;
        this.selectAnexo4_3_3 = "S"
        break;
      case this.codigosFile.PGMF_TAB3_3_4:
        this.idFile_PGMF_3_3_4 = data.idPGMFArchivo;
        this.selectAnexo4_3_4 = "S"
        break;
      case this.codigosFile.PGMF_TAB3_4_1:
        this.idFile_PGMF_3_4_1 = data.idPGMFArchivo;
        this.selectAnexo4_4_1 = "S"
        break;
      case this.codigosFile.PGMF_TAB3_4_2:
        this.idFile_PGMF_3_4_2 = data.idPGMFArchivo;
        this.selectAnexo4_4_2 = "S"
        break;
      case this.codigosFile.PGMF_TAB3_5_2:
        this.idFile_PGMF_3_5_2 = data.idPGMFArchivo;
        this.selectAnexo4_5_2 = "S"
        break;
      case this.codigosFile.PGMF_TAB3_6_1:
        this.idFile_PGMF_3_6_1 = data.idPGMFArchivo;
        this.selectAnexo4_6_1 = "S"
        break;
      case this.codigosFile.PGMF_TAB3_6_2:
        this.idFile_PGMF_3_6_2 = data.idPGMFArchivo;
        this.selectAnexo4_6_2 = "S"
        break;
      default:
        break;
    }
  }

  anexoN(idArchivo: number, fileAnexo?: any) {
    if (idArchivo !== 0) this.eliminarPlanManejo(idArchivo, fileAnexo);
  }

  eliminarPlanManejo(idArchivo: number, fileAnexo?: any) {
    var params = { idPlanManejoArchivo: idArchivo, idUsuarioElimina: this.user.idUsuario, };
    this.servicePostulacionPFDM.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      if(fileAnexo) {
        fileAnexo.nombreFile = "";
        fileAnexo.justificacion = "";
      }
    });
  }
  onChangeFileSHP(e: any) {
    
    e.preventDefault();
    e.stopPropagation();
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaVertices(data[0].features);

    });

    this.map.onChangeFile(e);
  }
  createTablaVertices(items: any) {
    
    this.listVertices = [];
    items.forEach((t: any) => {
      this.listVertices.push({
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
      });
    });
  }


  listRios() {
    var params = {
      id: 0,
      tipo: 'RIO',
    };
    this.informacionAreaPmfiService
      .obtenerHidrografia(params)
      .subscribe((response: any) => {
        this.cmbRios = [...response.data];
      });
  }
  guardarHidrografia() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.listaAspectoFisico.forEach(
      (det: InformacionBasicaDetalleModel) => {
        let rio = this.cmbRios.find(
          (element) => element.idHidrografia == det.idRios
        );
        if (rio) det.nombreRio = rio.descripcion;
      }
    );

    let params = {
      idInfBasica: this.idInfBasica_3_3_1,
      idPlanManejo: this.idPGMF,
      codInfBasica: "PGMF",
      codSubInfBasica: "PGMFIBAM",
      codNombreInfBasica: "PGMFIBAMAF",
      idUsuarioRegistro: this.user.idUsuario,
      listInformacionBasicaDet: this.listaAspectoFisico
    };

    this.informacionAreaPmfiService
      .registrarInformacionBasica([params])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró la Hidrografía correctamente.');
        this.listarHidrografia();
      });
  }

  listarHidrografia() {
    // this.dialog.open(LoadingComponent, { disableClose: true });
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPGMF,
      codCabecera: "PGMFIBAMAF"
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data) {
          // this.toast.ok("Se listó los Aspectos Físicos correctamente.");
          let list_InfoBasicDetalle4_5 = response.data.filter(
            (x: any) => (x.codInfBasicaDet = "PGMFIBAMAF")
          );
          if (list_InfoBasicDetalle4_5 && list_InfoBasicDetalle4_5.length > 0) {
            let infoBasic4_4 = list_InfoBasicDetalle4_5[0];
            //Object.assign(this.infoBasica_4_5, infoBasic4_4); //SE TOMA EL PRIMERO PARA LA CABECERA
            this.listaAspectoFisico = list_InfoBasicDetalle4_5.filter(
              (x: any) => x.codSubInfBasicaDet == "TAB_1"
            );
            this.idInfBasica_3_3_1 = this.listaAspectoFisico[0]?.idInfBasica;

          }
        } else {

        }
      });
  }

  validarGuardarPrincipallimit(): boolean{
    let valido = true;
    if(this.principalesLineamientos.length === 0) {
      valido = false;
      this.toast.warn("Debe agregar registro en la lista de limitaciones.");
    }
    if (this.selectAnexo4_3_3 === "S") {
      if (!this.fileAnexo4_3_3.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_3_3.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  guardarPrincipalesLimitaciones() {
    if (!this.validarGuardarPrincipallimit()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idInfBasica: this.idInfBasica_3_3_3,
      idPlanManejo: this.idPGMF,
      codInfBasica: "PGMF",
      codSubInfBasica: "PGMFIBAM",
      codNombreInfBasica: "PGMFIBAMAF",
      anexo: this.selectAnexo4_3_3,
      idUsuarioRegistro: this.user.idUsuario,
      listInformacionBasicaDet: this.principalesLineamientos
    };

    this.informacionAreaPmfiService
      .registrarInformacionBasica([params])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if(result.success) {
          this.toast.ok("Se registró la limitación correctamente.");
          this.listarPrincipalesLimitaciones();
        } else {
          this.toast.warn(result.message)
        }
      }, () => this.toast.error(Mensajes.MSJ_ERROR_CATCH));
  }

  listarPrincipalesLimitaciones() {
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPGMF,
      codCabecera: "PGMFIBAMAF"
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data) {
          // this.toast.ok("Se listó los Aspectos Físicos correctamente.");
          let list_InfoBasicDetalleTemp = response.data.filter(
            (x: any) => (x.codInfBasicaDet = "PGMFIBAMAF")
          );
          if (list_InfoBasicDetalleTemp && list_InfoBasicDetalleTemp.length > 0) {

            this.principalesLineamientos = list_InfoBasicDetalleTemp.filter(
              (x: any) => x.codSubInfBasicaDet == "TAB_3"
            );

            this.idInfBasica_3_3_3 = this.principalesLineamientos[0]?.idInfBasica;
            this.selectAnexo4_3_3 = this.principalesLineamientos[0]?.anexo || "N";

          }
        }
      });
  }

  validarGuardarTipoSuelo(): boolean{
    let valido = true;
    if(this.informacionTipos.length === 0) {
      valido = false;
      this.toast.warn("(*) Debe agregar al menos un registro en la lista de tipos de suelo.");
    }
    if (this.selectAnexo4_3_4 === "S") {
      if (!this.fileAnexo4_3_4.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_3_4.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  guardarInformaciontipoSuelo() {
    if (!this.validarGuardarTipoSuelo()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idInfBasica: this.idInfBasica_3_3_4,
      idPlanManejo: this.idPGMF,
      codInfBasica: "PGMF",
      codSubInfBasica: "PGMFIBAM",
      codNombreInfBasica: "PGMFIBAMAF",
      anexo: this.selectAnexo4_3_4,
      idUsuarioRegistro: this.user.idUsuario,
      listInformacionBasicaDet: this.informacionTipos
    };

    this.informacionAreaPmfiService.registrarInformacionBasica([params])
      .pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
        this.listarInformaciontipoSuelo();
        this.toast.ok("Se registró Tipo de Suelo correctamente.");
      });
  }

  listarInformaciontipoSuelo() {
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPGMF,
      codCabecera: "PGMFIBAMAF"
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data) {
          let list_InfoBasicDetalleTemp = response.data.filter(
            (x: any) => (x.codInfBasicaDet = "PGMFIBAMAF")
          );
          if (list_InfoBasicDetalleTemp && list_InfoBasicDetalleTemp.length > 0) {

            this.informacionTipos = list_InfoBasicDetalleTemp.filter(
              (x: any) => x.codSubInfBasicaDet == "TAB_4"
            );

            this.idInfBasica_3_3_4 = this.informacionTipos[0]?.idInfBasica;
            this.selectAnexo4_3_4 = this.informacionTipos[0]?.anexo || "N";
          }
        }
      });
  }

  //3.4.1
  infoDetalleAspectoBioFauna: any[] = [{ idInfBasicaDet: 0, codInfBasicaDet: "PGMFIBAMAB1", codSubInfBasicaDet: "", descripcion: "" }];
  guardarAspectoBioFaunaSilvestre() {
    if (!this.validarGuardarAspectoBioFaunasilvestre()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idInfBasica: this.idInfBasica_3_4_1,
      idPlanManejo: this.idPGMF,
      codInfBasica: "PGMF",
      codSubInfBasica: "PGMFIBAM",
      codNombreInfBasica: "PGMFIBAMAB1",
      anexo: this.selectAnexo4_4_1,
      idUsuarioRegistro: this.user.idUsuario,
      listInformacionBasicaDet: this.infoDetalleAspectoBioFauna
    };

    this.informacionAreaPmfiService.registrarInformacionBasica([params])
      .pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
        this.toast.ok("Se registró la Ampliación del Anexo04 correctamente");
        this.listarAspectoBioFaunaSilvestre();
      });
  }

  listarAspectoBioFaunaSilvestre() {
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPGMF,
      codCabecera: "PGMFIBAMAB1"
    };

    this.informacionAreaPmfiService.listarInformacionBasica(params).subscribe((response: any) => {
      if (response.data) {
        let list_InfoBasicDetalleTemp = response.data.filter((x: any) => (x.codInfBasicaDet = "PGMFIBAMAB1"));
        if (list_InfoBasicDetalleTemp && list_InfoBasicDetalleTemp.length > 0) {
          this.infoDetalleAspectoBioFauna = list_InfoBasicDetalleTemp;
          this.idInfBasica_3_4_1 = list_InfoBasicDetalleTemp[0]?.idInfBasica;
          this.selectAnexo4_4_1 = list_InfoBasicDetalleTemp[0]?.anexo || "N";
        }
      }
    });
  }

  validarGuardarAspectoBioFaunasilvestre(): boolean{
    let valido = true;
    if (this.selectAnexo4_4_1 === "S") {
      if (!this.fileAnexo4_4_1.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_4_1.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  //3.4.2
  infoDetalleAspectoBioTipobosque: any[] = [{ idInfBasicaDet: 0, codInfBasicaDet: "PGMFIBAMAB2", codSubInfBasicaDet: "", descripcion: "" }];
  guardarAspectoBioTiobosque() {
    if (!this.validarGuardarAspectoBioTiobosque()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idInfBasica: this.idInfBasica_3_4_2,
      idPlanManejo: this.idPGMF,
      codInfBasica: "PGMF",
      codSubInfBasica: "PGMFIBAM",
      codNombreInfBasica: "PGMFIBAMAB2",
      anexo: this.selectAnexo4_4_2,
      idUsuarioRegistro: this.user.idUsuario,
      listInformacionBasicaDet: this.infoDetalleAspectoBioTipobosque
    };

    this.informacionAreaPmfiService.registrarInformacionBasica([params])
      .pipe(finalize(() => this.dialog.closeAll())).subscribe((result: any) => {
        this.toast.ok("Se registró la Ampliación del Anexo04 correctamente");
        this.listarAspectoBioTiobosque();
      });
  }

  listarAspectoBioTiobosque() {
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPGMF,
      codCabecera: "PGMFIBAMAB2"
    };

    this.informacionAreaPmfiService.listarInformacionBasica(params).subscribe((response: any) => {
      if (response.data) {
        let list_InfoBasicDetalleTemp = response.data.filter((x: any) => (x.codInfBasicaDet = "PGMFIBAMAB2"));
        if (list_InfoBasicDetalleTemp && list_InfoBasicDetalleTemp.length > 0) {
          this.infoDetalleAspectoBioTipobosque = list_InfoBasicDetalleTemp;
          this.idInfBasica_3_4_2 = list_InfoBasicDetalleTemp[0]?.idInfBasica;
          this.selectAnexo4_4_2 = list_InfoBasicDetalleTemp[0]?.anexo || "N";
        }
      }
    });
  }

  validarGuardarAspectoBioTiobosque(): boolean{
    let valido = true;
    if (this.selectAnexo4_4_2 === "S") {
      if (!this.fileAnexo4_4_2.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_4_2.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  /*******************3.5.2******************* */
  agregarDetalle(acordeon: string, tipoTab: string) {
    if (acordeon == this.codigoAcordeonAspectosSocioeconomico) {
      if (tipoTab == this.codigoTabla1) {
        this.listInfoBasicaDet_3_5_1.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      } else if (tipoTab == this.codigoTabla2) {
        this.listInfoBasicaDet_3_5_2.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      }
    } else if (acordeon == this.codigoAcordeonAntecedentesUsoIdentificacionConflictos) {
      if (tipoTab == this.codigoTabla1) {
        this.listInfoBasicaDet_3_6_1.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAntecedentesUsoIdentificacionConflictos,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
            actividad: "Otra(Especificar)"
          })
        );
      }
    }
  }

  validarRegistarAapecSocieoEcon(): boolean{
    let valido = true;
    if (this.selectAnexo4_5_2 === "S") {
      if (!this.fileAnexo4_5_2.justificacion) {
        this.toast.warn("Debe ingresar la justificación de la sección 3.5.2.");
        valido = false;
      }
      if (!this.fileAnexo4_5_2.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia de la sección 3.5.2.");
        valido = false;
      }
    }
    return valido;
  }

  registrarAspectosSocieconomicos(array: any) {
    if(!this.validarRegistarAapecSocieoEcon()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_3_5.idPlanManejo = this.idPlanManejo;
    this.infoBasica_3_5.anexo = this.selectAnexo4_5_2;
    this.infoBasica_3_5.listInformacionBasicaDet = [];
    this.infoBasica_3_5.listInformacionBasicaDet = [
      this.infoBasicaDet_3_5_cab,
    ].concat(array);

    this.informacionAreaPmfiService
      .registrarInformacionBasica([this.infoBasica_3_5])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok(
          'Se registró los Aspectos Socioeconómicos correctamente.'
        );
        this.listarAspectosSocieconomicos();
      });
  }

  listarAspectosSocieconomicos() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_5: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAspectosSocioeconomico,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_5)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          //this.toast.ok("Se listó los Aspectos Físicos correctamente.");
          let list = response.data.filter(
            (x: any) =>
              (x.codInfBasicaDet = this.codigoAcordeonAspectosSocioeconomico)
          );
          if (list && list.length > 0) {
            let infoBasica = list[0];
            this.selectAnexo4_5_2 = infoBasica.anexo || "N";

            Object.assign(this.infoBasica_3_5, infoBasica);
            //this.listInfoBasicaDet_4_7_1 = list.filter((x: any) => x.codSubInfBasicaDet == this.codigoTabla1);

            let listCabecera = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTablaCabecera
            );
            if (listCabecera && listCabecera.length > 0) {
              this.infoBasicaDet_3_5_cab = listCabecera[0];
            }
            this.listInfoBasicaDet_3_5_1 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla1
            );
            this.listInfoBasicaDet_3_5_2 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla2
            );

            this.listInfoBasicaDet_3_5_1.forEach((eve: any) => {
              eve.coordenadaEsteIni = eve.coordenadaEste;
              eve.coordenadaNorteIni = eve.coordenadaNorte;
            });

            this.listInfoBasicaDet_3_5_2.forEach((eve: any) => {
              eve.coordenadaEsteIni = eve.coordenadaEste;
              eve.coordenadaNorteIni = eve.coordenadaNorte;
            });

            this.getGeneralFiltesAccordeon3_5();
          } else {
            this.getGeneralFiltesAccordeon3_5();
          }
        }
      });
  }


  getGeneralFiltesAccordeon3_5() {
    if (!this.listInfoBasicaDet_3_5_1.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: this.codigoTabla1,
        codCabecera: this.codigoAcordeonAspectosSocioeconomico,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: element.idInfBasicaDet,
              codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
              codSubInfBasicaDet: this.codigoTabla1,
              idUsuarioRegistro: this.user.idUsuario,
              descripcion: element.descripcion,
              coordenadaEste: element.coordenadaEste,
              coordenadaNorte: element.coordenadaNorte,
              nombre: element.nombre,
              areaHa: element.areaHa,
              numeroFamilia: element.numeroFamilia,
              actividad: element.actividad,
              subsistencia: element.subsistencia,
              perenne: element.perenne,
              ganaderia: element.ganaderia,
              caza: element.caza,
              pesca: element.pesca,
              madera: element.madera,
              otroProducto: element.otroProducto

            });

            this.listInfoBasicaDet_3_5_1.push(obj);
          });
        });
    }

    if (!this.listInfoBasicaDet_3_5_2.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: this.codigoTabla2,
        codCabecera: this.codigoAcordeonAspectosSocioeconomico,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: element.idInfBasicaDet,
              codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
              codSubInfBasicaDet: this.codigoTabla2,
              idUsuarioRegistro: this.user.idUsuario,
              descripcion: element.descripcion,
              coordenadaEsteIni: element.coordenadaEste,
              coordenadaNorteIni: element.coordenadaNorte,
              zonaVida: element.zonaVida,
              nombre: element.nombre,
              acceso: element.acceso
            });

            this.listInfoBasicaDet_3_5_2.push(obj);
          });
        });
    }

  }

  validarCheckUF(item: any) {
    this.listaFisiograficas.find((x) => {
      if (item.idUnidad === x.idUnidad && item.accion == false) {


        item.area = 0;
        item.porcentaje = 0;

      }
    });


    this.calculateAreaTotalUF();

  }



  registrarArchivoAdjunto(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.serviceArchivo.cargar(this.usuarioM.idusuario, this.codigosFile.PGMF_TAB3_5_2, file.file).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {

          this.archivoAdjuntoContrato.idArchivo = result.data;
          this.registrarArchivo(result.data);
        } else {
          this.toast.warn(result.message);
          this.archivoAdjuntoContrato = {};
          //this.limpiarFileRegFallo(tipo);
        }
      },
      () => {
        //this.limpiarFileRegFallo(tipo);
        this.dialog.closeAll();
        this.toast.error();
        this.archivoAdjuntoContrato = {};
      }
    );
  }

  registrarArchivo(id: number) {
    var params = {
      idPlanManejoArchivo: this.idPGMF,
      codigoTipoPGMF: this.codigosFile.PGMF_TAB3_5_2,
      codigoSubTipoPGMF: null,
      descripcion: null,
      observacion: null,
      idPlanManejo: this.idPlanManejo,
      idArchivo: id,
      idUsuarioRegistro: this.user.idUsuario,
    };


    this.servicePostulacionPFDM
      .registrarArchivoDetalle(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.disableSubir = true;
          this.toast.ok('Se adjuntó la ampliación del Anexo correctamente.');

          //this.listarArchivoPMFI();
        } else {
          this.toast.error(response?.message);
        }
      });
    //this.deshabilita = false;
  }

  eliminarArchivoGeneral() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    // this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
    this.servicePostulacionPFDM.eliminarArchivoDetalle(this.archivoAdjuntoContrato.idArchivo, this.user.idUsuario).subscribe((response: any) => {
      this.dialog.closeAll()
      if (response.success == true) {

        this.toast.ok("Se eliminó el Anexo correctamente.");
        this.disableSubir = false;
        this.archivoAdjuntoContrato = {}
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }

    }, () => {
      this.dialog.closeAll()
    });
  }

  listarArchivoPMFI() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(this.idPlanManejo, this.codigosFile.PGMF_TAB3_5_2)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        
        if (response.data && response.data.length != 0) {
          this.disableSubir = true;
          this.archivoAdjuntoContrato = response.data[response.data.length - 1];

        }
      });
  }
  /********************3.6*********************** */
  /********************3.6.1*********************** */
  listaRequerimientoMaquinaria: any[] = [];
  listaRequerimientoPersonal: any[] = [];
  objUser: UsuarioModel = JSON.parse('' + localStorage.getItem("usuario")?.toString());
  listaAntecedentes: any[] = [];
  listaTipos_fr: any[] = [];
  listaSubTipos_fr() {
    this.listaTipos_fr = [];
    let contTipos = -1;
    
    this.listaAntecedentes.forEach((element: any) => {
      if (element.esCabecera == true) {
        contTipos++;
        this.listaTipos_fr.push(element);
      }
      else {
        //if( this.listaTipos_fr[contTipos].subTipo == element.subTipo )
        this.listaTipos_fr[contTipos].items.push(
          { f: element.funcion, d: element.descripcion, i: element.id, n: element.numero }
        );

      }
    });

  }






  editar(data: any, indice: number) {

    let params = { idPGMF: this.idPGMF, data: data, indice: indice };

    const ref = this.dialogService.open(ActividadesComponent, {
      header: 'Editar Actividad',
      width: '70%',
      data: params
    });

    ref.onClose.subscribe((request: any) => {
      //console.log(reject);
      if (request?.reject == 'ok') {
        
        data.especieExtraida = request.especieExtraida;
        data.observaciones = request.observacion;
        if (indice >= 4) {
          data.actividad = request.actividad;
        }
        this.toast.ok('Se editó el antecedente de Uso de Recursos correctamente');


        //this.obtenerOrganizacionManejo();
      }
      // else{
      //   this.toast('error','Ocurrió un problema');
      // }
    });
  }

  /********************3.6*********** */
  listarAntecedentesUsoIdentificacion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_5: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAntecedentesUsoIdentificacionConflictos,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_5)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          let list = response.data.filter(
            (x: any) =>
              (x.codInfBasicaDet = this.codigoAcordeonAntecedentesUsoIdentificacionConflictos)
          );
          if (list && list.length > 0) {
            let infoBasica = list[0];
            this.selectAnexo4_6_1 = infoBasica.anexo || "N";

            Object.assign(this.infoBasica_3_6, infoBasica);
            //this.listInfoBasicaDet_4_7_1 = list.filter((x: any) => x.codSubInfBasicaDet == this.codigoTabla1);

            let listCabecera = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTablaCabecera
            );
            if (listCabecera && listCabecera.length > 0) {
              this.infoBasicaDet_3_6_cab = listCabecera[0];
            }
            this.listInfoBasicaDet_3_6_1 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla1
            );
            this.listInfoBasicaDet_3_6_1.forEach((eve: any) => {
              eve.coordenadaEsteIni = eve.coordenadaEste;
              eve.coordenadaNorteIni = eve.coordenadaNorte;
            });



            this.getGeneralFiltesAccordeon3_6();
          } else {
            this.getGeneralFiltesAccordeon3_6();
          }
        }
      });

  }


  getGeneralFiltesAccordeon3_6() {
    if (!this.listInfoBasicaDet_3_6_1.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: this.codigoTabla1,
        codCabecera: this.codigoAcordeonAntecedentesUsoIdentificacionConflictos,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: 0,
              codInfBasicaDet: this.codigoAcordeonAntecedentesUsoIdentificacionConflictos,
              codSubInfBasicaDet: this.codigoTabla1,
              idUsuarioRegistro: this.user.idUsuario,
              descripcion: element.descripcion,
              coordenadaEste: element.coordenadaEste,
              coordenadaNorte: element.coordenadaNorte,
              nombre: element.nombre,
              areaHa: element.areaHa,
              numeroFamilia: element.numeroFamilia,
              actividad: element.actividad,
              subsistencia: element.subsistencia,
              perenne: element.perenne,
              ganaderia: element.ganaderia,
              caza: element.caza,
              pesca: element.pesca,
              madera: element.madera,
              otroProducto: element.otroProducto,
              especieExtraida: element.especieExtraida,
              marcar: element.marcar

            });

            this.listInfoBasicaDet_3_6_1.push(obj);
          });
        });
    }

    
  }
  validarGuardar3_6_1(): boolean {
    let valido = true;
    if (this.selectAnexo4_6_1 === "S") {
      if (!this.fileAnexo4_6_1.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_6_1.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  registrarAntecedentesUsoIdentificacionConflictos(array: any) {
    if (!this.validarGuardar3_6_1()) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_3_6.idPlanManejo = this.idPlanManejo;
    this.infoBasica_3_6.anexo = this.selectAnexo4_6_1;
    this.infoBasica_3_6.listInformacionBasicaDet = [];
    this.infoBasica_3_6.listInformacionBasicaDet = [
      this.infoBasicaDet_3_6_cab,
    ].concat(array);

    this.informacionAreaPmfiService
      .registrarInformacionBasica([this.infoBasica_3_6])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok(
          'Se registró los Antecedentes de Uso e identificación de Conflictos correctamente.'
        );
        this.listarAntecedentesUsoIdentificacion();
      });
  }


  registrarArchivoAdjunto_3_6_1(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.serviceArchivo.cargar(this.usuarioM.idusuario, this.codigosFile.PGMF_TAB3_6_1, file.file).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {

          this.archivoAdjuntoAnexo_3_6_1.idArchivo = result.data;
          this.registrarArchivo_3_6_1(result.data);
        } else {
          this.toast.warn(result.message);
          this.archivoAdjuntoAnexo_3_6_1 = {};
          //this.limpiarFileRegFallo(tipo);
        }
      },
      () => {
        //this.limpiarFileRegFallo(tipo);
        this.dialog.closeAll();
        this.toast.error();
        this.archivoAdjuntoAnexo_3_6_1 = {};
      }
    );
  }

  listarArchivoPMFI_3_6_1() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(this.idPlanManejo, this.codigosFile.PGMF_TAB3_6_1)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        
        if (response.data && response.data.length != 0) {
          this.disableSubir_3_6_1 = true;
          this.archivoAdjuntoAnexo_3_6_1 = response.data[response.data.length - 1];

        }
      });
  }

  registrarArchivo_3_6_1(id: number) {
    var params = {
      idPlanManejoArchivo: this.idPGMF,
      codigoTipoPGMF: this.codigosFile.PGMF_TAB3_6_1,
      codigoSubTipoPGMF: null,
      descripcion: null,
      observacion: null,
      idPlanManejo: this.idPlanManejo,
      idArchivo: id,
      idUsuarioRegistro: this.user.idUsuario,
    };


    this.servicePostulacionPFDM
      .registrarArchivoDetalle(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.disableSubir_3_6_1 = true;
          this.toast.ok('Se adjuntó la ampliación de Anexo correctamente.');

          //this.listarArchivoPMFI();
        } else {
          this.toast.error(response?.message);
        }
      });
    //this.deshabilita = false;
  }

  eliminarAnexo_3_6_1() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    // this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
    this.servicePostulacionPFDM.eliminarArchivoDetalle(this.archivoAdjuntoAnexo_3_6_1.idArchivo, this.user.idUsuario).subscribe((response: any) => {
      this.dialog.closeAll()
      if (response.success == true) {

        this.toast.ok("Se eliminó la ampliación del anexo correctamente.");
        this.disableSubir_3_6_1 = false;
        this.archivoAdjuntoAnexo_3_6_1 = {}
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }

    }, () => {
      this.dialog.closeAll()
    });
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
