import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { UsuarioModel } from 'src/app/model/Usuario';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { ToastService } from '@shared';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls:['./actividades.component.scss']
})
export class ActividadesComponent implements OnInit {
  funcionObj: any;

  objUser : UsuarioModel = JSON.parse(''+localStorage.getItem("usuario")?.toString() );
  params_insetar_detalle = {
    codigoSeccion : "SECORGM",
    idOrganizacionManejo:  0,
    codigoSubTipoDetalle: '',
    codigoTipoDetalle: "",
    codigoTipoActividad: '',
    cabecera: false, tipoActividad : "",
    maquinaEquipo: "", 
    numero : 0,
    funcion: "", descripcion: "",
    medidaControl:"",
    medidaMonitoreo:"",
    frecuencia:"",
    responsable:"",
    contingencia:"",
    accionRealizada:"",
    nombreTipoDetalle:"",

    fecha: new Date(), idPGMF: 0,
    idUsuarioRegistro: this.objUser.idusuario,
    lugar: "",mecanismoParticipacion: "",
    metodologia: "", modalidadCapacitar: "",
    personalCapacitar: "", operacion :""
  }
  padre  : any; 
  actividad: string = '';
  observacion: string = '';
  especieExtraida: string = '';
  indice: number = 0;
  disabled: boolean = false;
  tipoActividad: any[] = [];
  request: any = {
    reject: '',
    actividad: '',
    especieExtraida: '',
    observacion: ''

  };
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private serv : PlanificacionService,
    private parametroValorService: ParametroValorService,
    private toast: ToastService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    

    this.padre = this.config.data;
    this.indice = this.config.data.indice;
    this.actividad = this.config.data.data.actividad;
    this.especieExtraida = this.config.data.data.especieExtraida;
    this.observacion = this.config.data.data.observaciones;
    this.disabled = this.indice >= 4? true: false;
    //console.log(this.padre)
  }

  listTipoActividad() {

  }

  

  guardar(){
    this.cerrarModal()
  }

  cerrarModal() {
    //if (this.ref) {
        this.request.reject = 'ok';
        this.request.actividad = this.actividad;
        this.request.especieExtraida = this.especieExtraida;
        this.request.observacion = this.observacion;
        this.ref.close( this.request);
    //}
  }

  close(){
    this.ref.close()
  }

  /*
  toast(saverty : String , datail: String){
    
    this.messageService.add({
      key: 'toast',
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }
  */
}
