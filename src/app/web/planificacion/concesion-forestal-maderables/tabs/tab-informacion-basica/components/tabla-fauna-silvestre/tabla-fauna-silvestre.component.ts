import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoPGMF } from 'src/app/model/CodigoPGMF';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { InformacionBasicaService } from 'src/app/service/informacion-basica.service';
import { ModalFaunaComponent } from '../../modal/modal-fauna/modal-fauna.component';

@Component({
  selector: 'app-tabla-fauna-silvestre',
  templateUrl: './tabla-fauna-silvestre.component.html',
  styleUrls: ['./tabla-fauna-silvestre.component.scss']
})
export class TablaFaunaSilvestreComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;

  ref!: DynamicDialogRef;
  selecFaunas: any[] = [];
  idInfBasica = 0;
  totalRecords: any = 0;
  optionPage = { pageNum: 1, pageSize: 10, };

  constructor(
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    public informacionBasicaService: InformacionBasicaService,
  ) { }

  ngOnInit(): void {
    this.listarFaunaSilvestre();
  }

  listarFaunaSilvestre() {
    const params = {
      codInfBasica: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPlanManejo,
      codInfBasicaDet: CodigoPGMF.ACORDEON_3_4_TAB_1,//CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS,
      pageNum: this.optionPage.pageNum,
      pageSize: this.optionPage.pageSize,
    };
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaService.listarPorFiltrosInfBasicaAerea(params).subscribe((response) => {
      load.close();
      if (response.success && response.data.length > 0) {
        this.selecFaunas = response.data.filter((element: any) => element.idFauna);
        this.idInfBasica = this.selecFaunas[0].idInfBasica;
        this.totalRecords = response.totalRecord;
      } else {
        this.toast.warn(response.message);
      }
    }, () => {
      load.close();
    });
    // this.informacionAreaPmfiService.listarInformacionBasica(params).subscribe((response: any) => {
    //   if (response.data) {
    //     this.selecFaunas = response.data.filter((element: any) => element.idFauna);
    //     this.idInfBasica = this.selecFaunas[0].idInfBasica; 
    //     console.log("this.selecFaunas: ", this.selecFaunas);
    //   }
    // });
  }

  loadData(e: any) {
    this.optionPage = { pageNum: e.first + 1, pageSize: e.rows, };
    this.listarFaunaSilvestre();
  }

  openModalFauna = () => {
    this.ref = this.dialogService.open(ModalFaunaComponent, {
      header: "Especies Fauna",
      width: "70%",
      contentStyle: { overflow: "auto" },
      data: { idPlanManejo: this.idPlanManejo, idUser: this.user.idUsuario, idInfBasica: this.idInfBasica }
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) this.listarFaunaSilvestre();
    });
  };
}
