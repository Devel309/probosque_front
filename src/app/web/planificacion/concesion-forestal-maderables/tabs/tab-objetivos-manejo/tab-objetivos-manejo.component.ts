import { Component, Input, OnChanges, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PlanManejoCcnnService } from 'src/app/service/planManejoCcnn.service';
import { ObjetivoManejoModel } from '../../../../../model/ObjetivoManejo';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { ObjetivoEspecificoManejoModel } from '../../../../../model/ObjetivoEspecificoManejo';
import { PlanManejoModel } from '../../../../../model/PlanManejo';
import { ParametroModel } from '../../../../../model/Parametro';
import { AnexoAdjuntoModel } from '../../../../../model/AnexoAdjunto';
import { AnexoAdjuntoService } from 'src/app/service/AnexoAdjunto.service';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { LineamientoSharedComponent } from 'src/app/shared/components/lineamiento-shared/lineamiento-shared.component';
import { CodigoPGMF } from 'src/app/model/CodigoPGMF';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-tab-objetivos-manejo',
  templateUrl: './tab-objetivos-manejo.component.html'
})

export class TabObjetivosManejoComponent implements OnInit {
  @ViewChild(LineamientoSharedComponent) lineamientoSharedComponent!: LineamientoSharedComponent;
  lstObjetoEspecifico: any[] = [];
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();

  constructor(
    private formBuilder: FormBuilder,
    private servPlanManejoCcnn: PlanManejoCcnnService,
    private servCoreCentral: CoreCentralService,
    private messageService: MessageService,
    private servAnexoAdjunto: AnexoAdjuntoService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private postulacionPFDMService: PostulacionPFDMService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
  ) { }

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input() idPGMF!: number;
  @Input() codigoEstado!: string;

  //@Input() listaAnexoAdjunto!: any[];
  @Input() disabled: boolean = false;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  objetoManejo = {} as ObjetivoManejoModel;
  listObjetoManejo: ObjetivoManejoModel[] = [];

  objetoEspecificoManejo = {} as ObjetivoEspecificoManejoModel;
  listObjetoEspecificoManejo: ObjetivoEspecificoManejoModel[] = [];
  listObjeto: any[] = [];

  planManejo = {} as PlanManejoModel;
  parametro = {} as ParametroModel;
  valorOtro: string = '';
  checked: number = 0;
  valorAnexoAdjunto: string = 'N';
  anexoAdjunto = {} as AnexoAdjuntoModel;
  listaAnexoAdjunto: any[] = [];
  
  idPlanManejoArchivo: number = 0;
  codigo_anexo4: string = CodigoPGMF.ACORDEON_2_2;

  fileAnexo4: any = {};

  ngOnInit(): void {
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
    this.servAnexoAdjunto.listarAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {
      for (let item of result.data) {
        if (item.codigoTab == "PRUEBAJM") {
          this.valorAnexoAdjunto = (item.adjuntaAnexo == true) ? "S" : "N";
        }
      };
    });

    let idPlanManejo = localStorage.getItem('idPlanManejo');
    this.planManejo.idPlanManejo = Number(idPlanManejo);

    this.planManejo.idPlanManejo = this.idPGMF;//solo para pruebas
    let idObjManejo = localStorage.getItem('idObjManejo');
    this.objetoManejo.idObjManejo = Number(idObjManejo);

    if (
      this.planManejo.idPlanManejo == undefined ||
      this.planManejo.idPlanManejo > 0
    ) {
      this.obtenerObjetoManejo();
    } else {
      this.comboPorFiltroObjetoEspecifico();
    }
  }

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN4', this.lineamiento);
  }

  dataLineamientos(data: any) {
    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN4' }
    );
  }

  registrarObjetoManejo() {
    this.objetoManejo.planManejo = this.planManejo;
    this.objetoManejo.estado = 'A';
    this.objetoManejo.idUsuarioRegistro = 1;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPlanManejoCcnn
      .registrarObjetivoManejo(this.objetoManejo)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if(result.success) {
          this.objetoManejo = result.data;
          const listObjetoEspecificoManejo: ObjetivoEspecificoManejoModel[] = [];
  
          for (let item of this.listObjeto) {
            const objetoEspecificoManejo = {} as ObjetivoEspecificoManejoModel;
            for (let obj of this.lstObjetoEspecifico) {
              for (let item_ of obj.items) {
                if (item_.value == item) {
                  objetoEspecificoManejo.idTipoObjEspecifico = item;
                  objetoEspecificoManejo.desObjEspecifico = item_.label;
                  objetoEspecificoManejo.detalleOtro = '';
                  if (item == "11") {
  
                    objetoEspecificoManejo.detalleOtro = this.valorOtro;
                  }
  
                  objetoEspecificoManejo.objetivoManejo = this.objetoManejo;
                  objetoEspecificoManejo.estado = 'A';
                  objetoEspecificoManejo.idUsuarioRegistro = 1;
                  listObjetoEspecificoManejo.push(objetoEspecificoManejo);
                  break;
                }
              }
            }
          }
  
          this.servPlanManejoCcnn
            .registrarObjetivoEspecificoManejo(listObjetoEspecificoManejo)
            .subscribe((result: any) => {
              this.listObjetoEspecificoManejo = result.data;
              this.checked = 0;
              this.messageService.add({
                severity: 'success',
                summary: '',
                detail: 'Se actualizó Objetivos del Manejo',
              });
            });
        } else {
          this.toast.warn(result.message);
        }
      }, () => this.dialog.closeAll());
  }
  actualizarObjetoManejo() {
    this.objetoManejo.planManejo = this.planManejo;
    this.objetoManejo.estado = 'A';
    this.objetoManejo.idUsuarioModificacion = 1;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servPlanManejoCcnn.actualizarrObjetivoManejo(this.objetoManejo).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.success) {
        this.messageService.add({
          severity: 'success',
          summary: '',
          detail: 'Se actualizó Objetivos generales del Manejo',
        });
  
        this.objetoManejo = result.data;
  
        const listObjetoEspecificoManejo: ObjetivoEspecificoManejoModel[] = [];
  
        for (let item of this.listObjeto) {
          const objetoEspecificoManejo = {} as ObjetivoEspecificoManejoModel;
          for (let obj of this.lstObjetoEspecifico) {
            for (let item_ of obj.items) {
              if (item_.value == item) {
                objetoEspecificoManejo.idTipoObjEspecifico = item;
                objetoEspecificoManejo.desObjEspecifico = item_.label;
                objetoEspecificoManejo.detalleOtro = '';
                if (item == "11") {
                  objetoEspecificoManejo.detalleOtro = this.valorOtro;
                }
                objetoEspecificoManejo.idUsuarioModificacion = 1;
                objetoEspecificoManejo.objetivoManejo = this.objetoManejo;
                objetoEspecificoManejo.estado = 'A';
                objetoEspecificoManejo.idUsuarioRegistro = 1;
                listObjetoEspecificoManejo.push(objetoEspecificoManejo);
                break;
              }
            }
          }
        }
  
        this.servPlanManejoCcnn.actualizarObjetivoEspecificoManejo(listObjetoEspecificoManejo).subscribe((result: any) => {
          
          if(result.success){
            this.messageService.add({
              severity: 'success',
              summary: '',
              detail: 'Se actualizó Objetivos especificos del Manejo',
            });
          }else{
            this.messageService.add({
              severity: 'error',
              summary: '',
              detail: result.message,
            });
          }
        },(error) => {
          this.messageService.add({
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un error.',
          });
        });
      } else {
        this.toast.warn(result.message);
      }
    }, () => this.dialog.closeAll());
  }
  obtenerObjetoManejo() {
    this.objetoManejo.planManejo = this.planManejo;

    this.servPlanManejoCcnn
      .obtenerObjetivoManejo(this.objetoManejo)
      .subscribe((result: any) => {
        this.objetoManejo = result.data;
        if (this.objetoManejo.idObjManejo > 0) {
          this.servPlanManejoCcnn
            .obtenerObjetivoEspecificoManejo(this.objetoManejo)
            .subscribe((result: any) => {
              for (let item of result.data) {

                if (item.idTipoObjEspecifico == "11") {
                  this.valorOtro = item.detalleOtro;
                  this.checked = 1;
                }

                this.listObjeto.push(item.idTipoObjEspecifico);
              }
              this.comboPorFiltroObjetoEspecifico();
            });
        } else {
          this.comboPorFiltroObjetoEspecifico();
        }
      });
  }

  comboPorFiltroObjetoEspecifico() {
    this.parametro.codigo = 'OBJPGMF';

    this.servPlanManejoCcnn.comboPorFiltroObjetivoEspecifico(this.parametro).subscribe((result: any) => {
      
      if (result.data) {
        this.lstObjetoEspecifico = result.data;
      }
    }, (error) => {
      

    });
  }

  tableListBoxSelectEvent(event: any) {
    let count = 0;
    let count2 = 0;
    let data = event.value;
    for (let key of data) {

      if ("11" == key) {
        this.checked = 1;
        count++;
      }
    }
    if (count == 0) {
      this.checked = 0;
      this.valorOtro = "";
    }
  }
  GuardarObjetoManejo() {
    /*********************/
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
    this.anexoAdjunto.codigoTab = "PRUEBAJM";
    this.anexoAdjunto.adjuntaAnexo = (this.valorAnexoAdjunto == "S") ? true : false;
    this.anexoAdjunto.idUsuarioRegistro = 33;

    this.servAnexoAdjunto.marcarParaAmpliacionAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {});

    if (!this.validarGuardar()) return;

    if (this.objetoManejo.idObjManejo == undefined || this.objetoManejo.idObjManejo == 0) {
      this.registrarObjetoManejo();
    } else {
      this.actualizarObjetoManejo();
    }
  }

  validarGuardar(): boolean{
    let valido = true;
    if (this.listObjeto == null || this.listObjeto.length == 0) {
      valido = false;
      this.toast.warn("Debe seleccionar Objetivo Específico.");
    }
    
    if (this.valorAnexoAdjunto === "S") {
      if (!this.fileAnexo4.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.valorAnexoAdjunto = "S";
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      if(response.success) this.limpiarFile();
    });
  }

  limpiarFile() {
    this.idPlanManejoArchivo = 0;
    this.fileAnexo4.nombreFile = "";
    this.fileAnexo4.justificacion = "";
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  onConfirm() {
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }
}
