
import {HttpErrorResponse, HttpParams} from '@angular/common/http';
import { Component, ViewChild, ElementRef, Renderer2, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { UsuarioModel } from 'src/app/model/Usuario';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import {UrlFormatos} from '../../../../../../model/urlFormatos';
import {LoadingComponent} from '../../../../../../components/loading/loading.component';
import {finalize, tap} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {ToastService} from '@shared';
import {OrdenamientoInternoPmfiService, UsuarioService} from '@services';
import {SimpleOuterSubscriber} from 'rxjs/internal/innerSubscribe';
import {isElementScrolledOutsideView} from '@angular/cdk/overlay/position/scroll-clip';
import {CodigosPGMF} from '../../../../../../model/util/PGMF/CodigosPGMF';
import {CodigoProceso} from '../../../../../../model/util/CodigoProceso';
import { PlantillaConst } from 'src/app/shared/plantilla.const';


@Component({
  selector: "app-proteccion-vigilancia",
  templateUrl: "../protecion-vigilancia/protecion-vigilancia.component.html"
})
export class ProteccionVigilanciaComponent implements OnInit {
  /*@Output() listUbicacionMarcadoVertices: EventEmitter<any> = new EventEmitter();
  @Output() listSenalizacion: EventEmitter<any> = new EventEmitter();
  @Output() listDemarcacionLinderos: EventEmitter<any> = new EventEmitter();
  @Output() listVigilancia: EventEmitter<any> = new EventEmitter();
 */

  @Input() disabled: boolean = false;
  @Input() isDisbledObjFormu!: boolean;
  file: any;
  params = {
    idPlanManejo: 0,
    codigoTipoDetalle: '',
    idUsuarioRegistro: 1
  }
  @Input() idPGMF!: number;
  objUser: UsuarioModel = JSON.parse('' + localStorage.getItem("usuario")?.toString());
  ubicacionMarcadoVertices: any[] = []; //4.3.1
  senalizacion: any[] = []; //4.3.2
  demarcacionLinderos: any[] = []; //4.3.3
  vigilancia: any[] = []; //4.3.4
  procesando: boolean = false;

  CodigosPGMF = CodigosPGMF;

  UrlFormatos = UrlFormatos;

  plantilla_4_3_1: string = PlantillaConst.PGMF_TAB_4_3_1;
  plantilla_4_3_2: string = PlantillaConst.PGMF_TAB_4_3_2;
  plantilla_4_3_3: string = PlantillaConst.PGMF_TAB_4_3_3;
  plantilla_4_3_4: string = PlantillaConst.PGMF_TAB_4_3_4;

  nombrefile = "";
  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private user: UsuarioService,
    private servPlanifiacion: PlanificacionService,
    private messageService: MessageService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService) { };

  ngOnInit() {
    this.listarOrdenamiento();
    //this.protecionVigilanciaObtener('ORDUBI');
  };

  /*elegirSeccion(e: any) {
    let tipo = '';
    if (e.index == 0) {
      tipo = 'ORDUBI';
      this.protecionVigilanciaObtener(tipo);
    }
    if (e.index == 1) {
      tipo = 'ORDSEN';
      this.protecionVigilanciaObtener(tipo);
    }
    if (e.index == 2) {
      tipo = 'ORDEMALI';
      this.protecionVigilanciaObtener(tipo);
    }
    if (e.index == 3) {
      tipo = 'ORVIGUMF';
      this.protecionVigilanciaObtener(tipo);
    }
  }
*/
  listarOrdenamiento() {
    this.ubicacionMarcadoVertices = [];
    this.senalizacion = [];
    this.demarcacionLinderos = [];
    this.vigilancia = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codTipoOrdenamiento: 'PGMF',
    };

    this.apiOrdenamiento.listarOrdenamientoDetalle(params).subscribe(
      (response: any) => {


        response.data[0].listOrdenamientoProteccionDet.forEach((element: any) => {
            const item = element;
            let detalle = {
              idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
              codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
              verticeBloque: item.verticeBloque,
              coordenadaEste: item.coordenadaEste,
              coordenadaNorte: item.coordenadaNorte,
              zonaUTM: item.zonaUTM,
              actividad: item.actividad,
              descripcion: item.descripcion,
              observacion: item.observacion
            };

            if(detalle.codigoTipoOrdenamientoDet == CodigosPGMF.PGMF_TAB_4_3_1){
              this.ubicacionMarcadoVertices.push(detalle);
            }else if(detalle.codigoTipoOrdenamientoDet == CodigosPGMF.PGMF_TAB_4_3_2){
              this.senalizacion.push(detalle);
            }else if(detalle.codigoTipoOrdenamientoDet == CodigosPGMF.PGMF_TAB_4_3_3){
              this.demarcacionLinderos.push(detalle);
            }else if(detalle.codigoTipoOrdenamientoDet == CodigosPGMF.PGMF_TAB_4_3_4){
              this.vigilancia.push(detalle);
            }


          }
        );
      }
    );
  }

  /*protecionVigilanciaObtener(tipo: string) {
    this.params.idPlanManejo = this.idPGMF;
    this.params.codigoTipoDetalle = tipo;

    this.servPlanifiacion.protecionVigilanciaObtener(this.params).subscribe(
      (result: any) => {
        
        if (result.success == true || this.params.codigoTipoDetalle == 'ORDUBI') {
          this.ubicacionMarcadoVertices = result.data;
          this.listUbicacionMarcadoVertices.emit(this.ubicacionMarcadoVertices);
        }
        if (result.success == true || this.params.codigoTipoDetalle == 'ORDSEN') {
          this.senalizacion = result.data;
          this.listSenalizacion.emit(this.senalizacion)
        }
        if (result.success == true || this.params.codigoTipoDetalle == 'ORDEMALI') {
          this.demarcacionLinderos = result.data;
          this.listDemarcacionLinderos.emit(this.demarcacionLinderos)
          
        }
        if (result.success == true || this.params.codigoTipoDetalle == 'ORVIGUMF') {
          this.vigilancia = result.data;
          this.listVigilancia.emit(this.vigilancia)
          
        }
      }
    );;
  }*/

  onFileChange(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        let i = 0;
        this.nombrefile = e.srcElement.files[0].name;
        this.file = e.srcElement.files[0];
        
        // while (i < e.srcElement.files.length) {
        //   i++;
        // }
      }
    }
  }


  cargarFormato(files: any, codigoAcordeon: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "orden",
          numeroFila: 5,
          numeroColumna: 1,
          codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
          subCodTipoOrdenamiento : CodigosPGMF.PGMF_TAB_4,
          codigoTipoOrdenamientoDet: codigoAcordeon,
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: this.user.idUsuario
        };


        this.dialog.open(LoadingComponent, { disableClose: true });

        this.apiOrdenamiento.proteccionVigilanciaCargarExcel(
          t.file, item.nombreHoja, item.numeroFila,
          item.numeroColumna, item.codTipoOrdenamiento,item.subCodTipoOrdenamiento,
          item.codigoTipoOrdenamientoDet, item.idPlanManejo, item.idUsuarioRegistro
        )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
              if (res.success == true) {
                this.toast.ok(res?.message);
                this.listarOrdenamiento();
              } else {
                this.toast.error(res?.message);
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }


  enviarArchivo() {
    this.procesando = true;
    var params = new HttpParams()
      .set('idPlanManejo', '21')
      .set('codigoTipoDetalle', 'ORDUBI')
      .set('idUsuarioRegistro', '1')


    const formData = new FormData();

    formData.append('file', this.file);

    this.servPlanifiacion.protecionVigilanciaArchivo(formData, params).subscribe(
      (result: any) => {
        
        if (result.success == true) {
          //this.protecionVigilanciaObtener('ORDUBI');
          this.procesando = false;
          //let vectorResult = result.data.split('-');
          // let mensaje = "";
          // vectorResult.forEach((element: string) => {
          //   if(element!=""){
          //     mensaje += "<p>"+ element + "</p>"
          //   }
          // });
          //this.toast("success",mensaje);
        }
        else {
          this.toast.error("Ocurrió un problema");
        }
      }
    );
  }

  descargarPlantilla() {
    this.servPlanifiacion.descargarguianexo4().subscribe(
      (result: any) => {
        if (result.success) {
          let p_file = this.base64ToArrayBuffer(result.archivo);
          var blob = new Blob([p_file], {
            type: result.contenTypeArchivo,
          });
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          var fileName = result.nombeArchivo;
          link.download = fileName;
          link.click();
        } else {
          this.messageService.add({
            key: 'an4',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      }
    );
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }



}
