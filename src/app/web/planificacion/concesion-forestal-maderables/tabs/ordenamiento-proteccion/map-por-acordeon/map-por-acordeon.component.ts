import { HttpErrorResponse } from '@angular/common/http';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {ArchivoService, OrdenamientoInternoPmfiService, UsuarioService} from '@services';
import {downloadBlobFile, DownloadFile, MapApi, PGMFArchivoTipo, RespuestaTipo, ToastService} from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { from } from 'rxjs';
import {concatMap, finalize, tap} from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ModalFormularioOrdenInternoComponent} from '../../../../plan-general-manejo/tabs/tab-ordenamiento-area-manejo/modal-formulario-orden-interno/modal-formulario-orden-interno.component';
import {CodigosPGMF} from '../../../../../../model/util/PGMF/CodigosPGMF';
import {CodigoProceso} from '../../../../../../model/util/CodigoProceso';

@Component({
  selector: 'app-map-por-acordeon',
  templateUrl: './map-por-acordeon.component.html',
  styleUrls: ['./map-por-acordeon.component.scss'],
})
export class MapPorAcordeonComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private user: UsuarioService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    public dialogService: DialogService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private toast: ToastService,
  ) {}

  selectAnexo:string = 'N';

  idPlanManejoArchivo: number = 0;


  @ViewChild('map2', { static: true }) private mapViewEl!: ElementRef;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() isShow?: boolean = true;
  //@Output() areaTotal = new EventEmitter<number>();
  @Input() cambioV?: boolean = false;
  @Input() pcV!: string;
  @Input() eventV?: any;
  @Input() idPGMF!: number;
  @Input() isDisbledObjFormu!: boolean;
  //@Output() deleteLayer = new EventEmitter();
  public view: any = null;
  public geoJsonLayer: any = null;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  idPlanManejo!: number;


  //@ViewChild(AppMapaComponent) map!: AppMapaComponent;
  listOrdenInterno: any[] = [];
  RespuestaTipo = RespuestaTipo;
  isEditedMap = false;
  ref!: DynamicDialogRef;
  relacionArchivo!: { idArchivo: number; idPlanManejoArchivo: number };
  cabecera: any;
  PGMF_CON_COBERTURA = "PGMFCCB";
  PGMF_SIN_COBERTURA = "PGMFSCB";
  colorActual :string = "";
  CodigosPGMF = CodigosPGMF;

  fileAnexo4_1:any = {};

  ngOnInit(): void {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.initializeMap();
    this.obtenerCapas();
    this.listarOrdenamiento();
  }

  ngOnChanges() {
    if (this.cambioV) {
      let files: any[] = [];
      files.push(this.eventV);
      this.onChangeFile2(files, this.pcV);
    }
  }

  onChangeFile2(files: any, pc: string) {
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      descripcion: pc,
    };
    if (files.length) {
      let i = 0;
      while (i < files.length) {
        let controls = this.mapApi.validateFileInputSHP(files);
        if (controls.success == false) {
          return;
        } else {
          this.processFile(files[i], config);
        }
        i++;
      }
    }
  }

  /*get totalArea() {

  }*/
  totalArea :number = 0;
  totalAreaPorcentaje :number = 0;

  calcularTotal() {
    this.totalArea = 0;
    this.totalAreaPorcentaje = 0;
    this.listOrdenInterno.forEach((e:Ordenamiento) => {
        //let num :number = 0;
        if(e.areaHA) {
          this.totalArea = this.totalArea + e.areaHA;
          //e.areaHAPorcentaje = e.areaHA / this.totalArea;
        }
    })

    //calculo de porcentale
    this.listOrdenInterno.forEach((e:Ordenamiento) => {
      //let num :number = 0;
      if(e.areaHA) {
        e.areaHAPorcentaje = e.areaHA / this.totalArea;
        this.totalAreaPorcentaje = this.totalAreaPorcentaje + Number(e.areaHAPorcentaje) ;

      }
    })

  }




  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }

  saveFileRelation(result: any, item: any) {
    /*let codigoTipo: any = PGMFArchivoTipo.PGMFA;
    if (this.codigoSubSeccion == 'POCCIBPCUEPC') {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion == 'PGMFIFPM' || this.codigoSubSeccion == 'PGMFIFRF') {
      codigoTipo = this.codigoSubSeccion;
    }*/
    //let codigoTipo = this.codigoSubSeccion




    let item2 = {
      codigoTipoPGMF: this.codigoProceso,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
    };

    //como ya obtengo el archivo lo seteo el mismo a uno de la tabla
    let ordenamie = this.listOrdenInterno.find((e:any)=>e.actividad == item.idLayer);
    if(ordenamie ) {
      ordenamie.tipoBosque = item2.idArchivo;
    }

    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));

  }

  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: this.tipoGeometria,
        codigoGeometria: itemFile.idLayer,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }

  listarOrdenamiento() {
    this.listOrdenInterno = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codTipoOrdenamiento: 'PGMF',
    };

    this.apiOrdenamiento.listarOrdenamientoDetalle(params).subscribe(
      (response: any) => {
        this.cabecera = {
          idOrdenamientoProteccion: response.data[0].idOrdenamientoProteccion,
          codTipoOrdenamiento: response.data[0].codTipoOrdenamiento,
          idPlanManejo: response.data[0].idPlanManejo,
        };

        this.totalArea = 0;
        this.totalAreaPorcentaje = 0;
        response.data[0].listOrdenamientoProteccionDet.forEach(
          (element: any) => {
            const item = element;
            // let coberBoscosa = ;
            if(item.codigoTipoOrdenamientoDet == CodigosPGMF.PGMF_TAB_4_1) {
              this.selectAnexo = item.accion ? "S" : "N";
              let detalle = {
                idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet==null?0:item.idOrdenamientoProteccionDet,
                codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
                categoria: item.categoria,
                areaHA: item.areaHA,
                areaHAPorcentaje: item.areaHAPorcentaje,
                observacion: item.observacion, //campo para saber si tiene asociado un shapefile
                descripcion: item.descripcion, //campo para guardar nombres de las capas
                actividad:   item.actividad,      //campo para guardar layerId (capa mapa)
                actividadesRealizar: item.actividadesRealizar, //campo para guardar groupId (grupo capa mapa)
                observacionDetalle: item.observacionDetalle, //campo para guardar color capa
                zonaUTM:  item.zonaUTM,
                tipoBosque: item.tipoBosque,
                accion:item.accion
              };

              this.listOrdenInterno.push(detalle);

              if ( detalle.areaHA ) {
                this.totalArea           = this.totalArea + detalle.areaHA;
                this.totalAreaPorcentaje = this.totalAreaPorcentaje + detalle.areaHAPorcentaje;
              }

            }
          }
        );
      }
    );
  }

  obtenerCapas() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = t.idArchivo;
              //this._layer.idLayer = this.mapApi.Guid2.newGuid;
              this._layer.idLayer = t.codigoGeometria;
              this._layer.inServer = true;
              this._layer.service = false;
              this._layer.nombre = t.nombreCapa;
              this._layer.groupId = groupId;
              this._layer.color = t.colorCapa;
              this._layer.annex = false;

              this._layers.push(this._layer);
              let geoJson = this.mapApi.getGeoJson(
                this._layer.idLayer,
                groupId,
                item
              );

              this.createLayer(geoJson);

            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje('Ocurrió un error');
      }
    );
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  onChangeFile(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {

            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }

  onChangeFileOrdenameinto(item: Ordenamiento, e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config, item);
          }
          i++;
        }
      }
      e.target.value = '';
    }

  }


  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;


  }


  onGuardarLayer() {
    if(this.isDisbledObjFormu) return 
    // let codigoTipo: any = this.codigoSubSeccion;
    //let codigoTipo: any = PGMFArchivoTipo.PGMFA;
    /*if (this.codigoSubSeccion == 'POCCIBPCUEPC') {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion == 'PGMFIFPM' || this.codigoSubSeccion == 'PGMFIFRF') {
      codigoTipo = this.codigoSubSeccion;
    }*/
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: this.codigoProceso,//this.codigoSubSeccion,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
          idLayer :t.idLayer
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, {disableClose: true});
    observer.pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => {
        this.dialog.closeAll();

        //console.log("Finaliza todos los archivos cargados...");
        this.cleanLayers();
        this.obtenerCapas();
      }))
      .subscribe(
        (result) => {
          //this.SuccessMensaje(result.message);
          //this.cleanLayers();
          //this.obtenerCapas();
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un error.');
        }
      );
    this.guardarSoloOrdenamiento();
  }

  onGuardarLayerLuegodeOrdenaMiento() {
    // let codigoTipo: any = this.codigoSubSeccion;
    //let codigoTipo: any = PGMFArchivoTipo.PGMFA;
    /*if (this.codigoSubSeccion == 'POCCIBPCUEPC') {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion == 'PGMFIFPM' || this.codigoSubSeccion == 'PGMFIFRF') {
      codigoTipo = this.codigoSubSeccion;
    }*/
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: this.codigoProceso,//this.codigoSubSeccion,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
          idLayer :t.idLayer
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      //this.ErrorMensaje('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, {disableClose: true});
    observer.pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => {
        this.dialog.closeAll();

        //console.log("Finaliza todos los archivos cargados...");
        this.cleanLayers();
        this.obtenerCapas();
      }))
      .subscribe(
        (result) => {
          //this.SuccessMensaje(result.message);
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un error.');
        }
      );
  }

  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      };
    });
  }



  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        //key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
         this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo)
            .pipe(finalize(() => {
              this.dialog.closeAll();
            }))
            .subscribe(
            (response: any) => {
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                
                this.mapApi.removeLayer(layer.idLayer, this.view);
                //this.deleteLayer.emit(layer);
                if (this._filesSHP.length === 0) {
                  this.cleanLayers();
                }

                let ordenamie = this.listOrdenInterno.find((e:any)=>e.actividad == layer.idLayer);
                if(ordenamie ){
                  ordenamie.observacionDetalle = null;
                  ordenamie.actividad =  null;
                  ordenamie.tipoBosque =  null;
                  ordenamie.observacion = RespuestaTipo.NO;
                  ordenamie.actividadesRealizar = null;
                  ordenamie.descripcion = null;
                  ordenamie.areaHA = 0;
                  ordenamie.areaHAPorcentaje = 0;
                  this.calcularTotal()
                  //setea lo valores a cero
                  this.guardarSoloOrdenamiento();
                }

                this.SuccessMensaje('El archivo se eliminó correctamente.');
              } else {
                this.ErrorMensaje('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error) => {
              this.ErrorMensaje('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      let ordenamie = this.listOrdenInterno.find((e:any)=>e.actividad == layer.idLayer);
      if(ordenamie ){
        ordenamie.observacionDetalle = null;
        ordenamie.actividad =  null;
        ordenamie.observacion = RespuestaTipo.NO;
        ordenamie.actividadesRealizar = null;
        ordenamie.tipoBosque =  null;
        ordenamie.descripcion = null;
        ordenamie.areaHA=0;
        ordenamie.areaHAPorcentaje=0;
        this.calcularTotal();
      }
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      //this.deleteLayer.emit(layer);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
      }
      this.SuccessMensaje('El archivo se eliminó correctamente.');
      this.guardarSoloOrdenamiento()
    }
  }


  onRemoveFileSHP_v2(layer: any) {
    if(this.isDisbledObjFormu) return 
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        //key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo)
            .pipe(finalize(() => {
              this.dialog.closeAll();
            }))
            .subscribe(
              (response: any) => {
                if (response.success) {
                  let ordenamie = this.listOrdenInterno.find((e:any)=>e.actividad == layer.idLayer);
                  if(ordenamie ){
                    ordenamie.observacionDetalle = null;
                    ordenamie.actividad =  null;
                    ordenamie.tipoBosque =  null;
                    ordenamie.observacion = RespuestaTipo.NO;
                    ordenamie.actividadesRealizar = null;
                    ordenamie.descripcion = null;
                    ordenamie.areaHA = 0;
                    ordenamie.areaHAPorcentaje = 0;
                    this.guardarSoloOrdenamientov2();
                  }

                  this.SuccessMensaje('El archivo se eliminó correctamente.');
                } else {
                  this.ErrorMensaje('No se pudo eliminar, vuelve a intertarlo.');
                }
              },
              (error) => {
                this.ErrorMensaje('Ocurrió un problema.' + error.statusText);
              }
            );
        },
      });
    } else {
      this.cleanLayers();
      this.obtenerCapas();
      this.listarOrdenamiento();

    }
  }

  onRemoveFileSHPSinComfirmacion(layer: any) {

    if (layer.inServer === true) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.eliminarArchivoDetalle(layer.codigo)
        .pipe(finalize(() => {
          this.dialog.closeAll();
        }))
        .subscribe(
          (response: any) => {
            if (response.success) {
              this.cleanLayers();
              this.obtenerCapas();
              this.listarOrdenamiento();
              this.SuccessMensaje('El archivo se eliminó correctamente.');
            } else {
              this.ErrorMensaje('No se pudo eliminar, vuelve a intertarlo.');
            }
          },
          (error) => {
            this.ErrorMensaje('Ocurrió un problema.' + error.statusText);
          }
        );

    } else {
      this.cleanLayers();
      this.obtenerCapas();
      this.listarOrdenamiento();

    }
  }


  openEliminar(event: Event, data: any, index: any): void {

   // console.log("data ",data);
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if (data.idOrdenamientoProteccionDet !== 0) {
          var parametros = {
            idOrdenamientoProtecccion: 0,
            idOrdenamientoProteccionDet: data.idOrdenamientoProteccionDet,
            codigoTipoOrdenamientoDet: '',
            idUsuarioElimina: this.user.idUsuario,
          };
          this.apiOrdenamiento
            .eliminarOrdenamiento(parametros)
            .subscribe((response: any) => {
              if (response.success) {
                this.toast.ok(
                  'Se eliminó el ordenamiento correctamente.'
                );
                this.listOrdenInterno.splice(index, 1);
              } else {
                this.toast.error(
                  'Ocurrió un problema, intente nuevamente',
                  'ERROR'
                );
              }
            });
        } else {
          this.listOrdenInterno.splice(index, 1);
          this.toast.ok(
            'Se eliminó el ordenamiento correctamente.'
          );
        }
        //this.calcularTotal();
        //console.log("DATA : ", data);
        let layer = this._layers.find(
          (t: any) => t.idLayer === data.actividad
        );
        if(layer)
        this.onRemoveFileSHPSinComfirmacion(layer);
        //this.removeLayer(data.actividadesRealizar);
      },
    });
  }

  removeLayer(id: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null) {
      this.view.map.layers.remove(layer);
      layer.visible = false;
      layer.forRemove = true;
    }
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }
  processFile(file: any, config: any,item?: Ordenamiento) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config, item);
     /* if (this.codigoSubSeccion === 'POCCIBPCUEPC' && !this.cambioV) {
        this.calculateArea(data[0], item);
      } else if (this.codigoSubSeccion === 'PGMFIFPM' || this.codigoSubSeccion === 'PGMFIFRF') {
        this.calculateArea(data[0], item);
      }else {
        this.calculateArea(data[0], item);
      }*/
      this.calculateArea(data[0], item);
    });
  }


  createLayers(layers: any, config: any,item?: Ordenamiento) {

    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      //this.colorActual = t.color;
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.idGroupLayer = config.idGroupLayer;
      t.groupId = this._id;

      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layer.descripcion = config.descripcion;

      if(item) {
        item.observacionDetalle = t.color;
        item.actividad =  t.idLayer;//se guarda el id del LAYER
        item.observacion = RespuestaTipo.SI;
        item.actividadesRealizar = config.idGroupLayer;
        item.descripcion = config.descripcion;
      }

      this._layers.push(this._layer);
      this.createLayer(t);
    });

    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    //if(item)
      this.file.idLayer = this._layer.idLayer;
    //else
     // this.file.idLayer = this._layer.idLayer

    this.file.idGroupLayer = config.idGroupLayer;
    this._filesSHP.push(this.file);
  }

  download(ordenamiento: Ordenamiento) {

    if(ordenamiento.tipoBosque){

      let idArchivo:number = Number(ordenamiento.tipoBosque);
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serviceArchivo.obtenerArchivo(idArchivo).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.file, result.data.nombre, 'application/octet-stream');
        }
        (error: HttpErrorResponse) => {this.ErrorMensaje(error.message);this.dialog.closeAll();};
      });

    }else{
      let nameFile = String(ordenamiento.descripcion).normalize('NFD').replace(/[\u0300-\u036f]/g, '');
      nameFile = nameFile.replace(/[^a-z0-9\s]/gi, '').replace(/[-\s]/g, '_');
      let file:any = this._filesSHP.find((item:any) => item.idLayer === ordenamiento.actividad);
      
      if(file){
        downloadBlobFile(file.file, file.file.name,
        );
      }
    }

  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => {});
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  calculateArea(data: any,item?: Ordenamiento) {
    let sumArea: number = 0;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    //let areaTotal = sumArea.toFixed(3);
    if(item) {
      item.areaHA = sumArea;
      this.calcularTotal();
      //this.totalArea = this.totalArea + areaTotal;
     // this.calculateTotalArea();
    }

    //this.areaTotal.emit(areaTotal);
  }

  guardarOrdenamiento() {
    if (!this.validarGuardarOrde()) return;

    let params: any = [];

    let anexoBool:boolean|null=null;
    if(this.selectAnexo == 'S'){
      anexoBool = true;
    }else if(this.selectAnexo == 'N'){
      anexoBool = false;
    }

    this.listOrdenInterno.forEach((e:any)=>{
      e.accion =anexoBool;
    });

    let param = {
      idOrdenamientoProteccion: this.cabecera.idOrdenamientoProteccion,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      anexo: null,
      idUsuarioModificacion: this.user.idUsuario,
      listOrdenamientoProteccionDet: this.listOrdenInterno
    };

    params.push(param);
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiOrdenamiento.registrarOrdenamiento(params).subscribe(
      (response: any) => {
        this.dialog.closeAll();
        if (response.success) {
          this.toast.ok('Se registró el Ordenamiento Interno correctamente.');
          this.listarOrdenamiento();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
      }, () => this.dialog.closeAll()
    );

    this.onGuardarLayerLuegodeOrdenaMiento();
  }

  validarGuardarOrde(): boolean{
    let valido = true;
    if (this.selectAnexo === "S") {
      if (!this.fileAnexo4_1.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_1.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.selectAnexo = "S";
  }

  anexoN() {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }

  eliminarPlanManejo() {
    const params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.servicePostulacionPFDM.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      this.fileAnexo4_1.nombreFile = "";
      this.fileAnexo4_1.justificacion = "";
    });
  }

  guardarSoloOrdenamiento() {
    let params: any = [];
    let param = {
      idOrdenamientoProteccion: this.cabecera.idOrdenamientoProteccion,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      idUsuarioModificacion: this.user.idUsuario,
      listOrdenamientoProteccionDet: this.listOrdenInterno
    };

    params.push(param);
    this.apiOrdenamiento.registrarOrdenamiento(params).subscribe(
      (response: any) => {
        if (response.success) {
          this.toast.ok('Se registró el Ordenamiento Interno correctamente.');
          this.listarOrdenamiento();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
      }
    );

  }


    guardarSoloOrdenamientov2(){
      let params: any = [];
      let param = {
        idOrdenamientoProteccion: this.cabecera.idOrdenamientoProteccion,
        codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
        idPlanManejo: this.idPGMF,
        idUsuarioRegistro: this.user.idUsuario,
        idUsuarioModificacion: this.user.idUsuario,
        listOrdenamientoProteccionDet: this.listOrdenInterno
      };

      params.push(param);
      this.apiOrdenamiento.registrarOrdenamiento(params).subscribe(
        (response: any) => {
          if (response.success) {
            this.toast.ok('Se registró el Ordenamiento Interno correctamente.');
            this.cleanLayers();
            this.obtenerCapas();
            this.calcularTotal();
          } else {
            this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
          }
        }
      );
    }


  getLastIndexCobertura(){
    let indice :number = -1;

    let index:number = 0;
    for (let obj of this.listOrdenInterno) {
      if(obj.zonaUTM  == this.PGMF_SIN_COBERTURA){
        indice = index;
        break;
      }
      index = index +1;
    }

    return indice;

  }


  openModal = (mesaje: string, data: any, tipo: any, index?: any) => {
    this.ref = this.dialogService.open(ModalFormularioOrdenInternoComponent, {
      header: mesaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: { data: data, type: tipo },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == 'C') {
          let ordenNueva = {
            idOrdenamientoProteccionDet: 0,
            codigoTipoOrdenamientoDet: CodigosPGMF.PGMF_TAB_4_1,
            categoria: resp.categoria || 'Sin Categoría',
            areaHA: 0.0,
            observacion: null, //tiene shapefile
            descripcion: '', //capas asociadas
            actividad: null, //layerId
            actividadesRealizar: null, // groupId
            observacionDetalle: null, //campo para guardar color capa
            zonaUTM :resp.zonaUTM
          };

          if(ordenNueva.zonaUTM == this.PGMF_SIN_COBERTURA){
            this.listOrdenInterno.push(ordenNueva);
          }else if(ordenNueva.zonaUTM == this.PGMF_CON_COBERTURA){
            //this.PGMF_SIN_COBERTURA
            let index = this.getLastIndexCobertura();
            this.listOrdenInterno.splice( index, 0, ordenNueva )
          }

          //this.listOrdenInterno.push(ordenNueva);
          //this.guardarOrdenInterno(ordenNueva);
        } else if (tipo == 'E') {
          this.listOrdenInterno[index].categoria = resp.categoria;
          this.listOrdenInterno[index].zonaUTM = resp.zonaUTM;
        }
      }
    });
  };
}

interface Ordenamiento {
  categoria: string;
  areaHA: number;
  areaHAPorcentaje: number;
  observacion: string;
  descripcion: string;
  actividad: string;
  actividadesRealizar: string;
  observacionDetalle: string;
  tipoBosque:string;
}

