import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrdenamientoInternoPmfiService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { concatMap, finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { CodigosPGMF } from 'src/app/model/util/PGMF/CodigosPGMF';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { MapCustomComponent } from 'src/app/shared/components/map-custom/map-custom.component';

@Component({
  selector: 'app-parcela-corta',
  templateUrl: './parcela-corta.component.html',
  styleUrls: ['./parcela-corta.component.scss']
})
export class ParcelaCortaComponent implements OnInit {

  constructor(private toast: ToastService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private dialog: MatDialog,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private confirmationService: ConfirmationService) { }
  @ViewChild("map") mapParcela!: MapCustomComponent;
  CodigosPGMF = CodigosPGMF;
  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() isDisbledObjFormu!: boolean;
  @Input() idUsuario!: number;
  @Input() idOrdenamientoProteccion!: number;
  totalAreaQuinquenalOpcion1: number = 0;
  totalPorcentajeQuinquenalOpcion1: number = 0;
  totalAreaQuinquenalOpcion2: number = 0;
  totalPorcentajeQuinquenalOpcion2: number = 0;
  bloqueQuinquenalOpcion1: any = [];
  bloqueQuinquenalOpcion2: any = [];
  opcionCheckParcela: boolean = true;
  valueCheckParcela!: string;
  listOrdenInterno: any = [];
  selectAnexo: string = 'S';
  opcion1: string = CodigosPGMF.PGMF_TAB_4_2_2 + 'O1';
  opcion2: string = CodigosPGMF.PGMF_TAB_4_2_2 + 'O2';
  bloque1: boolean = false;
  bloque2: boolean = false;
  idArchivo1 : number = 0;
  idArchivo2 : number = 0;

  idPlanManejoArchivo: number = 0;

  fileAnexo4_2_2:any = {};

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.getInitData();
  }

  getInitData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      ordenamiento: this.listarOrdenamiento()
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }
  onChangeFileSHP(e: any) {
    let opcion = e.target.dataset.zone;
    if (opcion === 'OPCION1') {
      this.valueCheckParcela = opcion;
      if (this.bloqueQuinquenalOpcion1.length >= 1) {
        this.toast.warn("Ya se encuentra datos registrados en la opción 1.");
        return;
      } else {
        this.mapParcela.onChangeFile(e);
      }
    } else if (opcion === 'OPCION2') {
      this.valueCheckParcela = opcion;
      if (this.bloqueQuinquenalOpcion2.length >= 1) {
        this.toast.warn("Ya se encuentra datos registrados en la opción 2.");
        return;
      } else {
        this.mapParcela.onChangeFile(e);
      }
    }
  }
  listarTipoBosquePorBQuinquenalParcelaOpcion2(items: any) {
    this.bloqueQuinquenalOpcion2 = [];
    
    if(!items.length){
      this.toast.warn('No se encontraron capas al identificar tipos de bosques para la Opción 2.');
    }else {
      this.bloqueQuinquenalOpcion2 = items;
      this.calculateAreaTotalQuinquenalParcelaOpcion2();
      this.bloque2 = true;
      this.toast.ok("Se encontraron capas de tipos de bosques para la la Opción 2.");
    }

  }

  listarTipoBosquePorBQuinquenalParcelaOpcion1(items: any) {
    this.bloqueQuinquenalOpcion1 = [];
    if(!items.length){
      this.toast.warn('No se encontraron capas al identificar tipos de bosques para la Opción 1.');
    }else {
      this.bloqueQuinquenalOpcion1 = items;
      this.calculateAreaTotalQuinquenalParcelaOpcion1();
      this.bloque1 = true;
      this.toast.ok("Se encontraron capas de tipos de bosques para la la Opción 1.");
    }

  }
  calculateAreaTotalQuinquenalParcelaOpcion1() {
    let sum1 = 0;
    for (let item of this.bloqueQuinquenalOpcion1) {
      item.tipoBosque.forEach((t: any) => {
        sum1 += t.areaHA;
      });
    }

    for (let item of this.bloqueQuinquenalOpcion1) {
      item.tipoBosque.forEach((t: any) => {

        t.areaHAPorcentaje = Number(((100 * t.areaHA) / sum1).toFixed(2));
      });

    }
    this.totalAreaQuinquenalOpcion1 = parseFloat(sum1.toFixed(2));
    this.totalPorcentajeQuinquenalOpcion1 = this.bloqueQuinquenalOpcion1.length === 0 ? 0 : parseFloat((100).toFixed(2));;
  }
  calculateAreaTotalQuinquenalParcelaOpcion2() {
    let sum1 = 0;
    for (let item of this.bloqueQuinquenalOpcion2) {
      item.tipoBosque.forEach((t: any) => {
        sum1 += t.areaHA;
      });
    }

    for (let item of this.bloqueQuinquenalOpcion2) {
      item.tipoBosque.forEach((t: any) => {

        t.areaHAPorcentaje = Number(((100 * t.areaHA) / sum1).toFixed(2));
      });

    }
    this.totalAreaQuinquenalOpcion2 = parseFloat(sum1.toFixed(2));
    this.totalPorcentajeQuinquenalOpcion2 = this.bloqueQuinquenalOpcion2.length === 0 ? 0 : parseFloat((100).toFixed(2));
  }
  onCheckboxChangeParcela(e: any) {
    if (e.target.checked) {
      this.opcionCheckParcela = true;
      this.valueCheckParcela = 'OPCION1'
    } else {
      this.opcionCheckParcela = false;
    }
  }
  onCheckboxChangeParcelaOpcion2(e: any) {
    if (e.target.checked) {
      this.opcionCheckParcela = true;
      this.valueCheckParcela = 'OPCION2'
    } else {
      this.opcionCheckParcela = false;
    }
  }

  guardar() {
    if (!this.validarGuardar()) return;

    if (this.validarIdArchivo()) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.guardarOrdenamiento()
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe();
    }
  }

  validarGuardar(): boolean{
    let valido = true;
    if (this.selectAnexo === "S") {
      if (!this.fileAnexo4_2_2.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_2_2.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.selectAnexo = 'S';
  }

  anexoN() {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }

  eliminarPlanManejo() {
    const params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.idUsuario,
    };
    this.servicePostulacionPFDM.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      this.fileAnexo4_2_2.nombreFile = "";
      this.fileAnexo4_2_2.justificacion = "";
    });
  }

  guardarOrdenamiento() {
    let listOrdenInterno:any = [];
    let params: any = [];
    this.bloqueQuinquenalOpcion1.forEach((t: any) => {
      t.tipoBosque.forEach((t2: any) => {
        let itemsTipoBosque = {
          idOrdenamientoProteccionDet: t.idOrdenamientoProteccionDet === null || t.idOrdenamientoProteccionDet === undefined   ? 0 : t.idOrdenamientoProteccionDet,
          codigoTipoOrdenamientoDet: this.opcion1,
          areaHA: t2.areaHA,
          porcentajeHA: t2.areaHAPorcentaje,
          bloqueQuinquenal: t.label,
          parcelaCorta: t.parcela,
          tipoBosque: t2.descripcion,
          observacion: this.selectAnexo,
          descripcion: 'OPCION1',
          idArchivo: this.idArchivo1
        };
        listOrdenInterno.push(itemsTipoBosque);

      });

    });
    this.bloqueQuinquenalOpcion2.forEach((t: any) => {
      t.tipoBosque.forEach((t2: any) => {
        let itemsTipoBosque = {
          idOrdenamientoProteccionDet: t.idOrdenamientoProteccionDet === null || t.idOrdenamientoProteccionDet === undefined   ? 0 : t.idOrdenamientoProteccionDet,
          codigoTipoOrdenamientoDet: this.opcion2,
          areaHA: t2.areaHA,
          porcentajeHA: t2.areaHAPorcentaje,
          bloqueQuinquenal: t.label,
          parcelaCorta: t.label,
          tipoBosque: t2.descripcion,
          observacion: this.selectAnexo,
          descripcion: 'OPCION2',
          idArchivo: this.idArchivo2
        };
        listOrdenInterno.push(itemsTipoBosque);

      });

    });
    let param = {
      idOrdenamientoProteccion: this.idOrdenamientoProteccion,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPGMF,
      anexo: null,
      idUsuarioRegistro: this.idUsuario,
      idUsuarioModificacion: this.idUsuario,
      listOrdenamientoProteccionDet: listOrdenInterno
    };
    params.push(param);
    return this.apiOrdenamiento.registrarOrdenamiento(params).pipe(
      tap((response: any) => {
        if (response.success) {
          this.toast.ok('Se registró el Ordenamiento Interno correctamente.');
          this.listarOrdenamiento().subscribe();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
      })
    );
  }
  listarOrdenamiento() {
    this.bloqueQuinquenalOpcion1 = [];
    this.bloqueQuinquenalOpcion2 = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
    };

    return this.apiOrdenamiento.listarOrdenamientoDetalle(params).pipe(
      tap((response: any) => {
        
        response.data[0].listOrdenamientoProteccionDet.forEach(
          (t: any) => {
            if (t.codigoTipoOrdenamientoDet === this.opcion1) {
              this.idArchivo1 = this.idArchivo1 == 0 ? t.idArchivo : this.idArchivo1;
              this.bloqueQuinquenalOpcion1.push({
                idOrdenamientoProteccionDet: t.idOrdenamientoProteccionDet,
                idOrdenamientoProteccion: response.data[0].idOrdenamientoProteccion,
                codigoTipoOrdenamientoDet: t.codigoTipoOrdenamientoDet,
                opcion: t.descripcion,
                label: t.bloqueQuinquenal,
                parcela: t.parcelaCorta,
                tipoBosque: [{
                  areaHA: t.areaHA,
                  porcentajeHA: t.areaHAPorcentaje,
                  descripcion: t.tipoBosque,
                }]
              });
              this.calculateAreaTotalQuinquenalParcelaOpcion1();
            } else if (t.codigoTipoOrdenamientoDet === this.opcion2) {
              this.selectAnexo = t.observacion || "N";
              this.idArchivo2 = this.idArchivo2 == 0 ? t.idArchivo : this.idArchivo2;
              this.bloqueQuinquenalOpcion2.push({
                idOrdenamientoProteccionDet: t.idOrdenamientoProteccionDet,
                idOrdenamientoProteccion: response.data[0].idOrdenamientoProteccion,
                codigoTipoOrdenamientoDet: t.codigoTipoOrdenamientoDet,
                opcion: t.descripcion,
                label: t.parcelaCorta,
                tipoBosque: [{
                  areaHA: t.areaHA,
                  porcentajeHA: t.areaHAPorcentaje,
                  descripcion: t.tipoBosque,
                }]
              });
              this.calculateAreaTotalQuinquenalParcelaOpcion2();
            }
          }
        );
      })
    );
  }
  deleteRegisterTable(opcion: any) {
    let parametros = {};
    if (opcion === 'OPCION1') {
      parametros = {
        idOrdenamientoProtecccion: this.idOrdenamientoProteccion,
        idOrdenamientoProteccionDet: 0,
        codigoTipoOrdenamientoDet: this.opcion1,
        idUsuarioElimina: this.idUsuario,
      };
    } else if (opcion === 'OPCION2') {
      parametros = {
        idOrdenamientoProtecccion: this.idOrdenamientoProteccion,
        idOrdenamientoProteccionDet: 0,
        codigoTipoOrdenamientoDet: this.opcion2,
        idUsuarioElimina: this.idUsuario,
      };
    }
    this.eliminarParcelaCorta(parametros,opcion);

  }
  eliminarParcelaCorta(item: any,opcion:any) {
    this.apiOrdenamiento
      .eliminarOrdenamiento(item)
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok(
            'Se eliminó el registro para la parcela seleccionada.'
          );
          if (opcion === 'OPCION1') {
            this.bloqueQuinquenalOpcion1 = [];
            this.calculateAreaTotalQuinquenalParcelaOpcion1();

          } else if (opcion === 'OPCION2') {
            this.bloqueQuinquenalOpcion2 = [];
            this.calculateAreaTotalQuinquenalParcelaOpcion2();
          }
        } else {
          this.toast.error(
            'Ocurrió un problema, intente nuevamente',
            'ERROR'
          );
        }
      });
  }
  eliminarParcelaCortaOnly(item:any){
    this.confirmationService.confirm({
      target: undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if (item[0].idOrdenamientoProteccionDet !== 0) {
          let parametros = {
            idOrdenamientoProtecccion: item[0].idOrdenamientoProteccion,
            idOrdenamientoProteccionDet: 0,
            codigoTipoOrdenamientoDet:item[0].codigoTipoOrdenamientoDet,
            idUsuarioElimina: this.idUsuario,
          };
          this.apiOrdenamiento
            .eliminarOrdenamiento(parametros)
            .subscribe((response: any) => {
              if (response.success) {
                this.toast.ok(
                  'Se eliminó el registro para la parcela seleccionada.'
                );
                if(item[0].opcion === 'OPCION1'){
                  this.bloqueQuinquenalOpcion1 = [];
                  this.calculateAreaTotalQuinquenalParcelaOpcion1();

                }else if(item[0].opcion === 'OPCION2'){
                  this.bloqueQuinquenalOpcion2 = [];
                  this.calculateAreaTotalQuinquenalParcelaOpcion2();
                }
              } else {
                this.toast.error(
                  'Ocurrió un problema, intente nuevamente',
                  'ERROR'
                );
              }
            });
        }
      },
    });
  }

  registroArchivo(idArchivo: number) {
    if (this.bloque1) {
      this.idArchivo1 = idArchivo;
    }
    if (this.bloque2) {
      this.idArchivo2 = idArchivo;
    }
  }

  validarIdArchivo() {
    let validar: boolean = true;

    if(this.bloque1 && this.idArchivo1 == 0){
      validar = false;
      this.toast.warn("(*) Debe guardar Archivo shapefile.")
    }
    if(this.bloque2 && this.idArchivo2 == 0){
      validar = false;
      this.toast.warn("(*) Debe guardar Archivo shapefile.")
    }

    return validar;
  }
}
