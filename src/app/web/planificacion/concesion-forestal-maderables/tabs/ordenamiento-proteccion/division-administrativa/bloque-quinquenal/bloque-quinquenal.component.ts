import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OrdenamientoInternoPmfiService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { concatMap, finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { CodigosPGMF } from 'src/app/model/util/PGMF/CodigosPGMF';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';

@Component({
  selector: 'app-bloque-quinquenal',
  templateUrl: './bloque-quinquenal.component.html',
  styleUrls: ['./bloque-quinquenal.component.scss']
})
export class BloqueQuinquenalComponent implements OnInit {

  constructor(
    private toast: ToastService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private servicePostulacionPFDM: PostulacionPFDMService
  ) { }

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() isDisbledObjFormu!: boolean;
  CodigosPGMF = CodigosPGMF;
  @Input() idUsuario!:number;
  @Input() idOrdenamientoProteccion!:number;

  idPlanManejoArchivo: number = 0;

  totalAreaQuinquenal: string = "";
  totalPorcentajeQuinquenal: number = 0;
  objBosque: any = {};
  bloques: any = [];
  cabecera:any={};
  listOrdenInterno:any = [];
  selectAnexo:string = 'N';
  totalRecords:number = 0;
  idArchivo : number = 0;

  fileAnexo4_2_1:any = {};

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.getInitData();
  }

  getInitData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      ordenamiento: this.listarOrdenamiento()
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }
  listarTipoBosquePorBQuinquenal(items: any) {
    
    let numBloque = items.groupBy((t: any) => t.label);
    this.objBosque.noBloques =numBloque.length;
    this.bloques = items;
    this.calculateAreaTotalQuinquenal();
  }
  calculateAreaTotalQuinquenal() {
    let sum1 = 0;
    for (let item of this.bloques) {
      item.tipoBosque.forEach((t: any) => {
        sum1 += t.areaHA;
      });
    }

    for (let item of this.bloques) {
      item.tipoBosque.forEach((t: any) => {

        t.areaHAPorcentaje = Number(((100 * t.areaHA) / sum1).toFixed(2));
      });

    }
    this.totalAreaQuinquenal = `${sum1.toFixed(2)}`;
    this.totalPorcentajeQuinquenal = this.bloques.length === 0 ? 0: parseFloat((100).toFixed(2));
  }

  guardar() {
    if (!this.validarGuardar()) return;

    if (this.validarIdArchivo()) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.guardarOrdenamiento()
        .pipe(concatMap(() => this.listarOrdenamiento()))
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe();
    }
  }

  validarGuardar(): boolean{
    let valido = true;
    if (this.selectAnexo === "S") {
      if (!this.fileAnexo4_2_1.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4_2_1.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.selectAnexo = 'S';
  }

  anexoN() {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }

  eliminarPlanManejo() {
    const params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.idUsuario,
    };
    this.servicePostulacionPFDM.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      this.fileAnexo4_2_1.nombreFile = "";
      this.fileAnexo4_2_1.justificacion = "";
    });
  }

  guardarOrdenamiento() {
    let params: any = [];
    this.listOrdenInterno = [];
    this.bloques.forEach((t:any) => {
      t.tipoBosque.forEach((t2:any) => {
        let itemsTipoBosque = {
          //idOrdenamientoProteccionDet: 0,
          idOrdenamientoProteccionDet: t.idOrdenamientoProteccionDet?t.idOrdenamientoProteccionDet:0,
          codigoTipoOrdenamientoDet: CodigosPGMF.PGMF_TAB_4_2_1,
          areaHA: t2.areaHA,
          categoria: "B",
          porcentajeHA: t2.areaHAPorcentaje,
          bloqueQuinquenal: t.label,
          parcelaCorta: 'PC 0',
          tipoBosque: t2.descripcion,
          observacion: this.selectAnexo,
          idArchivo: this.idArchivo
        };
        this.listOrdenInterno.push(itemsTipoBosque);

      });

    });

    let detalleQuinquenal = {
      idOrdenamientoProteccionDet: this.objBosque.idOrdenamientoProteccionDet,
      codigoTipoOrdenamientoDet: CodigosPGMF.PGMF_TAB_4_2_1,
      categoria: "A",
      descripcion: this.objBosque.noBloques,
      observacion: this.objBosque.anios
    }
    this.listOrdenInterno.push(detalleQuinquenal);


    let param = {
      idOrdenamientoProteccion: this.idOrdenamientoProteccion,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPGMF,
      anexo: null,
      idUsuarioRegistro: this.idUsuario,
      idUsuarioModificacion: this.idUsuario,
      listOrdenamientoProteccionDet: this.listOrdenInterno
    };

    params.push(param);

    return this.apiOrdenamiento.registrarOrdenamiento(params).pipe(
      tap((response: any) => {
        if (response.success) {
          this.toast.ok('Se registró el bloque quinquenal correctamente.');
          //this.listarOrdenamiento().subscribe();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
      })
    );
  }
  listarOrdenamiento() {
    this.bloques = [];
    this.objBosque.idOrdenamientoProteccionDet = 0;
    this.objBosque.noBloques = 0;
    this.objBosque.anios = 0;
    var params = {
      idPlanManejo: this.idPGMF,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
    };

    return this.apiOrdenamiento.listarOrdenamientoDetalle(params).pipe(
      tap((response: any) => {
        
        response.data[0].listOrdenamientoProteccionDet.forEach(
          (t: any) => {
            if (t.codigoTipoOrdenamientoDet === CodigosPGMF.PGMF_TAB_4_2_1) {
              if(t.categoria == "A"){
                this.objBosque.idOrdenamientoProteccionDet = t.idOrdenamientoProteccionDet;
                this.objBosque.noBloques = t.descripcion;
                this.objBosque.anios = t.observacion;
              }else if(t.categoria == "B"){
                this.selectAnexo = t.observacion || "N";
                this.idArchivo = t.idArchivo;
                this.bloques.push({
                  idOrdenamientoProteccionDet : t.idOrdenamientoProteccionDet,
                  idOrdenamientoProteccion: response.data[0].idOrdenamientoProteccion,
                  label: t.bloqueQuinquenal,
                  tipoBosque: [{
                    areaHA: t.areaHA,
                    porcentajeHA: t.areaHAPorcentaje,
                    descripcion: t.tipoBosque,
                  }]
                });
                this.calculateAreaTotalQuinquenal();
              }
            }
          }
        );
      })
    );
  }
  deleteRegisterTable(message:any){
    let parametros = {
      idOrdenamientoProtecccion: this.idOrdenamientoProteccion,
      idOrdenamientoProteccionDet: 0,
      codigoTipoOrdenamientoDet:CodigosPGMF.PGMF_TAB_4_2_1,
      idUsuarioElimina: this.idUsuario,
    };
    this.apiOrdenamiento
      .eliminarOrdenamiento(parametros)
      .subscribe((response: any) => {
        if (response.success) {
          this.toast.ok(
            'Se eliminó el registro del bloque quinquenal.'
          );
          this.bloques =  [];
          this.calculateAreaTotalQuinquenal();
        } else {
          this.toast.error(
            'Ocurrió un problema, intente nuevamente',
            'ERROR'
          );
        }
      });
  }
  eliminarBloque(bloques:any){
    this.confirmationService.confirm({
      target: undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if (bloques[0].idOrdenamientoProteccionDet !== 0) {
          let parametros = {
            idOrdenamientoProtecccion: bloques[0].idOrdenamientoProteccion,
            idOrdenamientoProteccionDet: 0,
            codigoTipoOrdenamientoDet:CodigosPGMF.PGMF_TAB_4_2_1,
            idUsuarioElimina: this.idUsuario,
          };
          this.apiOrdenamiento
            .eliminarOrdenamiento(parametros)
            .subscribe((response: any) => {
              if (response.success) {
                this.toast.ok(
                  'Se eliminó el registro del bloque quinquenal.'
                );
                this.bloques =  [];
                this.calculateAreaTotalQuinquenal();
              } else {
                this.toast.error(
                  'Ocurrió un problema, intente nuevamente',
                  'ERROR'
                );
              }
            });
        }
      },
    });
  }

  registroArchivo(idArchivo: number) {
    this.idArchivo = idArchivo;
  }

  validarIdArchivo() {
    let validar: boolean = true;

    if(this.idArchivo == 0){
      validar = false;
      this.toast.warn("(*) Debe guardar Archivo shapefile.")
    }

    return validar;
  }
}
