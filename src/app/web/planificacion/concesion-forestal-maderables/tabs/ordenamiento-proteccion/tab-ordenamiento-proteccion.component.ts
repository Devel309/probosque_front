import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  Renderer2,
  EventEmitter,
  OnInit,
  Output,
} from "@angular/core";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import GeoJSONLayer from "@arcgis/core/layers/GeoJSONLayer";
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer";
import Graphic from "@arcgis/core/Graphic";
import * as geometryEngine from "@arcgis/core/geometry/geometryEngine";
import * as wkt from "terraformer-wkt-parser";
import Color from "@arcgis/core/Color";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol";
import SimpleLineSymbol from "@arcgis/core/symbols/SimpleLineSymbol";
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol";
import SimpleRenderer from "@arcgis/core/renderers/SimpleRenderer";
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { OrdenamientoAreaMAnejoService } from "src/app/service/ordenamientoAreaManejo.service";
import { Table } from "primeng/table";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import {ConfirmationService, MessageService} from 'primeng/api';
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { FileModel } from "src/app/model/util/File";
import {DownloadFile, isNull, isNullOrEmpty, onlySemicolons, setOneSemicolon} from 'src/app/shared/util';
import { PlanificacionService } from "src/app/service/planificacion.service";
import { MapApi } from "src/app/shared/mapApi";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { PlanManejoEvaluacionService } from "src/app/service/plan-manejo-evaluacion.service";
import {ArchivoService, OrdenamientoInternoPmfiService, UsuarioService} from '@services';
import {AppMapaComponent, ArchivoTipo, LayerView, PGMFArchivoTipo, RespuestaTipo, ToastService} from '@shared';
import {ModalFormularioOrdenInternoComponent} from '../../../plan-general-manejo/tabs/tab-ordenamiento-area-manejo/modal-formulario-orden-interno/modal-formulario-orden-interno.component';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {finalize, tap, map, concatMap} from 'rxjs/operators';
import {forkJoin, Observable, of} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {PGMFArchivoDto, PlanManejoArchivo} from '@models';
import {CodigosPGMF} from '../../../../../model/util/PGMF/CodigosPGMF';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {CodigoPGMF} from '../../../../../model/CodigoPGMF';
import { ApiForestalService } from "src/app/service/api-forestal.service";
declare const shp: any;

@Component({
  selector: "app-tab-ordenamiento-proteccion",
  templateUrl: "./tab-ordenamiento-proteccion.component.html",
  styleUrls: ["./tab-ordenamiento-proteccion.component.scss"],
})
export class TabOrdenamientoProteccionComponent implements OnInit {
  anexo4: string = "";
  anexo42: string = "";
  params: any;
  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;
  idUsuario!: number;
  idOrdenamientoProteccion!:number;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();

  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @ViewChild("ulResult", { static: true }) private ulResult!: ElementRef;
  @ViewChild("tablaCatOrdenamiento", { static: false })
  private tablaCatOrdenamiento!: Table;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();
  public view: any = null;
  public _id = this.mapApi.Guid2.newGuid;
  listCatOrdenamiento: any = [];
  public filFile: any = null;

  //totalArea: string = "";
  totalPorcentaje: string = "";

  filFiles: FileModel[] = [];
  // listOrdenamientoAreaManejo: OrdenamientoAreaManejoModel[] = [];
  listOrdenamientoProteccion: any[] = [];
  proteccionVigilancia: any[] = [];
  ubicacionMarcadoVertices: any = [];
  senalizacion: any = [];
  demarcacionLinderos: any = [];
  vigilancia: any = [];

  nombrefile = "";
  listOrdenamientoProteccion2: any = [];

  idUsuarioRegistro = JSON.parse("" + localStorage.getItem("usuario"))
    .idusuario;

  bloques:any = [];
  bloques2 = [
    {
      label: "l",
      tipoBosque: [
        {
          idOrdenamientoProteccionDet: 61,
          codigoTipoOrdenamientoDet: "",
          descripcion: "Tipo Bosque",
          areaHA: 10,
          areaHAPorcentaje: 50,
          estado: "A",
          idUsuarioRegistro: 1,
        },
        {
          idOrdenamientoProteccionDet: 62,
          codigoTipoOrdenamientoDet: "",
          descripcion: "Tipo Bosque2",
          areaHA: 10,
          areaHAPorcentaje: 50,
          estado: "A",
          idUsuarioRegistro: 1,
        },
        {
          idOrdenamientoProteccionDet: 63,
          codigoTipoOrdenamientoDet: "",
          descripcion: "Total AFP del Bloque l",
          areaHA: 20,
          areaHAPorcentaje: 100,
          estado: "A",
          idUsuarioRegistro: 1,
        },
      ],
    },
    {
      label: "ll",
      tipoBosque: [{}, {}, {}],
    },
    {
      label: "lV",
      tipoBosque: [{}, {}, {}],
    },
    {
      label: "Total",
      tipoBosque: [{}, {}, {}],
    },
  ];

  bloqueQuinquenal = [
    {
      label: "1",
      tipoBosque: [
        {
          idOrdenamientoProteccionDet: 63,
          codigoTipoOrdenamientoDet: "",
          descripcion: "Tipo Bosque",
          areaHA: 10,
          areaHAPorcentaje: 100,
          estado: "A",
          idUsuarioRegistro: 1,
        },
        {},
      ],
    },
    {
      label: "2",
      tipoBosque: [{}, {}],
    },
    {
      label: "3",
      tipoBosque: [{}, {}],
    },
    {
      label: "Total Bloque ",
      tipoBosque: [{}],
    },
  ];

  CodigoProceso = CodigoProceso;
  CodigoPGMF = CodigoPGMF;

  usuario = {} as UsuarioModel;

  isLoadInt: boolean = true;

  PGMF_CON_COBERTURA = "PGMFCCB";
  PGMF_SIN_COBERTURA = "PGMFSCB";

  //nelson
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;
  listOrdenInterno: any[] = [];
  RespuestaTipo = RespuestaTipo;
  isEditedMap = false;
  ref!: DynamicDialogRef;
  relacionArchivo!: { idArchivo: number; idPlanManejoArchivo: number };
  cabecera: any;

  constructor(
    private apiArchivo: ArchivoService,
    private renderer: Renderer2,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private user: UsuarioService,
    private servOrdenamientoAreaMAnejo: OrdenamientoAreaMAnejoService,
    private MessageService: MessageService,
    private serviceGeoforestal: ApiGeoforestalService,
    private planificacionService: PlanificacionService,
    private mapApi: MapApi,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private serviceExternos: ApiForestalService
  ) { }

  ngOnInit() {
    this.idUsuario  = this.user.idUsuario;
    //this.initializeMap();
    //this.consultarUA();
    //this.listarOrdenamientoProteccion();
  }

  get totalArea() {
    return this.listOrdenInterno
      .map((i) => i.areaHA)
      .reduce((sum, x) => sum + x, 0);
  }

  ngAfterViewInit(): void {
    this.getInitData();
  }

  getInitData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      ordenamiento: this.listarOrdenamiento(),
      base64: this.obtenerArchivoMapa(),
      base64_2: this.obtenerArchivoMapaFromArea(),
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => {
        const layers = this.setLayer(this.listOrdenInterno);
        if (!isNullOrEmpty(res.base64)) {
          this.map.addBase64FileWithConfig(res.base64, layers);
        }
        if (!isNullOrEmpty(res.base64_2)) {
          this.map.addBase64FileWithConfig(res.base64_2, layers);
        }
      });
  }

  obtenerArchivoMapa(): Observable<string> {
    const item = {
      codigoProceso: PGMFArchivoTipo.PGMF,
      idPlanManejo: this.idPGMF,
      idTipoDocumento: ArchivoTipo.SHAPEFILE,
    };
    return this.obtenerRelacionArchivo(item)
      .pipe(
        tap((res) => {
          if (!isNull(res)) {
            const { idArchivo, idPlanManejoArchivo } = res;
            this.relacionArchivo = { idArchivo, idPlanManejoArchivo };
          }
        })
      )
      .pipe(map((res) => (res?.documento ? res.documento : '')));
  }

  obtenerRelacionArchivo(item: any) {
    return this.apiOrdenamiento.obtenerArchivo(item);
  }

  obtenerArchivoMapaFromArea(): Observable<string> {
    return this.obtenerRelacionArchivoFromArea(
      PGMFArchivoTipo.PMFI,
      this.idPGMF,
      ArchivoTipo.SHAPEFILE_03
    )
      .pipe(
        tap((res) => {
          if (!isNull(res)) {
            const { idArchivo, idPlanManejoArchivo } = res;
            this.relacionArchivo = { idArchivo, idPlanManejoArchivo };
          }
        })
      )
      .pipe(map((res: any) => (res?.documento ? res.documento : '')));
  }

  obtenerRelacionArchivoFromArea(
    codigoProceso: string,
    idPlanManejo: number,
    idTipoDocumento: string
  ) {
    return this.apiOrdenamiento.obtenerRelacionArchivo(
      codigoProceso,
      idPlanManejo,
      idTipoDocumento
    );
  }


  listarOrdenamiento() {
    this.listOrdenInterno = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codTipoOrdenamiento: 'PGMF',
    };

    return this.apiOrdenamiento.listarOrdenamientoDetalle(params).pipe(
      tap((response: any) => {
        this.idOrdenamientoProteccion = response.data[0].idOrdenamientoProteccion;
        this.cabecera = {
          idOrdenamientoProteccion: response.data[0].idOrdenamientoProteccion,
          codTipoOrdenamiento: response.data[0].codTipoOrdenamiento,
          idPlanManejo: response.data[0].idPlanManejo,
        };
        response.data[0].listOrdenamientoProteccionDet.forEach(
          (element: any) => {
            const item = element;
            // let coberBoscosa = ;
            if(item.codigoTipoOrdenamientoDet == CodigosPGMF.PGMF_TAB_4_1) {
              let detalle = {
                idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet,
                codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
                categoria: item.categoria,
                areaHA: item.areaHA,
                observacion: item.observacion, //campo para saber si tiene asociado un shapefile
                descripcion: item.descripcion, //campo para guardar nombres de las capas
                actividad:   item.actividad,      //campo para guardar layerId (capa mapa)
                actividadesRealizar: item.actividadesRealizar, //campo para guardar groupId (grupo capa mapa)
                observacionDetalle: item.observacionDetalle, //campo para guardar color capa
                zonaUTM:  item.zonaUTM
              };

              this.listOrdenInterno.push(detalle);
            }
          }
        );
      })
    );
  }

  setLayer(list: Ordenamiento[]): LayerView[] {
    let layers: LayerView[] = [];
    for (const item of list) {
      if (!isNullOrEmpty(item.actividad) && !isNullOrEmpty(item.descripcion)) {
        const layersId = item.actividad.split(';');
        const layersName = item.descripcion.split(';');
        for (let index = 0; index < layersId.length; index++) {
          const layerId = layersId[index];
          const title = layersName[index];
          const color = item.observacionDetalle;
          const groupId = item.actividadesRealizar;
          const layer: LayerView = {
            color,
            groupId,
            layerId,
            title,
            area: 0,
            features: [],
          };
          layers.push(layer);
        }
      }
    }
    return layers;
  }

  addLayer(item: Ordenamiento, file: any) {
    const groupId = this.map.genId();
    const color = this.map.genColor();

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.map.addLayerFile(file, groupId, color)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((layers) => {
        item.areaHA = layers.map((l) => l.area).reduce((sum, x) => sum + x, 0);
        item.observacion = RespuestaTipo.SI; //tiene shapefile
        item.descripcion = layers
          .map((l) => this.map.joinTitle(l.title))
          .join(';'); //capas asociadas
        item.actividad = layers.map((l) => l.layerId).join(';'); //layerId
        item.actividadesRealizar = groupId; // groupId
        item.observacionDetalle = color; //campo para guardar color capa
        this.isEditedMap = true;
      });
  }


  deleteLayer(l: LayerView) {
    let item = this.listOrdenInterno.find(
      (x) => x.actividadesRealizar == l.groupId
    );

    if (!isNull(item)) {
      item.areaHA = item.areaHA - l.area;
      item.areaHA = item.areaHA > 0 ? item.areaHA : 0;
      let capas = String(item.descripcion).replace(
        this.map.joinTitle(l.title),
        ''
      );
      capas = setOneSemicolon(capas);
      capas = onlySemicolons(capas) ? '' : capas;
      item.descripcion = capas;
      item.observacion = isNullOrEmpty(capas)
        ? RespuestaTipo.NO
        : RespuestaTipo.SI;
      let layerId = String(item.actividad).replace(String(l.layerId), '');
      layerId = setOneSemicolon(layerId);
      layerId = onlySemicolons(layerId) ? '' : layerId;
      item.actividad = layerId;
      item.observacionDetalle = isNullOrEmpty(capas)
        ? ''
        : item.observacionDetalle;
    }
  }

  deleteAllLayers() {
    this.listOrdenInterno.forEach((item) => {
      item.areaHA = 0;
      item.observacion = RespuestaTipo.NO; //tiene shapefile
      item.descripcion = '';
      item.actividad = '';
      item.actividadesRealizar = '';
      item.observacionDetalle = '';
    });
  }

  openModal = (mesaje: string, data: any, tipo: any, index?: any) => {
    this.ref = this.dialogService.open(ModalFormularioOrdenInternoComponent, {
      header: mesaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: { data: data, type: tipo },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == 'C') {
          let ordenNueva = {
            idOrdenamientoProteccionDet: 0,
            codigoTipoOrdenamientoDet: CodigosPGMF.PGMF_TAB_4_1,
            categoria: resp.categoria || 'Sin Categoría',
            areaHA: 0.0,
            observacion: null, //tiene shapefile
            descripcion: '', //capas asociadas
            actividad: null, //layerId
            actividadesRealizar: null, // groupId
            observacionDetalle: null, //campo para guardar color capa
            zonaUTM :resp.zonaUTM
          };

          if(ordenNueva.zonaUTM == this.PGMF_SIN_COBERTURA){
            this.listOrdenInterno.push(ordenNueva);
          }else if(ordenNueva.zonaUTM == this.PGMF_CON_COBERTURA){
            //this.PGMF_SIN_COBERTURA
            let index = this.getLastIndexCobertura();
            this.listOrdenInterno.splice( index, 0, ordenNueva )
          }

          //this.listOrdenInterno.push(ordenNueva);
          //this.guardarOrdenInterno(ordenNueva);
        } else if (tipo == 'E') {
          this.listOrdenInterno[index].categoria = resp.categoria;
          this.listOrdenInterno[index].zonaUTM = resp.zonaUTM;
        }
      }
    });
  };


  getLastIndexCobertura(){
    let indice :number = -1;

    let index:number = 0;
    for (let obj of this.listOrdenInterno) {
      if(obj.zonaUTM  == this.PGMF_SIN_COBERTURA){
        indice = index;
        break;
      }
      index = index +1;
    }

    return indice;

  }

  openEliminar(event: Event, data: any, index: any): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if (data.idOrdenamientoProteccionDet !== 0) {
          var parametros = {
            idOrdenamientoProtecccion: 0,
            idOrdenamientoProteccionDet: data.idOrdenamientoProteccionDet,
            codigoTipoOrdenamientoDet: '',
            idUsuarioElimina: this.user.idUsuario,
          };
          this.apiOrdenamiento
            .eliminarOrdenamiento(parametros)
            .subscribe((response: any) => {
              if (response.success) {
                this.toast.ok(
                  'Se eliminó el  ordenamiento del área de manejo correctamente.'
                );
                // this.listarOrdenamiento().subscribe();
                this.listOrdenInterno.splice(index, 1);
              } else {
                this.toast.error(
                  'Ocurrió un problema, intente nuevamente',
                  'ERROR'
                );
              }
            });
        } else {
          this.listOrdenInterno.splice(index, 1);
          this.toast.ok(
            'Se eliminó el  ordenamiento del área de manejo correctamente.'
          );
        }
        this.removeLayer(data);
      },
    });
  }

  download(e: Ordenamiento) {
    let nameFile = String(e.categoria)
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '');
    nameFile = nameFile.replace(/[^a-z0-9\s]/gi, '').replace(/[-\s]/g, '_');
    this.map.downloadGroup(e.actividadesRealizar, nameFile);
  }

  guardarOrdProteccion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.saveFileFlow()
      .pipe(concatMap(() => this.guardarOrdenamiento()))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  guardarOrdenamiento() {
    let params: any = [];
    let param = {
      idOrdenamientoProteccion: this.cabecera.idOrdenamientoProteccion,
      codTipoOrdenamiento: CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      idUsuarioModificacion: this.user.idUsuario,
      listOrdenamientoProteccionDet: this.listOrdenInterno
        /*[
        {
          idOrdenamientoProteccionDet: element.idOrdenamientoProteccionDet,
          codigoTipoOrdenamientoDet: 'PGMFDET',
          categoria: element.categoria,
          areaHA: element.areaHA,
          idUsuarioRegistro: this.user.idUsuario,
          idUsuarioModificacion: this.user.idUsuario,
          observacion: element.observacion, //tiene shapefile
          descripcion: element.descripcion, //capas asociadas
          actividad: element.actividad, //layerId
          actividadesRealizar: element.actividadesRealizar, // groupId
          observacionDetalle:
            element.observacionDetalle != 'PGMF'
              ? element.observacionDetalle
              : '', //campo para guardar color capa
        },
      ],*/
    };

    params.push(param);

    // if (arrayBody[0].listOrdenamientoProteccionDet[0].areaHA != null) {

    return this.apiOrdenamiento.registrarOrdenamiento(params).pipe(
      tap((response: any) => {
        if (response.success) {
          this.toast.ok('Se registró el Ordenamiento Interno correctamente.');
          this.listarOrdenamiento().subscribe();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
      })
    );
    // } else {
    //   this.toast.warn('No tiene Archivos cargados');
    //   return

    // }
  }

  saveFileFlow() {
    if (!this.isEditedMap) return of(null);

    const idArchivo = this.relacionArchivo?.idArchivo;
    const idPlanManejoArchivo = this.relacionArchivo?.idPlanManejoArchivo;

    const deleteFile = isNull(this.relacionArchivo)
      ? of(null)
      : this.deleteFile(idArchivo, idPlanManejoArchivo).pipe(
        tap(() => (this.isEditedMap = false))
      );

    if (this.map.isEmpty)
      return deleteFile.pipe(
        tap(() => (this.relacionArchivo = undefined as any))
      );

    return this.saveFile()
      .pipe(
        tap((res) => {
          this.relacionArchivo = {
            idArchivo: res?.data?.idArchivo,
            idPlanManejoArchivo: res?.data?.idPGMFArchivo,
          };
          this.isEditedMap = false;
        })
      )
      .pipe(concatMap(() => deleteFile));
  }

  deleteFile(idArchivo: number, idPlanManejoArchivo: number) {
    return forkJoin([
      this.eliminarArchivo(idArchivo, this.user.idUsuario),
      this.eliminarRelacionArchivo(idPlanManejoArchivo, this.user.idUsuario),
    ]).pipe(map(() => null));
  }

  eliminarRelacionArchivo(
    idPlanManejoArchivo: number,
    idUsuarioElimina: number
  ) {
    const item = new PlanManejoArchivo({
      idPlanManejoArchivo,
      idUsuarioElimina,
    });
    return this.apiOrdenamiento.eliminarArchivo(item);
  }

  eliminarArchivo(idArchivo: number, idUsuarioElimina: number) {
    return this.apiArchivo.eliminarArchivo(idArchivo, idUsuarioElimina);
  }

  saveFile() {
    return this.map
      .getZipFile()
      .pipe(tap({ error: (err) => this.toast.warn(err) }))
      .pipe(concatMap((blob) => this.guardarArchivo(blob as Blob)))
      .pipe(concatMap((idArchivo) => this.guardarRelacionArchivo(idArchivo)));
  }

  guardarArchivo(blob: Blob): Observable<number> {
    const file = new File([blob], `PGMF-OI-${this.idPGMF}.zip`);
    return this.apiArchivo
      .cargar(this.user.idUsuario, ArchivoTipo.SHAPEFILE, file)
      .pipe(map((res) => res?.data));
  }

  guardarRelacionArchivo(idArchivo: number) {
    const item = new PGMFArchivoDto({
      codigoTipoPGMF: PGMFArchivoTipo.PGMF,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      idArchivo,
    });
    return this.apiOrdenamiento.registrarArchivo(item);
  }




  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN6', this.lineamiento);
    this.ordenarLista(data, 'PGMFEVALLIN7', this.lineamiento2);
    this.ordenarLista(data, 'PGMFEVALLIN8', this.lineamiento3);
  }

  dataLineamientos(data: any) {

    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN6' }
    );
    this.lineamiento2 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN7' }
    );
    this.lineamiento3 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN8' }
    );
  }

  listarOrdenamientoProteccion() {
    const planManejo = {} as PlanManejoModel;
    planManejo.idPlanManejo = 0;
    this.planificacionService
      .listarOrdenamientoProteccion(planManejo)
      .subscribe((result: any) => {
        result.data.forEach((element: any, index1: number) => {
          element.listOrdenamientoProteccionDet.forEach((x: any) => {
            if (index1 == 0 && this.listOrdenamientoProteccion.length == 0) {
              this.listOrdenamientoProteccion.push({
                codigoTipoOrdenamientoDet: "",
                categoria: "Con Cobertura boscosa",
                areaHA: 0,
                areaHAPorcentaje: 0,
                estado: "",
                idUsuarioRegistro: 27,
                notShow: true,
              });
            }

            if (this.listOrdenamientoProteccion.length == 5) {
              this.listOrdenamientoProteccion.push({
                codigoTipoOrdenamientoDet: "",
                categoria: "Sin Cobertura boscosa",
                areaHA: 0,
                areaHAPorcentaje: 0,
                estado: "",
                idUsuarioRegistro: 27,
                notShow: true,
              });
            }
            if (x.codigoTipoOrdenamientoDet == "COCOBO") {
              this.listOrdenamientoProteccion.push(x);
            } else if (x.codigoTipoOrdenamientoDet == "SNCOBO") {
              this.listOrdenamientoProteccion.push(x);
            } else if (x.codigoTipoOrdenamientoDet == "PROVIG") {
              this.proteccionVigilancia.push(x);
            }
          });
        });
      });
  }

  /*eliminarOrdenamientoProteccion = (id: any) => {
    let sum1 = 0;
    for (let item of this.listOrdenamientoProteccion) {
      if (item.idOrdenamientoProteccionDet == id) {
        item.areaHA = 0;
        item.areaHAPorcentaje = 0;
        delete item.modificar;
        delete item.file;
      }
      sum1 += item.areaHA;
    }

    if (sum1 > 0) {
      for (let item of this.listOrdenamientoProteccion) {
        item.areaHAPorcentaje = Number(((100 * item.areaHA) / sum1).toFixed(2));
      }
    }

    this.totalArea = `${sum1.toFixed(2)}`;
    if (sum1 > 0) {
      this.totalPorcentaje = `${(100).toFixed(2)} %`;
    } else {
      this.totalPorcentaje = `${0} %`;
    }
  };*/

  /*guardar() {
    this.parameters();
    this.ordenamientoInternoPmfiService.registrarOrdenamiento(this.params).subscribe((response) => { });
    /!*this.planificacionService
      .registrarOrdenamientoProteccion(this.params)
      .subscribe((response) => { });*!/

  }*/

  parameters() {
    this.listOrdenamientoProteccion2 = [];

    this.listOrdenamientoProteccion.forEach((x: any) => {

      /*if (x.file && x.modificar) {
        x.idUsuarioRegistro = this.usuario.idusuario;
        x.estado = "A";
      }*/

      this.listOrdenamientoProteccion2.push({
        codigoTipoOrdenamientoDet: "POCCOPCPUMFPV",
        categoria: x.categoria ? x.categoria : "",
        areaHA: x.areaHA ? x.areaHA : 0,
        areaHAPorcentaje: x.areaHAPorcentaje ? x.areaHAPorcentaje : 0,
        descripcion: "-",
        observacion: null,
        observacionDetalle: null,
        actividad: null,
        actividadesRealizar: null,
        accion: 1,
        idUsuarioRegistro: this.idUsuarioRegistro,
      });

    });

    /**
     * PGMF
     * PGMFUMF
     * PGMFUMFORD
     * PGMFUMFADM
     * PGMFUMFPRO
     * */

    this.params = [
      {
        codTipoOrdenamiento: "PGMF",
        subCodTipoOrdenamiento: "POCCOPCPUMFPV",
        idPlanManejo: this.idPGMF,
        anexo: "N",
        idUsuarioRegistro: this.idUsuarioRegistro,
        listOrdenamientoProteccionDet: this.listOrdenamientoProteccion2,
      },
      /*{
        idPlanManejo: this.idPGMF,
        codTipoOrdenamiento: "PGMFUMFADM1",
        anexo: this.anexo4,
        idUsuarioRegistro: this.idUsuarioRegistro,
        listOrdenamientoProteccionDet: [
          //Listado de bloques  4.2.1
          {
            idOrdenamientoProteccionDet: 0,
            descripcion: "Tipo Bosque",
            areaHA: 10,
            areaHAPorcentaje: 100,
            estado: "A",
            idUsuarioRegistro: this.idUsuarioRegistro,
          },
        ],
      },
      {
        idPlanManejo: this.idPGMF,
        codTipoOrdenamiento: "PGMFUMFADM2",
        anexo: this.anexo42,
        idUsuarioRegistro: this.idUsuarioRegistro,
        listOrdenamientoProteccionDet: [
          //Listado de parcelas corta 4.2.2
          {
            idOrdenamientoProteccionDet: 0,
            descripcion: "Tipo Bosque",
            areaHA: 10,
            areaHAPorcentaje: 100,
            estado: "A",
            idUsuarioRegistro: this.idUsuarioRegistro,
          },
        ],
      },
      {
        idPlanManejo: this.idPGMF,
        codTipoOrdenamiento: "PGMFUMFPRO1",
        idUsuarioRegistro: this.idUsuarioRegistro,
        listOrdenamientoProteccionDet: this.ubicacionMarcadoVertices,
      },
      {
        idPlanManejo: this.idPGMF,
        codTipoOrdenamiento: "PGMFUMFPRO2",
        idUsuarioRegistro: this.idUsuarioRegistro,
        listOrdenamientoProteccionDet: this.senalizacion,
      },


      */







      /*    {
           idPlanManejo: this.idPGMF,
           codTipoOrdenamiento: "",
           idUsuarioRegistro: this.idUsuarioRegistro,
           listOrdenamientoProteccionDet: this.demarcacionLinderos,
         },
         {
           idPlanManejo: this.idPGMF,
           codTipoOrdenamiento: "",
           idUsuarioRegistro: this.idUsuarioRegistro,
           listOrdenamientoProteccionDet: this.vigilancia,
         }, */
    ];
  }

  listUbicacionMarcadoVertices(event: any) {
    event.forEach((element: any) => {
      this.ubicacionMarcadoVertices.push({

        codigoTipoOrdenamientoDet: element.codigoTipoDetalle
          ? element.codigoTipoDetalle
          : "",
        verticeBloque: element.verticeBloque ? element.verticeBloque : "",
        coordenadaEste: element.coordenadaEste ? element.coordenadaEste : 0,
        coordenadaNorte: element.coordenadaNorte ? element.coordenadaNorte : 0,
        zonaUTM: element.zonaUTM ? element.zonaUTM : "",
        descripcion: element.descripcion ? element.descripcion : "",
        observacionDetalle: element.observacion ? element.observacion : "",
        estado: element.estado ? element.estado : "",
        idUsuarioRegistro: this.idUsuarioRegistro,
      });
    });
  }
  listSenalizacion(event: any) {
    event.forEach((element: any) => {
      this.senalizacion.push({
        codigoTipoOrdenamientoDet: element.codigoTipoDetalle
          ? element.codigoTipoDetalle
          : "",
        verticeBloque: element.verticeBloque ? element.verticeBloque : "",
        coordenadaEste: element.coordenadaEste ? element.coordenadaEste : 0,
        coordenadaNorte: element.coordenadaNorte ? element.coordenadaNorte : 0,
        zonaUTM: element.zonaUTM ? element.zonaUTM : "",
        descripcion: element.descripcion ? element.descripcion : "",
        observacionDetalle: element.observacion ? element.observacion : "",
        estado: element.estado ? element.estado : "",
        idUsuarioRegistro: this.idUsuarioRegistro,
      });
    });
  }
  listDemarcacionLinderos(event: any) {
    event.forEach((element: any) => {
      this.demarcacionLinderos.push({
        codigoTipoOrdenamientoDet: element.codigoTipoDetalle
          ? element.codigoTipoDetalle
          : "",
        verticeBloque: element.verticeBloque ? element.verticeBloque : "",
        coordenadaEste: element.coordenadaEste ? element.coordenadaEste : 0,
        coordenadaNorte: element.coordenadaNorte ? element.coordenadaNorte : 0,
        zonaUTM: element.zonaUTM ? element.zonaUTM : "",
        descripcion: element.descripcion ? element.descripcion : "",
        observacionDetalle: element.observacion ? element.observacion : "",
        estado: element.estado ? element.estado : "",
        idUsuarioRegistro: this.idUsuarioRegistro,
      });
    });
  }
  listVigilancia(event: any) {
    event.forEach((element: any) => {
      this.vigilancia.push({
        codigoTipoOrdenamientoDet: element.codigoTipoDetalle
          ? element.codigoTipoDetalle
          : "",
        verticeBloque: element.verticeBloque ? element.verticeBloque : "",
        coordenadaEste: element.coordenadaEste ? element.coordenadaEste : 0,
        coordenadaNorte: element.coordenadaNorte ? element.coordenadaNorte : 0,
        zonaUTM: element.zonaUTM ? element.zonaUTM : "",
        descripcion: element.descripcion ? element.descripcion : "",
        observacionDetalle: element.observacion ? element.observacion : "",
        estado: element.estado ? element.estado : "",
        idUsuarioRegistro: 1,
      });
    });
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const map = new Map({
      basemap: "hybrid",
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }
  onFileDownload(e: any, data: any) {
    this.servOrdenamientoAreaMAnejo
      .obtenerOrdenamientoAreaManejoArchivo(data)
      .subscribe((result: any) => {
        DownloadFile(
          result.data.archivo,
          result.data.nombeArchivo,
          "application/octet-stream"
        );
      });
  }

  onFileChange(e: any, catOrd: any) {
    

    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.target.files[0];

    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          this.processFile(e.target.files[i], e, catOrd, true);
          i++;
        }
      }
    }
  }

  processFile(file: any, e: any, catOrd: any, modificar: Boolean) {
    let itemFile = file;
    try {
      const file_ = {} as FileModel;
      let val = 0;
      for (let item of this.filFiles) {
        if (item.descripcion == catOrd) {
          item.descripcion = catOrd;
          item.file = file;
        }
      }
      if (val == 0) {
        file_.descripcion = catOrd;
        file_.file = file;
        this.filFiles.push(file_);
      }

      let promise = Promise.resolve([]);
      promise = this.loadShapeFile(itemFile);
      promise
        .then((data) => {
          //this.createLayers(data, file);
         // this.calculateArea(data, e, catOrd, modificar);
        })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      console.log(error);
    }

  }

  /*calculateAreaTotal(id: any) {
    let sum1 = 0;
    for (let item of this.listOrdenamientoProteccion) {
      if (item.idOrdenamientoProteccionDet == id) {
        item.areaHA = 0;
        item.modificar = null;
        item.file = null;
      }
      sum1 += item.areaHA;
    }

    for (let item of this.listOrdenamientoProteccion) {
      item.areaHAPorcentaje = Number(((100 * item.areaHA) / sum1).toFixed(2));
    }
    this.totalArea = `${sum1.toFixed(2)}`;
    this.totalPorcentaje = `${(100).toFixed(2)} %`;
  }

  calculateArea(data: any, e: any, catOrd: any, modificar: Boolean) {
    let geometry: any = null;
    geometry = { spatialReference: { wkid: 4326 } };
    geometry.rings = data[0].features[0].geometry.coordinates;
    let area = geometryEngine.geodesicArea(geometry, "hectares");

    if (e == null) return;

    let txtArea = e.path[3].cells[4];
    let sum1 = 0;
    let sum2 = 0;
    let table = this.tablaCatOrdenamiento.tableViewChild.nativeElement;
    let body = table.querySelector("tbody");
    let footer = table.querySelector("tfoot");
    let tds = footer.querySelectorAll("td");
    let trs = body.querySelectorAll("tr");
    let total = tds[3];
    let totalPorcentaje = tds[4];
    let calculo = 0;
    let sum = 0;
    for (let item of this.listOrdenamientoProteccion) {
      if (item.idOrdenamientoProteccionDet == catOrd) {
        item.areaHA = Number(area.toFixed(2));
        if (item.modificar == null || item.file == null) {
          item.modificar = modificar;
          item.file = true;
        }
        item.modificar = modificar;
        item.file = true;
      }
      sum1 += item.areaHA;
    }

    for (let item of this.listOrdenamientoProteccion) {
      item.areaHAPorcentaje = Number(((100 * item.areaHA) / sum1).toFixed(2));
    }
    this.totalArea = `${sum1.toFixed(2)}`;
    this.totalPorcentaje = `${(100).toFixed(2)} %`;
    e = null;
  }
  */
  createLayers(layers: any, file: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.random();
      t.file = file;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this.createLayer(t);
    });
  }

  random() {
    var length = 6;
    var chars = "0123456789ABCDEF";
    var hex = "#";
    while (length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }

  loadShapeFile(item: any) {
    return this.getItem(item)
      .then((data) => {
        return new Promise((resolve, reject) => {
          let fileReader = new FileReader();
          fileReader.onload = (e: any) => {
            resolve(e.target.result);
          };
          fileReader.onerror = fileReader.onabort = reject;
          fileReader.readAsArrayBuffer(data);
        });
      })
      .then((data) => {
        return shp(data);
      })
      .then((data) => {
        if (Array.isArray(data) === false) data = [data];
        data.forEach((t: any) => (t.title = t.fileName));
        return data;
      })
      .catch((error) => {
        throw error;
      });
  }

  getItem(item: any) {
    let promise = Promise.resolve(item);
    if (typeof item === "string")
      promise = fetch(item).then((data) => {
        return data.blob();
      });
    return promise;
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = new GeoJSONLayer({
      url: url,
    });
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.changeLayerStyle(geoJsonLayer.id, geoJsonLayer.color);
        this.view.goTo({ target: data.fullExtent });
        //this.createTreeLayers();
      })
      .catch((error: any) => {
        console.log(error);
      });
    this.view.map.add(geoJsonLayer);
  }
  changeLayerStyle(id: any, color: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer === null) return;
    layer.color = color;
    let type = layer.geometryType;
    let style: any = null;
    if (type === "point" || type === "multipoint") {
      style = new SimpleMarkerSymbol();
      style.size = 10;
    } else if (type === "polyline") {
      style = new SimpleLineSymbol();
      style.width = 2;
    } else if (type === "polygon" || type === "extent") {
      style = new SimpleFillSymbol();
      style.outline = new SimpleLineSymbol();
      style.outline.width = 2;
    }
    style.color = Color.fromHex(color);
    layer.renderer = new SimpleRenderer({ symbol: style });
  }

  Base64toBlob(base64Data: string, contentType: string): Blob {
    contentType = contentType || "";
    var sliceSize = 1024;
    var byteCharacters = "";
    if (base64Data != "")
      byteCharacters = atob(base64Data.replace(/['"]+/g, ""));

    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      var begin = sliceIndex * sliceSize;
      var end = Math.min(begin + sliceSize, bytesLength);

      var bytes = new Array(end - begin);
      for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  DownloadFile(base64: string, name: string, mediaType: string) {
    let blob = this.Base64toBlob(base64, mediaType);
    const link = document.createElement("a");
    const objectUrl = URL.createObjectURL(blob);
    link.href = objectUrl;
    link.download = name;
    link.click();
    URL.revokeObjectURL(objectUrl);
  }
  consultarUA() {
    let geometry =
      "POLYGON ((-73.751046211999949 -8.90109354699996, -73.751199184999962 -8.8558786099999338, -73.751295108999955 -8.8274027349999642, -73.751347504999956 -8.8274069189999409, -73.765734878999979 -8.8274545109999281, -73.765773680999985 -8.8157736699999418, -73.751334350999969 -8.81572596899997, -73.751351346999968 -8.8106635629999346, -73.751502699999946 -8.7654484039999261, -73.796953448999943 -8.7655958389999569, -73.796807602999934 -8.81081176999993, -73.796660975999941 -8.8560275909999291, -73.796544736999977 -8.8917027939999684, -73.795301972999937 -8.8932089979999773, -73.794557891999943 -8.895331748999979, -73.794603946999985 -8.8975836599999525, -73.793962389999933 -8.8987662449999334, -73.793384964999973 -8.8991080169999464, -73.791577960999973 -8.8990026819999457, -73.789469155999939 -8.8982090739999649, -73.788473459999977 -8.89698500399993, -73.787602456999934 -8.8946580669999662, -73.786675847999959 -8.8936874289999537, -73.784453332999988 -8.89326419799994, -73.783319869999957 -8.8933238009999513, -73.78292248799994 -8.893530496999972, -73.782434083999988 -8.89427948499997, -73.782361609999953 -8.8954006029999277, -73.782942480999964 -8.8960807399999453, -73.785293585999966 -8.8978427939999278, -73.785513394999953 -8.8989196509999715, -73.785010837999948 -8.8996505099999581, -73.782598768999947 -8.9011980589999666, -73.782528712999977 -8.9012430459999337, -73.780910542999948 -8.9028112629999328, -73.780558968999969 -8.90411232799994, -73.780623945999935 -8.9081729259999634, -73.780068080999968 -8.9098893059999682, -73.77961970299998 -8.9103852089999691, -73.778624298999944 -8.9104452439999591, -73.777300105999984 -8.9092200659999321, -73.776060522999956 -8.9044321589999527, -73.775271245999988 -8.9040407069999787, -73.774370144999978 -8.90411912899998, -73.771014524999941 -8.90550070699993, -73.768303619999983 -8.90715568099995, -73.767307395999978 -8.90915995599994, -73.766427151999949 -8.9133530479999763, -73.765589742999964 -8.9140918049999414, -73.764163921999966 -8.914367403999961, -73.763576183999987 -8.913705302999972, -73.763426710999966 -8.9103226809999683, -73.763079819999973 -8.90953477599993, -73.762214512999947 -8.909613285999967, -73.760020330999964 -8.9112246929999515, -73.758576755999968 -8.91188002399997, -73.757037441999955 -8.9122727779999309, -73.755680300999984 -8.9122411099999681, -73.754194784999981 -8.91168450799995, -73.752960790999964 -8.9107037189999687, -73.75205410999996 -8.9094075169999769, -73.751612771999987 -8.9076516789999687, -73.751235555999983 -8.9070987859999491, -73.75102626599994 -8.9069714799999815, -73.751046211999949 -8.90109354699996))";
    let geojson: any = wkt.parse(geometry);
    let graphicsLayer: any = new GraphicsLayer();
    let polygon: any = {};
    const fillSymbol = {
      type: "simple-fill",
      color: [227, 139, 79, 0.8],
      outline: {
        color: [255, 255, 255],
        width: 1,
      },
    };
    const template = {
      title: "Unidad de Aprovechamiento",
    };
    polygon.type = "polygon";
    //polygon.rings = geojson.coordinates;
    let feature: any = null;
    geojson.coordinates.forEach((t: any) => {
      polygon.rings = t;
      feature = new Graphic({
        geometry: polygon,
        symbol: fillSymbol,
      });
      feature.popupTemplate = template;
      graphicsLayer.add(feature);
    });
    this.view.map.add(graphicsLayer);
    this.consultarTiposBosque(geojson.coordinates);
    this.superposicionPlanificacion(geojson.coordinates);

    setTimeout(() => {
      this.isLoadInt = false;
    }, 5000);
  }
  consultarTiposBosque(geometry: any) {
    let params = {
      idClasificacion: "",
      geometria: {
        poligono: geometry,
      },
    };
    this.serviceExternos
      .identificarTipoBosque(params)
      .subscribe((result) => {
        
        if (result.dataService.data.capas.length > 0) {
          result.dataService.data.capas.forEach((t: any) => {
            if (t.geoJson !== null) {
              t.geoJson.crs.type = "name";
              t.geoJson.opacity = 0.4;
              t.geoJson.color = this.mapApi.random();
              t.geoJson.title = t.nombreCapa;
              t.geoJson.service = true;
              
              this.createLayerFromServices(t.geoJson);
            }
          });
        }
      });
  }
  superposicionPlanificacion(geometry: any) {
    let params = {
      codProceso: "110102",
      geometria: {
        poligono: geometry,
      },
    };
    this.serviceExternos
      .validarSuperposicionPlanificacion(params)
      .subscribe((result: any) => {
        
        if (result.dataService.data.contenida.length) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.service = true;
                this.createLayerFromServices(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.noSuperpuesta.length) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.service = true;
                this.createLayerFromServices(t.geoJson);
              }
            }
          });
        }
      });
  }
  createLayerFromServices(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = new GeoJSONLayer({
      url: url,
    });
    geoJsonLayer.visible = true;
    geoJsonLayer.id = this.mapApi.Guid2.newGuid;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.service = item.service;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.changeLayerStyle(geoJsonLayer.id, geoJsonLayer.color);
        //this.view.goTo({ target: data.fullExtent });
        this.createTreeLayers();
      })
      .catch((error: any) => {
        console.log(error);
      });
    this.view.map.add(geoJsonLayer);
  }
  toggleLayer(id: any, visible: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null) layer.visible = visible;
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  removeLayer(id: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null) {
      this.view.map.layers.remove(layer);
      layer.visible = false;
      layer.forRemove = true;
    }
  }
  createTreeLayers() {
    let layers = this.getLayers();
    let ulResult = this.ulResult.nativeElement;
    ulResult.empty();
    layers.forEach((t: any) => {
      if (t.service === true) {
        let li = this.renderer.createElement("li");
        this.renderer.addClass(li, "list-group-item");
        this.renderer.addClass(li, "p-2");
        this.renderer.appendChild(ulResult, li);
        let chk = this.renderer.createElement("input");
        this.renderer.setAttribute(chk, "id", t.id);
        this.renderer.setProperty(chk, "type", "checkbox");
        chk.checked = true;
        this.renderer.appendChild(li, chk);
        chk.addEventListener("change", (e: any) => {
          this.toggleLayer(e.currentTarget.id, e.currentTarget.checked);
        });
        let text = this.renderer.createElement("label");
        text.style.color = t.color;
        text.textContent = t.title;
        this.renderer.appendChild(li, text);
      }
    });
  }

  // lo que se tenia //
  /*  guardarOrdenamientoAreaManejo() {
    const list: OrdenamientoAreaManejoModel[] = [];
    let count = 0;
    for (let item of this.listOrdenamientoAreaManejo) {
      if (item.file && item.modificar) {
        count++;
      }
    }

     for (let item of this.listOrdenamientoAreaManejo) {
      let add = 0;
      if (item.file && item.modificar) {
        item.idUsuarioRegistro = this.usuario.idusuario;
        item.estado = "A";
        for (var i = 0; i < this.filFiles.length; i++) {
          if (item.catOrdenamiento == this.filFiles[i].descripcion) {
            let x = JSON.stringify(item);
            var param = new HttpParams()
              .set("idPlanManejo", item.planManejo.idPlanManejo.toString())
              .set("idOrdAreaManejo", item.idOrdAreaManejo.toString())
              .set("catOrdenamiento", item.catOrdenamiento.toString())
              .set("area", item.area.toString())
              .set("porcentaje", item.porcentaje.toString())
              .set("accion", item.accion.toString())
              .set("file", item.file.toString())
              .set("idUsuarioRegistro", item.idUsuarioRegistro.toString())
              .set("estado", item.estado.toString());

            const formData = new FormData();
            let filFile: any = this.filFiles[i].file;
            formData.append('files', filFile);
            this.servOrdenamientoAreaMAnejo
              .registrarOrdenamientoAreaManejo(param, formData)
              .subscribe((result: any) => {
                add++;
                if (add == count) {
                  this.SuccessMensaje(result.message);
                  this.listarOrdenamientoAreaManejo();
                }
              });
            break;
          }
        }
        //  list.push(item);
      }
    }

  }

    guardarBloquesQuinquenales() {
    let params = [
      {
        anios: this.objBosque.anios,
        area: "area 1",
        bloque: "bloque 1",
        estado: "A",
        idUsuarioModificacion: 0,
        idUsuarioRegistro: this.idUsuarioRegistro,
        id_ordenprotec_divadm_umf: 0,
        id_plan_manejo: this.idPGMF,
        numero: 1,
        numerobloque: this.objBosque.noBloques,
        porcentaje: 50,
        tipobloque_seccion: 1, //correspondiente a Bloques
        tipobosque: 1,
      },
    ];
    this.planificacionService
      .registroOrdenamientoProteccionDivAdm(params)
      .subscribe((response) => {
        //  this.obtenerOrdenamientoProteccion();
      });
  }

  obtenerOrdenamientoProteccion() {
    let params = {
      id_plan_manejo: this.idPGMF,
    };
    this.planificacionService
      .obtenerOrdenamientoProteccionDivAdm(params)
      .subscribe((response) => {});
  }

  guardarParcelaCorta() {
    let params = [
      {
        anios: "",
        area: "20",
        bloque: "1",
        estado: "A",
        idUsuarioModificacion: 0,
        idUsuarioRegistro: this.idUsuarioRegistro,
        id_ordenprotec_divadm_umf: 0,
        id_plan_manejo: this.idPGMF,
        numero: 1,
        numerobloque: 0,
        porcentaje: 20,
        tipobloque_seccion: 2, //correspondiente a parcela corta
        tipobosque: 1,
      },
    ];
    this.planificacionService
      .registroOrdenamientoProteccionDivAdm(params)
      .subscribe((response) => {
        // this.obtenerOrdenamientoProteccion();
      });
  }
 processFile(file: any, config: any) {
    let itemFile = file;
    try {
      const file_ = {} as FileModel;
      let val = 0;
      for (let item of this.filFiles) {
        if (item.descripcion == config.catOrd) {
          item.descripcion = config.catOrd;
          item.file = file;
          // item.nombreFile=this.file.name;
        }
      }
      if (val == 0) {
        file_.descripcion = config.catOrd;
        file_.file = file;
        // file.nombreFile=this.filFile.name;
        this.filFiles.push(file_);
      }

      let promise = Promise.resolve([]);
      promise = this.mapApi.loadShapeFile(itemFile);
      promise
        .then((data) => {
          let controls = this.calculateArea(data, config);
          if (controls.success === true) {
            this.calculateAreaTotal();
            this.createLayers(data, file, config);
          }
        })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      console.log(error);
    }
  }
  calculateArea(data: any, config: any) {
    let controls: any = {};
    let sumArea: any = null;
    console.log(data);
    data[0].features.forEach((t: any) => {
      let geometry: any = null;
      if ((t.geometry.type = "Polygon")) {
        geometry = { spatialReference: { wkid: 4326 } };
        geometry.rings = t.geometry.coordinates;
        let area = geometryEngine.geodesicArea(geometry, "hectares");
        sumArea += area;
        controls.success = true;
      } else {
        controls.success = false;
      }
    });
    //if (config.inServer == true) return;
    let sum1 = 0;
    for (let item of this.listOrdenamientoProteccion) {
      if (item.catOrdenamiento == config.catOrd) {
        item.area = Number(sumArea.toFixed(2));
        item.modificar = config.modificar;
        item.idLayer = config.idLayer;
        item.inServer = config.inServer;
        //  item.porcentaje=Number((area/sum1).toFixed(2));
        item.file = true;
      }
      sum1 += item.area;
    }

    for (let item of this.listOrdenamientoProteccion) {
      item.porcentaje = Number(((100 * item.area) / sum1).toFixed(2));
    }
    return controls;
  }
  calculateAreaTotal() {
    let sum1 = 0;
    let sum2 = 0;
    for (let item of this.listOrdenamientoProteccion) {
      sum1 += item.area;
      sum2 += item.porcentaje;
    }
    this.totalArea = `${sum1.toFixed(2)}`;
    this.totalPorcentaje = `${sum2.toFixed(2)} %`;
  }
  createLayers(layers: any, file: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.file = file;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      t.idLayer = config.idLayer;
      this.createLayer(t);
    });
  }
  onFileChange(e: any, catOrd: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.target.files[0];
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        let config = {
          idLayer: this.mapApi.Guid2.newGuid,
          inServer: false,
          catOrd: catOrd,
          modificar: true,
        };
        while (i < e.target.files.length) {
          this.processFile(e.target.files[i], config);
          i++;
        }
      }
    }
  }

  listarOrdenamientoAreaManejo() {
    const planManejo = {} as PlanManejoModel;
    //let idPlanManejo = sessionStorage.getItem('idPlanManejo');
    planManejo.idPlanManejo = this.idPGMF; //Number(idPlanManejo);
    planManejo.codigoParametro = "CATORDPGMF";
    this.servOrdenamientoAreaMAnejo
      .listarOrdenamientoAreaManejo(planManejo)
      .subscribe((result: any) => {
        this.listOrdenamientoAreaManejo = result.data;
        console.log(this.listOrdenamientoAreaManejo);
        let total = 0;
        let porcentaje = 0;
        for (let item of this.listOrdenamientoAreaManejo) {
          if (item.resultArchivo != null && item.resultArchivo != undefined) {
            //application/octet-stream application/x-zip-compressed
            if (
              item.resultArchivo.archivo != null &&
              item.resultArchivo.archivo != undefined
            ) {
              let blob = this.Base64toBlob(
                item.resultArchivo.archivo,
                "application/octet-stream"
              );
              let config = {
                idLayer: this.mapApi.Guid2.newGuid,
                inServer: true,
                catOrd: item.catOrdenamiento,
                modificar: false,
              };
              this.processFile(blob, config);
            }
          }

          porcentaje += item.porcentaje;
          total += item.area;
        }

        this.totalArea = `${total.toFixed(2)}`;
        this.totalPorcentaje = `${porcentaje.toFixed(2)} %`;
      });
  }

  openEliminarOrdenamiento(event: any, index: number, data: any) {
    data.idUsuarioElimina = this.usuario.idusuario;
    const planManejo = {} as PlanManejoModel;
    //let idPlanManejo = sessionStorage.getItem('idPlanManejo');
    planManejo.idPlanManejo = this.idPGMF; //Number(idPlanManejo);

    data.planManejo = planManejo;

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Esta seguro de eliminar el registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idOrdAreaManejo > 0) {
          let inServer = event.target.parentNode.dataset.inServer;
          let idLayer = event.target.id || event.target.parentNode.id;
          console.log(inServer);
          if (inServer === true) {
            this.servOrdenamientoAreaMAnejo
              .eliminarOrdenamientoAreaManejo(data)
              .subscribe((result: any) => {
                this.SuccessMensaje(result.message);
                //this.listarOrdenamientoAreaManejo();
              });
          }
          this.removeLayer(idLayer);
          for (let item of this.listOrdenamientoAreaManejo) {
            if (item.idOrdAreaManejo === data.idOrdAreaManejo) {
              item.file = false;
              item.accion = true;
              item.area = 0;
              item.porcentaje = 0;
            }
          }
          this.calculateAreaTotal();
        }
      },
    });
  } */
  comboCategoriaOrdenamiento() {
    let params = {
      parametro: {
        codigo: "CATORD",
      },
    };

    this.servOrdenamientoAreaMAnejo
      .comboCategoriaOrdenamiento(params)
      .subscribe((data: any) => {
        this.listCatOrdenamiento = data.data;
      });
  }

  ErrorMensaje(mensaje: any) {
    this.MessageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.MessageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  ngOnDestroy(): void {
    if (this.view) {
      // destroy the map view
      this.view.destroy();
    }
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }
}

interface Ordenamiento {
  categoria: string;
  areaHA: number;
  observacion: string;
  descripcion: string;
  actividad: string;
  actividadesRealizar: string;
  observacionDetalle: string;
}
