import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, EventEmitter, OnInit, Output, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { InformacionGeneralService, PlanificacionService, PlanManejoService, UsuarioService } from "@services";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { FileModel } from "src/app/model/util/File";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service";

import { ActivatedRoute, Router } from "@angular/router";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
@Component({
  selector: "app-tab-descarga-envio",
  templateUrl: "./tab-descarga-envio.component.html",
  styleUrls: ["./tab-descarga-envio.component.scss"],
})
export class TabDescargaEnvioComponent implements OnInit {
  @Output()
  public regresar = new EventEmitter();
  nombrefile = "";

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
	@Input() isShowObjEval!: boolean;
	@Input() isDisabledObjEval!: boolean;

  fileList: any[] = [];
  compiladoPGMF: any = null;
  compiladoFirmadoPGMF: any = null;

  filesFirmados: FileModel[] = [];
  fileFirmadoPGMF: FileModel = {} as FileModel;
  verEnviar: boolean = false;
  eliminarPMFI: boolean = true;
  idArchivoPMFI: number = 0;
  cargarPMFI: boolean = false;
  verPGMF: boolean = false;
  verDescargaPGMF: boolean = true;

  usuario!: UsuarioModel;
  informacionGeneral: any = {};
  bloquearBtnEnviar: boolean = true;
  nroReqTUPA: number = 0;

  static get EXTENSIONSAUTHORIZATION2() {
    return [
      ".pdf",
      "image/png",
      "image/jpeg",
      "image/jpeg",
      "application/pdf"
    ];
  }

  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFIG",
      label: "1. Información General",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFOM",
      label: "2. Objetivos del Manejo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFIBU",
      label: "3. Información Básica de la UMF",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFOPU",
      label: "4. Ordenamiento y Protección de la UMF",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFPRFM",
      label: "5. Potencial de Producción del Recurso Forestal Maderable",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFMF",
      label: "6. Manejo Forestal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFEIA",
      label: "7. Evaluación de Impacto Ambiental",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFMO",
      label: "8. Monitoreo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFPC",
      label: "9. Participación Ciudadana",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFCAP",
      label: "10. Capacitación",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFORMA",
      label: "11. Organización del Manejo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },

    {
      idPlanManejoEstado: 0,
      codigo: "PGMFPI",
      label: "12. Programa de Inversiones",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFCA",
      label: "13. Cronograma de Actividades",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFA",
      label: "14. Anexos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    }
  ];

  pendientes: any[] = [];
  CodigoProceso = CodigoProceso;
  isVerObservacion: boolean = false;

  constructor(private planServ: PlanManejoService,
    private usuarioServ: UsuarioService,
    private planificacionService: PlanificacionService,
    private messageService: MessageService,
    private informacionGeneralService: InformacionGeneralService,
    private dialog: MatDialog,
    private toast: ToastService,
    private route: ActivatedRoute,
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private router: Router,) {

  }


  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    this.listarArchivosPlan();
    this.obtenerInfoGeneral(this.idPGMF);
    this.fileFirmadoPGMF.inServer = false;
    this.fileFirmadoPGMF.descripcion = "PDF";
    this.obtenerPlan();
    this.listarEstados();
  }

  obtenerInfoGeneral(idPlanManejo: number) {
    this.informacionGeneralService.obtener(idPlanManejo)
      .subscribe((result: any) => {
        if (result.success) {
          if (result.data != null) {

            this.informacionGeneral = result.data;
          }
        }
      });
  };

  regresarTab() {
    this.regresar.emit();
  }
  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === "PDF") {
          include = TabDescargaEnvioComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "el tipo de documento no es válido",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB",
          });
        } else {
          if (type === "PDF") {
            this.fileFirmadoPGMF.nombreFile = e.target.files[0].name;
            this.fileFirmadoPGMF.file = e.target.files[0];
            this.fileFirmadoPGMF.descripcion = type;
            this.fileFirmadoPGMF.inServer = false;
            this.filesFirmados.push(this.fileFirmadoPGMF);
            this.verEnviar = true;
          }
        }
      }
    }
  }


  listarArchivosPlan() {
    let params =
    {
      idPlanManejo: this.idPGMF,
      CodigoProceso: 'PGMF'
    }

    this.planServ.listarPorFiltroPlanManejoArchivo(params).subscribe((result: any) => {
      if (result.data) {
        this.fileList = result.data;
        this.nroReqTUPA = result.data.length;
      }
    })
  }



  eliminarArchivoPmfi() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivoPMFI))
      .set("idUsuarioElimina", String(this.usuarioServ.idUsuario));

    this.toast.ok("Se eliminó el PMFI firmado correctamente");

    /*  this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el PMFI firmado correctamente");
        this.cargarPMFI = false;
        this.eliminarPMFI = true;
        this.filePMFI.nombreFile = ''
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });*/
  }

  enviarSolicitud() {
     if (this.nroReqTUPA == 0) {
      this.ErrorMensaje("Debe ingresar requisitos TUPA");
      return;
     }

    let params: any =
    {
      idPlanManejo: this.idPGMF,
      nombreTitular: this.informacionGeneral.razonSocialTitularTH==null?(this.informacionGeneral.nombreTitularTH+" "+this.informacionGeneral.apellidoPaternoTitularTH+" "+this.informacionGeneral.apellidoMaternoTitularTH):this.informacionGeneral.razonSocialTitularTH,
      idDepartamento: this.informacionGeneral.departamento,
      idUsuario: this.usuario.idusuario,
      fecha: this.informacionGeneral.fechaPresentacion,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService.enviarEvaluacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      this.bloquearBtnEnviar = true;
      this.toast.ok("El correo fue notificado al titular/regente correctamente.");
      this.router.navigate(["/planificacion/bandeja-PGMF"]);
    },(error: HttpErrorResponse) => {
      this.ErrorMensaje(error.message);
      this.dialog.closeAll();
    });
  }

  registrarArchivo(param: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.usuario.idusuario,
      codTipoDocumento: param.idTipoDocumento,
      codTipo: "",
      subCodTipo: "",
      idPlanManejoArchivo: 0
    }

    
    this.planServ.regitrarArchivoPlanManejo(params, param.file).subscribe((result: any) => {
      

      this.dialog.closeAll();
      if (result.success) {
        this.SuccessMensaje("Se registró el requisito TUPA.");
        this.listarArchivosPlan();
      }else{
        this.ErrorMensaje("Ocurrió un error.");
      }
    },(error: HttpErrorResponse) => {
      this.dialog.closeAll();
      this.ErrorMensaje(error.message);
    });
  }

  actualizarArchivo(param: any) {
    
    var p = new HttpParams()
      .set('idPlanManejoArchivo', param.archivoModificar.idPlanManejoArchivo)
      .set('idUsuario', this.usuario.idusuario.toString())
      .set('idTipoDocumento', param.archivoModificar.idTipoDocumento)
      .set('idArchivo', param.archivoModificar.idArchivo);


    

    this.guardarArchivo(p, param.file);
  }

  consolidadoPGMF_PDF() {
    let param: any = {idPlanManejo: this.idPGMF};
    this.dialog.open(LoadingComponent, { disableClose: true });
    //let token: any = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.cargaEnvioDocumentacionService.consolidadoPGMF_PDF(param).pipe(finalize(() => this.dialog.closeAll())).subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.compiladoPGMF = res;
          this.verDescargaPGMF = false;
        } else {
          this.toast.error("Ocurrió un error al realizar la operación");
        }
      }, err => {
        this.toast.warn("Para generar el consolidado del Plan General  de Manejo Forestal - PGMF. Debe tener todos los items completados previamente.");
    });
  }


  compilarPGMF() {

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService.generarEvaluacionCompiladaPGMF(21, "", "")
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.compiladoPGMF = res;
          
          this.verDescargaPGMF = false;
          //this.verDescargaPMFI = false;
        } else {
          this.toast.error(res?.message);
        }
      })
  }

  descargarPGMF() {
    if (isNullOrEmpty(this.compiladoPGMF)) {
      this.toast.warn('Debe generar archivo compilado PGMF');
      return;
    }
    descargarArchivo(this.compiladoPGMF);
  }

  registrarArchivoId(event: any, auxNumero: number){
    if(event!=0 && event!=null){
      if(auxNumero === 1) {
        this.bloquearBtnEnviar=false;
      }
    }else{
      this.bloquearBtnEnviar=true;
    }
  }

  eliminarArchivoId(ok: boolean, auxNumero: number) {
    if(!ok) return;
    if(auxNumero === 1) {
      this.bloquearBtnEnviar=true;
    }
  }

  private guardarArchivo(params: any, file: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });

    const formData = new FormData();
    formData.append("file", file);

    this.planServ.guardarPlanManejoArchivo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();

      if (result.success) {
        this.SuccessMensaje(result.message);
        this.listarArchivosPlan();

      }
    },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    )
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }


  private ErrorMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'warn',
      key: 'tr',
      summary: '',
      detail: mensaje,
    });
  }

  obtenerPlan() {
    this.idPGMF = Number(this.route.snapshot.paramMap.get('idPlan'));
    let params: any =
    {
      idPlanManejo: this.idPGMF
    };
    this.planServ.obtenerPlanManejo(params).subscribe((result: any) => {
      if (result.data) {
        this.codigoEstado = result.data.codigoEstado;
        this.isVerObservacion = this.codigoEstado === 'EPLMOBSM';

        if (this.usuario.sirperfil == 'TITULARTH' &&
          (this.codigoEstado == 'EPLMCOMP' ||
            this.codigoEstado == 'EPLMOBSM' ||
            this.codigoEstado == 'EPLMOBSG')) {
          this.bloquearBtnEnviar = false;
        }
        else {
          this.bloquearBtnEnviar = true;
        }
      }

      (error: HttpErrorResponse) => {

      }
    });


  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }
  };

  guardarEstados() {

    let idPlanManejoAct = this.idPGMF;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: "PGMF", //
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        observacion: "",
      };
      listaEstadosPlanManejo.push(obje);
    });
// CAMBIAR POR EL SERVICIO QUE CORRESPONDA
    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == "CPMPEND") {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();

          this.messageService.add({
            key: "tl",
            severity: "success",
            summary: "",
            detail: response.message,
          });
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: response.message,
          });
        }
      });

  }

  listarEstados() {
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPGMF,
      codigoProceso: this.CodigoProceso.PLAN_GENERAL_MANEJO_FORESTAL, //
      idPlanManejoEstado: null,
    };
// CAMBIAR POR EL SERVICIO QUE CORRESPONDA
    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == "CPMPEND") {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == "CPMPEND") {
            this.listProcesos[objIndex].estado = "Pendiente";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == "CPMTERM") {
            this.listProcesos[objIndex].estado = "Terminado";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });
        this.pendiente();
      });
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPGMF = true;
    } else if (this.pendientes.length == 0) {
      this.verPGMF = false;
    }
  }
}
