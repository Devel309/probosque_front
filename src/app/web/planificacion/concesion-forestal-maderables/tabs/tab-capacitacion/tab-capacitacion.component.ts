import { style } from "@angular/animations";
import { HttpErrorResponse } from "@angular/common/http";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ParametroModel } from "@models";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AnexoAdjuntoModel } from "src/app/model/AnexoAdjunto";
import { CapacitacionModel } from "src/app/model/Capacitacion";
import { CapacitacionDetalleModel } from "src/app/model/CapacitacionDetalle";
import { CapacitacionPGMFA, ListCapacitacionDetalle } from "src/app/model/capacitacionPGMFA";
import { CodigoPGMF } from "src/app/model/CodigoPGMF";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { UsuarioModel } from "src/app/model/Usuario";
import { AnexoAdjuntoService } from "src/app/service/AnexoAdjunto.service";
import { PlanManejoEvaluacionService } from "src/app/service/plan-manejo-evaluacion.service";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { CapacitacionService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/capacitacion.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { NuevoTemaComponent } from "./nuevo-tema/nuevo-tema.component";

@Component({
  selector: "app-tab-capacitacion",
  templateUrl: "./tab-capacitacion.component.html",
})
export class TabCapacitacionComponent implements OnInit {
  isObjetivo: boolean = false;

  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();
  listaObjetivo: any[] = [];
  listaActividades: any[] = [];
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  idPlanManejoArchivo: number = 0;
  codigo_PGMF = 'PGMF10'
  codigo_anexo4: string = CodigoPGMF.TAB_10;

  objUser: UsuarioModel = JSON.parse(
    "" + localStorage.getItem("usuario")?.toString()
  );
  ref!: DynamicDialogRef;
  anexoAdjunto = {} as AnexoAdjuntoModel;
  listaAnexoAdjunto: any[] = [];

  listObjCapacitacion: any[] = [];
  params_insetar_detalle = {
    idCapacitacion: 0,
    idPlanManejo: 0,
    tema: "",
    codTipoCapacitacion: "",
    idUsuarioRegistro: this.objUser.idusuario,
    listCapacitacionDetalle: []
  };

  /* ********** CLONADO ************* */ 
  capacitaciones: any[] = [];
  selectAnexo: string = "N";
  temas = [] as any;
  tituloModal: string = "Registrar Capacitación";
  capacitacionObj: CapacitacionPGMFA = new CapacitacionPGMFA();
  listCapacitacion: CapacitacionPGMFA[] = [];
  detalleLista: ListCapacitacionDetalle[] = [];
  parametro = {} as ParametroModel;
  capacitacion = {} as CapacitacionModel;
  displayBasic: boolean = false;
  tema = [] as any;
  edit: boolean = false;
  usuario = {} as UsuarioModel;
  idPlanManejo!: number;
  rowIndex: number = 0;
  listParametro = [
    { valorPrimario: "Presencial", valorSecundario: "1" },
    { valorPrimario: "Virtual", valorSecundario: "2" },
  ];
  /* ************************** */ 
  fileAnexo4: any = {};

  constructor(
    public dialogService: DialogService,
    private serv: PlanificacionService,
    private messageService: MessageService,
    private servAnexoAdjunto: AnexoAdjuntoService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,

    /* ****** CLONADO *******/
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private capacitacionService: CapacitacionService,
    private mitoast: ToastService,
    private postulacionPFDMService: PostulacionPFDMService
    /* **************** */
  ) { }

  ngOnInit() {
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
    this.obtenerCapacitacion();
    this.listarAnexo();

    /* ********** CLONADO ************* */ 
    this.idPlanManejo = this.idPGMF; //cambiar por: this.idPGMF
    this.listaCapacitacion();
    /* *********************** */ 
  }

   /* *********** INICIO CLONADO ******************** */

  listaCapacitacion() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoCapacitacion: "PGMF",// Cambiar por: PGMF
    };
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.capacitacionService.listarCapacitacion(params).subscribe((response: any) => {
      load.close();

      this.capacitaciones = response.data;
      this.selectAnexo = this.capacitaciones[0].adjunto || "N";
    }, () => {
      load.close();
    });
  }

  guardarCapacitacion() {
    if (!this.validarGuardar()) return;

    this.listCapacitacion = [];

    this.capacitaciones.forEach((response: any) => {
      this.capacitacionObj = new CapacitacionPGMFA(response);
      this.capacitacionObj.idCapacitacion = response.idCapacitacion;
      this.capacitacionObj.codTipoCapacitacion = "PGMF";
      this.capacitacionObj.idUsuarioRegistro = this.user.idUsuario;
      this.capacitacionObj.adjunto = this.selectAnexo;
      this.capacitacionObj.idPlanManejo = this.idPlanManejo;
      for (let val of this.listParametro) {
        if (response.idTipoModalidad == val.valorPrimario) {
          this.capacitacionObj.idTipoModalidad = val.valorSecundario;
        }
      }
      this.detalleLista = [];
      response.listCapacitacionDetalle.forEach((item: any) => {
        let capacitacionDetalle = new ListCapacitacionDetalle(item);
        capacitacionDetalle.idUsuarioRegistro = this.user.idUsuario;
        //capacitacionDetalle
        this.detalleLista.push(capacitacionDetalle);
      });
      this.capacitacionObj.listCapacitacionDetalle = this.detalleLista;
      this.listCapacitacion.push(this.capacitacionObj);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.capacitacionService
      .registrarCapacitacionDetalle(this.listCapacitacion)
      .subscribe(
        (result: any) => {
          if (result.success == true) {
            this.dialog.closeAll();
            this.listaCapacitacion();
            this.messageService.add({
              severity: "success",
              summary: "",
              detail: result.message,
            });

            this.mitoast.ok('Se registró la capacitación correctamente');
          } else {
            this.mitoast.error(result?.message);
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );

  }

  openEditarCapacitacion(obj: any, rowIndex: number) {
    this.capacitacion.tema = obj.tema;
    this.capacitacion.idCapacitacion = obj.idCapacitacion;
    this.capacitacion.personaCapacitar = obj.personaCapacitar;
    this.capacitacion.lugar = obj.lugar;
    this.parametro.valorSecundario = obj.idTipoModalidad;
    this.temas = obj.listCapacitacionDetalle;
    this.capacitacion.idUsuarioRegistro = this.usuario.idusuario;
    this.capacitacion.estado = "A";
    this.capacitacion.periodo = obj.periodo
    this.capacitacion.responsable = obj.responsable
    if (obj.idTipoModalidad === "Presencial" || obj.idTipoModalidad === null) {
      this.parametro.valorSecundario = "1"
    } else {
      this.parametro.valorSecundario = "2"
    }
    this.rowIndex = rowIndex;

    this.displayBasic = true;
    this.edit = true;
    this.tituloModal = "Editar Capacitación";
  }

  openEliminarCapacitacion(event: Event, index: number, obj: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (obj.idCapacitacion != 0) {
          var parm = {
            idCapacitacion: obj.idCapacitacion,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.capacitacionService
            .eliminarCapacitacion(parm)
            .subscribe((res: any) => {
              if (res.success == true) {
                this.temas.splice(index, 1);
                this.mitoast.ok(res?.message);
                this.obtenerCapacitacion();
                this.listaCapacitacion();
                this.dialog.closeAll();
              } else {
                this.mitoast.error(res?.message);
                this.dialog.closeAll();
              }
            });
        } else {
          this.capacitaciones.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  //INICIO MODAL 
  abrirModal() {
    this.temas = [];
    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = true;
  }

  cerrarModal() {
    this.temas = [];
    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = "Registrar Capacitación";
  }
  
  agregarActividad() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.tema == null || this.tema == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar el Tema o Actividad.\n";
    }
    if (!validar) {
      this.ErrorMensaje(mensaje);
    } else {
      const obj = {} as any;
      obj.actividad = this.tema;
      obj.idCapacitacionDet = 0;
      this.temas.push(obj);
      this.tema = "";
    }
  }

  eliminarTema(event: Event, index: number, params: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (params.idCapacitacionDet != 0) {
          var parm = {
            idCapacitacionDet: params.idCapacitacionDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.capacitacionService
            .eliminarCapacitacionDetalle(parm)
            .subscribe((res: any) => {
              if (res.success == true) {
                this.temas.splice(index, 1);
                // this.toast.ok(res?.message);
                this.mitoast.ok(
                  "Se eliminó la actividad de la capacitación con éxito."
                );
              } else {
                this.mitoast.error(res?.message);
              }
            });
        } else {
          this.temas.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  agregarParticipacion() {
    if (!this.validarCapacitacion()) {
      return;
    }
    if (this.edit) {
      this.editarCapacitacion();
    } else {
      this.registrarCapacitacion();
    }
  }

  validarCapacitacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.temas == null || this.temas.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Tema o Actividad.\n";
    }
    if (
      this.capacitacion.personaCapacitar == null ||
      this.capacitacion.personaCapacitar == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Personal.\n";
    }
    if (
      this.parametro.valorSecundario == null ||
      this.parametro.valorSecundario == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Modalidad.\n";
    }
    if (this.capacitacion.lugar == null || this.capacitacion.lugar == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar Lugar.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  editarCapacitacion() {
    const obj = {} as any;
    obj.idCapacitacion = this.capacitacion.idCapacitacion;
    obj.estado = "A";
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.lugar = this.capacitacion.lugar;
    obj.personaCapacitar = this.capacitacion.personaCapacitar;
    obj.periodo = this.capacitacion.periodo;
    obj.responsable = this.capacitacion.responsable;
    obj.tema = this.capacitacion.tema;
    obj.idTipoModalidad = this.parametro.valorSecundario;
    for (let val of this.listParametro) {
      if (val.valorSecundario == this.parametro.valorSecundario) {
        obj.idTipoModalidad = val.valorPrimario;
      }
    }
    const listCapDet: CapacitacionDetalleModel[] = [];
    for (let item of this.temas) {
      const cap = {} as CapacitacionDetalleModel;
      cap.actividad = item.actividad;
      cap.idCapacitacionDet = item.idCapacitacionDet;
      listCapDet.push(cap);
    }

    obj.listCapacitacionDetalle = listCapDet;
    obj.idPlanManejo = this.idPlanManejo;

    this.capacitaciones[this.rowIndex] = obj;

    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;

    this.displayBasic = false;
    this.tituloModal = "Registrar Capacitación";
    this.edit = false;
  }

  registrarCapacitacion() {
    const obj = {} as any;
    obj.idCapacitacion = 0;
    obj.estado = "A";
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.lugar = this.capacitacion.lugar;
    obj.personaCapacitar = this.capacitacion.personaCapacitar;
    obj.periodo = this.capacitacion.periodo;
    obj.responsable = this.capacitacion.responsable;

    obj.idTipoModalidad = this.parametro.valorSecundario;
    for (let val of this.listParametro) {
      if (val.valorSecundario == this.parametro.valorSecundario) {
        obj.idTipoModalidad = val.valorPrimario;
      }
    }
    const listCapDet: CapacitacionDetalleModel[] = [];
    for (let item of this.temas) {
      const cap = {} as CapacitacionDetalleModel;
      cap.actividad = item.actividad;
      cap.idCapacitacionDet = 0;
      listCapDet.push(cap);
    }

    obj.listCapacitacionDetalle = listCapDet;
    obj.idPlanManejo = this.idPlanManejo;
    this.capacitaciones.push(obj);

    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;
  }

  //FIN MODAL

  ErrorMensaje(mensaje: any) {
    this.mitoast.warn(mensaje);
  }

  /* *********** FIN CLONADO******************** */



  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN15', this.lineamiento);
  }

  dataLineamientos(data: any) {

    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN15' }
    );
  }

  obtenerCapacitacion() {
    this.serv.capacitacion({ idPlanManejo: this.idPGMF, codTipoCapacitacion: 'PGMFOBJ' }).subscribe((result: any) => {
      this.listaObjetivo = [];
      this.listaActividades = [];

      if (result.success == true) {
        result.data.forEach((element: any) => {
            this.listaObjetivo.push(element);
        });
        this.listaSubTipos();
      } else {
        this.toast(
          "warn",
          "No se encontró registros en la sección Capacitación"
        );
      }
    });
  }

  listaTipos: any[] = [];
  listaSubTipos() {
    let items: any[] = [];
    this.listaTipos = [{
      nombreSubTipo: "Planeamiento e Inventario"
    },
    {
      nombreSubTipo: "Operaciones de Aprovechamiento"
    },
    {
      nombreSubTipo: "Actividades silviculturales"
    },
    {
      nombreSubTipo: "Trazabilidad"
    }];
    //this.listaActividades[0].html = html;

    let coutnTipo = 0;
    let i = 0;

    
    if (this.listaActividades.length > 0) {

      this.listaTipos.push(this.listaActividades[0]);

      this.listaActividades.forEach((element) => {
        i++;
        if (element.subTipo != this.listaTipos[coutnTipo].subTipo) {
          this.listaTipos[coutnTipo].items = items;
          this.listaTipos.push(element);
          coutnTipo++;
          items = [];
        }

        items.push({ d: element.descripcion, i: element.id });
        if (i == this.listaActividades.length) {
          this.listaTipos[coutnTipo].items = items;
        }
      });
    }

  }

  text_objetivo = "";
  agregarObjetivo = () => {
    if (this.text_objetivo == null || this.text_objetivo == "") {
      this.ErrorMensaje("Debe ingresar Objetivo.");
      return;
    }
    this.listObjCapacitacion = [];
    this.params_insetar_detalle.idPlanManejo = this.idPGMF;
    this.params_insetar_detalle.tema = this.text_objetivo;
    this.params_insetar_detalle.codTipoCapacitacion = "PGMFOBJ";
    this.listObjCapacitacion.push(this.params_insetar_detalle);
    
    
    this.serv
      .detalleRegistrar(this.listObjCapacitacion)
      .subscribe((result: any) => {
        if (result.success == true) {
          this.toast("success", "Guardado correctamente");
          this.text_objetivo = "";
          this.obtenerCapacitacion();
        } else {
          this.toast("error", "Ocurrió un problema");
        }
      });
  };

  adjuntarAnexo() {
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
    this.anexoAdjunto.codigoTab = "PGMFCAP";
    this.anexoAdjunto.adjuntaAnexo = (this.selectAnexo == "S") ? true : false;
    this.anexoAdjunto.idUsuarioRegistro = 1;
    this.servAnexoAdjunto.marcarParaAmpliacionAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {
      if (result.success == true) {
        this.toast("success", "Guardado correctamente");
        this.obtenerCapacitacion();
      } else {
        this.toast("error", "Ocurrió un problema");
      }
    });
  }

  listarAnexo() {
    //NOTA: no funciona siempre trae el codigo "PGMFORMA"
    this.servAnexoAdjunto.listarAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {
      for (let item of result.data) {
        if (item.codigoTab == "PGMFCAP") {
          // this.selectAnexo = (item.adjuntaAnexo == true) ? "S" : "N";
        }
      };
    });
  }

  modalTema(padre: any) {
    padre.idPGMF = this.idPGMF;

    this.ref = this.dialogService.open(NuevoTemaComponent, {
      header: "Agregar Tema o Actividad",
      width: "40%",
      contentStyle: { "min-height": "180px", overflow: "auto" },
      data: {
        padre: padre,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if (resp == "ok") {
        this.obtenerCapacitacion();
      }
    });
  }

  eliminarDetalle(id: Number) {
    
    this.serv
      .detalleEliminar({
        idCapacitacion: id,
        idUsuarioElimina: this.objUser.idusuario,
      })
      .subscribe((result: any) => {
        
        if (result.success == true) {
          this.obtenerCapacitacion();
          this.toast("success", "Eliminado correctamente");
        } else {
          this.toast("error", "Ocurrió un problema");
        }
      });
  }

  toast(saverty: String, datail: String) {
    this.messageService.add({
      key: "toast",
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.selectAnexo = "S";
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {
        if(response.success) this.limpiarFile();
      });
  }

  limpiarFile() {
    this.idPlanManejoArchivo = 0;
    this.fileAnexo4.nombreFile = "";
    this.fileAnexo4.justificacion = "";
  }

  validarGuardar(): boolean{
    let valido = true;
    if (this.capacitaciones == null || this.capacitaciones.length == 0) {
      valido = false;
      this.mitoast.warn("No hay Capacitaciones registradas.");
    }
    
    if (this.selectAnexo === "S") {
      if (!this.fileAnexo4.justificacion) {
        this.mitoast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4.nombreFile) {
        this.mitoast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

}
