import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { UsuarioModel } from 'src/app/model/Usuario';
import { PlanificacionService } from 'src/app/service/planificacion.service';

@Component({
  selector: 'app-nuevo-tema',
  templateUrl: './nuevo-tema.component.html',
})
export class NuevoTemaComponent implements OnInit {
  
  objUser : UsuarioModel = JSON.parse(''+localStorage.getItem("usuario")?.toString() );
  params_insetar_detalle = {
    codigoSeccion : "SECCAPA",
    codigoSubTipoDetalle: "",
    codigoTipoDetalle: "",
    descripcion: "",
    fecha: new Date(),
    idPGMF: 0,
    idUsuarioRegistro: this.objUser.idusuario,
    lugar: "",
    mecanismoParticipacion: "",
    metodologia: "",
    modalidadCapacitar: "",
    personalCapacitar: ""
  }

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private serv : PlanificacionService, 
    private messageService: MessageService,
  ) {}

  ngOnInit() {
  }

  texto_tema_actividad = "";
  agregar(){
    this.params_insetar_detalle.descripcion = this.texto_tema_actividad;
    this.params_insetar_detalle.idPGMF = this.config.data.padre.idPGMF; 
    this.params_insetar_detalle.codigoTipoDetalle = this.config.data.padre.tipo; 
    this.params_insetar_detalle.codigoSubTipoDetalle = this.config.data.padre.subTipo; 
    
    this.params_insetar_detalle.lugar = this.config.data.padre.lugar; 
    this.params_insetar_detalle.modalidadCapacitar = this.config.data.padre.modalidadCapacitar;
    this.params_insetar_detalle.personalCapacitar  = this.config.data.padre.personalCapacitar ;    
    

    

    if(this.texto_tema_actividad){
      this.serv.detalleRegistrar(this.params_insetar_detalle).subscribe(
        (result : any)=>{
          if(result.success == true){            
            this.cerrarModal();
          }else{
            this.toast('error','Ocurrió un problema');
          }
        }
      );
    }else{
      this.toast('warn','Debe escribir un Tema o Actividad');
    }
  }

  cerrarModal() {
    if (this.ref) {
        this.ref.close('ok');
    }
  }
  
  toast(saverty : String , datail: String){
    
    this.messageService.add({
      key: 'toast',
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }
}
