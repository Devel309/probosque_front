import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-funciones',
  templateUrl: './funciones.component.html',
})
export class FuncionesComponent implements OnInit {
  funcionObj: any;
  listaFuncion: any[] = [];
  funcion: string = '';
  isFuncion: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit() {
    this.funcionObj = { ...this.config.data.item };
    this.listaFuncion = [...this.config.data.item.listaFuncion];
  }

  agregarFuncion = () => {
    this.listaFuncion.push({
      idFuncion: this.listaFuncion.length + 1,
      descripcion: this.funcion,
    });
    this.funcion = '';
    this.isFuncion = false;
  };

  agregar = () => {
    this.funcionObj.listaFuncion = this.listaFuncion;
    this.ref.close(this.funcionObj);
  };
}
