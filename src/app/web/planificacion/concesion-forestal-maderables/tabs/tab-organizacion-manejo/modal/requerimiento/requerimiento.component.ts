import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { UsuarioModel } from 'src/app/model/Usuario';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { ToastService } from '@shared';

@Component({
  selector: 'app-requerimiento',
  templateUrl: './requerimiento.component.html',
  styleUrls:['./requerimiento.component.scss']
})
export class RequerimientoComponent implements OnInit {
  funcionObj: any;

  objUser : UsuarioModel = JSON.parse(''+localStorage.getItem("usuario")?.toString() );
  params_insetar_detalle = {
    codigoSeccion : "SECORGM",
    idOrganizacionManejo:  0,
    codigoSubTipoDetalle: '',
    codigoTipoDetalle: "",
    codigoTipoActividad: '',
    cabecera: false, tipoActividad : "",
    maquinaEquipo: "", 
    numero : 0,
    funcion: "", descripcion: "",
    medidaControl:"",
    medidaMonitoreo:"",
    frecuencia:"",
    responsable:"",
    contingencia:"",
    accionRealizada:"",
    nombreTipoDetalle:"",
    fecha: new Date(), idPGMF: 0,
    idUsuarioRegistro: this.objUser.idusuario,
    lugar: "",mecanismoParticipacion: "",
    metodologia: "", modalidadCapacitar: "",
    personalCapacitar: "", operacion :""
  }
  padre  : any; 
  tipoActividad: any[] = [];
  titulo:any;
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private serv : PlanificacionService,
    private parametroValorService: ParametroValorService,
    private toast: ToastService,
    private messageService: MessageService
  ) {}
    
  ngOnInit() {
    this.padre = this.config.data;
    
    this.listTipoActividad();
    
    this.titulo =this.padre.titulo
    if(this.padre.titulo != "Nuevo"){
    this.params_insetar_detalle.idOrganizacionManejo = this.padre.data.i;
    this.params_insetar_detalle.funcion = this.padre.data.f;
    this.params_insetar_detalle.descripcion = this.padre.data.d;
    this.params_insetar_detalle.numero = this.padre.data.n;
    }
    
  }

  listTipoActividad() {

    let strPrefijo="";

    if(this.config.data.padre=="11.2"){
      strPrefijo='TACTMNJRQPS';
    }
    if(this.config.data.padre=="11.3"){
      strPrefijo='TACTMNJRQMQ';
    }

    var params = {
      prefijo: strPrefijo,
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.tipoActividad = [...response.data];
      });
  }

  validarOrganizacionManejo(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if(this.config.data.padre == '11.3'){
     
      if(this.params_insetar_detalle.codigoTipoActividad == null || this.params_insetar_detalle.codigoTipoActividad == ''){
        validar = false;
        mensaje = mensaje += '(*) Debe seleccionar el tipo de actividad.\n';
      }

      if(this.params_insetar_detalle.maquinaEquipo == null || this.params_insetar_detalle.maquinaEquipo == '' ){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar la maquinaria/equipo.\n';
      }

      if(!this.params_insetar_detalle.numero){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar el número.\n';
      } 

    } 

    if(this.config.data.padre == '11.1'){
     
      if(this.params_insetar_detalle.descripcion == null || this.params_insetar_detalle.descripcion == '' ){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar la descripción.\n';
      }

      if(this.params_insetar_detalle.funcion == null || this.params_insetar_detalle.funcion == '' ){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar la función.\n';
      }

      if(!this.params_insetar_detalle.numero || this.params_insetar_detalle.numero <= 0){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar el número.\n';
      } 

    }

    if(this.config.data.padre == '11.2'){
    
      if(this.params_insetar_detalle.codigoTipoActividad == null || this.params_insetar_detalle.codigoTipoActividad == ''){
        validar = false;
        mensaje = mensaje += '(*) Debe seleccionar el tipo de actividad.\n';
      }
      
      if(this.params_insetar_detalle.funcion == null || this.params_insetar_detalle.funcion == '' ){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar la función.\n';
      }

      if(!this.params_insetar_detalle.numero || this.params_insetar_detalle.numero <= 0){
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar el número.\n';
      } 
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  guardar(){
    let ok  = false; 
    if(!this.validarOrganizacionManejo()) return;

    if(this.config.data.padre == '11.3'){
      this.params_insetar_detalle.codigoTipoDetalle = 'REQMAQU';
      this.params_insetar_detalle.codigoSubTipoDetalle = '';
      
    }
    if(this.config.data.padre == '11.2'){
      this.params_insetar_detalle.codigoTipoDetalle = 'REQPERS';
      this.params_insetar_detalle.codigoSubTipoDetalle = '';

    }
    if(this.config.data.padre == '11.1'){
      this.params_insetar_detalle.codigoSeccion = 'SECORFUN';
      this.params_insetar_detalle.codigoTipoDetalle = 'FUNRESP';
      this.params_insetar_detalle.codigoSubTipoDetalle = this.config.data.codigoSubTipo; 
      
    }

      this.params_insetar_detalle.idPGMF = this.config.data.idPGMF; 
      
      
      
      this.serv.PGMFAbreviadoDetalleRegistrar(this.params_insetar_detalle).subscribe(
        (result : any)=>{
          if(result.success == true){
 
            this.cerrarModal();
          }else{
            //this.toast('error','Ocurrió un problema');
          }
        }
      );

  }

  cerrarModal() {
    //if (this.ref) {
        this.ref.close('ok');
    //}
  }

  close(){
    this.ref.close()
  }

  /*
  toast(saverty : String , datail: String){
    
    this.messageService.add({
      key: 'toast',
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }
  */
}
