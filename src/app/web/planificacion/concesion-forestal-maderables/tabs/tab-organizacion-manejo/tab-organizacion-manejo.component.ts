import {HttpErrorResponse} from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AnexoAdjuntoModel } from 'src/app/model/AnexoAdjunto';
import { CodigoPGMF } from 'src/app/model/CodigoPGMF';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { UsuarioModel } from 'src/app/model/Usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize } from "rxjs/operators";
import { AnexoAdjuntoService } from 'src/app/service/AnexoAdjunto.service';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { FuncionesComponent } from './modal/funciones/funciones.component';
import { RequerimientoComponent } from './modal/requerimiento/requerimiento.component';
import { textChangeRangeIsUnchanged } from 'typescript';

@Component({
  selector: 'app-tab-organizacion-manejo',
  templateUrl: './tab-organizacion-manejo.component.html',
})
export class TabOrganizacionManejoComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
	@Input() isShowObjEval!: boolean;
	@Input() isDisabledObjEval!: boolean;
  
  objUser: UsuarioModel = JSON.parse('' + localStorage.getItem("usuario")?.toString());
  organizacion: string = '';
  listaFunciones: any[] = [];
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento4: LineamientoInnerModel = new LineamientoInnerModel();

  ref!: DynamicDialogRef;

  listaRequerimientoPersonal: any[] = [];
  listaRequerimientoMaquinaria: any[] = [];

  /* Anexo adjunto */
  anexoAdjunto = {} as AnexoAdjuntoModel;
  listaAnexoAdjunto: any[] = [];

  idPlanManejoArchivo: number = 0;
  codigoAnexo4 = CodigoPGMF.TAB_11;
  selectAnexo: string = "N";
  idPlanManejo!: number;

  fileAnexo4: any = {};

  constructor(public dialogService: DialogService,
    private dialog: MatDialog,
    private serv: PlanificacionService,
    private messageService: MessageService,
    private servAnexoAdjunto: AnexoAdjuntoService,
    private Toast: ToastService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private postulacionPFDMService: PostulacionPFDMService
  ) { }

  ngOnInit() {
    //this.idPGMF = 21;
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
    

    this.obtenerOrganizacionManejo();
    this.listarAnexo();
    this.idPlanManejo = this.idPGMF;

  }


  listarEvaluaciones(data: any) {
    // this.ordenarLista(data, 'PGMFEVALLIN16.1', this.lineamiento);
    // this.ordenarLista(data, 'PGMFEVALLIN16.2', this.lineamiento2);
    // this.ordenarLista(data, 'PGMFEVALLIN16.3', this.lineamiento3);
    this.ordenarLista(data, 'PGMFEVALLIN16', this.lineamiento4);
  }

  dataLineamientos(data: any) {

    // this.lineamiento = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN16.1' }
    // );
    // this.lineamiento2 = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN16.2' }
    // );
    // this.lineamiento3 = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN16.3' }
    // );
    this.lineamiento4 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN16' }
    );
  }

  adjuntarAnexo() {
    this.anexoAdjunto.idPlanManejo = this.idPGMF;
    this.anexoAdjunto.codigoTab = "PGMFORMA";
    this.anexoAdjunto.adjuntaAnexo = (this.selectAnexo == "S") ? true : false;
    this.anexoAdjunto.idUsuarioRegistro = 1;
    this.servAnexoAdjunto.marcarParaAmpliacionAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {
      if (result.success == true) {
        //this.toast("success", "Guardado correctamente");
        this.obtenerOrganizacionManejo();
      } else {
        //this.toast("error", "Ocurrió un problema");
      }
    });
  }

  listarAnexo() {
    this.servAnexoAdjunto.listarAnexoAdjunto(this.anexoAdjunto).subscribe((result: any) => {
      for (let item of result.data) {
        if (item.codigoTab == "PGMFORMA") {
          this.selectAnexo = (item.adjuntaAnexo == true) ? "S" : "N";
        }
      };
    });
  }

  obtenerOrganizacionManejo() {
    this.serv.organizacionManejo({ idPGMF: this.idPGMF }).subscribe(
      (result: any) => {
        //console.log(result.data);
        if (result.success == true) {
          
          this.listaFunciones = [];
          this.listaRequerimientoPersonal = [];
          this.listaRequerimientoMaquinaria = [];

          let auxListaFuncionesGerencial: any[]=[]
          let auxListaFuncionesProfesional: any[]=[]
          let auxListaFuncionesTecnico: any[]=[]
          let auxListaFuncionesOperario: any[]=[]
          let auxListaFuncionesPersonalCampo: any[]=[]
          let auxListaFuncionesPersonalServicio: any[]=[]
          let auxListaFuncionesAdministrativo: any[]=[]
          let auxListaFuncionesOtrosOperacios: any[]=[]
          

          result.data.forEach((element: any) => {
            if (element.tipo == 'FUNRESP') {
              element.items = [];
              
              switch (element.subTipo) {
                case "GERENC":
                  auxListaFuncionesGerencial.push(element)
                      break;
                case "PROFES":
                  auxListaFuncionesProfesional.push(element)
                  //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor2
                  break;
                
                case "TECNIC":
                  auxListaFuncionesTecnico.push(element)
                  //Declaraciones ejecutadas cuando el resultado de expresión coincide con valorN
                  break;
                case "OPERAR":
                  auxListaFuncionesOperario.push(element)
                    //Declaraciones ejecutadas cuando el resultado de expresión coincide con valorN
                    break;
                case "PERCAM":
                  auxListaFuncionesPersonalCampo.push(element)
                  //Declaraciones ejecutadas cuando el resultado de expresión coincide con valorN
                  break;
                case "PERSER":
                  auxListaFuncionesPersonalServicio.push(element)
                    //Declaraciones ejecutadas cuando el resultado de expresión coincide con valorN
                    break;
                case "ADMINI":
                  auxListaFuncionesAdministrativo.push(element)
                      //Declaraciones ejecutadas cuando el resultado de expresión coincide con valorN
                      break;
                case "OTROPER":
                  auxListaFuncionesOtrosOperacios.push(element)
                  //Declaraciones ejecutadas cuando el resultado de expresión coincide con valorN
                  break;

                default:
                  //Declaraciones ejecutadas cuando ninguno de los valores coincide con el valor de la expresión
                  break;
              }


              
             
            }
            if (element.tipo == 'REQPERS') {
              this.listaRequerimientoPersonal.push(element);
            }
            if (element.tipo == 'REQMAQU') {
              this.listaRequerimientoMaquinaria.push(element);
            }
          });
          auxListaFuncionesGerencial.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesProfesional.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesTecnico.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesOperario.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesPersonalCampo.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesPersonalServicio.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesAdministrativo.forEach(x => this.listaFunciones.push(x));
          auxListaFuncionesOtrosOperacios.forEach(x => this.listaFunciones.push(x));


          

          this.listaSubTipos_fr();
          //console.log(this.listaTipos_fr);
        }
      }
    );
  }

  listaTipos_fr: any[] = [];
  listaSubTipos_fr() {
    
    this.listaTipos_fr = [];
    let contTipos = -1;
    
    this.listaFunciones.forEach((element: any) => {
      if (element.esCabecera == true) {
        contTipos++;
        this.listaTipos_fr.push(element);
      }
      else {
        if( this.listaTipos_fr[contTipos].subTipo == element.subTipo )
        this.listaTipos_fr[contTipos].items.push(
          { f: element.funcion, d: element.descripcion, i: element.id, n: element.numero }
        );

      }
    });

    

  }

  nuevo(padre: string, codigoSubTipo: String, titulo: string) {

    let params = { idPGMF: this.idPGMF, padre: padre, codigoSubTipo: codigoSubTipo, titulo };
    const ref = this.dialogService.open(RequerimientoComponent, {
      header: titulo,
      width: '70%',
      contentStyle: {  overflow: 'auto' },
      data: params
    });

    ref.onClose.subscribe((reject: any) => {
      //console.log(reject);
      if (reject == 'ok') {
        let strMensaje="";

        if(padre=="11.1"){
          strMensaje='Se registró Función y Responsabilidad del Personal correctamente.';
        }
        if(padre=="11.2"){
          strMensaje='Se registró el requerimiento de personal correctamente.';
        }
        if(padre=="11.3"){
          strMensaje='Se registró el requerimiento de maquinaria correctamente.';
        }
        this.toast('success', strMensaje);
        this.obtenerOrganizacionManejo();
      }
      // else{
      //   this.toast('error','Ocurrió un problema');
      // }
    });
  }

  editar(padre: string, codigoSubTipo: String, data: any, titulo: string) {

    let params = { idPGMF: this.idPGMF, padre: padre, codigoSubTipo: codigoSubTipo, data:data };

    const ref = this.dialogService.open(RequerimientoComponent, {
      header: titulo,
      width: '70%',
      data: params
    });

    ref.onClose.subscribe((reject: any) => {
      //console.log(reject);
      if (reject == 'ok') {
        if(padre=="11.1"){
          this.toast('success', ' Se actualizó la Función y Responsabilidad del Personal correctamente.');
        }else{
        this.toast('success', 'Se editó correctamente');
        }
        this.obtenerOrganizacionManejo();
      }
      // else{
      //   this.toast('error','Ocurrió un problema');
      // }
    });
  }




  eliminarDetalle(padre: string,id: Number) {
    this.serv.organizacionManejoDetalleEliminar({ idOrganizacionManejo: id, idUsuarioElimina: this.objUser.idusuario }).subscribe(
      (result: any) => {
        //console.log(result);
        if (result.success == true) {
          let strMensaje="";

          if(padre=="11.2"){
            strMensaje='Se eliminó el requerimiento de personal correctamente.';
          }
          if(padre=="11.3"){
            strMensaje='Se eliminó el requerimiento de maquinaria correctamente.';
          }

          this.obtenerOrganizacionManejo();
          this.toast('success', strMensaje);
        } else {
          this.toast('error', 'No se pudo eliminar el registro seleccionado.');
        }
      }
    );
  }

  eliminarPlantilla(id: Number, data: any, funcion: string='') {

    let params_insetar_detalle = {
      codigoSeccion : "SECORFUN",
      idOrganizacionManejo:  id,
      codigoSubTipoDetalle: data.subTipo,
      codigoTipoDetalle: data.tipo,
      codigoTipoActividad: '',
      cabecera: false, 
      tipoActividad : "",
      maquinaEquipo: "", 
      numero : 0,
      funcion: "", descripcion: "",
      medidaControl:"",
      medidaMonitoreo:"",
      frecuencia:"",
      responsable:"",
      contingencia:"",
      accionRealizada:"",
      nombreTipoDetalle:"",
  
      fecha: new Date(), idPGMF: this.anexoAdjunto.idPlanManejo,
      idUsuarioRegistro: this.objUser.idusuario,
      lugar: "",mecanismoParticipacion: "",
      metodologia: "", modalidadCapacitar: "",
      personalCapacitar: "", operacion :""
    }
 
    // if(params_insetar_detalle.codigoSubTipoDetalle != "OTROPER"){

    //   params_insetar_detalle.funcion= funcion;
    //   this.serv.PGMFAbreviadoDetalleRegistrar(params_insetar_detalle).subscribe(
    //     (result : any)=>{
    //       if(result.success == true){

    //           this.Toast.ok('Se eliminó la responsabilidad del personal correctamente');
            
    //         this.obtenerOrganizacionManejo();
    //       }else{
    //         this.toast('error','Ocurrió un problema');
    //       }
    //     }
    //   );
    //  if (params_insetar_detalle.codigoSubTipoDetalle == "OTROPER"){
      
      this.serv.organizacionManejoDetalleEliminar({ idOrganizacionManejo: id, idUsuarioElimina: this.objUser.idusuario }).subscribe(
        (result: any) => {
          //console.log(result);
          if (result.success == true) {
            this.obtenerOrganizacionManejo();
            this.toast('success', 'Se eliminó la función del personal correctamente.');
          } else {
            this.toast('error', 'No se pudo eliminar el requerimiento de personal seleccionado.');
          }
        }
      );
    // }

  }



  

  toast(saverty: String, datail: String) {

    this.messageService.add({
      key: 'tr',
      severity: saverty.toString(),
      detail: datail.toString(),
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
    this.selectAnexo = "S";
  }

  anexoS(event: any) {
    this.adjuntarAnexo();
  }
  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
      
    }

    this.adjuntarAnexo();
  }

  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.objUser.idusuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {
        if(response.success) this.limpiarFile();
      });
  }


  btnGuardar() {
    if(!this.validarGuardar()) return;

    const request = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      descripcion: this.fileAnexo4?.justificacion || "",
      idUsuarioModificacion: this.objUser.idusuario
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoEvaluacionService.actualizarPlanManejoArchivo(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any )=> {
      if (result.success) {
        this.Toast.ok('Se registró la organización del manejo correctamente.');
      } else {
        this.errorMensaje(result.message)
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true, true));
   

  }

  limpiarFile() {
    this.idPlanManejoArchivo = 0;
    this.fileAnexo4.nombreFile = "";
    this.fileAnexo4.justificacion = "";
  }

  validarGuardar(): boolean{
    let valido = true;
    if (this.selectAnexo === "S") {
      if (!this.fileAnexo4.justificacion) {
        this.Toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo4.nombreFile) {
        this.Toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false, isError: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = '';
    if(isError) {
      mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
      this.Toast.error(mensajeError);
    }
    else {
      mensajeError = error;
      this.Toast.warn(mensajeError);
    }
  }
}
