import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, ToastService } from "@shared";
import { ConfirmationService, SelectItemGroup } from "primeng/api";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { ListRentabilidadManejoForestalDetalle, RentabilidadPGMFA } from "src/app/model/rentabilidadManejo";
import { RentabilidadManejoForestalModel } from "src/app/model/RentabilidadManejoForestal";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { Mensajes } from "src/app/model/util/Mensajes";
import { RentabilidadManejoForestalService } from "src/app/service/rentabilidadManejoForestal.service";
import { finalize } from "rxjs/operators";
import { PlantillaConst } from "src/app/shared/plantilla.const";
@Component({
  selector: "app-tab-programa-inversion",
  templateUrl: "./tab-programa-inversion.component.html",
})
export class TabProgramaInversionComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  nuevoIngresoEgresoNecesidades: boolean = false;

  listaNecesidades: any[] = [];
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();

  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;
  archivoCargado: any;

  params = {
    descripcion: [""],
    detalle: [{}],

    fechaRegistro: new Date(),
    idInversion: 0,
    idPgmf: 0,
    idUsuarioRegistro: 1,
  };

  ingresos: any = [];
  egresos: any = [];

  /**/
  usuario!: UsuarioModel;
  listRentabilidadManejo: RentabilidadManejoForestalModel[] = [];
  listRentabilidad: RentabilidadPGMFA[] = [];
  rentabilidadObj: RentabilidadPGMFA = new RentabilidadPGMFA();
  listDetalle: ListRentabilidadManejoForestalDetalle[] = [];
  rentabilidadManejo = {} as RentabilidadManejoForestalModel;
  displayBasic: boolean = false;
  edit: boolean = false;
  years: any[] = [];

  rubrosList: SelectItemGroup[] = [];
  listIngresos: any[] = [];
  listEgresos: any[] = [];

  colsIngreso: any[] = [{ field: "rubro", header: "Rubro" }];
  colsEgresos: any[] = [{ field: "rubro", header: "Rubro" }];
  codigoPGMF = "PGMF";
  verIngDescripcion: boolean = false;
  verEgreDescripcion: boolean = false;

  necesidad = "";

  indexList: any;

  file: any = {};

  constructor(
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private mitoast: ToastService,
    private confirmationService: ConfirmationService,
    private rentabilidadManejoForestalService: RentabilidadManejoForestalService,
    private archivoService: ArchivoService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit() {
    this.obtenerListadoRubros();
    this.obtenerListadoCheck();
    this.obtenerListadoRentabilidad();
  }

  obtenerListadoRubros() {
    this.rubrosList = [];
    this.listEgresos = [];
    this.listIngresos = [];

    var params = {
      codigoParametro: "PGMFA",
      idPlanManejo: null,
    };

    this.rentabilidadManejoForestalService.listarRentabilidadManejoForestal(params).subscribe((response: any) => {
      response.data.forEach((element: any, index: number) => {
        if (element.descripcion == "INGRE") {
          this.listIngresos.push({ label: element.rubro, value: element.rubro, id: index + 1, });
        }
        if (element.descripcion == "EGRES") {
          this.listEgresos.push({ label: element.rubro, value: element.rubro, id: index + 1, });
        }
      });

      this.rubrosList.push({ label: "I.Ingreso", value: "Maderable", items: this.listIngresos, });
      this.rubrosList.push({ label: "II.Egresos", value: "Maderable", items: this.listEgresos, });
    });
  }

  obtenerListadoRentabilidad() {
    this.listRentabilidadManejo = [];
    this.listaNecesidades = [];
    var params = {
      codigoParametro: this.codigoPGMF,
      idPlanManejo: this.idPGMF,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.rentabilidadManejoForestalService.listarRentabilidadManejoForestal(params).subscribe((result: any) => {
      this.dialog.closeAll();
      result.data.forEach((element: any) => {
        if (element.idTipoRubro == 3) {
          let item = {
            codigoRentabilidad: element.codigoRentabilidad,
            observacion: element.observacion,
            descripcion: element.descripcion,
            estado: "A",
            idPlanManejo: this.idPGMF,
            idRentManejoForestal: element.idRentManejoForestal,
            idTipoRubro: element.idTipoRubro,
            idUsuarioRegistro: this.usuario.idusuario,
            listRentabilidadManejoForestalDetalle:
              element.listRentabilidadManejoForestalDetalle,
            rubro: element.rubro,
          };
          this.listaNecesidades.push(item);
        } else {
          this.listRentabilidadManejo.push(element);
        }
      });
      this.crearFilas();
      this.crearColumnas();
    }, (error) => this.errorMensaje(error, true));
  }

  registrarManejoRentabilidad() {
    if (!this.validarIngresosEgresos()) return;
    this.listRentabilidad = [];

    this.listRentabilidadManejo.forEach((response) => {
      if (response.id > 5 || response.idTipoRubro == 2) {
        this.rentabilidadObj = new RentabilidadPGMFA(response);
        this.rentabilidadObj.codigoRentabilidad = this.codigoPGMF;
        this.rentabilidadObj.idPlanManejo = this.idPGMF;
        this.rentabilidadObj.idUsuarioRegistro = this.usuario.idusuario;
        this.rentabilidadObj.idTipoRubro = 2;
        this.listDetalle = [];
        response.listRentabilidadManejoForestalDetalle.forEach((item: any) => {
          let rentabilidadDetalle = new ListRentabilidadManejoForestalDetalle(
            item
          );
          rentabilidadDetalle.idUsuarioRegistro = this.usuario.idusuario;
          this.listDetalle.push(rentabilidadDetalle);
        });
        this.rentabilidadObj.listRentabilidadManejoForestalDetalle = this.listDetalle;
        this.listRentabilidad.push(this.rentabilidadObj);
      } else if (response.id <= 5 || response.idTipoRubro == 1) {
        this.rentabilidadObj = new RentabilidadPGMFA(response);
        this.rentabilidadObj.codigoRentabilidad = this.codigoPGMF;
        this.rentabilidadObj.idPlanManejo = this.idPGMF;
        this.rentabilidadObj.idUsuarioRegistro = this.usuario.idusuario;
        this.rentabilidadObj.idTipoRubro = 1;
        this.listDetalle = [];
        response.listRentabilidadManejoForestalDetalle.forEach((item: any) => {
          let rentabilidadDetalle = new ListRentabilidadManejoForestalDetalle(
            item
          );
          rentabilidadDetalle.idUsuarioRegistro = this.usuario.idusuario;
          this.listDetalle.push(rentabilidadDetalle);
        });
        this.rentabilidadObj.listRentabilidadManejoForestalDetalle = this.listDetalle;
        this.listRentabilidad.push(this.rentabilidadObj);
      }
    });

    this.listaNecesidades.forEach((item) => {
      this.listRentabilidad.push(item);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.rentabilidadManejoForestalService.registrarRentabilidadManejoForestal(this.listRentabilidad).subscribe((res: any) => {
      if (res.success == true) {
        this.mitoast.ok(res?.message);
        this.obtenerListadoRentabilidad();
        this.dialog.closeAll();
      } else {
        this.mitoast.error(res?.message);
        this.dialog.closeAll();
      }
    }, (error) => this.errorMensaje(error, true));
  }

  validarIngresosEgresos(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.egresos.length) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Egresos\n";
    }

    if (!this.ingresos.length) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Ingresos\n";
    }

    if (!validar) this.mitoast.warn(mensaje);

    return validar;
  }

  openEditarRentabilidadManejoIngreso(rentabilidad: RentabilidadManejoForestalModel) {
    this.rentabilidadManejo = {} as RentabilidadManejoForestalModel;
    this.rentabilidadManejo = rentabilidad;
    this.displayBasic = true;
    this.edit = true;
    this.clearYears();

    this.years.forEach((year: any) => {
      rentabilidad.listRentabilidadManejoForestalDetalle.forEach(
        (anio: any) => {
          if (year.valorSecundario == anio.anio) {
            year.checked = true;
            year.monto = anio.monto;
            year.idRentManejoForestalDet = anio.idRentManejoForestalDet;
          }
        }
      );
    });
  }

  eliminarRentabilidadIngreso(event: Event, index: number, rentabilidad: RentabilidadManejoForestalModel) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (rentabilidad.idRentManejoForestal > 0) {
          var params = {
            idUsuarioElimina: this.usuario.idusuario,
            idRentManejoForestal: rentabilidad.idRentManejoForestal,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService.eliminarRentabilidadManejoForestal(params).subscribe((data: any) => {
            this.mitoast.ok(data.message);
            this.ingresos.splice(index, 1);
            this.crearColumnas();
            this.dialog.closeAll();
          }, (error) => this.errorMensaje(error, true));
        } else {
          this.ingresos.splice(index, 1);
          this.crearColumnas();
        }
      },
      reject: () => { },
    });
  }

  eliminarRentabilidadEgreso(event: Event, index: number, rentabilidad: RentabilidadManejoForestalModel) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (rentabilidad.idRentManejoForestal > 0) {
          var params = {
            idUsuarioElimina: this.usuario.idusuario,
            idRentManejoForestal: rentabilidad.idRentManejoForestal,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService.eliminarRentabilidadManejoForestal(params).subscribe((data: any) => {
            this.mitoast.ok(data.message);
            this.egresos.splice(index, 1);
            this.crearColumnas();
            this.dialog.closeAll();
          },
            (error) => this.errorMensaje(error, true));
        } else {
          this.egresos.splice(index, 1);
          this.crearColumnas();
        }
      },
      reject: () => { },
    });
  }

  guardarNecesidad() {
    var params = {
      codigoRentabilidad: this.codigoPGMF,
      observacion: "",
      descripcion: this.necesidad,
      estado: "A",
      idPlanManejo: this.idPGMF,
      idRentManejoForestal: 0,
      idTipoRubro: 3,
      idUsuarioRegistro: this.usuario.idusuario,
      listRentabilidadManejoForestalDetalle: [],
      rubro: "",
    };
    this.listaNecesidades.push(params);
    this.necesidad = "";
  }

  eliminarNecesidad(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idRentManejoForestal > 0) {
          var params = {
            idUsuarioElimina: this.usuario.idusuario,
            idRentManejoForestal: data.idRentManejoForestal,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService.eliminarRentabilidadManejoForestal(params).subscribe((data: any) => {
            this.mitoast.ok(data.message);
            this.listaNecesidades.splice(index, 1);
            this.dialog.closeAll();
          }, (error) => this.errorMensaje(error, true));
        } else {
          this.listaNecesidades.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  //MODAL
  agregarRentabilidad() {
    if (!this.validarRentabilidad()) return;

    if (this.edit) {
      this.editarRentabilidad();
    } else {
      this.registrarRentabilidad();
    }
  }

  editarRentabilidad() {
    if (this.rentabilidadManejo.idRentManejoForestal != 0) {
      this.indexList = this.listRentabilidadManejo.findIndex(
        (rentabilidad) =>
          rentabilidad.idRentManejoForestal ==
          this.rentabilidadManejo.idRentManejoForestal
      );
    } else if (this.rentabilidadManejo.idRentManejoForestal == 0) {
      this.indexList = this.listRentabilidadManejo.findIndex(
        (rentabilidad) => rentabilidad.id == this.rentabilidadManejo.id
      );
    }

    if (
      this.rentabilidadManejo.id > 5 ||
      this.rentabilidadManejo.idTipoRubro == 2
    ) {
      const index = this.egresos.indexOf(this.rentabilidadManejo, 0);

      this.rentabilidadManejo.listRentabilidadManejoForestalDetalle = [];
      this.years.forEach((year: any) => {
        if (year.checked)
          this.rentabilidadManejo.listRentabilidadManejoForestalDetalle.push({
            ...year,
            anio: Number(year.valorSecundario),
            idRentManejoForestalDet: year.idRentManejoForestalDet,
          });
      });

      this.egresos[index] = this.convertirObjetoTableModel(
        this.rentabilidadManejo
      );

      this.listRentabilidadManejo[this.indexList] = this.rentabilidadManejo;

      this.crearColumnas();
      this.displayBasic = false;
      this.indexList = "";
    } else if (
      this.rentabilidadManejo.id < 5 ||
      this.rentabilidadManejo.idTipoRubro == 1
    ) {
      const index = this.ingresos.indexOf(this.rentabilidadManejo, 0);
      this.rentabilidadManejo.listRentabilidadManejoForestalDetalle = [];
      this.years.forEach((year: any) => {
        if (year.checked)
          this.rentabilidadManejo.listRentabilidadManejoForestalDetalle.push({
            ...year,
            anio: Number(year.valorSecundario),
            idRentManejoForestalDet: year.idRentManejoForestalDet,
          });
      });
      this.ingresos[index] = this.convertirObjetoTableModel(
        this.rentabilidadManejo
      );
      this.listRentabilidadManejo[this.indexList] = this.rentabilidadManejo;

      this.crearColumnas();
      this.displayBasic = false;
      this.indexList = "";
    }
  }

  registrarRentabilidad() {
    this.rentabilidadManejo.idRentManejoForestal = 0;
    this.rentabilidadManejo.listRentabilidadManejoForestalDetalle = [];
    this.years.forEach((year: any) => {
      if (year.checked)
        this.rentabilidadManejo.listRentabilidadManejoForestalDetalle.push({
          ...year,
          idRentManejoForestalDet: 0,
          anio: Number(year.valorSecundario),
        });
    });
    this.listRentabilidadManejo.push(this.rentabilidadManejo);

    this.agregarObjetoTableModel(this.rentabilidadManejo);
    this.crearColumnas();

    this.displayBasic = false;
    this.rentabilidadManejo = {} as RentabilidadManejoForestalModel;
    this.edit = false;
  }

  validarRentabilidad(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (
      this.rentabilidadManejo.rubro == null ||
      this.rentabilidadManejo.rubro == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Rubro\n";
    }
    if (
      this.rentabilidadManejo.id == 5 &&
      (this.rentabilidadManejo.descripcion == null ||
        this.rentabilidadManejo.descripcion == "")
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Descripción Otros.\n";
    }
    if (this.years.every((year: any) => year.checked === false)) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Año\n";
    }

    if (
      this.years.some((year: any) => year.checked == true && year.monto == "")
    ) {
      validar = false;
      mensaje = mensaje += "(*) Indicar el monto del año seleccionado.\n";
    }

    if (
      !this.edit &&
      this.listRentabilidadManejo.some(
        (rentabilidad) => rentabilidad.id == this.rentabilidadManejo.id
      )
    ) {
      validar = false;
      mensaje = mensaje += "(*) El Rubro seleccionado ya existe en la tabla \n";
    }

    if (!validar) this.mitoast.warn(mensaje);

    return validar;
  }

  convertirObjetoTableModel(rentabilidad: RentabilidadManejoForestalModel) {
    let customObject: any = {};
    customObject = {
      descripcion: rentabilidad.descripcion,
      idPlanManejo: rentabilidad.idPlanManejo,
      idRentManejoForestal: rentabilidad.idRentManejoForestal,
      id: rentabilidad.id,
      rubro: rentabilidad.rubro,
      listRentabilidadManejoForestalDetalle:
        rentabilidad.listRentabilidadManejoForestalDetalle,
    };

    rentabilidad.listRentabilidadManejoForestalDetalle.forEach((anio) => {
      customObject[`anio${anio.anio}`] = anio.anio;
      customObject[`monto${anio.anio}`] = anio.monto.toFixed(2);
    });

    return customObject;
  }

  agregarObjetoTableModel(rentabilidad: RentabilidadManejoForestalModel) {
    let customObject: any = {};
    customObject = {
      ...rentabilidad,
    };
    rentabilidad.listRentabilidadManejoForestalDetalle.forEach((anio) => {
      customObject[`anio${anio.anio}`] = anio.anio;
      customObject[`monto${anio.anio}`] = anio.monto.toFixed(2);
    });
    if (rentabilidad.id > 5) {
      this.egresos.push(customObject);
    } else {
      this.ingresos.push(customObject);
    }
  }

  rubrosChange(event: any) {
    let rubroMap: any[] = [];

    this.rubrosList.forEach((rubros) => {
      rubros.items.forEach((item: any) => { rubroMap.push(item); });
    });

    rubroMap.forEach((rubro) => {
      if (rubro.value == event.value) {
        this.rentabilidadManejo.id = rubro.id;
        this.rentabilidadManejo.rubro = rubro.value;
      }
    });
  }

  openModalAgregar() {
    this.displayBasic = true;
    this.rentabilidadManejo = {} as RentabilidadManejoForestalModel;
    this.edit = false;
    this.clearYears();
  }

  cerrarModal() {
    this.displayBasic = false;
    this.rentabilidadManejo = {} as RentabilidadManejoForestalModel;
    this.edit = false;
  }

  clearYears() {
    this.years.forEach((year) => {
      year.checked = false;
      year.monto = "";
      year.idRentManejoForestalDet = 0;
    });
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.mitoast.error(mensajeError);
  }

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN17', this.lineamiento);
  }

  dataLineamientos(data: any) {
    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN17' }
    );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }

  obtenerListadoCheck() {
    this.years = [
      {
        valorPrimario: "Año 1",
        valorSecundario: "1",
      },
      {
        valorPrimario: "Año 2",
        valorSecundario: "2",
      },
      {
        valorPrimario: "Año 3",
        valorSecundario: "3",
      },
      {
        valorPrimario: "Año 4",
        valorSecundario: "4",
      },
      {
        valorPrimario: "Año 5",
        valorSecundario: "5",
      },
      {
        valorPrimario: "Año 6",
        valorSecundario: "6",
      },
      {
        valorPrimario: "Año 7",
        valorSecundario: "7",
      },
      {
        valorPrimario: "Año 8",
        valorSecundario: "8",
      },
      {
        valorPrimario: "Año 9",
        valorSecundario: "9",
      },
      {
        valorPrimario: "Año 10",
        valorSecundario: "10",
      },
      {
        valorPrimario: "Año 11",
        valorSecundario: "11",
      },
      {
        valorPrimario: "Año 12",
        valorSecundario: "12",
      },
      {
        valorPrimario: "Año 13",
        valorSecundario: "13",
      },
      {
        valorPrimario: "Año 14",
        valorSecundario: "14",
      },
      {
        valorPrimario: "Año 15",
        valorSecundario: "15",
      },
      {
        valorPrimario: "Año 16",
        valorSecundario: "16",
      },
      {
        valorPrimario: "Año 17",
        valorSecundario: "17",
      },
      {
        valorPrimario: "Año 18",
        valorSecundario: "18",
      },
      {
        valorPrimario: "Año 19",
        valorSecundario: "19",
      },
      {
        valorPrimario: "Año 20",
        valorSecundario: "20",
      },
    ];
  }

  monto1: number = 0;
  monto2: number = 0;
  monto3: number = 0;
  monto4: number = 0;
  monto5: number = 0;
  monto6: number = 0;
  monto7: number = 0;
  monto8: number = 0;
  monto9: number = 0;
  monto10: number = 0;
  monto11: number = 0;
  monto12: number = 0;
  monto13: number = 0;
  monto14: number = 0;
  monto15: number = 0;
  monto16: number = 0;
  monto17: number = 0;
  monto18: number = 0;
  monto19: number = 0;
  monto20: number = 0;

  verAnio1: boolean = false;
  verAnio2: boolean = false;
  verAnio3: boolean = false;
  verAnio4: boolean = false;
  verAnio5: boolean = false;
  verAnio6: boolean = false;
  verAnio7: boolean = false;
  verAnio8: boolean = false;
  verAnio9: boolean = false;
  verAnio10: boolean = false;
  verAnio11: boolean = false;
  verAnio12: boolean = false;
  verAnio13: boolean = false;
  verAnio14: boolean = false;
  verAnio15: boolean = false;
  verAnio16: boolean = false;
  verAnio17: boolean = false;
  verAnio18: boolean = false;
  verAnio19: boolean = false;
  verAnio20: boolean = false;

  montoE1: number = 0;
  montoE2: number = 0;
  montoE3: number = 0;
  montoE4: number = 0;
  montoE5: number = 0;
  montoE6: number = 0;
  montoE7: number = 0;
  montoE8: number = 0;
  montoE9: number = 0;
  montoE10: number = 0;
  montoE11: number = 0;
  montoE12: number = 0;
  montoE13: number = 0;
  montoE14: number = 0;
  montoE15: number = 0;
  montoE16: number = 0;
  montoE17: number = 0;
  montoE18: number = 0;
  montoE19: number = 0;
  montoE20: number = 0;

  verAnioE1: boolean = false;
  verAnioE2: boolean = false;
  verAnioE3: boolean = false;
  verAnioE4: boolean = false;
  verAnioE5: boolean = false;
  verAnioE6: boolean = false;
  verAnioE7: boolean = false;
  verAnioE8: boolean = false;
  verAnioE9: boolean = false;
  verAnioE10: boolean = false;
  verAnioE11: boolean = false;
  verAnioE12: boolean = false;
  verAnioE13: boolean = false;
  verAnioE14: boolean = false;
  verAnioE15: boolean = false;
  verAnioE16: boolean = false;
  verAnioE17: boolean = false;
  verAnioE18: boolean = false;
  verAnioE19: boolean = false;
  verAnioE20: boolean = false;

  crearColumnas() {
    this.colsIngreso = [];
    this.colsEgresos = [];

    this.ingresos.forEach((ingreso: any) => {
      if (ingreso.rubro == "Otros ingresos") {
        this.verIngDescripcion = true;
      }
      ingreso.listRentabilidadManejoForestalDetalle.forEach((anio: any) => {
        if (
          this.colsIngreso.find((data) => data.header == `Año ${anio.anio}`)
        ) {
        } else {
          this.colsIngreso.push({
            field: `monto${anio.anio}`,
            header: `Año ${anio.anio}`,
            value: anio.anio,
          });
        }
      });
    });

    this.colsIngreso.sort(function (a, b) {
      if (a.value > b.value) {
        return 1;
      }
      if (a.value < b.value) {
        return -1;
      }
      return 0;
    });

    this.verAnio1 = false;
    this.verAnio2 = false;
    this.verAnio3 = false;
    this.verAnio4 = false;
    this.verAnio5 = false;
    this.verAnio6 = false;
    this.verAnio7 = false;
    this.verAnio8 = false;
    this.verAnio9 = false;
    this.verAnio10 = false;
    this.verAnio11 = false;
    this.verAnio12 = false;
    this.verAnio13 = false;
    this.verAnio14 = false;
    this.verAnio15 = false;
    this.verAnio16 = false;
    this.verAnio17 = false;
    this.verAnio18 = false;
    this.verAnio19 = false;
    this.verAnio20 = false;

    this.colsIngreso.forEach((item) => {
      if (item.header == "Año 1") {
        this.verAnio1 = true;
      } else if (item.header == "Año 2") {
        this.verAnio2 = true;
      } else if (item.header == "Año 3") {
        this.verAnio3 = true;
      } else if (item.header == "Año 4") {
        this.verAnio4 = true;
      } else if (item.header == "Año 5") {
        this.verAnio5 = true;
      } else if (item.header == "Año 6") {
        this.verAnio6 = true;
      } else if (item.header == "Año 7") {
        this.verAnio7 = true;
      } else if (item.header == "Año 8") {
        this.verAnio8 = true;
      } else if (item.header == "Año 9") {
        this.verAnio9 = true;
      } else if (item.header == "Año 10") {
        this.verAnio10 = true;
      } else if (item.header == "Año 11") {
        this.verAnio11 = true;
      } else if (item.header == "Año 12") {
        this.verAnio12 = true;
      } else if (item.header == "Año 13") {
        this.verAnio13 = true;
      } else if (item.header == "Año 14") {
        this.verAnio14 = true;
      } else if (item.header == "Año 15") {
        this.verAnio15 = true;
      } else if (item.header == "Año 16") {
        this.verAnio16 = true;
      } else if (item.header == "Año 17") {
        this.verAnio17 = true;
      } else if (item.header == "Año 18") {
        this.verAnio18 = true;
      } else if (item.header == "Año 19") {
        this.verAnio19 = true;
      } else if (item.header == "Año 20") {
        this.verAnio20 = true;
      }
    });

    this.monto1 = 0;
    this.monto2 = 0;
    this.monto3 = 0;
    this.monto4 = 0;
    this.monto5 = 0;
    this.monto6 = 0;
    this.monto7 = 0;
    this.monto8 = 0;
    this.monto9 = 0;
    this.monto10 = 0;
    this.monto11 = 0;
    this.monto12 = 0;
    this.monto13 = 0;
    this.monto14 = 0;
    this.monto15 = 0;
    this.monto16 = 0;
    this.monto17 = 0;
    this.monto18 = 0;
    this.monto19 = 0;
    this.monto20 = 0;
    this.ingresos.forEach((item: any) => {
      item.listRentabilidadManejoForestalDetalle.forEach((element: any) => {
        if (element.anio == 1) {
          this.monto1 += element.monto;
        } else if (element.anio == 2) {
          this.monto2 += element.monto;
        } else if (element.anio == 3) {
          this.monto3 += element.monto;
        } else if (element.anio == 4) {
          this.monto4 += element.monto;
        } else if (element.anio == 5) {
          this.monto5 += element.monto;
        } else if (element.anio == 6) {
          this.monto6 += element.monto;
        } else if (element.anio == 7) {
          this.monto7 += element.monto;
        } else if (element.anio == 8) {
          this.monto8 += element.monto;
        } else if (element.anio == 9) {
          this.monto9 += element.monto;
        } else if (element.anio == 10) {
          this.monto10 += element.monto;
        } else if (element.anio == 11) {
          this.monto11 += element.monto;
        } else if (element.anio == 12) {
          this.monto12 += element.monto;
        } else if (element.anio == 13) {
          this.monto13 += element.monto;
        } else if (element.anio == 14) {
          this.monto14 += element.monto;
        } else if (element.anio == 15) {
          this.monto15 += element.monto;
        } else if (element.anio == 16) {
          this.monto16 += element.monto;
        } else if (element.anio == 17) {
          this.monto17 += element.monto;
        } else if (element.anio == 18) {
          this.monto18 += element.monto;
        } else if (element.anio == 19) {
          this.monto19 += element.monto;
        } else if (element.anio == 20) {
          this.monto20 += element.monto;
        }
      });
    });

    this.egresos.forEach((ingreso: any) => {
      if (ingreso.rubro == "Otros gastos") {
        this.verEgreDescripcion = true;
      }
      ingreso.listRentabilidadManejoForestalDetalle.forEach((anio: any) => {
        if (
          this.colsEgresos.find((data) => data.header == `Año ${anio.anio}`)
        ) {
        } else {
          this.colsEgresos.push({
            field: `monto${anio.anio}`,
            header: `Año ${anio.anio}`,
            value: anio.anio,
          });
        }
      });
    });
    this.colsEgresos.sort(function (a, b) {
      if (a.value > b.value) {
        return 1;
      }
      if (a.value < b.value) {
        return -1;
      }
      return 0;
    });

    this.verAnioE1 = false;
    this.verAnioE2 = false;
    this.verAnioE3 = false;
    this.verAnioE4 = false;
    this.verAnioE5 = false;
    this.verAnioE6 = false;
    this.verAnioE7 = false;
    this.verAnioE8 = false;
    this.verAnioE9 = false;
    this.verAnioE10 = false;
    this.verAnioE11 = false;
    this.verAnioE12 = false;
    this.verAnioE13 = false;
    this.verAnioE14 = false;
    this.verAnioE15 = false;
    this.verAnioE16 = false;
    this.verAnioE17 = false;
    this.verAnioE18 = false;
    this.verAnioE19 = false;
    this.verAnioE20 = false;

    this.colsEgresos.forEach((item) => {
      if (item.header == "Año 1") {
        this.verAnioE1 = true;
      } else if (item.header == "Año 2") {
        this.verAnioE2 = true;
      } else if (item.header == "Año 3") {
        this.verAnioE3 = true;
      } else if (item.header == "Año 4") {
        this.verAnioE4 = true;
      } else if (item.header == "Año 5") {
        this.verAnioE5 = true;
      } else if (item.header == "Año 6") {
        this.verAnioE6 = true;
      } else if (item.header == "Año 7") {
        this.verAnioE7 = true;
      } else if (item.header == "Año 8") {
        this.verAnioE8 = true;
      } else if (item.header == "Año 9") {
        this.verAnioE9 = true;
      } else if (item.header == "Año 10") {
        this.verAnioE10 = true;
      } else if (item.header == "Año 11") {
        this.verAnioE11 = true;
      } else if (item.header == "Año 12") {
        this.verAnioE12 = true;
      } else if (item.header == "Año 13") {
        this.verAnioE13 = true;
      } else if (item.header == "Año 14") {
        this.verAnioE14 = true;
      } else if (item.header == "Año 15") {
        this.verAnioE15 = true;
      } else if (item.header == "Año 16") {
        this.verAnioE16 = true;
      } else if (item.header == "Año 17") {
        this.verAnioE17 = true;
      } else if (item.header == "Año 18") {
        this.verAnioE18 = true;
      } else if (item.header == "Año 19") {
        this.verAnioE19 = true;
      } else if (item.header == "Año 20") {
        this.verAnioE20 = true;
      }
    });

    this.montoE1 = 0;
    this.montoE2 = 0;
    this.montoE3 = 0;
    this.montoE4 = 0;
    this.montoE5 = 0;
    this.montoE6 = 0;
    this.montoE7 = 0;
    this.montoE8 = 0;
    this.montoE9 = 0;
    this.montoE10 = 0;
    this.montoE11 = 0;
    this.montoE12 = 0;
    this.montoE13 = 0;
    this.montoE14 = 0;
    this.montoE15 = 0;
    this.montoE16 = 0;
    this.montoE17 = 0;
    this.montoE18 = 0;
    this.montoE19 = 0;
    this.montoE20 = 0;
    this.egresos.forEach((item: any) => {
      item.listRentabilidadManejoForestalDetalle.forEach((element: any) => {
        if (element.anio == 1) {
          this.montoE1 += element.monto;
        } else if (element.anio == 2) {
          this.montoE2 += element.monto;
        } else if (element.anio == 3) {
          this.montoE3 += element.monto;
        } else if (element.anio == 4) {
          this.montoE4 += element.monto;
        } else if (element.anio == 5) {
          this.montoE5 += element.monto;
        } else if (element.anio == 6) {
          this.montoE6 += element.monto;
        } else if (element.anio == 7) {
          this.montoE7 += element.monto;
        } else if (element.anio == 8) {
          this.montoE8 += element.monto;
        } else if (element.anio == 9) {
          this.montoE9 += element.monto;
        } else if (element.anio == 10) {
          this.montoE10 += element.monto;
        } else if (element.anio == 11) {
          this.montoE11 += element.monto;
        } else if (element.anio == 12) {
          this.montoE12 += element.monto;
        } else if (element.anio == 13) {
          this.montoE13 += element.monto;
        } else if (element.anio == 14) {
          this.montoE14 += element.monto;
        } else if (element.anio == 15) {
          this.montoE15 += element.monto;
        } else if (element.anio == 16) {
          this.montoE16 += element.monto;
        } else if (element.anio == 17) {
          this.montoE17 += element.monto;
        } else if (element.anio == 18) {
          this.montoE18 += element.monto;
        } else if (element.anio == 19) {
          this.montoE19 += element.monto;
        } else if (element.anio == 20) {
          this.montoE20 += element.monto;
        }
      });
    });
  }

  crearFilas() {
    this.ingresos = [];
    this.egresos = [];

    this.listRentabilidadManejo.forEach((rentabilidad) => {
      let customObject: any = {};
      customObject = {
        ...rentabilidad,
      };
      rentabilidad.listRentabilidadManejoForestalDetalle.forEach((anio) => {
        customObject[`anio${anio.anio}`] = anio.anio;
        customObject[`monto${anio.anio}`] = anio.monto.toFixed(2);
        customObject.idRentManejoForestalDet = anio.idRentManejoForestalDet;
      });
      if (rentabilidad.idTipoRubro == 1) {
        this.ingresos.push(customObject);
      } else {
        this.egresos.push(customObject);
      }
    });
  }

  btnDescargarFormato() {
    const NombreGenerado: string = PlantillaConst.PGMF_TAB_12_1;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.errorMensaje('Ocurrió un problema, intente nuevamente', true);
      }
    }, () => {
      this.errorMensaje('Ocurrió un problema, intente nuevamente', true);
    });
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  btncargarFormato(){

    if (!(this.archivoCargado instanceof File)) {

      this.mitoast.error('Seleccione el Formato con los datos ingresados.');
      return;
    }
    let params = {
      codigoRentabilidad: 'PGMF',
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.rentabilidadManejoForestalService.registrarProgramaInversionesExcel(this.archivoCargado, params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.mitoast.ok('Se registró el plan de inversión.\n');
          this.obtenerListadoRentabilidad();
          this.archivoCargado = "";
        }else {
          this.mitoast.error('Ocurrió un error.');
        }
      });

  }

}
