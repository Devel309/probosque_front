import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, Input } from "@angular/core";
import MapView from "@arcgis/core/views/MapView";
import { isNullOrEmpty, MapApi } from "@shared";

@Component({
  selector: "mapa",
  templateUrl: "./mapa-component.html",
})
export class MapaComponent implements OnInit {

  @Input() name!: string;
  private _files!: File[];
  @Input()
  set files(files: File[]) {
    this._files = files;
    let i = 0;
    let config = { inServer: false, name: this.name }
    if (!isNullOrEmpty(files)) {
      files.forEach(file => this.mapApi.processFile(file, config, this._id, this.view));
    }

  }
  get files() {
    return this._files;
  }

  @Output() data = new EventEmitter();

  view!: MapView;
  _id = this.mapApi.Guid2.newGuid;

  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;

  constructor(private mapApi: MapApi) { }

  ngOnInit(): void {
    this.initializeMap();
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "340px";
    container.style.width = "100%";
    let view = this.mapApi.initializeMap(container);
    this.view = view;
  }

}
