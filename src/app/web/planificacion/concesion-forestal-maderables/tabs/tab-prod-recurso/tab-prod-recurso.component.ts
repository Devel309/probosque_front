import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  Input,
} from '@angular/core';

import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CheckboxModule } from 'primeng/checkbox';
import { Table } from 'primeng/table';

import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { DownloadFile, ExcelService, ToastService } from '@shared';
import { CensoForestalDetalle } from '@models';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { PotencialProduccionService } from 'src/app/service/planificacion/plan-general-manejo-pgmfa/potencialProduccion.service';
import { PotencialProduccionCabeceraModel } from 'src/app/model/potencialProduccionModel';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { CodigosPGMF } from 'src/app/model/util/PGMF/CodigosPGMF';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { ConfirmationService } from 'primeng/api';
import { CodigoPGMF } from 'src/app/model/CodigoPGMF';
import { PlantillaConst } from 'src/app/shared/plantilla.const';

@Component({
  selector: 'app-tab-prod-recurso',
  templateUrl: './tab-prod-recurso.component.html',
  styleUrls: ['tab-prod-recurso.component.scss'],
})
export class TabProdRecursoComponent implements OnInit {
  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  @ViewChild('tblPotencialMaderable', { static: false })
  private tblPotencialMaderable!: Table;

  @ViewChild('tblRegeneracion', { static: false })
  private tblRegeneracion!: Table;

  public filFile: any = null;
  eventName = '';

  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();

  detalle: CensoForestalDetalle[] = [];
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();
  file!: File;
  fileSHP!: File;

  potencialMaderableInvForestal: any = {}; //potencialMaderableInvForestal = {} as ProduccionRecursoForestalModel;
  regeneracionFustalesInvForestal: any = {}; // as ProduccionRecursoForestalModel;

  potencialProduccion: any[] = [];

  CodigosPGMF = CodigosPGMF;

  objIFPM!: PotencialProduccionCabeceraModel;
  objIFRF!: PotencialProduccionCabeceraModel;
  listIFPM: any[] = [];
  listIFRF: any[] = [];

  objPMB!: PotencialProduccionCabeceraModel;
  objPMD!: PotencialProduccionCabeceraModel;
  listPM: any[] = [];
  listPMD: any[] = [];
  objPME: PotencialProduccionCabeceraModel =
    new PotencialProduccionCabeceraModel();

  listF: any[] = [];
  listFC: any[] = [];
  totalListFC = {
    nDap1015: 0,
    nDap1520: 0,
    nTotalBosque: 0,
    nTotalHa: 0,
    abDap1015: 0,
    abDap1520: 0,
    abTotalBosque: 0,
    abTotalHa: 0,
  };

  sector: number = 0;
  area: number = 0;
  nTotalIndividuos: number = 0;
  individuosHa: number = 0;
  volumen: number = 0;
  sectorTotal: number = 0;

  totalesBosqueN: number = 0;
  totalesBosqueAB: number = 0;
  totalesBosqueVP: number = 0;
  totalesAreaN: number = 0;
  totalesAreaAB: number = 0;
  totalesAreaVP: number = 0;

  listEspeciesE: any[] = [];
  listEspeciesA: any[] = [];

  totalN: number = 0;
  totalPorcentaje: number = 0;
  totalNF: number = 0;
  totalPorcentajeF: number = 0;

  objFructales = new PotencialProduccionCabeceraModel();
  objPotencialProduccion = new PotencialProduccionCabeceraModel();
  listPotencialProduccion: PotencialProduccionCabeceraModel[] = [];

  variables: any[] = [{ var: 'N' }, { var: 'AB m2' }, { var: 'Vp m3' }];
  listPMDPrueba: any[] = [];
  anexoTab5_3: string = '';
  displayModal: boolean = false;
  ref!: DynamicDialogRef;
  showWarn: boolean = true;

  idFile_PGMF_5_1_2: number = 0;
  idFile_PGMF_5_3: number = 0;
  idFile_PGMF_5_4: number = 0;
  selectAnexo4_5_1_2: string = 'N';
  selectAnexo4_5_3: string = 'N';
  selectAnexo4_5_4: string = 'N';
  codFileAnexo = {
    PGMF_TAB5_1_2: CodigoPGMF.ACORDEON_5_1_TAB_2,
    PGMF_TAB5_3: CodigoPGMF.ACORDEON_5_3,
    PGMF_TAB5_4: CodigoPGMF.ACORDEON_5_4,
  };

  archivo: any;

  fileAnexo5_1_2: any = {};
  fileAnexo5_3: any = {};
  fileAnexo5_4: any = {};

  constructor(
    private api: PlanificacionService,
    private excel: ExcelService,
    private user: UsuarioService,
    private dialog: MatDialog,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private potencialProduccionService: PotencialProduccionService,
    private toast: ToastService,
    private censoForestalService: CensoForestalService,
    private postulacionPFDMService: PostulacionPFDMService,
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private archivoService: ArchivoService,
  ) {
    this.archivo = {
      file: null,
    };
  }

  CheckboxModule = new CheckboxModule();
  selectedValues: string[] = ['1', '0'];

  ngOnInit(): void {
    this.listar();
    this.listPotencialMaderable();
    this.listarFustales();
    this.listarPotencialProduccion();
  }

  listar() {
    this.objIFPM = new PotencialProduccionCabeceraModel();
    this.objIFRF = new PotencialProduccionCabeceraModel();
    this.objPMB = new PotencialProduccionCabeceraModel();
    this.listPMD = [];
    var params = {
      idPlanManejo: this.idPGMF,
      codigo: 'PGMF',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .listarPotencialProduccion(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.codigoSubTipoPotencialProdForestal == CodigosPGMF.PGMF_TAB_5_1) {
            this.objIFPM = element;
          } else if (element.codigoSubTipoPotencialProdForestal == CodigosPGMF.PGMF_TAB_5_2) {
            this.objIFRF = element;
            this.selectAnexo4_5_1_2 = this.objIFRF.anexo || "N";
          } else if (element.codigoSubTipoPotencialProdForestal == CodigosPGMF.PGMF_TAB_5_2_B) {
            this.objPMB = element;
          } else if (element.codigoSubTipoPotencialProdForestal == CodigosPGMF.PGMF_TAB_5_2_D) {
            let objPMD = new PotencialProduccionCabeceraModel(element);
            this.listPMD.push(objPMD);
          } else if (
            element.codigoSubTipoPotencialProdForestal ==
            CodigosPGMF.PGMF_TAB_5_2_E
          ) {
            this.objPME = new PotencialProduccionCabeceraModel(element);
            this.listEspeciesE = this.objPME.listPotencialProduccion;
          } else if (element.codigoSubTipoPotencialProdForestal == CodigosPGMF.PGMF_TAB_5_3_D) {
            this.objFructales = new PotencialProduccionCabeceraModel(element);
            this.selectAnexo4_5_3 = this.objFructales.anexo || "N";
            this.listEspeciesA = this.objFructales.listPotencialProduccion;
          } else if (element.codigoSubTipoPotencialProdForestal == CodigosPGMF.PGMF_TAB_5_4) {
            this.objPotencialProduccion = new PotencialProduccionCabeceraModel(element);
            this.selectAnexo4_5_4 = this.objPotencialProduccion.anexo || "N";
          }
        });
      });
  }

  listPotencialMaderable() {
    this.listPMDPrueba = [];
    var params = {
      idPlanDeManejo: this.idPGMF,
      tipoPlan: 'PGMF',
      /* idPlanDeManejo: 1146,
      tipoPlan: "PGMF", */
    };
    this.censoForestalService
      .ResultadosEspecieMuestreo(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            if (element.listTablaEspeciesMuestreo.length > 0) {
              var objBosque = new Bosques();
              objBosque.idTipoBosque = element.idTipoBosque;
              objBosque.nombreBosque = element.nombreBosque;
              element.listTablaEspeciesMuestreo.forEach((item: any) => {
                var obEspecies = new Especies();
                obEspecies.especies = item.especies;
                this.variables.forEach((i) => {
                  var objN = new PotencialMaderable();
                  var objAB = new PotencialMaderable();
                  var objVP = new PotencialMaderable();
                  if (i.var == 'N') {
                    objN.var = i.var;
                    objN.totalBosque = item.numeroArbolesTotalBosque;
                    this.totalesBosqueN += Number(
                      item.numeroArbolesTotalBosque
                    );
                    objN.totalHa = item.numeroArbolesTotalHa;
                    this.totalesAreaN += Number(item.numeroArbolesTotalHa);
                    obEspecies.array.push(objN);
                  } else if (i.var == 'AB m2') {
                    objAB.var = i.var;
                    objAB.totalBosque = item.areaBasalTotalBosque;
                    this.totalesBosqueAB += Number(item.areaBasalTotalBosque);
                    objAB.totalHa = item.areaBasalTotalHa;
                    this.totalesAreaAB += Number(item.numeroArbolesTotalHa);
                    obEspecies.array.push(objAB);
                  } else if (i.var == 'Vp m3') {
                    objVP.var = i.var;
                    objVP.totalBosque = item.volumenTotalBosque;
                    this.totalesBosqueVP += Number(item.volumenTotalBosque);
                    objVP.totalHa = item.volumenTotalHa;
                    this.totalesAreaVP += Number(item.volumenTotalHa);
                    obEspecies.array.push(objVP);
                  }
                });
                objBosque.arrayEspecies.push(obEspecies);
              });
              this.listPMDPrueba.push(objBosque);
            }
          });
          if (this.listEspeciesE.length != 0) {
            this.listEspeciesAbundantesResponse();
          } else {
            this.listEspeciesAbundantes();
          }
        } else {
          // this.toast.warn("No se encontraron registros.");
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(
              'Se necesita sincronización móvil para Plan Manejo ' + this.idPGMF
            );
          }
        }
      });
  }
  listEspeciesAbundantes() {
    this.listEspeciesE = [];
    this.totalPorcentaje = 0;
    this.totalN = 0;
    this.listPMDPrueba.forEach((item) => {
      item.arrayEspecies.forEach((element: any) => {
        let obj = new EspeciesAbundantes();
        obj.especie = element.especies;
        element.array.forEach((i: any) => {
          if (i.var == 'N') {
            obj.totalHa = i.totalHa;
            this.totalN += Number(obj.totalHa);
            obj.porcentaje = (Number(i.totalHa) * 100) / this.totalesAreaN;
            this.totalPorcentaje += obj.porcentaje;
          }
        });
        this.listEspeciesE.push(obj);
      });
    });
  }

  listEspeciesAbundantesResponse() {
    this.totalPorcentaje = 0;
    this.totalN = 0;
    this.listEspeciesE.forEach((item: any) => {
      this.totalPorcentaje += item.porcentaje;
      this.totalN += item.totalHa;
    });
  }

  listarFustales() {
    // if (this.listFC.length > 0) {
    //   this.calcularTotalFustales();
    // } else {
    this.listFC = [];
    var params = {
      idPlanManejo: this.idPGMF,
      tipoPlan: 'PGMF',
      /* idPlanManejo: 1146,
      tipoPlan: "PGMF", */
    };
    this.censoForestalService
      .listarResultadosFustales(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {

            if (element.listTablaEspeciesMuestreo.length > 0) {
              var objBosque = new Bosques();
              objBosque.idTipoBosque = element.idTipoBosque;
              objBosque.nombreBosque = element.nombreBosque;
              element.listTablaEspeciesMuestreo.forEach((item: any) => {
                var obEspecies = new Especies();
                obEspecies.especies = item.especies;
                this.variables.forEach((i) => {
                  var objN = new PotencialMaderable();
                  var objAB = new PotencialMaderable();
                  if (i.var == 'N') {
                    objN.var = i.var;
                    objN.dap1015 = item.numeroArbolesDap10a14;
                    objN.dap1520 = item.numeroArbolesDap15a19;
                    objN.totalHa = item.numeroArbolesTotalHa;
                    objN.totalBosque = item.numeroArbolesTotalBosque;
                    obEspecies.array.push(objN);
                  } else if (i.var == 'AB m2') {
                    objAB.var = i.var;
                    objAB.dap1015 = item.areaBasalDap10a14;
                    objAB.dap1520 = item.areaBasalDap15a19;
                    objAB.totalHa = item.areaBasalTotalHa;
                    objAB.totalBosque = item.areaBasalTotalBosque;
                    obEspecies.array.push(objAB);
                  }
                });
                objBosque.arrayEspecies.push(obEspecies);
              });
              this.listFC.push(objBosque);
            }

          });
          this.calcularTotalFustales();
          if (this.listEspeciesA.length != 0) {
            this.listFustalesEspeciesResponse();
          } else {
            this.listFustalesEspecies();
          }
        } else {
          // this.toast.warn("No se encontraron resultados para fustales.");
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(
              'Se necesita sincronización móvil para Plan Manejo ' + this.idPGMF
            );
          }
        }
      });
    // }
  }

  calcularTotalFustales() {
    this.listFC.map((data: any) => {
      if (data.arrayEspecies.length > 0) {
        data.arrayEspecies.forEach((item: any) => {
          item.array.forEach((element: any) => {
            if (element.var == 'N') {
              this.totalListFC.nDap1015 += parseFloat(
                !isNaN(element.dap1015) ? element.dap1015 : 0
              );
              this.totalListFC.nDap1520 += parseFloat(element.dap1520);
              this.totalListFC.nTotalHa += !isNaN(element.totalHa)
                ? parseFloat(element.totalHa)
                : 0;
              this.totalListFC.nTotalBosque += parseFloat(element.totalBosque);
            } else if (element.var == 'AB m2') {
              this.totalListFC.abDap1015 += parseFloat(element.dap1015);
              this.totalListFC.abDap1520 += parseFloat(element.dap1520);
              this.totalListFC.abTotalHa += parseFloat(element.totalHa);
              this.totalListFC.abTotalBosque += parseFloat(element.totalBosque);
            }
          });
        });
      }
    });
  }

  listFustalesEspecies() {
    this.listEspeciesA = [];
    this.totalPorcentajeF = 0;
    this.totalNF = 0;
    this.listFC.forEach((item) => {
      item.arrayEspecies.forEach((element: any) => {
        let obj = new EspeciesAbundantes();
        obj.especie = element.especies;
        element.array.forEach((i: any) => {
          if (i.var == 'N') {
            obj.totalHa = i.totalHa;
            this.totalNF += Number(obj.totalHa);
            obj.porcentaje =
              (Number(i.totalHa) * 100) / this.totalListFC.nTotalHa;
            this.totalPorcentajeF += obj.porcentaje;
          }
        });
        this.listEspeciesA.push(obj);
      });
    });
  }

  listFustalesEspeciesResponse() {
    this.totalPorcentajeF = 0;
    this.totalNF = 0;
    this.listEspeciesA.forEach((item: any) => {
      this.totalPorcentajeF += item.porcentaje;
      this.totalNF += item.totalHa;
    });
  }

  listarPotencialProduccion() {
    var params = {
      idPlanDeManejo: this.idPGMF,
      tipoPlan: 'PGMF',
      /* idPlanDeManejo: 1146,
      tipoPlan: "PGMF", */
    };
    this.censoForestalService
      .AprovechamientoRFNM(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          // this.potencialProduccion = [...response.data];
          response.data.forEach((element: any) => {
            var obj = new PotencialProduccion(element);
            obj.resultadosAprovechamientoRFNMDtos = [];
            element.resultadosAprovechamientoRFNMDtos.forEach((item: any) => {
              var objDetalle = new PotencialProduccionDetalle(item);
              obj.resultadosAprovechamientoRFNMDtos.push(objDetalle);
            });
            this.potencialProduccion.push(obj);
          });
          this.calcularTotalPotencialProduccion();
        } else {
          // this.toast.warn("No se encontraron segistros");
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(
              'Se necesita sincronización móvil para Plan Manejo ' + this.idPGMF
            );
          }
        }
      });
  }

  calcularTotalPotencialProduccion() {
    this.potencialProduccion.forEach((item) => {
      item.resultadosAprovechamientoRFNMDtos.forEach((element: any) => {
        this.area += Number(element.area);
        this.individuosHa += Number(element.individuosHa);
        this.nTotalIndividuos += Number(element.nTotalIndividuos);
        this.volumen += Number(element.volumen);
        this.sectorTotal += Number(element.sector);
      });
    });
  }

  //5.1.1
  GuardarIFPM() {
    this.objIFPM.idPlanManejo = this.idPGMF;
    this.objIFPM.codigoTipoPotProdForestal = 'PGMF';
    this.objIFPM.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_1;
    this.objIFPM.idUsuarioRegistro = this.user.idUsuario;
    this.listIFPM = [];
    this.listIFPM.push(this.objIFPM);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .registrarPotencialProduccion(this.listIFPM)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró potencial maderable.');
          this.listar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }
  //5.1.2
  GuardarIFRF() {
    if (!this.validarGuardarIFRF()) return;

    this.objIFRF.idPlanManejo = this.idPGMF;
    this.objIFRF.codigoTipoPotProdForestal = 'PGMF';
    this.objIFRF.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_2;
    this.objIFRF.anexo = this.selectAnexo4_5_1_2;
    this.objIFRF.idUsuarioRegistro = this.user.idUsuario;
    this.listIFRF = [];
    this.listIFRF.push(this.objIFRF);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .registrarPotencialProduccion(this.listIFRF)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró regeneración de fustales.');
          this.listar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  validarGuardarIFRF(): boolean{
    let valido = true;
    if (this.selectAnexo4_5_1_2 === "S") {
      if (!this.fileAnexo5_1_2.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo5_1_2.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  calcularAreaParcela(area: any, tipo: string) {
    if (tipo === CodigosPGMF.PGMF_TAB_5_1) {
      this.objIFPM.totalAreaInventariada = area;
    } else if (tipo === CodigosPGMF.PGMF_TAB_5_2) {
      this.objIFRF.areaMuestreada = area;
    }
  }

  //5.2
  GuardarPM() {
    this.listPM = [];
    //B
    this.objPMB.idPlanManejo = this.idPGMF;
    this.objPMB.codigoTipoPotProdForestal = 'PGMF';
    this.objPMB.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_2_B;
    this.objPMB.idUsuarioRegistro = this.user.idUsuario;
    this.listPM.push(this.objPMB);
    //D

    /*  if (this.listPMD.length > 0) {
      this.listPMD.forEach((item: any) => {
        item.idPlanManejo = this.idPGMF;
        item.codigoTipoPotProdForestal = "PGMF";
        item.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_2_D;
        item.idUsuarioRegistro = this.user.idUsuario;
        item.listPotencialProduccion.forEach((item2: any) => {
          item2.codigoTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_2_D;
          item2.codigoSubTipoPotencialProdForestalDet =
            CodigosPGMF.PGMF_TAB_5_2_D + "DET";
        });
        this.listPM.push(item);
      });
    } */

    //E
    this.objPME.idPlanManejo = this.idPGMF;
    this.objPME.codigoTipoPotProdForestal = 'PGMF';
    this.objPME.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_2_E;
    this.objPME.idUsuarioRegistro = this.user.idUsuario;
    this.objPME.listPotencialProduccion = [];
    this.listEspeciesE.forEach((item: any) => {
      var obj = new ListPotencialProduccion(item);
      obj.totalHa = Number(item.totalHa);
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.codigoTipoPotencialProdForestalDet = 'ESPABUN';
      this.objPME.listPotencialProduccion.push(obj);
    });
    this.listPM.push(this.objPME);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .registrarPotencialProduccion(this.listPM)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(
            'Se registró resultados para el potencial maderable correctamente.'
          );
          this.listar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  //5.3
  GuardarFustales() {
    if (!this.validarGuardarFustales()) return;
    this.listF = [];
    this.objFructales.idPlanManejo = this.idPGMF;
    this.objFructales.codigoTipoPotProdForestal = 'PGMF';
    this.objFructales.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_3_D;
    this.objFructales.anexo = this.selectAnexo4_5_3;
    this.objFructales.idUsuarioRegistro = this.user.idUsuario;
    this.objFructales.listPotencialProduccion = [];
    this.listEspeciesA.forEach((item: any) => {
      var obj = new ListPotencialProduccion(item);
      obj.totalHa = Number(item.totalHa);
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.codigoTipoPotencialProdForestalDet = 'ESPABUN';
      this.objFructales.listPotencialProduccion.push(obj);
    });
    this.listF.push(this.objFructales);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .registrarPotencialProduccion(this.listF)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(
            'Se registró resultados para los fustales correctamente.'
          );
          this.listar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  validarGuardarFustales(): boolean{
    let valido = true;
    if (this.selectAnexo4_5_3 === "S") {
      if (!this.fileAnexo5_3.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo5_3.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  //5.4
  GuardarPP() {
    if (!this.validarGuardarPP()) return;
    this.listPotencialProduccion = [];
    this.objPotencialProduccion.idPlanManejo = this.idPGMF;
    this.objPotencialProduccion.codigoTipoPotProdForestal = 'PGMF';
    this.objPotencialProduccion.codigoSubTipoPotencialProdForestal = CodigosPGMF.PGMF_TAB_5_4;
    this.objPotencialProduccion.anexo = this.selectAnexo4_5_4;
    this.objPotencialProduccion.idUsuarioRegistro = this.user.idUsuario;
    this.listPotencialProduccion.push(this.objPotencialProduccion);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .registrarPotencialProduccion(this.listPotencialProduccion)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(
            'Se registró Potencial de producción de recursos forestales correctamente.'
          );
          this.listar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  validarGuardarPP(): boolean{
    let valido = true;
    if (this.selectAnexo4_5_4 === "S") {
      if (!this.fileAnexo5_4.justificacion) {
        this.toast.warn("Debe ingresar la justificación.");
        valido = false;
      }
      if (!this.fileAnexo5_4.nombreFile) {
        this.toast.warn("Debe adjuntar documento de evidencia.");
        valido = false;
      }
    }
    return valido;
  }

  anexo2() {
    this.displayModal = true;
  }

  anexo3() {
    // this.toast.ok(
    //   "Esta información sera completada en la sincronización movil."
    // );
    this.displayModal = true;
  }

  cargarIdArchivo(data: any, codigoArchivo: string) {
    switch (codigoArchivo) {
      case this.codFileAnexo.PGMF_TAB5_1_2:
        this.idFile_PGMF_5_1_2 = data.idPGMFArchivo;
        this.selectAnexo4_5_1_2 = 'S';
        break;
      case this.codFileAnexo.PGMF_TAB5_3:
        this.idFile_PGMF_5_3 = data.idPGMFArchivo;
        this.selectAnexo4_5_3 = 'S';
        break;
      case this.codFileAnexo.PGMF_TAB5_4:
        this.idFile_PGMF_5_4 = data.idPGMFArchivo;
        this.selectAnexo4_5_4 = 'S';
        break;
      default:
        break;
    }
  }

  anexoN(idArchivo: any, fileAnexo?:any) {
    if (idArchivo != 0) {
      this.eliminarPlanManejo(idArchivo, fileAnexo);
    }
  }
  eliminarPlanManejo(idArchivo: number, fileAnexo?:any) {
    var params = {
      idPlanManejoArchivo: idArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => {
        fileAnexo.nombreFile = "";
        fileAnexo.justificacion = "";
      });
  }

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN9.1', this.lineamiento);
    this.ordenarLista(data, 'PGMFEVALLIN9.2', this.lineamiento2);
    this.ordenarLista(data, 'PGMFEVALLIN9.3', this.lineamiento3);
  }

  dataLineamientos(data: any) {
    this.lineamiento = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN9.1',
    });
    this.lineamiento2 = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN9.2',
    });
    this.lineamiento3 = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN9.3',
    });
  }

  onFileChangeSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = [...e.target.files];
    this.eventName = e.target.name;
  }
  cargarSHP() {
    this.filFile = [...[this.fileSHP]];
    this.eventName = '';
  }
  cargarSHP2(e: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = [...e.target.files];
    this.eventName = e.target.name;
  }

  cargarCenso(e: Event) {
    const target = e.target as HTMLInputElement;
    const file = target.files?.item(0);
    this.excel
      .excelToJson(file!)
      .then((json) => console.log(json))
      .catch((err) => {
        console.log(err);
        alert(err);
      });
  }

  cargarCenso2() {
    this.excel
      .excelToJson(this.file)
      .then((json) => console.log(json))
      .catch((err) => {
        console.log(err);
        alert(err);
      });
  }

  //#region REQUEST BEGIN

  listarDetalle() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.listarCensoForestalDetalle(this.idPGMF)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  listarCensoForestalDetalle(IdPlanManejo: number) {
    return this.api
      .listarCensoForestalDetalle(IdPlanManejo)
      .pipe(tap((res) => (this.detalle = res?.data)));
  }

  //#endregion REQUEST END

  enviarArchivo() {
    

    //var params = new HttpParams()
    // .set('CodigoAnexo', 'anexo1')
    // .set(
    //   'IdProcesoPostulacion',
    //   this.params_adjuntar_anexo.idprocesopostulacion
    // )
    // .set('IdTipoDocumento', '1')
    // .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
    // .set('NombreArchivo', this.file.name);
    // ;
    // const formData = new FormData();
    // // var params = new HttpParams().set('IdUsuarioRegistro', this.user.idUsuario);
    // formData.append("file", this.filFile);
    // formData.append("IdUsuarioRegistro", String(this.user.idUsuario));
    // this.api.registrarMasivoCensoForestalDetalle(formData)
    //   .subscribe((result: any) => console.log(result));
  }
  guardar() {
    let item = [
      {
        idPlanManejo: 21,
        idTipoProdRecursoForestal: 'RegenFustal',
        disenio: this.regeneracionFustalesInvForestal.disenio || '',
        diametroMinimoInventariadaCM:
          this.regeneracionFustalesInvForestal.diametroMinimoInventariadaCM ||
          0,
        tamanioParcela:
          this.regeneracionFustalesInvForestal.tamanioParcela || 0,
        nroParcela: this.regeneracionFustalesInvForestal.nroParcela || 0,
        distanciaParcela:
          this.regeneracionFustalesInvForestal.distanciaParcela || 0,
        totalAreaInventariada:
          this.regeneracionFustalesInvForestal.totalAreaInventariada || 0,
        rangoDiametroCM:
          this.regeneracionFustalesInvForestal.rangoDiametroCM || 0,
        areaMuestreada:
          this.regeneracionFustalesInvForestal.areaMuestreada || 0,
        metodoMuestro: this.regeneracionFustalesInvForestal.metodoMuestro || '',
        intensidadMuestreoPorcentaje:
          this.regeneracionFustalesInvForestal.intensidadMuestreoPorcentaje ||
          0,
        errorMuestreoPorcentaje:
          this.regeneracionFustalesInvForestal.errorMuestreoPorcentaje || 0,
      },
    ];
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach((element) => {
      if (
        element.codigoTipo !== undefined &&
        element.codigoTipo !== null &&
        element.codigoTipo === codigo
      ) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = {
          idArchivo: element.idArchivo,
          nombre: element.descArchivo,
        };
      }
    });
  }

  private obtenerUltimaEvaluacion() {
    let params = { idPlanManejo: this.idPGMF };
    this.planManejoEvaluacionService
      .obtenerUltimaEvaluacion(params)
      .subscribe((result: any) => {
        this.dataLineamientos(result.codigo);
        this.listarEvaluacionDetalle(result.codigo);
      });
  }

  private listarEvaluacionDetalle(id: any) {
    let params = {
      idPlanManejoEval: id,
    };
    this.planManejoEvaluacionService
      .listarPlanManejoEvaluacionDetalle(params)
      .subscribe((result: any) => {
        if (result.isSuccess) {
          this.listarEvaluaciones(result.data);
        }
      });
  }

  btnDescargarFormato() {
    const NombreGenerado: string = PlantillaConst.PGMF_TAB_5_0

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
	    if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.warn('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.warn('Ocurrió un problema, intente nuevamente');
    });
  }

  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService
      .registrarCargaMasiva(this.archivo.file)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registró el archivo correctamente.\n');
          this.listar();
          this.listPotencialMaderable();
          this.listarFustales();
          this.listarPotencialProduccion();

          this.archivo.file = '';
        } else {
          this.toast.error('Ocurrió un error.');
        }
      });
  }
}

export class Bosques {
  constructor(data?: any) {
    if (data) {
      this.idTipoBosque = data.idTipoBosque;
      this.nombreBosque = data.nombreBosque;
      return;
    }
  }
  nombreBosque: string = '';
  idTipoBosque: string = '';
  arrayEspecies: Especies[] = [];
}

export class Especies {
  constructor(data?: any) {
    if (data) {
      this.especies = data.especies;
      return;
    }
  }
  especies: string = '';
  array: PotencialMaderable[] = [];
}
export class PotencialMaderable {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = '';
  totalBosque: string = '';
  totalHa: string = '';
  dap1015: string = '';
  dap1520: string = '';
}

export class EspeciesAbundantes {
  constructor(data?: any) {
    if (data) {
      this.especie = data.especie ? data.especie : '';
      this.totalHa = data.nTotalHa ? data.nTotalHa : 0;
      this.porcentaje = data.porcentaje ? data.porcentaje : 0;
      this.grupoComercial = data.grupoComercial ? data.grupoComercial : '';
      return;
    }
  }
  especie: string = '';
  totalHa: string = '';
  porcentaje: number = 0;
  grupoComercial: string = '';
}

export class ListPotencialProduccion {
  constructor(data?: any) {
    if (data) {
      this.idPotProdForestalDet = data.idPotProdForestalDet
        ? data.idPotProdForestalDet
        : 0;
      this.codigoTipoPotencialProdForestalDet =
        data.codigoTipoPotencialProdForestalDet
          ? data.codigoTipoPotencialProdForestalDet
          : '';
      this.idEspecie = data.idEspecie ? data.idEspecie : 0;
      this.especie = data.especie ? data.especie : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.codigoSubTipoPotencialProdForestalDet =
        data.codigoSubTipoPotencialProdForestalDet
          ? data.codigoSubTipoPotencialProdForestalDet
          : '';
      this.grupoComercial = data.grupoComercial ? data.grupoComercial : ''; //GC
      this.nroArboles = data.nroArboles ? data.nroArboles : 0;
      this.nroArbolesPorcentaje = data.nroArbolesPorcentaje
        ? data.nroArbolesPorcentaje
        : 0;
      this.listPotencialProduccionVariable =
        data.listPotencialProduccionVariable
          ? data.listPotencialProduccionVariable
          : [];
      this.totalHa = data.totalHa ? data.totalHa : 0;
      this.porcentaje = data.porcentaje ? data.porcentaje : 0;
      return;
    }
  }
  idPotProdForestalDet: number = 0;
  codigoTipoPotencialProdForestalDet: string = '';
  idEspecie: number = 0;
  especie: string = '';
  idUsuarioRegistro: number = 0;
  codigoSubTipoPotencialProdForestalDet: string = '';
  grupoComercial: string = ''; //GC
  nroArboles: number = 0; //N
  nroArbolesPorcentaje: number = 0; //%
  totalHa: number = 0;
  porcentaje: number = 0;
  listPotencialProduccionVariable: any[] = [];
}

export class PotencialProduccion {
  constructor(data?: any) {
    if (data) {
      this.especie = data.especie ? data.especie : '';
      this.productoAprovechar = data.productoAprovechar
        ? data.productoAprovechar
        : '';
      this.resultadosAprovechamientoRFNMDtos =
        data.resultadosAprovechamientoRFNMDtos
          ? data.resultadosAprovechamientoRFNMDtos
          : [];
      return;
    }
  }
  especie: string = '';
  productoAprovechar: string = '';
  resultadosAprovechamientoRFNMDtos: PotencialProduccionDetalle[] = [];
}

export class PotencialProduccionDetalle {
  constructor(data?: any) {
    this.sector = data.sector != 'null' ? data.sector : '0';
    this.area = data.area != 'null' ? data.area : '0';
    this.nTotalIndividuos =
      data.nTotalIndividuos != 'null' ? data.nTotalIndividuos : '0';
    this.individuosHa = data.individuosHa != 'null' ? data.individuosHa : '0';
    this.volumen = data.volumen != 'null' ? data.volumen : '0';
  }
  sector: string = '';
  area: string = '';
  nTotalIndividuos: string = '';
  individuosHa: string = '';
  volumen: string = '';
}
