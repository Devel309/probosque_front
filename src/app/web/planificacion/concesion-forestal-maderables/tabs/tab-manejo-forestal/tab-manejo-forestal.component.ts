import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, OrdenamientoInternoPmfiService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoPGMF } from 'src/app/model/CodigoPGMF';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import {
  CodigosManejoBosquePGMF,
  ManejoForestalCabecera,
  ManejoForestalDetalleModel,
} from 'src/app/model/ManejoForestal';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { ManejoBosqueService } from 'src/app/service/manejoBosque.service';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { DialogTipoEspecieMultiselectComponent } from 'src/app/shared/components/dialog-tipo-especie-multiselect/dialog-tipo-especie-multiselect.component';
import { ModalFormularioEspeciesFloraComponent } from '../../modal/modal-formulario-especies-flora/modal-formulario-especies-flora.component';
import { ModalFormularioEspeciesImportantesComponent } from '../../modal/modal-formulario-especies-importantes/modal-formulario-especies-importantes.component';
import { ModalFormularioEspeciesComponent } from '../../modal/modal-formulario-especies/modal-formulario-especies.component';
import { ModalFormularioEspecificacionesComponent } from '../../modal/modal-formulario-especificaciones/modal-formulario-especificaciones.component';
import { ModalFormularioUsoPotencialComponent } from '../../modal/modal-formulario-uso-potencial/modal-formulario-uso-potencial.component';


import { CensoForestalService } from 'src/app/service/censoForestal';
import { DataModel } from '../../../elaboracion-declaracion-mpafpp/tabs/mpafpp-aprobechamiento-umf/mpafpp-aprobechamiento-umf.component';
import { ModalFormularioOrdenInternoComponent } from '../../../plan-general-manejo/tabs/tab-ordenamiento-area-manejo/modal-formulario-orden-interno/modal-formulario-orden-interno.component';
import { ConsoleLogger } from '@angular/compiler-cli/src/ngtsc/logging';
import { ModalFormularioEspeciesFloraPortegerComponent } from '../../modal/modal-formulario-especies-flora-proteger/modal-formulario-especies-flora-proteger.component';
import { ModalFormularioAreasDegradadasComponent } from '../../modal/modal-formulario-areasDegradas/modal-formulario-areasDegradas.component';
import { Console } from 'console';
@Component({
  selector: 'app-tab-manejo-forestal',
  templateUrl: './tab-manejo-forestal.component.html',
  styleUrls: ['./tab-manejo-forestal.component.scss'],
})
export class TabManejoForestalComponent implements OnInit {
  ref: DynamicDialogRef = new DynamicDialogRef();
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento4: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento5: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento6: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento7: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento8: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento9: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento10: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento11: LineamientoInnerModel = new LineamientoInnerModel();

  city: string = '';

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  codigoManejo = CodigosManejoBosquePGMF;

  //acordeones
  accordeonCategoriaOrdemaniento: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonSistemaManejo: ManejoForestalCabecera = new ManejoForestalCabecera();
  accordeonCicloCorta: ManejoForestalCabecera = new ManejoForestalCabecera();
  accordeonEspeciesManejarDiametrosMinCorta: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonEspeciesFloraProteger: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonCortaAnualPermisible6_6_1: ManejoForestalCabecera =
    new ManejoForestalCabecera();

  accordeonCortaAnualPermisible6_6_2: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonProyeccionCosecha: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonEspecificacionesSistemaAprovechamiento: ManejoForestalCabecera =
    new ManejoForestalCabecera();

  accordeonEspecificacionesPracticasSilviculturales6_9: ManejoForestalCabecera =
    new ManejoForestalCabecera();

  accordeonEspecificacionesPracticasSilviculturales6_9_1: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonEspecificacionesPracticasSilviculturales6_9_2: ManejoForestalCabecera =
    new ManejoForestalCabecera();
  accordeonEspecificacionesPracticasSilviculturales6_9_3: ManejoForestalCabecera =
    new ManejoForestalCabecera();

  sistemaPiliciclico: any = {
    codOpcion: '',
    descripcionOrd: '',
  };

  selectDuracionMinima6_3: any = {
    opcion: '',
    descripcion: '',
  };

  selectAnexo6_4: any = {
    opcion: '',
    descripcion: '',
  };

  agrupacion6_4 = '';

  selectAnexo6_9_3: any = {
    opcion: '',
    descripcion: '',
  };

  selectMarcacionProteccionSemillas: any = {
    opcion: '',
    descripcion: '',
  };

  select6_9_2: any = {
    opcion: '',
    ha: ''
  };

  selectAnexo: any = {
    opcion: '',
    descripcion: '',
  };

  tratamientos: any[] = [];
  listOrdenInterno: any[] = [];
  otrosTratamientos: any = false;
  indexOtrosTratamientos: any = false;

  otroTratamiento6_9_2 = new ManejoForestalDetalleModel();
  volumenCorta: ManejoForestalDetalleModel[] = [];

  volumenComercial: any = {
    periodoAprovechamiento: '',
    bosque_1: {
      tipo: '',
      areaHa: 0,
    },
    bosque_2: {
      tipo: '',
      areaHa: 0,
    },
    tabla: [],
  };

  objetivo: any []= [];
  listaPordefecto: any []= [];
  manejoForestal: any[] = [];
  especies: any[] = [];
  obtener:any[]=[];
  especiesFlora: ManejoForestalDetalleModel[] = [];
  llenarCombo: ManejoForestalDetalleModel[] = [];
  borrarDato: ManejoForestalDetalleModel[] = [];
  llenartabla: ManejoForestalDetalleModel[] = [];
  DatosPorDefecto: ManejoForestalDetalleModel[] = [];
  array:any =[];
  categoriaOrdenamiento: ManejoForestalDetalleModel[] = [];
  proyeccionCosecha: any[] = [];
  especificaciones: any[] = [];
  areasDegradas:any[]=[];
  especiesImportantes: any[] = [];
  cat6: any[] = [];
  items: any[] = [];
  listaElementos = [];
  cabecera: any;
  checked1: number = 0;
  checked2: number = 0;
  checked3: number = 0;
  btnElim: boolean=false;
  listDetalleOrdenamiento: any[] = [];

  usuario = {} as UsuarioModel;
  constructor(
    public dialogService: DialogService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private manejoBosqueService: ManejoBosqueService,
    private usuarioService: UsuarioService,
    private manejoService: ManejoBosqueService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private toast: ToastService,
    private ordenamientoInternoService: OrdenamientoInternoPmfiService,
    private messageService: MessageService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private archivoServ: ArchivoService,
    private censoForestalServ:CensoForestalService
  ) {
    this.usuario = this.usuarioService.usuario;
  }

  ngOnInit() {
    this.listActividadesOrd();
    this.listSistemaManejo();
    this.listCicloCorta();
    this.listEspeciesManejarDiametrosMinCorta();
    this.listEspeciesFloraProteger();
    this.listCortaAnualPermisibleVolumenComercial();
    this.listCortaAnualPermisibleVolumenCorta();
   // this.listProyeccionCosecha();
    this.listEspecificacionesSistemaProvechamiento();
    this.listEspecificacionesNecesidadDisenio();
    this.listEspecificacionesTratamientosSilviculturales();
    this.listarOrdenamiento();
    this.listEspecificacionesManejoEnriquesimientoAreasDegradadas();
    this.listapordef();
  }

  listActividadesOrdDefault() {
    this.manejoForestal = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: 'PGMF',
    };
    this.ordenamientoInternoService
      .listarOrdenamiento(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data[0].listOrdenamientoProteccionDet.map((element: any) => {
            const obj = new ManejoForestalDetalleModel();
            obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_1;
            obj.catOrdenamiento = element.categoria;
            obj.descripcionOrd = element.descripcion;
            obj.actividad = element.actividadesRealizar;
            this.categoriaOrdenamiento.push(obj);
          });
        }
      });
  }
  listarOrdenamiento() {
    this.listOrdenInterno = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: 'PGMF',
    };

    this.ordenamientoInternoService.listarOrdenamientoDetalle(params).subscribe(
      (response: any) => {
        this.cabecera = {
          idOrdenamientoProteccion: response.data[0].idOrdenamientoProteccion,
          codTipoOrdenamiento: response.data[0].codTipoOrdenamiento,
          idPlanManejo: response.data[0].idPlanManejo,
        };
        response.data[0].listOrdenamientoProteccionDet.forEach(
          (element: any) => {
            const item = element;
            // let coberBoscosa = ;
              let detalle = {
                idOrdenamientoProteccionDet: item.idOrdenamientoProteccionDet==null?0:item.idOrdenamientoProteccionDet,
                codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
                categoria: item.categoria,
                areaHA: item.areaHA,
                areaHAPorcentaje: item.areaHAPorcentaje,
                observacion: item.observacion, //campo para saber si tiene asociado un shapefile
                descripcion: item.descripcion, //campo para guardar nombres de las capas
                actividad:   item.actividad,      //campo para guardar layerId (capa mapa)
                actividadesRealizar: item.actividadesRealizar,
                observacionDetalle: item.observacionDetalle, //campo para guardar color capa
                zonaUTM:  item.zonaUTM,
                tipoBosque: item.tipoBosque,
                accion:item.accion,
                usoPotencial:item.usoPotencial,
                actRealizar:item.actRealizar
              };

              this.listOrdenInterno.push(detalle);
            }

        );
      }
    );
  }
  listActividadesOrd() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_1,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {

        if (
          response.data.length != 0 &&
          response.data[0].listManejoBosqueDetalle.length != 0
        ) {
          if (response.data[0].listManejoBosqueDetalle.length > 0) {
            response.data[0].listManejoBosqueDetalle.map((data: any) => {
              const obj = new ManejoForestalDetalleModel(data);
              this.categoriaOrdenamiento.push(obj);
            });
          }
        } else {
          this.listActividadesOrdDefault();
        }
      });
  }

  listSistemaManejo() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_2,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (
          response.data.length != 0 &&
          response.data[0].listManejoBosqueDetalle.length > 0
        ) {
          this.accordeonSistemaManejo = new ManejoForestalCabecera(
            response.data[0]
          );
          const element = response.data[0].listManejoBosqueDetalle[0];
          this.sistemaPiliciclico.codOpcion = element.codOpcion;
          this.sistemaPiliciclico.descripcionOrd = element.descripcionOrd;

          this.Anexo_6_2.selectModel = element.catOrdenamiento || "N";
        }
      });
  }

  listCicloCorta() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_3,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (
          response.data.length != 0 &&
          response.data[0].listManejoBosqueDetalle.length > 0
        ) {
          this.accordeonCicloCorta = new ManejoForestalCabecera(
            response.data[0]
          );
          this.selectDuracionMinima6_3.anio= response.data[0].anio;
          const element = response.data[0].listManejoBosqueDetalle[0];

          this.selectDuracionMinima6_3.opcion = element.codOpcion;
          this.selectDuracionMinima6_3.descripcion = element.descripcionOrd;

          this.Anexo_6_3.selectModel = element.catOrdenamiento || "N";
        }
      });
  }

  listEspeciesManejarDiametrosMinCorta() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_4,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let auxData = response.data[response.data.length -1];
          this.accordeonEspeciesManejarDiametrosMinCorta = new ManejoForestalCabecera(auxData);
          this.agrupacion6_4 = auxData.tipo;

          this.Anexo_6_4.selectModel = auxData.txDetalle || "N";

          if (auxData.listManejoBosqueDetalle.length > 0) {
            const array = auxData.listManejoBosqueDetalle;
            array?.map((data: any) => {
              const obj = new ManejoForestalDetalleModel(data);
              this.especies.push(obj);
            });
          }
        } else {
          this.listarResultadosMovil();
        }
      });
  }

  listEspeciesFloraProteger() {

    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_5,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let auxData = response.data[response.data.length -1];
          this.accordeonEspeciesFloraProteger = new ManejoForestalCabecera(auxData);

          this.Anexo_6_5.selectModel = auxData.txDetalle || "N";

          if (auxData.listManejoBosqueDetalle.length > 0) {
            const array = auxData.listManejoBosqueDetalle;
            array?.map((data: any) => {
              const obj = new ManejoForestalDetalleModel(data);
              this.especiesFlora.push(obj);

            });
            this.buscandoVacios();
            this.buscandoLlenos();

          }
        } else {
          this.listarResultadosMovil();
        }
      });
  }

  listCortaAnualPermisibleVolumenComercial() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_6_1,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data &&
          response.data.length != 0 &&
          response.data[0].listManejoBosqueDetalle.length > 0
        ) {
          this.accordeonCortaAnualPermisible6_6_1 = new ManejoForestalCabecera(
            response.data[0]
          );
          const array = response.data[0].listManejoBosqueDetalle;

          array?.map((data: any) => {
            const obj = new ManejoForestalDetalleModel(data);
            this.volumenComercial.push(obj);
          });
        }
      });
  }

  listCortaAnualPermisibleVolumenCorta() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_6_2,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (
          response.data.length != 0
        ) {
          this.Anexo_6_6_2.selectModel = response.data[0].txDetalle || "N";
          this.accordeonCortaAnualPermisible6_6_2 = new ManejoForestalCabecera(
            response.data[0]
          );
          const array = response.data[0].listManejoBosqueDetalle;

          array?.map((data: any) => {
            const obj = new ManejoForestalDetalleModel(data);
            this.volumenCorta.push(obj);
          });
        }
      });
  }

  // listProyeccionCosecha() {
  //   var params = {
  //     idPlanManejo: this.idPlanManejo,
  //     codigoGeneral: 'PGMF',
  //     subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_7,
  //   };
  //   this.manejoBosqueService
  //     .listarManejoBosque(params)
  //     .subscribe((response: any) => {
  //       if (
  //         response.data.length != 0 &&
  //         response.data[0].listManejoBosqueDetalle.length > 0
  //       ) {
  //         this.accordeonProyeccionCosecha = new ManejoForestalCabecera(
  //           response.data[0]
  //         );
  //         const array = response.data[0].listManejoBosqueDetalle;

  //         array?.map((data: any) => {
  //           const obj = new ManejoForestalDetalleModel(data);
  //           this.proyeccionCosecha.push(obj);
  //         });
  //       }
  //     });
  // }

  listEspecificacionesSistemaProvechamiento() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_8,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.accordeonEspecificacionesSistemaAprovechamiento =            new ManejoForestalCabecera(response.data[0]);
          this.Anexo_6_8.selectModel = response.data[0].txDetalle || "N";

          if (response.data[0].listManejoBosqueDetalle.length > 0) {
            const array = response.data[0].listManejoBosqueDetalle;

            array?.map((data: any) => {
              const obj = new ManejoForestalDetalleModel(data);
              obj.idManejoBosqueDet =
                this.accordeonEspecificacionesSistemaAprovechamiento
                  .idManejoBosque == 0
                  ? 0
                  : data.idManejoBosqueDet;
              this.especificaciones.push(obj);
            });
          }
        }
      });
  }

  listEspecificacionesNecesidadDisenio() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_9_1,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.accordeonEspecificacionesPracticasSilviculturales6_9_1 =
            new ManejoForestalCabecera(response.data[0]);
          const element = response.data[0];
          this.selectMarcacionProteccionSemillas.opcion =
            element.txDetalle == 'S' ? true : false;

          this.selectMarcacionProteccionSemillas.descripcion =
            element.txObservacion;
        }
      });
  }

  listEspecificacionesTratamientosSilviculturales() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_9_2,
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          if (response.data[0].listManejoBosqueDetalle.length > 0) {
            this.accordeonEspecificacionesPracticasSilviculturales6_9_2 =
              new ManejoForestalCabecera(response.data[0]);

            this.select6_9_2.anio=  response.data[0].anio;
            this.accordeonEspecificacionesPracticasSilviculturales6_9_2.codigoEscala=response.data[0].codigoEscala;

            if(response.data[0].codigoEscala!=null)
              this.select6_9_2.opcion =(response.data[0].codigoEscala=="ESCTRASILPIL"?"1":"2") ;

            const array = response.data[0].listManejoBosqueDetalle;

            array?.map((data: any) => {
              this.Anexo_6_9_2.selectModel = data.catOrdenamiento || "N";
              const obj = new ManejoForestalDetalleModel(data);
              obj.idManejoBosqueDet =
                this.accordeonEspecificacionesPracticasSilviculturales6_9_2
                  .idManejoBosque == 0
                  ? 0
                  : data.idManejoBosqueDet;

              if (
                this.accordeonEspecificacionesPracticasSilviculturales6_9_2
                  .idManejoBosque == 0 ||
                obj.subCodtipoManejoDet == 'B'
              ) {
                this.tratamientos.push(obj);
              } else {
                this.especiesImportantes.push(obj);
              }
            });

            const filter = this.tratamientos.filter(
              (data) => data.tratamientoSilvicultural == 'Otros (especificar)'
            )[0];

           // this.otrosTratamientos = filter.accion;

            this.otroTratamiento6_9_2 = filter;
          }
        }
      });
  }

  listEspecificacionesManejoEnriquesimientoAreasDegradadas = async () => {
    var params = {
      "idPlanManejo": +this.idPlanManejo,
      "codigoGeneral": 'PGMF',
      "subCodigoGeneral": CodigosManejoBosquePGMF.ACORDEON_6_9_3
    };

    await this.manejoBosqueService.listarManejoBosque(params).subscribe((response: any) => {

        if (response.data.length != 0) {

          this.accordeonEspecificacionesPracticasSilviculturales6_9_3 =
            new ManejoForestalCabecera(response.data[0]);

            this.Anexo_6_9_3.selectModel = response.data[0].txDetalle || "N";

          if (response.data[0].listManejoBosqueDetalle.length > 0) {
            const array = response.data[0].listManejoBosqueDetalle;
            array?.map((data: any) => {
              const obj = new ManejoForestalDetalleModel(data);

              obj.idManejoBosqueDet =
                this.accordeonEspecificacionesPracticasSilviculturales6_9_3
                  .idManejoBosque == 0
                  ? 0
                  : data.idManejoBosqueDet;
             /* if(response.data[0].listManejoBosqueDetalle.length>8){
                this.objetivo.push(obj);
                this.compararlistas();
              }else{
                this.llenartablavacia(),this.objetivo.push(obj);
                this.compararlistas();
              }*/
             this.objetivo.push(obj);
                this.compararlistas();

            });
          }
        }else{
          this.llenartablavacia();
        }
      });
  }

  updateOtros(index: number) {
    const accion = this.tratamientos.filter(
      (data) => data.tratamientoSilvicultural == 'Otros (especificar)'
    )[0].accion;

    this.indexOtrosTratamientos = index;
    if (accion) {
      this.otroTratamiento6_9_2 = this.tratamientos.filter(
        (data) => data.tratamientoSilvicultural == 'Otros (especificar)'
      )[0];
    } else {
      this.tratamientos[this.indexOtrosTratamientos].descripcionOtro = '';
    }

    this.otrosTratamientos = accion;
  }

  updateDescripcionOtro() {
    const value = this.otroTratamiento6_9_2.descripcionOtro;
    this.tratamientos[this.indexOtrosTratamientos].descripcionOtro = value;
  }

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN10.1', this.lineamiento);
    this.ordenarLista(data, 'PGMFEVALLIN10.2', this.lineamiento2);
    this.ordenarLista(data, 'PGMFEVALLIN10.3', this.lineamiento3);
    // this.ordenarLista(data, 'PGMFEVALLIN10.4', this.lineamiento4);
    // this.ordenarLista(data, 'PGMFEVALLIN10.5', this.lineamiento5);
    // this.ordenarLista(data, 'PGMFEVALLIN10.6.1', this.lineamiento6);
    // this.ordenarLista(data, 'PGMFEVALLIN10.7', this.lineamiento7);
    // this.ordenarLista(data, 'PGMFEVALLIN10.8', this.lineamiento8);
    // this.ordenarLista(data, 'PGMFEVALLIN10.9', this.lineamiento9);
    // this.ordenarLista(data, 'PGMFEVALLIN10', this.lineamiento10);
    this.ordenarLista(data, 'PGMFEVALLIN11', this.lineamiento11);
  }

  dataLineamientos(data: any) {
    this.lineamiento = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN10.1',
    });
    this.lineamiento2 = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN10.2',
    });
    this.lineamiento3 = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN10.3',
    });
    // this.lineamiento4 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10.4',
    // });
    // this.lineamiento5 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10.5',
    // });
    // this.lineamiento6 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10.6.1',
    // });
    // this.lineamiento7 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10.7',
    // });
    // this.lineamiento8 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10.8',
    // });
    // this.lineamiento9 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10.9',
    // });
    // this.lineamiento10 = new LineamientoInnerModel({
    //   idPlanManejoEvaluacion: data,
    //   codigoTipo: 'PGMFEVALLIN10',
    // });
    this.lineamiento11 = new LineamientoInnerModel({
      idPlanManejoEvaluacion: data,
      codigoTipo: 'PGMFEVALLIN11',
    });
  }

  openModal(mensaje: string, observacion: any, edit: any) {
    this.ref = this.dialogService.open(ModalFormularioUsoPotencialComponent, {
      header: mensaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: observacion,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Registro') {
        observacion.categoria = resp.categoria;
        observacion.usoPotencial = resp.usoPotencial;
        observacion.actRealizar = resp.actRealizar;
        observacion.editarForm = false;
      } else if (resp) {
        this.cat6.push(resp);

      }
    });
  }


  guardarOrdenamiento() {

    let params: any = [];

    let anexoBool:boolean|null=null;
    if(this.selectAnexo == 'S'){
      anexoBool = true;
    }else if(this.selectAnexo == 'N'){
      anexoBool = false;
    }

    this.listOrdenInterno.forEach((e:any)=>{
      e.accion =anexoBool;
    });

    let param = {
      idOrdenamientoProteccion: this.cabecera.idOrdenamientoProteccion,
      codTipoOrdenamiento: 'PGMF',
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
      anexo: null,
      idUsuarioModificacion: this.usuarioService.idUsuario,
      listOrdenamientoProteccionDet: this.listOrdenInterno
    };

    params.push(param);
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ordenamientoInternoService.registrarOrdenamiento(params).subscribe(
      (response: any) => {
        this.dialog.closeAll();
        if (response.success) {
          this.toast.ok('Se registró el Ordenamiento Interno correctamente.');
          this.listarOrdenamiento();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
      }, () => this.dialog.closeAll()
    );
  }

  eliminarManejoForestal(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idManejoBosqueDet != 0) {
          var params = {
            idManejoBosque:
              this.accordeonEspecificacionesSistemaAprovechamiento
                .idManejoBosque,
            idManejoBosqueDet: data.idManejoBosqueDet,
            idUsuarioElimina: this.usuarioService.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó el registro correctamente.');
                this.categoriaOrdenamiento.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.categoriaOrdenamiento.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  openModalEspecies(mensaje: string, especies: any, edit: any) {
    this.ref = this.dialogService.open(ModalFormularioEspeciesComponent, {
      header: mensaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: especies,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Información') {
        especies.nombreCientifico.label = resp.nombreCientifico;
        especies.nombreComun.label = resp.nombreComun;
        especies.prod = resp.prod;
        especies.dmcNorm = resp.dmcNorm;
        especies.dmcProp = resp.dmcProp;
      } else if (resp) {
        this.especies.push(resp);
      }
    });
  }

  eliminarEspecies(data: any) {

    this.especies = this.especies.filter((x) => x.prod != data.prod);
  }

  openModalEspeciesFlora(mensaje: string, especiesFlora: any, edit: any) {
    this.ref = this.dialogService.open(ModalFormularioEspeciesFloraComponent, {
      header: mensaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: especiesFlora,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Información') {
        especiesFlora.nombreCientifico = resp.nombreCientifico;
        especiesFlora.nombreComun = resp.nombreComun;
        especiesFlora.justificacion = resp.justificacion;
      } else if (resp) {
        this.especiesFlora.push(resp);
      }
    });
  }
  openModalFloraProteger(mensaje: string, especieFlora: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioEspeciesFloraPortegerComponent,
      {
        header: mensaje,
        width: '40%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        baseZIndex: 10000,
        data: {
          item: especieFlora,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Información') {
        especieFlora.tratamientoSilvicultural =
          resp.tratamientoSilvicultural;
          especieFlora.metodoConstruccion = resp.metodoConstruccion;
          especieFlora.manoObra = resp.manoObra;
          especieFlora.maquina = resp.maquina;
      } else if (resp) {
        this.llenartabla.push(resp);
        this.borrarDato=[];
        this.llenarCombo.forEach(
          (element: any) => {
           if (element.idManejoBosqueDet !== resp.idManejoBosqueDet) {
            this.borrarDato.push(element); } 
          });
          this.llenarCombo=[];
          this.llenarCombo=this.borrarDato;
  
      }
    });
  }

  eliminarEspeciesFlora(data: any) { }

  openModalEspecificaciones(mensaje: string, especificaciones: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioEspecificacionesComponent,
      {
        header: mensaje,
        width: '50%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        baseZIndex: 10000,
        data: {
          item: especificaciones,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Información') {
        especificaciones.tratamientoSilvicultural =
          resp.tratamientoSilvicultural;
        especificaciones.metodoConstruccion = resp.metodoConstruccion;
        especificaciones.manoObra = resp.manoObra;
        especificaciones.maquina = resp.maquina;
      } else if (resp) {
        this.especificaciones.push(resp);
      }
    });
  }
  openModalAreasDegradadas(mensaje: string, objetivo: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioAreasDegradadasComponent,
      {
        header: mensaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: objetivo,
        edit: edit,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Registro') {
        objetivo.tratamientoSilvicultural = resp.tratamientoSilvicultural;
        objetivo.descripcionOtro = resp.descripcionOtro;
      } else if (resp) {
          let obj = new ManejoForestalDetalleModel(resp);
          obj.tratamientoSilvicultural = resp.tratamientoSilvicultural;
          obj.descripcionOtro = resp.descripcionOtro;
          obj.idManejoBosqueDet = 0;
          obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
          obj.subCodtipoManejoDet = 'A';
          this.objetivo.push(obj);
          this.compararlistas();

      }
    });
  }

  eliminarEspecificaciones(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idManejoBosqueDet != 0) {
          var params = {
            idManejoBosque: this.accordeonCategoriaOrdemaniento.idManejoBosque,
            idManejoBosqueDet: data.idManejoBosqueDet,
            idUsuarioElimina: this.usuarioService.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó el registro correctamente.');
                this.especificaciones.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.especificaciones.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  openModalEspeciesImportantes(mensaje: string, especies: any, edit: any) {
    this.ref = this.dialogService.open(
      ModalFormularioEspeciesImportantesComponent,
      {
        header: mensaje,
        width: '40%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        baseZIndex: 10000,
        data: {
          item: especies,
          edit: edit,
        },
      }
    );
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar información') {
        especies.nombreCientifico = resp.nombreCientifico;
        especies.nombreComun = resp.nombreComun;
      } else if (resp) {
        this.especiesImportantes.push(resp);
      }
    });
  }

  openModalMultiEspeciesFlora = (mesaje: string) => {
    this.ref = this.dialogService.open(DialogTipoEspecieMultiselectComponent, {
      header: mesaje,
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          
          let obj = new ManejoForestalDetalleModel(resp);
          obj.nuIdEspecie = element.idEspecie;
          obj.especie = element.nombreCientifico;
          obj.descripcionOtro = element.nombreComun;
          obj.idManejoBosqueDet = 0;
          obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_2;
          obj.subCodtipoManejoDet = 'A';
          this.especiesImportantes.push(obj);
        });
        this.successMensaje('Se agregaron las especies Flora correctamente.');
      }
    });
  };

  successMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  eliminarEspeciesImportantes(data: any) {
    /* this.especiesImportantes.forEach((item: any) => {
      if(item.especie == data.especie) item.subCodtipoManejoDet = 'B';
    }); */

    //this.guardarManejoForestal(this.codigoManejo.ACORDEON_6_9_2);

    this.especiesImportantes = this.especiesImportantes.filter(
      (x) => x.especie !== data.especie
    );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach((element) => {
      if (
        element.codigoTipo !== undefined &&
        element.codigoTipo !== null &&
        element.codigoTipo === codigo
      ) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = {
          idArchivo: element.idArchivo,
          nombre: element.descArchivo,
        };
      }
    });
  }

  listarResultadosMovil() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: 'PGMF',
      // idPlanDeManejo: '998',
      // tipoPlan: 'POCC',
    };

    this.manejoService
      .actividadesDeAprovechamiento(params)
      .subscribe((response: any) => {
        if (
          this.especiesFlora.length == 0 &&
          !!response.data &&
          !!response.data.censoComercialDto.listaEspecieDtos
        ) {
          if (this.especiesFlora.length == 0) {
            response.data.censoComercialDto.listaEspecieDtos.map(
              (data: any) => {
                var obj = new ManejoForestalDetalleModel();
                obj.descripcionOtro = data.textNombreComun;
                obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_5;
                obj.especie = data.textNombreCientifico;

                this.especiesFlora.push(obj);
              }
            );
          }

          if (this.especies.length == 0) {
            response.data.censoComercialDto.listaEspecieDtos.map(
              (data: any) => {
                var obj = new ManejoForestalDetalleModel();
                obj.descripcionOtro = data.textNombreComun;
                obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_5;
                obj.especie = data.textNombreCientifico;

                this.especies.push(obj);
              }
            );
          }
        }
      });
  }

  crearCabeceras() {
    const codigoManejo = CodigosManejoBosquePGMF.CODIGO_PROCESO;
    const idPlanManejo = this.idPlanManejo;

    const idUsuarioRegistro = this.usuarioService.idUsuario;

    // 6.1
    this.accordeonCategoriaOrdemaniento.codigoManejo = codigoManejo;
    this.accordeonCategoriaOrdemaniento.idPlanManejo = idPlanManejo;
    this.accordeonCategoriaOrdemaniento.subCodigoManejo = CodigosManejoBosquePGMF.ACORDEON_6_1;
    this.accordeonCategoriaOrdemaniento.idUsuarioRegistro = idUsuarioRegistro;

    // 6.2
    this.accordeonSistemaManejo.idPlanManejo = idPlanManejo;
    this.accordeonSistemaManejo.codigoManejo = codigoManejo;
    this.accordeonSistemaManejo.subCodigoManejo = CodigosManejoBosquePGMF.ACORDEON_6_2;
    this.accordeonSistemaManejo.idUsuarioRegistro = idUsuarioRegistro;

    // 6.3
    this.accordeonCicloCorta.idPlanManejo = idPlanManejo;
    this.accordeonCicloCorta.codigoManejo = codigoManejo;
    this.accordeonCicloCorta.subCodigoManejo = CodigosManejoBosquePGMF.ACORDEON_6_3;
    this.accordeonCicloCorta.idUsuarioRegistro = idUsuarioRegistro;

    // 6.4
    this.accordeonEspeciesManejarDiametrosMinCorta.idPlanManejo = idPlanManejo;
    this.accordeonEspeciesManejarDiametrosMinCorta.codigoManejo = codigoManejo;
    this.accordeonEspeciesManejarDiametrosMinCorta.subCodigoManejo = CodigosManejoBosquePGMF.ACORDEON_6_4;
    this.accordeonEspeciesManejarDiametrosMinCorta.idUsuarioRegistro = idUsuarioRegistro;

    // 6.5

    this.accordeonEspeciesFloraProteger.idPlanManejo = idPlanManejo;
    this.accordeonEspeciesFloraProteger.codigoManejo = codigoManejo;
    this.accordeonEspeciesFloraProteger.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_5;
    this.accordeonEspeciesFloraProteger.idUsuarioRegistro = idUsuarioRegistro;

    // 6.6.1

    this.accordeonCortaAnualPermisible6_6_1.idPlanManejo = idPlanManejo;
    this.accordeonCortaAnualPermisible6_6_1.codigoManejo = codigoManejo;
    this.accordeonCortaAnualPermisible6_6_1.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_6;
    this.accordeonCortaAnualPermisible6_6_1.idUsuarioRegistro =
      idUsuarioRegistro;

    // 6.6.2

    this.accordeonCortaAnualPermisible6_6_2.idPlanManejo = idPlanManejo;
    this.accordeonCortaAnualPermisible6_6_2.codigoManejo = codigoManejo;
    this.accordeonCortaAnualPermisible6_6_2.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_6_2;
    this.accordeonCortaAnualPermisible6_6_2.idUsuarioRegistro =
      idUsuarioRegistro;

    // 6.7

    this.accordeonProyeccionCosecha.idPlanManejo = idPlanManejo;
    this.accordeonProyeccionCosecha.codigoManejo = codigoManejo;
    this.accordeonProyeccionCosecha.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_7;
    this.accordeonProyeccionCosecha.idUsuarioRegistro = idUsuarioRegistro;

    // 6.8

    this.accordeonEspecificacionesSistemaAprovechamiento.idPlanManejo =
      idPlanManejo;
    this.accordeonEspecificacionesSistemaAprovechamiento.codigoManejo =
      codigoManejo;
    this.accordeonEspecificacionesSistemaAprovechamiento.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_8;
    this.accordeonEspecificacionesSistemaAprovechamiento.idUsuarioRegistro =
      idUsuarioRegistro;

    // 6.9.1

    this.accordeonEspecificacionesPracticasSilviculturales6_9_1.idPlanManejo =
      idPlanManejo;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_1.codigoManejo =
      codigoManejo;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_1.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_9_1;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_1.idUsuarioRegistro =
      idUsuarioRegistro;

    // 6.9.2
    this.accordeonEspecificacionesPracticasSilviculturales6_9_2.idPlanManejo =
      idPlanManejo;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_2.codigoManejo =
      codigoManejo;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_2.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_9_2;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_2.idUsuarioRegistro =
      idUsuarioRegistro;

    // 6.9.3
    this.accordeonEspecificacionesPracticasSilviculturales6_9_3.idPlanManejo =
      idPlanManejo;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_3.codigoManejo =
      codigoManejo;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_3.subCodigoManejo =
      CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    this.accordeonEspecificacionesPracticasSilviculturales6_9_3.idUsuarioRegistro =
      idUsuarioRegistro;
  }

  agregarFila6_9_3() {
    const obj = new ManejoForestalDetalleModel();
    this.objetivo.push(obj);
  }

  deleteDescripcionOtros(){
    this.sistemaPiliciclico.descripcionOrd='';
  }

  guardarManejoForestal(codAccordeon: string, eliminar?: any) {
    this.crearCabeceras();
    let array = [];

    switch (codAccordeon) {
      case CodigosManejoBosquePGMF.ACORDEON_6_1:
        // 6.1 categoria de ordenamiento
        if (this.categoriaOrdenamiento.length > 0) {
          this.accordeonCategoriaOrdemaniento.listManejoBosqueDetalle =
            this.categoriaOrdenamiento;

          array.push(this.accordeonCategoriaOrdemaniento);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_2:
        // 6.2 sistema de manejo

        if(!this.validarGuardar_6_2()) return;

        if (!!this.sistemaPiliciclico.codOpcion || !!this.Anexo_6_2.selectModel) {
          if (this.accordeonSistemaManejo.listManejoBosqueDetalle.length > 0) {
            this.accordeonSistemaManejo.listManejoBosqueDetalle =
              this.accordeonSistemaManejo.listManejoBosqueDetalle.map(
                (data: any) => {
                  let obj = new ManejoForestalDetalleModel(data);
                  obj.codOpcion = this.sistemaPiliciclico.codOpcion ? this.sistemaPiliciclico.codOpcion : '';
                  obj.descripcionOrd = this.sistemaPiliciclico.descripcionOrd;

                  obj.catOrdenamiento = this.Anexo_6_2.selectModel;
                  obj.descripcionOtro = this.Anexo_6_2.selectModel === 'S' ? this.fileAnexo6_2.justificacion : '';
                  return obj;
                }
              );
          } else {
            let obj = new ManejoForestalDetalleModel();

            obj.codOpcion = this.sistemaPiliciclico.codOpcion ? this.sistemaPiliciclico.codOpcion : '';
            obj.descripcionOrd = this.sistemaPiliciclico.descripcionOrd;

            obj.catOrdenamiento = this.Anexo_6_2.selectModel;
            obj.descripcionOtro = this.Anexo_6_2.selectModel === 'S' ? this.fileAnexo6_2.justificacion : '';
            this.accordeonSistemaManejo.listManejoBosqueDetalle = [obj];
          }

          array.push(this.accordeonSistemaManejo);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_3:
        // 6.3 ciclo corta
        if(!this.validarGuardar_6_3()) return;

        if (!!this.selectDuracionMinima6_3.opcion || !!this.Anexo_6_3.selectModel) {
          if (this.accordeonCicloCorta.listManejoBosqueDetalle.length > 0) {
            this.accordeonCicloCorta.listManejoBosqueDetalle.map(
              (data: any) => {
                let obj = new ManejoForestalDetalleModel(data);
                obj.codOpcion = this.selectDuracionMinima6_3.opcion ? this.selectDuracionMinima6_3.opcion : '';
                obj.descripcionOrd = this.selectDuracionMinima6_3.opcion == 'N' ? this.selectDuracionMinima6_3.descripcion : '';
                this.accordeonCicloCorta.anio = (this.selectDuracionMinima6_3.opcion == 'N' ? +this.selectDuracionMinima6_3.anio : 20);

                obj.catOrdenamiento = this.Anexo_6_3.selectModel;
                obj.descripcionOtro = this.Anexo_6_3.selectModel === 'S' ? this.fileAnexo6_3.justificacion : '';

                this.accordeonCicloCorta.listManejoBosqueDetalle = [obj];
              }
            );
          } else {
            let obj = new ManejoForestalDetalleModel();
            obj.codOpcion = this.selectDuracionMinima6_3.opcion ? this.selectDuracionMinima6_3.opcion : '';
            obj.descripcionOrd = this.sistemaPiliciclico.codOpcion == 'N' ? this.sistemaPiliciclico.descripcionOrd : '';
            this.accordeonCicloCorta.anio = (this.selectDuracionMinima6_3.opcion == 'N' ? +this.selectDuracionMinima6_3.anio : 20);

            obj.catOrdenamiento = this.Anexo_6_3.selectModel;
            obj.descripcionOtro = this.Anexo_6_3.selectModel === 'S' ? this.fileAnexo6_3.justificacion : '';

            this.accordeonCicloCorta.listManejoBosqueDetalle = [obj];
          }

          array.push(this.accordeonCicloCorta);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_4:
        if(!this.validarGuardar_6_4()) return;
        // 6.4 especies a manejar y diametros min de corta
        if (!!this.selectDuracionMinima6_3.opcion || !!this.Anexo_6_4.selectModel || this.especies.length > 0) {
          // Guarda agrupacion para listado
          this.accordeonEspeciesManejarDiametrosMinCorta.tipo = this.agrupacion6_4 ? this.agrupacion6_4 : '';

          this.accordeonEspeciesManejarDiametrosMinCorta.txDetalle = this.Anexo_6_4.selectModel;
          this.accordeonEspeciesManejarDiametrosMinCorta.txObservacion = this.Anexo_6_4.selectModel === 'S' ? this.fileAnexo6_4.justificacion : '';

          this.accordeonEspeciesManejarDiametrosMinCorta.listManejoBosqueDetalle = this.especies ? this.especies : [];

          array.push(this.accordeonEspeciesManejarDiametrosMinCorta);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_5:
        if(!this.validarGuardar_6_5()) return;
        // 6.5 especies flora a proteger
        if (this.llenartabla.length > 0 || !!this.Anexo_6_5.selectModel) {
          
          this.accordeonEspeciesFloraProteger.txDetalle = this.Anexo_6_5.selectModel;
          this.accordeonEspeciesFloraProteger.txObservacion = this.Anexo_6_5.selectModel === 'S' ? this.fileAnexo6_5.justificacion : '';
          

          this.accordeonEspeciesFloraProteger.listManejoBosqueDetalle = this.llenartabla ? this.llenartabla : [];
          array.push(this.accordeonEspeciesFloraProteger);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_6_1:
        // 6.6.1 Es volumen comercial promedio por hectarea
        if (this.volumenComercial.tabla.length > 0) {
          this.volumenComercial.tabla.map((data: any) => {
            const obj = new ManejoForestalDetalleModel(data);
            obj.descripcionOtro = data.descripcionOtro; //nombre comun
            obj.especie = data.especie; //nombre cientifico
            obj.nuValorVcpProd = data.nuValorVcpProd; //areha arboles
            obj.nuValorVcap = data.nuValorVcap; //areha vpc
            obj.nuTotalNroArboles = data.nuTotalNroArboles;
            obj.nuTotalVcp = data.nuTotalVcp;

            this.accordeonCortaAnualPermisible6_6_1.listManejoBosqueDetalle.push(
              obj
            );
          });

          array.push(this.accordeonCortaAnualPermisible6_6_1);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_6_2:
        if (!this.validarGuardar_6_6_2()) return;
        // 6.6.2 volumen de corta anual permisible
        if (this.Anexo_6_6_2.selectModel) {
          this.accordeonCortaAnualPermisible6_6_2.txDetalle = this.Anexo_6_6_2.selectModel;
          this.accordeonCortaAnualPermisible6_6_2.txObservacion = this.Anexo_6_6_2.selectModel === 'S' ? this.fileAnexo6_6_2.justificacion : '';

          /*this.volumenCorta.map((data: any) => {
            const obj = new ManejoForestalDetalleModel(data);
            obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_2;
            obj.especie = data.especie; //tipo bosque
            obj.nuDiametro = data.nuDiametro; //AFP (Ha)
            obj.nuCaminoAcceso = data.nuCaminoAcceso; //AFP (%)
            obj.nuValorVcpProd = data.nuValorVcpProd;
            obj.nuTotalVcp = data.nuTotalVcp;
            obj.nuValorVcap = data.nuValorVcap;

            this.accordeonCortaAnualPermisible6_6_2.listManejoBosqueDetalle.push(
              obj
            );
          });*/

          array.push(this.accordeonCortaAnualPermisible6_6_2);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_7:
        // 6.7 proyeccion cosecha
        if (this.proyeccionCosecha.length > 0) {
          this.accordeonProyeccionCosecha.listManejoBosqueDetalle =
            this.proyeccionCosecha;

          array.push(this.accordeonProyeccionCosecha);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_8:
        if(!this.validarGuardar_6_8()) return;
        // 6.8 especificaciones sobre el sistema de aprovechamiento
        if (this.especificaciones.length > 0 || this.Anexo_6_8.selectModel) {
          this.accordeonEspecificacionesSistemaAprovechamiento.txDetalle = this.Anexo_6_8.selectModel;
          this.accordeonEspecificacionesSistemaAprovechamiento.txObservacion = this.Anexo_6_8.selectModel === 'S' ? this.fileAnexo6_8.justificacion : '';

          this.accordeonEspecificacionesSistemaAprovechamiento.listManejoBosqueDetalle = this.especificaciones ? this.especificaciones : [];

          array.push(this.accordeonEspecificacionesSistemaAprovechamiento);
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_9_1:
        // 6.9.1 necesidad y diseño
        this.accordeonEspecificacionesPracticasSilviculturales6_9_1.txDetalle =
          this.selectMarcacionProteccionSemillas.opcion ? 'S' : '';

        this.accordeonEspecificacionesPracticasSilviculturales6_9_1.txObservacion =
          this.selectMarcacionProteccionSemillas.opcion
            ? this.selectMarcacionProteccionSemillas.descripcion
            : '';

        array.push(
          this.accordeonEspecificacionesPracticasSilviculturales6_9_1
        );

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_9_2:
        if(!this.validarGuardar_6_9_2()) return;
        // 6.9.2
        if (!!this.selectMarcacionProteccionSemillas.opcion || this.especiesImportantes.length > 0 ||
          this.tratamientos.length > 0 || this.Anexo_6_9_2.selectModel
        ) {

          this.accordeonEspecificacionesPracticasSilviculturales6_9_2.anio= this.select6_9_2.anio ;

          this.accordeonEspecificacionesPracticasSilviculturales6_9_2.codigoEscala=
          ( this.select6_9_2.opcion == '1')?'ESCTRASILPIL':'ESCTRASILOP';

          if(this.select6_9_2.opcion == '1'){
            this.select6_9_2.anio=null;
            this.accordeonEspecificacionesPracticasSilviculturales6_9_2.anio=null;
          }


          this.accordeonEspecificacionesPracticasSilviculturales6_9_2.listManejoBosqueDetalle =
            [];
          this.accordeonEspecificacionesPracticasSilviculturales6_9_2.txDetalle =
            this.selectMarcacionProteccionSemillas.opcion ? 'S' : '';
          this.accordeonEspecificacionesPracticasSilviculturales6_9_2.txObservacion =
            this.selectMarcacionProteccionSemillas.opcion
              ? this.selectMarcacionProteccionSemillas.descripcion
              : '';

          if (this.especiesImportantes.length > 0) {
            this.especiesImportantes.map((data: any) => {
              const obj = new ManejoForestalDetalleModel(data);
              obj.subCodtipoManejoDet = 'A';
              obj.catOrdenamiento = this.Anexo_6_9_2.selectModel;
              this.accordeonEspecificacionesPracticasSilviculturales6_9_2.listManejoBosqueDetalle.push(
                obj
              );
            });
          }

          this.tratamientos.map((data: any) => {
            const obj = new ManejoForestalDetalleModel(data);
            obj.subCodtipoManejoDet = 'B';
            obj.catOrdenamiento = this.Anexo_6_9_2.selectModel;
            this.accordeonEspecificacionesPracticasSilviculturales6_9_2.listManejoBosqueDetalle.push(
              obj
            );
          });

          array.push(
            this.accordeonEspecificacionesPracticasSilviculturales6_9_2
          );
        }

        break;
      case CodigosManejoBosquePGMF.ACORDEON_6_9_3:
        if(!this.validarGuardar_6_9_3()) return;
        // 6.9.3
        this.accordeonEspecificacionesPracticasSilviculturales6_9_3.txDetalle = this.Anexo_6_9_3.selectModel;
        this.accordeonEspecificacionesPracticasSilviculturales6_9_3.txObservacion = this.Anexo_6_9_3.selectModel === 'S' ? this.fileAnexo6_9_3.justificacion : '';

        this.accordeonEspecificacionesPracticasSilviculturales6_9_3.listManejoBosqueDetalle =
          this.objetivo ? this.objetivo : [];

        array.push(
          this.accordeonEspecificacionesPracticasSilviculturales6_9_3
        );
          
        break;

    }
    let mensaje ="Se registró el Manejo Forestal";
    if(eliminar?.valor ){
      let arrayDetalle = [];
      eliminar.obj.descripcionOtros = null;
      arrayDetalle.push(eliminar.obj)
      array[0].listManejoBosqueDetalle = arrayDetalle;
      mensaje = "Se eliminó el Manejo Forestal Correctamente"
    }
    
    this.manejoBosqueService.registrarManejoBosque(array).subscribe((data: any) => {
      this.successMensaje(mensaje);
      this.especiesFlora=[];
      this.llenartabla =[];
      this.llenarCombo=[];
      this.listEspeciesFloraProteger();
    });

  }

  btnEliminarAreaDeg(event: any, data: any, index: number ){
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {

        if (data.idManejoBosqueDet != 0 && data.descripcionOtros.length>0) {
          var params = {
            idManejoBosqueDet: data.idManejoBosqueDet,
           idUsuarioElimina:  this.usuarioService.usuario.idusuario,
           idManejoBosque: 0
          };
          
          this.dialog.open(LoadingComponent, { disableClose: true });
         this.manejoBosqueService.eliminarManejoBosque(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.objetivo=[];
                this.listEspecificacionesManejoEnriquesimientoAreasDegradadas();
                this.toast.ok("Se eliminó un objetivo correctamente.");
              } else {
                this.toast.ok("Ocurrio un Error");
              }
            });
        }else {
          this.objetivo.splice(index, 1);
        }
      },
      reject: () => { },
    });
  }

  btnEliminar(event: any, data: any, index: number) {
    let especieFloreaProtegerAux = Object.assign({},this.accordeonEspeciesFloraProteger);
    // let lista = [];
    let arrayDetalle = [];
    
    arrayDetalle.push(data)
    especieFloreaProtegerAux.listManejoBosqueDetalle = arrayDetalle;
    // lista.push(especieFloreaProtegerAux);

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {

        if (data.idManejoBosqueDet != 0) {
          this.especiesFlora.forEach((item: any) => {
            if(item.idManejoBosqueDet==data.idManejoBosqueDet && item.descripcionOtros==""){
              this.llenartabla.splice(index, 1);
              this.llenarCombo=[];
              var resultado: any =[];
              this.llenartabla.forEach(
                (elem: any) => {
                  if(elem){ 
                    resultado.push(elem.idManejoBosqueDet);}
                });
              this.especiesFlora.forEach(
                (element: any) => {
                  if(element.idManejoBosqueDet!==resultado)
                  this.llenarCombo.push(element);
                });
              
            }
            if(item.idManejoBosqueDet==data.idManejoBosqueDet && item.descripcionOtros.length>0){
              var params = {
                obj: data,
                valor:  true
              };
              this.guardarManejoForestal(this.codigoManejo.ACORDEON_6_5,params)
            }
          });
          
         
        }else {
          this.llenartabla.splice(index, 1);
        }

      },
      reject: () => { },
    });
  }

  
  aceptTabla = ".pdf, .png";
  tipoArchivoTablaCod: string[] = ["application/pdf", "image/png"];
  addArchivoTabla(item: any, file: any) {
    const files = file?.target?.files as FileList
    if (files && files.length > 0) {
      const fileExt = files[0].type.toLocaleLowerCase();
      if (this.tipoArchivoTablaCod.includes(fileExt)) {
        const file = files[0];
        this.registrarArchivoTabla(item, file);
      } else {
        this.toast.warn("(*) Formato no valido (pdf o png)");
      }
    }
  }

  registrarArchivoTabla(item: any, file: any) {
    const codigo = TiposArchivos.TDOCTABLA692;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuario.idusuario, codigo, file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        item.idArchivo = result.data;
      } else {
        this.toast.warn(result.message);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error("No se pudo cargar el archivo.");
    }
    );
  }

  descargarArchivoTabla(idFile: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    const params = { "idArchivo": idFile };
    this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error("No se pudo descargar el archivo.");
    });
  }

  //6.6.1 ()
  auxComboBloque: any [] = [];
  listaTotalBloque: any[] = [];
  listaCabeceraBloque: any[] = [];
  listaEspecieBloque: any[] = [];
  listaTotalEspecieBloque: any[] = [];

  listaEspecieVolumenAnualPermisible: any[] = [];
  listaTotalCortaAnual: any[] = [];
  aux : any;
  toTalAreaHa: any;
  aprovechamiento: any;
  idvalor: any;

  listarVolComercialPromedio() {
    const params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: 'PGMF' };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalServ.listarVolumenComercialPromedio(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if(response.success && response.data){
        this.listaTotalBloque=response.data;
        this.auxComboBloque = response.data.map((item: any) => {
          return { valorPrimario: item.bloquequinquenal, codigo: item.bloque }
        });
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  listarEspecieVolComercialPromedio() {
    const params = { idPlanManejo: this.idPlanManejo, tipoPlan: 'PGMF' };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalServ.listarEspecieVolumenComercialPromedio(params).subscribe((response: any) => {
      this.dialog.closeAll();

      if(response.success && response.data){
        this.listaTotalEspecieBloque = response.data;

      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  listarEspecieVolumenCortaAnualPermisible() {
    this.listaEspecieVolumenAnualPermisible = [];
    const params = { idPlanManejo: this.idPlanManejo, tipoPlan: 'PGMF' };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalServ.listarEspecieVolumenCortaAnualPermisible(params).subscribe((response: any) => {
      this.dialog.closeAll();
      this.toTalAreaHa="-";
      if(response.success && response.data){
        this.listaEspecieVolumenAnualPermisible = response.data;
        this.obtenerToales662();
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  obtenerToales662() {
    this.listaTotalCortaAnual = [];
    let cont1 = 0;
    let cont2 = 0;
    let cont3 = 0;
    let contha = 0;
    let contPor = 0;

    this.listaEspecieVolumenAnualPermisible.forEach((item: any) => {
      contha = contha + item.cantidadArb;
      contPor = contPor + item.porcentajeArb;
      cont1 = cont1 + item.volumenComercialPromedio;
      cont2 = cont2 + item.volumenComercialPromedioPonderado;
      cont3 = cont3 + item.volumenCortaAnualPermisible;
    });
    contPor = Math.round(contPor);
    this.listaTotalCortaAnual.push(contha, contPor, cont1, cont2, cont3);
  }

  buscandoVacios() {
    this.llenarCombo=[];
    this.especiesFlora.forEach(
      (element: any) => {
      if(element.descripcionOtros == ""){
        this.llenarCombo.push(element);
      }

      });
  }
  buscandoLlenos() {

    this.especiesFlora.forEach(
      (element: any) => {
      if(element.descripcionOtros.length>0){
        this.array=element;
        this.llenartabla.push(this.array);
      }

      });
  }

  llenartablavacia(){
    let obj = new ManejoForestalDetalleModel();
    obj.tratamientoSilvicultural = "Ubicación y extensión del area degradada de manejo o enriquecimiento";
    obj.idManejoBosqueDet = 0;
    obj.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj.subCodtipoManejoDet = 'A';
    obj.descripcionOrd="PGMF";

    let obj1 = new ManejoForestalDetalleModel();
    obj1.tratamientoSilvicultural = "Métodos de manejo para recuperación del área degradada";
    obj1.idManejoBosqueDet = 0;
    obj1.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj1.subCodtipoManejoDet = 'A';
    obj1.descripcionOrd="PGMF";

    let obj2 = new ManejoForestalDetalleModel();
    obj2.tratamientoSilvicultural = "Método de enriquecimiento a utilizar";
    obj2.idManejoBosqueDet = 0;
    obj2.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj2.subCodtipoManejoDet = 'A';
    obj2.descripcionOrd="PGMF";

    let obj3 = new ManejoForestalDetalleModel();
    obj3.tratamientoSilvicultural = "Especies a utilizar";
    obj3.idManejoBosqueDet = 0;
    obj3.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj3.subCodtipoManejoDet = 'A';
    obj3.descripcionOrd="PGMF";

    let obj4 = new ManejoForestalDetalleModel();
    obj4.tratamientoSilvicultural = "Procedencia de las semillas";
    obj4.idManejoBosqueDet = 0;
    obj4.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj4.subCodtipoManejoDet = 'A';
    obj4.descripcionOrd="PGMF";

    let obj5 = new ManejoForestalDetalleModel();
    obj5.tratamientoSilvicultural = "Instalación de viveros temporales";
    obj5.idManejoBosqueDet = 0;
    obj5.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj5.subCodtipoManejoDet = 'A';
    obj5.descripcionOrd="PGMF";

    let obj6 = new ManejoForestalDetalleModel();
    obj6.tratamientoSilvicultural = "Producción de plantones forestales (semillas o repique de regeneración natural)";
    obj6.idManejoBosqueDet = 0;
    obj6.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj6.subCodtipoManejoDet = 'A';
    obj6.descripcionOrd="PGMF";

    let obj7 = new ManejoForestalDetalleModel();
    obj7.tratamientoSilvicultural = "Labores culturales previstas";
    obj7.idManejoBosqueDet = 0;
    obj7.codtipoManejoDet = CodigosManejoBosquePGMF.ACORDEON_6_9_3;
    obj7.subCodtipoManejoDet = 'A';
    obj7.descripcionOrd="PGMF";
    
      this.objetivo.push(obj,obj1,obj2,obj3,obj4,obj5,obj6,obj7);
  
  }

  listapordef(){
    let obj = new ManejoForestalDetalleModel();
    obj.tratamientoSilvicultural = "Ubicación y extensión del area degradada de manejo o enriquecimiento";

    let obj1 = new ManejoForestalDetalleModel();
    obj1.tratamientoSilvicultural = "Métodos de manejo para recuperación del área degradada";
    let obj2 = new ManejoForestalDetalleModel();
    obj2.tratamientoSilvicultural = "Método de enriquecimiento a utilizar";

    let obj3 = new ManejoForestalDetalleModel();
    obj3.tratamientoSilvicultural = "Especies a utilizar";

    let obj4 = new ManejoForestalDetalleModel();
    obj4.tratamientoSilvicultural = "Procedencia de las semillas";

    let obj5 = new ManejoForestalDetalleModel();
    obj5.tratamientoSilvicultural = "Instalación de viveros temporales";

    let obj6 = new ManejoForestalDetalleModel();
    obj6.tratamientoSilvicultural = "Producción de plantones forestales (semillas o repique de regeneración natural)";

    let obj7 = new ManejoForestalDetalleModel();
    obj7.tratamientoSilvicultural = "Labores culturales previstas";

    this.listaPordefecto.push(obj,obj1,obj2,obj3,obj4,obj5,obj6,obj7);
    }
    


compararlistas(){
  this.objetivo.forEach(element => {
    element.btnElim=true;
    this.listaPordefecto.forEach(elem => {
      if(element.tratamientoSilvicultural == elem.tratamientoSilvicultural){
        element.btnElim=false
      }
        
    });
  });
  
}
  auxChangeBloque(val: any) {
    this.listaCabeceraBloque = [];
    this.listaEspecieBloque = [];

     this.aux = this.listaTotalBloque.find(item => item.bloque === val.value);

    if (this.aux ) {
      this.toTalAreaHa=this.aux.toTalAreaHa;
      this.aprovechamiento=this.aux.detalle[0].periodoAprovechamiento;
      this.idvalor=this.aux.detalle[0].idOrdenamientoProteccion;
      this.listaCabeceraBloque = this.aux.detalle || [];
      this.listaEspecieBloque = this.aux.listEspecie || [];
    }
  }

  guardarAprovechamiento(){
    if(this.aux == null){
      this.toast.warn('Debe seleccionar un bloque');

    }
    else{
    var params = { "idOrdenamientoProteccion": this.idvalor, "periodoAprovechamiento": this.aprovechamiento };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalServ.actualizarPeriodoAprovechamiento(params).subscribe((response: any) => {
      this.dialog.closeAll();
        if (response.success) {
          this.toast.ok('Se registró el Periodo Aprovechamiento correctamente.');
          this.listarVolComercialPromedio();
        } else {
          this.toast.error('Ocurrió un problema, intente nuevamente', 'ERROR');
        }
    });}
  }

  //6.7 ()
  listaCosecha: any[] = [];
  listaCosechaCols: any[] = [];
  listaCosechaTotal: any[] = [];
  rango: number = 50;

  listarCosecha() {
    var params = { idPlanManejo: this.idPlanManejo, tipoPlan: 'PGMF' };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalServ.listarProyeccionCosecha(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success && response.data) {
        this.listaCosecha = response.data;

        this.listaCosechaCols = (this.listaCosecha.length > 0) ? [...this.listaCosecha[0]["detalle"]] : [];
        let auxListaAnio: any[] = this.listaCosecha.map(item => item.detalle);

        let cont = 0;
        for (let index = 0; index < this.listaCosechaCols.length; index++) {
          for (let index2 = 0; index2 < auxListaAnio.length; index2++) {
            cont = cont + auxListaAnio[index2][index].valor;
          }
          this.listaCosechaTotal.push(cont);
          cont = 0;
        }
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  // PARA LOS ANEXOS 4
  Anexo_6_2: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_2, show: false };
  Anexo_6_3: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_3, show: false };
  Anexo_6_4: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_4, show: false };
  Anexo_6_5: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_5, show: false };
  Anexo_6_6_2: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_6_TAB_2, show: false };
  Anexo_6_8: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_8, show: false };
  Anexo_6_9_2: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_9_TAB_2, show: false };
  Anexo_6_9_3: any = { id: 0, selectModel: "N", codigo: CodigoPGMF.ACORDEON_6_9_TAB_3, show: false };

  fileAnexo6_2: any = {};
  fileAnexo6_3: any = {};
  fileAnexo6_4: any = {};
  fileAnexo6_5: any = {};
  fileAnexo6_6_2: any = {};
  fileAnexo6_8: any = {};
  fileAnexo6_9_2: any = {};
  fileAnexo6_9_3: any = {};

  onTabOpen(event: any) {
    switch (event.index) {
      case 1: this.Anexo_6_2.show = true; break;
      case 2: this.Anexo_6_3.show = true; break;
      case 3: this.Anexo_6_4.show = true; break;
      case 4: this.Anexo_6_5.show = true; break;
      case 5:
        this.Anexo_6_6_2.show = true;
        this.listarVolComercialPromedio(); //TAB 6.6.1
        this.listarEspecieVolComercialPromedio();
        this.listarEspecieVolumenCortaAnualPermisible();
        break;
      case 6:
        this.listarCosecha(); //TAB 6.7
        break;
      case 7: this.Anexo_6_8.show = true; break;
      case 8: this.Anexo_6_9_2.show = true; this.Anexo_6_9_3.show = true; break;
      default: break;
    }
  };

  cargarIdArchivo(data: any, codigoArchivo: string) {
    switch (codigoArchivo) {
      case this.Anexo_6_2.codigo:
        this.Anexo_6_2.id = data.idPGMFArchivo;
        this.Anexo_6_2.selectModel = "S";
        break;
      case this.Anexo_6_3.codigo:
        this.Anexo_6_3.id = data.idPGMFArchivo;
        this.Anexo_6_3.selectModel = "S"
        break;
      case this.Anexo_6_4.codigo:
        this.Anexo_6_4.id = data.idPGMFArchivo;
        this.Anexo_6_4.selectModel = "S"
        break;
      case this.Anexo_6_5.codigo:
        this.Anexo_6_5.id = data.idPGMFArchivo;
        this.Anexo_6_5.selectModel = "S"
        break;
      case this.Anexo_6_6_2.codigo:
        this.Anexo_6_6_2.id = data.idPGMFArchivo;
        this.Anexo_6_6_2.selectModel = "S"
        break;
      case this.Anexo_6_8.codigo:
        this.Anexo_6_8.id = data.idPGMFArchivo;
        this.Anexo_6_8.selectModel = "S"
        break;
      case this.Anexo_6_9_2.codigo:
        this.Anexo_6_9_2.id = data.idPGMFArchivo;
        this.Anexo_6_9_2.selectModel = "S"
        break;
      case this.Anexo_6_9_3.codigo:
        this.Anexo_6_9_3.id = data.idPGMFArchivo;
        this.Anexo_6_9_3.selectModel = "S"
        break;
      default:
        break;
    }
  }

  anexoN(idArchivo: number, fileAnexo?: any) {
    if (idArchivo !== 0) this.eliminarPlanManejo(idArchivo, fileAnexo);
  }

  eliminarPlanManejo(idArchivo: number, fileAnexo?: any) {
    var params = { idPlanManejoArchivo: idArchivo, idUsuarioElimina: this.usuario.idusuario };
    this.servicePostulacionPFDM.eliminarPlanManejoArchivo(params).subscribe((response: any) => {
      fileAnexo.nombreFile = "";
      fileAnexo.justificacion = "";
    });
  }

  validarGuardar_6_2() {
    let valido = true;
    if (this.sistemaPiliciclico.codOpcion === 'N' && !this.sistemaPiliciclico?.descripcionOrd) {
      valido = false;
      this.toast.warn("(*)Ingrese la especificación del sistema.");
    }
    if (this.Anexo_6_2.selectModel === "S") {
      if (!this.fileAnexo6_2.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_2.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_3() {
    let valido = true;
    if (this.selectDuracionMinima6_3.opcion == 'N' && !this.selectDuracionMinima6_3.anio) {
      valido = false;
      this.toast.warn("(*)Ingrese la cantidad de Años.");
    }
    if (this.selectDuracionMinima6_3.opcion == 'N' && !this.selectDuracionMinima6_3.descripcion) {
      valido = false;
      this.toast.warn("(*)Ingrese el campo Justificar.");
    }
    if (this.Anexo_6_3.selectModel === "S") {
      if (!this.fileAnexo6_3.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_3.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_4() {
    let valido = true;
    if (this.Anexo_6_4.selectModel === "S") {
      if (!this.fileAnexo6_4.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_4.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_5() {
    let valido = true;
    if (this.Anexo_6_5.selectModel === "S") {
      if (!this.fileAnexo6_5.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_5.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_6_2() {
    let valido = true;
    if (this.Anexo_6_6_2.selectModel === "S") {
      if (!this.fileAnexo6_6_2.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_6_2.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_8() {
    let valido = true;
    if (this.Anexo_6_8.selectModel === "S") {
      if (!this.fileAnexo6_8.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_8.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_9_2() {
    let valido = true;
    if (this.select6_9_2.opcion == null || this.select6_9_2.opcion == "") {
      valido = false;
      this.toast.warn("(*) Seleccione la Escala.");
    } else if (this.select6_9_2.opcion == '2' && !this.select6_9_2.anio) {
      valido = false;
      this.toast.warn("(*) Ingrese cantidad de Años.");
    }

    if(this.otrosTratamientos && !this.otroTratamiento6_9_2.descripcionOtro) {
      valido = false;
      this.toast.warn("(*) Debe ingresar Descripción.");
    }

    if (this.Anexo_6_9_2.selectModel === "S") {
      if (!this.fileAnexo6_9_2.justificacion) {
        valido = false;
        this.toast.warn("(*) Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_9_2.nombreFile) {
        valido = false;
        this.toast.warn("(*) Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }

  validarGuardar_6_9_3() {
    let valido = true;
    if (this.Anexo_6_9_3.selectModel === "S") {
      if (!this.fileAnexo6_9_3.justificacion) {
        valido = false;
        this.toast.warn("Debe ingresar la justificación.");
      }
      if (!this.fileAnexo6_9_3.nombreFile) {
        valido = false;
        this.toast.warn("Debe adjuntar documento de evidencia.");
      }
    }
    return valido;
  }
}
