import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Observable, } from "rxjs";

import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";

import { PlanificacionService } from "src/app/service/planificacion.service";
import { ActividadComponent } from "./modal/actividad/actividad.component";
import { ImpactoComponent } from "./modal/impacto/impacto.component";
import { ContingenciaComponent } from "./modal/contingencia/contingencia.component";
import { EvaluacionAmbientalTabla } from "./evaluacion-ambiental-tabla/evaluacion-ambiental.tabla";

import { ArchivoService, UsuarioService } from "@services";
import { ArchivoTipo, DownloadFile, ToastService } from "@shared";
import { ResponseModel } from "@models";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";
import { FileModel } from 'src/app/model/util/File';
import { PlanManejoEvaluacionService } from "src/app/service/plan-manejo-evaluacion.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { PlantillaConst } from "src/app/shared/plantilla.const";

@Component({
  selector: "app-tab-evaluacion-impacto",
  templateUrl: "./tab-evaluacion-impacto.component.html",
  styleUrls: ["./tab-evaluacion-impacto.component.scss"],
})
export class TabEvaluacionImpactoComponent implements OnInit {
  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  @ViewChild(EvaluacionAmbientalTabla) tablaDinamica!: EvaluacionAmbientalTabla;
  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento2: LineamientoInnerModel = new LineamientoInnerModel();
  lineamiento3: LineamientoInnerModel = new LineamientoInnerModel();
  ref!: DynamicDialogRef;

  archivo: any

  archivoPGA: any

  archivoPAP: any

  archivoPVS: any

  archivoPCA: any

  listaContingencia: any[] = [];

  selectAnexo: string | null = null;
  idPlanManejoArchivo: number = 0;

  actividades: any[] = [];
  vigilancia : any[] =[];
  procesando: boolean = false;

  codigo_PGMF_ACORDEON = 'PCA';

  totalRecords: number = 0;
  totalRecords2: number = 0;
  totalRecords3: number = 0;

  file: any = {};

  constructor(
    private dialog: MatDialog,
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private apiArchivo: ArchivoService,
    private messageService: MessageService,
    private user: UsuarioService,
    private postulacionPFDMService: PostulacionPFDMService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private toast: ToastService,
    private confirmationService: ConfirmationService

  ) {
    this.archivo = {
      idEvalActividad: 0,
      codTipoActividad: 'POAPRO',
      tipoActividad: 'ARCHIVO',
      tipoNombreActividad: '',
      nombreActividad: '',
      file: null
    }
    this.archivoPGA = {
      file: null
    }
    this.archivoPAP = {
      file: null
    }
    this.archivoPVS = {
      file: null
    }
    this.archivoPCA = {
      file: null
    }
  }

  ngOnInit(): void {
    this.getArchivo();
    this.getContingencia();
  }

  listarEvaluaciones(data: any) {
    // this.ordenarLista(data, 'PGMFEVALLIN12.1', this.lineamiento);
    // this.ordenarLista(data, 'PGMFEVALLIN12.2', this.lineamiento2);
    this.ordenarLista(data, 'PGMFEVALLIN12', this.lineamiento3);
  }

  dataLineamientos(data: any) {

    // this.lineamiento = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN12.1' }
    // );
    // this.lineamiento2 = new LineamientoInnerModel(
    //   { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN12.2' }
    // );
    this.lineamiento3 = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN12' }
    );
  }

  onList(list: any[]) {
    this.actividades = [];
    this.totalRecords = 0;
    for (let i = 0; i < list.length; i++) {
      if(list[i].tipoAprovechamiento == "POPREVENTIVO"){
        this.actividades.push(list[i]);
        this.totalRecords++;
      }
    }

    this.vigilancia= [];
    this.totalRecords2 = 0;
    for (let i = 0; i < list.length; i++) {
    
      if(list[i].tipoAprovechamiento == "POVIGILANCIA"){
        this.vigilancia.push(list[i]);
        this.totalRecords2++;
      }
    }
  }

  getArchivo() {
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad({ idPlanManejo: this.idPGMF, tipoActividad: "ARCHIVO" })
      .subscribe((res: any) => {
        if (res?.success && res?.data?.length > 0) {
          this.archivo = res.data
          this.archivo.file = this.archivo.nombreActividad;
        }
      });
  }

  modalActividad = (mesaje: string, edit?: any, data?: any) => {
    let idEvalAprovechamiento: number = 0;
    if (edit == "true") {
      idEvalAprovechamiento = data.idEvalAprovechamiento;
    }
    this.ref = this.dialogService.open(ActividadComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        editar: edit,
      },
    });
    if (idEvalAprovechamiento > 0) {
      this.ref.onClose.subscribe((resp: any) => {
        if (resp) {
          const params = {
            idEvalAprovechamiento,
            codTipoAprovechamiento: data.codTipoAprovechamiento,
            tipoAprovechamiento: data.tipoAprovechamiento,
            tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
            nombreAprovechamiento: data.nombreAprovechamiento,
            impacto: resp.descripcionImpacto,
            medidasControl: resp.medidasControl,
            medidasMonitoreo: "",
            frecuencia: "",
            acciones: "",
            responsable: "",
            responsableSecundario: "",
            contingencia: "",
            auxiliar: "",
            idPlanManejo: this.idPGMF,
            idUsuarioRegistro: this.user.idUsuario
          };
          this.planificacionService
            .registrarAprovechamientoEvalAmbiental(params)
            .subscribe((_res) => {
              this.toast.ok('Se guardo plan de acción preventivo-corrector correctamente.\n');
              this.tablaDinamica.getData()
            });
        }
      });
    } else {
      this.ref.onClose.subscribe((resp: any) => {
        if (resp) {
          const params = {
            idEvalAprovechamiento,
            codTipoAprovechamiento: "POPREVENTIVO",
            tipoAprovechamiento: "POPREVENTIVO",
            tipoNombreAprovechamiento: "Plan Preventivo",
            nombreAprovechamiento: resp.nombreActividad,
            impacto: resp.descripcionImpacto,
            medidasControl: resp.medidasControl,
            medidasMonitoreo: "",
            frecuencia: "",
            acciones: "",
            responsable: "",
            responsableSecundario: "",
            contingencia: "",
            auxiliar: "",
            idPlanManejo: this.idPGMF,
            idUsuarioRegistro: this.user.idUsuario
          };
          this.planificacionService
            .registrarAprovechamientoEvalAmbiental(params)
            .subscribe((_res) => {
              this.toast.ok('Se registró plan de acción preventivo-corrector correctamente.\n');
              this.tablaDinamica.getData();
            });
        }
      });
    }
  };

  modalImpacto = (mesaje: string, edit?: any, data?: any) => {
    let idEvalAprovechamiento: number = 0;
    if (edit == "true") {
      idEvalAprovechamiento = data.idEvalAprovechamiento;
    }
    this.ref = this.dialogService.open(ImpactoComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        editar: edit,
      },
    });
    if (idEvalAprovechamiento > 0) {
      this.ref.onClose.subscribe((resp: any) => {
        if (resp) {
          const params = {
            idEvalAprovechamiento,
            codTipoAprovechamiento: data.codTipoAprovechamiento,
            tipoAprovechamiento: data.tipoAprovechamiento,
            tipoNombreAprovechamiento: data.tipoNombreAprovechamiento,
            nombreAprovechamiento: data.nombreAprovechamiento,
            impacto: data.impacto,
            medidasControl: data.medidasControl,
            medidasMonitoreo: resp.medidasMonitoreo,
            frecuencia: resp.frecuencia,
            acciones: "",
            responsableSecundario: "",
            contingencia: "",
            auxiliar: "",
            idPlanManejo: this.idPGMF,
            responsable: resp.responsable,
            idUsuarioRegistro: this.user.idUsuario
          };
          this.planificacionService
            .registrarAprovechamientoEvalAmbiental(params)
            .subscribe((_res) => this.tablaDinamica.getData());
        }
      });
    } else {
      this.ref.onClose.subscribe((resp: any) => {
        if (resp) {
          const params = {
            idEvalAprovechamiento,
            codTipoAprovechamiento: "POVIGILANCIA",
            tipoAprovechamiento: "POVIGILANCIA",
            tipoNombreAprovechamiento: "Plan Vigilancia",
            impacto: resp.descripcionImpacto,
            medidasControl: resp.medidasControl,
            medidasMonitoreo: resp.medidasMonitoreo,
            frecuencia: resp.frecuencia,
            acciones: "",
            responsableSecundario: "",
            contingencia: "",
            auxiliar: "",
            idPlanManejo: this.idPGMF,
            responsable: resp.responsable,
            idUsuarioRegistro: this.user.idUsuario
          };
          this.planificacionService
            .registrarAprovechamientoEvalAmbiental(params)
            .subscribe((_res) => this.tablaDinamica.getData());
        }
      });
    }
  };

  modalContingencia = (mesaje: string, edit?: any, data?: any) => {
    let idEvalAprovechamiento: number = 0;
    if (edit == "true") {
      idEvalAprovechamiento = data.idEvalAprovechamiento;
    }
    this.ref = this.dialogService.open(ContingenciaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        editar: edit,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        const params = {
          idEvalAprovechamiento,
          tipoAprovechamiento: "CONTIN",
          tipoNombreAprovechamiento: "",
          nombreAprovechamiento: "",
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: resp.accionesRealizar,
          responsable: "",
          responsableSecundario: resp.responsable,
          contingencia: resp.contingencia,
          auxiliar: this.selectAnexo,
          idPlanManejo: this.idPGMF,
          idUsuarioRegistro: this.user.idUsuario
        };

        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(params)
          .subscribe(() => this.getContingencia(edit));
      }
    });
  };

  getContingencia(edit?: any) {
    this.totalRecords3 = 0;
    this.listaContingencia = [];
    const body = { idPlanManejo: this.idPGMF, tipoAprovechamiento: "CONTIN" };

    this.planificacionService.getAprovechamiento(body).subscribe(
      (response: any) => {
        //this.listaContingencia = [...(response.data ? response.data : [])]

        response.data.forEach((element: any) => {
          this.selectAnexo = element.auxiliar;
          this.listaContingencia.push(element);
          this.totalRecords3++;
        });

      });
  }

  eliminarContingencia = (item: any) => {
    this.listaContingencia = this.listaContingencia.filter(
      (x) => x.idContigencia != item.idContigencia
    );
  };

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }


  enviarArchivo() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn('Seleccione un Archivo.');
      return;
    }
    
    this.archivo.nombreActividad = (this.archivo.file as File).name;
    this.archivo.idUsuarioRegistro = this.user.idUsuario;
    this.archivo.idPlanManejo = this.idPGMF;
    this.procesando = true;
    this.guardarArchivo(this.archivo).subscribe(
      () => this.guardarActividadArchivo(this.archivo)
        .pipe(finalize(() => this.procesando = false))
        .subscribe()
    )
  }

  guardar() {
    if (!this.validarNuevo()) {
      return;
    }
    let item = this.listaContingencia.length;
    let obj = this.listaContingencia[item - 1];

    obj.auxiliar = this.selectAnexo;
  
    // console.log("se guardar", obj);
    this.planificacionService
      .registrarAprovechamientoEvalAmbiental(obj)
      .subscribe(() => {
        this.toast.ok('Se registró plan de contingencia ambiental correctamente.\n');
        this.getContingencia()
      });
  }

  cargarIdArchivo(data: any) {
    this.idPlanManejoArchivo = data.idPGMFArchivo;
  }

  anexoN(event: any) {
    if (this.idPlanManejoArchivo != 0) {
      this.eliminarPlanManejo();
    }
  }
  eliminarPlanManejo() {
    var params = {
      idPlanManejoArchivo: this.idPlanManejoArchivo,
      idUsuarioElimina: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .eliminarPlanManejoArchivo(params)
      .subscribe((response: any) => { });
  }
  validarNuevo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectAnexo == "S") {
      if (this.idPlanManejoArchivo == 0) {
        validar = false;
        mensaje = mensaje += "(*) Debe Cargar el archivo requerido.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  guardarActividadArchivo(item: any) {
    return this.planificacionService
      .registrarActividadEvalAmbiental(item)
      .pipe(tap((res: any) => {
        if (res?.success)
          this.toast.ok('success', 'Archivo Guardado')
        else
          this.toast.error('Ocurrió un error.')
      }, _err => this.toast.error('error', 'Ocurrió un error.')))
  }

  guardarArchivo(item: any): Observable<ResponseModel<number>> {
    return this.apiArchivo.cargar(item.idUsuarioRegistro, ArchivoTipo.INFO_GENERAL, item.file)
      .pipe(tap(res => item.tipoNombreActividad = String(res?.data)));
  }

  /*toast(severity: string, detail: string) {
    this.messageService.add({ key: 'toast', severity, detail });
  }*/

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }

  private obtenerUltimaEvaluacion() {
    let params = { idPlanManejo: this.idPGMF }
    this.planManejoEvaluacionService.obtenerUltimaEvaluacion(params).subscribe((result: any) => {

      this.dataLineamientos(result.codigo);
      //this.listarEvaluacionDetalle(result.codigo);
    });
  }


  private listarEvaluacionDetalle(id: any) {
    let params =
    {
      idPlanManejoEval: id
    };
    this.planManejoEvaluacionService.listarPlanManejoEvaluacionDetalle(params).subscribe((result: any) => {

      if (result.isSuccess) {
        this.listarEvaluaciones(result.data);
      }
    })
  }

  btnDescargarFormatoPGA() {
    const NombreGenerado: string = PlantillaConst.PGMF_TAB_7_2

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiArchivo.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
	    if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }

  btnCargarPGA() {
    if (!(this.archivoPGA.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    let item = {
      nombreHoja: "DATOS",
      numeroFila: 2,
      numeroColumna: 1,
      idPlanManejo: this.idPGMF,
      codTipoAprovechamiento: "POAPRO",
      tipoAprovechamiento: "",
      tipoNombreAprovechamiento: "",
      idUsuarioRegistro: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarPlanGestionExcel(this.archivoPGA.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.idPlanManejo, item.codTipoAprovechamiento, item.tipoAprovechamiento, item.tipoNombreAprovechamiento, item.idUsuarioRegistro)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registraron los Planes de Gestión Ambiental correctamente.\n');
          this.getArchivo();
          this.getContingencia();
          this.tablaDinamica.getData();
          this.archivoPGA.file = "";
        } else {
          this.toast.error('Ocurrió un error.');
        }
      });
  }

  btnDescargarFormatoPAP() {
    const NombreGenerado: string = PlantillaConst.PGMF_TAB_7_2_1;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiArchivo.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }
  btnCargarPAP() {
    if (!(this.archivoPAP.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    let item = {
      nombreHoja: "Hoja1",
      numeroFila: 2,
      numeroColumna: 1,
      idPlanManejo: this.idPGMF,
      codTipoAprovechamiento: "POPREVENTIVO",
      tipoAprovechamiento: "POPREVENTIVO",
      tipoNombreAprovechamiento: "Plan Preventivo",
      idUsuarioRegistro: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarPlanAccionExcel(this.archivoPAP.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.idPlanManejo, item.codTipoAprovechamiento, item.tipoAprovechamiento, item.tipoNombreAprovechamiento, item.idUsuarioRegistro)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registraron los Planes de Acción Preventivo-Corrector correctamente.\n');
          this.tablaDinamica.getData();
          this.archivoPAP.file = "";
        } else {
          this.toast.error('Ocurrió un error.');
        }
      });
  }

  btnDescargarFormatoPVS() {
    const NombreGenerado: string = PlantillaConst.PGMF_TAB_7_2_2;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiArchivo.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
	    if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }
  
  btnCargarPVS() {
    if (!(this.archivoPVS.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    let item = {
      nombreHoja: "Hoja1",
      numeroFila: 2,
      numeroColumna: 1,
      idPlanManejo: this.idPGMF,
      codTipoAprovechamiento: "POVIGILANCIA",
      tipoAprovechamiento: "POVIGILANCIA",
      tipoNombreAprovechamiento: "Pre Aprovechamiento",
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarPlanVigilanciaExcel(this.archivoPVS.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.idPlanManejo, item.codTipoAprovechamiento, item.tipoAprovechamiento, item.tipoNombreAprovechamiento, item.idUsuarioRegistro)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registraron los Planes de Vigilancia y Seguimiento correctamente.\n');
          this.tablaDinamica.getData();
          this.archivoPVS.file = "";
        } else {
          this.toast.error('Ocurrió un error.');
        }
      });
  }

  btnDescargarFormatoPCA() {
    const NombreGenerado: string = PlantillaConst.PGMF_TAB_7_2_3

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiArchivo.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }

  btnCargarPCA() {

    if (!(this.archivoPCA.file instanceof File)) {
      this.toast.warn('Seleccione el Formato con los datos ingresados.');
      return;
    }
    let item = {
      nombreHoja: "Hoja1",
      numeroFila: 2,
      numeroColumna: 1,
      idPlanManejo: this.idPGMF,
      codTipoAprovechamiento: "",
      tipoAprovechamiento: "CONTIN",
      tipoNombreAprovechamiento: "Plan Contingencia",
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarPlanContingenciaExcel(this.archivoPCA.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.idPlanManejo, item.codTipoAprovechamiento, item.tipoAprovechamiento, item.tipoNombreAprovechamiento, item.idUsuarioRegistro)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok('Se registraron los Planes de Contingencia Ambiental correctamente.\n');
          this.getContingencia();
          this.tablaDinamica.getData();
          this.archivoPCA.file = "";
        } else {
          this.toast.error('Ocurrió un error.');
        }
      });
  }

  eliminarVig(event: any, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idEvalAprovechamiento: data.idEvalAprovechamiento,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamiento(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.getContingencia();
                this.tablaDinamica.getData();
                this.toast.ok("Se eliminó plan de vigilancia y seguimiento");
              } else {
                this.toast.ok("Ocurrio un Error");
              }
            });
        }

      }
    });
  }

  eliminarAcc(event: any, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idEvalAprovechamiento: data.idEvalAprovechamiento,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamiento(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.getContingencia();
                this.tablaDinamica.getData();
                this.toast.ok("Se Elimino plan de accion correctivo");
              } else {
                this.toast.ok("Ocurrio un Error");
              }
            });
        }

      }
    });
  }

  btnEliminar(event: any, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {

        if (data.idEvalAprovechamiento != 0) {
          var params = {
            idEvalAprovechamiento: data.idEvalAprovechamiento,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService
            .eliminarEvaluacionAmbientalAprovechamiento(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success) {
                this.getContingencia();
                this.tablaDinamica.getData();
                this.toast.ok("Se eliminó la información correctamente.");
              } else {
                this.toast.ok("Ocurrio un Error");
              }
            });
        }

      },
      reject: () => { },
    });
  }

}
