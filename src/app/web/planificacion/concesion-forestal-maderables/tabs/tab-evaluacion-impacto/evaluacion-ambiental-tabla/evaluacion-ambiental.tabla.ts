import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { concatMap, finalize, tap } from "rxjs/operators";

import { AprovechamientoTipo, RespuestaTipo, ToastService } from "@shared";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AprovechamientoResponse } from "src/app/model/evaluacionImpacto";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { ActividadComponent } from "../modal/actividad/actividad.component";
import { AprovechamientoComponent } from "../modal/aprovechamiento/aprovechamiento.component";
import { FactorAmbientalComponent } from "../modal/factor-ambiental/factor-ambiental.component";
import { FactorActividadModel, FactorModel } from "@models";
import { ConfirmationService, MessageService } from "primeng/api";

@Component({
  selector: "evaluacion-ambiental-tabla",
  templateUrl: "./evaluacion-ambiental.tabla.html",
  styleUrls: ["./evaluacion-ambiental.tabla.scss"],
})
export class EvaluacionAmbientalTabla implements OnInit {
  @Input() idPlanManejo: number = 0;
  @Input() disabled: boolean = false;

  idUsuario: number = 1;

  ref!: DynamicDialogRef;

  params: any;

  preAprovechamiento: AprovechamientoResponse[] = [];
  aprovechamiento: AprovechamientoResponse[] = [];
  postAprovechamiento: AprovechamientoResponse[] = [];

  @Output() list = new EventEmitter<any>();

  factores: FactorModel[] = [];

  codTipoAprovechamiento: string = "";
  tipoAprovechamiento: string = "";
  tipoNombreAprovechamiento: string = "";

  actividades: any[] = [];
  emit: any[] = [];

  sizeColActivities = 0;

  constructor(
    private messageService: MessageService,
    public dialogService: DialogService,
    private planificacionService: PlanificacionService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private toast: ToastService,
  ) {
    this.idUsuario = JSON.parse("" + localStorage.getItem("usuario"))
      .idusuario;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.listar()
      .pipe(finalize(() => load.close()))
      .subscribe();
  }

  listar() {
    this.sizeColActivities = 0;
    this.preAprovechamiento = [];
    this.aprovechamiento = [];
    this.postAprovechamiento = [];
    this.factores = [];
    this.actividades = [];

    return this.getActividades().pipe(
      concatMap(() => this.getFactores()),
      concatMap(() => this.getRespuestas())
    );
  }

  getFactores() {
    const body = { idPlanManejo: this.idPlanManejo, tipoActividad: "FACTOR" };
    return this.planificacionService.listarFiltrosEvalAmbientalActividad(body)
      .pipe(tap({
        next: (res: any) => {
          const factores = this.setDatosTabla(res?.data);
          this.factores = factores;
        },
        error: (_err) => console.error(_err),
      }));
  }

  getActividades() {
    const body = { idPlanManejo: this.idPlanManejo, tipoAprovechamiento: "" };
    return this.planificacionService.getActividades(body)
      .pipe(
        finalize(() => this.activitiesColSize()),
        tap({
          next: (res) => this.filtrarTipoAprovechamiento(res.data),
          error: (_err) => console.error(_err),
        })
      );
  }

  getRespuestas() {
    return this.planificacionService
      .listarFactoresActividades(this.idPlanManejo)
      .pipe(
        tap({
          next: (res) => this.responseToBoolean(res?.data),
          error: (_err) => console.error(_err),
        })
      );
  }
  responseToBoolean(data: FactorActividadModel[]) {
    if (data) {
      for (const respuesta of data) {
        const factor = this.factores.find(
          (x) => x.idEvalActividad == respuesta.idEvalActividad
        );

        if (factor == undefined) return;

        const preAprovechamiento = this.existFactor(factor, 'preAprovechamiento');
        const aprovechamiento = this.existFactor(factor, 'aprovechamiento');
        const postAprovechamiento = this.existFactor(factor, 'postAprovechamiento');
        const actividades = preAprovechamiento.concat(aprovechamiento).concat(postAprovechamiento);
        const item = actividades.find(
          (y) => y.idEvalAprovechamiento == respuesta.idEvalAprovechamiento
        ) as AprovechamientoResponse;
        if (item) {
          item.valorEvaluacion = respuesta.valorEvaluacion == RespuestaTipo.SI;
          item.idEvaluacionAmbiental = respuesta.idEvaluacionAmbiental;
        }
      }
    }
  }

  existFactor(factor: FactorModel | undefined, attr: 'preAprovechamiento' | 'aprovechamiento' | 'postAprovechamiento'): AprovechamientoResponse[] {
    if (factor == undefined || factor[attr] == undefined) {
      return [];
    }
    return factor[attr] as AprovechamientoResponse[];
  }

  activitiesColSize() {
    this.sizeColActivities =
      this.preAprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.preAprovechamiento.length;
    this.sizeColActivities =
      this.aprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.aprovechamiento.length;
    this.sizeColActivities =
      this.postAprovechamiento.length == 0
        ? this.sizeColActivities + 1
        : this.sizeColActivities + this.postAprovechamiento.length;
  }

  filtrarTipoAprovechamiento(data: AprovechamientoResponse[]) {
    let actividades: AprovechamientoResponse[] = [];
    if (data == null || data == undefined) return;

    data.forEach((x) => {
      if (x.valorEvaluacion == undefined || x.valorEvaluacion == null)
        x.valorEvaluacion = false;

      this.actividades.push(x);
      actividades.push({ ...x });

      switch (x.tipoAprovechamiento) {
        case AprovechamientoTipo.PRE_APROVECHAMIENTO:
          this.preAprovechamiento.push(x);
          break;
        case AprovechamientoTipo.APROVECHAMIENTO:
          this.aprovechamiento.push(x);
          break;
        case AprovechamientoTipo.POST_APROVECHAMIENTO:
          this.postAprovechamiento.push(x);
          break;
        default:
          break;
      }
    });

    this.addList(actividades);
  }

  addList(value: any) {
    this.list.emit(value);
  }

  setDatosTabla(data: any[]) {
    if (data == null || data == undefined) return [];

    data.forEach((x) => {
      x["preAprovechamiento"] = this.preAprovechamiento.map((o) => ({ ...o }));
      x["aprovechamiento"] = this.aprovechamiento.map((o) => ({ ...o }));
      x["postAprovechamiento"] = this.postAprovechamiento.map((o) => ({
        ...o,
      }));
    });
    return data;
  }

  guardar() {
    const body = this.requestToString(this.idPlanManejo, this.idUsuario);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.registrarRespuestas(body)
      .pipe(concatMap(() => this.listar()))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  requestToString(idPlanManejo: number, idUsuario: number) {
    let body: FactorActividadModel[] = [];
    this.factores.forEach((factor) => {
      const preAprovechamiento = this.existFactor(factor, 'preAprovechamiento');
      const aprovechamiento = this.existFactor(factor, 'aprovechamiento');
      const postAprovechamiento = this.existFactor(factor, 'postAprovechamiento');
      const actividades = factor
        ? preAprovechamiento.concat(aprovechamiento).concat(postAprovechamiento)
        : [];

      actividades.forEach((actividad) => {
        const item = new FactorActividadModel({
          idEvaluacionAmbiental: actividad?.idEvaluacionAmbiental
            ? actividad.idEvaluacionAmbiental
            : 0,
          idEvalAprovechamiento: actividad.idEvalAprovechamiento,
          idEvalActividad: factor.idEvalActividad,
          idPlanManejo,
          valorEvaluacion: actividad.valorEvaluacion
            ? RespuestaTipo.SI
            : RespuestaTipo.NO,
          descripcion: "",
          medidasMitigacion: factor?.medidasMonitoreo
            ? factor.medidasMonitoreo
            : "",
          adjunto: "",
          idUsuarioRegistro: idUsuario,
        });
        body.push(item);
      });

      const medidasMitigacion = new FactorActividadModel({
        idEvaluacionAmbiental: 0,
        idEvalAprovechamiento: 0,
        idEvalActividad: factor.idEvalActividad,
        idPlanManejo,
        valorEvaluacion: "",
        descripcion: "",
        medidasMitigacion: factor?.medidasMonitoreo
          ? factor.medidasMonitoreo
          : "",
        adjunto: "",
        idUsuarioRegistro: idUsuario,
      });
      body.push(medidasMitigacion);
    });
    return body;
  }

  registrarRespuestas(body: FactorActividadModel[]) {
    return this.planificacionService.registrarFactoresActividades(body)
      .pipe(tap(this.handlerResult()));
  }

  modalAprovechamiento = (mensaje: string, item: any, tipo: string) => {
    if (tipo == "Pre Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "PREAPR";
      this.tipoNombreAprovechamiento = "Pre Aprovechamiento";
    }

    if (tipo == "Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "APROVE";
      this.tipoNombreAprovechamiento = "Aprovechamiento";
    }
    if (tipo == "Post Aprovechamiento") {
      this.codTipoAprovechamiento = "POAPRO";
      this.tipoAprovechamiento = "POSTAP";
      this.tipoNombreAprovechamiento = "Post Aprovechamiento";
    }

    this.ref = this.dialogService.open(AprovechamientoComponent, {
      header: mensaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.params = {
          idPlanManejo: this.idPlanManejo,
          idEvalAprovechamiento: 0,
          codTipoAprovechamiento: this.codTipoAprovechamiento,
          tipoAprovechamiento: this.tipoAprovechamiento,
          tipoNombreAprovechamiento: this.tipoNombreAprovechamiento,
          nombreAprovechamiento: resp,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
          impacto: "",
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          responsableSecundario: "",
          contingencia: "",
          auxiliar: "",
        };
        this.planificacionService
          .registrarAprovechamientoEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe(() => this.getData());
      }
    });
  };

  modalFactorAmbiental = () => {
    this.ref = this.dialogService.open(FactorAmbientalComponent, {
      header: "Agregar Factor Ambiental",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.params = {
          idEvalActividad: 0,
          codTipoActividad: "POAPRO",
          tipoActividad: "FACTOR",
          tipoNombreActividad: "Factores Ambientales",
          nombreActividad: resp,
          medidasControl: "",
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };
        this.planificacionService
          .registrarActividadEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe(() => this.getData());
      }
    });
  };

  modalActividad = (data: any) => {
    this.ref = this.dialogService.open(ActividadComponent, {
      header: "Agregar Actividad",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        
        this.params = {
          idEvalActividad: 0,
          codTipoActividad: "POAPRO",
          tipoActividad: "INPAC",
          tipoNombreActividad: "IMPACTO",
          nombreActividad: data.nombreAprovechamiento,
          medidasControl: resp.medidasControl,
          medidasMonitoreo: "",
          frecuencia: "",
          acciones: "",
          responsable: "",
          idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
            .idusuario,
        };
        this.planificacionService
          .registrarActividadEvalAmbiental(this.params)
          .pipe(tap(this.handlerResult()))
          .subscribe();
        this.impactoAmbientales();
      }
    });
  };
  impactoAmbientales() {
    this.params = {
      tipoActividad: "INPAC",
    };
    this.planificacionService
      .listarFiltrosEvalAmbientalActividad(this.params)
      .subscribe((response: any) => (this.factores = response.data));
  }

  eliminarpre(event: any, data: any, numLista: number, index: number){
    

      this.confirmationService.confirm({
        target: event.target || undefined,
        message: '¿Está seguro de eliminar este registro?',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {

          if (data.idEvalAprovechamiento != 0) {
            const params = {
              idEvalAprovechamiento: data.idEvalAprovechamiento,
              idUsuarioElimina:  JSON.parse("" + localStorage.getItem("usuario")).idusuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.planificacionService
              .eliminarEvaluacionAmbientalAprovechamiento(params)
              .pipe(finalize(() => this.dialog.closeAll()))
              .subscribe((response: any) => {
                if (response.success) {
                  this.toast.ok("Se eliminó la información correctamente.");
                  this.sizeColActivities = this.sizeColActivities - 1;
                  switch (numLista) {
                    case 1:
                      this.preAprovechamiento.splice(index, 1);
                      this.factores.forEach(item => {
                        if(item?.preAprovechamiento) item.preAprovechamiento.splice(index, 1);
                      });
                      break;
                    case 2:
                      this.aprovechamiento.splice(index, 1);
                      this.factores.forEach(item => {
                        if(item?.aprovechamiento) item.aprovechamiento.splice(index, 1);
                      });
                      break;
                    case 3:
                      this.postAprovechamiento.splice(index, 1);
                      this.factores.forEach(item => {
                        if(item?.postAprovechamiento) item.postAprovechamiento.splice(index, 1);
                      });
                      break;
                    default:
                      break;
                  }
                  // this.getData();
                } else {
                  this.toast.ok("Ocurrio un Error");
                }
              });
          }

        },
        reject: () => { },
      });
  }

  handlerResult() {
    return {
      next: (res: any) =>
        this.messageService.add({
          severity: "success",
          detail: res?.message,
        }),
      error: (_err: any) => {
        this.messageService.add({
          severity: "warn",
          detail: _err?.message,
        });
        console.error(_err);
      },
    }
  }
}
