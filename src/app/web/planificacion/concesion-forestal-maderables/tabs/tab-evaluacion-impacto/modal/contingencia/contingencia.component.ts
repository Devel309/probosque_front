import { Component, OnInit } from "@angular/core";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-contingencia",
  templateUrl: "./contingencia.component.html",
  styleUrls: ["./contingencia.component.scss"],
})
export class ContingenciaComponent implements OnInit {
  listaContingencia: any[] = [
    { contingencia: "Incendios" },
    {
      contingencia: "Derrame de conbustible  y/o lubricantes",
    },
    {
      contingencia: "Riesgos a la salud",
    },
    { contingencia: "Invasiones" },
  ];

  autoResize: boolean = true;
  isDisabled: boolean = false;
  contingenciaObjt: any = {
    contingencia: "",
    accionesRealizar: "",
    responsable: "",
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  ngOnInit() {
    if (this.config.data.editar == "true") {
      this.contingenciaObjt.contingencia = this.config.data.data.contingencia;
      this.contingenciaObjt.accionesRealizar = this.config.data.data.acciones;
      this.contingenciaObjt.responsable = this.config.data.data.responsableSecundario;
      this.isDisabled = true;
    }
  }
  
  agregar = () => {
    if (this.contingenciaObjt.contingencia === "") {
      this.toast.warn('(*) Debe seleccionar: Contingencia.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Contingencia, es obligatorio",
      // });
    } else if (this.contingenciaObjt.accionesRealizar === "") {
      this.toast.warn('(*) Debe ingresar: Acciones a realizar.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Acciones a realizar, es obligatorio",
      // });
    } else if (this.contingenciaObjt.responsable === "") {
      this.toast.warn('(*) Debe ingresar: Responsable.\n');
      // this.messageService.add({
      //   key: "toast",
      //   severity: "error",
      //   summary: "ERROR",
      //   detail: "Responsable, es obligatorio",
      // });
    } else this.ref.close(this.contingenciaObjt);
  };

  cerrarModal() {
    this.ref.close();
  }
}
