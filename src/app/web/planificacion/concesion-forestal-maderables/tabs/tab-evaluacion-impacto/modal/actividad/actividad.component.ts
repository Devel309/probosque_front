import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-actividad",
  templateUrl: "./actividad.component.html",
  styleUrls: ["./actividad.component.scss"],
})
export class ActividadComponent implements OnInit {
  actividad: any;
  isDisabled: boolean = false;
  autoResize: boolean = true;
  actividadObjt: any = {
    nombreActividad: "",
    descripcionImpacto: "",
    medidasControl: "",
    
  };

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit() {
    if (this.config.data.editar == "true") {
        this.actividadObjt.nombreActividad = this.config.data.data.nombreAprovechamiento;
        this.actividadObjt.descripcionImpacto = this.config.data.data.impacto;
        this.actividadObjt.medidasControl = this.config.data.data.medidasControl;
        this.isDisabled = true;
    }
  }

  agregar = () => {
    if (this.actividadObjt.nombreActividad=== "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Descripción Actividad es obligatorio",
      });
    }

    if (this.actividadObjt.descripcionImpacto === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Descripción de impacto, es obligatorio",
      });
    } else if (this.actividadObjt.medidasControl === "") {
      this.messageService.add({
        key: "toast",
        severity: "error",
        summary: "ERROR",
        detail: "Media de control ambiental, es obligatorio",
      });
    } else this.ref.close(this.actividadObjt);
  };

  cerrarModal() {
    this.ref.close();
  }
}
