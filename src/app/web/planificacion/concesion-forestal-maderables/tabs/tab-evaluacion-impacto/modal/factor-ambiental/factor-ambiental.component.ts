import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-factor-ambiental',
  templateUrl: './factor-ambiental.component.html',
  styleUrls: ['./factor-ambiental.component.scss']
})
export class FactorAmbientalComponent implements OnInit {

  nombreActividad: string=''

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  agregar = () => {
    if (this.nombreActividad === '') {
      this.messageService.add({
        key: 'toast',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Nombre de Actividad, es obligatorio',
      });
    } else this.ref.close(this.nombreActividad);
  };

  cerrarModal() {
    this.ref.close();
  }

}
