import { Component, EventEmitter, OnInit, Output, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, PlanManejoService, UsuarioService } from "@services";
import { DownloadFile, ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoPGMF } from "src/app/model/CodigoPGMF";
import { CodigosManejoBosquePGMF } from "src/app/model/ManejoForestal";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { Mensajes } from "src/app/model/util/Mensajes";
import { TiposArchivos } from "src/app/model/util/TiposArchivos";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { Anexo3, anexo3Detalle } from "../../../plan-general-manejo/tabs/tab-anexos/tab-anexos.component";
@Component({
  selector: "app-tab-anexos",
  templateUrl: "./tab-anexos.component.html",
})
export class TabAnexosComponent implements OnInit {
  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();
  @Input() idPGMF!: number;
  @Input() disabled: boolean = false;
  @Input() codigoEstado!: string;

  @Input() isDisbledObjFormu!: boolean;
  @Input() isShowObjEval!: boolean;
  @Input() isDisabledObjEval!: boolean;

  usuario!: UsuarioModel;
  tiposArchivos = TiposArchivos;
  codigoPGMF: string = 'PGMF';
  // Anexo1
  fileMapa1_1: any = {};
  fileMapa1_2: any = {};
  fileMapa2_1: any = {};
  fileMapa2_2: any = {};
  fileMapa3_1: any = {};
  fileMapa3_2: any = {};
  // Anexo2
  listaEspecies: any[] = [];
  // Anexo3
  comboBosque: any[] = [];
  selectCodigoBosqueVP: any = 0;
  selectCodigoBosqueFU: any = 0;
  variablesVP: any[] = [{ var: "Arb/ha" }, { var: "AB/HA" }, { var: "Vol/ha" }];
  variablesFU: any[] = [{ var: "Arb/ha" }, { var: "AB/ha" }];
  listaInvExploraVP: any = [];
  listaInvExploraFU: any = [];
  // Anexo4
  listaAnexo4: any = [];

  constructor(
    public dialog: MatDialog,
    private toast: ToastService,
    private archivoServ: ArchivoService,
    private usuarioServ: UsuarioService,
    private planManejoService: PlanManejoService,
    private anexosService: AnexosService,
    private postulacionPfdmService: PostulacionPFDMService,
    private manejoBosqueService: ManejoBosqueService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarArchivosAnexo1();
    this.listarAnexo2();
    this.listarTipoBosque();
    this.listarArchivosAnexo4();
  }
  /* ANEXO1 */
  listarArchivosAnexo1() {
    let params = { idPlanManejo: this.idPGMF, subCodigoProceso: CodigoPGMF.ACORDEON_14_1 };

    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.listarPorFiltroPlanManejoArchivo(params).subscribe((result: any) => {
      load.close();
      if (result.success && result.data.length > 0) {
        result.data.forEach((item: any) => {
          this.setIInfoArchivo(item.idTipoDocumento, item);
        });
      }
    }, (error) => {
      this.errorMensaje(error);
      load.close()
    });
  }

  registrarArchivoAdjunto(file: any, codigo: string) {
    let params = {
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.usuario.idusuario,
      codTipo: CodigoPGMF.TAB_14,
      subCodTipo: CodigoPGMF.ACORDEON_14_1,
      codTipoDocumento: codigo,
      idPlanManejoArchivo: file.idPlanManejoArchivo || 0
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.regitrarArchivoPlanManejo(params, file.file).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        this.setIdArchivoRegistrado(codigo, result.data);
      } else {
        this.toast.warn(result.message);
        this.limpiarArchivoFile(codigo);
      }
    }, (error) => {
      this.limpiarArchivoFile(codigo);
      this.errorMensaje(error, true);
    });
  }

  eliminarArchivoAdjunto(file: any, codigo: string) {
    let params = {
      idPlanManejoArchivo: file.idPlanManejoArchivo,
      idUsuarioElimina: this.usuario.idusuario
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.eliminarPlanManejoArchivo(params).subscribe((result: any) => {
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
        this.limpiarArchivoFile(codigo);
        this.eliminarArchivo(file.idArchivo);
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarArchivo(idAdjuntoOut: number) {
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idusuario).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  setIInfoArchivo(codigoIn: string, datos: any) {
    switch (codigoIn) {
      case TiposArchivos.TDOCSFMAPGE: this.fileMapa1_1 = datos; break;
      case TiposArchivos.TDOCEVIMAPGE: this.fileMapa1_2 = datos; break;
      case TiposArchivos.TDOCSFMAPORDF: this.fileMapa2_1 = datos; break;
      case TiposArchivos.TDOCEVIMAPORDF: this.fileMapa2_2 = datos; break;
      case TiposArchivos.TDOCSFMAPDIVADM: this.fileMapa3_1 = datos; break;
      case TiposArchivos.TDOCEVIMAPDIVADM: this.fileMapa3_2 = datos; break;
      default: break;
    }
  }

  setIdArchivoRegistrado(codigoIn: string, datos: any) {
    switch (codigoIn) {
      case TiposArchivos.TDOCSFMAPGE: this.fileMapa1_1.idArchivo = datos.idArchivo; this.fileMapa1_1.idPlanManejoArchivo = datos.idPGMFArchivo; break;
      case TiposArchivos.TDOCEVIMAPGE: this.fileMapa1_2.idArchivo = datos.idArchivo; this.fileMapa1_2.idPlanManejoArchivo = datos.idPGMFArchivo; break;
      case TiposArchivos.TDOCSFMAPORDF: this.fileMapa2_1.idArchivo = datos.idArchivo; this.fileMapa2_1.idPlanManejoArchivo = datos.idPGMFArchivo; break;
      case TiposArchivos.TDOCEVIMAPORDF: this.fileMapa2_2.idArchivo = datos.idArchivo; this.fileMapa2_2.idPlanManejoArchivo = datos.idPGMFArchivo; break;
      case TiposArchivos.TDOCSFMAPDIVADM: this.fileMapa3_1.idArchivo = datos.idArchivo; this.fileMapa3_1.idPlanManejoArchivo = datos.idPGMFArchivo; break;
      case TiposArchivos.TDOCEVIMAPDIVADM: this.fileMapa3_2.idArchivo = datos.idArchivo; this.fileMapa3_2.idPlanManejoArchivo = datos.idPGMFArchivo; break;
      default: break;
    }
  }

  limpiarArchivoFile(codigoIn: string) {
    switch (codigoIn) {
      case TiposArchivos.TDOCSFMAPGE: this.fileMapa1_1 = {}; break;
      case TiposArchivos.TDOCEVIMAPGE: this.fileMapa1_2 = {}; break;
      case TiposArchivos.TDOCSFMAPORDF: this.fileMapa2_1 = {}; break;
      case TiposArchivos.TDOCEVIMAPORDF: this.fileMapa2_2 = {}; break;
      case TiposArchivos.TDOCSFMAPDIVADM: this.fileMapa3_1 = {}; break;
      case TiposArchivos.TDOCEVIMAPDIVADM: this.fileMapa3_2 = {}; break;
      default: break;
    }
  }

  /* ANEXO2 */
  listarAnexo2() {
    var params = {
      idPlanManejo: this.idPGMF,
      codigoGeneral: 'PGMF',
      subCodigoGeneral: CodigosManejoBosquePGMF.ACORDEON_6_4,
    };
    this.manejoBosqueService.listarManejoBosque(params).subscribe((response: any) => {
      if (response.success && response.data && response.data.length > 0 && response.data[0].listManejoBosqueDetalle) {
        this.listaEspecies = response.data[0].listManejoBosqueDetalle;
      }
    });
  }

  /* ANEXO3 */
  listarTipoBosque() {
    this.comboBosque = [];
    const params = {
      idPlanDeManejo: this.idPGMF,
      tipoPlan: 'PGMF',
    };
    this.planManejoService.listarPorPlanManejoTipoBosque(params).subscribe((response: any) => {
      this.comboBosque = [...response.data];
    });
  }

  listarVolPotencial(codigoBosqueOut: number) {
    this.listaInvExploraVP = [];
    const params = {
      idPlanDeManejo: this.idPGMF,
      idTipoBosque: codigoBosqueOut,
      tipoPlan: 'PGMF',
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService.pgmfInventarioExploracionVolumenPotencial(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.data && response.data.length > 0) {
        response.data.forEach((element: any) => {
          let obj = new Anexo3VP();
          obj.nombreComun = element.nombreComun;
          this.variablesVP.forEach((item: any, i: number) => {
            var ob1 = new ARB311();
            var ob2 = new ARB311();
            var ob3 = new ARB311();
            if (item.var == "Arb/ha") {
              ob1.var = "Arb/ha";
              ob1.Dap20a29 = element.arbHaDap20a29;
              ob1.Dap30a39 = element.arbHaDap30a39;
              ob1.Dap40a49 = element.arbHaDap40a49;
              ob1.Dap50a59 = element.arbHaDap50a59;
              ob1.Dap60a69 = element.arbHaDap60a69;
              ob1.Dap70aMas = element.arbHaDap70aMas;
              ob1.TotalTipoBosque = element.arbHaTotalTipoBosque;
              ob1.TotalPorArea = element.arbHaTotalPorArea;
              obj.array.push(ob1);
            } else if (item.var == "AB/HA") {
              ob2.var = "AB/HA";
              ob2.Dap20a29 = element.abHaDap20a29;
              ob2.Dap30a39 = element.abHaDap30a39;
              ob2.Dap40a49 = element.abHaDap40a49;
              ob2.Dap50a59 = element.abHaDap50a59;
              ob2.Dap60a69 = element.abHaDap60a69;
              ob2.Dap70aMas = element.arbHaDap70aMas;
              ob2.TotalTipoBosque = element.abHaTotalTipoBosque;
              ob2.TotalPorArea = element.abHaTotalPorArea;
              obj.array.push(ob2);
            } else if (item.var == "Vol/ha") {
              ob3.var = "Vol/ha";
              ob3.Dap20a29 = element.volHaDap20a29;
              ob3.Dap30a39 = element.volHaDap30a39;
              ob3.Dap40a49 = element.volHaDap40a49;
              ob3.Dap50a59 = element.volHaDap50a59;
              ob3.Dap60a69 = element.volHaDap60a69;
              ob3.Dap70aMas = element.volHaDap70aMas;
              ob3.TotalTipoBosque = element.volHaTotalTipoBosque;
              ob3.TotalPorArea = element.volHaTotalPorArea;
              obj.array.push(ob3);
            }
          });
          this.listaInvExploraVP.push(obj);
        });
      } else {
        this.toast.warn(response.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarFustal(codigoBosqueOut: number) {
    this.listaInvExploraFU = [];
    const params = {
      idPlanDeManejo: this.idPGMF,
      idTipoBosque: codigoBosqueOut,
      tipoPlan: 'PGMF',
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService.PGMFInventarioExploracionFustales(params).subscribe((response: any) => {
      this.dialog.closeAll();
      // let aux = {...response.data[0]};
      // response.data.push(aux);
      if (response.data && response.data.length > 0) {
        response.data.forEach((element: any, index: number) => {
          var obj = new Anexo3();
          obj.nombreComun = element.nombreComun;
          this.variablesFU.forEach((item: any, i: number) => {
            var ob1 = new anexo3Detalle();
            var ob2 = new anexo3Detalle();
            if (item.var == "Arb/ha") {
              ob1.var = "Arb/ha";
              ob1.Dap10a19 = element.arbHaDap10a19;
              ob1.Dap20a29 = element.arbHaDap20a29;
              ob1.TotalTipoBosque = element.arbHaTotalTipoBosque;
              ob1.TotalPorArea = element.arbHaTotalPorArea;
              obj.array.push(ob1);
            } else if (item.var == "AB/ha") {
              ob2.var = "AB/ha";
              ob2.Dap10a19 = element.abHaDap10a19;
              ob2.Dap20a29 = element.abHaDap20a29;
              ob2.TotalTipoBosque = element.arbHaTotalTipoBosque;
              ob2.TotalPorArea = element.abHaTotalTipoBosque;
              obj.array.push(ob2);
            }
          });
          this.listaInvExploraFU.push(obj);
        });
      } else {
        this.toast.warn(response.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  changeCodigoBosqueVP(event: any) {
    this.listarVolPotencial(this.selectCodigoBosqueVP);
  }

  changeCodigoBosqueFU(event: any) {
    this.listarFustal(this.selectCodigoBosqueFU);
  }

  btnLimpiarVP() {
    this.selectCodigoBosqueVP = null;
    this.listaInvExploraVP = [];
  }

  btnLimpiarFU() {
    this.selectCodigoBosqueFU = null;
    this.listaInvExploraFU = [];
  }

  /* ANEXO4 */
  auxTipoDoc = CodigoPGMF.ACORDEON_2_2 + "," + CodigoPGMF.ACORDEON_3_2_TAB_2 + "," + CodigoPGMF.ACORDEON_3_3_TAB_3 + "," + CodigoPGMF.ACORDEON_3_3_TAB_4
    + "," + CodigoPGMF.ACORDEON_3_4_TAB_1 + "," + CodigoPGMF.ACORDEON_3_4_TAB_2 + "," + CodigoPGMF.ACORDEON_3_5_TAB_1 + "," + CodigoPGMF.ACORDEON_3_5_TAB_2 + "," + CodigoPGMF.ACORDEON_3_6_TAB_1 + "," + CodigoPGMF.ACORDEON_3_6_TAB_2
    + "," + CodigoPGMF.ACORDEON_4_1 + "," + CodigoPGMF.ACORDEON_4_2_TAB_1 + "," + CodigoPGMF.ACORDEON_4_2_TAB_2 + "," + CodigoPGMF.ACORDEON_5_1_TAB_2 + "," + CodigoPGMF.ACORDEON_5_3 + "," + CodigoPGMF.ACORDEON_5_4
    + "," + CodigoPGMF.ACORDEON_6_2 + "," + CodigoPGMF.ACORDEON_6_3 + "," + CodigoPGMF.ACORDEON_6_4 + "," + CodigoPGMF.ACORDEON_6_5 + "," + CodigoPGMF.ACORDEON_6_6_TAB_2 + "," + CodigoPGMF.ACORDEON_6_8 + "," + CodigoPGMF.ACORDEON_6_9_TAB_2 + "," + CodigoPGMF.ACORDEON_6_9_TAB_3
    + "," + CodigoPGMF.TAB_8 + "," + CodigoPGMF.TAB_10 + "," + CodigoPGMF.TAB_11;
  listarArchivosAnexo4() {
    let params = { idPlanManejo: this.idPGMF, codigoTipo: CodigoPGMF.CODIGO_PROCESO, tipoDocumento: this.auxTipoDoc };

    let load = this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService.listarPlanManejoAnexoArchivo(params).subscribe((result: any) => {
      load.close()
      if (result.success && result.data.length > 0) {
        this.listaAnexo4 = result.data;
      }
    }, (error) => {
      this.errorMensaje(error);
      load.close();
    });
  }

  btnDescargar(idArchivo: any) {
    const params = { idArchivo: idArchivo };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data) {
        DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnGrabar(fila: any) {
    if (!this.validarGrabar(fila)) return;
    this.grabarService(fila);
  }

  grabarService(data: any) {
    var params = {
      idPGMFArchivo: data.idPlanManejoArchivo,
      codigoTipoPGMF: data.codigoTipo,
      descripcion: data.descripcion,
      idPlanManejo: data.idPlanManejo,
      idArchivo: data.idArchivo,
      idUsuarioRegistro: this.usuario.idusuario,
      asunto: data.asunto,
      acapite: data.acapite,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.postulacionPfdmService.registrarArchivoDetalle(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success == true) {
        this.toast.ok("Se actualizó el registro correctamente.");
        // this.obtenerCheckAnexos3();
      } else {
        this.toast.error(response?.message);
      }
    }, (error: any) => this.errorMensaje(error, true));
  }

  validarGrabar(fila: any): boolean {
    let validado = true;
    if (!fila.asunto) {
      validado = false;
      this.toast.warn('(*) Debe ingresar un asunto');
    }
    if (!fila.acapite) {
      validado = false;
      this.toast.warn('(*) Debe ingresar un acápite');
    }
    return validado;
  }

  /* ***************************** */
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}

export class Anexo3VP {
  constructor(data?: any) {
    if (data) {
      this.nombreComun = data.nombreComun;
      return;
    }
  }
  nombreComun: string = "";
  array: ARB311[] = [];
}
export class ARB311 {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = "";
  Dap20a29: string = "";
  Dap30a39: string = "";
  Dap40a49: string = "";
  Dap50a59: string = "";
  Dap60a69: string = "";
  Dap70aMas: string = "";
  TotalTipoBosque: string = "";
  TotalPorArea: string = "";
}
