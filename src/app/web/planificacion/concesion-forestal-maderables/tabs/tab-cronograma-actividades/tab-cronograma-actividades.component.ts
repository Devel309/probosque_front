import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ParametroValorService, UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ModalFormularioCronogramaComponent } from "./modal-formulario-cronograma/modal-formulario-cronograma.component";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";


@Component({
  selector: "app-tab-cronograma-actividades",
  templateUrl: "./tab-cronograma-actividades.component.html",
  styleUrls: ["./tab-cronograma-actividades.component.scss"],
})
export class TabCronogramaActividadesComponent implements OnInit {
  @Input() idPGMF!: number;
  @Input() isDisbledObjFormu: boolean = false;
  @Input() isShowObjEval!: boolean;
	@Input() isDisabledObjEval!: boolean;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  lineamiento: LineamientoInnerModel = new LineamientoInnerModel();

  ref!: DynamicDialogRef;
  context: Modelo = new Modelo();

  verAnio1: boolean = false;
  verAnio2: boolean = false;
  verAnio3: boolean = false;

  lstDetalle: Modelo[] = [];
  lstDetalle2: Modelo[] = [];

  lstOtrosAnos: any[] = [];
  params: any;
  vigencia: Number = 0;

  cmbAnios: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [{ label: "Año 2", value: "2-0" }],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [{ label: "Año 3", value: "3-0" }],
    },
    {
      label: "AÑO 4",
      value: "4",
      items: [{ label: "Año 4", value: "4-0" }],
    },
    {
      label: "AÑO 5",
      value: "5",
      items: [{ label: "Año 5", value: "5-0" }],
    },
    {
      label: "AÑO 6",
      value: "6",
      items: [{ label: "Año 6", value: "6-0" }],
    },
    {
      label: "AÑO 7",
      value: "7",
      items: [{ label: "Año 7", value: "7-0" }],
    },
    {
      label: "AÑO 8",
      value: "8",
      items: [{ label: "Año 8", value: "8-0" }],
    },
    {
      label: "AÑO 9",
      value: "9",
      items: [{ label: "Año 9", value: "9-0" }],
    },
    {
      label: "AÑO 10",
      value: "10",
      items: [{ label: "Año 10", value: "10-0" }],
    },
    {
      label: "AÑO 11",
      value: "11",
      items: [{ label: "Año 11", value: "11-0" }],
    },
    {
      label: "AÑO 12",
      value: "12",
      items: [{ label: "Año 12", value: "12-0" }],
    },
    {
      label: "AÑO 13",
      value: "13",
      items: [{ label: "Año 3", value: "13-0" }],
    },
    {
      label: "AÑO 14",
      value: "14",
      items: [{ label: "Año 14", value: "14-0" }],
    },
    {
      label: "AÑO 15",
      value: "15",
      items: [{ label: "Año 15", value: "15-0" }],
    },
    {
      label: "AÑO 16",
      value: "16",
      items: [{ label: "Año 16", value: "16-0" }],
    },
    {
      label: "AÑO 17",
      value: "17",
      items: [{ label: "Año 17", value: "17-0" }],
    },
    {
      label: "AÑO 18",
      value: "18",
      items: [{ label: "Año 18", value: "18-0" }],
    },
    {
      label: "AÑO 19",
      value: "19",
      items: [{ label: "Año 19", value: "19-0" }],
    },
    {
      label: "AÑO 20",
      value: "20",
      items: [{ label: "Año 20", value: "20-0" }],
    },
  ];


  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private parametroValorService: ParametroValorService
  ) {}

  ngOnInit(): void {
    this.listarCronogramaActividad();
  }

  listarEvaluaciones(data: any) {
    this.ordenarLista(data, 'PGMFEVALLIN18', this.lineamiento);
  }

  dataLineamientos(data: any) {
    this.lineamiento = new LineamientoInnerModel(
      { idPlanManejoEvaluacion: data, codigoTipo: 'PGMFEVALLIN18' }
    );
  }

  private ordenarLista(lista: any[], codigo: string, object: any) {
    lista.forEach(element => {
      if (element.codigoTipo !== undefined && element.codigoTipo !== null && element.codigoTipo === codigo) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.observacion = element.observacion;
        object.archivo = { idArchivo: element.idArchivo, nombre: element.descArchivo };
      }
    });
  }

  listarCronogramaActividad() {
    this.lstDetalle = [];
    this.lstDetalle2 = [];
    var params = {
      codigoProceso: "PGMF",
      idCronogramaActividad: null,
      idPlanManejo: this.idPGMF,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => {
        this.dialog.closeAll();
        
      }
      ))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((x: any) => {
            if(x.codigoActividad== "PLANIFICACION"){
              this.lstDetalle.push(x);
            }
            if(x.codigoActividad== "APROVECHAMIENTO"){
              this.lstDetalle2.push(x);
            }

          });
          this.marcar();
        }else{
          this.cargarDataInicial();
        }
      });
  }

  marcar() {
    var aniosMeses: any[] = [];

    this.lstDetalle.forEach((x: any, index) => {
      
      
      aniosMeses = [];

      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });

      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        codigoActividad: x.codigoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstDetalle[index] = this.context;
    });


    this.lstDetalle2.forEach((x: any, index) => {
      
      aniosMeses = [];

      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });

      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        codigoActividad: x.codigoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstDetalle2[index] = this.context;
    });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any, mostrarMensaje?:boolean) {
    
    mostrarMensaje=mostrarMensaje==null?true:mostrarMensaje;
    
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPGMF,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: resp.codigoActividad,
      codigoProceso: "PGMF",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          if(mostrarMensaje){
            this.messageService.add({
              key: "tl",
              severity: "success",
              detail: "Se actualizó el cronograma de actividades correctamente.",
            });
          }

          this.listarCronogramaActividad();
        } else {
          if(mostrarMensaje){
            this.messageService.add({
              key: "tl",
              severity: "error",
              summary: "ERROR",
              detail: "Ocurrió un problema, intente nuevamente.",
            });
          }

        }
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó la actividad del cronograma correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    this.ref = this.dialogService.open(ModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      this.lstDetalle.push(this.context);
    }
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail:
                    "Se eliminó la actividad del cronograma correctamente.",
                });
                this.listarCronogramaActividad();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  detail: "Ocurrió un problema, intente nuevamente.",
                });
              }
            });
        } else {
          this.lstDetalle.splice(
            this.lstDetalle.findIndex((x) => x.id == data.id),
            1
          );
        }
      },
      reject: () => {},
    });
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

cargarDataInicial(){

if(this.lstDetalle.length==0 && this.lstDetalle2.length==0){
  var params = {
    prefijo: "CRONACT",
  };
  this.parametroValorService
    .listarParametroPorPrefijo(params)
    .pipe(finalize(() => {}))
    .subscribe((result: any) => {

      if (result.success) {

        let numreg=0;
        result.data.forEach((element: any) => {

          let params = {
            actividad: element.valorPrimario,
            anio: null,
            idPlanManejo: this.idPGMF,
            idUsuarioRegistro: this.user.idUsuario,
            importe: null,
            codigoActividad: element.valorSecundario,
            codigoProceso: "PGMF",
          };
          this.cronogrmaActividadesService
            .registrarCronogramaActividades(params)
            .subscribe((response: any) => {
              numreg++;
              if(numreg==result.data.length){
                this.listarCronogramaActividad();
              }

            });



        });
        
      }
     
    });

}





 
}

}
export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.codigoActividad = data.codigoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.codigoActividad = data.codigoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.codigoActividad = data.codigoActividad);
      return;
    }
  }

  showRemove: boolean = true;
  codigoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m1: boolean = false;
  a1m2: boolean = false;
  a1m3: boolean = false;
  a1m4: boolean = false;
  a1m5: boolean = false;
  a1m6: boolean = false;
  a1m7: boolean = false;
  a1m8: boolean = false;
  a1m9: boolean = false;
  a1m10: boolean = false;
  a1m11: boolean = false;
  a1m12: boolean = false;

  a2m1: boolean = false;
  a3m1: boolean = false;
  a4m1: boolean = false;
  a5m1: boolean = false;
  a6m1: boolean = false;
  a7m1: boolean = false;
  a8m1: boolean = false;
  a9m1: boolean = false;
  a10m1: boolean = false;
  a11m1: boolean = false;
  a12m1: boolean = false;
  a13m1: boolean = false;
  a14m1: boolean = false;
  a15m1: boolean = false;
  a16m1: boolean = false;
  a17m1: boolean = false;
  a18m1: boolean = false;
  a19m1: boolean = false;
  a20m1: boolean = false;


  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-1") {
        this.a1m1 = x.aniosMeses.indexOf("1-1") > -1;
      } else if (x.aniosMeses == "1-2") {
        this.a1m2 = x.aniosMeses.indexOf("1-2") > -1;
      } else if (x.aniosMeses == "1-3") {
        this.a1m3 = x.aniosMeses.indexOf("1-3") > -1;
      } else if (x.aniosMeses == "1-4") {
        this.a1m4 = x.aniosMeses.indexOf("1-4") > -1;
      } else if (x.aniosMeses == "1-5") {
        this.a1m5 = x.aniosMeses.indexOf("1-5") > -1;
      } else if (x.aniosMeses == "1-6") {
        this.a1m6 = x.aniosMeses.indexOf("1-6") > -1;
      } else if (x.aniosMeses == "1-7") {
        this.a1m7 = x.aniosMeses.indexOf("1-7") > -1;
      } else if (x.aniosMeses == "1-8") {
        this.a1m8 = x.aniosMeses.indexOf("1-8") > -1;
      } else if (x.aniosMeses == "1-9") {
        this.a1m9 = x.aniosMeses.indexOf("1-9") > -1;
      } else if (x.aniosMeses == "1-10") {
        this.a1m10 = x.aniosMeses.indexOf("1-10") > -1;
      } else if (x.aniosMeses == "1-11") {
        this.a1m11 = x.aniosMeses.indexOf("1-11") > -1;
      } else if (x.aniosMeses == "1-12") {
        this.a1m12 = x.aniosMeses.indexOf("1-12") > -1;
      } else if (x.aniosMeses == "2-0") {
        this.a2m1 = x.aniosMeses.indexOf("2-0") > -1;
      } else if (x.aniosMeses == "3-0") {
        this.a3m1 = x.aniosMeses.indexOf("3-0") > -1;
      } else if (x.aniosMeses == "4-0") {
        this.a4m1 = x.aniosMeses.indexOf("4-0") > -1;
      } else if (x.aniosMeses == "5-0") {
        this.a5m1 = x.aniosMeses.indexOf("5-0") > -1;
      } else if (x.aniosMeses == "6-0") {
        this.a6m1 = x.aniosMeses.indexOf("6-0") > -1;
      } else if (x.aniosMeses == "7-0") {
        this.a7m1 = x.aniosMeses.indexOf("7-0") > -1;
      } else if (x.aniosMeses == "8-0") {
        this.a8m1 = x.aniosMeses.indexOf("8-0") > -1;
      } else if (x.aniosMeses == "9-0") {
        this.a9m1 = x.aniosMeses.indexOf("9-0") > -1;
      } else if (x.aniosMeses == "10-0") {
        this.a10m1 = x.aniosMeses.indexOf("10-0") > -1;
      } else if (x.aniosMeses == "11-0") {
        this.a11m1 = x.aniosMeses.indexOf("11-0") > -1;
      } else if (x.aniosMeses == "12-0") {
        this.a12m1 = x.aniosMeses.indexOf("12-0") > -1;
      } else if (x.aniosMeses == "13-0") {
        this.a13m1 = x.aniosMeses.indexOf("13-0") > -1;
      } else if (x.aniosMeses == "14-0") {
        this.a14m1 = x.aniosMeses.indexOf("14-0") > -1;
      } else if (x.aniosMeses == "15-0") {
        this.a15m1 = x.aniosMeses.indexOf("15-0") > -1;
      } else if (x.aniosMeses == "16-0") {
        this.a16m1 = x.aniosMeses.indexOf("16-0") > -1;
      } else if (x.aniosMeses == "17-0") {
        this.a17m1 = x.aniosMeses.indexOf("17-0") > -1;
      } else if (x.aniosMeses == "18-0") {
        this.a18m1 = x.aniosMeses.indexOf("18-0") > -1;
      } else if (x.aniosMeses == "19-0") {
        this.a19m1 = x.aniosMeses.indexOf("19-0") > -1;
      } else if (x.aniosMeses == "20-0") {
        this.a20m1 = x.aniosMeses.indexOf("20-0") > -1;
      }
      
    });
  }
}
