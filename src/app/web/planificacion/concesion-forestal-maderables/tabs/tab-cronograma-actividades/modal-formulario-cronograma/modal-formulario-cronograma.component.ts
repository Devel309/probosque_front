import { Component, OnInit } from "@angular/core";
import { UsuarioService } from "@services";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";

@Component({
  selector: "app-modal-formulario-cronograma",
  templateUrl: "./modal-formulario-cronograma.component.html",
  styleUrls: ["./modal-formulario-cronograma.component.scss"],
})
export class ModalFormularioCronogramaComponent implements OnInit {
  context: any = {};
  cmbAnios: any = [];
  isDisabled = false;
  fecha: any[] = [];
  edit: any[] = [];

  listaFase: any = [
    {label:"Planificación",value: "PLANIFICACION"},
    {label:"Aprovechamiento",value: "APROVECHAMIENTO"}
  ];



  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private messageService: MessageService,
  ) {}

  ngOnInit(): void {
    
    
    if (this.config.data.type == "E") {
      this.cmbAnios = this.config.data.cmbAnios;
      this.context = this.config.data.data;
      this.config.data.data.aniosMeses.forEach((element: any) => {
        this.fecha.push(element.aniosMeses);
      });
    } else {
      this.isDisabled = true;
    }
  }

  onChange(event: any) {
    if (event.itemValue != undefined) {
      this.context.aniosMeses.forEach((element: any, index: number) => {
        if (
          event.itemValue != undefined &&
          element.aniosMeses == event.itemValue
        ) {
          var params = {
            idCronogramaActividadDetalle: element.id,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividadDetalle(params)
            .subscribe((response) => {
              console.log(response);
            });

          this.context.aniosMeses.splice(index, 1);
        }
      });
    } else if (event.itemValue == undefined) {
      var array = [];
      for (var i = 0; i < this.context.aniosMeses.length; i++) {
        var igual = false;
        for (var j = 0; j < event.value.length && !igual; j++) {
          if (this.context.aniosMeses[i].aniosMeses == event.value[j])
            igual = true;
        }
        if (!igual) array.push(this.context.aniosMeses[i]);
      }

      array.forEach((response: any, index: number) => {
        var params = {
          idCronogramaActividadDetalle: response.id,
          idUsuarioElimina: this.user.idUsuario,
        };

        this.cronogrmaActividadesService
          .eliminarCronogramaActividadDetalle(params)
          .subscribe((response) => {
            console.log(response);
          });
      });
    }
  }

  eli(event: any) {
    console.log(event);
  }

  agregar = () => {

    if (this.config.data.type == "C") {

      if(!this.context.codigoActividad){

        this.messageService.add({
          key: "tml",
          severity: "warn",
          summary: "",
          detail: "El campo fase es obligatorio.",
        });
  
        return;
      }

      if(!this.context.actividad){

        this.messageService.add({
          key: "tml",
          severity: "warn",
          summary: "",
          detail: "El campo actividad es obligatorio.",
        });
  
        return;
      }

      this.ref.close(this.context);
    } else if (this.config.data.type == "E") {
      if (this.context.aniosMeses.length == 0) {
        this.context.aniosMeses = this.fecha;
        this.ref.close(this.context);
      } else if (this.context.aniosMeses.length != 0) {
        this.context.aniosMeses.forEach((element: any) => {
          this.fecha.forEach((x: any, index: number) => {
            if (element.aniosMeses == x) {
              this.fecha.splice(index, 1);
            }
          });
        });
        this.context.aniosMeses = this.fecha;
        if (this.fecha.length != 0) {
          this.ref.close(this.context);
        } else {
          this.ref.close((this.context = null));
        }
      }
    }
  };

  cerrarModal() {
    this.ref.close();
  }
}
