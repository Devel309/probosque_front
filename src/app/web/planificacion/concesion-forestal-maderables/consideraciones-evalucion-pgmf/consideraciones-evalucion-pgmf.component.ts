import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, PlanManejoService, UsuarioService } from '@services';
import { DowloadFileLocal, DownloadFile, ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { GenericoService } from 'src/app/service/generico.service';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ListarPlanManejoEvaluacionRequest } from 'src/app/model/plan-manejo-evaluacion';
import { CargaEnvioDocumentacionService } from 'src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service';
import { finalize } from 'rxjs/operators';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { environment } from '@env/environment';

@Component({
  selector: 'app-consideraciones-evalucion-pgmf',
  templateUrl: './consideraciones-evalucion-pgmf.component.html',
  styleUrls: ['./consideraciones-evalucion-pgmf.component.scss'],
})
export class ConsideracionesEvalucionPgmfComponent implements OnInit {
  consideracionA: any = {};
  consideracionB: any = {};
  consideracionC: any = {};
  consideracionD: any = {};
  consideracionE: any = {};
  consideracionF: any = {};
  consideracionG: any = {};
  consideracionH: any = { lstPlanManejoEvaluacionSubDetalle: [] };
  consideracionI: any = {};

  lstConsEspecial: any[] = [];
  lstEspciePresente: any[] = [];
  fileList: any[] = [];

  showComboList = true;
  idPlanManejo!: number;
  varUrl = `${environment.varUrl}`;
  informacion: any = { idPlanManejo: 0, idPlanManejoEval: 0 };
  mostrarRequisitoModal = false;

  listaTipoDocumento: any[] = [];
  archivoObtenido: any = {};
  usuario = {} as UsuarioModel;

  visibleTab: boolean = true;
  disabled: boolean = true;

  listarPlanManejoEvaluacionRequest: ListarPlanManejoEvaluacionRequest =
    new ListarPlanManejoEvaluacionRequest();

  disabledSend: boolean = true;

  botonLineamientos: boolean = false;

  guardarArchivoDeshabilitado: boolean = true;

  guardarConsideracionDeshabilitado: boolean = true;

  verEnviar: boolean = true;

  disabledRadio: boolean = true;

  minDate!: Date;

  idArchivoB: number = 0;

  constructor(
    private planManejoService: PlanManejoService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private archivoService: ArchivoService,
    private genericoService: GenericoService,
    private dialog: MatDialog,
    private messageService: MessageService,
    private usuarioSrv: UsuarioService,
    private router: Router,
    private route: ActivatedRoute,
    private apiForestalService: ApiForestalService,
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private toast: ToastService
  ) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.usuario = this.usuarioSrv.usuario;
    this.minDate = new Date();

    // this.route.paramMap.subscribe((params) => {
    if (this.idPlanManejo) {
      this.listarArchivosPlan();
      this.obtenerInformacionEvaluacion(
        Number(this.route.snapshot.paramMap.get('idPlan'))
      );
      this.informacion.idPlanManejoEval = Number(
        this.route.snapshot.paramMap.get('idPlan')
      );
    } else {
      this.listarArchivosPlan();
      this.listarTipoDocumento();
      this.listarEvaluacion();
      this.obtenerContrato(this.informacion.idPlanManejo);
    }
    // });
  }

  listarArchivoFirmado() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaEnvioDocumentacionService
      .listarArchivosPGMF(this.informacion.idPlanManejo, 'PGMF')
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess) {
          if (res.data && res.data.length > 0) {
            let auxArchivo = res.data.find(
              (x: any) => x.tipoDocumento == 'PGMFCONS'
            );
            if (auxArchivo) this.idArchivoB = auxArchivo.idArchivo;
          }
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  obtenerInformacionEvaluacion(idPlanManejoEval: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.listarPlanManejoEvaluacionRequest.idPlanManejoEval = idPlanManejoEval;
    this.planManejoEvaluacionService
      .listarPlanManejoEvaluacion(this.listarPlanManejoEvaluacionRequest)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.data.length > 0) {
            this.informacion = result.data[0];
            if (
              this.usuario.sirperfil == 'MDP' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVAPRES'
            ) {
              this.disabled = false;
              this.guardarConsideracionDeshabilitado = false;
              this.guardarArchivoDeshabilitado = false;
            }

            if (
              this.usuario.sirperfil == 'ARFFS' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL'
            ) {
              this.disabledSend = false;
              this.guardarConsideracionDeshabilitado = false;
              this.botonLineamientos = true;
            }

            if (
              this.usuario.sirperfil == 'ARFFS' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVARECI'
            ) {
              this.guardarConsideracionDeshabilitado = false;
            }

            if (
              this.usuario.sirperfil == 'TITULARTH' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVAOBS'
            ) {
              this.visibleTab = true;
            }

            if (
              this.usuario.sirperfil == 'MDP' ||
              this.informacion.estadoPlanManejoEvaluacion == 'EEVAPRES'
            ) {
              this.visibleTab = false;
            }

            if (
              this.usuario.sirperfil == 'ARFFS' &&
              (this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL' ||
                this.informacion.estadoPlanManejoEvaluacion == 'EEVARECI')
            ) {
              this.disabledRadio = false;
            }

            if (
              this.usuario.sirperfil == 'ARFFS' &&
              (this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL' ||
                this.informacion.estadoPlanManejoEvaluacion == 'EEVARESOLA')
            ) {
              this.botonLineamientos = true;
            }
            if (
              this.usuario.sirperfil == 'TITULARTH' &&
              (this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL' ||
                this.informacion.estadoPlanManejoEvaluacion == 'EEVARESOLA' ||
                this.informacion.estadoPlanManejoEvaluacion == 'EEVAOBS')
            ) {
              this.botonLineamientos = true;
            }
            if (
              this.usuario.sirperfil == 'OSINFOR' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVARESOLA'
            ) {
              this.botonLineamientos = true;
            }
            if (
              this.usuario.sirperfil == 'COMESTADIST' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVARESOLA'
            ) {
              this.botonLineamientos = true;
            }

            this.listarArchivoFirmado();
            this.listarArchivosPlan();
            this.listarTipoDocumento();
            this.listarEvaluacion();
            this.obtenerContrato(this.informacion.idPlanManejo);
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  listarArchivosPlan() {
    let params = {
      idPlanManejo: this.informacion.idPlanManejo,
      codigoProceso:'PGMF'
    };

    this.planManejoService
      .listarPorFiltroPlanManejoArchivo(params)
      .subscribe((result: any) => {
        if (result.data) {
          this.fileList = result.data;

          this.fileList.forEach((element) => {
            if (!element.conforme) {
            }
          });
        }
      });
  }

  listarArchivosPlan2() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idPlanManejo: this.informacion.idPlanManejo,
    };

    this.planManejoService
      .listarPorFiltroPlanManejoArchivo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.data) {
            this.fileList = result.data;
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  validarOsinfor(data: any) {
    //data.respuestaValidacionOSINFOR = (new Date()).getSeconds() % 2;

    let params = {
      pageNumber: 1,
      pageSize: 1,
      titular: '',
      tituloHabilitante: this.consideracionD.tituloHabilitante,
    };
    this.servicioOsinfor(params);
  }

  observacionConsEspecial(e: any, data: any) {
    // debugger
    data.observacion = '';

    if (
      this.usuario.sirperfil == 'ARFFS' &&
      this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL'
    ) {
      this.botonLineamientos = this.validarConforme();
    }
  }

  observacionEspecPresentes(e: any, data: any) {
    //  debugger
    data.observacion = '';

    if (
      this.usuario.sirperfil == 'ARFFS' &&
      this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL'
    ) {
      this.botonLineamientos = this.validarConforme();
    }
  }

  observacionClick(e: any, data: any) {
    //  debugger
    data.observacion = null;

    if (
      this.usuario.sirperfil == 'ARFFS' &&
      this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL'
    ) {
      this.botonLineamientos = this.validarConforme();
    }
  }

  guardarArchivo() {
    let params: any = {
      idPlanManejoArchivo: this.archivoObtenido.idPlanManejoArchivo,
      conforme: this.archivoObtenido.conforme,
      observacion: this.archivoObtenido.conforme
        ? null
        : this.archivoObtenido.observacion,
      idUsuarioModificacion: this.usuario.idusuario,
    };

    if (
      !this.archivoObtenido.conforme &&
      this.archivoObtenido.observacion == ''
    ) {
      this.ErrorMensaje('(*) Debe ingresar una observación.');
      return;
    }

    this.actualizarPlanManejoArcvhivo(params);
  }

  verRequisito(d: any) {
    this.showComboList = true;
    let params: any = {
      idPlanManejo: d.idPlanManejo,
      idArchivo: d.idArchivo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoService
      .obtenerPlanManejoArchivo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.data) {
            this.archivoObtenido = result.data;
            this.showComboList =
              this.listaTipoDocumento.filter(
                (v: any) => v.codigo == this.archivoObtenido.idTipoDocumento
              ).length >= 1;

            this.mostrarRequisitoModal = true;
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  verArchivo(d: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (d.idArchivo) {
      let params = {
        idArchivo: d.idArchivo,
      };

      this.archivoService
        .descargarArchivoGeneral(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(
          (result: any) => {
            if (result.data !== null && result.data !== undefined) {
              // debugger
              DownloadFile(
                result.data.archivo,
                result.data.nombeArchivo,
                result.data.contenTypeArchivo
              );
            }
          },
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
          }
        );
    } /*else {
      DowloadFileLocal(this.file.url, this.file.nombre);

    }*/
  }

  guardar() {
    if (!this.validarGuardarCorrecto()) {
      return;
    }

    let lista = this.obtenerListaGuardar();

    let b = false;
    let mensaje: string = '';
    let mostrarMensaje = false;
    let descSeccion: string = '';

    if (this.usuario.sirperfil == 'MDP') {
      if (!this.consideracionA.codigoTramite) {
        mensaje += '(*) Ingrese código de trámite. \n';
        mostrarMensaje = true;
      }

      if (!this.consideracionA.fechaRecepcion) {
        mensaje += '(*) Ingrese fecha de recepción. \n';
        mostrarMensaje = true;
      }
    }

    lista.forEach((element) => {
      if (
        element.idPlanManejoEvalDet !== null &&
        element.idPlanManejoEvalDet !== undefined
      ) {
        b = true;
      }
    });

    if (mostrarMensaje) {
      this.ErrorMensaje(mensaje);
      return;
    }

    if (b) {
      this.actualizarEvaluacion(lista);
    } else {
      this.registrarEvaluacion(lista);
    }
  }

  private validarEnvioCorrecto = (): boolean => {
    let pasoValidaciones: boolean = true;
    if (this.visibleTab) {
      for (let index = 0; index < this.fileList.length; index++) {
        if (
          this.fileList[index].conforme === null ||
          this.fileList[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Existen requisistos TUPA sin conformidad, no se puede continuar..'
          );
          return pasoValidaciones;
        }
        if (
          !this.fileList[index].conforme &&
          (this.fileList[index].observacion === null ||
            this.fileList[index].observacion === undefined ||
            this.fileList[index].observacion === '')
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'En la consideración A ingresar la observación.'
          );
          return pasoValidaciones;
        }
      }

      if (
        this.consideracionB.conforme === null ||
        this.consideracionB === undefined
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'Error En la consideración B seleccione conforme u observado'
        );
        return pasoValidaciones;
      }
      if (
        !this.consideracionB.conforme &&
        (this.consideracionB.observacion === null ||
          this.consideracionB.observacion === undefined ||
          this.consideracionB.observacion === '')
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración B ingresar la observación.'
        );
        return pasoValidaciones;
      }

      if (
        this.consideracionC.conforme === null ||
        this.consideracionC === undefined
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'Error En la consideración C seleccione conforme u observado'
        );
        return pasoValidaciones;
      }
      if (
        !this.consideracionC.conforme &&
        (this.consideracionC.observacion === null ||
          this.consideracionC.observacion === undefined ||
          this.consideracionC.observacion === '')
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración C ingresar la observación.'
        );
        return pasoValidaciones;
      }
      if (
        this.consideracionD.conforme === null ||
        this.consideracionD === undefined
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'Error En la consideración D seleccione conforme u observado'
        );
        return pasoValidaciones;
      }
      if (
        !this.consideracionD.conforme &&
        (this.consideracionD.observacion === null ||
          this.consideracionD.observacion === undefined ||
          this.consideracionD.observacion === '')
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración D ingresar la observación.'
        );
        return pasoValidaciones;
      }

      if (
        this.consideracionE.conforme === false &&
        !this.consideracionE.observacion
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración E ingresar la observación.'
        );
        return pasoValidaciones;
      }

      if (
        this.consideracionF.conforme === false &&
        !this.consideracionF.observacion
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración F ingresar la observación.'
        );
        return pasoValidaciones;
      }

      if (
        this.consideracionG.conforme === false &&
        !this.consideracionG.observacion
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración G ingresar la observación.'
        );
        return pasoValidaciones;
      }

      for (let index = 0; index < this.lstConsEspecial.length; index++) {
        if (
          this.lstConsEspecial[index].conforme === null ||
          this.lstConsEspecial[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Error En la consideración H seleccione conforme u observado'
          );
          return pasoValidaciones;
        }
        if (
          !this.lstConsEspecial[index].conforme &&
          (this.lstConsEspecial[index].observacion === null ||
            this.lstConsEspecial[index].observacion === undefined ||
            this.lstConsEspecial[index].observacion === '')
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'En la consideración H ingresar la observación.'
          );
          return pasoValidaciones;
        }
      }

      for (let index = 0; index < this.lstEspciePresente.length; index++) {
        if (
          this.lstEspciePresente[index].conforme === null ||
          this.lstEspciePresente[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Error En la consideración I seleccione conforme u observado'
          );
          return pasoValidaciones;
        }
        if (
          !this.lstEspciePresente[index].conforme &&
          (this.lstEspciePresente[index].observacion === null ||
            this.lstEspciePresente[index].observacion === undefined ||
            this.lstEspciePresente[index].observacion === '')
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'En la consideración I ingresar la observación.'
          );
          return pasoValidaciones;
        }
      }
      return pasoValidaciones;
    } else {
      for (let index = 0; index < this.fileList.length; index++) {
        if (
          this.fileList[index].conforme === null ||
          this.fileList[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Existen requisistos TUPA sin conformidad, no se puede continuar..'
          );
          return pasoValidaciones;
        }
        if (
          !this.fileList[index].conforme &&
          (this.fileList[index].observacion === null ||
            this.fileList[index].observacion === undefined ||
            this.fileList[index].observacion === '')
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'En la consideración A ingresar la observación.'
          );
          return pasoValidaciones;
        }
      }

      return pasoValidaciones;
    }
  };

  private validarGuardarCorrecto = (): boolean => {
    let pasoValidaciones: boolean = true;
    if (this.visibleTab) {
      for (let index = 0; index < this.fileList.length; index++) {
        if (
          this.fileList[index].conforme === null ||
          this.fileList[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Existen requisistos TUPA sin conformidad, no se puede continuar..'
          );
          return pasoValidaciones;
        }
      }

      if (
        this.consideracionB.conforme === null ||
        this.consideracionB === undefined
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'Error En la consideración B seleccione conforme u observado'
        );
        return pasoValidaciones;
      } else {
        if (!this.consideracionB.conforme && !this.consideracionB.observacion) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'En la consideración B ingresar la observación.'
          );
          return pasoValidaciones;
        }
      }

      if (
        this.consideracionC.conforme === null ||
        this.consideracionC === undefined
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'Error En la consideración C seleccione conforme u observado'
        );
        return pasoValidaciones;
      } else {
        if (!this.consideracionC.conforme && !this.consideracionC.observacion) {
          pasoValidaciones = false;
          this.ErrorMensaje('En la consideración C ingresar la observación.');
          return pasoValidaciones;
        }
      }

      if (
        this.consideracionD.conforme === null ||
        this.consideracionD === undefined
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'Error En la consideración D seleccione conforme u observado'
        );
        return pasoValidaciones;
      } else {
        if (!this.consideracionD.conforme && !this.consideracionD.observacion) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'En la consideración D ingresar la observación.'
          );
          return pasoValidaciones;
        }
      }

      if (
        this.consideracionE.conforme === false &&
        !this.consideracionE.observacion
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración E ingresar la observación.'
        );
        return pasoValidaciones;
      }

      if (
        this.consideracionF.conforme === false &&
        !this.consideracionF.observacion
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración F ingresar la observación.'
        );
        return pasoValidaciones;
      }

      if (
        this.consideracionG.conforme === false &&
        !this.consideracionG.observacion
      ) {
        pasoValidaciones = false;
        this.ErrorMensaje(
          'En la consideración G ingresar la observación.'
        );
        return pasoValidaciones;
      }

      for (let index = 0; index < this.lstConsEspecial.length; index++) {
        if (
          this.lstConsEspecial[index].conforme === null ||
          this.lstConsEspecial[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Error En la consideración H seleccione conforme u observado'
          );
          return pasoValidaciones;
        } else {
          if (
            !this.lstConsEspecial[index].conforme &&
            !this.lstConsEspecial[index].observacion
          ) {
            pasoValidaciones = false;
            this.ErrorMensaje(
              'En la consideración H ingresar la observación.'
            );
            return pasoValidaciones;
          }
        }
      }

      for (let index = 0; index < this.lstEspciePresente.length; index++) {
        if (
          this.lstEspciePresente[index].conforme === null ||
          this.lstEspciePresente[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Error En la consideración I seleccione conforme u observado'
          );
          return pasoValidaciones;
        } else {
          if (
            !this.lstEspciePresente[index].conforme &&
            !this.lstEspciePresente[index].observacion
          ) {
            pasoValidaciones = false;
            this.ErrorMensaje(
              'En la consideración I ingresar la observación.'
            );
            return pasoValidaciones;
          }
        }
      }
      return pasoValidaciones;
    } else {
      for (let index = 0; index < this.fileList.length; index++) {
        if (
          this.fileList[index].conforme === null ||
          this.fileList[index] === undefined
        ) {
          pasoValidaciones = false;
          this.ErrorMensaje(
            'Existen requisistos TUPA sin conformidad, no se puede continuar.'
          );
          return pasoValidaciones;
        }
      }
      return pasoValidaciones;
    }
  };

  enviar() {
    let conformeConsideraciones: boolean = this.validarConforme();

    if (!this.validarEnvioCorrecto()) {
      return;
    }

    let conformeReqTupa: boolean = true;

    this.fileList.forEach((element) => {
      if (!element.conforme) {
        conformeReqTupa = false;
      }
    });

    let param: any = {
      idPlanManejoEval: this.informacion.idPlanManejoEval,
      conformeReqTupa: conformeReqTupa,
      conformeConsideraciones: conformeConsideraciones,
      idPlanManejo: this.informacion.idPlanManejo,
      idUsuarioModificacion: this.usuario.idusuario,
      evaluado: false,
    };

    if (
      this.usuario.sirperfil == 'ARFFS' &&
      this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL'
    ) {
      param.observacionRestrictiva = false;

      if (
        !this.consideracionB.conforme ||
        !this.consideracionC.conforme ||
        !this.consideracionD.conforme ||
        !this.consideracionH.conforme ||
        !this.consideracionI.conforme
      ) {
        param.observacionRestrictiva = true;
      }

      if (
        this.consideracionB.conforme &&
        this.consideracionC.conforme &&
        this.consideracionD &&
        this.consideracionE.conforme &&
        this.consideracionH.conforme &&
        this.consideracionI.conforme
      ) {
        param.observacionRestrictiva = null;
      }
    }

    this.enviarEvaluacion(param);
  }

  irLineamientos() {
    if (
      this.informacion.estadoPlanManejoEvaluacion == 'EEVAEVAL' &&
      this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL
    ) {
      if (!this.validarGuardarCorrecto()) {
        return;
      }

      let params = this.obtenerListaGuardar();
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.planManejoEvaluacionService
        .actualizarPlanManejoEvaluacionDetalle(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(
          (result: any) => {
            if (result.success) {
              this.router.navigate([
                '/planificacion/lineamientos-pgmf',
                this.informacion.idPlanManejoEval,
              ]);
            }
          },
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
          }
        );
    } else {
      this.router.navigate([
        '/planificacion/lineamientos-pgmf',
        this.informacion.idPlanManejoEval,
      ]);
    }
  }

  private setIds(object: any) {
    object.idUsuarioRegistro = this.usuario.idusuario;
    object.idUsuarioModificacion = this.usuario.idusuario;
    if (
      object.idPlanManejoEvaluacion === null ||
      object.idPlanManejoEvaluacion === undefined
    ) {
      object.idPlanManejoEvaluacion = this.informacion.idPlanManejoEval;
    }
  }

  private listarTipoDocumento() {
    let parametro: any = {};
    parametro.idTipoParametro = 79;

    this.genericoService
      .listarPorFiltroParametro(parametro)
      .subscribe((result: any) => {
        this.listaTipoDocumento = result.data;
      });
  }

  private actualizarPlanManejoArcvhivo(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoService
      .actualizarPlanManejoArchivo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.SuccessMensaje(result.message);
            this.mostrarRequisitoModal = false;
            this.listarArchivosPlan2();
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  private listarEvaluacion() {
    let params = {
      idPlanManejoEval: this.informacion.idPlanManejoEval,
    };

    this.planManejoEvaluacionService
      .listarPlanManejoEvaluacionDetalle(params)
      .subscribe((result: any) => {
        if (result.data) {
          this.ordenarLista(result.data, 'PGMFEVALA', this.consideracionA);
          this.ordenarLista(result.data, 'PGMFEVALB', this.consideracionB);
          this.ordenarLista(result.data, 'PGMFEVALC', this.consideracionC);
          this.ordenarLista(result.data, 'PGMFEVALD', this.consideracionD);
          this.ordenarLista(result.data, 'PGMFEVALE', this.consideracionE);
          this.ordenarLista(result.data, 'PGMFEVALF', this.consideracionF);
          this.ordenarLista(result.data, 'PGMFEVALG', this.consideracionG);
          this.ordenarLista(result.data, 'PGMFEVALH', this.consideracionH);
          this.ordenarLista(result.data, 'PGMFEVALI', this.consideracionI);

          this.botonLineamientos = this.validarConforme();
        }
      });
  }

  idPlanManejoEvalDetEspeciesI: number | null = null;
  private ordenarLista(lista: any[], codigo: string, object: any) {
    if (codigo === 'PGMFEVALH' && lista.length === 0) {
      this.listarParametroEspeciales();
    }
    if (codigo === 'PGMFEVALI' && lista.length === 0) {
      this.listarEspecieXCenso();
    }

    lista.forEach((element) => {
      if (
        element.codigoTipo !== undefined &&
        element.codigoTipo !== null &&
        element.codigoTipo === codigo
      ) {
        object.codigoTipo = element.codigoTipo;
        object.conforme = element.conforme;
        object.estado = element.estado;
        object.idPlanManejoEvalDet = element.idPlanManejoEvalDet;
        object.idPlanManejoEvaluacion = element.idPlanManejoEvaluacion;
        object.tituloHabilitante = element.tituloHabilitante;
        object.estadoValidacion = element.estadoValidacion;
        object.observacion = element.observacion;
        object.codigoTramite = element.codigoTramite;
        object.fechaRecepcion = element.fechaRecepcion
          ? new Date(element.fechaRecepcion)
          : element.fechaRecepcion; //;
        object.disabledRadio = this.disabledRadio;

        if (
          codigo == 'PGMFEVALA' &&
          this.usuario.sirperfil == 'MDP' &&
          this.informacion.estadoPlanManejoEvaluacion == 'EEVAPRES'
        ) {
          this.disabledSend = false;
        }

        if (codigo === 'PGMFEVALH') {
          if (
            element.lstPlanManejoEvaluacionSubDetalle !== null &&
            element.lstPlanManejoEvaluacionSubDetalle !== undefined &&
            element.lstPlanManejoEvaluacionSubDetalle.length > 0
          ) {
            this.lstConsEspecial = element.lstPlanManejoEvaluacionSubDetalle;
          } else {
            this.listarParametroEspeciales();
          }
        }
        if (codigo === 'PGMFEVALI') {
          this.idPlanManejoEvalDetEspeciesI = element.idPlanManejoEvalDet;
          if (
            element.lstPlanManejoEvaluacionSubDetalle !== null &&
            element.lstPlanManejoEvaluacionSubDetalle !== undefined &&
            element.lstPlanManejoEvaluacionSubDetalle.length > 0
          ) {
            this.lstEspciePresente = element.lstPlanManejoEvaluacionSubDetalle;
          } else {
            this.listarEspecieXCenso();
          }
        }
        if (codigo === 'PGMFEVALD') {
          this.obtenerContrato(this.informacion.idPlanManejo);
        }
      }
    });
  }

  private registrarEvaluacion(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoEvaluacionService
      .registrarPlanManejoEvaluacionDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.success) {
            if (this.usuario.sirperfil == 'MDP') {
              this.SuccessMensaje('Se actualizaron los requisitos TUPA.');
            } else {
              this.SuccessMensaje('Se actualizó la evaluación de PMGF.');
            }
            this.listarEvaluacion();
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  private actualizarEvaluacion(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoEvaluacionService
      .actualizarPlanManejoEvaluacionDetalle(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.success) {
            if (this.usuario.sirperfil == 'MDP') {
              this.SuccessMensaje('Se actualizaron los requisitos TUPA.');
            } else {
              this.SuccessMensaje('Se actualizó la evaluación del detalle del plan de manejo PGMF.');
            }

            if (
              this.usuario.sirperfil == 'ARFFS' &&
              this.informacion.estadoPlanManejoEvaluacion == 'EEVARECI'
            ) {
              this.obtenerInformacionEvaluacion(
                this.informacion.idPlanManejoEval
              );
            } else {
              this.listarEvaluacion();
            }
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  private listarParametroEspeciales() {
    let parametro: any = {};
    parametro.idTipoParametro = 88;

    this.genericoService
      .listarPorFiltroParametro(parametro)
      .subscribe((result: any) => {
        let obj = null;

        result.data.forEach((element: any) => {
          obj = {
            idPlanManejoEvalDet: null,
            conforme: null,
            observacion: null,
            consideracion: element.codigo,
            consideracionDescripcion: element.valorPrimario,
          };
          this.lstConsEspecial.push(obj);
        });
      });
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  verPGMF() {
    // const url = this.router.serializeUrl(
    const url: any = this.router.createUrlTree([
      `${this.varUrl}/planificacion/concesion-forestal-maderables/${this.informacion.idPlanManejo}`,
    ]);
    // );
    window.open(url, '_blank');
  }

  regresar() {
    this.router.navigate(['/planificacion/bandeja-evaluacion-pgmf']);
  }

  private listarEspecieXCenso() {
    this.lstEspciePresente = [];
    let parametro: any = {};
    parametro.idPlanManejo = this.informacion.idPlanManejo;
    //parametro.idCensoForestal = 1146;

    this.planManejoEvaluacionService
      .listarEspecieXCenso(parametro)
      .subscribe((result: any) => {
        let obj = null;

        result.data.forEach((element: any) => {
          obj = {
            idPlanManejoEvalDet: this.idPlanManejoEvalDetEspeciesI,
            idEspecie: element.idCodigoEspecie,
            nombreCientifico: element.nombreCientifico,
            nombreComun: element.nombreComun,
            conforme: null,
            observacion: null,
          };
          this.lstEspciePresente.push(obj);
        });
      });
  }

  private enviarEvaluacion(data: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let lista = this.obtenerListaGuardar();

    this.planManejoEvaluacionService
      .actualizarPlanManejoEvaluacionDetalle(lista)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.planManejoEvaluacionService.enviarEvaluacion(data).subscribe(
              (result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                  this.router.navigate([
                    '/planificacion/bandeja-evaluacion-pgmf',
                  ]);
                }
              },
              (error: HttpErrorResponse) => {
                this.ErrorMensaje(error.message);
              }
            );
          } else {
            this.ErrorMensaje('Ocurrió un error');
          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  }

  private obtenerContrato(params: any) {
    this.planManejoService
      .listarPlanManejoContrato(params)
      .subscribe((result: any) => {
        if (result.success) {
          if (
            result.data !== null &&
            result.data !== undefined &&
            result.data.length > 0
          ) {
            this.consideracionD.tituloHabilitante = result.data[0].codigoTitulo;
          }
        }
      });
  }

  private servicioOsinfor(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiForestalService
      .consultarPlanesOsinfor(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.success) {
          if (result.dataService.items.length > 0) {
            this.consideracionD.respuestaEstadoValidacionOSINFOR = 1;
            this.consideracionD.respuestaValidacionOSINFOR =
              'Medida Cautelar: ' +
              result.dataService.items[0].medidaCautelar +
              '\n Medida Provisoria: ' +
              result.dataService.items[0].medidaProvisoria;
          } else {
            this.consideracionD.respuestaEstadoValidacionOSINFOR = 0;
            this.consideracionD.respuestaValidacionOSINFOR =
              'No se encuentran registros.';
          }
        } else {
          this.consideracionD.respuestaEstadoValidacionOSINFOR = 0;
          this.consideracionD.respuestaValidacionOSINFOR =
            'No se encuentran registros.';
        }
      });
  }

  obtenerListaGuardar() {
    this.setIds(this.consideracionA);
    this.setIds(this.consideracionB);
    this.setIds(this.consideracionC);
    this.setIds(this.consideracionD);
    this.setIds(this.consideracionE);
    this.setIds(this.consideracionF);
    this.setIds(this.consideracionG);
    this.setIds(this.consideracionH);
    this.setIds(this.consideracionI);

    this.consideracionA.codigoTipo = 'PGMFEVALA';
    this.consideracionB.codigoTipo = 'PGMFEVALB';
    this.consideracionC.codigoTipo = 'PGMFEVALC';
    this.consideracionD.codigoTipo = 'PGMFEVALD';
    this.consideracionE.codigoTipo = 'PGMFEVALE';
    this.consideracionF.codigoTipo = 'PGMFEVALF';
    this.consideracionG.codigoTipo = 'PGMFEVALG';
    this.consideracionH.codigoTipo = 'PGMFEVALH';
    this.consideracionI.codigoTipo = 'PGMFEVALI';

    this.consideracionH.lstPlanManejoEvaluacionSubDetalle =
      this.lstConsEspecial;
    this.consideracionI.lstPlanManejoEvaluacionSubDetalle =
      this.lstEspciePresente;

    let lista: any[] = [];
    lista.push(this.consideracionA);
    lista.push(this.consideracionB);
    lista.push(this.consideracionC);
    lista.push(this.consideracionD);
    lista.push(this.consideracionE);
    lista.push(this.consideracionF);
    lista.push(this.consideracionG);
    lista.push(this.consideracionH);
    lista.push(this.consideracionI);

    return lista;
  }

  validarConforme(): boolean {
    let conformeConsideraciones: boolean = true;

    this.consideracionH.conforme = this.lstConsEspecial.every(
      (item) => item.conforme
    );
    this.consideracionI.conforme = this.lstEspciePresente.every(
      (item) => item.conforme
    );

    if (
      this.consideracionB.conforme === undefined ||
      this.consideracionB.conforme === null ||
      !this.consideracionB.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionC.conforme === undefined ||
      this.consideracionC.conforme === null ||
      !this.consideracionC.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionD.conforme === undefined ||
      this.consideracionD.conforme === null ||
      !this.consideracionD.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionE.conforme != undefined &&
      !this.consideracionE.conforme != null &&
      !this.consideracionE.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionF.conforme != undefined &&
      this.consideracionF.conforme != null &&
      !this.consideracionF.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionG.conforme != undefined &&
      this.consideracionG.conforme != null &&
      !this.consideracionG.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionH.conforme === undefined ||
      this.consideracionH.conforme === null ||
      !this.consideracionH.conforme
    ) {
      conformeConsideraciones = false;
    }
    if (
      this.consideracionI.conforme === undefined ||
      this.consideracionI.conforme === null ||
      !this.consideracionI.conforme
    ) {
      conformeConsideraciones = false;
    }

    return conformeConsideraciones;
  }
}
