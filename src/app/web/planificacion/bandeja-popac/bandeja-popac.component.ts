import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Page, PlanManejoMSG as MSG } from '@models';
import { PlanManejoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LazyLoadEvent } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ModalInfoPlanComponent } from 'src/app/shared/components/modal-info-plan/modal-info-plan.component';
import { EvaluacionListarRequest } from '../../../model/EvaluacionListarRequest';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import { CodigoEstadoEvaluacion } from '../../../model/util/CodigoEstadoEvaluacion';
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from '../../../model/util/CodigoEstadoPlanManejo';
import { CodigoProceso } from '../../../model/util/CodigoProceso';

@Component({
  selector: 'bandeja-popac',
  styleUrls: ['./bandeja-popac.component.scss'],
  templateUrl: './bandeja-popac.component.html',
})
export class BandejaPopacComponent implements OnInit {
  f!: FormGroup;
  planes: any[] = [];
  usuario!: UsuarioModel;
  evaluacionRequest: EvaluacionListarRequest;
  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  CodigoProceso = CodigoProceso;
  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  CodigoEstadoMesaPartes = CodigoEstadoMesaPartes;

  PLAN = {
    idTipoProceso: 3,
    idTipoEscala: 2,
    idTipoPlan: 10,
    idSolicitud: 1,
    descripcion: CodigoProceso.PLAN_OPERATIVO_PMFI_CNCC,
  };

  loading = false;
  totalRecords = 0;

  ref!: DynamicDialogRef;

  permisos: IPermisoOpcion = {} as IPermisoOpcion;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialogService: DialogService,
    private activaRoute: ActivatedRoute
  ) {
    this.f = this.initForm();
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    this.buscar();
  }

  initForm() {
    return this.fb.group({
      dniElaborador: [null],
      rucComunidad: [null],
      nombreElaborador: [null],
      idPlanManejo: [null],
    });
  }

  buscar() {
    this.listarPlanes().subscribe();
  }

  limpiar() {
    this.f.reset();
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.buscar();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
  }

  nuevoPlan() {
    const body = { ...this.PLAN, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => this.navigate(res.data.idPlanManejo));
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo);
  }

  listarPlanes(page?: Page) {
    if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }
    const r = { ...this.PLAN, ...this.evaluacionRequest };
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(
        tap((res) => {
          if (res.success && res.data.length > 0) {
            this.planes = res.data;
            this.totalRecords = res.totalRecords;
          } else {
            this.planes = [];
            this.totalRecords = 0;
            this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
          }
        })
      );
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body).pipe(
      tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE),
      })
    );
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiPlanManejo
      .filtrar(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  navigate(idPlan: number) {
    const uri = 'planificacion/plan-operativo-pmfi-cncc';
    this.router.navigate([uri, idPlan]);
  }

  openModal(data: any) {
    this.ref = this.dialogService.open(ModalInfoPlanComponent, {
      header: 'Plan General ' + CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      width: '60%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        buscar: false,
        codTipoPlan: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
        idPlanManejo: data.idPlanManejo,
        idPlanManejoPadre: data.idPlanManejoPadre,
      },
    });
  }
}
