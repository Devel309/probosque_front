import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesaPartesComponent } from './mesa-partes.component';

describe('MesaPartesComponent', () => {
  let component: MesaPartesComponent;
  let fixture: ComponentFixture<MesaPartesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesaPartesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesaPartesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
