import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MesaPartesComponent } from './mesa-partes.component';
import { SharedModule } from 'primeng/api';
import { PanelModule } from 'primeng/panel';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RequisitoPgmfSimpleModule } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AccordionModule} from 'primeng/accordion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ButtonModule } from 'primeng/button';
import {TableModule} from 'primeng/table';


@NgModule({
  declarations: [
    MesaPartesComponent
  ],
  exports: [
    MesaPartesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PanelModule,
    ConfirmPopupModule,
    RadioButtonModule,
    ReactiveFormsModule,
    FormsModule,
    RequisitoPgmfSimpleModule,
    AccordionModule,
    MatTabsModule,
    MatDatepickerModule,
    ButtonModule,
    TableModule,
  ]
})
export class MesaPartesModule { }
