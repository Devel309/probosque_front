import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-requisitos-mesa-partes',
  templateUrl: './requisitos-mesa-partes.component.html',
  styleUrls: ['./requisitos-mesa-partes.component.scss']
})
export class RequisitosMesaPartesComponent implements OnInit {

  idPlanManejo!: number;

  tabIndex: number = 0;

  disabled: boolean = false;

  constructor(private route: ActivatedRoute) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

}
