import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitosMesaPartesComponent } from './requisitos-mesa-partes.component';

describe('RequisitosMesaPartesComponent', () => {
  let component: RequisitosMesaPartesComponent;
  let fixture: ComponentFixture<RequisitosMesaPartesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequisitosMesaPartesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitosMesaPartesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
