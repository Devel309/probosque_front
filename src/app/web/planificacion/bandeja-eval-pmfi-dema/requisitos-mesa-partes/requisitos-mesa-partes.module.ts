import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequisitosMesaPartesComponent } from './requisitos-mesa-partes.component';
import { SharedModule } from '@shared';
import { MatTabsModule } from '@angular/material/tabs';
import { MesaPartesModule } from './tabs/mesa-partes/mesa-partes.module';

@NgModule({
  declarations: [RequisitosMesaPartesComponent],
  exports: [RequisitosMesaPartesComponent],
  imports: [CommonModule, SharedModule, MatTabsModule, MesaPartesModule],
})
export class RequisitosMesaPartesModule {}
