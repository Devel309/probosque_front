import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService } from "@services";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { RegenteService } from "src/app/service/regente.service";
import { DowloadFileLocal, DownloadFile, ToastService } from "@shared";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastModule } from "primeng/toast";

@Component({
  selector: "lista-regente",
  templateUrl: "./lista-regente.component.html",
  styleUrls: ["./lista-regente.component.scss"],
})
export class ListaRegenteComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;

  listRegente: any[] = [];
  listArchivos: any[] = [];
  @Input() fileList: any[] = [];

  file: any = {};
  constructor(
    private archivoServ: ArchivoService,
    public regenteService: RegenteService,
    private dialog: MatDialog,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.listarRegenteArchivos();
  }

  listarRegenteArchivos() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: this.codigoEpica,
    };
    this.regenteService
      .listarRegenteArchivos(params)
      .subscribe((response: any) => {
        if (!!response.data) {
          this.listRegente = response.data;
          this.listArchivos = response.data[0].archivos.length ? response.data[0].archivos:[];
        }
      });
  }
  verArchivo(d: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (d.idArchivo) {
      let params = {
        idArchivo: d.idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.toast.error(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }
}
