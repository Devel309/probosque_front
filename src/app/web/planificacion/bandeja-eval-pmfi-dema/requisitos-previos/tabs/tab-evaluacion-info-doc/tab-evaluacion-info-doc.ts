import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { MapApi, observados, ToastService } from "@shared";
import { Accordion } from "primeng/accordion";
import { MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  ConsideracionesGenerales,
  EvaluacionObj,
} from "src/app/model/evaluacionInfoDoc";
import { CodigoEstadoEvaluacion } from "src/app/model/util/CodigoEstadoEvaluacion";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { DialogTipoEspecieComponent } from "src/app/shared/components/dialog-tipo-especie/dialog-tipo-especie.component";

@Component({
  selector: "app-tab-evaluacion-info-doc",
  templateUrl: "./tab-evaluacion-info-doc.component.html",
  styleUrls: ["./tab-evaluacion-info-doc.component.scss"],
})
export class TabEvaluacionInfoDoc implements OnInit, AfterViewInit {
  @ViewChild("acordion") acordion!: Accordion;
  @ViewChild("acordion_1") acordion_1!: Accordion;

  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  listCoordinatesAnexo: any[] = [];
  listAnexoArea: any = [];
  totalAnexoArea!: number;

  CodigoPMFIC = CodigoPMFIC;
  _idInfBasica: number = 0;
  _idInfBasicaDet: number = 0;

  accordeon_3: any = [];
  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;

  listadoVertices: any = [{}, {}];

  codigoSubSeccion1: string = "";
  codigoSubSeccion2: string = "";
  codigoSubSeccionTrack: string = "";

  acordeon_Tirulo: string = "";
  acordeon_body: ConsideracionesGenerales = new ConsideracionesGenerales();
  evaluacion: EvaluacionObj = new EvaluacionObj();

  ref!: DynamicDialogRef;

  constructor(
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private mapApi: MapApi,
    public dialogService: DialogService,
    private usuarioService: UsuarioService,
    private toast: ToastService,
    private messageService: MessageService
  ) {}

  ngAfterViewInit() {
    if (localStorage.getItem("EvalResuDet")) {
      this.acordion.tabs[0].selected = true;
      this.acordion_1.tabs[0].selected = true;
      localStorage.removeItem("EvalResuDet");
    }
    
  }

  ngOnInit(): void {
    
    this.consideracionesGenerales();
    this.listarCoordenadas();
    this.setearCodigo();
    this.listarEvaluacion();
  }

  consideracionesGenerales() {
    const param = {
      prefijo: "GENEDP",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .listarParametroLineamiento(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.map((response: any) => {
            response.listNivel2.map((value: any) => {
              if (value.codigo == "GENEDP532") {
                this.acordeon_Tirulo = value.valorPrimario;
                this.acordeon_body = new ConsideracionesGenerales(value);
              }
            });
          });
        }
      });
  }

  // listado coordenadas
  listarCoordenadas() {
    this.listCoordinatesAnexo = [];
    var params = {
      idInfBasica: this.codigoEpica,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.CodigoPMFIC.TAB_3_2,
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            let newList: any = [];
            this._idInfBasica = response.data[0].idInfBasica;
            this._idInfBasicaDet = response.data[0].idInfBasicaDet;
            response.data.forEach((t: any) => {
              if (t.codInfBasicaDet !== null) {
                newList.push({
                  idInfBasica: t.idInfBasica,
                  idInfBasicaDet: t.idInfBasicaDet,
                  codInfBasicaDet: t.codInfBasicaDet,
                  anexo: t.descripcion,
                  vertice: t.puntoVertice,
                  este: t.coordenadaEste,
                  norte: t.coordenadaNorte,
                  referencia: t.referencia,
                });
              }
            });
            this.listCoordinatesAnexo = newList.groupBy((t: any) => t.anexo);
            this.calculatArea();
          }
        }
      });
  }
  calculatArea() {
    this.listAnexoArea = [];
    this.listCoordinatesAnexo.forEach((t) => {
      let rings: number[][] = [];
      t.value.forEach((t2: any) => {
        rings.push([t2.este, t2.norte]);
      });
      let polygon = this.mapApi.createPolygon(rings);
      let area = this.mapApi.calculateArea(polygon, "hectares");
      this.listAnexoArea.push({
        anexo: t.key,
        area: Math.abs(parseFloat(area.toFixed(2))),
      });
    });
    let suma = 0;
    for (let j = 0; j < this.listAnexoArea.length; j++) {
      suma += parseFloat(this.listAnexoArea[j].area);
    }
    this.totalAnexoArea = suma;
  }
  changeConforme(value: string) {}

  setearCodigo() {
    if (this.codigoEpica == "PMFIC") {
      this.codigoSubSeccion1 = "PFMICORGAAnexo1";
      this.codigoSubSeccion2 = "PFMICORGAAnexo2";
      this.codigoSubSeccionTrack = "PMFICREAT";
    }
  }

  mostrarEspecie() {
    this.ref = this.dialogService.open(DialogTipoEspecieComponent, {
      header: "Especies",
      width: "70%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        invalid: true,
      },
    });
  }

  guardarEvaluacion() {
    let evaluacionDet: any = [];
    this.acordeon_body.listNivel3.forEach((item1: any) => {
      var objDet: any = {};
      objDet.idEvaluacionDet = item1.idEvaluacionDet
        ? item1.idEvaluacionDet
        : 0;
      objDet.codigoEvaluacionDet = this.codigoEpica;
      objDet.codigoEvaluacionDetSub = this.codigoEpica + "REEVRIIC";
      objDet.codigoEvaluacionDetPost = item1.codigo;
      objDet.conforme = item1.codigoSniffsDescripcion;
      objDet.observacion = item1.observacion;
      objDet.idUsuarioRegistro = this.usuarioService.idUsuario;
      evaluacionDet.push(objDet);
    });

    this.evaluacion.listarEvaluacionDetalle = evaluacionDet;

    if (!observados(this.evaluacion.listarEvaluacionDetalle, this.toast)) {
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .registrarEvaluacionPlanManejo(this.evaluacion)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response.message);
          this.consideracionesGenerales();
          this.listarEvaluacion();
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  listarEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoEpica,
      codigoEvaluacionDetSub: this.codigoEpica + "REEVRIIC",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .obtenerEvaluacion(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
          } else {
            this.evaluacion.idEvaluacion = 0;
            this.evaluacion.idPlanManejo = this.idPlanManejo;
            this.evaluacion.codigoEvaluacion = this.codigoEpica;
            this.evaluacion.idUsuarioRegistro = this.usuarioService.idUsuario;
            this.evaluacion.listarEvaluacionDetalle = [];
          }
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
