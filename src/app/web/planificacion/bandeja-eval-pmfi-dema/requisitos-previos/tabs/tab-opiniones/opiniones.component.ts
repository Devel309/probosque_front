import { Component, OnInit, Input } from "@angular/core";
import { ParametroValorService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { MatDialog } from "@angular/material/dialog";
import { OpinionesService } from "src/app/service/opiniones/opiniones.service";
import { EvaluacionService } from "src/app/service/opiniones/evaluacion.service";
import { Entidad, OpinionModel } from "src/app/model/opinionModel";
import { LoadingComponent } from "src/app/components/loading/loading.component";
@Component({
  selector: "app-opiniones",
  templateUrl: "./opiniones.component.html",
  styleUrls: ["./opiniones.component.scss"],
})
export class OpinionesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;

  @Input() disabled!: boolean;

  listEntidades: Entidad[] = [];
  lisTiposArchivo: any[] = [];

  objSERNANP!: OpinionModel;
  objANA!: OpinionModel;
  objMINCUL!: OpinionModel;
  objOTRA!: OpinionModel;

  constructor(
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private opinionesService: OpinionesService,
    private parametroValorService: ParametroValorService,
    private evaluacionService: EvaluacionService
  ) {}

  ngOnInit(): void {
    this.entidades();
    this.tipoDocumento();
    this.listSERNANP();
    this.listANA();
    this.listMINCUL();
    this.listOTRA();
  }

  listSERNANP() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codSolicitud: this.codigoEpica,
      subCodSolicitud: "SERNANP",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.opinionesService
      .listarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          var obj = new OpinionModel(element);
          this.objSERNANP = obj;
        });
        this.dialog.closeAll();
      });
  }

  listANA() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codSolicitud: this.codigoEpica,
      subCodSolicitud: "ANA",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.opinionesService
      .listarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          var obj = new OpinionModel(element);
          this.objANA = obj;
        });
        this.dialog.closeAll();
      });
  }

  listMINCUL() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codSolicitud: this.codigoEpica,
      subCodSolicitud: "MINCUL",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.opinionesService
      .listarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          var obj = new OpinionModel(element);
          this.objMINCUL = obj;
        });
        this.dialog.closeAll();
      });
  }

  listOTRA() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codSolicitud: this.codigoEpica,
      subCodSolicitud: "OTRA",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.opinionesService
      .listarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          var obj = new OpinionModel(element);
          this.objOTRA = obj;
        });
        this.dialog.closeAll();
      });
  }

  entidades() {
    var params = { prefijo: "AUTSAN" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listEntidades = [...response.data];
      });
  }

  tipoDocumento() {
    var params = { prefijo: "TDOCGE" };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.lisTiposArchivo = [...response.data];
      });
  }

  changeObj(event: any) {
    var obj = new OpinionModel(event.opinionObj);
    obj.idPlanManejo = this.idPlanManejo;
    obj.idUsuarioRegistro = this.user.idUsuario;
    obj.codSolicitud = this.codigoEpica;
    obj.subCodSolicitud = event.subCodigo;
    obj.idArchivo = event.idArchivo
    if (event.subCodigo == "SERNANP") {
      obj.entidad = event.subCodigo
    } else if (event.subCodigo == "ANA") {
      obj.entidad = event.subCodigo
    } else if (event.subCodigo == "MINCUL") {
      obj.entidad = event.subCodigo
    }

    var params = [obj];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.opinionesService
      .registrarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.dialog.closeAll();
          if (event.subCodigo == "SERNANP") {
            this.listSERNANP();
          } else if (event.subCodigo == "ANA") {
            this.listANA();
          } else if (event.subCodigo == "MINCUL") {
            this.listMINCUL();
          } else if (event.subCodigo == "OTRA") {
            this.listOTRA();
          }
          this.toast.ok(response?.message);
        } else {
          this.dialog.closeAll();
          this.toast.error(response?.message);
        }
      });
  }
}
