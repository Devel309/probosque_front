import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, TipoDocumentoPlan, ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { ResumenService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";

@Component({
  selector: "app-tab-remitir-informacion",
  templateUrl: "./tab-remitir-informacion.component.html",
  styleUrls: ["./tab-remitir-informacion.component.scss"],
})
export class TabRemitirInformacionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();
  listaInfoPlan: any[] = [];
  listDocumentos: any = [];
  totalRecordsID = 0;
  loading = false;
  PLAN_MANEJO = {
    idTipoProceso: 3,
    idTipoPlan: 0,
  }
  estado: string = "";
  isDisabled: boolean = false;

  constructor(
    private router: Router,
    private resumenService: ResumenService,
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private archivoService: ArchivoService,
    private messageService: MessageService,
    private toast: ToastService,
    private user: UsuarioService,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService
  ) { }

  ngOnInit(): void {
    this.obtenerEstadoPlan();
    this.listaPlanManejo();
    this.listarArchivos();
    this.PLAN_MANEJO.idTipoPlan = TipoDocumentoPlan.DEMA;
  }

  listaPlanManejo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.resumenService
      .obtenerInformacionPlan(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        this.listaInfoPlan = response.data;
      });
  }

  verPlan(item:any){
    let url = '';
    if(item.tipoPlan === 'DEMA' ){
        url = '/planificacion/generar-declaracion-manejo-dema';
    }else if(item.tipoPlan === 'PMFI'){
        url = '/planificacion/formulacion-PMFI';
    }else if(item.tipoPlan === 'PMFIC'){
      url = '/planificacion/plan-manejo-forestal-intermedio';
    }
    this.router.navigate([url, item.idPlanManejo], { state: { data: this.PLAN_MANEJO.idTipoPlan } });
  }

  listarArchivos() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      extension: 'pdf',
      tipoDocumento: 'REREEVPL'
    };
    this.loading = true;
    this.anexosService
      .listarArchivosPlanManejo(params)
      .pipe(finalize(() => (this.loading = false)))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          this.totalRecordsID = response.data.length;
          response.data.forEach((item: any) => {
            if (item.extension === '.pdf') {
              this.listDocumentos.push(item);
            }
          });
        }
        this.dialog.closeAll();
      });
  }

  descargarArchivo(idArchivo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoService
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.errorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.errorMensaje('No se ha encontrado un documento vinculado.');
      this.dialog.closeAll();
    }
  }

  obtenerEstadoPlan() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    const body = {
      idPlanManejo: this.idPlanManejo
    }
    this.informacionGeneralService
      .listarInfGeneralResumido(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((_res: any) => {
        if (_res.data.length > 0) {
          this.estado = _res.data[ 0 ].codigoEstado;
          

          if (this.estado == 'EPLMAPROB' ||
            this.estado == 'EPLMBORD' ||
            this.estado == 'EPLMBORN' ||
            (this.estado !== 'EPLMFAVO' && 
            this.estado !== 'EPLMDESFAVO')) {
            this.isDisabled = true;
          }
        }
      });
  }

  actualizarPlanManejoEstado() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: this.estado,
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.evaluacionService
      .actualizarPlanManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
