import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ResultadosEvaluacionDetalle } from "../../../../../../model/resultadosEvaluacion";
import { LoadingComponent } from "../../../../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { EvaluacionService } from "../../../../../../service/evaluacion/evaluacion.service";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { ParametroValorService, UsuarioService } from "@services";
import { ResultadosEvaluacionService } from "../../../../../../service/evaluacion/resultadosEvaluacion.service";
import { MatDialog } from "@angular/material/dialog";
import { ConfirmationService } from "primeng/api";
import { ResumenService } from "../../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";

@Component({
  selector: "app-tab-resolver-solicitud",
  templateUrl: "./tab-resolver-solicitud.component.html",
  styleUrls: ["./tab-resolver-solicitud.component.scss"],
})
export class TabResolverSolicitudComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  codigoFirmado: string = "";
  codigoLegal: string = "";
  codigoNotificacion: string = "";

  evaluacion: any = "";

  classEstado: string = "";
  archivoRecurso: any = {};
  radioSelect: string = "FAVO";
  listDocumentos: any[] = [];

  listaDetalle: ResultadosEvaluacionDetalle[] = [];

  observadoObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  favorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  desFavorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();

  idEvalResultadoCarga: number = 0;
  idEvalResultadoEnvio: number = 0;

  isSubmittingCreate$ = this.createQuery.selectSubmitting();

  constructor(
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private user: UsuarioService,
    private createStore: ButtonsCreateStore,
    private createQuery: ButtonsCreateQuery,
    private resultadosEvaluacionService: ResultadosEvaluacionService,
    private dialog: MatDialog,
    private resumenService: ResumenService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.codigoFirmado = this.codigoEpica + "IEFIRM";
    this.codigoLegal = this.codigoEpica + "CDLEGAL";
    this.codigoNotificacion = this.codigoEpica + "EVDNOTI";

    this.listTipoDocumento();
    this.listadoCargaInforme();
  }

  listadoCargaInforme() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codResultado: this.codigoEpica,
      subCodResultado: "ENINEVA",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoCarga = element.idEvalResultado;
              this.radioSelect = element.codResultadoDet;
              if (element.codResultadoDet == "FAVO") {
                this.favorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == "DESF") {
                this.desFavorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == "OBSE") {
                this.observadoObj = new ResultadosEvaluacionDetalle(element);
              }
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  onChange(event: any) {
    this.listaDetalle = [];
    var obj = new ResultadosEvaluacionDetalle(event.obj);
    obj.codResultadoDet = event.codResultadoDet;
    obj.idUsuarioRegistro = this.user.idUsuario;

    this.listaDetalle.push(obj);

    var params = [
      {
        idEvalResultado: this.idEvalResultadoCarga,
        idPlanManejo: this.idPlanManejo,
        codResultado: this.codigoEpica,
        subCodResultado: "ENINEVA",
        descripcionCab: "Carga de Informe de Evaluación",
        codInforme: "radioSelect",
        idUsuarioRegistro: this.user.idUsuario,
        fechaConsentimiento: obj.fechaConsentimiento,
        listEvaluacionResultado: this.listaDetalle,
      },
    ];

    
    //return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoCargaInforme();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listTipoDocumento() {
    var params = {
      prefijo: "TDRESULTEVAL",
    };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listDocumentos = [...response.data];
      });
  }

  consolidadoPGMFA: any = null;

  generarDispositivoLegal() {
    this.createStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resumenService
      .generarDispositivoLegal(this.idPlanManejo, this.codigoEpica)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          this.consolidadoPGMFA = res;
          this.consolidadoPGMFA.contenTypeArchivo = "pdf";
          this.createStore.submitSuccess();
          this.descargarPGMFA();
          this.toast.ok("Se generó el archivo correctamente.");
        },
        (err) => {
          this.toast.warn(
            "Para generar el consolidado del Plan General de Manejo Forestal - PGMFA. Debe tener todos los items completados previamente."
          );
        }
      );
  }

  descargarPGMFA() {
    if (isNullOrEmpty(this.consolidadoPGMFA)) {
      this.toast.warn("Debe generar archivo consolidado");
      return;
    }
    descargarArchivo(this.consolidadoPGMFA);
  }
}
