import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastService } from '@shared';

@Component({
  selector: 'app-tab-verificar-ubicacion-espacial',
  templateUrl: './tab-verificar-ubicacion-espacial.component.html',
  styleUrls: ['./tab-verificar-ubicacion-espacial.component.scss']
})
export class TabVerificarUbicacionEspacialComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();
  @Input() disabled!: boolean;

  constructor(private toast: ToastService,) { }


  ngOnInit(): void {
  }
  openInformacionHistorica(){
    this.toast.warn(`No se cuenta con información histórica tabular  para el plan Nro: ${this.idPlanManejo}.`)
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

}
