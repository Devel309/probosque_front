import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CodigoEstadoPlanManejo} from '../../../../model/util/CodigoEstadoPlanManejo';
import {PlanManejoService, UsuarioService} from '@services';
import {Perfiles} from '../../../../model/util/Perfiles';

@Component({
  selector: 'app-requisitos-previos-pmfi-dema',
  templateUrl: './requisitos-previos-pmfi-dema.component.html',
  styleUrls: ['./requisitos-previos-pmfi-dema.component.scss']
})
export class RequisitosPreviosPmfiDemaComponent implements OnInit, AfterViewInit {

  idPlanManejo!: number;
  codigoEpica!: string;
  tabIndex: number = 0;
  disabled: boolean = false;
  perfil:string = "";
  obj: any ={};

  constructor(private route: ActivatedRoute,private planManejoService: PlanManejoService, private user: UsuarioService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.perfil = this.user.usuario.sirperfil;

    if(this.perfil == Perfiles.SERFOR || this.perfil == Perfiles.COMPONENTE_ESTADISTICO){
      this.disabled = true;
    }

  }

  ngOnInit(): void {

    /*  if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    } */



  /*this.route.queryParams
      .subscribe(params => {
        console.log(params);
        if(params.index !=null){
          this.tabIndex =  params.index;
        }else{
          this.tabIndex = 0;
        }
      }
        );*/

    this.obtenerPlan();
    //console.log("MAIN ngOnInit");
  }


  ngAfterViewInit(){
    if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    }
    //console.log("MAIN ngAfterViewInit");
  }
  setTabIndex(obj: any){
    var cod = obj.tab.replace(obj.codigoEvaluacionDet,'');
    //console.log("MAIN setTabIndex ", cod);

    if(cod === 'COD_0') this.tabIndex = 0;
    /* else if (cod === 'REEVRIIC') {
      this.tabIndex = 0;

      setTimeout (() => {
        console.log("Hello from setTimeout");
        this.tabIndex = 1;
      }, 1000);} */
    else if (cod === 'REEVRIIC') {this.tabIndex = 1;}
    else if (cod === 'COD_2') {this.tabIndex = 2;}
    else if (cod === 'RFNM') this.tabIndex = 3;
    else if (cod === 'COD_4') this.tabIndex = 4;
    else if (cod === 'INSOCU') this.tabIndex = 5;
    else if (cod === 'RREVAL') this.tabIndex = 6;
    else if (cod === 'COD_7') this.tabIndex = 7;
    else if (cod === 'COD_8') this.tabIndex = 8;
    else if (cod === 'COD_9') this.tabIndex = 9;
    else if (cod === 'COD_10') this.tabIndex = 10;
    else if (cod === 'COD_11') this.tabIndex = 11;
    else if (cod === 'COD_12') this.tabIndex = 12;
    else if (cod === 'COD_13') this.tabIndex = 13;
    else if (cod === 'COD_14') this.tabIndex = 14;
  }

  tab1(){
    this.tabIndex = 1;
  }

  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {
       this.codigoEpica = result.data.descripcion;

     /*  if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
        this.disabled = true;
      } */
    });

  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

}
