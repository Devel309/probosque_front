import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LazyLoadEvent } from 'primeng/api';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { EvaluacionListarRequest } from 'src/app/model/EvaluacionListarRequest';
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from 'src/app/model/util/CodigoEstadoPlanManejo';
import { CodigoPrerequisitos } from 'src/app/model/util/CodigoPrerequisitos';
import { Perfiles } from 'src/app/model/util/Perfiles';

@Component({
  selector: 'bandeja-eval-pmfi-dema',
  templateUrl: './bandeja-eval-pmfi-dema.component.html',
  styleUrls: ['./bandeja-eval-pmfi-dema.component.scss'],
})
export class BandejaEvalPmfiDemaComponent implements OnInit {
  @Input() evaluacionRequest!: EvaluacionListarRequest;
  @Input() planes!: any[];
  @Input() perfil!: any;
  @Input() descripcionPlan: any;
  @Input() totalRecords!: any;
  @Input() isBandejaEvaluacion!: boolean;
  @Output() limpiar = new EventEmitter();
  @Output() nuevo = new EventEmitter();
  @Output() load = new EventEmitter();
  @Output() buscar = new EventEmitter();
  @Output() evaluacionPlan = new EventEmitter();
  @Output() visualizarPlan = new EventEmitter();
  @Output() visualizarRequisitosPrevios = new EventEmitter();
  @Output() visualizarRequisitosMesaPartes = new EventEmitter();
  @Output() remitirPlan = new EventEmitter();

  isPerfilArffs = false;
  codigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  codigoEstadoMesaPartes = CodigoEstadoMesaPartes;
  codigoPrerequisitos = CodigoPrerequisitos;
  Perfiles = Perfiles;

  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  constructor(private activaRoute: ActivatedRoute) {
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    if (this.perfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilArffs = true;
    }
  }

  nuevoPlan() {
    this.nuevo.emit();
  }
  buscarPlan() {
    this.buscar.emit();
  }
  limpiarPlan() {
    this.limpiar.emit();
  }

  loadPlan(e: LazyLoadEvent) {
    this.load.emit(e);
  }

  evaluarPlan(idPlanManejo: any, tipoPlan: any) {
    this.evaluacionPlan.emit({ idPlanManejo, tipoPlan });
  }

  verPlan(idPlanManejo: any, tipoPlan: string) {
    this.visualizarPlan.emit({ idPlanManejo, tipoPlan });
  }

  verRequisitosPrevios(idPlanManejo: any, tipoPlan: string) {
    this.visualizarRequisitosPrevios.emit({ idPlanManejo, tipoPlan });
  }

  verRequisitosMesaPartes(idPlanManejo: any, tipoPlan: string) {
    this.visualizarRequisitosMesaPartes.emit({ idPlanManejo, tipoPlan });
  }

  registroNotificacionEmit(event: any) {
    this.buscarPlan();
  }

  remitePlan(event: any, idPlanManejo: any, codEstado: any) {
    this.remitirPlan.emit({ event, idPlanManejo, codEstado });
  }
}
