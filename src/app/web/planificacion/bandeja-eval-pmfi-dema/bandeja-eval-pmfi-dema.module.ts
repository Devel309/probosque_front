import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "@shared";
import { BandejaEvalPmfiDemaComponent } from "./bandeja-eval-pmfi-dema.component";
import { RequisitosPreviosPmfiDemaComponent } from "./requisitos-previos/requisitos-previos-pmfi-dema.component";
import { MatTabsModule } from "@angular/material/tabs";
import { RequisitosMesaPartesModule } from "./requisitos-mesa-partes/requisitos-mesa-partes.module";
import { TabPresentacionDemaPmfiComponent } from "./requisitos-previos/tabs/tab-presentacion-dema-pmfi/tab-presentacion-dema-pmfi.component";
import { TabEvaluacionInfoDoc } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/tab-evaluacion-info-doc";
import { TabVerificarInformacionPmffComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/tab-verificar-informacion-pmff.component";
import { AccordionModule } from "primeng/accordion";
import { TableModule } from "primeng/table";
import { RadioButtonModule } from "primeng/radiobutton";
import { FormsModule } from "@angular/forms";
import { OpinionesComponent } from "./requisitos-previos/tabs/tab-opiniones/opiniones.component";
import { TabOpinionesComponent } from "./requisitos-previos/tabs/tab-opiniones/tab-opiniones.component";
import { SolicitarOpicionEvaluacionModule } from "src/app/shared/components/solicitar-opicion-evaluacion/solicitar-opicion-evaluacion.module";
import { ToastModule } from "primeng/toast";
import { TabRegistrarResultados } from "./requisitos-previos/tabs/tab-registrar-resultados/tab-registrar-resultados.component";
import { TabVerificarUbicacionEspacialComponent } from "./requisitos-previos/tabs/tab-verificar-ubicacion-espacial/tab-verificar-ubicacion-espacial.component";
import { ButtonModule } from "primeng/button";
import { TabGestionarInspeccionComponent } from "./requisitos-previos/tabs/tab-gestionar-inspeccion/tab-gestionar-inspeccion.component";
import { TabSolicitarSubsanarComponent } from "./requisitos-previos/tabs/tab-solicitar-subsanar/tab-solicitar-subsanar.component";
import { TabRegistrarSubsanarComponent } from "./requisitos-previos/tabs/tab-registrar-subsanar/tab-registrar-subsanar.component";
import { TabRegistrarInformacionComponent } from "./requisitos-previos/tabs/tab-registrar-informacion/tab-registrar-informacion.component";
import { TabResolverSolicitudComponent } from "./requisitos-previos/tabs/tab-resolver-solicitud/tab-resolver-solicitud.component";
import { TabRemitirInformacionComponent } from "./requisitos-previos/tabs/tab-remitir-informacion/tab-remitir-informacion.component";
import { TabEvaluarDemaComponent } from "./requisitos-previos/tabs/tab-evaluar-dema/tab-evaluar-dema.component";
import { InputButtonsCodigoModule } from "src/app/shared/components/input-button-codigo/input-buttons.module";
import { TableHistoricoPlanManejoModule } from "src/app/shared/components/tabla-historico-plan-manejo/tabla-historico-plan-manejo.module";
import { VerticesGeograficosModule } from "src/app/shared/components/vertices-geograficos/vertices-geograficos.module";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { ArbolesAprovechablesComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/arboles-aprovechables/arboles-aprovechables.component";
import { CensoForestalMaderableComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/censo-forestal-maderable/censo-forestal-maderable.component";
import { FustalesComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/fustales/fustales.component";
import { InventarioNoMaderableComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/inventario-no-maderable/inventario-no-maderable.component";
import { CensoComercialNoMaderableComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/censo-comercial-no-maderable/censo-comercial-no-maderable.component";
import { anexosComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/anexos/tab-anexos.component";
import { RequisitoPgmfSimpleModule } from "src/app/shared/components/requisitos-tupa/requisitos-tupa.module";
import { PmfiCronogramaComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/cronograma/pmfi-cronograma.component";
import { TabCronogramaActividadesComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/cronograma/dema/tab-cronograma-actividades.component";
import { TabImpactosAmbientalesNegativosComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/impactosAmbientales/dema/tab-impactos-ambientales-negativos.component";
import { PmfiImpactoAmbientalNegativoComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/impactosAmbientales/pmfi/pmfi-impacto-ambiental-negativo.component";
import { TabMedidasProteccionUnidadManejoForestalComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/629MedidasProteccion/dema/tab-medidas-proteccion-unidad-manejo-forestal.component";
import { PmfiManejoAprovechamientoComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/628LaboresSilviculturales/pmfi/pmfi-manejo-aprovechamiento.component";
import { TabSistemaManejoLaboralesSilviculturalesComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/628LaboresSilviculturales/dema/tab-sistema-manejo-laborales-silviculturales.component";
import { LaboresSilviculturalesFormComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/628LaboresSilviculturales/pmfi/labores-silviculturales-form/labores-silviculturales-form.component";
import { CheckboxModule } from "primeng/checkbox";
import { TabActividadesAprovechamientoEquiposComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/dema/tab-actividades-aprovechamiento-equipos.component";
import { PmfiManejoAprovechamiento2Component } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/pmfi/pmfi-manejo-aprovechamiento.component";
import { AprovechamientoFormComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/pmfi/aprovechamiento-form/aprovechamiento-form.component";
import { TabRecursosForestalesMaderablesComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/627ActividadesAprovechamiento/dema/maderable/tab-recursos-forestales-maderables.component";
import { TabZonificacionOrdenamientoInternoAreaComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/625OrdenamientoInterno/dema/tab-zonificacion-ordenamiento-interno-area.component";
import { PmfiOrdenamientoInternoComponent } from "./requisitos-previos/tabs/tab-verificar-informacion-pmff/625OrdenamientoInterno/pmfi/pmfi-ordenamiento-interno.component";
import { ResultadoEvaluacionSharedModule } from "../../../shared/components/resultado-evaluacion/resultado-evaluacion-shared.module";
import { OsinforSharedModule } from "../../../shared/components/osinfor-shared/osinfor-shared.module";
import { EspeciesCensadasModule } from "src/app/shared/components/especies-censadas/especies-censadas.module";
import { ListaRegenteComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/lista-regente/lista-regente.component";
import {SolicitudSanSharedModule} from '../../../shared/components/solicitud-san-shared/solicitud-san-shared.module';
import { ConfirmPopupModule } from "primeng/confirmpopup";
import { DialogModule } from "primeng/dialog";
import { PaginatorModule } from "primeng/paginator";
import { HistoricoTabularComponent } from "./requisitos-previos/tabs/tab-verificar-ubicacion-espacial/components/historico-tabular/historico-tabular.component";
import { ResultadoEvaluacionTitularSharedModule } from "src/app/shared/components/resultado-evaluacion-titular/resultado-evaluacion-titular-shared.module";
import { ConsultaSUNARPComponent } from "./requisitos-previos/tabs/tab-evaluacion-info-doc/components/consulta-sunarp/consulta-sunarp.component";
import {EvaluarImpugnacionSharedModule} from '../../../shared/components/evaluar-impugnacion-shared/evaluar-impugnacion-shared.module';
import {InputButtonsModule} from '../../../shared/components/input-button/input-buttons.module';
import {InformeEvaluacionSharedModule} from '../../../shared/components/informe-evaluacion-shared/informe-evaluacion-shared.module';
import { ProgressBarModule } from "primeng/progressbar";

@NgModule({
  declarations: [
    BandejaEvalPmfiDemaComponent,
    RequisitosPreviosPmfiDemaComponent,
    TabPresentacionDemaPmfiComponent,
    TabEvaluacionInfoDoc,
    TabVerificarInformacionPmffComponent,
    OpinionesComponent,
    TabOpinionesComponent,
    TabRegistrarResultados,
    TabVerificarUbicacionEspacialComponent,
    TabGestionarInspeccionComponent,
    TabSolicitarSubsanarComponent,
    TabRegistrarSubsanarComponent,
    TabRegistrarInformacionComponent,
    TabResolverSolicitudComponent,
    TabRemitirInformacionComponent,
    TabEvaluarDemaComponent,
    ArbolesAprovechablesComponent,
    CensoComercialNoMaderableComponent,
    CensoForestalMaderableComponent,
    FustalesComponent,
    InventarioNoMaderableComponent,
    PmfiCronogramaComponent,
    anexosComponent,
    TabCronogramaActividadesComponent,
    TabImpactosAmbientalesNegativosComponent,
    PmfiImpactoAmbientalNegativoComponent,
    TabMedidasProteccionUnidadManejoForestalComponent,
    PmfiManejoAprovechamientoComponent,
    TabSistemaManejoLaboralesSilviculturalesComponent,
    LaboresSilviculturalesFormComponent,
    TabActividadesAprovechamientoEquiposComponent,
    PmfiManejoAprovechamiento2Component,
    AprovechamientoFormComponent,
    TabRecursosForestalesMaderablesComponent,
    TabZonificacionOrdenamientoInternoAreaComponent,
    PmfiOrdenamientoInternoComponent,
    ListaRegenteComponent,
    HistoricoTabularComponent,
    ConsultaSUNARPComponent
  ],
  exports: [BandejaEvalPmfiDemaComponent, RequisitosPreviosPmfiDemaComponent, TabPresentacionDemaPmfiComponent, OpinionesComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatTabsModule,
    RequisitosMesaPartesModule,
    AccordionModule,
    TableModule,
    RadioButtonModule,
    FormsModule,
    SolicitarOpicionEvaluacionModule,
    ToastModule,
    ButtonModule,
    InputButtonsCodigoModule,
    TableHistoricoPlanManejoModule,
    VerticesGeograficosModule,
    MatDatepickerModule,
    RequisitoPgmfSimpleModule,
    AccordionModule,
    CheckboxModule,
    ResultadoEvaluacionSharedModule,
    ResultadoEvaluacionTitularSharedModule,
    OsinforSharedModule,
    EspeciesCensadasModule,
    SolicitudSanSharedModule,
    ConfirmPopupModule,
    DialogModule,
    PaginatorModule,
    EvaluarImpugnacionSharedModule,
    InputButtonsModule,
    InformeEvaluacionSharedModule,
    ProgressBarModule
  ],
})
export class BandejaEvalPmfiDemaModule {}
