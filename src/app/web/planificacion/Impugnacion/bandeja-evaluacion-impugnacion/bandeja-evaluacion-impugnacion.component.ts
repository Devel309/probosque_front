import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { PlanManejoService, UsuarioService } from '@services';
import { ConvertDateStringFormat, ConvertDateToString, ConvertNumberToDate, DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { ListarImpugnacionRequest } from 'src/app/model/Impugnacion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { TipoDocGestion } from 'src/app/model/util/TipoDocGestion';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ImpugnacionService } from 'src/app/service/impugnacion.service';
import { ResolucionService } from 'src/app/service/resolucion.service';

@Component({
  selector: 'app-bandeja-evaluacion-impugnacion',
  templateUrl: './bandeja-evaluacion-impugnacion.component.html',
  styleUrls: ['./bandeja-evaluacion-impugnacion.component.scss']
})
export class BandejaEvaluacionImpugnacionComponent implements OnInit {

  verModalImpugnacion: boolean = false;
  tipoEvaluacion: string = "";
  resultado: any = null;
  retrotraer: any = null;
  listarImpugnacionRequest: ListarImpugnacionRequest = new ListarImpugnacionRequest();

  impugnaciones: any[] = [];

  totalRecords = 0;

  comboEstado: any[] = [];
  comboTipodocGestion: any[] = [];

  resolusiones: any = [];

  usuario!: UsuarioModel;

  impugnacion: any = {};

  archivoRecurso: any = {};
  archivoSustento: any = {};
  archivoEvaluacion: any = {};
  archivoFirme: any = {};
  disabled: boolean = false;
  disabledGuardar: boolean = false;
  disabledFileFirmeza: boolean = false;
  hiddenFirmeza: boolean = false;
  isDocGestion: boolean = false;
  isEditArffs: boolean = false;

  disabledFirmeza: boolean = false;
  fechaFiremza: string = "";
  fecha:String ="";
  minDate!: Date;
  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  codigoPlan!: string;

  constructor(
    private impugnacionServ: ImpugnacionService,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    private usuarioServ: UsuarioService,
    private resolusionServ: ResolucionService,
    private messageService: MessageService,
    private activaRoute: ActivatedRoute,
    private planManejoService: PlanManejoService,
    private evaluacionService: EvaluacionService,
    public datepipe: DatePipe
  ) {
    let storage = localStorage.getItem('bandejaevaluacionimpugnacion');
    if (storage) {
      this.listarImpugnacionRequest.nroResolucion = JSON.parse(storage).idResolucion;
      localStorage.removeItem('bandejaevaluacionimpugnacion');
    }

    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    this.listarComboEstado();
    this.listarComboTipoDocGestion();
    this.listarImpungaciones();
    this.minDate = new Date(2022, 0, 1);
  }

  listarImpungaciones() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.usuario.sirperfil == 'ARFFS')
      this.listarImpugnacionRequest.perfil = this.usuario.sirperfil;

    this.impugnacionServ.listarImpugnacion(this.listarImpugnacionRequest).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data?.length > 0) {
        this.impugnaciones = result.data;
        this.totalRecords = result.totalRecord;
      } else {
        this.impugnaciones = [];
        this.totalRecords = 0;
        this.ErrorMensaje(result.message);
      }
    }, () => this.dialog.closeAll())

  }

  listarComboEstado() {
    const params = { prefijo: "EIMP" }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboEstado = result.data;
    })
  }

  listarComboTipoDocGestion() {
    const params = { prefijo: "TDG" }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboTipodocGestion = result.data;
    })
  }

  listarResolucion() {

    let params = {}

    this.resolusionServ.listarResolucion(params).subscribe((result: any) => {
      this.resolusiones = result.data;
    })
  }

  loadData(event: any) {
    this.listarImpugnacionRequest.pageNum = event.first + 1;
    this.listarImpugnacionRequest.pageSize = event.rows;

    this.listarImpungaciones();
  }


  buscarImpugnacion() {
    this.listarImpungaciones();
  }

  limpiarImpugnacion() {
    this.listarImpugnacionRequest = new ListarImpugnacionRequest();
    this.listarImpungaciones();
  }


  abrirImpugnacion(data: any, disabled: boolean) {

    this.resultado = null;
    this.retrotraer = null;

    this.tipoEvaluacion = "";
    this.disabled = disabled;
    this.disabledGuardar= disabled;
    let params = { idImpugnacion: data.idImpugnacion };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionServ.obtenerImpugnacion(params).subscribe((result: any) => {

      if (result.data) {

        this.impugnacion = {
          ...result.data,
          fechaImpugnacion: data.fechaImpugnacion ? new Date(data.fechaImpugnacion) : data.fechaImpugnacion,
        }
        


        this.hiddenFirmeza=false;
        this.disabledFileFirmeza=true;


        if(this.usuario.sirperfil == 'ARFFS'&& this.impugnacion.estadoImpugnacion=="EIMPFIRME"){

          this.fechaFiremza =
          ConvertDateStringFormat(
            ConvertNumberToDate(result.data.fechaRegistroFirme)
          );
          //this.fecha=this.datepipe.transform(this.fechaFiremza, 'dd/MM/yyyy');

          
          this.disabled = true;
          this.disabledGuardar= false;
          this.hiddenFirmeza=true;
          this.disabledFileFirmeza=false;
          if(this.impugnacion.idArchivoFirme){
            this.archivoFirme.nombre = result.data.nombreArchivoFirme;
            this.archivoFirme.idArchivo = result.data.idArchivoFirme;
            this.disabledGuardar= true;
            this.disabledFileFirmeza=true;
          }

        }

        this.archivoRecurso.nombre = result.data.nombreArchivoRecurso;
        this.archivoRecurso.idArchivo = result.data.idArchivoRecurso;

        this.archivoSustento.nombre = result.data.nombreArchivoSustento;
        this.archivoSustento.idArchivo = result.data.idArchivoSustento;

        this.archivoEvaluacion.nombre = result.data.nombreArchivoEvaluacion;
        this.archivoEvaluacion.idArchivo = result.data.idArchivoEvaluacion;

        this.tipoEvaluacion = result.data.tipoImpugnacion;
        this.resultado = result.data.esfundado;
        this.retrotraer = result.data.retrotraer;

        this.isDocGestion = result.data.tipoDocGestion === TipoDocGestion.PROCESO_OFERTA;
        if (this.isDocGestion) this.tipoEvaluacion = 'TIMPREC';

        this.verModalImpugnacion = true;
        this.dialog.closeAll();

      } else {
        this.dialog.closeAll();
        this.ErrorMensaje("Ocurrió un error al obtener la impugnacion");
      }
    }, () => {
      this.dialog.closeAll();
      this.ErrorMensaje("Ocurrió un error al obtener la impugnacion");
    })
  }

  guardar() {
    if(this.usuario.sirperfil == 'ARFFS' && this.impugnacion.estadoImpugnacion=="EIMPFIRME"){
      if(!this.archivoFirme.nombre){
        this.ErrorMensaje("(*) Debe adjuntar documento de Firmeza de Impugnación.");
        return;
      }
    } else {
      if (!this.validarImpugnacion()) return;
    }

    if (this.tipoEvaluacion === 'TIMPREC') {
      this.obtenerPlanManejo();
    } else {
      this.guardarImpugnacion();
    }
  }

  guardarImpugnacion() {
  //   if(this.usuario.sirperfil == 'ARFFS' && this.impugnacion.estadoImpugnacion=="EIMPFIRME"){
  //     if(!this.archivoFirme.nombre){
  //       this.ErrorMensaje("(*) Debe adjuntar documento de Firmeza de Impugnación.");
  //       return;
  //     }
  //   } else {
  //     if (!this.validarImpugnacion()) return;
  //   }

    let params = {
      idImpugnacion: this.impugnacion.idImpugnacion,
      tipoImpugnacion: this.tipoEvaluacion,
      esfundado: this.resultado==null?false:this.resultado,
      retrotraer: this.retrotraer || false,
      idUsuarioModificacion: this.usuario.idusuario,
      fileEvaluacion: this.archivoEvaluacion.file || null,
      fileFirme: this.archivoFirme.file || null,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionServ.actualizarImpugnacion(params).pipe(tap((res: any) => {
      if (res.body?.success) {
        // this.SuccessMensaje(res.body.message);
        this.SuccessMensaje('Se actualizó la impugnación correctamente.');
        this.verModalImpugnacion = false;
        this.listarImpungaciones();
      }
    }, error => {
      this.ErrorMensaje("Ocurrió un error");
    })).pipe(finalize(() => {
      this.dialog.closeAll()
    }))
      .subscribe();

  }


  validarImpugnacion() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.archivoRecurso.nombre) {
      validar = false;
      mensaje = mensaje +=
        '(*) Falta adjuntar recurso de impugnación.\n';
    }

    // if (!this.archivoSustento.nombre) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Falta adjuntar sustento extra.\n';
    // }

    if (this.tipoEvaluacion == "") {
      validar = false;
      mensaje = mensaje +=
        '(*) Seleccione tipo evaluacion.\n';
    }

    if (this.resultado == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Seleccione el Resultado de la Impugnación.\n';
    }

    if (!this.archivoEvaluacion.nombre && this.tipoEvaluacion === 'TIMPREC') {
      validar = false;
      mensaje = mensaje +=
        '(*) Adjunte el Archivo del Resultado de la Evaluación.\n';
    }


    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  obtenerPlanManejo() {
    var params = {
      idPlanManejo: this.impugnacion.nroDocGestion,
    };
    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((response: any) => {
        if (!!response.data.descripcion) {
          this.codigoPlan = response.data.descripcion;
          this.generarDescargarReporte();
        }
      });
  }

  generarDescargarReporte() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .generarInformeDEMAPMFI(this.impugnacion.nroDocGestion, this.codigoPlan)
      .subscribe((data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, '');
        this.guardarImpugnacion()
      });
  }

  eliminarArchivoEval() {
    this.archivoEvaluacion = {};
  }
  eliminarArchivoFirmeza() {
    this.archivoFirme = {};
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }



}
