import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { max } from 'lodash';
import { MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ImpugnacionRegistrar, ListarImpugnacionRequest } from 'src/app/model/Impugnacion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { GenericoService } from 'src/app/service/generico.service';
import { ImpugnacionService } from 'src/app/service/impugnacion.service';
import { NotificacionService } from 'src/app/service/notificacion';
import { ResolucionService } from 'src/app/service/resolucion.service';

@Component({
  selector: 'app-bandeja-solicitud',
  templateUrl: './bandeja-solicitud.component.html',
  styleUrls: ['./bandeja-solicitud.component.scss']
})
export class BandejaSolicitudComponent implements OnInit {
  minFecha: any = null;
  listarImpugnacionRequest: ListarImpugnacionRequest = new ListarImpugnacionRequest();
  CodigoProceso = CodigoProceso;
  impugnaciones: any[] = [];

  totalRecords = 0;

  comboEstado: any[] = [];
  comboTipodocGestion: any[] = [];

  resolusiones: any = [];

  usuario!: UsuarioModel;

  impugnacion: any = {};

  archivoRecurso: any = {};
  archivoSustento: any = {};


  accion: string = "";
  tituloModal: string = "";
  verModalSolicitud: boolean = false;
  abriModal(tipo: string, data: any): void {
    this.accion = tipo;
    this.impugnacion = {};
    this.archivoSustento = {};
    this.archivoRecurso = {};

    if (tipo == 'C') {
      this.tituloModal = "Nueva Impugnación";
    } else {
      this.obtenerImpugnacion(data);
      this.tituloModal = "Ver Impugnación";
    }
    this.verModalSolicitud = true;
  }

  constructor(private impugnacionServ: ImpugnacionService,
    private dialog: MatDialog,
    private genericoServ: GenericoService,
    private usuarioServ: UsuarioService,
    private resolusionServ: ResolucionService,
    private notificacionService: NotificacionService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;

    this.listarResolucion();
    this.listarComboEstado();
    this.listarComboTipoDocGestion();
    let storage = localStorage.getItem('bandejasolicitudimpugnacion');
    if (storage != null) {
      this.listarImpugnacionRequest.nroResolucion = JSON.parse('' + localStorage.getItem('bandejasolicitudimpugnacion')).idResolucion;
      localStorage.removeItem('bandejasolicitudimpugnacion');
    }
    this.listarImpungaciones();
  }


  listarImpungaciones() {
    this.listarImpugnacionRequest.idUsuarioRegistro = this.usuario.idusuario;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionServ.listarImpugnacion(this.listarImpugnacionRequest).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.success && result.data?.length > 0){
        this.impugnaciones = result.data;
        this.totalRecords = result.totalRecord;
      }else{
        this.impugnaciones = [];
        this.totalRecords = 0;
        this.ErrorMensaje(result.message);
      }
    }, () => this.dialog.closeAll())

  }

  listarComboEstado() {
    var params = { prefijo: "EIMP" }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboEstado = result.data;
    })
  }

  listarComboTipoDocGestion() {
    const params = { prefijo: "TDG" }
    this.genericoServ.listarPorFiltroParametro(params).subscribe((result: any) => {
      this.comboTipodocGestion = result.data;
    })
  }

  listarResolucion() {
    let params = {
      estadoResolucion: "ERESGEN"
    }

    this.resolusionServ.listarResolucion(params).subscribe((result: any) => {
      this.resolusiones = result.data;
    })
  }

  loadData(event: any) {
    this.listarImpugnacionRequest.pageNum = event.first + 1;
    this.listarImpugnacionRequest.pageSize = event.rows;

    this.listarImpungaciones();
  }

  buscarImpugnacion() {
    this.listarImpungaciones();
  }

  limpiarImpugnacion() {
    this.listarImpugnacionRequest = new ListarImpugnacionRequest();
    this.listarImpungaciones();
  }


  obtenerImpugnacion(data: any) {

    let params = {
      idImpugnacion: data.idImpugnacion
    }

    this.impugnacionServ.obtenerImpugnacion(params).subscribe((result: any) => {
      if (result.data) {
        this.impugnacion = {
          ...result.data,
          fechaImpugnacion: data.fechaImpugnacion ? new Date(data.fechaImpugnacion) : data.fechaImpugnacion,
        }
        this.archivoRecurso.nombre = result.data.nombreArchivoRecurso;
        this.archivoRecurso.idArchivo = result.data.idArchivoRecurso;

        this.archivoSustento.nombre = result.data.nombreArchivoSustento;
        this.archivoSustento.idArchivo = result.data.idArchivoSustento;
      }
    }, error => {
      this.ErrorMensaje("Ocurrió un error al obtener la impugnacion");
    })

  }

  agregarImpugnacion() {
    this.impugnacion = {};
    this.impugnacion.archivoRecurso = {};
    this.impugnacion.archivoSustento = {};

    this.abriModal('C', null);
  }

  remitir() {
   if (!this.validarImpugnacion()) return;

    let params: ImpugnacionRegistrar = {
      asunto: this.impugnacion.asunto,
      descripcion: this.impugnacion.descripcion,
      fechaImpugnacion: this.impugnacion.fechaImpugnacion,
      idResolucion: this.impugnacion.idResolucion,
      fileRecurso: this.archivoRecurso.file,
      fileSustento: this.archivoSustento.file,
      idUsuarioRegistro: this.usuario.idusuario,
      region: this.usuario.region,
    }
   this.dialog.open(LoadingComponent, { disableClose: true });

    this.impugnacionServ.registrarImpugnacion(params).pipe(tap((res: any) => {
      if (res.body?.success) {
        this.SuccessMensaje(res.body.message);
        this.verModalSolicitud = false;
        this.listarImpungaciones();

        let item = this.impugnaciones.find(
          (x) => x.idImpugnacion == Math.max(x.idImpugnacion)
        );
        
        this.registrarNotificacion(item.idImpugnacion);
         }
    }, error => {
      this.ErrorMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

   registrarNotificacion(Int : any) {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: "EPLMIMP",
      numDocgestion: Int,
      mensaje: 'Perfil TITULARTH Ha solicitado una impungnación con'
        + ' Nro. '
        + Int,
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.usuario.idusuario,
      //codigoPerfil: this.user.usuario.sirperfil,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 15,
      url: '/planificacion/bandeja-evaluacion-impugnacion',
      idUsuarioRegistro: this.usuario.idusuario
    }
    
    this.notificacionService
      .registrarNotificacion(params)
      .subscribe()
  }

  eliminarFileRecurso() {
    this.archivoRecurso = {};
  }

  eliminarFileSustento() {
    this.archivoSustento = {};
  }

  validarImpugnacion() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.impugnacion.idResolucion == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Seleccione una resolución.\n';
    }

    if (this.impugnacion.asunto == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Ingrese el asunto.\n';
    }

    if (this.impugnacion.descripcion == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Ingrese una descripción.\n';
    }

    if (this.impugnacion.fechaImpugnacion == null) {
      validar = false;
      mensaje = mensaje +=
        '(*) Ingrese la fecha de impugnación.\n';
    }
    
    if (!this.archivoRecurso.nombre) {
      validar = false;
      mensaje = mensaje +=
        '(*) Adjunte Archivo Recurso.\n';
    }
    
    // if (!this.archivoSustento.nombre) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Adjunte Archivo Sustento.\n';
    // }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }




  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }


}
