import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'app-solicitud-recurso-inpugacion',
  templateUrl: './solicitar-recurso-inpugnacion.component.html',
  styleUrls: ['./solicitar-recurso-inpugnacion.component.scss']
})
export class SolicitudRecursoInpugnacionComponent implements OnInit {

  lista: any[] = [
    { a: "inpugnado", b: "16-GTH52", c: "15/06/2021", remitido: false, }, { a: "inpugnacion resuelta", b: "17-GTH52", c: "18/06/2021", remitido: false }
  ];
  display: boolean = false;
  display2: boolean = false;


  constructor() { }

  ngOnInit() {
  }

  notificacion() {
    this.display = true;
  }

  resolucion(index: number) {
    this.lista[index].remitido = true;
    this.display2 = true;
  }

}