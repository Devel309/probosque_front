import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { PlanificacionService } from 'src/app/service/planificacion.service';

@Component({
  selector: 'app-modal-validar-anexo',
  templateUrl: './modal-validar-anexo.component.html',
})
export class ModalValidarAnexoComponent implements OnInit {
  minDate: Date = new Date();
  params_validar_anexo: any = {
    descripcion: '',
    devolucion: null,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: null,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };
  isSolCancel: boolean = false;

  constructor(
    private serv: PlanificacionService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.minDate.setDate(this.minDate.getDate() + 2);
    this.params_validar_anexo.idDocumentoAdjunto = this.config.data.idDocumentoAdjunto;
    this.params_validar_anexo.tipodoc = this.config.data.tipodoc;
    this.isSolCancel = this.config.data.isSolCancel;
  }

  enviar_validarAnexo() {
    /*if (this.params_validar_anexo.descripcion === '') {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'NOTA',
        detail: 'Debe ingresar una Descripción',
      });
    } else */if(this.params_validar_anexo.devolucion === null){
      this.messageService.add({
        key: 'dc',
        severity: 'warn',
        summary: 'NOTA',
        detail: 'Debe seleccionar conforme u observado',
      });
    }
    /*else if (
      this.params_validar_anexo.devolucion &&
      this.params_validar_anexo.fechaFinPlazo == null && this.params_validar_anexo.tipodoc != 'anexo1'
    ) {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'NOTA',
        detail: 'Debe indicar una Fecha Fin de Plazo',
      });
    }*/ else if (
      this.params_validar_anexo.devolucion &&
      this.params_validar_anexo.observacion === ''
    ) {
      this.messageService.add({
        key: 'dc',
        severity: 'warn',
        summary: 'NOTA',
        detail: 'Debe ingresar una observación',
      });
    } else {
      if (!this.params_validar_anexo.devolucion)
        this.params_validar_anexo.fechaFinPlazo = null;

      this.serv
        .validarAnexo(this.params_validar_anexo)
        .subscribe((result: any) => {
          if (result.success) {
            this.ref.close(this.params_validar_anexo.devolucion);
          } else {
            this.messageService.add({
              key: 'dc',
              severity: 'error',
              summary: 'ERROR',
              detail: result.message,
            });
          }
        });
    }
  }
}
