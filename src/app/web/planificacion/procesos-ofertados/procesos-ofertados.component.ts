import {
  Component,
  ViewChild,
  ElementRef,
  Renderer2,
  OnInit,
  Inject,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { datosUsuario_parcial } from 'src/app/model/user';
import { PlanificacionService } from 'src/app/service/planificacion.service';

import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import GraphicsLayer from '@arcgis/core/layers/GraphicsLayer';
import Graphic from '@arcgis/core/Graphic';

import * as wkt from 'terraformer-wkt-parser';
import { features } from 'process';
import { create } from 'domain';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { EstatusProcesoService } from 'src/app/service/estatusProceso.service';
import { EstadoProcesoModel } from 'src/app/model/EstadoProcesoModel';
import { ModalRegistroSolicitudesComponent } from '../otorgamiento-derechos/modal-registro-solicitudes/modal-registro-solicitudes.component';
import { DialogService, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { UsuarioService } from '@services';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';

@Component({
  selector: 'app-procesos-ofertados',
  templateUrl: './procesos-ofertados.component.html',
  styleUrls: ['./procesos-ofertados.component.scss'],
  providers: [MessageService],
})
export class ProcesosOfertadosComponent implements OnInit {
  permisos: IPermisoOpcion = {} as IPermisoOpcion;

  procesoOfertaSelect!: String;
  estadoProcesoSelect!: SelectItem;
  
  isFecha: boolean = false; 

  procesosOfertaFiltros: any = {
    idProceso: 0,
    razonSocial: '',
    ruc: '',
    estado: '',
  };
  lstProcesosOferta: any[] = [];
  usarioActual: datosUsuario_parcial = JSON.parse(
    '' + localStorage.getItem('usuario')?.toString()
  );

  j: number = 0;
  k: number = 0;
  
  
  constructor(
    private usuarioServ: UsuarioService,
    private serv: PlanificacionService,
    private router: Router,
    private activaRoute: ActivatedRoute,
    public dialog: MatDialog,
    public dialogService: DialogService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private anexoService: AnexoService,
    private estatusProcesoServ: EstatusProcesoService
  ) {
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
    
  }

  estadoProceso = {} as EstadoProcesoModel;
  listaEstadoProceso: EstadoProcesoModel[] = [];
  usuario!: UsuarioModel;
  listarGrid: any = {pageNum :1,pageSize: 10};
  totalRecords = 0;
  

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    localStorage.removeItem('bandejapostulacionrevisar');
    this.listarProcesoOferta();
    this.listarEstado();
    // this.permisos.isPerEditar
    // this.CopiaProcesoOferta();
  }





  // procesoOferta: any[] = [];
  // CopiaProcesoOferta() {
  //   var params = {
  //     agrupada: null,
  //     idProcesoOferta: null,
  //     idStatusProceso: '13,2',
  //     idTipoProceso: null,
  //   };
  //   console.log(params);

  //   this.serv.listarProcesoOferta(params).subscribe((result: any) => {
  //     result.data.splice(0, 0, {
  //       idProcesoOferta: '-- Seleccione --',
  //     });
  //     this.procesoOferta = result.data;
  //   });

  // }

  procesosPostular: any = [];
  seleccionarItem(obj: any) {
    let existeOferta = this.procesosPostular.filter(
      (x: any) => x.idProcesoOferta === obj.idProcesoOferta
    );
    if (existeOferta.length > 0)
      this.procesosPostular = this.procesosPostular.filter(
        (x: any) => x.idProcesoOferta != obj.idProcesoOferta
      );
    else {
      this.procesosPostular.push(obj);
    }

    // let index = this.procesosPostular.indexOf(obj)
    //   if(index == -1){
    //     this.procesosPostular.push(obj);
    //   }
    //   else{
    //     this.procesosPostular.splice(index, 1);
    //   }
  }

  listarEstado() {
    let params = { tipoStatus: 'PROCESO' }
    this.estatusProcesoServ.listarEstadoEstatusProceso(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        tipoStatus: '',
        idStatusProceso: 0,
        descripcion: '-- Seleccione --'});
      this.listaEstadoProceso = result.data;
      
    })
  }

  loadData(event: any) {
    this.listarGrid.pageNum = event.first + 1;
    this.listarGrid.pageSize = event.rows;

    this.listarProcesoOferta(true);
  }

  listarProcesoOferta(estado?: any) {
    if(estado==undefined){
      this.listarGrid= {pageNum :1,pageSize: 10};
    }
    
    var params = {
      agrupada: null,
      idProcesoOferta: this.procesoOfertaSelect ? this.procesoOfertaSelect : null,
      idStatusProceso: this.estadoProceso.idStatusProceso ? this.estadoProceso.idStatusProceso : null,
      idTipoProceso: null,
      pageNum: this.listarGrid.pageNum,
      pageSize: this.listarGrid.pageSize,
      idUsuarioConsulta: this.usuario.idusuario

    };
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.listarProcesoOferta(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.success && result.data.length > 0){
        this.lstProcesosOferta = result.data;
        if(this.lstProcesosOferta) this.totalRecords = result.totalRecord;
      }else{
        this.lstProcesosOferta = [];
        this.totalRecords = 0;
        this.messageService.add({
          key: 'tl',
          severity: 'warn',
          summary: '',
          detail: "No se encontraron registros",
        });
      }
    },()=>{
      this.dialog.closeAll();
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: '',
        detail: "Ocurrió un problema, intente nuevamente",
      });
    });
  }
  
  limpiarFiltro(){
    this.estadoProceso.idStatusProceso = 0;
    this.procesoOfertaSelect = '';
    this.listarProcesoOferta();

  }

  postular() {
    let _this = this;
    if (this.procesosPostular.length > 0) {
      ;
      
      let arrAux:number[] = [];
      var mensajeModal = '';
      _this.procesosPostular.forEach((element: any) => {
        arrAux.push(element.idProcesoOferta);
      });
      mensajeModal = arrAux.join(', ');

      let msjNumOfertas = '';
      if(_this.procesosPostular.length === 1){
        msjNumOfertas = '¿Está seguro de postular al siguiente proceso en oferta  ' + mensajeModal + ' ?';
      }else{
        msjNumOfertas = '¿Está seguro de postular a los siguientes procesos en oferta  ' + mensajeModal + ' ?';
      }

      this.confirmationService.confirm({
        message: msjNumOfertas,
        header: 'Confirmación',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          _this.j = 0;
          _this.procesosPostular.forEach((element: any) => {
            let params = {
              anexo1: null,
              anexo2: null,
              anexo3: null,
              idProcesoPostulacion: element.idProcesoPostulacion || 0,
              idUsuarioPostulacion: JSON.parse(
                '' + localStorage.getItem('usuario')
              ).idusuario,
              procesopostulacion: {
                fechaPostulacion: new Date(),
                fechaRecepcionDocumentos: new Date(),
                idEstatusProceso: 3,
                idProcesoOferta: element.idProcesoOferta,
                idProcesoPostulacion: 0,
                idRegenteForestal: 33,
                idUsuarioPostulacion: JSON.parse(
                  '' + localStorage.getItem('usuario')
                ).idusuario,
                idUsuarioRegistro: JSON.parse(
                  '' + localStorage.getItem('usuario')
                ).idusuario,
                observacion: '',
                recepcionDocumentos: false,
              },
            };

            _this.serv
              .guardaProcesoPostulacion(params)
              .subscribe((result: any) => {
                if (result.success) {
                  element.idProcesoPostulacion = result.codigo;
                  _this.insertarStatusAnexo(
                    'anexo1',
                    element.idProcesoPostulacion
                  );
                  _this.insertarStatusAnexo(
                    'anexo2',
                    element.idProcesoPostulacion
                  );
                  _this.insertarStatusAnexo(
                    'anexo3',
                    element.idProcesoPostulacion
                  );
                  _this.insertarStatusAnexo(
                    'anexo4',
                    element.idProcesoPostulacion
                  );
                  _this.insertarStatusAnexo(
                    'declaracionjurada',
                    element.idProcesoPostulacion
                  );
                  _this.insertarStatusAnexo(
                    'otrosanexos',
                    element.idProcesoPostulacion
                  );
                  // if (_this.procesosPostular.length === _this.j) {
                  //   _this.k = 0;
                  //   _this.procesosPostular.forEach((element: any) => {

                  //   });
                  // }
                  this.router.navigate(['/planificacion/bandeja-postulacion']);
                } else {
                  _this.messageService.add({
                    key: 'tl',
                    severity: 'error',
                    summary: '',
                    detail: 'Ocurrió un problema, intente nuevamente',
                  });
                }
              });
          });
        },
        reject: (type: any) => {},
      });
    } else {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: '',
        detail: '(*) Debe elegir al menos un proceso',
      });
    }
  }

  insertarStatusAnexo = (codigo: string, idProcesoPostulacion: number) => {
    var params = {
      codigoAnexo: codigo,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: idProcesoPostulacion,
      idStatusAnexo: 18,
      idTipoDocumento: null,
      idUsuarioCreacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService.insertarEstatusAnexo(params).subscribe((result: any) => {
      this.k = this.k + 1;
      if (result.success && this.k === this.procesosPostular.length * 6) {
        let mensaje = 'Se postulo a los procesos en oferta: ';
        this.procesosPostular.forEach((element: any) => {
          mensaje += element.idProcesoOferta + ',';
        });
        mensaje = mensaje.toString().slice(0, -1);
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'DE ACUERDO',
          detail: mensaje,
        });
        this.procesosPostular = [];
        this.listarProcesoOferta();
      }
    });
  };



  openDialog(idProOferta: number) {
    
    const ref = this.dialogService.open(ConsultaUAMapaComponent, {
      header: 'Visualizar Unidad de Aprovechamiento',
      height: '82%',
      width: '700px',
      data: {
        idProcesoOferta: idProOferta,
      },
    });
    ref.onClose.subscribe((result: any) => {
   // ref.afterClosed().subscribe((result) => {
      
    });
  }



}
@Component({
  selector: 'app-procesos-ofertados',
  templateUrl: './consultar-ua-mapa.component.html',
  styleUrls: ['./procesos-ofertados.component.scss'],
})
export class ConsultaUAMapaComponent implements OnInit {
  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  constructor(
    private serv: PlanificacionService,
    public config: DynamicDialogConfig,
    private messageService: MessageService
  ) {}
  public view: any = null;
  public graphicsLayer: any = null;
  ngOnInit() {
    this.initializeMap();
    this.consultarUaById();
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '467px';
    container.style.width = '100%';
    const map = new Map({
      basemap: 'hybrid',
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }
  consultarUaById() {
    let id = parseInt(this.config.data.idProcesoOferta)
    this.serv
      .buscarUnidadAprovechamientobyIdProcesoOferta(id)
      .subscribe((data: any) => {
        
        if (data.data !== null) {
          data.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              this.createLayer(t.geometry_wkt);
            } else {
              this.messageService.add({
                key: 'toast',
                severity: 'warn',
                summary: 'ADVERTENCIA',
                detail: 'No contiene geometria',
              });
            }
          });
          this.extentLayer();
        } else {
          this.messageService.add({
            key: 'toast',
            severity: 'warn',
            summary: 'ADVERTENCIA',
            detail: 'No contiene Datos',
          });
        }
      });
  }
  createLayer(geometry_wkt: any) {
    let polygon2: any =
      'POLYGON ((-73.517467527999941 -9.03586979399995, -73.516459073999954 -9.0344641819999651, -73.514137252999944 -9.0332340959999442, -73.51250995099997 -9.03288390199998, -73.511808278999979 -9.0331975329999636, -73.510303671999964 -9.0348099729999376, -73.509887281999966 -9.0358390869999425, -73.509678801999939 -9.0363536429999272, -73.509524753999983 -9.03745615799994, -73.509940205999953 -9.0379009109999515, -73.510582107999937 -9.0378854339999748, -73.51338975799996 -9.0359889269999485, -73.514056065999966 -9.035982583999953, -73.51496803699996 -9.0363750899999786, -73.515524248999952 -9.0371549669999354, -73.515537724999945 -9.0381677439999635, -73.514861221999979 -9.0398287639999353, -73.513537314999951 -9.0415956629999528, -73.512679248999973 -9.04233364099997, -73.510091699999975 -9.0432273489999488, -73.508594959999982 -9.0433297689999677, -73.506000866999955 -9.0429123119999417, -73.504707278999945 -9.0431059649999384, -73.503196962999937 -9.0442933609999727, -73.501980701999969 -9.04613298299995, -73.500867554999957 -9.0472315679999724, -73.499225214999967 -9.0478939609999429, -73.499061235999989 -9.0490506779999578, -73.499576666999985 -9.0503367669999761, -73.500513878999982 -9.0512267309999288, -73.500883532999978 -9.0521595799999659, -73.502804802999947 -9.05498857699996, -73.503357280999978 -9.0562567309999622, -73.503899977999936 -9.05672913899997, -73.504308423999987 -9.0577706509999416, -73.505224523999971 -9.0581360779999613, -73.505195942999933 -9.0587236989999269, -73.50552420799994 -9.0593579879999311, -73.504781333999972 -9.05999694299993, -73.502637850999974 -9.0601599789999341, -73.500860471999943 -9.06142763899993, -73.498018373999969 -9.0625191179999547, -73.496892461999948 -9.06366283799997, -73.496769281999946 -9.0648468449999768, -73.497668016999967 -9.0659717559999535, -73.498482363999983 -9.0664724149999643, -73.498471580999933 -9.0670148959999324, -73.497896730999969 -9.0674375129999589, -73.496023114999957 -9.0677734099999725, -73.49524082399995 -9.06831271599998, -73.494675597999958 -9.0703810239999711, -73.493568952999965 -9.0720763729999589, -73.493026862999955 -9.07390967699996, -73.492573366999977 -9.07467638099996, -73.491482946999952 -9.0756122549999532, -73.489681794999967 -9.0760117059999743, -73.488533784999959 -9.0757628229999341, -73.487982520999935 -9.07529939799997, -73.487398131999953 -9.0751432639999621, -73.486317176999989 -9.0753105859999437, -73.485810451999953 -9.0750733939999577, -73.485428203999959 -9.074303236999981, -73.48580659299995 -9.0711129669999764, -73.485505648999947 -9.07034314799995, -73.485116049999988 -9.0699708099999725, -73.483638833999976 -9.0712215269999774, -73.480783009999982 -9.0727106429999367, -73.480131299999982 -9.0734222519999435, -73.479655943999944 -9.0743787279999424, -73.479268841999954 -9.07624881199996, -73.480055180999955 -9.0779971919999412, -73.481116292999957 -9.0794392859999675, -73.481399492999969 -9.08023616099996, -73.481293772999948 -9.0806335689999287, -73.480874521999965 -9.0806860779999283, -73.480124996999962 -9.0799143879999633, -73.479003802999955 -9.07721520299998, -73.477886554999941 -9.07641485299996, -73.477523479999945 -9.0764947179999353, -73.47586464699998 -9.0768494809999538, -73.475349383999969 -9.07715475799995, -73.47462305199997 -9.078164428999969, -73.474035782999977 -9.0801692959999514, -73.473559130999945 -9.0806194029999574, -73.472990575999972 -9.08075265499997, -73.472399377999977 -9.080325208999966, -73.471985335999989 -9.0791480199999341, -73.470149772999946 -9.0773319419999439, -73.469311346999973 -9.07608063899994, -73.468674140999951 -9.0757795839999744, -73.46785641799994 -9.075947952999968, -73.467487067999969 -9.07643466899998, -73.467268672999978 -9.0777900459999614, -73.466210756999942 -9.0792232759999365, -73.466048740999952 -9.080153915999972, -73.465742667999962 -9.0808669449999684, -73.465342995999947 -9.081796585999939, -73.465157053999974 -9.0834052699999575, -73.465991454999937 -9.0841321329999687, -73.466692917999978 -9.084433464999961, -73.468380485999944 -9.084015577999935, -73.469634224999936 -9.0840027529999361, -73.470102807999979 -9.084384479999926, -73.470233924999945 -9.0849727569999459, -73.469984914999941 -9.0853695599999469, -73.469286935999946 -9.08571926899998, -73.466704745999948 -9.0856632199999581, -73.465329065999981 -9.086272289999954, -73.464424516999941 -9.0875976499999638, -73.464489895999975 -9.0888005039999484, -73.464231472999984 -9.0903365449999569, -73.463113205999946 -9.0911094419999472, -73.461949741999945 -9.0910141179999755, -73.460736471999951 -9.090466484999979, -73.460213245999967 -9.0899669709999671, -73.459534793999978 -9.08680847599993, -73.459850134999954 -9.0851280099999485, -73.459584357999972 -9.0831919169999651, -73.458907816999954 -9.0827821749999771, -73.457441191999976 -9.0827217289999567, -73.455194951999943 -9.080831514999943, -73.455181344999971 -9.0808224149999432, -73.451149718999943 -9.07817414699997, -73.450341127999934 -9.0779356279999774, -73.447701984999981 -9.0777887929999679, -73.445573640999953 -9.079434403999926, -73.443994655999973 -9.0797260609999739, -73.443652037999982 -9.0804660329999365, -73.443680275999952 -9.0807735769999454, -73.443826838999939 -9.0823836519999759, -73.442973346999963 -9.082931566999946, -73.438866229999974 -9.0842160509999417, -73.436903924999967 -9.0854463839999653, -73.434988059999966 -9.0850132079999639, -73.434333528999957 -9.0843865149999488, -73.434307614999966 -9.0838077269999644, -73.434569106999959 -9.0835556779999251, -73.435540101999948 -9.08357792399994, -73.435936180999988 -9.0833626179999669, -73.437158642999975 -9.08164086399995, -73.43741154199995 -9.0807468049999329, -73.437467551999987 -9.08054812399996, -73.436369735999961 -9.07869889099993, -73.433850618999941 -9.0759213009999371, -73.433212879999985 -9.076669036999931, -73.433197209999946 -9.0777901549999456, -73.432260755999948 -9.0800285069999518, -73.432022986999982 -9.080316823999965, -73.432217205999962 -9.03551542799994, -73.477695132999941 -9.0357069339999612, -73.477884450999966 -8.9904975759999388, -73.523357715999964 -8.9906825049999384, -73.523174047999987 -9.0358928079999714, -73.523173311999983 -9.0360736489999454, -73.52291306799998 -9.036181107999937, -73.520884463999948 -9.0385962409999365, -73.519239679999941 -9.0384810989999664, -73.518538261999936 -9.0380351999999675, -73.518163954999977 -9.0368401209999547, -73.517467527999941 -9.03586979399995), (-73.498910998999975 -9.0596383539999579, -73.499129643999936 -9.0594222409999361, -73.499276522999935 -9.057867602999977, -73.499025941999946 -9.0566006829999424, -73.499115357999983 -9.05587768199996, -73.499465009999938 -9.0553275469999335, -73.499942832999977 -9.05540184199998, -73.499864353999953 -9.05828594899998, -73.500187848999985 -9.0584229059999757, -73.500678889999961 -9.0581898229999638, -73.50086285499998 -9.05481787399998, -73.500784004999957 -9.05450107699994, -73.500339113999985 -9.0543003289999433, -73.499494948999939 -9.0544144179999648, -73.498572862999936 -9.05549568999993, -73.498322941999959 -9.05735733499995, -73.498540176999938 -9.05885017099996, -73.498910998999975 -9.0596383539999579), (-73.493705218999935 -9.0670766719999278, -73.494096369999966 -9.0670782829999439, -73.495017561999987 -9.06622307899994, -73.495084290999955 -9.0656265759999428, -73.494866005999938 -9.0653453739999463, -73.494183384999985 -9.06516172299996, -73.493275449999942 -9.0655648769999289, -73.493705218999935 -9.0670766719999278))';
    let geojson: any = wkt.parse(geometry_wkt);
    
    this.graphicsLayer = new GraphicsLayer();
    this.view.map.add(this.graphicsLayer);
    var polygon: any = {};
    const fillSymbol = {
      type: 'simple-fill',
      color: [227, 139, 79, 0.8],
      outline: {
        color: [255, 255, 255],
        width: 1,
      },
    };
    polygon.type = 'polygon';
    //polygon.rings = geojson.coordinates;
    let feature = null;
    geojson.coordinates.forEach((t: any) => {
      polygon.rings = t;
      feature = new Graphic({
        geometry: polygon,
        symbol: fillSymbol,
      });
      this.graphicsLayer.add(feature);
    });
  }
  extentLayer() {
    let layers = this.graphicsLayer.graphics.items;
    layers.forEach((t: any) => {
      this.view.goTo(t.geometry.extent);
    });
  }





}
