import { HttpParams } from '@angular/common/http';
import {
  Component,
  ViewChild,
  ElementRef,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { datosUsuario_parcial } from 'src/app/model/user';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';

import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import GraphicsLayer from '@arcgis/core/layers/GraphicsLayer';
import Graphic from '@arcgis/core/Graphic';
import * as print from '@arcgis/core/rest/print';
import PrintTemplate from '@arcgis/core/rest/support/PrintTemplate';
import PrintParameters from '@arcgis/core/rest/support/PrintParameters';

import * as wkt from 'terraformer-wkt-parser';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import {
  ConvertDateToString,
  ConvertNumberToDate,
  DownloadFile,
  ToastService,
} from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalObservacionComponent } from '../modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../modal-validar-anexo/modal-validar-anexo.component';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { UsuarioService } from '@services';
import { ValidarCondicionMinimaComponent } from 'src/app/shared/components/validar-condicion-minima/validar-condicion-minima.component';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-anexo1',
  templateUrl: '../anexos/anexo1.html',
  styleUrls: ['../anexos/stylos-anexos.scss'],
  providers: [MessageService],
})
export class Anexo1 {
  @Input() idAnexoStatus!: number;
  @Input() isSolCancel: boolean = false;
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('mapUbication', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild('imagenMap', { static: true }) private imagenMap!: ElementRef;
  public view: any = null;
  isGuardar: boolean = false;
  public graphicsLayer: any = null;
  srcImagenMap: any;
  distrito: String = '';
  provincia: String = '';
  departamento: String = '';
  fecha: Date = new Date();
  usarioActual: datosUsuario_parcial = JSON.parse(
    '' + localStorage.getItem('usuario')?.toString()
  );
  listaProcOferta: any = JSON.parse(
    '' + localStorage.getItem('procesosPostular')
  );
  j: number = 0;
  params_guardar: any = {
    solicitante: '',
    autoridadForestal: '-- Seleccione --',
    superficieSolicitada: '',
    plazoConcesion: 40,
    objetivo: '',
    distritoProvinciaDepartamento:
      this.distrito + ' - ' + this.provincia + ' - ' + this.departamento,
    idDepartamento: {
      idDepartamento: 0,
    },
    idDistrito: {
      idDistrito: 0,
    },
    idProvincia: {
      idProvincia: 0,
    },
    sectorAnexoCaserio: '',
    representanteLegal: '',
    rutaMapaUa: '',
    rutaPlantilla: '',
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    codigoAnexo: 'anexo1',
  };
  params_generar: any = {
    codigoAnexo: 'anexo1',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };
  params_obtener: any = {
    codigoAnexo: 'anexo1',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };
  file: any = {};
  params_adjuntar_anexo: any = {
    codigoanexo: 'anexo1',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };

  anexoActual: any = null;

  generar: boolean = false;
  isDisabled: boolean = false;

  autoridadForestal!: any;
  archivoEnviado: boolean = false;

  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;

  isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;

  isGuardarOK: boolean = false;

  constructor(
    private serv: PlanificacionService,
    private router: Router,
    private toast: ToastService,
    private messageService: MessageService,
    public dialog: MatDialog,
    private anexoService: AnexoService,
    private coreCentralService: CoreCentralService,
    public dialogService: DialogService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
  ) { }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;

    //this.obtenerUsuarioPostulante();
    this.obtenerUsuario();
    this.listarPorFiltroAutoridadForestal();
    // this.isDisabled = JSON.parse('' + localStorage.getItem('usuario')).perfiles[0].idPerfil ===  17 ? true: false;
    this.initializeMap();
    this.consultarUaById();

    this.obtenerAdjuntos();
    this.obtenerDetalleObservacion();
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '440px';
    container.style.width = '100%';
    const map = new Map({
      basemap: 'hybrid',
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }
  consultarUaById() {
    let item = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    );
    let id = parseInt(item.idProcesoOferta);
    this.serv
      .buscarUnidadAprovechamientobyIdProcesoOferta(id)
      .subscribe((data: any) => {
        if (data.data !== null) {
          data.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              this.createLayer(t.geometry_wkt);
            } else {
              
            }
          });
          this.extentLayer();
        }
      });
  }
  createLayer(geometry_wkt: any) {
    //let polygon2: any = 'POLYGON ((-73.517467527999941 -9.03586979399995, -73.516459073999954 -9.0344641819999651, -73.514137252999944 -9.0332340959999442, -73.51250995099997 -9.03288390199998, -73.511808278999979 -9.0331975329999636, -73.510303671999964 -9.0348099729999376, -73.509887281999966 -9.0358390869999425, -73.509678801999939 -9.0363536429999272, -73.509524753999983 -9.03745615799994, -73.509940205999953 -9.0379009109999515, -73.510582107999937 -9.0378854339999748, -73.51338975799996 -9.0359889269999485, -73.514056065999966 -9.035982583999953, -73.51496803699996 -9.0363750899999786, -73.515524248999952 -9.0371549669999354, -73.515537724999945 -9.0381677439999635, -73.514861221999979 -9.0398287639999353, -73.513537314999951 -9.0415956629999528, -73.512679248999973 -9.04233364099997, -73.510091699999975 -9.0432273489999488, -73.508594959999982 -9.0433297689999677, -73.506000866999955 -9.0429123119999417, -73.504707278999945 -9.0431059649999384, -73.503196962999937 -9.0442933609999727, -73.501980701999969 -9.04613298299995, -73.500867554999957 -9.0472315679999724, -73.499225214999967 -9.0478939609999429, -73.499061235999989 -9.0490506779999578, -73.499576666999985 -9.0503367669999761, -73.500513878999982 -9.0512267309999288, -73.500883532999978 -9.0521595799999659, -73.502804802999947 -9.05498857699996, -73.503357280999978 -9.0562567309999622, -73.503899977999936 -9.05672913899997, -73.504308423999987 -9.0577706509999416, -73.505224523999971 -9.0581360779999613, -73.505195942999933 -9.0587236989999269, -73.50552420799994 -9.0593579879999311, -73.504781333999972 -9.05999694299993, -73.502637850999974 -9.0601599789999341, -73.500860471999943 -9.06142763899993, -73.498018373999969 -9.0625191179999547, -73.496892461999948 -9.06366283799997, -73.496769281999946 -9.0648468449999768, -73.497668016999967 -9.0659717559999535, -73.498482363999983 -9.0664724149999643, -73.498471580999933 -9.0670148959999324, -73.497896730999969 -9.0674375129999589, -73.496023114999957 -9.0677734099999725, -73.49524082399995 -9.06831271599998, -73.494675597999958 -9.0703810239999711, -73.493568952999965 -9.0720763729999589, -73.493026862999955 -9.07390967699996, -73.492573366999977 -9.07467638099996, -73.491482946999952 -9.0756122549999532, -73.489681794999967 -9.0760117059999743, -73.488533784999959 -9.0757628229999341, -73.487982520999935 -9.07529939799997, -73.487398131999953 -9.0751432639999621, -73.486317176999989 -9.0753105859999437, -73.485810451999953 -9.0750733939999577, -73.485428203999959 -9.074303236999981, -73.48580659299995 -9.0711129669999764, -73.485505648999947 -9.07034314799995, -73.485116049999988 -9.0699708099999725, -73.483638833999976 -9.0712215269999774, -73.480783009999982 -9.0727106429999367, -73.480131299999982 -9.0734222519999435, -73.479655943999944 -9.0743787279999424, -73.479268841999954 -9.07624881199996, -73.480055180999955 -9.0779971919999412, -73.481116292999957 -9.0794392859999675, -73.481399492999969 -9.08023616099996, -73.481293772999948 -9.0806335689999287, -73.480874521999965 -9.0806860779999283, -73.480124996999962 -9.0799143879999633, -73.479003802999955 -9.07721520299998, -73.477886554999941 -9.07641485299996, -73.477523479999945 -9.0764947179999353, -73.47586464699998 -9.0768494809999538, -73.475349383999969 -9.07715475799995, -73.47462305199997 -9.078164428999969, -73.474035782999977 -9.0801692959999514, -73.473559130999945 -9.0806194029999574, -73.472990575999972 -9.08075265499997, -73.472399377999977 -9.080325208999966, -73.471985335999989 -9.0791480199999341, -73.470149772999946 -9.0773319419999439, -73.469311346999973 -9.07608063899994, -73.468674140999951 -9.0757795839999744, -73.46785641799994 -9.075947952999968, -73.467487067999969 -9.07643466899998, -73.467268672999978 -9.0777900459999614, -73.466210756999942 -9.0792232759999365, -73.466048740999952 -9.080153915999972, -73.465742667999962 -9.0808669449999684, -73.465342995999947 -9.081796585999939, -73.465157053999974 -9.0834052699999575, -73.465991454999937 -9.0841321329999687, -73.466692917999978 -9.084433464999961, -73.468380485999944 -9.084015577999935, -73.469634224999936 -9.0840027529999361, -73.470102807999979 -9.084384479999926, -73.470233924999945 -9.0849727569999459, -73.469984914999941 -9.0853695599999469, -73.469286935999946 -9.08571926899998, -73.466704745999948 -9.0856632199999581, -73.465329065999981 -9.086272289999954, -73.464424516999941 -9.0875976499999638, -73.464489895999975 -9.0888005039999484, -73.464231472999984 -9.0903365449999569, -73.463113205999946 -9.0911094419999472, -73.461949741999945 -9.0910141179999755, -73.460736471999951 -9.090466484999979, -73.460213245999967 -9.0899669709999671, -73.459534793999978 -9.08680847599993, -73.459850134999954 -9.0851280099999485, -73.459584357999972 -9.0831919169999651, -73.458907816999954 -9.0827821749999771, -73.457441191999976 -9.0827217289999567, -73.455194951999943 -9.080831514999943, -73.455181344999971 -9.0808224149999432, -73.451149718999943 -9.07817414699997, -73.450341127999934 -9.0779356279999774, -73.447701984999981 -9.0777887929999679, -73.445573640999953 -9.079434403999926, -73.443994655999973 -9.0797260609999739, -73.443652037999982 -9.0804660329999365, -73.443680275999952 -9.0807735769999454, -73.443826838999939 -9.0823836519999759, -73.442973346999963 -9.082931566999946, -73.438866229999974 -9.0842160509999417, -73.436903924999967 -9.0854463839999653, -73.434988059999966 -9.0850132079999639, -73.434333528999957 -9.0843865149999488, -73.434307614999966 -9.0838077269999644, -73.434569106999959 -9.0835556779999251, -73.435540101999948 -9.08357792399994, -73.435936180999988 -9.0833626179999669, -73.437158642999975 -9.08164086399995, -73.43741154199995 -9.0807468049999329, -73.437467551999987 -9.08054812399996, -73.436369735999961 -9.07869889099993, -73.433850618999941 -9.0759213009999371, -73.433212879999985 -9.076669036999931, -73.433197209999946 -9.0777901549999456, -73.432260755999948 -9.0800285069999518, -73.432022986999982 -9.080316823999965, -73.432217205999962 -9.03551542799994, -73.477695132999941 -9.0357069339999612, -73.477884450999966 -8.9904975759999388, -73.523357715999964 -8.9906825049999384, -73.523174047999987 -9.0358928079999714, -73.523173311999983 -9.0360736489999454, -73.52291306799998 -9.036181107999937, -73.520884463999948 -9.0385962409999365, -73.519239679999941 -9.0384810989999664, -73.518538261999936 -9.0380351999999675, -73.518163954999977 -9.0368401209999547, -73.517467527999941 -9.03586979399995), (-73.498910998999975 -9.0596383539999579, -73.499129643999936 -9.0594222409999361, -73.499276522999935 -9.057867602999977, -73.499025941999946 -9.0566006829999424, -73.499115357999983 -9.05587768199996, -73.499465009999938 -9.0553275469999335, -73.499942832999977 -9.05540184199998, -73.499864353999953 -9.05828594899998, -73.500187848999985 -9.0584229059999757, -73.500678889999961 -9.0581898229999638, -73.50086285499998 -9.05481787399998, -73.500784004999957 -9.05450107699994, -73.500339113999985 -9.0543003289999433, -73.499494948999939 -9.0544144179999648, -73.498572862999936 -9.05549568999993, -73.498322941999959 -9.05735733499995, -73.498540176999938 -9.05885017099996, -73.498910998999975 -9.0596383539999579), (-73.493705218999935 -9.0670766719999278, -73.494096369999966 -9.0670782829999439, -73.495017561999987 -9.06622307899994, -73.495084290999955 -9.0656265759999428, -73.494866005999938 -9.0653453739999463, -73.494183384999985 -9.06516172299996, -73.493275449999942 -9.0655648769999289, -73.493705218999935 -9.0670766719999278))';
    let geojson: any = wkt.parse(geometry_wkt);
    this.graphicsLayer = new GraphicsLayer();
    this.view.map.add(this.graphicsLayer);
    var polygon: any = {};
    const fillSymbol = {
      type: 'simple-fill',
      color: [227, 139, 79, 0.8],
      outline: {
        color: [255, 255, 255],
        width: 1,
      },
    };
    polygon.type = 'polygon';
    //polygon.rings = geojson.coordinates;
    let feature = null;
    geojson.coordinates.forEach((t: any) => {
      polygon.rings = t;
      feature = new Graphic({
        geometry: polygon,
        symbol: fillSymbol,
      });
      this.graphicsLayer.add(feature);
    });
  }
  extentLayer() {
    this.view.goTo(this.graphicsLayer.graphics);
  }
  params_obtener_adjunto: any = {
    codigoAnexo: 'anexo1',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 1,
  };
  obtenerAdjuntos(isClick: boolean = false) {
    const auxLoad = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.obtenerAdjuntos(this.params_obtener_adjunto).subscribe((result: any) => {
      auxLoad.close();
      if (result.data.length > 0) {
        this.anexoActual = result.data[0];
        this.params_validar_anexo.idDocumentoAdjunto =
          this.anexoActual.idDocumentoAdjunto;

        if (isClick) {
          DownloadFile(
            this.anexoActual.file,
            this.anexoActual.nombreDocumento,
            'application/octet-stream'
          );
        }

        this.generar = true;
        this.archivoEnviado = true;
      }
    }, () => {
      auxLoad.close();
    });
  }

  eliminarDocumentoAdjunto = () => {
    var params = {
      idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.anexoActual = null;
        this.generar = false;
        this.archivoEnviado = false;
        this.toast.ok(result.message);
        this.file = {};
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  };

  display_validar: boolean = false;
  params_validar_anexo: any = {
    descripcion: '',
    devolucion: false,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  enviar_validarAnexo() {
    this.ref = this.dialogService.open(ModalValidarAnexoComponent, {
      header: 'Validar Anexo N° 01',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
        tipodoc: 'anexo1',
        isSolCancel: this.isSolCancel
      },
    });

    this.ref.onClose.subscribe((resp: any) => {

      if (resp != null) {
        this.actualizarEstatusAnexo(resp ? 21 : 22);
        /*if (resp) {
          this.actualizarEstadoProcesoPostulacion();
        }*/
        this.validarAnexo.emit(true);
      }
    });
  }

  actualizarEstadoProcesoPostulacion = () => {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario'))
        .correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 4,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.messageService.add({
            key: 'an1',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  actualizarEstatusAnexo = (status: number) => {
    var params = {
      idAnexoDetalle: this.idAnexoStatus,
      idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
      idStatusAnexo: status,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .actualizarEstatusAnexo(params)
      .subscribe((result: any) => {
        if (result.success) {
          if (this.isGuardar == false) {
            this.messageService.add({
              key: 'an1',
              severity: 'success',
              summary: '',
              detail: 'Se actualizó correctamente',
            });
            this.isGuardar = false;
          }
          this.display_validar = false;
          this.obtenerAnexo();
          this.obtenerAdjuntos();
          this.validarAnexo.emit(true);
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  // archivo
  cargarAchivoAnexo1(e: any) {

  }
  eliminarArchivoAnexo1() {
    this.file = {};
  }
  enviarArchivo() {
    if (!this.file.nombre) {
      this.toast.warn('(*) Debe adjuntar un documento.');
      return;
    }

    var params = new HttpParams()
      .set('CodigoAnexo', 'anexo1')
      .set('IdProcesoPostulacion', this.params_adjuntar_anexo.idprocesopostulacion)
      .set('IdTipoDocumento', '1')
      .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
      .set('NombreArchivo', this.file.nombre);

    const formData = new FormData();
    formData.append('file', this.file.file);


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.isGuardar = true;
        this.archivoEnviado = true;
        this.toast.ok(result.message);

        this.obtenerAdjuntos();
        this.actualizarEstatusAnexo(20);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  listarPorFiltroAutoridadForestal = () => {
    var params = {
      estado: null,
      idAutoridadForestal: null,
      nombre: null,
    };
    this.coreCentralService
      .listarPorFiltroAutoridadForestal(params)
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          nombre: '-- Seleccione --',
        });
        this.autoridadForestal = resp.data;
      });
  };

  obtenerUsuario = () => {
    var params = {
      //codigoAnexo: 'anexo1',
      idProcesoPostulacion: JSON.parse('' + localStorage.getItem('bandejapostulacionrevisar')).idProcesoPostulacion
    };
    this.anexoService
      .obtenerUsuario(params)
      .subscribe((resp: any) => {
        if (resp.data) {
          
          this.departamentoSelect = Number(resp.data.idDistrito.substring(0,2));
          this.provinciaSelect = Number(resp.data.idDistrito.substring(0,4));
          this.distritoSelect = Number(resp.data.idDistrito.substring(0,6));
          this.params_guardar.superficieSolicitada = resp.data.superficieSolicitada;
          this.params_guardar.nombre = resp.data.nombre;
          this.params_guardar.apellidoPaterno = resp.data.apellidoPaterno;
          this.params_guardar.apellidoMaterno = resp.data.apellidoMaterno;
          //this.params_guardar.tipoDocumento = resp.data.codTipoDoc;
          if(resp.data.representanteLegal!=null) this.params_guardar.representanteLegal = resp.data.representanteLegal.nombre + resp.data.representanteLegal.apellidoPaterno + resp.data.representanteLegal.apellidoMaterno;
          else this.params_guardar.representanteLegal = "";
          this.params_guardar.numeroDocumento = resp.data.numeroDocumento;
          //this.params_guardar.sectorAnexoCaserio = 'Guayaquil /	Cruce Jaén Nuevo / Cruz Blanca';

          if (resp.data.codTipoDoc === "TDOCDNI") {
            this.params_guardar.tipoDocumento = "DNI";
            this.params_guardar.solicitante =  resp.data.nombre + resp.data.apellidoPaterno + resp.data.apellidoMaterno;
          } else {
            this.params_guardar.tipoDocumento = "RUC";
            this.params_guardar.solicitante = resp.data.razonSocial;
          }
        }
        this.obtenerAnexo();
      });
  };





  /* obtenerUsuarioPostulante = () => {
    var params = {
      codigoAnexo: 'anexo1',
      idProcesoPostulacion: JSON.parse('' + localStorage.getItem('bandejapostulacionrevisar')).idProcesoPostulacion,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerUsuarioPostulante(params)
      .subscribe((resp: any) => {
        if (resp.data) {
          console.log("params",params);
          console.log("Usuario",resp);
          this.params_guardar.sectorAnexoCaserio = resp.data.sectorAnexoCaserio;
          this.params_guardar.solicitante = resp.data.solicitante;
          this.params_guardar.superficieSolicitada =
            resp.data.superficieSolicitada;
          this.departamentoSelect = resp.data.idDepartamento;
          this.provinciaSelect = resp.data.idProvincia;
          this.distritoSelect = resp.data.idDistrito;
          this.params_guardar.nombre = resp.data.nombre;
          this.params_guardar.apellidoPaterno = resp.data.apellidoPaterno;
          this.params_guardar.apellidoMaterno = resp.data.apellidoMaterno;
          this.params_guardar.tipoDocumento = resp.data.tipoDocumento;
          if (resp.data.tipoDocumento === 3) {
            this.params_guardar.tipoDocumento = "DNI";
          } else {
            this.params_guardar.tipoDocumento = "RUC";
          }
          this.params_guardar.numeroDocumento = resp.data.numeroDocumento;
        }
        this.obtenerAnexo();
      });
  }; */

  // archivo
  obtenerAnexo() {
    this.params_obtener.idProcesoPostulacion = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion;
    this.serv.obtenerAnexo(this.params_obtener).subscribe((result: any) => {

      if (result.data?.length > 0) {

        result.data.forEach((element: any) => {

          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Solicitante'.toUpperCase()
          ) {
            this.params_guardar.solicitante = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'RepresentanteLegal'.toUpperCase()
          ) {
            this.params_guardar.representanteLegal = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'SuperficieSolicitada(ha)'.toUpperCase()
          ) {
            this.params_guardar.superficieSolicitada = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'PlazodeConcesion(años)'.toUpperCase()
          ) {
            this.params_guardar.plazoConcesion = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Sector/Anexo/Caserio'.toUpperCase()
          ) {
            this.params_guardar.sectorAnexoCaserio = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'IdDistrito'.toUpperCase()
          ) {
            this.distritoSelect = Number(element.value);
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'IdProvincia'.toUpperCase()
          ) {
            this.provinciaSelect = Number(element.value);
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'IdDepartamento'.toUpperCase()
          ) {
            this.departamentoSelect = Number(element.value);
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Objetivo'.toUpperCase()
          ) {
            this.params_guardar.objetivo = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'AutoridadForestal'.toUpperCase()
          ) {
            this.params_guardar.autoridadForestal = element.value;
          }
        });
        this.listarPorFiltroDepartamento();
      } else this.listarPorFiltroDepartamento();
    });
  }
  validarCargaMapUA() {
    let item = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    );
    let parametro: any = {
      codigoAnexo: null,
      idProcesoPostulacion: item.idProcesoPostulacion,
      idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idTipoDocumento: 19,
    };
    
    this.serv.obtenerAdjuntos(parametro).subscribe((result: any) => {
      //Valida si contiene data
      if (result.data.length === 0) {
        this.view.takeScreenshot().then((data: any) => {
          //this.srcImagenMap = data.dataUrl;
          //return;
          fetch(data.dataUrl)
            .then((data) => data.blob())
            .then((data) => {
              let params = new HttpParams()
                .set('CodigoAnexo', 'mapaua')
                .set('IdTipoDocumento', '19')
                .set('IdProcesoPostulacion', item.idProcesoPostulacion)
                .set(
                  'IdUsuarioAdjunta',
                  JSON.parse('' + localStorage.getItem('usuario')).idusuario
                )
                .set('NombreArchivo', 'MapaUnidadAprovechamiento.png');
              const file = new File([data], 'MapaUnidadAprovechamiento.png', {
                type: 'image/png',
              });

              const formData = new FormData();
              formData.append('file', file);
              this.serv
                .adjuntarAnexo(params, formData)
                .subscribe((data: any) => {
                  console.log(data);
                });
            });
        });
      }
    });
  }
  guardaranexo() {
    this.validarCargaMapUA();
    this.params_guardar.idDepartamento.idDepartamento = this.departamentoSelect;
    this.params_guardar.idProvincia.idProvincia = this.provinciaSelect;
    this.params_guardar.idDistrito.idDistrito = this.distritoSelect;

    ;
    let params = {
      anexo1: this.params_guardar,
      anexo2: null,
      anexo3: null,
      idProcesoPostulacion: this.params_obtener.idProcesoPostulacion,
      idUsuarioPostulacion: this.params_obtener.idUsuarioPostulacion,
      procesopostulacion: {
        fechaPostulacion: new Date(),
        fechaRecepcionDocumentos: null,
        idEstatusProceso: null,
        idProcesoOferta: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoOferta,
        idProcesoPostulacion: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoPostulacion,
        idRegenteForestal: 33,
        idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        observacion: null,
        recepcionDocumentos: false,
      },
    };


    let datosCompletos = this.validar(params);

    if (!datosCompletos) {
      this.toast.warn('Hay campos obligatorios por ser llenados');
      return;
    }

    this.valido = '*';

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.guardaProcesoPostulacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.isGuardarOK = true;
        this.actualizarEstatusAnexo(19);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  generaranexo() {
    let datosCompeltos = this.validar(this.params_generar);
    //console.log(this.params_generar);
    //console.log(datosCompeltos);

    if (!datosCompeltos) {
      this.messageService.add({
        key: 'an1',
        severity: 'error',
        summary: '',
        detail: 'Hay campos obligatorios por ser llenados',
      });
      return;
    }


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.generaranexo1(this.params_generar).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        let p_file = this.base64ToArrayBuffer(result.archivo);
        var blob = new Blob([p_file], {
          type: result.contenTypeArchivo,
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        var fileName = result.nombeArchivo;
        link.download = fileName;
        link.click();
        this.generar = true;
      } else {
        this.messageService.add({
          key: 'an1',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    }, () => {
      this.dialog.closeAll();
    });
  }
  valido: String = '*';
  validar(obj: any) {
    let val = false;
    if (
      document.getElementsByClassName('anexo1_required').length == null ||
      document.getElementsByClassName('anexo1_required').length == 0
    ) {
      this.valido = '';
      val = true;
    }

    return val;
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  departamentos: any = [];
  departamentoSelect: any = null;
  provincias: any = [];
  provinciaSelect: any = null;
  distritos: any = [];
  distritoSelect: any = null;
  listarPorFiltroDepartamento(ischange: boolean = false) {
    ;
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };

    //console.log(params);
    this.serv
      .listarPorFiltroDepartamento_core_central(params)
      .subscribe((result: any) => {
        if (result.success) {
          ;
          this.departamentos = result.data;
          this.departamentoSelect = ischange ? result.data[2].idDepartamento: this.departamentoSelect != null? this.departamentoSelect: result.data[2].idDepartamento;
          this.listarPorFilroProvincia(ischange);
        }
      });
  }
  listarPorFilroProvincia(ischange: boolean = false) {
    this.serv
      .listarPorFilroProvincia({
        idDepartamento: this.departamentoSelect,
      })
      .subscribe((result: any) => {
        if (result.success) {
          this.provincias = result.data;
          this.provinciaSelect = ischange
            ? result.data[0].idProvincia
            : this.provinciaSelect != null
              ? this.provinciaSelect
              : result.data[0].idProvincia;
          this.listarPorFilroDistrito(ischange);
        }
      });
  }
  listarPorFilroDistrito(ischange: boolean = false) {
    this.serv
      .listarPorFilroDistrito({ idProvincia: this.provinciaSelect })
      .subscribe((result: any) => {
        if (result.success) {
          this.distritos = result.data;
          this.distritoSelect = ischange
            ? result.data[0].idDistrito
            : this.distritoSelect != null
              ? this.distritoSelect
              : result.data[0].idDistrito;
        }
      });
  }

  selectDPD(obj: any, origen: string) {
    if (origen == 'd') {
      // this.departamentoSelect = obj;
      this.listarPorFilroProvincia(true);
    }
    if (origen == 'p') {
      // this.provinciaSelect = obj;
      this.listarPorFilroDistrito(true);
    }
    if (origen == 'dd') {
      // this.distritoSelect = obj;
    }
  }

  obtenerDetalleObservacion = () => {
    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: 1,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerDetalleObservacion(params)
      .subscribe((result: any) => {
        if (result.data?.length > 0) {

          result.data.forEach((element: any) => {
            if (
              element.fechaFinPlazo != null &&
              element.fechaFinPlazo != undefined
            ) {
              element.fechaFinPlazo = ConvertDateToString(
                ConvertNumberToDate(element.fechaFinPlazo)
              );
            }
          });
          this.lstObservacion = result.data;
        }
      });
  };

  verObservaciones = () => {
    this.ref = this.dialogService.open(ModalObservacionComponent, {
      header: 'Listado de observaciones',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        lstObservacion: this.lstObservacion,
      },
    });
  };

  validarCondiciones() {
    let param = {
      nombre: this.params_guardar.nombre,
      apellidoPaterno: this.params_guardar.apellidoPaterno,
      apellidoMaterno: this.params_guardar.apellidoMaterno,
      tipoDocumento: this.params_guardar.tipoDocumento,
      numeroDocumento: this.params_guardar.numeroDocumento
    };

    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: 'Validar Condiciones Mínimas',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        idCondicionMinima: null,
        obj: param
      },
    });
  }
}
