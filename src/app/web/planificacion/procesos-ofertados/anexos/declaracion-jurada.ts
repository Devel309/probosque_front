import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ConvertDateToString, ConvertNumberToDate, DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ModalObservacionComponent } from '../modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../modal-validar-anexo/modal-validar-anexo.component';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { UsuarioService } from '@services';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-declaracion-jurada',
  templateUrl: './declaracion-jurada.html',
})
export class AnexoDeclaracionJuradaComponent implements OnInit {
  @Input() idAnexoStatus!: number;
  @Input()  isSolCancel: boolean = false;
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();

  // @ViewChild('fileInput') fileInput!: ElementRef;
  display_validar: boolean = false;

  params_obtener: any = {
    codigoAnexo: 'declaracionjurada',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };

  generar: boolean = false;
  archivoEnviado: boolean = false;

  params_adjuntar_anexo: any = {
    codigoanexo: 'declaracionjurada',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };

  anexoActual: any = null;
  paramsGuardar = new HttpParams();
  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;

  archivoAdjunto:any = {};
  // file!: any;

  isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;

  constructor(
    private serv: PlanificacionService,
    private messageService: MessageService,
    private anexoService: AnexoService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
  ) {}

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;
    this.obtenerAdjuntos();
    this.obtenerDetalleObservacion();
  }

  obtenerAnexo() {
    this.params_obtener.idProcesoPostulacion = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion;
    this.serv.obtenerAnexo(this.params_obtener).subscribe((result: any) => {
      ;
    });
  }

  params_obtener_adjunto: any = {
    codigoAnexo: null,
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 25,

  };

  obtenerAdjuntos(isClick: boolean = false) {
    const auxLoad = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv
      .obtenerAdjuntos(this.params_obtener_adjunto)
      .subscribe((result: any) => {
        auxLoad.close();
        if (result.data?.length > 0) {
          this.anexoActual = result.data[0];
          this.params_validar_anexo.idDocumentoAdjunto =
            this.anexoActual.idDocumentoAdjunto;

            this.archivoAdjunto.nombre = this.anexoActual.nombreDocumento;
            this.archivoAdjunto.file = this.anexoActual.file;
            this.archivoAdjunto.url = 'application/octet-stream';
            

            if (isClick) {
              DownloadFile(
              this.anexoActual.file,
                this.anexoActual.nombreDocumento,
              'application/octet-stream'
              );
            
            }

          this.generar = false;
          this.archivoEnviado = true;
        }
      },()=>{
        auxLoad.close();
      });
  }

  params_validar_anexo: any = {
    descripcion: '',
    devolucion: false,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  // cargarAchivo(e: any) {
  //   if (e.srcElement) {
  //     if (e.srcElement.files.length) {
  //       this.file = e.srcElement.files[0];
  //     }
  //   }
  // }

  eliminarArchivoPrevio(){
    this.archivoAdjunto = {};
  }
  eliminarDocumentoAdjunto = () => {
      this.archivoAdjunto = {};
      var params = {
        idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
        idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
      };

      this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
        if (result.success) {
          this.anexoActual = null;
          this.generar = false;
          this.archivoEnviado = false;
        }
      });
  };



  @Input() file: any = {};

  guardarArchivo(){
    if(!this.archivoAdjunto.nombre){
      this.messageService.add({
        key: 'dc',
        severity: 'warn',
        summary: '',
        detail: '(*) Debe de adjuntar un archivo',
      });
      return;
    }

    if(this.anexoActual!=null && this.anexoActual.idDocumentoAdjunto!=null){
      this.eliminarDocumentoAdjunto();
    }

    const formData = new FormData();
    formData.append('file', this.file.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(this.paramsGuardar, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        // this.fileInput.nativeElement.value === '';
        this.archivoEnviado = true;
        // this.messageService.add({
        //   key: 'dc',
        //   severity: 'success',
        //   summary: '',
        //   detail: 'El archivo se guardó correctamente',
        // });
        this.obtenerAdjuntos();
        this.actualizarEstatusAnexo(20);
      } else {
        this.messageService.add({
          key: 'dc',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    },()=>{
      this.dialog.closeAll();
    });


  }

  enviarArchivo(event: any) {

    var params = new HttpParams()
      .set('CodigoAnexo', 'declaracionjurada')
      .set(
        'IdProcesoPostulacion',
        this.params_adjuntar_anexo.idprocesopostulacion
      )
      .set('IdTipoDocumento', '25')
      .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
      .set('NombreArchivo', this.archivoAdjunto.nombre)
      .set('idDocumentoAdjunto', '0');
      // .set('NombreArchivo', this.file.name);
      this.paramsGuardar = params;

      this.file.url = URL.createObjectURL(event.file);
      this.file.file = event.file;
      this.file.nombre = event.file.name;

  }
/*
  eliminarDocumentoAdjunto = () => {
    var params = {
      idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      if (result.success) {
        this.anexoActual = null;
        this.generar = false;
        this.archivoEnviado = false;
      }
    });
  };
*/
  enviar_validarAnexo() {

    if(this.params_validar_anexo.idDocumentoAdjunto==1){
      this.messageService.add({
        key: 'dc',
        severity: 'error',
        summary: '',
        detail: 'Debe cargar un anexo.',
      });
      return;
    }


    this.ref = this.dialogService.open(ModalValidarAnexoComponent, {
      header: 'Validar Declaración Jurada',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
        isSolCancel: this.isSolCancel
      },
    });

    this.ref.onClose.subscribe((resp: any) => {

      if (resp != null) {
        this.actualizarEstatusAnexo(
          resp ? 21 : 22
        );
        /*if(resp){
          this.actualizarEstadoProcesoPostulacion();
        }*/
      }
    });
  }

  actualizarEstadoProcesoPostulacion = () =>{
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 4,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.messageService.add({
            key: 'an1',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
          this.obtenerAdjuntos();
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
   }

  actualizarEstatusAnexo = (status: number) => {
    var params = {
      idAnexoDetalle: this.idAnexoStatus,
      idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
      idStatusAnexo: status,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .actualizarEstatusAnexo(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.messageService.add({
            key: 'dc',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
          this.display_validar = false;
          this.validarAnexo.emit(true);
        } else {
          this.messageService.add({
            key: 'dc',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  obtenerDetalleObservacion = () => {
    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: 25,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerDetalleObservacion(params)
      .subscribe((result: any) => {
        result.data.forEach((element: any) => {
          if (
            element.fechaFinPlazo != null &&
            element.fechaFinPlazo != undefined
          ) {
            element.fechaFinPlazo = ConvertDateToString(
              ConvertNumberToDate(element.fechaFinPlazo)
            );
          }
        });
        this.lstObservacion = result.data;
      });
  };

  verObservaciones = () => {
    this.ref = this.dialogService.open(ModalObservacionComponent, {
      header: 'Listado de observaciones',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        lstObservacion: this.lstObservacion,
      },
    });
  };

}
