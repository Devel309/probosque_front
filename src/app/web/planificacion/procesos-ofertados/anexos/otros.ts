import { HttpParams } from '@angular/common/http';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ConvertDateToString, ConvertNumberToDate, DowloadFileLocal, DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ModalObservacionComponent } from '../modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../modal-validar-anexo/modal-validar-anexo.component';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
@Component({
  selector: 'app-anexo-otros',
  templateUrl: './otros.html',
  styleUrls: ['./stylos-anexos.scss'],
})
export class AnexosOtrosComponent implements OnInit {
  @Input() idAnexoStatus!: number;
  @Input() isSolCancel: boolean = false;
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();
  descripcion: string = '';
  file!: any;
  listadoArchivos: any[] = [];

  j: number = 0;

  params_adjuntar_anexo: any = {
    // codigoanexo: 'anexo1',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };

  anexoActual: any = null;
  generar: boolean = false;
  archivoEnviado: boolean = false;

  params_validar_anexo: any = {
    descripcion: '',
    devolucion: false,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  display_validar: boolean = false;

  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;

  archivoAdjunto: any = {};

  isTitular: boolean = false;
  inHabilitar: boolean = true;
  validarInHabilitar: boolean = true;
  usuario!: UsuarioModel;
  perfil = Perfiles;

  constructor(
    private serv: PlanificacionService,
    private messageService: MessageService,
    private anexoService: AnexoService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
  ) { }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;

    this.obtenerAdjuntos();
    this.obtenerDetalleObservacion();
  }

  params_obtener_adjunto: any = {
    codigoAnexo: null,
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 7,
  };
  obtenerAdjuntos(isClick: boolean = false) {
    const auxLoad = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.obtenerAdjuntos(this.params_obtener_adjunto).subscribe((result: any) => {
      auxLoad.close();
      this.listadoArchivos = [];
      if (result.data?.length > 0) {
        result.data.forEach((element: any) => {
          this.listadoArchivos.push({
            idArchivo: this.listadoArchivos.length + 1,
            idDocumentoAdjunto: element.idDocumentoAdjunto,
            descripcion: element.descripcion,
            nombreArchivo: element.nombreDocumento,
            file: element.file,

          });
        });

        this.params_validar_anexo.idDocumentoAdjunto = this.listadoArchivos[0].idDocumentoAdjunto;

        this.generar = false;
        this.archivoEnviado = true;
      }
    }, () => {
      auxLoad.close();
    });
  }


  eliminarArchivoPrevio() {
    this.archivoAdjunto = {};
  }

  agregar = () => {
    this.validarInHabilitar = false;
    this.inHabilitar = this.isTitular ? this.validarInHabilitar ? true : false : true;
    if (this.listadoArchivos.length === 5) {
      this.messageService.add({
        key: 'other',
        severity: 'warn',
        summary: '',
        detail: '(*) Solo puede adjuntar 5 anexos adicionales',
      });
    } else if (this.descripcion === '') {
      this.messageService.add({
        key: 'other',
        severity: 'warn',
        summary: '',
        detail: '(*) Debe ingresar una descripción',
      });
    } else if (!this.archivoAdjunto.nombre) {
      this.messageService.add({
        key: 'other',
        severity: 'warn',
        summary: '',
        detail: '(*) Debe adjuntar un archivo',
      });
    } else {
      this.listadoArchivos.push({
        idArchivo: this.listadoArchivos.length + 1,
        idDocumentoAdjunto: (this.listadoArchivos.length + 1) * -1,
        descripcion: this.descripcion,
        file: this.archivoAdjunto.file,
        url: this.archivoAdjunto.url,
        nombreArchivo: this.archivoAdjunto.nombre,
      });
      this.descripcion = '';
      this.archivoAdjunto = {};
    }
  };

  descargarArchivo = (item: any) => {
    if (item.idDocumentoAdjunto < 0) {
      DowloadFileLocal(item.url, item.nombreArchivo);
    } else {
      DownloadFile(item.file, item.nombreArchivo, 'application/octet-stream');
    }

  };

  eliminarArchivo = (item: any) => {
    ;
    if (item.idDocumentoAdjunto < 0) {
      this.listadoArchivos = this.listadoArchivos.filter((x: any) => x.idArchivo != item.idArchivo);
    } else {
      this.eliminarDocumentoAdjunto(item);
    }
  };

  eliminarDocumentoAdjunto = (item: any) => {
    var params = {
      idDocumentoAdjunto: item.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      if (result.success) {
        this.obtenerAdjuntos();
      }
    });
  };

  enviarArchivo() {
    this.j = 1;
    this.listadoArchivos.forEach(element => {
      if (element.idDocumentoAdjunto > 0) {
        this.j++
      }
    });

    this.listadoArchivos.forEach((element) => {

      if (element.idDocumentoAdjunto < 0) {
        var params = new HttpParams()
          .set('CodigoAnexo', element.descripcion)
          .set(
            'IdProcesoPostulacion',
            this.params_adjuntar_anexo.idprocesopostulacion
          )
          .set('IdTipoDocumento', '7')
          .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
          .set('NombreArchivo', element.file.name);

        const formData = new FormData();
        formData.append('file', element.file);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
          this.dialog.closeAll();
          if (this.listadoArchivos.length === this.j) {
            if (result.success == true) {
              /*this.messageService.add({
                key: 'other',
                severity: 'success',3
                summary: '',
                detail: 'El archivo se guardó correctamente',
              });*/
              this.inHabilitar = true
              this.obtenerAdjuntos();
              this.actualizarEstatusAnexo(20);
            } else {
              this.messageService.add({
                key: 'other',
                severity: 'error',
                summary: '',
                detail: 'Ocurrió un problema, intente nuevamente',
              });
            }
          }
          this.j = this.j + 1;
        });
      }

    });


  }

  enviar_validarAnexo() {
    this.ref = this.dialogService.open(ModalValidarAnexoComponent, {
      header: 'Validar Otros Documentos',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
        isSolCancel: this.isSolCancel
      },
    });

    this.ref.onClose.subscribe((resp: any) => {

      if (resp != null) {
        this.actualizarEstatusAnexo(
          resp ? 21 : 22
        );
        if (resp) {
          this.actualizarEstadoProcesoPostulacion();
        }
      }
    });
  }

  actualizarEstadoProcesoPostulacion = () => {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 4,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.messageService.add({
            key: 'an1',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  }

  actualizarEstatusAnexo = (status: number) => {
    var params = {
      idAnexoDetalle: this.idAnexoStatus,
      idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
      idStatusAnexo: status,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .actualizarEstatusAnexo(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.messageService.add({
            key: 'other',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
          this.display_validar = false;
          this.obtenerAdjuntos();
          this.validarAnexo.emit(true);
        } else {
          this.messageService.add({
            key: 'other',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  obtenerDetalleObservacion = () => {
    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: 7,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerDetalleObservacion(params)
      .subscribe((result: any) => {
        if (result.data?.length > 0) {

          result.data.forEach((element: any) => {
            if (
              element.fechaFinPlazo != null &&
              element.fechaFinPlazo != undefined
            ) {
              element.fechaFinPlazo = ConvertDateToString(
                ConvertNumberToDate(element.fechaFinPlazo)
              );
            }
          });
          this.lstObservacion = result.data;
        }
      });
  };

  verObservaciones = () => {
    this.ref = this.dialogService.open(ModalObservacionComponent, {
      header: 'Listado de observaciones',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        lstObservacion: this.lstObservacion,
      },
    });
  };

}
