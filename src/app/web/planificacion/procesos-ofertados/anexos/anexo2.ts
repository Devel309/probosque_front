import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { datosUsuario_parcial } from 'src/app/model/user';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { MessageService } from 'primeng/api';
import { HttpParams } from '@angular/common/http';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import {
  ConvertDateToString,
  ConvertNumberToDate,
  DownloadFile,
  ToastService,
} from '@shared';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalObservacionComponent } from '../modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../modal-validar-anexo/modal-validar-anexo.component';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-anexo2',
  templateUrl: '../anexos/anexo2.html',
  styleUrls: ['../anexos/stylos-anexos.scss'],
  providers: [MessageService],
})
export class Anexo2 {
  @Input() idAnexoStatus!: number;
  @Input() isSolCancel: boolean = false;
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();

  usarioActual: datosUsuario_parcial = JSON.parse(
    '' + localStorage.getItem('usuario')?.toString()
  );
  listaProcOferta: any = JSON.parse(
    '' + localStorage.getItem('procesosPostular')
  );

  params_guardar: any = {
    nombreapellidos: '',
    dni: '',
    sector: '',
    idDepartamento: {
      idDepartamento: 0,
    },
    idDistrito: {
      idDistrito: 0,
    },
    idProvincia: {
      idProvincia: 0,
    },
    telefono: '',
    celular: '',
    correo: '',
    razonsocialsolicitante: '',
    profesion: '',
    nroregistrocolegioprofesional: '',
    nroregistroserfor: '',
    lugarfirma: '',
    direccion: '-',
    equipotecnico: '-',
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    codigoAnexo: 'anexo2',
  };

  params_generar: any = {
    codigoAnexo: 'anexo2',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };
  params_obtener: any = {
    codigoAnexo: 'anexo2',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };
  params_adjuntar_anexo: any = {
    codigoanexo: 'anexo2',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };

  file: any = {};

  generar: boolean = false;
  archivoEnviado: boolean = false;

  anexoActual: any = null;

  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;

  mailValido: boolean | null = true;

  isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;

  isGuardarOK: boolean = false;
  isValidDocRegente: boolean = false;

  constructor(
    private serv: PlanificacionService,
    private messageService: MessageService,
    private toast: ToastService,
    private anexoService: AnexoService,
    private dialog: MatDialog,
    private serviceApiForestal: ApiForestalService,
    public dialogService: DialogService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
  ) { }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;
    this.obtenerAnexo();
    this.obtenerAdjuntos();
    this.obtenerDetalleObservacion();
  }
  params_obtener_adjunto: any = {
    codigoAnexo: 'anexo2',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 2,
  };
  obtenerAdjuntos(isClick: boolean = false) {
    const auxLoad = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.obtenerAdjuntos(this.params_obtener_adjunto).subscribe((result: any) => {
      auxLoad.close();
      if (result.data?.length > 0) {
        this.anexoActual = result.data[0];
        this.params_validar_anexo.idDocumentoAdjunto =
          this.anexoActual.idDocumentoAdjunto;
        if (isClick) {
          DownloadFile(
            this.anexoActual.file,
            this.anexoActual.nombreDocumento,
            'application/octet-stream'
          );
        }

        this.generar = false;
        this.archivoEnviado = true;
      }
    }, () => {
      auxLoad.close();
    });
  }

  eliminarDocumentoAdjunto = () => {
    var params = {
      idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.anexoActual = null;
        this.generar = false;
        this.archivoEnviado = false;
        this.file = {};
        this.toast.ok(result.message);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  };

  display_validar: boolean = false;
  params_validar_anexo: any = {
    descripcion: '',
    devolucion: null,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  // archivo
  cargarAchivoAnexo2(e: any) {

  }
  eliminarArchivoAnexo2() {
    this.file = {};
  }
  enviarArchivo() {
    if (!this.file.nombre) {
      this.toast.warn('(*) Debe adjuntar un documento.');
      return;
    }

    var params = new HttpParams()
      .set('CodigoAnexo', 'anexo2')
      .set(
        'IdProcesoPostulacion',
        this.params_adjuntar_anexo.idprocesopostulacion
      )
      .set('IdTipoDocumento', '2')
      .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
      .set('NombreArchivo', this.file.nombre);

    const formData = new FormData();
    formData.append('file', this.file.file);


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.archivoEnviado = true;

        this.obtenerAdjuntos();
        this.actualizarEstatusAnexo(20);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  enviar_validarAnexo() {
    this.ref = this.dialogService.open(ModalValidarAnexoComponent, {
      header: 'Validar Anexo N° 02',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
        isSolCancel: this.isSolCancel
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp != null) {
        this.actualizarEstatusAnexo(resp ? 21 : 22);
        /*if (resp) {
          this.actualizarEstadoProcesoPostulacion();
        }*/
      }
    });
  }

  actualizarEstadoProcesoPostulacion = () => {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario'))
        .correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 4,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.messageService.add({
            key: 'an1',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  actualizarEstatusAnexo = (status: number) => {
    var params = {
      idAnexoDetalle: this.idAnexoStatus,
      idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
      idStatusAnexo: status,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .actualizarEstatusAnexo(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.messageService.add({
            key: 'an2',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
          this.display_validar = false;
          this.obtenerAnexo();
          this.obtenerAdjuntos();
          this.validarAnexo.emit(true);
        } else {
          this.messageService.add({
            key: 'an2',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  obtenerUsuarioPostulante = () => {
    var params = {
      codigoAnexo: 'anexo2',
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .obtenerUsuarioPostulante(params)
      .subscribe((resp: any) => {

      });
  };

  obtenerAnexo() {
    this.params_obtener.idProcesoPostulacion = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion;
    this.serv.obtenerAnexo(this.params_obtener).subscribe((result: any) => {
      if (result.data?.length > 0) {
        result.data.forEach((element: any) => {
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Nombres y Apellidos del Regente'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.nombreapellidos = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Dni'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.dni = element.value;
          }

          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Direccion'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.direccion = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Sector'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.sector = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'IdDistrito'.replace(/ /g, '').toUpperCase()
          ) {
            this.distritoSelect = Number(element.value);
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'IdProvincia'.replace(/ /g, '').toUpperCase()
          ) {
            this.provinciaSelect = Number(element.value);
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'IdDepartamento'.replace(/ /g, '').toUpperCase()
          ) {
            this.departamentoSelect = Number(element.value);
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Telf. Fijo'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.telefono = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Telf. Celular'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.celular = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Correo Electronico'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.correo = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Razon Social del Solicitante de la Concesion'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.razonsocialsolicitante = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Profesion'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.profesion = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'N° de Registro en el Colegio Profesional'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.nroregistrocolegioprofesional = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'N° de Registro de Regente en el SERFOR'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.nroregistroserfor = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Equipo Tecnico'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.equipotecnico = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Lugar y Fecha'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.lugarfirma = element.value;
          }
        });
        this.listarPorFiltroDepartamento();
      } else this.listarPorFiltroDepartamento();
    });
  }

  guardaranexo() {
    this.params_guardar.idDepartamento.idDepartamento = this.departamentoSelect;
    this.params_guardar.idProvincia.idProvincia = this.provinciaSelect;
    this.params_guardar.idDistrito.idDistrito = this.distritoSelect;

    let params = {
      anexo1: null,
      anexo2: this.params_guardar,
      anexo3: null,
      idProcesoPostulacion: this.params_obtener.idProcesoPostulacion,
      idUsuarioPostulacion: this.params_obtener.idUsuarioPostulacion,
      procesopostulacion: {
        fechaPostulacion: new Date(),
        fechaRecepcionDocumentos: null,
        idEstatusProceso: null,
        idProcesoOferta: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoOferta,
        idProcesoPostulacion: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoPostulacion,
        idRegenteForestal: 33,
        idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        observacion: null,
        recepcionDocumentos: false,
      },
    };

    if (!this.validar(params)) return;
    this.valido = '*';

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.guardaProcesoPostulacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.isGuardarOK = true;
        this.actualizarEstatusAnexo(19);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  generaranexo() {
    let datosCompeltos = this.validar(this.params_generar);

    if (!datosCompeltos) {
      this.messageService.add({
        key: 'an2',
        severity: 'error',
        summary: '',
        detail: 'Hay campos obligatorios por ser llenados',
      });
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.generaranexo2(this.params_generar).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        let p_file = this.base64ToArrayBuffer(result.archivo);
        var blob = new Blob([p_file], {
          type: result.contenTypeArchivo,
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        var fileName = result.nombeArchivo;
        link.download = fileName;
        link.click();
        this.generar = true;
      } else {
        this.messageService.add({
          key: 'an2',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  }

  valido: String = '*';
  validar(obj: any) {
    let val = false;
    if (
      document.getElementsByClassName('anexo2_required').length == null ||
      document.getElementsByClassName('anexo2_required').length == 0
    ) {
      this.valido = '';
      val = true;
    } else {
      this.valido = '*';
      val = false;
      this.toast.warn('(*) Hay campos obligatorios por ser llenados');
    }

    if (this.params_guardar.dni && !this.isValidDocRegente) {
      this.toast.warn('(*) Debe presionar en buscar para validar el número de documento de regente ingresado.');
      val = false;
    }

    if (this.params_guardar.dni && String(this.params_guardar.dni).length !== 8) {
      this.toast.warn('(*) El número de documento debe de ser de 8 dígitos.');
      val = false;
    }

    if (this.params_guardar.celular && String(this.params_guardar.celular).length !== 9) {
      this.toast.warn('(*) El número de teléfono celular debe de ser de 9 dígitos.');
      val = false;
    }

    if (this.params_guardar.telefono && String(this.params_guardar.telefono).length < 6) {
      this.toast.warn('(*) El número de teléfono fijo debe de tener 6 dígitos como mínimo.');
      val = false;
    }



    return val;
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  departamentos: any = [];
  departamentoSelect: any = null;
  provincias: any = [];
  provinciaSelect: any = null;
  distritos: any = [];
  distritoSelect: any = null;
  listarPorFiltroDepartamento(ischange: boolean = false) {
    let params = {
      codDepartamento: null,
      codDepartamentoInei: null,
      codDepartamentoReniec: null,
      codDepartamentoSunat: null,
      idDepartamento: null,
      nombreDepartamento: null,
    };
    this.serv
      .listarPorFiltroDepartamento_core_central(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.departamentos = result.data;
          this.departamentoSelect =
            ischange || !this.departamentoSelect
              ? result.data[2].idDepartamento
              : this.departamentoSelect; // por ahora
          this.listarPorFilroProvincia(ischange);
        }
      });
  }
  listarPorFilroProvincia(ischange: boolean = false) {
    this.serv
      .listarPorFilroProvincia({
        idDepartamento: this.departamentoSelect,
      })
      .subscribe((result: any) => {
        if (result.success) {
          this.provincias = result.data;
          this.provinciaSelect =
            ischange || !this.provinciaSelect
              ? result.data[0].idProvincia
              : this.provinciaSelect;
          this.listarPorFilroDistrito(ischange);
        }
      });
  }
  listarPorFilroDistrito(ischange: boolean = false) {
    this.serv
      .listarPorFilroDistrito({ idProvincia: this.provinciaSelect })
      .subscribe((result: any) => {
        if (result.success) {
          this.distritos = result.data;
          this.distritoSelect =
            ischange || !this.distritoSelect
              ? result.data[0].idDistrito
              : this.distritoSelect;
        }
      });
  }

  selectDPD(obj: any, origen: string) {
    if (origen == 'd') {
      // this.departamentoSelect = obj;
      this.listarPorFilroProvincia(true);
    }
    if (origen == 'p') {
      // this.provinciaSelect = obj;
      this.listarPorFilroDistrito(true);
    }
  }

  buscarRegente = () => {
    if (this.params_guardar.dni === '') {
      this.toast.warn('(*) Debe ingresar un número de documento de regente');
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serviceApiForestal.consultarRegente().subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.dataService?.length > 0) {
          const regente = resp.dataService.find((x: any) => x.numeroDocumento.toString() === this.params_guardar.dni.toString());
          if (regente) {
            this.params_guardar.nombreapellidos = `${regente.nombres} ${regente.apellidos}`;
            this.params_guardar.nroregistroserfor = regente.numeroLicencia;
            this.isValidDocRegente = true;
            this.toast.ok('Número de documento en base de datos de regente encontrado.');
          } else {
            this.toast.warn('Número de documento en base de datos de regente  no encontrado.');
          }
        }
      }, () => {
        this.dialog.closeAll();
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      });
    }
  };

  obtenerDetalleObservacion = () => {
    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: 2,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerDetalleObservacion(params)
      .subscribe((result: any) => {
        if (result.data?.length > 0) {

          result.data.forEach((element: any) => {
            if (
              element.fechaFinPlazo != null &&
              element.fechaFinPlazo != undefined
            ) {
              element.fechaFinPlazo = ConvertDateToString(
                ConvertNumberToDate(element.fechaFinPlazo)
              );
            }
          });
          this.lstObservacion = result.data;
        }
      });
  };

  verObservaciones = () => {
    this.ref = this.dialogService.open(ModalObservacionComponent, {
      header: 'Listado de observaciones',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        lstObservacion: this.lstObservacion,
      },
    });
  };

  validarCorreo = () => {

    this.mailValido = true;
    var EMAIL_REGEX =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!this.params_guardar.correo.match(EMAIL_REGEX)) {
      this.mailValido = false;
    }

  };
}
