import { HttpParams } from '@angular/common/http';
import { EventEmitter } from '@angular/core';
import { Component, ElementRef, Input, Output, ViewChild } from '@angular/core';
import {
  ConvertDateToString,
  ConvertNumberToDate,
  DownloadFile,
  ToastService,
} from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { datosUsuario_parcial } from 'src/app/model/user';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ModalObservacionComponent } from '../modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../modal-validar-anexo/modal-validar-anexo.component';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
@Component({
  selector: 'app-anexo3',
  templateUrl: '../anexos/anexo3.html',
  styleUrls: ['../anexos/stylos-anexos.scss'],
  providers: [MessageService],
})
export class Anexo3 {
  @Input() idAnexoStatus!: number;
  @Input() isSolCancel: boolean = false;
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();

  usarioActual: datosUsuario_parcial = JSON.parse(
    '' + localStorage.getItem('usuario')?.toString()
  );
  listaProcOferta: any = localStorage.getItem('procesosPostular')?.toString();
  params_guardar: any = {
    nomapellfuncionrespon: '',
    nomapellreprelegal: '',
    dnireprelegal: '',
    denomipersojuridica: '',
    cargoFuncioArffssRespo: '',
    rucpersojuridica: '',
    monto1: '',
    monto2: '',
    monto3: '',
    fechafirma: new Date(),
    lugarfirma: '',

    codigoAnexo: 'anexo3',
    descdocumentos: '',
    direccpersojuridica: '',

    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };
  params_generar: any = {
    codigoAnexo: 'anexo3',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };
  params_obtener: any = {
    codigoAnexo: 'anexo3',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };
  file: any = {};
  params_adjuntar_anexo: any = {
    codigoanexo: 'anexo3',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };

  generar: boolean = false;
  archivoEnviado: boolean = false;
  anexoActual: any = null;

  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;
  isGuardar: boolean = false;
  isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;

  isGuardarOK: boolean = false;

  constructor(
    private serv: PlanificacionService,
    private messageService: MessageService,
    private toast: ToastService,
    private anexoService: AnexoService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
  ) { }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;
    this.obtenerAnexo();
    this.obtenerAdjuntos();
    this.obtenerDetalleObservacion();
  }

  obtenerAnexo() {
    this.serv.obtenerAnexo(this.params_obtener).subscribe((result: any) => {
      if (result.data?.length > 0) {
        result.data.forEach((element: any) => {
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Nombres y Apellidos del Funcionario Responsable de la Concesion'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.nomapellfuncionrespon = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Cargo del funcionario de la ARFFS respinsable de la concesion'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.cargoFuncioArffssRespo = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Nombres y Apellidos del Representante Legal'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.nomapellreprelegal = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Dni del Representante Legal'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.dnireprelegal = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Denominacion de la Persona Juridica'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.denomipersojuridica = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Ruc de la Persona Juridica'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.rucpersojuridica = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Domicilio de la Persona Juridica'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.direccpersojuridica = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Descripcion de Documentos'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.descdocumentos = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Monto Estados Financieros'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.monto1 = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Monto Reporte Situciacion Crediticia'
              .replace(/ /g, '')
              .toUpperCase()
          ) {
            this.params_guardar.monto2 = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Monto Documentos de Bienes'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.monto3 = element.value;
          }
          if (
            element.descripcion.replace(/ /g, '').toUpperCase() ===
            'Lugar de Firma'.replace(/ /g, '').toUpperCase()
          ) {
            this.params_guardar.lugarfirma = element.value;
          }
        });
      }
    });
  }
  guardaranexo() {
    let params = {
      anexo1: null,
      anexo2: null,
      anexo3: this.params_guardar,
      idProcesoPostulacion: this.params_obtener.idProcesoPostulacion,
      idUsuarioPostulacion: this.params_obtener.idUsuarioPostulacion,
      procesopostulacion: {
        fechaPostulacion: new Date(),
        fechaRecepcionDocumentos: null,
        idEstatusProceso: null,
        idProcesoOferta: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoOferta,
        idProcesoPostulacion: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoPostulacion,
        idRegenteForestal: 33,
        idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        observacion: null,
        recepcionDocumentos: false,
      },
    };

    if (!this.validar(params)) return;
    this.valido = '*';

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.guardaProcesoPostulacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.isGuardarOK = true;
        this.actualizarEstatusAnexo(19);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }
  generaranexo() {
    let datosCompeltos = this.validar(this.params_generar);

    if (!datosCompeltos) {
      this.messageService.add({
        key: 'an3',
        severity: 'error',
        summary: '',
        detail: 'Hay campos obligatorios por ser llenados',
      });
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.generaranexo3(this.params_generar).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        let p_file = this.base64ToArrayBuffer(result.archivo);
        var blob = new Blob([p_file], {
          type: result.contenTypeArchivo,
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        var fileName = result.nombeArchivo;
        link.download = fileName;
        link.click();
        this.generar = true;
      } else {
        this.messageService.add({
          key: 'an3',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    }, () => {
      this.dialog.closeAll();
    });
  }

  valido: String = '*';
  validar(obj: any) {
    let val = false;
    if (
      document.getElementsByClassName('anexo3_required').length == null ||
      document.getElementsByClassName('anexo3_required').length == 0
    ) {
      this.valido = '';
      val = true;
    } else {
      this.valido = '*';
      val = false;
      this.toast.warn('Hay campos obligatorios por ser llenados');
    }

    if (this.params_guardar.dnireprelegal && String(this.params_guardar.dnireprelegal).length !== 8) {
      this.toast.warn('(*) El número de DNI debe de ser de 8 dígitos.');
      val = false;
    }

    if (this.params_guardar.monto1) {
      let auxMonto = Number(this.params_guardar.monto1).toFixed(2);
      this.params_guardar.monto1 = auxMonto;
    }

    if (this.params_guardar.monto2) {
      let auxMonto = Number(this.params_guardar.monto2).toFixed(2);
      this.params_guardar.monto2 = auxMonto;
    }

    if (this.params_guardar.monto3) {
      let auxMonto = Number(this.params_guardar.monto3).toFixed(2);
      this.params_guardar.monto3 = auxMonto;
    }

    return val;
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  display_validar: boolean = false;

  params_validar_anexo: any = {
    descripcion: '',
    devolucion: false,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  enviar_validarAnexo() {
    this.ref = this.dialogService.open(ModalValidarAnexoComponent, {
      header: 'Validar Anexo N° 03',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
        isSolCancel: this.isSolCancel
      },
    });

    this.ref.onClose.subscribe((resp: any) => {

      if (resp != null) {
        this.actualizarEstatusAnexo(resp ? 21 : 22);
        /*if (resp) {
          this.actualizarEstadoProcesoPostulacion();
        }*/
      }
    });
  }

  actualizarEstadoProcesoPostulacion = () => {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 4,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.messageService.add({
            key: 'an1',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  actualizarEstatusAnexo = (status: number) => {
    var params = {
      idAnexoDetalle: this.idAnexoStatus,
      idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
      idStatusAnexo: status,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .actualizarEstatusAnexo(params)
      .subscribe((result: any) => {
        if (result.success) {
          if (this.isGuardar == false) {
            this.messageService.add({
              key: 'an3',
              severity: 'success',
              summary: '',
              detail: 'Se actualizó correctamente',
            });
            this.isGuardar == false;
          }
          this.display_validar = false;
          this.obtenerAnexo();
          this.obtenerAdjuntos();
          this.validarAnexo.emit(true);
        } else {
          this.messageService.add({
            key: 'an3',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  params_obtener_adjunto: any = {
    codigoAnexo: 'anexo3',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 3,
  };
  obtenerAdjuntos(isClick: boolean = false) {
    const auxLoad = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.obtenerAdjuntos(this.params_obtener_adjunto).subscribe((result: any) => {
      auxLoad.close();
      if (result.data?.length > 0) {
        this.anexoActual = result.data[0];
        this.params_validar_anexo.idDocumentoAdjunto =
          this.anexoActual.idDocumentoAdjunto;
        if (isClick) {
          DownloadFile(
            this.anexoActual.file,
            this.anexoActual.nombreDocumento,
            'application/octet-stream'
          );
        }

        this.generar = false;
        this.archivoEnviado = true;
      }
    }, () => {
      auxLoad.close();
    });
  }

  eliminarDocumentoAdjunto = () => {
    var params = {
      idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.anexoActual = null;
        this.generar = false;
        this.archivoEnviado = false;
        this.file = {};
        this.toast.ok(result.message);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  };

  // archivo
  cargarAchivoAnexo3(e: any) {

  }
  eliminarArchivoAnexo3() {
    this.file = {};
  }
  enviarArchivo() {
    if (!this.file.nombre) {
      this.toast.warn('(*) Debe adjuntar un documento.');
      return;
    }

    var params = new HttpParams()
      .set('CodigoAnexo', 'anexo3')
      .set('IdProcesoPostulacion', this.params_adjuntar_anexo.idprocesopostulacion)
      .set('IdTipoDocumento', '3')
      .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
      .set('NombreArchivo', this.file.nombre);

    const formData = new FormData();
    formData.append('file', this.file.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.isGuardar = true;
        this.archivoEnviado = true;
        this.toast.ok(result.message);

        this.obtenerAdjuntos();
        this.actualizarEstatusAnexo(20);
      } else {
        this.toast.error(Mensajes.MSJ_ERROR_CATCH);
      }
    }, () => {
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  obtenerDetalleObservacion = () => {
    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: 3,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerDetalleObservacion(params)
      .subscribe((result: any) => {
        if (result.data?.length > 0) {

          result.data.forEach((element: any) => {
            if (
              element.fechaFinPlazo != null &&
              element.fechaFinPlazo != undefined
            ) {
              element.fechaFinPlazo = ConvertDateToString(
                ConvertNumberToDate(element.fechaFinPlazo)
              );
            }
          });
          this.lstObservacion = result.data;
        }
      });
  };

  verObservaciones = () => {
    this.ref = this.dialogService.open(ModalObservacionComponent, {
      header: 'Listado de observaciones',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        lstObservacion: this.lstObservacion,
      },
    });
  };
}
