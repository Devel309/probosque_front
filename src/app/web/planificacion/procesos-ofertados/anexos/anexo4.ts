import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { datosUsuario_parcial } from 'src/app/model/user';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { MessageService } from 'primeng/api';
import { ConvertDateToString, ConvertNumberToDate, DownloadFile } from '@shared';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalObservacionComponent } from '../modal-observacion/modal-observacion.component';
import { ModalValidarAnexoComponent } from '../modal-validar-anexo/modal-validar-anexo.component';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
@Component({
  selector: 'app-anexo4',
  templateUrl: '../anexos/anexo4.html',
  styleUrls: ['../anexos/stylos-anexos.scss'],
  providers: [MessageService],
})
export class Anexo4 {
  @Input() idAnexoStatus!: number;
  @Input() isSolCancel: boolean = false;
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();

  usarioActual: datosUsuario_parcial = JSON.parse(
    '' + localStorage.getItem('usuario')?.toString()
  );
  listaProcOferta: any = localStorage.getItem('procesosPostular')?.toString();

  params_adjuntar_anexo: any = {
    codigoanexo: 'anexo4',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };
  params_validar_anexo: any = {
    descripcion: '',
    devolucion: false,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: null,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  anexoActual: any = null;
  generar: boolean = false;
  archivoEnviado: boolean = false;

  params_generar: any = {
    codigoAnexo: 'anexo4',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };

  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;
  paramsGuardar = new HttpParams();
  archivoAdjunto: any = {};

  isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;




  constructor(
    private serv: PlanificacionService,
    private messageService: MessageService,
    private anexoService: AnexoService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
  ) { }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;
    this.obtenerAdjuntos();
    this.obtenerDetalleObservacion();
  }

  descargarGuia() {
    this.serv.descargarguianexo4().subscribe((result: any) => {
      if (result.success) {
        let p_file = this.base64ToArrayBuffer(result.archivo);
        var blob = new Blob([p_file], {
          type: result.contenTypeArchivo,
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        var fileName = result.nombeArchivo;
        link.download = fileName;
        link.click();
        this.generar = true;
      } else {
        this.messageService.add({
          key: 'an4',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  }
  generaranexo() {
    this.serv.generaranexo4(this.params_generar).subscribe((result: any) => {
      if (result.success) {
        let p_file = this.base64ToArrayBuffer(result.archivo);
        var blob = new Blob([p_file], {
          type: result.contenTypeArchivo,
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        var fileName = result.nombeArchivo;
        link.download = fileName;
        link.click();
      } else {
        this.messageService.add({
          key: 'an4',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  display_validar: boolean = false;
  params_obtener_adjunto: any = {
    codigoAnexo: 'anexo4',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 4,
  };
  obtenerAdjuntos(isClick: boolean = false) {
    const auxLoad = this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv
      .obtenerAdjuntos(this.params_obtener_adjunto)
      .subscribe((result: any) => {
        auxLoad.close();
        if (result.data?.length > 0) {
          this.anexoActual = result.data[0];
          this.params_validar_anexo.idDocumentoAdjunto =
            this.anexoActual.idDocumentoAdjunto;

          this.archivoAdjunto.nombre = this.anexoActual.nombreDocumento;
          this.archivoAdjunto.file = this.anexoActual.file;
          this.archivoAdjunto.url = 'application/octet-stream';

          if (isClick) {
            DownloadFile(
              this.anexoActual.file,
              this.anexoActual.nombreDocumento,
              'application/octet-stream'
            );
          }

          this.generar = false;
          this.archivoEnviado = true;
        }
      }, () => auxLoad.close());
  }

  eliminarDocumentoAdjunto = () => {
    this.archivoAdjunto = {};
    var params = {
      idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      if (result.success) {
        this.anexoActual = null;
        this.generar = false;
        this.archivoEnviado = false;
      }
    });
  };

  enviar_validarAnexo() {
    this.ref = this.dialogService.open(ModalValidarAnexoComponent, {
      header: 'Validar Anexo N° 04',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
        isSolCancel: this.isSolCancel
      },
    });

    this.ref.onClose.subscribe((resp: any) => {

      if (resp != null) {
        this.actualizarEstatusAnexo(
          resp ? 21 : 22
        );
        /* if(resp){
           this.actualizarEstadoProcesoPostulacion();
         }*/
      }
    });
  }

  actualizarEstadoProcesoPostulacion = () => {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 4,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.messageService.add({
            key: 'an1',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  }

  actualizarEstatusAnexo = (status: number) => {
    var params = {
      idAnexoDetalle: this.idAnexoStatus,
      idDocumentoAdjunto: this.params_validar_anexo.idDocumentoAdjunto,
      idStatusAnexo: status,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.anexoService
      .actualizarEstatusAnexo(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.messageService.add({
            key: 'an4',
            severity: 'success',
            summary: '',
            detail: 'Se actualizó correctamente',
          });
          this.display_validar = false;
          this.obtenerAdjuntos();
          this.validarAnexo.emit(true);
        } else {
          this.messageService.add({
            key: 'an4',
            severity: 'error',
            summary: '',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  guardarArchivo() {

    if(this.anexoActual!=null && this.anexoActual.idDocumentoAdjunto!=null){
      this.eliminarDocumentoAdjunto();
    }

    const formData = new FormData();
    formData.append('file', this.file.file);
    // formData.append('file', this.file);


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(this.paramsGuardar, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.archivoEnviado = true;
        this.obtenerAdjuntos();
        this.actualizarEstatusAnexo(20);
      } else {
        this.messageService.add({
          key: 'an4',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });

  }

  @Input() file: any = {};
  eliminarArchivoPrevio() {
    this.archivoAdjunto = {};
  }

  enviarArchivo(event: any) {
    var params = new HttpParams()
      .set('CodigoAnexo', 'anexo4')
      .set(
        'IdProcesoPostulacion',
        this.params_adjuntar_anexo.idprocesopostulacion
      )
      .set('IdTipoDocumento', '4')
      .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
      .set('NombreArchivo', this.archivoAdjunto.nombre);

    this.paramsGuardar = params;

    this.file.url = URL.createObjectURL(event.file);
    this.file.file = event.file;
    this.file.nombre = event.file.name;



  }

  obtenerDetalleObservacion = () => {
    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: 4,
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };
    this.anexoService
      .obtenerDetalleObservacion(params)
      .subscribe((result: any) => {
        if (result.data?.length > 0) {

          result.data.forEach((element: any) => {
            if (element.fechaFinPlazo != null && element.fechaFinPlazo != undefined) {
              element.fechaFinPlazo = ConvertDateToString(
                ConvertNumberToDate(element.fechaFinPlazo)
              );
            }
          });
          this.lstObservacion = result.data;
        }
      });
  };

  verObservaciones = () => {
    this.ref = this.dialogService.open(ModalObservacionComponent, {
      header: 'Listado de observaciones',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        lstObservacion: this.lstObservacion,
      },
    });
  };

}
