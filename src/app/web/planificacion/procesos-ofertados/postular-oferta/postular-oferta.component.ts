import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { datosUsuario_parcial } from 'src/app/model/user';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { Anexo1 } from '../anexos/anexo1';
import { Anexo2 } from '../anexos/anexo2';
import { Anexo3 } from '../anexos/anexo3';
import { Anexo4 } from '../anexos/anexo4';

@Component({
  selector: 'app-postular-oferta',
  templateUrl: './postular-oferta.component.html',
  styleUrls: ['./postular-oferta.component.scss'],
  providers: [MessageService],
})
export class PostularOfertaComponent implements OnInit {
  opcionesAdicionales: any = [
    { id: 0, descripcion: 'Agegar anexo' },
    { id: 5, descripcion: 'Anexo 5' },
    { id: 6, descripcion: 'Anexo 6' },
    { id: 7, descripcion: 'Anexo 7' },
    { id: 8, descripcion: 'Anexo 8' },
  ];
  opcionesAdicionalesSelect: any;
  opcionesAdicionalesSelects: any = [];
  listaProcOferta: any = JSON.parse(
    '' + localStorage.getItem('procesosPostular')
  );
  estadoAnexo1: String = 'Nuevo';
  estadoAnexo2: String = 'Nuevo';
  estadoAnexo3: String = 'Nuevo';
  estadoAnexo4: String = 'Nuevo';

  lstProcesosOferta: any[] = [];
  totalRegistrosPO: number = 0;

  constructor(
    public dialog: MatDialog,
    private messageService: MessageService,
    private router: Router,
    private serv: PlanificacionService
  ) {}

  ngOnInit() {
    this.listarProcesoOferta();
    setTimeout(() => {
      this.mensajePostulaciones();
    }, 800);
  }

  mensajePostulaciones() {
    let mensaje = 'Se postula a los procesos: ';
    this.listaProcOferta.forEach((element: any) => {
      mensaje += element.idProcesoOferta + ',';
    });

    mensaje = mensaje.toString().slice(0, -1);
    this.messageService.add({
      key: 'tl',
      severity: 'success',
      summary: 'DE ACUERDO',
      detail: mensaje,
    });
  }

  listarProcesoOferta() {
    var params = {
      agrupada: null,
      idProcesoOferta: null,
      idStatusProceso: null,
      idTipoProceso: null,
    };

    this.serv.listarProcesoOferta(params).subscribe((result: any) => {
      this.lstProcesosOferta = result.data;
      this.totalRegistrosPO = result.data.length;
      //console.log(result);
      
      //console.log(this.totalRegistrosPO);
    });
  }
  regresar() {
    this.router.navigate(['/mcsniffs/planificacion/procesos-oferta']);
  }

  postular() {}

  selectDpOpcionales(param: String) {
    
    let index = this.opcionesAdicionalesSelects.indexOf(
      this.opcionesAdicionalesSelect
    );
    if (param == 'add') {
      if (index == -1) {
        this.opcionesAdicionalesSelects.push(this.opcionesAdicionalesSelect);
      }

      
    }
    if (param == 'remove') {
      this.opcionesAdicionalesSelects.splice(index, 1);
    }
  }

  openDialoga1() {
    if (this.listaProcOferta.length > 0) {
      this.dialog.open(Anexo1, {
        disableClose: true,
        height: '80%',
        width: '90%',
      }).beforeClosed();
    }
  }
  openDialoga2() {
    if (this.listaProcOferta.length > 0) {
      this.dialog.open(Anexo2, {
        disableClose: true,
        height: '80%',
        width: '90%',
      });
    }
  }
  openDialoga3() {
    if (this.listaProcOferta.length > 0) {
      this.dialog.open(Anexo3, {
        disableClose: true,
        height: '80%',
        width: '90%',
      });
    }
  }
  openDialoga4() {
    if (this.listaProcOferta.length > 0) {
      this.dialog.open(Anexo4, {
        disableClose: true,
        height: '50%',
        width: '40%',
      });
    }
  }
}
