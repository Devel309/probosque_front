import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcesosOfertadosComponent } from './procesos-ofertados.component';

describe('ProcesosOfertadosComponent', () => {
  let component: ProcesosOfertadosComponent;
  let fixture: ComponentFixture<ProcesosOfertadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcesosOfertadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcesosOfertadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
