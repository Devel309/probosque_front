import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { ConvertDateToString, ConvertNumberToDate } from 'src/app/shared/util';
import { ModalNuevaConsultaSolicitudComponent } from './modal/modal-ampliacion-solicitud/modal-ampliacion-solicitud.component';

@Component({
  selector: 'app-consulta-solicitud',
  templateUrl: './ampliacion-solicitudes.component.html',
})
export class ConsultaSolicitudesComponent implements OnInit {
  tipoProcesoSelect!: number | null;
  procesoPostulacionSelect!: String | null;
  selectedestadosolicitudes!: SelectItem | null;
  ref!: DynamicDialogRef;

  estadosolicitudes: SelectItem[] = [
    { label: '-- Seleccione --', value: null },
    { label: 'Pendiente de Evaluación', value: 1 },
    { label: 'Aprobado', value: 2 },
    { label: 'Rechazado', value: 3 },
  ];

  totalRecords: number = 0;
  optionPage: any = { pageNum: 1, pageSize: 10 };
  usuario = {} as UsuarioModel;
  isPerfilARFFS: boolean = false;

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private procesoPostulaionService: ProcesoPostulaionService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    public dialogService: DialogService,
    private usuariServ: UsuarioService
  ) {
    this.usuario = this.usuariServ.usuario;
  }

  procesosOfertaFiltros: any = {
    idProceso: 0,
    razonSocial: '',
    ruc: '',
    estado: '',
  };

  lstProcesosOferta: any[] = [];
  tipoSolicitud: any[] = [];

  ngOnInit() {
    this.isPerfilARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    this.listarTipoSolicitud();
    this.listarAmpliacionSolicitud();
  }

  listarTipoSolicitud = () => {
    this.ampliacionSolicitudService.listarTipoSolicitud().subscribe((resp: any) => {
      resp.data.splice(0, 0, { idTipoSolicitud: null, descripcion: '-- Seleccione --' });
      this.tipoSolicitud = resp.data;
    });
  };

  listarAmpliacionSolicitud = () => {
    const params = {
      idEstado: this.selectedestadosolicitudes || null,
      idProcesoPostulacion: this.procesoPostulacionSelect || null,
      idTipoSolicitud: this.tipoProcesoSelect || null,
      idUsuarioRegistro: this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL ? null : this.usuario.idusuario,
      pageNum: this.optionPage.pageNum,
      pageSize: this.optionPage.pageSize,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.listarSolicitudesAmpliacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        result.data.forEach((element: any) => {
          if (element.fechaInicioSolicitud != null) {
            element.fechaInicioSolicitudString = ConvertDateToString(ConvertNumberToDate(element.fechaInicioSolicitud));
          }
          if (element.fechaFinSolicitud != null) {
            element.fechaFinSolicitudString = ConvertDateToString(ConvertNumberToDate(element.fechaFinSolicitud));
          }
        });
        this.lstProcesosOferta = result.data;
        this.totalRecords = result.totalRecord || 0;
        
        if(result.data.length === 0) this.toast.warn(result.message);

      } else {
        this.lstProcesosOferta = [];
        this.totalRecords = 0;
      }
    }, (error) => this.errorMensaje(error, true));
  };

  verAmpliacionSolicitud = (item: any = null, typeConsult: number = 1) => {
    this.ref = this.dialogService.open(ModalNuevaConsultaSolicitudComponent, {
      header: typeConsult === 1 ? 'Registro Solicitud' : 'Aprobar o Rechazar Solicitud',
      width: '700px',
      style: {margin: '15px'},
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        isNew: typeConsult === 1 ? true : false,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        
        this.listarAmpliacionSolicitud();
      }
    });
  };

  verDetalle = (item: any) => {
    if(item.vigencia){
      this.toast.warn('Pasaron los 30 días de vigencia de la solicitud.');
      return;
    }

    this.verAmpliacionSolicitud(item, 2);
  };

  loadData(event: any) {
    this.optionPage.pageNum = event.first + 1;
    this.optionPage.pageSize = event.rows;
    this.listarAmpliacionSolicitud();
  }

  btnLimpiar() {
    this.tipoProcesoSelect = null;
    this.procesoPostulacionSelect = null;
    this.selectedestadosolicitudes = null;
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.listarAmpliacionSolicitud();
  }

  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
