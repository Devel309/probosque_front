import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { TipoSolicitud } from 'src/app/shared/const';
import { ConvertDateToString, ConvertNumberToDate, funDateBetween, DownloadFile, } from 'src/app/shared/util';
@Component({
  selector: 'app-modal-nueva-consulta-solicitud',
  templateUrl: './modal-ampliacion-solicitud.component.html',
})
export class ModalNuevaConsultaSolicitudComponent implements OnInit {
  solicitudRequestEntity: any = {
    idTipoSolicitud: 0,
  };

  idProcesoPostulacion: number | string = '-- Seleccione --';

  autoResize: boolean = true;
  tipoSolicitud: any[] = [];
  procesoPostulacion: any[] = [];
  procesoPostulacionNuevo: any[] = [];

  idUsuarioPostulacion!: number;

  aprobado: boolean | null = null;
  rechazada: boolean = false;

  fileFormato!: any;
  fileInforme!: any;

  fileImpugnacion: any = {};
  fileImpSAN!: any;

  constTipoSolicitud = TipoSolicitud;

  idSolicitud!: number;
  idDocumentoAdjunto!: number;

  cont: number = 0;
  isFundada!: boolean;

  nombredocumentoImpugnacion: string = '';
  documentoImpugnacion: string = '';

  nombredocumentoImpugnacionRespuesta: string = '';
  documentoImpugnacionRespuesta: string = '';

  nombredocumentoImpSAN: string = '';
  documentoImpSAN: string = '';
  nombredocumentoImpSANRespuesta: string = '';
  documentoImpSANRespuesta: string = '';

  notaTotal!: number;

  nombreUsuarioPostulante: string = '';
  descripcionStatusProceso: string = '';
  idProcesoOferta: string = '';
  impugnacionSAN: boolean = false;
  perfil: any = null;


  usuario = {} as UsuarioModel;
  minDate!: Date;

  fileDeclaracionJurada: any = {};
  filePropuestaExploracion: any = {};
  fileInformeExploracion: any = {};
  fileResolucionResumen: any = {};
  fileAmpliacion: any = {};

  isAuthExploracion: boolean = false;
  isAmpliacionPlazo: boolean = false;
  isBtnEnviarAuthExploracion: boolean = false;
  isBtnEnviarAuthExploAprobar: boolean = false;
  isBtnEnviarAmpliacion: boolean = false;
  isBtnEnviarAmpliacionAprobar: boolean = false;
  isAuthInformeExploracion: boolean = false;
  isPerfilARFFS: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private messageService: MessageService,
    private toast: ToastService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private serv: PlanificacionService,
    private seguridadService: SeguridadService,
    private usuariServ: UsuarioService,
    private dialog: MatDialog,
  ) {
    this.usuario = this.usuariServ.usuario;
  }

  ngOnInit() {
    this.isPerfilARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear, 0, 1);

    this.listarTipoSolicitud();

    this.solicitudRequestEntity.idTipoSolicitud = 0;
    this.solicitudRequestEntity.idUsuarioRegistro =
      this.solicitudRequestEntity.idUsuarioSolicitante = this.usuariServ.idUsuario
    this.perfil = this.usuario.perfiles[0].idPerfil

    if (!this.config.data.isNew) {
      this.listarProcesoPostulacionStatus();
    }

    if (this.config.data.isNew) this.solicitudRequestEntity.aprobado = false;
    else if (this.config.data.isInforme) this.listarSolicitudesAmpliacion();
    else this.obtenerSolicitudAmpliacion();

    if (this.config.data.item?.tipoSolicitudDesc == "Impugnación al SAN") this.impugnacionSAN = true;
  }

  listarTipoSolicitud = () => {
    this.ampliacionSolicitudService.listarTipoSolicitud().subscribe((resp: any) => {
      this.tipoSolicitud = resp.data;
    });
  };

  listarSolicitudesAmpliacion = () => {
    var obj = {
      aprobado: null,
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
      idTipoSolicitud: null,
    };
    this.ampliacionSolicitudService.listarSolicitudesAmpliacion(obj).subscribe((result: any) => {
      result.data.forEach((element: any) => {
        if (element.fechaInicioSolicitud != null) {
          element.fechaInicioSolicitud = ConvertDateToString(ConvertNumberToDate(element.fechaInicioSolicitud));
        }
        if (element.fechaFinSolicitud != null) {
          element.fechaFinSolicitud = ConvertDateToString(ConvertNumberToDate(element.fechaFinSolicitud));
        }
      });

      this.solicitudRequestEntity = result.data.filter(
        (x: any) =>
          x.idTipoSolicitud === TipoSolicitud.autorizacionExpliracion
      )[0];
    });
  };

  listarProcesoPostulacionStatus = () => {
    var obj = {
      idStatus: 12,
      idProcesoOferta: this.config.data.item.idProcesoOferta,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService.listarProcesoPostulacion(obj).subscribe((resp: any) => {
      resp.data.splice(0, 0, { idProcesoPostulacion: '-- SELECCIONE --' });
      this.procesoPostulacionNuevo = resp.data.filter((x: any) => x.idProcesoPostulacion != this.solicitudRequestEntity.idProcesoPostulacion);
    });
  };

  // listarProcesoPostulacion = () => {
  //   var obj = {
  //     idStatus: this.solicitudRequestEntity.idTipoSolicitud === 5 ? 11 : 2,
  //     idProcesoOferta: null,
  //     idProcesoPostulacion: null,
  //     idUsuarioPostulacion: null,
  //   };

  //   this.procesoPostulaionService.listarProcesoPostulacion(obj).subscribe((resp: any) => {
  //     this.procesoPostulacion = [];
  //     resp.data.splice(0, 0, { idProcesoPostulacion: '-- SELECCIONE --', });
  //     this.procesoPostulacion = [...resp.data];
  //   });
  // };


  // listarComboProcesoPostulacion() {
  //   this.ampliacionSolicitudService.listarSolicitudesVencidas().subscribe((result: any) => {
  //     this.procesoPostulacion = result.data;
  //   })
  // }

  obtenerSolicitudAmpliacion = () => {
    const params = { idSolicitud: this.config.data.item.idSolicitud };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.obtenerSolicitudAmpliacion(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.data.fechaInicioSolicitud != null) {
        resp.data.fechaInicioSolicitud = ConvertNumberToDate(resp.data.fechaInicioSolicitud);
      }
      if (resp.data.fechaFinSolicitud != null) {
        resp.data.fechaFinSolicitud = ConvertNumberToDate(resp.data.fechaFinSolicitud);
      }

      if (resp.data.fechaRecepcionDocsFisicos != null) {
        resp.data.fechaRecepcionDocsFisicos = ConvertNumberToDate(resp.data.fechaRecepcionDocsFisicos);
      }

      this.solicitudRequestEntity = resp.data;
      this.idSolicitud = this.solicitudRequestEntity.idSolicitud;

      if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
        this.isAuthExploracion = true;

        if (this.solicitudRequestEntity.aprobado && this.solicitudRequestEntity.flagAutorizacion == null && !this.isPerfilARFFS) {
          let fechaHoy = new Date();
          if (funDateBetween(this.solicitudRequestEntity.fechaInicioSolicitud, this.solicitudRequestEntity.fechaFinSolicitud, fechaHoy)) {
            this.isBtnEnviarAuthExploracion = true;
          }
        }

        if (this.isPerfilARFFS && this.solicitudRequestEntity.aprobado === null) {
          this.isBtnEnviarAuthExploAprobar = true;
        }

        if (this.solicitudRequestEntity.aprobado) {
          this.isAuthInformeExploracion = true;
        }
      }

      if (this.solicitudRequestEntity.aprobado != null) {
        this.aprobado = this.solicitudRequestEntity.aprobado;
        this.rechazada = !this.solicitudRequestEntity.aprobado;
      }

      if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion) {
        this.isAmpliacionPlazo = true;

        if (this.isPerfilARFFS && this.solicitudRequestEntity.aprobado === null) {
          this.isBtnEnviarAmpliacionAprobar = true;
        }
      }

      if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.impugnacion) {
        this.obtenerArchivosSolicitudes();
        this.seleccionarNuevoProceso();
      } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.impSAN) {
        this.obtenerArchivosSolicitudes();
        this.seleccionarNuevoProceso();
      } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
        this.obtenerArchivosSolicitudes();
      } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion) {
        this.obtenerArchivosSolicitudes();
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  obtenerArchivosSolicitudes = () => {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.obtenerArchivosSolicitudes(this.config.data.item.idSolicitud).subscribe((resp: any) => {
      this.dialog.closeAll();
      resp.data.forEach((element: any) => {
        if (element.idTipoDocumento === 20) {
          this.nombredocumentoImpugnacion = element.nombre;
          this.documentoImpugnacion = element.file;
        } else if (element.idTipoDocumento === 21) {
          this.nombredocumentoImpugnacionRespuesta = element.nombre;
          this.documentoImpugnacionRespuesta = element.file;
        } else if (element.idTipoDocumento === 9) {
          this.fileDeclaracionJurada = element;
        } else if (element.idTipoDocumento === 10) {
          this.filePropuestaExploracion = element;
        } else if (element.idTipoDocumento === 12) {
          this.fileInformeExploracion = element;
        } else if (element.idTipoDocumento === 78) {
          this.fileResolucionResumen = element;
        } else if (element.idTipoDocumento === 17) {
          this.fileAmpliacion = element;
        }
      });
    }, () => {
      this.dialog.closeAll();
    });
  };


  validarRegistrarSolicitudAmpliacion() {
    let validar: boolean = true;
    let mensaje: string = '';


    if (!this.solicitudRequestEntity.idTipoSolicitud) {
      validar = false;
      mensaje = mensaje += '(*) Seleccione Tipo Solicitud.\n';
    }

    if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion && !this.solicitudRequestEntity.diaAmpliacion) {
      validar = false;
      mensaje = mensaje += '(*) Ingrese dias de Ampliación.\n';
    }

    if (!this.solicitudRequestEntity.idProcesoPostulacion) {
      validar = false;
      mensaje = mensaje += '(*) Seleccione Proceso postulación.\n';
    }

    if (!this.solicitudRequestEntity.motivoSolicitud && this.solicitudRequestEntity.idTipoSolicitud !==
      this.constTipoSolicitud.impSAN && this.solicitudRequestEntity.idTipoSolicitud !==
      this.constTipoSolicitud.autorizacionExpliracion) {
      validar = false;
      mensaje = mensaje += '(*) Ingrese Motivo Solicitud.\n';
    }

    if (this.solicitudRequestEntity.idTipoSolicitud ===
      this.constTipoSolicitud.ampliacionPlazoPublicacion && !this.fileAmpliacion) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar archivo de solicitud.\n';
    }

    if (this.solicitudRequestEntity.idTipoSolicitud ===
      this.constTipoSolicitud.impugnacion && !this.fileImpugnacion) {
      validar = false;
      mensaje = mensaje += '(*) Adjuntar archivo de impugnación.\n';
    }

    if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.impSAN) {
      if (!this.fileImpSAN) {
        validar = false;
        mensaje = mensaje += '(*) Adjuntar archivo de impugnación al SAN.\n';
      }

      if (!this.solicitudRequestEntity.descripcion) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese descripción.\n';
      }

      if (!this.solicitudRequestEntity.asunto) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese Asunto.\n';
      }
    }

    // PARA TIPO AUTTORIZACION DE EEXPLORACION
    if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
      if (!this.solicitudRequestEntity.asunto) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese Asunto.\n';
      }

      if (!this.solicitudRequestEntity.fechaInicioSolicitud) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese fecha de inicio.\n';
      }

      if (!this.solicitudRequestEntity.fechaFinSolicitud) {
        validar = false;
        mensaje = mensaje += '(*) Ingrese fecha de fin.\n';
      }

      if (this.solicitudRequestEntity.fechaInicioSolicitud && this.solicitudRequestEntity.fechaFinSolicitud) {
        if (this.solicitudRequestEntity.fechaInicioSolicitud >= this.solicitudRequestEntity.fechaFinSolicitud) {
          validar = false;
          mensaje = mensaje += '(*) La fecha final debe ser mayor a la fecha inicial.\n';
        } else if (!this.validarRangoFechas(this.solicitudRequestEntity.fechaFinSolicitud, this.solicitudRequestEntity.fechaInicioSolicitud)) {
          validar = false;
          mensaje = mensaje += '(*) El rango entre fechas es máximo de un año.\n';
        }
      }

      if (!this.fileDeclaracionJurada.nombre) {
        validar = false;
        mensaje = mensaje += '(*) Adjuntar declaración jurada.\n';
      }

      if (!this.filePropuestaExploracion.nombre) {
        validar = false;
        mensaje = mensaje += '(*) Adjuntar propuesta de exploración.\n';
      }
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  validarRangoFechas(dateFin: Date, dateInicio: Date): boolean {
    let validado = false;
    let auxFechaFin = new Date(ConvertDateToString(dateFin));
    let auxFechaInicio = new Date(ConvertDateToString(dateInicio));
    let diff = Math.floor(auxFechaFin.getTime() - auxFechaInicio.getTime());
    let day = 1000 * 60 * 60 * 24;
    let days = Math.floor(diff / day);

    if (days <= 365) validado = true;

    return validado;
  }

  enviarArchivoDeclaracionJurada = () => {
    if (!this.fileDeclaracionJurada.nombre) return;

    var params = new HttpParams()
      .set('CodigoAnexo', 'Declaracion Jurada')
      .set('IdTipoDocumento', '9')
      .set('IdProcesoPostulacion', this.solicitudRequestEntity.idProcesoPostulacion)
      .set('IdUsuarioAdjunta', this.usuario.idusuario.toString())
      .set('NombreArchivo', this.fileDeclaracionJurada.nombre);

    const formData = new FormData();
    formData.append('file', this.fileDeclaracionJurada.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.cont = this.cont + 1;
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivos();
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  enviarArchivoPropuesta = () => {
    if (!this.filePropuestaExploracion.nombre)
      return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Propuesta exploracion')
      .set('IdTipoDocumento', '10')
      .set('IdProcesoPostulacion', this.solicitudRequestEntity.idProcesoPostulacion)
      .set('IdUsuarioAdjunta', this.usuario.idusuario.toString())
      .set('NombreArchivo', this.filePropuestaExploracion.nombre);

    const formData = new FormData();
    formData.append('file', this.filePropuestaExploracion.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.cont = this.cont + 1;
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivos();
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  enviarArchivoInformeExplora = () => {
    if (!this.fileInformeExploracion.nombre) return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Informe de exploración')
      .set('IdTipoDocumento', '12')
      .set('IdProcesoPostulacion', this.solicitudRequestEntity.idProcesoPostulacion)
      .set('IdUsuarioAdjunta', this.usuario.idusuario.toString())
      .set('NombreArchivo', this.fileInformeExploracion.nombre);

    const formData = new FormData();
    formData.append('file', this.fileInformeExploracion.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.cont = 2;
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivos();
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  enviarArchivoResolucionResumen = () => {
    if (!this.fileResolucionResumen.nombre) return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Resolución y Resumen')
      .set('IdTipoDocumento', '78')
      .set('IdProcesoPostulacion', this.solicitudRequestEntity.idProcesoPostulacion)
      .set('IdUsuarioAdjunta', this.usuario.idusuario.toString())
      .set('NombreArchivo', this.fileResolucionResumen.nombre);

    const formData = new FormData();
    formData.append('file', this.fileResolucionResumen.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.cont = 2;
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivos();
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  actualizarEstadoProcesoPostulacionPosicion = () => {
    let params = {
      correoPostulante: this.usuario.correoElectronico,
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
      idStatus: this.config.data.item.posicion === 0 ? 2 : 8,
      idUsuarioModificacion: this.usuario.idusuario,
    };

    this.ampliacionSolicitudService.actualizarEstadoProcesoPostulacion(params).subscribe((resp: any) => {
      if (resp.success) {
        this.toast.ok('Se actualizó correctamente');
        this.ref.close(true);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    });
  };

  adjuntarArchivos = () => {
    var objAdjuntArchiv = {
      idDocumentoAdjunto: this.idDocumentoAdjunto,
      idSolicitud: this.idSolicitud,
      idUsuarioRegistro: this.usuario.idusuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.registrarDocumentoAdjunto(objAdjuntArchiv).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.success) {
        // if (
        //   this.solicitudRequestEntity.idTipoSolicitud ===
        //   this.constTipoSolicitud.ampliacionPlazoPublicacion
        // )
        //   this.cont = this.cont + 1;


        if (this.cont === 4) {
          this.actualizarEstadoProcesoPostulacionPosicion();
        } else if (this.cont === 6) {
          this.solicitudRequestEntity.aprobado = false;
          this.solicitudRequestEntity.fechaFinSolicitud = null;
          this.solicitudRequestEntity.fechaInicioSolicitud = null;
          this.enviarAprobarRechazarSolicitud();
        } else if (this.cont === 7) {
          this.actualizarEstadoProcesoPostulacion();
        } else if (this.cont === 3 && this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
          this.enviarAprobarRechazarSolicitud();
        } else if (this.cont === 2 && this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
          this.ref.close(this.idSolicitud);
        } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion ||
          this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.impugnacion) {
          this.ref.close(this.idSolicitud);
        }
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  registrarSolicitudAmpliacion = () => {
    if (!this.validarRegistrarSolicitudAmpliacion()) return;

    if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion) {
      this.enviarAmpliacion();
    } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
      this.enviarAmpliacion();
    } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.impugnacion) {
      this.enviarImpugnacion();
    } else if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.impSAN) {
      this.enviarImpSAN();
    }
  };

  registrarInformeExploracion() {
    if (!this.fileInformeExploracion.nombre) {
      this.toast.warn('(*) Debe adjuntar informe de exploración.');
      return;
    }
    this.actualizarAmpliacion();
  }

  enviarSolicitudAprobar() {
    if (!this.validarEnviarAprobarExploracion()) return;

    const params = {
      "idSolicitud": this.solicitudRequestEntity.idSolicitud,
      "aprobado": this.solicitudRequestEntity.aprobado,
      "idProcesoPostulacion": this.solicitudRequestEntity.idProcesoPostulacion,
      "idTipoSolicitud": this.solicitudRequestEntity.idTipoSolicitud,
      "idUsuarioModificacion": this.usuario.idusuario,
    }

    const auxFiles: any[] = [
      {
        "idDocumentoAdjunto": this.fileDeclaracionJurada.idDocumentoAdjunto,
        "conforme": this.fileDeclaracionJurada.conforme,
        "observacion": this.fileDeclaracionJurada.observacion,
        "idUsuarioModificacion": this.usuario.idusuario,
      },
      {
        "idDocumentoAdjunto": this.filePropuestaExploracion.idDocumentoAdjunto,
        "conforme": this.filePropuestaExploracion.conforme,
        "observacion": this.filePropuestaExploracion.observacion,
        "idUsuarioModificacion": this.usuario.idusuario,
      }
    ]

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.actualizarAprobadoSolicitud(params).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.success) {
        this.registrarObservacionDocumentoAdjunto(auxFiles);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }

    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });

  }

  enviarSolicitudAmpliacionAprobar() {
    if (!this.validarEnviarAprobarAmpliacion()) return;

    const params = {
      "idSolicitud": this.solicitudRequestEntity.idSolicitud,
      "aprobado": this.solicitudRequestEntity.aprobado,
      "idProcesoPostulacion": this.solicitudRequestEntity.idProcesoPostulacion,
      "idTipoSolicitud": this.solicitudRequestEntity.idTipoSolicitud,
      "idUsuarioModificacion": this.usuario.idusuario,
    };


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.actualizarAprobadoSolicitud(params).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.success) {
        this.toast.ok('Se actualizó correctamente.');
        this.ref.close(data);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }

    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });

  }

  registrarObservacionDocumentoAdjunto(params: any[]) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.registrarObservacionDocumentoAdjunto(params).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.success) {
        this.toast.ok('Se actualizó correctamente.');
        this.enviarArchivoResolucionResumen();
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  validarEnviarAprobarExploracion(): boolean {
    let validado = true;

    if(this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion){
      if (this.fileAmpliacion.conforme === undefined || this.fileAmpliacion.conforme === null) {
        this.toast.warn('(*) Debe seleccionar la conformidad de la declaración jurada.');
        validado = false;
      } else if (this.fileAmpliacion.conforme === false && !this.fileAmpliacion.observacion) {
        this.toast.warn('(*) Debe ingresar la observación para declaración jurada.');
        validado = false;
      } else if (this.fileAmpliacion.conforme === true) {
        this.fileAmpliacion.observacion === null;
      }
    }

    if(this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion){
      if (this.fileDeclaracionJurada.conforme === undefined || this.fileDeclaracionJurada.conforme === null) {
        this.toast.warn('(*) Debe seleccionar la conformidad de la declaración jurada.');
        validado = false;
      } else if (this.fileDeclaracionJurada.conforme === false && !this.fileDeclaracionJurada.observacion) {
        this.toast.warn('(*) Debe ingresar la observación para declaración jurada.');
        validado = false;
      } else if (this.fileDeclaracionJurada.conforme === true) {
        this.fileDeclaracionJurada.observacion === null;
      }

      if (this.filePropuestaExploracion.conforme === undefined || this.filePropuestaExploracion.conforme === null) {
        this.toast.warn('(*) Debe seleccionar la conformidad de la propuesta de exploración.');
        validado = false;
      } else if (this.filePropuestaExploracion.conforme === false && !this.filePropuestaExploracion.observacion) {
        this.toast.warn('(*) Debe ingresar la observación para propuesta de exploración.');
        validado = false;
      } else if (this.filePropuestaExploracion.conforme === true) {
        this.filePropuestaExploracion.observacion === null;
      }

      if (!this.fileResolucionResumen.nombre) {
        this.toast.warn('(*) Debe Adjuntar archivo de resolución y resumen.');
        validado = false;
      }
    }

    if (this.solicitudRequestEntity.aprobado === undefined || this.solicitudRequestEntity.aprobado === null) {
      this.toast.warn('(*) Debe seleccionar la conformidad de la autorización.');
      validado = false;
    }

    return validado;
  }

  validarEnviarAprobarAmpliacion(): boolean {
    let validado = true;

    if (this.solicitudRequestEntity.aprobado === undefined || this.solicitudRequestEntity.aprobado === null) {
      this.toast.warn('(*) Debe seleccionar la conformidad de la autorización.');
      validado = false;
    }

    return validado;
  }

  enviarArchivoAutorizacion = () => {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.fileAmpliacion == undefined)
      return;

    var params = new HttpParams()
      .set('CodigoAnexo', 'Archivo Solicitud')
      .set('IdTipoDocumento', '17')
      .set(
        'IdProcesoPostulacion',
        this.solicitudRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileAmpliacion.nombre);

    const formData = new FormData();
    formData.append('file', this.fileAmpliacion.file);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivos();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  enviarAmpliacion = () => {

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.registrarSolicitudAmpliacion(this.solicitudRequestEntity).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.success) {

        if (data.validateBusiness) {

          this.idSolicitud = data.codigo;
          if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion) {
            this.enviarArchivoAutorizacion();
          } else {
            this.enviarArchivoDeclaracionJurada();
            this.enviarArchivoPropuesta();
          }

          this.toast.ok('Se guardó correctamente.');

        }
        else {
          this.toast.warn(data.message);
        }

      } else {
        this.toast.warn(data.message);
      }

    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  };

  actualizarAmpliacion() {

    this.solicitudRequestEntity.flagAutorizacion = true;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ampliacionSolicitudService.actualizarSolicitudAmpliacion(this.solicitudRequestEntity).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.success) {
        this.toast.ok(data.message);
        this.enviarArchivoInformeExplora();
      } else {
        this.toast.error(data.message);
      }

    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  };

  enviarArchivoInpugnacion = () => {
    if (this.fileImpugnacion == undefined)
      return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Archivo Impugnacion')
      .set('IdTipoDocumento', '20')
      .set(
        'IdProcesoPostulacion',
        this.solicitudRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileImpugnacion.name);

    const formData = new FormData();
    formData.append('file', this.fileImpugnacion);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.cont = 5;
        this.adjuntarArchivos();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  enviarArchivoImpSAN = () => {
    if (this.fileImpSAN == undefined)
      return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Archivo Impugnacion al SAN')
      .set('IdTipoDocumento', '20')
      .set(
        'IdProcesoPostulacion',
        this.solicitudRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileImpSAN.name);

    const formData = new FormData();
    formData.append('file', this.fileImpSAN);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.cont = 5;
        this.adjuntarArchivos();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  enviarImpugnacion = () => {
    this.ampliacionSolicitudService
      .registrarSolicitudAmpliacion(this.solicitudRequestEntity)
      .subscribe((data: any) => {
        if (data.success) {
          this.idSolicitud = data.codigo;
          this.enviarArchivoInpugnacion();
        } else
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
      });
  };

  enviarImpSAN = () => {
    if (this.fileImpSAN == undefined)
      return;
    var params = new HttpParams()
      .set('idSolicitudImpugnacion', this.solicitudRequestEntity.idProcesoPostulacion)
      .set('codigoTipoSolicitud', this.solicitudRequestEntity.idTipoSolicitud)
      .set('asunto', this.solicitudRequestEntity.asunto)
      .set('descripcion', this.solicitudRequestEntity.descripcion)
      .set('auditoria', "")
      .set('idUsuarioRegistro', JSON.parse('' + localStorage.getItem('usuario')).idusuario)

    const formData = new FormData();
    formData.append('file', this.fileImpSAN);

    this.ampliacionSolicitudService
      .solicitudesVencidasEnviarCorreo(params, formData)
      .subscribe((data: any) => {
        if (data.success) {
          this.SuccessMensaje(data.message)
        } else
          this.WarningMessage("Ocurrió un problema, intente nuevamente")
      });
  };

  enviarAprobarRechazarSolicitud = () => {
    this.ampliacionSolicitudService.aprobarRechazarSolicitud(this.solicitudRequestEntity).subscribe((data: any) => {
      if (data.success) this.ref.close(data.codigo);
      else this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  };

  aprobarRechazarSolicitud = () => {
    if (this.aprobado || this.rechazada) {
      this.solicitudRequestEntity.aprobado = this.aprobado;
      if (
        this.solicitudRequestEntity.idTipoSolicitud ===
        this.constTipoSolicitud.autorizacionExpliracion &&
        this.aprobado
      ) {
        this.enviarArchivoFormato();
      } else {
        this.enviarAprobarRechazarSolicitud();
      }
    } else {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe marcar Aprobar o Rechazar',
      });
    }
  };

  aprobar = () => {
    this.aprobado = true;
    this.rechazada = false;
  };

  rechazar = () => {
    this.aprobado = false;
    this.rechazada = true;
  };

  descargarArchivoAmpliacion = () => {
    this.ampliacionSolicitudService.descargarDeclaracionJurada().subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    });
  };

  descargarDeclaracionJuradaPlant = () => {
    this.ampliacionSolicitudService.descargarDeclaracionJurada().subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    });
  };

  descargarPropuestaExploracionPlant = () => {
    this.ampliacionSolicitudService.descargarPropuestaExploracion().subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    });
  };

  descargarResolucionResumenPlant = () => {
    this.ampliacionSolicitudService.descargarResolucionResumen().subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    });
  };

  descargarFormatoAprobacion = () => {
    this.ampliacionSolicitudService.descargarFormatoAprobacion().subscribe((data: any) => {
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    });
  };

  descargarImpugnacion = () => {
    DownloadFile(
      this.documentoImpugnacion,
      this.nombredocumentoImpugnacion,
      'application/octet-stream'
    );
  };

  descargarImpSAN = () => {
    DownloadFile(
      this.documentoImpSAN,
      this.nombredocumentoImpSAN,
      'application/octet-stream'
    );
  };

  eliminarFileDeclaracionJurada() {
    this.fileDeclaracionJurada = {};
  }
  eliminarFilePropuestaExploracion() {
    this.filePropuestaExploracion = {};
  }
  eliminarFileInformeExploracion() {
    this.fileInformeExploracion = {};
  }
  eliminarFileResolucionResumen() {
    this.fileResolucionResumen = {};
  }
  eliminarFileAmpliacion() {
    this.fileAmpliacion = {};
  }

  cargarAchivoFormato(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileFormato = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoInforme(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileInforme = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoAmpliacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileAmpliacion = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoImpugnacion(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileImpugnacion = e.srcElement.files[0];
      }
    }
  }

  cargarArchivoImpSAN(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileImpSAN = e.srcElement.files[0];
      }
    }
  }

  cargarInforme(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileImpSAN = e.srcElement.files[0];
      }
    }
  }

  enviarArchivoFormato() {
    var params = new HttpParams()
      .set('CodigoAnexo', 'Formato de exploración')
      .set('IdTipoDocumento', '11')
      .set(
        'IdProcesoPostulacion',
        this.solicitudRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileFormato.name);

    const formData = new FormData();
    formData.append('file', this.fileFormato);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.cont = 3;
        this.idDocumentoAdjunto = result.codigo;
        this.idSolicitud = this.config.data.item.idSolicitud;
        this.adjuntarArchivos();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  }

  enviarInforme = () => {
    if (this.fileInforme == undefined)
      return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Informe')
      .set('IdTipoDocumento', '12')
      .set(
        'IdProcesoPostulacion',
        this.solicitudRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileInforme.name);

    const formData = new FormData();
    formData.append('file', this.fileInforme);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.cont = 4;
        this.adjuntarArchivos();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  actualizarEstadoProcesoPostulacion = () => {
    // ;

    // this.idUsuarioPostulacion = this.procesoPostulacion.find(
    //   (x) => x.idProcesoPostulacion === this.idProcesoPostulacion
    // ).idUsuarioPostulacion;

    var params = {
      idusuario: this.config.data.item.idUsuarioPostulacion,
    };
    this.seguridadService.obtenerUsuario(params).subscribe((resp: any) => {
      var params = {
        correoPostulante: resp.data[0].correoElectronico,
        idProcesoPostulacion: this.idProcesoPostulacion,
        idStatus: 11,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
      };

      this.ampliacionSolicitudService
        .actualizarEstadoProcesoPostulacion(params)
        .subscribe((resp: any) => {
          if (resp.success) {
            this.ampliacionSolicitudService
              .aprobarRechazarSolicitud(this.solicitudRequestEntity)
              .subscribe((data: any) => {
                if (data.success) this.ref.close(data.codigo);
                else
                  this.messageService.add({
                    key: 'tl',
                    severity: 'error',
                    summary: 'ERROR',
                    detail: 'Ocurrió un problema, intente nuevamente',
                  });
              });
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });
    });
  };

  enviarRespuestaImpugnacion = () => {
    // ;
    if (
      this.solicitudRequestEntity.fechaRecepcionDocsFisicos === null ||
      this.solicitudRequestEntity.fechaRecepcionDocsFisicos === undefined ||
      this.solicitudRequestEntity.fechaRecepcionDocsFisicos === ''
    ) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Fecha Recepción Documentos fisicos, es obligatorio',
      });
    } else if (this.solicitudRequestEntity.aprobado === null) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe seleccionar si es fundada o infundada la impugnación',
      });
    } else {
      if (this.solicitudRequestEntity.aprobado) {
        if (
          this.idProcesoPostulacion === null ||
          this.idProcesoPostulacion === '-- Seleccione --' ||
          this.idProcesoPostulacion === undefined
        ) {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Debe seleccionar el nuevo ganador',
          });
        } else if (
          this.fileImpugnacion === null ||
          this.fileImpugnacion === undefined
        ) {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Debe adjuntar un archivo',
          });
        } else {
          this.cont = 7;
          this.solicitudRequestEntity.idProcesoPostulacionGanador =
            this.idProcesoPostulacion;
          this.enviarArchivoRespuesta();
        }
      } else if (this.solicitudRequestEntity.aprobado === false) {
        if (
          this.fileImpugnacion === null ||
          this.fileImpugnacion === undefined
        ) {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Debe adjuntar un archivo',
          });
        } else {
          this.cont = 6;
          this.enviarArchivoRespuesta();
        }
      }
    }
  };

  enviarRespuestaImpSAN() { }

  enviarArchivoRespuesta = () => {
    if (this.fileImpugnacion == undefined)
      return;
    var params = new HttpParams()
      .set('CodigoAnexo', 'Impgunacion respuesta')
      .set('IdTipoDocumento', '21')
      .set(
        'IdProcesoPostulacion',
        this.solicitudRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileImpugnacion.name);

    const formData = new FormData();
    formData.append('file', this.fileImpugnacion);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivos();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  seleccionarNuevoProceso = () => {
    var params = {
      aprobado: null,
      idProcesoOferta: this.config.data.item.idProcesoOferta,
      idProcesoPostulacion: null,
      diaAmpliacion: null,
      idSolicitud: null,
      idTipoSolicitud: null,
    };
    this.ampliacionSolicitudService
      .obtenerNuevoUsuarioGanador(params)
      .subscribe((resp: any) => {
        let existeGanador = resp.data.find((x: any) => x.ganador);

        if (existeGanador) {
          this.nombreUsuarioPostulante = existeGanador.nombreUsuarioPostulante;
          this.idProcesoPostulacion = existeGanador.idProcesoPostulacion;
          this.idProcesoOferta = existeGanador.idProcesoOferta;
          this.notaTotal = existeGanador.notaTotal;
        } else {
          let data = resp.data.find(
            (x: any) => x.idProcesoPostulacion === this.idProcesoPostulacion
          );
          this.nombreUsuarioPostulante = data.nombreUsuarioPostulante;
          this.idProcesoPostulacion = data.idProcesoPostulacion;
          this.idProcesoOferta = data.idProcesoOferta;
          this.notaTotal = data.notaTotal;
        }
      });
  };

  descargarInfundada = () => {
    DownloadFile(
      this.documentoImpugnacionRespuesta,
      this.nombredocumentoImpugnacionRespuesta,
      'application/octet-stream'
    );
  };

  changeTipoDoc() {
    this.isAuthExploracion = false;
    this.isAmpliacionPlazo = false;

    if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.autorizacionExpliracion) {
      this.isAuthExploracion = true;
    }
    if (this.solicitudRequestEntity.idTipoSolicitud === this.constTipoSolicitud.ampliacionPlazoPublicacion) {
      this.isAmpliacionPlazo = true;
    }
  }

  btnCerrarModal() {
    this.ref.close();
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({ severity: 'error', summary: 'ERROR', detail: mensaje });
  }


}
