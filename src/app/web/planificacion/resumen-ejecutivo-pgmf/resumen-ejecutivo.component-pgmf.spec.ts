import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenEjecutivoPgmfComponent } from './resumen-ejecutivo.component-pgmf';

describe('ResumenEjecutivoComponent', () => {
  let component: ResumenEjecutivoPgmfComponent;
  let fixture: ComponentFixture<ResumenEjecutivoPgmfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumenEjecutivoPgmfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenEjecutivoPgmfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
