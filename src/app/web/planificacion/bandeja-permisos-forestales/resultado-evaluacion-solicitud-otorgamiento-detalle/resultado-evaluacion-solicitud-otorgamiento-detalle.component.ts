import { Component, OnInit } from '@angular/core';
import { EstadoTipo } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalNotificacionCasoDesfavorableComponent } from './modal/notificacion-caso-desfavorable/notificacion-caso-desfavorable.component';
import { ModalNotificacionCasoFavorableComponent } from './modal/notificacion-caso-favorable/notificacion-caso-favorable.component';
import { ModalPlantillaFirmezaResolucionComponent } from './modal/plantilla-firmeza-resolucion/plantilla-firmeza-resolucion.component';

@Component({
  selector: 'app-resultado-evaluacion-solicitud-otorgamiento-detalle',
  templateUrl: './resultado-evaluacion-solicitud-otorgamiento-detalle.component.html',
  styleUrls: ['./resultado-evaluacion-solicitud-otorgamiento-detalle.component.scss']
})
export class ResultadoEvaluacionSolicitudOtorgamientoDetalleComponent implements OnInit {
  EstadoTipo = EstadoTipo;
  home = { icon: 'pi pi-home', routerLink: '/inicio' };
  breadcrumbs: any[];
  ref!: DynamicDialogRef;
  isEmittedFavorable: boolean = false;
  isEmittedDesFavorable: boolean = false

  constructor(
    private messageService: MessageService,
    public dialogService: DialogService,
  ) {
    this.breadcrumbs = [
      {
        label: 'Bandeja de permisos forestales',
        routerLink: '/planificacion/bandeja-permisos-forestales',
      },
      { label: 'Resultado Evaluacion de la solicitud' },
    ];
  }

  ngOnInit(): void {
    
  }

  guardarCasoFavorable() {
    this.ref = this.dialogService.open(ModalNotificacionCasoFavorableComponent, {
      header: "",
      width: "30%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.isEmittedFavorable = resp;
        this.messageService.add({
          severity: 'success',
          detail: 'Guardado correctamente.',
        });
      }
    });
  }

  guardarCasoDesfavorable() {
    this.ref = this.dialogService.open(ModalNotificacionCasoDesfavorableComponent, {
      header: "",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.isEmittedDesFavorable = resp;
        this.messageService.add({
          severity: 'success',
          detail: 'Guardado correctamente.',
        });
      }
    });
  }

  openModalPlantilla() {
    this.ref = this.dialogService.open(ModalPlantillaFirmezaResolucionComponent, {
      header: 'Plantilla "Firmeza de la resolución" ',
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.messageService.add({
          severity: 'success',
          detail: 'Guardado correctamente.',
        });
      }
    });
  }

}
