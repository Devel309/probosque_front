import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-notificacion-caso-desfavorable",
  templateUrl: "./notificacion-caso-desfavorable.component.html",
  styleUrls: ["./notificacion-caso-desfavorable.component.scss"],
})
export class ModalNotificacionCasoDesfavorableComponent implements OnInit {

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  aceptar = () => {
    this.ref.close(true);
  }

}
