import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-notificacion-caso-favorable",
  templateUrl: "./notificacion-caso-favorable.component.html",
  styleUrls: ["./notificacion-caso-favorable.component.scss"],
})
export class ModalNotificacionCasoFavorableComponent implements OnInit {

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  aceptar = () => {
    this.ref.close(true);
  }

}
