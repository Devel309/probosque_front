import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-plantilla-firmeza-resolucion",
  templateUrl: "./plantilla-firmeza-resolucion.component.html",
  styleUrls: ["./plantilla-firmeza-resolucion.component.scss"],
})
export class ModalPlantillaFirmezaResolucionComponent implements OnInit {
  actividadObjt: any = {}
  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  guardar = () => {
    this.ref.close(this.actividadObjt);
  }

}
