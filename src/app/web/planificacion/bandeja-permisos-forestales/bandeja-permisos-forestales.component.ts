import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { SolicitudAccesoService } from 'src/app/service/solicitudAcceso.service';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolicitudService } from '../../../service/solicitud.service';
import { PlanManejoService, UsuarioService } from '@services';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { finalize, tap } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { CodigoPermisoForestal } from '../../../model/util/CodigoPermisoForestal';

@Component({
  selector: 'bandeja-permisos-forestales',
  templateUrl: './bandeja-permisos-forestales.component.html',
  styleUrls: ['./bandeja-permisos-forestales.component.scss'],
})
export class BandejaPermisosForestales implements OnInit {
  solicitudAcceso = {} as SolicitudAccesoModel;

  //Parametro
  lstEstado: any[] = [];
  lstTipoPersona: any[] = [];
  lstTipoActor: any[] = [];
  lstTipoCNCC: any[] = [];
  //bandeja
  lstSolicituAcceso: any[] = [];
  totalRegistrosSA: number = 0;
  visibleTipoCnnc = false;
  objbuscar = {
    dato: '',
    pageNum: 1,
    pageSize: 10,
  };
  totalRecords: number = 0;
  idSolicitudAcceso: number = 0;

  usuario!: UsuarioModel;

  Perfiles = Perfiles;

  CodigoPermisoForestal = CodigoPermisoForestal;

  PFCR = {
    idPermisoForestal: 0,
    descripcion: 'PFCR',
    idSolicitud: 1,
    idTipoProceso: 2,
    idTipoPermiso: 1,
    idTipoEscala: 1,
  };

  //usuario!: UsuarioModel;

  constructor(
    private serv: ParametroValorService,
    private dialog: MatDialog,
    private messageService: MessageService,
    private servSA: SolicitudAccesoService,
    private serPerFores: PermisoForestalService,
    private servPf: PermisoForestalService,
    private serSol: SolicitudService,
    private router: Router,
    private usuarioServ: UsuarioService,
    private apiPlanManejo: PlanManejoService
  ) {}

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;

    this.listarEstado();
    this.obtenerSolicitudAccesoPorUsuario();
    this.listarTipoActor();
    this.listarTipoCNCC();
    //Bandeja Solicitud
  }

  loadData(e: any) {
    const pageSize = Number(e.rows);
    this.objbuscar.pageNum = Number(e.first) / pageSize + 1;
    this.objbuscar.pageSize = pageSize;
    this.ListarPermisosForestales();
  }

  /******************************************************/
  /*Métodos**********************************************/
  /******************************************************/
  listarEstado() {
    var params = { prefijo: 'EPSF' };
    //acá debe obtener de tabla estados
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        codigo: '0',
        valor1: '--Seleccione--',
      });

      this.lstEstado = result.data;

      if (this.usuario.sirperfil == Perfiles.TITULARTH) {
        this.lstEstado.push({
          codigo : "EEVPFEVAL",
          valor1:"En Evaluación"
        })
      }


      this.solicitudAcceso.idestadoSolicitud = '0';
      this.listarTipoPersona();
    });
  }

  listarTipoPersona() {
    var params = { prefijo: 'TPER' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        codigo: '0',
        valor1: '--Seleccione--',
      });

      this.lstTipoPersona = result.data;
      this.solicitudAcceso.codigoTipoPersona = '0';
      this.ListarPermisosForestales();
    });
  }

  listarTipoActor() {
    var params = { prefijo: 'TACT' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        codigo: '0',
        valor1: '--Seleccione--',
      });
      this.lstTipoActor = result.data;
      this.lstTipoActor = this.lstTipoActor.slice(0, 3);

      this.solicitudAcceso.codigoTipoActor = '0';
    });
  }

  listarTipoCNCC() {
    var params = { prefijo: 'TCNC' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        codigo: '0',
        valor1: '--Seleccione--',
      });
      this.lstTipoCNCC = result.data;

      this.solicitudAcceso.codigoTipoCncc = '0';
    });
  }

  verPlan(idPlanManejo: number) {
    this.router.navigateByUrl(
      `/planificacion/revision-permisos-forestales/${idPlanManejo}`
    );
  }

  ListarPermisosForestales(haveParams = false) {
    let params = new HttpParams()
      .set('idTipoProceso', '2')
      .set('idTipoPermiso', '1')
      .set('pageNumber', this.objbuscar.pageNum.toString())
      .set('pageSize', this.objbuscar.pageSize.toString())
      .set('sortType', 'DESC');

    if (
      !!this.solicitudAcceso.idestadoSolicitud ||
      !!this.solicitudAcceso.codigoTipoPersona ||
      !!this.solicitudAcceso.codigoTipoActor ||
      !!this.solicitudAcceso.codigoTipoCncc ||
      !!this.solicitudAcceso.numeroDocumento ||
      !!this.solicitudAcceso.nombres ||
      !!this.solicitudAcceso.razonSocialEmpresa
    ) {
      if (this.solicitudAcceso.idestadoSolicitud != '0') {
        params = params.append(
          'codEstado',
          this.solicitudAcceso.idestadoSolicitud.toString()
        );
      }

      if (this.solicitudAcceso.codigoTipoPersona != '0') {
        params = params.append(
          'codTipoPersona',
          this.solicitudAcceso.codigoTipoPersona.toString()
        );
      }

      if (this.solicitudAcceso.codigoTipoActor != '0') {
        params = params.append(
          'codTipoActor',
          this.solicitudAcceso.codigoTipoActor.toString()
        );
      }

      if (this.solicitudAcceso.codigoTipoCncc != '0') {
        params = params.append(
          'codTipoCncc',
          this.solicitudAcceso.codigoTipoCncc.toString()
        );
      }

      if (!!this.solicitudAcceso.numeroDocumento) {
        params = params.append(
          'numeroDocumento',
          this.solicitudAcceso.numeroDocumento.toString()
        );
      }
      if (!!this.solicitudAcceso.nombres) {
        params = params.append(
          'nombrePersona',
          this.solicitudAcceso.nombres.toString()
        );
      }

      if (!!this.solicitudAcceso.razonSocialEmpresa) {
        params = params.append(
          'razonSocial',
          this.solicitudAcceso.razonSocialEmpresa.toString()
        );
      }
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serPerFores.ListarPermisosForestales(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.lstSolicituAcceso = result.data;
          this.totalRecords = result.totalRecords;
        } else {
          this.ErrorMensaje(result.message);
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.error.message);
      }
    );
  }

  validar(value: any) {
    this.visibleTipoCnnc = value === 'TACTTCNC';
  }

  limpiar() {
    this.solicitudAcceso.codigoTipoPersona = '0';
    this.solicitudAcceso.codigoTipoDocumento = '';
    this.solicitudAcceso.numeroDocumento = '';
    this.solicitudAcceso.nombres = '';
    this.solicitudAcceso.numeroRucEmpresa = '';
    this.solicitudAcceso.codigoTipoActor = '0';
    this.solicitudAcceso.codigoTipoCncc = '0';
    this.solicitudAcceso.estadoSolicitud = '';
    this.solicitudAcceso.razonSocialEmpresa = '';
    this.objbuscar.dato = '';
    this.solicitudAcceso.idestadoSolicitud = '0';
    this.ListarPermisosForestales();
  }

  revisarSolicitud(data: any): void {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([
        '/planificacion/revision-permisos-forestales',
        'ver',
        this.idSolicitudAcceso,
        data.idSolicitud,
      ])
    );
    window.open(url, '_blank');
  }

  evaluarSolicitud(data: any): void {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([
        '/planificacion/revision-permisos-forestales',
        'evaluar',
        this.idSolicitudAcceso,
        data.idSolicitud,
      ])
    );
    window.open(url, '_blank');
  }

  editarSolicitud(data: any): void {
    this.router.navigateByUrl(
      `/planificacion/revision-permisos-forestales/editar/${this.idSolicitudAcceso}/${data.idSolicitud}`
    );
  }

  nuevaSolicitud(): void {
    const body = { ...this.PFCR, idUsuarioRegistro: this.usuario.idusuario };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPermisoForestal(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) =>
        this.router.navigateByUrl(
          `/planificacion/revision-permisos-forestales/${res.codigo}`
        )
      );
  }

  guardarPermisoForestal(body: any) {
    

    return this.serPerFores.registrarPermisoForestal(body).pipe(
      tap({
        next: () =>
          this.SuccessMensaje('Permiso forestal registrado correctamente'),
        error: () =>
          this.ErrorMensaje('Ocurrió un error al registrar Permiso forestal'),
      })
    );
  }

  obtenerSolicitudAccesoPorUsuario() {
    this.servPf
      .obtenerSolicitudAccesoPersonaPorUsuario(this.usuario.idusuario)
      .subscribe(
        (result: any) => {
          this.idSolicitudAcceso = result.data.idSolicitudAcceso;
        },
        (error) => {
          console.log('error', error);
        }
      );
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  registroImpugnacionEmiter(event: any) {
    this.ListarPermisosForestales();
  }
}
