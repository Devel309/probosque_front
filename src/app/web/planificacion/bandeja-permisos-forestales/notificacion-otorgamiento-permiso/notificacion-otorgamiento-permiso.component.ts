import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { EstadoTipo } from '@shared';

@Component({
  selector: 'notificacion-otorgamiento-permiso',
  templateUrl: './notificacion-otorgamiento-permiso.component.html',
  styleUrls: ['./notificacion-otorgamiento-permiso.component.scss']
})
export class NotificacionOtorgamientoPermisoComponent implements OnInit {
  EstadoTipo = EstadoTipo;

  items = ITEMS;
  constructor(private route: Router) {

  }

  ngOnInit(): void {
    
  }

  detalle(item: any) {
    this.route.navigate(['/planificacion/notificacion-otorgamiento-permiso-detalle']);
  }

}


const ITEMS = [
  {
    idSolicitud: 1,
    codUnicoSol: "16-LOR-S001",
    estadoPermiso: "Aprobado",
    estadoPlanManejo: "Aprobado",
    codigoTh: '12-SEC/P-MAD-A-05-15',
    fechaEmision: '16/07/2020',
    fechaFin: '16/07/2020',
    estado: 'Vigente',
    adjunto: true
  },
  {
    idSolicitud: 2,
    codUnicoSol: "16-LOR-S002",
    estadoPermiso: "Desaprobado",
    estadoPlanManejo: "Desaprobado",
    codigoTh: '12-SEC/P-MAD-A-05-15',
    fechaEmision: '16/07/2020',
    fechaFin: '16/07/2020',
    estado: 'Caducado',
    adjunto: true,
  },
  {
    idSolicitud: 3,
    codUnicoSol: "16-LOR-S003",
    estadoPermiso: "Aprobado",
    estadoPlanManejo: "Aprobado",
    codigoTh: '12-SEC/P-MAD-A-05-15',
    fechaEmision: '16/07/2020',
    fechaFin: '16/07/2020',
    estado: 'Vigente',
    adjunto: true,

  },
  {
    idSolicitud: 4,
    codUnicoSol: "16-LOR-S004",
    estadoPermiso: "Aprobado",
    estadoPlanManejo: "Aprobado",
    codigoTh: '12-SEC/P-MAD-A-05-15',
    fechaEmision: '16/07/2020',
    fechaFin: '16/07/2020',
    estado: 'Vigente',
    adjunto: true,

  },
  {
    idSolicitud: 5,
    codUnicoSol: "16-LOR-S005",
    estadoPermiso: "Aprobado",
    estadoPlanManejo: "Aprobado",
    codigoTh: '12-SEC/P-MAD-A-05-15',
    fechaEmision: '16/07/2020',
    fechaFin: '16/07/2020',
    estado: 'Vigente',
    adjunto: true,
  },
  {
    idSolicitud: 6,
    codUnicoSol: "16-LOR-S006",
    estadoPermiso: "Aprobado",
    estadoPlanManejo: "Aprobado",
    codigoTh: '12-SEC/P-MAD-A-05-15',
    fechaEmision: '16/07/2020',
    fechaFin: '16/07/2020',
    estado: 'Vigente',
    adjunto: true,
  }
];