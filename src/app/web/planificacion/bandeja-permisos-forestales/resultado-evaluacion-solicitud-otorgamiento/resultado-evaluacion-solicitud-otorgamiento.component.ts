import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstadoTipo } from '@shared';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-resultado-evaluacion-solicitud-otorgamiento',
  templateUrl: './resultado-evaluacion-solicitud-otorgamiento.component.html',
  styleUrls: ['./resultado-evaluacion-solicitud-otorgamiento.component.scss']
})
export class ResultadoEvaluacionSolicitudOtorgamientoComponent implements OnInit {
  EstadoTipo = EstadoTipo;
  items = ITEMS;  

  constructor(
  
    private route: Router
  ) {
    
  }

  ngOnInit(): void {
    
  }


  detalle(item: any) {
    this.route.navigate(['/planificacion/resultado-evaluacion-solicitud-otorgamiento-detalle']);
  }

}


const ITEMS = [
  {
    idSolicitud: 1,
    codUnicoSol: "16-LOR-S001",
    resultadoEvaluacion: "Favorable",
  },
  {
    idSolicitud: 2,
    codUnicoSol: "16-LOR-S002",
    resultadoEvaluacion: "Favorable",
  },
  {
    idSolicitud: 3,
    codUnicoSol: "16-LOR-S003",
    resultadoEvaluacion: "Desfavorable",
  },
  {
    idSolicitud: 4,
    codUnicoSol: "16-LOR-S004",
    resultadoEvaluacion: "Desfavorable",
  },
  {
    idSolicitud: 5,
    codUnicoSol: "16-LOR-S005",
    resultadoEvaluacion: "Favorable",
  },
  {
    idSolicitud: 6,
    codUnicoSol: "16-LOR-S006",
    resultadoEvaluacion: "Desfavorable",
  }    
];
