import { Component, OnInit } from '@angular/core';

import { MessageService } from 'primeng/api';

@Component({
  selector: 'procedimiento-administrativo-sancionador',
  templateUrl: './procedimiento-administrativo-sancionador.component.html',
  styleUrls: ['./procedimiento-administrativo-sancionador.component.scss']
})
export class ProcedimientoAdministrativoSancionadorComponent implements OnInit {

  constructor(
    private messageService: MessageService
  ) {

  }

  ngOnInit(): void {
    
  }

  notificar(detail: string) {
    this.messageService.add({ severity: 'success', detail });
  }

}


