import { Component, OnInit } from '@angular/core';
import { EstadoTipo } from '@shared';

import { MessageService } from 'primeng/api';

@Component({
  selector: 'notificacion-otorgamiento-permiso-detalle',
  templateUrl: './notificacion-otorgamiento-permiso-detalle.component.html',
  styleUrls: ['./notificacion-otorgamiento-permiso-detalle.component.scss']
})
export class NotificacionOtorgamientoPermisoDetalleComponent implements OnInit {
  EstadoTipo = EstadoTipo;
  home = { icon: 'pi pi-home', routerLink: '/inicio' };
  breadcrumbs: any[];



  constructor(
    private messageService: MessageService
  ) {
    this.breadcrumbs = [
      {
        label: 'Bandeja de permisos forestales',
        routerLink: '/planificacion/bandeja-permisos-forestales',
      },
      { label: 'Notificar el otorgamiento del permiso a las entidades competentes' },
    ];
  }

  ngOnInit(): void {
    
  }

  guardar() {
    this.messageService.add({
      severity: 'success',
      detail: 'Guardado correctamente.',
    });
  }

}
