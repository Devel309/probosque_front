import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { UsuarioModel } from "../../../model/seguridad/usuario";
import { PlanManejoService, UsuarioService } from "@services";
import { Perfiles } from "../../../model/util/Perfiles";
import { EvaluacionService } from "../../../service/evaluacion/evaluacion.service";
import { CodigoEstadoPlanManejo } from "../../../model/util/CodigoEstadoPlanManejo";
@Component({
  selector: "app-plan-general-manejo",
  templateUrl: "./plan-general-manejo.component.html",
  styleUrls: ["./plan-general-manejo.component.scss"],
})
export class PlanGeneralManejoComponent implements OnInit {
  idPlanManejo!: number;
  isPerfilArffs: boolean = false;

  evaluacion!: any;

  usuario!: UsuarioModel;
  tabIndex = 0;
  disabled: boolean = false;
  obj: any = {};

  constructor(
    private route: ActivatedRoute,
    private userSer: UsuarioService,
    private planManejoService: PlanManejoService
  ) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get("idPlan"));
  }

  ngOnInit(): void {
    this.usuario = this.userSer.usuario;

    //console.log("-> ", this.usuario);

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilArffs = true;
    } else {
      this.isPerfilArffs = false;
    }
    this.obtenerPlan();
    //this.obtenerEvaluacion();
  }

  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    };

    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((result: any) => {
        if (
          result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO ||
          result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION
        ) {
          this.disabled = true;
        }
      });
  }

  /*obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          console.log("-----------------------------------------");
          this.evaluacion = result.data[0];
        }
      }
    })
  }*/

  ngAfterViewInit() {
    if (localStorage.getItem("EvalResuDet")) {
      this.obj = JSON.parse(
        "" + localStorage.getItem("EvalResuDet")?.toString()
      );
      this.setTabIndex(this.obj);
    }
    //console.log("MAIN ngAfterViewInit");
  }

  setTabIndex(obj: any) {
    

    var cod = obj.tab.replace(obj.codigoEvaluacionDet, "");

    if (cod === "RE") this.tabIndex = 0;
    else if (cod === "REEVRIIC") {
      this.tabIndex = 0;

      setTimeout(() => {
        
        this.tabIndex = 1;
      }, 1000);
    } else if (cod === "OM") {
      this.tabIndex = 1;
    } else if (cod === "DRP") this.tabIndex = 2;
    else if (cod === "IBAM") this.tabIndex = 3;
    else if (cod === "OAM") this.tabIndex = 4;
    else if (cod === "PPRF") this.tabIndex = 5;
    else if (cod === "MB") this.tabIndex = 6;
    else if (cod === "PB") this.tabIndex = 7;
    else if (cod === "M") this.tabIndex = 8;
    else if (cod === "PC") this.tabIndex = 9;
    else if (cod === "C") this.tabIndex = 10;
    else if (cod === "ODA") this.tabIndex = 11;
    else if (cod === "RMF") this.tabIndex = 12;
    else if (cod === "CA") this.tabIndex = 13;
    else if (cod === "AC") this.tabIndex = 14;
    else if (cod === "A") this.tabIndex = 15;
    else if (cod === "O") this.tabIndex = 16;
    else if (cod === "EC") this.tabIndex = 17;
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }
}
