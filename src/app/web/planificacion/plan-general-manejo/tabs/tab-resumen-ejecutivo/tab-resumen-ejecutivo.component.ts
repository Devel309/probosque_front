import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { MessageService } from 'primeng/api';
import { DepartamentoModel } from '../../../../../model/Departamento';
import { ProvinciaModel } from '../../../../../model/Provincia';
import { DistritoModel } from '../../../../../model/Distrito';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import {
  InformacionGeneralPGMFA,
  PgmfaRegente,
} from 'src/app/model/ResumenEjecutivoPGMFA';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { isNullOrEmpty, ToastService } from '@shared';
import { UsuarioService } from '@services';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { FileModel } from 'src/app/model/util/File';
import { finalize } from 'rxjs/operators';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import * as moment from 'moment';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalInfoPermisoComponent } from 'src/app/shared/components/modal-info-permiso/modal-info-permiso.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-resumen-ejecutivo',
  templateUrl: './tab-resumen-ejecutivo.component.html',
  styleUrls: ['./tab-resumen-ejecutivo.component.scss'],
  providers: [MessageService],
})
export class TabResumenEjecutivoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled: boolean = false;
  //@Input() evaluacion!: any;

  @Output()
  public siguiente = new EventEmitter();

  resumenEjecutivoObj: InformacionGeneralPGMFA = new InformacionGeneralPGMFA();
  selectRegente: PgmfaRegente = new PgmfaRegente();
  nombreCompleto: string = '';
  verModalRegente: boolean = false;
  edit: boolean = false;

  departamento1: string = '';
  distrito1: string = '';
  provincia1: string = '';

  defaultValues = false;

  departamento = {} as DepartamentoModel;
  listDepartamento: DepartamentoModel[] = [];
  provincia = {} as ProvinciaModel;
  listProvincia: ProvinciaModel[] = [];
  distrito = {} as DistritoModel;
  listDistrito: DistritoModel[] = [];

  //codigoTab: string ='PGMFRE';

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_1;

  codigoAcordeonResuEjeDelPlan: string =
    CodigosTabEvaluacion.PGMFA_TAB_1_DEL_PLAN_GENERAL;

  evaluacionResuEjeDelPlan: EvaluacionArchivoModel = new EvaluacionArchivoModel(
    {
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonResuEjeDelPlan,
    }
  );

  evaluacion: any;

  first = 0;
  rows = 10;
  regentes: any[] = [];
  queryRegente: string = '';

  minDate = moment(new Date()).format('YYYY-MM-DD');
  minDateFinal = moment(new Date()).add('days', 1).format('YYYY-MM-DD');
  static get EXTENSIONSAUTHORIZATION2() {
    return ['.pdf', 'image/png', 'image/jpeg', 'image/jpeg', 'application/pdf'];
  }
  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;
  isDisabledInput: boolean = false;

  idPermisoForestal: number = 0;
  idPermisoForestalTemp: number = 0;
  nombreComunidad?: string;
  representanteLegal?: string;
  ref!: DynamicDialogRef;
  displayModal: boolean = false;
  areatotal!: number;
  nroTituloPropiedadComunidad = null;

  constructor(
    private servCoreCentral: CoreCentralService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private apiForestal: ApiForestalService,
    private informacionGeneralService: InformacionGeneralService,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private evaluacionService: EvaluacionService,
    private permisoForestalService: PermisoForestalService,
    private dialogService: DialogService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = 'PDF';

    this.listarPorFiltroDepartamento();
    this.listResumenEjecutivo();
    // this.listarArchivoPMFI();

    //console.log("this.evaluacion -***** ", this.evaluacion)
    if (this.isPerfilArffs) {
      this.isDisabledInput = true;

      var x = document.getElementsByClassName('evaluacion');
      var i;
      for (i = 0; i < x.length; i++) {
        (<HTMLElement>x[i]).setAttribute('disabled', 'disabled');
      }

      this.obtenerEvaluacion();
    }
  }

  listarPorFiltroDepartamento() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        this.listarPorFilroProvincia(this.provincia);
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia(provincia: ProvinciaModel) {
    this.servCoreCentral.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral.listarPorFilroDistrito(distrito).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  Distrito(param: any) {
    this.resumenEjecutivoObj.idDistritoRepresentante = param.value;
  }

  // ------------- lo que esta funcionando -------------//

  listResumenEjecutivo() {
    this.edit = false;
    var params = {
      codigoProceso: 'PGMFA',
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService.listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        // this.dialog.closeAll();
        if (response.data.length != 0) {
          this.edit = true;
          this.defaultValues = false;

          const element = response.data[0];

          this.resumenEjecutivoObj = new InformacionGeneralPGMFA(element);
          if (!isNullOrEmpty(element.fechaInicioDema)) {
            this.resumenEjecutivoObj.fechaInicioDema = new Date(element.fechaInicioDema);
          }
          if (!isNullOrEmpty(element.fechaFinDema)) {
            this.resumenEjecutivoObj.fechaFinDema = new Date(element.fechaFinDema);
          }
          this.departamento1 = element.departamento;
          this.provincia1 = element.provincia;
          this.distrito1 = element.distrito;
          this.idPermisoForestal = element.idPermisoForestal;

          if (element.codTipoPersona === 'TPERJURI') {
            this.nombreComunidad = this.resumenEjecutivoObj.nombreElaboraDema;
            this.representanteLegal = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;
          } else {
            this.representanteLegal = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaboraDema + ' ' + element.apellidoMaternoElaboraDema;
          }
          this.nombreCompleto = element.regente ? element.regente.nombres + ' ' + element.regente.apellidos : '';
        }
      });
  }

  openModal() {

    let messageHeader='Buscar Solicitud de Permiso Forestal'

    if(!!this.idPermisoForestal){
      messageHeader='Solicitud de Permiso Forestal'
    }
    this.ref = this.dialogService.open(ModalInfoPermisoComponent, {
      header: messageHeader,
      width: '60%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        buscar: true,
        nroDocumento: this.usuarioService.nroDocumento,
        idTipoDocumento: this.usuarioService.idtipoDocumento,
        idPlanManejo: this.idPlanManejo,
        idPermisoForestal: this.idPermisoForestal,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.displayModal = true;
        this.idPermisoForestalTemp = resp.idPermisoForestal;
        this.areatotal = resp.areaComunidad;
        this.nroTituloPropiedadComunidad = resp.nroTituloPropiedad;
      }
    });
  }

  closeModal() {
    this.displayModal = false;
    this.listarInformacionGeneralPermisoForestal();
  }

  listarInformacionGeneralPermisoForestal() {
    var params = {
      codTipoInfGeneral: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idPermisoForestal: this.idPermisoForestalTemp,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .listarInformacionGeneralPF(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];

          this.resumenEjecutivoObj = new InformacionGeneralPGMFA();

          this.defaultValues = true;
          this.departamento.idDepartamento = element.idDepartamento;
          this.provincia.idProvincia = element.idProvincia;
          this.distrito.idDistrito = element.idDistrito;
          this.departamento1 = element.departamentoTitular;
          this.provincia1 = element.provinciaTitular;
          this.distrito1 = element.distritoTitular;

          this.resumenEjecutivoObj.idPermisoForestal =
            element.idPermisoForestal;
          this.resumenEjecutivoObj.codTipoPersona = element.codTipoPersona;
          this.resumenEjecutivoObj.esReprLegal = element.esReprLegal;
          this.resumenEjecutivoObj.idDistritoRepresentante = element.idDistrito;
          this.resumenEjecutivoObj.areaTotal = this.areatotal;
          this.resumenEjecutivoObj.nroTituloPropiedadComunidad =
            this.nroTituloPropiedadComunidad;
          if (element.codTipoPersona === 'TPERJURI') {
            this.resumenEjecutivoObj.nombreElaboraDema =
              element.federacionComunidad;
            this.resumenEjecutivoObj.nroRucComunidad = element.rucComunidad;
            this.resumenEjecutivoObj.direccionLegalTitular =
              element.domicilioLegal;
            this.resumenEjecutivoObj.codTipoDocumentoRepresentante =
              element.tipoDocumentoElaborador;
            this.resumenEjecutivoObj.documentoRepresentante =
              element.documentoElaborador;
            this.resumenEjecutivoObj.nombreRepresentante =
              element.nombreElaborador;
            this.resumenEjecutivoObj.apellidoPaternoRepresentante =
              element.apellidoPaternoElaborador;
            this.resumenEjecutivoObj.apellidoMaternoRepresentante =
              element.apellidoMaternoElaborador;
            this.resumenEjecutivoObj.dniElaboraDema = element.rucComunidad;

            this.nombreComunidad = element.federacionComunidad;
          } else {
            this.resumenEjecutivoObj.codTipoDocumentoElaborador =
              element.tipoDocumentoElaborador;
            this.resumenEjecutivoObj.dniElaboraDema =
              element.documentoElaborador;
            this.resumenEjecutivoObj.nombreElaboraDema =
              element.nombreElaborador;
            this.resumenEjecutivoObj.apellidoPaternoElaboraDema =
              element.apellidoPaternoElaborador;
            this.resumenEjecutivoObj.apellidoMaternoElaboraDema =
              element.apellidoMaternoElaborador;
          }
          this.representanteLegal =
            element.nombreElaborador +
            ' ' +
            element.apellidoPaternoElaborador +
            ' ' +
            element.apellidoMaternoElaborador;
        }

        this.resumenEjecutivoObj.regente = new PgmfaRegente();
      });
  }

  listarRegentes() {
    this.apiForestal.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {
        this.regentes.push({
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
        });
      });
    });
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegentes();
    }
  }

  guardarRegente() {
    this.nombreCompleto =
      this.selectRegente.nombres + ' ' + this.selectRegente.apellidos;
    this.resumenEjecutivoObj.regente = new PgmfaRegente(this.selectRegente);
    this.resumenEjecutivoObj.regente.idPlanManejo = this.idPlanManejo;
    this.resumenEjecutivoObj.regente.idUsuarioRegistro =
      this.usuarioService.idUsuario;
    this.verModalRegente = false;
  }

  abrirModalRegentes() {
    this.listarRegentes();
    this.selectRegente = new PgmfaRegente();
    this.verModalRegente = true;
    this.queryRegente = '';
  }

  registrarResumenEjecutivo() {
    if(!this.validarInformacion()) return;
    this.resumenEjecutivoObj.fechaElaboracionPmfi = new Date();
    let fechaInicio!: Date;
    let fechaFin!: Date;
    if (!isNullOrEmpty(this.resumenEjecutivoObj.fechaInicioDema)) {
      fechaInicio = new Date(this.resumenEjecutivoObj.fechaInicioDema);
    }
    if (!isNullOrEmpty(this.resumenEjecutivoObj.fechaFinDema)) {
      fechaFin = new Date(this.resumenEjecutivoObj.fechaFinDema);
    }

    var params = new InformacionGeneralPGMFA(this.resumenEjecutivoObj);
    params.fechaInicioDema = fechaInicio;
    params.fechaFinDema = fechaFin;
    params.idPlanManejo = this.idPlanManejo;
    params.idUsuarioRegistro = this.usuarioService.idUsuario;
    params.codigoProceso = 'PGMFA';
    params.idInformacionGeneralDema = 0;
    params.idDistritoRepresentante = this.distrito.idDistrito;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .registrarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.dialog.closeAll();
          this.SuccessMensaje(response.message);
          this.registrarPermisoForestalPlanManejo();
          this.listResumenEjecutivo();
        } else this.ErrorMensaje(response.message);
      });
  }

  registrarPermisoForestalPlanManejo() {
    let listPlanManejo: any[] = [];
    var params = {
      idPermisoForestal: this.idPermisoForestalTemp,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    listPlanManejo.push(params);

    this.permisoForestalService
      .registrarPermisoForestalPlanManejo(listPlanManejo)
      .subscribe();
  }

  editarResumenEjecutivo() {
    if(!this.validarInformacion()) return;
    this.resumenEjecutivoObj.fechaElaboracionPmfi = new Date();
    let fechaInicio!: Date;
    let fechaFin!: Date;
    if (!isNullOrEmpty(this.resumenEjecutivoObj.fechaInicioDema)) {
      fechaInicio = new Date(this.resumenEjecutivoObj.fechaInicioDema);
    }
    if (!isNullOrEmpty(this.resumenEjecutivoObj.fechaFinDema)) {
      fechaFin = new Date(this.resumenEjecutivoObj.fechaFinDema);
    }

    var params = new InformacionGeneralPGMFA(this.resumenEjecutivoObj);
    params.fechaInicioDema = fechaInicio;
    params.fechaFinDema = fechaFin;
    params.idPlanManejo = this.idPlanManejo;
    params.idUsuarioRegistro = this.usuarioService.idUsuario;
    params.codigoProceso = 'PGMFA';

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.dialog.closeAll();
          this.SuccessMensaje(response.message);
          this.listResumenEjecutivo();
        } else this.ErrorMensaje(response.message);
      });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  handleAdmDateChange() {
    // this.validarFechas();
    let fechaInicial = moment(this.resumenEjecutivoObj.fechaInicioDema);
    let fechaFinal = moment(this.resumenEjecutivoObj.fechaFinDema);
    if (fechaFinal.diff(fechaInicial, 'days') >= 0) {
      this.resumenEjecutivoObj.vigencia = fechaFinal.diff(
        fechaInicial,
        'years'
      );
      this.disabled = false;
    } else {
      this.toast.error('Fecha Fin debe ser posterior o igual a Fecha Inicio');
      this.disabled = true;
    }
  }


  validarFechas(){
    let fechaActual = moment();
    let fechaInicio = moment(this.resumenEjecutivoObj.fechaInicioDema);

    this.disabled = false;
    if (
      !!this.resumenEjecutivoObj.fechaInicioDema &&
      !!this.resumenEjecutivoObj.fechaFinDema
    ) {
      if (fechaInicio.diff(fechaActual, 'days') < 0) {

        this.toast.error(
          'La Fecha Inicio debe ser posterior a la Fecha Actual'
        );
        this.disabled = true;
      }

      if (
        this.resumenEjecutivoObj.fechaFinDema <=
        this.resumenEjecutivoObj.fechaInicioDema
      ) {
        this.disabled = true;

        this.toast.error('La Fecha Final debe ser posterior a la Fecha Inicio');
        return;
      }
    } else if (fechaInicio.diff(fechaActual, 'days') < 0) {

      this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
      this.disabled = true;
      return;
    }
  }

  validarInformacion() {
    let validar: boolean = true;
    let mensaje: string = '';

    let fechaActual = moment();
    let fechaInicio = moment(this.resumenEjecutivoObj.fechaInicioDema);

    this.disabled = false;
    if (
      !!this.resumenEjecutivoObj.fechaInicioDema &&
      !!this.resumenEjecutivoObj.fechaFinDema
    ) {
      if (fechaInicio.diff(fechaActual, 'days') < 0) {
        validar = false;
        mensaje = mensaje +=
          '(*) La Fecha Inicio debe ser posterior a la Fecha Actual.\n';;
        this.disabled = true;
      }

      if (
        this.resumenEjecutivoObj.fechaFinDema <=
        this.resumenEjecutivoObj.fechaInicioDema
      ) {
        this.disabled = true;
        validar = false;
        mensaje = mensaje +=
          '(*) La Fecha Final debe ser posterior a la Fecha Inicio.\n';
      }
    } else if (fechaInicio.diff(fechaActual, 'days') < 0) {
      validar = false;
      mensaje = mensaje +=
        '(*) La Fecha Inicio debe ser posterior a la Fecha Actual.\n';
      this.disabled = true;
    }

    if (this.nombreComunidad == '' || this.nombreComunidad == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Nombre de la comunidad.\n';
    }
    if (this.representanteLegal == '' || this.representanteLegal == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Nombre del Jefe de la Comunidad o del Representante Legal.\n';
    }
    if (this.resumenEjecutivoObj.federacionComunidad == '' || this.resumenEjecutivoObj.federacionComunidad == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Federación u Organización a la que pertenece la Comunidad.\n';
    }
    if (this.resumenEjecutivoObj.descripcion == '' || this.resumenEjecutivoObj.descripcion == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Nº de la Credencial.\n';
    }
    if (this.resumenEjecutivoObj.nroRucComunidad == '' || this.resumenEjecutivoObj.nroRucComunidad == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Nº de RUC.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  getFormattedDate(date: any) {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');

    return day + '/' + month + '/' + year;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === 'PDF') {
          include =
            TabResumenEjecutivoComponent.EXTENSIONSAUTHORIZATION2.includes(
              file.type
            );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El tipo de Documento no es Válido.',
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El archivo no debe tener más de  3MB.',
          });
        } else {
          if (type === 'PDF') {
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.fileInfGenreal);
          }
        }
      }
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionResuEjeDelPlan = Object.assign(
                this.evaluacionResuEjeDelPlan,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost ==
                    this.codigoAcordeonResuEjeDelPlan
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacionResuEjeDelPlan])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionResuEjeDelPlan
        );
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
