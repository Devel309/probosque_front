import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { MatDialog } from "@angular/material/dialog";
import { ToastService } from "@shared";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { InformacionGeneralPGMFA } from "src/app/model/ResumenEjecutivoPGMFA";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { LineamientoInnerModel } from "../../../../../model/Comun/LineamientoInnerModel";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { finalize } from "rxjs/operators";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import { Router } from "@angular/router";
@Component({
  selector: "app-tab-aspectos-complementarios",
  templateUrl: "./tab-aspectos-complementarios.component.html",
  styleUrls: ["./tab-aspectos-complementarios.component.scss"],
  providers: [MessageService],
})
export class TabAspectosComplementariosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;

  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_15;

  codigoAcordeon15_1: string = CodigosTabEvaluacion.PGMFA_TAB_15_1;
  evaluacion15_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon15_1,
  });
  evaluacion: any;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input("isDisabled") isDisabled: boolean = false;

  informacionGeneral: InformacionGeneralPGMFA = new InformacionGeneralPGMFA();

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.obtenerResumenEjecutivo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  obtenerResumenEjecutivo() {
    var params = {
      codigoProceso: "PGMFA",
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.isDisabled = true;
          response.data.forEach((element: any) => {
            this.informacionGeneral = new InformacionGeneralPGMFA(element);
          });
        } else {
          this.isDisabled = false;
        }
      });
  }

  ActualizarAspectosComplementarios() {
    var params = this.informacionGeneral;

    
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe(
        (result: any) => {
          this.obtenerResumenEjecutivo();
          this.dialog.closeAll();
          this.messageService.add({
            severity: "success",
            summary: "",
            detail: result.message,
          });
        },
        (error: any) => {
          this.dialog.closeAll();
          this.messageService.add({
            severity: "warn",
            summary: "",
            detail: error.message,
          });
        }
      );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion15_1 = Object.assign(
                this.evaluacion15_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon15_1
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion15_1])){

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion15_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
