import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { ModalFormularioCronogramaComponent } from "./modal/modal-formulario-zonas/modal-formulario-cronograma.component";
import { LineamientoInnerModel } from "../../../../../model/Comun/LineamientoInnerModel";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { ToastService } from "@shared";
import { MatDialog } from "@angular/material/dialog";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { FileModel } from "src/app/model/util/File";
import { HttpErrorResponse } from "@angular/common/http";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-cronograma",
  templateUrl: "./tab-cronograma.component.html",
  styleUrls: ["./tab-cronograma.component.scss"],
})
export class TabCronogramaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_14;

  codigoAcordeon14_1: string = CodigosTabEvaluacion.PGMFA_TAB_14_1;
  evaluacion14_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon14_1,
  });
  evaluacion: any;

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  ref!: DynamicDialogRef;
  context: Modelo = new Modelo();
  listaAnios: any[] = [];

  UrlFormatos = UrlFormatos;

  verAnio1: boolean = false;
  verAnio2: boolean = false;
  verAnio3: boolean = false;
  verAnio4: boolean = false;
  verAnio5: boolean = false;
  verAnio6: boolean = false;
  verAnio7: boolean = false;
  verAnio8: boolean = false;
  verAnio9: boolean = false;
  verAnio10: boolean = false;
  verAnio11: boolean = false;
  verAnio12: boolean = false;
  verAnio13: boolean = false;
  verAnio14: boolean = false;
  verAnio15: boolean = false;
  verAnio16: boolean = false;
  verAnio17: boolean = false;
  verAnio18: boolean = false;
  verAnio19: boolean = false;
  verAnio20: boolean = false;

  lstDetalle: Modelo[] = [];
  lstOtrosAnos: any[] = [];
  params: any;
  vigencia: Number = 0;

  cmbAnios: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [{ label: "Año 1", value: "1-0" }],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [{ label: "Año 2", value: "2-0" }],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [{ label: "Año 3", value: "3-0" }],
    },
  ];

  cmbAniosDefault: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [{ label: "Año 1", value: "1-0" }],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [{ label: "Año 2", value: "2-0" }],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [{ label: "Año 3", value: "3-0" }],
    },
  ];

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private messageService: MessageService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listarCronogramaActividad();
    this.lstOtrosAnos.length = 4;

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarCronogramaActividad() {
    this.lstOtrosAnos.length = 4;
    this.listaAnios = [];
    this.lstDetalle = [];
    this.lstDetalle = [];
    this.lstOtrosAnos = [];
    this.vigencia = 0;
    this.params = {};
    this.context = new Modelo();
    this.lstDetalle = [];
    this.lstDetalle = [];
    this.verAnio4 = false;
    this.verAnio5 = false;
    this.verAnio6 = false;
    this.verAnio7 = false;
    this.verAnio8 = false;
    this.verAnio9 = false;
    this.verAnio10 = false;
    this.verAnio11 = false;
    this.verAnio12 = false;
    this.verAnio13 = false;
    this.verAnio14 = false;
    this.verAnio15 = false;
    this.verAnio16 = false;
    this.verAnio17 = false;
    this.verAnio18 = false;
    this.verAnio19 = false;
    this.verAnio20 = false;

    var params = {
      codigoProceso: "PGMFA",
      idCronogramaActividad: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((x: any) => {
            this.obtenerAnios(x);
            this.lstDetalle.push(x);
          });
          this.marcar();
        }
      });
  }

  obtenerAnios(lista: any) {
    lista.detalle.forEach((element: any) => {
      this.listaAnios.push(element.anio);
    });
    this.filtrar();
  }

  filtrar() {
    this.cmbAnios = [];
    this.cmbAnios = [...this.cmbAniosDefault];
    var largest = Math.max.apply(Math, this.listaAnios);
    if (largest >= 4) {
      this.vigencia = largest;
      this.lstOtrosAnos.length = largest + 1;
      

      for (let i = 4; i <= largest; i++) {
        this.params = {
          label: "AÑO " + i,
          value: i.toString(),
          items: [{ label: "Año " + i, value: i + "-0" }],
        };
        this.cmbAnios.push(this.params);
      }
      
    }
  }

  agregarColumna() {
    this.lstOtrosAnos.push(this.lstOtrosAnos.length);
    this.lstOtrosAnos.forEach((x: any, i: number) => {
      if (i > 3 && i <= 20) {
        this.params = {
          label: "AÑO " + i,
          value: i.toString(),
          items: [{ label: "Año " + i, value: i + "-0" }],
        };
      }
    });
    if (this.lstOtrosAnos.length <= 21) {
      this.cmbAnios.push(this.params);
      this.vigencia = this.params.value;
      this.marcarAgregado();
    }
  }

  marcar() {
    var aniosMeses: any[] = [];
    this.lstDetalle.forEach((x: any, index) => {
      aniosMeses = [];
      if (this.vigencia == 4) {
        this.verAnio4 = true;
      } else if (this.vigencia == 5) {
        this.verAnio4 = true;
        this.verAnio5 = true;
      } else if (this.vigencia == 6) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
      } else if (this.vigencia == 7) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
      } else if (this.vigencia == 8) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
      } else if (this.vigencia == 9) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
      } else if (this.vigencia == 10) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
      } else if (this.vigencia == 11) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
      } else if (this.vigencia == 12) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
      } else if (this.vigencia == 13) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
      } else if (this.vigencia == 14) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
      } else if (this.vigencia == 15) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
      } else if (this.vigencia == 16) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
      } else if (this.vigencia == 17) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
      } else if (this.vigencia == 18) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
      } else if (this.vigencia == 19) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
      } else if (this.vigencia == 20) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
        this.verAnio20 = true;
      }

      if (x.detalle) {
        x.detalle.forEach((element: any) => {
          aniosMeses.push({
            aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
            id: element.idCronogramaActividadDetalle,
          });
        });
      }

      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };

      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstDetalle[index] = this.context;
    });
  }

  marcarAgregado() {
    this.lstDetalle.forEach((x: any, index) => {
      if (this.vigencia == 4) {
        this.verAnio4 = true;
      } else if (this.vigencia == 5) {
        this.verAnio4 = true;
        this.verAnio5 = true;
      } else if (this.vigencia == 6) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
      } else if (this.vigencia == 7) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
      } else if (this.vigencia == 8) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
      } else if (this.vigencia == 9) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
      } else if (this.vigencia == 10) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
      } else if (this.vigencia == 11) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
      } else if (this.vigencia == 12) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
      } else if (this.vigencia == 13) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
      } else if (this.vigencia == 14) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
      } else if (this.vigencia == 15) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
      } else if (this.vigencia == 16) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
      } else if (this.vigencia == 17) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
      } else if (this.vigencia == 18) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
      } else if (this.vigencia == 19) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
      } else if (this.vigencia == 20) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
        this.verAnio20 = true;
      }
    });
  }

  guardarTipoActividadPrincipal(resp: any) {
    let params = {
      actividad: resp,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "PGMFA",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        console.log(response);
      });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "PGMFA",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó el cronograma de actividades correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó la actividad del cronograma correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    this.ref = this.dialogService.open(ModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      this.lstDetalle.push(this.context);
    }
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail:
                    "Se eliminó la actividad del cronograma correctamente.",
                });
                this.listarCronogramaActividad();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  detail: "Ocurrió un problema, intente nuevamente.",
                });
              }
            });
        } else {
          this.lstDetalle.splice(
            this.lstDetalle.findIndex((x) => x.id == data.id),
            1
          );
        }
      },
      reject: () => {},
    });
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion14_1 = Object.assign(
                this.evaluacion14_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon14_1
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion14_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion14_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  cargarFormato(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Hoja1",
          numeroFila: 2,
          numeroColumna: 1,
          codigoProceso: "PGMFA",
          codigoActividad: "AAEAM",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cronogrmaActividadesService
          .registrarCronogramaActividadExcel(
            t.file,
            item.nombreHoja,
            item.numeroFila,
            item.numeroColumna,
            item.codigoProceso,
            item.codigoActividad,
            item.idPlanManejo,
            item.idUsuarioRegistro
          )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe(
            (res: any) => {
              if (res.success == true) {
                this.toast.ok(res?.message);
                this.listarCronogramaActividad();
              } else {
                this.toast.error(res?.message);
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            }
          );
      });
    } else {
      this.toast.warn("Debe seleccionar archivo.");
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.tipoActividad = data.tipoActividad);
      return;
    }
  }

  showRemove: boolean = true;
  tipoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m0: boolean = false;

  a2m0: boolean = false;

  a3m0: boolean = false;

  a4m0: boolean = false;
  a5m0: boolean = false;
  a6m0: boolean = false;
  a7m0: boolean = false;
  a8m0: boolean = false;
  a9m0: boolean = false;
  a10m0: boolean = false;
  a11m0: boolean = false;
  a12m0: boolean = false;
  a13m0: boolean = false;
  a14m0: boolean = false;
  a15m0: boolean = false;
  a16m0: boolean = false;
  a17m0: boolean = false;
  a18m0: boolean = false;
  a19m0: boolean = false;
  a20m0: boolean = false;

  otros: any[] = [];

  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-0") {
        this.a1m0 = x.aniosMeses.indexOf("1-0") > -1;
      } else if (x.aniosMeses == "2-0") {
        this.a2m0 = x.aniosMeses.indexOf("2-0") > -1;
      } else if (x.aniosMeses == "3-0") {
        this.a3m0 = x.aniosMeses.indexOf("3-0") > -1;
      } else if (x.aniosMeses == "4-0") {
        this.a4m0 = x.aniosMeses.indexOf("4-0") > -1;
      } else if (x.aniosMeses == "5-0") {
        this.a5m0 = x.aniosMeses.indexOf("5-0") > -1;
      } else if (x.aniosMeses == "6-0") {
        this.a6m0 = x.aniosMeses.indexOf("6-0") > -1;
      } else if (x.aniosMeses == "7-0") {
        this.a7m0 = x.aniosMeses.indexOf("7-0") > -1;
      } else if (x.aniosMeses == "8-0") {
        this.a8m0 = x.aniosMeses.indexOf("8-0") > -1;
      } else if (x.aniosMeses == "9-0") {
        this.a9m0 = x.aniosMeses.indexOf("9-0") > -1;
      } else if (x.aniosMeses == "10-0") {
        this.a10m0 = x.aniosMeses.indexOf("10-0") > -1;
      } else if (x.aniosMeses == "11-0") {
        this.a11m0 = x.aniosMeses.indexOf("11-0") > -1;
      } else if (x.aniosMeses == "12-0") {
        this.a12m0 = x.aniosMeses.indexOf("12-0") > -1;
      } else if (x.aniosMeses == "13-0") {
        this.a13m0 = x.aniosMeses.indexOf("13-0") > -1;
      } else if (x.aniosMeses == "14-0") {
        this.a14m0 = x.aniosMeses.indexOf("14-0") > -1;
      } else if (x.aniosMeses == "15-0") {
        this.a15m0 = x.aniosMeses.indexOf("15-0") > -1;
      } else if (x.aniosMeses == "16-0") {
        this.a16m0 = x.aniosMeses.indexOf("16-0") > -1;
      } else if (x.aniosMeses == "17-0") {
        this.a17m0 = x.aniosMeses.indexOf("17-0") > -1;
      } else if (x.aniosMeses == "18-0") {
        this.a18m0 = x.aniosMeses.indexOf("18-0") > -1;
      } else if (x.aniosMeses == "19-0") {
        this.a19m0 = x.aniosMeses.indexOf("19-0") > -1;
      } else if (x.aniosMeses == "20-0") {
        this.a20m0 = x.aniosMeses.indexOf("20-0") > -1;
      }
    });
  }
}
