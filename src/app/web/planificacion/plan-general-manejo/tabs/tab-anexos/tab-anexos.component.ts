import {
  Component,
  EventEmitter,
  OnInit,
  ElementRef,
  ViewChild,
  Output,
  Input,
} from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, PGMFArchivoTipo, MapApi, ToastService } from "@shared";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { LineamientoInnerModel } from "../../../../../model/Comun/LineamientoInnerModel";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { FileModel } from "src/app/model/util/File";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { HttpErrorResponse } from "@angular/common/http";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { AnexosCargaExcelService } from "src/app/service/anexos";
import { Router } from "@angular/router";
@Component({
  selector: "app-tab-anexos",
  templateUrl: "./tab-anexos.component.html",
  styleUrls: ["./tab-anexos.component.scss"],
  providers: [MessageService],
})
export class TabAnexosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;

  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_16;

  codigoAcordeon16_1: string = CodigosTabEvaluacion.PGMFA_TAB_16_1;
  codigoAcordeon16_2: string = CodigosTabEvaluacion.PGMFA_TAB_16_2;
  codigoAcordeon16_3: string = CodigosTabEvaluacion.PGMFA_TAB_16_3;

  evaluacionAnexo1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon16_1,
  });
  evaluacionAnexo2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon16_2,
  });
  evaluacionAnexo3: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon16_3,
  });

  variables: any[] = [{ var: "Arb/ha" }, { var: "AB/HA" }, { var: "Vol/ha" }];
  variablesAnexo3: any[] = [{ var: "Arb/ha" }, { var: "AB/ha" }];

  evaluacion: any;

  @ViewChild("filAddFileMap1", { static: true })
  private filAddFileMap1!: ElementRef;
  @ViewChild("filAddFileMap2", { static: true })
  private filAddFileMap2!: ElementRef;
  @ViewChild("ulMap1", { static: true }) private ulMap1!: ElementRef;
  @ViewChild("ulMap2", { static: true }) private ulMap2!: ElementRef;
  _filAddFileMap1: any = null;
  _filAddFileMap2: any = null;
  _filesMap1: any = [];
  _filesMap2: any = [];
  file: FileModel = {} as FileModel;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();
  listAnexo2Especies: any[] = [];
  listAnexo3Especies: any[] = [];
  comboBosque: any[] = [];
  codigoBosqueAnexo2: number = 0;
  codigoBosqueAnexo3: number = 0;
  UrlFormatos = UrlFormatos


  CodigoProceso = CodigoProceso
  verEnviar: boolean = false;
  listAnexo4: any[] = []
  pendiente: boolean = true;

  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private apiArchivo: ArchivoService,
    private user: UsuarioService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private toast: ToastService,
    private mapApi: MapApi,
    private anexosService: AnexosService,
    private anexosCargaExcelService: AnexosCargaExcelService, 
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.obtenerArchivos();
    //  this.domConfig();
    this.listarTipoBosque();
    if (this.isPerfilArffs) this.obtenerEvaluacion();
    //this.listAnexos();
  }

  listarTipoBosque() {
    this.comboBosque = [];
    this.anexosService.listarTipoBosque().subscribe((response: any) => {
      this.comboBosque = [...response.data];
    });
  }
  codigoBosque(event: any) {
    this.listAnexos(this.codigoBosqueAnexo2);
  }

  listAnexos(codigoBosqueAnexo2: number) {
    this.listAnexo2Especies = [];
    var params = {
      /* idPlanDeManejo: 1012,
      idTipoBosque: 8, */
      idPlanDeManejo: this.idPlanManejo,
      idTipoBosque: codigoBosqueAnexo2,
      tipoPlan: "PGMFA",
    };
    this.dialog.open(LoadingComponent, { disableClose: true }); 
    this.anexosService
      .Anexo2PGMFA(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            var obj = new Anexo2();
            obj.nombreComun = element.nombreComun;
            this.variables.forEach((item: any, i: number) => {
              var ob1 = new ARB();
              var ob2 = new ARB();
              var ob3 = new ARB();
              if (item.var == "Arb/ha") {
                ob1.var = "Arb/ha";
                ob1.Dap30a39 = element.arbHaDap30a39;
                ob1.Dap40a49 = element.arbHaDap40a49;
                ob1.Dap50a59 = element.arbHaDap50a59;
                ob1.Dap60a69 = element.arbHaDap60a69;
                ob1.Dap70a79 = element.arbHaDap70a79;
                ob1.Dap80a89 = element.arbHaDap80a89;
                ob1.Dap90aMas = element.arbHaDap90aMas;
                ob1.TotalTipoBosque = element.arbHaTotalTipoBosque;
                ob1.TotalPorArea = element.arbHaTotalPorArea;
                obj.array.push(ob1);
              } else if (item.var == "AB/HA") {
                ob2.var = "AB/HA";
                ob2.Dap30a39 = element.abHaDap30a39;
                ob2.Dap40a49 = element.abHaDap40a49;
                ob2.Dap50a59 = element.abHaDap50a59;
                ob2.Dap60a69 = element.abHaDap60a69;
                ob2.Dap70a79 = element.abHaDap70a79;
                ob2.Dap80a89 = element.abHaDap80a89;
                ob2.Dap90aMas = element.abHaDap90aMas;
                ob2.TotalTipoBosque = element.abHaTotalTipoBosque;
                ob2.TotalPorArea = element.abHaTotalPorArea;
                obj.array.push(ob2);
              } else if (item.var == "Vol/ha") {
                ob3.var = "Vol/ha";
                ob3.Dap30a39 = element.volHaDap30a39;
                ob3.Dap40a49 = element.volHaDap40a49;
                ob3.Dap50a59 = element.volHaDap50a59;
                ob3.Dap60a69 = element.volHaDap60a69;
                ob3.Dap70a79 = element.volHaDap70a79;
                ob3.Dap80a89 = element.volHaDap80a89;
                ob3.Dap90aMas = element.volHaDap90aMas;
                ob3.TotalTipoBosque = element.volHaTotalTipoBosque;
                ob3.TotalPorArea = element.volHaTotalPorArea;
                obj.array.push(ob3);
              }
            });
            this.listAnexo2Especies.push(obj);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  cargarFormato(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          tipoPlan: "PGMFA",
          idPlanManejo: this.idPlanManejo,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosCargaExcelService
          .FormatoPGMFFormuladoAnexo2Excel(t.file, item.tipoPlan,  item.idPlanManejo, )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }

  codigoBosque3(event: any) {
    this.listAnexos3(this.codigoBosqueAnexo3);
  }

  listAnexos3(codigoBosqueAnexo3: number) {
    this.listAnexo3Especies = [];
    var params = {
      // idPlanDeManejo: 1012,
      //idTipoBosque: 10,
      idPlanDeManejo: this.idPlanManejo,
      idTipoBosque: codigoBosqueAnexo3,
      tipoPlan: "PGMFA",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo3PGMFA(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            var obj = new Anexo3();
            obj.nombreComun = element.nombreComun;
            this.variablesAnexo3.forEach((item: any, i: number) => {
              var ob1 = new anexo3Detalle();
              var ob2 = new anexo3Detalle();
              if (item.var == "Arb/ha") {
                ob1.var = "Arb/ha";
                ob1.Dap10a19 = element.arbHaDap10a19;
                ob1.Dap20a29 = element.arbHaDap20a29;
                ob1.TotalTipoBosque = element.arbHaTotalTipoBosque;
                ob1.TotalPorArea = element.arbHaTotalPorArea;
                obj.array.push(ob1);
              } else if (item.var == "AB/ha") {
                ob2.var = "AB/ha";
                ob2.Dap10a19 = element.abHaDap10a19;
                ob2.Dap20a29 = element.abHaDap20a29;
                ob2.TotalTipoBosque = element.arbHaTotalTipoBosque;
                ob2.TotalPorArea = element.abHaTotalTipoBosque;
                obj.array.push(ob2);
              }
            });
            this.listAnexo3Especies.push(obj);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  cargarFormatoAnexo3(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          tipoPlan: "PGMFA",
          idPlanManejo: this.idPlanManejo,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosCargaExcelService
          .FormatoPGMFFormuladoAnexo3Excel(t.file, item.tipoPlan,  item.idPlanManejo, )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }

  cargarFormatoAnexo4(files: any){
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          tipoPlan: "PGMFA",
          //idPlanDeManejo: 1012,  //pruebas
          idPlanDeManejo: this.idPlanManejo,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosCargaExcelService
          .cargaTipoPlanMasivoExcel(t.file, item.tipoPlan,  item.idPlanDeManejo, )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listAnexo4 = [...res.data]
              this.pendiente = false;
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }

  static get Guid2() {
    return class Guid2 {
      constructor() {}
      static get newGuid() {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
          /[xy]/g,
          function (c) {
            var r = (Math.random() * 16) | 0,
              v = c == "x" ? r : (r & 0x3) | 0x8;
            return v.toString(16);
          }
        );
      }
    };
  }
  static get EXTENSIONSAUTHORIZATION() {
    return [
      ".pdf",
      ".docx",
      ".doc",
      "image/png",
      "image/jpeg",
      "image/jpeg",
      "application/pdf",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      "application/msword",
      "zip",
      "application/x-zip-compressed",
    ];
  }
  domConfig() {
    this._filAddFileMap1 = this.filAddFileMap1.nativeElement;
    this._filAddFileMap1.accept = TabAnexosComponent.EXTENSIONSAUTHORIZATION.join(
      ","
    );
    this._filAddFileMap2 = this.filAddFileMap2.nativeElement;
    this._filAddFileMap2.accept = TabAnexosComponent.EXTENSIONSAUTHORIZATION.join(
      ","
    );
    this._filAddFileMap1.addEventListener("change", (e: any) => {
      this.onChangeAddFileMap1(e);
    });
    this._filAddFileMap2.addEventListener("change", (e: any) => {
      this.onChangeAddFileMap2(e);
    });
  }
  onChangeAddFileMap1(e: any) {
    e.target.parentElement.querySelector(":scope > label").textContent =
      "Seleccione el archivo";
    if (e.target.files.length === 0) return;
    let file: any = null;
    for (var i = 0; i < this._filAddFileMap1.files.length; i++) {
      file = this._filAddFileMap1.files[i];
      let include = TabAnexosComponent.EXTENSIONSAUTHORIZATION.includes(
        file.type
      );
      if (include !== true) {
        e.target.value = "";
        e.target.parentElement.querySelector(":scope > label").innerHTML =
          '<strong class="alert alert-warning p-0">el tipo de documento no es v&aacute;lido</strorng>';
      } else if (file.size > 3000000) {
        e.target.parentElement.querySelector(":scope > label").innerHTML =
          '<strong class="alert alert-warning p-0">El archivo no debe tener más de  3MB</strorng>';
      } else {
        this._filesMap1.push({
          id: 0,
          name: file.name,
          codigo: "Map1",
          guid: TabAnexosComponent.Guid2.newGuid,
          inServer: false,
          url: window.URL.createObjectURL(file),
          type: file.type,
          file: file,
          originalName: file.name,
        });
      }
    }
    this.writeDocumentsMap1(this._filesMap1);
  }
  onChangeAddFileMap2(e: any) {
    e.target.parentElement.querySelector(":scope > label").textContent =
      "Seleccione el archivo";
    if (e.target.files.length === 0) return;
    let file: any = null;
    for (var i = 0; i < this._filAddFileMap2.files.length; i++) {
      file = this._filAddFileMap2.files[i];
      let include = TabAnexosComponent.EXTENSIONSAUTHORIZATION.includes(
        file.type
      );
      if (include !== true) {
        e.target.value = "";
        e.target.parentElement.querySelector(":scope > label").innerHTML =
          '<strong class="alert alert-warning p-0">el tipo de documento no es v&aacute;lido</strorng>';
      } else if (file.size > 3000000) {
        e.target.parentElement.querySelector(":scope > label").innerHTML =
          '<strong class="alert alert-warning p-0">El archivo no debe tener más de  3MB</strorng>';
      } else {
        this._filesMap2.push({
          id: 0,
          name: file.name,
          codigo: "Map2",
          guid: TabAnexosComponent.Guid2.newGuid,
          inServer: false,
          url: window.URL.createObjectURL(file),
          type: file.type,
          file: file,
          originalName: file.name,
        });
      }
    }
    this.writeDocumentsMap2(this._filesMap2);
  }
  writeDocumentsMap1(files: any) {
    let files2 = files.filter((t: any) => t.deleted !== true);
    let ul = this.ulMap1.nativeElement;
    ul.empty();
    files2.forEach((t: any) => {
      let li = ul.appendHTML(
        `<li id="" class="list-group-item ${
          t.inServer === true ? "gh-color-loaded" : ""
        }"></li>`
      );
      let btnQuit = li.appendHTML(
        `<button class="btn badge ml-auto float-right" data-in-server=${t.inServer} id=${t.id} data-guid="${t.guid}"></button>`
      );
      if (t.inServer === true) {
        let btnDownload = li.appendHTML(
          `<button class="btn badge float-right mr-1"></button>`
        );
        btnDownload.appendHTML(
          `<i class="pi pi-download" data-in-server= ${t.inServer}  role="button"></i>`
        );
        btnDownload.addEventListener(
          "click",
          this.onClickDocumentDownload.bind(this)
        );
      }
      if (!this.isPerfilArffs) {
        btnQuit.appendHTML(`<i class="pi pi-times-circle" role="button"></i>`);
        btnQuit.addEventListener(
          "click",
          this.onClickDocumentDeleteMap1.bind(this)
        );
      }

      li.appendHTML(
        `<span title="${t.name}" data-url= "${t.url}" data-mime-type ="${t.type}" data-guid="${t.guid}">${t.name}</span>`
      );
    });
  }
  writeDocumentsMap2(files: any) {
    let files2 = files.filter((t: any) => t.deleted !== true);
    let ul = this.ulMap2.nativeElement;
    ul.empty();
    files2.forEach((t: any) => {
      let li = ul.appendHTML(
        `<li id="" class="list-group-item ${
          t.inServer === true ? "gh-color-loaded" : ""
        }"></li>`
      );
      let btnQuit = li.appendHTML(
        `<button class="btn badge ml-auto float-right" data-in-server="${t.inServer}" id="${t.id}" data-guid="${t.guid}"></button>`
      );
      if (t.inServer === true) {
        let btnDownload = li.appendHTML(
          `<button class="btn badge float-right mr-1"></button>`
        );
        btnDownload.appendHTML(
          `<i class="pi pi-download" data-in-server="${t.inServer}"  role="button"></i>`
        );
        btnDownload.addEventListener(
          "click",
          this.onClickDocumentDownload.bind(this)
        );
      }
      if (!this.isPerfilArffs) {
        btnQuit.appendHTML(`<i class="pi pi-times-circle" role="button"></i>`);
        btnQuit.addEventListener(
          "click",
          this.onClickDocumentDeleteMap2.bind(this)
        );
      }
      li.appendHTML(
        `<span title="${t.name}" data-url= "${t.url}" data-mime-type ="${t.type}" data-guid="${t.guid}">${t.name}</span>`
      );
    });
  }
  onClickDocumentDeleteMap1(e: any) {
    e.preventDefault();
    let guid = e.target.parentNode.dataset.guid;
    let inServer = e.target.parentNode.dataset.inServer;
    let id = e.target.parentNode.id;
    let index = this._filesMap1.findIndex((t: any) => t.guid === guid);
    if (inServer === "true") {
      this.confirmationService.confirm({
        target: e.target || undefined,
        message: "¿Esta seguro de eliminar el archivo?",
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Si",
        rejectLabel: "No",
        key: "deleteFile",
        accept: () => {
          this._filesMap1.splice(index, 1);
          this.writeDocumentsMap1(this._filesMap1);
          this.eliminarArchivoDetalle(id);
        },
      });
    } else {
      this._filesMap1.splice(index, 1);
      this.writeDocumentsMap1(this._filesMap1);
    }
  }
  onClickDocumentDeleteMap2(e: any) {
    e.preventDefault();
    let guid = e.target.parentNode.dataset.guid;
    let inServer = e.target.parentNode.dataset.inServer;
    let id = e.target.parentNode.id;
    let index = this._filesMap2.findIndex((t: any) => t.guid === guid);
    if (inServer === "true") {
      this.confirmationService.confirm({
        target: e.target || undefined,
        message: "¿Esta seguro de eliminar el archivo?",
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Si",
        rejectLabel: "No",
        key: "deleteFile",
        accept: () => {
          this._filesMap2.splice(index, 1);
          this.writeDocumentsMap2(this._filesMap2);
          this.eliminarArchivoDetalle(id);
        },
      });
    } else {
      this._filesMap2.splice(index, 1);
      this.writeDocumentsMap2(this._filesMap2);
    }
  }
  onClickDocumentDownload(e: any) {
    let url = e.currentTarget.parentNode.children[2].dataset.url;
    let title = e.currentTarget.parentNode.children[2].title;
    let type = e.currentTarget.parentNode.children[2].dataset.mimeType;
    DownloadFile(url, title, type);
  }
  obtenerArchivos() {
    this._filesMap1 = [];
    this._filesMap2 = [];
    let item: any = {
      idPlanManejo: 171,
      codigoTipoPGMF: "PGMFANEXO",
    };
    this.servicePostulacionPFDM
      .obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF)
      .subscribe((result: any) => {
        result.data.forEach((t: any) => {
          if (t.codigoSubTipoPGMF === "Map1") {
            this._filesMap1.push({
              id: t.idArchivo,
              name: t.nombre,
              codigo: "Map1",
              guid: TabAnexosComponent.Guid2.newGuid,
              inServer: true,
              url: t.documento,
              type: t.documento.type,
              file: t.documento,
              originalName: t.nombre,
            });
          } else if (t.codigoSubTipoPGMF === "Map2") {
            this._filesMap2.push({
              id: t.idArchivo,
              name: t.nombre,
              codigo: "Map2",
              guid: TabAnexosComponent.Guid2.newGuid,
              inServer: true,
              url: t.documento,
              type: t.documento.type,
              file: t.documento,
              originalName: t.nombre,
            });
          }
          this.writeDocumentsMap1(this._filesMap1);
          this.writeDocumentsMap2(this._filesMap2);
        });
      });
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    this.servicePostulacionPFDM
      .eliminarArchivoDetalle(idArchivo, this.user.idUsuario)
      .subscribe();
  }
  guardar() {
    let item = {
      idUsuarioRegistro: this.user.idUsuario,
      idPlanManejo: 171,
    };
    let files = this._filesMap1.concat(this._filesMap2);
    files.forEach((t: any) => {
      if (t.inServer === false) {
        this.apiArchivo
          .cargar(item.idUsuarioRegistro, "", t.file)
          .subscribe((result: any) => {
            let item2 = {
              codigoTipoPGMF: "PGMFANEXO",
              descripcion: t.type,
              codigoSubTipoPGMF: t.codigo,
              idArchivo: result.data,
              idPlanManejo: item.idPlanManejo,
              idUsuarioRegistro: item.idUsuarioRegistro,
            };
            this.servicePostulacionPFDM
              .registrarArchivoDetalle(item2)
              .subscribe();
          });
      }
    });
    setTimeout(() => {
      this.messageService.add({
        severity: "success",
        summary: "",
        detail: "Se registró correctamente",
      });
      this.obtenerArchivos();
    }, 1000);
  }

  registrarArchivoId(event: any){
    if(event){
      this.verEnviar = true;
    }
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionAnexo1 = Object.assign(
                this.evaluacionAnexo1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon16_1
                )
              );
              this.evaluacionAnexo2 = Object.assign(
                this.evaluacionAnexo2,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon16_2
                )
              );
              this.evaluacionAnexo3 = Object.assign(
                this.evaluacionAnexo3,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon16_3
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacionAnexo1,
        this.evaluacionAnexo2,
        this.evaluacionAnexo3,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAnexo1);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAnexo2);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAnexo3);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export class Anexo2 {
  constructor(data?: any) {
    if (data) {
      this.nombreComun = data.nombreComun;
      return;
    }
  }
  nombreComun: string = "";
  array: ARB[] = [];
}

export class ARB {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = "";
  Dap30a39: string = "";
  Dap40a49: string = "";
  Dap50a59: string = "";
  Dap60a69: string = "";
  Dap70a79: string = "";
  Dap80a89: string = "";
  Dap90aMas: string = "";
  TotalTipoBosque: string = "";
  TotalPorArea: string = "";
}

export class Anexo3 {
  constructor(data?: any) {
    if (data) {
      this.nombreComun = data.nombreComun;
      return;
    }
  }
  nombreComun: string = "";
  array: anexo3Detalle[] = [];
}

export class anexo3Detalle {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = "";
  Dap10a19: string = "";
  Dap20a29: string = "";
  TotalTipoBosque: string = "";
  TotalPorArea: string = "";
}
