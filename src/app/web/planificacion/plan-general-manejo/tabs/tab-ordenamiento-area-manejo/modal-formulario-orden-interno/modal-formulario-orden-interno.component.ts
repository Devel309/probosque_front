import { Component, OnInit } from '@angular/core';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-orden-interno',
  templateUrl: './modal-formulario-orden-interno.component.html',
  styleUrls: ['./modal-formulario-orden-interno.component.scss'],
})
export class ModalFormularioOrdenInternoComponent implements OnInit {
  context: any = {};

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  categoriTmp: string = '';
  coberturaTmp: string = '';

  ngOnInit(): void {
    if (this.config.data.type == 'E') {
      this.context = { ...this.config.data.data };
      this.categoriTmp = this.config.data.data.categoria;
      this.coberturaTmp = this.config.data.data.zonaUTM;
    }
  }

  agregar = () => {
    if (this.context.zonaUTM == '' || this.context.zonaUTM == null) {
      this.toast.warn('Seleccione el tipo de Cobertura Boscosa.');
      return;
    }
    if (this.context.categoria == '') {
      this.toast.warn('El campo Denominación o espacios es obligatorio.');
      return;
    }

    this.ref.close(this.context);
  };

  cerrarModal() {
    if (this.config.data.type == 'E') {
      this.context.categoria = this.categoriTmp;
      this.context.zonaUTM = this.coberturaTmp;
    }

    this.ref.close();
  }
}
