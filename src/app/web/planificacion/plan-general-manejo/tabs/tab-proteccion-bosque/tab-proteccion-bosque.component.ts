import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProteccionBosqueService } from 'src/app/service/proteccionBosque.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { PlanManejoModel } from '../../../../../model/PlanManejo';
import {
  ListProteccionBosqueActividad2,
  ProteccionBosqueDemarcacionModel,
  ProteccionBosquePGMFAModel,
  ProteccionBosquePGMFAModel2,
} from '../../../../../model/ProteccionBosqueDemarcacion';
import { ParametroModel } from '../../../../../model/Parametro';
import { ProteccionBosqueGestionAmbientalModel } from '../../../../../model/ProteccionBosqueGestionAmbiental';
import { GenericoService } from 'src/app/service/generico.service';
import { UsuarioModel } from 'src/app/model/Usuario';
import { ProteccionBosqueImpactoAmbientalModel } from 'src/app/model/ProteccionBosqueImapctoAmbiental';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService, UsuarioService } from '@services';
import { ProteccionDelBosqueService } from 'src/app/service/planificacion/plan-general-manejo-pgmfa/proteccionBosque.service';
import { element } from 'protractor';
import { LineamientoInnerModel } from '../../../../../model/Comun/LineamientoInnerModel';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { finalize } from 'rxjs/operators';
import { ToastService } from '@shared';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { param } from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-proteccion-bosque',
  templateUrl: './tab-proteccion-bosque.component.html',
  styleUrls: ['./tab-proteccion-bosque.component.scss'],
  providers: [MessageService],
})
export class TabProteccionBosqueComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_8;

  codigoAcordeon81: string = CodigosTabEvaluacion.PGMFA_TAB_8_81;
  codigoAcordeon821: string = CodigosTabEvaluacion.PGMFA_TAB_8_821;
  codigoAcordeon822: string = CodigosTabEvaluacion.PGMFA_TAB_8_822;
  evaluaciond81: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon81,
  });

  evaluaciond821: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon821,
  });

  evaluaciond822: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon822,
  });

  evaluacion: any;
  editarPlanGe: boolean = false;
  programa: string = '';

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  planManejo = {} as PlanManejoModel;
  proteccionBosqueDemarcacion = {} as ProteccionBosqueDemarcacionModel;
  listProteccionBosqueDemarcacion: ProteccionBosqueDemarcacionModel[] = [];

  listProtecccionBosqueModel: ProteccionBosqueModel[] = [];
  protecccionBosqueModelV2: ProteccionBosqueCabeceraModel =
    new ProteccionBosqueCabeceraModel({
      idPlanManejo: this.idPlanManejo,
      idPlanGeneral: this.idPlanManejo, //aca va un codigo de plan "PGMF"
      codigoCabecera: 'DEMARC',
      descripcion: 'Demarcación y mantenimiento de linderos',
      listProteccionBosque: [],
    });

  evaluacionImpactoAmbientalProBosModel: ProteccionBosqueCabeceraModel =
    new ProteccionBosqueCabeceraModel({
      idPlanManejo: this.idPlanManejo,
      idPlanGeneral: this.idPlanManejo, //aca va un codigo de plan "PGMF"
      codigoCabecera: 'EVAL',
      descripcion: 'Evaluacion de impacto ambiental',
      listProteccionBosque: [],
    });

  //el modal para impacto ambiental
  detEvaluacionImpActual: ProteccionBosqueDetalleModel =
    new ProteccionBosqueDetalleModel();

  parametroModel = {} as ParametroModel;
  proteccinBosqueGestionAmbiental = {} as ProteccionBosqueGestionAmbientalModel;

  tituloModal: string = 'Registrar Plan Gestion Ambiental';
  displayBasic: boolean = false;
  displayModal: boolean = false;

  planGestion = {} as ProteccionBosqueGestionAmbientalModel;
  planGestionType1: ProteccionBosqueGestionAmbientalModel[] = [];
  planGestionType2: ProteccionBosqueGestionAmbientalModel[] = [];
  planGestionType3: ProteccionBosqueGestionAmbientalModel[] = [];

  tipos: any[] = [];
  edit: boolean = false;
  usuario = {} as UsuarioModel;
  factoresAmbientales: any[] = [];
  actividades: any[] = [];

  proteccionImpacto = {} as ProteccionBosqueImpactoAmbientalModel;
  proteccionImpactoList: ProteccionBosqueDetalleModel[] = [];
  editImpacto: boolean = false;

  //---------------- modelo ProteccionBosqueModel ---------------//

  lisLinderos: any[] = [];
  lisLinderosBase: any[] = [];
  listImpactoAmbientalFactores: any[] = [];
  listLinderosGuardar: ProteccionBosquePGMFAModel[] = [];
  listImpactoAmbientalFactoresGuardar: ProteccionBosquePGMFAModel[] = [];

  proteccionBosqueObj: ProteccionBosquePGMFAModel =
    new ProteccionBosquePGMFAModel();
  listPreventivoCorrector: ProteccionBosquePGMFAModel[] = [];
  listVigilanciaSeguimiento: ProteccionBosquePGMFAModel[] = [];
  listContingenciaAmbientales: ProteccionBosquePGMFAModel[] = [];

  idProBosque: number = 0;

  listProteccionBosque: any = [];

  constructor(
    private servProteccionBosque: ProteccionBosqueService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private genericoServices: GenericoService,
    private parametroValorService: ParametroValorService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private proteccionDelBosqueService: ProteccionDelBosqueService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private router: Router
  ) {}
  listSistemaDemarcacion: any[] = [];

  ngOnInit(): void {
    this.listarProteccionBosqueLinderosBase();
    this.listProteccion();
    this.obtenerListadoGestionAmbiental();
    this.obtenerActividades();
    this.obtenerFactores();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  //----------------------- lo que funciona --------------------//

  listProteccion() {
    this.lisLinderos = [];
    this.planGestionType1 = [];
    this.planGestionType2 = [];
    this.planGestionType3 = [];
    this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codPlanGeneral: 'PGMFA',
      subCodPlanGeneral: 'AMB',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.proteccionDelBosqueService
      .listarProteccionDelBosque(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.data != null) {
          if (response.data.length != 0) {
            let count = 0;
            response.data.forEach((element: any) => {
              this.detEvaluacionImpActual.actividades = [];
              this.idProBosque = element.idProBosque;
              if (element.codPlanGeneralDet == 'PGMFADEMA') {
                this.idProBosque = element.idProBosque;
                let tmp = this.lisLinderosBase.find(
                  (item) => item.tipoMarcacion == element.tipoMarcacion
                );
                if (tmp !== null && tmp !== undefined) {
                  if (tmp.idProBosqueDet != '6') {
                    this.lisLinderos.push({ ...element, otro: false });
                  } else {
                    count++;
                    this.lisLinderos.push({ ...element, otro: true });
                  }
                } else {
                  count++;
                  if (!element.nuAccion) {
                    if (count == 1) {
                      this.lisLinderos.push({ ...element, otro: true });
                    }
                  } else {
                    this.lisLinderos.push({ ...element, otro: true });
                  }
                }
              } else if (element.subCodPlanGeneralDet == 'PGMFAPRCO') {
                this.planGestionType1.push(element);
              } else if (element.subCodPlanGeneralDet == 'PGMFAVISE') {
                this.planGestionType2.push(element);
              } else if (element.subCodPlanGeneralDet == 'PGMFACOAM') {
                this.planGestionType3.push(element);
              } else if (element.subCodPlanGeneralDet == 'PGMFAIDIM') {
                element.listProteccionBosqueActividad.forEach((x: any) => {
                  if (x.accion == true && x.codigo == 'CEN') {
                    this.detEvaluacionImpActual.actividades.push('1');
                  } else if (x.accion == true && x.codigo == 'DELI') {
                    this.detEvaluacionImpActual.actividades.push('2');
                  } else if (x.accion == true && x.codigo == 'COCA') {
                    this.detEvaluacionImpActual.actividades.push('3');
                  } else if (x.accion == true && x.codigo == 'COMI') {
                    this.detEvaluacionImpActual.actividades.push('4');
                  } else if (x.accion == true && x.codigo == 'TAL') {
                    this.detEvaluacionImpActual.actividades.push('5');
                  } else if (x.accion == true && x.codigo == 'ARR') {
                    this.detEvaluacionImpActual.actividades.push('6');
                  } else if (x.accion == true && x.codigo == 'OTR') {
                    this.detEvaluacionImpActual.actividades.push('7');
                  }
                });
                element.actividades = this.detEvaluacionImpActual.actividades;
                this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.push(
                  element
                );
              }
            });
            if (
              this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque
                .length == 0
            ) {
              this.listarProteccionBosqueImpacto();
            } else if (this.lisLinderos.length == 0) {
              this.listarProteccionBosqueLinderos();
            }
          } else if (
            this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque
              .length == 0 &&
            this.lisLinderos.length == 0
          ) {
            this.listarProteccionBosque();
          }
        } else {
          this.listarProteccionBosque();
        }
      });
  }

  listarProteccionBosque() {
    this.lisLinderos = [];
    var params = {
      subCodPlanGeneral: 'PGMFA', //cambiar el codigo
    };

    this.proteccionDelBosqueService
      .listarProteccionBosqueCodigo(params)
      .subscribe((response: any) => {
        response.data.forEach((item: any) => {
          if (item.codPlanGeneralDet == 'PGMFADEMA') {
            if (item.idProBosqueDet == 6) {
              this.lisLinderos.push({ ...item, idProBosqueDet: 0, otro: true });
            } else {
              this.lisLinderos.push({
                ...item,
                idProBosqueDet: 0,
                otro: false,
              });
            }
          } else if (item.codPlanGeneralDet == 'PGMFAEVAL') {
            this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.push(
              {
                ...item,
                factorAmbiental: item.tipoMarcacion,
                idProBosqueDet: 0,
              }
            );
          }
        });
      });
  }

  listarProteccionBosqueLinderos() {
    this.lisLinderos = [];
    var params = {
      subCodPlanGeneral: 'PGMFA', //cambiar el codigo
    };

    this.proteccionDelBosqueService
      .listarProteccionBosqueCodigo(params)
      .subscribe((response: any) => {
        response.data.forEach((item: any) => {
          if (item.codPlanGeneralDet == 'PGMFADEMA') {
            this.lisLinderos.push({ ...item, idProBosqueDet: 0 });
          }
        });
      });
  }

  listarProteccionBosqueImpacto() {
    this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque = [];
    var params = {
      subCodPlanGeneral: 'PGMFA', //cambiar el codigo
    };

    this.proteccionDelBosqueService
      .listarProteccionBosqueCodigo(params)
      .subscribe((response: any) => {
        response.data.forEach((item: any) => {
          if (item.codPlanGeneralDet == 'PGMFAEVAL') {
            this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.push(
              {
                ...item,
                factorAmbiental: item.tipoMarcacion,
                idProBosqueDet: 0,
              }
            );
          }
        });
      });
  }

  listarProteccionBosqueLinderosBase() {
    this.lisLinderosBase = [];
    var params = {
      subCodPlanGeneral: 'PGMFA',
    };

    this.proteccionDelBosqueService
      .listarProteccionBosqueCodigo(params)
      .subscribe((response: any) => {
        response.data.forEach((item: any) => {
          if (item.codPlanGeneralDet == 'PGMFADEMA') {
            this.lisLinderosBase.push(item);
          }
        });
      });
  }

  //--------------------------------- 8.1 ----------------------------------------//

  AgregarSistemaDemarcacion() {
    this.lisLinderos.push({
      codPlanGeneralDet: 'PGMFADEMA',
      descripcion: 'Protección del Bosque',
      idProBosque: 4,
      idProBosqueDet: 0,
      nuAccion: true,
      subCodPlanGeneral: 'PGMFA',
      tipoMarcacion: 'Otros (especificar)',
      otro: true,
    });
  }

  GuardarProteccionBosqueDemarcacion() {
    if (!this.validarProteccionBosqueDemarcacion()) {
      return;
    } else {
      this.guardarProteccionBosque();
    }
  }

  validarProteccionBosqueDemarcacion(): boolean {
    let validar: boolean = true;
    let validarOtros: boolean = true;
    let result: boolean = true;
    let validarCount: number = 0;
    let mensaje: string = '';

    for (let item of this.lisLinderos) {
      if (item.nuAccion) {
        if (item.implementacion == null || item.implementacion == '') {
          validar = false;
        }
        if (
          (item.tipoMarcacion == null || item.tipoMarcacion == '') &&
          item.otro == true
        ) {
          validarOtros = false;
        }
      } else {
        validarCount++;
      }
    }

    if (validar == false) {
      mensaje = mensaje += '(*) Debe ingresar la implementación.\n';
    }

    if (validarOtros == false) {
      mensaje = mensaje += '(*) Debe especificar el sistema demarcación.\n';
    }

    if (this.lisLinderos.length == validarCount) {
      result = false;
      mensaje = mensaje +=
        '(*) Debe seleccionar por lo menos un sistema demarcación.\n';
    }

    if (!validar || !validarOtros || !result) {
      result = false;
    }

    if (mensaje != '') this.ErrorMensaje(mensaje);

    return result;
  }

  guardarProteccionBosque() {
    this.listLinderosGuardar = [];
    let count = 0;
    this.lisLinderos.forEach((item) => {
      let tmp = this.lisLinderosBase.find(
        (x) => x.tipoMarcacion == item.tipoMarcacion
      );
      if (tmp !== null && tmp !== undefined) {
      } else {
        count++;
        if (!item.nuAccion) {
          item.implementacion = '';
          if (count == 1) {
            item.tipoMarcacion = this.lisLinderosBase.find(
              (x) => x.idProBosqueDet == '6'
            ).tipoMarcacion;
          }
        }
      }

      let proteccionBosqueObj = new ProteccionBosquePGMFAModel(item);
      proteccionBosqueObj.codPlanGeneralDet = 'PGMFADEMA';
      proteccionBosqueObj.subCodPlanGeneralDet = 'PGMFADEMALI';
      proteccionBosqueObj.listProteccionBosqueActividad = [];
      this.listLinderosGuardar.push(proteccionBosqueObj);
    });

    var params = [
      {
        idProBosque: this.idProBosque,
        idPlanManejo: this.idPlanManejo,
        codPlanGeneral: 'PGMFA',
        subCodPlanGeneral: 'AMB',
        descripcion: '',
        idUsuarioRegistro: this.user.idUsuario,
        listProteccionBosque: this.listLinderosGuardar,
        listProteccionAnalisis: [],

        listProteccionGestion: [],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.proteccionDelBosqueService.registrarProteccionBosque(params).subscribe(
      (response: any) => {
        this.dialog.closeAll();
        this.listProteccion();
        this.toast.ok(
          'Se registró la demarcación y mantenimiento de linderos correctamente.'
        );
        // this.messageService.add({
        //   severity: "success",
        //   summary: "",
        //   detail: response.message,
        // });
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  //------------------------ 8.2.2 --------------------------------//
  obtenerListadoGestionAmbiental() {
    var params = { prefijo: 'TPRO' };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.tipos = data.data;
      });
  }

  registrarPlanGestion() {
    this.planGestion.idProBosqueGesAmb = 0;

    if (this.planGestion.idTipoPrograma == 1) {
      this.planGestionType1.push(this.planGestion);
    }

    if (this.planGestion.idTipoPrograma == 2) {
      this.planGestionType2.push(this.planGestion);
    }

    if (this.planGestion.idTipoPrograma == 3) {
      this.planGestionType3.push(this.planGestion);
    }

    this.planGestion = {} as ProteccionBosqueGestionAmbientalModel;
    this.displayBasic = false;
  }

  registrarPlanesGestion() {
    this.listPreventivoCorrector = [];
    this.listVigilanciaSeguimiento = [];
    this.listContingenciaAmbientales = [];

    this.planGestionType1.forEach((item) => {
      let preventivoCorrectorObj = new ProteccionBosquePGMFAModel(item);
      preventivoCorrectorObj.codPlanGeneralDet = 'PGMFAPGA'; //
      preventivoCorrectorObj.subCodPlanGeneralDet = 'PGMFAPRCO'; //
      this.listPreventivoCorrector.push(preventivoCorrectorObj);
    });

    this.planGestionType2.forEach((item) => {
      let vigilanciaSeguimientoObj = new ProteccionBosquePGMFAModel(item);
      vigilanciaSeguimientoObj.codPlanGeneralDet = 'PGMFAPGA'; //
      vigilanciaSeguimientoObj.subCodPlanGeneralDet = 'PGMFAVISE'; //
      this.listVigilanciaSeguimiento.push(vigilanciaSeguimientoObj);
    });

    this.planGestionType3.forEach((item) => {
      let contingenciaAmbientalesObj = new ProteccionBosquePGMFAModel(item);
      contingenciaAmbientalesObj.codPlanGeneralDet = 'PGMFAPGA'; //
      contingenciaAmbientalesObj.subCodPlanGeneralDet = 'PGMFACOAM'; //
      this.listContingenciaAmbientales.push(contingenciaAmbientalesObj);
    });

    var params = [
      {
        idProBosque: this.idProBosque,
        idPlanManejo: this.idPlanManejo,
        codPlanGeneral: 'PGMFA',
        subCodPlanGeneral: 'AMB',
        descripcion: '',
        idUsuarioRegistro: this.user.idUsuario,
        listProteccionBosque: [],
        listProteccionAnalisis: [],
        listProteccionGestion: [
          ...this.listPreventivoCorrector,
          ...this.listVigilanciaSeguimiento,
          ...this.listContingenciaAmbientales,
        ],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.proteccionDelBosqueService.registrarProteccionBosque(params).subscribe(
      (response: any) => {
        this.dialog.closeAll();
        this.listProteccion();
        this.toast.ok('Se registró el plan de gestión ambiental.');
        // this.messageService.add({
        //   severity: "success",
        //   summary: "",
        //   detail: response.message,
        // });
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }
  abrirModalPlanes() {
    this.displayBasic = true;
    this.editarPlanGe = false;
  }

  AgregarPlanGestion() {
    if (!this.validarPlanGestion()) {
      return;
    }
    if (this.edit) {
      this.editarPlanGestion();
    } else {
      this.registrarPlanGestion();
    }
  }

  validarPlanGestion(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.planGestion.actividad == null ||
      this.planGestion.actividad == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Actividad.\n';
    }
    if (this.planGestion.idTipoPrograma == null && !this.editarPlanGe) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Tipo de Programa.\n';
    }
    if (this.planGestion.impacto == null || this.planGestion.impacto == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Descripción.\n';
    }
    if (
      this.planGestion.mitigacionAmbiental == null ||
      this.planGestion.mitigacionAmbiental == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Medida de Mitigación.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }
  cerrarModalPlanes() {
    this.planGestion = {} as ProteccionBosqueGestionAmbientalModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = 'Registrar Plan Gestión Ambiental';
  }

  editarPlanGestion() {
    this.displayBasic = false;
    this.tituloModal = 'Registrar Plan Gestión Ambiental';
    this.editarPlanGe = false;
    this.planGestion = {} as ProteccionBosqueGestionAmbientalModel;
  }

  openEditarPlanGestion(data: any) {
    
    if (data.subCodPlanGeneralDet == 'PGMFAPRCO') {
      this.programa = 'Preventivo - corrector';
    } else if (data.subCodPlanGeneralDet == 'PGMFAVISE') {
      this.programa = 'Vigilacia y seguimiento';
    } else if (data.subCodPlanGeneralDet == 'PGMFACOAM') {
      this.programa = 'Contingencias ambientales';
    }

    this.planGestion = data;
    this.displayBasic = true;
    this.editarPlanGe = true;
    this.tituloModal = 'Editar Plan Gestión Ambiental';
  }

  openEliminarPlanGestion(event: Event, index: number, planGestion: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (planGestion.subCodPlanGeneralDet == 'PGMFAPRCO') {
          if (planGestion.idProBosqueDet) {
            var params = {
              idProBosque: this.idProBosque,
              idProBosqueDet: planGestion.idProBosqueDet,
              idUsuarioElimina: this.user.idUsuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.proteccionDelBosqueService
              .eliminarProteccionBosque(params)
              .subscribe(
                (data: any) => {
                  this.SuccessMensaje(data.message);
                  this.planGestionType1.splice(index, 1);
                  this.dialog.closeAll();
                },
                (error: HttpErrorResponse) => {
                  this.dialog.closeAll();
                }
              );
          } else {
            this.planGestionType1.splice(index, 1);
          }
        }
        if (planGestion.subCodPlanGeneralDet == 'PGMFAVISE') {
          if (planGestion.idProBosqueDet) {
            var params = {
              idProBosque: this.idProBosque,
              idProBosqueDet: planGestion.idProBosqueDet,
              idUsuarioElimina: this.user.idUsuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.proteccionDelBosqueService
              .eliminarProteccionBosque(params)
              .subscribe(
                (data: any) => {
                  this.SuccessMensaje(data.message);
                  this.planGestionType2.splice(index, 1);
                  this.dialog.closeAll();
                },
                (error: HttpErrorResponse) => {
                  this.dialog.closeAll();
                }
              );
          } else {
            this.planGestionType2.splice(index, 1);
          }
        }
        if (planGestion.subCodPlanGeneralDet == 'PGMFACOAM') {
          if (planGestion.idProBosqueDet) {
            var params = {
              idProBosque: this.idProBosque,
              idProBosqueDet: planGestion.idProBosqueDet,
              idUsuarioElimina: this.user.idUsuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.proteccionDelBosqueService
              .eliminarProteccionBosque(params)
              .subscribe(
                (data: any) => {
                  this.SuccessMensaje(data.message);
                  this.planGestionType3.splice(index, 1);
                  this.dialog.closeAll();
                },
                (error: HttpErrorResponse) => {
                  this.dialog.closeAll();
                }
              );
          } else {
            this.planGestionType3.splice(index, 1);
          }
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  //---------------------------- 8.2.1 -------------------------------------//

  obtenerActividades() {
    var params = { prefijo: 'ACTIMPAMB' };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.actividades = data.data;
      });
  }

  obtenerFactores() {
    var params = { prefijo: 'FACTAMB' };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.factoresAmbientales = data.data;
      });
  }

  abrirModalImpacto() {
    
    this.detEvaluacionImpActual = new ProteccionBosqueDetalleModel();
    this.editImpacto = false;
    this.displayModal = true;
  }

  agregarImpacto() {
    if (!this.validarImpacto()) {
      return;
    }
    if (this.editImpacto) {
      this.validarActividadesImpacto();
      this.editarImpacto();
    } else {
      this.registrarImpacto();
    }
  }

  validarImpacto() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.editImpacto &&
      (this.detEvaluacionImpActual.factorAmbiental == null ||
      this.detEvaluacionImpActual.factorAmbiental == '')
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Factor.\n';
    }

    if (
      this.detEvaluacionImpActual.impacto == null ||
      this.detEvaluacionImpActual.impacto == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Impacto.\n';
    }

    if (
      this.detEvaluacionImpActual.actividades == null ||
      this.detEvaluacionImpActual.actividades.length == 0
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar: Actividad.\n';
    }

    if (
      this.detEvaluacionImpActual.actividades != null &&
      this.detEvaluacionImpActual.actividades.includes('7')
    ) {
      if (
        this.detEvaluacionImpActual.descOtra == null ||
        this.detEvaluacionImpActual.descOtra == ''
      ) {
        validar = false;
        mensaje = mensaje += '(*) Debe ingresar: Descripción de Otra.\n';
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  openEditarImpacto(impacto: ProteccionBosqueDetalleModel) {
    this.detEvaluacionImpActual = impacto;
    this.validarActividadesBolean();
    this.editImpacto = true;
    this.displayModal = true;
  }

  onChangeAct(event: any) {
    if (this.detEvaluacionImpActual.actividades.includes('7')) {
      this.detEvaluacionImpActual.nuOtra = true;
    } else {
      this.detEvaluacionImpActual.nuOtra = false;
      this.detEvaluacionImpActual.descOtra = '';
    }
  }

  cancelarImpactoModal() {
    this.editImpacto = false;
    this.displayModal = false;
    this.detEvaluacionImpActual = {} as ProteccionBosqueDetalleModel;
  }

  editarImpacto() {
    this.editImpacto = false;
    this.displayModal = false;
    this.detEvaluacionImpActual = {} as ProteccionBosqueDetalleModel;
  }

  registrarImpacto() {
    this.validarActividadesImpacto();
    this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.push(
      this.detEvaluacionImpActual
    );
    this.displayModal = false;
    this.detEvaluacionImpActual = {} as ProteccionBosqueDetalleModel;
  }

  validarActividadesImpacto() {
    if (this.detEvaluacionImpActual.actividades.includes('1')) {
      this.detEvaluacionImpActual.nuCenso = true;
    } else {
      this.detEvaluacionImpActual.nuCenso = false;
    }

    if (this.detEvaluacionImpActual.actividades.includes('2')) {
      this.detEvaluacionImpActual.nuDemarcacionLineal = true;
    } else {
      this.detEvaluacionImpActual.nuDemarcacionLineal = false;
    }

    if (this.detEvaluacionImpActual.actividades.includes('3')) {
      this.detEvaluacionImpActual.nuConstruccionCampamento = true;
    } else {
      this.detEvaluacionImpActual.nuConstruccionCampamento = false;
    }

    if (this.detEvaluacionImpActual.actividades.includes('4')) {
      this.detEvaluacionImpActual.nuConstruccionCamino = true;
    } else {
      this.detEvaluacionImpActual.nuConstruccionCamino = false;
    }

    if (this.detEvaluacionImpActual.actividades.includes('5')) {
      this.detEvaluacionImpActual.nuTala = true;
    } else {
      this.detEvaluacionImpActual.nuTala = false;
    }

    if (this.detEvaluacionImpActual.actividades.includes('6')) {
      this.detEvaluacionImpActual.nuArrastre = true;
    } else {
      this.detEvaluacionImpActual.nuArrastre = false;
    }
  }

  openEliminarImpacto(event: Event, index: number, impacto: any) {
    const params = {
      idProBosque: impacto.idProBosque,
      idProBosqueDet: impacto.idProBosqueDet,
      idUsuarioElimina: this.user.idUsuario,
    };

    

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (impacto.idProBosqueDet) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.proteccionDelBosqueService
            .eliminarProteccionBosque(params)
            .subscribe(
              (result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                  this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.splice(
                    index,
                    1
                  );
                }

                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.splice(
            index,
            1
          );
        }
      },
    });
  }

  validarActividadesBolean() {
    if (this.proteccionImpacto.censo) {
      this.proteccionImpacto.actividades.push('1');
    }

    if (this.proteccionImpacto.demarcacionLineal) {
      this.proteccionImpacto.actividades.push('2');
    }

    if (this.proteccionImpacto.construccionCampamento) {
      this.proteccionImpacto.actividades.push('3');
    }

    if (this.proteccionImpacto.construccionCamino) {
      this.proteccionImpacto.actividades.push('4');
    }

    if (this.proteccionImpacto.tala) {
      this.proteccionImpacto.actividades.push('5');
    }

    if (this.proteccionImpacto.arrastre) {
      this.proteccionImpacto.actividades.push('6');
    }

    if (this.proteccionImpacto.otra) {
      this.proteccionImpacto.actividades.push('7');
    }
  }

  // --------------------------------------------------------------//

  //----------------------------------------------------------------------//

  actualizarProteccionBosqueDemarcacion() {}

  listActividades = [
    { idProBosqueActividad: 0, codigo: 'CEN', descripcion: 'Censo', accion: 0 },
    {
      idProBosqueActividad: 0,
      codigo: 'DELI',
      descripcion: 'Demarcación de Línderos',
      accion: 0,
    },
    {
      idProBosqueActividad: 0,
      codigo: 'COCA',
      descripcion: 'Construcción de Campamentos',
      accion: 0,
    },
    {
      idProBosqueActividad: 0,
      codigo: 'COMI',
      descripcion: 'Construccion de Caminos',
      accion: 0,
    },
    { idProBosqueActividad: 0, codigo: 'TAL', descripcion: 'Tala', accion: 0 },
    {
      idProBosqueActividad: 0,
      codigo: 'ARR',
      descripcion: 'Arrastre',
      accion: 0,
    },
    { idProBosqueActividad: 0, codigo: 'OTR', descripcion: 'Otras', accion: 0 },
  ];

  listProteccionBosqueActividad: any[] = [];
  listcopia: any[] = [];
  listMandar: any[] = [];
  GuardarEvaluacionImpactoAmbiental() {
    this.marcar();
    this.listMandar = [];
    this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.forEach(
      (item) => {
        let obj = new ProteccionBosquePGMFAModel2(item);
        obj.codPlanGeneralDet = 'PGMFAANIM';
        obj.subCodPlanGeneralDet = 'PGMFAIDIM';
        this.listMandar.push(obj);
      }
    );

    var params = [
      {
        idProBosque: this.idProBosque,
        idPlanManejo: this.idPlanManejo,
        codPlanGeneral: 'PGMFA',
        subCodPlanGeneral: 'AMB',
        descripcion: '',
        idUsuarioRegistro: this.user.idUsuario,
        listProteccionBosque: [],
        listProteccionAnalisis: this.listMandar,
        listProteccionGestion: [],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.proteccionDelBosqueService.registrarProteccionBosque(params).subscribe(
      (response: any) => {
        this.dialog.closeAll();
        this.listProteccion();
        this.toast.ok(
          'Se registró la matriz de identificación de impactos ambientales.'
        );
        // this.messageService.add({
        //   severity: "success",
        //   summary: "",
        //   detail: response.message,
        // });
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  marcar() {
    this.evaluacionImpactoAmbientalProBosModel.listProteccionBosque.forEach(
      (item, index) => {
        if (item.listProteccionBosqueActividad.length != 0) {
          if (item.actividades) {
            item.actividades.forEach((r) => {
              item.listProteccionBosqueActividad.forEach((y, i: number) => {
                if (r == '1' && y.codigo == 'CEN') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '2' && y.codigo == 'DELI') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '3' && y.codigo == 'COCA') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '4' && y.codigo == 'COMI') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '5' && y.codigo == 'TAL') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '6' && y.codigo == 'ARR') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '7' && y.codigo == 'OTR') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                }
              });
            });
          }
        } else {
          this.listActividades.forEach((x) => {
            item.listProteccionBosqueActividad.push({ ...x });
          });
          if (item.actividades) {
            item.actividades.forEach((r) => {
              item.listProteccionBosqueActividad.forEach((y, i: number) => {
                if (r == '1' && y.codigo == 'CEN') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '2' && y.codigo == 'DELI') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '3' && y.codigo == 'COCA') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '4' && y.codigo == 'COMI') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '5' && y.codigo == 'TAL') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '6' && y.codigo == 'ARR') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                } else if (r == '7' && y.codigo == 'OTR') {
                  item.listProteccionBosqueActividad[i].accion = 1;
                }
              });
            });
          }
        }
      }
    );
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  onConfirm() {
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluaciond81 = Object.assign(
                this.evaluaciond81,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon81
                )
              );
              this.evaluaciond821 = Object.assign(
                this.evaluaciond821,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon821
                )
              );
              this.evaluaciond822 = Object.assign(
                this.evaluaciond822,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon822
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluaciond81,
        this.evaluaciond821,
        this.evaluaciond822,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluaciond81);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluaciond821);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluaciond822);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

//cabecera
export class ProteccionBosqueModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.showRemove = data.showRemove;
      this.marcacion = data.marcacion;
      this.accion = data.accion;
      this.implementacion = data.implementacion;
      return;
    }
  }
  id: number = 0;
  showRemove: boolean = true;
  accion: boolean = true;
  marcacion: string = '';
  implementacion: string = '';
}

//Ccabecera
export class ProteccionBosqueCabeceraModel {
  constructor(data?: any) {
    if (data) {
      this.idProBosque = data.idProBosque;
      this.idPlanManejo = data.idPlanManejo;
      this.idPlanGeneral = data.idPlanGeneral;
      this.codigoCabecera = data.codigoCabecera;
      this.descripcion = data.descripcion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.listProteccionBosque = data.listProteccionBosque;
      return;
    }
  }

  idProBosque: number = 0;
  idPlanManejo: number = 0;
  idPlanGeneral: number = 0;
  codigoCabecera: string = '';
  descripcion: string = '';
  idUsuarioRegistro: number = 0;
  listProteccionBosque: ProteccionBosqueDetalleModel[] = [];
}

//Detalle
export class ProteccionBosqueDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.idProBosqueDet = data.idProBosqueDet;
      this.codigoCabeceraDetalle = data.codigoCabeceraDetalle;
      this.tipoMarcacion = data.tipoMarcacion;
      this.nuAccion = data.nuAccion;
      this.implementacion = data.implementacion;
      this.factorAmbiental = data.factorAmbiental;
      this.impacto = data.impacto;
      this.nuCenso = data.nuCenso;
      this.nuDemarcacionLineal = data.nuDemarcacionLineal;
      this.nuConstruccionCampamento = data.nuConstruccionCampamento;
      this.nuConstruccionCamino = data.nuConstruccionCamino;
      this.nuTala = data.nuTala;
      this.nuArrastre = data.nuArrastre;
      this.nuOtra = data.nuOtra;
      this.descOtra = data.descOtra;
      this.tipoPrograma = data.tipoPrograma;
      this.actividad = data.actividad;
      this.mitigacionAmbiental = data.mitigacionAmbiental;
      this.impactoGestion = data.impactoGestion;
      this.actividades = data.actividades;
      this.descripccionOtra = data.descripccionOtra;
      this.listProteccionBosqueActividad = data.listProteccionBosqueActividad;
      return;
    }
  }

  idProBosqueDet: number = 0;
  codigoCabeceraDetalle: string | null = null;
  tipoMarcacion: string | null = null;
  nuAccion: number | null = null;
  implementacion: string | null = null;
  factorAmbiental: string | null = null;
  impacto: string | null = null;
  nuCenso: boolean | null = null;
  nuDemarcacionLineal: boolean | null = null;
  nuConstruccionCampamento: boolean | null = null;
  nuConstruccionCamino: boolean | null = null;
  nuTala: boolean | null = null;
  nuArrastre: boolean | null = null;
  nuOtra: boolean | null = null;
  descOtra: string | null = null;
  tipoPrograma: string | null = null;
  actividad: string | null = null;
  mitigacionAmbiental: string | null = null;
  impactoGestion: string | null = null;
  actividades: string[] = [];
  descripccionOtra: string | null = null;
  listProteccionBosqueActividad: ListProteccionBosqueActividad2[] = [];
}
