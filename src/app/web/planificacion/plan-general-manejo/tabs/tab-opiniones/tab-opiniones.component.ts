import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-tab-opiniones",
  templateUrl: "./tab-opiniones.component.html",
  styleUrls: ["./tab-opiniones.component.scss"],
})
export class TabOpinionesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  codigoEpica: string = "PGMFA";

  constructor() {}

  ngOnInit(): void {}
}
