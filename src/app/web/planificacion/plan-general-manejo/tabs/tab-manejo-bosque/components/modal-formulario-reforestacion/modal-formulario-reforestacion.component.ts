import { Component, OnInit } from '@angular/core';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-formulario-reforestacion',
  templateUrl: './modal-formulario-reforestacion.component.html',
})
export class ModalFormularioReforestacionComponent implements OnInit {
  context: any = {};

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toast: ToastService
  ) {}

  categoriTmp: string = '';

  ngOnInit(): void {
    if (this.config.data.type == 'E') {
      this.context = { ...this.config.data.data };
      this.categoriTmp = this.config.data.data.categoria;
    }
  }

  agregar = () => {
  
    this.ref.close(this.context);
  };

  cerrarModal() {
  
    this.ref.close();
  }
}
