import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "tabla-construccion-caminos",
  templateUrl: "./tabla-construccion-caminos.component.html",
  styleUrls: ["./tabla-construccion-caminos.component.scss"],
})
export class TablaConstruccionCaminosComponent implements OnInit {
  tablaInformativa = [
    {
      infraestructura: "Caminos principales",
      espesificaciones:
        "Adecuado sistema de drenaje de las aguas de lluvia Ancho de superficie de rodaje entre 5 m y 6m (según volumen de transporte programado) Pendientes menores al 12% Obras de conservación necesarias para minimizar la erosión y los daños al suelo y las aguas Al concluir la operación, asegurar el mantenimiento apropiado , incluyendo la superficie de rodaje y las estructuras de drenaje De ser posible, tener una capa de revestimiento de cascajo de al menos 5cm. de espesor.",
    },
    { infraestructura: "Caminos Secundarios", espesificaciones: 
    "Pendientes menores al 14% Anchura carrozable de 3 a 4 m Utilización normalmente restringida a los períodos mas secos del año Contar con un eficiente sistema de drenaje Evitar atravesar cursos de agua Los pasos de agua son funcionales final de la operación se clausuran y se toman medidas para evitar la erosión y restituir las funciones y procesos del sistema natural de drenajes" },
    { infraestructura: "Patios de trozas", espesificaciones: " Ubicación, densidad y tamaño serán apropiados, en función al volumen de madera, la maquinaria disponible y las condiciones de sitio Localización próxima a las márgenes de los caminos y a un radio de distancia de operación del tractor forestal de hasta 1 km. Dentro de lo posible, la pendiente en el patio no deberá superar el 5% y drenarán directamente a los caminos ni a los arroyos circundantes deberán estar ubicados a más de 50 m de ríos o quebradas" },

  ];

  constructor() {}

  ngOnInit(): void {}
}
