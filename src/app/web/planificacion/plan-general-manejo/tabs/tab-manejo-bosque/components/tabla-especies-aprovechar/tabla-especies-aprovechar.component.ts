import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ManejoBosqueDetalleModel } from "src/app/model/ManejoBosque";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { CoreCentralService } from "src/app/service/coreCentral.service";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";
import { DialogTipoEspecieMultiselectComponent } from "src/app/shared/components/dialog-tipo-especie-multiselect/dialog-tipo-especie-multiselect.component";

@Component({
  selector: "tabla-especies-aprovechar",
  templateUrl: "./tabla-especies-aprovechar.component.html",
  styleUrls: ["./tabla-especies-aprovechar.component.scss"],
})
export class TablaEspeciesAprovecharComponent implements OnInit {
  planManejo = {} as PlanManejoModel;
  usuario = {} as UsuarioModel;

  displayModalEspecies: boolean = false;
  editEspecies: boolean = false;
  especie: any = {};

  comboListEspecies: any[] = [];
  selectEspecie!: any;
  indexEspecie: any = null;

  ref!: DynamicDialogRef;

  @Input() especiesList: any[] = [];
  @Input() idManejoBosque: number = 0;
  @Input() disabled!: boolean;
  @Output() update = new EventEmitter();

  @Output() agregarEsp = new EventEmitter();

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private manejoServ: ManejoBosqueService,
    private coreCentralService: CoreCentralService,
    private route: ActivatedRoute,
    public dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.planManejo.idPlanManejo = Number(
      this.route.snapshot.paramMap.get("idPlan")
    );
    this.usuario = JSON.parse("" + localStorage.getItem("usuario")?.toString());
    this.listaPorFiltroEspecieForestal({});
  }

  openModalEspecies = (mesaje: string) => {
    this.ref = this.dialogService.open(DialogTipoEspecieMultiselectComponent, {
      header: mesaje,
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          let obj = new ManejoBosqueDetalleModel(element);
          obj.nuIdEspecie = element.idEspecie;
          obj.especie = element.nombreCientifico;
          obj.lineaProduccion = "";
          obj.nuDiametro = 0;

          this.especiesList.push(obj);
        });
      }
    });
  };

  abrirModalEspecies() {
    this.selectEspecie = null;
    this.comboListEspecies = [];
    this.especie = {};
    this.displayModalEspecies = true;
    this.indexEspecie = null;
  }

  cerrarModalEspecies() {
    this.displayModalEspecies = false;
  }

  registrarEspcie() {
    if (!this.validarEspecies()) return;

    this.especie.idEspecie = this.selectEspecie.idEspecie;
    this.especie.especie = this.selectEspecie.nombreCientifico;

    if (this.editEspecies) {
      this.editarEspecie();
    } else {
      this.agregarEspecie();
    }
  }

  registrarEspecies() {
    let params = {};
    const listaEspecies: any[] = [];
    this.especiesList.forEach((data) => {
      let obj = new ManejoBosqueDetalleModel(data);
      obj.nuIdEspecie = data.nuIdEspecie;
      obj.especie = data.especie;
      obj.lineaProduccion = data.lineaProduccion;
      obj.nuDiametro = data.nuDiametro;

      listaEspecies.push(obj);
    });

    this.agregarEsp.emit(listaEspecies);
    /* params = [
      {
        idPlanManejo: this.planManejo.idPlanManejo,
        idManejoBosque: 0,
        tipo: "PGMFA",
        codigoManejo: "PGMFAAPO",
        descripcionCab: "",
        idUsuarioRegistro: this.usuario.idusuario,
        listManejoBosqueDetalle: listaEspecies,
      },
    ];

    this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
      if (result.success) {
        this.SuccessMensaje(result.message);
        this.update.emit();
      } else this.ErrorMensaje(result.message);
    }); */
  }

  agregarEspecie() {
    this.especie.idManejoBosqueEsp = 0;

    this.especiesList.push(this.especie);
    this.displayModalEspecies = false;
  }

  openEditarEspecie(data: any, index: any) {
    this.editEspecies = true;
    this.especie = { ...data };

    this.listaPorFiltroEspecieForestal(data.especie);

    /* this.selectEspecie = this.comboListEspecies.filter(
      (value) => value.nombreCientifico == data.especie
    )[0]; */

    this.displayModalEspecies = true;
    this.indexEspecie = index;
  }

  editarEspecie() {
    this.displayModalEspecies = false;
    this.especiesList.splice(this.indexEspecie, 1, this.especie);
  }

  onChange(event: any) {
    this.especie.idEspecie = this.selectEspecie.idEspecie;
    this.especie.especie = this.selectEspecie.nombreCientifico;
  }

  openEliminarEspecie(event: Event, index: number, data: any) {
    let params = {
      idManejoBosque: this.idManejoBosque,
      idUsuarioElimina: this.usuario.idusuario,
      idManejoBosqueDet: data.idManejoBosqueDet,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet != 0) {
          this.manejoServ
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.especiesList.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.especiesList.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  validarEspecies(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    this.especie.idEspecie = this.selectEspecie.idEspecie
      ? this.selectEspecie.idEspecie
      : null;

    if (this.especie.idEspecie == undefined) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar la especie\n";
    }
    if (!this.especie.lineaProduccion) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar linea produccion.\n";
    }

    if (!this.especie.nuDiametro) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar diametro.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  listaPorFiltroEspecieForestal = (event: any) => {
    let params = {
      nombreCientifico: event.query,
    };
    this.coreCentralService
      .ListaPorFiltroEspecieForestal(params)
      .subscribe((result: any) => {
        this.comboListEspecies = result.data || [];
      });
  };
}
