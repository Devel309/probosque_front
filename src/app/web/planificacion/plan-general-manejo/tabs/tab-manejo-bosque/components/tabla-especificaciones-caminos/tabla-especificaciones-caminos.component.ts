import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { ManejoBosqueDetalleModel } from "src/app/model/ManejoBosque";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";

@Component({
  selector: "tabla-especificaciones-caminos",
  templateUrl: "./tabla-especificaciones-caminos.component.html",
  styleUrls: ["./tabla-especificaciones-caminos.component.scss"],
})
export class TablaEspecificacionesCaminosComponent implements OnInit {
  planManejo = {} as PlanManejoModel;
  usuario = {} as UsuarioModel;

  @Input() comboInfraestructuraVial: any[] = [];
  @Input() idManBosqueAprove: any = null;
  @Input() manBosqueAproveCaminoList: ManejoBosqueDetalleModel[] = [];
  @Input() disabled!: boolean;
  @Input() idPlanManejo!: number;
  @Output() update = new EventEmitter();
  @Output() agregar = new EventEmitter();
  @Input() idManejoBosque!: number;

  

  displayModalInfraestructura: boolean = false;
  editInfraestructura: boolean = false;
  indexInfraestructura: number = 0;
  infraestructuraVial = {} as ManejoBosqueDetalleModel;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private manejoServ: ManejoBosqueService
  ) {}

  ngOnInit(): void {
    this.planManejo.idPlanManejo = Number(localStorage.getItem("idPlanManejo"));
    this.usuario = JSON.parse("" + localStorage.getItem("usuario")?.toString());
  }

  abrirModalInfraestructura() {
    this.infraestructuraVial = {} as ManejoBosqueDetalleModel;
    this.displayModalInfraestructura = true;
  }

  cerrarModalInfraestructura() {
    this.displayModalInfraestructura = false;
  }

  agregarInfraestructura() {
    this.manBosqueAproveCaminoList.push(this.infraestructuraVial);
    this.displayModalInfraestructura = false;
  }

  openEliminarInfraestructura(event: Event, index: number, data: any) {
    let param = {
      idManejoBosque: this.idManejoBosque,
      idUsuarioElimina: this.usuario.idusuario,
      idManejoBosqueDet: data.idManejoBosqueDet,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosque > 0) {
          this.manejoServ
            .eliminarManejoBosque(param)
            .subscribe((result: any) => {
              if (result.succes) {
                this.manBosqueAproveCaminoList.splice(index, 1);
                this.SuccessMensaje(result.message);
              } else this.ErrorMensaje(result.message);
            });
        } else {
          this.manBosqueAproveCaminoList.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  openEditarInfraestructura(data: ManejoBosqueDetalleModel, index: any) {
    this.editInfraestructura = true;
    this.displayModalInfraestructura = true;
    this.infraestructuraVial = new ManejoBosqueDetalleModel(data);
    this.indexInfraestructura = index;
  }

  onChange(event: any) {
    let infraestructura = this.comboInfraestructuraVial.find(
      (tipo) => tipo.valorSecundario == event.value
    );
    this.infraestructuraVial.infraestructura = infraestructura.valorSecundario;
    // this.infraestructuraVial.tipoInfraestructura =
    //   infraestructura.valorPrimario;
  }

  registrarEspecificacionCaminos() {
    if (!this.validarEspecificacionCaminos()) return;

    if (this.editInfraestructura) {
      this.editarEspecificacionCaminos();
    } else {
      this.agregarInfraestructura();
    }
  }

  editarEspecificacionCaminos() {
    this.editInfraestructura = false;
    this.displayModalInfraestructura = false;
    this.manBosqueAproveCaminoList.splice(
      this.indexInfraestructura,
      1,
      this.infraestructuraVial
    );
  }

  registrarEspecificacionesCaminos() {
    let params = {};
    const listaEspecificaciones: any = [];
    this.manBosqueAproveCaminoList.forEach((data) => {
      let obj = new ManejoBosqueDetalleModel(data);
      obj.manoObra = data.manoObra;
      obj.infraestructura = data.infraestructura;
      obj.caracteristicasTecnicas = data.caracteristicasTecnicas;
      obj.metodoConstruccion = data.metodoConstruccion;
      obj.maquina = data.maquina;
      listaEspecificaciones.push(obj);
    });

    this.agregar.emit(listaEspecificaciones);

    /*  params = [
      {
        idPlanManejo: this.idPlanManejo,
        idManejoBosque: 0,
        tipo: 'PGMFA',
        codigoManejo: 'PGMFAAPROV',
        descripcionCab: '',
        idUsuarioRegistro: this.usuario.idusuario,
        listManejoBosqueDetalle: listaEspecificaciones,
      },
    ];

    this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
      if (result.success) {
        this.SuccessMensaje(result.message);
        this.update.emit();
      } else this.ErrorMensaje(result.message);
    }); */
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  validarEspecificacionCaminos(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.infraestructuraVial.infraestructura) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar la Infraestructura Vial \n";
    }
    if (!this.infraestructuraVial.caracteristicasTecnicas) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar una Característica.\n";
    }

    if (!this.infraestructuraVial.metodoConstruccion) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar Construcción.\n";
    }

    if (!this.infraestructuraVial.manoObra) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar Mano de Obra.\n";
    }

    if (!this.infraestructuraVial.maquina) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar Maquinaria .\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }
}
