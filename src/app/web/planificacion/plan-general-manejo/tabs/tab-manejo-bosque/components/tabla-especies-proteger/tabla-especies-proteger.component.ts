import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { CoreCentralService } from "@services";
import { toInteger } from "lodash";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import {
  CodigosManejoBosque,
  ManBosqueEspecieProteger,
  ManejoBosqueDetalleModel,
} from "src/app/model/ManejoBosque";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";
import { DialogTipoEspecieMultiselectFaunaComponent } from "src/app/shared/components/dialog-tipo-especie-multiselect-fauna/dialog-tipo-especie-multiselect.component";
import { DialogTipoEspecieMultiselectComponent } from "src/app/shared/components/dialog-tipo-especie-multiselect/dialog-tipo-especie-multiselect.component";

@Component({
  selector: "tabla-especies-proteger",
  templateUrl: "./tabla-especies-proteger.component.html",
  styleUrls: ["./tabla-especies-proteger.component.scss"],
})
export class TablaEspeciesProtegerComponent implements OnInit {
  @Input() usuario!: UsuarioModel;
  @Input() planManejo!: PlanManejoModel;
  @Input() idManejoBosque!: number;

  @Input() especiesProtegerList: ManejoBosqueDetalleModel[] = [];
  @Input() disabled!: boolean;
  @Input() savedListEspecies: ManejoBosqueDetalleModel[] = [];
  @Output() update = new EventEmitter();

  @Input() listEspeciesFlora: ManejoBosqueDetalleModel[] = [];
  @Input() listEspeciesFauna: ManejoBosqueDetalleModel[] = [];

  displayModalEspecies: boolean = false;
  editEspecies: boolean = false;
  especieProteger: any = {};

  indexEspecie: any = null;

  comoTipo: any[] = [
    { valorPrimario: "Fauna", valorSecundario: "1" },
    { valorPrimario: "Flora", valorSecundario: "2" },
  ];

  selectEspecie!: any;
  pendienteFlora: boolean = false;
  pendienteFauna: boolean = false;

  // lo que sirve

  comboListEspecies: any[] = [];
  ref!: DynamicDialogRef;


  listEspecies: any[] = [];

  @Output() agregarEspecies = new EventEmitter();
  CodigosManejoBosque = CodigosManejoBosque;

  constructor(
    private confirmationService: ConfirmationService,
    private manejoServ: ManejoBosqueService,
    private messageService: MessageService,
    private coreCentralService: CoreCentralService,
    public dialogService: DialogService
  ) {}

  ngOnInit(): void {}

  // lo que sirve

  openModalEspeciesFlora = (mesaje: string) => {
    this.ref = this.dialogService.open(DialogTipoEspecieMultiselectComponent, {
      header: mesaje,
      width: "50%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          
          let obj = new ManejoBosqueDetalleModel(element);
          obj.nuIdEspecie = element.idEspecie;
          obj.especie = element.nombreCientifico;
          obj.idManejoBosqueDet = 0;
          obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_3_1;
          this.listEspeciesFlora.push(obj);
          this.pendienteFlora = true;
        });
        this.SuccessMensaje("Se agregaron las especies Flora correctamente.");
      }
    });
  };

  openModalEspeciesFauna = (mesaje: string) => {
    this.ref = this.dialogService.open(
      DialogTipoEspecieMultiselectFaunaComponent,
      {
        header: mesaje,
        width: "50%",
        contentStyle: { "max-height": "500px", overflow: "auto" },
      }
    );

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          
          let obj = new ManejoBosqueDetalleModel(element);
          obj.nuIdEspecie = element.idEspecie;
          obj.especie = element.nombreCientifico;
          obj.idManejoBosqueDet = 0;
          obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_3_2;
          this.listEspeciesFauna.push(obj);
          this.pendienteFauna = true;
        });

        this.SuccessMensaje("Se agregaron las especies Fauna correctamente.");
      }
    });
  };

  eliminarEspecieFlora(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet > 0) {
          let params = {
            idManejoBosque: this.idManejoBosque,
            idUsuarioElimina: this.usuario.idusuario,
            idManejoBosqueDet: data.idManejoBosqueDet,
          };

          this.manejoServ
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.listEspeciesFlora.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.listEspeciesFlora.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  eliminarEspecieFauna(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet > 0) {
          let params = {
            idManejoBosque: this.idManejoBosque,
            idUsuarioElimina: this.usuario.idusuario,
            idManejoBosqueDet: data.idManejoBosqueDet,
          };

          this.manejoServ
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.listEspeciesFauna.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.listEspeciesFauna.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  abrirModalEspecies() {
    this.displayModalEspecies = true;
    this.editEspecies = false;
    this.especieProteger = {};
    this.selectEspecie = null;
    this.comboListEspecies = [];
    this.indexEspecie = null;
  }

  cerrarModalEspecie() {
    this.displayModalEspecies = false;
    this.especieProteger = {};
  }

  editarModalEspecies(data: any, index: number) {
    this.especieProteger = { ...data };

    const select = {
      nombreCientifico: data.especie,
      idEspecie: this.comoTipo.filter(
        (value) => value.valorPrimario === data.tipoEspecie
      )[0].valorSecundario,
      especie: data.especie,
    };
    this.selectEspecie = select;

    this.especieProteger.nuIdEspecie = this.comoTipo.filter(
      (value) => value.valorPrimario === data.tipoEspecie
    )[0].valorSecundario;

    this.displayModalEspecies = true;
    this.editEspecies = true;

    this.comboListEspecies = [];
    this.indexEspecie = index;
  }

  agregarEspecie() {
    if (!this.validarEspecies()) return;

    if (this.editEspecies) {
      this.editarEspecie();
    } else {
      this.registrarEspecie();
    }
  }

  registrarEspecie() {
    this.especieProteger.idManejoBosque = 0;
    // this.especieProteger.nombreCientifico = this.selectEspecie.nombreCientifico;
    this.especieProteger.nuIdEspecie = this.selectEspecie.idEspecie;
    this.especieProteger.especie = this.selectEspecie.nombreCientifico;

    this.savedListEspecies.push(this.especieProteger);
    this.displayModalEspecies = false;
  }

  registrarEspecies() {
    this.listEspecies = [...this.listEspeciesFauna, ...this.listEspeciesFlora];
    this.agregarEspecies.emit(this.listEspecies);
  }

  editarEspecie() {
    this.savedListEspecies.splice(this.indexEspecie, 1, this.especieProteger);

    this.displayModalEspecies = false;
  }

  listaPorFiltroEspecieForestal = (event: any) => {
    let params = {
      nombreCientifico: event.query,
    };
    this.coreCentralService
      .ListaPorFiltroEspecieForestal(params)
      .subscribe((result: any) => {
        this.comboListEspecies = result.data || [];
      });
  };

  listaPorFiltroEspecieFauna = (event: any) => {
    let params = {
      nombreCientifico: event.query,
    };
    this.coreCentralService
      .listaPorFiltroEspecieFauna(params)
      .subscribe((result: any) => {
        this.comboListEspecies = result.data || [];
      });
  };

  validarEspecies(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";
    if (!this.especieProteger.nuIdEspecie) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar el Tipo\n";
    }
    /* if (!this.selectEspecie.nombreCientifico && !this.editEspecies) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar la Especie.\n';
    } */

    if (!this.especieProteger.justificacionEspecie) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar la Justificación.\n";
    }

    if (
      this.selectEspecie &&
      this.especiesProtegerList.some(
        (especie) => especie.nuIdEspecie == this.selectEspecie.nuIdEspecie
      )
    ) {
      validar = false;
      mensaje = mensaje += "(*) Ya se registro esta especie.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  tipoEspecie = (event: any) => {
    let tipo = this.comoTipo.find(
      (tipo) => tipo.valorSecundario == event.value
    );

    this.especieProteger.nuIdEspecie = tipo.valorSecundario;
    this.especieProteger.tipoEspecie = tipo.valorPrimario;
  };

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
