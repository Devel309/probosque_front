import { Component, ViewChild, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { InformacionBasicaDetalle } from 'src/app/model/InformacionAreaManejo';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { InformacionAreaManejoService } from 'src/app/service/informacion-area-manejo/informacion-area-manejo.service';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { MapApi } from 'src/app/shared/mapApi';
import {
  ArchivoService,
  PlanificacionService,
  UsuarioService,
} from '@services';
import { ToastService } from '@shared';
import { data } from 'jquery';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { TabManejoBosqueComponent } from '../../tab-manejo-bosque.component';

@Component({
  selector: 'tabla-manejo-vertices',
  templateUrl: './tabla-manejo-vertices.component.html',
  styleUrls: ['./tabla-manejo-vertices.component.scss'],
})
export class TablaManejoVerticesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() idInfBasica!: number;
  @Input() listCoordinatesAnexo: any[] = [];

 
  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;
 // listCoordinatesAnexo: any[] = []

  @Output() listaCoordenadas = new EventEmitter();

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private informacionAreaManejoService: InformacionAreaManejoService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private mapApi: MapApi,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private tabManejoBosqueComponent: TabManejoBosqueComponent,
  ) {}

  ngOnInit(): void {}
 

  registrar() {
    this.listaCoordenadas.emit(this.listCoordinatesAnexo)

  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje += '(*) Agregue archivo VERTICES PGMF \n';
    }

    if (this.listCoordinatesAnexo.some((cord: any) => cord.referencia == '')) {
      validar = false;
      mensaje = mensaje +=
        '(*) Todas las coordenadas deben llevar referencia \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  onChangeFileSHP(e: any) {
    this.tabManejoBosqueComponent.onChangeFileSHP(e, true, 'TPPUNTO','PGMFAIBAMCUAMAMC');
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaAnexo(data[0].features);
    });
    e.target.value = '';
  }

  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo.push({
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: '',
      });
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: mensaje,
    });
  }
}

export class Vertices {
  constructor(data?: any) {
    if (data) {
      this.este = data.coordenadaEsteIni ? data.coordenadaEsteIni : '';
      this.vertice = data.puntoVertice ? data.puntoVertice : '';
      this.norte = data.coordenadaNorteIni ? data.coordenadaNorteIni : '';
      this.referencia = data.referencia ? data.referencia : '';
    }
  }
  este: string = ''
  vertice: string = ''
  norte: string = ''
  referencia: string = ''
}