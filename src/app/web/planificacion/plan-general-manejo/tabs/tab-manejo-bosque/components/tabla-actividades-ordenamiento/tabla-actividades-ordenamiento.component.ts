import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";

@Component({
  selector: "tabla-actividades-ordenamiento",
  templateUrl: "./tabla-actividades-ordenamiento.component.html",
  styleUrls: ["./tabla-actividades-ordenamiento.component.scss"],
})
export class TablaActividadesOrdenamientoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() ordenamientoActList: any[] = [];
  @Input() disabled!: boolean;
  @Input() sinListado!: boolean;
  @Input() idManejoBosque!: number;

  comboCategorias: any[] = [];
  @Output() agregar = new EventEmitter();

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private manejoBosqueService: ManejoBosqueService,
    private user: UsuarioService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  openEliminarOrdenamientoAct(event: Event, index: number, data: any) {
    let params = {
      idManejoBosque: this.idManejoBosque,
      idUsuarioElimina: this.user.idUsuario,
      idManejoBosqueDet: data.idManejoBosqueDet,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet > 0) {
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.ordenamientoActList.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.ordenamientoActList.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }
  agregarRegistros() {
    this.agregar.emit(this.ordenamientoActList);
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
