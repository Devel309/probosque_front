import { HttpErrorResponse } from "@angular/common/http";
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import {
  ArchivoService,
  OrdenamientoInternoPmfiService,
  UsuarioService,
} from "@services";
import {
  DownloadFile,
  OgcGeometryType,
  PGMFArchivoTipo,
  ToastService,
} from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { concatMap, finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  CodigosManejoBosque,
  ManejoBosqueCabecera,
  ManejoBosqueDetalleModel,
} from "src/app/model/ManejoBosque";
import { ParametroModel } from "src/app/model/Parametro";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { GenericoService } from "src/app/service/generico.service";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";
import { PlanificacionService } from "src/app/service/planificacion.service";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { MapApi } from "src/app/shared/mapApi";
import { FileModel } from "src/app/model/util/File";
import { from } from "rxjs";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { CustomCapaModel } from "src/app/model/util/CustomCapa";
import { PlanManejoGeometriaModel } from "src/app/model/PlanManejo/PlanManejoGeometria";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import {
  lstactividadDet,
  lstactividadDetSub,
} from "src/app/model/ActividadesAprovechamientoPOCCModel";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ModalFormularioReforestacionComponent } from "./components/modal-formulario-reforestacion/modal-formulario-reforestacion.component";
import { element } from "protractor";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { Router } from "@angular/router";

declare const shp: any;
@Component({
  selector: "app-tab-manejo-bosque",
  templateUrl: "./tab-manejo-bosque.component.html",
  styleUrls: ["./tab-manejo-bosque.component.scss"],
})
export class TabManejoBosqueComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Input() isPerfilArffs!: boolean;
  isDisabledInput: boolean = false;
  listCoordenadas: any[] = [];

  ref!: DynamicDialogRef;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_7;

  codigoAcordeon71: string = CodigosTabEvaluacion.PGMFA_TAB_7_71;
  codigoAcordeon711: string = CodigosTabEvaluacion.PGMFA_TAB_7_711;
  codigoAcordeon712: string = CodigosTabEvaluacion.PGMFA_TAB_7_712;
  codigoAcordeon721: string = CodigosTabEvaluacion.PGMFA_TAB_7_721;
  codigoAcordeon721_CA: string = CodigosTabEvaluacion.PGMFA_TAB_7_721 + "CA";
  codigoAcordeon721_CP: string = CodigosTabEvaluacion.PGMFA_TAB_7_721 + "CP";
  codigoAcordeon722: string = CodigosTabEvaluacion.PGMFA_TAB_7_722;
  codigoAcordeon723: string = CodigosTabEvaluacion.PGMFA_TAB_7_723;
  codigoAcordeon73: string = CodigosTabEvaluacion.PGMFA_TAB_7_73;
  codigoAcordeon74: string = CodigosTabEvaluacion.PGMFA_TAB_7_74;

  evaluacion71: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon71,
  });
  evaluacion711: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon711,
  });
  evaluacion712: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon712,
  });

  evaluacion721: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon721,
  });
  evaluacion722: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon722,
  });
  evaluacion723: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon723,
  });

  evaluacion73: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon73,
  });
  evaluacion74: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon74,
  });

  evaluacion: any;
  sinListado: boolean = false;
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @ViewChild("chkInComponenteCatastro", { static: true })
  private chkInComponenteCatastro!: ElementRef;
  public view: any = null;
  public geoJsonLayer: any = null;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHPProdForestal: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  _files: any = [];
  planManejo = {} as PlanManejoModel;
  usuario = {} as UsuarioModel;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();
  isEdit = true;

  checked1: number = 0;
  checked2: number = 0;
  checked3: number = 0;

  checked4: number = 0;
  checked5: number = 0;

  tratamientosList: any[] = [];

  reforestacionList: any[] = [];

  especiesProtegerList: any[] = [];

  filteredComoTipo: any[] = [];

  operacionesCortaList: any[] = [];
  displayModalCorta: boolean = false;
  editCorta: boolean = false;
  cortaArrastre: any = {};
  indexCorta: any = null;

  sistemaManejo: any = {};
  produccionForestal: any = {};
  produccionForestalVertices: any = {};

  comboListCategoriaAct: any[] = [];
  ordenamientoActList: any[] = [];

  ordenamientoProdForestal: any[] = [];

  metodoAprovechamiento = {} as any;

  comboInfraestructuraVial: any[] = [];
  manBosqueAproveCaminoList: any[] = [];

  comboOperaciones: any[] = [];
  nombrefile = "";
  contadorActividadSolvicultural: number = 0;

  codigoManejo: string = "";
  isEditedMap = false;
  listCoordinatesAnexo: any[] = []

  actividadesOrdenamientoObj: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  actividadesAprovechamientoObj: ManejoBosqueDetalleModel = new ManejoBosqueDetalleModel();
  listActividadesOrdenamiento: ManejoBosqueDetalleModel[] = [];
  listActOrdenamientoDetSistemaManejo: ManejoBosqueDetalleModel[] = [];
  listActOrdenamientoDetProdForestal: ManejoBosqueDetalleModel[] = [];
  listAprovechamientoMetAprov: ManejoBosqueDetalleModel[] = [];
  listAprovechamientoInfAprovTrans: ManejoBosqueDetalleModel[] = [];
  infraEstructuraAprovechamiento: any = {} as ManejoBosqueDetalleModel;
  savedListEspecies: ManejoBosqueDetalleModel[] = [];

  //----- ManejoBosqueDetalleModel --------//

  actividadesSegunOrdemaniento: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  tratamientosSilviculturales: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  aprovechamiento: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  especiesObj: ManejoBosqueCabecera = new ManejoBosqueCabecera();
  listaGeneral: ManejoBosqueCabecera[] = [];
  listDetalleOrdenamiento: any[] = [];
  listDetalleproduccion: any[] = [];
  listAprovechamiento: any[] = [];
  listTransporte: any[] = [];
  listCaminos: any[] = [];
  listActividades: any[] = [];
  listOperaciones: any[] = [];
  listEspecies: any[] = [];
  listDetalleproduccionVertices : any[] = [];

  listBloque1: any[] = [];
  listBloque2: any[] = [];
  listBloque3: any[] = [];

  listEspeciesAprovechar: any[] = [];

  listEspeciesFlora: any[] = [];
  listEspeciesFauna: any[] = [];

  idManejoBosqueAco1: number = 0;
  idManejoBosqueAco2: number = 0;
  idManejoBosqueAco3: number = 0;
  idManejoBosqueAco4: number = 0;

  CodigosManejoBosque = CodigosManejoBosque;

  metodoAprovechamientoCombo = [
    { value: 1, label: "Manual" },
    { value: 2, label: "Mecanizado" },
    { value: 3, label: "Mixto" },
  ];

  sismenaTransporte = [
    { value: 1, label: "Fluvial" },
    { value: 2, label: "Terrestre" },
    { value: 3, label: "Mixto (fluvial y terrestre)" },
  ];

  mediosTransporte = [
    { value: 1, label: "Flotación" },
    { value: 2, label: "Chatas" },
    { value: 3, label: "Camión" },
    { value: 4, label: "Otros" },
  ];

  listResultados: any[] = [];

  pcListEspeciesVolumen: any[] = [{}];
  totalListEspeciesVolumen = {
    totalsPc1: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc2: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc3: { numArbolesPC: 0, volumenPC: 0 },
    totalArboles: 0,
    totalVolumen: 0,
  };

  areaTotalPc: number = 0;

  listEspeciesResultados: any[] = [];

  listEspeciesVolumen: any = [];

  UrlFormatos = UrlFormatos;

  idUsuario!: number;
  idOrdenamientoProteccion!:number;

  constructor(
    private genericoService: GenericoService,
    private confirmationService: ConfirmationService,
    private manejoServ: ManejoBosqueService,
    private messageService: MessageService,
    private servicioGeoforestal: ApiGeoforestalService,
    private planificacionService: PlanificacionService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private manejoBosqueService: ManejoBosqueService,
    private ordenamientoInternoPmfiService: OrdenamientoInternoPmfiService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    public dialogService: DialogService,
    private serviceExternos: ApiForestalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.planManejo.idPlanManejo = this.idPlanManejo;
    this.usuario = this.usuarioServ.usuario;
    this.idUsuario  = this.usuarioServ.idUsuario;
    this.obtenerListadoInfraestructuraVial();
    this.cargarOperaciones();
    this.initializeMap();
    this.obtenerCapas();
    this.crearCabeceras();
    this.sincronizacionCenso();

    this.listActividadesOrd();

    this.listadoActividadesPrincipales();
    this.listadoTratamientoSilviculturales();
    this.listadoTratamientoSilviculturalesB();

    if (this.isPerfilArffs) {
      this.isDisabledInput = true;
      this.obtenerEvaluacion();
    }
  }
  longitudCaminoAcceso(longitud: any) {
    this.infraEstructuraAprovechamiento.nuCaminoAcceso = longitud;
  }
  longitudCaminoPrincipal(longitud: any) {
    this.infraEstructuraAprovechamiento.nuCaminoPrincipal = longitud;
  }

  sincronizacionCenso() {
    // this.listEspeciesAprovechar = [];
    this.listBloque1 = [];
    this.listBloque2 = [];
    this.listBloque3 = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
    };
    this.manejoBosqueService
      .sincronizacionCenso(params)
      .subscribe((response: any) => {
        if (!!response.data) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              element.listCensoForestalDetalle.forEach((item: any) => {
                let obj = new ManejoBosqueDetalleModel(item);
                obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2_3;
                obj.especie = item.nombreCientifico;
                //this.listEspeciesAprovechar.push(obj);

                if (item.bloque == 1) {
                  let obj = new ManejoBosqueDetalleModel(item);
                  obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2_4;
                  obj.subCodtipoManejoDet = "BLOQUE1";
                  obj.especie = item.nombreCientifico;
                  this.listBloque1.push(obj);
                } else if (item.bloque == 2) {
                  let obj = new ManejoBosqueDetalleModel(item);
                  obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2_4;
                  obj.subCodtipoManejoDet = "BLOQUE2";
                  obj.especie = item.nombreCientifico;
                  this.listBloque2.push(obj);
                } else if (item.bloque == 3) {
                  let obj = new ManejoBosqueDetalleModel(item);
                  obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2_4;
                  obj.subCodtipoManejoDet = "BLOQUE3";
                  obj.especie = item.nombreCientifico;
                  this.listBloque3.push(obj);
                }
              });
            });
          }
        }
      });
  }

  crearCabeceras() {
    // 7.1
    this.actividadesSegunOrdemaniento = new ManejoBosqueCabecera();
    this.actividadesSegunOrdemaniento.idPlanManejo = this.idPlanManejo;
    this.actividadesSegunOrdemaniento.idUsuarioRegistro = this.usuarioServ.idUsuario;
    this.actividadesSegunOrdemaniento.codigoManejo =
      CodigosManejoBosque.CODIGO_PROCESO;
    this.actividadesSegunOrdemaniento.subCodigoManejo =
      CodigosManejoBosque.ACORDEON_7_1;

    //7.2
    this.aprovechamiento = new ManejoBosqueCabecera();
    this.aprovechamiento.idPlanManejo = this.idPlanManejo;
    this.aprovechamiento.idUsuarioRegistro = this.usuarioServ.idUsuario;
    this.aprovechamiento.subCodigoManejo = CodigosManejoBosque.ACORDEON_7_2;
    this.aprovechamiento.codigoManejo = CodigosManejoBosque.CODIGO_PROCESO;

    //7.3
    this.especiesObj = new ManejoBosqueCabecera();
    this.especiesObj.idPlanManejo = this.idPlanManejo;
    this.especiesObj.idUsuarioRegistro = this.usuarioServ.idUsuario;
    this.especiesObj.subCodigoManejo = CodigosManejoBosque.ACORDEON_7_3;
    this.especiesObj.codigoManejo = CodigosManejoBosque.CODIGO_PROCESO;

    // 7.4
    this.tratamientosSilviculturales = new ManejoBosqueCabecera();
    this.tratamientosSilviculturales.idPlanManejo = this.idPlanManejo;
    this.tratamientosSilviculturales.idUsuarioRegistro = this.usuarioServ.idUsuario;
    this.tratamientosSilviculturales.subCodigoManejo =
      CodigosManejoBosque.ACORDEON_7_4;
    this.tratamientosSilviculturales.codigoManejo =
      CodigosManejoBosque.CODIGO_PROCESO;
  }

  listActividadesOrd() {
    this.listCoordinatesAnexo = [];
    this.listDetalleproduccionVertices = [];
    this.ordenamientoActList = [];
    this.listDetalleOrdenamiento = [];
    this.listDetalleproduccion = [];
    this.listAprovechamiento = [];
    this.listTransporte = [];
    this.listCaminos = [];
    this.listActividades = [];
    this.listOperaciones = [];
    this.listEspecies = [];
    this.listEspeciesAprovechar = [];
    this.listEspeciesFlora = [];
    this.listEspeciesFauna = [];
    this.listBloque1 = [];
    this.listBloque2 = [];
    this.listBloque3 = [];
    this.operacionesCortaList = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: "PGMFA",
    };
    this.manejoBosqueService
      .listarManejoBosqueDetalle(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            if (element.idPlanManejo != null) {
              if (element.subCodigoManejo == CodigosManejoBosque.ACORDEON_7_1) {
                this.actividadesSegunOrdemaniento = new ManejoBosqueCabecera(
                  element
                );
                this.idManejoBosqueAco1 = element.idManejoBosque;
                element.listManejoBosqueDetalle.forEach((item: any) => {
                  if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_1
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.ordenamientoActList.push(obj);
                  } else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_1_1
                  ) {
                    this.sistemaManejo = new ManejoBosqueDetalleModel(item);

                    this.listDetalleOrdenamiento.push(this.sistemaManejo);
                  } else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_1_2
                  ) {
                    this.produccionForestal = new ManejoBosqueDetalleModel(
                      item
                    );
                    this.listDetalleproduccion.push(this.produccionForestal);

                  }  else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_1_2_0
                  ) {
                    this.produccionForestalVertices = new ManejoBosqueDetalleModel(
                      item
                    );
                    this.listDetalleproduccionVertices.push(this.produccionForestalVertices);
                  } else if (
                    item.codtipoManejoDet ==
                    CodigosManejoBosque.ACORDEON_7_1_2_1
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listEspeciesAprovechar.push(obj);
                  } else if (
                    item.codtipoManejoDet ==
                    CodigosManejoBosque.ACORDEON_7_1_2_2
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listBloque1.push(obj);
                  } else if (
                    item.codtipoManejoDet ==
                    CodigosManejoBosque.ACORDEON_7_1_2_3
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listBloque2.push(obj);
                  } else if (
                    item.codtipoManejoDet ==
                    CodigosManejoBosque.ACORDEON_7_1_2_4
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listBloque3.push(obj);
                  }
                });

                if (this.listEspeciesAprovechar.length > 0) {
                  this.datosSincronizacionMovilActDeAprovechamiento();
                }
              } else if (
                element.subCodigoManejo == CodigosManejoBosque.ACORDEON_7_2
              ) {
                this.aprovechamiento = new ManejoBosqueCabecera(element);
                this.idManejoBosqueAco2 = element.idManejoBosque;
                element.listManejoBosqueDetalle.forEach((item: any) => {
                  if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_2_1
                  ) {
                    this.metodoAprovechamiento = new ManejoBosqueDetalleModel(
                      item
                    );
                    this.listAprovechamiento.push(this.metodoAprovechamiento);
                  } else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_2_2
                  ) {
                    this.infraEstructuraAprovechamiento = new ManejoBosqueDetalleModel(
                      item
                    );
                    this.listTransporte.push(
                      this.infraEstructuraAprovechamiento
                    );
                  } else if (
                    item.codtipoManejoDet ==
                    CodigosManejoBosque.ACORDEON_7_2_2_1
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listCaminos.push(obj);
                  } else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_2_3
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);

                    obj.nuCaminoPrincipal = item.tipoTransDestino;

                    this.operacionesCortaList.push(obj);
                  }
                });
              } else if (
                element.subCodigoManejo == CodigosManejoBosque.ACORDEON_7_3
              ) {
                this.especiesObj = new ManejoBosqueCabecera(element);
                this.idManejoBosqueAco3 = element.idManejoBosque;
                element.listManejoBosqueDetalle.forEach((item: any) => {
                  if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_3_1
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listEspeciesFlora.push(obj);
                  } else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_3_2
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.listEspeciesFauna.push(obj);
                  }
                });
              } else if (
                element.subCodigoManejo == CodigosManejoBosque.ACORDEON_7_4
              ) {
                this.tratamientosSilviculturales = new ManejoBosqueCabecera(
                  element
                );

                this.idManejoBosqueAco4 = element.idManejoBosque;
                element.listManejoBosqueDetalle.forEach((item: any) => {
                  if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_4_1
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.tratamientosList.push(obj);
                  } else if (
                    item.codtipoManejoDet == CodigosManejoBosque.ACORDEON_7_4_2
                  ) {
                    let obj = new ManejoBosqueDetalleModel(item);
                    this.reforestacionList.push(obj);
                  }
                });
              }
            }
          });
        } else {
          this.datosSincronizacionMovilActDeAprovechamiento();
        }
        this.listEspeciesFlora.forEach((item) => {
          this.listEspecies.push(item);
        });
        this.listEspeciesFauna.forEach((item) => {
          this.listEspecies.push(item);
        });
        this.operacionesCortaList.forEach((data) => {
          this.listOperaciones.push(data);
        });
        this.ordenamientoActList.forEach((item) => {
          this.listActividades.push(item);
        });
        this.listDetalleproduccionVertices.forEach(element => {
          var obj = new Vertices(element)
          this.listCoordinatesAnexo.push(obj)
        })
      });

  }

  registrarEliminarArchivo(eve: any) {
    if (eve == 0) {
      this.infraEstructuraAprovechamiento.nuCaminoAcceso = null;
    }
  }

  registrarEliminarArchivo2(eve: any) {
    if (eve == 0) {
      this.infraEstructuraAprovechamiento.nuCaminoPrincipal = null;
    }
  }

  listadoActividadesPrincipales() {
    this.ordenamientoActList = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: "PGMFA",
      subCodigoGeneral: CodigosManejoBosque.ACORDEON_7_1,
      subCodigoGeneralDet: "A",
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idPlanManejo == null) {
            element.listManejoBosqueDetalle.forEach((item: any) => {
              let obj = new ManejoBosqueDetalleModel(item);
              obj.ordenamiento = item.tratamientoSilvicultural;
              obj.idUsuarioRegistro = this.usuarioServ.idUsuario;
              obj.idManejoBosqueDet = 0;
              obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1;
              this.ordenamientoActList.push(obj);
            });
          }
        });
      });
  }

  listadoTratamientoSilviculturales() {
    this.tratamientosList = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: "PGMFA",
      subCodigoGeneral: CodigosManejoBosque.ACORDEON_7_4,
      subCodigoGeneralDet: "A",
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idPlanManejo == null) {
            element.listManejoBosqueDetalle.forEach((item: any) => {
              if (item.subCodtipoManejoDet == "A") {
                let obj = new ManejoBosqueDetalleModel(item);
                obj.idManejoBosqueDet = 0;
                obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_4_1;
                this.tratamientosList.push(obj);
              }
            });
          }
        });
      });
  }

  listadoTratamientoSilviculturalesB() {
    this.reforestacionList = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoGeneral: "PGMFA",
      subCodigoGeneral: CodigosManejoBosque.ACORDEON_7_4,
      subCodigoGeneralDet: "B",
    };
    this.manejoBosqueService
      .listarManejoBosque(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idPlanManejo == null) {
            element.listManejoBosqueDetalle.forEach((item: any) => {
              if (item.subCodtipoManejoDet == "B") {
                let obj = new ManejoBosqueDetalleModel(item);
                obj.idManejoBosqueDet = 0;
                obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_4_2;
                this.reforestacionList.push(obj);
              }
            });
          }
        });
      });
  }

  openModal = (data: any, tipo: any, index?: any) => {
    this.ref = this.dialogService.open(ModalFormularioReforestacionComponent, {
      header: "Editar Reforestación",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: { data: data, type: tipo },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.reforestacionList[index] = resp;
      }
    });
  };

  /* listaItem5() {
    this.ordenamientoActList = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: "PGMFA",
    };
    this.ordenamientoInternoPmfiService
      .listarOrdenamiento(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          element.listOrdenamientoProteccionDet.forEach((item: any) => {
            if (!!item.idOrdenamientoProteccionDet) {
              let obj = new ManejoBosqueDetalleModel();
              obj.ordenamiento = item.categoria;
              obj.idUsuarioRegistro = this.usuarioServ.idUsuario;
              obj.idManejoBosqueDet = 0;
              obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1;
              this.ordenamientoActList.push(obj);
            }
          });
        });
        if (this.ordenamientoActList.length != 0) {
          this.sinListado = false;
        } else {
          this.sinListado = true;
        }
      });
  } */

  //7.1
  agregar(event: any) {
    this.listActividades = [];
    event.forEach((element: any) => {
      this.listActividades.push(element);
    });
    this.agregarAcordeon1();
  }

  agregarDetalleOrdenamiento() {
    if (this.listDetalleOrdenamiento.length == 1) {
      //ya existe
      this.listDetalleOrdenamiento[0].codOpcion = this.sistemaManejo.codOpcion;
      this.listDetalleOrdenamiento[0].descripcionOrd = this.sistemaManejo.descripcionOrd;
    } else {
      this.listDetalleOrdenamiento = [];
      let obj = new ManejoBosqueDetalleModel();
      obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_1;
      obj.codOpcion = this.sistemaManejo.codOpcion;
      obj.descripcionOrd = this.sistemaManejo.descripcionOrd;
      this.listDetalleOrdenamiento.push(obj);
    }
    this.agregarAcordeon1();
  }

  agregarDetalleProteccion() {

    if (this.listDetalleproduccion.length == 1) {
      //ya existe
      this.listDetalleproduccion[0].codOpcion = this.produccionForestal.codOpcion;
      this.listDetalleproduccion[0].descripcionOrd = this.produccionForestal.descripcionOrd;
    } else {
      this.listDetalleproduccion = [];
      let obj = new ManejoBosqueDetalleModel();
      obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2;
      obj.codOpcion = this.produccionForestal.codOpcion;
      obj.descripcionOrd = this.produccionForestal.descripcionOrd;
      this.listDetalleproduccion.push(obj);
    }

    this.agregarAcordeon1();

    /*this.listDetalleproduccion = [];
    let obj = new ManejoBosqueDetalleModel();
    obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2;
    obj.codOpcion = this.produccionForestal.codOpcion;
    obj.descripcionOrd = this.produccionForestal.descripcionOrd;
    this.listDetalleproduccion.push(obj);
    this.agregarAcordeon1();*/

  }

  agregarEsp(event: any) {
    this.listEspeciesAprovechar = [];
    event.forEach((element: any) => {
      let obj = new ManejoBosqueDetalleModel(element);
      obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2_1;
      this.listEspeciesAprovechar.push(obj);
    });
    this.agregarAcordeon1();
  }

  agregarBloques() {
    this.agregarAcordeon1();
  }
  // 7.2

  agregarAprovechamiento() {
    this.listAprovechamiento = [];
    let obj = new ManejoBosqueDetalleModel();
    obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_2_1;
    obj.codOpcion = this.metodoAprovechamiento.codOpcion;
    obj.descripcionOrd = this.metodoAprovechamiento.descripcion;
    obj.idManejoBosqueDet = this.metodoAprovechamiento.idManejoBosqueDet;
    this.listAprovechamiento.push(obj);
  }

  agregarTrasporte() {
    this.listTransporte = [];
    let obj = new ManejoBosqueDetalleModel();
    obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_2_2;
    obj.idtipoTransMaderable = this.infraEstructuraAprovechamiento.idtipoTransMaderable;
    obj.idtipoTransDestino = this.infraEstructuraAprovechamiento.idtipoTransDestino;
    obj.tipoTransDestino = this.infraEstructuraAprovechamiento.tipoTransDestino;
    obj.nuCaminoAcceso = this.infraEstructuraAprovechamiento.nuCaminoAcceso;
    obj.nuCaminoPrincipal = this.infraEstructuraAprovechamiento.nuCaminoPrincipal;
    obj.descripcionCamino = this.infraEstructuraAprovechamiento.descripcionCamino;
    obj.idManejoBosqueDet = this.infraEstructuraAprovechamiento.idManejoBosqueDet;
    this.listTransporte.push(obj);
    this.agregarAprovechamiento();

    this.guardarAcordeon2();
  }

  agregarCaminos(event: any) {
    this.listCaminos = [];
    event.forEach((element: any) => {
      let obj = new ManejoBosqueDetalleModel(element);
      obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_2_2_1;
      this.listCaminos.push(obj);
    });
    this.guardarAcordeon2();
  }

  registrarOperaciones = () => {
    this.listOperaciones = [];
    this.operacionesCortaList.forEach((data) => {
      let obj = new ManejoBosqueDetalleModel(data);
      obj.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_2_3;
      obj.manoObra = data.manoObra;
      obj.maquina = data.maquina;
      obj.nuCaminoPrincipal = data.nuDiametro;
      obj.tipoTransDestino = data.tipoTransDestino;
      obj.descripcionOrd = data.descripcionOrd;
      obj.descripcionOtro = data.descripcionOtro;
      this.listOperaciones.push(obj);
    });
    this.guardarAcordeon2();
  };

  //7.3 ACORDEON_7_3
  agregarEspecies(event: any) {
    this.listEspecies = [];
    event.forEach((item: any) => {
      this.listEspecies.push(item);
    });
    this.guardarAcordeon3();
  }

  //7.4
  Agregar7_4() {
    this.tratamientosList;
    this.reforestacionList;
    this.guardarAcordeon4();
  }

  /* guardar() {
    this.actividadesSegunOrdemaniento.listManejoBosqueDetalle = [];
    this.aprovechamiento.listManejoBosqueDetalle = [];
    this.especiesObj.listManejoBosqueDetalle = [];
    this.tratamientosSilviculturales.listManejoBosqueDetalle = [];
    // 7.1
    this.listActividades.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listDetalleOrdenamiento.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listDetalleproduccion.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listEspeciesAprovechar.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listBloque1.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listBloque2.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listBloque3.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });

    //7.2
    this.listAprovechamiento.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    this.listTransporte.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    this.listCaminos.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    this.listOperaciones.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    //7.3
    this.listEspecies.forEach((item) => {
      this.especiesObj.listManejoBosqueDetalle.push(item);
    });
    //7.4
    this.tratamientosList.forEach((item) => {
      this.tratamientosSilviculturales.listManejoBosqueDetalle.push(item);
    });
    this.reforestacionList.forEach((item) => {
      this.tratamientosSilviculturales.listManejoBosqueDetalle.push(item);
    });

    this.listaGeneral.push(this.actividadesSegunOrdemaniento);
    var params = [
      this.actividadesSegunOrdemaniento,
      this.aprovechamiento,
      this.especiesObj,
      this.tratamientosSilviculturales,
    ];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listActividadesOrd();
        } else {
          this.toast.error(response?.message);
        }
      });
  } */

  agregarAcordeon1() {
    this.actividadesSegunOrdemaniento.listManejoBosqueDetalle = [];
    // 7.1

    if (this.listActividades.length == 0) {
      this.ordenamientoActList.forEach((item) => {
        this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
      });
    } else {
      this.listActividades.forEach((item) => {
        this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
      });
    }

    this.listDetalleOrdenamiento.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listDetalleproduccion.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });

    this.listDetalleproduccionVertices.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });


    this.listEspeciesAprovechar.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listBloque1.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listBloque2.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });
    this.listBloque3.forEach((item) => {
      this.actividadesSegunOrdemaniento.listManejoBosqueDetalle.push(item);
    });

    this.listaGeneral.push(this.actividadesSegunOrdemaniento);
    var params = [this.actividadesSegunOrdemaniento];
    

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listActividadesOrd();

          this.listadoActividadesPrincipales();
          this.listadoTratamientoSilviculturales();
          this.listadoTratamientoSilviculturalesB();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  guardarAcordeon2() {
    this.aprovechamiento.listManejoBosqueDetalle = [];

    //7.2
    this.listAprovechamiento.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    this.listTransporte.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    this.listCaminos.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });
    this.listOperaciones.forEach((item) => {
      this.aprovechamiento.listManejoBosqueDetalle.push(item);
    });

    this.listaGeneral.push(this.actividadesSegunOrdemaniento);
    var params = [this.aprovechamiento];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listActividadesOrd();

          this.listadoActividadesPrincipales();
          this.listadoTratamientoSilviculturales();
          this.listadoTratamientoSilviculturalesB();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  guardarAcordeon3() {
    this.especiesObj.listManejoBosqueDetalle = [];

    //7.3
    this.listEspecies.forEach((item) => {
      this.especiesObj.listManejoBosqueDetalle.push(item);
    });

    this.listaGeneral.push(this.actividadesSegunOrdemaniento);
    var params = [this.especiesObj];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listActividadesOrd();

          this.listadoActividadesPrincipales();
          this.listadoTratamientoSilviculturales();
          this.listadoTratamientoSilviculturalesB();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  guardarAcordeon4() {
    this.tratamientosSilviculturales.listManejoBosqueDetalle = [];

    //7.4
    this.tratamientosList.forEach((item) => {
      this.tratamientosSilviculturales.listManejoBosqueDetalle.push(item);
    });
    this.reforestacionList.forEach((item) => {
      this.tratamientosSilviculturales.listManejoBosqueDetalle.push(item);
    });

    this.listaGeneral.push(this.actividadesSegunOrdemaniento);
    var params = [this.tratamientosSilviculturales];

    this.manejoBosqueService
      .registrarManejoBosque(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listActividadesOrd();
          this.listadoActividadesPrincipales();
          this.listadoTratamientoSilviculturales();
          this.listadoTratamientoSilviculturalesB();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openEliminarTratamientos(event: Event, index: number, data: any) {
    let params = {
      idManejoBosque: this.idManejoBosqueAco4,
      idUsuarioElimina: this.usuarioServ.idUsuario,
      idManejoBosqueDet: data.idManejoBosqueDet,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet > 0) {
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.tratamientosList.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.tratamientosList.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  openEliminarReforestacion(event: Event, index: number, data: any) {
    let params = {
      idManejoBosque: this.idManejoBosqueAco4,
      idUsuarioElimina: this.usuarioServ.idUsuario,
      idManejoBosqueDet: data.idManejoBosqueDet,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet > 0) {
          this.manejoBosqueService
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);
                this.reforestacionList.splice(index, 1);
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.reforestacionList.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  obtenerCategoriasOrdenamiento() {
    const parametro = { prefijo: "CATORD" };

    this.genericoService
      .getParametroPorPrefijo(parametro)
      .subscribe((result: any) => {
        this.comboListCategoriaAct = result.data;
      });
  }

  obtenerListadoInfraestructuraVial() {
    const parametro = { prefijo: "TINFV" };

    this.genericoService
      .getParametroPorPrefijo(parametro)
      .subscribe((result: any) => {
        this.comboInfraestructuraVial = result.data;
      });
  }

  obtenerManejoAprovechamiento() {
    let params = {
      planManejo: this.planManejo,
      idTipo: "1",
    };

    this.manejoServ
      .obtenerManejoBosqueAprovechamiento(params)
      .subscribe((result: any) => {
        if (result.data) this.metodoAprovechamiento = result.data;
      });
  }

  listarTodo() {
    this.listActividadesOrd();
    this.listadoActividadesPrincipales();
    this.listadoTratamientoSilviculturales();
    this.listadoTratamientoSilviculturalesB();
  }

  filtersManejoBosque(result: any) {
    this.listActOrdenamientoDetSistemaManejo = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPO" && data.catOrdenamiento === "DETSM"
    );

    this.listActOrdenamientoDetProdForestal = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPO" &&
        data.catOrdenamiento === "DETPF" &&
        data.codOpcion == "DETESP"
    );

    const descripcion =
      this.listActOrdenamientoDetSistemaManejo.length &&
      this.listActOrdenamientoDetSistemaManejo[0]?.codOpcion !== "ANIO"
        ? this.listActOrdenamientoDetSistemaManejo[0]?.descripcionOrd
        : "";

    this.sistemaManejo = {
      ...result.data[0],
      check:
        this.listActOrdenamientoDetSistemaManejo.length &&
        this.listActOrdenamientoDetSistemaManejo[0]?.codOpcion !== "SPBRN"
          ? 1
          : 0,
      descripcion: descripcion,
    };

    const listActOrdenamientoDetPF = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPO" && data.catOrdenamiento === "DETPF"
    );

    const descripcionPF =
      listActOrdenamientoDetPF.length &&
      listActOrdenamientoDetPF[0]?.codOpcion !== "SPBRN"
        ? listActOrdenamientoDetPF[0]?.descripcionOrd
        : "";
    this.produccionForestal = {
      ...result.data[0],
      check:
        listActOrdenamientoDetPF.length &&
        listActOrdenamientoDetPF[0]?.codOpcion !== "ANIO"
          ? 1
          : 0,
      descripcion: descripcionPF,
    };

    this.listAprovechamientoMetAprov = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPROV" &&
        data.catOrdenamiento === "METAPROV"
    );

    this.metodoAprovechamiento.idTipoMetodoAprove =
      this.listAprovechamientoMetAprov.length &&
      this.listAprovechamientoMetAprov[0]?.idtipoMetodoAprov
        ? this.listAprovechamientoMetAprov[0]?.idtipoMetodoAprov.toString()
        : "1";

    this.listAprovechamientoInfAprovTrans = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPROV" && data.catOrdenamiento === "IFAPTRA"
    );

    this.manBosqueAproveCaminoList = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPROV" &&
        data.catOrdenamiento === "IFAPTRES"
    );

    this.operacionesCortaList = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPROV" &&
        data.catOrdenamiento === "OPECORARR"
    );

    this.tratamientosList = result.data.filter(
      (data: any) =>
        data.codigoManejo === "TRASILV" && data.catOrdenamiento === "TRASILVAP"
    );

    this.reforestacionList = result.data.filter(
      (data: any) =>
        data.codigoManejo === "TRASILV" && data.catOrdenamiento === "TRASILVRE"
    );

    const infraestructuraAprov: any[] = result.data.filter(
      (data: any) =>
        data.codigoManejo === "PGMFAAPROV" && data.catOrdenamiento === "IFAPTRA"
    );

    this.infraEstructuraAprovechamiento = infraestructuraAprov.length
      ? infraestructuraAprov[0]
      : ({} as ManejoBosqueDetalleModel);

    this.savedListEspecies = result.data.filter(
      (data: any) => data.codigoManejo === "ESPAPRO"
    );
  }

  registrarDetalleOrdenamiento() {
    if (!!this.listActOrdenamientoDetSistemaManejo.length) {
      const newObj: any = [];
      this.listActOrdenamientoDetSistemaManejo.forEach((data: any) => {
        let obj = new ManejoBosqueDetalleModel(data);
        obj.codOpcion = this.sistemaManejo.check === 0 ? "SPBRN" : "OTRO";
        obj.descripcionOrd =
          this.sistemaManejo.check == 0 ? "" : this.sistemaManejo.descripcion;

        newObj.push(obj);
      });

      this.listActOrdenamientoDetSistemaManejo = newObj;
    } else {
      let obj = new ManejoBosqueDetalleModel();
      obj.catOrdenamiento = "DETSM";
      obj.codOpcion = this.sistemaManejo.check == 0 ? "SPBRN" : "OTRO";
      obj.descripcionOrd =
        this.sistemaManejo.check === 0 ? "" : this.sistemaManejo.descripcion;
      this.listActOrdenamientoDetSistemaManejo.push(obj);
    }

    /*  params = [
      {
        idPlanManejo: this.idPlanManejo,
        idManejoBosque: this.sistemaManejo.idManejoBosque
          ? this.sistemaManejo.idManejoBosque
          : 0,
        tipo: "PGMFA",
        codigoManejo: "PGMFAAPO",
        descripcionCab: "",
        idUsuarioRegistro: this.usuario.idusuario,
        listManejoBosqueDetalle: this.listActOrdenamientoDetSistemaManejo,
      },
    ];

    this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
      if (result.success) {
        this.listarTodo();
        this.SuccessMensaje(result.message);
      } else this.ErrorMensaje(result.message);
    }); */
  }

  registrarManejoAprovechamiento() {
    let params = {};
    if (!this.metodoAprovechamiento.idTipoMetodoAprove) {
      this.ErrorMensaje("Selecciona Método Aprovechamiento");
      return;
    }
    const metAprov =
      this.metodoAprovechamiento.idTipoMetodoAprove == "1"
        ? "Manual"
        : this.metodoAprovechamiento.idTipoMetodoAprove == "2"
        ? "Mecanizado"
        : "Mixto";

    if (!!this.listAprovechamientoMetAprov.length) {
      const newObj: any = [];
      this.listAprovechamientoMetAprov.forEach((data: any) => {
        let obj = new ManejoBosqueDetalleModel(data);
        obj.catOrdenamiento = "METAPROV";
        obj.tipoMetodoAprov = metAprov;
        //  obj.idtipoMetodoAprov = this.metodoAprovechamiento.idTipoMetodoAprove;

        newObj.push(obj);
      });

      this.listActOrdenamientoDetSistemaManejo = newObj;
    } else {
      let obj = new ManejoBosqueDetalleModel();
      obj.catOrdenamiento = "METAPROV";
      obj.tipoMetodoAprov = metAprov;
      // obj.idtipoMetodoAprov = this.metodoAprovechamiento.idTipoMetodoAprove;
      this.listActOrdenamientoDetSistemaManejo.push(obj);
    }

    params = [
      {
        idPlanManejo: this.idPlanManejo,
        idManejoBosque: this.produccionForestal.idManejoBosque
          ? this.produccionForestal.idManejoBosque
          : 0,
        tipo: "PGMFA",
        codigoManejo: "PGMFAAPROV",
        descripcionCab: "",
        idUsuarioRegistro: this.usuario.idusuario,
        listManejoBosqueDetalle: this.listActOrdenamientoDetSistemaManejo,
      },
    ];

    this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
      if (result.success) {
        this.listarTodo();
        this.SuccessMensaje(result.message);
      } else this.ErrorMensaje(result.message);
    });
  }

  registrarManejoInfraestructura() {
    let params = {};
    if (!this.validarManejoInfraestructura()) return;

    const tipoTransMaderable =
      this.infraEstructuraAprovechamiento.idtipoTransMaderable == "1"
        ? "Fluvial"
        : this.metodoAprovechamiento.idTipoMetodoAprove == "2"
        ? "Terrestre"
        : "Mixto (fluvial y terrestre)";

    if (!!this.listAprovechamientoInfAprovTrans.length) {
      const newObj: any = [];
      const arrayObj = [this.listAprovechamientoInfAprovTrans];
      arrayObj.forEach((data: any) => {
        let obj = new ManejoBosqueDetalleModel(data);
        obj.idtipoTransMaderable = this.infraEstructuraAprovechamiento.idtipoTransMaderable;
        obj.tipoTransMaderable = tipoTransMaderable;
        obj.idtipoTransDestino = this.infraEstructuraAprovechamiento.idtipoTransDestino;
        obj.tipoTransDestino =
          this.infraEstructuraAprovechamiento.idtipoTransDestino === "4"
            ? this.infraEstructuraAprovechamiento.tipoTransDestino
            : "";

        obj.nuCaminoAcceso = this.infraEstructuraAprovechamiento.nuCaminoAcceso;
        obj.nuCaminoPrincipal = this.infraEstructuraAprovechamiento.nuCaminoPrincipal;
        obj.descripcionCamino = this.infraEstructuraAprovechamiento.descripcionCamino;

        newObj.push(obj);
      });
      this.listAprovechamientoInfAprovTrans = newObj;
    } else {
      let obj = new ManejoBosqueDetalleModel();
      obj.catOrdenamiento = "IFAPTRA";
      obj.idtipoTransMaderable = this.infraEstructuraAprovechamiento.idtipoTransMaderable;
      obj.tipoTransMaderable = tipoTransMaderable;
      obj.idtipoTransDestino = this.infraEstructuraAprovechamiento.idtipoTransDestino;
      obj.tipoTransDestino =
        this.infraEstructuraAprovechamiento.idtipoTransDestino === "4"
          ? this.infraEstructuraAprovechamiento.tipoTransDestino
          : "";
      obj.nuCaminoAcceso = this.infraEstructuraAprovechamiento.nuCaminoAcceso;
      obj.nuCaminoPrincipal = this.infraEstructuraAprovechamiento.nuCaminoPrincipal;
      obj.descripcionCamino = this.infraEstructuraAprovechamiento.descripcionCamino;
      this.listAprovechamientoInfAprovTrans.push(obj);
    }
    params = [
      {
        idPlanManejo: this.idPlanManejo,
        idManejoBosque: this.produccionForestal.idManejoBosque
          ? this.produccionForestal.idManejoBosque
          : 0,
        tipo: "PGMFA",
        codigoManejo: "PGMFAAPROV",
        descripcionCab: "",
        idUsuarioRegistro: this.usuario.idusuario,
        listManejoBosqueDetalle: this.listAprovechamientoInfAprovTrans,
      },
    ];

    this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
      if (result.success) {
        this.listarTodo();
        this.SuccessMensaje(result.message);
      } else this.ErrorMensaje(result.message);
    });
  }

  validarManejoInfraestructura(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.infraEstructuraAprovechamiento.idtipoTransMaderable) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe seleccionar Sistema de Transporte de Madera\n";
    }

    if (this.infraEstructuraAprovechamiento.idtipoTransMaderable == "4") {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar detalle Medio Transporte\n";
    }

    if (!this.infraEstructuraAprovechamiento.idtipoTransDestino) {
      validar = false;
      mensaje = mensaje +=
        "(*) Debe seleccionar Sistema de Transporte de Destino.\n";
    }

    if (!this.infraEstructuraAprovechamiento.nuCaminoAcceso) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar archivo Camino de Acceso.\n";
    }

    if (!this.infraEstructuraAprovechamiento.nuCaminoPrincipal) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar archivo Camino Principal.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  AgregarOtrosTratamientos() {
    this.tratamientosList.push({
      idManejoBosqueDet: 0,
      descripcionDetalle: "",
      descripcionOrd: "PGMFA",
      tipoTratamiento: "Otros(Especificar)",
      idTipoTratamiento: "8",
      accion: false,
      idTipo: "1",
      codtipoManejoDet: "PGMFATRASILT",
      subCodtipoManejoDet: "A",
      //idActividadSilviculturalDet: (this.tratamientosList.length + 1) * -1,
    });
  }

  AgregarOtrosReforestacion() {
    this.reforestacionList.push({
      valorPrimario: "Otros (especificar)",
      valorSecundario: 8,
      accion: true,
    });
  }

  abrirModalCorta() {
    this.displayModalCorta = true;
    this.editCorta = false;
    this.cortaArrastre = {} as ManejoBosqueDetalleModel;
  }

  registrarCorta() {
    if (!this.validarOperaciones()) return;

    if (this.editCorta) {
      this.editarCorta();
    } else {
      this.agregarCorta();
    }
  }

  editarCorta() {
    const newCortaArrastre = new ManejoBosqueDetalleModel(this.cortaArrastre);
    newCortaArrastre.descripcionOtro = this.cortaArrastre.descripcion;
    this.operacionesCortaList[this.indexCorta] = newCortaArrastre;

    this.displayModalCorta = false;
    this.indexCorta = null;
  }

  updateOperaciones(e: any) {
    console.log(e);
  }

  agregarCorta() {
    const detalle = this.comboOperaciones.filter(
      (data) => data.valorSecundario === this.cortaArrastre.tipoOperacion
    )[0];

    const newObj = new ManejoBosqueDetalleModel();
    newObj.idOperacion = this.cortaArrastre.tipoOperacion;
    newObj.descripcionOrd = detalle.valorPrimario;
    newObj.manoObra = this.cortaArrastre.manoObra;
    newObj.maquina = this.cortaArrastre.maquina;
    newObj.nuCaminoPrincipal = this.cortaArrastre.nuCaminoPrincipal;
    newObj.tipoTransDestino = this.cortaArrastre.tipoTransDestino;
    newObj.descripcionOtro = this.cortaArrastre.descripcion;
    newObj.descripcionOtros =
      this.cortaArrastre.tipoOperacion === "5"
        ? this.cortaArrastre.descripcionOtros
        : "";

    this.operacionesCortaList.push(newObj);
    this.displayModalCorta = false;
  }

  cerrarModalCorta() {
    this.displayModalCorta = true;
    this.displayModalCorta = false;
  }

  openEditarCorta(data: ManejoBosqueDetalleModel, index: number) {
    this.displayModalCorta = true;
    this.editCorta = true;
    this.indexCorta = index;

    this.cortaArrastre = { ...data };

    this.cortaArrastre.tipoOperacion = data.idOperacion?.toString();
    this.cortaArrastre.descripcion = data.descripcionOtro;

    if (this.cortaArrastre.tipoOperacion === "5")
      this.cortaArrastre.descripcionOtros = data.descripcionOtros;
  }

  openEliminarCorta(event: Event, data: any, index: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idManejoBosqueDet > 0) {
          let params = {
            idManejoBosque: this.idManejoBosqueAco2,
            idUsuarioElimina: this.usuario.idusuario,
            idManejoBosqueDet: data.idManejoBosqueDet,
          };

          this.manejoServ
            .eliminarManejoBosque(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.SuccessMensaje(result.message);

                this.listarTodo();
              } else {
                this.ErrorMensaje(result.message);
              }
            });
        } else {
          this.operacionesCortaList.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  registrarProduccionForestal() {
    let params = {};

    if (!!this.listActOrdenamientoDetProdForestal.length) {
      const newObj: any = [];
      this.listActOrdenamientoDetProdForestal.forEach((data: any) => {
        let obj = new ManejoBosqueDetalleModel(data);
        obj.codOpcion = this.produccionForestal.check === 0 ? "ANIO" : "OTRO";
        obj.descripcionOrd =
          this.produccionForestal.check == 0
            ? ""
            : this.produccionForestal.descripcion;

        newObj.push(obj);
      });

      this.listActOrdenamientoDetProdForestal = newObj;
    } else {
      let obj = new ManejoBosqueDetalleModel();
      obj.catOrdenamiento = "DETPF";
      obj.codOpcion = this.produccionForestal.check == 0 ? "ANIO" : "OTRO";
      obj.descripcionOrd =
        this.produccionForestal.check === 0
          ? ""
          : this.produccionForestal.descripcion;
      this.listActOrdenamientoDetProdForestal.push(obj);
    }
    params = [
      {
        idPlanManejo: this.idPlanManejo,
        idManejoBosque: this.produccionForestal.idManejoBosque
          ? this.produccionForestal.idManejoBosque
          : 0,
        tipo: "PGMFA",
        codigoManejo: "PGMFAAPO",
        descripcionCab: "",
        idUsuarioRegistro: this.usuario.idusuario,
        listManejoBosqueDetalle: this.listActOrdenamientoDetProdForestal,
      },
    ];

    this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
      if (result.success) {
        this.SuccessMensaje(result.message);
        this.listarTodo();
      } else this.ErrorMensaje(result.message);
    });
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: PGMFArchivoTipo.PGMFA,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioServ.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.planManejo.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: "Otros",
        codigoGeometria: null,
        codigoSeccion: CodigosManejoBosque.CODIGO_TAB,
        codigoSubSeccion: CodigosManejoBosque.ACORDEON_7_1_2,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  obtenerCapas() {
    let item = {
      idPlanManejo: this.planManejo.idPlanManejo,
      codigoSeccion: CodigosManejoBosque.CODIGO_TAB,
      codigoSubSeccion: CodigosManejoBosque.ACORDEON_7_1_2,
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);

              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = t.idArchivo;
              this._layer.idLayer = this.mapApi.Guid2.newGuid;
              this._layer.inServer = true;
              this._layer.service = false;
              this._layer.nombre = t.nombreCapa;
              this._layer.groupId = groupId;
              this._layer.color = t.colorCapa;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layers.push(this._layer);
              let geoJson = this.mapApi.getGeoJson(
                this._layer.idLayer,
                groupId,
                item
              );

              this.createLayer(geoJson);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  onChangeFileProdForestal(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let chkInComponenteCatastro = this.chkInComponenteCatastro.nativeElement;
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: chkInComponenteCatastro.checked,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFileProdForestal(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = "";
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }

  onGuardarLayer() {
    let fileUpload: any = [];
    this._filesSHPProdForestal.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.usuarioServ.idUsuario,
          codigo: "37",
          codigoTipoPGMF: PGMFArchivoTipo.PGMFA,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result) => {
          this.SuccessMensaje(result.message);
          this.cleanLayers();
          this.obtenerCapas();
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error.");
        }
      );
  }

  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          "application/octet-stream"
        );
      }
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      };
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: "¿Está seguro de eliminar este grupo de archivo?",
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHPPF",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHPProdForestal.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHPProdForestal.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);

                if (this._filesSHPProdForestal.length === 0) {
                  this.cleanLayers();
                }
                this.SuccessMensaje("El archivo se eliminó correctamente.");
              } else {
                this.ErrorMensaje("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.ErrorMensaje("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHPProdForestal.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHPProdForestal.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);

      if (this._filesSHPProdForestal.length === 0) {
        this.cleanLayers();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.usuarioServ.idUsuario
    );
  }
  processFileProdForestal(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayersProdForestal(data, config);
      if (config.validate === true) {
        this.validateOverlap(data[0].features[0].geometry.coordinates);
        this.obtenerTiposBosques(data[0].features[0].geometry.coordinates);
      }
    });
  }
  validateOverlap(coordinates: any) {
    let item = {
      geometria: {
        poligono: coordinates,
      },
    };

    this.serviceExternos
      .validarSuperposicionAprovechamiento(item)
      .subscribe((result: any) => {
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                this._layer = {} as CustomCapaModel;
                this._layer.codigo = 0;
                this._layer.idLayer = t.geoJson.idLayer;
                this._layer.inServer = true;
                this._layer.nombre = t.geoJson.title;
                this._layer.groupId = t.geoJson.groupId;
                this._layer.color = t.geoJson.color;
                this._layer.overlap = t.geoJson.overlap;
                this._layer.service = t.geoJson.service;
                this._layers.push(this._layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                this._layer = {} as CustomCapaModel;
                this._layer.codigo = 0;
                this._layer.idLayer = t.geoJson.idLayer;
                this._layer.inServer = true;
                this._layer.nombre = t.geoJson.title;
                this._layer.groupId = t.geoJson.groupId;
                this._layer.color = t.geoJson.color;
                this._layer.overlap = t.geoJson.overlap;
                this._layer.service = t.geoJson.service;
                this._layers.push(this._layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
      });
  }
  obtenerTiposBosques(coordinates: any) {
    let parametros = {
      idClasificacion: "",
      geometria: {
        poligono: coordinates,
      },
    };
    this.serviceExternos
      .identificarTipoBosque(parametros)
      .subscribe((result) => {
        if (result.dataService.data.capas.length > 0) {
          result.dataService.data.capas.forEach((t: any) => {
            if (t.geoJson !== null) {
              t.geoJson.crs.type = "name";
              t.geoJson.opacity = 0.4;
              t.geoJson.groupId = this._id;
              t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
              t.geoJson.color = this.mapApi.random();
              t.geoJson.title = t.nombreCapa;
              t.geoJson.service = true;
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = 0;
              this._layer.idLayer = t.geoJson.idLayer;
              this._layer.nombre = t.geoJson.title;
              this._layer.groupId = t.geoJson.groupId;
              this._layer.color = t.geoJson.color;
              this._layer.service = t.geoJson.service;
              this._layers.push(this._layer);
              this.createLayer(t.geoJson);
            }
          });
        }
      });
  }
  createLayersProdForestal(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    this.file.idGroupLayer = config.idGroupLayer;
    this._filesSHPProdForestal.push(this.file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  getFilesAprovechamiento() {}

  onFileChangeReforestacion(event: any) {}

  cargarOperaciones = () => {
    const parametro = { prefijo: "TOPMANB" };
    this.genericoService
      .getParametroPorPrefijo(parametro)
      .subscribe((result: any) => {
        this.comboOperaciones = result.data;
      });
  };

  validarOperaciones = (): boolean => {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.cortaArrastre.tipoOperacion) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar una Operación.\n";
    }

    if (this.cortaArrastre.tipoOperacion != null) {
      if (
        this.cortaArrastre.tipoOperacion === "5" &&
        !this.cortaArrastre.descripcionOtros
      ) {
        validar = false;
        mensaje = mensaje += "(*) Debe registrar la Descripción de Otros.\n";
      }
    }

    if (!this.cortaArrastre.manoObra) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar la Mano de Obra.\n";
    }

    if (!this.cortaArrastre.maquina) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar el Equipo y Maquinaria.\n";
    }

    if (!this.cortaArrastre.descripcion) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar la Descripción.\n";
    }

    if (!this.editCorta) {
      if (
        this.operacionesCortaList.find(
          (x) => x.tipoOperacion === this.cortaArrastre.tipoOperacion
        )
      ) {
        validar = false;
        mensaje = mensaje +=
          "(*) No es factible registrar la misma operación de corta y arrastre, seleccione otra opción.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  };

  selectedIndexChange = (index: number) => {
    if (index === 2) this.listarManejoBosqueOperacion();
  };

  listarManejoBosqueOperacion = () => {
    let params = {
      idPlanManejo: this.planManejo.idPlanManejo,
    };
    // this.manejoServ
    //   .listarManejoBosqueOperacion(params)
    //   .subscribe((result: any) => {
    //     this.operacionesCortaList = result.data || [];
    //   });
  };

  eliminarManejoBosque = (data: any) => {
    data.idUsuarioElimina = this.usuario.idusuario;
    this.manejoServ.eliminarManejoBosque(data).subscribe((result: any) => {
      if (result.success) {
        this.listarTodo();
        this.SuccessMensaje(result.message);
        this.listarManejoBosqueOperacion();
      }
    });
  };

  onTabOpen = (event: any) => {
    if (event.index === 3) {
      this.contadorActividadSolvicultural = 0;
    }
  };

  guardarTratamientoSilvicultural = (tipo: number) => {
    let params = {};
    const listaOperaciones: any = [];
    let snOtro: any = 0;
    let snDesc: any = 0;

    this.tratamientosList.forEach((data: any) => {
      let obj = new ManejoBosqueDetalleModel(data);
      obj.tratamientoSilvicultural =
        tipo == 1 ? data.tratamientoSilvicultural : "";
      obj.descripcionTratamiento = data.descripcionTratamiento;
      obj.catOrdenamiento = "TRASILVAP";
      obj.Reforestacion = "";
      obj.accion = data.action ? 1 : 0;
      listaOperaciones.push(obj);
    });

    snOtro = this.tratamientosList.filter(
      (x: any) => !x.tratamientoSilvicultural
    );
    snDesc = this.tratamientosList.filter(
      (x: any) => !x.descripcionTratamiento
    );

    if (snDesc.length > 0) {
      this.messageService.add({
        severity: "warn",
        summary: "",
        detail:
          "(*) Debe ingresar: Descripción del(os) Tratamiento(s) Silvicultural(es) seleccionado(s).",
      });
    } else if (snOtro.length > 0) {
      this.messageService.add({
        severity: "warn",
        summary: "",
        detail:
          "(*) Debe ingresar: el nombre de tratamiento de uno de Otros(especificar).",
      });
    } else {
      params = [
        {
          idPlanManejo: this.idPlanManejo,
          idManejoBosque: 0,
          tipo: "PGMFA",
          codigoManejo: "TRASILV",
          descripcionCab: "",
          idUsuarioRegistro: this.usuario.idusuario,
          listManejoBosqueDetalle: listaOperaciones,
        },
      ];

      this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
        if (result.success) {
          this.listarTodo();
          this.SuccessMensaje(result.message);
        } else this.ErrorMensaje(result.message);
      });
    }
  };

  guardarReforestacion = () => {
    let params = {};
    const listaOperaciones: any = [];

    this.reforestacionList.forEach((data: any) => {
      let obj = new ManejoBosqueDetalleModel(data);
      obj.catOrdenamiento = "TRASILVRE";
      obj.Reforestacion = data.reforestacion;
      listaOperaciones.push(obj);
    });

    if (
      this.reforestacionList.filter((x: any) => x.Reforestacion != "")
        .length === 0
    ) {
      this.messageService.add({
        severity: "warn",
        summary: "",
        detail: "(*) Debe ingresar: La descripción de la Reforestación",
      });
    } else {
      params = [
        {
          idPlanManejo: this.idPlanManejo,
          idManejoBosque: 0,
          tipo: "PGMFA",
          codigoManejo: "TRASILV",
          descripcionCab: "",
          idUsuarioRegistro: this.usuario.idusuario,
          listManejoBosqueDetalle: listaOperaciones,
        },
      ];

      this.manejoServ.registrarManejoBosque(params).subscribe((result: any) => {
        if (result.success) {
          this.listarTodo();
          this.SuccessMensaje(result.message);
        } else this.ErrorMensaje(result.message);
      });
    }
  };

  eliminarActividadSilviculturalPgmf = (tipo: number, listaEliminar: any) => {
    let actividadEliminar: any = [];
    listaEliminar.forEach((element: any) => {
      actividadEliminar.push({
        idActividadSilviculturalDet: element.idActividadSilviculturalDet,
        idUsuarioElimina: this.usuario.idusuario,
      });
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .eliminarActividadSilviculturalPgmf(actividadEliminar)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            // this.listarActividadSilviculturalPgmf(tipo, true);
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion71 = Object.assign(
                this.evaluacion71,
                this.findEvaluacion(this.codigoAcordeon71)
              );
              this.evaluacion711 = Object.assign(
                this.evaluacion711,
                this.findEvaluacion(this.codigoAcordeon711)
              );
              this.evaluacion712 = Object.assign(
                this.evaluacion712,
                this.findEvaluacion(this.codigoAcordeon712)
              );
              this.evaluacion721 = Object.assign(
                this.evaluacion721,
                this.findEvaluacion(this.codigoAcordeon721)
              );
              this.evaluacion722 = Object.assign(
                this.evaluacion722,
                this.findEvaluacion(this.codigoAcordeon722)
              );
              this.evaluacion723 = Object.assign(
                this.evaluacion723,
                this.findEvaluacion(this.codigoAcordeon723)
              );
              this.evaluacion73 = Object.assign(
                this.evaluacion73,
                this.findEvaluacion(this.codigoAcordeon73)
              );
              this.evaluacion74 = Object.assign(
                this.evaluacion74,
                this.findEvaluacion(this.codigoAcordeon74)
              );
            }
          }
        }
      });
  }

  findEvaluacion(codigoAcorddeon: any) {
    return this.evaluacion.listarEvaluacionDetalle.find(
      (x: any) => x.codigoEvaluacionDetPost == codigoAcorddeon
    );
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacion71,
        this.evaluacion711,
        this.evaluacion712,
        this.evaluacion721,
        this.evaluacion722,
        this.evaluacion723,
        this.evaluacion73,
        this.evaluacion74,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion71);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion711);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion712);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion721);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion722);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion723);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion73);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion74);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  datosSincronizacionMovilActDeAprovechamiento() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: "PGMFA",
    };

    this.manejoServ
      .actividadesDeAprovechamiento(params)
      .subscribe((response: any) => {
        if (
          this.listEspeciesAprovechar.length == 0 &&
          !!response.data &&
          !!response.data.censoComercialDto.listaEspecieDtos
        ) {
          response.data.censoComercialDto.listaEspecieDtos.map((data: any) => {
            let obj = new ManejoBosqueDetalleModel(data);
            obj.nuIdEspecie = data.idCodigoEspecie;
            obj.especie = data.textNombreComun;
            obj.lineaProduccion = data.lineaProduccion;
            obj.nuDiametro = data.dmc;

            this.listEspeciesAprovechar.push(obj);
          });
        }

        if (
          this.listEspeciesVolumen.length == 0 &&
          !!response.data &&
          !!response.data.censoComercialDto.volumenCortaDto &&
          !!response.data.censoComercialDto.volumenCortaDto
            .resultadosVolumenDeCortaTablas
        ) {
          response.data.censoComercialDto.volumenCortaDto.resultadosVolumenDeCortaTablas.map(
            (data: any) => {
              const newArray: lstactividadDetSub[] = [];

              var params = new lstactividadDet(data);
              // params.codActvAproveDet =
              //   CodigosActividadesAprovechamiento.SUBACORDEON2_1;
              params.nombreCientifica = data.nombreCientifico;
              params.nombreComun = data.nombreComun;

              data.parcelaCorteDtos.map((data: any) => {
                var params = new lstactividadDetSub();
                params.nroArbol = data.numArbolesPC;
                params.volM = data.volumenPC;
                newArray.push(params);
              });

              params.lstactividadDetSub = newArray;
              params.volumen = data.volumenTotal;
              params.unidad = data.numArbolesTotales;
              params.totalIndividuos =
                response.data.censoComercialDto.volumenCortaDto.areaTotalPc;

              this.areaTotalPc =
                response.data.censoComercialDto.volumenCortaDto.areaTotalPc;
              this.listEspeciesVolumen.push(params);
            }
          );

          this.pcListEspeciesVolumen = this.listEspeciesVolumen[0].lstactividadDetSub;

          this.calcularTotalVolumenCorta();
        }
      });
  }

  calcularTotalVolumenCorta() {
    this.listEspeciesVolumen.map((response: any) => {
      if (response.lstactividadDetSub.length >= 1) {
        if (!!response.lstactividadDetSub[0].volM) {
          this.totalListEspeciesVolumen.totalsPc1.volumenPC += parseFloat(
            response.lstactividadDetSub[0].volM
          );
        }

        if (!!response.lstactividadDetSub[0].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc1.numArbolesPC += parseFloat(
            response.lstactividadDetSub[0].nroArbol
          );
        }
      }

      if (response.lstactividadDetSub.length >= 2) {
        if (!!response.lstactividadDetSub[1].volM) {
          this.totalListEspeciesVolumen.totalsPc2.volumenPC += parseFloat(
            response.lstactividadDetSub[1].volM
          );
        }

        if (!!response.lstactividadDetSub[1].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc2.numArbolesPC += parseFloat(
            response.lstactividadDetSub[1].nroArbol
          );
        }
      }

      if (response.lstactividadDetSub.length == 3) {
        if (!!response.lstactividadDetSub[2].volM) {
          this.totalListEspeciesVolumen.totalsPc3.volumenPC += parseFloat(
            response.lstactividadDetSub[2].volM
          );
        }

        if (!!response.lstactividadDetSub[2].nroArbol) {
          this.totalListEspeciesVolumen.totalsPc3.numArbolesPC += parseFloat(
            response.lstactividadDetSub[2].nroArbol
          );
        }
      }

      this.totalListEspeciesVolumen.totalArboles += parseFloat(response.unidad);

      this.totalListEspeciesVolumen.totalVolumen += parseFloat(
        response.volumen
      );
    });
  }

  cargarFormato(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Hoja1",
          numeroFila: 2,
          numeroColumna: 1,
          codigoManejo: "PGMFA",
          subCodigoManejo: "PGMFAMBAPO",
          codtipoManejoDet: "PGMFAACPORESP",
          idPlanManejo: this.idPlanManejo,
          idManejoBosque: this.idManejoBosqueAco1 ? this.idManejoBosqueAco1 : 0,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.manejoBosqueService
          .registrarManejoBosqueExcel(
            t.file,
            item.nombreHoja,
            item.numeroFila,
            item.numeroColumna,
            item.codigoManejo,
            item.subCodigoManejo,
            item.codtipoManejoDet,
            item.idPlanManejo,
            item.idManejoBosque,
            item.idUsuarioRegistro
          )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe(
            (res: any) => {
              if (res.success == true) {
                this.toast.ok(res?.message);
                this.listActividadesOrd();
              } else {
                this.toast.error(res?.message);
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            }
          );
      });
    } else {
      this.toast.warn("Debe seleccionar archivo.");
    }
  }

  //7.2
  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    code: any,
    codigoSubSeccion: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      codigoSubTipoPGMF: code,
      withVertice: withVertice,
      codigoSubSeccion: codigoSubSeccion,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (config.codigoSubTipoPGMF === "PGMFAREA") {
        // this.calculateArea(data);
      }
    });
  }
  createLayers(layers: any, config: any) {
    
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.codigoSubTipoPGMF;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layer.annex = config.annex;
      this._layer.descripcion = config.codigoSubTipoPGMF;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    this.file.idGroupLayer = config.idGroupLayer;
    this.file.descripcion = config.codigoSubTipoPGMF;
    this._filesSHPProdForestal.push(this.file);
  }

  listaCoordenadas(event: any) {
    this.listCoordenadas = []
    this.listCoordenadas = event;
    this.listCoordenadas.forEach((res) => {
      let objCoordenadas = new ManejoBosqueDetalleModel(res);
      objCoordenadas.codtipoManejoDet = CodigosManejoBosque.ACORDEON_7_1_2_0;
      objCoordenadas.puntoVertice = res.vertice
      objCoordenadas.coordenadaEsteIni = res.este
      objCoordenadas.coordenadaNorteIni = res.norte
      this.listDetalleproduccionVertices.push(objCoordenadas);
    });
    this.agregarAcordeon1();
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export class Vertices {
  constructor(data?: any) {
    if (data) {
      this.este = data.coordenadaEsteIni ? data.coordenadaEsteIni : '';
      this.vertice = data.puntoVertice ? data.puntoVertice : '';
      this.norte = data.coordenadaNorteIni ? data.coordenadaNorteIni : '';
      this.referencia = data.referencia ? data.referencia : '';
    }
  }
  este: string = ''
  vertice: string = ''
  norte: string = ''
  referencia: string = ''
}
