import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ToastService } from "@shared";
import { InformacionGeneralService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/informacion-general.service";
import { ResumenEjecutivo } from "src/app/model/resumenEjecutivo";
import {LineamientoInnerModel} from '../../../../../model/Comun/LineamientoInnerModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {CodigosTabEvaluacion} from '../../../../../model/util/CodigosTabEvaluacion';
import {CodigoEstadoEvaluacion} from '../../../../../model/util/CodigoEstadoEvaluacion';
import {finalize} from 'rxjs/operators';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import * as moment from 'moment';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-duracion-rvision-plan",
  templateUrl: "./tab-duracion-rvision-plan.component.html",
  styleUrls: ["./tab-duracion-rvision-plan.component.scss"],
})
export class TabDuracionRvisionPlanComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Input() isPerfilArffs!: boolean;



  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_3;

  codigoAcordeonDuraPlan: string =CodigosTabEvaluacion.PGMFA_TAB_3_DURACION_PLAN
  evaluacion:any;
  evaluacionDuracionPlan : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeonDuraPlan
  });
  fechaInicial = "";
  fechaFinal = "";

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  minDate = moment(new Date()).format('YYYY-MM-DD');
  minDateFinal = moment(new Date()).add('days', 1).format('YYYY-MM-DD');

  isDisabled: Boolean = false;
  isDisabledBtn: Boolean = false;
  informacionGeneral: ResumenEjecutivo = new ResumenEjecutivo();

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private toast: ToastService,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.obtenerResumenEjecutivo();

    if(this.isPerfilArffs)
    this.obtenerEvaluacion();
  }

  obtenerResumenEjecutivo() {
    var params = {
      codigoProceso: "PGMFA",
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.isDisabled = false;
          this.isDisabledBtn = false;
          response.data.forEach((element: any) => {
            this.fechaFinal = element.fechaFinDema;
            this.fechaInicial = element.fechaInicioDema;
            this.informacionGeneral = new ResumenEjecutivo(element);
          });
        } else {
          this.isDisabled = true;
          this.isDisabledBtn = true;
        }
      });
  }

  handleAdmDateChange() {
    this.validarInformacion();
    let fechaIni = moment(this.fechaInicial);
    let fechaFin = moment(this.fechaFinal);
    if (fechaFin.diff(fechaIni, 'days') >= 0) {
      this.informacionGeneral.vigencia = fechaFin.diff(fechaIni, 'years');
      this.isDisabledBtn = false;
    } else {
      this.toast.error('Fecha Fin debe ser posterior o igual a Fecha Inicio');
      this.isDisabledBtn = true;
    }
  }

  validarInformacion() {
    let fechaActual = moment();
    let fechaIni = moment(this.fechaInicial);
    this.isDisabledBtn = false;
    if (!!this.fechaInicial && !!this.fechaFinal) {
      if (fechaIni.diff(fechaActual, 'days') < 0) {
        this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
        this.isDisabledBtn = true;
        return;
      }

      if (this.fechaFinal <= this.fechaInicial) {
        this.isDisabledBtn = true;
        this.toast.error('La Fecha Fin debe ser posterior a la Fecha Inicio');
        return;
      }
    } else if (fechaIni.diff(fechaActual, 'days') < 0) {
      this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
      this.isDisabledBtn = true;
      return;
    }
  }

  ActualizarDuracion() {
    this.validarInformacion();
    this.informacionGeneral.fechaInicioDema = new Date(this.fechaInicial);
    this.informacionGeneral.fechaFinDema = new Date(this.fechaFinal);

    var params = this.informacionGeneral;

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe(
        (result: any) => {
          this.obtenerResumenEjecutivo();
          this.dialog.closeAll();
          this.toast.ok('Se registró la duración y revisión del plan correctamente.');
          // this.messageService.add({
          //   severity: "success",
          //   summary: "",
          //   detail: result.message,
          // });
        },
        (error: any) => {
          this.dialog.closeAll();
          this.messageService.add({
            severity: "warn",
            summary: "",
            detail: error.message,
          });
        }
      );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }

    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
              this.evaluacionDuracionPlan = Object.assign(this.evaluacionDuracionPlan,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeonDuraPlan));
          }
        }
      }
    })
  }


  registrarEvaluacion() {

    if(EvaluacionUtils.validar([this.evaluacionDuracionPlan])){

        if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionDuracionPlan);
        //console.log("EVALUACION -> ",this.evaluacion);

        this.dialog.open(LoadingComponent, { disableClose: true });

        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })

      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }

  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }


}
