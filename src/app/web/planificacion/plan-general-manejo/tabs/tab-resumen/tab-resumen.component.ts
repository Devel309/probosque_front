import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service";
import { UsuarioService } from "@services";
import { FileModel } from "src/app/model/util/File";
import { MatDialog } from "@angular/material/dialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { descargarArchivo, isNullOrEmpty, ToastService } from "@shared";
import { ResumenService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import {MessageService} from 'primeng/api';
import {InformacionGeneralService} from '../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import {ButtonsCreateQuery} from '../../../../../features/state/buttons-create.query';
import {ButtonsCreateStore} from '../../../../../features/state/buttons-create.store';
import {NotificacionService} from '../../../../../service/notificacion';

@Component({
  selector: "app-tab-resumen",
  templateUrl: "./tab-resumen.component.html",
  styleUrls: ["./tab-resumen.component.scss"],
})
export class TabResumenComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  isTodosPlanesTerminado : boolean = true;
  files: FileModel[] = [];
  filePGMFA: FileModel = {} as FileModel;

  consolidadoPGMFA: any = null;

  pendientes: any[] = [];
  verPGMFA: boolean = false;
  verDescargaPGMFA: boolean = true;

  verEnviar: boolean = false;
  CodigoProceso = CodigoProceso;
  estado: string = "";
  firma: string = "";


  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFARE",
      label: "1. Resumen Ejecutivo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAOM",
      label: "2. Objetivos del Manejo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFADRP",
      label: "3.  Duración y Revisión del Plan",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAIBAM",
      label: "4. Información Básica del Área de Manejo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAOAM",
      label: "5. Ordenamiento del Área de Manejo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAPPRF",
      label: "6. Potencial de Producción del Recurso Forestal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAMB",
      label: "7. Manejo del Bosque",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAPB",
      label: "8. Protección del Bosque",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAM",
      label: "9. Monitoreo",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAPC",
      label: "10. Participación Comunal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAC",
      label: "11. Capacitación",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },

    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAODA",
      label: "12. Organización para el Desarrollo de la Actividad",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFARMF",
      label: "13. Rentabilidad del Manejo Forestal",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFACA",
      label: "14. Cronograma de Actividades",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAAC",
      label: "15. Aspectos Complementarios",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: "PGMFAA",
      label: "16. Anexos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    },

    /* {
      idPlanManejoEstado: 0,
      codigo: "CPMCARDOC",
      label: "17. Carga Envío de Documentos",
      codigoEstado: "CPMPEND",
      estado: "Pendiente",
      marcar: false,
    }, */
  ];

  longitudTUPA:number=0;
  disabledCodigoEstadoPlanBorrador:boolean=true;
  disabledCodigoEstadoPlanPresentado:boolean=true;
  disablePlanFormulado: boolean = true;
  enviarARFFS: boolean = false;
  disableAgregarTUPA: boolean = true;
  disableEnviarARFFS: boolean = true;
  isSubmittingARFFS$ = this.ARFFSQuery.selectSubmitting();
  isSubmittingPlanFormulado$ = this.planFormuladoQuery.selectSubmitting();

  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private user: UsuarioService,
    private dialog: MatDialog,
    private toast: ToastService,
    private resumenService: ResumenService,
    private evaluacionService: EvaluacionService,
    private ARFFSQuery: ButtonsCreateQuery,
    private planFormuladoStore: ButtonsCreateStore,
    private ARFFSStore: ButtonsCreateStore,
    private planFormuladoQuery: ButtonsCreateQuery,
    private informacionGeneralService: InformacionGeneralService,
    private notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.filePGMFA.inServer = false;
    this.filePGMFA.descripcion = "PDF";
    this.listarEstados();
    this.obtenerEstadoPlan();
  }


  registrarArchivoIdPF(event: any) {
    if (event) {
      this.verEnviar = true;
      this.disablePlanFormulado = false;
    }
  }

  registrarArchivoId(event: any) {
    if (event) {
      this.verEnviar = true;
      this.disablePlanFormulado = false;
    }
  }


  obtenerEstadoPlan(){
    const body={
      idPlanManejo:this.idPlanManejo
    }
    this.informacionGeneralService.listarInfGeneralResumido(body).subscribe((_res:any)=>{

      if(_res.data.length>0){
        const estado= _res.data[0];
        if(estado.codigoEstado == 'EPLMPRES' || estado.codigoEstado == 'EMDOBS' || estado.codigoEstado == 'EMDBOR') {
          this.disabledCodigoEstadoPlanPresentado=false;
          this.disabledCodigoEstadoPlanBorrador= true;
        }
        if(estado.codigoEstado == 'EPLMBOR'){
          this.disabledCodigoEstadoPlanPresentado=true;
          this.disabledCodigoEstadoPlanBorrador= false;
        }
      }
    })
  }

  listarEstados() {
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: this.CodigoProceso.PLAN_GENERAL, //
      idPlanManejoEstado: null,
    };
// CAMBIAR POR EL SERVICIO QUE CORRESPONDA
    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == "CPMPEND") {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == "CPMPEND") {
            this.isTodosPlanesTerminado = false;
            this.listProcesos[objIndex].estado = "Pendiente";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
          } else if (element.codigoEstado == "CPMTERM") {
            this.listProcesos[objIndex].estado = "Terminado";
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });
        this.pendiente();
      });
  }


  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPGMFA = true;
    } else if (this.pendientes.length == 0) {
      this.verPGMFA = false;
    }
  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }
    if (
      this.listProcesos.length ===
      this.listProcesos.filter((data) => data.marcar === true).length
    ) {
     // this.verPGMFA = false;
      this.isTodosPlanesTerminado = true;
    }else{
      this.isTodosPlanesTerminado = false;
    }
  };

  guardarEstados() {
    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: "PGMFA", //
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario"))
          .idusuario,
        observacion: "",
      };
      listaEstadosPlanManejo.push(obje);
    });
// CAMBIAR POR EL SERVICIO QUE CORRESPONDA
    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == "CPMPEND") {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();

          this.messageService.add({
            key: "tl",
            severity: "success",
            summary: "",
            detail: response.message,
          });
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: response.message,
          });
        }
      });
  }

  regresarTab() {
    this.regresar.emit();
  }

  generarPGMFA() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resumenService
      .consolidadoPGMFA(this.idPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.consolidadoPGMFA = res;
            this.verDescargaPGMFA = false;
          } else {
            this.toast.error("Ocurrió un error al realizar la operación");
          }
        },
        (err) => {
          this.toast.warn(
            "Para generar el consolidado del Plan General  de Manejo Forestal - PGMFA. Debe tener todos los items completados previamente."
          );
        }
      );
  }

  descargarPGMFA() {
    if (isNullOrEmpty(this.consolidadoPGMFA)) {
      this.toast.warn("Debe generar archivo consolidado");
      return;
    }
    descargarArchivo(this.consolidadoPGMFA);
  }

  /*registrarArchivoId(event: any) {
    if (event) {
      this.verEnviar = true;
    }
  }*/

  guardarBorrador() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMBOR",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  /*guardarPlanFormulado() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }*/

  guardarPlanFormulado() {
    this.planFormuladoStore.submit();
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.planFormuladoStore.submitSuccess();
          this.toast.ok("Esta solicitud ha sido Formulada.");
          this.enviarARFFS = true
          this.disablePlanFormulado = true;
          this.disableAgregarTUPA = false;
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }

  /*enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }*/


  enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EMDPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  /*actualizarPlanManejoEstado(param: any) {
    this.evaluacionService
      .actualizarPlanManejo(param)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
        } else {
          this.toast.error(response?.message);
        }
      });
  }*/


  actualizarPlanManejoEstado(param: any) {
    this.ARFFSStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok(response?.message);
          this.enviarARFFS = false
          this.disableEnviarARFFS = true;
          this.registrarNotificacion();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarNotificacion() {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: this.CodigoProceso.PLAN_GENERAL,
      numDocgestion: this.idPlanManejo,
      mensaje: 'Se informa que se ha registrado el plan '
        + this.CodigoProceso.PLAN_GENERAL
        + ' Nro. '
        + this.idPlanManejo
        + ' para evaluación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.user.idUsuario,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 10,
      url: '/planificacion/evaluacion/plan-general-manejo',
      idUsuarioRegistro: this.user.idUsuario
    }
    this.notificacionService.registrarNotificacion(params).subscribe()
  }




}
