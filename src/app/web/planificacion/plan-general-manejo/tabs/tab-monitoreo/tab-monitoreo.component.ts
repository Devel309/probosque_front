import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService } from "primeng/api";
import { HttpErrorResponse } from "@angular/common/http";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ObjetivosService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/monitoreo.service";
import { ToastService } from "@shared";
import { LineamientoInnerModel } from "../../../../../model/Comun/LineamientoInnerModel";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { finalize } from "rxjs/operators";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { MonitoreoService } from "src/app/service/planificacion/monitoreo.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-tab-monitoreo",
  templateUrl: "./tab-monitoreo.component.html",
  styleUrls: ["./tab-monitoreo.component.scss"],
  providers: [MessageService, ConfirmDialogModule],
})
export class TabMonitoreoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;

  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_9;

  codigoAcordeon91: string = CodigosTabEvaluacion.PGMFA_TAB_9_1;
  evaluacion91: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon91,
  });
  evaluacion: any;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  tituloModalMantenimiento: string = " ";
  edit: boolean = false;

  tipoAccion: string = "";
  context1: Model1 = new Model1();
  verModalMantenimiento: boolean = false;
  lstDetalle1: Model1[] = [];
  pendiente: boolean = false;
  editar: boolean = false;
  guardar: boolean = false;
  idMonitoreo: number = 0;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private monitoreoService: MonitoreoService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listarMonitoreo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarMonitoreo() {
    this.editar = false;
    this.guardar = false;
    this.lstDetalle1 = [];
    var params = {
      codigoMonitoreo: "PGMFA",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.monitoreoService
      .listarMonitoreoPOCC(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if(response.data.length != 0){
          if (response.data[0].idMonitoreo != null)  {
            this.editar = true;
            this.idMonitoreo = response.data.idMonitoreo;
            response.data.forEach((element: any) => {
              element.lstDetalle.forEach((item: any) => {
                this.context1 = new Model1(item);
                this.lstDetalle1.push(this.context1);
              });
            });
          }
        } else {
          this.guardar = true;
        }
      });
  }

  guardarMonitoreo() {
    var params = [
      {
        idMonitoreo: this.idMonitoreo,
        descripcion: null,
        idPlanManejo: this.idPlanManejo,
        codigoMonitoreo: "PGMFA",
        observacion: null,
        monitoreo: "",
        indicador: "",
        frecuencia: "",
        responsable: "",
        idUsuarioRegistro: this.user.idUsuario,
        lstDetalle: this.lstDetalle1,
      },
    ];

    this.monitoreoService
      .registrarMonitoreoPOCC(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró el monitoreo correctamente.");
          this.pendiente = false;
          this.listarMonitoreo();
        }
      });
  }

  editarMonitoreo() {
    var params = [{
      idMonitoreo: this.idMonitoreo,
      descripcion: null,
      idPlanManejo: this.idPlanManejo,
      codigoMonitoreo: "PGMFA",
      observacion: null,
      monitoreo: "",
      indicador: "",
      frecuencia: "",
      responsable: "",
      idUsuarioRegistro: this.user.idUsuario,
      lstDetalle: this.lstDetalle1,
    }];
    this.monitoreoService
      .actualizarMonitoreoPOCC(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se edito el monitoreo correctamente.");
          this.pendiente = false;
          this.listarMonitoreo();
        }
      });
  }

  abrirModal(tipo: string, rowIndex: number, data?: Model1) {
    this.tipoAccion = tipo;
    if (this.tipoAccion == "C") {
      this.tituloModalMantenimiento = "Registrar Monitoreo";
      this.context1 = new Model1();
      this.context1.codigoMonotoreoDet = "PGMFA";
      this.context1.idUsuarioModificacion = this.user.idUsuario;
      this.context1.idUsuarioRegistro = this.user.idUsuario;
    } else {
      this.tituloModalMantenimiento = "Editar Monitoreo";
      this.context1 = new Model1(data);
      this.context1.id = rowIndex;
    }
    this.verModalMantenimiento = true;
  }

  AgregarMonitoreo(context?: any) {
    if (!this.validarMonitoreo()) {
      return;
    }
    if (this.tipoAccion == "C") {
      this.lstDetalle1.push(this.context1);
      this.pendiente = true;
    }
    if (this.tipoAccion == "E") {
      this.context1 = new Model1(context);
      this.lstDetalle1[context.id] = context;
      this.pendiente = true;
    }
    this.verModalMantenimiento = false;
  }

  openEliminarMonitoreo(event: Event, index: number, monitoreo: Model1) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (monitoreo.idMonitoreoDetalle != 0) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          var params = {
            idMonitoreoDetalle: monitoreo.idMonitoreoDetalle,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.monitoreoService.EliminarDetalleMonitoreo(params).subscribe(
            (result: any) => {
              if (result.success == true) {
                this.toast.ok("Se eliminó el monitoreo correctamente.");
                this.pendiente = false;
                this.listarMonitoreo();
                this.dialog.closeAll();
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            }
          );
        } else {
          this.lstDetalle1.splice(index, 1);
        }
      },
    });
  }

  validarMonitoreo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.context1.actividad == null || this.context1.actividad == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Actividad.\n";
    }
    if (this.context1.descripcion == null || this.context1.descripcion == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Descripción del Monitoreo.\n";
    }
    if (this.context1.responsable == null || this.context1.responsable == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Responsable.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion91 = Object.assign(
                this.evaluacion91,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon91
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion91])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion91);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export class Model1 {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.actividad = data.actividad ? data.actividad : "";
      this.codigoMonotoreoDet = data.codigoMonotoreoDet
        ? data.codigoMonotoreoDet
        : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.idMonitoreoDetalle = data.idMonitoreoDetalle
        ? data.idMonitoreoDetalle
        : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.observacion = data.observacion ? data.observacion : "";
      this.responsable = data.responsable ? data.responsable : "";
      this.operacion = data.operacion ? data.operacion : "";
      this.subOperacion = data.subOperacion ? data.subOperacion : "";

      return;
    }
  }
  id: number = 0;
  actividad: string = "";
  codigoMonotoreoDet: string = "";
  descripcion: string = "";
  idMonitoreoDetalle: number = 0;
  idUsuarioModificacion: number = 0;
  idUsuarioRegistro: number = 0;
  observacion: string = "";
  responsable: string = "";
  operacion: string = "";
  subOperacion: string = "";
}
