import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { HttpErrorResponse } from "@angular/common/http";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { ParticipacionComunalService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/participacion-comunal.service";
import { ToastService } from "@shared";
import { UsuarioService } from "@services";
import {LineamientoInnerModel} from '../../../../../model/Comun/LineamientoInnerModel';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {CodigosTabEvaluacion} from '../../../../../model/util/CodigosTabEvaluacion';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {finalize} from 'rxjs/operators';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import { Router } from "@angular/router";
@Component({
  selector: "app-tab-participacion-comunal",
  templateUrl: "./tab-participacion-comunal.component.html",
  styleUrls: ["./tab-participacion-comunal.component.scss"],
})
export class TabParticipacionComunalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;

  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_10;

  codigoAcordeon101: string =CodigosTabEvaluacion.PGMFA_TAB_10_1
  evaluacion101 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon101
  });
  evaluacion:any;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  tituloModalMantenimiento: string = " ";
  edit: boolean = false;

  tipoAccion: string = "";
  participacion: ModelParticipacionComunal = new ModelParticipacionComunal();
  verModalMantenimiento: boolean = false;
  listParticipacion: ModelParticipacionComunal[] = [];
  pendiente: boolean = false;

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private participacionComunalService: ParticipacionComunalService,
    private toast: ToastService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {

    this.listarParticipacionComunal();

    if(this.isPerfilArffs)
    this.obtenerEvaluacion();
  }

  listarParticipacionComunal() {
    this.listParticipacion = [];
    var params = {
      idPartComunal: null,
      codTipoPartComunal: "PGMFA",
      idPlanManejo: this.idPlanManejo,
    };

    this.participacionComunalService
      .listarPorFiltroParticipacionComunal(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((item: any) => {
            this.participacion = new ModelParticipacionComunal(item);
            this.listParticipacion.push(this.participacion);
          });
        }
      });
  }

  guardarParticipacionComunal() {
    var params = this.listParticipacion;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService
      .registrarParticipacionComunal(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró la participación comunal correctamente.");
          this.pendiente = false;
          this.listarParticipacionComunal();
        }
        this.dialog.closeAll();
      });
  }

  abrirModal(tipo: string, rowIndex: number, data?: ModelParticipacionComunal) {
    this.tipoAccion = tipo;
    if (this.tipoAccion == "C") {
      this.tituloModalMantenimiento = "Registrar Participación Comunal";
      this.participacion = new ModelParticipacionComunal();
      this.participacion.codTipoPartComunal = "PGMFA";
      this.participacion.idUsuarioModificacion = this.user.idUsuario;
      this.participacion.idUsuarioRegistro = this.user.idUsuario;
      this.participacion.idPlanManejo = this.idPlanManejo;
    } else {
      this.tituloModalMantenimiento = "Editar Participación Comunal";
      this.participacion = new ModelParticipacionComunal(data);
      this.participacion.id = rowIndex;
    }
    this.verModalMantenimiento = true;
  }

  agregarParticipacion(context?: any) {
    if (!this.validarParticipacion()) {
      return;
    }
    if (this.tipoAccion == "C") {
      this.listParticipacion.push(this.participacion);
      this.pendiente = true;
    }
    if (this.tipoAccion == "E") {
      this.participacion = new ModelParticipacionComunal(context);
      this.listParticipacion[context.id] = context;
      this.pendiente = true;
    }
    this.verModalMantenimiento = false;
  }

  openEliminarParticipacion(
    event: Event,
    index: number,
    monitoreo: ModelParticipacionComunal
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (monitoreo.idPartComunal != 0) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          var params = {
            idPartComunal: monitoreo.idPartComunal,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.participacionComunalService
            .eliminarParticipacionComunal(params)
            .subscribe(
              (result: any) => {
                if (result.success == true) {
                  this.toast.ok("Se eliminó la Participación Comunal correctamente.");
                  this.listarParticipacionComunal();
                  this.pendiente = false;
                  this.dialog.closeAll();
                }
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.listParticipacion.splice(index, 1);
        }
      },
    });
  }

  validarParticipacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (
      this.participacion.actividad == null ||
      this.participacion.actividad == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Actividad.\n";
    }
    if (
      this.participacion.responsable == null ||
      this.participacion.responsable == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Responsable.\n";
    }
    if (
      this.participacion.seguimientoActividad == null ||
      this.participacion.seguimientoActividad == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Seguimiento.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion101 = Object.assign(this.evaluacion101,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon101));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion101])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion101);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }
    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export class ModelParticipacionComunal {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.actividad = data.actividad ? data.actividad : "";
      this.codTipoPartComunal = data.codTipoPartComunal
        ? data.codTipoPartComunal
        : "";
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idPartComunal = data.idPartComunal ? data.idPartComunal : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.seguimientoActividad = data.seguimientoActividad
        ? data.seguimientoActividad
        : "";
      this.responsable = data.responsable ? data.responsable : "";

      return;
    }
  }
  id: number = 0;
  actividad: string = "";
  codTipoPartComunal: string = "";
  idPlanManejo: number = 0;
  idPartComunal: number = 0;
  idUsuarioModificacion: number = 0;
  idUsuarioRegistro: number = 0;
  seguimientoActividad: string = "";
  responsable: string = "";
}
