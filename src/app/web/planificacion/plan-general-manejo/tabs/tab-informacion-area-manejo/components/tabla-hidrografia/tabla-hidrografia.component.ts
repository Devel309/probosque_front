import { Component, Input, OnInit } from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { Rios } from "src/app/model/medioTrasporte";
import { AccesibilidadVias } from "./models";
import { ToastService } from "@shared";
import { UsuarioService } from "@services";

@Component({
  selector: "tabla-hidrografia",
  templateUrl: "./tabla-hidrografia.component.html",
  styleUrls: ["./tabla-hidrografia.component.scss"],
})
export class TablaHidrografiaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!:boolean;

  lstAspectoFisicoHidrografia: AccesibilidadVias[] = [];
  cmbRios: Rios[] = [];
  tituloModalCuerposAgua: String = "";
  verModalCuerposAgua: boolean = false;
  tipoAccionCuerposAgua: String = "";
  cuerposAgua: AccesibilidadVias = new AccesibilidadVias();
  idInfBasicaCuerposAgua: number = 0;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private toast: ToastService,
    private user: UsuarioService
  ) {}

  ngOnInit(): void {
    this.listRios();
    this.listarInfBasicaCuerposAgua();
  }

  listRios() {
    var params = {
      id: 0,
      tipo: "RIO",
    };
    this.informacionAreaPmfiService
      .obtenerHidrografia(params)
      .subscribe((response: any) => {
        this.cmbRios = [...response.data];
      });
  }

  listarInfBasicaCuerposAgua() {
    this.lstAspectoFisicoHidrografia = [];
    var params = {
      idInfBasica: "PGMFA",
      idPlanManejo: this.idPlanManejo,
      codCabecera: "PGMFARIO",
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idInfBasicaDet != null) {
            this.idInfBasicaCuerposAgua = element.idInfBasica;
            this.lstAspectoFisicoHidrografia.push(
              new AccesibilidadVias(element)
            );
          }
        });
      });
  }

  abrirModalCuerposAgua(
    tipo: String,
    index: number,
    data?: AccesibilidadVias
  ): void {
    this.tipoAccionCuerposAgua = tipo;

    if (this.tipoAccionCuerposAgua == "C") {
      this.tituloModalCuerposAgua = "Registro";
      this.cuerposAgua = new AccesibilidadVias();
    } else {
      this.tituloModalCuerposAgua = "Modificar";
      this.cuerposAgua = new AccesibilidadVias(data);
      this.cuerposAgua.id = index;
    }
    this.verModalCuerposAgua = true;
  }

  registrar2(): void {
    if (this.cuerposAgua.idRios == undefined) {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "El campo Rios es obligatorio.",
      });
      return;
    }
    if (this.tipoAccionCuerposAgua == "C") {
      this.filtrarIdRio();
      this.cuerposAgua.idUsuarioRegistro = this.user.idUsuario;
      this.cuerposAgua.codInfBasicaDet = "PGMFARIO";
      this.lstAspectoFisicoHidrografia.push(this.cuerposAgua);
    } else if (this.tipoAccionCuerposAgua == "E") {
      this.filtrarIdRio();
      this.cuerposAgua.idUsuarioRegistro = this.user.idUsuario;
      this.cuerposAgua.codInfBasicaDet = "PGMFARIO";
      this.cuerposAgua = new AccesibilidadVias(this.cuerposAgua);
      this.lstAspectoFisicoHidrografia[this.cuerposAgua.id] = this.cuerposAgua;
    }
    this.verModalCuerposAgua = false;
  }

  filtrarIdRio() {
    let rio = this.cmbRios.find(
      (ele) => ele.idHidrografia == this.cuerposAgua.idRios
    );
    let nombreRio = rio?.descripcion;
    this.cuerposAgua.idRios = Number(rio?.idHidrografia);
    this.cuerposAgua.nombreRio = nombreRio;
  }

  openEliminarCuerposAgua(
    e: any,
    codTabla: String,
    index: number,
    data?: AccesibilidadVias
  ): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data?.idInfBasicaDet == 0) {
          if (codTabla == "PGMFARIO") {
            this.lstAspectoFisicoHidrografia.splice(index, 1);
          }
        } else {
          if (codTabla == "PGMFARIO") {
            let params = {
              idInfBasica: 0,
              idInfBasicaDet: data?.idInfBasicaDet,
              codInfBasicaDet: "PGMFARIO",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaPmfiService
              .eliminarInformacionBasica(params)
              .subscribe((response: any) => {
                if (response.success == true) {
                  this.toast.ok(response?.message);
                  this.listarInfBasicaCuerposAgua();
                } else {
                  this.toast.error(response?.message);
                }
              });
          }
        }
      },
      reject: () => {},
    });
  }

  registrarAspectosFisicos() {
    var params = [
      {
        idInfBasica: this.idInfBasicaCuerposAgua
          ? this.idInfBasicaCuerposAgua
          : 0,
        codInfBasica: "PGMFA",
        codNombreInfBasica: "Aspectos físicos (hidrografía y fisiografía)",
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: [...this.lstAspectoFisicoHidrografia],
      },
    ];

    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarInfBasicaCuerposAgua();
        } else {
          this.toast.error(response?.message);
        }
      });
  }
}
