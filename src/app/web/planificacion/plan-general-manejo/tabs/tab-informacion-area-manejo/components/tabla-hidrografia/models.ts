
export class SolicitudFaunaModel {
  constructor(data?: any) {
    if (data) {
      this.idFauna = data.idFauna ? data.idFauna : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.codigoTipo = data.codigoTipo ? data.codigoTipo : 'PMFI';
      this.tipoFauna = data.tipoFauna ? data.tipoFauna : "";
      this.nombre = data.nombre ? data.nombre : "";
      this.nombreCientifico = data.nombreCientifico ? data.nombreCientifico : "";
      this.familia = data.familia ? data.familia : "";
      this.estatus = data.estatus ? data.estatus : "V";
      this.estadoSolicitud = data.estadoSolicitud ? data.estadoSolicitud : "Solicitado";
      this.adjunto = data.adjunto ? data.adjunto : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro : 0;
      this.idArchivo = data.idArchivo ? data.idArchivo : 0;
    }
  }

  idFauna: number = 0;
  idPlanManejo: number = 0;
  codigoTipo: string = "";
  tipoFauna: string = "";
  nombre: string = "";
  nombreCientifico: string = "";
  familia: string = "";
  estatus: string = "";
  estadoSolicitud: string = "";
  adjunto: string = "";
  idUsuarioRegistro: number = 0;
  idArchivo: number = 0
}

export class AccesibilidadVias {
  constructor(data?: any) {
    if (data) {
      this.id = data.id ? data.id : 0;
      this.referencia = data.referencia ? data.referencia : "";
      this.distanciaKm = data.distanciaKm ? data.distanciaKm : 0;
      this.tiempo = data.tiempo ? data.tiempo : "";
      this.medioTransporte = data.medioTransporte ? data.medioTransporte : ""; //
      this.epoca = data.epoca ? data.epoca : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idInfBasicaDet = data.idInfBasicaDet ? data.idInfBasicaDet : 0;

      this.nombreRio = data.nombreRio ? data.nombreRio : "";
      this.nombreQuebrada = data.nombreQuebrada ? data.nombreQuebrada : "";
      this.nombreLaguna = data.nombreLaguna ? data.nombreLaguna : "";
      this.idRios = data.idRios ? data.idRios : 0;

      this.codInfBasicaDet = data.codInfBasicaDet ? data.codInfBasicaDet : "";
      this.codSubInfBasicaDet = data.codSubInfBasicaDet ? data.codSubInfBasicaDet : "";

      this.puntoVertice = data.puntoVertice ? data.puntoVertice : "";
      this.coordenadaEsteIni = data.coordenadaEsteIni
        ? data.coordenadaEsteIni
        : "";
      this.coordenadaNorteIni = data.coordenadaNorteIni
        ? data.coordenadaNorteIni
        : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.areaHa = data.areaHa ? data.areaHa : "";
      this.areaHaPorcentaje = data.areaHaPorcentaje
        ? data.areaHaPorcentaje
        : "";
      this.zonaVida = data.zonaVida ? data.zonaVida : "";
      this.idFauna = data.idFauna ? data.idFauna : "";
      this.idFlora = data.idFlora ? data.idFlora : "";
      this.nombreComun = data.nombreComun ? data.nombreComun : ""
      this.nombreCientifico = data.nombreCientifico ? data.nombreCientifico :""
      this.familia = data.familia ? data.familia : ''
    }
  }
  id: number = 0;
  idInfBasicaDet: number = 0;
  codInfBasicaDet: string = "";
  codSubInfBasicaDet: string = "";
  puntoVertice?: string | null;
  coordenadaEsteIni?: string | null;
  coordenadaNorteIni?: string | null;
  referencia: string = "";
  distanciaKm: number = 0;
  tiempo: string = "";
  medioTransporte?: string;
  idUsuarioRegistro: number = 0;
  epoca: string = "";
  idRios?: number;
  descripcion?: string | null;
  areaHa?: string | null;
  areaHaPorcentaje?: string | null;
  zonaVida?: string | null;
  idFauna?: string | null;
  idFlora?: string | null;
  nombreRio?: string | null;
  nombreLaguna?: string | null;
  nombreQuebrada?: string | null;
  nombreComun?: string | null;
  nombreCientifico?: string | null;
  familia?: string | null;
}

