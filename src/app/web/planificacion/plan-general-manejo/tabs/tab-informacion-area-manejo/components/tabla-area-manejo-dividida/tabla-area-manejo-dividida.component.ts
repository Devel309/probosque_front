import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { InformacionCoordenadaUtm } from 'src/app/model/InformacionAreaManejo';
import { InformacionBasicaDetalle } from 'src/app/model/InformacionAreaManejo';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { InformacionAreaManejoService } from 'src/app/service/informacion-area-manejo/informacion-area-manejo.service';
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { TabInformacionAreaManejoComponent } from '../../tab-informacion-area-manejo.component';
import { MapApi } from "src/app/shared/mapApi";
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'tabla-area-manejo-dividida',
  templateUrl: './tabla-area-manejo-dividida.component.html',
  styleUrls: ['./tabla-area-manejo-dividida.component.scss']
})
export class TablaAreaManejoDivididaComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!:boolean;


  constructor(private messageService: MessageService, private confirmationService: ConfirmationService,
    private informacionAreaManejoService: InformacionAreaManejoService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private parentAreManejo: TabInformacionAreaManejoComponent,
    private mapApi: MapApi,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,) { }

  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
    this.listarCoordenadas()
  }
  listarCoordenadas() {
    this.listCoordinatesAnexo= [];
    var params = {
      idInfBasica: "PGMFA",
      idPlanManejo: this.idPlanManejo,
      codCabecera: "PGMFAIBAMCUMD"
    };
    //this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        //this.dialog.closeAll()
        let newList: any = []
        response.data.forEach((t: any) => {
          if(t.codInfBasicaDet !== null){
            newList.push({
              vertice: t.puntoVertice,
              referencia: t.referencia,
              idInfBasica: t.idInfBasica,
              idInfBasicaDet: t.idInfBasicaDet,
              codInfBasicaDet: t.codInfBasicaDet,
              este: t.coordenadaEste,
              norte: t.coordenadaNorte,
              anexo: t.descripcion
  
            })
          }
        });
        this.listCoordinatesAnexo = newList.groupBy((t: any) => t.anexo);
      });
  }
  registrar() {
    if (!this.validarRegistro())
      return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listMap: InformacionBasicaDetalle[] = [];

    this.listCoordinatesAnexo.forEach((group: any) => {
      group.value.forEach((value: any) => {
        let coordenada = {} as InformacionBasicaDetalle;
        coordenada.idInfBasica = value.idInfBasica || 0,
          coordenada.descripcion = group.key;
        coordenada.idInfBasicaDet = value.idInfBasicaDet ? value.idInfBasicaDet : 0;
        coordenada.idUnidadManejo = this.idPlanManejo;
        coordenada.estado = "A";
        coordenada.idUsuarioRegistro = this.usuario.idusuario;
        coordenada.codInfBasicaDet = 'PGMFAIBAMCUMD';
        coordenada.codSubInfBasicaDet = "PGMFAIBAMCUMD",
          coordenada.coordenadaEsteIni = value.este;
        coordenada.coordenadaNorteIni = value.norte;
        coordenada.idUsuarioModificacion = null;
        coordenada.referencia = value.referencia;
        coordenada.puntoVertice = value.vertice;
        listMap.push(coordenada);
      });
    })
    let params = [
      {
        idInfBasica: this.parentAreManejo.idInfBasica ? this.parentAreManejo.idInfBasica : 0,
        codInfBasica: "PGMFA",
        codNombreInfBasica: "Coordenadas UTM",
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ]
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        this.dialog.closeAll()
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarCoordenadas();
        } else {
          this.toast.error(response?.message);
        }
      });
  }
  onChangeFileSHP(e: any) {
    this.parentAreManejo.onChangeFileSHP(e, true, "TPPUNTO",'PGMFAIBAMCUAMAMD');
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaConAnexo(data[0].features);
    });
    e.target.value = '';
  }
  createTablaConAnexo(items: any) {
    let listMap = items.map((t: any) => {
      return {
        anexo: t.properties.anexo,
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: "",
      }
    })

    this.listCoordinatesAnexo = listMap.groupBy((t: any) => t.anexo);
  }
  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Agregue archivo VERTICES PARCELAS DE CORTA ANEXO \n";
    }

    if (this.listCoordinatesAnexo.some((group: any) => group.value.some((value: any) => value.referencia == ""))) {
      validar = false;
      mensaje = mensaje += "(*) Todas las coordenadas deben llevar referencia \n";
    }

    if (!validar)
      this.ErrorMensaje(mensaje);

    return validar;

  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({ severity: 'error', summary: 'ERROR', detail: mensaje });
  }


}
