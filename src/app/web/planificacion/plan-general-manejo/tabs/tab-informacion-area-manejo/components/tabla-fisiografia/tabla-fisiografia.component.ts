import { Component, Input, OnInit } from '@angular/core';
import { PlanManejoModel } from '@models';
import { ConfirmationService, MessageService } from 'primeng/api';
import { InfoAspectofisicofisiografia } from 'src/app/model/InformacionAreaManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { InformacionAreaManejoService } from 'src/app/service/informacion-area-manejo/informacion-area-manejo.service';

@Component({
  selector: 'tabla-fisiografia',
  templateUrl: './tabla-fisiografia.component.html',
  styleUrls: ['./tabla-fisiografia.component.scss']
})
export class TablaFisiografiaComponent implements OnInit {

  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() listFisio: InfoAspectofisicofisiografia[] = [];
  @Input() disabled!: boolean;

  constructor(private messageService: MessageService, private confirmationService: ConfirmationService,
    private informacionAreaManejoService: InformacionAreaManejoService) { }

  ngOnInit(): void {
  }


  agregarFisio() {

    let fisio = {} as InfoAspectofisicofisiografia;

    fisio.idApectofisicofisiografia = 0;
    fisio.idTipounidadfisiografica = "5";
    fisio.accion = true;

    this.listFisio.push(fisio);
  }

  registrar() {
    if (!this.validarRegistro()) return;

    this.listFisio.forEach(fisio => {
      fisio.estado = "A";
      fisio.idPlanManejo = this.planManejo.idPlanManejo;
      fisio.idUsuarioRegistro = this.usuario.idusuario;
    })

    this.informacionAreaManejoService.registrarAspectofisicofisiografia(this.listFisio)
      .subscribe((result: any) => {
        this.SuccessMensaje(result.message);
        this.listFisio = result.data;
      })

  }

  eliminarFisioOtros(event: any, index: number, data: InfoAspectofisicofisiografia) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idApectofisicofisiografia) {
          let params = {
            idApectofisicofisiografia: data.idApectofisicofisiografia,
            idUsuarioElimina: this.usuario.idusuario
          }

          this.informacionAreaManejoService.eliminarAspectofisicofisiografia(params)
            .subscribe((result: any) => {
              this.SuccessMensaje(result.message);
              this.listFisio.splice(index, 1);

            })

        } else {
          this.listFisio.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }


  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.listFisio.some(fisio => !fisio.descripcion && fisio.idTipounidadfisiografica == "5")) {
      validar = false;
      mensaje = mensaje += "(*) Todos los registros deben llevar descripcion \n";
    }

    if (this.listFisio.some(fisio => fisio.accion && !fisio.area)) {
      validar = false;
      mensaje = mensaje += "(*) Todos los registros deben llevar area \n";
    }

    if (this.listFisio.some(fisio => fisio.accion && !fisio.especificacion)) {
      validar = false;
      mensaje = mensaje += "(*) Todos los registros deben llevar especificacion \n";
    }

    if (!validar)
      this.ErrorMensaje(mensaje);

    return validar;

  }


  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({ severity: 'error', summary: 'ERROR', detail: mensaje });
  }



}
