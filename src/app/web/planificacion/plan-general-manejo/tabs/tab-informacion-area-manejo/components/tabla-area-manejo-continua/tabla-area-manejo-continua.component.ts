import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { InformacionBasicaDetalle } from 'src/app/model/InformacionAreaManejo';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { InformacionAreaManejoService } from 'src/app/service/informacion-area-manejo/informacion-area-manejo.service';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { TabInformacionAreaManejoComponent } from '../../tab-informacion-area-manejo.component';
import { MapApi } from 'src/app/shared/mapApi';
import {
  ArchivoService,
  PlanificacionService,
  UsuarioService,
} from '@services';
import { ToastService } from '@shared';
import { data } from 'jquery';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'tabla-area-manejo-continua',
  templateUrl: './tabla-area-manejo-continua.component.html',
  styleUrls: ['./tabla-area-manejo-continua.component.scss'],
})
export class TablaAreaManejoContinuaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() idInfBasica!: number;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() usuario = {} as UsuarioModel;
  @Input() planManejo = {} as PlanManejoModel;
  @Input() idUnidadManejo: any = null;
  @Input() disabled!: boolean;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private informacionAreaManejoService: InformacionAreaManejoService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private parentAreManejo: TabInformacionAreaManejoComponent,
    private mapApi: MapApi,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.listarCoordenadas();
  }
  listarCoordenadas() {
    this.listCoordinatesAnexo = [];
    var params = {
      idInfBasica: 'PGMFA',
      idPlanManejo: this.idPlanManejo,
      codCabecera: 'PGMFAIBAMCUMC',
    };
    //this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        //this.dialog.closeAll()
        response.data.forEach((t: any) => {
          this.listCoordinatesAnexo.push({
            vertice: t.puntoVertice,
            referencia: t.referencia,
            idInfBasica: t.idInfBasica,
            idInfBasicaDet: t.idInfBasicaDet,
            codInfBasicaDet: t.codInfBasicaDet,
            este: t.coordenadaEste,
            norte: t.coordenadaNorte,
          });
        });
      });
  }
  registrar() {
    if (!this.validarRegistro()) return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listMap: InformacionBasicaDetalle[] = this.listCoordinatesAnexo.map(
      (coordenada) => {
        return {
          idInfBasica: coordenada.idInfBasica || 0,
          idInfBasicaDet: coordenada.idInfBasicaDet
            ? coordenada.idInfBasicaDet
            : 0,
          idUnidadManejo: this.idPlanManejo,
          estado: 'A',
          idUsuarioRegistro: this.usuario.idusuario,
          codInfBasicaDet: 'PGMFAIBAMCUMC',
          codSubInfBasicaDet: 'PGMFAIBAMCUMC',
          coordenadaEsteIni: coordenada.este,
          coordenadaNorteIni: coordenada.norte,
          idUsuarioModificacion: null,
          referencia: coordenada.referencia,
          puntoVertice: coordenada.vertice,
          descripcion: '',
        };
      }
    );
    let params = [
      {
        idInfBasica: this.parentAreManejo.idInfBasica
          ? this.parentAreManejo.idInfBasica
          : 0,
        codInfBasica: 'PGMFA',
        codNombreInfBasica: 'Coordenadas UTM',
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listMap,
      },
    ];
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarCoordenadas();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.listCoordinatesAnexo.length == 0) {
      validar = false;
      mensaje = mensaje += '(*) Agregue archivo VERTICES PGMFA \n';
    }

    if (this.listCoordinatesAnexo.some((cord: any) => cord.referencia == '')) {
      validar = false;
      mensaje = mensaje +=
        '(*) Todas las coordenadas deben llevar referencia \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  onChangeFileSHP(e: any) {
    this.parentAreManejo.onChangeFileSHP(e, false, 'TPPUNTO','PGMFAIBAMCUAMAMC');
    this.mapApi.processFileSHP(e.target.files[0]).then((data: any) => {
      this.createTablaAnexo(data[0].features);
    });
    e.target.value = '';
  }

  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo.push({
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: '',
      });
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: mensaje,
    });
  }
}
