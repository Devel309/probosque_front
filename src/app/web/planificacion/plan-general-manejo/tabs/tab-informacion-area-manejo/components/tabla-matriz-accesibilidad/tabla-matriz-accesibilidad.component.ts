import { HttpErrorResponse } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { InformacionAccesibilidadmanejomatriz, listAccesibilidadManejoMatrizDetalle } from 'src/app/model/InformacionAreaManejo';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { InformacionAreaManejoService } from 'src/app/service/informacion-area-manejo/informacion-area-manejo.service';

@Component({
  selector: 'tabla-matriz-accesibilidad',
  templateUrl: './tabla-matriz-accesibilidad.component.html',
  styleUrls: ['./tabla-matriz-accesibilidad.component.scss']
})
export class TablaMatrizAccesibilidadComponent implements OnInit {

  @Input() usuario = {} as UsuarioModel;
  @Input() idAcceManejo: any = null;
  @Input() accesibilidadList: InformacionAccesibilidadmanejomatriz[] = [];
  @Input() disabled!:boolean;


  accesibilidad = {} as InformacionAccesibilidadmanejomatriz;
  detalle: any[] = [];
  editAccesibilidad: boolean = false;
  displayAccesibilidad: boolean = false;
  indexEdit: any = null;


  constructor(private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private informacionAreaManejoService: InformacionAreaManejoService
  ) { }

  ngOnInit(): void {

  }


  abrirModalAccesibilidad() {
    this.accesibilidad = {} as InformacionAccesibilidadmanejomatriz;
    this.accesibilidad.listAccesibilidadManejoMatrizDetalle = [];
    this.detalle = [];
    this.displayAccesibilidad = true;
    this.indexEdit = null;
    this.editAccesibilidad = false;
  }

  cerrarModalAccesibilidad() {
    this.displayAccesibilidad = false;
  }

  eliminarDetalleAccesibilidad(event: any, index: any, data: listAccesibilidadManejoMatrizDetalle) {

    if (this.editAccesibilidad && this.detalle.length == 1) {
      this.ErrorMensaje("Por lo menos debe tener un registro")
      return;
    }

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar esta acción?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idAcceManejomatrizdetalle) {

          let params = {
            idAcceManejomatrizdetalle: data.idAcceManejomatrizdetalle,
            idUsuarioElimina: this.usuario.idusuario,
          }

          this.informacionAreaManejoService.eliminarAccesibilidadmanejomatrizdetalle(params).subscribe((result: any) => {
            this.SuccessMensaje(result.message);
            this.detalle.splice(index, 1);
            this.accesibilidadList[this.indexEdit].listAccesibilidadManejoMatrizDetalle.splice(index, 1);
          })

        } else {
          this.detalle.splice(index, 1);
          this.accesibilidad.listAccesibilidadManejoMatrizDetalle.splice(index, 1);
        }


      },
      reject: () => {
        //reject action
      },
    });

  }

  nuevoDetalleAccesibilidad() {
    if (!this.editAccesibilidad) {
      if (this.accesibilidadList.some(acc => acc.anexosector == this.accesibilidad.anexosector)) {
        this.ErrorMensaje("Sector ya registrado")
        return;
      }
    }

    let detalle: any = {}
    detalle.idAcceManejomatrizdetalle = 0;

    this.detalle.push(detalle);
  }

  registrarAccesibilidad() {

    this.accesibilidad.listAccesibilidadManejoMatrizDetalle = this.detalle.map((detalle: any) => {
      return {
        idAcceManejomatriz: detalle.idAcceManejomatriz ? detalle.idAcceManejomatriz : 0,
        idAcceManejomatrizdetalle: detalle.idAcceManejomatrizdetalle ? detalle.idAcceManejomatrizdetalle : 0,
        distancia: detalle.dist,
        referencia: detalle.ref,
        subparcela: detalle.sub,
        tiempo: detalle.tiem,
        tipovehiculo: detalle.vehiculo,
        via: detalle.vi,
        estado: "A",
      }
    })

    this.accesibilidadList.push(this.accesibilidad);
    this.displayAccesibilidad = false;
  }

  agregarAccesibilidad() {
    if (!this.validarRegistro()) return;

    if (this.editAccesibilidad) {
      this.editarAccesibilidad();
    } else {
      this.registrarAccesibilidad();
    }
  }

  editarModalAccesibilidad(index: number) {
    this.accesibilidad = {} as InformacionAccesibilidadmanejomatriz;
    this.accesibilidad.listAccesibilidadManejoMatrizDetalle = [];
    this.detalle = [];
    this.indexEdit = index;

    this.accesibilidad.anexosector = this.accesibilidadList[index].anexosector;
    this.detalle = this.accesibilidadList[index].listAccesibilidadManejoMatrizDetalle.map(detalle => {
      return {
        ...detalle,
        sub: detalle.subparcela,
        ref: detalle.referencia,
        vi: detalle.via,
        dist: detalle.distancia,
        tiem: detalle.tiempo,
        vehiculo: detalle.tipovehiculo,
      }
    })

    this.editAccesibilidad = true;
    this.displayAccesibilidad = true;

    

  }

  editarAccesibilidad() {

    this.accesibilidadList[this.indexEdit].anexosector = this.accesibilidad.anexosector;

    this.accesibilidadList[this.indexEdit].listAccesibilidadManejoMatrizDetalle = this.detalle.map((detalle: any) => {
      return {
        idAcceManejomatriz: detalle.idAcceManejomatriz ? detalle.idAcceManejomatriz : 0,
        idAcceManejomatrizdetalle: detalle.idAcceManejomatrizdetalle ? detalle.idAcceManejomatrizdetalle : 0,
        distancia: detalle.dist,
        referencia: detalle.ref,
        subparcela: detalle.sub,
        tiempo: detalle.tiem,
        tipovehiculo: detalle.vehiculo,
        via: detalle.vi,
        estado: "A",
      }
    })

    this.displayAccesibilidad = false;
    this.indexEdit = null;
  }


  eliminarAccesibilidad(event: any, index: number, data: InformacionAccesibilidadmanejomatriz) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idAcceManejomatriz) {
          let params = {
            idAcceManejomatriz: data.idAcceManejomatriz,
            idUsuarioElimina: this.usuario.idusuario
          }

          this.informacionAreaManejoService.eliminarAccesibilidadmanejomatriz(params).subscribe((result: any) => {
            this.SuccessMensaje(result.message);
            this.accesibilidadList.splice(index, 1);
          })

        } else {
          this.accesibilidadList.splice(index, 1);
        }

      },
      reject: () => {
        //reject action
      },
    });
  }

  registrar() {

    this.accesibilidadList.forEach((acce: InformacionAccesibilidadmanejomatriz) => {
      acce.estado = "A";
      acce.idUsuarioRegistro = this.usuario.idusuario;
      acce.idAcceManejo = this.idAcceManejo;
      acce.idAcceManejomatriz = acce.idAcceManejomatriz ? acce.idAcceManejomatriz : 0;
    })

    this.informacionAreaManejoService.registrarAccesibilidadmanejomatriz(this.accesibilidadList)
      .subscribe((result: any) => {
        this.SuccessMensaje(result.message);
        this.listarMatrizAccesibilidad();

      })
  }


  validarRegistro(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.accesibilidad.anexosector) {
      validar = false;
      mensaje = mensaje += "(*) Registre anexo o sector\n";
    }

    if (this.detalle.length == 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe registrar al menos un detalle accesibilidad\n";
    }

    if (this.detalle.some(detalle =>
      !detalle.ref || !detalle.sub || !detalle.tiem ||
      !detalle.dist || !detalle.vehiculo
      || !detalle.vi)) {
      validar = false;
      mensaje = mensaje += "(*) Todos los campos son obligatorios\n";
    }

    if (!validar)
      this.ErrorMensaje(mensaje);

    return validar;

  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({ severity: 'error', summary: 'ERROR', detail: mensaje });
  }


  listarMatrizAccesibilidad = () => {
    let params = {
      idAcceManejo: this.idAcceManejo,
    };


    this.informacionAreaManejoService
      .listarAccesibilidadmanejomatriz(params)
      .subscribe(
        (result: any) => {
          
          this.accesibilidadList = result.data || [];
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        }
      );
  };



}
