import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CoreCentralService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EspeciesFauna } from "src/app/model/medioTrasporte";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import {
  AccesibilidadVias,
  SolicitudFaunaModel,
} from "../tabla-hidrografia/models";
import { ModalFormEspeciesComponent } from "./modal-form-especies/modal-form-especies.component";
import {CodigosTabEvaluacion} from '../../../../../../../model/util/CodigosTabEvaluacion';
import {CodigoProceso} from '../../../../../../../model/util/CodigoProceso';
import {LazyLoadEvent} from 'primeng/api';
import {Page} from '@models';
import {InformacionBasicaModel} from '../../../../../../../model/PlanManejo/InformacionBasica/InformacionBasicaModel';
import {finalize} from 'rxjs/operators';

@Component({
  selector: "tabla-fauna-silvestre",
  templateUrl: "./tabla-fauna-silvestre.component.html",
  styleUrls: ["./tabla-fauna-silvestre.component.scss"],
})
export class TablaFaunaSilvestreComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!:boolean;

  objbuscar = {
    dato: '',
    pageNum: 1,
    pageSize: 10,
  };
  totalRecords: number = 0;

  comboListEspeciesFauna: EspeciesFauna[] = [];
  verModalFauna: boolean = false;
  selecEspeciesFauna!: AccesibilidadVias;
  selecFaunas: any[] = [];
  listGuardarFaunas: AccesibilidadVias[] = [];
  selectedValues: AccesibilidadVias[] = [];
  queryFauna: string = "";
  listaFauna: any[] = [];
  listAspectosBiologicos: SolicitudFaunaModel[] = [];
  ref!: DynamicDialogRef;
  indexEdit: number = 0;
  indexElim: number = 0;
  nombreArchivo: string = "";

  infoBasica_4_6:InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica :0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica : CodigoProceso.PLAN_GENERAL,
    codSubInfBasica : CodigosTabEvaluacion.PGMFA_TAB_4,
    codNombreInfBasica : CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS,
    idUsuarioRegistro : this.user.idUsuario
  });

  constructor(
    private coreCentralService: CoreCentralService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private anexosService: AnexosService
  ) {}

  ngOnInit(): void {
    this.listaPorFiltroEspecieFauna();
    this.obtenerFaunas();
  }

  // ---------- Aspectos Biológicos Fauna------------------- //

  loadData(e: any) {
    const pageSize = Number(e.rows);
    this.objbuscar.pageNum = Number(e.first) / pageSize + 1;
    this.objbuscar.pageSize = pageSize;
    this.listaPorFiltroEspecieFauna();
  }

  listaPorFiltroEspecieFauna() {
    let params = {
      idEspecie: null,
      nombreComun: null,
      nombreCientifico: null,
      autor: null,
      familia: null,
      pageNum: this.objbuscar.pageNum,
      pageSize: this.objbuscar.pageSize,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.coreCentralService
      .listaEspecieFaunaPorFiltro(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.totalRecords = result.totalrecord;
        this.comboListEspeciesFauna = [...result.data];
        this.listarInfBasicaFauna();
      });
  }

  listarInfBasicaFauna() {
    this.selectedValues = [];
    this.selecFaunas = [];
    var params = {
      idInfBasica: CodigoProceso.PLAN_GENERAL,
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS,
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if(response.data) {
          //this.toast.ok("Se listó los datos de Fauna Silvestre correctamente.");
          let list_InfoBasicDetalle4_6 = response.data.filter((x: any) => x.codInfBasicaDet = CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS);
          if (list_InfoBasicDetalle4_6 && list_InfoBasicDetalle4_6.length > 0) {
            let infoBasic4_6 = list_InfoBasicDetalle4_6[0];
            Object.assign(this.infoBasica_4_6, infoBasic4_6);
          }

          response.data.forEach((element: any) => {
            this.listaFauna.push(element);
            this.selecFaunas.push(element);
            this.selectedValues.push(new AccesibilidadVias(element));
            //this.filtrarFaunaPorId(element);
          });

        }
      });
  }

  load(e: LazyLoadEvent) {

  }

  filtrarFaunaPorId(element: any) {
    var selecFaunas: any;
    if (this.comboListEspeciesFauna.length != 0) {
      selecFaunas = this.comboListEspeciesFauna.filter((x) => x.idEspecie == element.idFauna);
      selecFaunas.forEach((element: any) => {
        this.selecFaunas.push(element);
        //this.selectedValues.push(element);
      });
    }
  }

  onChangeFauna(event: any, data: any) {
    this.listaFauna.forEach((item) => {
      if (event.checked === false && data.idEspecie === item.idFauna) {
        let params = {
          idInfBasica: 0,
          idInfBasicaDet: item.idInfBasicaDet,
          codInfBasicaDet: "PGMFAFAU",
          idUsuarioElimina: this.user.idUsuario,
        };
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((response: any) => {});
      }
    });
  }

  abrirModalEspecieFauna() {
    this.selecEspeciesFauna = {} as AccesibilidadVias;
    this.selectedValues = this.selecFaunas;
    this.verModalFauna = true;
  }

  filtrarFauna() {
    if (this.queryFauna) {
      this.comboListEspeciesFauna = this.comboListEspeciesFauna.filter((r) =>
        r.nombreCientifico.toLowerCase().includes(this.queryFauna.toLowerCase())
      );
    } else {
      this.listaPorFiltroEspecieFauna();
    }
  }

  seleccionarNuevoaFauna() {
    this.listaFauna.forEach((item) => {
      this.selecFaunas.forEach((element: any, index) => {
        if (element.idEspecie == item.idFauna) {
          this.selecFaunas[index].idInfBasicaDet = item.idInfBasicaDet;
        }
      });
    });
  }

  guardarFauna() {
    this.selecFaunas = [];
    this.selecFaunas = [...this.selectedValues];
    this.seleccionarNuevoaFauna();

    this.selecFaunas.forEach((item: any) => {
      this.selecEspeciesFauna = {} as AccesibilidadVias;
      this.selecEspeciesFauna.idFauna = item.idEspecie;
      this.selecEspeciesFauna.nombreComun = item.nombreComun
      this.selecEspeciesFauna.nombreCientifico = item.nombreCientifico
      this.selecEspeciesFauna.familia = item.familia
      this.selecEspeciesFauna.codInfBasicaDet = CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS;
      this.selecEspeciesFauna.codSubInfBasicaDet = "TAB_1"
      this.selecEspeciesFauna.idUsuarioRegistro = this.user.idUsuario;
      this.selecEspeciesFauna.idInfBasicaDet = item.idInfBasicaDet
        ? item.idInfBasicaDet
        : 0;
      this.listGuardarFaunas.push(
        new AccesibilidadVias(this.selecEspeciesFauna)
      );
    });
    this.verModalFauna = false;
  }

  guardarAspectosBiologicos() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    var params = [{
        idInfBasica: this.infoBasica_4_6.idInfBasica,
        codInfBasica: CodigoProceso.PLAN_GENERAL,
        codSubInfBasica : CodigosTabEvaluacion.PGMFA_TAB_4,
        codNombreInfBasica : CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS,//aspectos biologicos
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: this.listGuardarFaunas,
      },
    ];


    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró los datos de fauna silvestre correctamente.");
          this.listaPorFiltroEspecieFauna();
        } else {
          this.toast.error(response?.message);
        }
      });
  }
  // -------------------------- Agergar Especies ----------------------------------------- //

  obtenerFaunas() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoTipo: "PGMFA",
    };
    this.informacionAreaPmfiService
      .obtenerFauna(params)
      .subscribe((response: any) => {
        this.listAspectosBiologicos = [];
        response.data.forEach((element: any, index: number) => {
          if (element.idArchivo !== 0) {
            this.listarArchivo(element, index);
          } else if (element.idArchivo == 0) {
            this.listAspectosBiologicos.push({
              ...element,
              nombreArchivo: null,
            });
          }
        });
      });
  }

  listarArchivo(element: any, index: number, editar?: boolean) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    var params = {
      idArchivo: element.idArchivo,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 38,
      codigoProceso: "PGMFA",
    };
    this.anexosService.listarArchivoDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data) {
          result.data.forEach((item: any) => {
            this.nombreArchivo = item.nombreArchivo;
            if (editar) {
              element.nombreArchivo = this.nombreArchivo;
              this.listAspectosBiologicos[ index ] = element;
            } else {
              this.listAspectosBiologicos.push({
                ...element,
                nombreArchivo: this.nombreArchivo,
              });
            }
          });
        }
      });
  }

  openModalEspecies(mesaje: string, edit: boolean, data?: SolicitudFaunaModel) {
    this.ref = this.dialogService.open(ModalFormEspeciesComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        edit: edit,
        idPlanManejo: this.idPlanManejo,
      },
    });

    this.ref.onClose.subscribe((resp: SolicitudFaunaModel) => {
      if (resp) {
        var params = new SolicitudFaunaModel();
        params.idPlanManejo = this.idPlanManejo;
        params.idUsuarioRegistro = this.user.idUsuario;
        params.codigoTipo = "PGMFA";
        params.nombreCientifico = resp.nombreCientifico;
        params.familia = resp.familia;
        params.nombre = resp.nombre;
        params.adjunto = resp.adjunto;
        params.estatus = "V";
        params.estadoSolicitud = "Solicitado";
        params.idFauna = resp.idFauna ? resp.idFauna : 0;
        params.idArchivo = resp.idArchivo;
        this.informacionAreaPmfiService
          .registrarSolicitudFauna(params)
          .subscribe((response) => {
            this.obtenerFaunas();
          });
      }
    });
  }

  indexEditar(event: number) {
    this.indexEdit = 0;
    this.indexEdit = event;
  }

  editar(event: SolicitudFaunaModel) {
    this.ref = this.dialogService.open(ModalFormEspeciesComponent, {
      header: "Editar Nueva Especie",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: event,
        edit: true,
        idPlanManejo: this.idPlanManejo,
      },
    });

    this.ref.onClose.subscribe((resp: SolicitudFaunaModel) => {
      if (resp) {
        var params = new SolicitudFaunaModel();
        params.idPlanManejo = this.idPlanManejo;
        params.idUsuarioRegistro = this.user.idUsuario;
        params.codigoTipo = "PGMFA";
        params.nombreCientifico = resp.nombreCientifico;
        params.familia = resp.familia;
        params.nombre = resp.nombre;
        params.adjunto = resp.adjunto;
        params.estatus = resp.estatus;
        params.estadoSolicitud = resp.estadoSolicitud;
        params.idFauna = resp.idFauna;
        params.idArchivo = resp.idArchivo;
        this.informacionAreaPmfiService
          .registrarSolicitudFauna(params)
          .subscribe((response) => {
            if (resp.idArchivo !== 0) {
              this.listarArchivo(resp, this.indexEdit, true);
            } else {
              this.listAspectosBiologicos[ this.indexEdit ] = resp;
            }
          });
      }
    });
  }

  indexEliminar(event: number) {
    this.indexElim = event;
  }

  eliminar(event: any) {
    if (event?.idInfBasicaDet == 0) {
      this.listAspectosBiologicos.splice(this.indexElim, 1);
    } else {
      var params = {
        idFauna: event.idFauna,
        codInfBasica: 'PGMFA',
        idUsuarioElimina: this.user.idUsuario,
      };
      this.informacionAreaPmfiService
        .eliminarFauna(params)
        .subscribe((response: any) => {
          if (response.success == true) {
            this.toast.ok(response?.message);
            this.obtenerFaunas();
          } else {
            this.toast.error(response?.message);
          }
        });
    }
  }
}
