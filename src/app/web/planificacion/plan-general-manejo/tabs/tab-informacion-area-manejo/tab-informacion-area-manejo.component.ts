import { Component, ViewChild, ElementRef, Renderer2, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DepartamentoModel } from 'src/app/model/Departamento';
import { ProvinciaModel } from 'src/app/model/Provincia';
import { DistritoModel } from 'src/app/model/Distrito';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { SolicitudModel } from 'src/app/model/Solicitud';
import { PlanManejoCcnnService } from 'src/app/service/planManejoCcnn.service';
import { InformacionAreaManejoService } from 'src/app/service/informacion-area-manejo/informacion-area-manejo.service';
import { PlanManejoModel } from 'src/app/model/PlanManejo';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { InfoEconomicaPgmf, InformacionUnidadManejo } from 'src/app/model/InformacionAreaManejo';
import { AspectoSocioEconomicoPgmfService } from 'src/app/service/aspecto-socio-economico-pgmf.service';
import { PlanManejoActEconomica, PlanManejoActividades, PlanManejoAspEconomico, PlanManejoConficto, PlanManejoInfraestructura } from 'src/app/model/PlanManejo/PlanManejoAspEconomico';
import { InformacionAreaPmfiService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service';
import { MapApi } from 'src/app/shared/mapApi';
import { ArchivoService, UsuarioService } from '@services';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { DownloadFile, isNullOrEmpty } from '@shared';
import { FileModel } from 'src/app/model/util/File';
import { PGMFArchivoTipo } from '@shared';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { concatMap, finalize } from 'rxjs/operators';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { ToastService } from '@shared';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { InformacionBasicaDetalleModel } from '../../../../../model/PlanManejo/InformacionBasica/InformacionBasicaDetalleModel';
import { InformacionBasicaModel } from '../../../../../model/PlanManejo/InformacionBasica/InformacionBasicaModel';
import { Rios } from '../../../../../model/medioTrasporte';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { from } from 'rxjs';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-informacion-area-manejo',
  templateUrl: './tab-informacion-area-manejo.component.html',
  styleUrls: ['./tab-informacion-area-manejo.component.scss'],
  providers: [MessageService],
})
export class TabInformacionAreaManejoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild('ulResult', { static: true }) private ulResult!: ElementRef;
  @ViewChild('areaTotal', { static: true }) private areaTotal!: ElementRef;
  @ViewChild('tblAreaManejoDividido', { static: true })
  private tblAreaManejoDividido!: ElementRef;
  @ViewChild('tblAcreditacionComunal', { static: true })
  private tblAcreditacionComunal!: ElementRef;
  @ViewChild('fileInput', { static: true }) private fileInput!: ElementRef;
  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input() isPerfilArffs!: boolean;

  cmbRios: Rios[] = [];

  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  listaBosque: any[] = [];
  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_4;

  codigoAcordeonAcreditacion: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_ACREDITACION;
  codigoAcordeonUbicacionPolitica: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_UBICPOLITCA;
  codigoAcordeonCoordenadasUTM: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_COORUTM;
  codigoAcordeonAccesibilidad: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_ACCESIBILIDAD;
  codigoAcordeonAspectosFisicos: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_FISICOS;
  codigoAcordeonAspectosBiologicos: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS;
  codigoAcordeonAspectosSocioeconomico: string =
    CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_SOCIECONOMICOS;

  evaluacionAcreditacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeonAcreditacion,
  });
  evaluacionUbicacionPolitica: EvaluacionArchivoModel =
    new EvaluacionArchivoModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonUbicacionPolitica,
    });
  evaluacionCoordenadasUTM: EvaluacionArchivoModel = new EvaluacionArchivoModel(
    {
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonCoordenadasUTM,
    }
  );
  evaluacionAccesibilidad: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeonAccesibilidad,
  });
  evaluacionAspectosFisicos: EvaluacionArchivoModel =
    new EvaluacionArchivoModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonAspectosFisicos,
    });
  evaluacionAspectosBiologicos: EvaluacionArchivoModel =
    new EvaluacionArchivoModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonAspectosBiologicos,
    });
  evaluacionAspectosSocioeconomico: EvaluacionArchivoModel =
    new EvaluacionArchivoModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonAspectosSocioeconomico,
    });

  evaluacion: any;

  codigoTabla1 = 'TAB_1';
  codigoTabla2 = 'TAB_2';
  codigoTabla3 = 'TAB_3';
  codigoTabla4 = 'TAB_4';
  codigoTablaCabecera = 'TAB_CAB';

  infoBasica_4_1: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAcreditacion,
    idUsuarioRegistro: this.user.idUsuario,
  });

  infoBasicaDet_4_1: InformacionBasicaDetalleModel =
    new InformacionBasicaDetalleModel({
      idInfBasicaDet: 0,
      codInfBasicaDet: this.codigoAcordeonAcreditacion,
      codSubInfBasicaDet: this.codigoTabla1,
      referencia: '',
      descripcion: '',
      areaHa: 0,
      idUsuarioRegistro: this.user.idUsuario,
    });

  infoBasica_4_2: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonUbicacionPolitica,
    idUsuarioRegistro: this.user.idUsuario,
  });

  infoBasicaDet_4_2: InformacionBasicaDetalleModel =
    new InformacionBasicaDetalleModel({
      idInfBasicaDet: 0,
      codInfBasicaDet: this.codigoAcordeonUbicacionPolitica,
      codSubInfBasicaDet: this.codigoTabla1,
      referencia: 'temporal para 4_2',
      idUsuarioRegistro: this.user.idUsuario,
    });

  infoBasica_4_4: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAccesibilidad,
    idUsuarioRegistro: this.user.idUsuario,
  });

  infoBasica_4_5: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAspectosFisicos,
    idUsuarioRegistro: this.user.idUsuario,
  });

  infoBasica_4_6: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAspectosBiologicos,
    idUsuarioRegistro: this.user.idUsuario,
  });

  listInfoBasicaDet_4_4_1: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_4_2: InformacionBasicaDetalleModel[] = [];

  listInfoBasicaDet_4_5_1: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_5_2: InformacionBasicaDetalleModel[] = [];

  listInfoBasicaDet_4_6_1: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_6_2: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_6_3: InformacionBasicaDetalleModel[] = [];

  infoBasica_4_7: InformacionBasicaModel = new InformacionBasicaModel({
    idInfBasica: 0,
    idPlanManejo: this.idPlanManejo,
    codInfBasica: this.codigoProceso,
    codSubInfBasica: this.codigoTab,
    codNombreInfBasica: this.codigoAcordeonAspectosSocioeconomico,
    idUsuarioRegistro: this.user.idUsuario,
  });

  infoBasicaDet_4_7_cab: InformacionBasicaDetalleModel =
    new InformacionBasicaDetalleModel({
      idInfBasicaDet: 0,
      codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
      codSubInfBasicaDet: this.codigoTablaCabecera,
      idFlora: 0,
      idFauna: 0,
      idUsuarioRegistro: this.user.idUsuario,
    });

  listInfoBasicaDet_4_7_1: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_7_2: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_7_3: InformacionBasicaDetalleModel[] = [];
  listInfoBasicaDet_4_7_4: InformacionBasicaDetalleModel[] = [];

  public _files: any[] = [];
  _id = this.mapApi.Guid2.newGuid;
  view: any = null;
  public graphicsLayerDepartment: any = null;
  public graphicsLayerProvince: any = null;
  public graphicsLayerDistrict: any = null;
  public titleAreaManejo = 'PGMF';
  public titleVerticeAreaManejo = '';
  public filFile: any = null;
  listCoordinatesAnexo1: any[] = [];
  listCoordinatesAnexo2: any[] = [];

  planManejo = {} as PlanManejoModel;
  idInfBasica: number = 0;
  usuario = {} as UsuarioModel;
  esTerrestre: string | null = null;
  idAcceManejo: number = 0;

  datap: PlanManejoAspEconomico = new PlanManejoAspEconomico();
  fileVertices: any = {};
  fileParcelas: any = {};
  fileBosques: any = {};

  actividadesEconomicas: PlanManejoAspEconomico[] = [];
  listActividadesEconomicas: PlanManejoActEconomica[] = [];
  listInfraestructura: PlanManejoInfraestructura[] = [];
  planManejoActividades: PlanManejoActividades[] = [];
  planManejoConficto: PlanManejoConficto[] = [];
  informacionBasicaReq: InformacionBasicaModel[] = [];
  informacionBasica: InformacionBasicaModel = {} as InformacionBasicaModel;

  totalAreaTipoBosque: String = '';
  totalPorcentajeTipoBosque: String = '';

  listFisio: any[] = [];

  infoEconomica = {} as InfoEconomicaPgmf;

  params4_1: any = {
    idInfBasica: this.codigoProceso,
    idPlanManejo: this.idPlanManejo,
    codCabecera: this.codigoAcordeonAcreditacion, // Hace referencia a codInfBasicaDet
  };
  //accesibilidad

  verModalBosque: boolean = false;
  queryBosque: string = '';

  isShowModalTC = false;
  tabIndex = 0;
  tabIndex2 = 0;
  tabIndex3 = 0;

  idInfBasica_4:number = 0
  idInfBasicaBosq: number = 0;

  listCuenca: any[] = [];
  listSubcuenca: any[] = [];
  listCuencaSubcuenca: CuencaSubcuencaModel[] = [];
  idCuenca!: number | null;
  idSubcuenca!: number | null;

  constructor(
    private renderer: Renderer2,
    private serviceGeoforestal: ApiGeoforestalService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private servCoreCentral: CoreCentralService,
    private servPlanManejoCcnn: PlanManejoCcnnService,
    private dialog: MatDialog,
    private informacionAreaManejoService: InformacionAreaManejoService,
    private aspSocioEcoServ: AspectoSocioEconomicoPgmfService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private informacionAreaService: InformacionAreaPmfiService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private informacionGeneralService: InformacionGeneralService,
    private serviceExternos: ApiForestalService,
    private router: Router
  ) {}

  listAntecedentesDeUsoBosque: any[] = [
    { valorPrimario: 'Extracción de Madera Comercial' },
    { valorPrimario: 'Extracción de Madera redonda, leña y carbón' },
    { valorPrimario: 'Extracción de productos No Maderables' },
    { valorPrimario: 'Caza o Captura de Fauna Silvestre' },
  ];

  listConflictosForestales: any[] = [];
  displayModalConflictos: boolean = false;
  editConflictos: boolean = false;
  conflicto: any = {};

  listFaunaSilvestre: any[] = [];

  departamento = {} as DepartamentoModel;
  listDepartamento: DepartamentoModel[] = [];
  provincia = {} as ProvinciaModel;
  listProvincia: ProvinciaModel[] = [];
  distrito = {} as DistritoModel;
  listDistrito: DistritoModel[] = [];

  accesibilidadDescripcionList: any[] = [];

  tipoBosquesList: any[] = [];

  listHidro: any[] = [];

  unidadManejo = {} as InformacionUnidadManejo;

  accesibilidadList: any[] = [];

  comboListBosques: any[] = [];
  selectedValues: any[] = [];

  ngOnInit(): void {
    /*this.planManejo.idPlanManejo = Number(
      localStorage.getItem('idPlanManejo')
    );*/

    //this.obtenerInformacionBasica(this.params4_1);
    //this.obtenerInformacionBasica(this.params4_2);
    this.listarCuencaSubcuenca();
    this.listRios();
    this.listarAcreditacion();
    this.obtenerSerforTipoBosque();

    this.initializeMap();
    this.obtenerCapas();
    this.usuario = this.user.usuario;
    this.listarPorFiltroDepartamentoIni();
    this.listarTiposBosque();

    //this.listarAccesibilidad();
    // this.obtenerInformacionBasica(this.params4_4);
    /*
    DESCOMENTAR AL AVANZAR DE TABS
    if (this.planManejo.idPlanManejo > 0) {
      this.obtenerSolicitudAprovechamientoCcnn();
      this.obtenerUnidadManejo();
      this.listarHidrografia();
      this.listarInfoFaunaSilvestre();
      this.configurarAspectofisicofisiografia();
      this.obtenerInfoEconomicaPgmf();
    }*/
    //console.log("this.isPerfilArffs", this.isPerfilArffs);
    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  static get EXTENSIONSAUTHORIZATIONSHP() {
    return ['zip', 'application/x-zip-compressed'];
  }

  //listar

  listarCuencaSubcuenca() {
    this.listCuencaSubcuenca = [];
    this.listCuenca = [];

    this.servCoreCentral.listarCuencaSubcuenca().subscribe(
      (result: any) => {
        this.listCuencaSubcuenca = result.data;

        let listC: any[] = [];
        this.listCuencaSubcuenca.forEach((item) => {
          listC.push(item);
        });

        const setObj = new Map();
        const unicos = listC.reduce((arr, cuenca) => {
          if (!setObj.has(cuenca.idCuenca)) {
            setObj.set(cuenca.idCuenca, cuenca)
            arr.push(cuenca)
          }
          return arr;
        }, []);

        this.listCuenca = unicos;
        this.listCuenca.sort((a, b) => a.cuenca - b.cuenca);

        if (!isNullOrEmpty(this.idCuenca) && !isNullOrEmpty(this.idSubcuenca)) {
          this.onSelectedCuenca({ value: Number(this.idCuenca) });
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  onSelectedCuenca(param: any) {    
    this.listSubcuenca = [];

    this.listCuencaSubcuenca.forEach((item) => {
      if (item.idCuenca == param.value) {
        this.listSubcuenca.push(item);
      }
    });
  }

  listRios() {
    var params = {
      id: 0,
      tipo: 'RIO',
    };
    this.informacionAreaPmfiService
      .obtenerHidrografia(params)
      .subscribe((response: any) => {
        this.cmbRios = [...response.data];
      });
  }

  obtenerInformacionBasica(parametro: any) {
    this.informacionAreaPmfiService
      .listarInformacionBasica(parametro)
      .subscribe((response: any) => {
        //console.log(response);
        //FILTRAR

        //para acordeon 4.1
        let list_InfoBasicDetalle4_1 = response.data.filter(
          (x: any) => (x.codInfBasicaDet = this.codigoAcordeonAcreditacion)
        );
        if (list_InfoBasicDetalle4_1 && list_InfoBasicDetalle4_1.length > 0) {
          let infoBasic4_1 = list_InfoBasicDetalle4_1[0];
          this.idInfBasica_4 = infoBasic4_1.idInfBasica
          Object.assign(this.infoBasica_4_1, infoBasic4_1);
          Object.assign(this.infoBasicaDet_4_1, infoBasic4_1);
        }

        //para acordeon 4.2
        let list_InfoBasicDetalle4_2 = response.data.filter(
          (x: any) => (x.codInfBasicaDet = this.codigoAcordeonUbicacionPolitica)
        );
        if (list_InfoBasicDetalle4_2 && list_InfoBasicDetalle4_2.length > 0) {
          let infoBasic4_2 = list_InfoBasicDetalle4_2[0];
          infoBasic4_2.idDepartamento = Number.parseInt(
            infoBasic4_2.departamento
          );
          this.onSelectedProvincia({ value: infoBasic4_2.idDepartamento });
          infoBasic4_2.idProvincia = Number.parseInt(infoBasic4_2.provincia);
          this.onSelectedDistrito({ value: infoBasic4_2.idProvincia });
          infoBasic4_2.idDistrito = Number.parseInt(infoBasic4_2.distrito);
          Object.assign(this.infoBasica_4_2, infoBasic4_2);
        }

        let list_InfoBasicDetalle4_4 = response.data.filter(
          (x: any) => (x.codInfBasicaDet = this.codigoAcordeonAccesibilidad)
        );
        if (list_InfoBasicDetalle4_4 && list_InfoBasicDetalle4_4.length > 0) {
          let infoBasic4_4 = list_InfoBasicDetalle4_4[0];
          Object.assign(this.infoBasica_4_4, infoBasic4_4); //SE TOMA EL PRIMERO PARA LA CABECERA
          this.listInfoBasicaDet_4_4_1 = list_InfoBasicDetalle4_4.filter(
            (x: any) => (x.codSubInfBasicaDet = 'TAB_1')
          );
          this.listInfoBasicaDet_4_4_2 = list_InfoBasicDetalle4_4.filter(
            (x: any) => (x.codSubInfBasicaDet = 'TAB_2')
          );
        }

        //COMENTADO
        //this.idInfBasica = response.data[0] === undefined ? 0 : response.data[0].idInfBasica;
      });
  }

  listarPorFiltroDepartamentoIni() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.message,
        });
      }
    );
  }

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        this.listarPorFilroProvincia(this.provincia);
      },
      (error: any) => {
        this.messageService.add({
          severity: 'warn',
          summary: '',
          detail: error.message,
        });
      }
    );
  }
  listarPorFilroProvincia(provincia: ProvinciaModel) {
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      });
  }
  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }
  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }
  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }
  obtenerSolicitudAprovechamientoCcnn() {
    const solicitud = {} as SolicitudModel;
    solicitud.idSolicitud = 143;

    this.servPlanManejoCcnn
      .obtenerSolicitudAprovechamientoCcnn(solicitud)
      .subscribe((result: any) => {
        let resultado = result.data.data;

        this.departamento.idDepartamento = resultado.distrito.idDepartamento;
        this.provincia.idProvincia = resultado.distrito.idProvincia;
        this.distrito.idDistrito = resultado.distrito.idDistrito;

        this.listarPorFiltroDepartamento(this.departamento);
        if (resultado.distrito.idDepartamento !== null) {
          this.searchDepartment(`0${resultado.distrito.idDepartamento}`);
        }
        if (resultado.distrito.idProvincia !== null) {
          this.searchProvince(`0${resultado.distrito.idProvincia}`);
        }
        if (resultado.distrito.idDistrito !== null) {
          this.searchDistrict(`0${resultado.distrito.idDistrito}`);
        }
      });
  }
  searchDepartment(id: any) {
    let options = {
      layerId: '14',
      where: `codigo=${id}`,
    };

    if (this.graphicsLayerDepartment !== null)
      this.graphicsLayerDepartment.removeAll();
    this.graphicsLayerDepartment = this.mapApi.graphicsLayer();
    this.view.map.add(this.graphicsLayerDepartment);
    this.mapApi.getDatosbyAGS(options).then((data: any) => {
      const fillSymbol = {
        type: 'simple-fill',
        color: [227, 139, 79, 0.4],
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        t.geometry.type = 'polygon';
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.properties;
        this.graphicsLayerDepartment.add(feature);
      });
    });
  }
  searchProvince(id: any) {
    let options = {
      layerId: '13',
      where: `codigo=${id}`,
    };

    this.graphicsLayerProvince = this.mapApi.graphicsLayer();
    this.view.map.add(this.graphicsLayerProvince);
    this.mapApi.getDatosbyAGS(options).then((data: any) => {
      const fillSymbol = {
        type: 'simple-fill',
        color: [227, 139, 79, 0.4],
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        t.geometry.type = 'polygon';
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.properties;
        this.graphicsLayerProvince.add(feature);
      });
    });
  }
  searchDistrict(id: any) {
    let options = {
      layerId: '12',
      where: `codigo=${id}`,
    };
    this.graphicsLayerDistrict = this.mapApi.graphicsLayer();
    this.view.map.add(this.graphicsLayerDistrict);
    this.mapApi.getDatosbyAGS(options).then((data: any) => {
      const fillSymbol = {
        type: 'simple-fill',
        color: [227, 139, 79, 0.4],
        outline: {
          color: [255, 255, 255],
          width: 1,
        },
      };
      data.features.forEach((t: any) => {
        t.geometry.type = 'polygon';
        let feature: any = null;
        feature = this.mapApi.graphic(t.geometry, fillSymbol);
        feature.id = t.guid;
        feature.attributes = t.properties;
        this.graphicsLayerDistrict.add(feature);
        this.graphicsLayerDistrict.when((data: any) => {
          this.view.goTo(this.graphicsLayerDistrict.graphics);
        });
      });
    });
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  /* START REGION GEOMETRIA */
  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    code: any,
    codigoSubSeccion: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      codigoSubTipoPGMF: code,
      withVertice: withVertice,
      codigoSubSeccion: codigoSubSeccion,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  onGuardarLayer() {
    let fileUpload: any = [];
    
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: PGMFArchivoTipo.PGMFA,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() =>{ this.dialog.closeAll()
        this.cleanLayers();
        this.obtenerCapas();}))
      .subscribe(
        (result) => {
          
          this.SuccessMensaje(result.message);
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un error.');
        }
      );
  }
  onDownloadFileSHP(id: any) {
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      };
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                
                if (this._filesSHP.length === 0) {
                  this.cleanLayers();
                }
                this.SuccessMensaje('El archivo se eliminó correctamente.');
              } else {
                this.ErrorMensaje('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error) => {
              
              this.dialog.closeAll();
              this.ErrorMensaje('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (config.codigoSubTipoPGMF === 'PGMFAREA') {
        this.calculateArea(data);
      }
    });
  }
  createLayers(layers: any, config: any) {
    
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.codigoSubTipoPGMF;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layer.annex = config.annex;
      this._layer.descripcion = config.codigoSubTipoPGMF;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    this.file.idGroupLayer = config.idGroupLayer;
    this.file.descripcion = config.codigoSubTipoPGMF;
    this._filesSHP.push(this.file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.withVertice === true) {
      popupTemplate.content = [
        {
          type: 'fields',
          fieldInfos: [
            {
              fieldName: 'anexo',
              label: 'Anexo',
            },
            {
              fieldName: 'vertice',
              label: 'Vertice',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'este',
              label: 'Este (X)',
            },
            {
              fieldName: 'norte',
              label: 'Norte (Y)',
            },
          ],
        },
      ];
    }
    return popupTemplate;
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: PGMFArchivoTipo.PGMFA,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: null,
        codigoSeccion: 'PGMFAIBAM',
        codigoSubSeccion: t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });
    
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  obtenerCapas() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: 'PGMFAIBAM',
      codigoSubSeccion: '',
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = t.idArchivo;
              this._layer.idLayer = this.mapApi.Guid2.newGuid;
              this._layer.inServer = true;
              this._layer.service = false;
              this._layer.nombre = t.nombreCapa;
              this._layer.groupId = groupId;
              this._layer.color = t.colorCapa;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layers.push(this._layer);
              let geoJson = this.mapApi.getGeoJson(
                this._layer.idLayer,
                groupId,
                item
              );
              
              this.createLayer(geoJson);
              if (t.codigoSubSeccion === 'PGMFAIBAMCUAMAMC') {
                this.obtenerCordenadaUTM();
              } else if (t.codigoSubSeccion === 'PGMFAIBAMCUAMAMD') {
                this.obtenerCordenadaUTM2();
              }
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje('Ocurrió un error');
      }
    );
  }
  /* END REGION GEOMETRIA */
  eliminarCoordenadas(code: any) {
    let params = {
      idInfBasica: this.idInfBasica,
      codInfBasicaDet: code,
      idPlanManejo: this.idPlanManejo,
      idUsuarioElimina: this.user.idUsuario,
    };
    return;
    this.informacionAreaPmfiService
      .eliminarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.SuccessMensaje(response?.message);
        } else {
          this.ErrorMensaje(response?.message);
        }
      });
  }
  validate(item: any, accion: boolean) {
    let geometry = item.features[0];
    let params = { geometria: { poligono: geometry.geometry.coordinates } };
    let demo = {
      geometria: {
        poligono: [
          [
            [-71.91650390625, -9.914743843241718],
            [-71.883544921875, -10.228437266155943],
            [-71.641845703125, -10.196000424383548],
            [-71.619873046875, -9.925565912405494],
            [-71.91650390625, -9.914743843241718],
          ],
        ],
      },
    };
    this.serviceExternos
      .validarSuperposicionAprovechamiento(demo)
      .subscribe((result: any) => {
        if (result.dataService.data.nroCapasEvaluadas > 0) {
          if (result.dataService.data.noSuperpuesta.length > 0) {
            result.dataService.data.noSuperpuesta.map((capa: any) => {
              if (capa.seSuperpone === 'Si') {
                capa.geoJson.crs.type = 'name';
                capa.geoJson.fileName = capa.nombreCapa;
                capa.geoJson.opacity = 0.2;
                capa.geoJson.color = this.mapApi.random();
                //this.createLayer(capa.geoJson, accion);
              }
            });
          }
        }
      });
  }
  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo1 = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo1.push({
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: '',
      });
    });
  }

  createTablaConAnexo(items: any) {
    let listMap = items.map((t: any) => {
      return {
        anexo: t.properties.anexo,
        vertice: t.properties.vertice,
        este: t.properties.este,
        norte: t.properties.norte,
        referencia: '',
      };
    });

    this.listCoordinatesAnexo2 = listMap.groupBy((t: any) => t.anexo);
  }
  createTablaConAnexo2(items: any) {
    var tblAreaManejoDividido = this.tblAreaManejoDividido.nativeElement;
    var thead = tblAreaManejoDividido.querySelector('thead');
    var tbody = tblAreaManejoDividido.querySelector('tbody');
    while (tbody.firstChild) tbody.removeChild(tbody.firstChild);

    if (items.length > 0) {
      items.forEach((t: any, i: any) => {
        let tr = tbody.insertRow(i);
        tr.dataset.guid = t.properties.vertice;
        tr.insertCell(0).textContent = t.properties.anexo;
        tr.insertCell(1).textContent = t.properties.vertice;
        tr.insertCell(2).textContent = t.properties.este;
        tr.insertCell(3).textContent = t.properties.norte;
        let td = tr.insertCell(4);
        td.innerHTML = `<input type="text" class="form-control bg-gray"
      placeholder="Referencia" />`;
      });
    } else {
      let tr = tbody.insertRow(0);
      let td = tr.insertCell(0);
      td.colSpan = thead.querySelectorAll(':scope > tr > th').length;
      td.classList.add('text-center');
      td.innerHTML = 'No se han encontrado registros';
    }

    var values = tbody.querySelectorAll(':scope tr>td:first-of-type');
    var run = 1;
    for (var i = values.length - 1; i > -1; i--) {
      if (i > 0) {
        var text1 = values[i].textContent;
        var text2 = values[i - 1].textContent;
        if (text1 === text2) {
          values[i].remove();
          run++;
        } else {
          values[i].rowSpan = run;
          run = 1;
        }
      }
    }
  }
  calculateArea(data: any) {
    if (data[0].title === 'PGMF') {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = data[0].features[0].geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      let tabla = this.tblAcreditacionComunal.nativeElement;
      let body = tabla.querySelector('tbody');
      let trs = body.querySelectorAll('tr');
      let tdArea = trs[0].querySelectorAll('td')[2];
      tdArea.innerHTML = area.toFixed(3);
    }
    data[0].features.forEach((t: any) => {
      if (t.geometry.type === 'Polygon') {
        let item2: any = {
          anexoSector: t.properties.ANEXO,
          detalle: [],
        };
        this.consultarTiposBosque(t.geometry.coordinates).subscribe(
          (result: any) => {
            result.dataService.data.capas.forEach((t2: any) => {
              if (t2.geoJson !== null) {
                let item = {
                  subparcela: '',
                  tipoBosque: t2.nombreCapa,
                  porcentaje: 0,
                  area:
                    t2.geoJson.features[0].properties.SUPAPR ||
                    t2.geoJson.features[0].properties.za_super,
                };
                item2.detalle.push(item);
              }
            });
            this.tipoBosquesList.push(item2);
            this.calculateAreaTotal();
          }
        );
      }
    });
  }
  calculateAreaTotal() {
    let sum1 = 0;
    let sum2 = 0;

    for (let item of this.tipoBosquesList) {
      item.detalle.forEach((t: any) => {
        sum1 += t.area;
      });
    }
    for (let item of this.tipoBosquesList) {
      item.detalle.forEach((t: any) => {
        t.porcentaje = Number(((100 * t.area) / sum1).toFixed(1)) || 0;
        sum2 += t.porcentaje;
      });
    }
    this.totalAreaTipoBosque = `${sum1.toFixed(2)}`;
    this.totalPorcentajeTipoBosque = `${sum2.toFixed(1)} %`;
  }
  consultarTiposBosque(geometry: any) {
    let params = {
      idClasificacion: '',
      geometria: {
        poligono: geometry,
      },
    };
    return this.serviceExternos.identificarTipoBosque(params);
  }
  ngOnDestroy(): void {
    if (this.view) {
      // destroy the map view
      this.view.destroy();
    }
  }

  listarHidrografia() {
    this.informacionAreaManejoService
      .listarAspectoHidrografico(this.planManejo)
      .subscribe((result: any) => {
        this.listHidro = result.data;
      });
  }

  listarInfoFaunaSilvestre() {
    this.informacionAreaManejoService
      .obtenerInfoBiologicoFaunaSilvestrePgmf(this.planManejo)
      .subscribe((result: any) => {
        this.listFaunaSilvestre = result.data;
      });
  }

  configurarAspectofisicofisiografia() {
    this.informacionAreaManejoService
      .configurarAspectofisicofisiografia(this.planManejo)
      .subscribe((result: any) => {
        if (result.success) this.listarAspectofisicofisiografia();
      });
  }

  listarAspectofisicofisiografia() {
    this.informacionAreaManejoService
      .listarAspectofisicofisiografia(this.planManejo)
      .subscribe((result: any) => {
        this.listFisio = result.data;
      });
  }

  obtenerUnidadManejo() {
    this.informacionAreaManejoService
      .obtenerUnidadManejo(this.planManejo)
      .subscribe((result: any) => {
        if (result.data) this.unidadManejo = result.data;
      });
  }

  obtenerCordenadaUTM() {
    let params = {
      idUnidadManejo: this.idPlanManejo,
      tipo: 1,
    };
    this.informacionAreaManejoService
      .listarCoordenadaUtm(params)
      .subscribe((result: any) => {
        if (result) {
          if (result.data && result.data.length > 0) {
            this.listCoordinatesAnexo1 = result.data;
          }
        }
      });
  }

  obtenerCordenadaUTM2() {
    let params = {
      idUnidadManejo: this.unidadManejo.idUnidadManejo,
      tipo: 2,
    };
    this.informacionAreaManejoService
      .listarCoordenadaUtm(params)
      .subscribe((result: any) => {
        if (result) {
          if (result.data && result.data.length > 0) {
            this.listCoordinatesAnexo2 = result.data.groupBy(
              (t: any) => t.anexoSector
            );
          }
        }
      });
  }

  eliminarCoordenadaUTM(tipo: number) {
    let params = {
      idUnidadManejo: this.unidadManejo.idUnidadManejo,
      tipo: tipo,
      idUsuarioElimina: this.usuario.idusuario,
    };

    this.informacionAreaManejoService
      .eliminarCoordenadaUtm(params)
      .subscribe((result: any) => {
        this.SuccessMensaje(result.message);
      });
  }

  agregarAccesibilidadDescripcion() {
    this.accesibilidadDescripcionList.push({
      requisito: '',
      idAcceManejoreq: (this.accesibilidadDescripcionList.length + 1) * -1,
    });
  }

  eliminarAccesibilidadDescripcion(event: any, item: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (item.idAcceManejoreq < 0)
          this.accesibilidadDescripcionList =
            this.accesibilidadDescripcionList.filter(
              (x: any) => x.idAcceManejoreq != item.idAcceManejoreq
            );
        else this.eliminarAccesibilidadmanejorequisito(item);
      },
      reject: () => {
        //reject action
      },
    });
  }

  AgregarActividadEconomica() {
    let obj: any = {};

    obj.idTipoactividadEconomica = 8;
    obj.accion = false;
    obj.tipoActividadEconomica = 'Otras(especifique)';

    this.listActividadesEconomicas.push(obj);
  }

  AgregarInfraestructura() {
    let obj: any = {};

    obj.idTipoactividadEconomica = 8;
    obj.activo = false;
    obj.tipoInfraestructura = 'Otros (radiofonia,telefonia,etc.)';

    this.listInfraestructura.push(obj);
  }

  agregarConflicto() {
    if (this.editConflictos) {
      this.displayModalConflictos = false;
    } else {
      this.listConflictosForestales.push(this.conflicto);
      this.displayModalConflictos = false;
    }
  }

  abrirModalConflicto() {
    this.conflicto = {};
    this.editConflictos = false;
    this.displayModalConflictos = true;
  }

  cerrarModalConflicto() {
    this.displayModalConflictos = false;
  }

  openEditarConflicto(data: any, index: number) {
    this.editConflictos = true;
    this.conflicto = this.listConflictosForestales[index];
    this.displayModalConflictos = true;
  }

  openEliminarConflicto(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.listConflictosForestales.splice(index, 1);
      },
      reject: () => {
        //reject action
      },
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  WarningMessage(mensaje: any) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: mensaje,
    });
  }

  onTabOpen = (event: any) => {
    if (event.index === 0) {
      this.listarAcreditacion();
    } else if (event.index === 1) {
      this.listarUbicacionPolitica();
    } else if (event.index === 2) {
      //this.listarUbicacionPolitica()
    } else if (event.index === 3) {
      this.listarAccesibilidad();
    } else if (event.index === 4) {
      this.listarAspectosFisicos();
    } else if (event.index === 5) {
      this.listarTipoDeBosque();
    } else if (event.index === 6) {
      this.listarAspectosSocieconomicos();
    }
  };

  listarAccesibilidadmanejo = () => {
    let params = {
      idPlanManejo: this.planManejo.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaManejoService
      .listarAccesibilidadmanejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          result.data.forEach((element: any) => {
            this.esTerrestre = element.tipoAccesibilidad ? '1' : '2';
            this.idAcceManejo = element.idAcceManejo;
            this.listarAccesibilidadmanejorequisito();
            this.listarMatrizAccesibilidad();
          });
          //this.dialog.closeAll();
        },
        (error: HttpErrorResponse) => {
          // this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  listarAccesibilidadmanejorequisito = () => {
    let params = {
      idAcceManejo: this.idAcceManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.informacionAreaManejoService
      .listarAccesibilidadmanejorequisito(params)
      .subscribe(
        (result: any) => {
          this.accesibilidadDescripcionList = result.data || [];
          this.dialog.closeAll();
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  listarMatrizAccesibilidad = () => {
    let params = {
      idAcceManejo: this.idAcceManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.informacionAreaManejoService
      .listarAccesibilidadmanejomatriz(params)
      .subscribe(
        (result: any) => {
          this.accesibilidadList = result.data || [];
          this.dialog.closeAll();
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  guardarAccesibilidad = () => {
    if (this.esTerrestre === null) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: '(*) Debe seleccionar: Terrestre o Fluvial',
      });
      return;
    }

    if (this.esTerrestre != null && this.idAcceManejo === 0) {
      this.registrarAccesibilidadmanejo();
    } else {
      this.actualizarAccesibilidadmanejo();
    }
  };

  registrarAccesibilidadmanejo = () => {
    let params = {
      idPlanManejo: this.planManejo.idPlanManejo,
      idUsuarioRegistro: this.usuario.idusuario,
      tipoAccesibilidad: this.esTerrestre === '1' ? true : false,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.informacionAreaManejoService
      .registrarAccesibilidadmanejo(params)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            this.listarAccesibilidadmanejo();
          } else this.ErrorMensaje(result.message);
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  actualizarAccesibilidadmanejo = () => {
    let params = {
      idAcceManejo: this.idAcceManejo,
      idUsuarioModificacion: this.usuario.idusuario,
      tipoAccesibilidad: this.esTerrestre === '1' ? true : false,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaManejoService
      .actualizarAccesibilidadmanejo(params)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            this.listarAccesibilidadmanejo();
          } else this.ErrorMensaje(result.message);
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  eliminarAccesibilidadmanejorequisito = (item: any) => {
    let params = {
      idAcceManejoreq: item.idAcceManejoreq,
      idUsuarioElimina: this.usuario.idusuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaManejoService
      .eliminarAccesibilidadmanejorequisito(params)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            this.listarAccesibilidadmanejo();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  guardarCaminos = () => {
    if (
      this.accesibilidadDescripcionList.filter((x: any) => x.requisito === '')
        .length > 0
    ) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: '(*) Debe ingresar: la descripción del requisito',
      });
    } else {
      let listaAccesibilidad: any[] = [];
      if (
        this.accesibilidadDescripcionList.filter(
          (x: any) => x.idAcceManejoreq < 0
        ).length > 0
      ) {
        this.accesibilidadDescripcionList
          .filter((x: any) => x.idAcceManejoreq < 0)
          .forEach((element: any) => {
            listaAccesibilidad.push({
              idAcceManejo: this.idAcceManejo,
              idUsuarioRegistro: this.usuario.idusuario,
              requisito: element.requisito,
            });
          });
        this.registrarAccesibilidadmanejorequisito(listaAccesibilidad);
      } else if (
        this.accesibilidadDescripcionList.filter(
          (x: any) => x.idAcceManejoreq > 0
        ).length > 0
      ) {
        this.actualizarAccesibilidadmanejorequisito();
      }
    }
  };

  registrarAccesibilidadmanejorequisito = (data: any) => {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaManejoService
      .registrarAccesibilidadmanejorequisito(data)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            if (
              this.accesibilidadDescripcionList.filter(
                (x: any) => x.idAcceManejoreq > 0
              ).length > 0
            ) {
            } else this.listarAccesibilidadmanejo();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  actualizarAccesibilidadmanejorequisito = () => {
    let data: any[] = [];
    this.accesibilidadDescripcionList
      .filter((x: any) => x.idAcceManejoreq > 0)
      .forEach((element: any) => {
        data.push({
          idAcceManejoreq: element.idAcceManejoreq,
          idUsuarioModificacion: this.usuario.idusuario,
          requisito: element.requisito,
        });
      });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaManejoService
      .actualizarAccesibilidadmanejorequisito(data)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            this.listarAccesibilidadmanejo();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  };

  registrarAcreditacion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_4_1.idPlanManejo = this.idPlanManejo;
    this.infoBasica_4_1.idInfBasica = this.idInfBasica_4
    this.infoBasica_4_1.listInformacionBasicaDet = [];
    this.infoBasica_4_1.listInformacionBasicaDet.push(this.infoBasicaDet_4_1);
    
    this.informacionAreaService
      .registrarInformacionBasica([this.infoBasica_4_1])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró la Acreditación correctamente.');
        //
        this.listarAcreditacion();
      });
  }

  registrarUbicacionPolitica() {
    //console.log(this.infoModelBasica_4_2);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_4_2.idPlanManejo = this.idPlanManejo;
    this.infoBasica_4_2.departamento = this.infoBasica_4_2.idDepartamento + '';
    this.infoBasica_4_2.provincia = this.infoBasica_4_2.idProvincia + '';
    this.infoBasica_4_2.distrito = this.infoBasica_4_2.idDistrito + '';
    this.infoBasica_4_2.cuenca = this.idCuenca ? String(this.idCuenca) : null;
    this.infoBasica_4_2.subCuenca = this.idSubcuenca ? String(this.idSubcuenca) : null;
    this.infoBasica_4_2.listInformacionBasicaDet = [];
    this.infoBasica_4_2.listInformacionBasicaDet.push(this.infoBasicaDet_4_2);

    this.informacionAreaService
      .registrarInformacionBasica([this.infoBasica_4_2])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró la Ubicación Política correctamente.');
        this.listarUbicacionPolitica();
        /*let params4_2:any = {
          idInfBasica  :  this.codigoProceso,
          idPlanManejo : this.idPlanManejo,
          codCabecera  : this.codigoAcordeonUbicacionPolitica // Hace referencia a codInfBasicaDet
        };
        this.obtenerInformacionBasica(params4_2);*/
      });
    /*
    if (!this.unidadManejo.cuenca) {
      this.ErrorMensaje("(*) Ingresa cuenca")
      return;
    }

    if (this._files.length === 0) {
      this.ErrorMensaje("(*) Cargue los archivos shape file")
      return;
    }


    if (this.unidadManejo.idUnidadManejo) {

      this.unidadManejo = {
        ...this.unidadManejo,
        idUsuarioModificacion: this.usuario.idusuario,
        estado: "A",
        idPlanManejo: this.planManejo.idPlanManejo,
        idDistrito: this.distrito.idDistrito,
      }

      this.informacionAreaManejoService.actualizarUnidadManejo(this.unidadManejo).subscribe((result: any) => {
        if (result.success) {

          this.SuccessMensaje(result.message);
          this.registrarArchivosVertices();

        } else {
          this.WarningMessage(result.message);
        }
      }, errr => {
        this.WarningMessage(errr);
      })



    } else {

      this.unidadManejo = {
        ...this.unidadManejo,
        idUnidadManejo: 0,
        idUsuarioRegistro: this.usuario.idusuario,
        estado: "A",
        idPlanManejo: this.planManejo.idPlanManejo,
        idDistrito: this.distrito.idDistrito,
      }

      this.informacionAreaManejoService.registrarUnidadManejo(this.unidadManejo).subscribe((result: any) => {
        if (result.success) {
          this.SuccessMensaje(result.message);
          this.obtenerUnidadManejoId();

        } else {
          this.WarningMessage(result.message);
        }
      }, errr => {
        this.WarningMessage(errr);
      })

    }
    */
  }

  /*agregarListaAccesibilidad1() {

    let infoModelBasicaDetalle_4_4_1:InformacionBasicaDetalleModel = new InformacionBasicaDetalleModel({
      idInfBasicaDet :0,
      codInfBasicaDet: this.codigoAcordeonAccesibilidad,
      codSubInfBasicaDet : "TAB_1",
      idUsuarioRegistro : this.user.idUsuario
    });

    this.listInfoBasicaDet_4_4_1.push(infoModelBasicaDetalle_4_4_1);
  }

  agregarListaAccesibilidad2() {
    let infoModelBasicaDetalle_4_4_2:InformacionBasicaDetalleModel = new InformacionBasicaDetalleModel({
      idInfBasicaDet :0,
      codInfBasicaDet: this.codigoAcordeonAccesibilidad,
      codSubInfBasicaDet : "TAB_2",
      idUsuarioRegistro : this.user.idUsuario
    });
    this.listInfoBasicaDet_4_4_2.push(infoModelBasicaDetalle_4_4_2);
  }

   */

  agregarDetalle(acordeon: string, tipoTab: string) {
    if (acordeon == this.codigoAcordeonAspectosFisicos) {
      if (tipoTab == this.codigoTabla1) {
        this.listInfoBasicaDet_4_5_1.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosFisicos,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      } else if (tipoTab == this.codigoTabla2) {
        this.listInfoBasicaDet_4_5_2.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosFisicos,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
            observaciones: '3. Otros Especificar',
          })
        );

        
      }
    } else if (acordeon == this.codigoAcordeonAccesibilidad) {
      if (tipoTab == this.codigoTabla1) {
        this.listInfoBasicaDet_4_4_1.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAccesibilidad,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      } else if (tipoTab == this.codigoTabla2) {
        this.listInfoBasicaDet_4_4_2.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAccesibilidad,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      }
    } else if (acordeon == this.codigoAcordeonAspectosSocioeconomico) {
      if (tipoTab == this.codigoTabla1) {
        this.listInfoBasicaDet_4_7_1.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      } else if (tipoTab == this.codigoTabla2) {
        this.listInfoBasicaDet_4_7_2.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      } else if (tipoTab == this.codigoTabla3) {
        this.listInfoBasicaDet_4_7_3.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      } else if (tipoTab == this.codigoTabla4) {
        this.listInfoBasicaDet_4_7_4.push(
          new InformacionBasicaDetalleModel({
            idInfBasicaDet: 0,
            codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
            codSubInfBasicaDet: tipoTab,
            idUsuarioRegistro: this.user.idUsuario,
          })
        );
      }
    }
  }

  eliminarItemInformacionAreaManejo(
    event: any,
    item: InformacionBasicaDetalleModel,
    index: number,
    acordeon: string,
    tipo: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (acordeon == this.codigoAcordeonAccesibilidad) {
          if (tipo == this.codigoTabla1) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_4_1.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_4_1.splice(index, 1);
                  //this.listarUbicacionPolitica();
                });
            }
          } else if (tipo == this.codigoTabla2) {
            //this.listInfoBasicaDet_4_4_2.splice(index,1);

            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_4_2.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_4_2.splice(index, 1);
                });
            }
          }
        } else if (acordeon == this.codigoAcordeonAspectosFisicos) {
          if (tipo == this.codigoTabla1) {
            //this.listInfoBasicaDet_4_5_1.splice(index,1);
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_5_1.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_5_1.splice(index, 1);
                });
            }
          } else if (tipo == this.codigoTabla2) {
            //this.listInfoBasicaDet_4_5_2.splice(index,1);

            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_5_2.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_5_2.splice(index, 1);
                });
            }
          }
        } else if (acordeon == this.codigoAcordeonAspectosBiologicos) {
          if (tipo == this.codigoTabla3) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_6_3.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_6_3.splice(index, 1);
                });
            }

            this.obtenerSerforTipoBosque();
            //this.listarTipoDeBosque();
          }
        } else if (acordeon == this.codigoAcordeonAspectosSocioeconomico) {
          if (tipo == this.codigoTabla1) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_7_1.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_7_1.splice(index, 1);
                });
            }
          } else if (tipo == this.codigoTabla2) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_7_2.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_7_2.splice(index, 1);
                });
            }
          } else if (tipo == this.codigoTabla3) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_7_3.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_7_3.splice(index, 1);
                });
            }
          } else if (tipo == this.codigoTabla4) {
            if (item.idInfBasicaDet == 0) {
              this.toast.ok('Se eliminó el registró correctamente.');
              this.listInfoBasicaDet_4_7_4.splice(index, 1);
            } else {
              this.dialog.open(LoadingComponent, { disableClose: true });
              this.informacionAreaService
                .eliminarInformacionBasica({
                  idInfBasica: 0,
                  codInfBasicaDet: '',
                  idInfBasicaDet: item.idInfBasicaDet,
                  idUsuarioElimina: this.usuario.idusuario,
                })
                .pipe(finalize(() => this.dialog.closeAll()))
                .subscribe((result: any) => {
                  this.toast.ok('Se eliminó el registró correctamente.');
                  this.listInfoBasicaDet_4_7_4.splice(index, 1);
                });
            }
          }
        }
      },
      reject: () => {},
    });
  }

  listarAcreditacion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_1: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAcreditacion,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_1)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data && response.data.length > 0) {
          let list_InfoBasicDetalle4_1 = response.data.filter(
            (x: any) => (x.codInfBasicaDet = this.codigoAcordeonAcreditacion)
          );

          if (list_InfoBasicDetalle4_1 && list_InfoBasicDetalle4_1.length > 0) {
            let infoBasic4_1 = list_InfoBasicDetalle4_1[0];
            Object.assign(this.infoBasica_4_1, infoBasic4_1);
            Object.assign(this.infoBasicaDet_4_1, infoBasic4_1);
          } else {
            this.getAcreditacionGeneral();
          }
        } else {
          this.getAcreditacionGeneral();
        }
      });
  }

  getAcreditacionGeneral() {
    let numeroDoc = '';
    let nroRucEmp = '';
    if (this.user.idtipoDocumento == 4) {
      nroRucEmp = this.user.nroDocumento;
    } else {
      numeroDoc = this.user.nroDocumento;
    }
    let params4_1: any = {
      nroDocumento: numeroDoc,
      nroRucEmpresa: nroRucEmp,
      idPlanManejo: this.idPlanManejo
    };

    this.informacionGeneralService
      .listarPorFiltroOtorgamiento(params4_1)
      .subscribe((response: any) => {
        let data = response.data[0];

        const infoBasic4_1 = new InformacionBasicaDetalleModel({
          idInfBasicaDet: 0,
          codInfBasicaDet: this.codigoAcordeonAcreditacion,
          codSubInfBasicaDet: this.codigoTabla1,
          referencia: 'Número de título',
          observaciones: 'Área titulada',
          descripcion: data.nroTituloPropiedad,
          areaHa: data.areaComunidad,
          idUsuarioRegistro: this.user.idUsuario,
        });

        Object.assign(this.infoBasicaDet_4_1, infoBasic4_1);

        
      });
  }

  listarUbicacionPolitica() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_1: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonUbicacionPolitica,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_1)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          //this.toast.ok("Se listó la Ubicación Política correctamente.");
          let list_InfoBasicDetalle4_2 = response.data.filter(
            (x: any) =>
              (x.codInfBasicaDet = this.codigoAcordeonUbicacionPolitica)
          );
          if (list_InfoBasicDetalle4_2 && list_InfoBasicDetalle4_2.length > 0) {
            let infoBasic4_2 = list_InfoBasicDetalle4_2[0];
            infoBasic4_2.idDepartamento = Number.parseInt(
              infoBasic4_2.departamento
            );
            this.onSelectedProvincia({ value: infoBasic4_2.idDepartamento });
            infoBasic4_2.idProvincia = Number.parseInt(infoBasic4_2.provincia);
            this.onSelectedDistrito({ value: infoBasic4_2.idProvincia });
            infoBasic4_2.idDistrito = Number.parseInt(infoBasic4_2.distrito);
            this.idCuenca = isNullOrEmpty(infoBasic4_2.cuenca) ? null : Number(infoBasic4_2.cuenca);
            this.onSelectedCuenca({ value: this.idCuenca });
            this.idSubcuenca = isNullOrEmpty(infoBasic4_2.subCuenca) ? null : Number(infoBasic4_2.subCuenca);
            Object.assign(this.infoBasica_4_2, infoBasic4_2);
          }
        }
      });
  }

  listarAccesibilidad() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_4: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAccesibilidad,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_4)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          // this.toast.ok("Se listó la Accesibilidad correctamente.");
          let list_InfoBasicDetalle4_4 = response.data.filter(
            (x: any) => (x.codInfBasicaDet = this.codigoAcordeonAccesibilidad)
          );
          if (list_InfoBasicDetalle4_4 && list_InfoBasicDetalle4_4.length > 0) {
            let infoBasic4_4 = list_InfoBasicDetalle4_4[0];
            Object.assign(this.infoBasica_4_4, infoBasic4_4); //SE TOMA EL PRIMERO PARA LA CABECERA
            this.listInfoBasicaDet_4_4_1 = list_InfoBasicDetalle4_4.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla1
            );
            this.listInfoBasicaDet_4_4_2 = list_InfoBasicDetalle4_4.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla2
            );
          }
        }
      });
  }

  listarAspectosFisicos() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_5: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAspectosFisicos,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_5)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          // this.toast.ok("Se listó los Aspectos Físicos correctamente.");
          let list_InfoBasicDetalle4_5 = response.data.filter(
            (x: any) => (x.codInfBasicaDet = this.codigoAcordeonAspectosFisicos)
          );
          if (list_InfoBasicDetalle4_5 && list_InfoBasicDetalle4_5.length > 0) {
            let infoBasic4_4 = list_InfoBasicDetalle4_5[0];
            Object.assign(this.infoBasica_4_5, infoBasic4_4); //SE TOMA EL PRIMERO PARA LA CABECERA
            this.listInfoBasicaDet_4_5_1 = list_InfoBasicDetalle4_5.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla1
            );
            this.listInfoBasicaDet_4_5_2 = list_InfoBasicDetalle4_5.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla2
            );
            this.getGeneralFiltersAccordeon4_6();
          }
        } else {
          this.getGeneralFiltersAccordeon4_6();
        }
      });
  }

  getGeneralFiltersAccordeon4_6() {
    if (!this.listInfoBasicaDet_4_5_2.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: 'TAB_2',
        codCabecera: this.codigoAcordeonAspectosFisicos,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: 0,
              codInfBasicaDet: this.codigoAcordeonAspectosFisicos,
              codSubInfBasicaDet: 'TAB_2',
              idUsuarioRegistro: this.user.idUsuario,
              observaciones: element.observaciones,
              areaHa: element.areaHa,
              descripcion: element.descripcion,
              referencia: element.referencia,
            });

            

            this.listInfoBasicaDet_4_5_2.push(obj);
          });

          // = data;
          
        });
    }
  }

  registrarAccesibilidad() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_4_4.idPlanManejo = this.idPlanManejo;
    this.infoBasica_4_4.listInformacionBasicaDet = [];
    this.infoBasica_4_4.listInformacionBasicaDet =
      this.listInfoBasicaDet_4_4_1.concat(this.listInfoBasicaDet_4_4_2);
    this.informacionAreaService
      .registrarInformacionBasica([this.infoBasica_4_4])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró la Accesibilidad correctamente.');
        this.listarAccesibilidad();
      });
  }

  registrarAspectosFisicos(array: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.infoBasica_4_5.idPlanManejo = this.idPlanManejo;
    this.infoBasica_4_5.listInformacionBasicaDet = [];

    this.listInfoBasicaDet_4_5_1.forEach(
      (det: InformacionBasicaDetalleModel) => {
        let rio = this.cmbRios.find(
          (element) => element.idHidrografia == det.idRios
        );
        if (rio) det.nombreRio = rio.descripcion;
      }
    );

    this.infoBasica_4_5.listInformacionBasicaDet = array;
    // this.listInfoBasicaDet_4_5_1.concat(this.listInfoBasicaDet_4_5_2);

    this.informacionAreaService
      .registrarInformacionBasica([this.infoBasica_4_5])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró los Aspectos Físicos correctamente.');
        this.listarAspectosFisicos();
      });
  }

  registrarArchivosVertices() {
    let params = {
      idUnidadManejo: this.unidadManejo.idUnidadManejo,
      idUsuarioRegistro: this.usuario.idusuario,
    };

    this.informacionAreaManejoService
      .registrarUnidadManejoArchivo(
        params,
        this.fileVertices,
        this.fileParcelas,
        this.fileBosques
      )
      .subscribe((result) => {
        
      });
  }

  obtenerUnidadManejoId() {
    this.informacionAreaManejoService
      .obtenerUnidadManejo(this.planManejo)
      .subscribe((result: any) => {
        this.unidadManejo = result.data;
        this.registrarArchivosVertices();
      });
  }

  /************************* */
  registroInfoEconomicaPgmf() {
    if (!this.validarRegistroInformacionEco()) return;

    this.infoEconomica.estado = 'A';
    this.infoEconomica.idInformacionSocioEconomica = this.infoEconomica
      .idInformacionSocioEconomica
      ? this.infoEconomica.idInformacionSocioEconomica
      : 0;
    this.infoEconomica.idUsuarioRegistro = this.usuario.idusuario;
    this.infoEconomica.idPlanManejo = this.planManejo.idPlanManejo;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ
      .registroInfoEconomicaPgmf(this.infoEconomica)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            ///this.listarAccesibilidadmanejo();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  }
  obtenerInfoEconomicaPgmf() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ.obtenerInfoEconomicaPgmf(this.planManejo).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.data) {
          this.infoEconomica = result.data;
          this.obtenerInfoEconomicaActEconomicaPgmf();
          this.obtenerInfoEconomicaInfraestructuraPgmf();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }

  registroInfoEconomicaActEconomicaPgmf() {
    this.listActividadesEconomicas.forEach((data) => {
      data.idInfoeconomicaActiEconomica = data.idInfoeconomicaActiEconomica
        ? data.idInfoeconomicaActiEconomica
        : 0;
      data.idInformacionSocioEconomica =
        this.infoEconomica.idInformacionSocioEconomica;
      data.estado = 'A';
      data.idUsuarioRegistro = this.usuario.idusuario;
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ
      .registroInfoEconomicaActEconomicaPgmf(this.listActividadesEconomicas)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.isSuccess) {
            this.SuccessMensaje(result.message);
            this.obtenerInfoEconomicaActEconomicaPgmf();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  }
  obtenerInfoEconomicaActEconomicaPgmf() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ
      .obtenerInfoEconomicaActEconomicaPgmf(this.infoEconomica)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.data) {
            this.listActividadesEconomicas = result.data;
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  }
  EliminarInfoEconomicaActEconomicaPgmf(
    event: any,
    index: number,
    data: PlanManejoActEconomica
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idInfoeconomicaActiEconomica) {
          let params = {
            idInfoeconomicaActiEconomica: data.idInfoeconomicaActiEconomica,
            idUsuarioElimina: this.usuario.idusuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.aspSocioEcoServ
            .EliminarInfoEconomicaActEconomicaPgmf(params)
            .subscribe(
              (result: any) => {
                this.dialog.closeAll();
                if (result.isSuccess) {
                  this.SuccessMensaje(result.message);
                  this.listActividadesEconomicas.splice(index, 1);
                }
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
                this.ErrorMensaje(error.message);
              }
            );
        } else {
          this.listActividadesEconomicas.splice(index, 1);
        }
      },
    });
  }

  registroInfoEconomicaActividadesPgmf() {
    let data: any;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ.registroInfoEconomicaActividadesPgmf(data).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(result.message);
          ///this.listarAccesibilidadmanejo();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }
  obtenerInfoEconomicaActividadesPgmf() {
    let data: any;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ.obtenerInfoEconomicaActividadesPgmf(data).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(result.message);
          ///this.listarAccesibilidadmanejo();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }

  registroInfoEconomicaInfraestructuraPgmf() {
    if (!this.validarRegistroInfraestructura()) return;

    this.listInfraestructura.forEach((data) => {
      data.idInformacionSocioEconomica = data.idInformacionSocioEconomica
        ? data.idInformacionSocioEconomica
        : 0;
      data.idInformacionSocioEconomica =
        this.infoEconomica.idInformacionSocioEconomica;
      data.estado = 'A';
      data.idUsuarioRegistro = this.usuario.idusuario;
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ
      .registroInfoEconomicaInfraestructuraPgmf(this.listInfraestructura)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.isSuccess) {
            this.SuccessMensaje(result.message);
            this.obtenerInfoEconomicaInfraestructuraPgmf();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  }
  obtenerInfoEconomicaInfraestructuraPgmf() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ
      .obtenerInfoEconomicaInfraestructuraPgmf(this.infoEconomica)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.isSuccess) {
            this.listInfraestructura = result.data;
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  }
  EliminarInfoEconomicaInfraestructuraPgmf() {
    let data: any;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ
      .EliminarInfoEconomicaInfraestructuraPgmf(data)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            ///this.listarAccesibilidadmanejo();
          }
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
          this.ErrorMensaje(error.message);
        }
      );
  }

  registroInfoEconomicaConfictosPgmf() {
    let data: any;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ.registroInfoEconomicaConfictosPgmf(data).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(result.message);
          ///this.listarAccesibilidadmanejo();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }
  obtenerInfoEconomicaConfictosPgmf() {
    let data: any;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ.obtenerInfoEconomicaConfictosPgmf(data).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(result.message);
          ///this.listarAccesibilidadmanejo();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }
  EliminarInfoEconomicaConfictosPgmf() {
    let data: any;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.aspSocioEcoServ.EliminarInfoEconomicaConfictosPgmf(data).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.SuccessMensaje(result.message);
          ///this.listarAccesibilidadmanejo();
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }

  guardarSeccion(data: any, Seccion: any) {
    if (Seccion == 1) {
    }
    if (Seccion == 2) {
    }
    if (Seccion == 3) {
    }
    if (Seccion == 4) {
    }
  }

  validarRegistroInformacionEco(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.infoEconomica.peronasTrabajan) {
      validar = false;
      mensaje = mensaje += '(*) Registre Numero personas que trabajan\n';
    }

    if (!this.infoEconomica.personasEmpadronada) {
      validar = false;
      mensaje = mensaje += '(*) Registre Numero personas empadronadas\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  abrirModalTipoBosque() {
    //this.selecEspeciesFauna = {} as AccesibilidadVias;
    //this.selectedValues = this.selecFaunas;
    let yFilter = this.listInfoBasicaDet_4_6_3.map((itemY) => {
      return itemY.idFlora;
    });
    this.comboListBosques = this.comboListBosques.filter(
      (item) => !yFilter.includes(item.idTipoBosque)
    );
    this.verModalBosque = true;
  }

  obtenerSerforTipoBosque() {
    let params = {};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servCoreCentral
      .listarPorFiltroTipoBosque(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.comboListBosques = [...result.data];

        let yFilter = this.listInfoBasicaDet_4_6_3.map((itemY) => {
          return itemY.idFlora;
        });
        this.comboListBosques = this.comboListBosques.filter(
          (item) => !yFilter.includes(item.idTipoBosque)
        );
      });
  }

  filtrarBosque() {
    if (this.queryBosque) {
      this.comboListBosques = this.comboListBosques.filter((r) =>
        r.descripcion.toLowerCase().includes(this.queryBosque.toLowerCase())
      );
    } else {
      this.obtenerSerforTipoBosque();
    }
  }

  guardarBosqueDesdeModal() {
    
    //this.listInfoBasicaDet_4_6_3 = [];

    //this.listInfoBasicaDet_4_6_3 = [...this.selectedValues];
    // this.seleccionarNuevoaFauna();

    this.selectedValues.forEach((item: any) => {
      this.listInfoBasicaDet_4_6_3.push(
        new InformacionBasicaDetalleModel({
          idInfBasicaDet: 0,
          codInfBasicaDet: this.codigoAcordeonAspectosBiologicos,
          codSubInfBasicaDet: this.codigoTabla3,
          idUsuarioRegistro: this.user.idUsuario,
          idFlora: item.idTipoBosque,
          descripcion: item.descripcion,
          zonaVida: item.region,
          nombreComun: item.descripcion,
          familia: item.region,
        })
      );
    });

    let yFilter = this.listInfoBasicaDet_4_6_3.map((itemY) => {
      return itemY.idFlora;
    });
    this.comboListBosques = this.comboListBosques.filter(
      (item) => !yFilter.includes(item.idTipoBosque)
    );

    this.selectedValues = [];
    this.verModalBosque = false;
  }

  registrarTipoBosque() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_4_6.idPlanManejo = this.idPlanManejo;
    this.infoBasica_4_6.listInformacionBasicaDet = [];
    this.infoBasica_4_6.listInformacionBasicaDet = this.listInfoBasicaDet_4_6_3;
    this.informacionAreaService
      .registrarInformacionBasica([this.infoBasica_4_6])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró los Tipos de Bosque correctamente.');
        this.listarTipoDeBosque();
      });
  }

  // registrarAspectosSocieconomicos() {
  //   this.dialog.open(LoadingComponent, { disableClose: true });
  //   this.infoBasica_4_7.idPlanManejo = this.idPlanManejo;
  //   this.infoBasica_4_7.listInformacionBasicaDet = [];
  //   this.infoBasica_4_7.listInformacionBasicaDet = [
  //     this.infoBasicaDet_4_7_cab,
  //   ].concat(
  //     this.listInfoBasicaDet_4_7_1,
  //     this.listInfoBasicaDet_4_7_2,
  //     this.listInfoBasicaDet_4_7_3,
  //     this.listInfoBasicaDet_4_7_4
  //   );

  //   this.informacionAreaService
  //     .registrarInformacionBasica([this.infoBasica_4_7])
  //     .pipe(finalize(() => this.dialog.closeAll()))
  //     .subscribe((result: any) => {
  //       this.toast.ok(
  //         'Se registró los Aspectos Socioeconómicos correctamente.'
  //       );
  //       this.listarAspectosSocieconomicos();
  //     });
  // }

  registrarAspectosSocieconomicos(array: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.infoBasica_4_7.idPlanManejo = this.idPlanManejo;
    this.infoBasica_4_7.listInformacionBasicaDet = [];
    this.infoBasica_4_7.listInformacionBasicaDet = [
      this.infoBasicaDet_4_7_cab,
    ].concat(array);

    this.informacionAreaService
      .registrarInformacionBasica([this.infoBasica_4_7])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok(
          'Se registró los Aspectos Socioeconómicos correctamente.'
        );
        this.listarAspectosSocieconomicos();
      });
  }

  listarAspectosSocieconomicos() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_5: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAspectosSocioeconomico,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_5)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          //this.toast.ok("Se listó los Aspectos Físicos correctamente.");
          let list = response.data.filter(
            (x: any) =>
              (x.codInfBasicaDet = this.codigoAcordeonAspectosSocioeconomico)
          );
          if (list && list.length > 0) {
            let infoBasica = list[0];
            Object.assign(this.infoBasica_4_7, infoBasica);
            //this.listInfoBasicaDet_4_7_1 = list.filter((x: any) => x.codSubInfBasicaDet == this.codigoTabla1);

            let listCabecera = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTablaCabecera
            );
            if (listCabecera && listCabecera.length > 0) {
              this.infoBasicaDet_4_7_cab = listCabecera[0];
            }

            this.listInfoBasicaDet_4_7_1 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla1
            );
            this.listInfoBasicaDet_4_7_2 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla2
            );

            this.listInfoBasicaDet_4_7_2.forEach((eve: any) => {
              eve.coordenadaEsteIni = eve.coordenadaEste;
              eve.coordenadaNorteIni = eve.coordenadaNorte;
            });

            this.listInfoBasicaDet_4_7_3 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla3
            );

            if (
              this.listInfoBasicaDet_4_7_3 &&
              this.listInfoBasicaDet_4_7_3.length == 0
            ) {
              this.listAntecedentesDeUsoBosque.forEach((elem: any) => {
                this.listInfoBasicaDet_4_7_3.push(
                  new InformacionBasicaDetalleModel({
                    idInfBasicaDet: 0,
                    codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
                    codSubInfBasicaDet: this.codigoTabla3,
                    idUsuarioRegistro: this.user.idUsuario,
                    descripcion: elem.valorPrimario,
                  })
                );
              });
            }

            this.listInfoBasicaDet_4_7_4 = list.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla4
            );

            this.getGeneralFiltesAccordeon4_7();
          } else {
            this.getGeneralFiltesAccordeon4_7();
          }
        }
      });
  }

  getGeneralFiltesAccordeon4_7() {
    if (!this.listInfoBasicaDet_4_7_1.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: this.codigoTabla1,
        codCabecera: this.codigoAcordeonAspectosSocioeconomico,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: 0,
              codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
              codSubInfBasicaDet: this.codigoTabla1,
              idUsuarioRegistro: this.user.idUsuario,
              descripcion: element.descripcion,
            });

            this.listInfoBasicaDet_4_7_1.push(obj);
          });
        });
    }

    if (!this.listInfoBasicaDet_4_7_2.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: this.codigoTabla2,
        codCabecera: this.codigoAcordeonAspectosSocioeconomico,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: 0,
              codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
              codSubInfBasicaDet: this.codigoTabla2,
              idUsuarioRegistro: this.user.idUsuario,
              descripcion: element.descripcion,
              coordenadaEste: element.coordenadaEste,
              coordenadaNorte: element.coordenadaNorte,
              zonaVida: element.zonaVida,
            });

            this.listInfoBasicaDet_4_7_2.push(obj);
          });
        });
    }

    if (!this.listInfoBasicaDet_4_7_3.length) {
      let params2: any = {
        idInfBasica: '0',
        codSubTipo: this.codigoTabla3,
        codCabecera: this.codigoAcordeonAspectosSocioeconomico,
      };

      this.informacionAreaPmfiService
        .listarInformacionBasicaDetalle(params2)
        .subscribe((response: any) => {
          response.data.forEach((element: any) => {
            const obj = new InformacionBasicaDetalleModel({
              idInfBasicaDet: 0,
              codInfBasicaDet: this.codigoAcordeonAspectosSocioeconomico,
              codSubInfBasicaDet: this.codigoTabla3,
              idUsuarioRegistro: this.user.idUsuario,
              descripcion: element.descripcion,
              nombreLaguna: element.nombreLaguna,
              referencia: element.referencia,
            });

            this.listInfoBasicaDet_4_7_3.push(obj);
          });
        });
    }
  }

  listarTipoDeBosque() {
    this.selectedValues = [];
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params4_4: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeonAspectosBiologicos,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params4_4)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          //this.toast.ok("Se listó los Tipos de Bosque correctamente.");
          let list_InfoBasicDetalle4_6 = response.data.filter(
            (x: any) =>
              (x.codInfBasicaDet = this.codigoAcordeonAspectosBiologicos)
          );
          if (list_InfoBasicDetalle4_6 && list_InfoBasicDetalle4_6.length > 0) {
            let infoBasic4_6 = list_InfoBasicDetalle4_6[0];
            Object.assign(this.infoBasica_4_6, infoBasic4_6);
            this.listInfoBasicaDet_4_6_3 = list_InfoBasicDetalle4_6.filter(
              (x: any) => x.codSubInfBasicaDet == this.codigoTabla3
            );

            let yFilter = this.listInfoBasicaDet_4_6_3.map((itemY) => {
              return itemY.idFlora;
            });
            this.comboListBosques = this.comboListBosques.filter(
              (item) => !yFilter.includes(item.idTipoBosque)
            );
          }
        }
      });
  }

  validarRegistroActEconomicas(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.listActividadesEconomicas.some(
        (data) => data.activo && !data.descripcion
      )
    ) {
      validar = false;
      mensaje = mensaje += '(*) Registre descripcion actividad\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  validarRegistroInfraestructura(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.listInfraestructura.some(
        (data) =>
          data.activo &&
          !data.referencia &&
          !data.norte &&
          !data.este &&
          !data.zonautm
      )
    ) {
      validar = false;
      mensaje = mensaje += '(*) Todos los campos son obligatorios\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionAcreditacion = Object.assign(
                this.evaluacionAcreditacion,
                this.findEvaluacion(this.codigoAcordeonAcreditacion)
              );
              this.evaluacionUbicacionPolitica = Object.assign(
                this.evaluacionUbicacionPolitica,
                this.findEvaluacion(this.codigoAcordeonUbicacionPolitica)
              );
              this.evaluacionCoordenadasUTM = Object.assign(
                this.evaluacionCoordenadasUTM,
                this.findEvaluacion(this.codigoAcordeonCoordenadasUTM)
              );
              this.evaluacionAccesibilidad = Object.assign(
                this.evaluacionAccesibilidad,
                this.findEvaluacion(this.codigoAcordeonAccesibilidad)
              );
              this.evaluacionAspectosFisicos = Object.assign(
                this.evaluacionAspectosFisicos,
                this.findEvaluacion(this.codigoAcordeonAspectosFisicos)
              );
              this.evaluacionAspectosBiologicos = Object.assign(
                this.evaluacionAspectosBiologicos,
                this.findEvaluacion(this.codigoAcordeonAspectosBiologicos)
              );
              this.evaluacionAspectosSocioeconomico = Object.assign(
                this.evaluacionAspectosSocioeconomico,
                this.findEvaluacion(this.codigoAcordeonAspectosSocioeconomico)
              );
            }
          }
        }
      });
  }

  findEvaluacion(codigoAcorddeon: any) {
    return this.evaluacion.listarEvaluacionDetalle.find(
      (x: any) => x.codigoEvaluacionDetPost == codigoAcorddeon
    );
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacionAcreditacion,
        this.evaluacionUbicacionPolitica,
        this.evaluacionCoordenadasUTM,
        this.evaluacionAccesibilidad,
        this.evaluacionAspectosFisicos,
        this.evaluacionAspectosBiologicos,
        this.evaluacionAspectosSocioeconomico,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionAcreditacion
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionUbicacionPolitica
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionCoordenadasUTM
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionAccesibilidad
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionAspectosFisicos
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionAspectosBiologicos
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionAspectosSocioeconomico
        );

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  openModalTC() {
    this.isShowModalTC = true;
  }

  guardarTC() {}

  listarTipoBosque(items: any) {
    this.listaBosque = [];
    this.listaBosque = items;
    this.calculateAreaTotalBosques();
  }

  calculateAreaTotalBosques() {  
    let sum1 = 0;
    for (let item of this.listaBosque) {
      item.tipoBosque.forEach((t: any) => {
        sum1 += t.areaHA;
      });
    }

    for (let item of this.listaBosque) {
      item.tipoBosque.forEach((t: any) => {
        t.areaHAPorcentaje = Number(((100 * t.areaHA) / sum1).toFixed(2));
      });
    }
    this.totalAreaBosque = parseFloat(`${sum1.toFixed(2)}`);
    this.totalPorcentajeBosque = parseFloat(`${(100).toFixed(2)} %`);
  }

  registrarTiposBosque() {
    
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    let listBosqueDet: any[] = [];
    for (let item of this.listaBosque) {
      this.idInfBasicaBosq = item.idInfBasica,
      item.tipoBosque.forEach((t: any) => {
        let obj = {
          idInfBasicaDet: t.idInfBasicaDet,
          codInfBasicaDet: CodigosTabEvaluacion.PGMFA_TAB_4_6_2,
          areaHa: t.areaHA,
          areaHaPorcentaje: t.areaHAPorcentaje,
          nombre: t.descripcion,
          idTipoBosque: t.idBosque,
          descripcion: t.des,
          observaciones: t.subPc
        }
        listBosqueDet.push(obj);
      });
    }
    let params = [
      {
        idInfBasica: this.idInfBasicaBosq ? this.idInfBasicaBosq : 0,
        codInfBasica: CodigoProceso.PLAN_GENERAL,
        codSubInfBasica: CodigosTabEvaluacion.PGMFA_TAB_4,
        codNombreInfBasica: CodigosTabEvaluacion.PGMFA_TAB_4_6_2,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuario.idusuario,
        listInformacionBasicaDet: listBosqueDet
      },
    ];
    

    this.informacionAreaService
      .registrarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.SuccessMensaje(response?.message);
          this.listarTiposBosque();
        } else {
          this.ErrorMensaje(response?.message);
        }
        this.dialog.closeAll();
      });
  }

  listarTiposBosque() {
    this.listaBosque = [];
    const params = {
      idInfBasica: CodigoProceso.PLAN_GENERAL,
      idPlanManejo: this.idPlanManejo,
      codCabecera: CodigosTabEvaluacion.PGMFA_TAB_4_6_2,
    };

    this.informacionAreaService.listarInformacionBasica(params).subscribe(
      (result: any) => {
        if (result.data) {
         ;
          result.data.forEach((element: any) => {
            const array: any[] = [];
            let objDet = {
              idInfBasicaDet: element.idInfBasicaDet,
              codInfBasicaDet: element.codInfBasicaDet,
              areaHA: element.areaHa,
              areaHAPorcentaje: element.areaHaPorcentaje,
              descripcion: element.nombre,
              idBosque: element.idTipoBosque,
              des: element.descripcion,
              subPc: element.observaciones
            }
            array.push(objDet);
            let item = {
              idInfBasica: element.idInfBasica,
              tipoBosque: array
            }
            this.listaBosque.push(item);
          });
          
          
          this.calculateAreaTotalBosques();
        }
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  };

  eliminarTiposBosque(event: any) {
    if (this.listaBosque.length > 0) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: '¿Está seguro de eliminar los registros?.',
        icon: 'pi pi-exclamation-triangle',
        acceptLabel: 'Sí',
        rejectLabel: 'No',
        accept: () => {
          for (let item of this.listaBosque) {
            this.idInfBasicaBosq = item.idInfBasica;
          }
          
          
          if (this.idInfBasicaBosq != null && this.idInfBasicaBosq != undefined) {
            let params = {
              idInfBasica: this.idInfBasicaBosq,
              idInfBasicaDet: 0,
              codInfBasicaDet: '',
              idUsuarioElimina: this.usuario.idusuario,
            };
            this.informacionAreaService
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                } else {
                  this.ErrorMensaje(result.message);
                }
                this.listarTiposBosque();
              }); 
          } else {
            this.listarTiposBosque();
          }
          
        },
        reject: () => {},
      });
    }
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

  informChange2(event: any) {
    this.tabIndex = event;
  }

  informChange3(event: any) {
    this.tabIndex = event;
  }

  get totalArea() {
    return this.listInfoBasicaDet_4_5_2
      .map((i: any) => i.areaHa)
      .reduce((sum, x) => sum + x, 0);
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export interface CuencaSubcuencaModel { 
  codCuenca: string,
  codSubCuenca: string,
  codigo: string,
  cuenca: string,
  idCuenca: number,
  idSubCuenca: number,
  subCuenca: string,
  valor: string
}
