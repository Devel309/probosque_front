import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { finalize } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";
import { descargarArchivo, ToastService } from "@shared";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { ArchivoService, ParametroValorService, UsuarioService } from "@services";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { PotencialProduccionService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/potencialProduccion.service";
import { PotencialProduccionCabeceraModel, potencialProduccionDetalleModel, VariablesModel } from "src/app/model/potencialProduccionModel";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { HttpErrorResponse } from "@angular/common/http";
import { CensoForestalService } from "src/app/service/censoForestal";
import {environment} from '@env/environment';
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-potencial-produccion-rf",
  templateUrl: "./tab-potencial-produccion-rf.component.html",
  styleUrls: ["./tab-potencial-produccion-rf.component.scss"],
})
export class TabPotencialProduccionRfComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  planManejo = {} as PlanManejoModel;
  usuario = {} as UsuarioModel;

  @Input() isPerfilArffs!: boolean;

  codigoProceso = CodigoProceso.PLAN_GENERAL;
  codigoTab = CodigosTabEvaluacion.PGMFA_TAB_6;

  codigoAcordeonEspeciesMaderables: string =
    CodigosTabEvaluacion.PGMFA_TAB_6_INVENTARIO_MADERABLES;
  codigoAcordeonEspeciesFrutales: string =
    CodigosTabEvaluacion.PGMFA_TAB_6_INVENTARIO_FRUTALES;

  evaluacionEspeciesMaderables: EvaluacionArchivoModel = new EvaluacionArchivoModel(
    {
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonEspeciesMaderables,
    }
  );
  evaluacionEspeciesFrutales: EvaluacionArchivoModel = new EvaluacionArchivoModel(
    {
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonEspeciesFrutales,
    }
  );

  evaluacion: any;

  comboListTipoBosque: any[] = [];
  comboListEspecies: any[] = [];
  comboListVariables: any[] = [];

  comboListVariables2: any[] = [];
  codigoSubTipoPotencialProdForestalA = "PGMFAA";
  codigoSubTipoPotencialProdForestalB = "PGMFAB";
  listBosquesA: any[] = [];
  listBosquesB: any[] = [];
  totalRecordsA = 0;
  totalRecordsB = 0;
  variables: any[] = [{ var: "N" }, { var: "AB m2" }, { var: "Vp m3" }];

  UrlFormatos = UrlFormatos;

  nombreGenerado: string = "";
  plantillaPGMFA: string = "";


  archivo: any;

  constructor(
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private potencialProduccionService: PotencialProduccionService,
    private user: UsuarioService,
    private censoForestalService: CensoForestalService,
    private router: Router,
    private archivoService: ArchivoService
    ) {

      this.archivo = {
        file: null,
      };
  }

  ngOnInit(): void {
    this.obtenerListadoVariables();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
    this.listar();
  }

  listar() {
    this.listBosquesA = [];
    this.listBosquesB = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigo: "PGMFA",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .listarPotencialProduccion(params)
      .pipe(finalize(() => {
          this.listarEspecieMuestreo();
          this.listarEspecieFrutales();
          this.dialog.closeAll();
        }))
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.codigoSubTipoPotencialProdForestal == "PGMFAA") {
            let objA = new PotencialProduccionCabeceraModel(element);
            this.listBosquesA.push(objA);
            this.totalRecordsA = this.listBosquesA.length;
          } else {
            let objB = new PotencialProduccionCabeceraModel(element);
            this.listBosquesB.push(objB);
            this.totalRecordsB = this.listBosquesB.length;
          }
        });
      });
  }

  listarEspecieMuestreo() {
    if (!this.listBosquesA.length) {
      var params = {
        idPlanDeManejo: this.idPlanManejo,
        tipoPlan: "PGMFA"
      };
      this.censoForestalService
        .ResultadosEspecieMuestreo(params)
        .subscribe((response: any) => {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              if (element.listTablaEspeciesMuestreo.length > 0) {
                var objBosque = new PotencialProduccionCabeceraModel();
                objBosque.idTipoBosque = element.idTipoBosque;
                objBosque.tipoBosque = element.nombreBosque;
                objBosque.codigoSubTipoPotencialProdForestal = "PGMFAA";
                objBosque.codigoTipoPotProdForestal = "PGMFA";
                objBosque.idPlanManejo = this.idPlanManejo;
                objBosque.idPotProdForestal = 0;
                objBosque.idUsuarioRegistro = this.user.idUsuario;
                element.listTablaEspeciesMuestreo.forEach((item: any) => {
                  var objEspecie = new potencialProduccionDetalleModel();
                  objEspecie.especie = item.especies;
                  objEspecie.codigoSubTipoPotencialProdForestalDet = "INVESDET";
                  objEspecie.codigoTipoPotencialProdForestalDet = "INVES";
                  objEspecie.idPotProdForestalDet = 0
                  objEspecie.idUsuarioRegistro = this.user.idUsuario;
                  this.variables.forEach((i) => {
                    var objN = new VariablesModel();
                    var objAB = new VariablesModel();
                    var objVP = new VariablesModel();
                    if (i.var == "N") {
                      objN.variable = i.var;
                      objN.nuTotalTipoBosque = item.numeroArbolesTotalBosque;
                      objN.totalHa = item.numeroArbolesTotalHa;
                      objN.idVariable = 0;
                      objN.idUsuarioRegistro = this.user.idUsuario;
                      objEspecie.listPotencialProduccionVariable.push(objN);
                    } else if (i.var == "AB m2") {
                      objAB.variable = i.var;
                      objAB.nuTotalTipoBosque = item.areaBasalTotalBosque;
                      objAB.totalHa = item.areaBasalTotalHa;
                      objAB.idVariable = 0;
                      objAB.idUsuarioRegistro = this.user.idUsuario;
                      objEspecie.listPotencialProduccionVariable.push(objAB);
                    } else if (i.var == "Vp m3") {
                      objVP.variable = i.var;
                      objVP.nuTotalTipoBosque = item.volumenTotalBosque;
                      objVP.totalHa = item.volumenTotalHa;
                      objVP.idVariable = 0;
                      objVP.idUsuarioRegistro = this.user.idUsuario;
                      objEspecie.listPotencialProduccionVariable.push(objVP);
                    }
                  });
                  objBosque.listPotencialProduccion.push(objEspecie);
                });
                this.listBosquesA.push(objBosque);
              }
            });
          }
        });
    }
  }

  listarEspecieFrutales() {
    if (!this.listBosquesB.length) {
      var params = {
        idPlanDeManejo: this.idPlanManejo,
        tipoPlan: "PGMFA"
      };
      this.censoForestalService
        .ResultadosEspecieFrutales(params)
        .subscribe((response: any) => {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              if (element.listTablaEspeciesMuestreo.length > 0) {
                var objBosque = new PotencialProduccionCabeceraModel();
                objBosque.idTipoBosque = element.idTipoBosque;
                objBosque.tipoBosque = element.nombreBosque;
                objBosque.codigoSubTipoPotencialProdForestal = "PGMFAB";
                objBosque.codigoTipoPotProdForestal = "PGMFA";
                objBosque.idPlanManejo = this.idPlanManejo;
                objBosque.idPotProdForestal = 0;
                objBosque.idUsuarioRegistro = this.user.idUsuario;
                element.listTablaEspeciesMuestreo.forEach((item: any) => {
                  var objEspecie = new potencialProduccionDetalleModel();
                  objEspecie.especie = item.especies;
                  objEspecie.codigoSubTipoPotencialProdForestalDet = "INVFRDET";
                  objEspecie.codigoTipoPotencialProdForestalDet = "INVFR";
                  objEspecie.idPotProdForestalDet = 0;
                  objEspecie.idUsuarioRegistro = this.user.idUsuario;
                  this.variables.forEach((i) => {
                    var objN = new VariablesModel();
                    var objAB = new VariablesModel();
                    var objVP = new VariablesModel();
                    if (i.var == "N") {
                      objN.variable = i.var;
                      objN.nuTotalTipoBosque = item.numeroArbolesTotalBosque;
                      objN.totalHa = item.numeroArbolesTotalHa;
                      objN.idVariable = 0;
                      objN.idUsuarioRegistro = this.user.idUsuario;
                      objEspecie.listPotencialProduccionVariable.push(objN);
                    } else if (i.var == "AB m2") {
                      objAB.variable = i.var;
                      objAB.nuTotalTipoBosque = item.areaBasalTotalBosque;
                      objAB.totalHa = item.areaBasalTotalHa;
                      objAB.idVariable = 0;
                      objAB.idUsuarioRegistro = this.user.idUsuario;
                      objEspecie.listPotencialProduccionVariable.push(objAB);
                    }
                  });
                  objBosque.listPotencialProduccion.push(objEspecie);
                });
                this.listBosquesB.push(objBosque);
              }
            });
          }
        });
    }
  }

  obtenerListadoVariables() {
    var params = {
      prefijo: "TVARPMA",
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((result: any) => {
        this.comboListVariables = result.data;
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionEspeciesMaderables = Object.assign(
                this.evaluacionEspeciesMaderables,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost ==
                    this.codigoAcordeonEspeciesMaderables
                )
              );
              this.evaluacionEspeciesFrutales = Object.assign(
                this.evaluacionEspeciesFrutales,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost ==
                    this.codigoAcordeonEspeciesFrutales
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacionEspeciesMaderables,
        this.evaluacionEspeciesFrutales,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionEspeciesMaderables
        );
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionEspeciesFrutales
        );
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  onChange() {
    this.listar();
  }

  cargarFormato(files: any, tipo: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Hoja1",
          numeroFila: 3,
          numeroColumna: 1,
          codigoTipo: "PGMFA",
          codigoSubTipo: "PGMFAA",
          codigoTipoDet: "INVES",
          codigoSubTipoDet: "INVESDET",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario
        };
        if (tipo == 'B') {
          item.codigoSubTipo = "PGMFAB";
          item.codigoTipoDet = "INVFR";
          item.codigoSubTipoDet = "INVFRDET";
        }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.potencialProduccionService
        .registrarPotencialProduccionExcel(t.file, item.nombreHoja, item.numeroFila, item.numeroColumna, item.codigoTipo, item.codigoSubTipo, item.codigoTipoDet, item.codigoSubTipoDet, item.idPlanManejo, item.idUsuarioRegistro)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listar();
            } else {
              this.toast.error(res?.message);
            }
          },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }

  varAssets = `${environment.varAssets}`;
/*   btnDescargarFormato() {
    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }

    window.location.href = urlExcel;
  } */

  btnDescargarFormato() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.nombreGenerado = UrlFormatos.CARGA_MASIVA_PGMFA;
    this.archivoService
      .descargarPlantilla(this.nombreGenerado)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess == true) {
          this.toast.ok(res?.message);
          this.plantillaPGMFA = res;
          descargarArchivo(this.plantillaPGMFA);
        } else {
          this.toast.error(res?.message);
        }
      });
  }


  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService.registrarCargaMasiva(
        this.archivo.file
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok(
            "Se registró el archivo correctamente.\n"
          );
          this.obtenerListadoVariables();
          this.listar();

          this.archivo.file = "";
        } else {
          this.toast.error("Ocurrió un error.");
        }
      });
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
