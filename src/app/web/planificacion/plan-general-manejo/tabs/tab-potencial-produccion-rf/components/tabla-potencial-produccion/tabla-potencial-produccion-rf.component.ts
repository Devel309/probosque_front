import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import {
  potencialProduccionDetalleModel,
  PotencialProduccionCabeceraModel,
  VariablesModel,
} from "src/app/model/potencialProduccionModel";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { DialogTipoBosqueComponent } from "src/app/shared/components/dialog-tipo-bosque/dialog-tipo-bosque.component";
import { DialogTipoEspecieComponent } from "src/app/shared/components/dialog-tipo-especie/dialog-tipo-especie.component";
import { PotencialProduccionService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/potencialProduccion.service";
import { ToastService } from "@shared";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { finalize } from "rxjs/operators";

@Component({
  selector: "tabla-potencial-produccion-rf",
  templateUrl: "./tabla-potencial-produccion-rf.component.html",
  styleUrls: ["./tabla-potencial-produccion-rf.component.scss"],
})
export class TablaPotencialProduccionRfComponent implements OnInit {
  @Input() planManejo = {} as PlanManejoModel;
  @Input() disabled!: boolean;
  @Input() set codigoSubTipoPotencialProdForestal(data: any) {
    if (data == "PGMFAA") {
      this.subCodigo = data;
      this.codigoTipoPotencialProdForestalDet = "INVES";
      this.codigoSubTipoPotencialProdForestalDet = "INVESDET";
    } else {
      this.subCodigo = data;
      this.codigoTipoPotencialProdForestalDet = "INVFR";
      this.codigoSubTipoPotencialProdForestalDet = "INVFRDET";
    }
  }
  @Input() comboListVariables: any[] = [];
  @Input() tipo: number = 0;
  @Input() title: string = "";
  @Input() idPlanManejo!: number;
  @Input() listBosques: any[] = [];
  @Input() totalRecords: number = 0;

  @Output() onChange: EventEmitter<any> = new EventEmitter();

  ref!: DynamicDialogRef;
  especieSelect = {} as potencialProduccionDetalleModel;
  data: any = null;
  codigoTipoPotencialProdForestalDet: string = "";
  codigoSubTipoPotencialProdForestalDet: string = "";
  subCodigo: string = "";

  constructor(
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    public dialogService: DialogService,
    private potencialProduccionService: PotencialProduccionService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {}

  openDialog() {
    this.ref = this.dialogService.open(DialogTipoBosqueComponent, {
      header: "Tipo de Bosque",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });
    this.ref.onClose.subscribe((resp: any) => {
      resp.forEach((element: any) => {
        let objCabecera = new PotencialProduccionCabeceraModel();
        objCabecera.idPotProdForestal = 0;
        objCabecera.idPlanManejo = this.idPlanManejo;
        objCabecera.codigoTipoPotProdForestal = "PGMFA";
        objCabecera.tipoBosque = element.descripcion;
        objCabecera.idTipoBosque = element.idTipoBosque;
        objCabecera.idUsuarioRegistro = this.user.idUsuario;
        objCabecera.codigoSubTipoPotencialProdForestal = this.subCodigo;

        objCabecera.listPotencialProduccion = [];
        this.listBosques.push(objCabecera);
        this.totalRecords = this.listBosques.length;
      });
    });
  }

  buscarEspecie(data: any, index: number) {
    this.ref = this.dialogService.open(DialogTipoEspecieComponent, {
      header: "Especies",
      width: "70%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        invalid: false,
      },
    });
    if (this.subCodigo == "PGMFAA") {
      this.ref.onClose.subscribe((resp: any) => {
        this.especieSelect = new potencialProduccionDetalleModel(resp);
        this.especieSelect.idPotProdForestalDet = 0;
        this.especieSelect.codigoTipoPotencialProdForestalDet = this.codigoTipoPotencialProdForestalDet;
        this.especieSelect.especie = resp.nombreCientifico;
        this.especieSelect.idEspecie = resp.idEspecie;
        this.especieSelect.idUsuarioRegistro = this.user.idUsuario;
        this.especieSelect.codigoSubTipoPotencialProdForestalDet = this.codigoSubTipoPotencialProdForestalDet;
        this.especieSelect.listPotencialProduccionVariable = [];
        this.comboListVariables.forEach((response) => {
          let obj = new VariablesModel();
          obj.variable = response.valorPrimario;
          obj.totalHa = 0;
          obj.nuTotalTipoBosque = 0;
          obj.accion = 1;
          obj.idUsuarioRegistro = this.user.idUsuario;
          this.especieSelect.listPotencialProduccionVariable.push(obj);
        });
        this.listBosques[index].listPotencialProduccion.push(
          this.especieSelect
        );
      });
    } else {
      this.ref.onClose.subscribe((resp: any) => {
        this.especieSelect = new potencialProduccionDetalleModel(resp);
        this.especieSelect.idPotProdForestalDet = 0;
        this.especieSelect.codigoTipoPotencialProdForestalDet = this.codigoTipoPotencialProdForestalDet;
        this.especieSelect.especie = resp.nombreCientifico;
        this.especieSelect.idEspecie = resp.idEspecie;
        this.especieSelect.idUsuarioRegistro = this.user.idUsuario;
        this.especieSelect.codigoSubTipoPotencialProdForestalDet = this.codigoSubTipoPotencialProdForestalDet;
        this.especieSelect.listPotencialProduccionVariable = [];
        let obj = new VariablesModel();
        obj.variable = "N";
        obj.totalHa = 0;
        obj.nuTotalTipoBosque = 0;
        obj.accion = 1;
        obj.idUsuarioRegistro = this.user.idUsuario;
        let objAB = new VariablesModel();
        objAB.variable = "AB m2";
        objAB.totalHa = 0;
        objAB.nuTotalTipoBosque = 0;
        objAB.accion = 1;
        objAB.idUsuarioRegistro = this.user.idUsuario;
        this.especieSelect.listPotencialProduccionVariable.push(obj, objAB);

        this.listBosques[index].listPotencialProduccion.push(
          this.especieSelect
        );
      });
    }
  }

  eliminarMaderables(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idPotProdForestal != 0) {
          var params = {
            idPotProdForestal: data.idPotProdForestal,
            idPotProdForestalDet: 0,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.potencialProduccionService
            .eliminarPotencialProducForestal(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.listBosques.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listBosques.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }
  eliminarEspecie(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idPotProdForestalDet != 0) {
          var params = {
            idPotProdForestal: 0,
            idPotProdForestalDet: data.idPotProdForestalDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.potencialProduccionService
            .eliminarPotencialProducForestal(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.listBosques.forEach((response) => {
                  response.listPotencialProduccion.splice(index, 1);
                });
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listBosques.forEach((response) => {
            response.listPotencialProduccion.splice(index, 1);
          });
        }
      },
      reject: () => {},
    });
  }
  GuardarEvaluacionInventarioEspecies() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.potencialProduccionService
      .registrarPotencialProduccion(this.listBosques)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          if (this.subCodigo == "PGMFAA") {
            this.toast.ok('Se registró el cuadro de resultados de inventario de especies maderables por muestreo.');
          } else {
            this.toast.ok('Se registró el cuadro de resultados de inventario de frutales.');
          }
          this.onChange.emit();
        } else {
          this.toast.error(response?.message);
        }
      });
  }
}
