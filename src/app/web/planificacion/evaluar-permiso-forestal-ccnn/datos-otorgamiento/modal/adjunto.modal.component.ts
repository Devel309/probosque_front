import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatDialog} from "@angular/material/dialog";
import {LoadingComponent} from 'src/app/components/loading/loading.component';
import {SolicitudModel} from "src/app/model/Solicitud";
import {SolicitudCargaArchivoModel} from "src/app/model/SolicitudArchivo";
import {SolicitudService} from "src/app/service/solicitud.service";
import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

//import { CapacitacionDetalleMaderableModel } from 'src/app/model/CapacitacionMaderableModel';

@Component({
  selector: 'adjunto-modal',
  templateUrl: './adjunto.modal.component.html',
  styleUrls: ['./adjunto.modal.component.scss']
})
export class AdjuntoModalComponent {
  @Input('solicitudCargaArchivo') solicitudCargaArchivo!: SolicitudCargaArchivoModel;
  @Input('solicitud') solicitud: any = {} as SolicitudModel;
  ///detalle: CapacitacionDetalleMaderableModel = new CapacitacionDetalleMaderableModel();
  labelBtn: string = '';
  archivoDeclaracionJurada: string = '';
  solArchivoDecJur: any = {} as SolicitudCargaArchivoModel;
  idArchivoSolDeclaracionJurada: string = '';

  constructor(
    private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private dialog: MatDialog,
    private servSol: SolicitudService,
  ) {}

  ngOnInit() {}

  onFileSelectedDC(event: any) {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.solArchivoDecJur.idSolicitud = 0;
    this.solArchivoDecJur.tipoArchivo = 'DECJUR';
    this.solArchivoDecJur.archivo = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoDecJur = this.solArchivoDecJur;
    this.archivoDeclaracionJurada = event.target.files[0].name;
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  obtenerNombresArchivos() {
    this.servSol.obtenerArchivoSolicitud(this.solicitud.idSolicitud).subscribe(
      (response: any) => {
        for (let item of response) {
          if (item.tipoDocumento == 'DECJUR') {
            this.archivoDeclaracionJurada = item.nombreArchivo.concat('.', item.extensionArchivo);
            this.idArchivoSolDeclaracionJurada = item.idArchivoSolicitud;
          }
        }
      });
  }


  cancelar() {
    this.ref.close();
  }
}