import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {ParametroValorService} from 'src/app/service/parametro.service';
import {DepartamentoModel} from "src/app/model/Departamento";
import {ProvinciaModel} from "src/app/model/Provincia";
import {DistritoModel} from 'src/app/model/Distrito';
import {CoreCentralService} from 'src/app/service/coreCentral.service';

import {LoadingComponent} from 'src/app/components/loading/loading.component';
import {MatDialog} from "@angular/material/dialog";
import {SolicitudAccesoService} from "../../../../../service/solicitudAcceso.service";
import {DownloadFile} from "@shared";

@Component({
  selector: 'solicitud-permiso-forestal-general',
  templateUrl: './solicitud-permiso-forestal-general.component.html',
  styleUrls: ['./solicitud-permiso-forestal-general.component.scss']
})
export class SolicitudPermisoForestalGeneral implements OnInit {
  constructor(
    private serv: ParametroValorService,
    private servCoreCentral: CoreCentralService,
    private servSA: SolicitudAccesoService,
    private dialog: MatDialog,
    private router: Router) {
  }

  //Parametro
  departamento = {} as DepartamentoModel;
  listDepartamento: DepartamentoModel[] = [];
  provincia = {} as ProvinciaModel;
  listProvincia: ProvinciaModel[] = [];
  distrito = {} as DistritoModel;
  listDistrito: DistritoModel[] = [];
  lstEstado: any[] = [];
  lstTipoPersona: any[] = [];
  lstTipoActor: any[] = [];
  lstTipoCNCC: any[] = [];

  @Input('dataBase') dataBase: any = {disabled: false, tipoPersona: ''};
  @Input('solicitudAcceso') solicitudAcceso: any = {};
 
  @Output() asignarArchivo = new EventEmitter<File>();
  file!: File;
  fileName: string = '';



  ngOnInit(): void {
    this.solicitudAcceso.tipoPersonaAprobado = 'false';
    this.solicitudAcceso.tipoActorAprobado = 'false';
    this.solicitudAcceso.tipoCnccAprobado = 'false';
     this.solicitudAcceso.departamentoAprobado = 'true';
    this.solicitudAcceso.provinciaAprobado = 'true';
    this.solicitudAcceso.distritoAprobado = 'true';
    this.solicitudAcceso.sustentoAprobado = 'true';

    this.listarEstado();
    this.listarTipoPersona();
    this.listarTipoActor();
    this.listarTipoCNCC();
    this.listarPorFiltroDepartamento(this.departamento);
    this.onGetNombreArchivo();
  }




  listarEstado() {
    var params = {prefijo: 'ESAC'}
    //acá debe obtener de tabla estados
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstEstado = result.data;
      }
    );
  };

  listarTipoPersona() {
    var params = {prefijo: 'TPER'}
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoPersona = result.data;
      }
    );
  };

  listarTipoActor() {
    var params = {prefijo: 'TACT'}
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoActor = result.data;
      }
    );
  };

  listarTipoCNCC() {
    var params = {prefijo: 'TCNC'}
    this.serv.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoCNCC = result.data;
      }
    );
  };

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        

        this.listarPorFilroProvincia(this.provincia);
      }, (error: any) => {
        //this.messageService.add({severity:"warn", summary: "", detail: error.message});
      }
    );
  };

  listarPorFilroProvincia(provincia: ProvinciaModel) {
    this.servCoreCentral.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      }
    );
  };

  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral.listarPorFilroDistrito(distrito).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
      }
    );
  };

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral.listarPorFilroProvincia(provincia).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
      }
    );
  };

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral.listarPorFilroDistrito(distrito).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
      }
    );

  };

  onGetNombreArchivo(): void {
    if (this.solicitudAcceso.idSolicitudAcceso) {
      this.servSA.obtenerArchivoSolicitud(this.solicitudAcceso.idSolicitudAcceso).subscribe(
        (result: any) => {

          this.fileName = result.data.nombre.concat('.', result.data.extension);
        });
    }
  }

  onDescargarArchivo(): void {
    this.dialog.open(LoadingComponent, {disableClose: true});
    this.servSA.descargarArchivoSolicitudAcceso(this.solicitudAcceso.idSolicitudAcceso).subscribe(
      (data: any) => {
        let archive: string = this.fileName;
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
        this.dialog.closeAll();
      });
  }
}
