import {Component, Input, OnInit} from '@angular/core';
import {MessageService} from "primeng/api";
import {SolicitudAccesoService} from "../../../service/solicitudAcceso.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SolicitudAccesoModel} from "../../../model/SolicitudAcceso";
import {PermisoForestalService} from "../../../service/permisoForestal.service";
import {SolicitudModel} from "../../../model/Solicitud";
import {SolicitudService} from "../../../service/solicitud.service";
import { SolicitudCargaArchivoModel} from "../../../model/SolicitudArchivo";
import { DialogService } from 'primeng/dynamicdialog';
import { ValidarRequisitosModal } from './modal/validar-requisitos-modal/validar-requisitos.modal';

@Component({
  selector: 'revision-permiso-forestal-ccnn',
  templateUrl: './evaluar-permiso-forestal-ccnn.component.html',
  styleUrls: ['./evaluar-permiso-forestal-ccnn.component.scss'],
  providers: [MessageService]
})
export class EvaluarPermisoForestalCCNNComponent implements OnInit {

  dataBase: any = {tipoPersona: '', disabled: false};
  enabledControl: boolean = false;
  enabledControlOtorgamiento: string = 'false';
  boolNuevo: boolean = false;
  accion: string | null = '';
  idSolAcceso: string | null = '';
  idSolicitudEdit: string | null = null;
  idSolicitud: number = 0;
  idPersona: string | null = '';
  solicitudAcceso = {} as SolicitudAccesoModel;
  solicitud = {} as SolicitudModel;
  solicitudCargaArchivo = {} as SolicitudCargaArchivoModel;
  validDatosPersona: boolean = false;
  validDatosPlanManejo: boolean = false;
  validDatosArea: boolean = false;
  validDatosAnexo: boolean = false;

  informeFirmado: boolean = false;
  favorable: boolean | null = null;

  enabledControlGeneral: boolean = false;
  enabledControlOperativo: boolean = false;
  enabledControlDema: boolean = false;

  esGuardado: boolean = false;

  constructor(
    private messageService: MessageService,
    private servSA: SolicitudAccesoService,
    private servPf: PermisoForestalService,
    private servSol: SolicitudService,
    private router: Router,
    private route: ActivatedRoute,
    public dialogService: DialogService,
  ) {
  }


  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.accion = params.get('action');
      if (this.accion == 'nuevo') {
        this.idSolAcceso = params.get('idSolicitudAcceso');
      } else {
        this.idSolAcceso = params.get('idSolicitudAcceso');
        this.idSolicitudEdit = params.get('idSolicitud');
        if (this.idSolicitudEdit != null) {
          this.solicitud.idSolicitud = parseInt(this.idSolicitudEdit);
        }
        this.obtenerSolicitudPorAccesoSolicitud();
      }
      this.boolNuevo = this.accion == 'nuevo';
      this.dataBase.disabled = this.accion == 'editar';
    });

    this.solicitudAcceso.codigoEstadoSolicitud = "ESACREGI";
    if (this.idSolAcceso != null) {
      this.solicitudAcceso.idSolicitudAcceso = parseInt(this.idSolAcceso);
    }
    this.obtenerSolicitudAcceso();
    this.obtenerSolicitudAccesoPorUsuario();

  }


  obtenerSolicitudAcceso() {
    this.servSA.obtenerSolicitudAcceso(this.solicitudAcceso).subscribe(
      (result: any) => {
        this.solicitudAcceso = result.data;
        
      });
  }

  obtenerSolicitudAccesoPorUsuario() {
    let usuario = JSON.parse(localStorage.getItem("usuario") as any);
    this.servPf.obtenerSolicitudAccesoPersonaPorUsuario(usuario.idusuario).subscribe(
      (result: any) => {
        this.idPersona = result.data.idPersona;

      }
    );
  }

  registrarArchivoSolicitud(idSolicitud: number) {

    if (this.solicitudCargaArchivo.solArchivoTitProp != null) {
      this.solicitudCargaArchivo.solArchivoTitProp.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoTitProp.idSolicitud, this.solicitudCargaArchivo.solArchivoTitProp.archivo, this.solicitudCargaArchivo.solArchivoTitProp.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoDecJur != null) {
      this.solicitudCargaArchivo.solArchivoDecJur.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoDecJur.idSolicitud, this.solicitudCargaArchivo.solArchivoDecJur.archivo, this.solicitudCargaArchivo.solArchivoDecJur.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoLimCol != null) {
      this.solicitudCargaArchivo.solArchivoLimCol.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoLimCol.idSolicitud, this.solicitudCargaArchivo.solArchivoLimCol.archivo, this.solicitudCargaArchivo.solArchivoLimCol.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoActRepLeg != null) {
      this.solicitudCargaArchivo.solArchivoActRepLeg.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoActRepLeg.idSolicitud, this.solicitudCargaArchivo.solArchivoActRepLeg.archivo, this.solicitudCargaArchivo.solArchivoActRepLeg.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoAreCom != null) {
      this.solicitudCargaArchivo.solArchivoAreCom.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoAreCom.idSolicitud, this.solicitudCargaArchivo.solArchivoAreCom.archivo, this.solicitudCargaArchivo.solArchivoAreCom.tipoArchivo);
    }
    if (this.solicitudCargaArchivo.solArchivoAsamAcu != null) {
      this.solicitudCargaArchivo.solArchivoAsamAcu.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoAsamAcu.idSolicitud, this.solicitudCargaArchivo.solArchivoAsamAcu.archivo, this.solicitudCargaArchivo.solArchivoAsamAcu.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoRegFore != null) {
      this.solicitudCargaArchivo.solArchivoRegFore.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoRegFore.idSolicitud, this.solicitudCargaArchivo.solArchivoRegFore.archivo, this.solicitudCargaArchivo.solArchivoRegFore.tipoArchivo);
    }
    if (this.solicitudCargaArchivo.solArchivoArrf != null) {
      this.solicitudCargaArchivo.solArchivoArrf.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoArrf.idSolicitud, this.solicitudCargaArchivo.solArchivoArrf.archivo, this.solicitudCargaArchivo.solArchivoArrf.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoComPag != null) {
      this.solicitudCargaArchivo.solArchivoComPag.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoComPag.idSolicitud, this.solicitudCargaArchivo.solArchivoComPag.archivo, this.solicitudCargaArchivo.solArchivoComPag.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoConTer != null) {
      this.solicitudCargaArchivo.solArchivoConTer.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoConTer.idSolicitud, this.solicitudCargaArchivo.solArchivoConTer.archivo, this.solicitudCargaArchivo.solArchivoConTer.tipoArchivo);
    }

    if (this.solicitudCargaArchivo.solArchivoActAsa != null) {
      this.solicitudCargaArchivo.solArchivoActAsa.idSolicitud = idSolicitud;
      this.registrarArchivoSolicitud2(this.solicitudCargaArchivo.solArchivoActAsa.idSolicitud, this.solicitudCargaArchivo.solArchivoActAsa.archivo, this.solicitudCargaArchivo.solArchivoActAsa.tipoArchivo);
    }
  }

  registrarArchivoSolicitud2(idSolicitud: number, archivo: File, tipoArchivo: string) {
    this.servSol.registrarArchivoSolicitud(idSolicitud, archivo, tipoArchivo).subscribe((response: any) => {
    });
  }

  toast(severty: string, msg: string) {

    this.messageService.add({severity: severty, summary: "", detail: msg});
  }

  obtenerSolicitudPorAccesoSolicitud() {
    let idSolicitudAcceso: number = this.idSolAcceso != null ? parseInt(this.idSolAcceso) : 0;
    let idSolicitud: number = this.idSolicitudEdit != null ? parseInt(this.idSolicitudEdit) : 0;
    this.servSol.obtenerSolicitudPorAccesoSolicitud(idSolicitud, idSolicitudAcceso).subscribe((response: any) => {
     // this.solicitud = response.data;
      
    });
  }




  habilitarAlta(value: boolean) {
    this.enabledControlGeneral = value;
    this.enabledControlOperativo = false;
    this.enabledControlDema = false;
  }


  habilitarOperativo(value: boolean) {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = value;
    this.enabledControlDema = false;
  }

  habilitarDema(value: boolean) {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = false;
    this.enabledControlDema = value;
  }




  // BOTON GUARDAR
  guardarSolicitud() {

    this.messageService.add({severity:'success', detail: 'Se guardaron los datos correctamente'});
    this.esGuardado = true;
  }


  validarRequisitos(){
    const ref = this.dialogService.open(ValidarRequisitosModal, {
      header: "Validar Requisitos",
      width: "44%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
    });

    ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.messageService.add({severity:'success', detail: 'Se validaron los requisitos'});
      }
    });
  }

// BOTON CONTINUAR
  registroSolicitud() {

    if(this.solicitud.tipoAprovechamiento == 'Por Comunidad') {
      this.solicitud.nroContratoTercero='';
      this.solicitud.nroActaAprovechamiento='';
    }

    if (this.idSolAcceso != null && this.idPersona != null) {
      this.solicitud.idSolicitudAcceso = parseInt(this.idSolAcceso);
      this.solicitud.idPersonaRepresentante = parseInt(this.idPersona);
    }

    this.servSol.registrarSolicitud(this.solicitud).subscribe((response: any) => {
      this.idSolicitud = response.data.idSolicitud;
      this.solicitud.idSolicitud = this.idSolicitud;
      this.registrarArchivoSolicitud(this.idSolicitud);

      this.servSol.registrarSolicitudMovimientoFlujo(this.idSolicitud, 1).subscribe((data: any) => {
        console.log('registrarSolicitudMovimientoFlujo()');
        
      })
      this.toast('success', 'La solicitud se guardó correctamente.');
      this.enabledControl = true;
      this.enabledControlOtorgamiento = 'true';
    });
  }
  validating(event: boolean) {
    this.validDatosPersona = event
  }
  validatingPlanManejo(event: boolean) {
    this.validDatosPlanManejo = event;
  }
  validatingAnexo(event: boolean){
    this.validDatosAnexo = event;

  }
  validatingArea(event: boolean){
    this.validDatosArea = event;
  }
}

