import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { PlanManejoContrato } from '@models';
import {
  ArchivoService,
  CoreCentralService,
  PlanificacionService,
  PlanManejoService,
  UsuarioService,
} from '@services';
import { MessageService } from 'primeng/api';
import { InformacionGeneralPlanManejoContrato } from 'src/app/model/plan-manejo-contrato';
import {
  PmfiInformacionGeneral,
  PmfiRegente,
} from 'src/app/model/pmfi-informacion-general';
import { PmfiInformacionGeneralRequest } from 'src/app/model/pmfi-informacion-general-request';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import * as _moment from 'moment';
import { isNullOrEmpty, ToastService } from '@shared';
import { FileModel } from 'src/app/model/util/File';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { DatePipe } from '@angular/common';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ModalInfoContratoComponent } from './modal-info-contrato/modal-info-contrato.component';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { CodigosPMFI } from '../../../../../model/util/PMFI/PMFI';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { PideService } from 'src/app/service/evaluacion/pide.service';

// export const MY_FORMATS = {
//   parse: {
//     dateInput: 'MM/YYYY',
//   },
//   display: {
//     dateInput: 'MM/YYYY',
//     monthYearLabel: 'MMM YYYY',
//     dateA11yLabel: 'LL',
//     monthYearA11yLabel: 'MMMM YYYY',
//   },
// };
@Component({
  selector: 'app-pmfi-informacion-general',
  templateUrl: './pmfi-informacion-general.component.html',
  styleUrls: ['./pmfi-informacion-general.component.scss'],
  // providers: [
  //   {
  //     provide: DateAdapter,
  //     useClass: MomentDateAdapter,
  //     deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  //   },
  //   {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  // ],
})
export class PmfiInformacionGeneralComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() siguiente = new EventEmitter();
  @Input() isPerfilArffs!: boolean;

  @Input() disabled!: boolean;
  mailValido: boolean = true;
  disabledFecha: boolean = false;

  static get EXTENSIONSAUTHORIZATION2() {
    return ['.pdf', 'image/png', 'image/jpeg', 'image/jpeg', 'application/pdf'];
  }

  showDatosContrato: boolean = false;
  verModalRL: boolean = false;
  verModalContratos: boolean = false;
  isEdit: boolean = false;

  fechaInicial = '';
  fechaFinal = '';
  fechaElaboracionPmfi: string = '';

  nombreFileContrato: string = '';
  selectRegente: PmfiRegente = new PmfiRegente();
  verModalRegente: boolean = false;
  queryRegente: string = '';
  regentes: any[] = [];

  queryIdContratos: string = '';
  queryContratos: string = '';
  contratos: PlanManejoContrato[] = [];
  lstcontratos: InformacionGeneralPlanManejoContrato[] = [];
  selectContrato!: PlanManejoContrato;

  nombreCompleto: string = '';

  infoGeneral: PmfiInformacionGeneralRequest =
    new PmfiInformacionGeneralRequest();

  informacionGeneral: PmfiInformacionGeneral = new PmfiInformacionGeneral();

  distrito = '';
  provincia = '';
  departamento = '';

  first = 0;
  rows = 10;

  usuario!: UsuarioModel;

  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;

  dniRepresentanteLegal: string = '';
  rucTitular: string = '';

  ref!: DynamicDialogRef;

  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_1;

  codigoAcordeon: string = CodigosPMFI.TAB_1;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  minDate = moment(new Date()).format('YYYY-MM-DD');
  minDateFinal = moment(new Date()).add('days', 1).format('YYYY-MM-DD');

  idArchivoR: number = 0;
  validaPIDEReniecClassRepre: string = 'btn btn-sm btn-danger2';
  flagPIDEReniecClassRepre: boolean = false;

  editRepresentante: boolean = false;

  fechaInicialRegente = '';
  fechaFinalRegente = '';

  constructor(
    private apiPlanificacion: PlanificacionService,
    private apiForestal: ApiForestalService,
    private user: UsuarioService,
    private informacionGeneralService: InformacionGeneralService,
    private messageService: MessageService,
    private apiCore: CoreCentralService,
    private toast: ToastService,
    private dialog: MatDialog,
    private anexosService: AnexosService,
    public datepipe: DatePipe,
    public dialogService: DialogService,
    private planManejoService: PlanManejoService,
    private evaluacionService: EvaluacionService,
    private router: Router,
    private planServ: PlanManejoService,
    private pideService: PideService
  ) {}

  ngOnInit(): void {
    this.usuario = this.user.usuario;
    // this.listarContratosVigentes();
    this.listarInfGeneral();
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = 'PDF';
    this.listarArchivoPMFI();
    this.obtenerPlan();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarInfGeneral() {
    this.informacionGeneral.idPlanManejo = this.idPlanManejo;
    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: 'PMFI',
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.isEdit = false;
          response.data.forEach((element: any) => {
            this.informacionGeneral = element;
            this.informacionGeneral.fechaElaboracionPmfi =
              element.fechaElaboracionPmfi;
            this.fechaElaboracionPmfi = element.fechaElaboracionPmfi
              .toString('dd/MMM/yyyy')
              .slice(0, 10);
            if (element.codTipoPersona === 'TPERJURI') {
              this.informacionGeneral.nombreCompletoTitular =
                element.nombreElaboraDema;
              this.informacionGeneral.nombreCompletoRepresentante =
                element.nombreRepresentante +
                ' ' +
                element.apellidoPaternoRepresentante +
                ' ' +
                element.apellidoMaternoRepresentante;
            } else {
              this.informacionGeneral.nombreCompletoTitular =
                element.nombreElaboraDema +
                ' ' +
                element.apellidoPaternoElaboraDema +
                ' ' +
                element.apellidoMaternoElaboraDema;
            }
            // this.fechaInicial = element.fechaInicioDema;
            // this.fechaFinal = element.fechaFinDema;
            this.informacionGeneral.fechaInicioDema = new Date(
              element.fechaInicioDema
            );
            this.informacionGeneral.fechaFinDema = new Date(
              element.fechaFinDema
            );
            this.departamento = element.departamentoRepresentante;
            this.provincia = element.provinciaRepresentante;
            this.distrito = element.distritoRepresentante;
            if (element.regente) {
              this.nombreCompleto =
                element.regente.nombres + ' ' + element.regente.apellidos;
                this.fechaInicialRegente = element.regente.fechaIniContrato
                this.fechaFinalRegente = element.regente.fechaFinContrato
            }
            //  this.getDistrito(element.idDistritoTitular);

            if (this.contratos.length != 0) {
              this.filtarIdContrato(element);
            }
          });
        } else {
          this.isEdit = true;
        }
      });
  }

  obtenerPlan() {
    let params: any = {
      idPlanManejo: this.idPlanManejo,
    };
    this.planServ.obtenerPlanManejo(params).subscribe((result: any) => {
      if (result.data) {
        let element = result.data;
        if (element.codigoEstado == 'EPLMAPROB') {
          this.informacionGeneral.fechaInicioDema = element.fechaModificacion;
          this.setFechaFin();
        }
      }

      (error: HttpErrorResponse) => {};
    });
  }

  openModal() {
    this.ref = this.dialogService.open(ModalInfoContratoComponent, {
      header: 'Buscar Contrato',
      width: '60%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      data: {
        nroDocumento: this.user.nroDocumento,
        codTipoPlan: 'PMFI',
        idPlanManejo: this.idPlanManejo,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp.codigoTipoPersonaTitular == 'TPERJURI') {
          this.informacionGeneral.nombreCompletoTitular =
            resp.razonSocialTitular;
          this.informacionGeneral.nombreElaboraDema = resp.razonSocialTitular;
          this.informacionGeneral.direccionLegalTitular =
            resp.domicilioLegalTitular;
          this.informacionGeneral.tipoDocumentoElaborador =
            resp.codigoTipoDocumentoTitular;
          this.informacionGeneral.nroRucComunidad = resp.numeroDocumentoTitular;
          this.informacionGeneral.dniElaboraDema = resp.numeroDocumentoTitular;
          this.informacionGeneral.esReprLegal = 'S';
          this.informacionGeneral.nombreCompletoRepresentante =
            resp.nombreRepresentanteLegal +
            ' ' +
            resp.apePaternoRepLegal +
            ' ' +
            resp.apeMaternoRepLegal;
          this.informacionGeneral.nombreRepresentante =
            resp.nombreRepresentanteLegal;
          this.informacionGeneral.apellidoPaternoRepresentante =
            resp.apePaternoRepLegal;
          this.informacionGeneral.apellidoMaternoRepresentante =
            resp.apeMaternoRepLegal;
          this.informacionGeneral.codTipoDocumentoRepresentante =
            resp.codigoTipoDocIdentidadRepLegal;
          this.informacionGeneral.documentoRepresentante =
            resp.numeroDocumentoRepLegal;
        } else {
          this.informacionGeneral.nombreCompletoTitular =
            resp.nombreTitular +
            ' ' +
            resp.apePaternoTitular +
            ' ' +
            resp.apeMaternoTitular;
          this.informacionGeneral.nombreElaboraDema = resp.nombreTitular;
          this.informacionGeneral.apellidoPaternoElaboraDema =
            resp.apePaternoTitular;
          this.informacionGeneral.apellidoMaternoElaboraDema =
            resp.apeMaternoTitular;
          this.informacionGeneral.dniElaboraDema = resp.numeroDocumentoTitular;
          this.informacionGeneral.esReprLegal = 'N';
        }
        this.informacionGeneral.idContrato = resp.idContrato;
        this.informacionGeneral.codTipoPersona = resp.codigoTipoPersonaTitular;
        this.departamento = resp.departamento;
        this.provincia = resp.provincia;
        this.distrito = resp.distrito;
        this.informacionGeneral.idDistritoTitular = resp.idDistritoContrato;
        this.informacionGeneral.areaTotal = resp.areaTotal;
        this.informacionGeneral.nombreTituloHabilitante =
          resp.tituloHabilitante;
        this.informacionGeneral.regente = new PmfiRegente();
      }
    });
  }

  listarContratosVigentes() {
    this.contratos = [];
    this.apiPlanificacion.listarContratosVigentes(0).subscribe((res) => {
      if (!!res.data) {
        this.contratos = [...res.data];
      }
    });
  }

  clearContrato() {
    this.informacionGeneral.nombreElaboraDema = '';
    this.informacionGeneral.representanteLegal = '';
    this.informacionGeneral.dniElaboraDema = '';
    this.informacionGeneral.direccionLegalTitular = '';
    this.informacionGeneral.direccionLegalRepresentante = '';
  }

  guardarContrato() {
    this.informacionGeneral.idContrato = this.selectContrato.idContrato;
    this.informacionGeneral.nombreElaboraDema =
      this.selectContrato.nombreTitular;
    this.informacionGeneral.representanteLegal =
      this.selectContrato.nombreRepresentanteLegal;
    this.informacionGeneral.dniElaboraDema = this.selectContrato.dniTitular;
    this.informacionGeneral.direccionLegalTitular =
      this.selectContrato.domicilioLegalTitular;
    this.informacionGeneral.direccionLegalRepresentante =
      this.selectContrato.direccionLegalRepresentante;
    this.informacionGeneral.idDistritoTitular =
      this.selectContrato.distritoTitular;
    this.dniRepresentanteLegal = this.selectContrato.dniRepresentanteLegal;
    this.rucTitular = this.selectContrato.rucTitular;
    this.verModalContratos = false;

    this.getDepartamento(this.selectContrato.departamentoTitular);
    this.getProvincia(this.selectContrato.provinciaTitular);
    this.getDistrito(this.selectContrato.distritoTitular);
  }

  filtarIdContrato(element: any) {
    let contrato: any = this.contratos.find(
      (ele) => ele.idContrato === element.idContrato
    );
    this.dniRepresentanteLegal = contrato.dniRepresentanteLegal;
    this.rucTitular = contrato.rucTitular;
  }

  abrirModalContrato() {
    this.listarContratosVigentes();
    this.queryIdContratos = '';
    this.selectContrato = {} as PlanManejoContrato;
    this.verModalContratos = true;
  }

  filtrarContrato() {
    if (this.queryContratos) {
      this.contratos = this.contratos.filter(
        (r) =>
          r.nombreTitular
            .toLowerCase()
            .includes(this.queryContratos.toLowerCase()) ||
          (r.dniTitular &&
            r.dniTitular
              .toLowerCase()
              .includes(this.queryContratos.toLowerCase()))
      );
    } else if (this.queryIdContratos) {
      this.filtrarId();
    } else {
      this.listarContratosVigentes();
    }
  }

  filtrarId() {
    if (this.queryIdContratos) {
      this.contratos = this.contratos.filter((r) =>
        r.idContrato
          .toString()
          .toLowerCase()
          .includes(this.queryIdContratos.toLowerCase())
      );
    } else {
      this.listarContratosVigentes();
    }
  }

  fileChangeEvent(e: any): void {
    let files = e.target.files;

    if (files.length > 0) {
      this.nombreFileContrato = files[0].name;
      this.showDatosContrato = true;
    }
  }

  abrirModalRegentes() {
    this.listarRegentes();
    this.selectRegente = new PmfiRegente();
    this.verModalRegente = true;
    this.queryRegente = '';
  }

  listarRegentes() {
    this.apiForestal.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {
        this.regentes.push({
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
        });
      });
    });
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegentes();
    }
  }

  guardarRegente() {
    this.nombreCompleto =
      this.selectRegente.nombres + ' ' + this.selectRegente.apellidos;
    this.informacionGeneral.regente = new PmfiRegente(this.selectRegente);
    this.informacionGeneral.regente.idPlanManejo = this.idPlanManejo;
    this.informacionGeneral.regente.idUsuarioRegistro = this.usuario.idusuario;
    this.informacionGeneral.regente.estadoRegente = this.selectRegente.estado;
    this.verModalRegente = false;
  }

  handleAdmDateChange() {
    this.validarInformacion();
    this.informacionGeneral.fechaInicioDema = new Date(this.fechaInicial);
    this.informacionGeneral.fechaFinDema = new Date(this.fechaFinal);
    var anios = this.calcDate(
      this.informacionGeneral.fechaInicioDema,
      this.informacionGeneral.fechaFinDema
    );
    this.informacionGeneral.vigencia = anios;
  }

  calcDate(date1: Date, date2: Date) {
    var months;
    months = (date2.getFullYear() - date1.getFullYear()) * 12;
    months -= date1.getMonth();
    months += date2.getMonth();
    months <= 0 ? 0 : months;
    var years = Math.floor(months / 12);
    return years;
  }

  datosGenerales() {
    // this.validarInformacion();
    this.informacionGeneral.idUsuarioRegistro = this.user.idUsuario;
    this.informacionGeneral.fechaElaboracionPmfi = new Date();
  }

  datosGeneralesEditar() {
    // this.validarInformacion();
    this.informacionGeneral.idUsuarioRegistro = this.user.idUsuario;
    this.informacionGeneral.departamentoRepresentante = this.departamento;
    this.informacionGeneral.distritoRepresentante = this.distrito;
    this.informacionGeneral.provinciaRepresentante = this.provincia;
    this.informacionGeneral.fechaElaboracionPmfi = new Date();
  }

  registrarInformacionGeneral() {
    if (this.informacionGeneral.regente)
      this.informacionGeneral.conRegente = true;
    if (this.validationFields(this.informacionGeneral)) {
      this.datosGenerales();
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .registrarInformacionGeneralDema(this.informacionGeneral)
        .subscribe((response: any) => {
          if (response.success == true) {
            this.dialog.closeAll();
            this.toast.ok('Se registró la Información General correctamente.');
            this.registrarPlanManejoContrato();
            this.listarInfGeneral();
          } else {
            this.toast.error('Ocurrió un error al realizar la operación.');
          }
        });
      this.guardarArchivo();
    }
  }

  validationFields(data: PmfiInformacionGeneral) {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!data.idContrato) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Id Contrato.\n';
    }
    if (!data.nombreTituloHabilitante) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Título Habilitante.\n';
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  registrarPlanManejoContrato() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      idContrato: this.informacionGeneral.idContrato,
      contratoPrincipal: 1,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.planManejoService.registrarPlanManejoContrato(params).subscribe();
  }

  actualizarInformacionGeneralDema() {
    if (this.informacionGeneral.regente)
      this.informacionGeneral.conRegente = true;
    if (this.validationFields(this.informacionGeneral)) {
      this.datosGeneralesEditar();
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(this.informacionGeneral)
        .subscribe((response: any) => {
          if (response.success == true) {
            this.dialog.closeAll();
            this.toast.ok('Se actualizó la Información General correctamente.');
            this.listarInfGeneral();
          } else {
            this.toast.error('Ocurrió un error al realizar la operación.');
          }
        });
      this.guardarArchivo();
    }
  }

  validarInformacion() {
    this.setFechaFin();
    let fechaActual = new Date();
    let fechaInicio = new Date(this.informacionGeneral.fechaInicioDema);

    this.disabledFecha = false;
    if (
      this.informacionGeneral.fechaInicioDema &&
      this.informacionGeneral.fechaFinDema
    ) {
      if (fechaInicio < fechaActual) {
        this.disabledFecha = true;
        this.toast.error(
          'La Fecha Inicio debe ser posterior a la Fecha Actual'
        );
        return;
      }

      if (
        this.informacionGeneral.fechaFinDema <=
        this.informacionGeneral.fechaInicioDema
      ) {
        this.disabledFecha = true;
        this.toast.error('La Fecha Final debe ser posterior a la Fecha Inicio');
        return;
      }
    } else if (fechaInicio < fechaActual) {
      this.disabledFecha = true;
      this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
      return;
    }
  }

  setFechaFin() {
    let dInicio: Date = new Date(this.informacionGeneral.fechaInicioDema);
    let dFin = new Date(
      dInicio.getFullYear() + Number(this.informacionGeneral.vigencia),
      dInicio.getMonth(),
      dInicio.getDate()
    );

    this.informacionGeneral.fechaFinDema = dFin;
  }

  getDepartamento(idDepartamento: number) {
    this.apiCore
      .listarPorFiltroDepartamento({ idDepartamento })
      .subscribe((result: any) => {
        result.data.forEach((element: any) => {
          this.departamento = element.nombreDepartamento;
        });
      });
  }
  getProvincia(idProvincia: number) {
    this.apiCore
      .listarPorFilroProvincia({ idProvincia })
      .subscribe((result: any) => {
        result.data.forEach((element: any) => {
          this.provincia = element.nombreProvincia;
        });
      });
  }
  getDistrito(idDistrito: number) {
    this.apiCore
      .listarPorFilroDistrito({ idDistrito })
      .subscribe((result: any) => {
        result.data.forEach((element: any) => {
          this.distrito = element.nombreDistrito;
        });
      });
  }
  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === 'PDF') {
          include =
            PmfiInformacionGeneralComponent.EXTENSIONSAUTHORIZATION2.includes(
              file.type
            );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El tipo de Documento no es Válido.',
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El archivo no debe tener más de  3MB.',
          });
        } else {
          if (type === 'PDF') {
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.files.push(this.fileInfGenreal);
          }
        }
      }
    }
  }

  guardarArchivo() {
    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: 35,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
      }
    });
  }

  registrarArchivo(id: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: 'PMFI',
      descripcion: '',
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: '',
    };
    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok('Se cargó el Contrato del Regente correctamente.');
        this.listarArchivoPMFI();
      } else {
        this.toast.error('Ocurrió un error al realizar la operación.');
      }
    });
  }

  listarArchivoPMFI() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 35,
      codigoProceso: 'PMFI',
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarArchivo = true;
        this.eliminarArchivo = false;
        result.data.forEach((element: any) => {
          this.fileInfGenreal.nombreFile = element.nombreArchivo;
          this.idArchivo = element.idArchivo;
        });
      } else {
        this.eliminarArchivo = true;
        this.cargarArchivo = false;
      }
    });
  }

  eliminarArchivoContrato() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idArchivo))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok('Se eliminó  Contrato Regente correctamente');
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = '';
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  validarCorreo = () => {
    

    this.mailValido = false;
    var EMAIL_REGEX =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (this.informacionGeneral.correoTitular.match(EMAIL_REGEX)) {
      this.mailValido = true;
    }
  };

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(
                this.detEvaluacion,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon
                )
              );
            } else {
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.detEvaluacion])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion() {
    localStorage.setItem(
      'EvalResuDet',
      JSON.stringify({
        tab: 'RFNM',
        acordeon: 'LINEADP6231',
      })
    );

    this.router.navigateByUrl(
      '/planificacion/evaluacion/requisitos-previos/' +
        this.idPlanManejo +
        '/' +
        this.codigoProceso
    );
  }

  registraArchivo(idArchivo: number) {
    this.idArchivoR = idArchivo;
  }

  eliminaArchivo(idArchivo: any) {
    this.idArchivoR = 0;
  }

  validarFechaRegente() {
    let fechaActual = new Date();
    let fechaInicioR = new Date(this.fechaInicialRegente);
    let fechaFinR = new Date(this.fechaFinalRegente);
    this.informacionGeneral.regente.fechaIniContrato = fechaInicioR
    this.informacionGeneral.regente.fechaFinContrato = fechaFinR
    this.disabledFecha = false;
    if (
      this.informacionGeneral.regente?.fechaIniContrato &&
      this.informacionGeneral.regente?.fechaFinContrato
    ) {
      if (
        this.informacionGeneral.regente?.fechaFinContrato <=
        this.informacionGeneral.regente?.fechaIniContrato
      ) {
        this.disabledFecha = true;
        this.toast.error('La Fecha Final debe ser posterior a la Fecha Inicio');
        return;
      }
    } else if (fechaInicioR < fechaActual) {
      this.disabledFecha = true;
      this.toast.error('La Fecha Inicio debe ser posterior a la Fecha Actual');
      return;
    }
  }

  editarRepresentante() {
    this.informacionGeneral.nombreCompletoRepresentante = '';
    this.informacionGeneral.documentoRepresentante = '';
    this.informacionGeneral.direccionLegalRepresentante = '';
    this.editRepresentante = true;
  }

  validarPideReniecRepresentanteLegal() {
    if (
      this.informacionGeneral.documentoRepresentante == null ||
      this.informacionGeneral.documentoRepresentante == '' ||
      this.informacionGeneral.documentoRepresentante == undefined
    ) {
      this.toast.warn('Se requiere número DNI.');
      return;
    } else {
      let params = {
        numDNIConsulta: this.informacionGeneral.documentoRepresentante,
      };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.pideService
        .consultarDNI(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(
          (result: any) => {
            if (result.dataService) {
              let persona = result.dataService.datosPersona;
              if (result.dataService.datosPersona) {
                this.flagPIDEReniecClassRepre = true;
                this.validaPIDEReniecClassRepre = 'btn btn-sm btn-danger2';
                this.toast.ok('Se validó existencia de DNI en RENIEC.');

                this.informacionGeneral.nombreCompletoRepresentante =
                  persona.prenombres +
                  ' ' +
                  persona.apPrimer +
                  ' ' +
                  persona.apSegundo;
                this.informacionGeneral.direccionLegalRepresentante =
                  persona.direccion;
                this.informacionGeneral.nombreRepresentante =
                  persona.prenombres;
                this.informacionGeneral.apellidoPaternoRepresentante =
                  persona.apPrimer;
                this.informacionGeneral.apellidoMaternoRepresentante =
                  persona.apSegundo;
              } else {
                this.flagPIDEReniecClassRepre = false;
                this.validaPIDEReniecClassRepre = 'btn btn-sm btn-danger2';
                this.toast.warn('DNI ingresado no existe en RENIEC.');
              }
            } else {
              this.flagPIDEReniecClassRepre = false;
              this.validaPIDEReniecClassRepre = 'btn btn-sm btn-danger2';
              this.toast.error(
                'Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.'
              );
            }
          },
          (error) => {
            this.flagPIDEReniecClassRepre = false;
            this.validaPIDEReniecClassRepre = 'btn btn-sm btn-danger2';
            this.toast.error(
              'Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.'
            );
          }
        );
    }
  }
}
