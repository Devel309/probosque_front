import { Component, OnInit } from "@angular/core";
import { ParametroValorService, UsuarioService } from "@services";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { CensoMaderable } from "src/app/model/CensoMaderable";

@Component({
  selector: "modal-formulario-maderables",
  templateUrl: "./modal-formulario-maderables.component.html",
})
export class ModalMaderablesComponent implements OnInit {
  form: CensoMaderable = new CensoMaderable();
  disabled: boolean =  false;

  actividades: any[] = [];
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private parametroValorService: ParametroValorService,
    private user: UsuarioService
  ) {}

  ngOnInit() {
    //  this.form = new CensoMaderable();
    this.form.idUsuarioRegistro = this.user.idUsuario;
    this.form.idCensoForestal = this.config.data.idCensoForestal
    this.obtenerActividades();
    if (this.config.data.type == "E") {
      this.form = new CensoMaderable(this.config.data.form);
      this.disabled = true;
    }
  }
  obtenerActividades() {
    var params = { prefijo: "PRDT" };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.actividades = data.data;
      });
  }

  validarDatos(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.form.idCodigoEspecie === null) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: N°.\n";
    }

    if (this.form.nombreCientifico === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre científico.\n";
    }

    if (this.form.nombreComun === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre común.\n";
    }

    if (this.form.productoTipo === "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: producto.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  editar() {
    if (!this.validarDatos()) {
      return;
    }
    //  var obj = new CensoMaderable(this.form);
    this.ref.close(this.form);
  }

  cerrarModal() {
    

    this.ref.close();
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
