import { HttpParams } from "@angular/common/http";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { environment } from "@env/environment";
import {
  ArchivoService,
  PlanificacionService,
  UsuarioService,
} from "@services";
import { descargarArchivo, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CensoForestalService } from "src/app/service/censoForestal";
import { AnexosService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/anexo.service";
import { RecursoServicioPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/recursos-servicios.service";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { Router } from "@angular/router";
import { CodigosPMFI } from "../../../../../model/util/PMFI/PMFI";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CensoMaderable } from "src/app/model/CensoMaderable";
import { ModalMaderablesComponent } from "./modals/modal-formulario-zonas/modal-formulario-maderables.component";
import * as moment from "moment";

@Component({
  selector: "app-pmfi-especies-recursos",
  templateUrl: "./pmfi-especies-recursos.component.html",
  styleUrls: ["./pmfi-especies-recursos.component.scss"],
})
export class PmfiEspeciesRecursosComponent implements OnInit {
  @Input() isPerfilArffs!: boolean;
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @Input() disabled!: boolean;
  f: FormGroup;

  lstEspecies: any[] = [];
  lstResultadosInventarios: any[] = [];
  lstResumenInventarios: any[] = [];

  lstEspeciesNoMade: any[] = [];
  lstResultadosInventariosNoMade: any[] = [];
  lstResumenInventariosNoMade: any[] = [];

  isConsultoCenso: boolean = true;

  especie: any;

  especieModal: any;

  especieModalNoMad: any;

  tituloModalMantenimiento1: string = "";
  verModalMantenimiento1: boolean = false;
  tipoAccion: string = "";

  //fechaInventario : Date = new Date();
  //descripcionInventario:string= "";

  //Maderables
  totalAprovMade: number = 0;
  totalSemiMade: number = 0;
  totalPromedioHaMade: number = 0;
  totalProdBarricasMade: number = 0;
  totalProdAprovMade: number = 0;

  totalAprovNoMade: number = 0;
  totalSemiNoMade: number = 0;
  totalPromedioHaNoMade: number = 0;
  totalProdBarricasNoMade: number = 0;
  totalProdAprovNoMade: number = 0;
  pendiente: boolean = false;

  varAssets = `${environment.varAssets}`;
  archivo: any;

  listAnexo2Maderable: any[] = [];
  listAnexo2NoMaderable: any[] = [];
  listResultadosInventarioMaderableA: any[] = [];
  listAnexo2InventarioMaderableB: any[] = [];
  listResultadosInventarioNoMaderableA: any[] = [];
  listAnexo2InventarioNoMaderableB: any[] = [];
  listFecha: any[] = [];
  showWarn: boolean = true;

  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_5;

  codigoAcordeon: string = CodigosPMFI.TAB_5;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;
  UrlFormatos = UrlFormatos;
  nombreGenerado: string = "";
  plantillaPMFI: string = "";
  idCensoForestal: number = 0;
  fechaRealizacion: string = "";

  ref!: DynamicDialogRef;
  minDateInicio: any = moment(new Date()).format("YYYY-MM-DD");
  constructor(
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private recursoServicioService: RecursoServicioPmfiService,
    private messageService: MessageService,
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private dialog: MatDialog,
    private censoForestalService: CensoForestalService,
    private archivoService: ArchivoService,
    public dialogService: DialogService,
    private anexosService: AnexosService,
    private evaluacionService: EvaluacionService,
    private router: Router,
    private planificacionService: PlanificacionService
  ) {
    this.f = this.formInit();
    this.f.patchValue({
      fechaInventario: new Date(),
      descripcionInventario: "",
      fechaInventarioNoMaderable: new Date(),
      descripcionInventarioNoMaderable: "",
    });

    this.archivo = {
      file: null,
    };
  }

  ngOnInit(): void {
    this.especieModal = {
      idRecursoForestal: 0,
      idRecursoForestalDetalle: 0,
      nombreCientifico: "",
      nombreComun: "",
      producto: "",
      indice: null,
      tipoRecurso: "", //1 - 2
    };
    // this.consultarServicio1('1');

    this.listarAnexo2Maderable();
    this.listarAnexo2NoMaderable();
    this.listarResultadosInventarioMaderableA();
    this.listarAnexo2InventarioMaderableB();
    this.listarResultadosInventarioNoMaderableA();
    this.listarAnexo2InventarioNoMaderableB();
    this.listarFecha();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  formInit() {
    const f = this.fb.group({});
    const info = {
      fechaInventario: new Date(),
      descripcionInventario: "",
      fechaInventarioNoMaderable: new Date(),
      descripcionInventarioNoMaderable: "",
    };
    Object.keys(info).forEach((key) =>
      f.addControl(key, new FormControl(null))
    );
    return f;
  }

  consultarServicio() {
    if (!this.isConsultoCenso) this.consultarCenso();

    //this.consultarServicio1('1');
    //al validar el censo llama al resto
    //  this.consultarServicio1('2');
    //  this.consultarServicio2('1');
    //  this.consultarServicio2('2');
    //  this.consultarServicio3('1');
    //  this.consultarServicio3('2');
  }

  //en pruebas
  consultarCenso() {
    this.cosultarCenso1("1");
    this.cosultarCenso1("2");
    this.cosultarCenso2("1");
    this.cosultarCenso2("2");
    this.cosultarCenso3("1");
    this.cosultarCenso3("2");
    //this.isConsultoCenso = true;
  }

  cosultarCenso1(tipo: string) {
    const params = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("tipoCenso", "")
      .set("tipoRecurso", tipo);

    this.recursoServicioService
      .listarCenso(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          let fecha = response.data[0].fechaRegistro;
          this.f.patchValue({ fechaInventario: fecha });
          this.isConsultoCenso = true;

          if (tipo == "1") {
            this.toast.ok("Se cargo recursos censados");
          }
        } else {
          this.isConsultoCenso = false;
          if (tipo == "1") {
            this.toast.warn("No existen datos censados");
            /*this.messageService.add({
          key: 'tr',
          severity: 'success',
          detail: 'No existe datos del censo para el PMFI N° '+this.idPlanManejo,
        });*/
          }
        }

        response.data.forEach((element: any) => {
          //tabla 1
          let tabla1: any = {
            nombreCientifico: element.nombreCientifico,
            producto: element.productoTipo,
            nombreComun: element.nombreComun,
            idRecursoForestal: 0,
            idRecursoForestalDetalle: 0,
          };

          if (tipo == "1") {
            this.lstEspecies.push(tabla1);
          } else {
            this.lstEspeciesNoMade.push(tabla1);
          }
        });
      });
  }

  cosultarCenso2(tipo: string) {
    const params2 = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("tipoRecurso", tipo)
      .set("tipoInventario", "A");

    this.recursoServicioService
      .listarResumenesCenso(params2)
      .subscribe((response: any) => {
        let cantidadItems: number = response.data.length;
        response.data.forEach((element: any) => {
          //tabla 2
          let inventario: any = {
            estrada: element.parcela,
            indivAprov: element.numeroArbolesAprovechables,
            indivSem: element.numeroArbolSemilleros,
            productoObtener: element.producto,
            unidadMedida: element.unidadMedida,
            total: element.totalBarricas,
            aprov: element.numeroArbolesAprovechables,
            volumenComercial: element.volumenAprovechable,
            idRecursoForestal: 0,
            idRecursoForestalDetalle: 0,
            cantProducto: element.cantProducto,
          };

          if (tipo == "1") {
            this.lstResultadosInventarios.push(inventario);
            //calculo de totales
            this.totalAprovMade = this.totalAprovMade + inventario.indivAprov;
            this.totalSemiMade = this.totalSemiMade + inventario.indivSem;
            this.totalProdBarricasMade =
              this.totalProdBarricasMade + inventario.total;
            this.totalProdAprovMade =
              this.totalProdAprovMade + inventario.volumenComercial;
            if (cantidadItems == 0) {
              this.totalPromedioHaMade = 0;
            } else {
              this.totalPromedioHaMade =
                this.totalProdAprovMade / cantidadItems;
            }
          } else {
            this.lstResultadosInventariosNoMade.push(inventario);
            //calculo de totales
            this.totalAprovNoMade =
              this.totalAprovNoMade + inventario.indivAprov;
            this.totalSemiNoMade = this.totalSemiNoMade + inventario.indivSem;
            this.totalProdBarricasNoMade =
              this.totalProdBarricasNoMade + inventario.total;
            this.totalProdAprovNoMade =
              this.totalProdAprovNoMade + inventario.volumenComercial;
            if (cantidadItems == 0) {
              this.totalPromedioHaNoMade = 0;
            } else {
              this.totalPromedioHaNoMade =
                this.totalProdAprovNoMade / cantidadItems;
            }
          }
        });
      });
  }

  cosultarCenso3(tipo: string) {
    const params3 = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("tipoRecurso", tipo)
      .set("tipoInventario", "B");

    this.recursoServicioService
      .listarResumenesCenso(params3)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          //tabla 3
          let resumen: any = {
            especie: element.nombre,
            estrada: element.parcela,
            indivTotal: element.numeroArbolTotal,
            indivAprov: element.numeroArbolesAprovechables,
            indivSem: element.numeroArbolSemilleros,
            unidadMedida: element.unidadMedida,
            cantidad: element.cantidad,
            idRecursoForestal: 0,
            idRecursoForestalDetalle: 0,
            volumenAprovechable: element.volumenAprovechable,
          };
          if (tipo == "1") {
            this.lstResumenInventarios.push(resumen);
          } else {
            this.lstResumenInventariosNoMade.push(resumen);
          }
        });
      });
  }

  consultarServicio1(tipoMaderable: any): void {
    if (tipoMaderable == "1") {
      this.lstEspecies = [];
    } else {
      this.lstEspeciesNoMade = [];
    }

    const params = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("idRecursoForestal", tipoMaderable)
      .set("codCabecera", "IDEN");

    this.recursoServicioService
      .listarIdentificacionEspecieAprovechar(params)
      .subscribe((response: any) => {
        //validamos que exista data en recursoforestal
        if (response.data.length > 0) {
          this.toast.ok("Se cargó información del censo");

          response.data.forEach((element: any) => {
            let especie: any = {
              nombreCientifico: element.nombreCientifico,
              producto: element.producto,
              nombreComun: element.nombreComun,
              idRecursoForestal: element.idRecursoForestal,
              idRecursoForestalDetalle: element.idRecursoForestalDetalle,
            };

            if (tipoMaderable == "1") {
              this.lstEspecies.push(especie);
            } else {
              this.lstEspeciesNoMade.push(especie);
            }
          });

          if (tipoMaderable == "1") {
            this.consultarServicio1("2");
            this.consultarServicio2("1");
            this.consultarServicio2("2");
            this.consultarServicio3("1");
            this.consultarServicio3("2");
          }
        } else {
          if (tipoMaderable == "1") {
            this.isConsultoCenso = false;

            //llama al censo
            this.consultarServicio();
          }
        }
      });
  }

  consultarServicio2(tipoMaderable: any): void {
    if (tipoMaderable == "1") {
      this.lstResultadosInventarios = [];
    } else {
      this.lstResultadosInventariosNoMade = [];
    }

    const params = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("idRecursoForestal", tipoMaderable)
      .set("codCabecera", "INVE");

    this.recursoServicioService
      .listarIdentificacionEspecieAprovechar(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          let cantidadItems: number = response.data.length;

          response.data.forEach((element: any) => {
            let inventario: any = {
              estrada: element.numParcela,
              indivAprov: element.numeroArbolesAprovechables,
              indivSem: element.numeroArbolesSemilleros,
              productoObtener: element.producto,
              unidadMedida: element.uniMedida,
              total: element.numeroArbolesTotal,
              aprov: element.numeroArbolesAprovechables, //sera porcetaje
              volumenComercial: element.volumenComercial,
              idRecursoForestal: element.idRecursoForestal,
              idRecursoForestalDetalle: element.idRecursoForestalDetalle,
            };

            if (tipoMaderable == "1") {
              let infoDate = element.fechaRealizacionInventario;
              this.f.patchValue({ fechaInventario: infoDate });

              let infoDescripcion = element.descripcionFechaRealizacion;
              this.f.patchValue({ descripcionInventario: infoDescripcion });

              this.lstResultadosInventarios.push(inventario);
              this.totalAprovMade = this.totalAprovMade + inventario.indivAprov;
              this.totalSemiMade = this.totalSemiMade + inventario.indivSem;
              this.totalProdBarricasMade =
                this.totalProdBarricasMade + inventario.total;
              this.totalProdAprovMade =
                this.totalProdAprovMade + Number(inventario.volumenComercial);
              if (cantidadItems == 0) {
                this.totalPromedioHaMade = 0;
              } else {
                this.totalPromedioHaMade =
                  this.totalProdAprovMade / cantidadItems;
              }
            } else {
              let infoDate = element.fechaRealizacionInventario;
              this.f.patchValue({ fechaInventarioNoMaderable: infoDate });

              let infoDescripcion = element.descripcionFechaRealizacion;
              this.f.patchValue({
                descripcionInventarioNoMaderable: infoDescripcion,
              });

              this.lstResultadosInventariosNoMade.push(inventario);
              //calculo de totales
              this.totalAprovNoMade =
                this.totalAprovNoMade + inventario.indivAprov;
              this.totalSemiNoMade = this.totalSemiNoMade + inventario.indivSem;
              this.totalProdBarricasNoMade =
                this.totalProdBarricasNoMade + inventario.total;
              this.totalProdAprovNoMade =
                this.totalProdAprovNoMade + Number(inventario.volumenComercial);
              if (cantidadItems == 0) {
                this.totalPromedioHaNoMade = 0;
              } else {
                this.totalPromedioHaNoMade =
                  this.totalProdAprovNoMade / cantidadItems;
              }
            }
          });
        }
      });
  }

  consultarServicio3(tipoMaderable: any): void {
    if (tipoMaderable == "1") {
      this.lstResumenInventarios = [];
    } else {
      this.lstResumenInventariosNoMade = [];
    }

    const params = new HttpParams()
      .set("idPlanManejo", this.idPlanManejo.toString())
      .set("idRecursoForestal", tipoMaderable)
      .set("codCabecera", "RESU");

    this.recursoServicioService
      .listarIdentificacionEspecieAprovechar(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.forEach((element: any) => {
            let resumen: any = {
              especie: element.nombreComun,
              estrada: element.numParcela,
              indivTotal: element.numeroArbolesTotal,
              indivAprov: element.numeroArbolesAprovechables,
              indivSem: element.numeroArbolesSemilleros,
              unidadMedida: element.uniMedida,
              idRecursoForestal: element.idRecursoForestal,
              idRecursoForestalDetalle: element.idRecursoForestalDetalle,
              volumenAprovechable: element.volumenComercial,
              cantidad: element.cantidad,
            };

            if (tipoMaderable == "1") {
              this.lstResumenInventarios.push(resumen);
            } else {
              this.lstResumenInventariosNoMade.push(resumen);
            }
          });
        }
      });
  }

  abrirModal(tipo: string, tipoRecurso: string): void {
    if (tipo == "C") {
      this.especieModal.nombreCientifico = "";
      this.especieModal.nombreComun = "";
      this.especieModal.producto = "";
      this.especieModal.indice = null;
      this.especieModal.idRecursoForestal = 0;
      this.especieModal.idRecursoForestalDetalle = 0;
      this.especieModal.tipoRecurso = tipoRecurso;
    }

    this.tipoAccion = tipo;
    this.tituloModalMantenimiento1 = tipo == "C" ? "Registrar" : "Modificar";
    this.verModalMantenimiento1 = true;
  }

  abrirModalE(tipo: string, data: any, i: any, tipoRecurso: string): void {
    if (tipo == "E") {
      this.especieModal.nombreCientifico = data.nombreCientifico;
      this.especieModal.nombreComun = data.nombreComun;
      this.especieModal.producto = data.producto;
      this.especieModal.indice = i;
      this.especieModal.idRecursoForestal = data.idRecursoForestal;
      this.especieModal.idRecursoForestalDetalle =
        data.idRecursoForestalDetalle;
      this.especieModal.tipoRecurso = tipoRecurso;

      
    }

    this.tipoAccion = tipo;
    this.tituloModalMantenimiento1 = tipo == "C" ? "Registrar" : "Modificar";
    this.verModalMantenimiento1 = true;
  }

  openEliminar(e: any, data: any, index: any, tipoRecurso: string): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (Number(data.idCensoForestalDetalle) == 0) {
          if (tipoRecurso == "1") {
            this.listAnexo2Maderable.splice(index, 1);
            this.pendiente = true;
          } else if (tipoRecurso == "2") {
            this.listAnexo2NoMaderable.splice(index, 1);
            this.pendiente = true;
          }
        } else {
          //llamar al servicio eliminar
          let objeto = {
            idCensoForestalDetalle: data.idCensoForestalDetalle,
            idUsuarioElimina: this.usuarioServ.usuario.idusuario,
          };

          this.censoForestalService
            .eliminarCensoForestalDetalle(objeto)
            .subscribe((response: any) => {
              if (response.success) {
                this.toast.ok("Se eliminó la especie correctamente.");
                this.listarAnexo2Maderable();
                this.listarAnexo2NoMaderable();
              } else {
                this.toast.warn("Ocurrió un problema, intente nuevamente");
              }
            });
        }
      },
      reject: () => {},
    });
  }

  registrar(): void {
    if (this.especieModal.nombreCientifico == "") {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "El campo nombre científico es obligatorio.",
      });
      return;
    }

    if (this.especieModal.nombreComun == "") {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "El campo nombre común es obligatorio.",
      });

      return;
    }

    if (this.especieModal.producto == "") {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "El campo producto es obligatorio.",
      });

      return;
    }

    if (this.tipoAccion == "C") {
      let newObjeto = {
        idRecursoForestal: this.especieModal.idRecursoForestal,
        idRecursoForestalDetalle: this.especieModal.idRecursoForestalDetalle,
        nombreCientifico: this.especieModal.nombreCientifico,
        nombreComun: this.especieModal.nombreComun,
        producto: this.especieModal.producto,
      };

      if (this.especieModal.tipoRecurso == "1") {
        this.lstEspecies.push(newObjeto);
        this.pendiente = true;
      } else if (this.especieModal.tipoRecurso == "2") {
        if (this.lstEspeciesNoMade.length != 0) {
          this.lstEspeciesNoMade.forEach((item) => {
            if (
              item.nombreCientifico == newObjeto.nombreCientifico &&
              item.nombreComun == newObjeto.nombreComun
            ) {
              this.toast.warn("La Especie ya existe");
            } else {
              this.lstEspeciesNoMade.push(newObjeto);
              this.pendiente = true;
            }
          });
        } else {
          this.lstEspeciesNoMade.push(newObjeto);
          this.pendiente = true;
        }
      }
    } else if (this.tipoAccion == "E") {
      if (this.especieModal.idRecursoForestalDetalle == 0) {
        let indix = this.especieModal.indice;
        if (this.especieModal.tipoRecurso == "1") {
          this.lstEspecies[
            indix
          ].nombreCientifico = this.especieModal.nombreCientifico;
          this.lstEspecies[indix].nombreComun = this.especieModal.nombreComun;
          this.lstEspecies[indix].producto = this.especieModal.producto;
          this.pendiente = true;
        } else if (this.especieModal.tipoRecurso == "2") {
          this.lstEspeciesNoMade[
            indix
          ].nombreCientifico = this.especieModal.nombreCientifico;
          this.lstEspeciesNoMade[
            indix
          ].nombreComun = this.especieModal.nombreComun;
          this.lstEspeciesNoMade[indix].producto = this.especieModal.producto;
          this.pendiente = true;
        }
      } else {
        if (this.especieModal.tipoRecurso == "1") {
          //si tiene id filtar y reeemplazar
          let index = this.lstEspecies.findIndex(
            (x: any) =>
              x.idRecursoForestalDetalle ===
              this.especieModal.idRecursoForestalDetalle
          );
          this.lstEspecies[
            index
          ].nombreCientifico = this.especieModal.nombreCientifico;
          this.lstEspecies[index].nombreComun = this.especieModal.nombreComun;
          this.lstEspecies[index].producto = this.especieModal.producto;
          this.pendiente = true;
        } else if (this.especieModal.tipoRecurso == "2") {
          //si tiene id filtar y reeemplazar
          let index = this.lstEspeciesNoMade.findIndex(
            (x: any) =>
              x.idRecursoForestalDetalle ===
              this.especieModal.idRecursoForestalDetalle
          );
          this.lstEspeciesNoMade[
            index
          ].nombreCientifico = this.especieModal.nombreCientifico;
          this.lstEspeciesNoMade[
            index
          ].nombreComun = this.especieModal.nombreComun;
          this.lstEspeciesNoMade[index].producto = this.especieModal.producto;
          this.pendiente = true;
        }
      }
    }

    this.verModalMantenimiento1 = false;
  }

  guardarTodo() {
    this.f.get("fechaInventario")?.value;

    this.f.get("fechaInventarioNoMaderable")?.value;

    if (this.f.get("fechaInventario")?.value == null) {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "La fecha de realización del inventario Maderable esta vacía.",
      });
      return;
    }

    if (this.f.get("fechaInventarioNoMaderable")?.value == null) {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail:
          "La fecha de realización del inventario No Maderable esta vacía.",
      });
      return;
    }

    let arraySave: any[] = [];
    //---------
    //maderable
    if (this.lstEspecies.length > 0) {
      let idRecursoFors: any = this.lstEspecies[0].idRecursoForestal;
      let objIdentificacion = {
        idRecursoForestal: idRecursoFors,
        codigoTipoRecursoForestal: "1",
        fechaRealizacionInventario: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        listRecursoForestal: [{}],
      };

      this.lstEspecies.forEach((elem: any) => {
        let objto = {
          idRecursoForestalDetalle: elem.idRecursoForestalDetalle,
          codigoCabecera: "IDEN",
          nombreComun: elem.nombreComun,
          nombreCientifico: elem.nombreCientifico,
          productoTipo: elem.producto,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
          idUsuarioModificacion: this.usuarioServ.idUsuario,
        };

        objIdentificacion.listRecursoForestal.push(objto);
      });

      if (objIdentificacion.listRecursoForestal.length > 1) {
        objIdentificacion.listRecursoForestal.shift();
      }

      arraySave.push(objIdentificacion);
    }

    //NO maderable
    if (this.lstEspeciesNoMade.length > 0) {
      let idRecursoFors: any = this.lstEspeciesNoMade[0].idRecursoForestal;

      let objIdentificacion = {
        idRecursoForestal: idRecursoFors,
        codigoTipoRecursoForestal: "2",
        idPlanManejo: this.idPlanManejo,
        fechaRealizacionInventario: null,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        listRecursoForestal: [{}],
      };

      this.lstEspeciesNoMade.forEach((elem: any) => {
        let objto = {
          idRecursoForestalDetalle: elem.idRecursoForestalDetalle,
          codigoCabecera: "IDEN",
          nombreComun: elem.nombreComun,
          nombreCientifico: elem.nombreCientifico,
          productoTipo: elem.producto,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
          idUsuarioModificacion: this.usuarioServ.idUsuario,
        };
        objIdentificacion.listRecursoForestal.push(objto);
      });

      if (objIdentificacion.listRecursoForestal.length > 1) {
        objIdentificacion.listRecursoForestal.shift();
      }

      arraySave.push(objIdentificacion);
    }

    //--a resultados de inventario
    if (this.lstResultadosInventarios.length > 0) {
      let idRecursoFors2: any = this.lstResultadosInventarios[0]
        .idRecursoForestal;
      //maderable
      let objIdentificacion2 = {
        idRecursoForestal: idRecursoFors2,
        codigoTipoRecursoForestal: "1",
        fechaRealizacionInventario: this.f.get("fechaInventario")?.value,
        descripcionFechaRealizacion: this.f.get("descripcionInventario")?.value,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        listRecursoForestal: [{}],
      };

      this.lstResultadosInventarios.forEach((elem: any) => {
        let objto = {
          idRecursoForestalDetalle: elem.idRecursoForestalDetalle,
          codigoCabecera: "INVE",
          numParcelaCorte: elem.estrada,
          numeroArbolesAprovechables: elem.indivAprov,
          numeroArbolesSemilleros: elem.indivSem,
          numeroArbolesTotal: elem.total,
          productoTipo: elem.productoObtener,
          unidadMedida: elem.unidadMedida,
          volumenComercial: elem.volumenComercial,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
          idUsuarioModificacion: this.usuarioServ.idUsuario,
        };

        objIdentificacion2.listRecursoForestal.push(objto);
      });

      if (objIdentificacion2.listRecursoForestal.length > 1) {
        objIdentificacion2.listRecursoForestal.shift();
      }
      arraySave.push(objIdentificacion2);
    }

    ///a NO maderables
    if (this.lstResultadosInventariosNoMade.length > 0) {
      let idRecursoFors2: any = this.lstResultadosInventariosNoMade[0]
        .idRecursoForestal;
      //maderable
      let objIdentificacion2 = {
        idRecursoForestal: idRecursoFors2,
        codigoTipoRecursoForestal: "2",
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        fechaRealizacionInventario: this.f.get("fechaInventarioNoMaderable")
          ?.value,
        descripcionFechaRealizacion: this.f.get(
          "descripcionInventarioNoMaderable"
        )?.value,
        listRecursoForestal: [{}],
      };

      this.lstResultadosInventariosNoMade.forEach((elem: any) => {
        let objto = {
          idRecursoForestalDetalle: elem.idRecursoForestalDetalle,
          codigoCabecera: "INVE",
          numParcelaCorte: elem.estrada,
          numeroArbolesAprovechables: elem.indivAprov,
          numeroArbolesSemilleros: elem.indivSem,
          numeroArbolesTotal: elem.total,
          productoTipo: elem.productoObtener,
          unidadMedida: elem.unidadMedida,
          volumenComercial: elem.volumenComercial,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
          idUsuarioModificacion: this.usuarioServ.idUsuario,
        };
        objIdentificacion2.listRecursoForestal.push(objto);
      });

      if (objIdentificacion2.listRecursoForestal.length > 1) {
        objIdentificacion2.listRecursoForestal.shift();
      }
      arraySave.push(objIdentificacion2);
    }

    //-------------
    //--b Resumen de Resultado
    if (this.lstResumenInventarios.length > 0) {
      let idRecursoFors3: any = this.lstResumenInventarios[0].idRecursoForestal;
      //maderable
      let objIdentificacion3 = {
        idRecursoForestal: idRecursoFors3,
        codigoTipoRecursoForestal: "1",
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        fechaRealizacionInventario: null,
        listRecursoForestal: [{}],
      };

      this.lstResumenInventarios.forEach((elem: any) => {
        let objto = {
          idRecursoForestalDetalle: elem.idRecursoForestalDetalle,
          codigoCabecera: "RESU",
          nombreComun: elem.especie,
          numParcelaCorte: elem.estrada,
          numeroArbolesTotal: elem.indivTotal,
          numeroArbolesAprovechables: elem.indivAprov,
          numeroArbolesSemilleros: elem.indivSem,
          unidadMedida: elem.unidadMedida,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
          idUsuarioModificacion: this.usuarioServ.idUsuario,
          volumenComercial: elem.volumenAprovechable,
          cantidad: elem.cantidad,
        };

        objIdentificacion3.listRecursoForestal.push(objto);
      });

      //eliminar el objeto vacio
      if (objIdentificacion3.listRecursoForestal.length > 1) {
        objIdentificacion3.listRecursoForestal.shift();
      }
      arraySave.push(objIdentificacion3);
    }

    //b - no maderables

    if (this.lstResumenInventariosNoMade.length > 0) {
      let idRecursoFors3: any = this.lstResumenInventariosNoMade[0]
        .idRecursoForestal;
      //maderable
      let objIdentificacion3 = {
        idRecursoForestal: idRecursoFors3,
        codigoTipoRecursoForestal: "2",
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.usuarioServ.idUsuario,
        fechaRealizacionInventario: null,
        listRecursoForestal: [{}],
      };

      this.lstResumenInventariosNoMade.forEach((elem: any) => {
        let objto = {
          idRecursoForestalDetalle: elem.idRecursoForestalDetalle,
          codigoCabecera: "RESU",
          nombreComun: elem.especie,
          numParcelaCorte: elem.estrada,
          numeroArbolesTotal: elem.indivTotal,
          numeroArbolesAprovechables: elem.indivAprov,
          numeroArbolesSemilleros: elem.indivSem,
          unidadMedida: elem.unidadMedida,
          idUsuarioRegistro: this.usuarioServ.idUsuario,
          idUsuarioModificacion: this.usuarioServ.idUsuario,
          volumenComercial: elem.volumenAprovechable,
          cantidad: elem.cantidad,
        };

        objIdentificacion3.listRecursoForestal.push(objto);
      });

      if (objIdentificacion3.listRecursoForestal.length > 1) {
        objIdentificacion3.listRecursoForestal.shift();
      }
      arraySave.push(objIdentificacion3);
    }

    this.recursoServicioService
      .registrarRecursosyServicios(arraySave)
      .subscribe((response: any) => {
        if (response.success) {
          this.messageService.add({
            key: "tr",
            severity: "success",
            detail:
              "Se registró la Información de Especies, Recursos o Servicios correctamente.",
          });
          this.consultarServicio();
          this.pendiente = false;
        } else {
          this.messageService.add({
            key: "tr",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente",
          });
        }
      });
  }

  btnDescargarFormato() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.nombreGenerado = UrlFormatos.CARGA_MASIVA_PMFI;
    this.archivoService
      .descargarPlantilla(this.nombreGenerado)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess == true) {
          this.toast.ok(res?.message);
          this.plantillaPMFI = res;
          descargarArchivo(this.plantillaPMFI);
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  openModalEditarArbol(
    message: string,
    type: number,
    action: string,
    rowindex: any,
    item?: any
  ) {
    this.ref = this.dialogService.open(ModalMaderablesComponent, {
      header: message,
      width: "40%",
      contentStyle: { "max-height": "200px", overflow: "auto" },
      data: {
        idPlanManejo: this.idPlanManejo,
        form: item,
        type: action,
        idCensoForestal: this.idCensoForestal,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (!!resp) {
        resp.idPlanManejo = this.idPlanManejo
        if (type == 1 && action == "C") {
          resp.idTipoRecurso = "RECTMAD";
          if (resp.productoTipo == "PROTMAR") {
            resp.productoDescr = "Madera en rollo";
          } else if (resp.productoTipo == "PROTHOJ") {
            resp.productoDescr = "Hojas";
          } else if (resp.productoTipo == "PROTFRU") {
            resp.productoDescr = "Frutos";
          } else if (resp.productoTipo == "PROTACE") {
            resp.productoDescr = "Aceites";
          } else if (resp.productoTipo == "PROTLAT") {
            resp.productoDescr = "Latex";
          } else if (resp.productoTipo == "PROTCOR") {
            resp.productoDescr = "Corteza";
          } else if (resp.productoTipo == "PROTOTR") {
            resp.productoDescr = "Otro";
          }
          this.listAnexo2Maderable.push(resp);
        } else if (type == 1 && action == "E") {
          if (resp.productoTipo == "PROTMAR") {
            resp.productoDescr = "Madera en rollo";
          } else if (resp.productoTipo == "PROTHOJ") {
            resp.productoDescr = "Hojas";
          } else if (resp.productoTipo == "PROTFRU") {
            resp.productoDescr = "Frutos";
          } else if (resp.productoTipo == "PROTACE") {
            resp.productoDescr = "Aceites";
          } else if (resp.productoTipo == "PROTLAT") {
            resp.productoDescr = "Latex";
          } else if (resp.productoTipo == "PROTCOR") {
            resp.productoDescr = "Corteza";
          } else if (resp.productoTipo == "PROTOTR") {
            resp.productoDescr = "Otro";
          }
          this.listAnexo2Maderable[rowindex] = resp;
        } else if (type == 2 && action == "C") {
          resp.idTipoRecurso = "RECTNOMA";
          if (resp.productoTipo == "PROTMAR") {
            resp.productoDescr = "Madera en rollo";
          } else if (resp.productoTipo == "PROTHOJ") {
            resp.productoDescr = "Hojas";
          } else if (resp.productoTipo == "PROTFRU") {
            resp.productoDescr = "Frutos";
          } else if (resp.productoTipo == "PROTACE") {
            resp.productoDescr = "Aceites";
          } else if (resp.productoTipo == "PROTLAT") {
            resp.productoDescr = "Latex";
          } else if (resp.productoTipo == "PROTCOR") {
            resp.productoDescr = "Corteza";
          } else if (resp.productoTipo == "PROTOTR") {
            resp.productoDescr = "Otro";
          }
          this.listAnexo2NoMaderable.push(resp);
        } else if (type == 2 && action == "E") {
          if (resp.productoTipo == "PROTMAR") {
            resp.productoDescr = "Madera en rollo";
          } else if (resp.productoTipo == "PROTHOJ") {
            resp.productoDescr = "Hojas";
          } else if (resp.productoTipo == "PROTFRU") {
            resp.productoDescr = "Frutos";
          } else if (resp.productoTipo == "PROTACE") {
            resp.productoDescr = "Aceites";
          } else if (resp.productoTipo == "PROTLAT") {
            resp.productoDescr = "Latex";
          } else if (resp.productoTipo == "PROTCOR") {
            resp.productoDescr = "Corteza";
          } else if (resp.productoTipo == "PROTOTR") {
            resp.productoDescr = "Otro";
          }
          this.listAnexo2NoMaderable[rowindex] = resp;
        }
      }
    });
  }

  registrarMaderable() {
    
    this.censoForestalService
      .registrarCensoForestalDet(this.listAnexo2Maderable)
      .subscribe((response) => {
        if (response.success == true) {
          this.listarAnexo2Maderable();
          this.toast.ok("Se registró la Especie correctamente.");
        } else {
          this.toast.ok("Ocurrio un error al registrar la Especie.");
        }
      });
  }

  registraNoMaderable() {
    
    this.censoForestalService
      .registrarCensoForestalDet(this.listAnexo2NoMaderable)
      .subscribe((response) => {
        if (response.success == true) {
          
          this.listarAnexo2NoMaderable();
          this.toast.ok("Se registró la Especie correctamente.");
        } else {
          this.toast.ok("Ocurrio un error al registrar la Especie.");
        }
      });
  }

  registrarFecha() {
    this.listFecha = [];
    var obj = new CensoMaderable();
    obj.idUsuarioRegistro = this.usuarioServ.idUsuario;
    obj.fechaRealizacion = new Date(this.fechaRealizacion);
    obj.idCensoForestal = this.idCensoForestal;
    obj.idCensoForestalDetalle = null;

    this.listFecha.push(obj);
    

    this.censoForestalService
      .registrarCensoForestalDet(this.listFecha)
      .subscribe((response) => {
        if (response.success == true) {
          this.listarFecha();
          this.toast.ok("Se registró la fecha correctamente.");
        } else {
          this.toast.ok("Ocurrio un error al registrar la fecha.");
        }
      });
  }

  listarFecha() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: "PMFI",
    };
    this.censoForestalService
      .obtenerCensoForestal(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          var obj = new CensoMaderable(response.data);
          this.fechaRealizacion = obj.fechaRealizacion;
          
        } else {
          this.toast.ok("Ocurrio un error al listar la fecha.");
        }
      });
  }

  /*   btnDescargarFormato() {
    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }

    window.location.href = urlExcel;
  } */

  btnCargar() {
    if (!(this.archivo.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService
      .registrarCargaMasiva(this.archivo.file)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok("Se registró el archivo correctamente.\n");
          this.listarAnexo2Maderable();
          this.listarAnexo2NoMaderable();
          this.listarAnexo2InventarioMaderableB();
          this.listarAnexo2InventarioNoMaderableB();
          this.listarResultadosInventarioMaderableA();
          this.archivo.file = "";
        } else {
          this.toast.error(
            "Error al procesar archivo, el archivo seleccionado no cumple con el formato de carga masiva."
          );
        }
      });
  }

  listarAnexo2Maderable() {
    this.listAnexo2Maderable = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoRecurso: 1,
    };
    

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .listarCensoForestalDetalleMaderable(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.idCensoForestal = response.data[0].idCensoForestal;
          response.data.forEach((element: any) => {
            var obj = new CensoMaderable(element);
            obj.idUsuarioRegistro = this.usuarioServ.idUsuario;
            this.listAnexo2Maderable.push(obj);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn("No hay elementos en el listado");
          }
        }
      });
  }

  listarAnexo2NoMaderable() {
    this.listAnexo2NoMaderable = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoRecurso: 2,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .listarCensoForestalDetalleMaderable(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            var obj = new CensoMaderable(element);
            obj.idUsuarioRegistro = this.usuarioServ.idUsuario;
            this.listAnexo2NoMaderable.push(obj);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            // this.toast.warn(response.message);
          }
        }
      });
  }

  listarResultadosInventarioMaderableA() {
    this.listResultadosInventarioMaderableA = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: "PMFI",
      tipoProceso: 1,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .ResultadosInventarios(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.listResultadosInventarioMaderableA.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  listarAnexo2InventarioMaderableB() {
    this.listAnexo2InventarioMaderableB = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: "PMFI",
      tipoProceso: 3,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.listAnexo2InventarioMaderableB.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  listarResultadosInventarioNoMaderableA() {
    this.listResultadosInventarioNoMaderableA = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: "PMFI",
      tipoProceso: 2,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .ResultadosInventarios(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.listResultadosInventarioNoMaderableA.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  listarAnexo2InventarioNoMaderableB() {
    this.listAnexo2InventarioNoMaderableB = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: "PMFI",
      tipoProceso: 4,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.listAnexo2InventarioNoMaderableB.push(element);
          });
        } else {
          if (this.showWarn) {
            this.showWarn = false;
            this.toast.warn(response.message);
          }
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(
                this.detEvaluacion,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon
                )
              );
            } else {
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.usuarioServ.idUsuario,
              };
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.detEvaluacion])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion() {
    localStorage.setItem(
      "EvalResuDet",
      JSON.stringify({
        tab: "RFNM",
        acordeon: "",
      })
    );
    this.router.navigateByUrl(
      "/planificacion/evaluacion/requisitos-previos/" +
        this.idPlanManejo +
        "/" +
        this.codigoProceso
    );
  }
}

export class Model1 {
  constructor(data?: any) {}
}
