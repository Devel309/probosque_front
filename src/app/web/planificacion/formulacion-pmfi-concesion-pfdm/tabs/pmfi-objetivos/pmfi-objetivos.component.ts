import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ObjetivosService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/objetivos.service";
import {CodigosPMFI} from '../../../../../model/util/PMFI/PMFI';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {Router} from '@angular/router';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';

@Component({
  selector: "app-pmfi-objetivos",
  templateUrl: "./pmfi-objetivos.component.html",
  styleUrls: ["./pmfi-objetivos.component.scss"],
})
export class PmfiObjetivosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() isPerfilArffs!: boolean;

  @Input() disabled!: boolean;
  objetivo: string = "";
  idObj: number = 0;
  listaObjetivos: any[] = [];
  isEdit = false;


  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_2;

  codigoAcordeon: string = CodigosPMFI.TAB_2;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  constructor(
    private user: UsuarioService,
    private objetivosService: ObjetivosService,
    private dialog: MatDialog,
    private toast: ToastService,
    private confirm: ConfirmationService,private evaluacionService: EvaluacionService,private router: Router,
  ) {}

  ngOnInit(): void {
    this.listarObjetivos();
    if (this.isPerfilArffs) this.obtenerEvaluacion();

  }
  listarObjetivos() {
    this.listaObjetivos = [];
    let params = {
      codigoObjetivo: "PMFI",
      idPlanManejo: this.idPlanManejo,
    };

    this.objetivosService
      .listarObjetivoManejo(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.listaObjetivos.push(element);
        });
      });
  }

  crearObjetivos() {

    if (this.objetivo === "") {
      this.toast.warn("El campo Objetivo es obligatorio.");

    } else {

      let params = {
        codigoObjetivo: "PMFI",
        estado: "A",
        idUsuarioModificacion: null,
        idUsuarioRegistro: this.user.idUsuario,
        idObjManejo: null,
        general: this.objetivo,
        planManejo: {
          idPlanManejo: this.idPlanManejo,
        },

      };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.objetivosService
        .registrarObjetivoManejo(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.objetivo = "";
            this.listarObjetivos();
          } else {
            this.toast.error(res?.message);
            this.objetivo = "";
          }
        });
    }

  }

  editarObj(item: any) {
    this.objetivo = item.general;
    this.isEdit = true;
    this.idObj = item.idObjManejo;
  }

  editarObjetivos() {
    if (this.objetivo === "") {
      this.toast.warn("El campo Objetivo es obligatorio.");

    } else {
      let params = {
        estado: "A",
        idUsuarioModificacion: this.user.idUsuario,
        idUsuarioRegistro: null,
        idObjManejo: this.idObj,
        general: this.objetivo,
        planManejo: null,
      };

      this.objetivosService
        .actualizarObjetivoManejo(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
          if (res.success == true) {
            this.toast.ok(res?.message);
            this.objetivo = "";
            this.idObj = 0;
            this.isEdit = false;
            this.listarObjetivos();
          } else {
            this.toast.error(res?.message);
            this.objetivo = "";
          }
        });
    }
  }

  eliminarObjetivo(target: any, idObjManejo: number) {
    this.confirm.confirm({
      target,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => this.eliminar(idObjManejo),
    });
  }

  eliminar(idObjManejo: number) {
    let params = {
      idUsuarioElimina: this.user.idUsuario,
      idObjManejo: idObjManejo,
    };
    this.objetivosService
      .eliminarObjetivoManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok("Se eliminó el Objetivo de Manejo correctamente");
          this.objetivo = "";
          this.listarObjetivos();
        } else {
          this.toast.error(res?.message);
          this.objetivo = "";
        }
      });
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(this.detEvaluacion,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon));
            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP626"
    }));


    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

}
