import { Component, Input, OnInit, Output, EventEmitter} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ImpactoAmbientalPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/impacto-ambiental-pmfi.service";
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';
import {CodigosPMFI} from '../../../../../model/util/PMFI/PMFI';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';

@Component({
  selector: "app-pmfi-medida-proteccion-inpacto-ambiental",
  templateUrl: "./pmfi-medida-proteccion-inpacto-ambiental.component.html",
  styleUrls: ["./pmfi-medida-proteccion-inpacto-ambiental.component.scss"],
})
export class PmfiMedidaProteccionInpactoAmbientalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @Input() disabled!: boolean;
  lstDetalle1: Model1[] = [];
  tituloModalMantenimiento1: string = "";
  verModalMantenimiento1: boolean = false;
  tipoAccion1: string = "";
  context1: Model1 = new Model1();
  lstDetalleParams: any[] = [];
  pendiente: boolean = false;
  idImpactoAmbiental: number = 0;

  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_7;

  codigoAcordeon: string = CodigosPMFI.TAB_7;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  constructor(
    private confirmationService: ConfirmationService,
    private impactoAmbientalPmfiService: ImpactoAmbientalPmfiService,
    private user: UsuarioService,
    private dialog: MatDialog,
    private toast: ToastService,private evaluacionService: EvaluacionService,private router: Router,

  ) {}

  ngOnInit(): void {
    this.listarPMFI();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarPMFI() {
    this.lstDetalle1 = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoImpactoAmbiental: "PMFIMP",
      codigoImpactoAmbientalDet: "PMFIMPPRO",
    };

    this.impactoAmbientalPmfiService
      .listarPmfiImpactoAmbiental(body)
      .subscribe((response: any) => {
        this.idImpactoAmbiental = response.idImpactoAmbiental;
        if (response.data.detalle.length != 0) {
          response.data.detalle.forEach((element: any) => {
            this.lstDetalle1.push(
              new Model1({
                showRemove: false,
                medidasProteccion: element.medidasProteccion,
                descripcion: element.descripcion,
                idImpactoambientaldetalle: element.idImpactoambientaldetalle,
              })
            );
          });
        } else {
          this.listaPmfiDetalle();
        }
      });
  }

  listaPmfiDetalle() {
    this.lstDetalle1 = [];
    let body = {
      idPlanManejo: 0,
      codigoImpactoAmbiental: "PMFIMP",
      codigoImpactoAmbientalDet: "PMFIMPPRO",
    };

    this.impactoAmbientalPmfiService
      .listarPmfiImpactoAmbiental(body)
      .subscribe((response: any) => {
        if (response.data) {
          response.data.detalle.forEach((element: any) => {
            this.lstDetalle1.push(
              new Model1({
                showRemove: false,
                medidasProteccion: element.medidasProteccion,
                descripcion: element.descripcion,
                idImpactoambientaldetalle: element.idImpactoambientaldetalle,
              })
            );
          });
        }
      });
  }

  abrirModal(tipo: string, rowIndex:number, data?: Model1) {
    this.tipoAccion1 = tipo;
    if (this.tipoAccion1 == "C") {
      this.tituloModalMantenimiento1 = "Registro de Medida de Protección";
      this.context1 = new Model1();
    } else {
      this.tituloModalMantenimiento1 = "Editar Medida de Protección";
      this.context1 = new Model1(data);
      this.context1.id = rowIndex;
    }
    this.verModalMantenimiento1 = true;
  }

  openEliminar(e: any, data: Model1, rowIndex: number): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => this.eliminar(data, rowIndex),
      reject: () => {},
    });
  }

  eliminar(data: any, rowIndex: number) {
    if (data.idImpactoambientaldetalle != 0) {
      let params = {
        idImpactoambientaldetalle: data.idImpactoambientaldetalle,
        idUsuarioElimina: this.user.idUsuario,
      };

      this.impactoAmbientalPmfiService
        .eliminarImpactoAmbientalDetalle(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
          if (res.success == true) {
            this.toast.ok('Se eliminó la medida de protección correctamente.');
            this.listarPMFI();
          } else {
            this.toast.error(res?.message);
          }
        });
    } else {
      this.lstDetalle1.splice(rowIndex, 1);
      this.toast.ok("Se eliminó la medida de protección correctamente.");
      this.pendiente = true;
    }
  }

  registrar(context?: any): void {
    if(this.context1.medidasProteccion == ''){
      this.toast.warn("El campo Medida es obligatorio.");
      return;
    }
    if (this.tipoAccion1 == "C") {
      this.lstDetalle1.push(this.context1);
      this.pendiente = true;
    }
    if (this.tipoAccion1 == "E") {
      if (this.context1.idImpactoambientaldetalle != 0) {
        this.context1 = new Model1(context);
        let index = this.lstDetalle1.findIndex(
          (x) =>
            x.idImpactoambientaldetalle == context.idImpactoambientaldetalle
        );
        this.lstDetalle1[index] = this.context1;
        this.pendiente = true;
      } else {
        this.context1 = new Model1(context);
        //let index = this.lstDetalle1.findIndex((x) => x.id == context.id);
        this.lstDetalle1[context.id] = context;
        this.pendiente = true;
      }
    }
    this.verModalMantenimiento1 = false;
  }

  guardar() {
    this.lstDetalleParams = [];
    this.lstDetalle1.forEach((x) => {
      this.lstDetalleParams.push({
        idUsuarioRegistro: this.user.idUsuario,
        medidasProteccion: x.medidasProteccion ? x.medidasProteccion : "",
        descripcion: x.descripcion ? x.descripcion : "",
        idImpactoambientaldetalle: x.idImpactoambientaldetalle
          ? x.idImpactoambientaldetalle
          : 0,
        idImpactoambiental: x.idImpactoambiental ? x.idImpactoambiental : 0,
        medidasprevencion: x.medidasprevencion ? x.medidasprevencion : "",
        codigoImpactoAmbientalDet: x.codigoImpactoAmbientalDet
          ? x.codigoImpactoAmbientalDet
          : "",
        medidasMitigacion: x.medidasMitigacion ? x.medidasMitigacion : "",
        actividad: x.actividad ? x.actividad : "",
      });
    });
    var params = {
      idUsuarioRegistro: this.user.idUsuario,
      idImpactoAmbiental: 0,
      codigoImpactoAmbiental: "PMFIMP",
      idPlanManejo: this.idPlanManejo,
      detalle: this.lstDetalleParams,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impactoAmbientalPmfiService
      .registrarImpactoAmbientalPmfi(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        this.pendiente = false;
        if (res.success == true) {
          this.toast.ok('Se registró la medida de protección correctamente.');
          this.listarPMFI();
        } else {
          this.toast.error(res?.message);
          this.listarPMFI();
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(this.detEvaluacion,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon));
            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP629"
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

}

export class Model1 {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.showRemove = data.showRemove;
      this.descripcion = data.descripcion;
      this.medidasProteccion = data.medidasProteccion;
      this.idImpactoambientaldetalle = data.idImpactoambientaldetalle;
      this.idImpactoambiental = data.idImpactoambiental;
      this.medidasprevencion = data.medidasprevencion;
      this.codigoImpactoAmbientalDet = "PMFIMPPRO";
      this.medidasMitigacion = data.medidasMitigacion;
      this.actividad = data.actividad;
      return;
    } else {
      //this.id = new Date().toISOString();
    }
  }
  id: number = 0;
  showRemove: boolean = true;
  medidasProteccion: string = "";
  descripcion: string = "";
  idImpactoambientaldetalle: number = 0;
  idImpactoambiental: number = 0;
  medidasprevencion: string = "";
  codigoImpactoAmbientalDet: string = "PMFIMPPRO";
  medidasMitigacion: string = "";
  actividad: string = "";
}
