import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ImpactoAmbientalPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/impacto-ambiental-pmfi.service";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { PmfiModalFormularioCronogramaComponent } from "./modal/modal-formulario-zonas/modal-formulario-cronograma.component";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigosPMFI } from "../../../../../model/util/PMFI/PMFI";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { Router } from "@angular/router";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { ToastService } from "@shared";

@Component({
  selector: "app-pmfi-cronograma",
  templateUrl: "./pmfi-cronograma.component.html",
  styleUrls: ["./pmfi-cronograma.component.scss"],
})
export class PmfiCronogramaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @Input() disabled!: boolean;
  ref!: DynamicDialogRef;
  context: Modelo = new Modelo();

  verAnio1: boolean = true;
  verAnio2: boolean = false;
  verAnio3: boolean = false;
  verAnio4: boolean = false;
  verAnio5: boolean = false;
  verAnio6: boolean = false;
  verAnio7: boolean = false;
  verAnio8: boolean = false;
  verAnio9: boolean = false;
  verAnio10: boolean = false;
  verAnio11: boolean = false;
  verAnio12: boolean = false;
  verAnio13: boolean = false;
  verAnio14: boolean = false;
  verAnio15: boolean = false;
  verAnio16: boolean = false;
  verAnio17: boolean = false;
  verAnio18: boolean = false;
  verAnio19: boolean = false;
  verAnio20: boolean = false;

  lstDetalle: Modelo[] = [];
  lstOtrosAnos: any[] = [];
  params: any;
  vigencia: Number = 0;

  cmbAnios: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [
        { label: "Año 2 - Mes 1", value: "2-1" },
        { label: "Año 2 - Mes 2", value: "2-2" },
        { label: "Año 2 - Mes 3", value: "2-3" },
        { label: "Año 2 - Mes 4", value: "2-4" },
        { label: "Año 2 - Mes 5", value: "2-5" },
        { label: "Año 2 - Mes 6", value: "2-6" },
        { label: "Año 2 - Mes 7", value: "2-7" },
        { label: "Año 2 - Mes 8", value: "2-8" },
        { label: "Año 2 - Mes 9", value: "2-9" },
        { label: "Año 2 - Mes 10", value: "2-10" },
        { label: "Año 2 - Mes 11", value: "2-11" },
        { label: "Año 2 - Mes 12", value: "2-12" },
      ],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [
        { label: "Año 3 - Mes 1", value: "3-1" },
        { label: "Año 3 - Mes 2", value: "3-2" },
        { label: "Año 3 - Mes 3", value: "3-3" },
        { label: "Año 3 - Mes 4", value: "3-4" },
        { label: "Año 3 - Mes 5", value: "3-5" },
        { label: "Año 3 - Mes 6", value: "3-6" },
        { label: "Año 3 - Mes 7", value: "3-7" },
        { label: "Año 3 - Mes 8", value: "3-8" },
        { label: "Año 3 - Mes 9", value: "3-9" },
        { label: "Año 3 - Mes 10", value: "3-10" },
        { label: "Año 3 - Mes 11", value: "3-11" },
        { label: "Año 3 - Mes 12", value: "3-12" },
      ],
    },
  ];

  cmbAniosDefault: any[] = [
    {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [
        { label: "Año 2 - Mes 1", value: "2-1" },
        { label: "Año 2 - Mes 2", value: "2-2" },
        { label: "Año 2 - Mes 3", value: "2-3" },
        { label: "Año 2 - Mes 4", value: "2-4" },
        { label: "Año 2 - Mes 5", value: "2-5" },
        { label: "Año 2 - Mes 6", value: "2-6" },
        { label: "Año 2 - Mes 7", value: "2-7" },
        { label: "Año 2 - Mes 8", value: "2-8" },
        { label: "Año 2 - Mes 9", value: "2-9" },
        { label: "Año 2 - Mes 10", value: "2-10" },
        { label: "Año 2 - Mes 11", value: "2-11" },
        { label: "Año 2 - Mes 12", value: "2-12" },
      ],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [
        { label: "Año 3 - Mes 1", value: "3-1" },
        { label: "Año 3 - Mes 2", value: "3-2" },
        { label: "Año 3 - Mes 3", value: "3-3" },
        { label: "Año 3 - Mes 4", value: "3-4" },
        { label: "Año 3 - Mes 5", value: "3-5" },
        { label: "Año 3 - Mes 6", value: "3-6" },
        { label: "Año 3 - Mes 7", value: "3-7" },
        { label: "Año 3 - Mes 8", value: "3-8" },
        { label: "Año 3 - Mes 9", value: "3-9" },
        { label: "Año 3 - Mes 10", value: "3-10" },
        { label: "Año 3 - Mes 11", value: "3-11" },
        { label: "Año 3 - Mes 12", value: "3-12" },
      ],
    },
  ];

  listaAnios: any[] = [];

  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_9;

  codigoAcordeon: string = CodigosPMFI.TAB_9;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private messageService: MessageService,
    private impactoAmbientalPmfiService: ImpactoAmbientalPmfiService,
    private dialog: MatDialog,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService,
    private router: Router,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.listarInfGeneral();
    this.listarCronogramaActividad();
    this.lstOtrosAnos.length = 4;

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarCronogramaActividad() {
    this.lstOtrosAnos = [];
    this.lstOtrosAnos.length = 4;
    this.lstDetalle = [];
    this.listaAnios = [];
    this.verAnio4 = false;
    this.verAnio5 = false;
    this.verAnio6 = false;
    this.verAnio7 = false;
    this.verAnio8 = false;
    this.verAnio9 = false;
    this.verAnio10 = false;
    this.verAnio11 = false;
    this.verAnio12 = false;
    this.verAnio13 = false;
    this.verAnio14 = false;
    this.verAnio15 = false;
    this.verAnio16 = false;
    this.verAnio17 = false;
    this.verAnio18 = false;
    this.verAnio19 = false;
    this.verAnio20 = false;

    var params = {
      codigoProceso: "PMFI",
      idCronogramaActividad: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((x: any) => {
            this.obtenerAnios(x);
            this.lstDetalle.push(x);
          });
          this.marcar();
        } else {
          this.listaDefinida()
            .pipe(
              finalize(() => {
                if (this.lstDetalle.length != 0) {
                  setTimeout(() => {
                    this.listarCronogramaActividad();
                  }, 1000);
                }
                this.dialog.closeAll();
              })
            )
            .subscribe();
        }
      });
  }
  obtenerAnios(lista: any) {
    lista.detalle.forEach((element: any) => {
      this.listaAnios.push(element.anio);
    });
    this.filtrar();
  }

  filtrar() {
    this.cmbAnios = [];
    this.cmbAnios = [...this.cmbAniosDefault];
    var largest = Math.max.apply(Math, this.listaAnios);
    if (largest >= 4) {
      this.vigencia = largest;
      this.lstOtrosAnos.length = largest + 1;

      for (let i = 4; i <= largest; i++) {
        this.params = {
          label: "AÑO " + i,
          value: i.toString(),
          items: [{ label: "Año " + i, value: i + "-0" }],
        };
        this.cmbAnios.push(this.params);
      }
    }
  }

  listaDefinida() {
    this.lstDetalle = [];
    let body = {
      idPlanManejo: 0,
      codigoImpactoAmbiental: "PMFIIA",
      codigoImpactoAmbientalDet: "PMFIIAMIT",
    };

    return this.impactoAmbientalPmfiService
      .listarPmfiImpactoAmbiental(body)
      .pipe(
        tap((response: any) => {
          if (response.data) {
            response.data.detalle.forEach((element: any) => {
              this.lstDetalle.push(
                new Modelo({
                  actividad: element.actividad,
                })
              );
              setTimeout(() => {
                this.guardarTipoActividadPrincipal(element.actividad);
              }, 1000);
            });
          }
        })
      );
  }

  listarInfGeneral() {
    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: "PMFI",
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.vigencia = element.vigencia.toString();
        });
        if (this.vigencia != null || this.vigencia > 3) {
          for (let i = 4; i <= this.vigencia; i++) {
            this.params = {
              label: "AÑO " + i,
              value: i.toString(),
              items: [{ label: "Año " + i, value: i + "-0" }],
            };
            this.cmbAnios.push(this.params);
          }
        }
      });
    //   this.listarCronogramaActividad();
  }

  agregarColumna() {
    this.lstOtrosAnos.push(this.lstOtrosAnos.length);
    this.lstOtrosAnos.forEach((x: any, i: number) => {
      if (i <= 20) {
        this.params = {
          label: "AÑO " + i,
          value: i.toString(),
          items: [{ label: "Año " + i, value: i + "-0" }],
        };
      }
    });
    if (this.lstOtrosAnos.length <= 21) {
      this.cmbAnios.push(this.params);
      this.vigencia = this.params.value;
      this.marcarAgregado();
    }
  }

  marcarAgregado() {
    this.lstDetalle.forEach((x: any, index) => {
      if (this.vigencia == 4) {
        this.verAnio4 = true;
      } else if (this.vigencia == 5) {
        this.verAnio4 = true;
        this.verAnio5 = true;
      } else if (this.vigencia == 6) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
      } else if (this.vigencia == 7) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
      } else if (this.vigencia == 8) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
      } else if (this.vigencia == 9) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
      } else if (this.vigencia == 10) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
      } else if (this.vigencia == 11) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
      } else if (this.vigencia == 12) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
      } else if (this.vigencia == 13) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
      } else if (this.vigencia == 14) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
      } else if (this.vigencia == 15) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
      } else if (this.vigencia == 16) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
      } else if (this.vigencia == 17) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
      } else if (this.vigencia == 18) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
      } else if (this.vigencia == 19) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
      } else if (this.vigencia == 20) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
        this.verAnio20 = true;
      }
    });
  }

  marcar() {
    var aniosMeses: any[] = [];

    this.lstDetalle.forEach((x: any, index) => {
      aniosMeses = [];
      if (this.vigencia == 4) {
        this.verAnio4 = true;
      } else if (this.vigencia == 5) {
        this.verAnio4 = true;
        this.verAnio5 = true;
      } else if (this.vigencia == 6) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
      } else if (this.vigencia == 7) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
      } else if (this.vigencia == 8) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
      } else if (this.vigencia == 9) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
      } else if (this.vigencia == 10) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
      } else if (this.vigencia == 11) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
      } else if (this.vigencia == 12) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
      } else if (this.vigencia == 13) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
      } else if (this.vigencia == 14) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
      } else if (this.vigencia == 15) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
      } else if (this.vigencia == 16) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
      } else if (this.vigencia == 17) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
      } else if (this.vigencia == 18) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
      } else if (this.vigencia == 19) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
      } else if (this.vigencia == 20) {
        this.verAnio4 = true;
        this.verAnio5 = true;
        this.verAnio6 = true;
        this.verAnio7 = true;
        this.verAnio8 = true;
        this.verAnio9 = true;
        this.verAnio10 = true;
        this.verAnio11 = true;
        this.verAnio12 = true;
        this.verAnio13 = true;
        this.verAnio14 = true;
        this.verAnio15 = true;
        this.verAnio16 = true;
        this.verAnio17 = true;
        this.verAnio18 = true;
        this.verAnio19 = true;
        this.verAnio20 = true;
      }

      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });

      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstDetalle[index] = this.context;
    });
  }

  guardarTipoActividadPrincipal(resp: any) {
    let params = {
      actividad: resp,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "PMFI",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        console.log(response);
      });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "PMFI",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó el cronograma de actividades correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó la actividad del cronograma correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    this.ref = this.dialogService.open(PmfiModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      this.lstDetalle.push(this.context);
    }
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail:
                    "Se eliminó la actividad del cronograma correctamente.",
                });
                this.listarCronogramaActividad();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  detail: "Ocurrió un problema, intente nuevamente.",
                });
              }
            });
        } else {
          this.lstDetalle.splice(
            this.lstDetalle.findIndex((x) => x.id == data.id),
            1
          );
        }
      },
      reject: () => {},
    });
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(
                this.detEvaluacion,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon
                )
              );
            } else {
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.detEvaluacion])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion() {
    localStorage.setItem(
      "EvalResuDet",
      JSON.stringify({
        tab: "RFNM",
        acordeon: "LINEADP6211",
      })
    );

    this.router.navigateByUrl(
      "/planificacion/evaluacion/requisitos-previos/" +
        this.idPlanManejo +
        "/" +
        this.codigoProceso
    );
  }
}
export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.tipoActividad = data.tipoActividad);
      return;
    }
  }

  showRemove: boolean = true;
  tipoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m1: boolean = false;
  a1m2: boolean = false;
  a1m3: boolean = false;
  a1m4: boolean = false;
  a1m5: boolean = false;
  a1m6: boolean = false;
  a1m7: boolean = false;
  a1m8: boolean = false;
  a1m9: boolean = false;
  a1m10: boolean = false;
  a1m11: boolean = false;
  a1m12: boolean = false;

  a2m1: boolean = false;
  a2m2: boolean = false;
  a2m3: boolean = false;
  a2m4: boolean = false;
  a2m5: boolean = false;
  a2m6: boolean = false;
  a2m7: boolean = false;
  a2m8: boolean = false;
  a2m9: boolean = false;
  a2m10: boolean = false;
  a2m11: boolean = false;
  a2m12: boolean = false;

  a3m1: boolean = false;
  a3m2: boolean = false;
  a3m3: boolean = false;
  a3m4: boolean = false;
  a3m5: boolean = false;
  a3m6: boolean = false;
  a3m7: boolean = false;
  a3m8: boolean = false;
  a3m9: boolean = false;
  a3m10: boolean = false;
  a3m11: boolean = false;
  a3m12: boolean = false;

  a4m0: boolean = false;
  a5m0: boolean = false;
  a6m0: boolean = false;
  a7m0: boolean = false;
  a8m0: boolean = false;
  a9m0: boolean = false;
  a10m0: boolean = false;
  a11m0: boolean = false;
  a12m0: boolean = false;
  a13m0: boolean = false;
  a14m0: boolean = false;
  a15m0: boolean = false;
  a16m0: boolean = false;
  a17m0: boolean = false;
  a18m0: boolean = false;
  a19m0: boolean = false;
  a20m0: boolean = false;

  otros: any[] = [];

  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-1") {
        this.a1m1 = x.aniosMeses.indexOf("1-1") > -1;
      } else if (x.aniosMeses == "1-2") {
        this.a1m2 = x.aniosMeses.indexOf("1-2") > -1;
      } else if (x.aniosMeses == "1-3") {
        this.a1m3 = x.aniosMeses.indexOf("1-3") > -1;
      } else if (x.aniosMeses == "1-4") {
        this.a1m4 = x.aniosMeses.indexOf("1-4") > -1;
      } else if (x.aniosMeses == "1-5") {
        this.a1m5 = x.aniosMeses.indexOf("1-5") > -1;
      } else if (x.aniosMeses == "1-6") {
        this.a1m6 = x.aniosMeses.indexOf("1-6") > -1;
      } else if (x.aniosMeses == "1-7") {
        this.a1m7 = x.aniosMeses.indexOf("1-7") > -1;
      } else if (x.aniosMeses == "1-8") {
        this.a1m8 = x.aniosMeses.indexOf("1-8") > -1;
      } else if (x.aniosMeses == "1-9") {
        this.a1m9 = x.aniosMeses.indexOf("1-9") > -1;
      } else if (x.aniosMeses == "1-10") {
        this.a1m10 = x.aniosMeses.indexOf("1-10") > -1;
      } else if (x.aniosMeses == "1-11") {
        this.a1m11 = x.aniosMeses.indexOf("1-11") > -1;
      } else if (x.aniosMeses == "1-12") {
        this.a1m12 = x.aniosMeses.indexOf("1-12") > -1;
      } else if (x.aniosMeses == "2-1") {
        this.a2m1 = x.aniosMeses.indexOf("2-1") > -1;
      } else if (x.aniosMeses == "2-2") {
        this.a2m2 = x.aniosMeses.indexOf("2-2") > -1;
      } else if (x.aniosMeses == "2-3") {
        this.a2m3 = x.aniosMeses.indexOf("2-3") > -1;
      } else if (x.aniosMeses == "2-4") {
        this.a2m4 = x.aniosMeses.indexOf("2-4") > -1;
      } else if (x.aniosMeses == "2-5") {
        this.a2m5 = x.aniosMeses.indexOf("2-5") > -1;
      } else if (x.aniosMeses == "2-6") {
        this.a2m6 = x.aniosMeses.indexOf("2-6") > -1;
      } else if (x.aniosMeses == "2-7") {
        this.a2m7 = x.aniosMeses.indexOf("2-7") > -1;
      } else if (x.aniosMeses == "2-8") {
        this.a2m8 = x.aniosMeses.indexOf("2-8") > -1;
      } else if (x.aniosMeses == "2-9") {
        this.a2m9 = x.aniosMeses.indexOf("2-9") > -1;
      } else if (x.aniosMeses == "2-10") {
        this.a2m10 = x.aniosMeses.indexOf("2-10") > -1;
      } else if (x.aniosMeses == "2-11") {
        this.a2m11 = x.aniosMeses.indexOf("2-11") > -1;
      } else if (x.aniosMeses == "2-12") {
        this.a2m12 = x.aniosMeses.indexOf("2-12") > -1;
      } else if (x.aniosMeses == "3-1") {
        this.a3m1 = x.aniosMeses.indexOf("3-1") > -1;
      } else if (x.aniosMeses == "3-2") {
        this.a3m2 = x.aniosMeses.indexOf("3-2") > -1;
      } else if (x.aniosMeses == "3-3") {
        this.a3m3 = x.aniosMeses.indexOf("3-3") > -1;
      } else if (x.aniosMeses == "3-4") {
        this.a3m4 = x.aniosMeses.indexOf("3-4") > -1;
      } else if (x.aniosMeses == "3-5") {
        this.a3m5 = x.aniosMeses.indexOf("3-5") > -1;
      } else if (x.aniosMeses == "3-6") {
        this.a3m6 = x.aniosMeses.indexOf("3-6") > -1;
      } else if (x.aniosMeses == "3-7") {
        this.a3m7 = x.aniosMeses.indexOf("3-7") > -1;
      } else if (x.aniosMeses == "3-8") {
        this.a3m8 = x.aniosMeses.indexOf("3-8") > -1;
      } else if (x.aniosMeses == "3-9") {
        this.a3m9 = x.aniosMeses.indexOf("3-9") > -1;
      } else if (x.aniosMeses == "3-10") {
        this.a3m10 = x.aniosMeses.indexOf("3-10") > -1;
      } else if (x.aniosMeses == "3-11") {
        this.a3m11 = x.aniosMeses.indexOf("3-11") > -1;
      } else if (x.aniosMeses == "3-12") {
        this.a3m12 = x.aniosMeses.indexOf("3-12") > -1;
      } else if (x.aniosMeses == "4-0") {
        this.a4m0 = x.aniosMeses.indexOf("4-0") > -1;
      } else if (x.aniosMeses == "5-0") {
        this.a5m0 = x.aniosMeses.indexOf("5-0") > -1;
      } else if (x.aniosMeses == "6-0") {
        this.a6m0 = x.aniosMeses.indexOf("6-0") > -1;
      } else if (x.aniosMeses == "7-0") {
        this.a7m0 = x.aniosMeses.indexOf("7-0") > -1;
      } else if (x.aniosMeses == "8-0") {
        this.a8m0 = x.aniosMeses.indexOf("8-0") > -1;
      } else if (x.aniosMeses == "9-0") {
        this.a9m0 = x.aniosMeses.indexOf("9-0") > -1;
      } else if (x.aniosMeses == "10-0") {
        this.a10m0 = x.aniosMeses.indexOf("10-0") > -1;
      } else if (x.aniosMeses == "11-0") {
        this.a11m0 = x.aniosMeses.indexOf("11-0") > -1;
      } else if (x.aniosMeses == "12-0") {
        this.a12m0 = x.aniosMeses.indexOf("12-0") > -1;
      } else if (x.aniosMeses == "13-0") {
        this.a13m0 = x.aniosMeses.indexOf("13-0") > -1;
      } else if (x.aniosMeses == "14-0") {
        this.a14m0 = x.aniosMeses.indexOf("14-0") > -1;
      } else if (x.aniosMeses == "15-0") {
        this.a15m0 = x.aniosMeses.indexOf("15-0") > -1;
      } else if (x.aniosMeses == "16-0") {
        this.a16m0 = x.aniosMeses.indexOf("16-0") > -1;
      } else if (x.aniosMeses == "17-0") {
        this.a17m0 = x.aniosMeses.indexOf("17-0") > -1;
      } else if (x.aniosMeses == "18-0") {
        this.a18m0 = x.aniosMeses.indexOf("18-0") > -1;
      } else if (x.aniosMeses == "19-0") {
        this.a19m0 = x.aniosMeses.indexOf("19-0") > -1;
      } else if (x.aniosMeses == "20-0") {
        this.a20m0 = x.aniosMeses.indexOf("20-0") > -1;
      }
    });
  }
}
