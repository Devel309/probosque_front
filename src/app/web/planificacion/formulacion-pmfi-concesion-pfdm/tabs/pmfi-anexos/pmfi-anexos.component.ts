import { Component, Input, OnInit, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { ConfirmationService, MessageService } from "primeng/api";
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MapApi } from "src/app/shared/mapApi";
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import {DownloadFile, ToastService} from '@shared';
import { FileModel } from "src/app/model/util/File";
import { PGMFArchivoTipo } from "@shared";
import { jsPDF } from "jspdf";
import autoTable from 'jspdf-autotable'
import {CodigosPMFI} from '../../../../../model/util/PMFI/PMFI';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {finalize} from 'rxjs/operators';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';

@Component({
  selector: 'app-pmfi-anexos',
  templateUrl: './pmfi-anexos.component.html',
  styleUrls: ['./pmfi-anexos.component.scss']
})
export class PmfiAnexosComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild("ulResult", { static: true }) private ulResult!: ElementRef;


  @Input() disabled!: boolean;
  view: any = null;
  file: FileModel = {} as FileModel;
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
 /*  _filesPDF: FileModel[] = []; */
  _lisDataCenso: any = [];

  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_10;

  codigoAcordeon: string = CodigosPMFI.TAB_10;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;
  listDocuments = [
    { descripcion: 'Mapa Base', id: 1,  codigo:'PMFICEDMAPAB' },
    { descripcion: 'Mapa de unidad de mapeo', id: 2, codigo:'PMFICEDMAPAUM' },
    { descripcion: 'Mapa de dispersión maderable', id: 3, codigo:'PMFICEDMAPDM' },
    { descripcion: 'Mapa de dispersión no maderable', id: 4, codigo:'PMFICEDMAPDNM' },
    { descripcion: 'Datos de los resultados de inventario', id: 5, codigo:'PMFICEDDATOSRI'},
    { descripcion: 'Lista de beneficiarios', id: 6, codigo:'PMFICEDLISTAB' },
    { descripcion: 'Otros', id: 7, codigo:'PMFICEDOTROS' },
  ];

  constructor(private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private messageService: MessageService, private dialog: MatDialog,
    private servicePlanificacion: PlanificacionService, private evaluacionService: EvaluacionService, private router: Router,private toast: ToastService,
) { }

  ngOnInit(): void {
    this.initializeMap();
    
    
    /* thii.listDocuments.forEach((t: any) => {
      this.file = {} as FileModel;
      this.file.id = t.id;
      this.file.descripcion = t.descripcion;
      this.file.file = null;
      this.file.inServer = false;
      this.file.annex = false;
      this.file.nombreFile = '';
      this._filesPDF.push(this.file);
    }); */

    this.obtenerArchivoCapas();

    if (this.isPerfilArffs) this.obtenerEvaluacion();

  }
  static get EXTENSIONSAUTHORIZATION() {
    return [".pdf", "application/pdf"];
  }
  static get EXTENSIONSAUTHORIZATIONSHP() {
    return ["zip", "application/x-zip-compressed"];
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '490px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    let btnView = document.createElement("button");
    btnView.classList.add("btn-primary2");
    btnView.setAttribute("title", "Mostrar/Ocultar Exportar mapa");
    let i = document.createElement("span");
    i.classList.add("esri-icon-documentation");
    btnView.appendChild(i);
    view.ui.add(btnView, "bottom-right");
    btnView.addEventListener("click", (e) => {
      let print = container.querySelector(".esri-widget--panel");
      if (print.style.display === "none") {
        print.style.display = "block";
      } else {
        print.style.display = "none";
      }
    });

    view.when(() => {
      let print = this.mapApi.print(view);
      view.ui.add(print, "top-right");
    });
    this.view = view;
  }
  onChangeFilePDF(e: any, file: FileModel) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let archivo = e.target.files[0];
        let include = null;
        include = PmfiAnexosComponent.EXTENSIONSAUTHORIZATION.includes(
          archivo.type
        );

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "el tipo de documento no es válido",
          });
        } else if (archivo.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB",
          });
        } else {
         /*  for (let item of this._filesPDF) {
            if (item.id === file.id) {
              item.file = archivo;
              item.nombreFile = archivo.name;
            }
          } */
        }
      }
    }
  }
  onChangeFileSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      inServer: false,
      service: false,
      annex: false,
      codigo: e.target.id
    }
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFileSHP(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }

  }
  processFileSHP(file: any, config: any) {
    config.file = file;
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.createLayers(data, config);
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.file = config.file;
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.codZona = config.codZona;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this.file = {} as FileModel;
      this.file.codigo = config.idArchivo;
      this.file.file = config.file;
      this.file.idLayer = t.idLayer;
      this.file.inServer = config.inServer;
      this.file.nombreFile = t.title || config.fileName;
      this.file.groupId = t.groupId;
      this.file.color = t.color;
      this.file.annex = config.annex;
      this._filesSHP.push(this.file);
      this.createLayer(t);
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = item.groupId;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(geoJsonLayer.id, geoJsonLayer.color, this.view);
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {
        console.log(error);
      });
    this.view.map.add(geoJsonLayer);
  }
  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: []
    }
    if (item.isCenso === true) {
      popupTemplate.content = [
        {
          type: 'fields',
          fieldInfos: [
            {
              fieldName: 'nombreComun',
              label: 'Nombre Comun',
            },
            {
              fieldName: 'nombreCientifico',
              label: 'Nombre Cientifico',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'productoTipo',
              label: 'Tipo Producto',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'descripcionOtros',
              label: 'Descripcion',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
          ],
        },
      ];
    }


    return popupTemplate;
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    let ulResult = this.ulResult.nativeElement;
    ulResult.empty();
  }
  onRemoveFileSHP(file: FileModel) {
    if (file.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: "deleteFileSHP",
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          let index = this._filesSHP.findIndex((t: any) => t.idLayer === file.idLayer);
          this._filesSHP.splice(index, 1);
          this.mapApi.removeLayer(file.idLayer, this.view);
          this.eliminarArchivoDetalle(file.codigo);
        }
      });
    } else {
      let index = this._filesSHP.findIndex((t: any) => t.idLayer === file.idLayer);
      this._filesSHP.splice(index, 1);
      this.mapApi.removeLayer(file.idLayer, this.view);
    }
  }
  onDownloadFileSHP(file: FileModel) {
    if (file.file !== null) {
      let url = window.URL.createObjectURL(file.file);
      let name = file.nombreFile;
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.href = url;
      a.download = `${name}.zip`;
      a.click();
    } else if (file.geoJson !== null) {
      let nombreFile = file.nombreFile.replace(/\s+/g, "");
      let options = {
        folder: nombreFile,
        types: {
          point: nombreFile,
          polygon: nombreFile,
          line: nombreFile
        }
      }
      this.mapApi.exportToSHPFile(file.geoJson, options);
    }
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    this.servicePostulacionPFDM.eliminarArchivoDetalle(idArchivo, this.user.idUsuario).subscribe((response: any) => {
      if (response.success) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          detail: 'Se eliminó correctamente',
        });
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  }
  serviceCargarArchivo(item: any) {
    this.serviceArchivo.cargar(item.idUsuario, item.codigo, item.file).subscribe((result: any) => {
      let item2 = {
        codigoTipoPGMF: item.codigoTipoPGMF,
        codigoSubTipoPGMF: item.subCodigoTipoPGMF,
        idArchivo: result.data,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: item.idUsuario
      }
      if (result.success === true) {
        this.servicePostulacionPFDM.registrarArchivoDetalle(item2).subscribe();
      }
    });
  }
  guardarArchivosCapas() {
    let count = 0;
    for (let item of this._filesSHP) {
      if (item.inServer !== true) {
        count++;
      }
    }
    let add = 0;
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: PGMFArchivoTipo.PMFI,
          subCodigoTipoPGMF: 'ANEXOSHP',
          file: t.file
        }
        this.serviceCargarArchivo(item);
        add++;
      }
    });
    if (count === add) {
     // this.guardarArchivosPDF();
    }
  }
/*   guardarArchivosPDF() {
    let count = 0;
    for (let item of this._filesPDF) {
      if (item.inServer !== true && item.file !== null) {
        count++;
      }
    }
    let add = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this._filesPDF.forEach((t: any) => {
      if (t.inServer !== true && t.file !== null) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: t.id,
          codigoTipoPGMF: PGMFArchivoTipo.PMFI,
          subCodigoTipoPGMF: 'ANEXOPDF',
          file: t.file
        }
        this.serviceCargarArchivo(item);
        add++;
      }
    });
    if (count === add) {
      this.messageService.add({
        key: 'tl',
        severity: 'success',
        detail: 'Se registró los anexos correctamente.',
      });
      setTimeout(() => {
        this.dialog.closeAll();
      }, 8000);
    }
  } */
  guardar() {
    this.guardarArchivosCapas();
  }
  obtenerArchivoCapas() {
    this.cleanLayers();
    this._filesSHP = [];
    let item: any = {
      idPlanManejo: this.idPlanManejo,
      codigoTipoPGMF: PGMFArchivoTipo.PMFI
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servicePostulacionPFDM.obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF).subscribe((result: any) => {
      if (result.isSuccess) {
        this.dialog.closeAll();
        let count = 0
        for (let item of result.data) {
          if (item.documento != null) {
            count++;
          }
        }
        let add = 0;
        result.data.forEach((t: any) => {
          if (t.documento != null) {
            add++;
            if (t.codigoTipoPGMF === PGMFArchivoTipo.PMFI && t.codigoSubTipoPGMF === "ANEXOPDF") {
              /* for (let item of this._filesPDF) {
                if (item.id === parseInt(t.tipoDocumento)) {
                  item.file = t.documento;
                  item.nombreFile = t.nombre;
                  item.codigo = t.idArchivo;
                  item.inServer = true;
                }
              } */
            } else {
              let annex: Boolean = false;
              if (t.codigoTipoPGMF === PGMFArchivoTipo.PMFI && t.codigoSubTipoPGMF !== "ANEXOSHP") {
                annex = true;
              }
              if (t.codigoTipoPGMF === PGMFArchivoTipo.PMFI && t.tipoDocumento !== "35") {
                let blob = this.mapApi.readFileByte(t.documento);
                let config = {
                  inServer: true,
                  service: false,
                  fileName: t.nombre,
                  idArchivo: t.idArchivo,
                  annex: annex
                }
                this.processFileSHP(blob, config);
              }
            }
          }
        });
        if (add == count) {
          this.obtenerCensoMaderable();
          this.obtenerCensoNoMaderable();
        }
      } else {
        this.dialog.closeAll();
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema',
        });
      }
    });
  }
  obtenerCensoMaderable() {
    let item = {
      tipoRecurso: '1',
      title: 'Maderable',
      color: '#783f04'
    }
    this.obtenerDatosDispersion(item);
  }
  obtenerCensoNoMaderable() {
    let item = {
      tipoRecurso: '2',
      title: 'No Maderable',
      color: '#0df700'
    }
    this.obtenerDatosDispersion(item);
  }
  obtenerDatosDispersion(item: any) {
    let params = {
      idPlanManejo: this.idPlanManejo,
      tipoCenso: ' ',
      tipoRecurso: item.tipoRecurso
    }
    let geoJson: any = {
      type: "FeatureCollection",
      features: [],
      opacity: 1,
      color: item.color,
      title: item.title,
      service: true,
      inServer: true,
      idLayer: this.mapApi.Guid2.newGuid,
      isCenso: true,
      groupId: this._id,
      crs: {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      }
    }
    this.servicePlanificacion.listarCensoForestalDetalle2(params).subscribe((result: any) => {
      if (result.data.length) {
        result.data.forEach((t: any) => {
          this._lisDataCenso.push(t);
          if (t.coorEste !== null) {
            let jsonGeometry: any = this.mapApi.wktParse(t.coorEste);
            geoJson.features.push({
              type: "Feature",
              properties: {
                nombreComun: t.nombreComun,
                nombreCientifico: t.nombreCientifico,
                productoTipo: t.productoTipo,
                alturaTotal: t.alturaTotal,
                alturaComercial: t.alturaComercial,
                descripcionOtros: t.descripcionOtros
              },
              geometry: jsonGeometry
            })
          }
        });
        this.file = {} as FileModel;
        this.file.codigo = 0;
        this.file.file = null;
        this.file.idLayer = geoJson.idLayer;
        this.file.inServer = geoJson.inServer;
        this.file.nombreFile = geoJson.title
        this.file.groupId = geoJson.groupId;
        this.file.color = geoJson.color;
        this.file.geoJson = geoJson;
        this._filesSHP.push(this.file);
        this.createLayer(geoJson);
      }
    });
  }
  onDownloadFilePDF(data: FileModel) {
    let file: any = data.file;
    DownloadFile(file, data.nombreFile, "");
  }
/*   onDeleteFilePDF(file: FileModel) {
    this.confirmationService.confirm({
      message: '¿Está seguro de eliminar este archivo?',
      icon: 'pi pi-exclamation-triangle',
      key: "deleteFilePDF",
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        for (let item of this._filesPDF) {
          if (item.id === file.id) {
            this.eliminarArchivoDetalle(file.codigo);
            item.file = null;
            item.inServer = false;
            item.nombreFile = '';
            item.codigo = 0;
          }
        }
      }
    });
  } */
  onGenerateInventory() {
    if (!this._lisDataCenso.length) {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'Mensaje',
        detail: 'No se encontró ningún registro',
      });
    } else {
      const doc = new jsPDF({
        // orientation: 'portrait'
        orientation: 'landscape'
      });
      // const head = [['N°', 'Nombre Común', 'Nombre Nativo', 'Nombre Cientifico', 'Estrada', 'Parcela', 'Codigo', 'Coordenadas', 'DAP', 'Altura', 'Condicion', 'Observaciones']];
      const head = [['N°', 'Nombre Común', 'Nombre Nativo', 'Nombre Cientifico', 'Estrada', 'Parcela', 'Codigo', 'Coordenada Este', 'Coordenada Norte', 'DAP', 'Altura', 'Condicion', 'Observaciones']];
      let data: any = [];
      this.dialog.open(LoadingComponent, { disableClose: true });
      this._lisDataCenso.forEach((t: any, i: any) => {
        // let geoJson: any = this.mapApi.wktParse(t.coorEste);
        // let coordinates = `X: ${geoJson.coordinates[0]} Y: ${geoJson.coordinates[1]}`;
        let coordinates = 'X: ' + t.coorEste + ' Y: ' + t.coorNorte;
        // data.push([i + 1, t.nombreComun, t.nombreNativo, t.nombreCientifico, t.faja, t.parcelaCorte, t.codigoArbolCalidad, coordinates, t.dap, t.alturaTotal, t.idTipoArbol, t.descripcionOtros])
        data.push([i + 1, t.nombreComun, t.nombreNativo, t.nombreCientifico, t.faja, t.parcelaCorte, t.codigoArbolCalidad, t.coorEste, t.coorNorte, t.dap, t.alturaTotal, t.idTipoArbol, t.descripcionOtros])

      });
      // console.log('adta',data);
      
      doc.text(`Resultado de Inventario del Plan de Manejo N° ${this.idPlanManejo} `, 10, 10);
      autoTable(doc, {
        head: head,
        margin: { top: 20 },
        body: data,
        didDrawCell: (data: any) => {
        },
        styles: { fontSize: 8, },
      });
      this.dialog.closeAll();
      doc.save(`Resultado de inventario Plan N° ${this.idPlanManejo}.pdf`);
    };
  }
  siguienteTab() {
    this.siguiente.emit();
  }
  regresarTab() {
    this.regresar.emit();
  }


  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso
    };
    this.evaluacionService
        .obtenerEvaluacion(params)
        .subscribe((result: any) => {
          if (result.data) {
            if (result.data.length > 0) {
              this.evaluacion = result.data[0];
              if (this.evaluacion) {
                this.detEvaluacion = Object.assign(this.detEvaluacion,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon));
              }else{
                this.evaluacion = {
                  idPlanManejo: this.idPlanManejo,
                  codigoEvaluacion: this.codigoProceso,
                  codigoEvaluacionDet: this.codigoProceso,
                  codigoEvaluacionDetSub: this.codigoTab,
                  listarEvaluacionDetalle: [],
                  idUsuarioRegistro: this.user.idUsuario,
                };
              }
            }
          }
        });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
            .registrarEvaluacionPlanManejo(this.evaluacion)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((res: any) => {
              this.toast.ok(res.message);
              this.obtenerEvaluacion();
            });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:""
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
