import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { CargaEnvioDocumentacionService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/carga-envio-documentacion.service';
import { ArchivoService, PlanManejoService, UsuarioService } from '@services';
import { FileModel } from 'src/app/model/util/File';
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { CargaArchivosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/caragaArchivos.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { finalize } from 'rxjs/operators';
import { descargarArchivo, isNullOrEmpty, ToastService } from '@shared';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { ArchivoDema } from 'src/app/model/anexoDema';
import { DownloadFile } from 'src/app/shared/util';
import { ResumenService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { ButtonsCreateStore } from 'src/app/features/state/buttons-create.store';
import { ButtonsCreateQuery } from 'src/app/features/state/buttons-create.query';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { NotificacionService } from 'src/app/service/notificacion';

@Component({
  selector: 'app-pmfi-resumen',
  templateUrl: './pmfi-resumen.component.html',
  styleUrls: ['./pmfi-resumen.component.scss']
})
export class PmfiResumenComponent implements OnInit {

  @Input() disabled: boolean = false;
  listProcesos = [
    { idPlanManejoEstado: 0, codigo: "CPMINFG", label: "1. Información General", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMZONO", label: "2. Objetivos", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMRFM", label: "3. Información del Área", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMRFN", label: "4. Ordenamiento Interno", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMSMLS", label: "5. Información de Especies, Recursos y Servicios", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMMPU", label: "6. Sistema de Manejo, Aprovechamiento y Labores Silviculturales", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMDAAE", label: "7. Información de las Medidas de Protección de la Unidad de Manejo Forestal", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMDAA2", label: "8. Identificación de Impactos Ambientales Negativos y Establecimiento de Medidas de Prevención y Mitigación", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMCRA", label: "9. Cronograma de Actividades", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMA4", label: "10. Anexos", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
    { idPlanManejoEstado: 0, codigo: "CPMCARDOC", label: "11. Carga Envío de Documentos", codigoEstado: 'CPMPEND', estado: 'Pendiente', marcar: false },
  ]



  static get EXTENSIONSAUTHORIZATION2() {
    return [
      ".pdf",
      "image/png",
      "image/jpeg",
      "image/jpeg",
      "application/pdf"
    ];
  }

  @Input() idPlanManejo!: number;
  @Output() public regresar = new EventEmitter();

  longitudTUPA:number=0;

  files: FileModel[] = [];
  filePMFI: FileModel = {} as FileModel;

  consolidadoPMFI: any = null;

  cargarPMFI: boolean = false;
  eliminarPMFI: boolean = true;
  idArchivoPMFI: number = 0;

  pendientes: any[] = [];
  verPMFI: boolean = false;
  verDescargaPMFI: boolean = true;

  anexo1!: ArchivoDema;
  anexo2!: ArchivoDema;
  anexo3!: ArchivoDema;
  anexo4!: ArchivoDema;

  anexos: ArchivoDema[]=[]

  verAnexo1: boolean = true;
  verAnexo2: boolean = true;
  verAnexo3: boolean = true;
  verAnexo4: boolean = true;
  verEnviar: boolean = false;

  isSubmittingARFFS$ = this.ARFFSQuery.selectSubmitting();

  disabledCodigoEstadoPlanBorrador:boolean=true;
  disabledCodigoEstadoPlanPresentado:boolean=true;

  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private messageService: MessageService,
    private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private cargaArchivosService: CargaArchivosService,
    private dialog: MatDialog,
    private toast: ToastService,
    private anexosService: AnexosService,
    private resumenService: ResumenService,
    private planManejoService: PlanManejoService,
    private evaluacionService: EvaluacionService,
    private ARFFSStore: ButtonsCreateStore,
    private ARFFSQuery: ButtonsCreateQuery,
    private informacionGeneralService: InformacionGeneralService,
    private notificacionService: NotificacionService
  ) { }

  ngOnInit(): void {
    this.filePMFI.inServer = false;
    this.filePMFI.descripcion = "PDF";
    this.listarArchivoDemaFirmado();
    this.listarEstados();
    /* this.listarAnexos1()
    this.listarAnexos2();
    this.listarAnexos3();
    this.listarAnexos4(); */
    this.obtenerEstadoPlan();
  }

  obtenerEstadoPlan(){
    const body={
      idPlanManejo:this.idPlanManejo
  }
    this.informacionGeneralService.listarInfGeneralResumido(body).subscribe((_res:any)=>{

      if(_res.data.length>0){
        const estado= _res.data[0];
        if(estado.codigoEstado == 'EPLMPRES' || estado.codigoEstado == 'EMDOBS' || estado.codigoEstado == 'EMDBOR'){
          this.disabledCodigoEstadoPlanPresentado=false;
          this.disabledCodigoEstadoPlanBorrador= true;
          this.disabled = true;
        }
        if(estado.codigoEstado == 'EPLMBOR'){
          this.disabledCodigoEstadoPlanPresentado=true;
          this.disabledCodigoEstadoPlanBorrador= true;
          this.disabled = false;
        } 
      }
    })
  }


  adjuntoDocumento(id:number){
    if(id>0){
      this.disabledCodigoEstadoPlanBorrador= false
    }
  }

  listarEstados(){
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: 'PMFI',
      idPlanManejoEstado: null
    }

    this.cargaEnvioDocumentacionService.listarEstadosPlanManejo(body).subscribe((response: any) => {
      if(response.data.length == 0){
        this.pendientes.push({pendiente: 0})
      }
      response.data.forEach((element: any) => {
        if (element.codigoEstado == 'CPMPEND') {
          this.pendientes.push(element)
        }
      });

      response.data.forEach((element: any) => {

        let objIndex = this.listProcesos.findIndex(function (e) {
          return e.codigo === element.codigo;
        });

        this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

        if (element.codigoEstado == 'CPMPEND') {
          this.listProcesos[objIndex].estado = "Pendiente";
          this.listProcesos[objIndex].idPlanManejoEstado = element.idPlanManejoEstado;
          this.listProcesos[objIndex].marcar = false;

        } else if (element.codigoEstado == 'CPMTERM') {
          this.listProcesos[objIndex].estado = "Terminado";
          this.listProcesos[objIndex].idPlanManejoEstado = element.idPlanManejoEstado;
          this.listProcesos[objIndex].marcar = true;
        }

      });
      this.pendiente();
    })
  }

  pendiente() {
    if(this.pendientes.length != 0 ){
      this.verPMFI = true;
    } else if(this.pendientes.length == 0 ) {
      this.verPMFI = false;
    }
  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = "CPMTERM";
      this.listProcesos[objIndex].estado = "Terminado";
    } else {
      this.listProcesos[objIndex].codigoEstado = "CPMPEND";
      this.listProcesos[objIndex].estado = "Pendiente";
    }


  };

  guardarEstados() {

    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {

      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: "PMFI",
        descripcion: "",
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
        observacion: ""
      }
      listaEstadosPlanManejo.push(obje);
    });

    this.cargaEnvioDocumentacionService.registrarEstadosPlanManejo(listaEstadosPlanManejo).subscribe((response: any) => {
      if (response.success) {
        response.data.forEach((element: any) => {
          if (element.codigoEstado == 'CPMPEND') {
            this.pendientes.push(element)
          }
        });
        this.listarEstados();

        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: '',
          detail: response.message,
        });
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: response.message,
        });
      }
    });
  }

  regresarTab() {
    this.regresar.emit();
  }


  guardarArchivoPMFIFirmado() {
    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: 36,
        };
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
      }
    });
  }

  registrarArchivo(id: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: "PMFI",
      descripcion: "",
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: "",
    };
    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se cargó el Plan de Manejo Forestal Intermedio - PMFI firmado correctamente.");
        this.listarArchivoDemaFirmado();
        let p : any = {
          idPlanManejo: this.idPlanManejo, codigoEstado:'EPLMPRES', idUsuarioModificacion: this.user.idUsuario
        };
        this.actualizarPlanManejoEstado(p);
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  listarArchivoDemaFirmado() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 36,
      codigoProceso: "PMFI",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if ( result.data && result.data.length != 0) {
        this.cargarPMFI = true;
        this.eliminarPMFI = false;
        result.data.forEach((element: any) => {
          this.filePMFI.nombreFile = element.nombreArchivo;
          this.idArchivoPMFI = element.idArchivo;
        });
      } else {
        this.eliminarPMFI = true;
        this.cargarPMFI = false;
      }
    });
  }

  eliminarArchivoPmfi() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivoPMFI))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el PMFI firmado correctamente");
        this.cargarPMFI = false;
        this.eliminarPMFI = true;
        this.filePMFI.nombreFile = ''
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  generarPMFI() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cargaArchivosService.consolidadoPMFI(this.idPlanManejo)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.consolidadoPMFI = res;
          this.verDescargaPMFI = false;
        } else {
          this.toast.error(res?.message);
        }
      })
  }

  descargarPMFI() {

    if (isNullOrEmpty(this.consolidadoPMFI)) {
      this.toast.warn('Debe generar archivo consolidado');
      return;
    }
    descargarArchivo(this.consolidadoPMFI);
  }

  enviar() {
    this.guardarArchivoPMFIFirmado();
  }

  private actualizarPlanManejoEstado(param:any){

    this.planManejoService
    .actualizarPlanManejoEstado(param)
    .subscribe(
      (result: any) => {
      },
      (error: HttpErrorResponse) => {
      }
    );
  }

  /* Aplica si la descarga se realiza en esta pantalla */

  /* listarAnexos1() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 1,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.verAnexo1 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo1 = element;
        });
      }
    });
  }

  listarAnexos2() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 2,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.verAnexo2 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo2 = element;
        });
      }
    });
  }

  listarAnexos3() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 3,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if ( result.data && result.data.length != 0) {
        this.verAnexo3 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo3 = element;
        });
      }
    });
  }

  listarAnexos4() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 4,
      codigoProceso: "DEMA",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if ( result.data && result.data.length != 0) {
        this.verAnexo4 = false;
        result.data.forEach((element: ArchivoDema) => {
          this.anexo4 = element;
        });
      }
    });
  }
  descargaAnexo1(){
    DownloadFile(
      this.anexo1.documento,
      this.anexo1.nombreArchivo,
      'application/octet-stream'
    );
  }

  descargaAnexo2(){
    DownloadFile(
      this.anexo2.documento,
      this.anexo2.nombreArchivo,
      'application/octet-stream'
    );
  }

  descargaAnexo3(){
    DownloadFile(
      this.anexo3.documento,
      this.anexo3.nombreArchivo,
      'application/octet-stream'
    );
  }
  descargaAnexo4(){
    DownloadFile(
      this.anexo4.documento,
      this.anexo4.nombreArchivo,
      'application/octet-stream'
    );
  } */

  actualizarPlanManejoEstadoE(param: any) {
    this.ARFFSStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok("Esta solicitud ha sido Formulada.")
          // this.toast.ok(response?.message);
          this.verEnviar = true
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }

  guardarPlanFormulado() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstadoE(params);
  }

  enviarArffs() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EMDPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstadoARFFS(params);
  }

  actualizarPlanManejoEstadoARFFS(param: any) {
    this.ARFFSStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok(response?.message);
          this.registrarNotificacion();
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarNotificacion() {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: 'PMFI',
      numDocgestion: this.idPlanManejo,
      mensaje: 'Se informa que se ha registrado el plan '
        + 'PMFI'
        + ' Nro. '
        + this.idPlanManejo
        + ' para evaluación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.user.idUsuario,
      //codigoPerfil: this.user.usuario.sirperfil,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 10,
      url: '/planificacion/evaluacion/bandeja-eval-pmfi-dema',
      idUsuarioRegistro: this.user.idUsuario
    }
    this.notificacionService
      .registrarNotificacion(params)
      .subscribe()
  }
}
