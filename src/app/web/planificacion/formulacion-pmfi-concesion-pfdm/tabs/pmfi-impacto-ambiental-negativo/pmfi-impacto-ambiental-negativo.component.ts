import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ImpactoAmbientalPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/impacto-ambiental-pmfi.service";
import {CodigosPMFI} from '../../../../../model/util/PMFI/PMFI';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {Router} from '@angular/router';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';

@Component({
  selector: "app-pmfi-impacto-ambiental-negativo",
  templateUrl: "./pmfi-impacto-ambiental-negativo.component.html",
  styleUrls: ["./pmfi-impacto-ambiental-negativo.component.scss"],
})
export class PmfiImpactoAmbientalNegativoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @Input() disabled!: boolean;
  lstDetalle2: Model1[] = [];
  tituloModalMantenimiento2: string = "";
  verModalMantenimiento2: boolean = false;
  tipoAccion2: string = "";
  context2: Model1 = new Model1();
  pendiente: boolean = false;
  lstDetalleParams: any[] = [];
  idImpactoAmbiental: number = 0;


  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_8;

  codigoAcordeon: string = CodigosPMFI.TAB_8;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  constructor(
    private confirmationService: ConfirmationService,
    private impactoAmbientalPmfiService: ImpactoAmbientalPmfiService,
    private user: UsuarioService,
    private dialog: MatDialog,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,private router: Router,
  ) {}

  ngOnInit(): void {
    this.listarPMFI();

    if (this.isPerfilArffs) this.obtenerEvaluacion();

  }

  listarPMFI() {
    this.lstDetalle2 = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoImpactoAmbiental: "PMFIIA",
      codigoImpactoAmbientalDet: "PMFIIAMIT",
    };

    this.impactoAmbientalPmfiService
      .listarPmfiImpactoAmbiental(body)
      .subscribe((response: any) => {
        this.idImpactoAmbiental = response.idImpactoAmbiental;
        if (response.data.detalle.length != 0) {
          response.data.detalle.forEach((element: any) => {
            this.lstDetalle2.push(
              new Model1({
                showRemove: false,
                descripcion: element.descripcion,
                medidasProteccion: element.medidasProteccion,
                idImpactoambientaldetalle: element.idImpactoambientaldetalle,
                idImpactoambiental: element.idImpactoambiental,
                medidasMitigacion: element.medidasMitigacion,
                actividad: element.actividad,
              })
            );
          });
        } else {
          this.listaPmfiDetalle();
        }
      });
  }

  listaPmfiDetalle() {
    this.lstDetalle2 = [];
    let body = {
      idPlanManejo: 0,
      codigoImpactoAmbiental: "PMFIIA",
      codigoImpactoAmbientalDet: "PMFIIAMIT",
    };

    this.impactoAmbientalPmfiService
      .listarPmfiImpactoAmbiental(body)
      .subscribe((response: any) => {
        if (response.data) {
          response.data.detalle.forEach((element: any) => {
            this.lstDetalle2.push(
              new Model1({
                showRemove: false,
                descripcion: element.descripcion,
                medidasProteccion: element.medidasProteccion,
                idImpactoambientaldetalle: element.idImpactoambientaldetalle,
                idImpactoambiental: element.idImpactoambiental,
                medidasMitigacion: element.medidasMitigacion,
                actividad: element.actividad,
              })
            );
          });
        }
      });
  }

  abrirModal(tipo: string,rowIndex:number, data?: Model1) {
    this.tipoAccion2 = tipo;
    if (this.tipoAccion2 == "C") {
      this.tituloModalMantenimiento2 = "Registro de Impactos Ambientales Negativos";
      this.context2 = new Model1();
    } else {
      this.tituloModalMantenimiento2 = "Editar Registro de Impactos Ambientales Negativos";
      this.context2 = new Model1(data);
      this.context2.id = rowIndex;
    }
    this.verModalMantenimiento2 = true;
  }

  openEliminar(e: any, data: Model1, rowIndex: number): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => this.eliminar(data, rowIndex),
      reject: () => {},
    });
  }

  eliminar(data: any, rowIndex: number) {
    if (data.idImpactoambientaldetalle != 0) {
      let params = {
        idImpactoambientaldetalle: data.idImpactoambientaldetalle,
        idUsuarioElimina: this.user.idUsuario,
      };

      this.impactoAmbientalPmfiService
        .eliminarImpactoAmbientalDetalle(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
          if (res.success == true) {
            this.toast.ok("Se eliminó el Impacto Ambiental Negatico correctamente.");
            this.listarPMFI();
          } else {
            this.toast.error(res?.message);
          }
        });
    } else {
      this.lstDetalle2.splice(rowIndex, 1);
      this.toast.ok("Se eliminó el Impacto Ambiental Negatico correctamente.");
      this.pendiente = true;
    }
  }

  registrar(context?: any): void {

    if(context.actividad == ''){
      this.toast.warn("El campo Actividad que genera impacto es obligatorio.");
      return;
    }

    if (this.tipoAccion2 == "C") {
      this.lstDetalle2.push(this.context2);
      this.pendiente = true;
    }
    if (this.tipoAccion2 == "E") {
      if (this.context2.idImpactoambientaldetalle != 0) {
        this.context2 = new Model1(context);
        let index = this.lstDetalle2.findIndex(
          (x) =>
            x.idImpactoambientaldetalle == context.idImpactoambientaldetalle
        );
        this.lstDetalle2[index] = this.context2;
        this.pendiente = true;
      } else {
        this.context2 = new Model1(context);
        //let index = this.lstDetalle2.findIndex((x) => x.id == context.id);
        this.lstDetalle2[context.id] = context;
        this.pendiente = true;
      }
    }
    this.verModalMantenimiento2 = false;
  }

  guardar() {
    this.lstDetalleParams = [];

    this.lstDetalle2.forEach((x) => {
      this.lstDetalleParams.push({
        idUsuarioRegistro: this.user.idUsuario,
        medidasProteccion: x.medidasProteccion ? x.medidasProteccion : "",
        descripcion: x.descripcion ? x.descripcion : "",
        idImpactoambientaldetalle: x.idImpactoambientaldetalle
          ? x.idImpactoambientaldetalle
          : 0,
        idImpactoambiental: x.idImpactoambiental ? x.idImpactoambiental : 0,
        medidasprevencion: x.medidasprevencion ? x.medidasprevencion : "",
        codigoImpactoAmbientalDet: x.codigoImpactoAmbientalDet
          ? x.codigoImpactoAmbientalDet
          : "",
        medidasMitigacion: x.medidasMitigacion ? x.medidasMitigacion : "",
        actividad: x.actividad ? x.actividad : "",
      });
    });
    var params = {
      idUsuarioRegistro: this.user.idUsuario,
      idImpactoAmbiental: 0,
      codigoImpactoAmbiental: "PMFIIA",
      idPlanManejo: this.idPlanManejo,
      detalle: this.lstDetalleParams,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impactoAmbientalPmfiService
      .registrarImpactoAmbientalPmfi(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        this.pendiente = false;
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.listarPMFI();
        } else {
          this.toast.error(res?.message);
          this.listarPMFI();
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(this.detEvaluacion,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon));
            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6210"
    }));


    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }



}

export class Model1 {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.showRemove = data.showRemove;
      this.descripcion = data.descripcion;
      this.medidasProteccion = data.medidasProteccion;
      this.idImpactoambientaldetalle = data.idImpactoambientaldetalle;
      this.idImpactoambiental = data.idImpactoambiental;
      this.medidasprevencion = data.medidasprevencion;
      this.codigoImpactoAmbientalDet = "PMFIIAMIT";
      this.medidasMitigacion = data.medidasMitigacion;
      this.actividad = data.actividad;
      return;
    } else {
      //this.id = new Date().toISOString();
    }
  }
  id: number = 0;
  showRemove: boolean = true;
  medidasProteccion: string = "";
  descripcion: string = "";
  idImpactoambientaldetalle: number = 0;
  idImpactoambiental: number = 0;
  medidasprevencion: string = "";
  codigoImpactoAmbientalDet: string = "PMFIIAMIT";
  medidasMitigacion: string = "";
  actividad: string = "";
}
