import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewInit,
} from "@angular/core";
import { forkJoin, Observable, of } from "rxjs";
import { concatMap, finalize, map, tap } from "rxjs/operators";

import { MatDialog } from "@angular/material/dialog";
import { ConfirmationService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";

import { PGMFArchivoDto, PlanManejoArchivo } from "@models";
import { ArchivoService, UsuarioService } from "@services";
import {
  ArchivoTipo,
  isNull,
  isNullOrEmpty,
  AppMapaComponent,
  LayerView,
  onlySemicolons,
  PGMFArchivoTipo,
  RespuestaTipo,
  setOneSemicolon,
  ToastService,
} from "@shared";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { OrdenamientoInternoPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/ordenamiento-interno-pmfi.service";
import { ModalFormularioOrdenInternoComponent } from "./modal-formulario-orden-interno/modal-formulario-orden-interno.component";
import { CodigosPMFI } from "../../../../../model/util/PMFI/PMFI";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-pmfi-ordenamiento-interno",
  templateUrl: "./pmfi-ordenamiento-interno.component.html",
  styleUrls: ["./pmfi-ordenamiento-interno.component.scss"],
})
export class PmfiOrdenamientoInternoComponent implements OnInit, AfterViewInit {
  @Input() idPlanManejo!: number;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() isPerfilArffs!: boolean;

  @Input() disabled!: boolean;
  @ViewChild(AppMapaComponent) map!: AppMapaComponent;

  ref!: DynamicDialogRef;
  ordenInterno: any;
  cabecera: any;
  listOrdenInterno: any[] = [];

  RespuestaTipo = RespuestaTipo;

  isEditedMap = false;
  relacionArchivo!: { idArchivo: number; idPlanManejoArchivo: number };

  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_4;

  codigoAcordeon: string = CodigosPMFI.TAB_4;

  detEvaluacion: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon,
  });

  evaluacion: any;

  constructor(
    private apiArchivo: ArchivoService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private confirmationService: ConfirmationService,
    private user: UsuarioService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  get totalArea() {
    return this.listOrdenInterno
      .map((i) => i.areaHA)
      .reduce((sum, x) => sum + x, 0);
  }

  ngAfterViewInit(): void {
    this.getInitData();
  }

  getInitData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      ordenamiento: this.listarOrdenamiento(),
      base64: this.obtenerArchivoMapa(),
      base64_2: this.obtenerArchivoMapaFromArea(),
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => {
        const layers = this.setLayer(this.listOrdenInterno);
        if (!isNullOrEmpty(res.base64)) {
          this.map.addBase64FileWithConfig(res.base64, layers);
        }
        if (!isNullOrEmpty(res.base64_2)) {
          this.map.addBase64FileWithConfig(res.base64_2, layers);
        }
      });
  }

  obtenerArchivoMapa(): Observable<string> {
    const item = {
      codigoProceso: PGMFArchivoTipo.PMFI,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: ArchivoTipo.SHAPEFILE,
    };
    return this.obtenerRelacionArchivo(item)
      .pipe(
        tap((res) => {
          if (!isNull(res)) {
            const { idArchivo, idPlanManejoArchivo } = res;
            this.relacionArchivo = { idArchivo, idPlanManejoArchivo };
          }
        })
      )
      .pipe(map((res) => (res?.documento ? res.documento : "")));
  }

  obtenerArchivoMapaFromArea(): Observable<string> {
    return this.obtenerRelacionArchivoFromArea(
      PGMFArchivoTipo.PMFI,
      this.idPlanManejo,
      ArchivoTipo.SHAPEFILE_03
    )
      .pipe(
        tap((res) => {
          if (!isNull(res)) {
            const { idArchivo, idPlanManejoArchivo } = res;
            this.relacionArchivo = { idArchivo, idPlanManejoArchivo };
          }
        })
      )
      .pipe(map((res) => (res?.documento ? res.documento : "")));
  }

  obtenerRelacionArchivoFromArea(
    codigoProceso: string,
    idPlanManejo: number,
    idTipoDocumento: string
  ) {
    return this.apiOrdenamiento.obtenerRelacionArchivo(
      codigoProceso,
      idPlanManejo,
      idTipoDocumento
    );
  }

  listarOrdenamiento() {
    this.listOrdenInterno = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoOrdenamiento: "PMFI",
    };

    return this.apiOrdenamiento.listarOrdenamiento(params).pipe(
      tap((response: any) => {
        this.cabecera = {
          idOrdenamientoProteccion: !!response.data[0].anexo
            ? response.data[0].idOrdenamientoProteccion
            : 0,
          codTipoOrdenamiento: response.data[0].codTipoOrdenamiento,
          idPlanManejo: response.data[0].idPlanManejo,
        };

        response.data[0].listOrdenamientoProteccionDet.forEach(
          (element: any) => {
            const item = element;
            let detalle = {
              idOrdenamientoProteccionDet: !!response.data[0].anexo
                ? item.idOrdenamientoProteccionDet
                : 0,
              codigoTipoOrdenamientoDet: item.codigoTipoOrdenamientoDet,
              categoria: item.categoria,
              areaHA: item.areaHA,
              observacion: item.observacion, //campo para saber si tiene asociado un shapefile
              descripcion: item.descripcion, //campo para guardar nombres de las capas
              actividad: item.actividad, //campo para guardar layerId(capa mapa)
              actividadesRealizar: item.actividadesRealizar, //campo para guardar groupId (grupo capa mapa)
              observacionDetalle: item.observacionDetalle, //campo para guardar color capa
            };
            this.listOrdenInterno.push(detalle);
          }
        );
      })
    );
  }

  openModal = (mesaje: string, data: any, tipo: any, index?: any) => {
    this.ref = this.dialogService.open(ModalFormularioOrdenInternoComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: { data: data, type: tipo },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          let ordenNueva = {
            idOrdenamientoProteccionDet: 0,
            codigoTipoOrdenamientoDet: "PMFIDET",
            categoria: resp.categoria || "Sin Categoría",
            areaHA: 0.0,
            observacion: null, //tiene shapefile
            descripcion: "", //capas asociadas
            actividad: null, //layerId
            actividadesRealizar: null, // groupId
            observacionDetalle: null, //campo para guardar color capa
          };

          this.listOrdenInterno.push(ordenNueva);
          //this.guardarOrdenInterno(ordenNueva);
        } else if (tipo == "E") {
          this.listOrdenInterno[index].categoria = resp.categoria;
        }
      }
    });
  };

  guardarOrdenamiento() {
    let arrayBody: any = [];
    this.listOrdenInterno.forEach((element: any) => {
      let bodyOrdenInterno = {
        idOrdenamientoProteccion: this.cabecera.idOrdenamientoProteccion,
        codTipoOrdenamiento: "PMFI",
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        idUsuarioModificacion: this.user.idUsuario,
        listOrdenamientoProteccionDet: [
          {
            idOrdenamientoProteccionDet: element.idOrdenamientoProteccionDet,
            codigoTipoOrdenamientoDet: "PMFIDET",
            categoria: element.categoria,
            areaHA: element.areaHA,
            idUsuarioRegistro: this.user.idUsuario,
            idUsuarioModificacion: this.user.idUsuario,
            observacion: element.observacion, //tiene shapefile
            descripcion: element.descripcion, //capas asociadas
            actividad: element.actividad, //layerId
            actividadesRealizar: element.actividadesRealizar, // groupId
            observacionDetalle:
              element.observacionDetalle != "PMFI"
                ? element.observacionDetalle
                : "", //campo para guardar color capa
          },
        ],
      };
      arrayBody.push(bodyOrdenInterno);
    });
    return this.apiOrdenamiento.registrarOrdenamiento(arrayBody).pipe(
      tap((response: any) => {
        if (response.success) {
          this.toast.ok("Se registró el Ordenamiento Interno correctamente.");
          this.listarOrdenamiento().subscribe();
        } else {
          this.toast.error("Ocurrió un problema, intente nuevamente", "ERROR");
        }
      })
    );
  }

  openEliminar(event: Event, data: any, index: any): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idOrdenamientoProteccionDet !== 0) {
          var parametros = {
            idOrdenamientoProtecccion: 0,
            idOrdenamientoProteccionDet: data.idOrdenamientoProteccionDet,
            codigoTipoOrdenamientoDet: "",
            idUsuarioElimina: this.user.idUsuario,
          };
          this.apiOrdenamiento
            .eliminarOrdenamiento(parametros)
            .subscribe((response: any) => {
              if (response.success) {
                this.toast.ok(
                  "Se eliminó el Ordenamiento Interno correctamente."
                );
                // this.listarOrdenamiento().subscribe();
                this.listOrdenInterno.splice(index, 1);
              } else {
                this.toast.error(
                  "Ocurrió un problema, intente nuevamente",
                  "ERROR"
                );
              }
            });
        } else {
          this.listOrdenInterno.splice(index, 1);
          this.toast.ok("Se eliminó el Ordenamiento Interno correctamente.");
        }
        this.removeLayer(data);
      },
    });
  }

  obtenerRelacionArchivo(item: any) {
    return this.apiOrdenamiento.obtenerArchivo(item);
  }

  guardar() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.saveFileFlow()
      .pipe(concatMap(() => this.guardarOrdenamiento()))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }

  saveFileFlow() {
    if (!this.isEditedMap) return of(null);

    const idArchivo = this.relacionArchivo?.idArchivo;
    const idPlanManejoArchivo = this.relacionArchivo?.idPlanManejoArchivo;

    const deleteFile = isNull(this.relacionArchivo)
      ? of(null)
      : this.deleteFile(idArchivo, idPlanManejoArchivo).pipe(
          tap(() => (this.isEditedMap = false))
        );

    if (this.map.isEmpty)
      return deleteFile.pipe(
        tap(() => (this.relacionArchivo = undefined as any))
      );

    return this.saveFile()
      .pipe(
        tap((res) => {
          this.relacionArchivo = {
            idArchivo: res?.data?.idArchivo,
            idPlanManejoArchivo: res?.data?.idPGMFArchivo,
          };
          this.isEditedMap = false;
        })
      )
      .pipe(concatMap(() => deleteFile));
  }

  saveFile() {
    return this.map
      .getZipFile()
      .pipe(tap({ error: (err) => this.toast.warn(err) }))
      .pipe(concatMap((blob) => this.guardarArchivo(blob as Blob)))
      .pipe(concatMap((idArchivo) => this.guardarRelacionArchivo(idArchivo)));
  }

  guardarArchivo(blob: Blob): Observable<number> {
    const file = new File([blob], `PMFI-OI-${this.idPlanManejo}.zip`);
    return this.apiArchivo
      .cargar(this.user.idUsuario, ArchivoTipo.SHAPEFILE, file)
      .pipe(map((res) => res?.data));
  }

  guardarRelacionArchivo(idArchivo: number) {
    const item = new PGMFArchivoDto({
      codigoTipoPGMF: PGMFArchivoTipo.PMFI,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      idArchivo,
    });
    return this.apiOrdenamiento.registrarArchivo(item);
  }

  deleteFile(idArchivo: number, idPlanManejoArchivo: number) {
    return forkJoin([
      this.eliminarArchivo(idArchivo, this.user.idUsuario),
      this.eliminarRelacionArchivo(idPlanManejoArchivo, this.user.idUsuario),
    ]).pipe(map(() => null));
  }

  eliminarRelacionArchivo(
    idPlanManejoArchivo: number,
    idUsuarioElimina: number
  ) {
    const item = new PlanManejoArchivo({
      idPlanManejoArchivo,
      idUsuarioElimina,
    });
    return this.apiOrdenamiento.eliminarArchivo(item);
  }

  eliminarArchivo(idArchivo: number, idUsuarioElimina: number) {
    return this.apiArchivo.eliminarArchivo(idArchivo, idUsuarioElimina);
  }

  //#region MAP FUNCTIONS

  setLayer(list: Ordenamiento[]): LayerView[] {
    let layers: LayerView[] = [];
    for (const item of list) {
      if (!isNullOrEmpty(item.actividad) && !isNullOrEmpty(item.descripcion)) {
        const layersId = item.actividad.split(";");
        const layersName = item.descripcion.split(";");
        for (let index = 0; index < layersId.length; index++) {
          const layerId = layersId[index];
          const title = layersName[index];
          const color = item.observacionDetalle;
          const groupId = item.actividadesRealizar;
          const layer: LayerView = {
            color,
            groupId,
            layerId,
            title,
            area: 0,
            features: [],
          };
          layers.push(layer);
        }
      }
    }
    return layers;
  }

  addLayer(item: Ordenamiento, file: any) {
    const groupId = this.map.genId();
    const color = this.map.genColor();

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.map
      .addLayerFile(file, groupId, color)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((layers) => {
        item.areaHA = layers.map((l) => l.area).reduce((sum, x) => sum + x, 0);
        item.observacion = RespuestaTipo.SI; //tiene shapefile
        item.descripcion = layers
          .map((l) => this.map.joinTitle(l.title))
          .join(";"); //capas asociadas
        item.actividad = layers.map((l) => l.layerId).join(";"); //layerId
        item.actividadesRealizar = groupId; // groupId
        item.observacionDetalle = color; //campo para guardar color capa
        this.isEditedMap = true;
      });
  }

  deleteLayer(l: LayerView) {
    let item = this.listOrdenInterno.find(
      (x) => x.actividadesRealizar == l.groupId
    );

    if (!isNull(item)) {
      item.areaHA = item.areaHA - l.area;
      item.areaHA = item.areaHA > 0 ? item.areaHA : 0;
      let capas = String(item.descripcion).replace(
        this.map.joinTitle(l.title),
        ""
      );
      capas = setOneSemicolon(capas);
      capas = onlySemicolons(capas) ? "" : capas;
      item.descripcion = capas;
      item.observacion = isNullOrEmpty(capas)
        ? RespuestaTipo.NO
        : RespuestaTipo.SI;
      let layerId = String(item.actividad).replace(String(l.layerId), "");
      layerId = setOneSemicolon(layerId);
      layerId = onlySemicolons(layerId) ? "" : layerId;
      item.actividad = layerId;
      item.observacionDetalle = isNullOrEmpty(capas)
        ? ""
        : item.observacionDetalle;
    }
  }

  deleteAllLayers() {
    this.listOrdenInterno.forEach((item) => {
      item.areaHA = 0;
      item.observacion = RespuestaTipo.NO; //tiene shapefile
      item.descripcion = "";
      item.actividad = "";
      item.actividadesRealizar = "";
      item.observacionDetalle = "";
    });
  }

  download(e: Ordenamiento) {
    let nameFile = String(e.categoria)
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "");
    nameFile = nameFile.replace(/[^a-z0-9\s]/gi, "").replace(/[-\s]/g, "_");
    this.map.downloadGroup(e.actividadesRealizar, nameFile);
  }

  removeLayer(item: Ordenamiento) {
    this.map.deleteGroupLayer(item.actividadesRealizar);
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.detEvaluacion = Object.assign(
                this.detEvaluacion,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon
                )
              );
            } else {
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.detEvaluacion])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion() {
    localStorage.setItem(
      "EvalResuDet",
      JSON.stringify({
        tab: "RFNM",
        acordeon: "LINEADP6251",
      })
    );

    this.router.navigateByUrl(
      "/planificacion/evaluacion/requisitos-previos/" +
        this.idPlanManejo +
        "/" +
        this.codigoProceso
    );
  }

  //#endregion
}

interface Ordenamiento {
  categoria: string;
  areaHA: number;
  observacion: string;
  descripcion: string;
  actividad: string;
  actividadesRealizar: string;
  observacionDetalle: string;
}
