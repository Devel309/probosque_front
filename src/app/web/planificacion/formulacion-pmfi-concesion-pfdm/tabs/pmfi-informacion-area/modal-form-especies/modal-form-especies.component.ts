import { Component, OnInit, Input} from "@angular/core";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { FileModel } from "src/app/model/util/File";
import { HttpParams } from '@angular/common/http';
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { MessageService } from "primeng/api";

@Component({
  selector: "app-modal-form-especies",
  templateUrl: "./modal-form-especies.component.html",
  styleUrls: ["./modal-form-especies.component.scss"],
})
export class ModalFormEspeciesComponent implements OnInit {
  @Input() idPlanManejo!: number;

  especiesObj: SolicitudFaunaModel = new SolicitudFaunaModel();

  files: FileModel[] = [];
  filePMFI: FileModel = {} as FileModel;
  cargarPMFI: boolean = false;
  eliminarPMFI: boolean = true;
  idArchivoPMFI: number = 0;
  tieneArchivo: boolean = false;

  static get EXTENSIONSAUTHORIZATION2() {
    return [
      ".pdf",
      "image/png",
      "image/jpeg",
      "image/jpeg",
      "application/pdf"
    ];
  }

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private user: UsuarioService,
    private anexosService: AnexosService,
    private toast: ToastService,
    private messageService: MessageService,

  ) {}

  ngOnInit(): void {
  //  this.listarArchivoDemaFirmado();
    this.filePMFI.inServer = false;
    this.filePMFI.descripcion = "PDF";
    this.especiesObj.adjunto = 'N'
    if (this.config.data.edit) {
      this.especiesObj = new SolicitudFaunaModel(this.config.data.data);
      this.listarArchivoDemaFirmado(this.especiesObj.idArchivo);
    }
  }

  agregar() {
    
        if(this.especiesObj.adjunto == 'S'  ){
          if(this.especiesObj.nombreCientifico == '') {
            this.messageService.add({
              key: 'tr',
              severity: 'warn',
              detail: 'El campo Nombre Científico es obligatorio.',
            });
              return;
          } else {
            this.ref.close(this.especiesObj);
          }
    
        } else if(this.especiesObj.adjunto == 'N'  ) {
          if(this.especiesObj.nombreCientifico == '') {
            this.messageService.add({
              key: 'tr',
              severity: 'warn',
              detail: 'El campo Nombre Científico es obligatorio.',
            });
              return;
          } else {
            this.ref.close(this.especiesObj);
          }
        }

    
  }
  cerrarModal() {
    this.ref.close();
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === "PDF") {
          include = ModalFormEspeciesComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "el tipo de documento no es válido",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB",
          });
        } else {
          if (type === "PDF") {
            this.filePMFI.nombreFile = e.target.files[0].name;
            this.filePMFI.file = e.target.files[0];
            this.filePMFI.descripcion = type;
            this.filePMFI.inServer = false;
            this.especiesObj.adjunto = 'S'
            this.files.push(this.filePMFI);

          }
        }
      }
    }
  }

  guardarArchivoPMFIFirmado() {
    if(this.files.length != 0){
      this.files.forEach((t: any) => {
        if (t.inServer !== true) {
          this.tieneArchivo = true
          let item = {
            id: this.user.idUsuario,
            tipoDocumento: 38,
          };
          this.anexosService
            .cargarAnexos(item.id, item.tipoDocumento, t.file)
            .subscribe((result: any) => {
              this.registrarArchivo(result.data);
              this.especiesObj.idArchivo = result.data;
             
            });
  
        }  
      });

    } else {
      this.agregar();
    }
    
  }

  registrarArchivo(id: number) {

    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: "PMFI",
      descripcion: "",
      idArchivo: id,
      idPlanManejo: this.config.data.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: "",
    };
    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se cargó la evidencia de la especie correctamente.");
        this.listarArchivoDemaFirmado();
        this.agregar()
        
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  listarArchivoDemaFirmado(id?:number) {
    var params = {
      idArchivo: id ? id:  null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 38,
      codigoProceso: "PMFI",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarPMFI = true;
        this.eliminarPMFI = false;
        result.data.forEach((element: any) => {
          this.filePMFI.nombreFile = element.nombreArchivo;
          this.idArchivoPMFI = element.idArchivo;
        });
      } else {
        this.eliminarPMFI = true;
        this.cargarPMFI = false;
      }
    });
  }

  eliminarArchivoPmfi() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivoPMFI))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó la Evidencia correctamente");
        this.cargarPMFI = false;
        this.eliminarPMFI = true;
        this.filePMFI.nombreFile = "";
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }
}
export class SolicitudFaunaModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.idFauna = data.idFauna ? data.idFauna : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.codigoTipo = data.codigoTipo ? data.codigoTipo : "";
      this.tipoFauna = data.tipoFauna ? data.tipoFauna : "";
      this.nombre = data.nombre ? data.nombre : "";
      this.nombreCientifico = data.nombreCientifico
        ? data.nombreCientifico
        : "";
      this.familia = data.familia ? data.familia : "";
      this.estatus = data.estatus ? data.estatus : "V";
      this.estadoSolicitud = data.estadoSolicitud
        ? data.estadoSolicitud
        : "Solicitado";
      this.adjunto = data.adjunto ? data.adjunto : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idArchivo = data.idArchivo ? data.idArchivo :0;
    }
  }
  id: number = 0;
  idFauna: number = 0;
  idPlanManejo: number = 0;
  codigoTipo: string = "";
  tipoFauna: string = "";
  nombre: string = "";
  nombreCientifico: string = "";
  familia: string = "";
  estatus: string = "";
  estadoSolicitud: string = "";
  adjunto: string = "";
  idUsuarioRegistro: number = 0;
  idArchivo: number = 0
}
