import { PMFITipo } from "@shared";

export class InformacionRegistroModel {
  constructor(data?: any) { }
  idInfBasicaDet: number = 0;
  codInfBasicaDet?: String | null;
  puntoVertice?: String | null;
  coordenadaEsteIni?: number | null;
  coordenadaNorteIni?: number | null;
  referencia?: String | null;
  distanciaKm?: String | null;
  tiempo?: String | null;
  medioTransporte?: String | null;
  idUsuarioRegistro: number = 0;
  epoca?: String | null;
  idRios?: number | null;
  nombreRio?: String | null;
  nombreLaguna?: String | null;
  nombreQuebrada?: String | null;
  descripcion?: String | null;
  areaHa: number = 0;
  areaHaPorcentaje: number = 0;
  zonaVida?: String | null;
  idFauna?: number | null;
  idFlora?: number | null;
}

// modificar cuando se tenga el modelo
export class InfoListaModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.referencia = data.referencia;
      this.distanciaKm = data.distanciaKm;
      this.tiempo = data.tiempo;
      this.nombreTrasporte = data.nombreTrasporte;
      this.epoca = data.epoca;
      this.idMedioTransporte = data.idMedioTransporte;
      this.coordenadaEste = data.coordenadaEste;
      this.coordenadaNorte = data.coordenadaNorte;
      this.puntoVertice = data.puntoVertice;
      this.descripcion = data.descripcion;
      this.areaHa = data.areaHa;
      this.areaHaPorcentaje = data.areaHaPorcentaje;
      this.codInfBasicaDet = data.codInfBasicaDet;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.coordenadaEsteIni = data.coordenadaEste;;
      this.coordenadaNorteIni =  data.coordenadaNorte;
    }
  }
  id: number = 0;
  idInfBasica: number = 0;
  codInfBasica?: String | null;
  codNombreInfBasica?: String | null;
  departamento?: String | null;
  provincia?: String | null;
  distrito?: String | null;
  comunidad?: String | null;
  cuenca?: String | null;
  idPlanManejo: number = 0;
  idInfBasicaDet: number = 0;
  codInfBasicaDet?: String | null;
  puntoVertice?: String | null;
  coordenadaEste?: number | null;
  coordenadaNorte?: number | null;
  referencia?: String | null;
  distanciaKm?: String | null;
  tiempo?: String | null;
  medioTransporte?: String | null;
  idUsuarioRegistro: number = 0;
  epoca?: String | null;
  idHidrografia?: number | null;
  codRio?: String | null;
  nombreRio?: String | null;
  nombreLaguna?: String | null;
  nombreQuebrada?: String | null;
  descripcion?: String | null;
  areaHa: number = 0;
  areaHaPorcentaje: number = 0;
  zonaVida?: String | null;
  idFauna?: number | null;
  idFlora?: number | null;
  codFauna?: String | null;
  nombreComunFauna?: String | null;
  nombreComunCientifico?: String | null;
  codFlora?: String | null;
  idMedioTransporte?: number;
  nombreTrasporte?: string;
  coordenadaEsteIni?: string;
  coordenadaNorteIni?: string;
}

export class AccesibilidadVias {
  constructor(data?: any) {
    if (data) {
      this.id = data.id ? data.id : 0;
      this.referencia = data.referencia ? data.referencia : "";
      this.distanciaKm = data.distanciaKm ? data.distanciaKm : 0;
      this.tiempo = data.tiempo ? data.tiempo : "";
      this.medioTransporte = data.medioTransporte ? data.medioTransporte : ""; //
      this.epoca = data.epoca ? data.epoca : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idInfBasicaDet = data.idInfBasicaDet ? data.idInfBasicaDet : 0;

      this.nombreRio = data.nombreRio ? data.nombreRio : "";
      this.nombreQuebrada = data.nombreQuebrada ? data.nombreQuebrada : "";
      this.nombreLaguna = data.nombreLaguna ? data.nombreLaguna : "";
      this.idRios = data.idRios ? data.idRios : 0;

      this.codInfBasicaDet = data.codInfBasicaDet ? data.codInfBasicaDet : "";
      this.puntoVertice = data.puntoVertice ? data.puntoVertice : "";
      this.coordenadaEsteIni = data.coordenadaEsteIni
        ? data.coordenadaEsteIni
        : "";
      this.coordenadaNorteIni = data.coordenadaNorteIni
        ? data.coordenadaNorteIni
        : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.areaHa = data.areaHa ? data.areaHa : "";
      this.areaHaPorcentaje = data.areaHaPorcentaje
        ? data.areaHaPorcentaje
        : "";
      this.zonaVida = data.zonaVida ? data.zonaVida : "";
      this.idFauna = data.idFauna ? data.idFauna : "";
      this.idFlora = data.idFlora ? data.idFlora : "";
      this.nombreComun = data.nombreComun ? data.nombreComun : "";
      this.nombreCientifico = data.nombreCientifico ? data.nombreCientifico : "";
      this.familia = data.familia ? data.familia : "";
    }
  }
  id: number = 0;
  idInfBasicaDet: number = 0;
  codInfBasicaDet: string = "";
  puntoVertice?: string | null;
  coordenadaEsteIni?: string | null;
  coordenadaNorteIni?: string | null;
  referencia: string = "";
  distanciaKm: number = 0;
  tiempo: string = "";
  medioTransporte?: string;
  idUsuarioRegistro: number = 0;
  epoca: string = "";
  idRios?: number;
  descripcion?: string | null;
  areaHa?: string | null;
  areaHaPorcentaje?: string | null;
  zonaVida?: string | null;
  idFauna?: string | null;
  idFlora?: string | null;
  nombreRio?: string | null;
  nombreLaguna?: string | null;
  nombreQuebrada?: string | null;
  nombreComun: string = "";
  nombreCientifico:string = "";
  familia:string = "";
}

// modificar cuando se tenga el modelo
export class CuerposAguaModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.nombreRio = data.nombreRio;
      this.nombreQuebrada = data.nombreQuebrada;
      this.nombreLaguna = data.nombreLaguna;
      this.idHidrografia = data.idHidrografia;
    }
  }
  id: number = 0;
  nombreRio?: string = "";
  nombreQuebrada?: string = "";
  nombreLaguna?: string = "";
  idDetalle?: string = "";
  idHidrografia?: number = 0;
  idUsuarioRegistro?: number = 0;
  codInfBasicaDet?: string = "";
  idInfBasicaDet?: number = 0; // id del detalle
}

// model Zona

export class ZonaModel {
  constructor(data?: any) { }
  zonaDescripcion: string = "";
}

export class AcpectosSocialesModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.organizacionAprovechamientio = data.organizacionAprovechamientio;
      this.noFamilias = data.noFamilias;
      this.especies = data.especies;
      this.tipoProducto = data.tipoProducto;
      this.idInfBasicaDet = data.idInfBasicaDet;
    }
  }
  id: number = 0;
  organizacionAprovechamientio: string = "";
  noFamilias: string = "";
  especies: string = "";
  tipoProducto: string = "";
  idInfBasicaDet: number = 0;
}

export class SolicitudFaunaModel {
  constructor(data?: any) {
    if (data) {
      this.idFauna = data.idFauna ? data.idFauna : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.codigoTipo = data.codigoTipo ? data.codigoTipo : 'PMFI';
      this.tipoFauna = data.tipoFauna ? data.tipoFauna : "";
      this.nombre = data.nombre ? data.nombre : "";
      this.nombreCientifico = data.nombreCientifico ? data.nombreCientifico : "";
      this.familia = data.familia ? data.familia : "";
      this.estatus = data.estatus ? data.estatus : "V";
      this.estadoSolicitud = data.estadoSolicitud ? data.estadoSolicitud : "Solicitado";
      this.adjunto = data.adjunto ? data.adjunto : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro : 0;
      this.idArchivo = data.idArchivo ? data.idArchivo : 0;
    }
  }

  idFauna: number = 0;
  idPlanManejo: number = 0;
  codigoTipo: string = "";
  tipoFauna: string = "";
  nombre: string = "";
  nombreCientifico: string = "";
  familia: string = "";
  estatus: string = "";
  estadoSolicitud: string = "";
  adjunto: string = "";
  idUsuarioRegistro: number = 0;
  idArchivo: number = 0
}



export interface UnidadFisiograficaMap {
  OBJECTID?: number,
  CobVeg2013?: string,
  Simbolo?: string,
  Fisiogr?: string,
  Shape_Leng?: number,
  CV_Label?: "Bcb",
  Shape_Le_1?: number,
  Shape_Area?: number
}

export class FieldMap {
  totalAreaPredio: number;
  totalAreaManejo: number;
  totalAreaFisiografia: number;
  totalPorcentajeFisiografia: number
  isEditedMap: boolean;
  relacionArchivo: { idArchivo: number; idPlanManejoArchivo: number; } | null;
  /**
   * Se guarda la relación de tablas vista, con el contenido del archivo shapefile
   */
  relacionArchivoTabla: string;

  constructor() {
    this.totalAreaPredio = 0;
    this.totalAreaManejo = 0;
    this.totalAreaFisiografia = 0;
    this.totalPorcentajeFisiografia = 0;
    this.isEditedMap = false;
    this.relacionArchivo = null;
    this.relacionArchivoTabla = '';
  }

  setRelacion(relacion: any) {
    this.relacionArchivo = { idArchivo: relacion?.idArchivo, idPlanManejoArchivo: relacion?.idPlanManejoArchivo };
  }
}

//#region REQUEST PARAMS
export class InfoBasicaRequest {

  idInfBasica!: number | null;
  codInfBasica!: PMFITipo | null;
  codNombreInfBasica!: string | null;
  departamento!: string | null;
  provincia!: string | null;
  distrito!: string | null;
  comunidad!: string | null;
  cuenca!: string | null;
  subCuenca!: string | null;
  idPlanManejo!: number | null;
  idUsuarioRegistro!: number | null;
  codCabecera1: string | null;
  codCabecera2: string | null;
  codCabecera3: string | null;
  listInformacionBasicaDet!: InfoListaModel[];
  constructor() {
    this.idInfBasica = null;
    this.codInfBasica = null;
    this.codNombreInfBasica = null;
    this.departamento = null;
    this.provincia = null;
    this.distrito = null;
    this.comunidad = null;
    this.cuenca = null;
    this.subCuenca = null;
    this.idPlanManejo = null;
    this.idUsuarioRegistro = null;
    this.codCabecera1 = null;
    this.codCabecera2 = null;
    this.codCabecera3 = null;
    this.listInformacionBasicaDet = [];
  }
}


//#endregion