import {
  Component,
  Input,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
} from "@angular/core";
import { MatNativeDateModule } from "@angular/material/core";
import { MatDialog } from "@angular/material/dialog";
import { PGMFArchivoDto, PlanManejoArchivo } from "@models";
import { ArchivoService, CoreCentralService, UsuarioService } from "@services";
import {
  AppMapaComponent,
  ArchivoTipo,
  CustomFeature,
  CustomLayerView,
  FigureTypes,
  isNull,
  isNullOrEmpty,
  LayerView,
  noop,
  PGMFArchivoTipo,
  PMFITipo,
  setOneSemicolon,
  ToastService,
} from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { element } from "protractor";
import { forkJoin, Observable, of } from "rxjs";
import { concatMap, finalize, map, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  EspecieForestal,
  EspeciesFauna,
  MedioTrasporte,
  Rios,
} from "src/app/model/medioTrasporte";
import { UbigeoResponse } from "src/app/model/ubigeo";
import { ComboModel } from "src/app/model/util/Combo";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { OrdenamientoInternoPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/ordenamiento-interno-pmfi.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { MapCustomPMFIComponent } from "src/app/shared/components/map-custom-pmfi/map-custom-pmfi.component";
import { MapCustomComponent } from "src/app/shared/components/map-custom/map-custom.component";
import { ModalFormEspeciesComponent } from "./modal-form-especies/modal-form-especies.component";
import {
  AccesibilidadVias,
  FieldMap,
  InfoBasicaRequest,
  InfoListaModel,
  SolicitudFaunaModel,
  UnidadFisiograficaMap,
} from "./models";
import {CodigosPMFI} from '../../../../../model/util/PMFI/PMFI';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {Router} from '@angular/router';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import { HttpErrorResponse } from "@angular/common/http";
import Polygon from "@arcgis/core/geometry/Polygon";

@Component({
  selector: "app-pmfi-informacion-area",
  templateUrl: "./pmfi-informacion-area.component.html",
  styleUrls: ["./pmfi-informacion-area.component.scss"],
})
export class PmfiInformacionAreaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @ViewChild(AppMapaComponent) map!: AppMapaComponent;
  @ViewChild(MapCustomPMFIComponent) mapCustom!: MapCustomPMFIComponent;
  //codigoProceso:string = '';
  codigoSubSeccion:string = '';


  ref!: DynamicDialogRef;

  idInfBasica: number = 0;
  departamento: string = "";
  provincia: string = "";
  distrito: string = "";
  comunidad: string = "";
  cuenca: string = "";
  nombreArchivo: string = "";

  lstAccesibilidad1: AccesibilidadVias[] = [];
  lstAccesibilidad2: AccesibilidadVias[] = [];

  lstAspectoFisicoFisiografia: any[] = [];
  lstUbicacionGeografica: any[] = [];
  lstUbicacionAreaManejo: any[] = [];

  lstZonas: AccesibilidadVias[] = [];
  ZonaModel: AccesibilidadVias = new AccesibilidadVias();
  idInfBasicaZonas: number = 0;

  cmbZona: ComboModel[] = [
    { name: "17", code: "17" },
    { name: "18", code: "18" },
    { name: "19", code: "19" },
  ];

  tituloModalMantenimiento: String = "";
  tipoModalAccebilidad: String = "1";
  verModalMantenimiento: boolean = false;
  tipoAccion: String = "";

  accesibilidadModel: AccesibilidadVias = new AccesibilidadVias();
  idInfBasicaAcesibilidad: number = 0;
  idInfBasicaUbicacionPolitica: number = 0;

  cmbMedioTransporte: MedioTrasporte[] = [];
  idContrato: number = 0;

  lstAspectoFisicoHidrografia: AccesibilidadVias[] = [];
  cmbRios: Rios[] = [];
  tituloModalCuerposAgua: String = "";
  verModalCuerposAgua: boolean = false;
  tipoAccionCuerposAgua: String = "";
  cuerposAgua: AccesibilidadVias = new AccesibilidadVias();
  idInfBasicaCuerposAgua: number = 0;

  tituloModalAspectosBiologicos: String = "";
  verModalAspectosBiologicos: boolean = false;
  tipoAccionAspectosBilogicos: String = "";
  infoFaunaModel: InfoListaModel = new InfoListaModel();

  comboListEspeciesFauna: EspeciesFauna[] = [];
  comboListEspeciesForestal: EspecieForestal[] = [];

  verModalFauna: boolean = false;
  verModalForestal: boolean = false;
  selecEspeciesFauna!: AccesibilidadVias;
  selectEspecieForestal!: AccesibilidadVias;

  selectFauna: AccesibilidadVias[] = [];
  selectForestal: AccesibilidadVias[] = [];

  queryFauna: string = "";
  queryForestal: string = "";

  selectedValues: AccesibilidadVias[] = [];
  selectedValuesForestal: AccesibilidadVias[] = [];

  listGuardarFaunas: AccesibilidadVias[] = [];
  listGuardarFlora: AccesibilidadVias[] = [];

  deleteFauna: any[] = [];
  selectFaunaLocal: any[] = [];

  verAddEspecie: boolean = false;
  escomunidad: any = null;

  lstDetalle5: AccesibilidadVias[] = [];
  tituloModalMantenimiento5: String = "";
  verModalMantenimiento5: boolean = false;
  tipoAccion5: String = "";
  context5: AccesibilidadVias = new AccesibilidadVias();
  idInfBasicaContext5: number = 0;

  listAspectosBiologicos: SolicitudFaunaModel[] = [];

  listaForestal: any[] = [];
  listaFauna: any[] = [];

  indexEdit: number = 0;
  indexElim: number = 0;

  objbuscar = {
    dato: '',
    nombreCientifico: '',
    nombreComun: '',
    pageNum: 1,
    pageSize: 10,
  };
  totalRecords: number = 0;

  //#region map
  fieldMap: FieldMap = new FieldMap();

  //#endregion

  idUsuario!: number;
  idOrdenamientoProteccion!:number;



  codigoProceso = CodigosPMFI.CODIGO_PROCESO;
  codigoTab = CodigosPMFI.TAB_3;

  codigoAcordeon_3_1: string = CodigosPMFI.ACORDEON_3_1;
  codigoAcordeon_3_2: string = CodigosPMFI.ACORDEON_3_2;
  codigoAcordeon_3_3: string = CodigosPMFI.ACORDEON_3_3;
  codigoAcordeon_3_4: string = CodigosPMFI.ACORDEON_3_4;
  codigoAcordeon_3_5: string = CodigosPMFI.ACORDEON_3_5;
  codigoAcordeon_3_6: string = CodigosPMFI.ACORDEON_3_6;

  detEvaluacion_3_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_1,
  });

  detEvaluacion_3_2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_2,
  });

  detEvaluacion_3_3: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_3,
  });

  detEvaluacion_3_4: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_4,
  });

  detEvaluacion_3_5: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_5,
  });

  detEvaluacion_3_6: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon_3_6,
  });

  evaluacion: any;

  listCuenca: any[] = [];
  listSubcuenca: any[] = [];
  listCuencaSubcuenca: CuencaSubcuencaModel[] = [];
  idCuenca!: number | null;
  idSubcuenca!: number | null;
  transporte = [
    { label: "Terrestre", value: "TERRESTRE" },
    { label: "Fluvial", value: "FLUVIAL" },
    { label: "Mixto", value: "MIXTO" },
  ];

  idInfBasicaF: number = 0;
  listaFisiografiaUMF: any = [];
  totalSumaUF!: string;
  totalPorcentajeUF!: string;
  totalRecordsFA: number = 0;
  totalRecordsFL: number = 0;
  idInfBasicaUG: number = 0;
  idInfBasicaUA: number = 0;

  constructor(
    private confirmationService: ConfirmationService,
    private informacionAreaPmfiService: InformacionAreaPmfiService,
    private user: UsuarioService,
    private informacionGeneralService: InformacionGeneralService,
    private coreCentralService: CoreCentralService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private messageService: MessageService,
    private apiArchivo: ArchivoService,
    private apiOrdenamiento: OrdenamientoInternoPmfiService,
    private anexosService: AnexosService,
    private evaluacionService: EvaluacionService,
    private router: Router,
    private servCoreCentral: CoreCentralService
  ) {}

  ngOnInit(): void {
    this.listarCuencaSubcuenca();
    this.listarUbicacionPolitica();
    this.listarUbicacionGeografica();
    this.listarUbicacionArea();
    this.idUsuario  = this.user.idUsuario;

    this.listarInfBasicaGeneral();
    this.obtenerContrato();
    this.listMedioTrasporte();
    this.listRios();
    // this.listarInfBasicaFauna();
    // this.listaPorFiltroEspecieFauna();
    this.listaPorFiltroEspecieForestal();

    this.listarInfBasicaRutasTerrestres();
    this.listarInfBasicaRutasAereas();

    this.listarInfBasicaCuerposAgua();
    this.listarFisiografia();

    this.listarInfBasicaZonasVida();
    this.listarInfBasicaFauna();
    this.listarInfBasicaFlora();
    this.listarInfBasicaAspectosSociales();

    this.obtenerFaunas();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  ngAfterViewInit(): void {
    this.getInitData();
  }

  listarInfBasicaGeneral() {
    this.lstAspectoFisicoHidrografia = [];
    var params = {
      idInfBasica: "PMFI",
      idPlanManejo: this.idPlanManejo,
      codCabecera: "PMFIAM",
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          // this.cuenca = element.cuenca;
          this.idCuenca = isNullOrEmpty(element.cuenca) ? null : Number(element.cuenca);
          this.onSelectedCuenca({ value: this.idCuenca });
          this.idSubcuenca = isNullOrEmpty(element.subCuenca) ? null : Number(element.subCuenca);
        });
      });
  }

  getInitData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.obtenerArchivoMapa()
      .pipe(
        concatMap((res) =>
          isNullOrEmpty(res) ? of([]) : this.map.addBase64File(res)
        )
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((layers) => this.setListLayer(layers));
  }

  obtenerContrato() {
    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: "PMFI",
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data) {
          response.data.forEach((element: any) => {
            this.idContrato = element.idContrato;
            this.listarUbigeoContratos(element.idContrato);
          });
        }
      });
  }

  listarUbigeoContratos(id: number) {
    this.informacionAreaPmfiService
      .listarUbigeoContratos(id)
      .subscribe((response: any) => {
        if (response.data) {
          response.data.forEach((element: UbigeoResponse) => {
            this.departamento = element.nombreDepartamento;
            this.provincia = element.nombreProvincia;
            this.distrito = element.nombreDistrito;
          });
        }
      });
  }

  listarCuencaSubcuenca() {
    this.listCuencaSubcuenca = [];
    this.listCuenca = [];

    this.servCoreCentral.listarCuencaSubcuenca().subscribe(
      (result: any) => {
        this.listCuencaSubcuenca = result.data;

        let listC: any[] = [];
        this.listCuencaSubcuenca.forEach((item) => {
          listC.push(item);
        });

        const setObj = new Map();
        const unicos = listC.reduce((arr, cuenca) => {
          if (!setObj.has(cuenca.idCuenca)) {
            setObj.set(cuenca.idCuenca, cuenca)
            arr.push(cuenca)
          }
          return arr;
        }, []);

        this.listCuenca = unicos;
        this.listCuenca.sort((a, b) => a.cuenca - b.cuenca);

        if (!isNullOrEmpty(this.idCuenca) && !isNullOrEmpty(this.idSubcuenca)) {
          this.onSelectedCuenca({ value: Number(this.idCuenca) });
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  onSelectedCuenca(param: any) {
    this.listSubcuenca = [];

    this.listCuencaSubcuenca.forEach((item) => {
      if (item.idCuenca == param.value) {
        this.listSubcuenca.push(item);
      }
    });
  }

  registrarUbicacionPolitica() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    var det = [
      {
        idInfBasicaDet: 0,
        codInfBasicaDet: this.codigoAcordeon_3_1,
        codSubInfBasicaDet: this.codigoAcordeon_3_1,
        idUsuarioRegistro: this.user.idUsuario,
      }
    ]

    var params = [
      {
        idInfBasica: this.idInfBasicaUbicacionPolitica ? this.idInfBasicaAcesibilidad : 0,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoTab,
        codNombreInfBasica: this.codigoAcordeon_3_1,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        departamento: this.departamento,
        provincia: this.provincia,
        distrito: this.distrito,
        cuenca: this.idCuenca ? String(this.idCuenca) : null,
        subCuenca: this.idSubcuenca ? String(this.idSubcuenca) : null,
        listInformacionBasicaDet: det,
      },
    ]

    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        this.toast.ok('Se registró la Ubicación Política correctamente.');
        this.listarUbicacionPolitica();
      });
  }

  listarUbicacionPolitica() {
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon_3_1,
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let element = response.data[0]
          this.idInfBasicaUbicacionPolitica = element.idInfBasica;
          this.idCuenca = isNullOrEmpty(element.cuenca) ? null : Number(element.cuenca);
          this.onSelectedCuenca({ value: this.idCuenca });
          this.idSubcuenca = isNullOrEmpty(element.subCuenca) ? null : Number(element.subCuenca);
        }
      });
  }

  listarUbicacionGeografica() {
    this.lstUbicacionGeografica = [];
    this.idInfBasicaUG = 0;
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera:  this.codigoAcordeon_3_1 + "UGAP",
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            const element = response.data[0];
            this.idInfBasicaUG = element.idInfBasica;
            this.areaTotalPredio(element.areaTotal);
            response.data.forEach((item: any) => {
              this.lstUbicacionGeografica.push({
                idInfBasica: item.idInfBasica,
                idInfBasicaDet: item.idInfBasicaDet,
                codInfBasicaDet: item.codInfBasicaDet,
                vertice: item.puntoVertice,
                este: item.coordenadaEste,
                norte: item.coordenadaNorte,
              });
            });
          }
        }
      });
  }

  registrarUbicacionGeografica() {
    let listaUbicacionGeografica: any[] = [];

    this.lstUbicacionGeografica.map((t: any) => {
      let obj: any = {};
      obj.codInfBasicaDet = this.codigoAcordeon_3_1 + "UGAP";
      obj.codSubInfBasicaDet = this.codigoAcordeon_3_1 + "UGAP";
      obj.puntoVertice = t.vertice,
      obj.coordenadaEsteIni = t.este,
      obj.coordenadaNorteIni = t.norte,
      obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
      listaUbicacionGeografica.push(obj);
    })

    let params = [
      {
        idInfBasica: this.idInfBasicaUG ? this.idInfBasicaUG : 0,
        idPlanManejo: this.idPlanManejo,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoAcordeon_3_1,
        codNombreInfBasica: this.codigoAcordeon_3_1 + "UGAP",
        idUsuarioRegistro: this.user.idUsuario,
        areaTotal: this.fieldMap.totalAreaPredio,
        listInformacionBasicaDet: listaUbicacionGeografica,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            this.toast.ok("Se registró ubicación geográfica del área del predio correctamente.");
            this.listarUbicacionGeografica();
          } else {
            this.toast.error(response?.message);
          }
        },
        (error) => {
        }
      );
  }

  listarUbicacionArea() {
    this.lstUbicacionAreaManejo = [];
    this.idInfBasicaUA = 0;
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera:  this.codigoAcordeon_3_1 + "UAM",
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            const element = response.data[0];
            this.idInfBasicaUA = element.idInfBasica;
            this.areaTotalManejo(element.areaTotal);
            response.data.forEach((item: any) => {
              this.lstUbicacionAreaManejo.push({
                idInfBasica: item.idInfBasica,
                idInfBasicaDet: item.idInfBasicaDet,
                codInfBasicaDet: item.codInfBasicaDet,
                vertice: item.puntoVertice,
                este: item.coordenadaEste,
                norte: item.coordenadaNorte,
              });
            });
          }
        }
      });
  }

  registrarUbicacionArea() {
    let listaUbicacionArea: any[] = [];

    this.lstUbicacionAreaManejo.map((t: any) => {
      let obj: any = {};
      obj.codInfBasicaDet = this.codigoAcordeon_3_1 + "UAM";
      obj.codSubInfBasicaDet = this.codigoAcordeon_3_1 + "UAM";
      obj.puntoVertice = t.vertice,
      obj.coordenadaEsteIni = t.este,
      obj.coordenadaNorteIni = t.norte,
      obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
      listaUbicacionArea.push(obj);
    })

    let params = [
      {
        idInfBasica: this.idInfBasicaUA ? this.idInfBasicaUA : 0,
        idPlanManejo: this.idPlanManejo,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoAcordeon_3_1,
        codNombreInfBasica: this.codigoAcordeon_3_1 + "UAM",
        idUsuarioRegistro: this.user.idUsuario,
        areaTotal: this.fieldMap.totalAreaPredio,
        listInformacionBasicaDet: listaUbicacionArea,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            this.toast.ok("Se registró ubicación del área de manejo correctamente.");
            this.listarUbicacionArea();
          } else {
            this.toast.error(response?.message);
          }
        },
        (error) => {
        }
      );
  }

  // ----- Accesibilidad -----

  listMedioTrasporte() {
    this.informacionAreaPmfiService
      .listarMedioTransporte(0)
      .subscribe((response: any) => {
        if (response.data) {
          this.cmbMedioTransporte = [...response.data];
        }
      });
  }

  listarInfBasicaRutasTerrestres() {
    this.lstAccesibilidad1 = [];
    var params = {
      idInfBasica: "PMFI",
      idPlanManejo: this.idPlanManejo,
      codCabecera: "PMFACC1",
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idInfBasicaDet != null) {
            this.idInfBasicaAcesibilidad = element.idInfBasica;
            this.lstAccesibilidad1.push(new AccesibilidadVias(element));
          }
        });
      });
  }

  listarInfBasicaRutasAereas() {
    this.lstAccesibilidad2 = [];
    var params = {
      idInfBasica: "PMFI",
      idPlanManejo: this.idPlanManejo,
      codCabecera: "PMFACC2",
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idInfBasicaDet != null) {
            this.lstAccesibilidad2.push(new AccesibilidadVias(element));
          }
        });
      });
  }

  abrirModal(
    tipo: String,
    tipoAccesibilidad: String,
    index: number,
    data?: AccesibilidadVias
  ): void {
    this.tipoAccion = tipo;
    if (this.tipoAccion == "C") {
      this.tituloModalMantenimiento = "Registro";
      this.tipoModalAccebilidad = tipoAccesibilidad;
      this.accesibilidadModel = new AccesibilidadVias();
      this.accesibilidadModel.idUsuarioRegistro = this.user.idUsuario;
      this.accesibilidadModel.codInfBasicaDet =
        tipoAccesibilidad == "1" ? "PMFACC1" : "PMFACC2";
    } else {
      this.accesibilidadModel = new AccesibilidadVias(data);
      this.tituloModalMantenimiento = "Modificar";
      this.accesibilidadModel.id = index;
    }
    this.verModalMantenimiento = true;
  }

  registrar(): void {
    if (this.accesibilidadModel.referencia == "") {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "El campo Punto de Referencia es obligatorio.",
      });
      return;
    }

    if (this.tipoAccion == "C") {
      if (this.tipoModalAccebilidad == "1") {
        this.accesibilidadModel.idUsuarioRegistro = this.user.idUsuario;
        this.accesibilidadModel.codInfBasicaDet = "PMFACC1";
        this.lstAccesibilidad1.push(this.accesibilidadModel);
      } else if (this.tipoModalAccebilidad == "2") {
        this.accesibilidadModel.idUsuarioRegistro = this.user.idUsuario;
        this.accesibilidadModel.codInfBasicaDet = "PMFACC2";
        this.lstAccesibilidad2.push(this.accesibilidadModel);
      }
    } else if (this.tipoAccion == "E") {
      if (this.tipoModalAccebilidad == "1") {
        this.accesibilidadModel.idUsuarioRegistro = this.user.idUsuario;
        this.accesibilidadModel.codInfBasicaDet = "PMFACC1";
        this.accesibilidadModel = new AccesibilidadVias(
          this.accesibilidadModel
        );
        this.lstAccesibilidad1[
          this.accesibilidadModel.id
        ] = this.accesibilidadModel;
      } else if (this.tipoModalAccebilidad == "2") {
        this.accesibilidadModel.idUsuarioRegistro = this.user.idUsuario;
        this.accesibilidadModel.codInfBasicaDet = "PMFACC2";
        this.accesibilidadModel = new AccesibilidadVias(
          this.accesibilidadModel
        );
        this.lstAccesibilidad2[
          this.accesibilidadModel.id
        ] = this.accesibilidadModel;
      }
    }

    this.verModalMantenimiento = false;
  }

  openEliminar(
    e: any,
    tipoAccesibilidad: String,
    index: number,
    data?: AccesibilidadVias
  ): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data?.idInfBasicaDet == 0) {
          if (tipoAccesibilidad == "1") {
            this.lstAccesibilidad1.splice(index, 1);
          } else if (tipoAccesibilidad == "2") {
            this.lstAccesibilidad2.splice(index, 1);
          }
        } else {
          if (tipoAccesibilidad == "1") {
            let accesibilidadEliminar = {
              idInfBasica: 0,
              idInfBasicaDet: data?.idInfBasicaDet,
              codInfBasicaDet: "PMFACC1",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaPmfiService
              .eliminarInformacionBasica(accesibilidadEliminar)
              .subscribe((response: any) => {
                if (response.success == true) {
                  this.toast.ok(response?.message);
                  this.listarInfBasicaRutasTerrestres();
                } else {
                  this.toast.error(response?.message);
                }
              });
          } else if (tipoAccesibilidad == "2") {
            let accesibilidadEliminar = {
              idInfBasica: 0,
              idInfBasicaDet: data?.idInfBasicaDet,
              codInfBasicaDet: "PMFACC2",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.informacionAreaPmfiService
              .eliminarInformacionBasica(accesibilidadEliminar)
              .subscribe((response: any) => {
                if (response.success == true) {
                  this.toast.ok(response?.message);
                  this.listarInfBasicaRutasAereas();
                } else {
                  this.toast.error(response?.message);
                }
              });
          }
        }
      },
      reject: () => {},
    });
  }

  guardarAccesibilidad() {
    var params = [
      {
        idInfBasica: this.idInfBasicaAcesibilidad
          ? this.idInfBasicaAcesibilidad
          : 0,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoTab,
        codNombreInfBasica: this.codigoAcordeon_3_2,

        // codNombreInfBasica: "Accesibilidad",
        departamento: this.departamento,
        provincia: this.provincia,
        distrito: this.distrito,
        comunidad: null,
        // cuenca: this.cuenca,
        cuenca: this.idCuenca ? String(this.idCuenca) : null,
        subCuenca: this.idSubcuenca ? String(this.idSubcuenca) : null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: [
          ...this.lstAccesibilidad1,
          ...this.lstAccesibilidad2,
        ],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          // this.toast.ok(response?.message);
          this.toast.ok('Se registró Accesibilidad correctamente.');
          this.listarInfBasicaRutasTerrestres();
          this.listarInfBasicaRutasAereas();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  // ----------- Aspectos fisicos 3.3.1  ---------------------- //

  listRios() {
    var params = {
      id: 0,
      tipo: "RIO",
    };
    this.informacionAreaPmfiService
      .obtenerHidrografia(params)
      .subscribe((response: any) => {
        this.cmbRios = [...response.data];
      });
  }

  listarInfBasicaCuerposAgua() {
    this.lstAspectoFisicoHidrografia = [];
    var params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon_3_3 + "CA",
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idInfBasicaDet != null) {
            this.idInfBasicaCuerposAgua = element.idInfBasica;
            this.lstAspectoFisicoHidrografia.push(
              new AccesibilidadVias(element)
            );
          }
        });
      });
  }

  abrirModalCuerposAgua(
    tipo: String,
    index: number,
    data?: AccesibilidadVias
  ): void {
    this.tipoAccionCuerposAgua = tipo;

    if (this.tipoAccionCuerposAgua == "C") {
      this.tituloModalCuerposAgua = "Registro";
      this.cuerposAgua = new AccesibilidadVias();
    } else {
      this.tituloModalCuerposAgua = "Modificar";
      this.cuerposAgua = new AccesibilidadVias(data);
      this.cuerposAgua.id = index;
    }
    this.verModalCuerposAgua = true;
  }

  registrar2(): void {
    if (this.cuerposAgua.idRios == undefined) {
      this.messageService.add({
        key: "tr",
        severity: "warn",
        detail: "El campo Ríos es obligatorio.",
      });
      return;
    }
    if (this.tipoAccionCuerposAgua == "C") {
      this.filtrarIdRio();
      this.cuerposAgua.idUsuarioRegistro = this.user.idUsuario;
      this.cuerposAgua.codInfBasicaDet = this.codigoAcordeon_3_3 + "CA";
      this.lstAspectoFisicoHidrografia.push(this.cuerposAgua);
    } else if (this.tipoAccionCuerposAgua == "E") {
      this.filtrarIdRio();
      this.cuerposAgua.idUsuarioRegistro = this.user.idUsuario;
      this.cuerposAgua.codInfBasicaDet = this.codigoAcordeon_3_3 + "CA";
      this.cuerposAgua = new AccesibilidadVias(this.cuerposAgua);
      this.lstAspectoFisicoHidrografia[this.cuerposAgua.id] = this.cuerposAgua;
    }
    this.verModalCuerposAgua = false;
  }

  filtrarIdRio() {
    let rio = this.cmbRios.find(
      (ele) => ele.idHidrografia == this.cuerposAgua.idRios
    );
    let nombreRio = rio?.descripcion;
    this.cuerposAgua.idRios = Number(rio?.idHidrografia);
    this.cuerposAgua.nombreRio = nombreRio;
  }

  openEliminarCuerposAgua(
    e: any,
    codTabla: String,
    index: number,
    data?: AccesibilidadVias
  ): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data?.idInfBasicaDet == 0) {
          if (codTabla == "PMFRIO") {
            this.lstAspectoFisicoHidrografia.splice(index, 1);
          }
        } else {
          if (codTabla == "PMFRIO") {
            let params = {
              idInfBasica: 0,
              idInfBasicaDet: data?.idInfBasicaDet,
              codInfBasicaDet: "PMFRIO",
              idUsuarioElimina: this.user.idUsuario,
            };
            this.dialog.open(LoadingComponent, { disableClose: true });
            this.informacionAreaPmfiService
              .eliminarInformacionBasica(params)
              .pipe(finalize(() => this.dialog.closeAll()))
              .subscribe((response: any) => {
                if (response.success == true) {
                  this.toast.ok(response?.message);
                  this.lstAspectoFisicoHidrografia.splice(index, 1);
                } else {
                  this.toast.error(response?.message);
                }
              });
          }
        }
      },
      reject: () => {},
    });
  }

  registrarAspectosFisicos() {
    var params = [
      {
        idInfBasica: this.idInfBasicaCuerposAgua
          ? this.idInfBasicaCuerposAgua
          : 0,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoTab,
        codNombreInfBasica: this.codigoAcordeon_3_3,
        // codNombreInfBasica: "Aspectos físicos (hidrografía y fisiografía)",
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        subCuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: [...this.lstAspectoFisicoHidrografia],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarInfBasicaCuerposAgua();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listarFisiografia() {
    this.listaFisiografiaUMF = [];
    this.idInfBasicaF = 0;
    let params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera:  this.codigoAcordeon_3_3 + "PUFA",
    };

    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasicaDet != null) {
            const element = response.data[0];
            this.idInfBasicaF = element.idInfBasica;
            response.data.forEach((item: any) => {
              this.listaFisiografiaUMF.push(item);
            });
            this.calculateTotalFisiografia();
          }
        }
      });
  }

  calculateTotalFisiografia() {
    let sum1 = 0;
    this.listaFisiografiaUMF.forEach((item: any) => {
      sum1 += parseFloat(item.areaHa);
      item.areaHaPorcentaje = Number(((100 * parseFloat(item.areaHa)) / sum1).toFixed(2));
    });

    this.totalSumaUF = `${sum1.toFixed(2)}`;
    this.totalPorcentajeUF = this.listaFisiografiaUMF.length === 0 ? `${0}` : `${(100).toFixed(2)}`;
  }

  registrarFisiografia() {
    let listaFisiografia: any[] = [];

    this.listaFisiografiaUMF.map((t: any) => {
      let obj: any = {};
      obj.codInfBasicaDet = this.codigoAcordeon_3_3 + "PUFA";
      obj.codSubInfBasicaDet = this.codigoAcordeon_3_3 + "PUFA";
      obj.areaHa = t.areaHa;
      obj.areaHaPorcentaje = t.areaHaPorcentaje;
      obj.nombre = t.nombre;
      obj.descripcion = t.descripcion;
      obj.idInfBasicaDet = t.idInfBasicaDet ? t.idInfBasicaDet : 0;
      listaFisiografia.push(obj);
    })

    let params = [
      {
        idInfBasica: this.idInfBasicaF ? this.idInfBasicaF : 0,
        idPlanManejo: this.idPlanManejo,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoAcordeon_3_3,
        codNombreInfBasica: this.codigoAcordeon_3_3 + "PUFA",
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: listaFisiografia,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            this.toast.ok("Se registró aspectos de fisiografía correctamente.");
            this.listarFisiografia();
          } else {
            this.toast.error(response?.message);
          }
        },
        (error) => {
        }
      );
  }

  // ------------------------- Aspectos fisicos 3.3.2 -----------------------------------//

  tituloModalMantenimiento3: String = "";
  verModalMantenimiento3: boolean = false;
  tipoAccion3: String = "";

  context3: InfoListaModel = new InfoListaModel();

  openEliminar2(
    e: any,
    codTabla: String,
    index: number,
    data?: InfoListaModel
  ): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data?.idInfBasicaDet == 0) {
          if (codTabla == "PMFRIO") {
            this.lstAspectoFisicoHidrografia.splice(index, 1);
          } else if (codTabla == "PMFIUF") {
            this.lstAspectoFisicoFisiografia.splice(index, 1);
          }
        }
      },
      reject: () => {},
    });
  }

  abrirModal3(tipo: String, data?: InfoListaModel): void {
    this.tipoAccion3 = tipo;
    if (this.tipoAccion3 == "C") {
      this.tituloModalMantenimiento3 = "Registro";
      this.context3 = new InfoListaModel();
      this.context3.idUsuarioRegistro = this.user.idUsuario;
      this.context3.codInfBasicaDet = "PMFIUF";
    } else {
      this.tituloModalMantenimiento3 = "Modificar";
      this.context3 = new InfoListaModel(data);
    }

    this.verModalMantenimiento3 = true;
  }

  registrar3(): void {
    if (this.tipoAccion3 == "C") {
      this.lstAspectoFisicoFisiografia.push(this.context3);
    }
    this.verModalMantenimiento3 = false;
  }

  // ---------- Zonas de vida ------------------- //

  listarInfBasicaZonasVida() {
    this.lstZonas = [];
    var params = {
      idInfBasica: "PMFI",
      idPlanManejo: this.idPlanManejo,
      codCabecera: "PMFIZV",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idInfBasicaDet != null) {
            this.idInfBasicaZonas = element.idInfBasica;
            this.ZonaModel.zonaVida = element.zonaVida;
            this.ZonaModel.idInfBasicaDet = element.idInfBasicaDet;
          }
        });
      });
  }

  registrarZonas() {
    this.ZonaModel.idUsuarioRegistro = this.user.idUsuario;
    this.ZonaModel.codInfBasicaDet = "PMFIZV";
    this.ZonaModel = new AccesibilidadVias(this.ZonaModel);
    this.lstZonas.push(this.ZonaModel);
    var params = [
      {
        idInfBasica: this.idInfBasicaZonas ? this.idInfBasicaZonas : 0,
        codInfBasica: "PMFI",
        codNombreInfBasica: "Zonas de vida",
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        subCuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: [...this.lstZonas],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarInfBasicaZonasVida();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  // ---------- Aspectos Biológicos Fauna------------------- //
  loadData(e: any) {
    const pageSize = Number(e.rows);
    this.objbuscar.pageNum = Number(e.first) / pageSize + 1;
    this.objbuscar.pageSize = pageSize;
    this.listaPorFiltroEspecieFauna();
  }

  filtrarFauna() {
    if (this.queryFauna) {
      this.objbuscar.nombreCientifico = this.queryFauna;
      this.objbuscar.nombreComun = this.queryFauna;
    } else {
      this.objbuscar.nombreCientifico = "";
      this.objbuscar.nombreComun = "";
    }
    this.listaPorFiltroEspecieFauna();
  }

  // listaPorFiltroEspecieFauna() {
  //   let params = {
  //     idEspecie: null,
  //     nombreComun: null,
  //     nombreCientifico: this.queryFauna ? this.queryFauna : null,
  //     autor: null,
  //     familia: null,
  //     pageNum: null,
  //     pageSize: null,
  //   };
  //   this.dialog.open(LoadingComponent, { disableClose: true });
  //   this.coreCentralService
  //     .listaEspecieFaunaPorFiltro(params)
  //     .pipe(finalize(() => this.dialog.closeAll()))
  //     .subscribe((result: any) => {
  //       this.comboListEspeciesFauna = [...result.data];
  //       this.totalRecords = result.totalrecord;
  //       this.listarInfBasicaFauna();
  //     });
  // }

  listarInfBasicaFauna() {
    this.selectedValues = [];
    this.listaFauna = [];
    this.selectFauna = [];
    var params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon_3_5 + "FAU",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data) {
          response.data.forEach((element: any) => {
            this.selectFauna.push(element);
            this.listaFauna.push(element);
            // this.selectedValues.push(new AccesibilidadVias(element));
            // this.filtrarFaunaPorId(element);
            // this.escomunidad = element.comunidad;
          });

          this.totalRecordsFA = this.selectFauna.length;
        }
      });
  }

  abrirModalEspecieFauna() {
    this.queryFauna = "";
    this.objbuscar.nombreCientifico = "";
    this.objbuscar.nombreComun = "";
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.comboListEspeciesFauna = [];
    this.listaPorFiltroEspecieFauna();
    this.selecEspeciesFauna = {} as AccesibilidadVias;
    this.verModalFauna = true;
  }

  listaPorFiltroEspecieFauna() {
    let params = {
      idEspecie: null,
      nombreComun: this.objbuscar.nombreComun,
      nombreCientifico: this.objbuscar.nombreCientifico,
      autor: null,
      familia: null,
      pageNum: this.objbuscar.pageNum,
      pageSize: this.objbuscar.pageSize,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.coreCentralService
      .listaEspecieFaunaPorFiltro(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.totalRecords = result.totalrecord ? result.totalrecord : 0;
        this.comboListEspeciesFauna = [ ...result.data ];
        this.filtrarFaunaPorId();
      });
  }

  filtrarFaunaPorId() {
    this.selectedValues = [];
    var fauna: any[] = [];
    this.selectFauna.forEach((item: any) => {
      fauna = this.comboListEspeciesFauna.filter(
        (x) => x.idEspecie === item.idFauna || x.idEspecie === item.idEspecie
      );
      fauna.forEach((item: any) => {
        this.selectedValues.push(item);
      })
    });
  }

  onChangeFauna(event: any, data: any) {
    if (!event.checked) {
      this.deleteFauna.push(data);
    } else {
      this.deleteFauna.forEach((item, index) => {
        if (data.idEspecie === item.idEspecie) {
          this.deleteFauna.splice(index, 1);
        }
      })
      this.selectFaunaLocal.push(data)
    }
  }

  guardarFauna() {
    var tmpValues: any[] = [];
    tmpValues =  this.selectFaunaLocal;

    if (this.deleteFauna.length > 0) {
      this.deleteFauna.forEach((item) => {
        this.selectFaunaLocal.forEach((data, index) => {
          if (data.idEspecie === item.idEspecie) {
            this.selectFaunaLocal.splice(index, 1);
          }
        });
        this.selectFauna.forEach((data: any, index) => {
          if (data.idEspecie === item.idEspecie) {
            this.selectFauna.splice(index, 1);
          }
        });
        // this.selectFaunaLocal.map((data) => {
        //   this.selectFauna.push(data);
        // });
        this.eliminarFauna(item);
      });
    }

    this.selectFauna.forEach((item: any) => {
      tmpValues.forEach((element: any, index) => {
        if (item.idEspecie == element.idEspecie) {
          tmpValues.splice(index, 1);
        }
      });
    });

    tmpValues.forEach((item: any) => {
      this.selectFauna.push(item);
    });

    this.listGuardarFaunas = [];
    this.selectFauna.forEach((item: any) => {
      this.selecEspeciesFauna = {} as AccesibilidadVias;
      this.selecEspeciesFauna.idFauna = item.idEspecie;
      this.selecEspeciesFauna.nombreComun = item.nombreComun;
      this.selecEspeciesFauna.nombreCientifico = item.nombreCientifico;
      this.selecEspeciesFauna.familia = item.familia;
      this.selecEspeciesFauna.codInfBasicaDet = this.codigoAcordeon_3_5 + "FAU";
      this.selecEspeciesFauna.idUsuarioRegistro = this.user.idUsuario;
      this.selecEspeciesFauna.idInfBasicaDet = item.idInfBasicaDet
        ? item.idInfBasicaDet
        : 0;
      this.listGuardarFaunas.push(
        new AccesibilidadVias(this.selecEspeciesFauna)
      );
    });

    tmpValues = [];
    this.selectedValues = [];
    this.selectFaunaLocal = [];
    this.deleteFauna = [];
    this.totalRecordsFA = this.selectFauna.length;
    this.verModalFauna = false;
  }

  eliminarFauna(data: any) {
    this.listaFauna.forEach((item) => {
      if (data.idEspecie === item.idEspecie) {
        let params = {
          idInfBasica: 0,
          idInfBasicaDet: item.idInfBasicaDet,
          codInfBasicaDet: this.codigoAcordeon_3_5 + "FAU",
          idUsuarioElimina: this.user.idUsuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((response: any) => {
            this.dialog.closeAll();
          });
      }
    });
  }

  // filtrarFaunaPorId(element: any) {
  //   var selecFaunas: any;
  //   if (this.comboListEspeciesFauna.length != 0) {
  //     selecFaunas = this.comboListEspeciesFauna.filter(
  //       (x) => x.idEspecie === element.idFauna
  //     );
  //     selecFaunas.forEach((element: any) => {
  //       this.selectFauna.push(element);
  //     });
  //   }
  // }

  // onChangeFauna(event: any, data: any) {
  //   this.listaFauna.forEach((item) => {
  //     if (event.checked === false && data.idEspecie === item.idFauna) {
  //       let params = {
  //         idInfBasica: 0,
  //         idInfBasicaDet: item.idInfBasicaDet,
  //         codInfBasicaDet: this.codigoAcordeon_3_5 + "FAU",
  //         idUsuarioElimina: this.user.idUsuario,
  //       };
  //       this.informacionAreaPmfiService
  //         .eliminarInformacionBasica(params)
  //         .subscribe((response: any) => {});
  //     }
  //   });
  // }

  // abrirModalEspecieFauna() {
  //   this.selecEspeciesFauna = {} as AccesibilidadVias;
  //   this.selectedValues = this.selectFauna;
  //   this.verModalFauna = true;
  // }

  // filtrarFauna() {
  //   if (this.queryFauna) {
  //     this.comboListEspeciesFauna = this.comboListEspeciesFauna.filter((r) =>
  //       r.nombreCientifico.toLowerCase().includes(this.queryFauna.toLowerCase())
  //     );
  //   } else {
  //     this.listaPorFiltroEspecieFauna();
  //   }
  // }

  seleccionarNuevoaFauna() {
    this.listaFauna.forEach((item) => {
      this.selectFauna.forEach((element: any, index) => {
        if (element.idEspecie == item.idFauna) {
          this.selectFauna[index].idInfBasicaDet = item.idInfBasicaDet;
        }
      });
    });
  }

  // guardarFauna() {
  //   this.selectFauna = [];
  //   this.selectFauna = [...this.selectedValues];
  //   this.seleccionarNuevoaFauna();

  //   this.selectFauna.forEach((item: any) => {
  //     this.selecEspeciesFauna = {} as AccesibilidadVias;
  //     this.selecEspeciesFauna.idFauna = item.idEspecie;
  //     this.selecEspeciesFauna.nombreComun = item.nombreComun;
  //     this.selecEspeciesFauna.nombreCientifico = item.nombreCientifico;
  //     this.selecEspeciesFauna.familia = item.familia;
  //     this.selecEspeciesFauna.codInfBasicaDet = this.codigoAcordeon_3_5 + "FAU";
  //     this.selecEspeciesFauna.idUsuarioRegistro = this.user.idUsuario;
  //     this.selecEspeciesFauna.idInfBasicaDet = item.idInfBasicaDet
  //       ? item.idInfBasicaDet
  //       : 0;
  //     this.listGuardarFaunas.push(
  //       new AccesibilidadVias(this.selecEspeciesFauna)
  //     );
  //   });
  //   this.totalRecordsFA = this.selectFauna.length;
  //   this.verModalFauna = false;
  // }

  //--------------------- Aspectos Biológicos Forestal ----------------------------------//

  listaPorFiltroEspecieForestal() {
    let params = {};
    this.coreCentralService
      .ListaPorFiltroEspecieForestal(params)
      .subscribe((result: any) => {
        this.comboListEspeciesForestal = [...result.data];
        this.listarInfBasicaFlora();
      });
  }

  listarInfBasicaFlora() {
    this.selectedValuesForestal = [];
    this.listaForestal = [];
    this.selectForestal = [];
    var params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon_3_5 + "FLO",
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.listaForestal.push(element);
          this.selectedValuesForestal.push(new AccesibilidadVias(element));
          this.filtrarFaunaPorIdForesta(element);
          this.escomunidad = element.comunidad;
        });
        this.totalRecordsFL = this.selectForestal.length;
      });
  }

  filtrarFaunaPorIdForesta(element: any) {
    var selecFlora: any;
    if (this.comboListEspeciesForestal.length != 0) {
      selecFlora = this.comboListEspeciesForestal.filter(
        (x) => x.idEspecie == element.idFlora
      );
      selecFlora.forEach((item: any) => {
        this.selectForestal.push(item);
      });
    }
  }

  onChange(event: any, data: any) {
    this.listaForestal.forEach((item) => {
      if (event.checked === false && data.idEspecie === item.idFlora) {
        let params = {
          idInfBasica: 0,
          idInfBasicaDet: item.idInfBasicaDet,
          codInfBasicaDet: this.codigoAcordeon_3_5 + "FLO",
          idUsuarioElimina: this.user.idUsuario,
        };
        this.informacionAreaPmfiService
          .eliminarInformacionBasica(params)
          .subscribe((response: any) => {});
      }
    });
  }

  abrirModalEspecieForestal() {
    this.selectEspecieForestal = {} as AccesibilidadVias;
    this.selectedValuesForestal = this.selectForestal;
    this.verModalForestal = true;
  }

  filtrarForestal() {
    if (this.queryForestal) {
      this.comboListEspeciesForestal = this.comboListEspeciesForestal.filter(
        (r) =>
          r.nombreCientifico
            .toLowerCase()
            .includes(this.queryForestal.toLowerCase())
      );
    } else {
      this.listaPorFiltroEspecieFauna();
    }
  }

  seleccionarNuevos() {
    this.listaForestal.forEach((item) => {
      this.selectForestal.forEach((element: any, index) => {
        if (element.idEspecie == item.idFlora) {
          this.selectForestal[index].idInfBasicaDet = item.idInfBasicaDet;
        }
      });
    });
  }

  guardarForestal() {
    this.selectForestal = [];
    this.selectForestal = [...this.selectedValuesForestal];
    this.seleccionarNuevos();

    this.selectForestal.forEach((item: any) => {
      this.selectEspecieForestal = {} as AccesibilidadVias;
      this.selectEspecieForestal.idFlora = item.idEspecie;
      this.selectEspecieForestal.nombreComun = item.nombreComun;
      this.selectEspecieForestal.nombreCientifico = item.nombreCientifico;
      this.selectEspecieForestal.familia = item.familia;
      this.selectEspecieForestal.codInfBasicaDet = this.codigoAcordeon_3_5 + "FLO";
      this.selectEspecieForestal.idUsuarioRegistro = this.user.idUsuario;
      this.selectEspecieForestal.idInfBasicaDet = item.idInfBasicaDet
        ? item.idInfBasicaDet
        : 0;
      this.listGuardarFlora.push(
        new AccesibilidadVias(this.selectEspecieForestal)
      );
    });
    this.totalRecordsFL = this.selectForestal.length;
    this.verModalForestal = false;
  }

  guardarAspectosBiologicos() {
    var params = [
      {
        idInfBasica: 0,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoTab,
        codNombreInfBasica: this.codigoAcordeon_3_5,
        // codNombreInfBasica: "Aspectos Biológicos",
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: this.escomunidad,
        cuenca: null,
        subCuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: [
          ...this.listGuardarFaunas,
          ...this.listGuardarFlora,
        ],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listaPorFiltroEspecieFauna();
          this.listaPorFiltroEspecieForestal();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  // -------------------------- Agergar Especies ----------------------------------------- //

  obtenerFaunas() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoTipo: this.codigoProceso,
    };
    this.informacionAreaPmfiService
      .obtenerFauna(params)
      .subscribe((response: any) => {
        this.listAspectosBiologicos = [];
        response.data.forEach((element: any, index: number) => {
          if (element.idArchivo !== 0) {
            this.listarArchivo(element, index);
          } else if (element.idArchivo == 0) {
            this.listAspectosBiologicos.push({
              ...element,
              nombreArchivo: null,
            });
          }
        });
      });
  }

  listarArchivo(element: any, index: number, editar?: boolean) {
    var params = {
      idArchivo: element.idArchivo,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 38,
      codigoProceso: this.codigoProceso,
    };
    this.anexosService.listarArchivoDema(params)
      .subscribe((result: any) => {
      if (result.data) {
        result.data.forEach((item: any) => {
          this.nombreArchivo = item.nombreArchivo;
          if (editar) {
            element.nombreArchivo = this.nombreArchivo;
            this.listAspectosBiologicos[ index ] = element;
          } else {
            this.listAspectosBiologicos.push({
              ...element,
              nombreArchivo: this.nombreArchivo,
            });
          }
        });
      }
    });
  }

  openModalEspecies(mesaje: string, edit: boolean, data?: SolicitudFaunaModel) {
    this.ref = this.dialogService.open(ModalFormEspeciesComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        edit: edit,
        idPlanManejo: this.idPlanManejo,
      },
    });

    this.ref.onClose.subscribe((resp: SolicitudFaunaModel) => {
      if (resp) {
        var params = new SolicitudFaunaModel();
        params.idPlanManejo = this.idPlanManejo;
        params.idUsuarioRegistro = this.user.idUsuario;
        params.codigoTipo = this.codigoProceso;
        params.nombreCientifico = resp.nombreCientifico;
        params.familia = resp.familia;
        params.nombre = resp.nombre;
        params.adjunto = resp.adjunto;
        params.estatus = "V";
        params.estadoSolicitud = "Solicitado";
        params.idFauna = resp.idFauna ? resp.idFauna : 0;
        params.idArchivo = resp.idArchivo;
        this.informacionAreaPmfiService
          .registrarSolicitudFauna(params)
          .subscribe((response) => {
            this.obtenerFaunas();
          });
      }
    });
  }

  indexEditar(event: number) {
    this.indexEdit = 0;
    this.indexEdit = event;
  }

  editar(event: SolicitudFaunaModel) {
    this.ref = this.dialogService.open(ModalFormEspeciesComponent, {
      header: "Editar Nueva Especie",
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: event,
        edit: true,
        idPlanManejo: this.idPlanManejo,
      },
    });

    this.ref.onClose.subscribe((resp: SolicitudFaunaModel) => {
      if (resp) {
        var params = new SolicitudFaunaModel();
        params.idPlanManejo = this.idPlanManejo;
        params.idUsuarioRegistro = this.user.idUsuario;
        params.codigoTipo = this.codigoProceso;
        params.nombreCientifico = resp.nombreCientifico;
        params.familia = resp.familia;
        params.nombre = resp.nombre;
        params.adjunto = resp.adjunto;
        params.estatus = resp.estatus;
        params.estadoSolicitud = resp.estadoSolicitud;
        params.idFauna = resp.idFauna;
        params.idArchivo = resp.idArchivo;
        this.informacionAreaPmfiService
        .registrarSolicitudFauna(params)
          .subscribe((response) => {
            if (resp.idArchivo !== 0) {
              this.listarArchivo(resp, this.indexEdit, true);
            } else {
              this.listAspectosBiologicos[ this.indexEdit ] = resp;
            }
          });
      }
    });
  }

  indexEliminar(event: number) {
    this.indexElim = event;
  }

  eliminar(event: any) {
    if (event?.idInfBasicaDet == 0) {
      this.listAspectosBiologicos.splice(this.indexElim, 1);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      var params = {
        idFauna: event.idFauna,
        codInfBasica: this.codigoProceso,
        idUsuarioElimina: this.user.idUsuario,
      };
      this.informacionAreaPmfiService
        .eliminarFauna(params)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((response: any) => {
          if (response.success == true) {
            this.toast.ok(response?.message);
            this.obtenerFaunas();
          } else {
            this.toast.error(response?.message);
          }
        });
    }
  }

  /*   guardarNuevasespecies() {
    this.listAspectosBiologicos.forEach((item, index) => {
      this.informacionAreaPmfiService
        .registrarSolicitudFauna(item)
        .subscribe((response) => {
          console.log(response);
        });
      if (index == this.listAspectosBiologicos.length - 1) {
        this.obtenerFaunas();
      }
    });
  } */

  // ----------------------------- 3.6 Aspectos Sociales -------------------------------------- //

  listarInfBasicaAspectosSociales() {
    this.lstDetalle5 = [];
    var params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoAcordeon_3_6,
    };
    this.informacionAreaPmfiService
      .listarInformacionBasica(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          if (element.idInfBasicaDet != null) {
            this.idInfBasicaContext5 = element.idInfBasica;
            this.lstDetalle5.push(new AccesibilidadVias(element));
          }
        });
      });
  }

  abrirModal5(tipo: String, index: number, data?: AccesibilidadVias): void {
    this.tipoAccion5 = tipo;
    if (this.tipoAccion5 == "C") {
      this.tituloModalMantenimiento5 = "Registro";
      this.context5 = new AccesibilidadVias();
    } else if (this.tipoAccion5 == "E") {
      this.tituloModalMantenimiento5 = "Modificar";
      this.context5 = new AccesibilidadVias(data);
      this.context5.id = index;
    }
    this.verModalMantenimiento5 = true;
  }

  registrar5(): void {
    if (this.validation5(this.context5)) {
      if (this.tipoAccion5 == "C") {
        this.context5.idUsuarioRegistro = this.user.idUsuario;
        this.context5.codInfBasicaDet = this.codigoAcordeon_3_6;
        this.lstDetalle5.push(this.context5);
      } else if (this.tipoAccion5 == "E") {
        this.context5.idUsuarioRegistro = this.user.idUsuario;
        this.context5.codInfBasicaDet = this.codigoAcordeon_3_6;
        this.context5 = new AccesibilidadVias(this.context5);
        this.lstDetalle5[ this.context5.id ] = this.context5;
      }
      this.verModalMantenimiento5 = false;
    }
  }

  validation5(data: AccesibilidadVias) {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!data.descripcion) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Organización para el aprovechamiento.\n";
    }
    if (!data.puntoVertice) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: N° de familias o personas involucradas en la actividad.\n";
    }
    if (!data.zonaVida) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Especie a aprovechar.\n";
    }
    if (!data.referencia) {
      validar = false;
      mensaje = mensaje + '(*) Debe ingresar: Tipo de producto a aprovechar.\n';
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }

  openEliminar5(e: any, index: number, data?: AccesibilidadVias): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data?.idInfBasicaDet == 0) {
          this.lstDetalle5.splice(index, 1);
        } else {
          let params = {
            idInfBasica: 0,
            idInfBasicaDet: data?.idInfBasicaDet,
            codInfBasicaDet: this.codigoAcordeon_3_6,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.informacionAreaPmfiService
            .eliminarInformacionBasica(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.listarInfBasicaAspectosSociales();
              } else {
                this.toast.error(response?.message);
              }
            });
        }
      },
      reject: () => {},
    });
  }

  guardarAspectosSociales() {
    var params = [
      {
        idInfBasica: this.idInfBasicaContext5 ? this.idInfBasicaContext5 : 0,
        codInfBasica: this.codigoProceso,
        codSubInfBasica: this.codigoTab,
        codNombreInfBasica: this.codigoAcordeon_3_6,
        // codNombreInfBasica: "Aspectos sociales para el aprovechamiento",
        departamento: null,
        provincia: null,
        distrito: null,
        comunidad: null,
        cuenca: null,
        subCuenca: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listInformacionBasicaDet: [...this.lstDetalle5],
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionAreaPmfiService
      .registrarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarInfBasicaAspectosSociales();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listarDetalleInfoBasica(tipo: PMFITipo) {
    const params = {
      idInfBasica: PMFITipo.PMFI,
      idPlanManejo: this.idPlanManejo,
      codCabecera: tipo,
    };
    return this.informacionAreaPmfiService.listarInformacionBasica(params).pipe(
      tap((res: any) => {
        // console.log(res);
      })
    );
  }

  actualizarDetalleInfoBasica(detalle: InfoListaModel[]) {
    let p = new InfoBasicaRequest();
    p.idInfBasica = 0;
    p.codInfBasica = PMFITipo.PMFI;
    p.idPlanManejo = this.idPlanManejo;
    p.idUsuarioRegistro = this.user.idUsuario;
    p.listInformacionBasicaDet = detalle;
    p.codCabecera1 = PMFITipo.PMFI_AM;
    p.codCabecera2 = PMFITipo.PMFI_UF;
    p.departamento = this.departamento;
    p.distrito = this.distrito;
    p.provincia = this.provincia;
    // p.cuenca = this.cuenca;
    p.cuenca = this.idCuenca ? String(this.idCuenca) : null;
    p.subCuenca = this.idSubcuenca ? String(this.idSubcuenca) : null;

    return this.informacionAreaPmfiService
      .actualizarInformacionBasica([p])
      .pipe(
        tap((res: any) =>
          res.success
            ? this.toast.ok(res?.message)
            : this.toast.error(res?.message)
        )
      );
  }

  guardarShapes() {
    const list = [
      ...this.lstUbicacionGeografica,
      ...this.lstUbicacionAreaManejo,
      ...this.lstAspectoFisicoFisiografia,
    ];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.saveFileFlow()
      .pipe(concatMap(() => this.actualizarDetalleInfoBasica(list)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }
  //#region MAP FUNCTIONS
  loadShape(layers: CustomLayerView<UnidadFisiograficaMap>[] | LayerView[]) {
    if (layers?.length > 2) {
      this.toast.warn("Archivo no válido");
      this.map.deleteGroupLayer(String(layers[0].groupId));
      return;
    }
    const layer = layers[0];
    const items = this.setUnidadFisiografica(layer);
    this.lstAspectoFisicoFisiografia = [...items];
    this.fieldMap.isEditedMap = true;
    const title = this.map.joinTitle(`${layer.title}`);
    this.fieldMap.relacionArchivoTabla += `${PMFITipo.PMFI_UF}:${title};`;
  }

  abrirUrlMapa(){
    window.open("https://1drv.ms/u/s!Akun_qhd6hLSgpARyN-dbKJLjt1-UQ?e=gRFWmH", "_blank");
  }

  cargarAreaPredio2(event: any) {
    this.loadPoints(event).subscribe((layers) => {
      const items = this.setArea(layers, PMFITipo.PMFI_AP);
      this.fieldMap.totalAreaPredio = this.map.calculateAreaHaPoints(
        layers[0].features
      );
      this.lstUbicacionGeografica = [...items];
      this.fieldMap.isEditedMap = true;
      const title = this.map.joinTitle(`${layers[0].title}`);
      this.fieldMap.relacionArchivoTabla += `${PMFITipo.PMFI_AP}:${title};`;
    });
  }
  cargarAreaPredio(event: any) {
    this.mapCustom.onChangeFile(event,'TPPUNTOAREAPREDIO','AREAPREDIO');
  }
  listarVerticesAreaPredio(items:any){
    // console.log(items);
    this.lstUbicacionGeografica = items;
  }
  areaTotalPredio(area:number){
    this.fieldMap.totalAreaPredio = area;
  }
  cargarAreaManejo2(event: any) {
    this.loadPoints(event).subscribe((layers) => {
      const items = this.setArea(layers, PMFITipo.PMFI_AM);
      this.fieldMap.totalAreaManejo = this.map.calculateAreaHaPoints(
        layers[0].features
      );
      this.lstUbicacionAreaManejo = [...items];
      this.fieldMap.isEditedMap = true;
      const title = this.map.joinTitle(`${layers[0].title}`);
      this.fieldMap.relacionArchivoTabla += `${PMFITipo.PMFI_AM}:${title};`;
    });
  }
  cargarAreaManejo(event: any) {
    this.mapCustom.onChangeFile(event,'TPPUNTOAREAMANEJO','AREAMANEJO');
  }
  listarVerticesAreaManejo(items:any){
    // console.log(items);
    this.lstUbicacionAreaManejo = items;
  }
  areaTotalManejo(area:number){
    this.fieldMap.totalAreaManejo = area;
  }

  cargarUnidadFisiografica(event: any) {
    this.mapCustom.onChangeFile(event, 'TPFISIO', 'FISIOGRAFIA');
  }

  listarFisiografiaE(items: any){
    this.listaFisiografiaUMF = items;
    this.calculateTotalFisiografia();
  }

  loadPoints(event: any): Observable<LayerView[]> {
    const groupId = this.map.genId();
    const color = this.map.genColor();
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.map
      .addLayerFile(event, groupId, color)
      .pipe(finalize(() => this.dialog.closeAll()));
  }

  setArea(layers: LayerView[], tipo: PMFITipo): InfoListaModel[] {
    if (layers?.length > 2) {
      this.toast.warn("Archivo no válido");
      this.map.deleteGroupLayer(String(layers[0].groupId));
      return [];
    }
    const layer = layers[0];
    if (
      layer?.features?.length == 0 ||
      layer.features[0].geometry.type != FigureTypes.POINT
    ) {
      this.toast.warn("Cargue un Shapefile de puntos");
      this.map.deleteGroupLayer(String(layers[0].groupId));
      return [];
    }
    let items: InfoListaModel[] = [];
    layer.features.forEach((feature) => {
      const item: InfoListaModel = new InfoListaModel({
        coordenadaEste: feature?.properties?.ESTE,
        coordenadaNorte: feature?.properties?.NORTE,
        puntoVertice: feature?.properties?.VERTICES,
        idUsuarioRegistro: this.user.idUsuario,
        codInfBasicaDet: tipo,
      });
      items.push(item);
    });
    return items;
  }
  setUnidadFisiografica(layer: LayerView) {
    let items: InfoListaModel[] = [];
    let areaTotal = 0;
    let porcentaje = 0;
    layer.features.forEach((feature: any) => {
      const f = feature as CustomFeature<UnidadFisiograficaMap>;
      const areaHa = this.map.calculateAreaHaPolygon(f);
      const item: InfoListaModel = new InfoListaModel({
        descripcion: `${f?.properties?.CV_Label}-${f?.properties?.CobVeg2013}-${f?.properties?.Fisiogr}`,
        areaHa,
        areaHaPorcentaje: (areaHa * 100) / layer.area,
        codInfBasicaDet: PMFITipo.PMFI_UF,
        idUsuarioRegistro: this.user.idUsuario,
      });
      items.push(item);
      areaTotal += areaHa;
      porcentaje += item.areaHaPorcentaje;
    });
    this.fieldMap.totalAreaFisiografia = areaTotal;
    this.fieldMap.totalPorcentajeFisiografia = porcentaje;
    return items;
  }
  deleteLayer(l: LayerView) {
    let relaciones = setOneSemicolon(this.fieldMap.relacionArchivoTabla).split(
      ";"
    );
    let titulo = this.map.joinTitle(l.title);
    const index = relaciones.findIndex((x) => x.endsWith(titulo));
    if (index > -1) {
      const item = relaciones[index].split(":"); // PMFIUG:nombreArchivo
      const nameList = item[0];
      relaciones.splice(index, 1);
      switch (nameList) {
        case PMFITipo.PMFI_AP:
          this.lstUbicacionGeografica = [];
          this.fieldMap.totalAreaPredio = 0;
          break;
        case PMFITipo.PMFI_AM:
          this.lstUbicacionAreaManejo = [];
          this.fieldMap.totalAreaManejo = 0;
          break;
        case PMFITipo.PMFI_UF:
          this.lstAspectoFisicoFisiografia = [];
          this.fieldMap.totalAreaFisiografia = 0;
          this.fieldMap.totalPorcentajeFisiografia = 0;
      }
      this.fieldMap.relacionArchivoTabla = relaciones.join(";");
    }
  }
  deleteAllLayers() {
    this.fieldMap.relacionArchivoTabla = "";
    this.lstUbicacionGeografica = [];
    this.lstUbicacionAreaManejo = [];
    this.lstAspectoFisicoFisiografia = [];
    this.fieldMap.totalAreaManejo = 0;
    this.fieldMap.totalAreaPredio = 0;
    this.fieldMap.totalAreaFisiografia = 0;
    this.fieldMap.totalPorcentajeFisiografia = 0;
  }

  //#endregion

  //#region ARCHIVO - RELACION ARCHIVO
  obtenerArchivoMapa(): Observable<string> {
    return this.obtenerRelacionArchivo(
      PGMFArchivoTipo.PMFI,
      this.idPlanManejo,
      ArchivoTipo.SHAPEFILE_03
    )
      .pipe(
        tap((res) => {
          if (!isNull(res)) {
            this.fieldMap.setRelacion(res);
            this.fieldMap.relacionArchivoTabla = String(res?.observacion);
          }
        })
      )
      .pipe(map((res) => (res?.documento ? res.documento : "")));
  }
  obtenerRelacionArchivo(
    codigoProceso: string,
    idPlanManejo: number,
    idTipoDocumento: string
  ) {
    return this.apiOrdenamiento.obtenerRelacionArchivo(
      codigoProceso,
      idPlanManejo,
      idTipoDocumento
    );
  }

  setListLayer(layers: LayerView[]) {
    let relaciones = setOneSemicolon(this.fieldMap.relacionArchivoTabla).split(
      ";"
    );
    for (const relacion of relaciones) {
      let claveValor = relacion.split(":");
      claveValor = claveValor.length == 2 ? claveValor : ["", ""];
      const clave = claveValor[0];
      const valor = claveValor[1];
      const layer =
        layers.find((x) => this.map.joinTitle(x.title) == valor) ||
        new LayerView();
      switch (clave) {
        case PMFITipo.PMFI_AP:
          this.lstUbicacionGeografica = this.setArea([layer], clave);
          this.fieldMap.totalAreaPredio = this.map.calculateAreaHaPoints(
            layer.features
          );
          break;
        case PMFITipo.PMFI_AM:
          this.lstUbicacionAreaManejo = this.setArea([layer], clave);
          this.fieldMap.totalAreaManejo = this.map.calculateAreaHaPoints(
            layer.features
          );
          break;
        case PMFITipo.PMFI_UF:
          this.lstAspectoFisicoFisiografia = this.setUnidadFisiografica(layer);
      }
    }
  }

  saveFileFlow() {
    if (!this.fieldMap.isEditedMap) return of(null);

    const idArchivo = Number(this.fieldMap.relacionArchivo?.idArchivo);
    const idPlanManejoArchivo = Number(
      this.fieldMap.relacionArchivo?.idPlanManejoArchivo
    );

    const deleteFile = isNull(this.fieldMap.relacionArchivo)
      ? of(null)
      : this.deleteFile(idArchivo, idPlanManejoArchivo).pipe(
          tap(() => (this.fieldMap.isEditedMap = false))
        );

    if (this.map.isEmpty)
      return deleteFile.pipe(tap(() => (this.fieldMap.relacionArchivo = null)));

    return this.saveFile()
      .pipe(
        tap((res) => {
          this.fieldMap.relacionArchivo = {
            idArchivo: res?.data?.idArchivo,
            idPlanManejoArchivo: res?.data?.idPGMFArchivo,
          };
          this.fieldMap.isEditedMap = false;
        })
      )
      .pipe(concatMap(() => deleteFile));
  }
  saveFile() {
    return this.map
      .getZipFile()
      .pipe(tap({ error: (err) => this.toast.warn(err) }))
      .pipe(concatMap((blob) => this.guardarArchivo(blob as Blob)))
      .pipe(concatMap((idArchivo) => this.guardarRelacionArchivo(idArchivo)));
  }

  guardarArchivo(blob: Blob): Observable<number> {
    const file = new File([blob], `PMFI-IA-${this.idPlanManejo}.zip`);
    return this.apiArchivo
      .cargar(this.user.idUsuario, ArchivoTipo.SHAPEFILE_03, file)
      .pipe(map((res) => res?.data));
  }

  guardarRelacionArchivo(idArchivo: number) {
    const item = new PGMFArchivoDto({
      codigoTipoPGMF: PGMFArchivoTipo.PMFI,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: this.fieldMap.relacionArchivoTabla,
      idArchivo,
    });
    return this.apiOrdenamiento.registrarArchivo(item);
  }

  deleteFile(idArchivo: number, idPlanManejoArchivo: number) {
    return forkJoin([
      this.eliminarArchivo(idArchivo, this.user.idUsuario),
      this.eliminarRelacionArchivo(idPlanManejoArchivo, this.user.idUsuario),
    ]).pipe(map(() => null));
  }

  eliminarRelacionArchivo(
    idPlanManejoArchivo: number,
    idUsuarioElimina: number
  ) {
    const item = new PlanManejoArchivo({
      idPlanManejoArchivo,
      idUsuarioElimina,
    });
    return this.apiOrdenamiento.eliminarArchivo(item);
  }

  eliminarArchivo(idArchivo: number, idUsuarioElimina: number) {
    return this.apiArchivo.eliminarArchivo(idArchivo, idUsuarioElimina);
  }
  //#endregion



  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacion: this.codigoProceso
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {

              this.detEvaluacion_3_1 = Object.assign(this.detEvaluacion_3_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_1));
              this.detEvaluacion_3_2 = Object.assign(this.detEvaluacion_3_2,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_2));
              this.detEvaluacion_3_3 = Object.assign(this.detEvaluacion_3_3,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_3));
              this.detEvaluacion_3_4 = Object.assign(this.detEvaluacion_3_4,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_4));
              this.detEvaluacion_3_5 = Object.assign(this.detEvaluacion_3_5,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_5));
              this.detEvaluacion_3_6 = Object.assign(this.detEvaluacion_3_6,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon_3_6));

            }else{
              this.evaluacion = {
                idPlanManejo: this.idPlanManejo,
                codigoEvaluacion: this.codigoProceso,
                codigoEvaluacionDet: this.codigoProceso,
                codigoEvaluacionDetSub: this.codigoTab,
                listarEvaluacionDetalle: [],
                idUsuarioRegistro: this.user.idUsuario,
              };
            }
          }
        }
      });
  }


  registrarEvaluacion() {

    if (EvaluacionUtils.validar([this.detEvaluacion_3_1, this.detEvaluacion_3_2,this.detEvaluacion_3_3,this.detEvaluacion_3_4,this.detEvaluacion_3_5,this.detEvaluacion_3_6])) {

      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_1);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_2);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_3);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_4);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_5);
        this.evaluacion.listarEvaluacionDetalle.push(this.detEvaluacion_3_6);


        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){
    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:""
    }));
    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export interface CuencaSubcuencaModel {
  codCuenca: string,
  codSubCuenca: string,
  codigo: string,
  cuenca: string,
  idCuenca: number,
  idSubCuenca: number,
  subCuenca: string,
  valor: string
}
