import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LaborSilviculturalDto } from 'src/app/model/LaborSilviculturalDto';
import { TipoModal } from '../pmfi-manejo-aprovechamiento.component';
import { MessageService } from 'primeng/api';
import { AccionTipo, ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';

@Component({
  selector: 'app-registro-modificacion-modal',
  templateUrl: './registro-modificacion-modal.component.html',
  styleUrls: ['./registro-modificacion-modal.component.scss']
})
export class RegistroModificacionModalComponent implements OnInit {
  TipoModal = TipoModal;
  tipoModal!: TipoModal;
  labor: LaborSilviculturalDto = new LaborSilviculturalDto();
  f: FormGroup;
  labelBtn: string = (this.config.data.accion === AccionTipo.REGISTRAR) ? 'Agregar' : 'Editar';
  idGenerado: string = this.generarId();
  chkMarca: Boolean = true;

  constructor(private fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private toast: ToastService,
    private dialog: MatDialog) {
    this.tipoModal = (this.config.data.tipoModal) ?? TipoModal.Aprovechamiento;
    this.chkMarca = this.config.data.marca;
    this.f = this.initForm();
  }

  ngOnInit(): void {
    this.labor = this.config.data as LaborSilviculturalDto;
    
    if (this.tipoModal == TipoModal.Aprovechamiento) {
      const { idUsuarioRegistro, descripcion, equipos } = this.labor;
      const etapas = this.labor.actividad;

      this.f.patchValue({ idUsuarioRegistro, etapas, descripcion, equipos });
    }else{
      const { idUsuarioRegistro, descripcion, marca } = this.labor;
      const labores = this.labor.actividad;

      this.f.patchValue({ idUsuarioRegistro, labores, descripcion, marca });
    }
    
  }

  initForm(): FormGroup {
    let form: FormGroup;
    if (this.tipoModal == TipoModal.Aprovechamiento) {
      form = this.fb.group({
        etapas : [null, [Validators.required, Validators.minLength(1)]],
        descripcion : [null, [Validators.required, Validators.minLength(1)]],
        equipos : [null, [Validators.required, Validators.minLength(1)]]
      });
    } else {
      form = this.fb.group({
        labores : [null, [Validators.required, Validators.minLength(1)]],
        // marca : this.chkMarca,
        // marca : [false],
        descripcion : [{ value: null, disabled: !this.chkMarca }, [Validators.required, Validators.minLength(1)]]
      });
    }
    
    return form;
  }

  // toogleDescripcion(event?: any) {
    
  //   if(!event.checked){
  //     this.f.controls['descripcion'].disable();
  //     this.f.controls['descripcion'].setValue('');
  //   }else{
  //     this.f.controls['descripcion'].enable();
  //   }

  //   this.chkMarca = event.checked;
  // }

  guardar() {
    this.showFormError();
    if (this.f.invalid) return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    
    const id = (this.config.data.accion === AccionTipo.REGISTRAR ) ? this.idGenerado : this.config.data.id;
    if(this.config.data.accion === AccionTipo.EDITAR && this.config.data.id.substr(0,1) === "_"){
      this.config.data.accion = AccionTipo.REGISTRAR;
    }

    if (this.tipoModal == TipoModal.Aprovechamiento) {
      const  { idUsuarioRegistro, descripcion, equipos, etapas } = this.f.value;
      const actividad = etapas;

      this.labor = { ...this.labor, idUsuarioRegistro, actividad, descripcion, equipos, id };
    }else{
     
      const  { idUsuarioRegistro, descripcion, labores } = this.f.value;
      const actividad = labores;
      const marca = this.chkMarca;

      this.labor = { ...this.labor, idUsuarioRegistro, actividad, descripcion, marca, id };
    }
    this.labor.idUsuarioRegistro = 1;

    this.dialog.closeAll();
    this.ref.close(this.labor);
  }

  cancelar() {
    this.ref.close();
  }

  showFormError() {

    const keys = Object.keys(this.f.value);
    const valid = this.toast.validAndShowOneToastError<LaborSilviculturalDto>(keys, this.f.value, (this.tipoModal == TipoModal.Aprovechamiento) ? ValidacionCampoAprovechamiento : ValidacionCampoLabores);
    if (this.f.invalid && !valid) return;
  }

  generarId() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

}

const ValidacionCampoAprovechamiento: any = {
  etapas: 'Etapas',
  descripcion: 'Descripción',
  equipos: 'Equipos'
}

const ValidacionCampoLabores: any = {
  labores: 'Labores',
  descripcion: 'Descripción'
}