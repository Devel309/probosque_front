import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AccionTipo, compareObjects } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Detalle } from 'src/app/model/ActividadesSilviculturalesModel';
import { LaborSilviculturalDto } from 'src/app/model/LaborSilviculturalDto';
import { LaborSilviculturalServiceService } from 'src/app/service/labor-silvicultural-service.service';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';
import { TipoModal } from '../pmfi-manejo-aprovechamiento.component';
import { RegistroModificacionModalComponent } from '../registro-modificacion-modal/registro-modificacion-modal.component';

@Component({
  selector: 'app-aprovechamiento-form',
  templateUrl: './aprovechamiento-form.component.html',
  styleUrls: ['./aprovechamiento-form.component.scss']
})
export class AprovechamientoFormComponent implements OnInit {
  @Input() idPlanManejo: number = 0;
  @Output() public aprovechamientoDetalleOutput = new EventEmitter<LaborSilviculturalDto[]>();

  @Input() disabled!: boolean;
  lstAprovechamiento: LaborSilviculturalDto[] = [];
  lstModificados: LaborSilviculturalDto[] = [];
  AccionTipo = AccionTipo;
  usuario = 27;
  pendiente = false;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private api: LaborSilviculturalServiceService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private eventEmitterService: EventEmitterService
  ) { }

  ngOnInit(): void {
    // if(this.eventEmitterService.subsEtapa == undefined){
      this.eventEmitterService.subsEtapa = this.eventEmitterService.aprovechamientoListarFunction.subscribe((
        idPlanManejo: number) => this.listarEtapaAprovechamiento(idPlanManejo).subscribe());
    // }

    this.listarEtapaAprovechamiento(this.idPlanManejo).subscribe();
  }

  abrirModal(titulo: AccionTipo, labor?: LaborSilviculturalDto) {
    const header = `${titulo} Etapa de Aprovechamiento`;
    labor = labor ? labor : new LaborSilviculturalDto();
    labor.accion = titulo;

    let data = { ...labor, tipoModal: TipoModal.Aprovechamiento }

    const config = { header, data, width: '50vw', closable: true };
    const ref = this.dialogService.open(RegistroModificacionModalComponent, config);

    ref.onClose.subscribe((labor: LaborSilviculturalDto) => {
      if (labor) {
        this.pendiente = !compareObjects(data, labor) ? true : false;
        if (titulo === AccionTipo.REGISTRAR) {
          this.lstAprovechamiento.push(labor);
        } else {
          var index = this.lstAprovechamiento.findIndex(x => x.id === labor.id);

          if (index >= 0) {
            this.lstAprovechamiento[index] = labor;
          }
        }
        this.addListModificados(labor);
      }
    });
  }

  openEliminar(e: any, labor: LaborSilviculturalDto): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.lstAprovechamiento = this.eliminar(this.lstAprovechamiento, labor.id);
      },
      reject: () => {
      }
    });
  }

  listarEtapaAprovechamiento(idPlanManejo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.api.obtenerListaLaborSilviculturalPMFI(idPlanManejo, 5)
      .pipe(
        finalize(() => this.dialog.closeAll()),
        tap({
          next: res => {
            if (res?.data && res?.data !== undefined) {
              this.lstAprovechamiento = [];
              let response: Detalle[] = res.data.detalle;
              this.aprovechamientoDetalleOutput.emit([]);
              this.lstModificados = [];

              response.forEach(aprov => {
                const objLabor = new LaborSilviculturalDto(aprov);

                objLabor.id = (aprov.idActividadSilviculturalDet === null) ? this.generarId() : objLabor.id;
                objLabor.accion = (aprov.idActividadSilviculturalDet === null) ? AccionTipo.REGISTRAR : AccionTipo.EDITAR;

                this.lstAprovechamiento.push(objLabor);
                if (aprov.idActividadSilviculturalDet === null) {
                  this.addListModificados(objLabor);
                }
              })
              this.pendiente = false;
            };
          },
          error: (detail) => {
            
            this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
          }
        }));
  }
  eliminar(list: LaborSilviculturalDto[], id: string) {
    const item = list.find(x => x.id == id);
    if (item) {
      if (item.accion !== AccionTipo.REGISTRAR) {
        this.api.eliminarActividadSilviculturalPFDM({
          'idActividadSilviculturalDet': id,
          'idUsuarioElimina': this.usuario
        }).pipe(
          finalize(() => this.dialog.closeAll()),
          tap({
            next: res => {
              this.messageService.add({ severity: "success", summary: "", detail: "Se eliminó la etapa de aprovechamiento con éxito." });
              
            },
            error: (detail) => {
              
              this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
            }
          })).subscribe();
      } else {
        this.lstModificados = this.lstModificados.filter(item => item.id !== id);
        this.aprovechamientoDetalleOutput.emit(this.lstModificados);
        this.messageService.add({ severity: "success", summary: "", detail: "Se eliminó la etapa de aprovechamiento con éxito." });
      }

      list = list.filter(item => item.id !== id);
    }
    return list;
  }

  generarId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  addListModificados(data: LaborSilviculturalDto) {
    const itemModificado = this.lstModificados.findIndex((item) => item.id == data.id);
    if (itemModificado >= 0) {
      this.lstModificados.splice(itemModificado, 1);
    }

    this.lstModificados.push(data);
    this.aprovechamientoDetalleOutput.emit(this.lstModificados);
  }

}
