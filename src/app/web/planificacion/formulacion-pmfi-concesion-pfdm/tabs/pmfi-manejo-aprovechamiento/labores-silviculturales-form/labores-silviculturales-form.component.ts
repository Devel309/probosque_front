import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AccionTipo, compareObjects } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Detalle } from 'src/app/model/ActividadesSilviculturalesModel';
import { LaborSilviculturalDto } from 'src/app/model/LaborSilviculturalDto';
import { LaborSilviculturalServiceService } from 'src/app/service/labor-silvicultural-service.service';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';
import { TipoModal } from '../pmfi-manejo-aprovechamiento.component';
import { RegistroModificacionModalComponent } from '../registro-modificacion-modal/registro-modificacion-modal.component';

@Component({
  selector: 'app-labores-silviculturales-form',
  templateUrl: './labores-silviculturales-form.component.html',
  styleUrls: ['./labores-silviculturales-form.component.scss']
})
export class LaboresSilviculturalesFormComponent implements OnInit {
  @Input() idPlanManejo: number = 0;
  @Output() public laboresDetalleOutput = new EventEmitter<LaborSilviculturalDto[]>();
  @Input() disabled!: boolean;
  lstLaboresComparable: LaborSilviculturalDto[] = [];
  lstLaboresSilviculturales: LaborSilviculturalDto[] = [];
  lstModificados: LaborSilviculturalDto[] = [];
  AccionTipo = AccionTipo;
  usuario = 27;
  pendiente = false;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private api: LaborSilviculturalServiceService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private eventEmitterService: EventEmitterService
  ) { }

  ngOnInit(): void {
    // if(this.eventEmitterService.subsLabores == undefined){
      this.eventEmitterService.subsLabores = this.eventEmitterService.laboresSilviculturalesListarFunction.subscribe((idPlanManejo: number) => this.listaLaborSilvicultural(idPlanManejo).subscribe());
    // }

    this.listaLaborSilvicultural(this.idPlanManejo).subscribe();
  }

  abrirModal(titulo: AccionTipo, labor?: LaborSilviculturalDto) {
    const header = `${titulo} Labores Silviculturales`;
    labor = labor ? labor : new LaborSilviculturalDto();
    labor.accion = titulo;
    labor.marca = labor ? labor.marca : true;

    let data = { ...labor, tipoModal: TipoModal.LaborSilvicultural }

    const config = { header, data, width: '50vw', closable: true };
    const ref = this.dialogService.open(RegistroModificacionModalComponent, config);

    ref.onClose.subscribe((labor: LaborSilviculturalDto) => {
      if (labor) {
        this.pendiente = !compareObjects(data, labor) ? true : false;
        if (titulo === AccionTipo.REGISTRAR) {
          this.lstLaboresSilviculturales.push(labor);
        } else {
          var index = this.lstLaboresSilviculturales.findIndex(x => x.id === labor.id);

          if (index >= 0) {
            this.lstLaboresSilviculturales[index] = labor;
          }
        }
        this.addListModificados(labor);
      }
    });

  }

  openEliminar(e: any, labor: LaborSilviculturalDto): void {
    this.confirmationService.confirm({
      target: e.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.lstLaboresSilviculturales = this.eliminar(this.lstLaboresSilviculturales, labor.id);
      },
      reject: () => {
      }
    });
  }

  listaLaborSilvicultural(idPlanManejo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.api.obtenerListaLaborSilviculturalPMFI(idPlanManejo, 6)
      .pipe(
        finalize(() => this.dialog.closeAll()),
        tap({
          next: res => {
            if (res?.data && res?.data !== undefined) {
              this.lstLaboresSilviculturales = [];
              let response: Detalle[] = res.data.detalle;
              this.laboresDetalleOutput.emit([]);
              this.lstModificados = [];

              response.forEach(aprov => {
                const objLabor = new LaborSilviculturalDto(aprov);

                objLabor.id = (aprov.idActividadSilviculturalDet === null) ? this.generarId() : objLabor.id;
                objLabor.accion = (aprov.idActividadSilviculturalDet === null) ? AccionTipo.REGISTRAR : AccionTipo.EDITAR;

                this.lstLaboresSilviculturales.push(objLabor);
                if (aprov.idActividadSilviculturalDet === null) {
                  this.addListModificados(objLabor);
                }
              })
              this.lstLaboresComparable = Object.assign([], this.lstLaboresSilviculturales);
              this.pendiente = false;
            };
          },
          error: (detail) => {
            
            this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
          }
        }));
  }

  eliminar(list: LaborSilviculturalDto[], id: string) {
    const item = list.find(x => x.id == id);
    if (item) {
      if (item.accion !== AccionTipo.REGISTRAR) {
        //llamar al servicio que elimina
        this.api.eliminarActividadSilviculturalPFDM({
          'idActividadSilviculturalDet': id,
          'idUsuarioElimina': this.usuario
        }).pipe(
          finalize(() => this.dialog.closeAll()),
          tap({
            next: res => {
              this.messageService.add({ severity: "success", summary: "", detail: "Se eliminó la labor silvicultural con éxito." });
              
            },
            error: (detail) => {
              
              this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
            }
          })).subscribe();
      } else {
        this.lstModificados = this.lstModificados.filter(item => item.id !== id);
        this.laboresDetalleOutput.emit(this.lstModificados);
        this.messageService.add({ severity: "success", summary: "", detail: "Se eliminó la labor silvicultural con éxito." });
      }

      list = list.filter(item => item.id !== id);
    }
    return list;
  }


  generarId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  addListModificados(data: LaborSilviculturalDto) {
    const itemModificado = this.lstModificados.findIndex((item) => item.id == data.id);
    if (itemModificado >= 0) {
      this.lstModificados.splice(itemModificado, 1);
    }

    this.lstModificados.push(data);
    this.laboresDetalleOutput.emit(this.lstModificados);
  }

  comprobarDiferencia(event?: any, data?: any) {
    if(event.checked == false) {
      data.descripcion = '';
    }
    
    let obj = this.lstLaboresComparable.find( x => x.id === data.id);
    if(compareObjects(this.lstLaboresComparable, this.lstLaboresSilviculturales)){
      this.addListModificados(data);
      if(!this.pendiente){
        this.pendiente = true;
      }
    }else{
      this.pendiente = true;
    }
  }
}
