import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlanManejoService, UsuarioService } from '@services';
import { CodigoEstadoPlanManejo } from '../../../model/util/CodigoEstadoPlanManejo';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import { Perfiles } from '../../../model/util/Perfiles';

@Component({
  selector: 'app-formulacion-pmfi-concesion-pfdm',
  templateUrl: './formulacion-pmfi-concesion-pfdm.component.html',
  styleUrls: ['./formulacion-pmfi-concesion-pfdm.component.scss'],
})
export class FormulacionPMFIConcesionPFDMComponent implements OnInit {
  idPlanManejo!: number;
  disabled: boolean = true;
  isPerfilArffs:boolean = false;

  usuario!: UsuarioModel;

  tabIndex: number = 0;

  obj: any ={};

  constructor(
    private route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private planManejoService: PlanManejoService
  ) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.obtenerPlan();
  }

  obtenerPlan() {
    this.usuario = this.usuarioService.usuario;
    let params = {
      idPlanManejo: this.idPlanManejo,
    };

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.disabled = true
      this.isPerfilArffs = true;
    } else {
      this.planManejoService
        .obtenerPlanManejo(params)
        .subscribe((result: any) => {
          if (
            result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO ||
            result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION
          ) {
            this.disabled = true;
          } else if (
            result.data.codigoEstado == CodigoEstadoPlanManejo.BORRADOR
          ) {
            this.disabled = false;
          }
        });
    }
  }

  ngAfterViewInit(){
    if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    }
    //console.log("MAIN ngAfterViewInit");
  }

  setTabIndex(obj: any){

    var cod = obj.tab.replace(obj.codigoEvaluacionDet,'');
    //console.log("MAIN setTabIndex ", cod);

    if(cod === 'IG') this.tabIndex = 0;
    else if (cod === 'REEVRIIC') {
      this.tabIndex = 0;

      setTimeout (() => {
        
        this.tabIndex = 1;
      }, 1000);}
      else if (cod === 'O') {this.tabIndex = 1;}
    else if (cod === 'IA') this.tabIndex = 2;
    else if (cod === 'OI') this.tabIndex = 3;
    else if (cod === 'IERS') this.tabIndex = 4;
    else if (cod === 'SMALS') this.tabIndex = 5;
    else if (cod === 'IMPUMF') this.tabIndex = 6;
    else if (cod === 'IIANEMPM') this.tabIndex = 7;
    else if (cod === 'CA') this.tabIndex = 8;
    else if (cod === 'AN') this.tabIndex = 9;
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }
}
