import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { finalize, tap } from "rxjs/operators";

import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PlanManejoService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { Page, PlanManejoMSG as MSG } from "@models";
import { FormBuilder, FormGroup } from "@angular/forms";
import { LazyLoadEvent } from "primeng/api";
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from "../../../model/util/CodigoEstadoPlanManejo";
import { UsuarioModel } from "../../../model/seguridad/usuario";
import { Perfiles } from "../../../model/util/Perfiles";
import { CodigoEstadoEvaluacion } from "../../../model/util/CodigoEstadoEvaluacion";
import { ResultadoEvaluacionTitularSharedComponent } from "src/app/shared/components/resultado-evaluacion-titular/resultado-evaluacion-titular-shared.component";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
@Component({
  selector: "bandeja-pmfi",
  templateUrl: "./bandeja-pmfi.component.html",
  styleUrls: ["./bandeja-pmfi.component.scss"],
})
export class BandejaPmfi implements OnInit {
  f!: FormGroup;
  planes: any[] = [];

  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;

  CodigoEstadoMesaPartes = CodigoEstadoMesaPartes;

  PMFI = {
    idTipoProceso: 3, //proceso PMFI
    idTipoEscala: 2,
    idTipoPlan: 5, // PLAN PMFI
    idSolicitud: 1,
    descripcion: "PMFI",
  };

  usuario!: UsuarioModel;

  Perfiles = Perfiles;

  loading = false;
  totalRecords = 0;
  first: number = 0;

  ref!: DynamicDialogRef;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialogService: DialogService,
    private evaluacionService: EvaluacionService
  ) {
    this.f = this.initForm();
    this.usuario = this.user.usuario;
  }

  ngOnInit(): void {
    this.buscar();
  }

  verDetalle(item: any) {
    var params = {
      idPlanManejo: item.idPlanManejo,
      codigoEvaluacion: "PLAN",
    };
    this.evaluacionService
      .listarEvaluacionResumido(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          if (response.data.length != 0) {
            this.ref = this.dialogService.open(
              ResultadoEvaluacionTitularSharedComponent,
              {
                header: "RESULTADO EVALUACIÓN",
                width: "60%",
                contentStyle: { overflow: "auto" },
                data: {
                  item: item,
                },
              }
            );

            this.ref.onClose.subscribe((resp: any) => {
              if (resp) {
                if (resp.tipo == "EVAL") {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );

                  this.router.navigate([
                    "/planificacion/evaluacion/requisitos-previos/" +
                      item.idPlanManejo +
                      "/" +
                      resp.data.codigoEvaluacionDet,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "PMFI"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/formulacion-PMFI/" + item.idPlanManejo,
                  ]);
                }
              }
            });
          } else {
            this.toast.warn("El Plan Seleccionado No Tiene Observaciones");
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  initForm() {
    return this.fb.group({
      dniElaborador: [null],
      rucComunidad: [null],
      nombreElaborador: [null],
      idPlanManejo: [null],
    });
  }

  buscar() {
    this.listarPlanes().subscribe();
  }

  limpiar() {
    this.first = 0;
    this.f.reset();
    this.buscar();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
  }

  nuevoPlan() {
    const body = { ...this.PMFI, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => this.navigate(res.data.idPlanManejo));
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo);
  }

  listarPlanes(page?: Page) {
    let estados = {
      codigoEstado: "",
    };

    if (this.usuario.sirperfil == Perfiles.TITULARTH) {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.COMPLETADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO_MESA_DE_PARTES +
        "," +
        CodigoEstadoPlanManejo.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.APROBADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoPlanManejo.APROBADO_NOTIFICADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR +
        "," +
        CodigoEstadoPlanManejo.REGISTRADO +
        "," +
        CodigoEstadoPlanManejo.PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.RECIBIDO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_DENEGADO +
        "," +
        CodigoEstadoPlanManejo.IMPUGNADO_SAN +
        "," +
        CodigoEstadoPlanManejo.PROCEDE +
        "," +
        CodigoEstadoPlanManejo.NO_PROCEDE +
        "," +
        CodigoEstadoMesaPartes.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoMesaPartes.OBSERVADO_PREREQUISITO +
        "," +
        CodigoEstadoMesaPartes.OBSERVADOMP +
        "," +
        CodigoEstadoMesaPartes.PRESENTADOMDP +
        "," +
        CodigoEstadoMesaPartes.COMPLETADOMP +
        "," +
        CodigoEstadoMesaPartes.COMPLETADO_MED_CORREC +
        "," +
        CodigoEstadoMesaPartes.COMPLETADO_PREREQUISITO +
        "," +
        CodigoEstadoMesaPartes.OBSERVADO_PREREQUISITO +
        "," +
        CodigoEstadoMesaPartes.PRESENTADO_PREREQUISITO;
    } else if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.COMPLETADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO_MESA_DE_PARTES +
        "," +
        CodigoEstadoPlanManejo.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.APROBADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoPlanManejo.APROBADO_NOTIFICADO;
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.BORRADOR +
        "," +
        CodigoEstadoPlanManejo.REGISTRADO +
        "," +
        CodigoEstadoPlanManejo.PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.RECIBIDO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_DENEGADO;
    } else {
      estados.codigoEstado =
        CodigoEstadoPlanManejo.COMPLETADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO_MESA_DE_PARTES +
        "," +
        CodigoEstadoPlanManejo.EN_EVALUACION +
        "," +
        CodigoEstadoPlanManejo.APROBADO +
        "," +
        CodigoEstadoPlanManejo.OBSERVADO +
        "," +
        CodigoEstadoPlanManejo.APROBADO_NOTIFICADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR +
        "," +
        CodigoEstadoPlanManejo.REGISTRADO +
        "," +
        CodigoEstadoPlanManejo.PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.RECIBIDO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO +
        "," +
        CodigoEstadoPlanManejo.BORRADOR_DENEGADO;
    }

    const r = { ...this.PMFI, ...this.f.value, ...estados };
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(tap((res) => (this.planes = res.data)));
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body).pipe(
      tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE),
      })
    );
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiPlanManejo
      .filtrar(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  navigate(idPlan: number) {
    const uri = "planificacion/formulacion-PMFI";
    this.router.navigate([uri, idPlan]);
  }

  registroImpugnacionEmiter(event: any) {
    this.limpiar();
  }
}
