import { Component, OnInit } from "@angular/core";
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-enviar-respuesta',
  templateUrl: './modal-enviar-respuesta.component.html',
})
export class ModalEnviarRespuestaComponent implements OnInit {

  listadoAdjuntos: any[] = [];
  lstContrato: any = {};

  constructor(
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    public ref: DynamicDialogRef
  ) {}

  ngOnInit(): void {
    // console.log(config);
    // throw new Error("Method not implemented.");
  }

  enviar = () => {

  }

  descargarDocumento = () => {

  }


}
