import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContratoConcesionModel } from 'src/app/model/util/dataDemoMPAFPP';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalEnviarRespuestaComponent } from './modal-enviar-respuesta/modal-enviar-respuesta.component';
import { MessageService} from 'primeng/api';

@Component({
  selector: 'app-bandeja-contrato-concesion-pfdm',
  templateUrl: './bandeja-contrato-concesion-pfdm.component.html',
  styleUrls: ['./bandeja-contrato-concesion-pfdm.component.scss']
})
export class BandejaContratoConcesionPfdmComponent implements OnInit {

  tipoUsuario:string = 'S';
  dataTable:any[] = [];
  ref!: DynamicDialogRef;

  constructor(
    private router: Router,
    private messageService: MessageService,
    public dialogService: DialogService
  ) { }

  ngOnInit(): void {
    let datoStr:any =  localStorage.getItem('contratosconcesiones');
    if(!datoStr)
    {
      this.dataTable = [{id : '99G5R2', estado: 'publicado', creado: new Date(), nombrePersona: 'persona', zona:'zona', putaje: '10' }];
    }else{
      this.dataTable = JSON.parse(datoStr);
    }
  }

  revisar(data:ContratoConcesionModel)
  {
    data.tipoUsuario = this.tipoUsuario;
    this.router.navigateByUrl('/concesion/solicitud-contrato-concesion', { state: { data: data } });
  }

  evalTecnica()
  {
    this.router.navigateByUrl('/planificacion/evaluacion-contrato-concesion', { state: { data: {} } });
  }

  verExpediente = () => {
    this.ref = this.dialogService.open(ModalEnviarRespuestaComponent, {
      header: 'Enviar Respuesta',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        // item: item,
        // typeModal: type,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp.success) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: resp.mensaje,
        });
        // this.listarContrato();
      }
    });
  };

}
