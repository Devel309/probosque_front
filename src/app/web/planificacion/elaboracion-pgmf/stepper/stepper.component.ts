//declare const shp : any;
import { Component, Input, ViewChild, ElementRef, Renderer2, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, NgControlStatus } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from "@angular/cdk/stepper";
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { MessageService } from 'primeng/api';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import GeoJSONLayer from '@arcgis/core/layers/GeoJSONLayer';

import * as geometryEngine from '@arcgis/core/geometry/geometryEngine';
import Legend from '@arcgis/core/widgets/Legend';
import Color from '@arcgis/core/Color';
import GeometryService from '@arcgis/core/tasks/GeometryService';
import AreasAndLengthsParameters from '@arcgis/core/tasks/support/AreasAndLengthsParameters';
import Polygon from '@arcgis/core/geometry/Polygon'

import SimpleMarkerSymbol from '@arcgis/core/symbols/SimpleMarkerSymbol';
import SimpleLineSymbol from '@arcgis/core/symbols/SimpleLineSymbol';
import SimpleFillSymbol from '@arcgis/core/symbols/SimpleFillSymbol';
import SimpleRenderer from '@arcgis/core/renderers/SimpleRenderer';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
declare const shp: any;
declare const modern: any;

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true }
  }, MessageService],
  encapsulation: ViewEncapsulation.None
})
export class StepperComponent implements OnInit, OnDestroy {
  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild('ulResult', { static: true }) private ulResult!: ElementRef;
  @ViewChild('areaTotal', { static: true }) private areaTotal!: ElementRef;
  @ViewChild('tablaAreaManejoDividido', { static: true }) private tablaAreaManejoDividido!: ElementRef;
  @ViewChild('fileInput', { static: true }) private fileInput!: ElementRef;
  isLinear = true;
  formInfoGeneral: FormGroup | any;
  formInfoBascica: FormGroup | any;
  public _files: [] = [];
  public view: any = null;
  public _shapefile: any = null;
  public geoJsonLayer: any = null;
  public _id = StepperComponent.Guid2.newGuid;
  public titleAreaManejo = "PGMF";
  public titleVerticeAreaManejo = "";
  public filFile: any = null;
  listCoordinatesAnexo1: any = [];
  constructor(private formBuilder: FormBuilder, private renderer: Renderer2, private serviceGeoforestal: ApiGeoforestalService, private messageService: MessageService,
    private serviceExternos: ApiForestalService) {
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '370px';
    container.style.width = '400px';
    const map = new Map({
      basemap: "hybrid"
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6
    });
    this.view = view;
    /*     view.when().then(function() {
          var legend = new Legend({
            view: view
          });
          view.ui.add(legend, "bottom-right");
        }); */

  }
  domConfig() {
    this.filFile = this.fileInput.nativeElement;
    this.filFile.hide2();
    this.filFile.accept = '.zip';
  }
  ngOnInit(): void {
    this.domConfig();
    this.initializeMap();
    this.soapRequest();
    this.formInfoGeneral = this.formBuilder.group({

    });
    this.formInfoBascica = this.formBuilder.group({
    });
  }
  onUpload() {
    var btnCargarShapeFile = document.getElementById("btnCargaShapeFile");
  }
  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    this.filFile = e.srcElement.files;
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        let i = 0;
        while (i < e.srcElement.files.length) {
          this.processFile(e.srcElement.files[i])
          i++;
        }
      }
    }
  }
  processFile(file: any) {
    let controls = this.validateFileInput();
    
    if (controls.success === false) {
      this.messageService.add({ key: 'toast', severity: 'warn', summary: "", detail: controls.message });
    } else {
      let item = file;
      try {
        let promise = Promise.resolve([]);
        promise = this.loadShapeFile(item);
        promise.then(data => {
          
          this.createLayers(data);
          this.calculateArea(data);
        }).catch(error => {
          throw error;
        });
      } catch (error) {
        console.log(error);
      }
    }
  }
  validateFileInput() {
    let controls = {
      message: '',
      success: true
    }
    
    if (this.filFile[0] === undefined || this.filFile[0].length === 0) {
      controls.message = "Seleccione un archivo";
      controls.success = false;
    }
    else if (this.filFile[0].name.endsWith(".zip") === false) {
      controls.message = "Seleccione un archivo .zip";
      controls.success = false;
    }
    return controls;
  }
  getItem(item: any) {
    let promise = Promise.resolve(item);
    if (typeof item === "string")
      promise = fetch(item).then(data => { return data.blob(); });
    return promise;
  }
  loadShapeFile(item: any) {
    return this.getItem(item).then(data => {
      return new Promise((resolve, reject) => {
        let fileReader = new FileReader();
        fileReader.onload = (e: any) => {
          resolve(e.target.result);
        };
        fileReader.onerror = fileReader.onabort = reject;
        fileReader.readAsArrayBuffer(data);
      });
    }).then(data => {
      return shp(data);
    }).then(data => {
      if (Array.isArray(data) === false)
        data = [data];
      data.forEach((t: any) => t.title = t.fileName);
      return data;
    }).catch(error => {
      throw error;
    });
  }
  static get Guid2() {
    return class Guid2 {
      constructor() { }
      static get newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
          var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });

      }
    }
  }
  random() {
    var length = 6;
    var chars = '0123456789ABCDEF';
    var hex = '#';
    while (length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }
  createLayers(layers: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.random();
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`
        }
      };
      this.createLayer(t);
      //this.validate(t);
    });
    /*var url = "https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_110m_admin_1_states_provinces_shp.geojson";
    fetch(url).then(data => data.json()).then(t =>{
      this.createLayer(t);
    });*/
  }
  validate(item: any) {
    let geometry = item.features[0]
    let params = { "geometria": { "poligono": geometry.geometry.coordinates } };
    let demo = {
      "geometria": {
        "poligono": [[[-71.91650390625, -9.914743843241718], [-
          71.883544921875, -10.228437266155943], [-71.641845703125, -10.196000424383548], [-
            71.619873046875, -9.925565912405494], [-71.91650390625, -9.914743843241718]]]
      }
    }
    this.serviceExternos.validarSuperposicionAprovechamiento(demo).subscribe((result: any) => {
      if (result.dataService.data.nroCapasEvaluadas > 0) {
        if (result.dataServicedata.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.map((capa: any) => {
            if (capa.seSuperpone === "Si") {
              capa.geoJson.crs.type = "name";
              capa.geoJson.fileName = capa.nombreCapa;
              capa.geoJson.opacity = 0.2;
              capa.geoJson.color = this.random();
              this.createLayer(capa.geoJson);
            }
          });
        }
      }
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: "Es una demo"
    }
    this.geoJsonLayer = new GeoJSONLayer({
      url: url,
      outFields: ["*"],
    });
    
    this.geoJsonLayer.visible = true;
    this.geoJsonLayer.id = StepperComponent.Guid2.newGuid;
    this.geoJsonLayer.ID = this.geoJsonLayer.id;
    this.geoJsonLayer.title = item.title;
    this.geoJsonLayer.layerType = 'vector';
    this.geoJsonLayer.groupId = this._id;
    this.geoJsonLayer.color = item.color;
    this.geoJsonLayer.opacity = item.opacity;
    this.geoJsonLayer.attributes = item.features;
    this.geoJsonLayer.when((data: any) => {
      URL.revokeObjectURL(url);
      this.changeLayerStyle(this.geoJsonLayer.id, this.geoJsonLayer.color);
      this.view.goTo({ target: data.fullExtent });
      this.createTreeLayers();
    }).catch((error: any) => {
      console.log(error);
    });
    this.view.map.add(this.geoJsonLayer);

  }
  changeLayerStyle(id: any, color: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer === null) return;
    layer.color = color;
    let type = layer.geometryType;
    let style: any = null;
    
    if (type === "point" || type === "multipoint") {
      style = new SimpleMarkerSymbol();
      style.size = 10;
    }
    else if (type === "polyline") {
      style = new SimpleLineSymbol();
      style.width = 2;
    }
    else if (type === "polygon" || type === "extent") {
      style = new SimpleFillSymbol();
      style.outline = new SimpleLineSymbol();
      style.outline.width = 2;
    }
    style.color = Color.fromHex(color);
    layer.renderer = new SimpleRenderer({ symbol: style });

  }
  getLayers() {
    let layers = this.view.map.allLayers.filter((t: any) => t.groupId === this._id);;
    return layers;
  }
  createTreeLayers() {
    let layers = this.getLayers();
    let ulResult = this.ulResult.nativeElement;
    while (ulResult.firstChild)
      ulResult.removeChild(ulResult.firstChild);
    layers.forEach((t: any) => {
      let li = this.renderer.createElement('li');
      //li.style.backgroundColor = t.color;
      this.renderer.addClass(li, 'list-group-item');
      this.renderer.addClass(li, 'p-2');
      this.renderer.appendChild(ulResult, li);
      let span = this.renderer.createElement('span');

      this.renderer.addClass(span, 'float-right');
      this.renderer.setAttribute(span, 'id', t.id);
      this.renderer.appendChild(li, span);
      let i = this.renderer.createElement('i');
      this.renderer.addClass(i, 'pi');
      this.renderer.addClass(i, 'pi-times-circle');
      this.renderer.appendChild(span, i);

      let chk = this.renderer.createElement('input');
      this.renderer.setAttribute(chk, 'id', t.id);
      this.renderer.setProperty(chk, 'type', 'checkbox');
      chk.checked = true;
      this.renderer.appendChild(li, chk);
      chk.addEventListener("change", (e: any) => {
        this.toggleLayer(e.currentTarget.id, e.currentTarget.checked)
      });
      let text = this.renderer.createElement('label');
      text.style.color = t.color;
      text.textContent = t.title;
      span.addEventListener("click", (e: any) => {
        this.onElementRemoveClick(e)
      });
      this.renderer.appendChild(li, text);
      if (t.title === "VERTICES vert_pgmf") {
        this.createTablaAnexo(t.attributes);
      } else if (t.title === "PGMF") {

      } else if (t.title === "VERTICES PARCELAS DE CORTA - vert_pc") {
        this.createTablaConAnexo(t.attributes);
      }
    });
  }
  createTablaAnexo(items: any) {
    this.listCoordinatesAnexo1 = [];
    items.forEach((t: any) => {
      this.listCoordinatesAnexo1.push({
        "vertice": t.properties.vertice,
        "este": t.properties.este,
        "norte": t.properties.norte
      });
    });
  }
  createTablaConAnexo(items: any) {
    var table = this.tablaAreaManejoDividido.nativeElement;
    var thead = table.querySelector('thead');
    var tbody = table.querySelector('tbody');
    tbody.empty();
    let groups = items.groupBy((t: any) => t.properties.anexo);
    
    let k = 0;
    groups.forEach((t: any, i: any) => {
    
      let tr = tbody.insertRow(k);
      let td = tr.insertCell(0);
      td.textContent = t.key;
      td.rowSpan = t.value.length;
      td.classList.add('align-middle');
      tr.insertCell(1).textContent = t.value[0].properties.vertice;
      tr.insertCell(2).textContent = t.value[0].properties.este;
      tr.insertCell(3).textContent = t.value[0].properties.norte;
      tr.insertCell(4).innerHTML = `<input type="text" class="form-control bg-gray"
  placeholder="Referencia" />`;
      t.value = t.value.slice(1);
      k++;
      t.value.forEach((t2: any, j: any) => {
        let tr = tbody.insertRow(k);
        tr.insertCell(0).textContent = t2.properties.vertice;
        tr.insertCell(1).textContent = t2.properties.este;
        tr.insertCell(2).textContent = t2.properties.norte;
        tr.insertCell(3).innerHTML = `<input type="text" class="form-control bg-gray"
  placeholder="Referencia" />`;
        k++;
      });
    });
  }
  createTablaConAnexo2(items: any) {
    var tablaAreaManejoDividido = this.tablaAreaManejoDividido.nativeElement;
    var thead = tablaAreaManejoDividido.querySelector('thead');
    var tbody = tablaAreaManejoDividido.querySelector('tbody');
    while (tbody.firstChild)
      tbody.removeChild(tbody.firstChild);

    if (items.length > 0) {
      items.forEach((t: any, i: any) => {
        let tr = tbody.insertRow(i);
        tr.dataset.guid = t.properties.vertice;
        tr.insertCell(0).textContent = t.properties.anexo;
        tr.insertCell(1).textContent = t.properties.vertice;
        tr.insertCell(2).textContent = t.properties.este;
        tr.insertCell(3).textContent = t.properties.norte;
        let td = tr.insertCell(4);
        td.innerHTML = `<input type="text" class="form-control bg-gray"
      placeholder="Referencia" />`;

      });
    } else {
      let tr = tbody.insertRow(0);
      let td = tr.insertCell(0);
      td.colSpan = thead.querySelectorAll(':scope > tr > th').length;
      td.classList.add("text-center");
      td.innerHTML = 'No se han encontrado registros';
    }

    var values = tbody.querySelectorAll(':scope tr>td:first-of-type');
    
    var run = 1
    
    for (var i = values.length - 1; i > -1; i--) {
      if (i > 0) {
        var text1 = values[i].textContent;
        var text2 = values[i - 1].textContent;
        if (text1 === text2) {
          
          values[i].remove()
          run++
        } else {
          
          values[i].rowSpan = run;
          run = 1
        }
      }
    }

  }
  removeLayer(id: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null) {
      this.view.map.layers.remove(layer);
      layer.visible = false;
      layer.forRemove = true;
    }
  }
  toggleLayer(id: any, visible: any) {
    let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null)
      layer.visible = visible;
  }
  calculateArea(data: any) {
    if (data[0].title === "PGMF") {
      var geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = data[0].features[0].geometry.coordinates;
      
      var area = geometryEngine.geodesicArea(geometry, "hectares");
      this.areaTotal.nativeElement.value = area;
    }
  }
  onElementRemoveClick(e: any) {
    this.removeLayer(e.currentTarget.id);
    this.createTreeLayers();
  }
  ngOnDestroy(): void {
    if (this.view) {
      // destroy the map view
      this.view.destroy();
    }
  }
  soapRequest() {
    var str = '<?xml version="1.0" encoding="utf-8"?>' + `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://soa.osce.gob.pe/rnp04/GestionarProveedorRNP/service">
    <soapenv:Header/>
    <soapenv:Body>
       <ser:obtenerProveedorVigente>
          <!--Optional:-->
          <input>
             <!--Optional:-->
             <ruc>20574746966</ruc>
          </input>
       </ser:obtenerProveedorVigente>
    </soapenv:Body>
 </soapenv:Envelope>`;
    var xhr = this.createCORSRequest("POST", "http://ws3.pide.gob.pe/services/OsceVigente?wsdl");
    if (!xhr) {
      
      return;
    }
    var results = null;
    xhr.onload = function () {
      results = xhr.responseText;
    }

    xhr.setRequestHeader('Content-Type', 'text/xml');
    xhr.send(str);
    let demo = this.xml2json(results);
    
  }
  
  createCORSRequest(method: any, url: any) {
    var xhr:any = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, false);
    }else {
      
      alert("CORS not supported");
      xhr = null;
    }
    return xhr;
  }
  xml2json(doc:any) {
		if (typeof (doc) === "string")
			doc = new DOMParser().parseFromString(doc, "application/xml");
		let rename = (name:any) => {
			return String(name || '').replace(/-/g, "_").replace(/(.*):/, '');
		};
		const children = [...doc.children];
		if (!children.length) return doc.innerHTML
		const jsonResult = Object.create(null);
		const childIsArray = (x:any, y:any) => x.filter((z:any) => rename(z.nodeName) === y.simpleName).length > 1;
		let index = -1;
		for (const child of children) {
			index = -1;
			child.simpleName = rename(child.nodeName);
			if (!childIsArray(children, child))
				jsonResult[child.simpleName] = this.xml2json(child);
			else {
				index = 0;
				if (jsonResult[child.simpleName] !== undefined)
					index = jsonResult[child.simpleName].push(this.xml2json(child)) - 1;
				else
					jsonResult[child.simpleName] = [this.xml2json(child)];
			}
			let attributes = child.attributes;
			if (attributes.length > 0) {
				let childd = jsonResult[child.simpleName];
				if (index > -1) {
					if (typeof (childd[index]) !== "object")
						childd[index] = { value: childd[index] };
					for (let i = attributes.length - 1; i >= 0; i--) {
						childd[index][attributes[i].name] = attributes[i].value;
					}
				} else {
					if (typeof (childd) !== "object")
						jsonResult[child.simpleName] = { value: childd };
					for (let i = attributes.length - 1; i >= 0; i--) {
						jsonResult[child.simpleName][attributes[i].name] = attributes[i].value;
					}
				}
			}
		}
		return jsonResult;
	}
}
