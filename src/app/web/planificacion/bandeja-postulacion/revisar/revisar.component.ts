import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ConvertNumberToDate, DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { ValidarCondicionMinimaComponent } from 'src/app/shared/components/validar-condicion-minima/validar-condicion-minima.component';
import { ModalRecepcionComponent } from '../modal-recepcion/modal-recepcion.component';

@Component({
  selector: 'app-revisar',
  templateUrl: './revisar.component.html',
  styleUrls: ['./revisar.component.scss'],
})
export class RevisarComponent implements OnInit {
  anexo1Status: string = '';
  anexo2Status: string = '';
  anexo3Status: string = '';
  anexo4Status: string = '';
  declaraJuradaStatus: string = '';
  otrosAnexoStatus: string = '';

  idAnexo1Status: number = 0;
  idAnexo2Status: number = 0;
  idAnexo3Status: number = 0;
  idAnexo4Status: number = 0;
  idDeclaraJuradaStatus: number = 0;
  idOtrosAnexoStatus: number = 0;


  idStatusAnexo: number = 0;
  idStatusAnexoTerminado: number = 0;
  idCargarResolucion: number = 0;

  IdProcesoPostulacion: number = 0;
  idEstatusProceso: number = 0;
  observacionARFFS!: string;
  ref?: DynamicDialogRef;

  fechaRecepcionDocumentos: any = null;
  numeroTramite: any = null;

  showConfirm: boolean = false;
  showConfirmObservado: boolean = false;

  usuario!: UsuarioModel;
  perfil = Perfiles;
  isArffs: boolean = false;
  observadoArffs: boolean = false;
  isTitular: boolean = false;
  habilitarTextArea: boolean = false;
  habilitarAprobarSol: boolean = false;
  habilitarObservarSol: boolean = false;
  habilitarRechazarSol: boolean = false;
  habilitaBtnObservar: boolean=false;
  disabledValidarAnexo: boolean = false;
  disabledDatosCancelacion: boolean = false;

  constructor(
    private confirmationService: ConfirmationService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private messageService: MessageService,
    private router: Router,
    private anexoService: AnexoService,
    private serv: PlanificacionService,
    public dialogService: DialogService,
    private usuarioServ: UsuarioService,
    public dialog: MatDialog,
    private procesoPostulaionService: ProcesoPostulaionService,
    private toast: ToastService,
  ) {
    this.idEstatusProceso = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idEstatusProceso;
    this.fechaRecepcionDocumentos = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).fechaRecepcionDocumentos;
    if(this.fechaRecepcionDocumentos != null){
      this.fechaRecepcionDocumentos = ConvertNumberToDate(this.fechaRecepcionDocumentos);
    }
    this.numeroTramite = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).numeroTramite;
    
    this.refrescarRevision();
  }

  ngOnInit() {
   
    this.usuario = this.usuarioServ.usuario;
    this.isArffs = this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL ? true : false;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;

    this.disabledValidarAnexo=this.idEstatusProceso == 32  || this.idEstatusProceso == 33? true:false;
    this.disabledDatosCancelacion= this.usuario.sirperfil !== this.perfil.AUTORIDAD_REGIONAL || this.idEstatusProceso == 33? true : false;
    
    this.obtenerStatusAnexo();
  }


  refrescarRevision = () => {
    this.IdProcesoPostulacion = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion;
    this.idEstatusProceso = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idEstatusProceso;

    this.observacionARFFS = JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).observacionARFFS;

      this.observadoArffs = JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).observado;

      this.refrescarHabilitar()
  };

  refrescarHabilitar(){

    if(this.isArffs){

      if(this.observadoArffs == null){
        // if(this.idEstatusProceso != 30){
        //   this.habilitarObservarSol = true
        // }
        this.habilitarObservarSol = false;
        this.habilitarAprobarSol = false;
        this.habilitarRechazarSol = false;

      }
      if(this.observadoArffs == true){
        this.habilitarAprobarSol = true;
        this.habilitarRechazarSol = true;
        this.habilitarObservarSol = false;

      }
      if(this.observadoArffs == false){
        this.habilitarObservarSol = false;
        this.habilitarAprobarSol = false;
        this.habilitarRechazarSol = false;
      }

      if(this.idEstatusProceso != 34 && this.idEstatusProceso != 31){
        this.habilitarTextArea = true


      }

    }
  }


  obtenerStatusAnexo = () => {

    //this.habilitaBtnObservar=false;
    this.habilitarObservarSol = false

    let params = {
      codigoAnexo: null,
      idAnexoDetalle: null,
      idDocumentoAdjunto: null,
      idProcesoPostulacion: this.IdProcesoPostulacion,
      idStatusAnexo: null,
      idTipoDocumento: null,
      idUsuarioCreacion: null,
      idUsuarioModificacion: null
      //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    };

    this.anexoService.obtenerEstatusAnexo(params).subscribe((result: any) => {
      if (result.isSuccess) {
        this.idStatusAnexo = 0;
        this.idStatusAnexoTerminado = 0;
        result.data.forEach((element: any) => {
          if (element.codigoAnexo === 'anexo1') {
            this.idAnexo1Status = element.idAnexoDetalleStatus;
            this.anexo1Status = element.descripcionStatus;
            if (element.idStatusAnexo === 20)
              this.idStatusAnexo = this.idStatusAnexo + 1;
            if (element.idStatusAnexo === 22)
              this.idStatusAnexoTerminado = this.idStatusAnexoTerminado + 1;
          } else if (element.codigoAnexo === 'anexo2') {
            this.idAnexo2Status = element.idAnexoDetalleStatus;
            this.anexo2Status = element.descripcionStatus;
            if (element.idStatusAnexo === 20)
              this.idStatusAnexo = this.idStatusAnexo + 1;
            if (element.idStatusAnexo === 22)
              this.idStatusAnexoTerminado = this.idStatusAnexoTerminado + 1;
          } else if (element.codigoAnexo === 'anexo3') {
            this.idAnexo3Status = element.idAnexoDetalleStatus;
            this.anexo3Status = element.descripcionStatus;
            if (element.idStatusAnexo === 20)
              this.idStatusAnexo = this.idStatusAnexo + 1;
            if (element.idStatusAnexo === 22)
              this.idStatusAnexoTerminado = this.idStatusAnexoTerminado + 1;
          } else if (element.codigoAnexo === 'anexo4') {
            this.idAnexo4Status = element.idAnexoDetalleStatus;
            this.anexo4Status = element.descripcionStatus;
            if (element.idStatusAnexo === 20)
              this.idStatusAnexo = this.idStatusAnexo + 1;
            if (element.idStatusAnexo === 22)
              this.idStatusAnexoTerminado = this.idStatusAnexoTerminado + 1;
          } else if (element.codigoAnexo === 'declaracionjurada') {
            this.idDeclaraJuradaStatus = element.idAnexoDetalleStatus;
            this.declaraJuradaStatus = element.descripcionStatus;
            if (element.idStatusAnexo === 20)
              this.idStatusAnexo = this.idStatusAnexo + 1;
            if (element.idStatusAnexo === 22)
              this.idStatusAnexoTerminado = this.idStatusAnexoTerminado + 1;
          } else if (element.codigoAnexo === 'otrosanexos') {
            this.idOtrosAnexoStatus = element.idAnexoDetalleStatus;
            this.otrosAnexoStatus = element.descripcionStatus;
          }

          if(element.descripcionStatus=='Observado'){
            //this.habilitaBtnObservar=true;
            this.habilitarObservarSol = true
          }
        });



      }
    });
  };


  validarCondiciones(){
    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: 'Validar Condiciones Mínimas',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        idCondicionMinima: 0
      },
    });
  }

  openAprobarSolicitud(event: Event){

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Este proceso es irreversible, ¿desea continuar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.aprobar()
      },
      reject: () => {
        //reject action
      }
    });

  }

  aprobar (){


    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      /*idStatus:
        JSON.parse('' + localStorage.getItem('bandejapostulacionrevisar'))
          .posicion === 0
          ? 2
          : 8,*/
      idStatus: 34,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {


          let paramsObs = {
            idProcesoPostulacion: JSON.parse(
              '' + localStorage.getItem('bandejapostulacionrevisar')
            ).idProcesoPostulacion,
            idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
              .idusuario,
            observado: false,
            observacionARFFS: this.observacionARFFS
          };
          this.procesoPostulaionService
          .actualizarObservacionARFFSProcesoPostulacion(paramsObs)
          .subscribe((resp: any) => {
            if (resp.success) {


            } else {
              this.messageService.add({
                key: 'an1',
                severity: 'error',
                summary: 'ERROR',
                detail: 'Ocurrió un problema, intente nuevamente',
              });
            }
          });



          this.messageService.add({
            key: 'tl',
            severity: 'success',
            summary: 'CORRECTO',
            detail: 'Se aprobó la solicitud correctamente',
          });

          this.router.navigate(['planificacion/bandeja-postulacion']);
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });

  };

  enviarValidacion = () => {
    let recepcionDocumentos = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).recepcionDocumentos;

    if (!recepcionDocumentos) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe enviar la recepción de los documentos',
      });
    } else {
      let params = {
        correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
        idProcesoPostulacion: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoPostulacion,
        idStatus: 5,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
      };

      this.ampliacionSolicitudService
        .actualizarEstadoProcesoPostulacion(params)
        .subscribe((resp: any) => {
          if (resp.success) {
            this.messageService.add({
              key: 'tl',
              severity: 'success',
              summary: 'CORRECTO',
              detail: 'Se actualizó correctamente',
            });
            this.router.navigate(['planificacion/bandeja-postulacion']);
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });
    }
  };

  enviarCerrarConfim(){
    var _this = this;
    this.showConfirm = false;
    _this.router.navigate(['planificacion/bandeja-postulacion']);
  }
  enviarCerrarConfimObservado(){
    var _this = this;
    this.showConfirmObservado = false;
    _this.router.navigate(['planificacion/bandeja-postulacion']);
  }

  postular = () => {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 14,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.showConfirm = true;
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  enviarSubsanacion = () => {
    if(!this.declaraJuradaStatus || !this.anexo1Status || !this.anexo2Status || !this.anexo3Status || 
      !this.anexo4Status || !this.otrosAnexoStatus) {
        this.toast.warn("Debe completar todas las secciones.");
        return;
    }
    
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: this.idEstatusProceso === 4 ? 14 : 5,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          this.showConfirmObservado = true;
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
      let paramsObs = {
        idProcesoPostulacion: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoPostulacion,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        observado: false,
        observacionARFFS: this.observacionARFFS
      };
      this.procesoPostulaionService
      .actualizarObservacionARFFSProcesoPostulacion(paramsObs)
      .subscribe((resp: any) => {
        if (resp.success) {


        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  openRechazarSolicitud(event: Event){

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Este proceso es irreversible, ¿desea continuar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.rechazarSolicitud()
      },
      reject: () => {
        //reject action
      }
    });

  }

  rechazarSolicitud() {
    let params = {
      correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
      idProcesoPostulacion: JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion,
      idStatus: 31,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.ampliacionSolicitudService
      .actualizarEstadoProcesoPostulacion(params)
      .subscribe((resp: any) => {
        if (resp.success) {
          //this.showConfirm = true;

          let paramsObs = {
            idProcesoPostulacion: JSON.parse(
              '' + localStorage.getItem('bandejapostulacionrevisar')
            ).idProcesoPostulacion,
            idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
              .idusuario,
            observado: false,
            observacionARFFS: this.observacionARFFS
          };
          this.procesoPostulaionService
          .actualizarObservacionARFFSProcesoPostulacion(paramsObs)
          .subscribe((resp: any) => {
            if (resp.success) {


            } else {
              this.messageService.add({
                key: 'an1',
                severity: 'error',
                summary: 'ERROR',
                detail: 'Ocurrió un problema, intente nuevamente',
              });
            }
          });
        this.messageService.add({
          key: 'dc',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se rechazó la solicitud correctamente.',
        });

        this.router.navigate(['planificacion/bandeja-postulacion']);
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  validar = (esValidado: boolean) => {

    this.refrescarRevision();
    if (esValidado) this.obtenerStatusAnexo();
  };

  recepcionDocumento = () => {
    this.ref = this.dialogService.open(ModalRecepcionComponent, {
      header: 'Recepción de documentos físicos',
      width: '70%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data:{
        fechaRecepcionDocumentos: this.fechaRecepcionDocumentos,
        numeroTramite: this.numeroTramite
      }
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.fechaRecepcionDocumentos = resp.fechaRecepcionDocumentos;
        this.numeroTramite = resp.numeroTramite;
        this.guardaProcesoPostulacion();
      }
    });
  };

  guardaProcesoPostulacion = () => {
    let params = {
      anexo1: null,
      anexo2: null,
      anexo3: null,
      idProcesoPostulacion: this.IdProcesoPostulacion,
      idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      procesopostulacion: {
        fechaRecepcionDocumentos: this.fechaRecepcionDocumentos,
        numeroTramite: this.numeroTramite,
        idEstatusProceso: null,
        idProcesoPostulacion: this.IdProcesoPostulacion,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        recepcionDocumentos: true,
      },
    };
    ;
    this.serv.guardaProcesoPostulacion(params).subscribe((result: any) => {
      ;
      if (result.success) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se actualizó correctamente',
        });

        let bandeja = JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        );
        bandeja.fechaRecepcionDocumentos = this.fechaRecepcionDocumentos;
        bandeja.numeroTramite = this.numeroTramite;
        bandeja.recepcionDocumentos = true;
        localStorage.setItem(
          'bandejapostulacionrevisar',
          JSON.stringify(bandeja)
        );

        this.refrescarRevision();
        this.obtenerStatusAnexo();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  validarGuardar(e: boolean){

    if(e){
      this.observadoArffs = true;

      let paramsObs = {
        idProcesoPostulacion: JSON.parse(
          '' + localStorage.getItem('bandejapostulacionrevisar')
        ).idProcesoPostulacion,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        observado: this.observadoArffs,
        observacionARFFS: this.observacionARFFS
      };
      this.procesoPostulaionService
      .actualizarObservacionARFFSProcesoPostulacion(paramsObs)
      .subscribe((resp: any) => {
        if (resp.success) {

          this.refrescarHabilitar()
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
    }
  }


  btObservar(){

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.anexoService.obtenerUsuarioPostulante({
      "idProcesoPostulacion": JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion
    }).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {

          let params = {
            correoPostulante: result.data.correoElectronico,
            idProcesoPostulacion: JSON.parse(
              '' + localStorage.getItem('bandejapostulacionrevisar')
            ).idProcesoPostulacion,
            idStatus: 30,
            idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
              .idusuario,
          };

          this.ampliacionSolicitudService
            .actualizarEstadoProcesoPostulacion(params)
            .subscribe((resp: any) => {
              if (resp.success) {
                this.messageService.add({
                  key: 'an1',
                  severity: 'success',
                  summary: 'CORRECTO',
                  detail: 'Se observó la solicitud correctamente',
                });
                //this.router.navigate(['planificacion/bandeja-postulacion']);
              } else {
                this.messageService.add({
                  key: 'an1',
                  severity: 'error',
                  summary: 'ERROR',
                  detail: 'Ocurrió un problema, intente nuevamente',
                });
              }
            });

            let paramsObs = {
              idProcesoPostulacion: JSON.parse(
                '' + localStorage.getItem('bandejapostulacionrevisar')
              ).idProcesoPostulacion,
              idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
                .idusuario,
              observado: null,
              observacionARFFS: this.observacionARFFS
            };
            this.procesoPostulaionService
            .actualizarObservacionARFFSProcesoPostulacion(paramsObs)
            .subscribe((resp: any) => {
              if (resp.success) {

                this.router.navigate(['planificacion/bandeja-postulacion']);
              } else {
                this.messageService.add({
                  key: 'an1',
                  severity: 'error',
                  summary: 'ERROR',
                  detail: 'Ocurrió un problema, intente nuevamente',
                });
              }
            });
        } else {
          this.messageService.add({
            key: 'an1',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.messageService.add({
          key: 'an1',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    );

  }

  generarDocumentoCompilado(){

    this.dialog.open(LoadingComponent, { disableClose: true });
    var params = {
      codigoAnexo: 'anexo1',
      idProcesoPostulacion: this.IdProcesoPostulacion
    };
    this.anexoService
      .obtenerUsuarioPostulante(params)
      .subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.data) {
          let d : any = {};
          d.idProcesoPostulacion = this.IdProcesoPostulacion;
          d.nombres = resp.data.nombre;
          d.apellidoPaterno = resp.data.apellidoPaterno;
          d.apellidoMaterno = resp.data.apellidoMaterno;
          d.tipoDocumento = resp.data.tipoDocumento;
          if (resp.data.tipoDocumento === 3) {
            d.tipoDocumento = "DNI";
          } else {
            d.tipoDocumento = "RUC";
          }
         d.nroDocumento = resp.data.numeroDocumento;
         this.generarDocumento(d);
        }
      },
      (error: HttpErrorResponse) => {
        this.errorMensaje(error.message);
        this.dialog.closeAll();
      }
    )
  }


  private generarDocumento(params:any){
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexoService.generarInformeAnexo(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();

        DownloadFile(result.archivo, result.nombeArchivo, '');

      },
      (error: HttpErrorResponse) => {
        this.errorMensaje(error.message);
        this.dialog.closeAll();
      }
    );

  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}
