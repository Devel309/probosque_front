import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DownloadFile, ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';

@Component({
  selector: 'app-modal-documentos',
  templateUrl: './modal-documentos.component.html',
  styleUrls: ['./modal-documentos.component.scss']
})
export class ModalDocumentosComponent implements OnInit {
  listaDocumentos: any = [];
  listaDocumentosTemp: any = [];
  isdisabled: boolean = false;

  codigoTramite!: any;
  fechaRecepcion!: Date;
  fechaRecepcionNula!: Date;
  minDate!: Date;

  tieneObs: boolean | null = null;
  isClickBtnSubsanar: boolean = false;
  isClickBtnEvaluar: boolean = false;
  usuario: any;

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private toast: ToastService,
    private dialog: MatDialog,
    private anexoService: AnexoService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
  ) {
    this.isdisabled = this.config.data.isDisabled;
  }

  ngOnInit(): void {
    this.minDate = new Date(2021, 0, 1);
    this.listarDocumentos();
    this.usuario = { sirperfil: JSON.parse("" + localStorage.getItem("usuario")).sirperfil}
  }

  listarDocumentos() {

    this.procesoPostulaionService.obtenerProcesoPostulacionValidarRequisito(
      { "idProcesoPostulacion": this.config.data.idProcPostulacion })
      .subscribe((result: any) => {
        if (result.success) {

          this.codigoTramite = result.data.numeroTramite;

          if (result.data.fechaRecepcionDocumentos == null) {
          } else {
            this.fechaRecepcion = new Date(result.data.fechaRecepcionDocumentos);
          }

        } else {
          this.toast.warn(result.message);
        }
      });

    const params = { "idProcesoPostulacion": this.config.data.idProcPostulacion };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexoService.listarDocumentos(params).subscribe((result: any) => {
      if (result.success) {
        this.listaDocumentos = result.data.filter((item: any) => item.tipoDocumento !== "--");
        this.validarObservaciones();
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  validarObservaciones() {
    let params = { "idProcesoPostulacion": this.config.data.idProcPostulacion };

    this.procesoPostulaionService.tieneObservacion(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.tieneObs = result.data;
        } else {
          this.toast.warn(result.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  btnDescargar(fila: any) {
    let params = {
      "codigoAnexo": fila.descripcion,
      "idProcesoPostulacion": fila.idProcesoPostulacion,
      "idTipoDocumento": fila.idTipoDocumento
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexoService.obtenerAdjuntos(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.isSuccess && result.data.length > 0) {
          let auxArchivo = result.data[result.data.length - 1];
          DownloadFile(auxArchivo.file, auxArchivo.nombreDocumento, 'application/octet-stream');
        } else {
          this.toast.warn("No se encontró ningun documento");
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  validarGuardar(): boolean {
    let validado = true;

    let hayNulos = this.listaDocumentos.some((item: any) => item.conforme === null);
    let isConformeFalsesinObs = this.listaDocumentos.some((item: any) => (item.conforme === false && !item.observacion));

    if (hayNulos) {
      validado = false;
      this.toast.warn("(*) Existen docuemntos sin evaluar");
    } else if (isConformeFalsesinObs) {
      validado = false;
      this.toast.warn("(*) Debe ingresar las observaciones");
    }

    return validado;
  }

  btnGuardar() {
    if (!this.validarGuardar()) return;

    let params: any = [];
    this.listaDocumentos.forEach((item: any) => {
      let aux = {
        "idDocumentoAdjunto": item.idDocumentoAdjunto,
        "conforme": item.conforme,
        "observacion": item.conforme ? null : item.observacion,
        "idUsuarioModficacion": this.config.data.idUsuario
      }

      params.push(aux);
    });

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexoService.guardarObservacion(params).subscribe((result: any) => {
      if (result.success) {
        let auxTieneObs = this.listaDocumentos.some((item: any) => (item.conforme === false));
        this.actualizarProcesoPostulacion(auxTieneObs);
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarProcesoPostulacion(valor: boolean) {
    let params = {
      "idProcesoPostulacion": this.config.data.idProcPostulacion,
      "tieneObservacion": valor,
      "idUsuarioModificacion": this.config.data.idUsuario,
      //"fechaRecepcionDocumentos": this.fechaRecepcion == null? this.fechaRecepcion : this.fechaRecepcionNula,
      "fechaRecepcionDocumentos": this.fechaRecepcion,
      "numeroTramite": this.codigoTramite
    };

    this.procesoPostulaionService.actualizarProcesoPostulacion(params).subscribe(
      (result: any) => {
        if (result.success) {
          if (this.isClickBtnSubsanar) {
            this.actualizarEstadoSubsanar();
          } else if (this.isClickBtnEvaluar) {
            this.actualizarEstadoEvaluar();
          } else {
            this.dialog.closeAll();
            this.toast.ok(result.message);
            this.listarDocumentos();
          }
        } else {
          this.dialog.closeAll();
          this.toast.warn(result.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  actualizarEstadoSubsanar() {
    this.isClickBtnSubsanar = false;
    const params = { "idProcesoPostulacion": this.config.data.idProcPostulacion };

    this.anexoService.obtenerUsuarioPostulante(params).subscribe((result: any) => {
      if (result.success) {
        const params = {
          "idProcesoPostulacion": this.config.data.idProcPostulacion,
          "idStatus": 4,
          "idUsuarioModificacion": this.config.data.idUsuario,
          "correoPostulante": result.data.correoElectronico
        }
        this.ampliacionSolicitudService.actualizarEstadoProcesoPostulacion(params).subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.toast.ok(result.message);
            this.ref.close(true);
          } else {
            this.toast.warn(result.message);
          }
        }, (error) => this.errorMensaje(error, true));

      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarEstadoEvaluar() {
    this.isClickBtnEvaluar = false;
    const params = {
      "idProcesoPostulacion": this.config.data.idProcPostulacion,
      "idStatus": 5,
      "idUsuarioModificacion": this.config.data.idUsuario,
    }
    this.ampliacionSolicitudService.actualizarEstadoProcesoPostulacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.ref.close(true);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnSubsanar() {
    this.isClickBtnSubsanar = true;
    this.btnGuardar();
  }

  btnEvaluador() {
    this.isClickBtnEvaluar = true;
    this.btnGuardar();
  }

  clickValidarRadio() {
    let auxTodoOk = this.listaDocumentos.every((item: any) => (item.conforme === true));
    if (auxTodoOk) this.tieneObs = false;
    else this.tieneObs = true;
  }

  btnCancelar() {
    this.ref.close()
  }

  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
