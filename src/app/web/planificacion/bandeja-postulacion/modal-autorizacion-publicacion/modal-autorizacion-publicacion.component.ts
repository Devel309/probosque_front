import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DownloadFile} from "../../../../shared/util";
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import {MatDialog} from "@angular/material/dialog";
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { EstatusProceso } from 'src/app/shared/const';
import { ToastService } from '@shared';
import * as moment from 'moment';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-modal-autorizacion-publicacion',
  templateUrl: './modal-autorizacion-publicacion.component.html'
})
export class ModalAutorizacionPublicacionComponent implements OnInit {
  estadoProceso = EstatusProceso;
  autoResize: boolean = true;
  idAutorizacionPublicacionSolicitud: any;
  idProcesoPostulacion: any;
  numeroDocumento: string = '';
  fechaAutorizacion!: Date;

  usuario!: UsuarioModel;

  minDate = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private config: DynamicDialogConfig,
    private procesoPostulacionService: ProcesoPostulaionService,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private toast: ToastService,
  ) {}

  @Input() disabled: boolean = false;

  @Input() archivoSolicitudFirmada: any = {};
  @Input() archivoCarta: any = {};

  @Output() asignarArchivo = new EventEmitter<File>();
  @Output() asignarArchivoResumen = new EventEmitter<File>();
  file!: File;
  fileName:string='';
  fileResumen!: File;
  fileNameResumen:string='';

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    if(this.config.data.item.tieneAutorizacion != null){
      this.obtenerAutorizacion();
    }
  }

  obtenerAutorizacion = () => {
    let params = {
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
      idUsuarioPostulacion: this.config.data.item.idUsuarioPostulacion,
    };

    this.procesoPostulacionService
      .obtenerAutorizacion(params)
      .subscribe((result: any) => {
        // ;
      });
  };

  cargarAchivo(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.file = e.srcElement.files[0];
      }
    }
  }

  adjuntarArchivoCarta(file: any) {
    let params = {
      file: file.file,
      //idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo ? file.idEvaluacionCampoArchivo : 0,
      //idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      //idTipoDocumento: 11,
      //idTipoSeccionArchivo: 2,
      //idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });

    /*
    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje("Se registró el archivo sustento de comunicación correctamente.");
        this.archivoComunicacion = res.body.data;
        file.idEvaluacionCampoArchivo =this.archivoComunicacion.idEvaluacionCampoArchivo;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe()
*/
    this.toast.ok("Se cargó la carta de autorización correctamente");
    this.dialog.closeAll()
  }

  adjuntarSolicitudFirmada(file: any) {
    let params = {
      file: file.file,
      //idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo ? file.idEvaluacionCampoArchivo : 0,
      //idEvaluacionCampo: this.evaluacion.idEvaluacionCampo,
      //idTipoDocumento: 12,
      //idTipoSeccionArchivo: 2,
      //idUsuarioRegistro: this.usuario.idusuario,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
/*
    this.evaluacionServ.registroEvaluacionCampoArchivo(params).pipe(tap((res: any) => {
      if (res.body?.message) {
        this.SuccessMensaje("Se registró el archivo sustento de autoridad correctamente.");
        this.archivoSustento = res.body.data;
        file.idEvaluacionCampoArchivo =this.archivoComunicacion.idEvaluacionCampoArchivo;
      }
    }, error => {
      this.SuccessMensaje("Ocurrió un error");
    })).pipe(finalize(() => this.dialog.closeAll()))
      .subscribe()

    */
    this.toast.ok("Se cargó el resumen de solicitud firmado correctamente");
    this.dialog.closeAll()

  }

  eliminarArchivoCarta(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    /*
    let params = {
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
      idUsuarioRegistro: this.usuario.idusuario,

    }

    this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
      this.SuccessMensaje("Se eliminó el archivo sustento de comunicación correctamente.");
      this.archivoComunicacion = {};
      this.dialog.closeAll();
    })
    */
    this.archivoCarta = {};
    this.toast.ok("Se eliminó la carta de autorización correctamente");
    this.dialog.closeAll();
  }

  eliminarArchivoSolicitudFirmada(file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
/*
    let params = {
      idEvaluacionCampoArchivo: file.idEvaluacionCampoArchivo,
      idUsuarioRegistro: this.usuario.idusuario,

    }

    this.evaluacionServ.eliminarEvluacionCampoArchivo(params).subscribe((result: any) => {
      this.SuccessMensaje("Se eliminó el archivo sustento de autoridad correctamente.");
      this.archivoSustento = {};
      this.dialog.closeAll();
    })

     */
    this.archivoSolicitudFirmada = {};
    this.toast.ok(" Se eliminó el resumen de solicitud firmado correctamente");
    this.dialog.closeAll();
  }


  btnDescargarPlantillaCarta() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.procesoPostulacionService.descargarPlantillaCartaAutorizacion().subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, (error) => this.errorMensaje(error, true));
  }

  onFileSelected(event:any):void {
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.file = event.target.files[0];
    this.asignarArchivo.emit(this.file);
    this.fileName = event.target.files[0].name;
    setTimeout(() => {this.dialog.closeAll();},1000);

  }

  onFileResumenSelected(event:any):void {
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.fileResumen = event.target.files[0];
    this.asignarArchivoResumen.emit(this.fileResumen);
    this.fileNameResumen = event.target.files[0].name;
    setTimeout(() => {this.dialog.closeAll();},1000);

  }

  onDescargarArchivo():void{
    this.dialog.open(LoadingComponent,{ disableClose: true });
    /*this.servSA.descargarArchivoSolicitudAcceso(this.solicitudAcceso.idSolicitudAcceso).subscribe(
      (data:any) => {
        let archive:string =  this.fileName;
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
        this.dialog.closeAll();
      });*/
      this.dialog.closeAll();
  }


  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  validarEnvio = (): boolean => {
    if (this.numeroDocumento === '') {
     this.toast.warn("(*)Debe ingresar el número de documento");
     return false;

    } else if (!this.fechaAutorizacion) {
     this.toast.warn("(*)Debe ingresar la fecha de autorización");
     return false;

    }/*
    else if (this.file === undefined || this.file === null) {
      console.log('entro 2');
      this.toast.warn("Debe adjuntar un archivo");

      return false;
    }
*/
    return true;
  };

  btnCancelar() {
    this.ref.close()
  }

  guardar = ()=>{

    var params = {

      idAutorizacionPublicacionSolicitud: this.idAutorizacionPublicacionSolicitud == null ? 0: this.idAutorizacionPublicacionSolicitud,
      idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
      numeroDocumento: this.numeroDocumento,
      fechaAutorizacion: this.fechaAutorizacion,
      idUsuarioRegistro: this.usuario.idusuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.procesoPostulacionService.guardarAutorizacionPublicacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.success){
        this.toast.ok(result.message);
      }else{
        this.messageService.add({
          key: 'tl',
          severity: 'warn',
          summary: '',
          detail: "No se pudo realizar la autorización de la publicación",
        });
      }
    },()=>{
      this.dialog.closeAll();
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: '',
        detail: 'Ocurrió un problema, intente nuevamente',
      });
    });
  }

  enviar2 = () => {
    if (this.validarEnvio()) {
      let obj1 = {
        Descripcion: this.numeroDocumento,
        file: this.file,
      };
      this.ref.close(obj1);
    }
  };

  enviar = () => {
      let obj1 = {
        NumeroDocumemto: this.numeroDocumento,
        file: this.file,
      };

      let params = {
        idPostulante: this.config.data.item.idUsuarioPostulacion,
        correoPostulante: JSON.parse('' + localStorage.getItem('usuario')).correoElectronico,
        idProcesoPostulacion: this.config.data.item.idProcesoPostulacion,
        idStatus: this.estadoProceso.enPublicacion,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
      };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.procesoPostulacionService.enviarAutorizacionPublicacion(params).subscribe((resp: any) => {

        if (resp.success) {
          this.toast.ok('Se envió la autorización de publicación correctamente');
          this.procesoPostulacionService.enviarAutorizacionPublicacion(params).subscribe((respta: any) => {
            if (respta.success) {
              this.toast.ok(respta.message);
              this.validarPublicacionId();
            } else {
              this.dialog.closeAll();
              this.toast.error(respta.message);
            }
          });

        } else {
          this.dialog.closeAll();
          this.toast.error('Ocurrió un problema. No se pudo enviar la autorización de publicación');
        }
      });

  };

  validarPublicacionId(){
    let idParam ={
      idProcesoPostulacion : this.config.data.item.idProcesoPostulacion
    }
    this.procesoPostulacionService.validarPublicacionId(idParam).subscribe((data: any) => {
      this.dialog.closeAll();
      this.ref.close(true);
    });
  }

  btnGuardar = () => {
    if (this.validarEnvio()) {
      this.guardar();
    }
    /*else{
      this.ref.close();
    }*/
  };

  btnEnviar = () => {
    if (this.validarEnvio()) {
      this.enviar();
    }
    /*else{
      this.ref.close();
    }*/
  };
}
