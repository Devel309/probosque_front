import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ConvertDateToString, ConvertNumberToDate, DownloadFile } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { UsuarioService } from '@services';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
@Component({
  selector: 'app-cargar-resolucion',
  templateUrl: './cargar-resolucion.component.html',
  styleUrls: ['./cargar-resolucion.component.scss'],
})
export class CargarResolucionComponent implements OnInit {

  minDate: Date = new Date();
  fechaResolucion!: Date;
  numeroResolucion!: number;
  @Input() idAnexoStatus!: number;
  @Input() file: any = {};
  @Output() validarAnexo: EventEmitter<boolean> = new EventEmitter<boolean>();
  

  display_validar: boolean = false;
  idArchivoCargado: number=0;



  
  params_obtener: any = {
    codigoAnexo: 'declaracionjurada',
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioPostulacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
  };

  generar: boolean = false;
  archivoEnviado: boolean = false;

  params_adjuntar_anexo: any = {
    codigoanexo: 'declaracionjurada',
    idprocesopostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    idusuarioadjunta: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
  };

  anexoActual: any = null;
  paramsGuardar = new HttpParams();
  lstObservacion: any[] = [];
  ref!: DynamicDialogRef;

  archivoAdjunto:any = {};
  // file!: any;
  isArffs: boolean = false;
  isTitular: boolean = false;
  usuario!: UsuarioModel;
  perfil = Perfiles;

  constructor(
    private serv: PlanificacionService,
    private messageService: MessageService,
    private anexoService: AnexoService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuarioServ: UsuarioService,
    private procesoPostulaionService: ProcesoPostulaionService
  ) {}

  ngOnInit() {
    this.minDate.setDate(this.minDate.getDate() - 365);
    this.usuario = this.usuarioServ.usuario;
    this.isTitular = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;
    this.isArffs = this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL ? true : false;
    this.obtenerAdjuntos();
    this.refrescarResolucion();
    //this.obtenerDetalleObservacion();
  }

  refrescarResolucion(){
    this.numeroResolucion = JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).numeroResolucion;
    if(JSON.parse('' + localStorage.getItem('bandejapostulacionrevisar')).fechaResolucion != null){
      this.fechaResolucion = new Date(JSON.parse('' + localStorage.getItem('bandejapostulacionrevisar')).fechaResolucion)
    }
  }
  
  enviarArchivo(event: any) {

    var params = new HttpParams()
      .set('CodigoAnexo', 'cargarresolucion')
      .set(
        'IdProcesoPostulacion',
        this.params_adjuntar_anexo.idprocesopostulacion
      )
      .set('IdTipoDocumento', '52')
      .set('IdUsuarioAdjunta', this.params_adjuntar_anexo.idusuarioadjunta)
      .set('NombreArchivo', this.archivoAdjunto.nombre)
      .set('idDocumentoAdjunto', '0');
      // .set('NombreArchivo', this.file.name);
      this.paramsGuardar = params;

      this.file.url = URL.createObjectURL(event.file);
      this.file.file = event.file;
      this.file.nombre = event.file.name;

      //this.guardarArchivo();

  }

  eliminarDocumentoAdjunto() {
    this.archivoAdjunto = {};
    var params = {
      idDocumentoAdjunto: this.anexoActual.idDocumentoAdjunto,
      idUsuarioModficacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };

    this.serv.eliminarDocumentoAdjunto(params).subscribe((result: any) => {
      if (result.success) {
        this.anexoActual = null;
        this.generar = false;
        this.archivoEnviado = false;
      }
    });
  }

  guardarArchivo(){
    if(!this.archivoAdjunto.nombre){
      this.messageService.add({
        key: 'dc',
        severity: 'warn',
        summary: '',
        detail: '(*) Debe de adjuntar un archivo',
      });
      return;
    }

    const formData = new FormData();
    formData.append('file', this.file.file);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.adjuntarAnexo(this.paramsGuardar, formData).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        

        this.archivoEnviado = true;
        this.idArchivoCargado=result.codigo;
        this.obtenerAdjuntos();


        let paramsResolucion = {
          idProcesoPostulacion: JSON.parse(
            '' + localStorage.getItem('bandejapostulacionrevisar')
          ).idProcesoPostulacion,
          idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
            .idusuario,
          numeroResolucion: this.numeroResolucion,
          fechaResolucion: this.fechaResolucion
        };
        this.procesoPostulaionService
        .actualizarResolucionProcesoPostulacion(paramsResolucion)
        .subscribe((resp: any) => {
          if (resp.success) {
            
           

          } else {
            this.messageService.add({
              key: 'an1',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });


        //this.actualizarEstadoProcesoPostulacion(32);

        this.messageService.add({
          key: 'dc',
          severity: 'success',
          detail: 'Se guardó correctamente',
        });



        this.validarAnexo.emit(true);
      } else {
        this.messageService.add({
          key: 'dc',
          severity: 'error',
          summary: '',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    },()=>{
      this.dialog.closeAll();
    });


  }

  params_obtener_adjunto: any = {
    codigoAnexo: null,
    idProcesoPostulacion: JSON.parse(
      '' + localStorage.getItem('bandejapostulacionrevisar')
    ).idProcesoPostulacion,
    //idUsuarioAdjunta: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
    idTipoDocumento: 52,

  };
  


  params_validar_anexo: any = {
    descripcion: '',
    devolucion: false,
    fechaFinPlazo: null,
    idAnexoDetalleStatus: null,
    idDocumentoAdjunto: 1,
    idStatusAnexo: null,
    idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
      .idusuario,
    idValidadAnexo: null,
    idValidarDetalle: null,
    observacion: '',
    pageNum: null,
    pageSize: null,
    search: null,
    sla: 2,
    startIndex: null,
    totalPage: null,
    totalRecord: null,
  };

  obtenerAdjuntos(isClick: boolean = false) {
    this.serv
      .obtenerAdjuntos(this.params_obtener_adjunto)
      .subscribe((result: any) => {

        if (result.data?.length > 0) {
          this.anexoActual = result.data[0];
          this.params_validar_anexo.idDocumentoAdjunto =
            this.anexoActual.idDocumentoAdjunto;

            this.archivoAdjunto.nombre = this.anexoActual.nombreDocumento;
            this.archivoAdjunto.file = this.anexoActual.file;
            this.archivoAdjunto.url = 'application/octet-stream';
            

            if (isClick) {
              DownloadFile(
              this.anexoActual.file,
                this.anexoActual.nombreDocumento,
              'application/octet-stream'
              );
            
            }

          this.generar = false;
          this.archivoEnviado = true;
        }
      });
  }


  btnDescargarFormato(){

      this.serv.descargarguianexo4().subscribe((result: any) => {
            if (result.success) {
              let p_file = this.base64ToArrayBuffer(result.archivo);
              var blob = new Blob([p_file], {
                type: result.contenTypeArchivo,
              });
              var link = document.createElement('a');
              link.href = window.URL.createObjectURL(blob);
              var fileName = result.nombeArchivo;
              link.download = fileName;
              link.click();
              this.generar = true;
            } else {
              this.messageService.add({
                key: 'dc',
                severity: 'error',
                summary: '',
                detail: 'Ocurrió un problema, intente nuevamente',
              });
            }
          });
  }

  base64ToArrayBuffer(base64: any) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  btnNotificarCancelacion(){
    this.actualizarEstadoProcesoPostulacion(33);

    this.messageService.add({
      key: 'dc',
      severity: 'success',
      detail: 'Se notificó correctamente',
    });
  }

    actualizarEstadoProcesoPostulacion(idStatus: number){
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.anexoService.obtenerUsuarioPostulante({
      "idProcesoPostulacion": JSON.parse(
        '' + localStorage.getItem('bandejapostulacionrevisar')
      ).idProcesoPostulacion
    }).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {

          let params = {
            correoPostulante: result.data.correoElectronico,
            idProcesoPostulacion: JSON.parse(
              '' + localStorage.getItem('bandejapostulacionrevisar')
            ).idProcesoPostulacion,
            idStatus: idStatus,
            idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
              .idusuario,
              idArchivoResolucion: this.idArchivoCargado
          };

          this.ampliacionSolicitudService
            .actualizarEstadoProcesoPostulacion(params)
            .subscribe((resp: any) => {

            });

        } 
      }
    );

  }


}
