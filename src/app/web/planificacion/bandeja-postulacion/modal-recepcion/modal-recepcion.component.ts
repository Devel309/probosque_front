import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-modal-recepcion',
  templateUrl: './modal-recepcion.component.html',
})
export class ModalRecepcionComponent implements OnInit {
  fechaRecepcionDocumentos: any = null;
  numeroTramite: any = null;

  constructor(
    public ref: DynamicDialogRef,
    public dialogService: DialogService,
    private messageService: MessageService,
    private config: DynamicDialogConfig
  ) {}

  ngOnInit() {
    this.fechaRecepcionDocumentos = this.config.data.fechaRecepcionDocumentos;
    this.numeroTramite = this.config.data.numeroTramite;
  }

  enviarRecepcion = () => {
    if (this.fechaRecepcionDocumentos === null) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe seleccionar una fecha',
      });
    } else if (this.numeroTramite === null || this.numeroTramite === '') {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe ingresar numero tramite',
      });
    }else{
      let body = {
        fechaRecepcionDocumentos: this.fechaRecepcionDocumentos,
        numeroTramite: this.numeroTramite,
      };
      this.ref.close(body);
    }

  };
}
