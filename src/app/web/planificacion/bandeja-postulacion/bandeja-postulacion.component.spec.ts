import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BandejaPostulacionComponent } from './bandeja-postulacion.component';

describe('BandejaPostulacionComponent', () => {
  let component: BandejaPostulacionComponent;
  let fixture: ComponentFixture<BandejaPostulacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BandejaPostulacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BandejaPostulacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
