import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '@services';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ModalAutorizacionComponent } from 'src/app/components/modal/modal-autorizacion/modal-autorizacion.component';
import { ModalFileComponent } from 'src/app/components/modal/modal-file/modal-file.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ProcesoOfertaService } from 'src/app/service/proceso-oferta/proceso-oferta.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { EstatusProceso } from 'src/app/shared/const';
import { ModalNuevaConsultaSolicitudComponent } from '../ampliacion-solicitud/modal/modal-ampliacion-solicitud/modal-ampliacion-solicitud.component';
import { ModalDocumentosComponent } from './modal-documentos/modal-documentos.component';
import { ModalAutorizacionPublicacionComponent } from './modal-autorizacion-publicacion/modal-autorizacion-publicacion.component';
import { ModalFileListComponent } from './modal-file-list/modal-file-list.component';
import { GenericoService } from 'src/app/service/generico.service';
import { EstatusProcesoService } from 'src/app/service/estatusProceso.service';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';

@Component({
  selector: 'app-bandeja-postulacion',
  templateUrl: './bandeja-postulacion.component.html',
  styleUrls: ['./bandeja-postulacion.component.scss'],
})
export class BandejaPostulacionComponent implements OnInit {
  permisos: IPermisoOpcion = {} as IPermisoOpcion;

  ref: DynamicDialogRef = new DynamicDialogRef();
  estadoProceso = EstatusProceso;
  procesoPostulacionSelect!: String;
  procesoOfertaSelect!: String;
  usuario!: UsuarioModel;
  perfil = Perfiles;
  selectedEstadoContrato!: string | null;

  listEstadoPostu: any[] = [];

  lista_postulaciones: any = [];


  regiones: any[] = [];

  constructor(
    private serv: PlanificacionService,
    private router: Router,
    private activaRoute: ActivatedRoute,
    private messageService: MessageService,
    private procesoPostulaionService: ProcesoPostulaionService,
    public dialogService: DialogService,
    private seguridadService: SeguridadService,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private anexoService: AnexoService,
    private procesoOfertaService: ProcesoOfertaService,
    private usuarioServ: UsuarioService,
    private dialog: MatDialog,
    private estatusProcesoServ: EstatusProcesoService
  ) {
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
    
  }

  listarGrid: any = { pageNum: 1, pageSize: 10 };
  totalRecords = 0;

  isPerMesaPartes: boolean = false;
  isAutRegional: boolean = false;
  isPostulante: boolean = false;
  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;

    this.isPerMesaPartes = this.usuario.sirperfil === this.perfil.MESA_DE_PARTES ? true : false;
    this.isAutRegional = this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL ? true : false;
    this.isPostulante = this.usuario.sirperfil === this.perfil.POSTULANTE ? true : false;

    localStorage.removeItem('oposicion');
    localStorage.removeItem('procesosPostular');
    localStorage.removeItem('bandejapostulacionrevisar');
    this.listarProcesoPostulacion();
    this.listarEstadoPostulacion();
  }
  listarEstadoPostulacion() {
    let params = { tipoStatus: 'PROCESOP' }
    this.estatusProcesoServ.listarEstadoEstatusProceso(params).subscribe(
      (result: any) => {
        if (result.success) {
          result.data.splice(0, 0, {
            tipoStatus: '',
            idStatusProceso: null,
            descripcion: '-- Seleccione --'
          });
          this.listEstadoPostu = result.data;
          
        }
      })
  }

  limpiarFiltro() {

    this.procesoOfertaSelect = '';
    this.procesoPostulacionSelect = '';
    this.selectedEstadoContrato=null;
    this.listarProcesoPostulacion()
  }

  verDetalle(item: any) {
    let filtro = this.lista_postulaciones.filter(
      (x: any) => x.idProcesoOferta === item.idProcesoOferta
    );
    let posicion = filtro.findIndex(
      (x: any) => x.idProcesoPostulacion === item.idProcesoPostulacion
    );
    item.posicion = posicion;
    localStorage.setItem('bandejapostulacionrevisar', JSON.stringify(item));
    this.router.navigate(['/planificacion/bandeja-postulacion/revisar']);
  }

  Impugnacion(item: any) {
     let resolucion:any = {
      idResolucion: item
    }

    if(this.isAutRegional){
      localStorage.setItem('bandejaevaluacionimpugnacion', JSON.stringify(resolucion));
      this.router.navigate(['/planificacion/bandeja-evaluacion-impugnacion']);
    }else{
      localStorage.setItem('bandejasolicitudimpugnacion', JSON.stringify(resolucion));
      this.router.navigate(['/planificacion/bandeja-solicitud-impugnacion']);
    }
  }


  listarProcesoPostulacion(estado?: any) {

    if (estado == undefined) {
      this.listarGrid = { pageNum: 1, pageSize: 10 };
    }

    let idUsuarioFinal = (this.usuario.sirperfil === this.perfil.AUTORIDAD_REGIONAL || this.usuario.sirperfil === this.perfil.MESA_DE_PARTES) ? null : this.usuario.idusuario;
    let perfilUsuario = this.isPerMesaPartes ? this.perfil.MESA_DE_PARTES : this.isAutRegional ? this.perfil.AUTORIDAD_REGIONAL : null;
    var params = {

      idProcesoOferta: this.procesoOfertaSelect == null ||
        this.procesoOfertaSelect == '-- Seleccione --'
        ? null
        : this.procesoOfertaSelect,
      idProcesoPostulacion: this.procesoPostulacionSelect == null ||
        this.procesoPostulacionSelect == '-- Seleccione --'
        ? null
        : this.procesoPostulacionSelect,

      idStatus: this.selectedEstadoContrato,
      idUsuarioPostulacion: idUsuarioFinal,
      pageNum: this.listarGrid.pageNum,
      pageSize: this.listarGrid.pageSize,
      perfil:  perfilUsuario
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serv.listarProcesoPostulacion(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if(result.isSuccess && result.data.length > 0){
        this.lista_postulaciones = result.data;
        this.totalRecords = result.totalrecord;
      }else{
        this.lista_postulaciones = [];
        this.totalRecords = 0;
        this.messageService.add({
          key: 'tr',
          severity: 'warn',
          summary: '',
          detail: "No se encontraron registros",
        });
      }
    },()=>{
      this.dialog.closeAll();
      this.messageService.add({
        key: 'tr',
        severity: 'error',
        summary: '',
        detail: 'Ocurrió un problema, intente nuevamente',
      });
    });
  }

  obtenerAutorizacion = (element: any, existe: boolean = false) => {
    let params = {
      idProcesoPostulacion: element.idProcesoPostulacion,
      idUsuarioPostulacion: element.idUsuarioPostulacion,
    };

    this.procesoPostulaionService
      .obtenerAutorizacion(params)
      .subscribe((result: any) => {
        if (result.data.length > 0) {
          if (existe) {
            this.ref = this.dialogService.open(ModalAutorizacionComponent, {
              header: 'Autorización',
              width: '70%',
              contentStyle: { 'max-height': '500px', overflow: 'auto' },
              baseZIndex: 10000,
              data: {
                item: result.data,
              },
            });
          }
        }
      });
  };

  cargarModalFile = (mensaje: string, tipo: number, item: any) => {

    this.ref = this.dialogService.open(ModalFileComponent, {
      header: mensaje,
      width: '70%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: item,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {

        var params = new HttpParams()
          .set('CodigoDoc', resp.Descripcion)
          .set('IdProcesoPostulacion', item.idProcesoPostulacion)
          .set('IdSolicitante', item.idUsuarioPostulacion)
          .set(
            'IdUsuarioRegistro',
            JSON.parse('' + localStorage.getItem('usuario')).idusuario
          )
          .set('IdTipoDocumento', tipo === 1 ? '6' : tipo === 2 ? '5' : '16')
          .set('NombreArchivo', resp.file.name);

        const formData = new FormData();
        formData.append('file', resp.file);

        if (tipo === 1) this.enviarAutorizacion(params, formData);
        else if (tipo === 2) this.enviarResolucion(params, formData, item);
        else this.enviarArchivoAnexo(params, formData, item);
      }
    });

};

  cargarModalFileList = (mensaje: string, tipo: number, item: any) => {

      let d : boolean = tipo===1 ? true : false;
      this.ref = this.dialogService.open(ModalFileListComponent, {
        header: mensaje,
        width: '80%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        baseZIndex: 10000,
        data: {
          item: item,
          editar: d
        },
      });

      this.ref.onClose.subscribe((resp: any) => {
        if (resp) {
           this.actualizarEstadoProcesoPostulacion(item, 8);
           let params : any = {
              idProcesoPostulacion: item.idProcesoPostulacion,
              idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
              envioPrueba : true
            };
           this.actualizarEnvioPrueba(params);
        }
      });
  };


  loadData(event: any) {
    this.listarGrid.pageNum = event.first + 1;
    this.listarGrid.pageSize = event.rows;

    this.listarProcesoPostulacion(true);
  }

  enviarArchivoAnexo = (params: any, formData: any, item: any) => {
    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.messageService.add({
          key: 'tr',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'El archivo se guardó correctamente',
        });
        this.actualizarEstadoProcesoPostulacion(item, 8);
      } else {
        this.messageService.add({
          key: 'an2',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  enviarAutorizacion = (params: any, formData: any) => {
    this.procesoPostulaionService
      .autorizarPublicacion(params, formData)
      .subscribe((result: any) => {
        if (result.success) {
          this.messageService.add({
            key: 'tr',
            severity: 'success',
            summary: 'CORRECTO',
            detail: 'Se guardó correctamente',
          });
          this.listarProcesoPostulacion();
        } else {
          this.messageService.add({
            key: 'tr',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  enviarResolucion = (params: any, formData: any, item: any) => {
    this.procesoPostulaionService
      .adjuntarResolucionDenegada(params, formData)
      .subscribe((result: any) => {
        if (result.success) {
          this.actualizarEstadoProcesoPostulacion(item, 26);
        } else {
          this.messageService.add({
            key: 'tr',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  actualizarEstadoProcesoPostulacion = (item: any, status: number) => {
    var params = {
      idusuario: item.idUsuarioPostulacion,
    };
    this.seguridadService.obtenerUsuario(params).subscribe((resp: any) => {
      var params = {
        correoPostulante: resp.data[0].correoElectronico,
        idProcesoPostulacion: item.idProcesoPostulacion,
        idStatus: status,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
      };

      this.ampliacionSolicitudService
        .actualizarEstadoProcesoPostulacion(params)
        .subscribe((resp: any) => {
          if (resp.success) {
            this.actualizarEstatusProcesoOferta(item);
          } else {
            this.messageService.add({
              key: 'tr',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });
    });
  };

  actualizarEstatusProcesoOferta = (item: any) => {
    let params = {
      idProcesoOferta: item.idProcesoOferta,
      idStatus: 8,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.procesoOfertaService
      .actualizarEstatusProcesoOferta(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.messageService.add({
            key: 'tr',
            severity: 'success',
            summary: 'CORRECTO',
            detail: 'Se guardó correctamente',
          });
          this.listarProcesoPostulacion();
        } else {
          this.messageService.add({
            key: 'tr',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  cargarOposicion = (item: any) => {
    localStorage.setItem(
      'oposicion',
      JSON.stringify(item.idProcesoPostulacion)
    );
    this.router.navigate(['planificacion/bandeja-postulacion/oposicion']);
  };

  cargarModalSolicitud = (item: any) => {

    let filtro = this.lista_postulaciones.filter(
      (x: any) => x.idProcesoOferta === item.idProcesoOferta
    );
    let posicion = filtro.findIndex(
      (x: any) => x.idProcesoPostulacion === item.idProcesoPostulacion

    );
    
    item.posicion = posicion;

    // 3, 8
    this.ref = this.dialogService.open(ModalNuevaConsultaSolicitudComponent, {
      header: 'Informe',
      width: '30%',
      contentStyle: { 'min-height': '400px', overflow: 'auto' },
      data: {
        item: item,
        isNew: false,
        isInforme: true,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.messageService.add({
          key: 'tr',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se guardó correctamente ',
        });
        this.listarProcesoPostulacion();
      }
    });
  };

  verModalDocumentos(fila: any) {
    this.ref = this.dialogService.open(ModalDocumentosComponent, {
      header: 'Documentos Presentados',
      width: '70%',
      contentStyle: { overflow: 'auto' },
      data: {
        isDisabled: this.isPerMesaPartes ? !(this.isPerMesaPartes && fila.idEstatusProceso != this.estadoProceso.conObservacion) : !this.isPerMesaPartes,
        idProcPostulacion: fila.idProcesoPostulacion,
        idUsuario: this.usuario.idusuario,
        idEstatusProceso: fila.idEstatusProceso
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarProcesoPostulacion(true);
      }
    });


  }

  cargarModalAutorizacion = (mensaje: string, tipo: number, item: any) => {

    this.ref = this.dialogService.open(ModalAutorizacionPublicacionComponent, {
      header: mensaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        item: item,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarProcesoPostulacion();
/*         var params = new HttpParams()
          .set('CodigoDoc', resp.Descripcion)
          .set('IdProcesoPostulacion', item.idProcesoPostulacion)
          .set('IdSolicitante', item.idUsuarioPostulacion)
          .set(
            'IdUsuarioRegistro',
            JSON.parse('' + localStorage.getItem('usuario')).idusuario
          )
          .set('IdTipoDocumento', tipo === 1 ? '6' : tipo === 2 ? '5' : '16')
          .set('NombreArchivo', resp.file.name);

        const formData = new FormData();
        formData.append('file', resp.file); */


        //if (tipo === 1) this.enviarAutorizacion(params, formData);
        //else if (tipo === 2) this.enviarResolucion(params, formData, item);
        //else this.enviarArchivoAnexo(params, formData, item);
      }
    });

};
  private actualizarEnvioPrueba(params:any){

    this.procesoPostulaionService.actualizarEnvioPrueba(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );

  }
}
