import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { DowloadFileLocal, DownloadFile, ToastService } from '@shared';
import { forEach } from 'lodash';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { AnexoService } from 'src/app/service/anexo/anexo.service';
import { ContratoService } from 'src/app/service/contrato.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';

@Component({
  selector: 'app-modal-list.file',
  templateUrl: './modal-file-list.component.html',
  styleUrls: ['./modal-file-list.scss']
})
export class ModalFileListComponent implements OnInit {
  @ViewChild("fileInput") fileInput: any;
  usuario!: UsuarioModel;
  comboTipoDocumento: any[] = [];
  listaTablaOtros: any[] = [];
  fileArchivosOtro: any = {};
  documento: any = {
        idTipoDocumento:undefined,
        descripcionTipoDocumento:undefined,
        asunto:undefined,
        descripcion:undefined,file:{}
  };

  editar : boolean = false;



  constructor(
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private config: DynamicDialogConfig,
    private procesoPostulaionService: ProcesoPostulaionService,
    private genericoService: GenericoService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private anexoService: AnexoService,
    private usuarioServ: UsuarioService,
    private contratoService: ContratoService,
    private toast: ToastService,
    private archivoServ: ArchivoService,
  ) {
    this.editar = this.config.data.editar;
  }

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.listarComboTipoDocumentos();
    this.listarDocumentos();
  }

   Base64toBlob(base64Data: string, contentType: string): Blob {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data.replace(/['"]+/g, ''));
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      var begin = sliceIndex * sliceSize;
      var end = Math.min(begin + sliceSize, bytesLength);

      var bytes = new Array(end - begin);
      for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }

  registrar() {
    let listaObject: any[] = [];
    let e: any = {}

    let auxLista = this.listaTablaOtros.filter(x => x.idDocumentoAdjunto === 0);
    if (auxLista.length === 0) {
      this.toast.warn("Debe agreagr al menos un documento.");
      return;
    }

    auxLista.forEach(element => {
      e = {};
      e.idProcesoPostulacion = this.config.data.item.idProcesoPostulacion;
      e.idUsuarioAdjunta = this.usuario.idusuario;
      e.descripcion = element.descripcion;
      e.idTipoDocumento = 16;
      e.codigoTipoDocumento = element.idTipoDocumento;
      e.asunto = element.asunto;
      e.nombreGenerado = element.file.nombreGenerado;
      e.nombreDocumento = element.file.nombre;
      e.idDocumentoAdjunto = element.idDocumentoAdjunto;

      listaObject.push(e);
    });

    this.registrarListAnexo(listaObject);
  }

  registrarArchivoAdjunto() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuarioServ.idUsuario, this.documento.idTipoDocumento, this.documento.file.file).subscribe((result) => {
      if (result.success) {
        this.archivoServ.obtenerArchivoGeneral(result.data).subscribe((resp: any) => {
          this.dialog.closeAll();
          if (resp.success && resp.data && resp.data.idArchivo) {
            this.comboTipoDocumento.forEach(i => {
              if (i.codigo === this.documento.idTipoDocumento) {
                this.documento.descripcionTipoDocumento = i.valorPrimario;
              }
            });

            this.documento.file = {
              nombre: resp.data.nombre,
              nombreGenerado: resp.data.nombreGenerado
            }
            this.documento.idDocumentoAdjunto = 0;

            this.listaTablaOtros.push(this.documento);
            this.fileInput.nativeElement.value = "";
            this.documento = {
              idTipoDocumento: undefined,
              descripcionTipoDocumento: undefined,
              asunto: undefined,
              descripcion: undefined, file: {}
            }
          } else {
            this.toast.warn(result.message);
          }
        }, (error) => {
          this.dialog.closeAll();
          this.errorMensaje(error.message)
        });
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => {
      this.dialog.closeAll();
      this.errorMensaje(error.message);
    });
  }

  agregar() {

    if (this.listaTablaOtros.length > 4) { this.warnMensaje("Ha alcanzado el límite máximo de 5 evidencias o documentos a publicar."); return }
    if (this.documento.idTipoDocumento === undefined) { this.warnMensaje("Ingrese el tipo de documento"); return }
    if (this.documento.asunto === undefined) { this.warnMensaje("Ingrese el asunto del documento"); return }
    if (this.documento.descripcion === undefined) { this.warnMensaje("Ingrese descripción del documento"); return }
    if (this.documento.file.file === undefined) { this.warnMensaje("Adjunte archivo"); return }

    this.registrarArchivoAdjunto();
  }

  deleteRow(posicion:any){

    const index = this.listaTablaOtros.indexOf(posicion, 0);
    if (index > -1) {
      this.listaTablaOtros.splice(index, 1);
    }
  }

  listarComboTipoDocumentos() {
    const params = { prefijo: 'TDRESULTEVAL' }
    this.genericoService.getParametroPorPrefijo(params).subscribe((result: any) => {
      if (result.success) {
        result.data.splice(0, 0, { codigo: null, valorPrimario: '-- Seleccione --' });
        this.comboTipoDocumento = result.data;
      }
    });
  }

  private listarDocumentos() {
    this.listaTablaOtros = [];
    const params = { idProcesoPostulacion: this.config.data.item.idProcesoPostulacion, idTipoDocumento:16}
    this.anexoService.listarDocumentos(params).subscribe((result: any) => {
      if (result.success) {
        this.convertListDocumento(result.data);
      }
    });
  }

  private convertListDocumento(lista:any[]){

    let documento = {};

    lista.forEach(element => {
      documento = {
        idDocumentoAdjunto : element.idDocumentoAdjunto,
        idTipoDocumento : element.idTipoDocumento,
        descripcionTipoDocumento : element.descripcionCodigoTipoDocumento,
        asunto : element.asunto,
        descripcion : element.descripcion,
        file : {
            nombre:element.nombreDocumento,
            nombreGenerado : element.nombreGenerado
        }
      };
      this.listaTablaOtros.push(documento);
    });
  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.documento.file.url = URL.createObjectURL(event.target.files[0]);
    this.documento.file.file = event.target.files[0];
    this.documento.file.nombre = event.target.files[0].name;

  }

  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.documento.file.id) {
      this.dialog.closeAll();
      /*descargar por nombre*/

    } else {
      DowloadFileLocal(this.documento.file.url, this.documento.file.nombre);
      this.dialog.closeAll();
    }

  }
  descargarArchivoList(param:any){
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (param.file.id) {
      this.dialog.closeAll();
      /*descargar por nombre*/

    } else {
      DowloadFileLocal(param.file.url, param.file.nombre);
      this.dialog.closeAll();
    }
  }

  descargarArchivoDocumento(obj:any){
    this.btnDescargarArchivoContrato(obj);
  }

  private btnDescargarArchivoContrato(fila: any) {
    let params = { "nombreGenerado": fila.file.nombreGenerado };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.obtenerArchivoContrato(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data) DownloadFile(result.data, fila.file.nombre, 'application/octet-stream');
      else this.warnMensaje("No se encontro el documento.");
     }, (error: HttpErrorResponse) => {
      this.dialog.closeAll();
      this.errorMensaje(error.message);
    });
  };

  eliminar(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {

        this.documento.file = {};
        this.fileInput.nativeElement.value = "";
      },

    });

  }

  cerrarModal() {
    this.ref.close();
  }

  private registrarListAnexo(listObjc:any){
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexoService.adjuntarListAnexo(listObjc).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          if (result.validateBusiness===false) {
            this.warnMensaje(result.message);
          }else{
            this.listarDocumentos();
            this.successMensaje(result.message);
            this.ref.close(result);
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.errorMensaje(error.message);
        this.dialog.closeAll();
      }
    );

  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  successMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'success', summary: '', detail: mensaje });
  }

  warnMensaje(mensaje: any) {
    this.messageService.add({ key: 'tr', severity: 'warn', summary: '', detail: mensaje });
  }
}
