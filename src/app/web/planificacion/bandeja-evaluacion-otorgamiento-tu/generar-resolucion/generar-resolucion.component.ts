import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoPostulacion } from 'src/app/model/util/CodigoEstadoPostulacion';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ResolucionService } from 'src/app/service/resolucion.service';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { ModalResolucionComponent } from '../modal-resolucion/modal-resolucion.component';

@Component({
  selector: 'app-generar-resolucion',
  templateUrl: './generar-resolucion.component.html',
})
export class GenerarResolucionComponent implements OnInit {
  idProcesoOferta!: number | null;

  listItems!: MenuItem[];
  homes!: MenuItem;

  procesosOfertas: any[] = [];

  listaResultado: any[] = [];

  ref!: DynamicDialogRef;
  listaProcesos: any[] = [];

  listarGrid: any = { pageNum: 1, pageSize: 10 };
  totalRecords = 0;

  isDisabledNotificar: boolean = true;

  constructor(
    private resultadoPPService: ResultadoPPService,
    private messageService: MessageService,
    public dialogService: DialogService,
    private serv: PlanificacionService,
    private toast: ToastService,
    private dialog: MatDialog,
    private resolucionService: ResolucionService,
  ) { }

  ngOnInit() {

    this.listItems = [
      {
        label: 'Bandeja evaluación y aprobación de otorgamiento de TH',
        routerLink: '/planificacion/bandeja-evaluacion-otorgamiento-tu',
      },
      { label: 'Generar Resolución' },
    ];

    this.homes = { icon: 'pi pi-home', routerLink: '/inicio' };

    this.listarPorFiltroOtorgPP();
  }

  loadData(event: any) {
    this.listarGrid.pageNum = event.first + 1;
    this.listarGrid.pageSize = event.rows;

    this.listarPorFiltroOtorgPP(true);
  }

  resolucionValidarServ(idProOferIn: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resolucionService.resolucionValidar(idProOferIn).subscribe(result => {
      this.dialog.closeAll();

      if (result.success && result.validateBusiness) {
        let auxPO = this.listaResultado.find(item => (item.idProcesoOferta == idProOferIn && item.ganador==true));

        debugger
        if (!auxPO) {
            if(this.listaResultado !=null && this.listaResultado.length>0 ){
              this.toast.warn('El proceso oferta ingresado no tiene ganador.');
            }
             else{
              this.toast.warn('El proceso oferta ingresado no se encuentra en la tabla.');
             }
          return;
        } else if (auxPO.notaTotal === null) {
          this.toast.warn('El proceso oferta esta pendiente de evaluación.');
          return;
        }

        this.modalResolucion(auxPO);
      } else {
        this.toast.warn(result.message);
      }

    }, (error) => this.errorMensaje(error, true));
  }

  btnEnviar() {
    if (!this.idProcesoOferta) {
      this.toast.warn('(*) Debe ingresar un proceso oferta.');
      return;
    }

    this.resolucionValidarServ(this.idProcesoOferta);

    // var nota = this.listaResultado.filter((x) => x.notaTotal === 0);
    // let cantidadPP = this.listaResultado.length;
    // let cantidadProceso = this.listaProcesos.length;
    // let pendiente: Boolean = false;
    // this.listaResultado.map((l) =>{
    //   if(l.ganador == null) pendiente = true;
    // });

    // if (pendiente) {
    //   this.toast.warn('Procesos de Postulación pendientes en la bandeja de postulación');
    //   return;
    // }
  };

  modalResolucion = (procesoOk: any) => {
    this.ref = this.dialogService.open(ModalResolucionComponent, {
      header: 'Notificar Resultado de Propuesta Técnica',
      width: '500px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        listaResultado: this.listaResultado,
        procesoOfertaSelect: procesoOk
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) this.listarPorFiltroOtorgPP();
    });
  };

  listarPorFiltroOtorgPP = (estado?: any) => {
    if (estado == undefined) this.listarGrid = { pageNum: 1, pageSize: 10 };

    var parms = {
      idEstadoPostulacion: CodigoEstadoPostulacion.APROBADO,
      idProcesoOferta: this.idProcesoOferta || null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
      pageNum: this.listarGrid.pageNum,
      pageSize: this.listarGrid.pageSize,
      ganador: null
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadoPPService.listarPorFiltroOtrogamientoProcesoPostulacion(parms).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success && resp.data?.length > 0) {
        this.totalRecords = resp.totalRecord;

        var objMaxim: any = {};
        resp.data.forEach((element: any) => {
          if (element.notaTotal > 0) {
            if (objMaxim.notaTotal < element.notaTotal) objMaxim = element;
            else if (objMaxim.notaTotal > element.notaTotal) objMaxim = objMaxim;
            else objMaxim = element;
          }
        });

        if (this.idProcesoOferta) {
          this.isDisabledNotificar = false;

          resp.data.forEach((element: any) => {
            if (element.idProcesoPostulacion === objMaxim.idProcesoPostulacion)
              element.background = '#bdf1a87a';
          });
        }

        this.listaResultado = resp.data;

        for (let i = 0; i < this.listaResultado.length; i++) {
          if (this.listaResultado[i].notaTotal == null) {
            this.listaResultado[i].ganador = null;
            this.listaResultado[i].background = "";
          } else if (this.listaResultado[i].notaTotal >= 70) {
            this.listaResultado[i].ganador = true;
          } else {
            this.listaResultado[i].ganador = false;
            this.listaResultado[i].background = "";
          }
        }
      } else {
        this.totalRecords = 0;
        this.listaResultado = [];
        this.toast.warn(resp.message);
      }

    }, (error) => this.errorMensaje(error, true));
  };

  btnLimpiar(){
    this.idProcesoOferta = null;
    this.isDisabledNotificar = true;
    this.listarPorFiltroOtorgPP();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
