import { MESSAGES_CONTAINER_ID } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastService } from '@shared';
import { SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { ConvertDateToString, ConvertNumberToDate } from 'src/app/shared/util';
import { ModalResolucionComponent } from './modal-resolucion/modal-resolucion.component';

@Component({
  selector: 'app-bandeja-evaluacion-otorgamiento-tu',
  templateUrl: './bandeja-evaluacion-otorgamiento-tu.component.html',
  styleUrls: ['./bandeja-evaluacion-otorgamiento-tu.component.scss'],
})
export class BandejaEvaluacionOtorgamientoTuComponent implements OnInit {
  listaResultadoPP: any[] = [];
  estadoevaluacion: SelectItem[];
  procesoPostulacionSelect!: any;
  selectestadoevaluacion!: any;
  ref!: DynamicDialogRef;
  listarGrid: any = { pageNum: 1, pageSize: 10 };
  totalRecords = 0;
  adicional: boolean = false;
  constructor(
    private procesoPostulaionService: ProcesoPostulaionService,
    private route: Router,
    private resultadoPPService: ResultadoPPService,
    public dialogService: DialogService,
    public dialog: MatDialog,
    private toast: ToastService,
  ) {
    this.estadoevaluacion = [
      { label: '-- Seleccione --', value: null },
      { label: 'Ganador', value: true },
      { label: 'Pendiente envio resolución', value: false },
    ];
  }

  procesoPostulacion: any[] = [];

  ngOnInit(): void {
    localStorage.removeItem('evaluacion-otorgamiento-tu');
    this.cargarPP();
  }

  cargarPP = (estado?: any) => {

    if (estado == undefined) {
      this.listarGrid = { pageNum: 1, pageSize: 10 };
    }

    var form = {
      idProcesoOferta: null,
      idProcesoPostulacion: parseInt(this.procesoPostulacionSelect) || null,
      idUsuarioPostulacion: null,
      ganador: this.selectestadoevaluacion,
      pageNum: this.listarGrid.pageNum,
      pageSize: this.listarGrid.pageSize,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadoPPService.listarPorFiltroOtrogamientoProcesoPostulacion(form).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success && resp.data.length > 0) {

        resp.data.forEach((element: any) => {
          if (element.fechaPostulacion != null)
            element.fechaPostulacionString = ConvertDateToString(
              ConvertNumberToDate(element.fechaPostulacion)
            );
        });

        this.listaResultadoPP = resp.data;

        for (let i = 0; i < this.listaResultadoPP.length; i++) {
          if (this.listaResultadoPP[i].notaTotal == null) {
            this.listaResultadoPP[i].ganador = null
          } else if (this.listaResultadoPP[i].notaTotal >= 70) {
            this.listaResultadoPP[i].ganador = true
          } else {
            this.listaResultadoPP[i].ganador = false
          }
        }
        this.totalRecords = resp.totalRecord;
      }else{
        this.totalRecords = 0;
        this.listaResultadoPP = [];
        this.toast.warn( Mensajes.MSJ_SIN_REGISTROS);
      }
    }, (error) => this.errorMensaje(Mensajes.MSJ_ERROR_CATCH, true));
  };

  generarResolucion = () => {
    this.route.navigate(['/planificacion/generar-resolucion']);
  };

  verDetalle = (data: any) => {
    localStorage.setItem('evaluacion-otorgamiento-tu', JSON.stringify(data));
    this.route.navigate(['/planificacion/evaluacion-otorgaminento-tu']);
  };

  verRespuesta = (data: any) => {
    this.ref = this.dialogService.open(ModalResolucionComponent, {
      header: 'Ver Resolución',
      width: '30%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: data,
        isView: true,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      this.cargarPP();
    });
  };

  loadData(event: any) {
    this.listarGrid.pageNum = event.first + 1;
    this.listarGrid.pageSize = event.rows;

    this.cargarPP(true);
  }

  btnLimpiar(){
    this.procesoPostulacionSelect = null;
    this.selectestadoevaluacion = null;
    this.cargarPP();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
