import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { TipoDocGestion } from 'src/app/model/util/TipoDocGestion';
import { GenericoService } from 'src/app/service/generico.service';
import { ResolucionService } from 'src/app/service/resolucion.service';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { Perfiles } from 'src/app/model/util/Perfiles';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-resolucion',
  templateUrl: './modal-resolucion.component.html',
})
export class ModalResolucionComponent implements OnInit {
  idDocumentoAdjunto!: number;
  file!: any;
  j: number = 0;
  asunto: string = '';
  contenidoEmail: string = '';

  nombreArchivo: string = '';
  documento: string = '';

  usuario!: UsuarioModel;
  comboTipoDocumento: any[] = [];
  resolucionRequest: any = {};
  fileResolucion: any = {};
  fileGarantia: any = {};
  procesoOfertaSelect: any = {};

  concesionSelect: any = {};
  tipoDocGestion : string = '';
  descargaFormato : boolean = false;
  idResolucion : number = 0;

  isEnviar: boolean = false;
  idResolucionArchivo: number = 0;
  isPerTitularTH: boolean = false;
  isPerARFFS: boolean = false;
  isGarantia: boolean = false;
  minDate = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    public ref: DynamicDialogRef,
    public dialog: MatDialog,
    public config: DynamicDialogConfig,
    private resultadoPPService: ResultadoPPService,
    private seguridadService: SeguridadService,
    private archivoServ: ArchivoService,
    private usuarioServ: UsuarioService,
    private toast: ToastService,
    private genericoService: GenericoService,
    private resolucionService: ResolucionService
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.procesoOfertaSelect = this.config.data?.procesoOfertaSelect || {};
    this.concesionSelect = this.config.data?.concesionSelect || {};
    this.descargaFormato = this.config.data?.descargaFormato || false;
    this.tipoDocGestion = this.config.data?.tipoDocGestion || '';
    this.usuario = this.usuarioServ.usuario;

    this.isPerTitularTH = this.usuario.sirperfil === Perfiles.TITULARTH;
    this.isPerARFFS = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
  }

  ngOnInit() {
    this.listarComboTipoDocumentosArchivo();

    if(this.concesionSelect.idResolucion){



      this.obtenerResolucion(this.concesionSelect.idResolucion);

     if(this.tipoDocGestion== TipoDocGestion.CONCESIONPFDM ){
      this.descargaFormato=false;
     }

    }
    else{
      if(this.isPerARFFS)
      this.isEnviar=true;
    }

    if (this.config.data.isView) this.obtenerResultados();
  }

  obtenerResolucion(param:any) {
    const params = { idResolucion: param }
    this.resolucionService.obtenerResolucion(params).subscribe((result: any) => {
    //  debugger
      if (result.success && result.data.length > 0) {

        const auxData = result.data[0];

        if(auxData?.idResolucion){
          this.config.data.isView = true;

          this.resolucionRequest.comentarioResolucion=auxData.comentarioResolucion;
          this.resolucionRequest.fechaResolucion=auxData.fechaResolucion;
          this.fileResolucion.idArchivo=auxData.idArchivoResolucion;
          this.onDownloadFile(this.fileResolucion.idArchivo);

          if(auxData.listaResolucionArchivo.length>0){

            this.fileGarantia.idArchivo=auxData.listaResolucionArchivo[0].idArchivo;
            this.idResolucionArchivo=auxData.listaResolucionArchivo[0].idResolucionArchivo;

            this.onDownloadFile_( this.fileGarantia.idArchivo);
            if(this.isPerTitularTH){
              this.isGarantia=true;
            }
        }
        else{

          if(this.isPerTitularTH){
            this.isGarantia=false;
            this.isEnviar=true;
          }
        }

      }




      }


    });


  }
  onDownloadFile(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
     // debugger
        this.fileResolucion=result.data;

        this.resolucionRequest.tipoDocArchivo=result.data.tipoDocumento;

      }

    });
  }
  onDownloadFile_(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
     // debugger
        this.fileGarantia=result.data;
      }

    });
  }

  listarComboTipoDocumentosArchivo() {
    const params = { prefijo: 'TDOCGE' }
    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success) {
        result.data.splice(0, 0, { codigo: null, valorPrimario: '-- Seleccione --' });
        this.comboTipoDocumento = result.data;
      }
    });
  }

  obtenerResultados = () => {
    this.resultadoPPService.obtenerResultados(this.config.data.item.idResultadoPP).subscribe((resp: any) => {
      this.nombreArchivo = resp.data.nombredocumento;
      this.documento = resp.data.documento;
      this.asunto = resp.data.resultado.asuntoEmail;
      this.contenidoEmail = resp.data.resultado.contenidoEmail;
    });
  };

  registrarResolucionResultado(paramsIn: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resolucionService.registrarResolucionResultado(paramsIn).subscribe(result => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.ref.close(true);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  registrarResolucion(paramsIn: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resolucionService.registrarResolucion(paramsIn).subscribe(result => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.ref.close(true);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  actualizarResolucion(paramsIn: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resolucionService.actualizarResolucion(paramsIn).subscribe(result => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(result.message);
        this.ref.close(true);
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  registrarArchivoAdjunto(file: any) {
    if(!this.resolucionRequest.tipoDocArchivo){
      this.fileResolucion = {};
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuario.idusuario, this.resolucionRequest.tipoDocArchivo, file.file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        this.fileResolucion.idArchivo = result.data;
      } else {
        this.toast.warn(result.message);
        this.fileResolucion = {};
      }
    }, (error) => {
      this.fileResolucion = {};
      this.errorMensaje(error, true);
    });
  }
  registrarArchivoGarantia(file: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuario.idusuario, 'TDOCGFC', file.file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        this.fileGarantia.idArchivo = result.data;
      } else {
        this.toast.warn(result.message);
        this.fileGarantia = {};
      }
    }, (error) => {
      this.fileGarantia = {};
      this.errorMensaje(error, true);
    });
  }
  eliminarArchivoAdjunto(idAdjuntoOut: number) {
    if (!idAdjuntoOut) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idusuario).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
        this.fileResolucion = {};
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  eliminarArchivoGarantia(idAdjuntoOut: number) {
    if (!idAdjuntoOut) return;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(idAdjuntoOut, this.usuario.idusuario).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
        this.fileGarantia = {};
      } else {
        this.toast.warn(result.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }
  btnEnviar() {
    if (!this.validarEnviar()) return;
    let auxArchivos: any[] = [];
    if( TipoDocGestion.CONCESIONPFDM === this.tipoDocGestion ){



      if(!this.fileGarantia.idArchivo)
    //  debugger
      auxArchivos = [{ "idArchivo": this.fileGarantia.idArchivo}]


      const params = {
          idResolucion:this.concesionSelect.idResolucion,
          tipoDocumentoGestion:  this.tipoDocGestion,
          nroDocumentoGestion: this.concesionSelect?.idConcesion,
          idArchivoResolucion: this.fileResolucion.idArchivo,
          comentarioResolucion: this.resolucionRequest.comentarioResolucion,
          fechaResolucion: this.resolucionRequest.fechaResolucion,
          idUsuarioRegistro: this.usuario.idusuario,
          idPostulante:this.concesionSelect.idSolcitante,
          listaResolucionArchivo: auxArchivos
      };

     if(!this.concesionSelect.idResolucion){

      const params = {
        tipoDocumentoGestion:  this.tipoDocGestion,
        nroDocumentoGestion: this.concesionSelect?.idConcesion,
        idArchivoResolucion: this.fileResolucion.idArchivo,
        comentarioResolucion: this.resolucionRequest.comentarioResolucion,
        fechaResolucion: this.resolucionRequest.fechaResolucion,
        idUsuarioRegistro: this.usuario.idusuario,
        idPostulante:this.concesionSelect.idSolcitante,
        listaResolucionArchivo: auxArchivos
    };
      this.registrarResolucion(params);

     }
     else{
      const params = {
        idResolucion:this.concesionSelect.idResolucion,
        tipoDocumentoGestion:  this.tipoDocGestion,
        nroDocumentoGestion: this.concesionSelect?.idConcesion,
        idArchivoResolucion: this.fileResolucion.idArchivo,
        comentarioResolucion: this.resolucionRequest.comentarioResolucion,
        fechaResolucion: this.resolucionRequest.fechaResolucion,
        idUsuarioRegistro: this.usuario.idusuario,
        idPostulante:this.concesionSelect.idSolcitante,
        listaResolucionArchivo: auxArchivos
    };
      this.actualizarResolucion(params);
    }


    } else{
      const paramsOut = this.getParametros();
      this.registrarResolucionResultado(paramsOut);
    }
  };

  getParametros() {
    const params = {
      "resolucion": {
        "tipoDocumentoGestion": TipoDocGestion.PROCESO_OFERTA,
        "nroDocumentoGestion": this.procesoOfertaSelect?.idProcesoOferta,
        "idArchivoResolucion": this.fileResolucion.idArchivo,
        "comentarioResolucion": this.resolucionRequest.asunto,
        "fechaResolucion": this.resolucionRequest.fechaResolucion,
        "idUsuarioRegistro": this.usuario.idusuario
      },
      "resultadoPp": {
        "idProcesoPostulacion": this.procesoOfertaSelect?.idProcesoPostulacion,
        "asuntoMail": "resultados",
        "contenidoMail": "Se declara ganador al primer solicitante",
        "notaTotal": this.procesoOfertaSelect?.notaTotal,
        "ganador": this.procesoOfertaSelect?.ganador,
        "idUsuarioRegistro": this.usuario.idusuario
      }
    }

    return params;
  }

  validarEnviar(): boolean {
    let validado = true;

    if (!this.resolucionRequest.comentarioResolucion) {
      validado = false;
      this.toast.warn('(*) Debe ingresar un asunto.');
    }
    if (!this.resolucionRequest.fechaResolucion) {
      validado = false;
      this.toast.warn('(*) Debe ingresar una fecha de resolución.');
    }
    if (!this.resolucionRequest.tipoDocArchivo) {
      validado = false;
      this.toast.warn('(*) Debe seleccionar un tipo de documento.');
    }
    if (!this.fileResolucion.nombre) {
      validado = false;
      this.toast.warn('(*) Debe adjuntar un documento de resolución.');
    }

    if (this.isPerTitularTH && !this.fileGarantia.nombre) {
      validado = false;
      this.toast.warn('(*) Debe adjuntar Documento de garantía de fiel cumplimiento.');
    }

    return validado;
  }

  cerrarModal() {
    this.ref.close();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }





  buscarUsuario = () => {
    this.j = 1;
    this.config.data.listaResultado.forEach((element: any) => {
      var params = {
        idusuario: element.idUsuarioPostulacion,
      };
      this.seguridadService.obtenerUsuario(params).subscribe((resp: any) => {
        element.correoPostulante = resp.data[0].correoElectronico;
        this.registrarResultados(element);
      });
    });
  };

  registrarResultados = (element: any) => {
    // ;
    var parms = {
      asuntoEmail: this.asunto,
      contenidoEmail: this.contenidoEmail,
      correoPostulante: element.correoPostulante,
      ganador: !element.background ? false : true,
      idAutoridadRegional: 33,
      idDocumentoAdjunto: this.idDocumentoAdjunto,
      idResultadoPP: element.idResultadoPP,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idProcesoPostulacion: element.idProcesoPostulacion,
    };

    this.resultadoPPService.registrarResultados(parms).subscribe((data: any) => {
      // ;
      if (this.config.data.listaResultado.length === this.j) {
        if (data.success) this.ref.close(true);
      } else this.j = this.j + 1;
    });
  };

  btnDescargaFormato(){


  }


}
