import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { Page } from '@models';
import { LazyLoadEvent } from 'primeng/api';
import { EvaluacionListarRequest } from '../../../model/EvaluacionListarRequest';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import { CodigoEstadoEvaluacion } from '../../../model/util/CodigoEstadoEvaluacion';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { CodigoEstadoPlanManejo } from 'src/app/model/util/CodigoEstadoPlanManejo';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ModalDocumentosDigitalizadosComponent } from 'src/app/shared/components/modal-documentos-digitalizados/modal-documentos-digitalizados.component';

interface plan {
  idTipoProceso: any;
  idTipoPlan: any;
  codigoEstado: string;
  descripcion: string;
}

@Component({
  selector: 'bandeja-documentos-digitalizados',
  styleUrls: [ './bandeja-documentos-digitalizados.component.scss' ],
  templateUrl: './bandeja-documentos-digitalizados.component.html'
})
export class BandejaDocumentosDigitalizadosComponent implements OnInit {
  planes: any[] = [];
  usuario!: UsuarioModel;
  evaluacionRequest: EvaluacionListarRequest
  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  PLAN: plan = {
    idTipoProceso: 3,
    idTipoPlan: null,
    codigoEstado: '',
    descripcion: '',
  }

  loading = false;
  totalRecords = 0;
  ref!: DynamicDialogRef;

  constructor(
    private user: UsuarioService,
    private evaluacionService: EvaluacionService,
    private dialogService: DialogService
  ) {
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionListarRequest();
  }

  ngOnInit(): void {
    this.setCodigoEstado(CodigoEstadoPlanManejo.FAVORABLE);
    this.buscar();
  }

  setCodigoEstado(codigos: string) {
    this.PLAN = {
      idTipoProceso: 3,
      idTipoPlan: null,
      codigoEstado: codigos,
      descripcion: 'DEMA',
    };
  }

  buscar() {
    this.listarPlanes();
  }

  limpiar() {
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.buscar();
  }

  listarPlanes(page?: Page) {
    if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }
    this.evaluacionService
      .listarPlanManejoEvaluacionPmfiDema(
        this.PLAN,
        this.evaluacionRequest,
        page
      )
      .subscribe((res: any) => {
        this.planes = res.data;
        this.totalRecords = res.totalRecords;
      });
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = (Number(e.first) / pageSize) + 1;
    const page = new Page({ pageNumber, pageSize })
    this.listarPlanes(page);
  }

  nuevoPlan() { }

  openModal(item: any) {
    this.ref = this.dialogService.open(ModalDocumentosDigitalizadosComponent, {
      header: "Documentos Digitalizados",
      width: "70%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        item: item
      },
    });
  }

}
