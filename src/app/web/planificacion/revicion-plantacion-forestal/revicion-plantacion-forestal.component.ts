import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AreaPFModel, AreaPlantadaModel, DetallePlantacion1Model, DetallePlantacion2Model, InformacionSolicitantePFModel, ObservacionesPFModel, PlantacionForestalModel } from 'src/app/model/util/dataDemoMPAFPP';

@Component({
  selector: 'app-revicion-plantacion-forestal',
  templateUrl: './revicion-plantacion-forestal.component.html',
  styleUrls: ['./revicion-plantacion-forestal.component.scss']
})
export class RevicionPlantacionForestalComponent implements OnInit {

  data:PlantacionForestalModel = new PlantacionForestalModel();



  constructor(private router: Router) { 

    let temp = window.history.state.data;

    this.data = new PlantacionForestalModel(temp);
    this.data.observacion = "";
    this.data.informacionSolicitante.readonly = true;
    this.data.area.readonly = true;
    this.data.areaPlantada.readonly = true;
    this.data.detalleAreaPlanteada1.readonly = true;
    this.data.detalleAreaPlanteada2.readonly = true;
    
  }

  guardar(tipo:string)
  {
    ;
    this.data.estado = tipo;
    let obs:ObservacionesPFModel = new ObservacionesPFModel();
    obs.observacion = this.data.observacion;
    obs.estado = tipo;
    this.data.observaciones.push(obs);

    let datoStr:any =  localStorage.getItem('plantacionesForestales');
    let datos:any[] = [];
    if(!datoStr)
    {
      datos = [];
    }else{
      datos = JSON.parse(datoStr);
      let index = datos.findIndex(x => x.id == this.data.id);
      datos[index] = this.data;
      localStorage.setItem('plantacionesForestales',JSON.stringify(datos));
    }



    this.router.navigateByUrl('/planificacion/bandeja-plantacion-forestal');
  }



  ngOnInit(): void {
  }

}
