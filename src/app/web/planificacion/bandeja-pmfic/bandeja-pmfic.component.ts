import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PlanManejoService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { Page, PlanManejoMSG as MSG } from "@models";
import { FormBuilder, FormGroup } from "@angular/forms";
import { LazyLoadEvent } from "primeng/api";
import { EvaluacionListarRequest } from "../../../model/EvaluacionListarRequest";
import { UsuarioModel } from "../../../model/seguridad/usuario";
import { CodigoProceso } from "../../../model/util/CodigoProceso";
import { CodigoEstadoEvaluacion } from "../../../model/util/CodigoEstadoEvaluacion";
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from "../../../model/util/CodigoEstadoPlanManejo";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ModalInfoPermisoComponent } from "src/app/shared/components/modal-info-permiso/modal-info-permiso.component";
import { Mensajes } from "src/app/model/util/Mensajes";
import { ResultadoEvaluacionTitularSharedComponent } from "src/app/shared/components/resultado-evaluacion-titular/resultado-evaluacion-titular-shared.component";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { Perfiles } from "../../../model/util/Perfiles";
import { IPermisoOpcion } from "src/app/model/Comun/IPermisoOpcion";

@Component({
  selector: "bandeja-pmfic",
  styleUrls: ["./bandeja-pmfic.component.scss"],
  templateUrl: "./bandeja-pmfic.component.html",
})
export class BandejaPmficComponent implements OnInit {
  f!: FormGroup;
  planes: any[] = [];
  usuario!: UsuarioModel;
  evaluacionRequest: EvaluacionListarRequest;
  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  CodigoProceso = CodigoProceso;
  Perfiles = Perfiles;
  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  CodigoEstadoMesaPartes = CodigoEstadoMesaPartes;
  first: number = 0;

  PLAN = {
    idTipoProceso: 3,
    idTipoEscala: 2,
    idTipoPlan: 7,
    idSolicitud: 1,
    descripcion: CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
  };

  loading = false;
  totalRecords = 0;

  ref!: DynamicDialogRef;
  permisos: IPermisoOpcion = {} as IPermisoOpcion;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    private dialogService: DialogService,
    private evaluacionService: EvaluacionService,
    private activaRoute: ActivatedRoute
  ) {
    this.f = this.initForm();
    this.usuario = this.user.usuario;
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    this.buscar();
  }

  initForm() {
    return this.fb.group({
      dniElaborador: [null],
      rucComunidad: [null],
      nombreElaborador: [null],
      idPlanManejo: [null],
    });
  }

  buscar() {
    this.listarPlanes().subscribe();
  }

  limpiar() {
    this.first = 0;
    this.f.reset();
    this.evaluacionRequest = new EvaluacionListarRequest();
    this.buscar();
  }

  load(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listarPlanes(page).subscribe((res) => {
      this.totalRecords = res.totalRecords;
    });
  }

  nuevoPlan() {
    const body = { ...this.PLAN, idUsuarioRegistro: this.user.idUsuario };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarPlanManejo(body)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res) => this.navigate(res.data.idPlanManejo));
  }

  verPlan(idPlanManejo: number) {
    this.navigate(idPlanManejo);
  }

  verDetalle(item: any) {
    var params = {
      idPlanManejo: item.idPlanManejo,
      codigoEvaluacion: "PLAN",
    };
    this.evaluacionService
      .listarEvaluacionResumido(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          if (response.data.length != 0) {
            this.ref = this.dialogService.open(
              ResultadoEvaluacionTitularSharedComponent,
              {
                header: "RESULTADO EVALUACIÓN",
                width: "60%",
                contentStyle: { overflow: "auto" },
                data: {
                  item: item,
                },
              }
            );

            this.ref.onClose.subscribe((resp: any) => {
              if (resp) {
                if (resp.tipo == "EVAL") {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );

                  this.router.navigate([
                    "/planificacion/evaluacion/requisitos-previos/" +
                      item.idPlanManejo +
                      "/" +
                      resp.data.codigoEvaluacionDet,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "PMFIC"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/plan-manejo-forestal-intermedio/" +
                      item.idPlanManejo,
                  ]);
                }
              }
            });
          } else {
            this.toast.warn("El Plan Seleccionado No Tiene Observaciones");
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listarPlanes(page?: Page) {
    if (!this.evaluacionRequest.nombreElaborador) {
      this.evaluacionRequest.nombreElaborador = null;
    }
    const r = { ...this.PLAN, ...this.evaluacionRequest };
    this.loading = true;
    return this.filtrarPlanManejo(r, page)
      .pipe(finalize(() => (this.loading = false)))
      .pipe(
        tap((res) => {
          if (res.success && res.data.length > 0) {
            this.planes = res.data;
            this.totalRecords = res.totalRecords;
          } else {
            this.planes = [];
            this.totalRecords = 0;
            this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
          }
        })
      );
  }

  guardarPlanManejo(body: any) {
    return this.apiPlanManejo.registrarPlanManejo(body).pipe(
      tap({
        next: () => this.toast.ok(MSG.OK.CREATE),
        error: () => this.toast.error(MSG.ERR.CREATE),
      })
    );
  }

  filtrarPlanManejo(request: any, page?: Page) {
    return this.apiPlanManejo
      .filtrar(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  navigate(idPlan: number) {
    const uri = "planificacion/plan-manejo-forestal-intermedio";
    this.router.navigate([uri, idPlan]);
  }

  openModal(data: any) {
    this.ref = this.dialogService.open(ModalInfoPermisoComponent, {
      header: "Solicitud de Permiso Forestal",
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        buscar: false,
        idPlanManejo: data.idPlanManejo,
        idPermisoForestal: data.idPermisoForestal,
      },
    });
  }

  registroImpugnacionEmiter(event: any) {
    this.limpiar();
  }
}
