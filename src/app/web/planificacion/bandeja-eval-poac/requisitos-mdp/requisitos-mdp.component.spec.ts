import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitosMdpComponentPoac } from './requisitos-mdp.component';

describe('RequisitosMdpComponent', () => {
  let component: RequisitosMdpComponentPoac;
  let fixture: ComponentFixture<RequisitosMdpComponentPoac>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequisitosMdpComponentPoac ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitosMdpComponentPoac);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
