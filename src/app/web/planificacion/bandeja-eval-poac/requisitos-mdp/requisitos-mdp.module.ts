import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import {MatTabsModule} from '@angular/material/tabs';
import { SharedModule } from "@shared";
import { RequisitosMdpComponentPoac } from "./requisitos-mdp.component";
import { MesaPartesPoacModule } from "./tabs/mesa-partes-poac/mesa-partes-poac.module";

@NgModule({
  exports:[RequisitosMdpComponentPoac],
  declarations: [RequisitosMdpComponentPoac],
  imports: [CommonModule, MatTabsModule, SharedModule,MesaPartesPoacModule],
})
export class RequisitosMdpPoacModule {}
