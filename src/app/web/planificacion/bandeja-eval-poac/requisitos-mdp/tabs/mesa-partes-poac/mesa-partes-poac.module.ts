import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { AccordionModule } from 'primeng/accordion';
import { SharedModule } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { RequisitoPgmfSimpleModule } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.module';
import { MesaPartesPoacComponent } from './mesa-partes-poac.component';

@NgModule({
  declarations: [
    MesaPartesPoacComponent
  ],
  exports: [
    MesaPartesPoacComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PanelModule,
    ConfirmPopupModule,
    RadioButtonModule,
    ReactiveFormsModule,
    FormsModule,
    RequisitoPgmfSimpleModule,
    AccordionModule,
    MatTabsModule,
    MatDatepickerModule,
    ButtonModule,
    TableModule,
  ]
})
export class MesaPartesPoacModule { }
