import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesaPartesPoacComponent } from './mesa-partes-poac.component';

describe('MesaPartesPgmfaComponent', () => {
  let component: MesaPartesPoacComponent;
  let fixture: ComponentFixture<MesaPartesPoacComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesaPartesPoacComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesaPartesPoacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
