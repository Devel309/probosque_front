import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { observados, ToastService } from "@shared";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import {
  ConsideracionesGenerales,
  EvaluacionObj,
} from "src/app/model/evaluacionInfoDoc";
import { CodigoEstadoEvaluacion } from "src/app/model/util/CodigoEstadoEvaluacion";
import { EvaluacionCampoService } from "src/app/service/evaluacion-campo.service";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";

@Component({
  selector: "app-tab-gestionar-inspeccion",
  templateUrl: "./tab-gestionar-inspeccion.component.html",
  styleUrls: ["./tab-gestionar-inspeccion.component.scss"],
})
export class TabGestionarInspeccionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;@Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;

  accordeon_6: ConsideracionesGenerales = new ConsideracionesGenerales();
  listaInspecciones: ConsideracionesGenerales = new ConsideracionesGenerales();
  acordeon_6Tirulo: string = "";
  listaInspeccionesTirulo: string = "";

  acordeon_Tirulo: string = "";
  acordeon_body: string = "";

  evaluacion: EvaluacionObj = new EvaluacionObj();

  idEvaluacionCampo: number = 0;
  constructor(
    private evaluacionService: EvaluacionService,
    private dialog: MatDialog,
    private usuarioService: UsuarioService,
    private toast: ToastService,
    private evaluacionCampoService: EvaluacionCampoService
  ) {}

  ngOnInit(): void {
    this.listarEvaluacion();
    this.consideracionesGenerales();
    this.consideraciones();
  //  this.obtenerEvaluacionOcular();
  }

  // Inspeccion ocular

  obtenerEvaluacionOcular() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionCampoService.obtenerEvaluacionOcular(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          if (!!result.data) {
            this.idEvaluacionCampo = result.data.idEvaluacionCampo;
          }
        } else {
          this.toast.warn(result.message);
        }
      },
      (error) => this.toast.error(error)
    );
  }

  btnSolInspOcular() {
    let params = {
      idEvaluacionCampo: this.idEvaluacionCampo ? this.idEvaluacionCampo : 0,
      documentoGestion: this.idPlanManejo,
      tipoDocumentoGestion: this.codigoEpica,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionCampoService.registroEvaluacionCampo(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok("Se registró la Solicitud de Inspección Ocular correctamente");
        } else {
          this.toast.warn("No se registró la Solicitud de Inspección Ocular");
        }
      },
      (error) => this.toast.error(error)
    );
  }

  // listados
  consideracionesGenerales() {
    const param = {
      prefijo: "GENEDP",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .listarParametroLineamiento(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.map((response: any) => {
            response.listNivel2.map((value: any) => {
              if (value.codigo == "GENEDP534") {
                this.accordeon_6 = value;
                this.acordeon_6Tirulo = value.valorPrimario.substring(0, 3);
              }
            });
          });
        }
      });
  }

  consideraciones() {
    const param = {
      prefijo: "GENEDP",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .listarParametroLineamiento(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.map((response: any) => {
            response.listNivel2.map((value: any) => {
              if (value.codigo == "GENEDP535") {
                this.acordeon_Tirulo = value.valorPrimario;
                this.acordeon_body = value.valorSecundario;
              }
            });
          });
        }
      });
  }

  inspeccionesFacultativas() {
    const param = {
      prefijo: "INSFADP",
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .listarParametroLineamiento(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.map((value: any) => {
            this.listaInspecciones = value;
            this.listaInspeccionesTirulo = value.valorPrimario;
          });
        }
      });
  }

  guardarEvaluacion() {
    let evaluacionDet: any = [];

    this.accordeon_6.listNivel3.forEach((item1: any) => {
      var objDet: any = {};
      objDet.idEvaluacionDet = item1.idEvaluacionDet
        ? item1.idEvaluacionDet
        : 0;
      objDet.codigoEvaluacionDet = this.codigoEpica;
      objDet.codigoEvaluacionDetSub = this.codigoEpica + "INSOCU";
      objDet.codigoEvaluacionDetPost = item1.codigo;
      objDet.conforme = item1.codigoSniffsDescripcion;
      objDet.observacion = item1.observacion;
      objDet.idUsuarioRegistro = this.usuarioService.idUsuario;
      evaluacionDet.push(objDet);
    });

    this.evaluacion.listarEvaluacionDetalle = evaluacionDet;
    if (!observados(this.evaluacion.listarEvaluacionDetalle, this.toast)) {
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .registrarEvaluacionPlanManejo(this.evaluacion)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response.message);
          this.inspeccionesFacultativas();
        } else {
          this.toast.warn(response.message);
        }
      });
  }
  listarEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoEpica,
      codigoEvaluacionDetSub: this.codigoEpica + "INSOCU",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .obtenerEvaluacion(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
          } else {
            this.evaluacion.idEvaluacion = 0;
            this.evaluacion.idPlanManejo = this.idPlanManejo;
            this.evaluacion.codigoEvaluacion = this.codigoEpica;
            this.evaluacion.idUsuarioRegistro = this.usuarioService.idUsuario;
            this.evaluacion.listarEvaluacionDetalle = [];
          }
        }
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
