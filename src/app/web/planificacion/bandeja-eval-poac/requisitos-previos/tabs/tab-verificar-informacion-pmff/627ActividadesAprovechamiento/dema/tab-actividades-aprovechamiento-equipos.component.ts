import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService } from 'primeng/api';

import { forkJoin } from 'rxjs';
import { concatMap, finalize, tap } from 'rxjs/operators';

import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SistemaManejoForestalService, UsuarioService, ParametroValorService } from '@services';
import { ParametroValor, SistemaManejoForestal, SistemaManejoForestalDetalle } from '@models';
import { AccionTipo, compareObjects, DEMATipo, handlerSaveResult, isNullOrEmpty, tipoAAE, ToastService } from '@shared';
//import { ModalActividadesAprovechamiento } from './modal-actividades-aprovechamiento/modal-actividades-aprovechamiento';
import {EvaluacionUtils} from '../../../../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../../../../model/util/Mensajes';
import {CodigosDEMA} from '../../../../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../../../../service/evaluacion/evaluacion.service';
@Component({
  selector: 'app-tab-actividades-aprovechamiento-equipos',
  templateUrl: './tab-actividades-aprovechamiento-equipos.component.html',
  styleUrls: ['./tab-actividades-aprovechamiento-equipos.component.scss']
})
export class TabActividadesAprovechamientoEquiposComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();

  AccionTipo = AccionTipo;
  tipoAAE = tipoAAE;
  /* actividad de aprovechamiento y equipo */
  aae: SistemaManejoForestal = new SistemaManejoForestal({ codigoProceso: DEMATipo.AAE });
  actividades: SistemaManejoForestalDetalle[] = [];
  tiposActividades: ParametroValor[] = [];
  pendiente: boolean = false;
  rowGroup: any = {};




  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_7;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_7_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;
  constructor(public dialogService: DialogService,
    private dialog: MatDialog,
    private confirm: ConfirmationService,
    private api: SistemaManejoForestalService,
    private apiParametro: ParametroValorService,
    private toast: ToastService,
    private user: UsuarioService,private evaluacionService: EvaluacionService
  ) { }

  ngOnInit(): void {
    /*if(this.isPerfilArffs)
    this.obtenerEvaluacion();*/

    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin([this.obtenerAAE(this.idPlanManejo, DEMATipo.AAE), this.listarTiposActividades()])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe();
  }


  //#region REQUESTS BEGIN
  obtenerAAE(idPlanManejo: number, codigoProceso: DEMATipo) {
    return this.api.obtener(idPlanManejo, codigoProceso)
      .pipe(tap(res => {
        this.aae = res?.data;
        this.actividades = res?.data?.detalle as SistemaManejoForestalDetalle[];
        this.setEnviar(this.actividades);
      }))
  }

  guardarAAE(body: SistemaManejoForestal) {
    return this.api.guardar(body)
      .pipe(tap({
        next: (res: any) => this.toast.ok(res?.message ? res.message : 'Se actualizó: Actividades de Aprovechamiento y Equipos'),
        error: (err: any) => {
          this.toast.error(err?.message ? err.message : 'Ocurrió un error al realizar la operación')
          console.error(err);
        }
        }));
  }

  eliminarAAEDetalle(idDetalle: number | null, idUsuario: number) {
    return this.api.eliminarDetalle(idDetalle, idUsuario)
      .pipe(tap({
        next: (res: any) => this.toast.ok(res?.message ? res.message : 'Se eliminó la Actividad de Aprovechamiento'),
        error: (err: any) => {
          this.toast.error(err?.message ? err.message : 'Ocurrió un error al realizar la operación')
          console.error(err);
        }
        }));
  }

  listarTiposActividades() {
    return this.apiParametro.listarParametros(DEMATipo.DEMA)
      .pipe(tap(res => this.tiposActividades = res?.data.filter(x => x.codigo.includes(DEMATipo.AAEA))));
  };
  //#endregion REQUESTS END

  setEnviar(items: SistemaManejoForestalDetalle[]) {
    if (isNullOrEmpty(items)) return;
    items.forEach(x => x.enviar = isNullOrEmpty(x.idSistemaManejoForestalDetalle))
  }



  agregarDetalle(accion: AccionTipo, detalle: SistemaManejoForestalDetalle, index?: number) {
    if (accion == AccionTipo.REGISTRAR) {
      this.actividades.push(detalle);

    } else {
      if (isNullOrEmpty(detalle.idSistemaManejoForestalDetalle) && index) {
        this.actividades[index] = detalle;
      } else {
        const posicion = this.actividades.findIndex(x =>
          (x.idSistemaManejoForestalDetalle == detalle.idSistemaManejoForestalDetalle));
        this.actividades[posicion] = detalle;
      }
    }
    this.actividades = [...this.actividades];
  }

  guardar() {
    this.aae.codigoProceso = DEMATipo.AAE;
    this.aae.idUsuarioRegistro = this.user.idUsuario;
    this.aae.detalle = this.actividades.filter(x => x.enviar);

    this.aae.detalle.forEach(x => {
      if (x.idSistemaManejoForestalDetalle == null) {
        x.idUsuarioRegistro = this.user.idUsuario;
      } else {
        x.idUsuarioModificacion = this.user.idUsuario;
      }
    })

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.guardarAAE(this.aae)
      .pipe(finalize(() => this.dialog.closeAll()),
        concatMap(() => this.obtenerAAE(this.idPlanManejo, DEMATipo.AAE))
      ).subscribe(() => this.pendiente = false);
  }


  eliminar(target: any, id: number, index: number) {
    this.confirm.confirm({
      target,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => this.eliminarRegistro(id, index)
    });
  }

  eliminarRegistro(id: number, index: number) {
    if (isNullOrEmpty(id)) {
      this.actividades.splice(index, 1);
      this.toast.ok('Se eliminó la Actividad de Aprovechamiento con éxito.');
      this.pendiente = false;
      for (const el of this.actividades) {
        if (el?.enviar) {
          this.pendiente = true;
          break;
        }
      }
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.eliminarAAEDetalle(id, this.user.idUsuario)
        .pipe(finalize(() => this.dialog.closeAll()),
          concatMap(() => this.obtenerAAE(this.idPlanManejo, DEMATipo.AAE))
        ).subscribe(() => this.pendiente = false);
    }
  }

  loadData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    setTimeout(() => this.dialog.closeAll(), 250);
  }

  onSort() {
    this.rowGroup = {};
    if (this.actividades) {
      for (let i = 0; i < this.actividades.length; i++) {
        let rowData = this.actividades[i];
        let prop = rowData.codigoTipoDetalle as string;
        if (i == 0) {
          this.rowGroup[prop] = { index: 0, size: 1 };
        }
        else {
          let previousRowData = this.actividades[i - 1];
          let previousRowGroup = previousRowData.codigoTipoDetalle;
          if (prop === previousRowGroup)
            this.rowGroup[prop].size++;
          else
            this.rowGroup[prop] = { index: i, size: 1 };
        }
      }
    }
  }


  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

}
