import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RecursoForestalService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/recursos-forestales.service';
import {ToastService} from '@shared';
import {CodigosDEMA} from '../../../../../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../../../../../service/evaluacion/evaluacion.service';
import {EvaluacionUtils} from '../../../../../../../../../model/util/EvaluacionUtils';
import {LoadingComponent} from '../../../../../../../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {Mensajes} from '../../../../../../../../../model/util/Mensajes';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-tab-recursos-forestales-maderables',
  templateUrl: './tab-recursos-forestales-maderables.component.html',
  styleUrls: ['./tab-recursos-forestales-maderables.component.scss']
})
export class TabRecursosForestalesMaderablesComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;@Input() isPerfilArffs!: boolean;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();
  lstGrilla:any[] = [];
  lstGrillaResumen:any[] = [];



  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_1;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_1_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;


  constructor(private recursoForestalService: RecursoForestalService,
              private toast: ToastService,private evaluacionService: EvaluacionService,private dialog: MatDialog) { }

  ngOnInit(): void {
    this.consultarServicio();

    /*if(this.isPerfilArffs)
      this.obtenerEvaluacion();*/

  }


  consultarServicio():void{

    this.lstGrilla = [];
    this.lstGrillaResumen = [];

    const params = new HttpParams().set('idPlanManejo',this.idPlanManejo.toString()).set('tipoCenso','').set('tipoEspecie','1');
    const params2 = new HttpParams().set('idPlanManejo',this.idPlanManejo.toString()).set('tipoCenso','').set('tipoRecurso','1');

    //?idPlanManejo=2&tipoCenso=2&tipoEspecie=1

    this.recursoForestalService.listarRecurosForestales(params2).subscribe( (response: any) => {

      if(response.data.length>0){
          this.toast.ok("Se listó la información de Recursos Forestales Maderables");
          this.lstGrilla = response.data;

         /* response.data.forEach((element: any) => {
            this.lstGrilla.push(element);
          });*/
      }else{
        this.toast.warn("No existe información de Recursos Forestales Maderables");
      }

    })

    this.recursoForestalService.listarRecurosResumen(params).subscribe( (response: any) => {


      this.lstGrillaResumen = response.data;
      /*response.data.forEach((element: any) => {
        this.lstGrillaResumen.push(element);
      });*/
    })


    //
    //for (let index = 0; index < Math.floor(Math.random() * (100 - 1)) + 1; index++) {

     // this.lstGrilla.push({});

    //}
  }




  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

}
