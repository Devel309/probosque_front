import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ImpactosAmbientalesNegativosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/impactos-ambientales-negativos.service";
//import { ModalFormularioImpactosNegativosComponent } from "./modal/modal-formulario-impactos-negativos/modal-formulario-impactos-negativos.component";
import {EvaluacionUtils} from '../../../../../../../../model/util/EvaluacionUtils';
import {LoadingComponent} from '../../../../../../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {Mensajes} from '../../../../../../../../model/util/Mensajes';
import {CodigosDEMA} from '../../../../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../../../../service/evaluacion/evaluacion.service';
import {ToastService} from '@shared';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: "app-tab-impactos-ambientales-negativos",
  templateUrl: "./tab-impactos-ambientales-negativos.component.html",
  styleUrls: ["./tab-impactos-ambientales-negativos.component.scss"],
})
export class TabImpactosAmbientalesNegativosComponent implements OnInit {
  @Input() idPlanManejo!: number;@Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  context: Modelo = new Modelo();
  lstGrilla: Modelo[] = [];
  ref!: DynamicDialogRef;
  pendiente: boolean = false;

  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_8;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_8_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,private toast: ToastService,private dialog: MatDialog,
    private impactosAmbientalesNegativosService: ImpactosAmbientalesNegativosService,
    private user: UsuarioService,private evaluacionService: EvaluacionService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {

   /* if(this.isPerfilArffs)
      this.obtenerEvaluacion();*/
    this.listarImpactoAmbiental();
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    /*this.ref = this.dialogService.open(
      ModalFormularioImpactosNegativosComponent,
      {
        header: mesaje,
        width: "50%",
        contentStyle: { "max-height": "500px", overflow: "auto" },
        data: {
          data: data,
          type: tipo,
        },
      }
    );

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.pendiente = true;
          this.guardar();
        } else if (tipo == "E") {
          this.context = new Modelo(resp);
          this.pendiente = true;
        //  this.guardar();
        }
      }
    });*/
  };

  guardar(): void {
    if (this.context.idPlanManejo == 0) {
      this.context.idPlanManejo = this.idPlanManejo;
      this.lstGrilla.push(this.context);
    }
  }

  openEliminar(event: Event, data: Modelo): void {
    

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idImpactoambiental != 0) {
          var params = {
            idImpactoambiental: data.idImpactoambiental,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.impactosAmbientalesNegativosService
            .eliminarImpactoAmbiental(params)
            .subscribe((response: any) => {
              if (response.success) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail: "Se eliminó la actividad de impacto ambiental negativo correctamente.",
                });
                this.listarImpactoAmbiental();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  detail: "Ocurrió un problema, intente nuevamente",
                });
              }
            });
        } else {
          this.lstGrilla.splice(
            this.lstGrilla.findIndex(
              (x) => x.actividad == data.actividad
            ),
            1
          );
          this.pendiente = true;
        }
      },
      reject: () => {},
    });
  }

  listarImpactoAmbiental() {
    this.lstGrilla = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
    };
    this.impactosAmbientalesNegativosService
      .listarImpactoAmbiental(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.lstGrilla.push({
            idImpactoambiental: element.idImpactoambiental
              ? element.idImpactoambiental
              : 0,
            idPlanManejo: element.idPlanManejo
              ? element.idPlanManejo
              : this.idPlanManejo,
            actividad: element.actividad ? element.actividad : "",
            descripcion: element.descripcion ? element.descripcion : "",
            detalle: element.detalle ? this.item(element.detalle) : [],
            idUsuarioModificacion: element.idUsuarioModificacion
              ? element.idUsuarioModificacion
              : this.user.idUsuario,
            idUsuarioRegistro: element.idUsuarioRegistro
              ? element.idUsuarioRegistro
              : this.user.idUsuario,
          });
        });
      });
  }

  item(element: any) {
    let array: any[] = [];
    element.forEach((element: any) => {
      array.push({
        idImpactoambiental: element.idImpactoambiental
          ? element.idImpactoambiental
          : 0,
        idImpactoambientaldetalle: element.idImpactoambientaldetalle
          ? element.idImpactoambientaldetalle
          : 0,
        idUsuarioModificacion: element.idUsuarioModificacion
          ? element.idUsuarioModificacion
          : this.user.idUsuario,
        idUsuarioRegistro: element.idUsuarioRegistro
          ? element.idUsuarioRegistro
          : this.user.idUsuario,
        medidasprevencion: element.medidasprevencion
          ? element.medidasprevencion
          : "",
      });
    });
    return array;
  }

  registrarImpactoAmbiental() {
    this.impactosAmbientalesNegativosService
      .registrarImpactoAmbiental(this.lstGrilla)
      .subscribe((response: any) => {
        if (response.success) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó: Impactos Ambientales Negativos",
          });
          this.pendiente = false;
          this.listarImpactoAmbiental();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            detail: "Ocurrió un problema, intente nuevamente",
          });
          this.pendiente = false;
        }
      });
  }
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }



}

export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.actividad = data.actividad;
      this.descripcion = data.descripcion;
      this.detalle = data.detalle;
      return;
    } else if (data && tipo == "E") {
      this.actividad = data.actividad;
      this.descripcion = data.descripcion;
      this.detalle = data.detalle;
      return;
    } else if (data) {
      this.actividad = data.actividad;
      this.descripcion = data.descripcion;
      this.detalle = data.medidasPrevencion;
      return;
    }
  }

  actividad: string = "";
  descripcion: string = "";
  detalle: any[] = [];
  idImpactoambiental: number = 0;
  idPlanManejo: number = 0;
  idUsuarioModificacion: number = 0;
  idUsuarioRegistro: number = 0;
}
