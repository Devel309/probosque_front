import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, Input } from '@angular/core';
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
//import { ModalFormularioZonasComponent } from "./modal/modal-formulario-zonas/modal-formulario-zonas.component";
import { MapApi } from "src/app/shared/mapApi";
import { ArchivoService, UsuarioService } from '@services';
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { ZonificacionOrdenamientoInternoAreaService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/zonificacion-ordenamiento-interno-area.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/informacion-general.service';
import { ResultArchivoModel } from "src/app/model/ResultArchivo";
import {DownloadFile, PGMFArchivoTipo, ToastService} from '@shared';
import { ZonaModel } from 'src/app/model/dema/zonificacion/ZonaModel';
import { DataTableDirective } from 'angular-datatables';
import {EvaluacionUtils} from '../../../../../../../../model/util/EvaluacionUtils';
import {concatMap, finalize} from 'rxjs/operators';
import {Mensajes} from '../../../../../../../../model/util/Mensajes';
import {CodigosDEMA} from '../../../../../../../../model/util/DEMA/DEMA';
import {EvaluacionArchivoModel} from '../../../../../../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionService} from '../../../../../../../../service/evaluacion/evaluacion.service';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { HttpErrorResponse } from '@angular/common/http';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { from } from 'rxjs';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';

@Component({
  selector: "app-tab-zonificacion-ordenamiento-interno-area",
  templateUrl: "./tab-zonificacion-ordenamiento-interno-area.component.html",
  styleUrls: ["./tab-zonificacion-ordenamiento-interno-area.component.scss"],
})
export class TabZonificacionOrdenamientoInternoAreaComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;
  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild("ulResult", { static: true }) private ulResult!: ElementRef;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  public view: any = null;
  _files: any = [];
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  public filFile: any = null;
  zona: ZonaModel = {} as ZonaModel;
  listZona: ZonaModel[] = {} as ZonaModel[]
  ref!: DynamicDialogRef;
  tituloModalMantenimiento: string = "";
  verModalMantenimiento: boolean = false;
  cmbZonPadre: any[] = [];
  listSubzonas: any[] = [];
  listGuardar: any[] = [];
  lstParams: any[] = [];
  nroAnexosComunidad: any;
  sizeAnexos: any[] = [];
  totalAreaZona: any;
  totalPorcentajeZona: any;




  /* EVALUACION */
  codigoProceso = CodigosDEMA.CODIGO_PROCESO;
  codigoTab = CodigosDEMA.TAB_1;
  codigoAcordeon1: string =CodigosDEMA.ACORDEON_1_1;
  codigoAcordeon2: string =CodigosDEMA.ACORDEON_2_1;

  evaluacion_1 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon1
  });

  evaluacion:any;

  constructor(private confirmationService: ConfirmationService, private mapApi: MapApi, private serviceArchivo: ArchivoService,
    private user: UsuarioService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    public dialogService: DialogService,
    private ZonificacionService: ZonificacionOrdenamientoInternoAreaService,
    private messageService: MessageService,
    private informacionGeneralService: InformacionGeneralService,
    private dialog: MatDialog,private toast: ToastService,private evaluacionService: EvaluacionService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
  ) { }

  ngOnInit(): void {
    this.initializeMap();
    this.obtenerCapas();
    //this.obtenerArchivoCapas();
    //this.getAllFiles();
    this.obtenerAnexos();
    this.listarZona();

    /*if(this.isPerfilArffs)
      this.obtenerEvaluacion();*/
  }
  static get TipoCodigo() {
    return {
      TZONDEMA: 'TZONDEMA',
      ZOIAREA: 'ZOIAREA'
    };
  }
  obtenerAnexos() {
    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: "DEMA",
      idPlanManejo: this.idPlanManejo
    }
    this.informacionGeneralService.listarInformacionGeneralDema(params).subscribe((response: any) => {
      response.data.forEach((element: any) => {
        this.nroAnexosComunidad = element.nroAnexosComunidad
      });
      for (let i = 1; i <= this.nroAnexosComunidad; i++) {
        this.sizeAnexos.push({ index: i });
      }
    })
  }
  listarZona() {
    this.listZona = [];
    let params = {
      idPlanManejo: this.idPlanManejo
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.ZonificacionService.listarZona(params).subscribe((result: any) => {
      this.listZona = result.data;
      this.listZona.forEach((t: any, i: any) => {
        if (t.idPlanManejo === null) {
          t.codigoZona = `Z${t.idZona}`
        }
      });
      for (let i = 1; i <= this.nroAnexosComunidad; i++) {
        this.listZona.forEach((item2: any) => {
          if (item2.idPlanManejo === null) {
            let zonaAnexo: any = {
              idZonaAnexo: 0,
              valor: '',
              nombre: `Anexo ${i}`,
              idZona: 0
            }
            if (item2.zonaAnexo === null) {
              item2.zonaAnexo = [];
            }
            item2.idZona = 0;
            item2.zonaAnexo.push(zonaAnexo);
          } else if (item2.idPlanManejo > 0) {
            if (!item2.zonaAnexo.length) {
              let zonaAnexo: any = {
                idZonaAnexo: 0,
                valor: '',
                nombre: `Anexo ${i}`,
                idZona: item2.idZona,
                inServer: false
              }
              if (item2.zonaAnexo === null) {
                item2.zonaAnexo = [];
              }
              item2.zonaAnexo.push(zonaAnexo);
            } else {
              if (item2.zonaAnexo[0].inServer === false) {
                let zonaAnexo: any = {
                  idZonaAnexo: 0,
                  valor: '',
                  nombre: `Anexo ${i}`,
                  idZona: item2.idZona,
                  inServer: false
                }
                if (item2.zonaAnexo === null) {
                  item2.zonaAnexo = [];
                }
                item2.zonaAnexo.push(zonaAnexo);
              }
            }
          }
        });
      }
      this.dialog.closeAll();
      this.getAllFiles();
    })
  }

  private initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  onChangeFile(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      opcion: e.target.dataset.zone || null
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }

  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer: any = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.opcion;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.opcion;
    this._filesSHP.push(file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
    }, (error: HttpErrorResponse) => {
      this.toast.error(error.message);
      this.dialog.closeAll();
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._layers.length === 0) {
                  this.cleanLayers();
                }
                this.toast.ok('El archivo se eliminó correctamente.');
              } else {
                this.toast.error('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error:HttpErrorResponse) => {
              this.dialog.closeAll();
              this.toast.error('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._layers.length === 0) {
        this.cleanLayers();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }



  openEliminar(event: Event, data: ZonaModel): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        if (data.idPlanManejo !== null) {
          if (data.idZona != null || data.idZona !== 0) {
            var params = {
              idUsuarioElimina: this.user.idUsuario,
              idZona: data.idZona,
              idPlanManejo: this.idPlanManejo
            }
            this.ZonificacionService.eliminarZona(params).subscribe((response: any) => {
              if (response.success) {
                this.messageService.add({
                  key: 'tl',
                  severity: 'success',
                  detail: 'Se eliminó la Zonificación u Ordenamiento Interno del Área correctamente.',
                });
                this.listarZona();
              } else {
                this.messageService.add({
                  key: 'tl',
                  severity: 'error',
                  summary: 'ERROR',
                  detail: 'Ocurrió un problema, intente nuevamente',
                });
              }
            })
          }
        }
        else {
          this.listZona.splice(
            this.listZona.findIndex((x) => x.idZona == data.idZona),
            1
          );
        }
      },
      reject: () => { },
    });
  }

  private CargarComboPadre() {
    this.cmbZonPadre = [
      {
        code: 0,
        name: "Seleccionar",
      },
    ];
    this.listZona.forEach((d: any, i: any) => {
      if (d.idZonaPadre === 0) {
        this.cmbZonPadre.push({
          code: d.idZona,
          name: d.idZona + " - " + d.nombre,
        });
      }
    });
  }
  onChangeFileSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      inServer: false,
      service: false,
      idLayer: this.mapApi.Guid2.newGuid,
      codZona: e.target.dataset.zone,
      codigo: e.target.id
    }
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFileSHP(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  calculateArea(data: any, config: any) {
    let controls: any = {};
    let sumArea: any = null;
    data[0].features.forEach((t: any) => {
      let geometry: any = null;
      if ((t.geometry.type = "Polygon")) {
        geometry = { spatialReference: { wkid: 4326 } };
        geometry.rings = t.geometry.coordinates;
        let area = this.mapApi.calculateArea(geometry, "hectares");
        sumArea += area;
        controls.success = true;
      } else {
        controls.success = false;
      }
    });
    let anexos: any = []
    data[0].features.forEach((name: any) => {
      for (let item in name.properties) {
        if (item.toUpperCase().startsWith("ANEXO")) {
          anexos.push(name.properties[item]);
        };
      };
    });
    this.listZona.forEach((item: any) => {
      if (item.codigoZona === config.codZona) {
        item.total = Number(sumArea.toFixed(2));
        item.archivo = config.file;
        item.inServer = config.inServer;
        item.idLayer = config.idLayer;
        for (let i = 0; i < item.zonaAnexo.length; ++i) {
          if (anexos[i] !== undefined) {
            item.zonaAnexo[i].valor = anexos[i];
          }
        }
      }
      this.calculateAreaTotalZona();
    });
  }
  calculateAreaTotalZona() {
    let sum1 = 0;
    let sum2 = 0;

    for (let item of this.listZona) {
      sum1 += Number(item.total);
    }
    for (let item of this.listZona) {
      item.porcentaje = Number(((100 * Number(item.total)) / sum1).toFixed(2)) || 0;
      sum2 += Number(item.porcentaje);
    }
    this.totalAreaZona = `${sum1.toFixed(2)}`;
    this.totalPorcentajeZona = `${sum2.toFixed(2)} %`;
  }
  processFileSHP(file: any, config: any) {
    config.file = file;
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.createLayers(data, config);
      if (config.codZona !== undefined) {
        this.calculateArea(data, config);
      }
    });
  }
  createLayers_old(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.file = config.file;
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = config.idLayer;
      t.groupId = this._id;
      t.codZona = config.codZona;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this.createLayer(t);
    });
  }
  createLayer_old(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = item.groupId;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.service = item.service;
    geoJsonLayer.inServer = item.inServer;
    geoJsonLayer.fileName = item.fileName;
    geoJsonLayer.idArchivo = item.idArchivo;
    geoJsonLayer.codZona = item.codZona;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(geoJsonLayer.id, geoJsonLayer.color, this.view);
        this.view.goTo({ target: data.fullExtent });
        this.createTreeLayers();
      })
      .catch((error: any) => {
        console.log(error);
      });
    this.view.map.add(geoJsonLayer);
  }
  createTreeLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    let ulResult = this.ulResult.nativeElement;
    this._files = [];
    ulResult.empty();
    layers.forEach((t: any) => {
      if (t.codZona !== undefined) return;
      let li = ulResult.prependHTML(`<li class="list-group-item p-2"></li>`);
      let objFile = {
        id: t.inServer === false ? 0 : t.idArchivo,
        name: t.inServer === false ? t.file.name : t.fileName,
        guid: this.mapApi.Guid2.newGuid,
        inServer: t.inServer,
        url: window.URL.createObjectURL(t.file),
        type: t.file.type,
        file: t.file,
        codigo: 'SHP',
        nombreFile: t.inServer === false ? t.file.name : t.fileName,
      };
      this._files.push(objFile);
      let btnDelete = li.appendHTML(
        `<span class="float-right" data-guid="${objFile.guid}" data-idArchivo = "${objFile.id}" data-inServer = ${t.inServer} id="${t.id}"><i class="pi pi-trash"></i></span>`
      );
      if (t.inServer === true) {
        let btnDownload = li.appendHTML(
          `<span class="float-right mr-2" data-name = "${objFile.name}" id="${t.id}" data-url= "${objFile.url}" data-mime-type ="${objFile.type}" data-guid="${objFile.guid}"><i class="pi pi-download"></i></span>`
        );
        btnDownload.addEventListener("click", (e: any) => {
          this.onDownloadFile(e, null);
        });
      }
      btnDelete.addEventListener("click", (e: any) => {
        let inServer = e.target.parentNode.dataset.inserver;
        if (inServer === "true") {
          this.confirmationService.confirm({
            message: '¿Está seguro de eliminar este archivo?',
            icon: 'pi pi-exclamation-triangle',
            key: "deleteFileSHP",
            acceptLabel: 'Si',
            rejectLabel: 'No',
            accept: () => {
              this.onElementRemoveClick(e);
              let id = e.target.parentNode.dataset.idarchivo;
              this.eliminarArchivoDetalle(id);
            }
          });
        } else {
          this.onElementRemoveClick(e);
        }
      });
      let chk = li.appendHTML(`<input class="float-left mr-1 mt-1" type="checkbox" id="${t.id}" />`);
      chk.checked = t.visible;
      chk.addEventListener("change", (e: any) => {
        this.mapApi.toggleLayer(e.currentTarget.id, e.currentTarget.checked, this.view);
      });
      let text = li.appendHTML('<label class="float-left m-0 p-0">' + `${t.title}` + '</label>');
      text.style.color = t.color;
    });
  }
  onElementRemoveClick(e: any) {
    e.preventDefault();
    let guid = e.target.parentNode.dataset.guid;
    this._files.splice(guid, 1);
    let index = this._files.findIndex((t: any) => t.guid === guid);
    this._files.splice(index, 1);
    this.mapApi.removeLayer(e.target.parentNode.id, this.view);
    this.createTreeLayers();
  }
  onDownloadFile(e: any, data: any) {
    if (data !== null) {
      let file: any = data.archivo;
      DownloadFile(file, data.nombre, 'application/x-zip-compressed');
    } else {
      let url = e.target.parentNode.dataset.url;
      let name = e.target.parentNode.dataset.name;
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.href = url;
      a.download = name;
      a.click();
    }
  }
  onDeleteFileZona(data: any) {
    this.confirmationService.confirm({
      message: '¿Está seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      key: "deleteZona",
      accept: () => {
        this.listZona.forEach((item: any) => {
          if (item.idZona === data.idZona) {

            data.archivo = null;
            data.total = 0;
            data.porcentaje = 0;
            if (data.zonaAnexo !== null) {
              if (data.zonaAnexo.length) {
                data.zonaAnexo.forEach((t: any) => {
                  t.valor = '';
                  t.nombre = '';
                });
              }
            }
            if (data.inServer === true) {
              data.inServer = false;
              this.eliminarArchivoDetalle(data.idArchivo);
              this.guardar();
            }
            this.mapApi.removeLayer(data.idLayer, this.view);
            data.idLayer = null;
            this.calculateAreaTotalZona();
          }
        });
      }
    });
  }
  onChangeTogleLayer(e: any, idLayer: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {

    let codigoTipo: any = TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA;
    let item2 = {
      codigoTipoPGMF: codigoTipo,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      descripcion: item.descripcion
    };
    return this.servicePostulacionPFDM.registrarArchivoDetalle(item2).pipe(
      concatMap((response: any) => {
        return this.guardarGeometria(item, item2.idArchivo);
      })
    );
  }
  guardarGeometria(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoTab,
        codigoSubSeccion: this.codigoAcordeon2,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  guardarCapas(){
    let codigoTipo: any = TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA;
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: codigoTipo,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
          descripcion: t.descripcion
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.toast.error('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result:any) => {
          this.toast.ok(result.message);
          this.cleanLayers();
          this.obtenerCapas();
        },
        (error:HttpErrorResponse) => {
          this.toast.error('Ocurrió un error.');
        }
      );
  }
  obtenerCapas() {
    let item = {
        idPlanManejo: this.idPlanManejo,
        codigoSeccion: this.codigoTab,
        codigoSubSeccion: this.codigoAcordeon2,
      };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              let layer: any = {} as CustomCapaModel;
              layer.codigo = t.idArchivo;
              layer.idLayer = this.mapApi.Guid2.newGuid;
              layer.inServer = true;
              layer.service = false;
              layer.nombre = t.nombreCapa;
              layer.groupId = groupId;
              layer.color = t.colorCapa;
              layer.annex = false;
              layer.descripcion = t.descripcion;

              this._layers.push(layer);
              let geoJson = this.mapApi.getGeoJson(
                layer.idLayer,
                groupId,
                item
              );
              this.createLayer(geoJson);
            }
          });
        }
      },
      (error) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }
  guardarArchivoCapas() {
    let count = 0;
    let add = 0;
    for (let item of this._files) {
      if (item.inServer !== true) {
        count++;
      }
    }
    this._files.forEach((t: any) => {
      if (t.inServer !== true) {
        add++;
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: t.codigo,
          codigoTipoPGMF: TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA,
          file: t.file
        }
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.serviceArchivo.cargar(item.idUsuario, item.codigo, item.file).subscribe((result) => {
          let item2 = {
            codigoTipoPGMF: item.codigoTipoPGMF,
            codigoSubTipoPGMF: item.codigo,
            idArchivo: result.data,
            idPlanManejo: this.idPlanManejo,
            idUsuarioRegistro: item.idUsuario
          }
          this.servicePostulacionPFDM.registrarArchivoDetalle(item2).subscribe((data) => {
            this.dialog.closeAll();
          });
        });
      }
    });
    if (count === add) {
      this.messageService.add({
        key: 'tl',
        severity: 'success',
        detail: 'Se guardó las capas correctamente.',
      });
      setTimeout(() => { this.getAllFiles() }, 10000);
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter((t: any) => t.groupId === this._id);
    return layers;
  }
  getAllFiles() {
    this.cleanLayers();
    let item: any = {
      idPlanManejo: this.idPlanManejo,
      codigoTipoPGMF: TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servicePostulacionPFDM.obtenerArchivoDetalle(item.idPlanManejo, item.codigoTipoPGMF).subscribe((result: any) => {
      if (result.isSuccess) {
        result.data.forEach((t: any) => {
          if (t.documento != null) {
            if (t.codigoTipoPGMF === TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA && t.codigoSubTipoPGMF === TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.TZONDEMA) {
              let idLayer = this.mapApi.Guid2.newGuid;
              this.listZona.forEach((item: any) => {
                if (item.codigoZona == t.tipoDocumento) {
                  item.archivo = t.documento;
                  item.idLayer = idLayer;
                  item.inServer = true;
                  item.idArchivo = t.idArchivo;
                }
              });
              let blob = this.mapApi.readFileByte(t.documento);
              let config = {
                inServer: true,
                service: false,
                fileName: t.nombre,
                idArchivo: t.idArchivo,
                idLayer: idLayer,
                codZona: t.tipoDocumento
              }
              this.processFileSHP(blob, config);
            } else {
              let blob = this.mapApi.readFileByte(t.documento);
              let config = {
                inServer: true,
                service: false,
                fileName: t.nombre,
                idArchivo: t.idArchivo,
                idLayer: this.mapApi.Guid2.newGuid
              }
              this.processFileSHP(blob, config);
            }
          }
        });
      }
      this.dialog.closeAll();
    });

  }
  cleanLayers_old() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    let ulResult = this.ulResult.nativeElement;
    ulResult.empty();
  }
  eliminarArchivoDetalle_old(idArchivo: Number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servicePostulacionPFDM.eliminarArchivoDetalle(idArchivo, this.user.idUsuario).subscribe((response: any) => {
      if (response.success) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          detail: 'Se eliminó el archivo correctamente.',
        });
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
      this.dialog.closeAll();
    });
  }
  guardarArchivoZona() {
    let count = 0;
    let add = 0;
    for (let item of this.listZona) {
      if (item.inServer === false) {
        if (item.archivo !== null && item.archivo !== undefined) {
          count++;
        }
      }
    }
    this.listZona.forEach((t: any) => {
      if (t.inServer === false) {
        if (t.archivo !== null && t.archivo !== undefined) {
          add++;
          let item: any = {
            idUsuario: this.user.idUsuario,
            codigo: t.codigoZona,
            codigoTipoPGMF: TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.ZOIAREA,
            codigoSubTipoPGMF: TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.TZONDEMA,
            file: t.archivo
          }
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.serviceArchivo.cargar(item.idUsuario, item.codigo, item.file).subscribe((result) => {
            let item2 = {
              codigoTipoPGMF: item.codigoTipoPGMF,
              codigoSubTipoPGMF: item.codigoSubTipoPGMF,
              idArchivo: result.data,
              idPlanManejo: this.idPlanManejo,
              idUsuarioRegistro: item.idUsuario
            }
            this.servicePostulacionPFDM.registrarArchivoDetalle(item2).subscribe((data) => {
              this.dialog.closeAll();
            });
          });
        }
      }
    });
    if (count === add) {
      setTimeout(() => {this.listarZona();}, 10000);
    }
  }
  guardarZona() {
    this.listZona.forEach((item: any, i: any) => {
      item.idPlanManejo = this.idPlanManejo;
      item.idUsuarioRegistro = this.user.idUsuario;
      item.codigoZona = item.codigoZona || TabZonificacionOrdenamientoInternoAreaComponent.TipoCodigo.TZONDEMA;
    });
    this.ZonificacionService.registrarZona(this.listZona).subscribe((response: any) => {
      if (response.success) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          detail: 'Se actualizó la Zonificación u Ordenamiento Interno del Área.',
        });
        this.guardarArchivoZona();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  }
  guardar() {
    this.guardarZona()
    //this.guardarArchivoZona();
    //this.guardarArchivoCapas();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion_1 = Object.assign(this.evaluacion_1,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon1));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion_1])){

      if(this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

}

