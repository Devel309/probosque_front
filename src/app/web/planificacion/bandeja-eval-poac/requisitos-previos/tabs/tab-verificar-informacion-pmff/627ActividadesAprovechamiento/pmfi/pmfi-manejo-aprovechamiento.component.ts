import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { DateRange } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { AccionTipo } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize, tap } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Actividades, Detalle } from 'src/app/model/ActividadesSilviculturalesModel';
import { LaborSilviculturalDto } from 'src/app/model/LaborSilviculturalDto';
import { LaborSilviculturalServiceService } from 'src/app/service/labor-silvicultural-service.service';
import { EventEmitterService } from 'src/app/shared/services/event-emitter.service';
//import { LaboresSilviculturalesFormComponent } from './labores-silviculturales-form/labores-silviculturales-form.component';

@Component({
  selector: 'app-pmfi2-manejo-aprovechamiento',
  templateUrl: './pmfi-manejo-aprovechamiento.component.html',
  styleUrls: ['./pmfi-manejo-aprovechamiento.component.scss']
})
export class PmfiManejoAprovechamiento2Component implements OnInit {
  @Input() idPlanManejo: number = 280;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  @Input() disabled!: boolean;
  objAprovechamienito?: any;
  objDivisionAdministrativa?: any = {
    observacion: ''
  };
  objLaboresSilviculturales?: any;

  lstAprovechamiento?: [];
  lstLaboresSilviculturales?: [];

  idUsuario = 27;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private api: LaborSilviculturalServiceService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private eventEmitterService: EventEmitterService
  ) { }


  ngOnInit(): void {
    this.obtenerSistemaManejo().subscribe();
  }

  ngOnDestroy() {
    this.eventEmitterService.subsEtapa.unsubscribe();
    this.eventEmitterService.subsLabores.unsubscribe();
  }

  obtenerSistemaManejo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.api.obtenerListaLaborSilviculturalCabecera(this.idPlanManejo)
      .pipe(
        finalize(() => this.dialog.closeAll()),
        tap({
          next: res => {
            if (res?.data && res?.data !== undefined) {
              if (res.data.length === 1) {
                let data = res.data[0];

                data.idActividadSilvicultural = "0";
                data.monitoreo = 'N';
                data.anexo = 'N';

                this.objAprovechamienito = Object.assign({}, data);
                this.objAprovechamienito.codigoTipoActSilvicultural = 'APROVEC'
                this.objAprovechamienito.idActSilvicultural = data.idActividadSilvicultural;
                this.objAprovechamienito.idUsuarioRegistro = this.idUsuario;
                this.objAprovechamienito.idTipoTratamiento = 0;

                this.objDivisionAdministrativa = Object.assign({}, data);
                this.objDivisionAdministrativa.codigoTipoActSilvicultural = 'DIVICAD'
                this.objDivisionAdministrativa.idActSilvicultural = data.idActividadSilvicultural;
                this.objDivisionAdministrativa.idUsuarioRegistro = this.idUsuario;
                this.objDivisionAdministrativa.idTipoTratamiento = 0;

                this.objLaboresSilviculturales = Object.assign({}, data);
                this.objLaboresSilviculturales.codigoTipoActSilvicultural = 'LABSILV'
                this.objLaboresSilviculturales.idActSilvicultural = data.idActividadSilvicultural;
                this.objLaboresSilviculturales.idUsuarioRegistro = this.idUsuario;
                this.objLaboresSilviculturales.idTipoTratamiento = 0;
              } else {
                this.objAprovechamienito = res.data.find(x => x.codActividad === 'APROVEC');
                this.objAprovechamienito.codigoTipoActSilvicultural = this.objAprovechamienito.codActividad;
                this.objAprovechamienito.idActSilvicultural = this.objAprovechamienito.idActividadSilvicultural;
                this.objAprovechamienito.idUsuarioRegistro = this.idUsuario;
                this.objAprovechamienito.idTipoTratamiento = 0;

                this.objDivisionAdministrativa = res.data.find(x => x.codActividad === 'DIVICAD');
                this.objDivisionAdministrativa.codigoTipoActSilvicultural = this.objDivisionAdministrativa.codActividad;
                this.objDivisionAdministrativa.idActSilvicultural = this.objDivisionAdministrativa.idActividadSilvicultural;
                this.objDivisionAdministrativa.idUsuarioRegistro = this.idUsuario;
                this.objDivisionAdministrativa.idTipoTratamiento = 0;

                this.objLaboresSilviculturales = res.data.find(x => x.codActividad === 'LABSILV');
                this.objLaboresSilviculturales.codigoTipoActSilvicultural = this.objLaboresSilviculturales.codActividad;
                this.objLaboresSilviculturales.idActSilvicultural = this.objLaboresSilviculturales.idActividadSilvicultural;
                this.objLaboresSilviculturales.idUsuarioRegistro = this.idUsuario;
                this.objLaboresSilviculturales.idTipoTratamiento = 0;
              }
            };
          },
          error: (detail) => {
            
            this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
          }
        }));
  }

  onAprovechamiento(event: LaborSilviculturalDto[]) {
    if (this.objAprovechamienito) {
      if(!event) {
        // alert('no hay datos???')
      }
      // this.lstAprovechamiento = [];
      let detalle = event.map((x) =>
      ({
        accion: false,
        idActividadSilviculturalDet: ((x.accion === AccionTipo.REGISTRAR) ? 0 : parseInt(x.id)),
        idTipo: "5",
        idTipoTratamiento: x.tipoActividad.toString(),
        actividad: (x.actividad) ?? '',
        descripcionDetalle: (x.descripcion) ?? '',
        equipo: x.equipos,
        insumo: x.insumos,
        tratamiento: '',
        justificacion: '',
        observacionDetalle: (x.observaciones) ?? '',
        tipoTratamiento: '',
        idUsuarioRegistro: this.idUsuario.toString(),
        personal: x.personal
      })
      )
      this.lstAprovechamiento = Object.assign(detalle);
    }
  }

  onLaboresSilviculturales(event: LaborSilviculturalDto[]) {
    if (this.objLaboresSilviculturales) {
      if(!event) {
        // alert('no hay datos???')
      }
      let detalle2 = event.map(x => ({
        accion: x.marca,
        idActividadSilviculturalDet: ((x.accion === AccionTipo.REGISTRAR) ? 0 : parseInt(x.id)),
        idTipo: "6",
        idTipoTratamiento: x.tipoActividad.toString(),
        actividad: (x.actividad) ?? '',
        descripcionDetalle: (x.descripcion) ?? '',
        equipo: x.equipos,
        insumo: x.insumos,
        tratamiento: '',
        justificacion: '',
        observacionDetalle: (x.observaciones) ?? '',
        tipoTratamiento: '',
        idUsuarioRegistro: this.idUsuario.toString(),
        personal: x.personal
      }))
      this.lstLaboresSilviculturales = Object.assign(detalle2);
    }
  }

  generarId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  siguienteTab() {
    this.siguiente.emit();
  }
  regresarTab() {
    this.regresar.emit();
  }

  guardar() {

    if(!this.lstAprovechamiento) {
      // alert('no hay datos???')
    }
    if(!this.lstLaboresSilviculturales) {
      // alert('no hay datos???')
    }
    this.objAprovechamienito.listActividadSilvicultural = this.lstAprovechamiento ?? [];
    this.objLaboresSilviculturales.listActividadSilvicultural = this.lstLaboresSilviculturales ?? [];
    this.objDivisionAdministrativa.listActividadSilvicultural = [];

    let objRequest = {
      aprovechamiento: this.objAprovechamienito,
      divisionAdministrativa: this.objDivisionAdministrativa,
      laboresSilviculturales: this.objLaboresSilviculturales
    };
    this.guardarSistemaManejo(objRequest).subscribe();
  }
  guardarSistemaManejo(objRequest: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.api.guardarSistemaManejo(objRequest)
      .pipe(
        finalize(() => {
          this.eventEmitterService.onLlenarEtapaAprovechamiento(this.idPlanManejo);
          this.eventEmitterService.onLlenarLaboresSilviculturales(this.idPlanManejo);

          this.dialog.closeAll();
        }),
        tap({
          next: res => {
            this.messageService.add({ severity: "success", summary: "", detail: "Se guardó el sistema de manejo y aprovechamiento con éxito." });
          },
          error: (detail) => {
            
            this.messageService.add({ severity: "warn", summary: "", detail: detail?.message });
          }
        }));
  }


}

export enum TipoModal {
  Aprovechamiento = 1,
  LaborSilvicultural = 2
}
