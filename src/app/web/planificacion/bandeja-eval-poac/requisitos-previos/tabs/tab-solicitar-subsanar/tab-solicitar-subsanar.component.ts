import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-tab-solicitar-subsanar',
  templateUrl: './tab-solicitar-subsanar.component.html',
  styleUrls: ['./tab-solicitar-subsanar.component.scss'],
})
export class TabSolicitarSubsanarComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  verRemitir: boolean = false;
  verEditarObservacion: boolean = false;
  observacion: string = '';

  observaciones: any = [
    {
      title: 'Item 1',
      list: [
        {
          observaciones: 'PRUEBA',
        },
        {
          observaciones: 'PRUEBA',
        },
      ],
    },
    {
      title: 'Item 2',
      list: [
        {
          observaciones: 'PRUEBA',
        },
        {
          observaciones: 'PRUEBA',
        },
      ],
    },
    {
      title: 'Item 3',
      list: [
        {
          observaciones: 'PRUEBA',
        },
        {
          observaciones: 'PRUEBA',
        },
      ],
    },
  ];

  listObs: any = [{}];

  indexObservacion: number = 0;
  indexListObservacion: number = 0;

  constructor(private confirmationService: ConfirmationService) {}

  ngOnInit(): void {}

  editarObservacion(data: any, indexObs: number, index: number) {
    
    this.verEditarObservacion=true;
    this.observacion = { ...data };
    this.indexListObservacion = indexObs;
    this.indexObservacion = index;
  }

  remitir(event: any, data: any, index: number) {
    this.verRemitir = true;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
