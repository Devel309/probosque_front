import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-tab-registrar-informacion",
  templateUrl: "./tab-registrar-informacion.component.html",
  styleUrls: ["./tab-registrar-informacion.component.scss"],
})
export class TabRegistrarInformacionComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;@Input() disabled!: boolean;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();


  idArchivoEvidencia: number = 0;

  codigoArchivoEvidencia: string = "";

  constructor() {}

  ngOnInit(): void {
    this.codigoArchivoEvidencia = this.codigoEpica + "EVID";
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  editarCalendario() {

  }


  solicitarSubsanacion() {

  }

  cargarEvidencia(idArchivo: number) {
    if (idArchivo !=null && idArchivo > 0) {
      this.idArchivoEvidencia = idArchivo;

    }
  }

  regresarTab() {
    this.regresar.emit();
  }
}
