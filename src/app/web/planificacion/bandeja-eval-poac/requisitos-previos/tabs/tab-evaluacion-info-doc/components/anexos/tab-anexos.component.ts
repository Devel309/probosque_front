import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { environment } from '@env/environment';
import { ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { AnexosService } from 'src/app/service/plan-operativo-concesion-maderable/anexos.service';

@Component({
  selector: 'app-anexos',
  templateUrl: './tab-anexos.component.html',
  styleUrls: ['./tab-anexos.component.scss'],
})
export class anexosComponent implements OnInit {
  varAssets = `${environment.varAssets}`;

  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;

  mapaBase = [
    { label: '-- Seleccione --', value: null },
    { label: 'Archivos shapefiles', value: true },
    { label: 'Formato imagen', value: false },
  ];

  listAnexo3Especies: any[] = [];
  listAnexo4Especies: any[] = [];
  listAnexo5Maderables: any[] = [];
  listAnexo6NoMaderables: any[] = [];
  listAnexo7NoMaderables: any[] = [];
  listAnexo8Especies: any[] = [];
  listAnexo6Especies: any[] = [];
  variables: any[] = [{ var: 'N' }, { var: 'AB m2' }, { var: 'Vc m3' }];
  variablesAnexo6: any[] = [{ var: 'N' }, { var: 'C**' }];

  comboBosque: any[] = [];
  catalogoTipoBosque: any[] = [];
  codigoBosqueAnexo3: number = 1;
  areaBosqueAnexo3: number = 0;
  areaBosqueAnexo4: number = 0;
  codigoBosqueAnexo4: number = 2;

  Dap30a39ARB: number = 0;
  Dap40a49ARB: number = 0;
  Dap50a59ARB: number = 0;
  Dap60a69ARB: number = 0;
  Dap70a79ARB: number = 0;
  Dap80a89ARB: number = 0;
  Dap90aMasARB: number = 0;
  TotalTipoBosqueARB: number = 0;
  TotalPorAreaARB: number = 0;

  Dap30a39AB: number = 0;
  Dap40a49AB: number = 0;
  Dap50a59AB: number = 0;
  Dap60a69AB: number = 0;
  Dap70a79AB: number = 0;
  Dap80a89AB: number = 0;
  Dap90aMasAB: number = 0;
  TotalTipoBosqueAB: number = 0;
  TotalPorAreaAB: number = 0;

  Dap30a39VOL: number = 0;
  Dap40a49VOL: number = 0;
  Dap50a59VOL: number = 0;
  Dap60a69VOL: number = 0;
  Dap70a79VOL: number = 0;
  Dap80a89VOL: number = 0;
  Dap90aMasVOL: number = 0;
  TotalTipoBosqueVOL: number = 0;
  TotalPorAreaVOL: number = 0;

  Dap10a20: number = 0;
  Dap20a30: number = 0;
  TotalTipoBosque: number = 0;
  TotalPorArea: number = 0;

  Dap10a20A: number = 0;
  Dap20a30A: number = 0;
  TotalTipoBosqueA: number = 0;
  TotalPorAreaA: number = 0;

  nTotalPorTipoBosqueN: number = 0;
  nTotalPorHaN: number = 0;
  cTotalPorTipoBosqueC: number = 0;
  cTotalPorHaC: number = 0;

  constructor(
    private toast: ToastService,
    private anexosService: AnexosService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.listarCatalogoTipoBosque();
  }

  listarCatalogoTipoBosque() {
    this.anexosService.listarTipoBosque().subscribe((data: any) => {
      if (data.success) {
        this.catalogoTipoBosque = data.data;
      }
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  listAnexo3(codigoBosqueAnexo2: number) {
    this.listAnexo3Especies = [];
    this.areaBosqueAnexo3 = 0;
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      idTipoBosque: codigoBosqueAnexo2,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo3(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((response: any, index: number) => {
            this.Dap30a39ARB = 0;
            this.Dap40a49ARB = 0;
            this.Dap50a59ARB = 0;
            this.Dap60a69ARB = 0;
            this.Dap70a79ARB = 0;
            this.Dap80a89ARB = 0;
            this.Dap90aMasARB = 0;
            this.TotalTipoBosqueARB = 0;
            this.TotalPorAreaARB = 0;

            this.Dap30a39AB = 0;
            this.Dap40a49AB = 0;
            this.Dap50a59AB = 0;
            this.Dap60a69AB = 0;
            this.Dap70a79AB = 0;
            this.Dap80a89AB = 0;
            this.Dap90aMasAB = 0;
            this.TotalTipoBosqueAB = 0;
            this.TotalPorAreaAB = 0;

            this.Dap30a39VOL = 0;
            this.Dap40a49VOL = 0;
            this.Dap50a59VOL = 0;
            this.Dap60a69VOL = 0;
            this.Dap70a79VOL = 0;
            this.Dap80a89VOL = 0;
            this.Dap90aMasVOL = 0;
            this.TotalTipoBosqueVOL = 0;
            this.TotalPorAreaVOL = 0;

            response.map((element: any) => {
              var obj = new Anexo3();
              obj.nombreComun = element.nombreComun;
              obj.areaBosque = element.areaBosque;
              obj.nombreBosque = element.nombreBosque;
              this.areaBosqueAnexo3 = element.areaBosque;
              this.variables.forEach((item: any, i: number) => {
                var ob1 = new ARB();
                var ob2 = new ARB();
                var ob3 = new ARB();
                if (item.var == 'N') {
                  ob1.var = 'N';
                  ob1.Dap30a39 = element.arbHaDap30a39;
                  this.Dap30a39ARB += Number(element.arbHaDap30a39);
                  ob1.Dap40a49 = element.arbHaDap40a49;
                  this.Dap40a49ARB += Number(element.arbHaDap40a49);
                  ob1.Dap50a59 = element.arbHaDap50a59;
                  this.Dap50a59ARB += Number(element.arbHaDap50a59);
                  ob1.Dap60a69 = element.arbHaDap60a69;
                  this.Dap60a69ARB += Number(element.arbHaDap60a69);
                  ob1.Dap70a79 = element.arbHaDap70a79;
                  this.Dap70a79ARB += Number(element.arbHaDap70a79);
                  ob1.Dap80a89 = element.arbHaDap80a89;
                  this.Dap80a89ARB += Number(element.arbHaDap80a89);
                  ob1.Dap90aMas = element.arbHaDap90aMas;
                  this.Dap90aMasARB += Number(element.arbHaDap90aMas);
                  ob1.TotalTipoBosque = element.arbHaTotalTipoBosque;
                  this.TotalTipoBosqueARB += Number(
                    element.arbHaTotalTipoBosque
                  );
                  ob1.TotalPorArea = element.arbHaTotalPorArea;
                  this.TotalPorAreaARB += Number(element.arbHaTotalPorArea);
                  obj.array.push(ob1);
                } else if (item.var == 'AB m2') {
                  ob2.var = 'AB m2';
                  ob2.Dap30a39 = element.abHaDap30a39;
                  this.Dap30a39AB += Number(element.abHaDap30a39);
                  ob2.Dap40a49 = element.abHaDap40a49;
                  this.Dap40a49AB += Number(element.abHaDap40a49);
                  ob2.Dap50a59 = element.abHaDap50a59;
                  this.Dap50a59AB += Number(element.abHaDap50a59);
                  ob2.Dap60a69 = element.abHaDap60a69;
                  this.Dap60a69AB += Number(element.abHaDap60a69);
                  ob2.Dap70a79 = element.abHaDap70a79;
                  this.Dap70a79AB += Number(element.abHaDap70a79);
                  ob2.Dap80a89 = element.abHaDap80a89;
                  this.Dap80a89AB += Number(element.abHaDap80a89);
                  ob2.Dap90aMas = element.abHaDap90aMas;
                  this.Dap90aMasAB += Number(element.abHaDap90aMas);
                  ob2.TotalTipoBosque = element.abHaTotalTipoBosque;
                  this.TotalTipoBosqueAB += Number(element.abHaTotalTipoBosque);
                  ob2.TotalPorArea = element.abHaTotalPorArea;
                  this.TotalPorAreaAB += Number(element.abHaTotalPorArea);
                  obj.array.push(ob2);
                } else if (item.var == 'Vc m3') {
                  ob3.var = 'Vc m3';
                  ob3.Dap30a39 = element.volHaDap30a39;
                  this.Dap30a39VOL += Number(element.volHaDap30a39);
                  ob3.Dap40a49 = element.volHaDap40a49;
                  this.Dap40a49VOL += Number(element.volHaDap40a49);
                  ob3.Dap50a59 = element.volHaDap50a59;
                  this.Dap50a59VOL += Number(element.volHaDap50a59);
                  ob3.Dap60a69 = element.volHaDap60a69;
                  this.Dap60a69VOL += Number(element.volHaDap60a69);
                  ob3.Dap70a79 = element.volHaDap70a79;
                  this.Dap70a79VOL += Number(element.volHaDap70a79);
                  ob3.Dap80a89 = element.volHaDap80a89;
                  this.Dap80a89VOL += Number(element.volHaDap80a89);
                  ob3.Dap90aMas = element.volHaDap90aMas;
                  this.Dap90aMasVOL += Number(element.volHaDap90aMas);
                  ob3.TotalTipoBosque = element.volHaTotalTipoBosque;
                  this.TotalTipoBosqueVOL += Number(
                    element.volHaTotalTipoBosque
                  );
                  ob3.TotalPorArea = element.volHaTotalPorArea;
                  this.TotalPorAreaVOL += Number(element.volHaTotalPorArea);
                  obj.array.push(ob3);
                }
              });
              this.listAnexo3Especies.push({
                ...obj,
                total: {
                  Dap30a39ARB: this.Dap30a39ARB,
                  Dap40a49ARB: this.Dap40a49ARB,
                  Dap50a59ARB: this.Dap50a59ARB,
                  Dap60a69ARB: this.Dap60a69ARB,
                  Dap70a79ARB: this.Dap70a79ARB,
                  Dap80a89ARB: this.Dap80a89ARB,
                  Dap90aMasARB: this.Dap90aMasARB,
                  TotalTipoBosqueARB: this.TotalTipoBosqueARB,
                  TotalPorAreaARB: this.TotalPorAreaARB,

                  Dap30a39AB: this.Dap30a39AB,
                  Dap40a49AB: this.Dap40a49AB,
                  Dap50a59AB: this.Dap50a59AB,
                  Dap60a69AB: this.Dap60a69AB,
                  Dap70a79AB: this.Dap70a79AB,
                  Dap80a89AB: this.Dap80a89AB,
                  Dap90aMasAB: this.Dap90aMasAB,
                  TotalTipoBosqueAB: this.TotalTipoBosqueAB,
                  TotalPorAreaAB: this.TotalPorAreaAB,

                  Dap30a39VOL: this.Dap30a39VOL,
                  Dap40a49VOL: this.Dap40a49VOL,
                  Dap50a59VOL: this.Dap50a59VOL,
                  Dap60a69VOL: this.Dap60a69VOL,
                  Dap70a79VOL: this.Dap70a79VOL,
                  Dap80a89VOL: this.Dap80a89VOL,
                  Dap90aMasVOL: this.Dap90aMasVOL,
                  TotalTipoBosqueVOL: this.TotalTipoBosqueVOL,
                  TotalPorAreaVOL: this.TotalPorAreaVOL,
                },
              });
            });
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  listAnexo4(codigoBosqueAnexo2: number) {
    this.listAnexo4Especies = [];
    this.areaBosqueAnexo4 = 0;
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      idTipoBosque: codigoBosqueAnexo2,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo4(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((response: any, index: number) => {
            this.Dap10a20 = 0;
            this.Dap20a30 = 0;
            this.TotalTipoBosque = 0;
            this.TotalPorArea = 0;
            this.Dap10a20A = 0;
            this.Dap20a30A = 0;
            this.TotalTipoBosqueA = 0;
            this.TotalPorAreaA = 0;

            response.map((element: any) => {
              var obj = new Anexo4();
              obj.nombreComun = element.nombreComun;
              obj.areaBosque = element.areaBosque;
              obj.nombreBosque = element.nombreBosque;
              this.areaBosqueAnexo4 = element.areaBosque;
              this.variables.forEach((item: any, i: number) => {
                var ob1 = new arbAnexo4();
                var ob2 = new arbAnexo4();
                if (item.var == 'N') {
                  ob1.var = 'N';
                  ob1.Dap10a20 = element.arbHaDap10a19;
                  this.Dap10a20 += Number(element.arbHaDap10a19);
                  ob1.Dap20a30 = element.arbHaDap20a29;
                  this.Dap20a30 += Number(element.arbHaDap20a29);
                  ob1.TotalTipoBosque = element.arbHaTotalTipoBosque;
                  this.TotalTipoBosque += Number(element.arbHaTotalTipoBosque);
                  ob1.TotalPorArea = element.arbHaTotalPorArea;
                  this.TotalPorArea += Number(element.arbHaTotalPorArea);
                  obj.array.push(ob1);
                } else if (item.var == 'AB m2') {
                  ob2.var = 'AB m2';
                  ob2.Dap10a20 = element.abHaDap10a19;
                  this.Dap10a20A += Number(element.abHaDap10a19);
                  ob2.Dap20a30 = element.abHaDap20a29;
                  this.Dap20a30A += Number(element.abHaDap20a29);
                  ob2.TotalTipoBosque = element.abHaTotalTipoBosque;
                  this.TotalTipoBosqueA += Number(element.abHaTotalTipoBosque);
                  ob2.TotalPorArea = element.abHaTotalPorArea;
                  this.TotalPorAreaA += Number(element.abHaTotalPorArea);
                  obj.array.push(ob2);
                }
              });
              this.listAnexo4Especies.push({
                ...obj,
                total: {
                  Dap10a20: this.Dap10a20,
                  Dap20a30: this.Dap20a30,
                  TotalTipoBosque: this.TotalTipoBosque,
                  TotalPorArea: this.TotalPorArea,

                  Dap10a20A: this.Dap10a20A,
                  Dap20a30A: this.Dap20a30A,
                  TotalTipoBosqueA: this.TotalTipoBosqueA,
                  TotalPorAreaA: this.TotalPorAreaA,
                },
              });
            });
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  listAnexo5() {
    this.listAnexo5Maderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo2(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo5Maderables.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  listAnexo6(codigoBosqueAnexo2: number) {
    this.listAnexo6NoMaderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      idTipoBosque: codigoBosqueAnexo2,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo6(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.nTotalPorTipoBosqueN = 0;
            this.nTotalPorHaN = 0;
            this.cTotalPorTipoBosqueC = 0;
            this.cTotalPorHaC = 0;
            element.listaTablaAnexo6Dto.forEach((element: any) => {
              this.nTotalPorTipoBosqueN += Number(element.nTotalPorTipoBosque);
              this.nTotalPorHaN += Number(element.nTotalPorHa);
              this.cTotalPorTipoBosqueC += Number(element.cTotalPorTipoBosque);
              this.cTotalPorHaC += Number(element.cTotalPorHa);
            });            
            this.listAnexo6NoMaderables.push({
              ...element,
              nTotalPorTipoBosqueN: this.nTotalPorTipoBosqueN,
              nTotalPorHaN: this.nTotalPorHaN,
              cTotalPorTipoBosqueC: this.cTotalPorTipoBosqueC,
              cTotalPorHaC: this.cTotalPorHaC,
            });
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  listAnexo7() {
    this.listAnexo7NoMaderables = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .Anexo7NoMaderable(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo7NoMaderables.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }
  listAnexo8() {
    this.listAnexo8Especies = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .AnexoListaEspeciesInventariadasMaderablesObtener(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo8Especies.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }

  selectedTab(tab: any, accion?: any) {
    if (accion) {
      switch (tab) {
        case 3:
          this.listAnexo3(this.codigoBosqueAnexo3);
          break;
        case 4:
          this.listAnexo4(this.codigoBosqueAnexo4);
          break;
        case 5:
          this.listAnexo5();
          break;
        case 6:
          this.listAnexo6(this.codigoBosqueAnexo4);
          break;

        case 7:
          this.listAnexo7();
          break;

        case 8:
          this.listAnexo8();
          break;
        default:
          break;
      }
    }
  }

}

export class Anexo3 {
  constructor(data?: any) {
    if (data) {
      this.nombreComun = data.nombreComun;
      this.areaBosque = data.areaBosque;
      this.nombreBosque = data.nombreBosque;
      return;
    }
  }
  nombreComun: string = '';
  nombreBosque: string = '';
  areaBosque: string = '';
  array: ARB[] = [];
}

export class ARB {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = '';
  Dap30a39: string = '';
  Dap40a49: string = '';
  Dap50a59: string = '';
  Dap60a69: string = '';
  Dap70a79: string = '';
  Dap80a89: string = '';
  Dap90aMas: string = '';
  TotalTipoBosque: string = '';
  TotalPorArea: string = '';
}

export class Anexo4 {
  constructor(data?: any) {
    if (data) {
      this.nombreComun = data.nombreComun;
      this.areaBosque = data.areaBosque;
      this.nombreBosque = data.nombreBosque;
      return;
    }
  }
  nombreComun: string = '';
  areaBosque: string = '';
  nombreBosque: string = '';
  array: arbAnexo4[] = [];
}
export class arbAnexo4 {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = '';
  Dap10a20: string = '';
  Dap20a30: string = '';
  TotalTipoBosque: string = '';
  TotalPorArea: string = '';
}

export class Anexo6 {
  constructor(data?: any) {
    if (data) {
      this.tipoBosque = data.tipoBosque;
      this.areaBosque = data.areaBosque;
      return;
    }
  }

  tipoBosque: string = '';
  areaBosque: string = '';
  especies: arbAnexo6[] = [new arbAnexo6()];
}

export class arbAnexo6 {
  constructor(data?: any) {
    if (data) {
      this.nombreComun = data.nombreComun;
      this.array = data.array;
    }
  }

  nombreComun: string = '';
  array: detalleAnexo6[] = [];
}

export class detalleAnexo6 {
  constructor(data?: any) {
    if (data) {
      return;
    }
  }
  var: string = '';
  nombreComun: string = '';
  TotalTipoBosque: string = '';
  TotalPorArea: string = '';
}
