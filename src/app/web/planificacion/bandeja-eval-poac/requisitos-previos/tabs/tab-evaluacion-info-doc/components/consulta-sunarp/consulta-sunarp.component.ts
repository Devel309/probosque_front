import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { ToastService } from '@shared';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PmfiInformacionGeneral } from 'src/app/model/pmfi-informacion-general';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { PideService } from 'src/app/service/evaluacion/pide.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { RegenteService } from 'src/app/service/regente.service';
import {Mensajes} from '../../../../../../../../model/util/Mensajes';

@Component({
  selector: 'consulta-sunarp',
  templateUrl: './consulta-sunarp.component.html',
  styleUrls: ['./consulta-sunarp.component.scss'],
})
export class ConsultaSUNARPComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;

  listRegente: any[] = [];
  listArchivos: any[] = [];
  @Input() fileList: any[] = [];

  informacionGeneral: PmfiInformacionGeneral = new PmfiInformacionGeneral();

  dniTitular: string = '';
  rucTitular: string = '';
  nombre_o_razonSocialTitular: string = '';
  nombreTitular: string = '';
  apellidoPaternoTitular: string = '';
  apellidoMaternoTitular: string = '';

  direccionTitular: string = '';
  tipoPersonaTitular: string = ''; //N-J
  nroPartidaRegistral: string = '';

  esReprLegal: string = '';
  dniRucRepresentante: string = '';
  nombre_o_razonSocialRepresentante: string = '';
  nombreRepresentante: string = '';
  apellidoPaternoRepresentante: string = '';
  apellidoMaternoRepresentante: string = '';

  direccionRepresentante: string = '';
  tipoPersonaRepresentante: string = ''; //N-J
  tipoDocumentoRepresentante: string = ''; //DNI  RUC

  validaPIDEReniecClass: boolean = false;
  validaPIDESunatClass: boolean = false;
  validaPIDESunarpClass: boolean = false;

  validaPIDEReniecClassRepre: boolean = false;
  validaPIDESunatClassRepre: boolean = false;
  validaPIDESunarpClassRepre: boolean = false;

  nombreCompleto: string = '';
  numeroDocumentoRegente: string = '';
  numeroLicenciaRegente: string = '';

  validaRegente: boolean = false;

  file: any = {};
  constructor(
    private archivoServ: ArchivoService,
    public regenteService: RegenteService,
    private dialog: MatDialog,
    private toast: ToastService,
    private pideService: PideService,
    private informacionGeneralService: InformacionGeneralService,
    private serviceExternos: ApiForestalService,
  ) {}

  ngOnInit(): void {
    this.listarInfGeneral();
  }

  listarInfGeneral() {
    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: this.codigoEpica,
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          let info = response.data[0];

          this.informacionGeneral = new PmfiInformacionGeneral(info);

          if (info.regente) {
            this.nombreCompleto = info.regente.nombres + info.regente.apellidos;
            this.numeroDocumentoRegente = info.regente.numeroDocumento;
            this.numeroLicenciaRegente = info.regente.numeroLicencia;
          }

          this.dniTitular = info.documentoRepresentante;
          this.rucTitular = info.dniElaboraDema;
          this.nombre_o_razonSocialTitular = info.nombreElaboraDema;
          this.nombreTitular = info.nombreRepresentante;
          this.apellidoPaternoTitular = info.apellidoPaternoRepresentante;
          this.apellidoMaternoTitular = info.apellidoMaternoRepresentante;

          this.direccionTitular = info.direccionLegalTitular;
          this.tipoPersonaTitular =
            info.codTipoPersona == 'TPERJURI' ? 'J' : 'N';

          this.esReprLegal = info.esReprLegal; //S

          this.nroPartidaRegistral = info.observacion ? info.observacion : '';
          if (info.codTipoDocumentoRepresentante == 'TDOCDNI') {
            this.tipoDocumentoRepresentante = 'DNI';
            this.nombre_o_razonSocialRepresentante =
              info.nombreRepresentante +
              ' ' +
              info.apellidoPaternoRepresentante +
              ' ' +
              info.apellidoMaternoRepresentante;
            this.dniRucRepresentante = info.documentoRepresentante;
            this.direccionRepresentante = info.direccionLegalRepresentante;
          } else {
            this.tipoDocumentoRepresentante = 'RUC';
            this.nombre_o_razonSocialRepresentante = info.representanteLegal;
            this.dniRucRepresentante = info.documentoRepresentante;
            this.direccionRepresentante = info.direccionLegalRepresentante;
          }
        }
      });
  }

  validarInformacionRegente() {
    if (this.numeroDocumentoRegente == "") {
      this.toast.warn("(*) Debe ingresar un número de documento de regente");
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });

      this.serviceExternos.consultarRegente().subscribe(
        (resp: any) => {
          this.dialog.closeAll();
          if (resp.dataService?.length > 0) {
            const regente = resp.dataService.find(
              (x: any) =>
                x.numeroDocumento.toString() === this.numeroDocumentoRegente
            );
            if (regente) {
              this.toast.ok(
                "Número de documento en base de datos de regente encontrado."
              );

              if (regente.numeroLicencia == this.numeroLicenciaRegente) {
                if (regente.descripcionEstado == "VIGENTE") {
                  this.toast.ok("La licencia se encuentra en estado VIGENTE");
                  this.validaRegente = true;
                } else {
                  this.toast.warn(
                    "La licencia no se encuentra en estado VIGENTE"
                  );
                  this.validaRegente = false;
                }
              } else {
                this.toast.warn(
                  "La licencia no concuerda con la registrada en BD."
                );
                this.validaRegente = false;
              }
            } else {
              this.toast.warn(
                "Número de documento en base de datos de regente  no encontrado."
              );
              this.validaRegente = false;
            }
          }
        },
        () => {
          this.dialog.closeAll();
          this.toast.error(Mensajes.MSJ_ERROR_CATCH);
        }
      );
    }
  }

  validarPideReniec() {
    let params = {
      numDNIConsulta: this.dniTitular,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          if (result.dataService.datosPersona) {
            this.validaPIDEReniecClass = true;
            this.toast.ok('Se validó existencia de DNI en RENIEC.');
          } else {
            this.validaPIDEReniecClass = false;
            this.toast.warn('DNI ingresado no existe en RENIEC.');
          }
        } else {
          this.validaPIDEReniecClass = false;
          this.toast.error(
            'Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.'
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDEReniecClass = false;
        this.toast.error(
          'Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.'
        );
      }
    );
  }
  validarPideReniecRepresentanteLegal() {
    let params = {
      numDNIConsulta: this.dniRucRepresentante,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService.consultarDNI(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          if (result.dataService.datosPersona) {
            this.validaPIDEReniecClassRepre = true;
            this.toast.ok('Se validó existencia de DNI en RENIEC.');
          } else {
            this.validaPIDEReniecClassRepre = false;
            this.toast.warn('DNI ingresado no existe en RENIEC.');
          }
        } else {
          this.validaPIDEReniecClassRepre = false;
          this.toast.error(
            'Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.'
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDEReniecClassRepre = false;
        this.toast.error(
          'Hay errores con el servicio de validación de DNI. Contactar con el administrador del sistema.'
        );
      }
    );
  }
  validarPideSunat() {
    if (this.rucTitular == '') {
      this.toast.warn('Debe ingresar un número de RUC.');
      return;
    }
    if (this.rucTitular.length != 11) {
      this.toast.warn('RUC ingresado no tiene 11 caracteres.');
      return;
    }

    let params = {
      numRUC: this.rucTitular,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          let respuesta = result.dataService.respuesta;
          if (respuesta.esHabido) {
            this.validaPIDESunatClass = true;
            this.toast.ok('Se validó existencia de RUC en SUNAT.');
          } else {
            this.validaPIDESunatClass = false;
            this.toast.warn('RUC ingresado no existe en SUNAT.');
          }
        } else {
          this.validaPIDESunatClass = false;
          this.toast.error(
            'Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.'
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDESunatClass = false;
        this.toast.error(
          'Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.'
        );
      }
    );
  }
  validarPideSunatRepresentante() {
    if (this.dniRucRepresentante == '') {
      this.toast.warn('Debe ingresar un número de RUC.');
      return;
    }
    if (this.dniRucRepresentante.length != 11) {
      this.toast.warn('RUC ingresado no tiene 11 caracteres.');
      return;
    }

    let params = {
      numRUC: this.dniRucRepresentante,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.pideService.consultarRazonSocial(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.dataService) {
          let respuesta = result.dataService.respuesta;
          if (respuesta.esHabido) {
            this.validaPIDESunatClassRepre = true;
            this.toast.ok('Se validó existencia de RUC en SUNAT.');
          } else {
            this.validaPIDESunatClassRepre = false;
            this.toast.warn('RUC ingresado no existe en SUNAT.');
          }
        } else {
          this.validaPIDESunatClassRepre = false;
          this.toast.error(
            'Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.'
          );
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.validaPIDESunatClassRepre = false;
        this.toast.error(
          'Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.'
        );
      }
    );
  }

  validarPideSunarp() {


    /*let param = {
      apellidoMaterno: this.apellidoMaternoTitular,
      apellidoPaterno: this.apellidoPaternoTitular,
      nombres: this.nombreTitular,
      razonSocial: this.nombre_o_razonSocialTitular,
      tipoParticipante: this.tipoPersonaTitular,
    };

    this.consultarTitularidadSUNARP(param);*/
    this.toast.warn(
      'El servicio de validación no se encuentra disponible.'
    );

  }

  consultarTitularidadSUNARP(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    return this.serviceExternos
      .consultarTitularidadSUNARP(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        
        if (
          response.dataService && response.dataService.respuesta != '' &&
          response.dataService.respuesta.respuestaTitularidad.length > 0

        ) {
          this.toast.ok(
            'Se validó la existencia de la Partida Registral en SUNARP'
          );
          const res = response.dataService.respuesta.respuestaTitularidad[0];
          this.nroPartidaRegistral = res.numeroPartida;
          this.validaPIDESunarpClass = true;
          this.actualizarInfGeneral();
        } else {
          

          this.validaPIDESunarpClass = false;
          this.validarPideSunarpJ();
        }
      });
  }

  validarPideSunarpJ() {
    let param = {
      tipoParticipante: 'J',
      apellidoPaterno: '',
      apellidoMaterno: '',
      nombres: '',
      razonSocial: 'CAJA MUNICIPAL AREQUIPA',
    };

    

    this.consultarTitularidadSUNARP(param);
  }

  actualizarInfGeneral() {
    const obj = new PmfiInformacionGeneral(this.informacionGeneral);
    obj.observacion = this.nroPartidaRegistral;

    this.informacionGeneralService
      .actualizarInformacionGeneralDema(obj)
      .subscribe((response: any) => {
        if (response.success) {
          this.listarInfGeneral();
        }
      });
  }
}
