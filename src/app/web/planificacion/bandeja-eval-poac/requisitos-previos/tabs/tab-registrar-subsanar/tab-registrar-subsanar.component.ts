import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-tab-registrar-subsanar",
  templateUrl: "./tab-registrar-subsanar.component.html",
  styleUrls: ["./tab-registrar-subsanar.component.scss"],
})
export class TabRegistrarSubsanarComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();

  verRemitir: boolean = false;
  verEditarObservacion: boolean = false;
  observacion: string = '';

  observaciones: any = [
    {
      title: 'Item 1',
      anotacion: 'Item 1',
      list: [
        {
          observaciones: 'PRUEBA',
          revisar: 'S',
        },
        {
          observaciones: 'PRUEBA',
          revisar: 'N',
        },
      ],
    },
    {
      title: 'Item 2',
      anotacion: 'Item 1',
      list: [
        {
          observaciones: 'PRUEBA',
          revisar: 'N',
        },
        {
          observaciones: 'PRUEBA',
          revisar: 'S',
        },
      ],
    },
    {
      title: 'Item 3',
      anotacion: 'Item 1',
      list: [
        {
          observaciones: 'PRUEBA',
          revisar: 'N',
        },
        {
          observaciones: 'PRUEBA',
          revisar: 'S',
        },
      ],
    },
  ];

  listObs: any = [{}];

  indexObservacion: number = 0;
  indexListObservacion: number = 0;

  constructor() {}

  ngOnInit(): void {}

  editarObservacion(data: any, indexObs: number, index: number) {
    
    this.verEditarObservacion=true;
    this.observacion = { ...data };
    this.indexListObservacion = indexObs;
    this.indexObservacion = index;
  }

  remitir(event: any, data: any, index: number) {
    this.verRemitir = true;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
