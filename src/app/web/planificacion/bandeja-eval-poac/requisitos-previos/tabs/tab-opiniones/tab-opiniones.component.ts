import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-tab-opiniones",
  templateUrl: "./tab-opiniones.component.html",
  styleUrls: ["./tab-opiniones.component.scss"],
})
export class TabOpinionesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;@Input() disabled!: boolean;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
}
