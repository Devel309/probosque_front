import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TabRequisitosTupaComponent } from 'src/app/shared/components/requisitos-tupa/requisitos-tupa.component';

@Component({
  selector: 'app-tab-presentacion',
  templateUrl: './tab-presentacion-poac.component.html',
  styleUrls: ['./tab-presentacion-poac.component.scss'],
})
export class TabPresentacionPoacComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;

  @ViewChild(TabRequisitosTupaComponent)
  TabRequisitosTupaComponent!: TabRequisitosTupaComponent;

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  tabIndex: number = 0;

  evaluacionRequisitos: any = {
    fechaTramite: null,
    detalle: null,
  };

  tipoPlan: string = '';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.tipoPlan = String(this.route.snapshot.paramMap.get('tipoPlan'));
  }

  filtroRequisitosTUPA() {
    this.TabRequisitosTupaComponent.listMesaPartesFiltro(
      this.evaluacionRequisitos
    );
  }

  limpiarFiltroRequisitosTUPA() {
    this.evaluacionRequisitos = {
      fechaTramite: null,
      detalle: null,
    };

    this.TabRequisitosTupaComponent.filtro = {
      fechaTramite: null,
      detalle: null,
    };
    this.TabRequisitosTupaComponent.listMesaPartes();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  informChange(event: any) {
    this.tabIndex = event;
  }
}
