import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import * as moment from 'moment';
import {FileModel} from '../../../../../../model/util/File';
import {ConfirmationService, LazyLoadEvent, MessageService} from 'primeng/api';
import {UsuarioService} from '@services';
import {MedidasCorrectivasService} from '../../../../../../service/evaluacion/evaluacion-plan-operativo/medidas-correctivas.service';
import {ToastService} from '@shared';
import {AnexosService} from '../../../../../../service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import {MatDialog} from '@angular/material/dialog';
import {GenericoService} from '../../../../../../service/generico.service';
import {Page} from '@models';
import {LoadingComponent} from '../../../../../../components/loading/loading.component';
import {HttpParams} from '@angular/common/http';
import {InformacionGeneralService} from '../../../../../../service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';

@Component({
  selector: "app-tab-medidas-correctivas",
  templateUrl: "./tab-medidas-correctivas.component.html",
  styleUrls: ["./tab-medidas-correctivas.component.scss"],
})
export class TabMedidasCorrectivasComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Output()public siguiente = new EventEmitter();
  @Output()public regresar = new EventEmitter();
  @Input() disabled!: boolean;
  minDate = moment(new Date()).format("YYYY-MM-DD");
  implementacion: any;

  tipoAccion: string = "";
  tituloModalMedidas: string = " ";
  medidaCorrectivaObj: medidasCorrectivasModel = new medidasCorrectivasModel();
  verModalMedidas: boolean = false;
  lstMedidasCorrectivasDetalle: medidasCorrectivasModel[] = [];
  pendiente: boolean = false;
  edit: boolean = false;
  idArchivo: number = 0;

  idMedida: number = 0;

  fechaResolucion!: Date | string;
  archivoCargado: boolean = false;

  archivoEvidenciaMedidaCorrectiva: boolean = true;

  medidasCorrectivasCabeceraObj: medidasCorrectivasCbeceraModel = new medidasCorrectivasCbeceraModel();

  files: FileModel[] = [];
  filePMFI: FileModel = {} as FileModel;
  cargarPMFI: boolean = false;
  eliminarPMFI: boolean = true;
  idArchivoPMFI: number = 0;
  tieneArchivo: boolean = false;
  verEnviar1: boolean = false;
  rowGroup: any = {};

  editar: boolean = false;
  totalRecords = 0;

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  listadoAutoridasSancionadora: any[] = [];


  constructor(
    private messageService: MessageService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private medidasCorrectivasService: MedidasCorrectivasService,
    private toast: ToastService,
    private anexosService: AnexosService,
    private dialog: MatDialog,
    private genericoService: GenericoService,
    private informacionGeneralService: InformacionGeneralService
  ) {}

  ngOnInit(): void {

    this.filePMFI.inServer = false;
    this.filePMFI.descripcion = "PDF";
    // this.listMedidasCorrectivas();

    this.getAutoridadSancionadora();



  }


  listResumenEjecutivo() {
    var params = {
      codigoProceso: "POAC",
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    //this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          response.data.forEach((element: any) => {

            this.medidasCorrectivasCabeceraObj.numeroContrato = element.nombreTituloHabilitante;
          });
        }
      });
  }

  getAutoridadSancionadora() {
    this.genericoService
      .listarParametroPorPrefijo()
      .subscribe((response: any) => {
        //const array: any[] = response.data;

        this.listadoAutoridasSancionadora = response.data;

        /* this.listadoAutoridasSancionadora = array.map((data: any) => {
          return {
            label: data.valorPrimario,
          };
        });*/
      });
  }

  loadEvaluacion(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });
    this.listMedidasCorrectivas(page);
  }

  loadData() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    setTimeout(() => this.dialog.closeAll(), 250);
  }

  listMedidasCorrectivas(page?: Page) {
    this.editar = false;
    this.lstMedidasCorrectivasDetalle = [];
    this.medidasCorrectivasCabeceraObj = new medidasCorrectivasCbeceraModel();
    var params = {
      idPlanManejo: this.idPlanManejo,
      pageNum: page?.pageNumber,
      pageSize: page?.pageSize,
    };
    this.medidasCorrectivasService
      .listarMedida(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.idMedida = element.idMedida;
            this.medidasCorrectivasCabeceraObj.fechaCumplimiento = element.fechaCumplimiento;
            this.medidasCorrectivasCabeceraObj.cumplimiento = element.cumplimiento;

            this.medidasCorrectivasCabeceraObj.numeroContrato = element.numeroContrato;

            this.implementacion = element.proceso;
            this.medidasCorrectivasCabeceraObj.conforme = element.conforme;
            element.listaMedidaDetalle.forEach((item: any) => {
              this.medidaCorrectivaObj = new medidasCorrectivasModel(item);
              this.lstMedidasCorrectivasDetalle.push(this.medidaCorrectivaObj);
            });
          });
          this.editar = false;
        } else {
          this.editar = true;

          this.listResumenEjecutivo();

        }


      });
  }

  registrarArchivoID(event:any){

    if(event>0){
      this.archivoEvidenciaMedidaCorrectiva = false;
    }else{
      this.archivoEvidenciaMedidaCorrectiva = true;
    }

  }

  abrirModal(tipo: string, rowIndex: number, data?: medidasCorrectivasModel) {
    this.tipoAccion = tipo;
    if (this.tipoAccion == "C") {
      this.fechaResolucion = "";
      this.filePMFI.nombreFile = "";
      this.tituloModalMedidas = "Agregar Medida Correctiva";
      this.medidaCorrectivaObj = new medidasCorrectivasModel();
    } else {
      this.tituloModalMedidas = "Editar Medida Correctiva";
      this.filePMFI.nombreFile = "";
      this.listarArchivoDemaFirmado(data?.idArchivo);
      this.medidaCorrectivaObj = new medidasCorrectivasModel(data);
      this.medidaCorrectivaObj.id = rowIndex;
    }
    this.verModalMedidas = true;
  }

  AgregarMedida(context?: any) {

    if (!this.validarMedidas()) {
      return;
    }



    if (this.tipoAccion == "C") {
      this.medidaCorrectivaObj.fechaResolucion = new Date(this.fechaResolucion);

      if(this.medidaCorrectivaObj.autoridadSancionadora == "" || this.medidaCorrectivaObj.autoridadSancionadora == null){
        this.medidaCorrectivaObj.autoridadSancionadora = this.listadoAutoridasSancionadora[0].valorPrimario;
      }

      this.lstMedidasCorrectivasDetalle.push(this.medidaCorrectivaObj);
      this.pendiente = true;
    }


    if (this.tipoAccion == "E") {
      this.medidaCorrectivaObj = new medidasCorrectivasModel(context);
      this.lstMedidasCorrectivasDetalle[context.id] = context;
      this.pendiente = true;
    }
    this.verModalMedidas = false;

  }

  guardarMedidasCorrectivas() {
    if (!this.validarFechaCumplimiento()) {
      return;
    }
    let fechaCumplimiento = new Date(
      this.medidasCorrectivasCabeceraObj.fechaCumplimiento
    );
    var params = {
      idMedida: this.idMedida,
      idPlanManejo: this.idPlanManejo,
      cumplimiento: this.medidasCorrectivasCabeceraObj.cumplimiento,
      descripcion: "POAC",
      conforme: this.medidasCorrectivasCabeceraObj.conforme,
      fechaCumplimiento: fechaCumplimiento,
      idUsuarioRegistro: this.user.idUsuario,
      numeroContrato: this.medidasCorrectivasCabeceraObj.numeroContrato,
      observacion: "",
      listaMedidaDetalle: [...this.lstMedidasCorrectivasDetalle],
    };

    this.medidasCorrectivasService
      .registrarMedida(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.pendiente = false;
          this.listMedidasCorrectivas();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openEliminarMedida(event: Event, index: number, medida: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (medida.idMedidaDet != 0) {
          var params = [
            {
              idMedidaDet: medida.idMedidaDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];
          this.medidasCorrectivasService
            .eliminarMedidaDetalle(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                // this.toast.ok(response?.message);
                this.toast.ok('Se eliminó medida correctiva.');
                this.lstMedidasCorrectivasDetalle.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.lstMedidasCorrectivasDetalle.splice(index, 1);
        }
      },
    });
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";
    let fechaActual = new Date();
    let fechaInicio = new Date(this.fechaResolucion);

    if (
      this.medidaCorrectivaObj.numeroResolucion == null ||
      this.medidaCorrectivaObj.numeroResolucion == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Número de Resolución.\n";
    }
    if (this.fechaResolucion == null) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar:Fecha de Documento.\n";
    }
    if (
      this.medidaCorrectivaObj.detalle == null ||
      this.medidaCorrectivaObj.detalle == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Detalle.\n";
    }
    if (
      this.medidaCorrectivaObj.proceso == null ||
      this.medidaCorrectivaObj.proceso == 0
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un proceso.\n";
    }
    if (
      this.medidaCorrectivaObj.plazo == null ||
      this.medidaCorrectivaObj.plazo == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Plazo.\n";
    }
    if (fechaInicio < fechaActual) {
      validar = false;
      mensaje = mensaje +=
        "(*)La Fecha de Documento debe ser posterior a la Fecha Actual.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === "PDF") {
          include = TabMedidasCorrectivasComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "el tipo de documento no es válido",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB",
          });
        } else {
          if (type === "PDF") {
            this.filePMFI.nombreFile = e.target.files[0].name;
            this.filePMFI.file = e.target.files[0];
            this.filePMFI.descripcion = type;
            this.filePMFI.inServer = false;
            this.verEnviar1 = true;
            this.archivoCargado = true;
            this.files.push(this.filePMFI);
            this.guardarArchivoPMFIFirmado();
          }
        }
      }
    }
  }

  guardarArchivoPMFIFirmado() {
    if (this.files.length != 0) {
      this.files.forEach((t: any) => {
        if (t.inServer !== true) {
          this.tieneArchivo = true;
          let item = {
            id: this.user.idUsuario,
            tipoDocumento: 52,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.anexosService
            .cargarAnexos(item.id, item.tipoDocumento, t.file)
            .subscribe((result: any) => {
              this.dialog.closeAll();
              this.registrarArchivo(result.data);
              this.medidaCorrectivaObj.idArchivo = result.data;
              this.archivoCargado = false;
            });
        }
      });
    }
  }

  registrarArchivo(id: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: "POACADR",
      descripcion: "",
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: "",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      this.dialog.closeAll();
      if (response.success == true) {
        this.toast.ok("Se cargó el archivo correctamente.");
        this.listarArchivoDemaFirmado();
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  listarArchivoDemaFirmado(id?: number) {
    var params = {
      idArchivo: id ? id : null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: 52,
      codigoProceso: "POACADR",
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarPMFI = true;
        this.eliminarPMFI = false;
        result.data.forEach((element: any) => {
          this.filePMFI.nombreFile = element.nombreArchivo;
          this.idArchivoPMFI = element.idArchivo;
        });
      } else {
        this.eliminarPMFI = true;
        this.cargarPMFI = false;
      }
    });
  }

  eliminarArchivoPmfi() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivoPMFI))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el archivo correctamente");
        this.cargarPMFI = false;
        this.eliminarPMFI = true;
        this.filePMFI.nombreFile = "";
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  validarFechaCumplimiento(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";
    let fechaActual = new Date();
    let fechaCumplimiento = new Date(
      this.medidasCorrectivasCabeceraObj.fechaCumplimiento
    );
    if (
       this.medidasCorrectivasCabeceraObj.numeroContrato == null ||
       this.medidasCorrectivasCabeceraObj.numeroContrato == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: N° contrato.\n";
     }
    if (
      this.medidasCorrectivasCabeceraObj.cumplimiento == null ||
      this.medidasCorrectivasCabeceraObj.cumplimiento == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Cumplimiento.\n";
    }
    if (
      this.medidasCorrectivasCabeceraObj.conforme == null ||
      this.medidasCorrectivasCabeceraObj.conforme == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un proceso .\n";
    }

    if (fechaCumplimiento < fechaActual) {
      validar = false;
      mensaje = mensaje +=
        "(*)La Fecha de Cumplimiento debe ser posterior a la Fecha Actual.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

}


export class medidasCorrectivasModel {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.detalle = data.detalle ? data.detalle : "";
      this.idArchivo = data.idArchivo ? data.idArchivo : 0;
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.autoridadSancionadora = data.autoridadSancionadora
        ? data.autoridadSancionadora
        : 0;
      this.numeroResolucion = data.numeroResolucion
        ? data.numeroResolucion
        : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.responsable = data.responsable ? data.responsable : "";
      this.plazo = data.plazo ? data.plazo : "";
      this.proceso = data.proceso ? data.proceso : "";
      this.contrato = data.contrato ? data.contrato : "";
      this.fechaResolucion = data.fechaResolucion
        ? new Date(data.fechaResolucion)
        : null;
      this.idMedidaDet = data.idMedidaDet ? data.idMedidaDet : null;

      return;
    }
  }
  id!: number | null;
  descripcion: string = "";
  detalle: string = "";
  idArchivo: number = 0;
  autoridadSancionadora: string = "";
  numeroResolucion: string = "";
  observacion: string = "";
  responsable: string = "";
  plazo: string = "";
  proceso: number = 0;
  fechaResolucion!: Date | null;
  idMedidaDet: any = null;
  contrato: string = "";
}

export class medidasCorrectivasCbeceraModel {
  constructor(data?: any) {
    if (data) {
      this.cumplimiento = data.cumplimiento ? data.cumplimiento : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.fechaCumplimiento = data.fechaCumplimiento
        ? data.fechaCumplimiento
        : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.numeroContrato = data.numeroContrato ? data.numeroContrato : "";
      this.conforme = data.conforme ? data.conforme : "";

      return;
    }
  }
  cumplimiento: string = "";
  descripcion: string = "";
  idUsuarioRegistro: number = 0;
  fechaCumplimiento: string = "";
  detalle: string = "";
  numeroContrato: string = "";
  observacion: string = "";
  conforme: string = "";
}
