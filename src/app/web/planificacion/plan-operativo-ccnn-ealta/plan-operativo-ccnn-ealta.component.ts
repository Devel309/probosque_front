import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import { PlanManejoService, UsuarioService } from '@services';
import { Perfiles } from '../../../model/util/Perfiles';
import {CodigoEstadoPlanManejo} from '../../../model/util/CodigoEstadoPlanManejo';
@Component({
  selector: 'plan-operativo-ccnn-ealta',
  templateUrl: './plan-operativo-ccnn-ealta.component.html',
  styleUrls: ['./plan-operativo-ccnn-ealta.component.scss']
})
export class PlanOperativoComponent implements OnInit {

  idPlanManejo!: number;
  usuario!: UsuarioModel;
  isPerfilArffs: boolean = false;
  disabled: boolean = false;
  idPlanManejoPadre!: number;
  tabIndex = 0;
  obj: any ={};

  constructor(private route: ActivatedRoute, private user: UsuarioService, private planManejoService: PlanManejoService) {
    this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
    this.usuario = this.user.usuario;
  }

  ngOnInit(): void {
    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilArffs = true;
    } else {
      this.isPerfilArffs = false;
    }

    this.obtenerPlan();
  }


  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    }

    this.planManejoService.obtenerPlanManejo(params).subscribe((result: any) => {
      if(result.data.codigoEstado == CodigoEstadoPlanManejo.PRESENTADO || result.data.codigoEstado == CodigoEstadoPlanManejo.EN_EVALUACION){
        this.disabled = true;
      }

      this.idPlanManejoPadre = result.data.idPlanManejoPadre;
    })
  }
  ngAfterViewInit(){
    if (localStorage.getItem('EvalResuDet')) {
      this.obj = JSON.parse('' + localStorage.getItem('EvalResuDet')?.toString());
      this.setTabIndex(this.obj);
    }
    //console.log("MAIN ngAfterViewInit");
  }

  setTabIndex(obj: any){
    var cod = obj.tab.replace(obj.codigoEvaluacionDet,'');

    if(cod === 'IG') this.tabIndex = 0;
    else if (cod === 'REEVRIIC') {
      this.tabIndex = 0;

      setTimeout (() => {
      
        this.tabIndex = 1;
      }, 1000);}
    else if (cod === 'RARPA') {this.tabIndex = 1;}
    else if (cod === 'O') this.tabIndex = 2;
    else if (cod === 'IBAAA') this.tabIndex = 3;
    else if (cod === 'AA') this.tabIndex = 4;
    else if (cod === 'AS') this.tabIndex = 5;
    else if (cod === 'PB') this.tabIndex = 6;
    else if (cod === 'M') this.tabIndex = 7;
    else if (cod === 'PC') this.tabIndex = 8;
    else if (cod === 'C') this.tabIndex = 9;
    else if (cod === 'ODA') this.tabIndex = 10;
    else if (cod === 'RMF') this.tabIndex = 11;
    else if (cod === 'CA') this.tabIndex = 12;
    else if (cod === 'AC') this.tabIndex = 13;
    else if (cod === 'AN') this.tabIndex = 14;
    else if (cod === 'GPPP') this.tabIndex = 15;
    else if (cod === 'RE') this.tabIndex = 16;
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }

}
