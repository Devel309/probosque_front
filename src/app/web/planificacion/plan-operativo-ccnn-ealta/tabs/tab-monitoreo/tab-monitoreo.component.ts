import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmationService } from "primeng/api";
import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ObjetivosService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/monitoreo.service";
import { ToastService } from "@shared";
import {LineamientoInnerModel} from '../../../../../model/Comun/LineamientoInnerModel';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {CodigosTabEvaluacion} from '../../../../../model/util/CodigosTabEvaluacion';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {finalize} from 'rxjs/operators';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import { lstDetalleModel, MonitoreoPOCCModel } from "src/app/model/monitoreoPOCC";
import { timeStamp } from "console";
import { MonitoreoService } from "src/app/service/planificacion/monitoreo.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-tab-monitoreo",
  templateUrl: "./tab-monitoreo.component.html",
  styleUrls: ["./tab-monitoreo.component.scss"],
  providers: [MessageService, ConfirmDialogModule],
})
export class TabMonitoreoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;



  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  tituloModalMantenimiento: string = " ";
  edit: boolean = false;

  tipoAccion: string = "";
  verModalMantenimiento: boolean = false;
  lstDetalle1: Model1[] = [];
  pendiente: boolean = false;
  idMonitoreo: number = 0;

  monitoreo! : MonitoreoPOCCModel;
  detalleMonitoreo! : lstDetalleModel;
  idEdit : number = -1;
  //lstDetalleModel

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_8;

  codigoAcordeon8_81: string =CodigosTabEvaluacion.POAC_TAB_8_81

  evaluacion8_81 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon8_81
  });

  evaluacion:any;


  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private objetivosService: ObjetivosService,
    private toast: ToastService,
    private monitoreoService: MonitoreoService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {



  }

  ngOnInit(): void {

    this.monitoreo = new MonitoreoPOCCModel({
      idMonitoreo : 0,
      idPlanManejo : this.idPlanManejo,
      codigoMonitoreo : this.codigoProceso,
      idUsuarioRegistro : this.user.idUsuario,
      lstDetalle : []
  });
    

    this.listarMonitoreo();

    if(this.isPerfilArffs)
      this.obtenerEvaluacion();

  }

  listarMonitoreo() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codigoMonitoreo: this.codigoProceso,
    };

    this.monitoreoService.listarMonitoreoPOCC(params).subscribe((response: any) => {

      if(response.data.length > 0){
        this.monitoreo = response.data[0];
      }


    });
  }

  guardarMonitoreo() {

    this.monitoreoService.registrarMonitoreoPOCC([this.monitoreo])
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se registró el monitoreo correctamente.");
          this.pendiente = false;
          this.listarMonitoreo();
        }
      });
  }


  abrirModal(tipo: string, rowIndex: number, data?: Model1) {
    this.tipoAccion = tipo;
    if (this.tipoAccion == "C") {
      this.tituloModalMantenimiento = "Registrar Monitoreo";
      this.detalleMonitoreo = new lstDetalleModel({
        idMonitoreoDetalle: 0 ,
        codigoMonotoreoDet : this.codigoTab,
        idUsuarioRegistro: this.user.idUsuario,
        idUsuarioModificacion : this.user.idUsuario
      });
    } else {
      this.tituloModalMantenimiento = "Editar Monitoreo";
      this.detalleMonitoreo = new lstDetalleModel(data);
      this.idEdit = rowIndex;
    }
    this.verModalMantenimiento = true;
  }


  AgregarMonitoreo() {

    if(this.validarMonitoreo()){

      if (this.tipoAccion == "C") {
        this.monitoreo.lstDetalle.push(this.detalleMonitoreo);
        this.pendiente = true;
      }
      if (this.tipoAccion == "E") {
        let deta = new lstDetalleModel(this.detalleMonitoreo);
        this.monitoreo.lstDetalle[this.idEdit] = deta;
        this.pendiente = true;
      }
      this.verModalMantenimiento = false;

    }
  }

  openEliminarMonitoreo(event: Event, index: number, monitoreo: lstDetalleModel) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (monitoreo.idMonitoreoDetalle != 0) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          var params = {
            idMonitoreoDetalle: monitoreo.idMonitoreoDetalle,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.objetivosService.EliminarDetalleMonitoreo(params).subscribe(
            (result: any) => {
              if (result.success == true) {
                this.toast.ok("Se eliminó el monitoreo correctamente.");
                this.listarMonitoreo();
                this.pendiente = false;
                this.dialog.closeAll();
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            }
          );
        } else {
          this.monitoreo.lstDetalle.splice(index, 1);
          this.toast.ok("Se eliminó el monitoreo correctamente.");
        }
      },
    });
  }

  validarMonitoreo(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.detalleMonitoreo.actividad == null || this.detalleMonitoreo.actividad == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Actividad.\n";
    }
    if (this.detalleMonitoreo.descripcion == null || this.detalleMonitoreo.descripcion == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Descripción del Monitoreo.\n";
    }
    if (this.detalleMonitoreo.responsable == null || this.detalleMonitoreo.responsable == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Responsable.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion8_81 = Object.assign(this.evaluacion8_81,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon8_81));

          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion8_81])){

      if(this.evaluacion) {
      this.evaluacion.listarEvaluacionDetalle = [];
      this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion8_81);

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res:any) => {
          this.toast.ok(res.message);
          this.obtenerEvaluacion();
        })
    }
    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }

}

export class Model1 {
  constructor(data?: any) {
    if (data) {
      this.id = data.id;
      this.actividad = data.actividad ? data.actividad : "";
      this.codigoMonotoreoDet = data.codigoMonotoreoDet
        ? data.codigoMonotoreoDet
        : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.idMonitoreoDetalle = data.idMonitoreoDetalle
        ? data.idMonitoreoDetalle
        : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.observacion = data.observacion ? data.observacion : "";
      this.responsable = data.responsable ? data.responsable : "";

      return;
    }
  }
  id: number = 0;
  actividad: string = "";
  codigoMonotoreoDet: string = "";
  descripcion: string = "";
  idMonitoreoDetalle: number = 0;
  idUsuarioModificacion: number = 0;
  idUsuarioRegistro: number = 0;
  observacion: string = "";
  responsable: string = "";
}
