import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { MatDialog } from '@angular/material/dialog';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { descargarArchivo, ToastService } from '@shared';
import { LoadingComponent } from '../../../../../components/loading/loading.component';
import { finalize } from 'rxjs/operators';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { ActividadesAprovechamientoService } from 'src/app/service/actividades-aprovechamiento-pocc.service';
import { ArchivoService, UsuarioService } from '@services';
import { ActividadesAprovechamientoPOACModel, CodigosActividadesAprovechamientoPOAC, listAtividadDet,} from 'src/app/model/ActividadesAprovechamientoPOACModel';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CensoForestalService } from 'src/app/service/censoForestal';
import {environment} from '@env/environment';
import { Router } from '@angular/router';
import { UrlFormatos } from 'src/app/model/urlFormatos';

@Component({
  selector: 'app-tab-actividades-aprovechamiento',
  templateUrl: './tab-actividades-aprovechamiento.component.html',
  styleUrls: ['./tab-actividades-aprovechamiento.component.scss'],
})
export class TabActividadesAprovechamientoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  varAssets = `${environment.varAssets}`;

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  displayBasic: boolean = false;
  edit: boolean = false;

  displayPlan: boolean = false;
  editPlanificacion: boolean = false;
  indexPlanificacion: number = 0;

  displayOperacion: boolean = false;
  editOperacion: boolean = false;
  indexOperacion: number = 0;

  displayArrastre: boolean = false;
  editArrastre: boolean = false;
  indexArrastre: number = 0;

  displayTransporte: boolean = false;
  editTransporte: boolean = false;
  indexTrasporte: number = 0;

  descripcion: string = '';

  actividades: [] = [];
  showWarn: boolean = true;
  masterSelected: boolean = false;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_5;

  codigoAcordeon5_51: string = CodigosTabEvaluacion.POAC_TAB_5_51;
  codigoAcordeon5_52: string = CodigosTabEvaluacion.POAC_TAB_5_52;
  codigoAcordeon5_53: string = CodigosTabEvaluacion.POAC_TAB_5_53;
  codigoAcordeon5_54: string = CodigosTabEvaluacion.POAC_TAB_5_54;
  codigoAcordeon5_55: string = CodigosTabEvaluacion.POAC_TAB_5_55;
  codigoAcordeon5_56: string = CodigosTabEvaluacion.POAC_TAB_5_56;
  codigoAcordeon5_57: string = CodigosTabEvaluacion.POAC_TAB_5_57;

  evaluacion5_51: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_51,
  });
  evaluacion5_52: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_52,
  });
  evaluacion5_53: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_53,
  });
  evaluacion5_54: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_54,
  });
  evaluacion5_55: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_55,
  });
  evaluacion5_56: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_56,
  });
  evaluacion5_57: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon5_57,
  });

  evaluacion: any;

  // Variables actividades aprovechamiento

  delimitacionAreaAprov: any[] = [
    { demarcacion: 'Trochas' },
    { demarcacion: 'Letreros' },
    { demarcacion: 'Árboles pintados' },
    { demarcacion: 'Hitos' },
    { demarcacion: 'Otros (especificar)' },
  ];

  distanciaTrochas: number = 0;
  listEspecies: any[] = [];
  planificacionForm: listAtividadDet = {} as listAtividadDet;
  arrastreForm: listAtividadDet = {} as listAtividadDet;
  trasporteForm: listAtividadDet = {} as listAtividadDet;
  operacionForm: listAtividadDet = {} as listAtividadDet;

  delimitacionAreaAprovechamiento5_1: ActividadesAprovechamientoPOACModel =
    new ActividadesAprovechamientoPOACModel();

  censoComercial: ActividadesAprovechamientoPOACModel =
    new ActividadesAprovechamientoPOACModel();

  planificacionConstruccionInfAprov5_3: ActividadesAprovechamientoPOACModel =
    new ActividadesAprovechamientoPOACModel();

  operacionesCorta5_4: ActividadesAprovechamientoPOACModel =
    new ActividadesAprovechamientoPOACModel();

  operacionesArrastreTransporte5_5: ActividadesAprovechamientoPOACModel =
    new ActividadesAprovechamientoPOACModel();

  procesamientoLocal5_6: ActividadesAprovechamientoPOACModel =
    new ActividadesAprovechamientoPOACModel();

  listAprovechamiento: listAtividadDet[] = [];
  list: ActividadesAprovechamientoPOACModel[] = [];

  listaPlanificacionConstruccion: listAtividadDet[] = [];
  listaTransportes: listAtividadDet[] = [];
  listaArrastres: listAtividadDet[] = [];
  listaOperacionesCorta: listAtividadDet[] = [];
  listEspeciesResultados: any[] = [];
  listEspeciesVolumen: any = [];
  totalListEspeciesVolumen = {
    totalsPc1: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc2: { numArbolesPC: 0, volumenPC: 0 },
    totalsPc3: { numArbolesPC: 0, volumenPC: 0 },
    totalArboles: 0,
    totalVolumen: 0,
  };

  codigosActividadesAprovechamientoPOAC = CodigosActividadesAprovechamientoPOAC;
  selectArrastre: string = '';
  selectTransporte: string = '';
  selectMaquinariaEquipo: string = '';
  listCensoComercialResultados: ActividadesAprovechamientoPOACModel = new ActividadesAprovechamientoPOACModel();

  totalListEspeciesResultados: any = {
    total30a39: 0,
    total40a49: 0,
    total50a59: 0,
    total60a69: 0,
    total70a79: 0,
    total80a89: 0,
    total90aMas: 0,
    totalPc: 0,
    totalHa: 0,
    totalV30a39: 0,
    totalV40a49: 0,
    totalV50a59: 0,
    totalV60a69: 0,
    totalV70a79: 0,
    totalV80a89: 0,
    totalV90aMas: 0,
    totalVPc: 0,
    totalVHa: 0,
  };


  archivoPOAC: any;

  nombreGenerado: string = "";
  plantillaPOAC: string = "";

  constructor(
    private dialog: MatDialog,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private actividadesAprovechamientoService: ActividadesAprovechamientoService,
    private user: UsuarioService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private censoForestalService: CensoForestalService,
    private router: Router,
    private archivoService: ArchivoService
  ) {

    this.archivoPOAC = {
      file: null,
    };
  }

  ngOnInit(): void {
    // this.listarResultadosForestalesMaderables();
    // this.listarResultadosForestales();
    this.listarActividadesAprov();
    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  agregarEspecie() {}

  abrirModalEspecies() {
    this.displayBasic = true;
  }

  cerrarModalEspecies() {
    this.displayBasic = false;
  }

  // Planificacion modal
  abrirPlanificacion() {
    this.displayPlan = true;
    this.planificacionForm = new listAtividadDet();
    this.editPlanificacion = false;
  }

  openEditarPlanificacion(data: any, index: number) {
    this.displayPlan = true;
    this.planificacionForm = new listAtividadDet(data);
    this.editPlanificacion = true;
    this.indexPlanificacion = index;
  }

  agregarPlanificacion() {
    if (this.editPlanificacion) {
      this.editarPlanificacion();
    } else {
      this.registrarNuevaPlanificacion();
    }
  }

  validacionCamposPlanificacion() {
    let esValido = true;
    let mensaje = '';

    if (this.planificacionForm.descripcion == '') {
      mensaje += '(*) Debe ingresar camino.\n';
      esValido = false;
    }
    if (this.planificacionForm.detalle == '') {
      mensaje += '(*) Debe ingresar Km. de construcción.\n';
      esValido = false;
    }
    if (this.planificacionForm.familia == '') {
      mensaje += '(*) Debe ingresar n° de alcantarillas.\n';
      esValido = false;
    }
    if (this.planificacionForm.observacion == '') {
      mensaje += '(*) Debe ingresar n° de puentes.\n';
      esValido = false;
    }
    if (this.planificacionForm.observacion == '') {
      mensaje += '(*) Debe ingresar n° de cunetas.\n';
      esValido = false;
    }

    if (!esValido) this.errorMessage(mensaje);

    return esValido;
  }

  registrarNuevaPlanificacion() {
    if (!this.validacionCamposPlanificacion()) return;

    this.listaPlanificacionConstruccion.push(this.planificacionForm);
    this.displayPlan = false;
  }

  editarPlanificacion() {
    if (!this.validacionCamposPlanificacion()) return;

    this.listaPlanificacionConstruccion[this.indexPlanificacion] =
      this.planificacionForm;
    this.displayPlan = false;
  }

  cerrarModalPlanificacion() {
    this.displayPlan = false;
  }

  successMessage(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: message,
    });
  }

  errorMessage(message: string) {
    this.messageService.add({ severity: 'warn', summary: '', detail: message });
  }

  // arrastre modal

  abrirModalArrastre() {
    this.displayArrastre = true;
    this.arrastreForm = new listAtividadDet();
    this.editArrastre = false;
  }

  openEditarArrastre(data: any, index: number) {
    this.displayArrastre = true;
    this.arrastreForm = new listAtividadDet(data);
    this.editArrastre = true;
    this.indexArrastre = index;
  }

  agregarArrastre() {
    if (this.editArrastre) {
      this.editarArrastre();
    } else {
      this.registrarNuevoArrastre();
    }
  }
  validacionCamposArrastre() {
    let esValido = true;
    let mensaje = '';
    if (this.arrastreForm.descripcion == '') {
      mensaje += '(*) Debe ingresar método.\n';
      esValido = false;
    }
    if (this.arrastreForm.personal == '') {
      mensaje += '(*) Debe ingresar personal.\n';
      esValido = false;
    }
    if (!esValido) this.errorMessage(mensaje);

    return esValido;
  }

  registrarNuevoArrastre() {
    if (!this.validacionCamposArrastre()) return;

    this.listaArrastres.push(this.arrastreForm);
    this.displayArrastre = false;
  }

  editarArrastre() {
    if (!this.validacionCamposArrastre()) return;

    this.listaArrastres[this.indexPlanificacion] = this.arrastreForm;
    this.displayArrastre = false;
  }

  cerrarModalArrastre() {
    this.displayArrastre = false;
  }

  // transporte modal

  abrirModalTransporte() {
    this.displayTransporte = true;
    this.trasporteForm = new listAtividadDet();
    this.editTransporte = false;
  }

  openEditarTransporte(data: any, index: number) {
    this.displayTransporte = true;
    this.trasporteForm = new listAtividadDet(data);
    this.editTransporte = true;
    this.indexTrasporte = index;
  }

  agregarTransporte() {
    if (this.editTransporte) {
      this.editarTransporte();
    } else {
      this.registrarNuevoTransporte();
    }
  }

  validacionCamposTransporte() {
    let esValido = true;
    let mensaje = '';
    if (this.trasporteForm.descripcion == '') {
      mensaje += '(*) Debe ingresar transporte.\n';
      esValido = false;
    }
    if (this.trasporteForm.personal == '') {
      mensaje += '(*) Debe ingresar personal.\n';
      esValido = false;
    }
    if (this.trasporteForm.maquinariaMaterial == '') {
      mensaje += '(*) Debe ingresar maquinaria/equipo.\n';
      esValido = false;
    }
    if (!esValido) this.errorMessage(mensaje);

    return esValido;
  }

  registrarNuevoTransporte() {
    if (!this.validacionCamposTransporte()) return;

    this.listaTransportes.push(this.trasporteForm);
    this.displayTransporte = false;
  }

  editarTransporte() {
    if (!this.validacionCamposTransporte()) return;

    this.listaTransportes[this.indexTrasporte] = this.trasporteForm;
    this.displayTransporte = false;
  }

  cerrarModalTransporte() {
    this.displayTransporte = false;
  }

  // Operaciones corta modal

  abrirModalOperacionesCorta() {
    this.displayOperacion = true;
    this.operacionForm = new listAtividadDet();
    this.editOperacion = false;
  }

  openEditarOperacionCorta(data: any, index: number) {
    this.displayOperacion = true;
    this.operacionForm = new listAtividadDet(data);
    this.editOperacion = true;
    this.indexOperacion = index;
  }

  agregarOperacionCorta() {
    if (this.editOperacion) {
      this.editarOperacion();
    } else {
      this.registrarNuevaOperacion();
    }
  }

  validacionCamposOperacion() {
    let esValido = true;
    let mensaje = '';
    if (this.operacionForm.descripcion == '') {
      mensaje += '(*) Debe ingresar método.\n';
      esValido = false;
    }
    if (this.operacionForm.personal == '') {
      mensaje += '(*) Debe ingresar personal.\n';
      esValido = false;
    }

    if (this.operacionForm.personal == '') {
      mensaje += '(*) Debe ingresar equipo.\n';
      esValido = false;
    }
    if (!esValido) this.errorMessage(mensaje);

    return esValido;
  }

  registrarNuevaOperacion() {
    if (!this.validacionCamposOperacion()) return;

    this.listaOperacionesCorta.push(this.operacionForm);
    this.displayOperacion = false;
  }

  editarOperacion() {
    if (!this.validacionCamposOperacion()) return;

    this.listaOperacionesCorta[this.indexOperacion] = this.operacionForm;
    this.displayOperacion = false;
  }

  cerrarModalOperacion() {
    this.displayOperacion = false;
  }

  openEditarEspecie(data: any) {}

  openEliminarEspecie(event: Event, index: number, data: any) {}

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion5_51 = Object.assign(
                this.evaluacion5_51,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_51
                )
              );
              this.evaluacion5_52 = Object.assign(
                this.evaluacion5_52,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_52
                )
              );
              this.evaluacion5_53 = Object.assign(
                this.evaluacion5_53,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_53
                )
              );
              this.evaluacion5_54 = Object.assign(
                this.evaluacion5_54,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_54
                )
              );
              this.evaluacion5_55 = Object.assign(
                this.evaluacion5_55,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_55
                )
              );
              this.evaluacion5_56 = Object.assign(
                this.evaluacion5_56,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_56
                )
              );
              this.evaluacion5_57 = Object.assign(
                this.evaluacion5_57,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon5_57
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacion5_51,
        this.evaluacion5_52,
        this.evaluacion5_53,
        this.evaluacion5_54,
        this.evaluacion5_55,
        this.evaluacion5_56,
        this.evaluacion5_57,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_51);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_52);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_53);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_54);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_55);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_56);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion5_57);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  listarResultadosForestales() {
    if (this.listEspeciesResultados.length == 0) {
      var params = {
        idPlanDeManejo: this.idPlanManejo,
        tipoPlan: 'POAC',
      };

      this.actividadesAprovechamientoService
        .listarResultadosForestalesMaderables(params)
        .subscribe((response: any) => {
          if (response.data.length != 0 &&
            !!response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto &&
            !!response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto.resultadosRFMTablas
          ) {
            this.censoComercial.areaTotal = response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto.areaTotalCensada;
            this.censoComercial.nroEspecies = response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto.contadorEspecies;
            this.censoComercial.nroArboles = response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto.numeroArbolesTotal;
            this.listEspeciesResultados = response.data.censoComercialDto.resultadosRecursosForestalesMaderablesDto.resultadosRFMTablas;

            this.calculoTotalesEspeciesResultado();
          } else {
            if (this.showWarn) {
              this.showWarn = false;
              this.errorMessage("No existen datos sincronizados para el Plan de Manejo " + this.idPlanManejo + " Tipo de plan " + this.codigoProceso);
            }
          }
        });
    }
  }

  listarResultadosForestalesMaderables() {
    if (this.listEspecies.length == 0) {
      var params = {
        idPlanDeManejo: this.idPlanManejo,
        tipoPlan: 'POAC',
      };

      this.actividadesAprovechamientoService
        .listarSincronizacionEspeciesMaderables(params)
        .subscribe((response: any) => {
          if (response.data.length != 0) {
            response.data.map((data: any) => {
              var params = new listAtividadDet();
              params.codActvAproveDet = CodigosActividadesAprovechamientoPOAC.SUBACORDEON5_2_1;
              params.idUsuarioRegistro = this.user.idUsuario;
              params.nombreCientifica = data.textNombreCientifico;
              params.nombreComun = data.textNombreComun;
              params.familia = data.familia;
              params.dcm = data.dmc;
              this.listEspecies.push(params);
            });
          } else {
            if (this.showWarn) {
              this.showWarn = false;
              this.errorMessage("No existen datos sincronizados para el Plan de Manejo " + this.idPlanManejo + " Tipo de plan " + this.codigoProceso);
            }
          }
        });
    }
  }

  calculoTotalesEspeciesResultado() {
    this.listEspeciesResultados.map((data) => {
      this.totalListEspeciesResultados.total30a39 += parseFloat(data.dap30a39);

      this.totalListEspeciesResultados.total40a49 += parseFloat(data.dap40a49);

      this.totalListEspeciesResultados.total50a59 += parseFloat(data.dap50a59);

      this.totalListEspeciesResultados.total60a69 += parseFloat(data.dap60a69);

      this.totalListEspeciesResultados.total70a79 += parseFloat(data.dap70a79);

      this.totalListEspeciesResultados.total80a89 += parseFloat(data.dap80a89);

      this.totalListEspeciesResultados.total90aMas += parseFloat(
        data.dap90aMas
      );

      this.totalListEspeciesResultados.totalPc += parseFloat(data.dapTotalPC);
      this.totalListEspeciesResultados.totalHa += parseFloat(
        data.dapTotalporHa
      );

      this.totalListEspeciesResultados.totalV30a39 += parseFloat(data.vol30a39);

      this.totalListEspeciesResultados.totalV40a49 += parseFloat(data.vol40a49);

      this.totalListEspeciesResultados.totalV50a59 += parseFloat(data.vol50a59);

      this.totalListEspeciesResultados.totalV60a69 += parseFloat(data.vol60a69);

      this.totalListEspeciesResultados.totalV70a79 += parseFloat(data.vol70a79);

      this.totalListEspeciesResultados.totalV80a89 += parseFloat(data.vol80a89);

      this.totalListEspeciesResultados.totalV90aMas += parseFloat(
        data.vol90aMas
      );

      this.totalListEspeciesResultados.totalVPc += parseFloat(data.volTotalPC);
      this.totalListEspeciesResultados.totalVHa += parseFloat(
        data.volTotalporHa
      );
    });
  }

  validarActividadesAprov() {
    if (this.validar5_1()) {
      this.guardarActividadesAprov();
    }
  }

  validar5_1() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.delimitacionAreaAprov.length > 0) {
      for (let item of this.delimitacionAreaAprov) {
        if (item.observacion === 'Otros') {
          if (!item.operaciones) {
            validar = false;
            mensaje = '(*) Debe especificar: Sistema de demarcación.';
            break;
          }
        }
      }
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }


  guardarActividadesAprov() {
    this.list = [];
    // 5.1 Delimitacion del area de aprovechamiento anual
    if (this.delimitacionAreaAprov.length > 0 || this.delimitacionAreaAprovechamiento5_1.trochas != '') {
      const delimitacionAreaAprov = new ActividadesAprovechamientoPOACModel(this.delimitacionAreaAprovechamiento5_1);

      delimitacionAreaAprov.lstactividadDet = [];

      delimitacionAreaAprov.idPlanManejo = this.idPlanManejo;
      delimitacionAreaAprov.codSubActvAprove = CodigosActividadesAprovechamientoPOAC.ACORDEON5_1;
      delimitacionAreaAprov.idUsuarioRegistro = this.user.idUsuario;
      this.delimitacionAreaAprov.map((item) => {
        if (item.checked) {
          item.detalle = "S";
        } else {
          item.detalle = "";
        }
        delimitacionAreaAprov.lstactividadDet.push(item);
      });

      this.list.push(delimitacionAreaAprov);
    }

    // 5.2 Censo comercial

    if (this.listEspecies.length > 0) {
      let censoComercialMObj = new ActividadesAprovechamientoPOACModel(
        this.censoComercial
      );
      censoComercialMObj.codSubActvAprove =
        CodigosActividadesAprovechamientoPOAC.ACORDEON5_2;

      censoComercialMObj.lstactividadDet = [];
      censoComercialMObj.idPlanManejo = this.idPlanManejo;
      censoComercialMObj.idUsuarioRegistro = this.user.idUsuario;
      if (this.listEspecies.length > 0) {
        this.listEspecies.map((item) => {
          censoComercialMObj.lstactividadDet.push(item);
        });
      }

      this.list.push(censoComercialMObj);
    }

    // 5.3 planificacion y construccion

    if (this.listaPlanificacionConstruccion.length > 0) {
      const planificacionContruccion = new ActividadesAprovechamientoPOACModel(
        this.planificacionConstruccionInfAprov5_3
      );
      planificacionContruccion.lstactividadDet = [];
      planificacionContruccion.idPlanManejo = this.idPlanManejo;
      planificacionContruccion.codSubActvAprove =
        CodigosActividadesAprovechamientoPOAC.ACORDEON5_3;
      planificacionContruccion.idUsuarioRegistro = this.user.idUsuario;
      this.listaPlanificacionConstruccion.map((item) => {
        planificacionContruccion.lstactividadDet.push(item);
      });

      this.list.push(planificacionContruccion);
    }

    //5.4 operaciones y transporte

    if (this.listaOperacionesCorta.length > 0) {
      const operacionesCorta = new ActividadesAprovechamientoPOACModel(
        this.operacionesCorta5_4
      );
      operacionesCorta.lstactividadDet = [];
      operacionesCorta.idPlanManejo = this.idPlanManejo;
      operacionesCorta.codSubActvAprove =
        CodigosActividadesAprovechamientoPOAC.ACORDEON5_4;
      operacionesCorta.idUsuarioRegistro = this.user.idUsuario;
      this.listaOperacionesCorta.map((item) => {
        operacionesCorta.lstactividadDet.push(item);
      });

      this.list.push(operacionesCorta);
    }

    // 5.5 operaciones de arrastre y transporte

    if (this.selectArrastre ||
      this.selectTransporte ||
      this.selectMaquinariaEquipo ||
      this.listaTransportes.length > 0 ||
      this.listaArrastres.length > 0) {
      const operacionesArrastreTransporte = new ActividadesAprovechamientoPOACModel(this.operacionesArrastreTransporte5_5);

      operacionesArrastreTransporte.observacion = this.selectArrastre ? this.selectArrastre : '';
      operacionesArrastreTransporte.detalle = this.selectMaquinariaEquipo ? this.selectMaquinariaEquipo : '';
      operacionesArrastreTransporte.descripcion = this.selectTransporte ? this.selectTransporte : '';
      operacionesArrastreTransporte.lstactividadDet = [];
      operacionesArrastreTransporte.idPlanManejo = this.idPlanManejo;
      operacionesArrastreTransporte.codSubActvAprove = CodigosActividadesAprovechamientoPOAC.ACORDEON5_5;
      operacionesArrastreTransporte.idUsuarioRegistro = this.user.idUsuario;

      if (this.listaTransportes.length > 0) {
        this.listaTransportes.map((item) => {
          const obj = new listAtividadDet(item);
          obj.codSubActvAproveDet = 'T';
          operacionesArrastreTransporte.lstactividadDet.push(obj);
        });
      }
      if (this.listaArrastres.length > 0) {
        this.listaArrastres.map((item) => {
          const obj = new listAtividadDet(item);
          obj.codSubActvAproveDet = 'A';
          operacionesArrastreTransporte.lstactividadDet.push(obj);
        });
      }

      this.list.push(operacionesArrastreTransporte);
    }

    // 5.6 procesamiento local

    if (this.procesamientoLocal5_6.observacion != '') {
      const procesamientoLocal = new ActividadesAprovechamientoPOACModel(this.procesamientoLocal5_6);

      procesamientoLocal.idPlanManejo = this.idPlanManejo;
      procesamientoLocal.codSubActvAprove =
        CodigosActividadesAprovechamientoPOAC.ACORDEON5_6;
      procesamientoLocal.idUsuarioRegistro = this.user.idUsuario;
      this.list.push(procesamientoLocal);
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .registrarActvidadAprovechamiento(this.list)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listarActividadesAprov();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openEliminarAprovechamiento(
    event: any,
    index: number,
    data: any,
    codigo: string,
    transArr?: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (data.idActvAproveDet != 0) {
          var params = {
            idActvAproveDet: data.idActvAproveDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesAprovechamientoService
            .eliminarActvidadAprovechamientoDet(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok('Se eliminó el registro correctamente.');

                if (codigo == CodigosActividadesAprovechamientoPOAC.ACORDEON5_3) {
                  this.listaPlanificacionConstruccion.splice(index, 1);
                } else if (codigo == CodigosActividadesAprovechamientoPOAC.ACORDEON5_4) {
                  this.listaOperacionesCorta.splice(index, 1);
                } else {
                  if (transArr == 'A') {
                    this.listaArrastres.splice(index, 1);
                  } else {
                    this.listaTransportes.splice(index, 1);
                  }
                }

                // this.listarActividadesAprov();
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          if (codigo == CodigosActividadesAprovechamientoPOAC.ACORDEON5_3) {
            this.listaPlanificacionConstruccion.splice(index, 1);
          } else if (codigo == CodigosActividadesAprovechamientoPOAC.ACORDEON5_4) {
            this.listaOperacionesCorta.splice(index, 1);
          } else {
            if (transArr == 'A') {
              this.listaArrastres.splice(index, 1);
            } else {
              this.listaTransportes.splice(index, 1);
            }
          }
        }
      },
      reject: () => {},
    });
  }

  listarActividadesAprovDefault5_1() {
    this.delimitacionAreaAprov = [];
    var parasm = {
      codActvAprove: 'POAC',
      idPlanManejo: null,
      codSubActvAprove: CodigosActividadesAprovechamientoPOAC.ACORDEON5_1,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .listarActvidadAprovechamiento(parasm)
      .subscribe((response: any) => {
        this.dialog.closeAll();

        response.data.map((element: any) => {
          // this.delimitacionAreaAprovechamiento5_1 =
          //   new ActividadesAprovechamientoPOACModel(element);

          this.delimitacionAreaAprovechamiento5_1.codSubActvAprove = CodigosActividadesAprovechamientoPOAC.ACORDEON5_3;
          if (element.lstactividadDet.length > 0) {
            element.lstactividadDet.map((data: any) => {
              const obj = new listAtividadDet(data);

              obj.idActvAproveDet = 0;
              if (obj.operaciones === "Otros (especificar)") {
                obj.observacion = "Otros";
              }

              this.delimitacionAreaAprov.push(obj);
            });
          }
        });
      });
  }

  listarActividadesAprovDefault5_3() {
    this.listaPlanificacionConstruccion = [];

    var parasm = {
      codActvAprove: 'POAC',
      idPlanManejo: null,
      codSubActvAprove: CodigosActividadesAprovechamientoPOAC.ACORDEON5_3,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .listarActvidadAprovechamiento(parasm)
      .subscribe((response: any) => {
        this.dialog.closeAll();

        response.data.map((element: any) => {
          if (element.lstactividadDet.length > 0) {
            element.lstactividadDet.map((data: any) => {
              const obj = new listAtividadDet(data);
              obj.idActvAproveDet = 0;
              this.listaPlanificacionConstruccion.push(obj);
            });
          }
        });
      });
  }

  listarActividadesAprovDefault5_5() {
    this.listaArrastres = [];
    this.listaTransportes = [];

    var parasm = {
      codActvAprove: 'POAC',
      idPlanManejo: null,
      codSubActvAprove: CodigosActividadesAprovechamientoPOAC.ACORDEON5_5,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .listarActvidadAprovechamiento(parasm)
      .subscribe((response: any) => {
        this.dialog.closeAll();

        response.data.map((element: any) => {
          // this.operacionesArrastreTransporte5_5 =
          //   new ActividadesAprovechamientoPOACModel(element);
          if (element.lstactividadDet.length > 0) {
            element.lstactividadDet.map((data: any) => {
              const obj = new listAtividadDet(data);
              obj.idActvAproveDet = 0;
              if (obj.codSubActvAproveDet == 'T') {
                this.listaTransportes.push(obj);
              }
            });
          }
        });
      });
  }

  listarActividadesAprov() {
    this.delimitacionAreaAprov = [];
    this.listEspecies = [];
    this.listaPlanificacionConstruccion = [];
    this.listaOperacionesCorta = [];
    this.listaArrastres = [];
    this.listaTransportes = [];

    var parasm = {
      codActvAprove: 'POAC',
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesAprovechamientoService
      .listarActvidadAprovechamiento(parasm)
      .pipe(finalize(() => {
        this.listarResultadosForestalesMaderables();
        this.listarResultadosForestales();
      }))
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.data.length != 0) {
          response.data.map((element: any) => {
            if (element.codSubActvAprove == CodigosActividadesAprovechamientoPOAC.ACORDEON5_1) {
              this.delimitacionAreaAprovechamiento5_1 = new ActividadesAprovechamientoPOACModel(element);

              if (element.lstactividadDet.length > 0) {
                element.lstactividadDet.map((data: any) => {
                  const obj = new listAtividadDet(data);

                  if (obj.detalle === "S") {
                    this.delimitacionAreaAprov.push({ ...obj, checked: true });
                  } else {
                    this.delimitacionAreaAprov.push(obj);
                  }
                });
                this.isAllSelected();
              } else {
                this.listarActividadesAprovDefault5_1();
              }
            }

            if (element.codSubActvAprove == CodigosActividadesAprovechamientoPOAC.ACORDEON5_2) {
              this.censoComercial = new ActividadesAprovechamientoPOACModel(element);

              if (element.lstactividadDet.length > 0) {
                element.lstactividadDet.map((data: any) => {
                  const obj = new listAtividadDet(data);
                  this.listEspecies.push(obj);
                });
              }
            }

            if (element.codSubActvAprove == CodigosActividadesAprovechamientoPOAC.ACORDEON5_3) {
              this.planificacionConstruccionInfAprov5_3 = new ActividadesAprovechamientoPOACModel(element);

              if (element.lstactividadDet.length > 0) {
                element.lstactividadDet.map((data: any) => {
                  const obj = new listAtividadDet(data);
                  this.listaPlanificacionConstruccion.push(obj);
                });
              } else {
                this.listarActividadesAprovDefault5_3();
              }
            }

            if (element.codSubActvAprove == CodigosActividadesAprovechamientoPOAC.ACORDEON5_4) {
              this.operacionesCorta5_4 = new ActividadesAprovechamientoPOACModel(element);

              if (element.lstactividadDet.length > 0) {
                element.lstactividadDet.map((data: any) => {
                  const obj = new listAtividadDet(data);
                  this.listaOperacionesCorta.push(obj);
                });
              }
            }

            if (element.codSubActvAprove == CodigosActividadesAprovechamientoPOAC.ACORDEON5_5) {
              this.operacionesArrastreTransporte5_5 = new ActividadesAprovechamientoPOACModel(element);
              this.selectArrastre = element.observacion;
              this.selectMaquinariaEquipo = element.detalle;
              this.selectTransporte = element.descripcion;

              if (element.lstactividadDet.length > 0) {
                element.lstactividadDet.map((data: any) => {
                  const obj = new listAtividadDet(data);

                  if (obj.codSubActvAproveDet == 'T') {
                    this.listaTransportes.push(obj);
                  } else {
                    this.listaArrastres.push(obj);
                  }
                });
              } else {
                this.listarActividadesAprovDefault5_3();
              }
            }

            if (element.codSubActvAprove == CodigosActividadesAprovechamientoPOAC.ACORDEON5_6) {
              this.procesamientoLocal5_6 =new ActividadesAprovechamientoPOACModel(element);
            }
          });
        } else {
          if (this.delimitacionAreaAprov.length == 0) {
            this.listarActividadesAprovDefault5_1();
          }

          if (this.listaPlanificacionConstruccion.length == 0) {
            this.listarActividadesAprovDefault5_3();
          }

          if (this.listaTransportes.length == 0) {
            this.listarActividadesAprovDefault5_5();
          }
        }
      });
  }

  agregarDemarcacion() {
    let obj: any = {};
    obj = {
      codActvAproveDet: "POACAADAAA",
      descripcion: "",
      idActvAproveDet: 0,
      operaciones: "Otros (especificar)",
      checked: true,
      observacion: "Otros"
    }
    this.delimitacionAreaAprov.push(obj);
  }

  checkUncheckAll() {
    this.delimitacionAreaAprov.map((item) => {
      item.checked = this.masterSelected;
    });
  }

  isAllSelected() {
    this.masterSelected = this.delimitacionAreaAprov.every(function (item: any) {
      return item.checked == true;
    });
  }


 /*  btnDescargarFormatoPOAC() {

    let urlExcel:string= "";

    if(this.varAssets && this.varAssets != ""){
      urlExcel = '/'+this.varAssets+'/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }else{
      urlExcel = '/assets/evaluacionAmbiental/Plantilla_CargaInventarioCenso_v1.00.xlsx';
    }
 window.location.href = urlExcel;
  } */

  btnDescargarFormato() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.nombreGenerado = UrlFormatos.CARGA_MASIVA_POAC;
    this.archivoService
      .descargarPlantilla(this.nombreGenerado)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.isSuccess == true) {
          this.toast.ok(res?.message);
          this.plantillaPOAC = res;
          descargarArchivo(this.plantillaPOAC);
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  btnCargarPOAC() {
    if (!(this.archivoPOAC.file instanceof File)) {
      this.toast.warn("Seleccione el Formato con los datos ingresados.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.censoForestalService.registrarCargaMasiva(
        this.archivoPOAC.file
      )
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response?.success) {
          this.toast.ok(
            "Se registró el archivo correctamente.\n"
          );
          this.listarActividadesAprov();

          this.archivoPOAC.file = "";
        } else {
          this.toast.error("Ocurrió un error.");
        }
      });
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
