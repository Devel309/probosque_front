import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ConfirmationService, MessageService } from "primeng/api";
import { RentabilidadManejoForestalModel } from "src/app/model/RentabilidadManejoForestal";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { HttpErrorResponse } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { RentabilidadManejoForestalService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/rentavilidad-manejo-forestal.service";
import {
  ListRentabilidadManejoForestalDetalle,
  RentabilidadPGMFA,
} from "src/app/model/rentabilidadManejo";
import { UsuarioService } from "@services";
import { isNullOrEmpty, ToastService } from "@shared";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { finalize } from "rxjs/operators";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { PoacInformacionGeneral } from "src/app/model/poac-informacion-general";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-rentabilidad-manejo-forestal",
  templateUrl: "./tab-rentabilidad-manejo-forestal.component.html",
  styleUrls: ["./tab-rentabilidad-manejo-forestal.component.scss"],
})
export class TabRentabilidadManejoForestalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled: boolean = false;
  @Input() isPerfilArffs!: boolean;

  @Output()
  public siguiente = new EventEmitter();
  @Output()
  public regresar = new EventEmitter();

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_12;
  codigoAcordeon12_1: string = CodigosTabEvaluacion.POAC_TAB_12_1;
  codigoAcordeon12_2: string = CodigosTabEvaluacion.POAC_TAB_12_2;
  UrlFormatos = UrlFormatos;

  form: PoacInformacionGeneral = new PoacInformacionGeneral();
  vigencia: number = 0;
  listRentabilidadManejo: RentabilidadManejoForestalModel[] = [];
  listRentabilidad: RentabilidadPGMFA[] = [];
  colsAnioN: any[] = [];
  colsMesN: any[] = [];
  ingresoN: any[] = [];
  egresoN: any[] = [];
  totalIngresoN: any[] = [];
  totalEgresoN: any[] = [];
  saldoTotalN: any[] = [];
  listRubrosI: any[] = [];
  listRubrosE: any[] = [];
  edit: boolean = false;
  displayBasic: boolean = false;
  tipoRubro!: number;
  otros: boolean = false;
  otrosDescripcion: string = "";
  rubro: string = "";

  evaluacionIngreso: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon12_1,
  });
  evaluacionEgreso: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon12_2,
  });
  nuevoIngresoEgresoNecesidades: boolean = false;
  evaluacion: any;

  isSaving: boolean = false;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private rentabilidadManejoForestalService: RentabilidadManejoForestalService,
    private user: UsuarioService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private informacionGeneralService: InformacionGeneralService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.obtenerListadoRubros();
    this.listarInformacionGeneralN();
    this.listarRentabilidadN();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
    this.rubro = "";
    this.otros = false;
  }

  obtenerListadoRubros() {
    this.listRubrosI = [];
    this.listRubrosE = [];

    var params = {
      codigoParametro: "PGMFA",
      idPlanManejo: null,
    };

    this.rentabilidadManejoForestalService
      .listarRentabilidadManejoForestal(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any, index: number) => {
          if (element.descripcion == "INGRE") {
            this.listRubrosI.push({
              label: element.rubro,
              value: element.rubro,
              id: index + 1,
            });
          }
          if (element.descripcion == "EGRES") {
            this.listRubrosE.push({
              label: element.rubro,
              value: element.rubro,
              id: index + 1,
            });
          }
        });
      });
  }

  listarInformacionGeneralN() {
    var params = {
      codigoProceso: this.codigoProceso,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length) {
          const last = response.data.length - 1;
          this.form = new PoacInformacionGeneral(response.data[last]);
          this.vigencia = this.form.vigencia ? this.form.vigencia : 0;
          this.crearColumnasN();
        } else {
          this.toast.warn("Se debe registrar duración de PO.");
        }
      });
  }

  crearColumnasN() {
    this.colsAnioN = [];
    this.colsMesN = [];
    if (this.vigencia > 0) {
      for (let i = 1; i <= this.vigencia; i++) {
        this.colsAnioN.push({
          header: "Año " + i,
        });
      }
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          this.colsMesN.push({
            header: "Mes " + j,
            field: "monto" + i + "-" + j,
          });
        }
      }
    }
  }

  listarRentabilidadN() {
    this.listRentabilidadManejo = [];
    var params = {
      codigoParametro: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.rentabilidadManejoForestalService
      .listarRentabilidadManejoForestal(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            element.estado = "A";
            this.listRentabilidadManejo.push(element);
          });
        }
        this.crearFilasN();
      });
  }

  crearFilasN() {
    this.ingresoN = [];
    this.egresoN = [];

    if (this.listRentabilidadManejo.length > 0) {
      this.listRentabilidadManejo.forEach((rentab) => {
        let obj: any = {};
        obj = {
          ...rentab,
        };
        rentab.listRentabilidadManejoForestalDetalle.forEach((item) => {
          obj.edit = false;
          obj.estado = "A";
          obj.idUsuarioRegistro = this.user.idUsuario;
          obj["monto" + item.anio + "-" + item.mes] = item.monto.toFixed(2);
          obj["idRentManejoForestalDet" + item.anio + "-" + item.mes] =
            item.idRentManejoForestalDet;
        });
        if (isNullOrEmpty(rentab.descripcion)) {
          obj.otros = false;
        } else {
          obj.otros = true;
        }
        if (rentab.idTipoRubro == 1) {
          this.ingresoN.push(obj);
        } else {
          this.egresoN.push(obj);
        }
      });
    }

    // this.crearFilasDefaultN();
    this.calcularTotalIngresoN(false);
    this.calcularTotalEgresoN(false);
    this.calcularSaldoTotalN();
  }

  crearFilasDefaultN() {
    if (this.listRubrosI.length == 0 && this.listRubrosE.length == 0) {
      this.obtenerListadoRubros();
    }

    if (this.ingresoN.length == 0) {
      this.listRubrosI.forEach((item) => {
        if (item.value == "Otros ingresos") {
          this.otros = true;
        } else {
          this.otros = false;
        }
        this.rubro = item.value;
        this.agregarIngreso();
      });
    }

    if (this.egresoN.length == 0) {
      this.listRubrosE.forEach((item) => {
        if (item.value == "Otros gastos") {
          this.otros = true;
        } else {
          this.otros = false;
        }
        this.rubro = item.value;
        this.agregarEgreso();
      });
    }

    this.rubro = "";
    this.otros = false;
  }

  calcularTotalIngresoN(edit: boolean) {
    this.totalIngresoN = [];
    let obj: any = {};
    let arr: any[] = [];
    this.ingresoN.forEach((item) => {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          let monto1: number;
          let monto2: number;
          if (obj["total" + i + "-" + j] === undefined) {
            monto1 = 0;
          } else {
            monto1 = Number(obj["total" + i + "-" + j]);
          }
          if (item["monto" + i + "-" + j] === undefined) {
            monto2 = 0;
          } else {
            monto2 = Number(item["monto" + i + "-" + j]);
          }
          obj["total" + i + "-" + j] = (
            parseFloat(monto1.toFixed(2)) + parseFloat(monto2.toFixed(2))
          ).toFixed(2);
        }
      }
    });
    arr.push(obj);
    arr.forEach((a) => {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          this.totalIngresoN.push({
            total: a["total" + i + "-" + j],
          });
        }
      }
    });

    if (edit) this.calcularSaldoTotalN();
  }

  calcularTotalEgresoN(edit: boolean) {
    this.totalEgresoN = [];
    let obj: any = {};
    let arr: any[] = [];
    this.egresoN.forEach((item) => {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          let monto1: number;
          let monto2: number;
          if (obj["total" + i + "-" + j] === undefined) {
            monto1 = 0;
          } else {
            monto1 = Number(obj["total" + i + "-" + j]);
          }
          if (item["monto" + i + "-" + j] === undefined) {
            monto2 = 0;
          } else {
            monto2 = Number(item["monto" + i + "-" + j]);
          }
          obj["total" + i + "-" + j] = (
            parseFloat(monto1.toFixed(2)) + parseFloat(monto2.toFixed(2))
          ).toFixed(2);
        }
      }
    });
    arr.push(obj);
    arr.forEach((a) => {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          this.totalEgresoN.push({
            total: a["total" + i + "-" + j],
          });
        }
      }
    });

    if (edit) this.calcularSaldoTotalN();
  }

  calcularSaldoTotalN() {
    this.saldoTotalN = [];
    let obj: any = {};

    if (this.totalIngresoN.length > 0 && this.totalEgresoN.length > 0) {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          let pos = (i - 1) * 12 + (j - 1);
          let op1 = this.totalIngresoN[pos]["total"]
            ? this.totalIngresoN[pos]["total"]
            : 0;
          let op2 = this.totalEgresoN[pos]["total"]
            ? this.totalEgresoN[pos]["total"]
            : 0;
          let saldo = op1 - op2;
          saldo = parseFloat(saldo.toFixed(2));

          obj["monto" + i + "-" + j] = saldo;
        }
      }
      this.saldoTotalN.push(obj);
    }

    if (this.totalIngresoN.length > 0 && this.totalEgresoN.length == 0) {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          let pos = (i - 1) * 12 + (j - 1);
          let op1 = this.totalIngresoN[pos]["total"]
            ? this.totalIngresoN[pos]["total"]
            : 0;
          let op2 = 0;
          let saldo = op1 - op2;
          saldo = parseFloat(saldo.toFixed(2));

          obj["monto" + i + "-" + j] = saldo;
        }
      }
      this.saldoTotalN.push(obj);
    }

    if (this.totalIngresoN.length == 0 && this.totalEgresoN.length > 0) {
      for (let i = 1; i <= this.vigencia; i++) {
        for (let j = 1; j <= 12; j++) {
          let pos = (i - 1) * 12 + (j - 1);
          let op1 = 0;
          let op2 = this.totalEgresoN[pos]["total"]
            ? this.totalEgresoN[pos]["total"]
            : 0;
          let saldo = op1 - op2;
          saldo = parseFloat(saldo.toFixed(2));

          obj["monto" + i + "-" + j] = saldo;
        }
      }

      this.saldoTotalN.push(obj);
    }
  }

  openModalAgregar(tipo: number) {
    this.tipoRubro = tipo;
    this.displayBasic = true;
  }

  rubrosChange(event: any, tipo: number) {
    if (tipo == 1) {
      this.listRubrosI.forEach((rubro) => {
        if (rubro.value == event.value) {
          this.rubro = rubro.value;
        }
      });
    } else if (tipo == 2) {
      this.listRubrosE.forEach((rubro) => {
        if (rubro.value == event.value) {
          this.rubro = rubro.value;
        }
      });
    }

    if (this.rubro === "Otros ingresos" || this.rubro === "Otros gastos") {
      this.otros = true;
    } else {
      this.otros = false;
    }
  }

  agregarRubro(tipo: number) {
    if (this.validarRubro(tipo)) {
      if (tipo == 1) {
        this.agregarIngreso();
      } else if (tipo == 2) {
        this.agregarEgreso();
      }
      this.rubro = "";
      this.otros = false;
      this.displayBasic = false;
    }
  }

  validarRubro(tipo: number): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.rubro == null || this.rubro == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un rubro.\n";
    }

    if (tipo == 1 && this.rubro != "Otros ingresos") {
      if (this.ingresoN.some((item) => item.rubro == this.rubro)) {
        validar = false;
        mensaje = mensaje +=
          "(*) El Rubro seleccionado ya existe en la tabla de Ingresos.\n";
      }
    } else if (tipo == 2 && this.rubro != "Otros gastos") {
      if (this.egresoN.some((item) => item.rubro == this.rubro)) {
        validar = false;
        mensaje = mensaje +=
          "(*) El Rubro seleccionado ya existe en la tabla de Egresos.\n";
      }
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  cerrarModal() {
    this.rubro = "";
    this.otros = false;
    this.displayBasic = false;
  }

  agregarIngreso() {
    let obj: any = {};
    obj = {
      codigoRentabilidad: this.codigoProceso,
      edit: true,
      estado: "A",
      idPlanManejo: this.idPlanManejo,
      idRentManejoForestal: 0,
      idRentManejoForestalDet: 0,
      idTipoRubro: 1,
      idUsuarioRegistro: this.user.idUsuario,
      rubro: this.rubro,
      descripcion: this.otros ? this.rubro : "",
      otros: this.otros,
    };
    for (let i = 1; i <= this.vigencia; i++) {
      for (let j = 1; j <= 12; j++) {
        obj["idRentManejoForestalDet" + i + "-" + j] = 0;
        obj["monto" + i + "-" + j] = "";
      }
    }
    this.ingresoN.push(obj);
    this.calcularTotalIngresoN(false);
    this.rubro = "";
    this.otros = false;
  }

  agregarEgreso() {
    let obj: any = {};
    obj = {
      codigoRentabilidad: this.codigoProceso,
      edit: true,
      estado: "A",
      idPlanManejo: this.idPlanManejo,
      idRentManejoForestal: 0,
      idRentManejoForestalDet: 0,
      idTipoRubro: 2,
      idUsuarioRegistro: this.user.idUsuario,
      rubro: this.rubro,
      descripcion: this.otros ? this.rubro : "",
      otros: this.otros,
    };
    for (let i = 1; i <= this.vigencia; i++) {
      for (let j = 1; j <= 12; j++) {
        obj["idRentManejoForestalDet" + i + "-" + j] = 0;
        obj["monto" + i + "-" + j] = "";
      }
    }
    this.egresoN.push(obj);
    this.calcularTotalEgresoN(false);
    this.rubro = "";
    this.otros = false;
  }

  registrarManejoRentabilidadN() {
    this.isSaving = true;

    if (this.validarIngresoEgresoN()) {
      this.listRentabilidad = [];

      this.ingresoN.forEach((item) => {
        var obj = new RentabilidadPGMFA();
        obj.codigoRentabilidad = item.codigoRentabilidad;
        obj.estado = item.estado;
        obj.idPlanManejo = item.idPlanManejo;
        obj.idRentManejoForestal = item.idRentManejoForestal;
        obj.idTipoRubro = item.idTipoRubro;
        obj.idUsuarioRegistro = item.idUsuarioRegistro;
        obj.rubro = item.rubro;
        obj.descripcion = item.descripcion;
        obj.listRentabilidadManejoForestalDetalle = [];
        for (let i = 1; i <= this.vigencia; i++) {
          for (let j = 1; j <= 12; j++) {
            if (
              item["monto" + i + "-" + j] === undefined ||
              item["monto" + i + "-" + j] === null ||
              item["monto" + i + "-" + j] === ""
            ) {
            } else {
              var objDet = new ListRentabilidadManejoForestalDetalle();
              objDet.anio = i;
              objDet.estado = item.estado;
              objDet.mes = j;
              objDet.monto = item["monto" + i + "-" + j];
              if (item["idRentManejoForestalDet" + i + "-" + j] === undefined) {
                objDet.idRentManejoForestalDet = 0;
              } else {
                objDet.idRentManejoForestalDet =
                  item["idRentManejoForestalDet" + i + "-" + j];
              }
              objDet.idUsuarioRegistro = item.idUsuarioRegistro;
              obj.listRentabilidadManejoForestalDetalle.push(objDet);
            }
          }
        }
        this.listRentabilidad.push(obj);
      });

      this.egresoN.forEach((item) => {
        var obj = new RentabilidadPGMFA();
        obj.codigoRentabilidad = item.codigoRentabilidad;
        obj.estado = item.estado;
        obj.idPlanManejo = item.idPlanManejo;
        obj.idRentManejoForestal = item.idRentManejoForestal;
        obj.idTipoRubro = item.idTipoRubro;
        obj.idUsuarioRegistro = item.idUsuarioRegistro;
        obj.rubro = item.rubro;
        obj.descripcion = item.descripcion;
        obj.listRentabilidadManejoForestalDetalle = [];
        for (let i = 1; i <= this.vigencia; i++) {
          for (let j = 1; j <= 12; j++) {
            if (
              item["monto" + i + "-" + j] === undefined ||
              item["monto" + i + "-" + j] === null ||
              item["monto" + i + "-" + j] === ""
            ) {
            } else {
              var objDet = new ListRentabilidadManejoForestalDetalle();
              objDet.anio = i;
              objDet.estado = item.estado;
              objDet.mes = j;
              objDet.monto = item["monto" + i + "-" + j];
              if (item["idRentManejoForestalDet" + i + "-" + j] === undefined) {
                objDet.idRentManejoForestalDet = 0;
              } else {
                objDet.idRentManejoForestalDet =
                  item["idRentManejoForestalDet" + i + "-" + j];
              }
              objDet.idUsuarioRegistro = item.idUsuarioRegistro;
              obj.listRentabilidadManejoForestalDetalle.push(objDet);
            }
          }
        }
        this.listRentabilidad.push(obj);
      });

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.rentabilidadManejoForestalService
        .registrarRentabilidadManejoForestal(this.listRentabilidad)
        .subscribe(
          (res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listarRentabilidadN();
              this.dialog.closeAll();
            } else {
              this.toast.error(res?.message);
              this.dialog.closeAll();
            }

            this.isSaving = false;
          },
          (error: HttpErrorResponse) => {
            this.dialog.closeAll();
            this.isSaving = false;
          }
        );

      this.rubro = "";
      this.otros = false;
    } else {
      this.isSaving = false;
    }
  }

  validarIngresoEgresoN(): boolean {
    let validar: boolean = true;
    let validarI: boolean = true;
    let validarE: boolean = true;
    let mensaje: string = "";

    this.ingresoN.forEach((item) => {
      if (item.rubro === "") {
        validarI = false;
      }
    });

    this.egresoN.forEach((item) => {
      if (item.rubro === "") {
        validarE = false;
      }
    });

    if (!validarI) mensaje += "(*) Debe ingresar: Rubro - Ingresos\n";
    if (!validarE) mensaje += "(*) Debe ingresar: Rubro - Egresos\n";

    if (!validarI || !validarE) {
      this.ErrorMensaje(mensaje);
      validar = false;
    }

    return validar;
  }

  eliminarRentabilidadN(event: Event, index: number, rentabilidad: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (rentabilidad.idRentManejoForestal > 0) {
          var params = {
            idUsuarioElimina: this.user.idUsuario,
            idRentManejoForestal: rentabilidad.idRentManejoForestal,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService
            .eliminarRentabilidadManejoForestal(params)
            .subscribe(
              (data: any) => {
                this.SuccessMensaje(data.message);
                if (rentabilidad.idTipoRubro == 1) {
                  this.ingresoN.splice(index, 1);
                } else {
                  this.egresoN.splice(index, 1);
                }
                // this.listarRentabilidadN();
                this.calcularTotalIngresoN(false);
                this.calcularTotalEgresoN(false);
                this.calcularSaldoTotalN();
                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          if (rentabilidad.idTipoRubro == 1) {
            this.ingresoN.splice(index, 1);
          } else {
            this.egresoN.splice(index, 1);
          }
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionIngreso = Object.assign(
                this.evaluacionIngreso,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon12_1
                )
              );
              this.evaluacionEgreso = Object.assign(
                this.evaluacionEgreso,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon12_2
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([this.evaluacionIngreso, this.evaluacionEgreso])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionIngreso);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionEgreso);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  cargarFormato(files: any) {
    if (this.vigencia > 0) {
      if (files.length > 0) {
        files.forEach((t: any) => {
          let item = {
            nombreHoja: "Hoja1",
            numeroFila: 3,
            numeroColumna: 1,
            codigoRentabilidad: this.codigoProceso,
            idPlanManejo: this.idPlanManejo,
            idUsuarioRegistro: this.user.idUsuario,
            vigencia: this.vigencia,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.rentabilidadManejoForestalService
            .registrarRentabilidadManejoForestalExcel(
              t.file,
              item.nombreHoja,
              item.numeroFila,
              item.numeroColumna,
              item.codigoRentabilidad,
              item.idPlanManejo,
              item.idUsuarioRegistro,
              item.vigencia
            )
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe(
              (res: any) => {
                if (res.success == true) {
                  this.toast.ok(res?.message);
                  this.listarRentabilidadN();
                } else {
                  this.toast.error(res?.message);
                }
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        });
      } else {
        this.toast.warn("Debe seleccionar archivo.");
      }
    } else {
      this.toast.warn("Se debe registrar duración de PO.");
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
