import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageService } from "primeng/api";
import { MatDialog } from "@angular/material/dialog";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { InformacionGeneralPGMFA } from "src/app/model/ResumenEjecutivoPGMFA";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { LineamientoInnerModel } from "../../../../../model/Comun/LineamientoInnerModel";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { ToastService } from "@shared";
import { finalize } from "rxjs/operators";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-aspectos-complementarios",
  templateUrl: "./tab-aspectos-complementarios.component.html",
  styleUrls: ["./tab-aspectos-complementarios.component.scss"],
})
export class TabAspectosComplementariosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  @Input("isDisabled") isDisabled: boolean = false;

  accept: string = 'application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

  informacionGeneral: InformacionGeneralPGMFA = new InformacionGeneralPGMFA();

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_14;

  codigoAcordeon14_1: string = CodigosTabEvaluacion.POAC_TAB_14_1;
  evaluacion14_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon14_1,
  });
  evaluacion: any;

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.obtenerResumenEjecutivo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }
  obtenerResumenEjecutivo() {
    var params = {
      codigoProceso: "POAC",
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.isDisabled = true;
          response.data.forEach((element: any) => {
            this.informacionGeneral = new InformacionGeneralPGMFA(element);
          });
        } else {
          this.isDisabled = false;
        }
      });
  }

  ActualizarAspectosComplementarios() {

    if(this.informacionGeneral.detalle == undefined || this.informacionGeneral.detalle ==''){
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: "Debe ingresar cualquier otro aspecto importante.",
      });

      return;
    }


    var params = this.informacionGeneral;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          this.obtenerResumenEjecutivo();
          this.toast.ok("Se registró los aspectos complementarios correctamente.");
        },
        (error: any) => {
          this.toast.warn(error.message);
        }
      );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion14_1 = Object.assign(
                this.evaluacion14_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon14_1
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion14_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion14_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
