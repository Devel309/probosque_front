import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { UsuarioService } from "@services";
import { ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ActividadSilvicultural } from "src/app/model/ActividadesSilviculturalesModel";
import { OrganizacionActividadModel } from "src/app/model/OrganizacionActividad";
import { PlanManejoModel } from "src/app/model/PlanManejo";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { FileModel } from "src/app/model/util/File";
import { OrganizacionActividadService } from "src/app/service/organizacionActividad.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { ActividadesSilviculturalesService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/actividadesSilviculturales.service";
import { LineamientoInnerModel } from "../../../../../model/Comun/LineamientoInnerModel";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-organizacion-desarrollo-actividad",
  templateUrl: "./tab-organizacion-desarrollo-actividad.component.html",
  styleUrls: ["./tab-organizacion-desarrollo-actividad.component.scss"],
})
export class TabOrganizacionDesarrolloActividadComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @ViewChild("fileInput", { static: true }) private fileInput!: ElementRef;

  tituloModal: string = "Registrar Organización de Desarrollo de Actividad";
  displayBasic: boolean = false;

  organizacion = {} as OrganizacionActividadModel;
  organizacionesList = [] as OrganizacionActividadModel[];

  edit: boolean = false;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  nombrefile = "";
  public filFile: any = null;

  usuario = {} as UsuarioModel;
  planManejo = {} as PlanManejoModel;

  lblArchivo: any;

  listDetalle: ActividadSilvicultural[] = [];

  idActSilvicultural: number = 0;

  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }
  archivoNotificacion: any = {};
  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_11;

  codigoAcordeon11_1: string = CodigosTabEvaluacion.POAC_TAB_11_1;

  evaluacion11_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon11_1,
  });

  evaluacion: any;

  indexActividad: number = 0;
  idArchivoOrganigrama: number = 0;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private organizacionActividadService: OrganizacionActividadService,
    private dialog: MatDialog,
    private actividadesSilviculturalesService: ActividadesSilviculturalesService,
    private user: UsuarioService,
    private anexosService: AnexosService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";
    this.listarOrganizacionDesarrollo();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarOrganizacionDesarrollo() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      idTipo: "POAC",
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .subscribe((response: any) => {
        this.dialog.closeAll();
        if (response.data.detalle.length != 0) {
          this.idActSilvicultural = response.dataidActividadSilvicultural;
          this.organizacionesList = response.data.detalle;
        }
      });
  }

  agregarOrganizacion() {
    if (!this.validarOrganizacion()) {
      return;
    }
    if (this.edit) {
      this.editarOrganizacion();
    } else {
      this.registrarOrganizacion();
    }
  }

  validarOrganizacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (
      this.organizacion.actividad == null ||
      this.organizacion.actividad == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Actividad Realizada.\n";
    }
    if (
      this.organizacion.descripcionDetalle == null ||
      this.organizacion.descripcionDetalle == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Forma de Organización.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }
  findIndexById(id: number): number {
    let index = -1;
    for (let i = 0; i < this.organizacionesList.length; i++) {
      if (this.organizacionesList[i].idActividadSilviculturalDet === id) {
        index = i;
        break;
      }
    }
    return index;
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  cerrarModal() {
    this.organizacion = {} as OrganizacionActividadModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = "Registrar Organización";
  }

  abrirModal() {
    this.displayBasic = true;
  }

  registrarOrganizacion() {
    this.organizacion.idActividadSilviculturalDet = 0;
    this.organizacionesList.push(this.organizacion);
    this.organizacion = {} as OrganizacionActividadModel;
    this.displayBasic = false;
  }

  editarOrganizacion() {
    this.displayBasic = false;
    this.tituloModal = "Registrar Organización";
    this.edit = false;
    this.organizacionesList[this.indexActividad] = this.organizacion;
    this.organizacion = {} as OrganizacionActividadModel;
    this.indexActividad = 0;
  }

  openEditarOrganizacion(
    organizacion: OrganizacionActividadModel,
    index: number
  ) {
    this.organizacion = { ...organizacion };
    this.displayBasic = true;
    this.edit = true;
    this.tituloModal = "Editar Organización";
    this.indexActividad = index;
  }

  openEliminarOrganizacion(
    event: Event,
    index: number,
    organizacion: OrganizacionActividadModel
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (organizacion.idActividadSilviculturalDet != 0) {
          var params = [
            {
              estado: "I",
              idActividadSilviculturalDet:
                organizacion.idActividadSilviculturalDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.actividadesSilviculturalesService
            .eliminarActividadSilviculturalPFDM(params)
            .subscribe(
              (data: any) => {
                this.SuccessMensaje(
                  "Se eliminó la organización para el desarrollo de la actividad correctamente."
                );
                this.organizacionesList.splice(index, 1);
                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.organizacionesList.splice(index, 1);
          this.dialog.closeAll();
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  validarOrganizacionOrganizacionDesarrollo = (): boolean => {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.organizacionesList.length === 0) {
      validar = false;
      mensaje = mensaje += "(*) Debe de agregar al menos 1 registro";
    }

    /*  if (this.nombrefile === '') {
      validar = false;
      mensaje = mensaje += '(*) Debe de adjuntar: Organigrama';
    } */

    if (mensaje != "") this.ErrorMensaje(mensaje);

    return validar;
  };

  registrarOrganizacionesDesarrollo() {
    this.listDetalle = [];
    if (!this.validarOrganizacionOrganizacionDesarrollo()) return;

    this.organizacionesList.forEach((item) => {
      let organizacionListMap = new ActividadSilvicultural(item);
      organizacionListMap.idTipo = "POAC";
      organizacionListMap.idUsuarioRegistro = this.idPlanManejo;
      organizacionListMap.idUsuarioRegistro = this.user.idUsuario;
      this.listDetalle.push(organizacionListMap);
    });

    var params = {
      aprovechamiento: null,
      divisionAdministrativa: null,
      laboresSilviculturales: {
        idActSilvicultural: this.idActSilvicultural
          ? this.idActSilvicultural
          : 0,
        codigoTipoActSilvicultural: "POAC",
        actividad: null,
        descripcion: null,
        observacion: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listActividadSilvicultural: this.listDetalle,
      },
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.actividadesSilviculturalesService
      .guardarSistemaManejoPFDM(params)
      .subscribe(
        (result: any) => {
          if (result.success) {
            this.dialog.closeAll();
            this.SuccessMensaje(
              "Se registró la organización para el desarrollo de la actividad correctamente."
            );
            this.listarOrganizacionDesarrollo();
          } else this.ErrorMensaje(result.message);
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  }

  registrarOrganizacionActividadArchivo = () => {
    var param = new HttpParams()
      .set("idPlanManejo", this.planManejo.idPlanManejo.toString())
      .set("idUsuarioRegistro", this.usuario.idusuario.toString());

    const formData = new FormData();
    formData.append("file", this.filFile);

    this.organizacionActividadService
      .registrarOrganizacionActividadArchivo(param, formData)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            this.SuccessMensaje(result.message);
            this.listarOrganizacionDesarrollo();
          } else this.ErrorMensaje(result.message);
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  };

  listarOrganizacionActividadArchivo = () => {
    let params = {
      idPlanManejo: this.planManejo.idPlanManejo,
    };

    this.organizacionActividadService
      .listarOrganizacionActividadArchivo(params)
      .subscribe(
        (result: any) => {
          
          this.nombrefile = result.data.nombreArchivo;
          this.dialog.closeAll();
        },
        (error: HttpErrorResponse) => {
          this.dialog.closeAll();
        }
      );
  };
  cargarIdArchivo(idArchivo: any) {
    
    this.idArchivoOrganigrama = idArchivo;
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion11_1 = Object.assign(
                this.evaluacion11_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon11_1
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion11_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion11_1);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
