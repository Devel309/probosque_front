import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { ParticipacionComunalService } from 'src/app/service/planificacion/plan-general-manejo-pgmfa/participacion-comunal.service';
import { ToastService } from '@shared';
import { UsuarioService } from '@services';
import { LineamientoInnerModel } from '../../../../../model/Comun/LineamientoInnerModel';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { finalize } from 'rxjs/operators';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tab-participacion-comunal',
  templateUrl: './tab-participacion-comunal.component.html',
  styleUrls: ['./tab-participacion-comunal.component.scss'],
})
export class TabParticipacionComunalComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  tituloModalMantenimiento: string = ' ';
  edit: boolean = false;

  tipoAccion: string = '';
  participacion: ModelParticipacionComunal = new ModelParticipacionComunal();
  verModalMantenimiento: boolean = false;
  listParticipacion: ModelParticipacionComunal[] = [];
  pendiente: boolean = false;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_9;

  codigoAcordeon9_91: string = CodigosTabEvaluacion.POAC_TAB_9_91;

  evaluacion9_91: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon9_91,
  });

  evaluacion: any;

  constructor(
    private messageService: MessageService,
    private dialog: MatDialog,
    private participacionComunalService: ParticipacionComunalService,
    private toast: ToastService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listarParticipacionComunal();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarParticipacionComunal() {
    this.listParticipacion = [];
    var params = {
      codTipoPartComunal: 'POAC',
      codTipoPartComunalDet: null,
      idPartComunal: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.participacionComunalService
      .listarParticipacionComunalCaberaDetalle(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((item: any) => {
            this.participacion = new ModelParticipacionComunal(item);
            this.listParticipacion.push(this.participacion);
          });
        }
      });
  }

  guardarParticipacionComunal() {
    var params = this.listParticipacion;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.participacionComunalService
      .registrarParticipacionComunalCaberaDetalle(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se registró la participación comunal correctamente.');
          this.pendiente = false;
          this.listarParticipacionComunal();
        }
        this.dialog.closeAll();
      });
  }

  abrirModal(tipo: string, rowIndex: number, data?: ModelParticipacionComunal) {
    this.tipoAccion = tipo;
    if (this.tipoAccion == 'C') {
      this.tituloModalMantenimiento = 'Registrar Participación Comunal';
      this.participacion = new ModelParticipacionComunal();
      this.participacion.codTipoPartComunal = 'POAC'; // cambiar por el codigo correspondiente
      this.participacion.idUsuarioRegistro = this.user.idUsuario;
      this.participacion.idPlanManejo = this.idPlanManejo;
      this.edit = false;
    } else {
      this.tituloModalMantenimiento = 'Editar Participación Comunal';
      this.edit = true;
      this.participacion = new ModelParticipacionComunal(data);
      this.participacion.id = rowIndex;
    }
    this.verModalMantenimiento = true;
  }

  agregarParticipacion(context?: any) {
    if (!this.validarParticipacion()) {
      return;
    }
    if (this.tipoAccion == 'C') {
      this.listParticipacion.push(this.participacion);
      this.pendiente = true;
    }
    if (this.tipoAccion == 'E') {
      this.participacion = new ModelParticipacionComunal(context);
      this.listParticipacion[context.id] = context;
      this.pendiente = true;
    }
    this.verModalMantenimiento = false;
  }

  openEliminarParticipacion(
    event: Event,
    index: number,
    monitoreo: ModelParticipacionComunal
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (monitoreo.idPartComunal != 0) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          var params = {
            idPartComunal: monitoreo.idPartComunal,
            idUsuarioElimina: this.user.idUsuario,
          };

          this.participacionComunalService
            .eliminarParticipacionComunalCaberaDetalle(params)
            .subscribe(
              (result: any) => {
                if (result.success == true) {
                  this.toast.ok('Se eliminó la participación comunal correctamente.');
                  this.listarParticipacionComunal();
                  this.pendiente = false;
                  this.dialog.closeAll();
                }
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.listParticipacion.splice(index, 1);
        }
      },
    });
  }

  validarParticipacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.participacion.actividad == null ||
      this.participacion.actividad == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Actividad.\n';
    }
    if (
      this.participacion.responsable == null ||
      this.participacion.responsable == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Responsable.\n';
    }
    if (
      this.participacion.seguimientoActividad == null ||
      this.participacion.seguimientoActividad == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Seguimiento.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion9_91 = Object.assign(
                this.evaluacion9_91,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon9_91
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion9_91])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion9_91);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}

export class ModelParticipacionComunal {
  constructor(data?: any) {
    if (data) {
      this.actividad = data.actividad ? data.actividad : '';
      this.codTipoPartComunal = data.codTipoPartComunal
        ? data.codTipoPartComunal
        : '';
      this.fecha = data.fecha ? data.fecha : '';
      this.idPartComunal = data.idPartComunal ? data.idPartComunal : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.lugar = data.lugar ? data.lugar : '';
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;

      this.responsable = data.responsable ? data.responsable : '';
      this.seguimientoActividad = data.seguimientoActividad
        ? data.seguimientoActividad
        : '';

      this.lstDetalle =
        data.lstDetalle.length > 0
          ? data.lstDetalle
          : [new ModelParticipacionComunalDetalle()];

      return;
    }
  }
  id: any;
  actividad: string = '';
  codTipoPartComunal: string = '';
  fecha: string = '';
  idPartComunal: number = 0;
  idUsuarioRegistro: number = 0;
  lugar: string = '';
  idPlanManejo: number = 0;
  responsable: string = '';
  seguimientoActividad: string = '';
  lstDetalle: ModelParticipacionComunalDetalle[] = [
    new ModelParticipacionComunalDetalle(),
  ];
}

export class ModelParticipacionComunalDetalle {
  constructor(data?: any) {
    if (data) {
      this.actividad = data.actividad ? data.actividad : '';
      this.codPartComunalDet = data.codPartComunalDet
        ? data.codPartComunalDet
        : '';
      this.codigoPartComunal = data.codigoPartComunal
        ? data.codigoPartComunal
        : '';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.detalle = data.detalle ? data.detalle : '';
      this.fecha = data.fecha ? data.fecha : '';
      this.idPartComunalDet = data.idPartComunalDet ? data.idPartComunalDet : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.lugar = data.lugar ? data.lugar : '';
      this.mecanismos = data.mecanismos ? data.mecanismos : '';
      this.metodologia = data.metodologia ? data.metodologia : '';
      this.observacion = data.observacion ? data.observacion : '';
      this.participacion = data.participacion ? data.participacion : '';

      return;
    }
  }

  actividad: string = '';
  codPartComunalDet: string = '';
  codigoPartComunal: string = '';
  descripcion: string = '';
  detalle: string = '';
  fecha: string = '';
  idPartComunalDet: number = 0;
  idUsuarioRegistro: number = 0;
  lugar: string = '';
  mecanismos: string = '';
  metodologia: string = '';
  observacion: string = '';
  participacion: string = '';
}
