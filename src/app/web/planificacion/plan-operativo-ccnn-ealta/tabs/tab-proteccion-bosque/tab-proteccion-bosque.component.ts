import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProteccionBosqueService } from 'src/app/service/proteccionBosque.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { PlanManejoModel } from '../../../../../model/PlanManejo';
import {
  ProteccionBosquePGMFAModel,
  ProteccionBosquePGMFAModel2
} from '../../../../../model/ProteccionBosqueDemarcacion';
import { ParametroModel } from '../../../../../model/Parametro';
import { ProteccionBosqueGestionAmbientalModel } from '../../../../../model/ProteccionBosqueGestionAmbiental';
import { GenericoService } from 'src/app/service/generico.service';
import { ProteccionBosqueImpactoAmbientalModel } from 'src/app/model/ProteccionBosqueImapctoAmbiental';
import {EvaluacionArchivoModel} from '../../../../../model/Comun/EvaluacionArchivoModel';
import {CodigosTabEvaluacion} from '../../../../../model/util/CodigosTabEvaluacion';
import {EvaluacionService} from '../../../../../service/evaluacion/evaluacion.service';
import {ToastService} from '@shared';
import {MatDialog} from '@angular/material/dialog';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import {ProteccionDelBosqueService} from '../../../../../service/planificacion/plan-general-manejo-pgmfa/proteccionBosque.service';
import {
  ProteccionBosqueCabeceraModel,
  //ProteccionBosqueDetalle
} from '../../../plan-general-manejo/tabs/tab-proteccion-bosque/tab-proteccion-bosque.component';
import {ParametroValorService, UsuarioService} from '@services';
import {HttpErrorResponse} from '@angular/common/http';
import {UrlFormatos} from '../../../../../model/urlFormatos';
import {CodigosPOAC} from '../../../../../model/util/POAC/CodigosPOAC';
import {ProteccionBosqueDetalle} from '../../../../../model/util/POAC/ProteccionBosqueDetalle';
import {ProteccionBosque} from '../../../../../model/util/POAC/ProteccionBosque';
import {ProteccionBosqueActividad} from '../../../../../model/util/POAC/ProteccionBosqueActividad';
import {isElementScrolledOutsideView} from '@angular/cdk/overlay/position/scroll-clip';
import {OrganizacionActividadModel} from '../../../../../model/OrganizacionActividad';
import {CodigoUtil} from '../../../../../model/util/CodigoUtil';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-proteccion-bosque',
  templateUrl: './tab-proteccion-bosque.component.html',
  styleUrls: ['./tab-proteccion-bosque.component.scss'],
  providers: [MessageService]
})
export class TabProteccionBosqueComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();


  //CABECERA 7.1
  longitudMarcar:number = 0;
  longitudMantener:number = 0;
  delimitacionLindero:string="";


  parametroModel = {} as ParametroModel;

  tituloModal: string = "Registrar Plan Gestion Ambiental";
  showModal_7_2_2: boolean = false;
  showModalImpacto: boolean = false;

  displayModalActividad: boolean = false;
  editarActividad: boolean = false;
  det_item_actividad : ProteccionBosqueActividad = new ProteccionBosqueActividad();
  indexUpdate : number = -1;

  cabPlanGestion  :ProteccionBosque = {} as ProteccionBosque;
  planGestionType1: ProteccionBosqueDetalle[] = [];
  planGestionType2: ProteccionBosqueDetalle[] = [];
  planGestionType3: ProteccionBosqueDetalle[] = [];
  det_item_7_2_2: ProteccionBosqueDetalle = new ProteccionBosqueDetalle();

  tipos: any[] = [];
  edit: boolean = false;
  factoresAmbientales: any[] = [];
  mediosAmbientales: any[] = [];
  actividades: any[] = [];

  cabLinderos:ProteccionBosque= {} as ProteccionBosque;
  lisLinderos: ProteccionBosqueDetalle[] = [];


  cabImpactosAmbientales:ProteccionBosque= {} as ProteccionBosque;
  listImpactosAmbientales: ProteccionBosqueDetalle[] = [];

  proteccionImpactoList: ProteccionBosqueImpactoAmbientalModel[] = [];
  editImpacto: boolean = false;


  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_7;

  codigoAcordeon7_71: string =CodigosTabEvaluacion.POAC_TAB_7_71;
  codigoAcordeon7_721: string =CodigosTabEvaluacion.POAC_TAB_7_721;
  codigoAcordeon7_722: string =CodigosTabEvaluacion.POAC_TAB_7_722;

  evaluacion7_71 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon7_71
  });

  evaluacion7_721 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon7_721
  });

  evaluacion7_722 : EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet : this.codigoProceso,
    codigoEvaluacionDetSub : this.codigoTab,
    codigoEvaluacionDetPost : this.codigoAcordeon7_722
  });

  evaluacion:any;
  idProBosque: number = 0;
  lisLinderosBase: any[] = [];

  det_item_7_2_1: ProteccionBosqueDetalle = new ProteccionBosqueDetalle();


  UrlFormatos = UrlFormatos;
  CodigosPOAC= CodigosPOAC;
  @Input() isDisbledObjFormu!: boolean;

  editar_7_2_2: boolean = false;

  actividadesPorDefecto : ProteccionBosqueActividad[] = [];

  CodigoUtil = CodigoUtil;
  masterSelected: boolean = false;

  constructor(
    private servProteccionBosque: ProteccionBosqueService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private genericoServices: GenericoService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private parametroValorService: ParametroValorService,
    private proteccionDelBosqueService: ProteccionDelBosqueService,
    private router: Router
    ) {
  }


  ngOnInit(): void {

    this.cabLinderos = new ProteccionBosque({
      idProBosque : 0,
      idPlanManejo : this.idPlanManejo,
      codPlanGeneral : this.codigoProceso,
      subCodPlanGeneral : this.codigoAcordeon7_71,
      idUsuarioRegistro : this.user.idUsuario,

      listProteccionBosque : [],
      listProteccionGestion : [],
      listProteccionAnalisis : []
    });

    this.cabImpactosAmbientales = new ProteccionBosque({
      idProBosque : 0,
      idPlanManejo : this.idPlanManejo,
      codPlanGeneral : this.codigoProceso,
      subCodPlanGeneral : this.codigoAcordeon7_721,
      idUsuarioRegistro : this.user.idUsuario,

      listProteccionBosque : [],
      listProteccionGestion : [],
      listProteccionAnalisis : []
    });

    this.cabPlanGestion = new ProteccionBosque({
      idProBosque : 0,
      idPlanManejo : this.idPlanManejo,
      codPlanGeneral : this.codigoProceso,
      subCodPlanGeneral : this.codigoAcordeon7_722,
      idUsuarioRegistro : this.user.idUsuario,

      listProteccionBosque : [],
      listProteccionGestion : [],
      listProteccionAnalisis : []
    });

    this.mediosAmbientales.push({codigo:"Abiótico", descripcion :"Abiótico"});
    this.mediosAmbientales.push({codigo:"Biótico", descripcion :"Biótico"});
    this.mediosAmbientales.push({codigo:"Social", descripcion :"Social"});

    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({descripcion:"Censo"}));
    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({descripcion:"Demarcación de linderos"}));
    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({descripcion:"Construcción de campamentos"}));
    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({descripcion:"Construccion de caminos"}));
    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({descripcion:"Tala"}));
    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({descripcion:"Arrastre"}));

    this.obtenerListadoGestionAmbiental();
    //this.obtenerFactores();
    this.listar_7_1();
    this.listar_7_2_1();
    this.listar_7_2_2();

    if(this.isPerfilArffs)
      this.obtenerEvaluacion();
  }

  listar_7_1() {
    this.lisLinderos = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codPlanGeneral: this.codigoProceso,
      subCodPlanGeneral: this.codigoAcordeon7_71,
    };

    this.proteccionDelBosqueService
      .listarProteccionDelBosque(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          if (response.data.length != 0) {
            let cab = response.data[0];

            response.data.forEach((element: any) => {
              if (element.subCodPlanGeneralDet == 'CAB') {
                this.longitudMarcar = element.implementacion?Number(element.implementacion):0;
                this.longitudMantener = element.factorAmbiental?Number(element.factorAmbiental):0;
                this.delimitacionLindero = element.descOtra;
              }else  if (element.subCodPlanGeneralDet == 'DET') {
                element.nuAccionb = element.nuAccion == 1;
                this.lisLinderos.push(new ProteccionBosqueDetalle(element));
              }else  if (element.subCodPlanGeneralDet == null) {
                element.subCodPlanGeneralDet = 'DET';
                element.nuAccionb = element.nuAccion == 1;
                element.idUsuarioRegistro = this.user.idUsuario;
                this.lisLinderos.push(new ProteccionBosqueDetalle(element));
              }
              this.isAllSelected();
            });
          }
        }
      });

  }

  cerrarModal() {
    this.det_item_7_2_1 = {} as ProteccionBosqueDetalle;
    this.showModal_7_2_2 = false;
    this.edit = false;
    this.tituloModal = "Registrar Organización";
  }

  listar_7_2_1() {
    this.listImpactosAmbientales = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codPlanGeneral: this.codigoProceso,
      subCodPlanGeneral: this.codigoAcordeon7_721,
    };

    this.proteccionDelBosqueService.listarProteccionDelBosque(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          if (response.data.length != 0) {
            let cab = response.data[0];
            let count : number = 0;
            this.cabImpactosAmbientales.idProBosque = cab.idProBosque;
            response.data.forEach((element: any) => {
              if (element.idProBosqueDet !== 0) {

                if(count == 0){
                  this.actividadesPorDefecto = [];
                  element.listProteccionBosqueActividad.forEach((e:ProteccionBosqueActividad)=>{
                    this.actividadesPorDefecto.push(new ProteccionBosqueActividad({
                      descripcion : e.descripcion
                    }))
                  });
                }
                element.actividades  = [];
                element.listProteccionBosqueActividad.forEach((e:ProteccionBosqueActividad)=>{
                  if(e.accion) element.actividades.push(e.descripcion);
                });

                this.listImpactosAmbientales.push(new ProteccionBosqueDetalle(element));

              } else if (element.idProBosqueDet === 0) {
                element.idUsuarioRegistro = this.user.idUsuario;
                element.nuAccionb = element.nuAccion == 1;
                element.descOtra = CodigoUtil.VALOR_DEFECTO;
                element.actividades  = [];
                this.actividadesPorDefecto.forEach((e:ProteccionBosqueActividad)=>{
                    element.listProteccionBosqueActividad.push(new ProteccionBosqueActividad({
                      idProBosqueActividad : 0,
                      codigo : null,
                      descripcion :e.descripcion,
                      accion : false
                    }));
                });
                this.listImpactosAmbientales.push(new ProteccionBosqueDetalle(element));
              }
              count = count + 1;
            });
          }
        }
      });
  }

  listar_7_2_2() {
    this.planGestionType1 = [];
    this.planGestionType2 = [];
    this.planGestionType3 = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      codPlanGeneral: this.codigoProceso,
      subCodPlanGeneral: this.codigoAcordeon7_722,
    };

    this.proteccionDelBosqueService.listarProteccionDelBosque(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          if (response.data.length != 0) {
            let cab = response.data[0];
            this.cabPlanGestion.idProBosque = cab.idProBosque;

            response.data.forEach((element: any) => {
              this.idProBosque = element.idProBosque;
               if (element.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_1) {
                this.planGestionType1.push(new ProteccionBosqueDetalle(element));
              } else if (element.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_2) {
                this.planGestionType2.push(new ProteccionBosqueDetalle(element));
              } else if (element.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_3) {
                this.planGestionType3.push(new ProteccionBosqueDetalle(element));
              }
            });
          }
        }
      });
  }


  obtenerListadoGestionAmbiental() {
    var params = { prefijo: 'TPRO' };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.tipos = data.data;
      });
  }


  addItem_7_1() {
    this.lisLinderos.push( new ProteccionBosqueDetalle({
      idProBosqueDet: 0,
      codPlanGeneralDet: this.codigoAcordeon7_71,
      subCodPlanGeneralDet: "DET",
      nuAccion: 1,
      nuAccionb :true,
      tipoMarcacion: "",
      implementacion:"",
      idUsuarioRegistro : this.user.idUsuario,
      listProteccionBosqueActividad : []
    }));
  }

  checkUncheckAll() {
    this.lisLinderos.map((item) => {
      item.nuAccionb = this.masterSelected;
    });
  }

  isAllSelected() {
    this.masterSelected = this.lisLinderos.every(function (item: any) {
      return item.nuAccionb == true;
    });
  }

  abrirModalPlanes() {
    this.showModal_7_2_2 = true;
    this.editar_7_2_2 = false;
    this.det_item_7_2_2 = new ProteccionBosqueDetalle();
  }

  abrirModalImpacto() {
    this.det_item_7_2_1 = new ProteccionBosqueDetalle();
    this.editImpacto = false;
    this.showModalImpacto = true;
  }

  abrirModalActividad() {
    this.det_item_actividad = new ProteccionBosqueActividad();
    this.editarActividad = false;
    this.displayModalActividad = true;
  }

  guardar_7_1() {
    if (!this.validarDemarcacionyMantenimientoLinderos()) {
      return;
    } else {
      let inputs_7_1 = new ProteccionBosqueDetalle({});
      inputs_7_1.codPlanGeneralDet = this.codigoAcordeon7_71;
      inputs_7_1.subCodPlanGeneralDet = 'CAB';
      inputs_7_1.implementacion = this.longitudMarcar?this.longitudMarcar.toString():"";
      inputs_7_1.factorAmbiental =this.longitudMantener?this.longitudMantener.toString():"";
      inputs_7_1.descOtra =this.delimitacionLindero;
      inputs_7_1.nuAccion = 0;
      inputs_7_1.listProteccionBosqueActividad = [];

      let arrayInput:ProteccionBosqueDetalle[] =[];

      this.lisLinderos.forEach((e:ProteccionBosqueDetalle)=>{
        if(e.nuAccionb){
          e.nuAccion = 1;
        }else{
          e.nuAccion = 0;
        }
      });

      arrayInput = [...this.lisLinderos];
      arrayInput.push(inputs_7_1);
      this.cabLinderos.listProteccionBosque = arrayInput;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.proteccionDelBosqueService.registrarProteccionBosque([this.cabLinderos])
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe(
          (response: any) => {
            this.listar_7_1();
            this.toast.ok(
              'Se registró la demarcación y mantenimiento de linderos correctamente.'
            );
          }
        );
    }
  }


  obtenerFactores() {
    var params = { prefijo: 'FACTAMB' };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.factoresAmbientales = data.data;
      });
  }

  obtenerActividades() {
    var params = { prefijo: 'ACTIMPAMB' };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((data: any) => {
        this.actividades = data.data;
      });
  }

  obtenerListadoImpactoHambientales() {
    let params = {
      planManejo: {
        idPlanManejo: this.idPlanManejo
      }
    }

    this.servProteccionBosque.listarPorFiltroProteccionBosqueImpactoAmbiental(params).subscribe((data: any) => {
      this.proteccionImpactoList = data.data;
    })
  }

  validarDemarcacionyMantenimientoLinderos(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    for (let item of this.lisLinderos) {
      if (item.tipoMarcacion == null || item.tipoMarcacion == '') {
        validar = false;
        mensaje = '(*) Debe especificar: Sistema de demarcación.';
        break;
      }
    }

    if (validar == false) {
      this.toast.warn(mensaje);
    }

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  onConfirm() {
    this.messageService.clear('c');
  }


  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }

  agregar_7_2_2() {
    //VALIDAR
    let flag: boolean = true;

    if (this.det_item_7_2_2.tipoMarcacion == undefined || this.det_item_7_2_2.tipoMarcacion == '') {
      this.toast.warn("(*) Debe seleccionar: Tipo de Programa.");
      flag = false;
    }

    if (this.det_item_7_2_2.actividad == undefined || this.det_item_7_2_2.actividad == '') {
      this.toast.warn("(*) Debe ingresar: Actividad.")
      flag = false;
    }

    if (!flag) return;

    if (this.existeDuplicados_7_2_2(this.det_item_7_2_2.actividad, this.det_item_7_2_2.tipoMarcacion)) {
      this.toast.warn("Ya existe la actividad " + this.det_item_7_2_2.actividad + " en la tabla " + this.det_item_7_2_2.tipoMarcacion);
      return;
    }

    //return;

    if (this.editar_7_2_2) {
      if (this.det_item_7_2_2.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_1) {
        const newObj = { ...this.det_item_7_2_2 };
        this.planGestionType1[ this.indexUpdate ] = newObj;
      }

      if (this.det_item_7_2_2.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_2) {
        const newObj = { ...this.det_item_7_2_2 };
        this.planGestionType2[ this.indexUpdate ] = newObj;
      }

      if (this.det_item_7_2_2.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_3) {
        const newObj = { ...this.det_item_7_2_2 };
        this.planGestionType3[ this.indexUpdate ] = newObj;
      }

      this.editar_7_2_2 = false;
      this.showModal_7_2_2 = false;
      this.det_item_7_2_2 = {} as ProteccionBosqueDetalle;
      this.indexUpdate = -1;
    } else {
      this.det_item_7_2_2.listProteccionBosqueActividad = [];
      this.det_item_7_2_2.codPlanGeneralDet = CodigosPOAC.ACORDEON_7_722;
      this.det_item_7_2_2.nuAccion = 0;
      this.det_item_7_2_2.nuAccionb = false;

      if (this.det_item_7_2_2.tipoMarcacion == "Preventivo-corrector") {
        this.det_item_7_2_2.subCodPlanGeneralDet = CodigosPOAC.ACORDEON_7_722_TABLA_1;
        const newObj = { ...this.det_item_7_2_2 };
        this.planGestionType1.push(newObj);
      }

      if (this.det_item_7_2_2.tipoMarcacion == "Vigilancia y seguimiento") {
        this.det_item_7_2_2.subCodPlanGeneralDet = CodigosPOAC.ACORDEON_7_722_TABLA_2;
        const newObj = { ...this.det_item_7_2_2 };
        this.planGestionType2.push(newObj);
      }

      if (this.det_item_7_2_2.tipoMarcacion == "Contingencia ambientales") {
        this.det_item_7_2_2.subCodPlanGeneralDet = CodigosPOAC.ACORDEON_7_722_TABLA_3;
        const newObj = { ...this.det_item_7_2_2 };
        this.planGestionType3.push(newObj);
      }

      this.det_item_7_2_2 = {} as ProteccionBosqueDetalle;
      this.showModal_7_2_2 = false;
    }
  }

  existeDuplicados_7_2_2(campo:any , tipoTabla : any) {
    let flag = false;
    if(tipoTabla == "Preventivo-corrector") {

      for (var _i = 0; _i < this.planGestionType1.length; _i++) {
        if(this.editar_7_2_2 && _i != this.indexUpdate){
          if(this.planGestionType1[_i].actividad?.toLowerCase() == campo.toLowerCase())
            flag = true;
        }else if(!this.editar_7_2_2){
          if(this.planGestionType1[_i].actividad?.toLowerCase() == campo.toLowerCase())
            flag = true;
        }
      }

    } else if(tipoTabla == "Vigilancia y seguimiento") {

      for (var _i = 0; _i < this.planGestionType2.length; _i++) {
        if(this.editar_7_2_2 && _i != this.indexUpdate){
          if(this.planGestionType2[_i].actividad?.toLowerCase() == campo.toLowerCase())
            flag = true;
        }else if(!this.editar_7_2_2){
          if(this.planGestionType2[_i].actividad?.toLowerCase() == campo.toLowerCase())
            flag = true;
        }
      }
    } else if(tipoTabla == "Contingencia ambientales") {

      for (var _i = 0; _i < this.planGestionType3.length; _i++) {
        if(this.editar_7_2_2 && _i != this.indexUpdate){
          if(this.planGestionType3[_i].actividad?.toLowerCase() == campo.toLowerCase())
            flag = true;
        }else if(!this.editar_7_2_2){
          if(this.planGestionType3[_i].actividad?.toLowerCase() == campo.toLowerCase())
            flag = true;
        }
      }
    }
    return flag;
  }

  existeDuplicados_7_2_1(medio:any,factorAmbiental:any) {
    let flag = false;
    for (var _i = 0; _i < this.listImpactosAmbientales.length; _i++) {
      if (this.editImpacto && _i != this.indexUpdate) {
        if (this.listImpactosAmbientales[_i].actividad?.toLowerCase()  == medio.toLowerCase() && this.listImpactosAmbientales[_i].tipoMarcacion?.toLowerCase() == factorAmbiental.toLowerCase())
          flag = true;
      } else if (!this.editImpacto) {
        if (this.listImpactosAmbientales[_i].actividad?.toLowerCase() == medio.toLowerCase() && this.listImpactosAmbientales[_i].tipoMarcacion?.toLowerCase() == factorAmbiental.toLowerCase())
          flag = true;
      }
    }

    return flag;
  }


  openEditarPlanGestion(data: ProteccionBosqueDetalle, index:number) {
    const newObj = { ...data };
    this.det_item_7_2_2 = newObj;
    this.showModal_7_2_2 = true;
    this.editar_7_2_2 = true;
    this.tituloModal = 'Editar Plan Gestión Ambiental';
    this.indexUpdate = index;
  }

  openEliminarPlanGestion(event: Event, index: number, planGestion: ProteccionBosqueDetalle) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (planGestion.idProBosqueDet) {
          var params = {
            idProBosque: 0,
            idProBosqueDet: planGestion.idProBosqueDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.proteccionDelBosqueService
            .eliminarProteccionBosque(params)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe(
              (data: any) => {
                this.toast.ok("Plan de Gestión Ambiental eliminado correctamente.");
                if(planGestion.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_1){
                  this.planGestionType1.splice(index, 1);
                }else if(planGestion.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_2){
                  this.planGestionType2.splice(index, 1);
                }else if(planGestion.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_3){
                  this.planGestionType3.splice(index, 1);
                }
              }
            );
        } else {
          if(planGestion.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_1){
            this.planGestionType1.splice(index, 1);
          }else if(planGestion.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_2){
            this.planGestionType2.splice(index, 1);
          }else if(planGestion.subCodPlanGeneralDet == CodigosPOAC.ACORDEON_7_722_TABLA_3){
            this.planGestionType3.splice(index, 1);
          }
          this.toast.ok("Plan de Gestión Ambiental eliminado correctamente.");
        }

      },
      reject: () => {

      },
    });
  }

  autocompletar() {
    if(this.planGestionType1.length==0){
      this.actividadesPorDefecto.forEach((e:ProteccionBosqueActividad)=>{
        let item: ProteccionBosqueDetalle = {} as ProteccionBosqueDetalle;
        item.listProteccionBosqueActividad = [];
        item.codPlanGeneralDet = CodigosPOAC.ACORDEON_7_722;
        item.subCodPlanGeneralDet = CodigosPOAC.ACORDEON_7_722_TABLA_1;
        item.nuAccion = 0;
        item.nuAccionb = false;
        item.actividad = e.descripcion;
        item.tipoMarcacion = "Preventivo-corrector"
        item.descOtra = CodigoUtil.VALOR_DEFECTO
        this.planGestionType1.push(item);
      });
      this.toast.ok("Las actividades fueron cargadas de la sección 7.2.1");
    }else{
      this.toast.warn("Ya existen actividades para el programa Preventivo Corrector");
    }

    if(this.planGestionType2.length==0){
      this.actividadesPorDefecto.forEach((e:ProteccionBosqueActividad)=>{
        let item2: ProteccionBosqueDetalle = {} as ProteccionBosqueDetalle;
        item2.listProteccionBosqueActividad = [];
        item2.codPlanGeneralDet = CodigosPOAC.ACORDEON_7_722;
        item2.subCodPlanGeneralDet = CodigosPOAC.ACORDEON_7_722_TABLA_2;
        item2.nuAccion = 0;
        item2.nuAccionb = false;
        item2.actividad = e.descripcion;
        item2.tipoMarcacion = "Vigilancia y seguimiento";
        item2.descOtra = CodigoUtil.VALOR_DEFECTO
        this.planGestionType2.push(item2);
      });
      this.toast.ok("Las actividades fueron cargadas de la sección 7.2.1");
    }else{
      this.toast.warn("Ya existen actividades para el programa vigilancia y seguimiento");
    }

  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }


  registrar_7_2_2() {
    let arrayInput: ProteccionBosqueDetalle[];
    arrayInput = [...this.planGestionType1,...this.planGestionType2,...this.planGestionType3];
    this.cabPlanGestion.listProteccionBosque = arrayInput;

    this.dialog.open(LoadingComponent, {disableClose: true});
    this.proteccionDelBosqueService.registrarProteccionBosque([this.cabPlanGestion])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          this.listar_7_2_2()
          this.toast.ok(
            'Se registró el Plan de Gestión Ambiental correctamente.'
          );
        }
      );

  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  onChangeAct(event: any) {
  }

  agregarImpacto() {
    //validar
    let flag: boolean = true;
    if (!this.det_item_7_2_1.actividad) {
      this.toast.warn("(*) Debe seleccionar: Medio.")
      flag = false;
    }

    if (!this.det_item_7_2_1.tipoMarcacion) {
      this.toast.warn("(*) Debe ingresar: Factor ambiental.")
      flag = false
    }

    if (!flag) {
      return;
    }

    if (this.existeDuplicados_7_2_1(this.det_item_7_2_1.actividad, this.det_item_7_2_1.tipoMarcacion)) {
      this.toast.warn("Existe duplicidad en el campo Medio y Factor ambiental ")
      return;
    }

    if (this.editImpacto) {
      this.editarImpacto();
    } else {
      this.registrarImpacto();
    }
  }

  cancelarImpactoModal() {
    this.indexUpdate = -1;
    this.editImpacto = false;
    this.showModalImpacto = false;
    this.det_item_7_2_1 = {} as ProteccionBosqueDetalle;
  }

  cancelarModal_7_2_2() {
    this.indexUpdate = -1;
    this.editImpacto = false;
    this.showModal_7_2_2 = false;
    this.det_item_7_2_2 = {} as ProteccionBosqueDetalle;
  }

  editarImpacto() {
    if( this.indexUpdate >= 0 ){
      this.det_item_7_2_1.actividades.forEach((e:string)=>{
        let index = this.det_item_7_2_1.listProteccionBosqueActividad.findIndex((act:ProteccionBosqueActividad) => act.descripcion == e);
        this.det_item_7_2_1.listProteccionBosqueActividad[index].accion =true;
      });
      const newObj = { ...this.det_item_7_2_1 };

      this.listImpactosAmbientales[this.indexUpdate] = newObj;
    }

    this.indexUpdate = -1;
    this.editImpacto = false;
    this.showModalImpacto = false;
    this.det_item_7_2_1 = {} as ProteccionBosqueDetalle;
  }

  registrarImpacto() {
    this.actividadesPorDefecto.forEach((e:ProteccionBosqueActividad)=>{
      this.det_item_7_2_1.listProteccionBosqueActividad.push(new ProteccionBosqueActividad({
        idProBosqueActividad  : 0,
        codigo                : null,
        descripcion           : "",
        accion                :  false}));
    });

    this.det_item_7_2_1.actividades.forEach((e:string)=>{
        let index = this.actividadesPorDefecto.findIndex((act:ProteccionBosqueActividad) => act.descripcion == e);
        this.det_item_7_2_1.listProteccionBosqueActividad[index].descripcion = e;
        this.det_item_7_2_1.listProteccionBosqueActividad[index].accion =true;
    });

    this.det_item_7_2_1.nuAccion = 0;
    this.det_item_7_2_1.nuAccionb = false;
    this.det_item_7_2_1.idUsuarioRegistro = this.user.idUsuario;

    const newObj = { ...this.det_item_7_2_1 };

    this.listImpactosAmbientales.push(
      newObj
    );
    this.showModalImpacto = false;
    this.det_item_7_2_1 = {} as ProteccionBosqueDetalle;
  }


  openEliminarImpacto(event: Event, index: number, impacto: ProteccionBosqueDetalle) {
    const params = {
      idProBosque: 0,
      idProBosqueDet: impacto.idProBosqueDet,
      idUsuarioElimina: this.user.idUsuario,
    };

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (impacto.idProBosqueDet) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.proteccionDelBosqueService
            .eliminarProteccionBosque(params)
            .subscribe(
              (result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                  this.listImpactosAmbientales.splice(index, 1);
                }
                this.dialog.closeAll();
              },
              (error: HttpErrorResponse) => {
                this.dialog.closeAll();
              }
            );
        } else {
          this.listImpactosAmbientales.splice(index, 1);
        }
      },
    });
  }


  openEditarImpacto(impacto: ProteccionBosqueDetalle, index:number) {
    this.indexUpdate = index;
    const newObj = { ...impacto };
    //this.det_item_7_2_1 = impacto;
    this.det_item_7_2_1 = newObj;
    this.editImpacto = true;
    this.showModalImpacto = true;
  }

  guardar_7_2_1() {
    let arrayInput:ProteccionBosqueDetalle[];
    this.listImpactosAmbientales.forEach((e:ProteccionBosqueDetalle)=>{
      if(e.nuAccionb){
        e.nuAccion = 1;
      }else{
        e.nuAccion = 0;
      }
    });

    arrayInput = [...this.listImpactosAmbientales];
    this.cabImpactosAmbientales.listProteccionBosque = arrayInput;


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.proteccionDelBosqueService.registrarProteccionBosque([this.cabImpactosAmbientales])
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          this.listar_7_2_1()
          this.toast.ok(
            'Se registró el Modelo de Matriz de identificación de Impactos Ambientales correctamente.'
          );
        })
  }




  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab
    }
    this.evaluacionService.obtenerEvaluacion(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion){
            this.evaluacion7_71 = Object.assign(this.evaluacion7_71,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon7_71));
            this.evaluacion7_721 = Object.assign(this.evaluacion7_721,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon7_721));
            this.evaluacion7_722 = Object.assign(this.evaluacion7_722,this.evaluacion.listarEvaluacionDetalle.find((x: any) => x.codigoEvaluacionDetPost == this.codigoAcordeon7_722));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validar([this.evaluacion7_71,this.evaluacion7_721,this.evaluacion7_722])){

      if(this.evaluacion) {
      this.evaluacion.listarEvaluacionDetalle = [];
      this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion7_71);
      this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion7_721);
      this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion7_722);
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.evaluacionService.registrarEvaluacionPlanManejo(this.evaluacion)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res:any) => {
          this.toast.ok(res.message);
          this.obtenerEvaluacion();
        })
    }

    }else{
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }


  agregarColumna(){

    if(this.det_item_actividad.descripcion == undefined || this.det_item_actividad.descripcion == ''){
      this.toast.warn("Ingrese una actividad");
      return;
    }

    if (this.editarActividad) {

    } else {
      this.actividadesPorDefecto.push(
        this.det_item_actividad
      );

      this.listImpactosAmbientales.forEach((e:ProteccionBosqueDetalle)=>{

        e.listProteccionBosqueActividad.push(new ProteccionBosqueActividad({
          idProBosqueActividad : 0,
          codigo                :  null,
          descripcion           : this.det_item_actividad.descripcion,
          accion                : 0
        }));
      });

      this.displayModalActividad = false;
      this.det_item_actividad = {} as ProteccionBosqueActividad;

    }
  }

  cargarFormato(files: any, codigoAcordeon: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Matriz",
          numeroFila: 6,
          numeroColumna: 1,
          codPlanGeneral: CodigosPOAC.CODIGO_PROCESO,
          subCodPlanGeneral :codigoAcordeon,
          codPlanGeneralDet : codigoAcordeon,
          subCodPlanGeneralDet : null,
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario
        };

        this.dialog.open(LoadingComponent, { disableClose: true });

        this.proteccionDelBosqueService.registrarProteccionBosqueExcel(
          t.file,
          item.nombreHoja,
          item.numeroFila,
          item.numeroColumna,
          item.codPlanGeneral,
          item.subCodPlanGeneral,
          item.codPlanGeneralDet,
          item.subCodPlanGeneralDet,
          item.idPlanManejo,
          item.idUsuarioRegistro
        ).pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
            if (res.success == true) {
              this.toast.ok(res?.message);
              this.listar_7_2_1();
            } else {
              this.toast.error(res?.message);
            }
          },
          (error: HttpErrorResponse) => {
            this.dialog.closeAll();
          });
      });
    } else {
      this.toast.warn('Debe seleccionar archivo.')
    }
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
