import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { Anexo2CensoComercialModelPOAC } from "src/app/model/AnexosPOACModel";
import { EvaluacionArchivoModel } from "src/app/model/Comun/EvaluacionArchivoModel";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { CodigosTabEvaluacion } from "src/app/model/util/CodigosTabEvaluacion";
import { AnexosCargaExcelService } from "src/app/service/anexos";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { EvaluacionUtils } from "src/app/model/util/EvaluacionUtils";
import { Mensajes } from "src/app/model/util/Mensajes";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-anexos",
  templateUrl: "./tab-anexos.component.html",
  styleUrls: ["./tab-anexos.component.scss"],
})
export class TabAnexosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() public regresar = new EventEmitter();
  @Output()
  public siguiente = new EventEmitter();

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_15;

  codigoAcordeon15_1: string = CodigosTabEvaluacion.POAC_TAB_15_1;
  codigoAcordeon15_2: string = CodigosTabEvaluacion.POAC_TAB_15_2;

  evaluacionAnexo1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon15_1,
  });
  evaluacionAnexo2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon15_2,
  });
 /*  evaluacionAnexo3: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon15_3,
  }); */

  public view: any = null;
  listAnexos2: any[] = [];
  totalRecords: number = 0;
  listTabsToAnexo3: any[] = [];

  displayAnexo2CensoComercial: boolean = false;
  editAnexo2CensoComercial: boolean = false;
  indexAnexo2CensoComercial: number = 0;
  anexo2CensoComercialForm: Anexo2CensoComercialModelPOAC = {} as Anexo2CensoComercialModelPOAC;

  actualizarRegistros: Anexo2CensoComercialModelPOAC[] = [];
  nuevosRegistros: Anexo2CensoComercialModelPOAC[] = [];
  accept: string =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel";
  acceptMapaDispersion: string =
    "image/jpeg,image/png,image/jpeg,application/pdf";

  codigoCalidad = {
    ARCALMUB: "ARCALMUB",
    ARCALBUE: "ARCALBUE",
    ARCALREG: "ARCALREG",
    ARCALMAL: "ARCALMAL",
  };

  codigoCategoria = {
    ARBTAPRO: "ARBTAPRO",
    ARBTSEMI: "ARBTSEMI",
    ARBTFUTC: "ARBTFUTC",
  };

  calidadFuste = [
    { codigo: "ARCALMUB", valor1: "Muy Bueno" },
    { codigo: "ARCALBUE", valor1: "Bueno" },
    { codigo: "ARCALREG", valor1: "Regular" },
    { codigo: "ARCALMAL", valor1: "Malo" },
  ];

  arbolesSemilleros = [
    { codigo: "ARBTAPRO", valor1: "A" },
    { codigo: "ARBTSEMI", valor1: "S" },
    { codigo: "ARBTFUTC", valor1: "FC" },
  ];
  evaluacion: any;

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private anexosService: AnexosService,
    private anexosCargaExcelService: AnexosCargaExcelService,
    private archivoService: ArchivoService,
    private confirmationService: ConfirmationService,
    private usuarioService: UsuarioService,
    private messageService: MessageService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listAnexo2();
    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  regresarTab() {
    this.regresar.emit();
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  listAnexo2() {
    this.listAnexos2 = [];
    this.actualizarRegistros = [];
    this.nuevosRegistros = [];

    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: "POAC",
    };

    this.anexosService.Anexo2(params).subscribe((response: any) => {
      this.listAnexos2 = response.data;
      this.totalRecords = response.data.length;
    });
  }

  descargarArchivo(idArchivo: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idArchivo: idArchivo,
    };
    this.archivoService
      .descargarArchivoGeneral(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.archivo,
            result.data.nombeArchivo,
            result.data.contenTypeArchivo
          );
        }
        (error: HttpErrorResponse) => {
          this.toast.error(error.message);
          this.dialog.closeAll();
        };
      });
  }

  cargarFormato(files: any) {
    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          tipoPlan: "POCC",
          idPlanManejo: this.idPlanManejo,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosCargaExcelService
          .FormatoPGMFFormuladoAnexo2Excel(
            t.file,
            item.tipoPlan,
            item.idPlanManejo
          )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe(
            (res: any) => {
              if (res.success == true) {
                this.toast.ok(res?.message);
                // this.listAnexo2();
              } else {
                this.toast.error(res?.message);
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            }
          );
      });
    } else {
      this.toast.warn("Debe seleccionar archivo.");
    }
  }

  successMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({
      severity: "warn",
      summary: "",
      detail: mensaje,
    });
  }

  eliminar(event: any, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de querer eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        var params = {
          idCensoForestalDetalle: data.idCensoForestalDetalle,
          idUsuarioElimina: this.usuarioService.idUsuario,
        };

        if (data.idCensoForestalDetalle > 0) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.anexosService.eliminarCensoForestal(params).subscribe(
            (response: any) => {
              if (response.success == true) {
                this.toast.ok("Se eliminó el registro correctamente.");
                this.listAnexo2();
                this.actualizarRegistros = [];

                const findIndex = this.actualizarRegistros
                  .map((data: any) => {
                    return data.idCensoForestalDetalle;
                  })
                  .indexOf(
                    this.anexo2CensoComercialForm.idCensoForestalDetalle
                  );

                if (findIndex !== -1) {
                  this.actualizarRegistros.splice(findIndex, 1);
                }
                this.listAnexos2.splice(index, 1);
              } else {
                this.errorMensaje(response.message);
              }
              this.dialog.closeAll();
            },
            (error) => {
              this.dialog.closeAll();
              this.errorMensaje(error);
            }
          );
        } else {
          const findIndex = this.nuevosRegistros.indexOf(data);

          if (findIndex !== -1) {
            this.nuevosRegistros.splice(findIndex, 1);
          }
          this.listAnexos2.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  // Anexo2 Censo Comercial modal

  abrirModalAnexo2CensoComercial() {
    this.displayAnexo2CensoComercial = true;
    this.anexo2CensoComercialForm = new Anexo2CensoComercialModelPOAC();
    this.editAnexo2CensoComercial = false;
  }

  openEditarAnexo2CensoComercial(
    data: Anexo2CensoComercialModelPOAC,
    index: number
  ) {
    const calidadF = this.calidadFuste.filter(
      (res) =>
        res.valor1 === data.calidadFuste || res.codigo === data.calidadFuste
    );

    const categoria = this.arbolesSemilleros.filter(
      (res) => res.valor1 === data.categoria || res.codigo === data.categoria
    );

    this.displayAnexo2CensoComercial = true;

    const obj = new Anexo2CensoComercialModelPOAC(data);
    this.anexo2CensoComercialForm = { ...obj };
    this.anexo2CensoComercialForm.condicionArbol = calidadF.length
      ? calidadF[0].codigo
      : "";
    this.anexo2CensoComercialForm.bosqueSecundario = categoria.length
      ? categoria[0].codigo
      : "";
    this.editAnexo2CensoComercial = true;
    this.indexAnexo2CensoComercial = index;
  }

  agregarAnexo2CensoComercial() {
    if (this.editAnexo2CensoComercial) {
      this.editarAnexo2CensoComercial();
    } else {
      this.registrarNuevaAnexo2CensoComercial();
    }
  }

  validacionCamposOperacion() {
    let esValido = true;
    let mensaje = "";
    if (this.anexo2CensoComercialForm.numeroFaja == "") {
      mensaje += "(*) Debe ingresar n° faja.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.nArbol == "") {
      mensaje += "(*) Debe ingresar n° árbol.\n";
      esValido = false;
    }

    if (this.anexo2CensoComercialForm.nombreEspecies == "") {
      mensaje += "(*) Debe ingresar especie.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.dap == "") {
      mensaje += "(*) Debe ingresar dap.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.alturaComercial == "") {
      mensaje += "(*) Debe ingresar altura comercial.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.condicionArbol == "") {
      mensaje += "(*) Debe ingresar calidad de fuste.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.este == "") {
      mensaje += "(*) Debe ingresar coordenada X.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.norte == "") {
      mensaje += "(*) Debe ingresar coordenada Y.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.volumen == "") {
      mensaje += "(*) Debe ingresar Vc.\n";
      esValido = false;
    }
    if (this.anexo2CensoComercialForm.bosqueSecundario == "") {
      mensaje += "(*) Debe ingresar árbol semillero.\n";
      esValido = false;
    }
    if (!esValido) this.errorMensaje(mensaje);

    return esValido;
  }

  registrarNuevaAnexo2CensoComercial() {
    if (!this.validacionCamposOperacion()) return;

    this.listAnexos2.push(this.anexo2CensoComercialForm);
    this.nuevosRegistros.push(this.anexo2CensoComercialForm);
    this.displayAnexo2CensoComercial = false;
  }

  editarAnexo2CensoComercial() {
    if (!this.validacionCamposOperacion()) return;

    const findIndex = this.actualizarRegistros
      .map((data: any) => {
        return data.idCensoForestalDetalle;
      })
      .indexOf(this.anexo2CensoComercialForm.idCensoForestalDetalle);

    const findIndexRegistro = this.nuevosRegistros
      .map((data: any) => {
        return data.idCensoForestalDetalle;
      })
      .indexOf(this.anexo2CensoComercialForm.idCensoForestalDetalle);

    if (findIndex !== -1) {
      this.actualizarRegistros[
        findIndexRegistro
      ] = this.anexo2CensoComercialForm;
    } else {
      if (this.anexo2CensoComercialForm.idCensoForestalDetalle == "0") {
        this.nuevosRegistros[findIndexRegistro] = this.anexo2CensoComercialForm;
      } else {
        this.actualizarRegistros.push(this.anexo2CensoComercialForm);
      }
    }

    this.listAnexos2[
      this.indexAnexo2CensoComercial
    ] = this.anexo2CensoComercialForm;
    this.displayAnexo2CensoComercial = false;
    this.indexAnexo2CensoComercial = 0;
  }

  cerrarModalAnexo2CensoComercial() {
    this.displayAnexo2CensoComercial = false;
  }

  guardarAnexos() {
    if (this.actualizarRegistros.length > 0) {
      this.actualizarRegistros.map((data: Anexo2CensoComercialModelPOAC) => {
        const obj = {
          idTipoArbol: data.bosqueSecundario,
          codigoArbolCalidad: data.condicionArbol,
          faja: data.numeroFaja,
          dap: data.dap,
          alturaComercial: data.alturaComercial,
          este: data.este,
          norte: data.norte,
          volumen: data.volumen,
          nombreCientifico: data.nombreEspecies,
          numeroCorrelativoArbol: data.nArbol,
        };
        const params = {
          idCensoForestalDetalle: data.idCensoForestalDetalle,
          idUsuarioRegistro: this.usuarioService.idUsuario,
        };

        this.anexosService
          .actualizarCensoForestal(params, obj)
          .subscribe((data: any) => {
            if (data.success) {
              this.successMensaje(
                "Se actualizaron datos del censo correctamente."
              );

              this.listAnexo2();
            } else {
              this.errorMensaje(data.message);
            }
          });
      });
    }
    if (this.nuevosRegistros.length > 0) {
      this.nuevosRegistros.map((data: Anexo2CensoComercialModelPOAC) => {
        const obj = {
          idTipoArbol: data.bosqueSecundario,
          codigoArbolCalidad: data.condicionArbol,
          faja: data.numeroFaja,
          dap: parseInt(data.dap),
          alturaComercial: data.alturaComercial,
          este: data.este,
          norte: data.norte,
          volumen: parseInt(data.volumen),
          nombreCientifico: data.nombreEspecies,
          numeroCorrelativoArbol: parseInt(data.nArbol),
          // condicionArbol : data.condicionArbol,
          // bosqueSecundario :data.bosqueSecundario
        };
        const params = {
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.usuarioService.idUsuario,
        };

        this.anexosService
          .registrarCensoForestal(params, obj)
          .subscribe((data: any) => {
            if (data.success) {
              this.successMensaje("Se registró datos del censo correctamente.");

              this.listAnexo2();
            } else {
              this.errorMensaje(data.message);
            }
          });
      });
    }
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionAnexo1 = Object.assign(
                this.evaluacionAnexo1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon15_1
                )
              );
              this.evaluacionAnexo2 = Object.assign(
                this.evaluacionAnexo2,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon15_2
                )
              );
              /* this.evaluacionAnexo3 = Object.assign(
                this.evaluacionAnexo3,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon16_3
                )
              ); */
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacionAnexo1,
        this.evaluacionAnexo2,
      //  this.evaluacionAnexo3,
      ])
    ) {
      if (this.evaluacion) {
        
        
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAnexo1);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAnexo2);
      //  this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionAnexo3);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"COD_2",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-pgmfa/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
