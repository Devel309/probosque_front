import { HttpErrorResponse } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { UsuarioService } from "@services";
import { isNullOrEmpty, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { ActividadSilviculturales } from "src/app/model/ActividadesSilviculturalesModel";
import { ActividadesSilviculturalesService } from "src/app/service/planificacion/plan-general-manejo-pgmfa/actividadesSilviculturales.service";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";

@Component({
  selector: "app-tab-actividades-silviculturales",
  templateUrl: "./tab-actividades-silviculturales.component.html",
  styleUrls: ["./tab-actividades-silviculturales.component.scss"],
})
export class TabActividadesSilviculturalesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  displayReforestacion: boolean = false;
  editReforestacion: boolean = false;
  indexReforestacion: number = 0;

  listTratamientoSilvicultural: any[] = [];
  listaReforestacion: any[] = [];

  listTratamiento: any[] = [];
  listaRefrs: any[] = [];

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_6;

  codigoAcordeon6_61: string = CodigosTabEvaluacion.POAC_TAB_6_61;
  codigoAcordeon6_62: string = CodigosTabEvaluacion.POAC_TAB_6_62;

  evaluacion6_61: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon6_61,
  });
  evaluacion6_62: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon6_62,
  });

  evaluacion: any;
  idActividadSilviculturalPOAC2: number = 0;
  idActividadSilviculturalPOAC1: number = 0;
  context = {} as any;

  isSubmitting$ = this.query.selectSubmitting();

  constructor(
    private actividadesSilviculturalesService: ActividadesSilviculturalesService,
    private user: UsuarioService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private createStore: ButtonsCreateStore,
    private query: ButtonsCreateQuery,
    private router: Router
  ) {}

  tratamientoSilvicultural: any[] = [];

  reforestacion: any[] = [];

  ngOnInit(): void {
    this.listAplicacionTratamientos();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listAplicacionTratamientos() {
    this.tratamientoSilvicultural = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      idTipo: "POAC1",
    };
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          this.idActividadSilviculturalPOAC1 =
            response.data.idActividadSilvicultural;
          response.data.detalle.forEach((element: any) => {
            this.tratamientoSilvicultural.push({
              ...element,
              checked: element.accion,
            });
          });
          this.listReforestacion();
        } else {
          this.listActividadesSilviculturales();
        }
      });
  }

  listReforestacion() {
    this.reforestacion = [];
    var params = {
      idPlanManejo: this.idPlanManejo,
      idTipo: "POAC2",
    };
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .subscribe((response: any) => {
        if (response.data != null) {
          this.idActividadSilviculturalPOAC2 =
            response.data.idActividadSilvicultural;
          this.reforestacion = [...response.data.detalle];
        } else {
          /*  this.listActividadesSilviculturales(); */
        }
      });
  }

  listActividadesSilviculturales() {
    var params = {
      idPlanManejo: 0,
      idTipo: "POAC",
    };
    this.actividadesSilviculturalesService
      .listarActividadSilviculturalPFDM(params)
      .subscribe((response: any) => {
        response.data.detalle.forEach((element: any) => {
          if (element.idTipoTratamiento == "POAC1") {
            if (element.actividad === "Otros (especificar)") {
              element.nuevo = true;
              element.observacionDetalle = "Otros";
            }
            this.tratamientoSilvicultural.push({
              ...element,
              idActividadSilviculturalDet: 0,
            });            
          } else if (element.idTipoTratamiento == "POAC2") {
            this.reforestacion.push({
              ...element,
              idActividadSilviculturalDet: 0,
            });
          }
        });
      });
  }

  abrirModalReforestacion() {
    this.displayReforestacion = true;
    this.context = {};
    this.editReforestacion = false;
    this.indexReforestacion = 0;
  }

  agregarActividad() {
    this.tratamientoSilvicultural.push({ nuevo: true, checked: true, actividad: "Otros (especificar)", observacionDetalle: 'Otros' });
  }

  agregarActividadReforestacion() {
    if (this.validarReforestacion()) {
      this.displayReforestacion = false;
      if (this.editReforestacion) {
        this.reforestacion[this.indexReforestacion] = this.context;
      } else {
        this.reforestacion.push(this.context);
      }
    }
  }

  openEditarEspecie(data: any, index: number) {
    this.displayReforestacion = true;
    this.context = {...data};
    this.editReforestacion = true;
    this.indexReforestacion = index;
  }

  registrar() {
    if (this.validarActividad()) {
      this.registrarActividad();
    }
  }

  registrarActividad() {
    this.listTratamientoSilvicultural = [];
    this.listaReforestacion = [];
    this.tratamientoSilvicultural.forEach((item) => {
      let tratamientoObj = new ActividadSilviculturales(item);
      tratamientoObj.idTipo = "POAC1";
      tratamientoObj.idTipoTratamiento = "POAC1";
      tratamientoObj.idUsuarioRegistro = this.user.idUsuario;
      tratamientoObj.accion = item.checked;
      this.listTratamientoSilvicultural.push(tratamientoObj);
    });
    this.reforestacion.forEach((item) => {
      let reforestacionObj = new ActividadSilviculturales(item);
      reforestacionObj.idTipo = "POAC2";
      reforestacionObj.idTipoTratamiento = "POAC2";
      reforestacionObj.idUsuarioRegistro = this.user.idUsuario;
      reforestacionObj.accion = item.checked;
      this.listaReforestacion.push(reforestacionObj);
    });

    var params = {
      aprovechamiento: {
        idActSilvicultural: this.idActividadSilviculturalPOAC1
          ? this.idActividadSilviculturalPOAC1
          : 0,
        codigoTipoActSilvicultural: "POAC",
        actividad: "Aplicación de tratamientos silviculturales",
        descripcion: null,
        observacion: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listActividadSilvicultural: this.listTratamientoSilvicultural,
      },
      divisionAdministrativa: null,
      laboresSilviculturales: {
        idActSilvicultural: this.idActividadSilviculturalPOAC2
          ? this.idActividadSilviculturalPOAC2
          : 0,
        codigoTipoActSilvicultural: "POAC",
        actividad: "Reforestación",
        descripcion: null,
        observacion: null,
        idPlanManejo: this.idPlanManejo,
        idUsuarioRegistro: this.user.idUsuario,
        listActividadSilvicultural: this.listaReforestacion,
      },
    };

    this.createStore.submit();

    return this.actividadesSilviculturalesService
      .guardarSistemaManejoPFDM(params)
      .pipe(
        tap(
          (response) => {
            this.createStore.submitSuccess();
            this.toast.ok(response.message);
            this.listAplicacionTratamientos();
            this.listReforestacion();
          },
          (error) => {
            this.createStore.submitError(error);
            this.toast.ok(error.message);
          }
        )
      )
      .subscribe();
  }

  validarActividad() {
    let validar: boolean = true;
    let mensaje: string = "";

    this.tratamientoSilvicultural.forEach((item) => {
      if (item.observacionDetalle !== null) {
        if (isNullOrEmpty(item.actividad)) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Tratamiento silvicultural.\n";
        }
      }
    });

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  validarReforestacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (isNullOrEmpty(this.context.actividad)) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Reforestación.\n";
    }
    if (isNullOrEmpty(this.context.descripcionDetalle)) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Descipción.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  openEliminarEspecie(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idActividadSilviculturalDet > 0) {
          let params = [
            {
              idActividadSilviculturalDet: data.idActividadSilviculturalDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];

          this.actividadesSilviculturalesService
            .eliminarActividadSilviculturalPFDM(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.toast.ok(result.message);

                this.reforestacion.splice(index, 1);
              } else {
                this.toast.warn(result.message);
              }
            });
        } else {
          this.reforestacion.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion6_61 = Object.assign(
                this.evaluacion6_61,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon6_61
                )
              );
              this.evaluacion6_62 = Object.assign(
                this.evaluacion6_62,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon6_62
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion6_61, this.evaluacion6_62])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion6_61);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion6_62);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  eliminar(event: any, index: number, idActividadSilviculturalDet: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (idActividadSilviculturalDet > 0) {
          let params = [
            {
              idActividadSilviculturalDet: idActividadSilviculturalDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];

          this.actividadesSilviculturalesService
            .eliminarActividadSilviculturalPFDM(params)
            .subscribe((result: any) => {
              if (result.success) {
                this.toast.ok(result.message);

                this.tratamientoSilvicultural.splice(index, 1);
              } else {
                this.toast.warn(result.message);
              }
            });
        } else {
          this.tratamientoSilvicultural.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
