import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { UsuarioService } from "@services";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { CronogrmaActividadesService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/cronograma-actividades.service";
import { ModalFormularioCronogramaComponent } from "./modal/modal-formulario-zonas/modal-formulario-cronograma.component";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { ToastService } from "@shared";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { finalize, tap } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { EvaluacionUtils } from "src/app/model/util/EvaluacionUtils";
import { Mensajes } from "src/app/model/util/Mensajes";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-cronograma-actividades",
  templateUrl: "./tab-cronograma-actividades.component.html",
  styleUrls: ["./tab-cronograma-actividades.component.scss"],
})
export class TabCronogramaActividadesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  ref!: DynamicDialogRef;
  context: Modelo = new Modelo();

  verMesAnio1: boolean = false;
  verMesAnio2: boolean = false;
  verMesAnio3: boolean = false;

  verAnio1: boolean = false;
  verAnio2: boolean = false;
  verAnio3: boolean = false;

  lstDetalle: Modelo[] = [];
  lstOtrosAnos: any[] = [];
  params: any;
  vigencia: number = 0;

  cmbAnios: any[] = [
    /*  {
      label: "AÑO 1",
      value: "1",
      items: [
        { label: "Año 1 - Mes 1", value: "1-1" },
        { label: "Año 1 - Mes 2", value: "1-2" },
        { label: "Año 1 - Mes 3", value: "1-3" },
        { label: "Año 1 - Mes 4", value: "1-4" },
        { label: "Año 1 - Mes 5", value: "1-5" },
        { label: "Año 1 - Mes 6", value: "1-6" },
        { label: "Año 1 - Mes 7", value: "1-7" },
        { label: "Año 1 - Mes 8", value: "1-8" },
        { label: "Año 1 - Mes 9", value: "1-9" },
        { label: "Año 1 - Mes 10", value: "1-10" },
        { label: "Año 1 - Mes 11", value: "1-11" },
        { label: "Año 1 - Mes 12", value: "1-12" },
      ],
    },
    {
      label: "AÑO 2",
      value: "2",
      items: [
        { label: "Año 2 - Mes 1", value: "2-1" },
        { label: "Año 2 - Mes 2", value: "2-2" },
        { label: "Año 2 - Mes 3", value: "2-3" },
        { label: "Año 2 - Mes 4", value: "2-4" },
        { label: "Año 2 - Mes 5", value: "2-5" },
        { label: "Año 2 - Mes 6", value: "2-6" },
        { label: "Año 2 - Mes 7", value: "2-7" },
        { label: "Año 2 - Mes 8", value: "2-8" },
        { label: "Año 2 - Mes 9", value: "2-9" },
        { label: "Año 2 - Mes 10", value: "2-10" },
        { label: "Año 2 - Mes 11", value: "2-11" },
        { label: "Año 2 - Mes 12", value: "2-12" },
      ],
    },
    {
      label: "AÑO 3",
      value: "3",
      items: [
        { label: "Año 3 - Mes 1", value: "3-1" },
        { label: "Año 3 - Mes 2", value: "3-2" },
        { label: "Año 3 - Mes 3", value: "3-3" },
        { label: "Año 3 - Mes 4", value: "3-4" },
        { label: "Año 3 - Mes 5", value: "3-5" },
        { label: "Año 3 - Mes 6", value: "3-6" },
        { label: "Año 3 - Mes 7", value: "3-7" },
        { label: "Año 3 - Mes 8", value: "3-8" },
        { label: "Año 3 - Mes 9", value: "3-9" },
        { label: "Año 3 - Mes 10", value: "3-10" },
        { label: "Año 3 - Mes 11", value: "3-11" },
        { label: "Año 3 - Mes 12", value: "3-12" },
      ],
    },
  
   */
  ];

  evaluacion: any;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_13;

  codigoAcordeon13_1: string = CodigosTabEvaluacion.POAC_TAB_13_1;
  evaluacion13_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon13_1,
  });

  constructor(
    private confirmationService: ConfirmationService,
    public dialogService: DialogService,
    private cronogrmaActividadesService: CronogrmaActividadesService,
    private user: UsuarioService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listarInfGeneral();
    this.listarCronogramaActividad();
    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  listarCronogramaActividad() {
    this.lstDetalle = [];
    this.verAnio1 = false;
    this.verAnio2 = false;
    this.verAnio3 = false;

    var params = {
      codigoProceso: "POAC",
      idCronogramaActividad: null,
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.cronogrmaActividadesService
      .listarCronogramaActividad(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((x: any) => {
            this.lstDetalle.push(x);
          });
          this.marcar();
        }
      });
  }

  listarInfGeneral() {
    var params = {
      idInformacionGeneralDema: null,
      codigoProceso: "POAC",
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        response.data.forEach((element: any) => {
          this.vigencia = Number(element.vigencia);
        });
        if (this.vigencia <= 3) {
          for (let i = 1; i <= this.vigencia; i++) {
            this.params = {
              label: "AÑO " + i,
              value: i.toString(),
              items: [
                { label: "Año " + i + "- Mes 1", value: i + "-1" },
                { label: "Año " + i + "- Mes 2", value: i + "-2" },
                { label: "Año " + i + "- Mes 3", value: i + "-3" },
                { label: "Año " + i + "- Mes 4", value: i + "-4" },
                { label: "Año " + i + "- Mes 5", value: i + "-5" },
                { label: "Año " + i + "- Mes 6", value: i + "-6" },
                { label: "Año " + i + "- Mes 7", value: i + "-7" },
                { label: "Año " + i + "- Mes 8", value: i + "-8" },
                { label: "Año " + i + "- Mes 9", value: i + "-9" },
                { label: "Año " + i + "- Mes 10", value: i + "-10" },
                { label: "Año " + i + "- Mes 11", value: i + "-11" },
                { label: "Año " + i + "- Mes 12", value: i + "-12" },
              ],
            };
            this.cmbAnios.push(this.params);
          }
        }
      });
    //   this.listarCronogramaActividad();
  }

  marcar() {
    var aniosMeses: any[] = [];

    this.lstDetalle.forEach((x: any, index) => {
      aniosMeses = [];
      if (this.vigencia == 1) {
        this.verAnio1 = true;
      } else if (this.vigencia == 2) {
        this.verAnio1 = true;
        this.verAnio2 = true;
      } else if (this.vigencia == 3) {
        this.verAnio1 = true;
        this.verAnio2 = true;
        this.verAnio3 = true;
      }
      x.detalle.forEach((element: any) => {
        aniosMeses.push({
          aniosMeses: element.anio.toString() + "-" + element.mes.toString(),
          id: element.idCronogramaActividadDetalle,
        });
      });

      var cont = {
        aniosMeses: aniosMeses,
        actividad: x.actividad,
        idCronogramaActividad: x.idCronogramaActividad,
        tipoActividad: x.tipoActividad,
      };
      this.context = new Modelo(cont);
      this.context.marcarMeses();
      this.lstDetalle[index] = this.context;
    });
  }

  guardarTipoActividadPrincipal(resp: any) {
    let params = {
      actividad: resp,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "POAC",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        console.log(response);
      });
  }

  // agregar gronograma solo el nombre de la actividad
  guardarTipoActividad(resp: any) {
    let params = {
      actividad: resp.actividad,
      anio: null,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      importe: null,
      codigoActividad: "AAEAM",
      codigoProceso: "POAC",
    };
    this.cronogrmaActividadesService
      .registrarCronogramaActividades(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó el cronograma de actividades correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  // guardar actividad detalle relacion año - mes
  guardarTipoActividadDetalle(resp: any, data: any) {
    let anosMes = resp.aniosMeses.toString();
    let params = {
      idCronogramaActividad: data.idCronogramaActividad
        ? data.idCronogramaActividad
        : 0,
      mesesAnios: anosMes,
      idCronogramaActividadDetalle: 0,
      idUsuarioRegistro: this.user.idUsuario,
    };

    this.cronogrmaActividadesService
      .registrarMarcaCronogramaActividadDetalle(params)
      .subscribe((response: any) => {
        if (response.isSuccess) {
          this.messageService.add({
            key: "tl",
            severity: "success",
            detail: "Se actualizó la actividad del cronograma correctamente.",
          });
          this.listarCronogramaActividad();
        } else {
          this.messageService.add({
            key: "tl",
            severity: "error",
            summary: "ERROR",
            detail: "Ocurrió un problema, intente nuevamente.",
          });
        }
      });
  }

  openModal = (mesaje: string, data: any, tipo: any) => {
    

    this.ref = this.dialogService.open(ModalFormularioCronogramaComponent, {
      header: mesaje,
      width: "40%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        data: data,
        type: tipo,
        cmbAnios: this.cmbAnios,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (tipo == "C") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividad(resp);
        } else if (tipo == "E") {
          this.context = new Modelo(resp, tipo);
          this.guardarTipoActividadDetalle(resp, data);
        }
      } else if (resp == null) {
        this.listarCronogramaActividad();
      }
    });
  };

  guardar(): void {
    if (this.context.id == "") {
      this.context.id = new Date().toUTCString();
      this.context.marcarMeses();
      this.lstDetalle.push(this.context);
    }
  }

  openEliminar(event: Event, data: Modelo): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar este registro?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idCronogramaActividad != null) {
          var params = {
            idCronogramaActividad: data.idCronogramaActividad,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.cronogrmaActividadesService
            .eliminarCronogramaActividad(params)
            .subscribe((response: any) => {
              if (response.isSuccess) {
                this.messageService.add({
                  key: "tl",
                  severity: "success",
                  detail:
                    "Se eliminó la actividad del cronograma correctamente.",
                });
                this.listarCronogramaActividad();
              } else {
                this.messageService.add({
                  key: "tl",
                  severity: "error",
                  detail: "Ocurrió un problema, intente nuevamente.",
                });
              }
            });
        } else {
          this.lstDetalle.splice(
            this.lstDetalle.findIndex((x) => x.id == data.id),
            1
          );
        }
      },
      reject: () => {},
    });
  }
  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion13_1 = Object.assign(
                this.evaluacion13_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon13_1
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion13_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion13_1);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
 /*  cargarFormato(files: any) {
    console.log("files", files);

    if (files.length > 0) {
      files.forEach((t: any) => {
        let item = {
          nombreHoja: "Hoja1",
          numeroFila: 3,
          numeroColumna: 1,
          codigoProceso: "POAC",
          codigoActividad: "AAEAM",
          idPlanManejo: this.idPlanManejo,
          idUsuarioRegistro: this.user.idUsuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.cronogrmaActividadesService
          .registrarCronogramaActividadExcel(
            t.file,
            item.nombreHoja,
            item.numeroFila,
            item.numeroColumna,
            item.codigoProceso,
            item.codigoActividad,
            item.idPlanManejo,
            item.idUsuarioRegistro
          )
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe(
            (res: any) => {
              if (res.success == true) {
                this.toast.ok(res?.message);
                this.listarCronogramaActividad();
              } else {
                this.toast.error(res?.message);
              }
            },
            (error: HttpErrorResponse) => {
              this.dialog.closeAll();
            }
          );
      });
    } else {
      this.toast.warn("Debe seleccionar archivo.");
    }
  } */
  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
export class Modelo {
  constructor(data?: any, tipo?: any) {
    if (data && tipo == "C") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data && tipo == "E") {
      this.id = "";
      this.tipoActividad = data.tipoActividad;
      this.actividad = data.actividades;
      this.aniosMeses = data.aniosMeses;
      return;
    } else if (data) {
      this.aniosMeses = data.aniosMeses;
      (this.actividad = data.actividad),
        (this.idCronogramaActividad = data.idCronogramaActividad),
        (this.tipoActividad = data.tipoActividad);
      return;
    }
    
  }

  showRemove: boolean = true;
  tipoActividad?: string = "";
  actividad?: string = "";
  actividades?: string = "";
  aniosMeses: any[] = [];
  id?: string = "";
  idCronogramaActividadDetalle?: number = 0;
  idCronogramaActividad: number = 0;

  a1m1: boolean = false;
  a1m2: boolean = false;
  a1m3: boolean = false;
  a1m4: boolean = false;
  a1m5: boolean = false;
  a1m6: boolean = false;
  a1m7: boolean = false;
  a1m8: boolean = false;
  a1m9: boolean = false;
  a1m10: boolean = false;
  a1m11: boolean = false;
  a1m12: boolean = false;

  a2m1: boolean = false;
  a2m2: boolean = false;
  a2m3: boolean = false;
  a2m4: boolean = false;
  a2m5: boolean = false;
  a2m6: boolean = false;
  a2m7: boolean = false;
  a2m8: boolean = false;
  a2m9: boolean = false;
  a2m10: boolean = false;
  a2m11: boolean = false;
  a2m12: boolean = false;

  a3m1: boolean = false;
  a3m2: boolean = false;
  a3m3: boolean = false;
  a3m4: boolean = false;
  a3m5: boolean = false;
  a3m6: boolean = false;
  a3m7: boolean = false;
  a3m8: boolean = false;
  a3m9: boolean = false;
  a3m10: boolean = false;
  a3m11: boolean = false;
  a3m12: boolean = false;

  otros: any[] = [];

  marcarMeses(): void {
    this.aniosMeses.forEach((x) => {
      if (x.aniosMeses == "1-1") {
        this.a1m1 = x.aniosMeses.indexOf("1-1") > -1;
      } else if (x.aniosMeses == "1-2") {
        this.a1m2 = x.aniosMeses.indexOf("1-2") > -1;
      } else if (x.aniosMeses == "1-3") {
        this.a1m3 = x.aniosMeses.indexOf("1-3") > -1;
      } else if (x.aniosMeses == "1-4") {
        this.a1m4 = x.aniosMeses.indexOf("1-4") > -1;
      } else if (x.aniosMeses == "1-5") {
        this.a1m5 = x.aniosMeses.indexOf("1-5") > -1;
      } else if (x.aniosMeses == "1-6") {
        this.a1m6 = x.aniosMeses.indexOf("1-6") > -1;
      } else if (x.aniosMeses == "1-7") {
        this.a1m7 = x.aniosMeses.indexOf("1-7") > -1;
      } else if (x.aniosMeses == "1-8") {
        this.a1m8 = x.aniosMeses.indexOf("1-8") > -1;
      } else if (x.aniosMeses == "1-9") {
        this.a1m9 = x.aniosMeses.indexOf("1-9") > -1;
      } else if (x.aniosMeses == "1-10") {
        this.a1m10 = x.aniosMeses.indexOf("1-10") > -1;
      } else if (x.aniosMeses == "1-11") {
        this.a1m11 = x.aniosMeses.indexOf("1-11") > -1;
      } else if (x.aniosMeses == "1-12") {
        this.a1m12 = x.aniosMeses.indexOf("1-12") > -1;
      } else if (x.aniosMeses == "2-1") {
        this.a2m1 = x.aniosMeses.indexOf("2-1") > -1;
      } else if (x.aniosMeses == "2-2") {
        this.a2m2 = x.aniosMeses.indexOf("2-2") > -1;
      } else if (x.aniosMeses == "2-3") {
        this.a2m3 = x.aniosMeses.indexOf("2-3") > -1;
      } else if (x.aniosMeses == "2-4") {
        this.a2m4 = x.aniosMeses.indexOf("2-4") > -1;
      } else if (x.aniosMeses == "2-5") {
        this.a2m5 = x.aniosMeses.indexOf("2-5") > -1;
      } else if (x.aniosMeses == "2-6") {
        this.a2m6 = x.aniosMeses.indexOf("2-6") > -1;
      } else if (x.aniosMeses == "2-7") {
        this.a2m7 = x.aniosMeses.indexOf("2-7") > -1;
      } else if (x.aniosMeses == "2-8") {
        this.a2m8 = x.aniosMeses.indexOf("2-8") > -1;
      } else if (x.aniosMeses == "2-9") {
        this.a2m9 = x.aniosMeses.indexOf("2-9") > -1;
      } else if (x.aniosMeses == "2-10") {
        this.a2m10 = x.aniosMeses.indexOf("2-10") > -1;
      } else if (x.aniosMeses == "2-11") {
        this.a2m11 = x.aniosMeses.indexOf("2-11") > -1;
      } else if (x.aniosMeses == "2-12") {
        this.a2m12 = x.aniosMeses.indexOf("2-12") > -1;
      } else if (x.aniosMeses == "3-1") {
        this.a3m1 = x.aniosMeses.indexOf("3-1") > -1;
      } else if (x.aniosMeses == "3-2") {
        this.a3m2 = x.aniosMeses.indexOf("3-2") > -1;
      } else if (x.aniosMeses == "3-3") {
        this.a3m3 = x.aniosMeses.indexOf("3-3") > -1;
      } else if (x.aniosMeses == "3-4") {
        this.a3m4 = x.aniosMeses.indexOf("3-4") > -1;
      } else if (x.aniosMeses == "3-5") {
        this.a3m5 = x.aniosMeses.indexOf("3-5") > -1;
      } else if (x.aniosMeses == "3-6") {
        this.a3m6 = x.aniosMeses.indexOf("3-6") > -1;
      } else if (x.aniosMeses == "3-7") {
        this.a3m7 = x.aniosMeses.indexOf("3-7") > -1;
      } else if (x.aniosMeses == "3-8") {
        this.a3m8 = x.aniosMeses.indexOf("3-8") > -1;
      } else if (x.aniosMeses == "3-9") {
        this.a3m9 = x.aniosMeses.indexOf("3-9") > -1;
      } else if (x.aniosMeses == "3-10") {
        this.a3m10 = x.aniosMeses.indexOf("3-10") > -1;
      } else if (x.aniosMeses == "3-11") {
        this.a3m11 = x.aniosMeses.indexOf("3-11") > -1;
      } else if (x.aniosMeses == "3-12") {
        this.a3m12 = x.aniosMeses.indexOf("3-12") > -1;
      }
    });
  }
}
