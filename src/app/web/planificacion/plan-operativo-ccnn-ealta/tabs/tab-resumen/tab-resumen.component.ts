import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { UsuarioService } from "@services";
import { descargarArchivo, ToastService } from "@shared";
import { ButtonsCreateQuery } from "src/app/features/state/buttons-create.query";
import { ButtonsCreateStore } from "src/app/features/state/buttons-create.store";
import { UrlFormatos } from "src/app/model/urlFormatos";
import { CodigoProceso } from "src/app/model/util/CodigoProceso";
import { FileModel } from "src/app/model/util/File";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { CargaArchivosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/caragaArchivos.service";
import { CargaEnvioDocumentacionService } from "src/app/service/planificacion/plan-general-manejo-forestal/carga.envio.documentacion.service";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import {MessageService} from 'primeng/api';
import {LoadingComponent} from '../../../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {NotificacionService} from '../../../../../service/notificacion';
import { PoacInformacionGeneral } from "src/app/model/poac-informacion-general";
import { environment } from "@env/environment";

@Component({
  selector: "app-tab-resumen",
  templateUrl: "./tab-resumen.component.html",
  styleUrls: ["./tab-resumen.component.scss"],
})
export class TabResumenComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Output()
  public regresar = new EventEmitter();
  UrlFormatos = UrlFormatos;

  verPOAC: boolean = false;
  pendientes: any[] = [];
  verDescargaPOAC: boolean = true;
  verDescargaAnexoPOAC: boolean = true;

  files: FileModel[] = [];
  filePOAC: FileModel = {} as FileModel;

  consolidadoPOAC: any = null;

  cargarPOAC: boolean = false;
  eliminarPOAC: boolean = true;
  idArchivoPOAC: number = 0;
  verEnviar: boolean = false;
  isTodosPlanesTerminado : boolean = true;

  listProcesos = [
    {
      idPlanManejoEstado: 0,
      codigo: 'POACIG',
      label: '1. Información General',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACRARPA',
      label: '2. Resumen Actividades y Recomendaciones del PO Anterior',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACO',
      label: '3. Objetivos',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACIBAAA',
      label: '4. Información básica del área de aprovechamiento anual',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACAA',
      label:
        '5. Actividades de aprovechamiento',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACAS',
      label: '6. Actividades silviculturales',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACPB',
      label: '7. Protección del bosque',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACM',
      label: '8. Monitoreo',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACPC',
      label: '9. Participación comunal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACC',
      label: '10. Capacitación',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACODA',
      label: '11. Organización para el desarrollo de la actividad',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACRMF',
      label: '12. Rentabilidad del manejo forestal',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACCA',
      label: '13. Cronograma de Actividades',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACAC',
      label: '14. Aspectos complementarios',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
    {
      idPlanManejoEstado: 0,
      codigo: 'POACAN',
      label: '15. Anexos',
      codigoEstado: 'CPMPEND',
      estado: 'Pendiente',
      marcar: false,
    },
  ];


  CodigoProceso = CodigoProceso;

  isSubmittingFormulario$ = this.formularioQuery.selectSubmitting();
  isLoadingFormulario$ = this.formularioQuery.selectLoading();

  isSubmittingGenerar$ = this.generarQuery.selectSubmitting();
  isLoadingGenerar$ = this.generarQuery.selectLoading();
  estado: string = "";
  firma: string = "";

  isSubmittingInformacion$ = this.informcionQuery.selectSubmitting();



  longitudTUPA:number=0;
  disabledCodigoEstadoPlanBorrador:boolean=true;
  disabledCodigoEstadoPlanPresentado:boolean=true;
  disabledCodigoEstadoPlanEnviado:boolean=false;
  disablePlanFormulado: boolean = true;
  enviarARFFS: boolean = false;
  disableAgregarTUPA: boolean = true;
  disableEnviarARFFS: boolean = true;
  isSubmittingARFFS$ = this.ARFFSQuery.selectSubmitting();
  isSubmittingPlanFormulado$ = this.planFormuladoQuery.selectSubmitting();
  form: PoacInformacionGeneral = new PoacInformacionGeneral();
  idInformacionGeneral: number = 0;
  urlFormatosolicitud: string = "";
  varAssets = `${environment.varAssets}`;

  constructor(
    private cargaEnvioDocumentacionService: CargaEnvioDocumentacionService,
    private cargaArchivosService: CargaArchivosService,
    private toast: ToastService,
    private user: UsuarioService,
    private messageService: MessageService,
    private evaluacionService: EvaluacionService,
    private generarStore: ButtonsCreateStore,
    private generarQuery: ButtonsCreateQuery,
    private formularioStore: ButtonsCreateStore,
    private formularioQuery: ButtonsCreateQuery,
    private informacionGeneralService: InformacionGeneralService,
    private iformacionStore: ButtonsCreateStore,
    private informcionQuery: ButtonsCreateQuery,
    private planFormuladoQuery: ButtonsCreateQuery,
    private planFormuladoStore: ButtonsCreateStore,
    private ARFFSQuery: ButtonsCreateQuery,
    private ARFFSStore: ButtonsCreateStore,
    private dialog: MatDialog,private notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.listarEstados();
    this.listarInfGeneral();
    this.obtenerEstadoPlan();
    this.listarInformacionGeneral();

    if (this.varAssets && this.varAssets != "") {
      this.urlFormatosolicitud = '/' + this.varAssets + UrlFormatos.POAC_17;
    } else {
      this.urlFormatosolicitud = UrlFormatos.POAC_17;
    }
  }

  obtenerEstadoPlan(){
    const body={
      idPlanManejo:this.idPlanManejo
    }
    this.informacionGeneralService.listarInfGeneralResumido(body).subscribe((_res:any)=>{

      if(_res.data.length>0){
        const estado= _res.data[0];
        if(estado.codigoEstado == 'EPLMPRES' || estado.codigoEstado == 'EMDOBS' || estado.codigoEstado == 'EMDBOR') {
          this.disabledCodigoEstadoPlanPresentado=false;
          this.disabledCodigoEstadoPlanBorrador= true;
        }
        if(estado.codigoEstado == 'EPLMBOR'){
          // this.disabledCodigoEstadoPlanPresentado=true;
          this.disabledCodigoEstadoPlanPresentado=false;  // Permite agregar cuando aún no está presentado
          this.disabledCodigoEstadoPlanBorrador= false;
        }
        if (estado.codigoEstado == 'EMDPRES') {
          this.disabledCodigoEstadoPlanEnviado = true;
        }
      }
    })
  }

  guardarPlanFormulado() {
    this.planFormuladoStore.submit();
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.planFormuladoStore.submitSuccess();
          this.toast.ok("Esta solicitud ha sido Formulada.");
          this.enviarARFFS = true
          this.disablePlanFormulado = true;
          this.disableAgregarTUPA = false;
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }



  listarInfGeneral() {

    var params = {
      idPlanManejo: this.idPlanManejo
    };

    this.informacionGeneralService
      .listarInfGeneralResumido(params)
      .subscribe((response: any) => {
        if (!!response.data) {
          response.data.forEach((item: any) => {
            this.firma = item.codigo;
          });
        }
      });
  }

  listarInformacionGeneral() {
    var params = {
      codigoProceso: CodigoProceso.PLAN_OPERATIVO,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.form = new PoacInformacionGeneral(element);
            this.idInformacionGeneral = element.idInformacionGeneralDema;
          });
        }
      });
  }

  editarInformacionGeneral() {
    this.form.fechaElaboracionPmfi = new Date();

    var params = new PoacInformacionGeneral(this.form);
    params.idPlanManejo = this.idPlanManejo;
    params.idUsuarioRegistro = this.user.idUsuario;
    params.codigoProceso = CodigoProceso.PLAN_OPERATIVO;
    params.idInformacionGeneralDema = this.idInformacionGeneral;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .actualizarInformacionGeneralDema(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.dialog.closeAll();
          this.SuccessMensaje(response.message);
          this.listarInformacionGeneral();
        } else this.ErrorMensaje(response.message);
      });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({ severity: "success", summary: "", detail: mensaje });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  seleccionarFirma = (event: any, data: any) => {
    let objIndex = this.listProcesos.findIndex(function (e) {
      return e.codigo === data.codigo;
    });

    if (event.checked) {
      this.listProcesos[objIndex].codigoEstado = 'CPMTERM';
      this.listProcesos[objIndex].estado = 'Terminado';
    } else {
      this.listProcesos[objIndex].codigoEstado = 'CPMPEND';
      this.listProcesos[objIndex].estado = 'Pendiente';
    }

    if (this.listProcesos.length === this.listProcesos.filter((data) => data.marcar === true).length) {
      this.verPOAC = false;
      this.isTodosPlanesTerminado = true;
    }else{
      this.isTodosPlanesTerminado = false;
    }
  };


  listarEstados() {
    this.formularioStore.submit();
    this.pendientes = [];
    let body = {
      idPlanManejo: this.idPlanManejo,
      codigoProceso: CodigoProceso.PLAN_OPERATIVO,
      idPlanManejoEstado: null,
    };

    this.cargaEnvioDocumentacionService
      .listarEstadosPlanManejo(body)
      .subscribe((response: any) => {
        this.formularioStore.submitSuccess();
        if (response.data.length == 0) {
          this.pendientes.push({ pendiente: 0 });
        }
        response.data.forEach((element: any) => {
          if (element.codigoEstado == 'CPMPEND') {
            this.pendientes.push(element);
          }
        });

        response.data.forEach((element: any) => {
          let objIndex = this.listProcesos.findIndex(function (e) {
            return e.codigo === element.codigo;
          });

          this.listProcesos[objIndex].codigoEstado = element.codigoEstado;

          if (element.codigoEstado == 'CPMPEND') {
            this.listProcesos[objIndex].estado = 'Pendiente';
            this.listProcesos[objIndex].idPlanManejoEstado = element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = false;
            this.isTodosPlanesTerminado = false;

          } else if (element.codigoEstado == 'CPMTERM') {
            this.listProcesos[objIndex].estado = 'Terminado';
            this.listProcesos[objIndex].idPlanManejoEstado =
              element.idPlanManejoEstado;
            this.listProcesos[objIndex].marcar = true;
          }
        });
        this.pendiente();
      },(err)=>{
        this.formularioStore.submitError(err);
      });
  }

  pendiente() {
    if (this.pendientes.length != 0) {
      this.verPOAC = true;
    } else {
      this.verPOAC = false;
    }
  }


  guardarEstados() {
    this.formularioStore.submit();
    let idPlanManejoAct = this.idPlanManejo;

    let listaEstadosPlanManejo: any[] = [];
    this.pendientes = [];

    this.listProcesos.forEach(function (element, index, array) {
      let idPlanManejoEstadoInpu = null;
      if (element.idPlanManejoEstado != 0) {
        idPlanManejoEstadoInpu = element.idPlanManejoEstado;
      }

      let obje = {
        codigo: element.codigo,
        codigoEstado: element.codigoEstado,
        codigoProceso: CodigoProceso.PLAN_OPERATIVO,
        descripcion: '',
        idPlanManejo: idPlanManejoAct,
        idPlanManejoEstado: idPlanManejoEstadoInpu,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
        idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario')).idusuario,
        observacion: '',
      };
      listaEstadosPlanManejo.push(obje);
    });

    this.cargaEnvioDocumentacionService
      .registrarEstadosPlanManejo(listaEstadosPlanManejo)
      .subscribe((response: any) => {
        if (response.success) {
          response.data.forEach((element: any) => {
            if (element.codigoEstado == 'CPMPEND') {
              this.pendientes.push(element);
            }
          });
          this.listarEstados();

          this.messageService.add({
            key: 'tl',
            severity: 'success',
            summary: '',
            detail:
              'Se registró los estados de la Declaración del Plan de Manejo correctamente.',
          });
          this.formularioStore.submitSuccess()
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: response.message,
          });
          this.formularioStore.submitError('')
        }
      },(error)=>{
        this.formularioStore.submitError(error)
      });
  }



  generarPOAC() {
    this.generarStore.submit();
    this.cargaArchivosService.consolidadoPOAC(this.idPlanManejo).subscribe(
      (res: any) => {
        if (res.success == true) {
          this.toast.ok(res?.message);
          this.consolidadoPOAC = res;
          this.verDescargaPOAC = false;
          descargarArchivo(this.consolidadoPOAC);
          this.generarStore.submitSuccess();
        } else {
          this.toast.error(res?.message);
          this.generarStore.submitError("");
        }
      },
      (err) => {
        this.toast.warn(
          "Para generar el consolidado del Plan Operativo POAC, debe tener todos los items completados previamente."
        );
        this.generarStore.submitError(err);
      }
    );
  }

  registrarArchivoId(event: any) {
    if (event) {
      this.verEnviar = true;
    }
  }

  registrarArchivoIdPF(event: any) {
    if (event) {
      this.verEnviar = true;
      this.disablePlanFormulado = false;
    }
  }


  /*enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }*/

  enviar() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEstado: "EMDPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  }

  /*   guardarPlanFormulado() {
    let params = {
      idPlanManejo: this.idPGMF,
      codigoEstado: "EPLMPRES",
      idUsuarioModificacion: this.user.idUsuario,
    };
    this.actualizarPlanManejoEstado(params);
  } */

  actualizarPlanManejoEstado(param: any) {
    this.ARFFSStore.submit();
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .actualizarPlanManejo(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.ARFFSStore.submitSuccess();
          this.toast.ok(response?.message);
          this.enviarARFFS = false
          this.disableEnviarARFFS = true;
          this.registrarNotificacion();
          this.editarInformacionGeneral();
          this.obtenerEstadoPlan();
        } else {
          this.toast.error(response?.message);
          this.obtenerEstadoPlan();
        }
      });
  }

  registrarNotificacion() {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: this.CodigoProceso.PLAN_OPERATIVO,
      numDocgestion: this.idPlanManejo,
      mensaje: 'Se informa que se ha registrado el plan '
        + this.CodigoProceso.PLAN_OPERATIVO
        + ' Nro. '
        + this.idPlanManejo
        + ' para evaluación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.user.idUsuario,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 10,
      url: '/planificacion/evaluacion/plan-operativo-ccnn',
      idUsuarioRegistro: this.user.idUsuario
    }
    this.notificacionService.registrarNotificacion(params).subscribe()
  }


  guardar() {
    this.iformacionStore.submit();
    
    var params = [{
      idPlanManejo: this.idPlanManejo,
      codigo: this.firma,
    }];
    this.informacionGeneralService
      .actualizarInfGeneralResumido(params)
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            this.iformacionStore.submitSuccess();
            this.toast.ok(response?.message);
          } else {
            this.iformacionStore.submitError("");
          }
        },
        (err) => {
          this.iformacionStore.submitError(err);
        }
      );
  }
  regresarTab() {
    this.regresar.emit();
  }
}
