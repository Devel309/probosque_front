import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CapacitacionModel } from 'src/app/model/Capacitacion';
import { ParametroModel } from 'src/app/model/Parametro';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CapacitacionDetalleModel } from 'src/app/model/CapacitacionDetalle';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import {
  CapacitacionPGMFA,
  ListCapacitacionDetalle,
} from 'src/app/model/capacitacionPGMFA';
import { UsuarioService } from '@services';
import { CapacitacionService } from 'src/app/service/planificacion/plan-general-manejo-pgmfa/capacitacion.service';
import { ToastService } from '@shared';
import { LineamientoInnerModel } from '../../../../../model/Comun/LineamientoInnerModel';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { finalize } from 'rxjs/operators';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { LOADIPHLPAPI } from 'dns';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tab-capacitacion',
  templateUrl: './tab-capacitacion.component.html',
  styleUrls: ['./tab-capacitacion.component.scss'],
})
export class TabCapacitacionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  tituloModal: string = 'Registrar Capacitación';
  displayBasic: boolean = false;

  capacitacion = {} as CapacitacionModel;
  indexCapacitacion: number = 0;
  capacitaciones: any[] = [];
  edit: boolean = false;
  parametro = {} as ParametroModel;
  tema = [] as any;
  temas = [] as any;
  usuario = {} as UsuarioModel;

  capacitacionObj: CapacitacionPGMFA = new CapacitacionPGMFA();
  listCapacitacion: CapacitacionPGMFA[] = [];
  detalleLista: ListCapacitacionDetalle[] = [];

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_10;

  codigoAcordeon10_1: string = CodigosTabEvaluacion.POAC_TAB_10_1;
  evaluacion10_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon10_1,
  });

  evaluacion: any;

  listParametro = [
    { valorPrimario: 'Presencial', valorSecundario: '1' },
    { valorPrimario: 'Virtual', valorSecundario: '2' },
  ];

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private capacitacionService: CapacitacionService,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listaCapacitacion();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  agregarParticipacion() {
    if (!this.validarCapacitacion()) {
      return;
    }
    if (this.edit) {
      this.editarCapacitacion();
    } else {
      this.registrarCapacitacion();
    }
  }

  listaCapacitacion() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoCapacitacion: 'POAC',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.capacitacionService
      .listarCapacitacion(params)
      .subscribe((response: any) => {
        this.capacitaciones = response.data;
        this.dialog.closeAll();
      });
  }

  validarCapacitacion(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.temas == null || this.temas.length == 0) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Tema o Actividad.\n';
    }
    if (
      this.capacitacion.personaCapacitar == null ||
      this.capacitacion.personaCapacitar == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Personal.\n';
    }
    if (
      this.capacitacion.idTipoModalidad == null ||
      this.capacitacion.idTipoModalidad == 0
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Modalidad.\n';
    }
    if (this.capacitacion.lugar == null || this.capacitacion.lugar == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar Lugar.\n';
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  abrirModal() {
    this.temas = [];
    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = true;
  }

  cerrarModal() {
    this.temas = [];
    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;
    this.edit = false;
    this.tituloModal = 'Registrar Capacitación';
  }

  registrarCapacitacion() {
    const obj = {} as any;
    obj.idCapacitacion = 0;
    obj.estado = 'A';
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.lugar = this.capacitacion.lugar;
    obj.personaCapacitar = this.capacitacion.personaCapacitar;

    obj.idTipoModalidad = this.capacitacion.idTipoModalidad;

    const listCapDet: CapacitacionDetalleModel[] = [];
    for (let item of this.temas) {
      const cap = {} as CapacitacionDetalleModel;
      cap.actividad = item.actividad;
      cap.idCapacitacionDet = 0;
      listCapDet.push(cap);
    }

    obj.listCapacitacionDetalle = listCapDet;
    obj.idPlanManejo = this.idPlanManejo;
    this.capacitaciones.push(obj);

    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;
    this.temas = [];
  }

  editarCapacitacion() {
    const obj = {} as any;
    obj.idCapacitacion = this.capacitacion.idCapacitacion;
    obj.estado = 'A';
    obj.idUsuarioRegistro = this.usuario.idusuario;
    obj.lugar = this.capacitacion.lugar;
    obj.personaCapacitar = this.capacitacion.personaCapacitar;
    obj.idTipoModalidad = this.capacitacion.idTipoModalidad;
    const listCapDet: CapacitacionDetalleModel[] = [];
    for (let item of this.temas) {
      const cap = {} as CapacitacionDetalleModel;
      cap.actividad = item.actividad;
      cap.idCapacitacionDet = item.idCapacitacionDet;
      listCapDet.push(cap);
    }

    obj.listCapacitacionDetalle = listCapDet;
    obj.idPlanManejo = this.idPlanManejo;
    this.capacitaciones[this.indexCapacitacion] = obj;

    this.capacitacion = {} as CapacitacionModel;
    this.parametro = {} as ParametroModel;
    this.displayBasic = false;

    this.displayBasic = false;
    this.tituloModal = 'Registrar Capacitación';
    this.edit = false;
    this.indexCapacitacion = 0;
    this.temas = [];
  }

  agregarActividad() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.tema == null || this.tema == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar el Tema o Actividad.\n';
    }
    if (!validar) {
      this.ErrorMensaje(mensaje);
    } else {
      const obj = {} as any;
      obj.actividad = this.tema;
      obj.idCapacitacionDet = 0;
      this.temas.push(obj);
      this.tema = '';
    }
  }

  guardarCapacitacion() {
    let validar: boolean = true;
    let mensaje: string = '';
    this.detalleLista = [];
    this.listCapacitacion = [];

    this.capacitaciones.map((response: any) => {
      const capacitacionObj = new CapacitacionPGMFA(response);
      capacitacionObj.codTipoCapacitacion = 'POAC';
      capacitacionObj.idUsuarioRegistro = this.user.idUsuario;
      capacitacionObj.idPlanManejo = this.idPlanManejo;
      capacitacionObj.idTipoModalidad =
        response.idTipoModalidad == 'Presencial'
          ? '1'
          : response.idTipoModalidad == 'Virtual'
          ? '2'
          : response.idTipoModalidad;

      this.detalleLista = [];

      response.listCapacitacionDetalle.map((item: any) => {
        let capacitacionDetalle = new ListCapacitacionDetalle(item);
        capacitacionDetalle.idUsuarioRegistro = this.user.idUsuario;
        this.detalleLista.push(capacitacionDetalle);
      });

      

      capacitacionObj.listCapacitacionDetalle = this.detalleLista;
      this.listCapacitacion.push(capacitacionObj);
    });

    if (this.capacitaciones == null || this.capacitaciones.length == 0) {
      validar = false;
      mensaje = mensaje += '(*) No hay Capacitaciones registradas.\n';
    }

    if (!validar) {
      this.ErrorMensaje(mensaje);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.capacitacionService
        .registrarCapacitacionDetalle(this.listCapacitacion)
        .subscribe(
          (result: any) => {
            this.dialog.closeAll();
            if (result.success) {
              this.listaCapacitacion();
              this.messageService.add({
                severity: 'success',
                summary: '',
                detail: result.message,
              });
            } else {
              this.messageService.add({
                severity: 'warn',
                summary: '',
                detail: result.message,
              });
            }
          },
          (error: HttpErrorResponse) => {
            this.dialog.closeAll();
          }
        );
    }
  }

  eliminarTema(event: Event, index: number, params: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (params.idCapacitacionDet != 0) {
          var parm = {
            idCapacitacionDet: params.idCapacitacionDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.capacitacionService
            .eliminarCapacitacionDetalle(parm)
            .subscribe((res: any) => {
              if (res.success == true) {
                this.temas.splice(index, 1);
                this.toast.ok('Se eliminó el tema correctamente');
              } else {
                this.toast.error(res?.message);
              }
            });
        } else {
          this.temas.splice(index, 1);
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  openEditarCapacitacion(obj: any, index: any) {
    const newObj = { ...obj };

    this.capacitacion.idCapacitacion = newObj.idCapacitacion;
    this.capacitacion.personaCapacitar = newObj.personaCapacitar;
    this.capacitacion.lugar = newObj.lugar;
    this.capacitacion.idTipoModalidad =
      newObj.idTipoModalidad == 'Presencial' ? '1' : '2';
    this.temas = newObj.listCapacitacionDetalle;
    this.capacitacion.idUsuarioRegistro = this.usuario.idusuario;
    this.capacitacion.estado = 'A';
    this.indexCapacitacion = index;

    this.displayBasic = true;
    this.edit = true;
    this.tituloModal = 'Editar Capacitacion';
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  openEliminarCapacitacion(event: Event, index: number, obj: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        if (obj.idCapacitacion != 0) {
          var parm = {
            idCapacitacion: obj.idCapacitacion,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.capacitacionService
            .eliminarCapacitacion(parm)
            .subscribe((res: any) => {
              if (res.success == true) {
                this.temas.splice(index, 1);
                this.toast.ok('Se eliminó la capacitacón correctamente');
                this.listaCapacitacion();
                this.dialog.closeAll();
              } else {
                this.toast.error(res?.message);
                this.dialog.closeAll();
              }
            });
        } else {
          this.capacitaciones.splice(index, 1);
        }
      },
      reject: () => {},
    });
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };
    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion10_1 = Object.assign(
                this.evaluacion10_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon10_1
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion10_1])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion10_1);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
