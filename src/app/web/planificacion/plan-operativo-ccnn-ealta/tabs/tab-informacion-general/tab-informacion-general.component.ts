import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { CoreCentralService } from "src/app/service/coreCentral.service";
import { ApiForestalService } from "src/app/service/api-forestal.service";

import { MessageService } from "primeng/api";
import { DepartamentoModel } from "../../../../../model/Departamento";
import { ProvinciaModel } from "../../../../../model/Provincia";
import { DistritoModel } from "../../../../../model/Distrito";
import { HttpErrorResponse } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { isNullOrEmpty, ToastService } from "@shared";
import { PlanManejoService, UsuarioService } from "@services";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { FileModel } from "src/app/model/util/File";
import { finalize } from "rxjs/operators";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ModalInfoPlanComponent } from 'src/app/shared/components/modal-info-plan/modal-info-plan.component';
import { PoacInformacionGeneral, RegenteForestal } from "src/app/model/poac-informacion-general";
import * as moment from "moment";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab-informacion-general",
  templateUrl: "./tab-informacion-general.component.html",
  styleUrls: ["./tab-informacion-general.component.scss"],
  providers: [MessageService],
})
export class TabInformacionGeneralComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  selectRegente: RegenteForestal = new RegenteForestal();
  nombreCompleto: string = "";
  verModalRegente: boolean = false;
  edit: boolean = false;

  departamento = {} as DepartamentoModel;
  listDepartamento: DepartamentoModel[] = [];
  provincia = {} as ProvinciaModel;
  listProvincia: ProvinciaModel[] = [];
  distrito = {} as DistritoModel;
  listDistrito: DistritoModel[] = [];

  first = 0;
  rows = 10;
  regentes: any[] = [];
  queryRegente: string = "";
  disabledDescarga: boolean = true;

  minDate = moment(new Date()).format('YYYY-MM-DD');
  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }
  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_1;
  codigoAcordeonAprovechamiento: string = CodigosTabEvaluacion.POAC_TAB_1_APROVECHAMIENTO;

  evaluacionDelAprovechamiento: EvaluacionArchivoModel = new EvaluacionArchivoModel(
    {
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonAprovechamiento,
    }
  );

  evaluacion: any;

  form: PoacInformacionGeneral = new PoacInformacionGeneral();
  ref!: DynamicDialogRef;
  idPlanManejoPadre: number = 0;
  idPlanManejoPadreTemp: number = 0;
  displayModal: boolean = false;
  nombreComunidad: string = '';
  nombreJefeComunidad: string = '';
  idInformacionGeneral: number = 0;

  constructor(
    private servCoreCentral: CoreCentralService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private apiForestal: ApiForestalService,
    private informacionGeneralService: InformacionGeneralService,
    private toast: ToastService,
    private user: UsuarioService,
    private evaluacionService: EvaluacionService,
    private dialogService: DialogService,
    private planManejoService: PlanManejoService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";
    this.obtenerPlanManejo();
    this.listarPorFiltroDepartamento();
    this.listarPorFilroProvincia();
    this.listarPorFilroDistrito();
    this.listarInformacionGeneral();
    // this.listarArchivoPMFI();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  // ------------- lo que esta funcionando -------------//

  obtenerPlanManejo() {
    var params = {
      idPlanManejo: this.idPlanManejo,
    };
    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((response: any) => {
        this.idPlanManejoPadre = response.data.idPlanManejoPadre ? response.data.idPlanManejoPadre : 0;
      });
  }

  listarInformacionGeneral() {
    this.edit = false;
    var params = {
      codigoProceso: CodigoProceso.PLAN_OPERATIVO,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejo,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.edit = true;
          response.data.forEach((element: any) => {
            this.form = new PoacInformacionGeneral(element);
            
            this.distrito.idDistrito = element.idDistrito;
            this.provincia.idProvincia = element.idProvincia;
            this.departamento.idDepartamento = element.idDepartamento;
            this.idInformacionGeneral = element.idInformacionGeneralDema;

            this.form.idDistritoRepresentante =  element.idDistrito;
            if (!isNullOrEmpty(element.fechaElaboracionPmfi)) {
              this.form.fechaElaboracionPmfi = new Date(element.fechaElaboracionPmfi);
            }
            element.nombreElaboraDema = !isNullOrEmpty(element.nombreElaboraDema) ? element.nombreElaboraDema : '';
            element.apellidoPaternoElaboraDema = !isNullOrEmpty(element.apellidoPaternoElaboraDema) ? element.apellidoPaternoElaboraDema : '';
            element.apellidoMaternoElaboraDema = !isNullOrEmpty(element.apellidoMaternoElaboraDema) ? element.apellidoMaternoElaboraDema : '';
            element.nombreRepresentante = !isNullOrEmpty(element.nombreRepresentante) ? element.nombreRepresentante : '';
            element.apellidoPaternoRepresentante = !isNullOrEmpty(element.apellidoPaternoRepresentante) ? element.apellidoPaternoRepresentante : '';
            element.apellidoMaternoRepresentante = !isNullOrEmpty(element.apellidoMaternoRepresentante) ? element.apellidoMaternoRepresentante : '';
            if (element.codTipoPersona === 'TPERJURI') {
              this.nombreComunidad = element.nombreElaboraDema;
              this.nombreJefeComunidad = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;
            } else {
              this.nombreJefeComunidad = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaboraDema + ' ' + element.apellidoMaternoElaboraDema;
            }

            if (!isNullOrEmpty(element.regente)) {
              this.nombreCompleto = element.regente.nombres + ' ' + element.regente.apellidos;
            }
          });
        }
      });
  }

  openModal() {
    this.ref = this.dialogService.open(ModalInfoPlanComponent, {
      header: "Buscar Plan Manejo " + CodigoProceso.PLAN_GENERAL,
      width: "60%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        buscar: true,
        codTipoPlan: CodigoProceso.PLAN_GENERAL,
        nroDocumento: this.user.nroDocumento,
        idTipoDocumento: this.user.idtipoDocumento,
        idPlanManejo: this.idPlanManejo,
        idPlanManejoPadre: this.idPlanManejoPadre,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.displayModal = true;
        this.idPlanManejoPadreTemp = resp.idPlanManejo;
      }
    });
  }

  closeModal() {
    this.displayModal = false;
    this.listarInformacionGeneralPrevio();
  }

  listarInformacionGeneralPrevio() {
    var params = {
      codigoProceso: CodigoProceso.PLAN_GENERAL,
      idInformacionGeneralDema: null,
      idPlanManejo: this.idPlanManejoPadreTemp,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarInformacionGeneralDema(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any) => {            
            this.form = new PoacInformacionGeneral(element);
            this.distrito.idDistrito = element.idDistrito;
            this.provincia.idProvincia = element.idProvincia;
            this.departamento.idDepartamento = element.idDepartamento;
            
            this.form.areaTotal = element.superficieHaMaderables;
            this.form.idDistritoRepresentante =  element.idDistrito;
            this.form.superficieHaMaderables = null;
            this.form.vigencia = 0;
            this.form.observacion = '';
            this.form.fechaElaboracionPmfi = null;
            this.form.federacionComunidad = null;
            this.form.regente = new RegenteForestal();
            element.nombreElaboraDema = !isNullOrEmpty(element.nombreElaboraDema) ? element.nombreElaboraDema : '';
            element.apellidoPaternoElaboraDema = !isNullOrEmpty(element.apellidoPaternoElaboraDema) ? element.apellidoPaternoElaboraDema : '';
            element.apellidoMaternoElaboraDema = !isNullOrEmpty(element.apellidoMaternoElaboraDema) ? element.apellidoMaternoElaboraDema : '';
            element.nombreRepresentante = !isNullOrEmpty(element.nombreRepresentante) ? element.nombreRepresentante : '';
            element.apellidoPaternoRepresentante = !isNullOrEmpty(element.apellidoPaternoRepresentante) ? element.apellidoPaternoRepresentante : '';
            element.apellidoMaternoRepresentante = !isNullOrEmpty(element.apellidoMaternoRepresentante) ? element.apellidoMaternoRepresentante : '';
            if (element.codTipoPersona === 'TPERJURI') {
              this.nombreComunidad = element.nombreElaboraDema;
              this.nombreJefeComunidad = element.nombreRepresentante + ' ' + element.apellidoPaternoRepresentante + ' ' + element.apellidoMaternoRepresentante;
            } else {
              this.nombreJefeComunidad = element.nombreElaboraDema + ' ' + element.apellidoPaternoElaboraDema + ' ' + element.apellidoMaternoElaboraDema;
            }
          });
        }
      });
  }

  abrirModalRegentes() {
    this.listarRegentes();
    this.selectRegente = new RegenteForestal();
    this.verModalRegente = true;
    this.queryRegente = "";
  }

  listarRegentes() {
    this.apiForestal.consultarRegente().subscribe((result: any) => {
      result.dataService.forEach((element: any) => {        
        this.regentes.push({
          apellidos: element.apellidos,
          estado: element.estado,
          nombres: element.nombres,
          numeroDocumento: element.numeroDocumento,
          numeroLicencia: element.numeroLicencia,
          periodo: element.periodo,
        });
      });
    });
  }

  filtrarRegente() {
    if (this.queryRegente) {
      this.regentes = this.regentes.filter((r) =>
        r.numeroDocumento
          .toLowerCase()
          .includes(this.queryRegente.toLowerCase())
      );
    } else {
      this.listarRegentes();
    }
  }

  guardarRegente() {
    this.nombreCompleto = this.selectRegente.nombres + ' ' + this.selectRegente.apellidos;
    this.form.regente = new RegenteForestal(this.selectRegente);
    this.form.regente.idPlanManejo = this.idPlanManejo;
    this.form.regente.idUsuarioRegistro = this.user.idUsuario;
    this.verModalRegente = false;
  }

  listarPorFiltroDepartamento() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia() {
    this.servCoreCentral.listarPorFilroProvincia({}).subscribe(
      (result: any) => {
        this.listProvincia = result.data;
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  listarPorFilroDistrito() {
    this.servCoreCentral.listarPorFilroDistrito({}).subscribe(
      (result: any) => {
        this.listDistrito = result.data;
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
      }
    );
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  Distrito(param: any) {
    this.form.idDistritoRepresentante = param.value;
  }

  registrarInformacionGeneral() {
    if (this.validationFields(this.form)) {
      this.form.fechaElaboracionPmfi = new Date(this.form.fechaElaboracionPmfi);

      var params = new PoacInformacionGeneral(this.form);
      params.idPlanManejo = this.idPlanManejo;
      params.idUsuarioRegistro = this.user.idUsuario;
      params.codigoProceso = CodigoProceso.PLAN_OPERATIVO;
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .registrarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.SuccessMensaje(response.message);
            this.guardarPlanManejo();
            this.listarInformacionGeneral();
          } else this.ErrorMensaje(response.message);
        });
    }
  }

  validationFields(data: PoacInformacionGeneral) {
    let validar: boolean = true;
    let mensaje: string = "";
    
    if (!isNullOrEmpty(data.codTipoPersona)) {
      if (data.codTipoPersona === 'TPERJURI') {
        if (!this.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Nombre de la Comunidad.\n";
        }
      }
    } else {
      if (!this.nombreJefeComunidad) {
        if (!this.nombreComunidad) {
          validar = false;
          mensaje = mensaje += "(*) Debe ingresar: Nombre de la Comunidad.\n";
        }
      }
    }
    if (!this.nombreJefeComunidad) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Nombre del Jefe comunal o representante legal.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  guardarPlanManejo() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      idUsuarioModificacion: this.user.idUsuario,
      idPlanManejoPadre: this.idPlanManejoPadreTemp,
    };
    this.evaluacionService
      .actualizarPlanManejo(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  editarInformacionGeneral() {
    if (this.validationFields(this.form)) {
      this.form.fechaElaboracionPmfi = new Date(this.form.fechaElaboracionPmfi);

      var params = new PoacInformacionGeneral(this.form);
      params.idPlanManejo = this.idPlanManejo;
      params.idUsuarioRegistro = this.user.idUsuario;
      params.codigoProceso = "POAC";
      params.idInformacionGeneralDema = this.idInformacionGeneral;

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionGeneralService
        .actualizarInformacionGeneralDema(params)
        .subscribe((response: any) => {
          if (response.success) {
            this.dialog.closeAll();
            this.SuccessMensaje(response.message);
            this.listarInformacionGeneral();
          } else this.ErrorMensaje(response.message);
        });
    }
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  /* handleAdmDateChange() {
    this.validarInformacion();
    let fechaInicial = new Date(this.resumenEjecutivoObj.fechaInicioDema);
    let fechaFinal = new Date(this.resumenEjecutivoObj.fechaFinDema);
    var anios = this.calcDate(fechaInicial, fechaFinal);
    this.resumenEjecutivoObj.vigencia = anios;
  } */
  calcDate(date1: Date, date2: Date) {
    var months;
    months = (date2.getFullYear() - date1.getFullYear()) * 12;
    months -= date1.getMonth();
    months += date2.getMonth();
    months <= 0 ? 0 : months;
    var years = Math.floor(months / 12);
    return years;
  }

  /*   validarInformacion() {
    let fechaActual = new Date();
    let fechaInicio = new Date(this.resumenEjecutivoObj.fechaInicioDema);
    if (
      this.resumenEjecutivoObj.fechaInicioDema &&
      this.resumenEjecutivoObj.fechaFinDema
    ) {
      if (fechaInicio < fechaActual) {
        this.toast.error(
          "La Fecha Inicio debe ser posterior a la Fecha Actual"
        );
        return;
      }

      if (
        this.resumenEjecutivoObj.fechaFinDema <=
        this.resumenEjecutivoObj.fechaInicioDema
      ) {
        this.toast.error("La Fecha Final debe ser posterior a la Fecha Inicio");
        return;
      }
    } else if (fechaInicio < fechaActual) {
      this.toast.error("La Fecha Inicio debe ser posterior a la Fecha Actual");
      return;
    }
  } */

  getFormattedDate(date: any) {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, "0");
    let day = date.getDate().toString().padStart(2, "0");

    return day + "/" + month + "/" + year;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionDelAprovechamiento = Object.assign(
                this.evaluacionDelAprovechamiento,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost ==
                    this.codigoAcordeonAprovechamiento
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacionDelAprovechamiento])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionDelAprovechamiento
        );
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
