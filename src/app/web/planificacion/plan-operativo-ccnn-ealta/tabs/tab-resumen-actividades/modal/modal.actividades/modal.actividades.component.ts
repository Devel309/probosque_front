import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { PlanOPResumenActDetModel } from 'src/app/model/planOperativo';
import { ResumenActDetModelPOAC } from 'src/app/model/poac-resumen-actividades';
import { UtilitariosService } from 'src/app/model/util/util.service';

@Component({
  selector: 'app-modal.actividades',
  templateUrl: './modal.actividades.component.html',
  styleUrls: ['./modal.actividades.component.scss'],
})
export class ModalActividadesComponent implements OnInit {
  form = new ResumenActDetModelPOAC();

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    if (this.config.data.edit == true) {
      
      this.form.actividad = this.config.data.item.actividad;
      this.form.indicador = this.config.data.item.indicador;
      this.form.programado = this.config.data.item.programado;
      this.form.realizado = this.config.data.item.realizado;
    }
  }

  validar() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.form.actividad === '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: campo actividad.\n';
    }

    if (this.form.realizado === '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: campo indicadores.\n';
    }

    if (this.form.indicador === '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: campo programado.\n';
    }

    if (this.form.programado === '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: campo realizado.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  agregar() {
    if (!this.validar()) {
      return;
    }
    return this.ref.close(this.form);
  }

  cerrarModal() {
    this.ref.close();
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}
