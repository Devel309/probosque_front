import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ResumenActDetModelPOAC } from 'src/app/model/poac-resumen-actividades';

@Component({
  selector: 'app-modal-recomendaciones',
  templateUrl: './modal-recomendaciones.component.html',
  styleUrls: ['./modal-recomendaciones.component.scss'],
})
export class ModalRecomendacionesComponent implements OnInit {
  form = new ResumenActDetModelPOAC();
  objectRecomendaciones: any = {};

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.form.tipoResumen = this.config.data.item.tipoResumen;
    if (this.config.data.edit == true) {
      this.form.resumen = this.config.data.item.data.resumen;
    }
  }

  validar() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.form.resumen === '') {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: campo descripción.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  agregar() {
    if (!this.validar()) {
      return;
    }
    return this.ref.close(this.form);
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  cancelar() {
    this.ref.close();
  }

}
