import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import {
  CodigosResumenActividadesPOAC,
  ResumenActDetModelPOAC,
  ResumenActModelPOAC,
} from 'src/app/model/poac-resumen-actividades';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { ModalRecomendacionesComponent } from './modal/modal-recomendaciones/modal-recomendaciones.component';
import { ModalActividadesComponent } from './modal/modal.actividades/modal.actividades.component';

@Component({
  selector: 'app-resumen-actividades',
  templateUrl: './resumen-actividades.component.html',
  styleUrls: ['./resumen-actividades.component.scss'],
})
export class TabResumenActividadesComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;

  ref: DynamicDialogRef = new DynamicDialogRef();

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  displayBasic: boolean = false;
  edit: boolean = false;

  codigosResumenActividadesPOAC = CodigosResumenActividadesPOAC;

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_2;

  codigoAcordeon2_1: string = CodigosTabEvaluacion.POAC_TAB_2_21;
  codigoAcordeon2_2: string = CodigosTabEvaluacion.POAC_TAB_2_22;

  evaluacion2_1: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon2_1,
  });

  evaluacion2_2: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon2_2,
  });

  evaluacion: any;

  listActividades: ResumenActDetModelPOAC[] = [];
  lstBalance: ResumenActDetModelPOAC[] = [];
  resumenAct: ResumenActModelPOAC = new ResumenActModelPOAC();
  recomendacion: ResumenActModelPOAC = new ResumenActModelPOAC();
  dataContext: ResumenActModelPOAC[] = [];

  constructor(
    private confirmationService: ConfirmationService,
    private planificacionService: PlanificacionService,
    public dialogService: DialogService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listaResumenDetalle();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  agregarData(mensaje: any, edit: boolean, tipoResumen: number, data?: any) {
    this.ref = this.dialogService.open(ModalRecomendacionesComponent, {
      header: mensaje,
      width: '30%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        edit: edit,
        item: {
          data: data,
          tipoResumen: tipoResumen,
        },
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && edit) {
        data.resumen = resp.resumen;
        data.edit = false;
      } else if (resp) {
        this.lstBalance.push(resp);
      }
    });
  }

  abrirModalActividades(mensaje: any, edit: boolean, actividad?: any) {
    this.ref = this.dialogService.open(ModalActividadesComponent, {
      header: mensaje,
      width: '40%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        edit: edit,
        item: actividad,
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp && mensaje == 'Editar Actividades') {
        actividad.actividad = resp.actividad;
        actividad.indicador = resp.indicador;
        actividad.programado = resp.programado;
        actividad.realizado = resp.realizado;
        actividad.editarForm = false;
      } else if (resp) {
        this.listActividades.push(resp);
      }
    });
  }
  eliminar(
    event: any,
    data: ResumenActDetModelPOAC,
    index: number,
    codigo: string
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        var params = {
          idResumenActDet: data.idResumenActDet,
          idUsuarioElimina: this.usuarioService.idUsuario,
        };

        if (data.idResumenActDet > 0) {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.planificacionService.eliminarResumenDetalle(params).subscribe(
            (response: any) => {
              if (response.success == true) {
                // this.successMensaje(response.message);
                // this.listaResumenDetalle();
                if (codigo == CodigosResumenActividadesPOAC.ACORDEON_2_1) {
                  this.listActividades.splice(index, 1);
                  this.successMensaje(
                    'Se eliminó registro de Resumen de Actividades correctamente.'
                  );
                } else {
                  this.lstBalance.splice(index, 1);
                  this.successMensaje(
                    'Se eliminó registro de Recomendaciones del POA anterior correctamente.'
                  );
                }
              } else {
                this.errorMensaje(response.message);
              }
              this.dialog.closeAll();
            },
            (error) => {
              this.dialog.closeAll();
              this.errorMensaje(error);
            }
          );
        } else {
          if (codigo == CodigosResumenActividadesPOAC.ACORDEON_2_1) {
            this.listActividades.splice(index, 1);
          } else {
            this.lstBalance.splice(index, 1);
          }
        }
      },
      reject: () => {
        //reject action
      },
    });
  }

  listaResumenDetalle() {
    this.lstBalance = [];
    this.listActividades = [];

    var params = {
      idPlanManejo: this.idPlanManejo,
      codTipoResumen: 'POAC',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService.listarResumenDetallePOAC(params).subscribe(
      (response: any) => {
        if (response.success == true) {
          response.data.forEach((item: any) => {
            if (
              item.codigoSubResumen ==
                CodigosResumenActividadesPOAC.ACORDEON_2_1 ||
              item.codTipoResumen == null
            ) {
              this.resumenAct = new ResumenActModelPOAC(item);

              item.listResumenActividadDetalle.map((data: any) => {
                const obj = new ResumenActDetModelPOAC(data);
                obj.idResumenActDet =
                  this.resumenAct.idResumenAct != 0 ? obj.idResumenActDet : 0;
                this.listActividades.push(obj);
              });
            } else {
              this.recomendacion = new ResumenActModelPOAC(item);

              item.listResumenActividadDetalle.map((data: any) => {
                const obj = new ResumenActDetModelPOAC(data);
                this.lstBalance.push(obj);
              });
            }
          });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        this.errorMensaje(error);
      }
    );
  }

  successMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'warn',
      summary: '',
      detail: mensaje,
    });
  }

  guardar() {
    this.dataContext = [];

    this.listActividades.forEach((item: ResumenActDetModelPOAC) => {
      item.idUsuarioRegistro = this.usuarioService.idUsuario;
    });
    this.resumenAct.idPlanManejo = this.idPlanManejo;
    this.resumenAct.codigoSubResumen =
      CodigosResumenActividadesPOAC.ACORDEON_2_1;
    this.resumenAct.listResumenEvaluacion = this.listActividades;

    this.dataContext.push(this.resumenAct);

    this.lstBalance.forEach((item: ResumenActDetModelPOAC) => {
      item.idUsuarioRegistro = this.usuarioService.idUsuario;
    });

    this.recomendacion.listResumenEvaluacion = this.lstBalance;
    this.recomendacion.codigoSubResumen =
      CodigosResumenActividadesPOAC.ACORDEON_2_2;
    this.recomendacion.idPlanManejo = this.idPlanManejo;
    this.dataContext.push(this.recomendacion);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService
      .registrarResumenDetalle(this.dataContext)
      .subscribe(
        (response: any) => {
          if (response.success == true) {
            this.successMensaje(response.message);

            this.listaResumenDetalle();
          } else {
            this.errorMensaje(response.message);
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          this.errorMensaje(error);
        }
      );
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion2_1 = Object.assign(
                this.evaluacion2_1,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon2_1
                )
              );
              this.evaluacion2_2 = Object.assign(
                this.evaluacion2_2,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon2_2
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacion2_1, this.evaluacion2_2])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion2_1);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion2_2);

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }
  retornarFlujoEvaluacion(){

    localStorage.setItem('EvalResuDet', JSON.stringify({
      tab:"RFNM",
      acordeon:"LINEADP6211"
    }));

    this.router.navigateByUrl("/planificacion/evaluacion/requisitos-previos-poac/"+this.idPlanManejo+"/"+this.codigoProceso);
  }
}
