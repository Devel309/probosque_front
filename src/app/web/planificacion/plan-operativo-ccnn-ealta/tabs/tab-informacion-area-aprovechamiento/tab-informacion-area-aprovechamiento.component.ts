import { HttpErrorResponse } from "@angular/common/http";
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { DepartamentoModel, DistritoModel, ProvinciaModel } from "@models";
import {
  ArchivoService,
  CoreCentralService,
  PlanManejoService,
  UsuarioService,
} from "@services";
import {
  HidrografiaTipo,
  ToastService,
  isNullOrEmpty,
  MapApi,
  DownloadFile,
  Base64toBlob,
} from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { forkJoin, from } from "rxjs";
import { concatMap, finalize, tap } from "rxjs/operators";
import { InformacionBasicaDetalleModel } from "src/app/model/PlanManejo/InformacionBasica/InformacionBasicaDetalleModel";
import { InformacionBasicaModel } from "src/app/model/PlanManejo/InformacionBasica/InformacionBasicaModel";
import { PlanManejoGeometriaModel } from "src/app/model/PlanManejo/PlanManejoGeometria";
import { CustomCapaModel } from "src/app/model/util/CustomCapa";
import { FileModel } from "src/app/model/util/File";
import { ApiForestalService } from "src/app/service/api-forestal.service";
import { ApiGeoforestalService } from "src/app/service/api-geoforestal.service";
import { PlanManejoGeometriaService } from "src/app/service/plan-manejo-geometria.service";
import { InformacionBasicaSOPCMervice } from "src/app/service/plan-operativo-concesion-maderable/informacion-basica.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { LoadingComponent } from "../../../../../components/loading/loading.component";
import { EvaluacionArchivoModel } from "../../../../../model/Comun/EvaluacionArchivoModel";
import { InformacionBasicaAreaAprovechamientoPOAC } from "../../../../../model/InformacionBasicaAreaAprovechamientoPOAC";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { EvaluacionUtils } from "../../../../../model/util/EvaluacionUtils";
import { Mensajes } from "../../../../../model/util/Mensajes";
import { EvaluacionService } from "../../../../../service/evaluacion/evaluacion.service";

declare const shp: any;

@Component({
  selector: "app-tab-informacion-area-aprovechamiento",
  templateUrl: "./tab-informacion-area-aprovechamiento.component.html",
  styleUrls: ["./tab-informacion-area-aprovechamiento.component.scss"],
  providers: [MessageService],
})
export class TabInformacionAreaAprovechamientoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() isPerfilArffs!: boolean;
  @Input() disabled!: boolean;
  @Input() idPlanManejoPadre!: number;

  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @ViewChild("mapTB", { static: true }) private mapViewElTB!: ElementRef;
  @ViewChild("mapAC", { static: true }) private mapViewElAC!: ElementRef;
  @ViewChild("ulResult", { static: true }) private ulResult!: ElementRef;
  @ViewChild("tblVertices", { static: true }) private tblVertices!: ElementRef;
  @ViewChild("fileInput", { static: true }) private fileInput!: ElementRef;

  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();

  //inicio map
  view: any = null;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  _layer: any;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  isPadreVertice: boolean = false;
  newSPVertice: boolean = false;
  deleteVertice: boolean = false;
  idArchivoVertice: number = 0;
  //fin map
  //inicio map TB
  viewTB: any = null;
  _idTB = this.mapApi.Guid2.newGuid;
  fileTB: FileModel = {} as FileModel;
  _filesSHPTB: FileModel[] = [];
  _layersTB: CustomCapaModel[] = [];
  _layerTB: any;
  listQuinquenal: any = [];
  layerInternal: any = [];
  geoJsonInternal: any = [];
  newLayerInternal: any = [];
  newGeojsonInternal: any = [];
  planManejoGeometriaTB: PlanManejoGeometriaModel[] = [];
  isPadreTB: boolean = false;
  newSPTB: boolean = false;
  deleteTB: boolean = false;
  idArchivoTB: number = 0;
  //fin map TB
  //inicio map AC
  viewAC: any = null;
  _idAC = this.mapApi.Guid2.newGuid;
  fileAC: FileModel = {} as FileModel;
  _filesSHPAC: FileModel[] = [];
  _layersAC: CustomCapaModel[] = [];
  _layerAC: any;
  idArchivoAC: number = 0;
  //fin map AC

  listActividadesEconomicas: any[] = [
    { valorPrimario: "Agricultura" },
    { valorPrimario: "Ganaderia" },
    { valorPrimario: "Caza" },
    { valorPrimario: "Refinamiento" },
    { valorPrimario: "Pezca" },
    { valorPrimario: "Elaboracion de artesanias" },
    { valorPrimario: "Extraccion de productos no maderables" },
  ];

  listInfraestructura: any[] = [
    { valorPrimario: "Escuela Primaria" },
    { valorPrimario: "Escuela Secundaria" },
    { valorPrimario: "Centro de Salud" },
    { valorPrimario: "Posta medica" },
    { valorPrimario: "Aserradero" },
    { valorPrimario: "Energia electrica" },
  ];

  listActividadesBosque: any[] = [
    { valorPrimario: "Extracción de madera comercial" },
    { valorPrimario: "Extracción de madera redonda, leña y carbon" },
    { valorPrimario: "Extracción de productos no maderables" },
    { valorPrimario: "Caza o captura de Fauna Silvestre" },
  ];

  listConflictosForestales: any[] = [];
  displayModalConflictos: boolean = false;
  editConflictos: boolean = false;
  conflicto: any = {};

  listFaunaSilvestre: any[] = [];
  displayModalFauna: boolean = false;
  editFauna: boolean = false;
  fauna: any = {};

  listHidroFisio: any[] = [];

  codigoProceso = CodigoProceso.PLAN_OPERATIVO;
  codigoTab = CodigosTabEvaluacion.POAC_TAB_4;

  codigoAcordeon4_41: string = CodigosTabEvaluacion.POAC_TAB_4_41;
  codigoAcordeon4_42: string = CodigosTabEvaluacion.POAC_TAB_4_42;
  codigoAcordeon4_43: string = CodigosTabEvaluacion.POAC_TAB_4_43;
  codigoAcordeon4_44: string = CodigosTabEvaluacion.POAC_TAB_4_44;

  evaluacion4_41: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon4_41,
  });

  evaluacion4_42: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon4_42,
  });

  evaluacion4_43: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon4_43,
  });

  evaluacion4_44: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeon4_44,
  });

  evaluacion: any;

  rios: any[] = [];

  codigoTabla1 = "TAB_1";

  informacionBasica4_1_1: InformacionBasicaModel = {} as InformacionBasicaModel;
  informacionBasica4_1_2: InformacionBasicaModel = {} as InformacionBasicaModel;
  informacionBasica4_2: InformacionBasicaModel = {} as InformacionBasicaModel;
  informacionBasica4_3: InformacionBasicaModel = {} as InformacionBasicaModel;
  informacionBasica4_4: InformacionBasicaModel = {} as InformacionBasicaModel;

  informacionBasicaDet4_1_1: InformacionBasicaDetalleModel = {} as InformacionBasicaDetalleModel;
  informacionBasicaDet4_1_2: InformacionBasicaDetalleModel = {} as InformacionBasicaDetalleModel;
  informacionBasicaDet4_2: any[] = [];
  informacionBasicaDet4_3: InformacionBasicaDetalleModel[] = [];
  informacionBasicaDet4_4: InformacionBasicaDetalleModel = {} as InformacionBasicaDetalleModel;

  totalAreaBosque: number = 0;
  totalPorcentajeBosque: number = 0;
  ampliarAnexo4_3: boolean = false;

  listDepartamento: DepartamentoModel[] = [];
  departamento = {} as DepartamentoModel;

  listProvincia: ProvinciaModel[] = [];
  provincia = {} as ProvinciaModel;

  listDistrito: DistritoModel[] = [];
  distrito = {} as DistritoModel;

  departamentoSeleccionado: any;
  provinciaSeleccionado: any;
  distritoSeleccionado: any;

  selectSistemaTransporteMadera: string = "";
  selectMedioTransportePCA: string = "";

  listCuenca: any[] = [];
  listSubcuenca: any[] = [];
  listCuencaSubcuenca: CuencaSubcuencaModel[] = [];
  idCuenca!: number | null;
  idSubcuenca!: number | null;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private servCoreCentral: CoreCentralService,
    private informacionBasicaSOPCMervice: InformacionBasicaSOPCMervice,
    private usuarioService: UsuarioService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private serviceGeoforestal: ApiGeoforestalService,
    private serviceExternos: ApiForestalService,
    private router: Router,
    private planManejoService: PlanManejoService
  ) {}

  ngOnInit(): void {
    // this.domConfig();
    this.obtenerPlan();
    this.initializeMap();
    this.initializeMapTB();
    this.initializeMapAC();
    this.obtenerCapasAC();
    this.listarPorFiltroDepartamentoIni();
    this.buscarHidrografia(HidrografiaTipo.RIO);
    this.listarCuencaSubcuenca();
    this.listarInfoBasicaAreaAprovechamiento4_1_1();
    this.listarInfoBasicaAreaAprovechamiento4_1_2();
    this.listarInfoBasicaAreaAprovechamiento4_2();
    this.listarInfoBasicaAreaAprovechamiento4_3();
    this.listarInfoBasicaAreaAprovechamiento4_4();

    if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    };

    this.planManejoService
      .obtenerPlanManejo(params)
      .subscribe((result: any) => {
        this.idPlanManejoPadre = result.data.idPlanManejoPadre;
      });
  }

  buscarHidrografia(tipo: HidrografiaTipo) {
    this.informacionBasicaSOPCMervice
      .obtenerHidrografia(0, tipo)
      .subscribe((response: any) => {
        this.rios = [...response.data];
      });
  }

  listarPorFiltroDepartamentoIni() {
    this.servCoreCentral.listarPorFiltroDepartamento({}).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;

        this.listarPorFilroProvincia({});
      },
      (error: any) => {
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
        this.listarPorFilroProvincia(this.provincia);
      },
      (error: any) => {
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  listarPorFilroProvincia(provincia: ProvinciaModel | any) {
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
        this.listarPorFilroDistrito(this.distrito);
      });
  }

  listarPorFilroDistrito(distrito: DistritoModel) {
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  listarCuencaSubcuenca() {
    this.listCuencaSubcuenca = [];
    this.listCuenca = [];

    this.servCoreCentral.listarCuencaSubcuenca().subscribe(
      (result: any) => {
        this.listCuencaSubcuenca = result.data;

        let listC: any[] = [];
        this.listCuencaSubcuenca.forEach((item) => {
          listC.push(item);
        });

        const setObj = new Map();
        const unicos = listC.reduce((arr, cuenca) => {
          if (!setObj.has(cuenca.idCuenca)) {
            setObj.set(cuenca.idCuenca, cuenca);
            arr.push(cuenca);
          }
          return arr;
        }, []);

        this.listCuenca = unicos;
        this.listCuenca.sort((a, b) => a.cuenca - b.cuenca);

        if (!isNullOrEmpty(this.idCuenca) && !isNullOrEmpty(this.idSubcuenca)) {
          this.onSelectedCuenca({ value: Number(this.idCuenca) });
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.messageService.add({
          severity: "warn",
          summary: "",
          detail: error.message,
        });
      }
    );
  }

  onSelectedCuenca(param: any) {
    this.listSubcuenca = [];

    this.listCuencaSubcuenca.forEach((item) => {
      if (item.idCuenca == param.value) {
        this.listSubcuenca.push(item);
      }
    });
  }

  AgregarActividadEconomica() {
    this.listActividadesEconomicas.push({
      valorPrimario: "Otros (especificar)",
      valorSecundario: 8,
      accion: true,
    });
  }

  AgregarInfraestructura() {
    this.listInfraestructura.push({
      valorPrimario: "Otros (radiofonia,telefonia,etc.)",
      valorSecundario: 8,
      accion: true,
    });
  }

  agregarConflicto() {
    this.listConflictosForestales.push(this.conflicto);
    this.displayModalConflictos = false;
  }

  abrirModalConflicto() {
    this.conflicto = {};
    this.editConflictos = false;
    this.displayModalConflictos = true;
  }

  cerrarModalConflicto() {
    this.displayModalConflictos = false;
  }

  openEditarConflicto(data: any) {
    this.editConflictos = true;
    this.conflicto = data;
    this.displayModalConflictos = true;
  }

  openEliminarConflicto(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Esta seguro de querer eliminar esta acción?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        this.listConflictosForestales.splice(index, 1);
      },
      reject: () => {
        //reject action
      },
    });
  }

  agregarFauna() {
    this.listFaunaSilvestre.push(this.fauna);
    this.displayModalFauna = false;
  }

  abrirModalFauna() {
    this.fauna = {};
    this.editFauna = false;
    this.displayModalFauna = true;
  }

  cerrarModalFauna() {
    this.displayModalFauna = false;
  }

  openEditarFauna(data: any) {
    this.editFauna = true;
    this.fauna = data;
    this.displayModalFauna = true;
  }

  openEliminarFauna(event: Event, index: number, data: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Esta seguro de querer eliminar esta acción?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        this.listFaunaSilvestre.splice(index, 1);
      },
      reject: () => {
        //reject action
      },
    });
  }

  agregarHidroFisio() {
    this.listHidroFisio.push({});
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacion4_41 = Object.assign(
                this.evaluacion4_41,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon4_41
                )
              );
              this.evaluacion4_42 = Object.assign(
                this.evaluacion4_42,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon4_42
                )
              );
              this.evaluacion4_43 = Object.assign(
                this.evaluacion4_43,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon4_43
                )
              );
              this.evaluacion4_44 = Object.assign(
                this.evaluacion4_44,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeon4_44
                )
              );
            }
          }
        }
      });
  }

  registrarEvaluacion() {
    if (
      EvaluacionUtils.validar([
        this.evaluacion4_41,
        this.evaluacion4_42,
        this.evaluacion4_43,
        this.evaluacion4_44,
      ])
    ) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion4_41);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion4_42);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion4_43);
        this.evaluacion.listarEvaluacionDetalle.push(this.evaluacion4_44);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }
  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }

  validacionValoresUbicacionExtension() {
    let esValido = true;
    let mensaje = "";

    if (this.informacionBasica4_1_1.comunidad == null) {
      mensaje += "(*) Debe ingresar número.\n";
      esValido = false;
    }
    if (this.informacionBasica4_1_1.areaTotal == null) {
      mensaje += "(*) Debe ingresar área total (ha).\n";
      esValido = false;
    }
    if (this.provinciaSeleccionado == null) {
      mensaje += "(*) Debe ingresar provincia.\n";
      esValido = false;
    }
    if (this.departamentoSeleccionado == null) {
      mensaje += "(*) Debe ingresar departamento.\n";
      esValido = false;
    }
    if (this.distritoSeleccionado == null) {
      mensaje += "(*) Debe ingresar distrito.\n";
      esValido = false;
    }

    if (!esValido) this.errorMessage(mensaje);

    return esValido;
  }

  errorMessage(message: string) {
    this.messageService.add({ severity: "warn", summary: "", detail: message });
  }

  successMensaje = (message: string) => {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: message,
    });
  };

  registrarInfoBasicaAreaAprovechamiento() {
    const array = [];

    if (!this.validacionValoresUbicacionExtension()) return;

    const newAccordeon4_1 = new InformacionBasicaModel(
      this.informacionBasica4_1_1
    );
    newAccordeon4_1.codInfBasica =
      InformacionBasicaAreaAprovechamientoPOAC.CODIGO_MANEJO;
    newAccordeon4_1.idPlanManejo = this.idPlanManejo;
    newAccordeon4_1.codSubInfBasica =
      InformacionBasicaAreaAprovechamientoPOAC.ACORDEON4_1;
    newAccordeon4_1.idUsuarioRegistro = this.usuarioService.idUsuario;
    newAccordeon4_1.idProvincia = this.provinciaSeleccionado;
    newAccordeon4_1.idDepartamento = this.departamentoSeleccionado;
    newAccordeon4_1.idDistrito = this.distritoSeleccionado;

    const newAccordeon4_4 = new InformacionBasicaModel();

    newAccordeon4_4.idPlanManejo = this.idPlanManejo;
    newAccordeon4_4.codInfBasica =
      InformacionBasicaAreaAprovechamientoPOAC.CODIGO_MANEJO;
    newAccordeon4_4.codSubInfBasica =
      InformacionBasicaAreaAprovechamientoPOAC.ACORDEON4_4;
    newAccordeon4_4.idUsuarioRegistro = this.usuarioService.idUsuario;

    const informaciondet = new InformacionBasicaDetalleModel(
      this.informacionBasicaDet4_4
    );
    // informaciondet.codSubInfBasicaDet =  InformacionBasicaAreaAprovechamientoPOAC.ACORDEON4_4;
    informaciondet.codInfBasicaDet =
      InformacionBasicaAreaAprovechamientoPOAC.ACORDEON4_4;
    informaciondet.idUsuarioRegistro = this.usuarioService.idUsuario;

    newAccordeon4_4.listInformacionBasicaDet.push(informaciondet);
    newAccordeon4_4.idUsuarioRegistro = this.usuarioService.idUsuario;

    array.push(newAccordeon4_1, newAccordeon4_4);

    this.informacionBasicaSOPCMervice
      .registrarInformacionBasica(array)
      .subscribe((data: any) => {
        if (data.success) {
          this.successMensaje(data.message);
          this.listarInfoBasicaAreaAprovechamiento4_1_1();
          this.listarInfoBasicaAreaAprovechamiento4_1_2();
          this.listarInfoBasicaAreaAprovechamiento4_2();
          this.listarInfoBasicaAreaAprovechamiento4_3();
          this.listarInfoBasicaAreaAprovechamiento4_4();
        }
      });
  }

  listarInfoBasicaAreaAprovechamiento4_1_1() {
    const params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoProceso + "IBAMATC",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaSOPCMervice
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            const element = response.data[0];
            element.idUsuarioRegistro = this.usuarioService.idUsuario;
            this.informacionBasica4_1_1 = new InformacionBasicaModel(element);
            this.informacionBasicaDet4_1_1 = new InformacionBasicaDetalleModel(
              element
            );
          }
        }
      });
  }

  listarInfoBasicaAreaAprovechamiento4_1_2() {
    const params: any = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoProceso + "IBAMUP",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaSOPCMervice
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            const element = response.data[0];
            element.idUsuarioRegistro = this.usuarioService.idUsuario;
            this.informacionBasica4_1_2 = new InformacionBasicaModel(element);
            this.informacionBasicaDet4_1_2 = new InformacionBasicaDetalleModel(
              element
            );
            this.informacionBasicaDet4_1_2.referencia = null;

            this.departamento.idDepartamento = Number.parseInt(
              element.departamento
            );
            this.provincia.idProvincia = Number.parseInt(element.provincia);
            this.distrito.idDistrito = Number.parseInt(element.distrito);
            this.idCuenca = isNullOrEmpty(element.cuenca)
              ? null
              : Number(element.cuenca);
            this.onSelectedCuenca({ value: this.idCuenca });
            this.idSubcuenca = isNullOrEmpty(element.subCuenca)
              ? null
              : Number(element.subCuenca);
          }
        }
      });
  }

  listarInfoBasicaAreaAprovechamiento4_2() {
    this.informacionBasicaDet4_2 = [];
    const params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoProceso + "IBAMCUMD",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaSOPCMervice
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            response.data[0].idUsuarioRegistro = this.usuarioService.idUsuario;
            this.informacionBasica4_2 = new InformacionBasicaModel(
              response.data[0]
            );

            if (!this.deleteVertice) {
              if (response.data[0].idInfBasica == 0) {
                this.cleanLayers();
                this.obtenerCapasPadre();
              } else {
                this.cleanLayers();
                this.obtenerCapas();
              }

              let det = new InformacionBasicaDetalleModel();
              let newList: any = [];
              response.data.forEach((item: any) => {
                item.codSubInfBasicaDet = this.codigoProceso + "IBAMCUMD";
                item.idUsuarioRegistro = this.usuarioService.idUsuario;
                // if (item.idInfBasicaDet == 0) item.referencia = "";
                det = item;
                newList.push(det);
              });

              this.informacionBasicaDet4_2 = newList.groupBy(
                (t: any) => t.descripcion
              );
            }
            this.deleteVertice = false;
          }
        }
      });
  }

  listarInfoBasicaAreaAprovechamiento4_3() {
    this.informacionBasicaDet4_3 = [];
    const params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoProceso + "IBAMABTB",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaSOPCMervice
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            response.data[0].idUsuarioRegistro = this.usuarioService.idUsuario;
            this.informacionBasica4_3 = new InformacionBasicaModel(
              response.data[0]
            );
            this.ampliarAnexo4_3 = response.data[0].ampliarAnexo;

            if (!this.deleteTB) {
              if (response.data[0].idInfBasica == 0) {
                this.cleanLayersTB();
                this.obtenerCapasPadreTB();
              } else {
                this.cleanLayersTB();
                this.obtenerCapasTB();
              }

              let det = new InformacionBasicaDetalleModel();
              response.data.forEach((item: any) => {
                if (item.codInfBasicaDet !== null) {
                  item.codSubInfBasicaDet = this.codigoProceso + "IBAMABTB";
                  det = item;
                  this.informacionBasicaDet4_3.push(det);
                }
              });

              this.calculateAreaTotalBosques();
            }
            this.deleteTB = false;
          }
        }
      });
  }

  calculateAreaTotalBosques() {
    let sum1 = 0;
    let sum2 = 0;
    this.informacionBasicaDet4_3.forEach((t: any) => {
      sum1 += t.areaHa;
    });

    this.informacionBasicaDet4_3.forEach((t: any) => {
      t.areaHaPorcentaje = t.areaHa>0? Number(((100 * t.areaHa) / sum1).toFixed(2)):0;
      sum2 += t.areaHaPorcentaje;
    });

    this.totalAreaBosque = parseFloat(`${sum1.toFixed(2)}`);
    this.totalPorcentajeBosque = parseFloat(`${sum2.toFixed(2)} %`);
  }

  listarInfoBasicaAreaAprovechamiento4_4() {
    const params = {
      idInfBasica: this.codigoProceso,
      idPlanManejo: this.idPlanManejo,
      codCabecera: this.codigoProceso + "IBAMA",
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionBasicaSOPCMervice
      .listarInformacionBasica(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          if (response.data[0].idInfBasica != null) {
            const element = response.data[0];
            this.informacionBasica4_4 = new InformacionBasicaModel(element);
            this.informacionBasicaDet4_4 = new InformacionBasicaDetalleModel(
              element
            );
          }
        }
      });
  }

  registrar4_1() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin([this.registrar4_1_1(), this.registrar4_1_2()])
      .pipe(
        finalize(() => {
          this.toast.ok(
            "Se registró la ubicación y extensión del área de aprovechamiento anual correctamente."
          );
          this.dialog.closeAll();
        })
      )
      .subscribe();
  }

  registrar4_1_1() {
    this.informacionBasica4_1_1.codSubInfBasica = this.codigoProceso + "IBAM";
    this.informacionBasica4_1_1.codNombreInfBasica =
      this.codigoProceso + "IBAMATC";
    this.informacionBasica4_1_1.listInformacionBasicaDet = [];
    this.informacionBasica4_1_1.listInformacionBasicaDet.push(
      this.informacionBasicaDet4_1_1
    );

    return this.informacionBasicaSOPCMervice
      .registrarInformacionBasica([this.informacionBasica4_1_1])
      .pipe(
        tap((data: any) => {
          if (data.success) {
            this.listarInfoBasicaAreaAprovechamiento4_1_1();
          }
        })
      );
  }

  registrar4_1_2() {
    this.informacionBasica4_1_2.codSubInfBasica = this.codigoProceso + "IBAM";
    this.informacionBasica4_1_2.codNombreInfBasica =
      this.codigoProceso + "IBAMUP";
    this.informacionBasica4_1_2.cuenca = this.idCuenca
      ? String(this.idCuenca)
      : null;
    this.informacionBasica4_1_2.subCuenca = this.idSubcuenca
      ? String(this.idSubcuenca)
      : null;
    this.informacionBasica4_1_2.listInformacionBasicaDet = [];
    this.informacionBasica4_1_2.listInformacionBasicaDet.push(
      this.informacionBasicaDet4_1_2
    );

    return this.informacionBasicaSOPCMervice
      .registrarInformacionBasica([this.informacionBasica4_1_2])
      .pipe(
        tap((data: any) => {
          if (data.success) {
            this.listarInfoBasicaAreaAprovechamiento4_1_2();
          }
        })
      );
  }

  registrar4_2() {
    if (this.isPadreVertice && this.idArchivoVertice == 0) {
      this.toast.warn("Debe guardar archivo shapefile.");
    } else if (this.newSPVertice && this.idArchivoVertice == 0) {
      this.toast.warn("Debe guardar archivo shapefile.");
    } else {
      this.informacionBasica4_2.codSubInfBasica = this.codigoProceso + "IBAM";
      this.informacionBasica4_2.idPlanManejo = this.idPlanManejo;
      this.informacionBasica4_2.idUsuarioRegistro = this.usuarioService.idUsuario;
      this.informacionBasica4_2.codInfBasica = this.codigoProceso;
      this.informacionBasica4_2.listInformacionBasicaDet = [];
      this.informacionBasicaDet4_2.forEach((item) => {
        item.value.forEach((element: any) => {
          element.coordenadaEsteIni = element.coordenadaEste;
          element.coordenadaNorteIni = element.coordenadaNorte;
          this.informacionBasica4_2.listInformacionBasicaDet.push(element);
        });
      });
      

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionBasicaSOPCMervice
        .registrarInformacionBasica([this.informacionBasica4_2])
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((data: any) => {
          if (data.success) {
            this.isPadreVertice = false;
            this.newSPVertice = false;
            this.deleteVertice = false;
            this.idArchivoVertice = 0;
            this.toast.ok(
              "Se registró Coordenadas UTM del área de aprovechamiento anual correctamente."
            );
            this.listarInfoBasicaAreaAprovechamiento4_2();
          }
        });
    }
  }

  registrar4_3() {
    if (this.isPadreTB && this.idArchivoTB == 0) {
      this.toast.warn("Debe guardar archivo shapefile.");
    } else if (this.newSPTB && this.idArchivoTB == 0) {
      this.toast.warn("Debe guardar archivo shapefile.");
    } else {
      this.informacionBasica4_3.codSubInfBasica = this.codigoProceso + "IBAM";
      this.informacionBasica4_3.codNombreInfBasica =
        this.codigoProceso + "IBAMABTB";
      this.informacionBasica4_3.idPlanManejo = this.idPlanManejo;
      this.informacionBasica4_3.idUsuarioRegistro = this.usuarioService.idUsuario;
      this.informacionBasica4_3.codInfBasica = this.codigoProceso;
      this.informacionBasica4_3.listInformacionBasicaDet = [];
      this.informacionBasicaDet4_3.forEach((item) => {
        item.ampliarAnexo = this.ampliarAnexo4_3;
        this.informacionBasica4_3.listInformacionBasicaDet.push(item);
      });

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionBasicaSOPCMervice
        .registrarInformacionBasica([this.informacionBasica4_3])
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((data: any) => {
          if (data.success) {
            this.isPadreTB = false;
            this.newSPTB = false;
            this.deleteTB = false;
            this.idArchivoTB = 0;
            this.toast.ok("Se registró Tipos de bosque correctamente.");
            this.listarInfoBasicaAreaAprovechamiento4_3();
          }
        });
    }
  }

  registrar4_4() {
    if (
      this.informacionBasicaDet4_4.medioTransporte == "OTRO" &&
      isNullOrEmpty(this.informacionBasicaDet4_4.justificacion)
    ) {
      this.toast.warn("Debe especificar: Otros medios de Transporte.");
    } else {
      this.informacionBasica4_4.idPlanManejo = this.idPlanManejo;
      this.informacionBasica4_4.codInfBasica = this.codigoProceso;
      this.informacionBasica4_4.codSubInfBasica = this.codigoProceso + "IBAM";
      this.informacionBasica4_4.codNombreInfBasica =
        this.codigoProceso + "IBAMA";
      this.informacionBasica4_4.idUsuarioRegistro = this.usuarioService.idUsuario;
      this.informacionBasica4_4.listInformacionBasicaDet = [];
      this.informacionBasicaDet4_4.codInfBasicaDet =
        this.codigoProceso + "IBAMA";
      this.informacionBasicaDet4_4.codSubInfBasicaDet =
        this.codigoProceso + "IBAMA";
      this.informacionBasica4_4.listInformacionBasicaDet.push(
        this.informacionBasicaDet4_4
      );

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.informacionBasicaSOPCMervice
        .registrarInformacionBasica([this.informacionBasica4_4])
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((data: any) => {
          if (data.success) {
            this.toast.ok(
              "Se registró la accesibilidad de la UMF correctamente."
            );
            this.listarInfoBasicaAreaAprovechamiento4_4();
          }
        });
    }
  }

  createTablaCoodenadas(items: any) {
    this.informacionBasicaDet4_2 = [];
    let listMap = items.map((t: any) => {
      return {
        descripcion: t.properties.anexo,
        puntoVertice: t.properties.vertice,
        coordenadaEste: t.properties.este,
        coordenadaNorte: t.properties.norte,
        referencia: "",
        codInfBasica: this.codigoProceso,
        codInfBasicaDet: this.codigoProceso + "IBAMCUMD",
        codNombreInfBasica: "Coordenadas UTM",
        codSubInfBasicaDet: this.codigoProceso + "IBAMCUMD",
        idInfBasicaDet: 0,
        idUsuarioRegistro: this.usuarioService.idUsuario,
      };
    });

    this.informacionBasicaDet4_2 = listMap.groupBy((t: any) => t.descripcion);
    this.newSPVertice = true;
  }

  eliminarCoordenadas(event: any) {
    if (this.informacionBasicaDet4_2.length > 0) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: "¿Está seguro de eliminar los registros?.",
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Sí",
        rejectLabel: "No",
        accept: () => {
          let idInfBasicaCoordenada: number | null = 0;
          if (this.informacionBasica4_2 != null) {
            idInfBasicaCoordenada = this.informacionBasica4_2.idInfBasica;
          }

          if (
            idInfBasicaCoordenada != null &&
            idInfBasicaCoordenada != undefined
          ) {
            let params = {
              idInfBasica: idInfBasicaCoordenada,
              idInfBasicaDet: 0,
              codInfBasicaDet: "",
              idUsuarioElimina: this.usuarioService.idUsuario,
            };
            this.informacionBasicaSOPCMervice
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                } else {
                  this.ErrorMensaje(result.message);
                }
                this.isPadreVertice = false;
                this.newSPVertice = false;
                this.deleteVertice = true;
                this.idArchivoTB = 0;
                this.listarInfoBasicaAreaAprovechamiento4_2();
                // this.informacionBasicaDet4_2 = [];
              });
          } else {
            this.deleteVertice = true;
            this.listarInfoBasicaAreaAprovechamiento4_2();
            // this.informacionBasicaDet4_2 = [];
          }
        },
        reject: () => {},
      });
    }
  }

  listarTiposBosque(items: any) {
    if (this.newSPTB) {
      this.informacionBasicaDet4_3 = [];
      let listMap: any = {};

      items.forEach((item: any) => {
        item.tipoBosque.forEach((t: any) => {
          listMap = {
            areaHa: t.areaHA,
            areaHaPorcentaje: t.areaHAPorcentaje,
            nombre: t.descripcion,
            idTipoBosque: t.idBosque,
            referencia: "",
            codInfBasica: this.codigoProceso,
            codInfBasicaDet: this.codigoProceso + "IBAMABTB",
            codNombreInfBasica: this.codigoProceso + "IBAMABTB",
            codSubInfBasicaDet: this.codigoProceso + "IBAMABTB",
            idInfBasicaDet: 0,
            idUsuarioRegistro: this.usuarioService.idUsuario,
          };
          this.informacionBasicaDet4_3.push(listMap);
        });
      });

      this.calculateAreaTotalBosques();
    }
  }

  eliminarTiposBosque(event: any) {
    if (this.informacionBasicaDet4_3.length > 0) {
      this.confirmationService.confirm({
        target: event.target || undefined,
        message: "¿Está seguro de eliminar los registros?.",
        icon: "pi pi-exclamation-triangle",
        acceptLabel: "Sí",
        rejectLabel: "No",
        accept: () => {
          let idInfBasicaTB: number | null = 0;
          if (this.informacionBasica4_3 != null) {
            idInfBasicaTB = this.informacionBasica4_3.idInfBasica;
          }

          if (idInfBasicaTB != null && idInfBasicaTB != undefined) {
            let params = {
              idInfBasica: idInfBasicaTB,
              idInfBasicaDet: 0,
              codInfBasicaDet: "",
              idUsuarioElimina: this.usuarioService.idUsuario,
            };
            this.informacionBasicaSOPCMervice
              .eliminarInformacionBasica(params)
              .subscribe((result: any) => {
                if (result.success) {
                  this.SuccessMensaje(result.message);
                } else {
                  this.ErrorMensaje(result.message);
                }
                this.informacionBasicaDet4_3 = [];
                this.calculateAreaTotalBosques();
                this.isPadreTB = false;
                this.newSPTB = false;
                this.deleteTB = true;
                this.idArchivoTB = 0;
                this.listarInfoBasicaAreaAprovechamiento4_3();
              });
          } else {
            this.informacionBasicaDet4_3 = [];
            this.calculateAreaTotalBosques();
            this.deleteTB = true;
            this.listarInfoBasicaAreaAprovechamiento4_3();
          }
        },
        reject: () => {},
      });
    }
  }

  /* START REGION GEOMETRIA */
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  obtenerCapasPadre() {
    

    let item = {
      idPlanManejo: this.idPlanManejoPadre,
      codigoSeccion: "PGMFAIBAM",
      codigoSubSeccion: "",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              if (
                t.codigoSubSeccion == "PGMFAIBAMCUAM" ||
                t.codigoSubSeccion == "PGMFAIBAMCUAMAMD"
              ) {
                

                this.crearFile(
                  t.idArchivo,
                  t.tipoGeometria,
                  t.codigoSubSeccion
                );
              }
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  crearFile(id: any, tipoGeometria: any, codigoSubSeccionPadre: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          let config = {
            idGroupLayer: this.mapApi.Guid2.newGuid,
            inServer: false,
            service: false,
            validate: false,
            annex: false,
            withVertice: false,
            codigoProceso: this.codigoProceso + "IBAM",
            codigoSubSeccion: this.codigoProceso + "IBAMCUMD",
            tipoGeometria: tipoGeometria,
            codigoSubSeccionPadre: codigoSubSeccionPadre,
          };

          const binary_string = window.atob(result.data.file);
          const len = binary_string.length;
          const bytes = new Uint8Array(len);
          for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
          }
          const file: File = new File([bytes.buffer], result.data.nombre, {
            type: "application/x-zip-compressed",
          });

          this.isPadreVertice = true;
          this.processFile(file, config);
        }
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    codigoProceso: any,
    codigoSubSeccion: any,
    tipoGeometria: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      withVertice: withVertice,
      codigoProceso: codigoProceso,
      codigoSubSeccion: codigoSubSeccion,
      tipoGeometria: tipoGeometria,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }

  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);

      if (config.withVertice) {
        this.createTablaCoodenadas(data[0].features);
      }
    });
  }

  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.tipoGeometria;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.codigoSubSeccionPadre = config.codigoSubSeccionPadre;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layer.annex = config.annex;
      this._layer.descripcion = config.tipoGeometria;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    this.file.idGroupLayer = config.idGroupLayer;
    this.file.descripcion = config.tipoGeometria;
    this._filesSHP.push(this.file);
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.codigoSubSeccionPadre = item.codigoSubSeccionPadre;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }

  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.withVertice === true) {
      popupTemplate.content = [
        {
          type: "fields",
          fieldInfos: [
            {
              fieldName: "anexo",
              label: "Anexo",
            },
            {
              fieldName: "vertice",
              label: "Vertice",
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: "este",
              label: "Este (X)",
            },
            {
              fieldName: "norte",
              label: "Norte (Y)",
            },
          ],
        },
      ];
    }
    return popupTemplate;
  }

  onGuardarLayer() {
    let fileUpload: any = [];

    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.usuarioService.idUsuario,
          codigo: "37",
          codigoTipoPGMF: this.codigoProceso,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.cleanLayers();
          this.obtenerCapas();
        })
      )
      .subscribe(
        (result) => {
          this.SuccessMensaje(result.message);
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error.");
        }
      );
  }

  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }

  saveFileRelation(result: any, item: any) {
    this.idArchivoVertice = result.data;
    let item2 = {
      codigoTipoPGMF: this.codigoProceso,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }

  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoProceso + "IBAM",
        codigoSubSeccion: t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        propiedad: t3.codigoSubSeccionPadre
          ? t3.codigoSubSeccionPadre
          : t3.attributes[0].properties.ET_ID,
        idUsuarioRegistro: this.usuarioService.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }

  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }

  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }

  obtenerCapas() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.codigoProceso + "IBAM",
      codigoSubSeccion: this.codigoProceso + "IBAMCUMD",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = t.idArchivo;
              this._layer.idLayer = this.mapApi.Guid2.newGuid;
              this._layer.inServer = true;
              this._layer.service = false;
              this._layer.nombre = t.nombreCapa;
              this._layer.groupId = groupId;
              this._layer.color = t.colorCapa;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layers.push(this._layer);
              let geoJson = this.mapApi.getGeoJson(
                this._layer.idLayer,
                groupId,
                item
              );
              this.createLayer(geoJson);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }

  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          "application/octet-stream"
        );
      }
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      };
    });
  }

  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: "¿Está seguro de eliminar este archivo?",
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHP",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                // if (this._filesSHP.length === 0) {
                //   this.cleanLayers();
                // }
                this.SuccessMensaje("El archivo se eliminó correctamente.");
              } else {
                this.ErrorMensaje("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.ErrorMensaje("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
      }
    }
  }

  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.usuarioService.idUsuario
    );
  }
  /* END REGION GEOMETRIA */

  /* START REGION GEOMETRIA TB*/
  initializeMapTB(): void {
    const container = this.mapViewElTB.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.viewTB = view;
  }

  obtenerCapasPadreTB() {
    let item = {
      idPlanManejo: this.idPlanManejoPadre,
      codigoSeccion: "PGMFAIBAM",
      codigoSubSeccion: "PGMFAIBAMABTB",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              this.crearFileTB(t.idArchivo, t.tipoGeometria);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  crearFileTB(id: any, tipoGeometria: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          let config = {
            idGroupLayer: this.mapApi.Guid2.newGuid,
            inServer: false,
            service: false,
            validate: false,
            annex: false,
            withVertice: false,
            codigoProceso: this.codigoProceso + "IBAM",
            codigoSubSeccion: this.codigoProceso + "IBAMABTB",
            tipoGeometria: tipoGeometria,
          };

          const binary_string = window.atob(result.data.file);
          const len = binary_string.length;
          const bytes = new Uint8Array(len);
          for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
          }
          const file: File = new File([bytes.buffer], result.data.nombre, {
            type: "application/x-zip-compressed",
          });

          this.isPadreTB = true;
          this.processFileTB(file, config);
        }
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  processFileTB(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayersTB(data, config);

      if (this.newSPTB) {
        this.consultarTiposBosquePorPoligono(data[0].features);
        let coordinates: any = [];
        data[0].features.forEach((t: any) => {
          coordinates.push(t.geometry.coordinates[0]);
        });
        this.servicioSuperposicionPlanificacion(coordinates);
      }
    });
  }

  createLayersTB(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._idTB;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.tipoGeometria;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layerTB = {} as CustomCapaModel;
      this._layerTB.codigo = config.idArchivo;
      this._layerTB.idLayer = t.idLayer;
      this._layerTB.inServer = config.inServer;
      this._layerTB.nombre = t.title || config.fileName;
      this._layerTB.groupId = t.groupId;
      this._layerTB.color = t.color;
      this._layerTB.idGroupLayer = config.idGroupLayer;
      this._layerTB.annex = config.annex;
      this._layerTB.descripcion = config.tipoGeometria;
      this._layersTB.push(this._layerTB);
      this.createLayerTB(t);
    });
    this.fileTB = {} as FileModel;
    this.fileTB.codigo = config.idArchivo;
    this.fileTB.file = config.file;
    this.fileTB.inServer = config.inServer;
    this.fileTB.idGroupLayer = config.idGroupLayer;
    this.fileTB.descripcion = config.tipoGeometria;
    this._filesSHPTB.push(this.fileTB);
  }

  createLayerTB(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = { title: item.title };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._idTB;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.viewTB.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.viewTB
        );
        this.viewTB.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => {});
  }

  consultarTiposBosquePorPoligono(items: any) {
    this.listQuinquenal = [];
    let observer = from(items);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.servicioIdentificarTipoBosque(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.newLayerInternal.forEach((t: any) => {
            this._layersTB.push(t);
          });
          this.newGeojsonInternal.forEach((t: any) => {
            this.createLayerTB(t);
          });
          let newItems = this.listQuinquenal.sort((a: any, b: any) => {
            if (a.label < b.label) {
              return -1;
            }
            if (a.label > b.label) {
              return 1;
            }
            return 0;
          });
          let validBosque: any = false;
          newItems.forEach((t: any) => {
            if (t.tipoBosque.length) {
              validBosque = true;
            }
          });
          if (validBosque === true) {
            this.listarTiposBosque(newItems);
          } else {
            this.toast.warn(
              "No se encontro ningun objeto espacial en el servicio de identificar tipo de bosques."
            );
          }
        })
      )
      .subscribe();
  }

  servicioSuperposicionPlanificacion(geometry: any) {
    let params = {
      codProceso: "110102",
      geometria: {
        poligono: geometry,
      },
    };
    this.serviceExternos
      .validarSuperposicionPlanificacion(params)
      .subscribe((result: any) => {
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._idTB;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.included = false;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.overlap = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layersTB.push(layer);
                this.createLayerTB(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._idTB;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layersTB.push(layer);

                this.createLayerTB(t.geoJson);
              }
            }
          });
        }
      });
  }

  servicioIdentificarTipoBosque(item: any) {
    let params = {
      idClasificacion: "",
      geometria: {
        poligono: item.geometry.coordinates,
      },
    };
    return this.serviceExternos
      .identificarTipoBosque(params)
      .pipe(concatMap(async (item2) => this.listarQuinquenal(item2, item)));
  }

  listarQuinquenal(result: any, item: any) {
    let listBosquesService: any = {};
    listBosquesService = {
      label: item.properties.Name_1 || item.properties.Name,
      tipoBosque: [],
    };

    let tiposBosques: any = [];
    if (result.dataService.data.capas.length > 0) {
      result.dataService.data.capas.forEach((t: any) => {
        if (t.geoJson !== null) {
          t.geoJson.crs.type = "name";
          t.geoJson.opacity = 0.4;
          t.geoJson.color = this.mapApi.random();
          t.geoJson.title = t.nombreCapa;
          t.geoJson.service = true;
          t.geoJson.groupId = this._idTB;
          t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
          let layer: any = {} as CustomCapaModel;
          layer.codigo = null;
          layer.idLayer = t.geoJson.idLayer;
          layer.inServer = false;
          layer.service = true;
          layer.nombre = t.nombreCapa;
          layer.groupId = t.geoJson.groupId;
          layer.color = t.geoJson.color;
          layer.annex = false;
          this.layerInternal.push(layer);
          //this._layers.push(this._layer);
          this.geoJsonInternal.push(t.geoJson);
          //this.createLayer(t.geoJson);
          let coordinates: any = [];
          t.geoJson.features.forEach((t: any) => {
            t.geometry.coordinates.forEach((t2: any) => {
              coordinates.push(t2);
            });
          });
          tiposBosques.push({
            idBosque: t.codigoCapa,
            descripcion: t.nombreCapa,
            areaHA: this.mapApi.calculateIntersection(
              item.geometry.coordinates,
              coordinates
            ), //this.calculateArea(t.geoJson),
            areaHAPorcentaje: "",
          });
        }
      });
      listBosquesService.tipoBosque = tiposBosques;
    }
    this.listQuinquenal.push(listBosquesService);
    this.newLayerInternal = this.removeDuplicates(this.layerInternal, "nombre");
    this.newGeojsonInternal = this.removeDuplicates(
      this.geoJsonInternal,
      "title"
    );
  }

  removeDuplicates(items: any, nodo: string) {
    let newArray = [];
    let uniqueObject: any = {};
    for (let i in items) {
      let objTitle: any = items[i][nodo];
      uniqueObject[objTitle] = items[i];
    }
    for (let i in uniqueObject) {
      newArray.push(uniqueObject[i]);
    }

    return newArray;
  }

  onChangeFileSHPTB(
    e: any,
    withVertice: Boolean,
    codigoProceso: any,
    codigoSubSeccion: any,
    tipoGeometria: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      withVertice: withVertice,
      codigoProceso: codigoProceso,
      codigoSubSeccion: codigoSubSeccion,
      tipoGeometria: tipoGeometria,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.newSPTB = true;
            this.processFileTB(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }

  onGuardarLayerTB() {
    let fileUpload: any = [];

    this._filesSHPTB.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.usuarioService.idUsuario,
          codigo: "37",
          codigoTipoPGMF: this.codigoProceso,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFileTB(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.cleanLayersTB();
          this.obtenerCapasTB();
        })
      )
      .subscribe(
        (result) => {
          this.SuccessMensaje(result.message);
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error.");
        }
      );
  }

  saveFileTB(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelationTB(result, item)));
  }

  saveFileRelationTB(result: any, item: any) {
    this.idArchivoTB = result.data;
    let item2 = {
      codigoTipoPGMF: this.codigoProceso,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapaTB(item, item2.idArchivo)));
  }

  guardarCapaTB(itemFile: any, idArchivo: any) {
    let layers = this.getLayersTB();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometriaTB = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.viewTB.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: null,
        codigoSeccion: this.codigoProceso + "IBAM",
        codigoSubSeccion: t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.usuarioService.idUsuario,
      };
      this.planManejoGeometriaTB.push(item);
    });
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometriaTB
    );
  }

  getLayersTB() {
    let layers = this.viewTB.map.allLayers.filter(
      (t: any) => t.groupId === this._idTB
    );
    return layers;
  }

  cleanLayersTB() {
    let layers = this.mapApi.getLayers(this._idTB, this.viewTB);
    layers.forEach((t: any) => {
      this.viewTB.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layersTB = [];
  }

  obtenerCapasTB() {
    let item = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.codigoProceso + "IBAM",
      codigoSubSeccion: this.codigoProceso + "IBAMABTB",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._idTB;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layerTB = {} as CustomCapaModel;
              this._layerTB.codigo = t.idArchivo;
              this._layerTB.idLayer = this.mapApi.Guid2.newGuid;
              this._layerTB.inServer = true;
              this._layerTB.service = false;
              this._layerTB.nombre = t.nombreCapa;
              this._layerTB.groupId = groupId;
              this._layerTB.color = t.colorCapa;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layersTB.push(this._layerTB);
              let geoJson = this.mapApi.getGeoJson(
                this._layerTB.idLayer,
                groupId,
                item
              );
              this.createLayerTB(geoJson);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  toggleLayerTB(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.viewTB);
  }

  onRemoveFileSHPTB(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: "¿Está seguro de eliminar este archivo?",
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHPTB",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layersTB.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHPTB.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHPTB.splice(indexFile, 1);
                this._layersTB.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.viewTB);
                // if (this._filesSHP.length === 0) {
                //   this.cleanLayers();
                // }
                this.SuccessMensaje("El archivo se eliminó correctamente.");
              } else {
                this.ErrorMensaje("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.ErrorMensaje("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layersTB.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHPTB.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHPTB.splice(indexFile, 1);
      this._layersTB.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.viewTB);
      if (this._filesSHPTB.length === 0) {
        this.cleanLayersTB();
      }
    }
  }
  /* END REGION GEOMETRIA */

  /* START REGION GEOMETRIA AC */
  initializeMapAC(): void {
    const container = this.mapViewElAC.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.viewAC = view;
  }

  onChangeFileSHPAC(
    e: any,
    codigoProceso: any,
    codigoSubSeccion: any,
    tipoGeometria: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      codigoProceso: codigoProceso,
      codigoSubSeccion: codigoSubSeccion,
      tipoGeometria: tipoGeometria,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFileAC(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = "";
    }
  }

  processFileAC(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayersAC(data, config);
    });
  }

  createLayersAC(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.file = config.file;
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._idAC;
      t.idGroupLayer = config.idGroupLayer;
      t.descripcion = config.tipoGeometria;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layerAC = {} as CustomCapaModel;
      this._layerAC.codigo = config.idArchivo;
      this._layerAC.idLayer = t.idLayer;
      this._layerAC.inServer = config.inServer;
      this._layerAC.nombre = t.title || config.fileName;
      this._layerAC.groupId = t.groupId;
      this._layerAC.color = t.color;
      this._layerAC.idGroupLayer = config.idGroupLayer;
      this._layerAC.annex = config.annex;
      this._layerAC.descripcion = config.tipoGeometria;
      this._layersAC.push(this._layerAC);
      this.createLayerAC(t);
    });
    this.fileAC = {} as FileModel;
    this.fileAC.codigo = config.idArchivo;
    this.fileAC.file = config.file;
    this.fileAC.inServer = config.inServer;
    this.fileAC.idGroupLayer = config.idGroupLayer;
    this.fileAC.descripcion = config.tipoGeometria;
    this._filesSHPAC.push(this.fileAC);
  }

  createLayerAC(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setPropertiesAC(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._idAC;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.viewAC.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.viewAC
        );
        this.viewAC.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }

  setPropertiesAC(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    popupTemplate.content = [
      {
        type: "fields",
        fieldInfos: [
          {
            fieldName: "TrackName",
            label: "Nombre",
          },
          {
            fieldName: "Position",
            label: "Posicion",
            format: {
              digitSeparator: true,
              places: 0,
            },
          },
          {
            fieldName: "Time",
            label: "Tiempo",
          },
          {
            fieldName: "Altitude",
            label: "Altitud",
          },
        ],
      },
    ];
    return popupTemplate;
  }

  onGuardarLayerAC() {
    let fileUpload: any = [];

    this._filesSHPAC.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.usuarioService.idUsuario,
          codigo: "37",
          codigoTipoPGMF: this.codigoProceso,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFileAC(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.cleanLayersAC();
          this.obtenerCapasAC();
        })
      )
      .subscribe(
        (result) => {
          this.SuccessMensaje(result.message);
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error.");
        }
      );
  }

  saveFileAC(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveLayerRelationAC(result, item)));
  }

  saveLayerRelationAC(result: any, item: any) {
    this.idArchivoAC = result.data;
    let item2 = {
      codigoTipoPGMF: this.codigoProceso,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    return this.servicePostulacionPFDM
      .registrarArchivoDetalle(item2)
      .pipe(concatMap(() => this.guardarCapaAC(item, item2.idArchivo)));
  }

  guardarCapaAC(itemFile: any, idArchivo: any) {
    let layers = this.getLayersAC();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.viewAC.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: null,
        codigoSeccion: this.codigoProceso + "IBAM",
        codigoSubSeccion: t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.usuarioService.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }

  getLayersAC() {
    let layers = this.viewAC.map.allLayers.filter(
      (t: any) => t.groupId === this._idAC
    );
    return layers;
  }

  cleanLayersAC() {
    let layers = this.mapApi.getLayers(this._idAC, this.viewAC);
    layers.forEach((t: any) => {
      this.viewAC.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layersAC = [];
  }

  obtenerCapasAC() {
    let item: any = {
      idPlanManejo: this.idPlanManejo,
      codigoSeccion: this.codigoProceso + "IBAM",
      codigoSubSeccion: this.codigoProceso + "IBAMA",
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._idAC;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              this._layerAC = {} as CustomCapaModel;
              this._layerAC.codigo = t.idArchivo;
              this._layerAC.idLayer = this.mapApi.Guid2.newGuid;
              this._layerAC.inServer = true;
              this._layerAC.service = false;
              this._layerAC.nombre = t.nombreCapa;
              this._layerAC.groupId = groupId;
              this._layerAC.color = t.colorCapa;
              //this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
              this._layersAC.push(this._layerAC);
              let geoJson = this.mapApi.getGeoJson(
                this._layerAC.idLayer,
                groupId,
                item
              );
              this.createLayerAC(geoJson);
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
      }
    );
  }

  toggleLayerAC(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.viewAC);
  }

  onRemoveFileSHPAC(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: "¿Está seguro de eliminar este archivo?",
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHPAC",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layersAC.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHPAC.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHPAC.splice(indexFile, 1);
                this._layersAC.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.viewAC);
                // if (this._filesSHPAC.length === 0) {
                //   this.cleanLayersAC();
                // }
                this.SuccessMensaje("El archivo se eliminó correctamente.");
              } else {
                this.ErrorMensaje("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.ErrorMensaje("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layersAC.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHPAC.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHPAC.splice(indexFile, 1);
      this._layersAC.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.viewAC);
      if (this._filesSHPAC.length === 0) {
        this.cleanLayersAC();
      }
    }
  }
  /* END REGION GEOMETRIA AC */

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }
  retornarFlujoEvaluacion() {
    localStorage.setItem(
      "EvalResuDet",
      JSON.stringify({
        tab: "RFNM",
        acordeon: "LINEADP6211",
      })
    );

    this.router.navigateByUrl(
      "/planificacion/evaluacion/requisitos-previos-poac/" +
        this.idPlanManejo +
        "/" +
        this.codigoProceso
    );
  }
}

export interface CuencaSubcuencaModel {
  codCuenca: string;
  codSubCuenca: string;
  codigo: string;
  cuenca: string;
  idCuenca: number;
  idSubCuenca: number;
  subCuenca: string;
  valor: string;
}
