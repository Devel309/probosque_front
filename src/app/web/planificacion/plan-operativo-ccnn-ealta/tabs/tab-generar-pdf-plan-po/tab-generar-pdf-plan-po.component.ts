import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
@Component({
  selector: 'app-tab-generar-pdf-plan-po',
  templateUrl: './tab-generar-pdf-plan-po.component.html',
  styleUrls: ['./tab-generar-pdf-plan-po.component.scss'],
  providers: [MessageService, ConfirmDialogModule],
})
export class TabGenerarPdfPlanComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() isPerfilArffs!: boolean;

  @Output()
  public siguiente = new EventEmitter();

  @Output()
  public regresar = new EventEmitter();

  constructor(
    private messageService: MessageService,
    private router: Router
  ) {

  }

  plan: any = {}
  display: boolean = false;


  ngOnInit(): void {
    this.plan.contingencia = {}
    this.plan.suscrito = {};
    this.plan.track = {};
    this.plan.tramite = {};
    this.plan.comprobante = {};
  }


  descargarPDF() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', "temporal");
    link.setAttribute('download', "formato_solicitud.pdf");
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  descargarWord() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', "temporal");
    link.setAttribute('download', "solicitud_po.docx");
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  onFileSuscritoChange(e: any) {

    this.plan.suscrito.url = URL.createObjectURL(e.target.files[0]);
    this.plan.suscrito.file = e.srcElement.files[0];
    this.plan.suscrito.descripcion = e.srcElement.files[0].name;

    this.SuccessMensaje("Plan operativo suscrito cargado correctamente")

    e.preventDefault();
    e.stopPropagation();

  }

  onFileContingenciaChange(e: any) {

    this.plan.contingencia.url = URL.createObjectURL(e.target.files[0]);
    this.plan.contingencia.file = e.srcElement.files[0];
    this.plan.contingencia.descripcion = e.srcElement.files[0].name;

    e.preventDefault();
    e.stopPropagation();

  }


  onFileTrackChange(e: any) {

    this.plan.track.url = URL.createObjectURL(e.target.files[0]);
    this.plan.track.file = e.srcElement.files[0];
    this.plan.track.descripcion = e.srcElement.files[0].name;

    e.preventDefault();
    e.stopPropagation();

  }

  onFileTramiteChange(e: any) {

    this.plan.tramite.url = URL.createObjectURL(e.target.files[0]);
    this.plan.tramite.file = e.srcElement.files[0];
    this.plan.tramite.descripcion = e.srcElement.files[0].name;

    e.preventDefault();
    e.stopPropagation();

  }

  onFileComprobanteChange(e: any) {

    this.plan.comprobante.url = URL.createObjectURL(e.target.files[0]);
    this.plan.comprobante.file = e.srcElement.files[0];
    this.plan.comprobante.descripcion = e.srcElement.files[0].name;

    e.preventDefault();
    e.stopPropagation();

  }


  validarPDF(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';



    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  showDialog() {
    this.display = true;
  }

  guardar() {
    this.router.navigateByUrl(`/planificacion/bandeja-permisos-forestales`);
  }


}
