import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/service/config.service';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { SolicitudAccesoService } from 'src/app/service/solicitudAcceso.service';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { MessageService } from 'primeng/api';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';

@Component({
  selector: 'app-revision-solicitud-acceso',
  templateUrl: './revision-solicitud-acceso.component.html',
  styleUrls: ['./revision-solicitud-acceso.component.scss'],
  providers: [MessageService]
})
export class RevisionSolicitudAccesoComponent implements OnInit {

  dataBase:any={
    tipoPersona:''
  };
  constructor(
    private serv : ParametroValorService,
    private messageService: MessageService,
    private servSA : SolicitudAccesoService,
    private usuarioServ: UsuarioService,
    private router: Router,
    public _configService: ConfigService,
    private dialog: MatDialog,
  ) {
    
    this.dataBase = window.history.state.data;
    this.dataBase['disabled']=true;
   }

  solicitudAcceso={} as SolicitudAccesoModel;
  usuario!: UsuarioModel;

  ngOnInit(): void {
    this.usuario = this.usuarioServ.usuario;
    this.solicitudAcceso.idSolicitudAcceso = this.dataBase.idSolicitudAcceso;
    this.solicitudAcceso.codigoEstadoSolicitud = "ESACREGI";
    this.obtenerSolicitudAcceso();
  }

  obtenerSolicitudAcceso(){
    this.servSA.obtenerSolicitudAcceso(this.solicitudAcceso).subscribe(
      (result : any)=>{
        
        this.solicitudAcceso = result.data;
        //this.totalRegistrosSA = result.data.length;
    });
  }

  actualizarRevisionSolicitudAcceso(accion: any){
    
    this.solicitudAcceso.idUsuarioRevision = this.usuario.idusuario; //3647 -- cambiar por usuario actual
    this.solicitudAcceso.codigoAccion = accion;

    if(!this.validarRegistroSolicitudAcceso()){
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.servSA.actualizarRevisionSolicitudAcceso(this.solicitudAcceso).subscribe(
      (result : any)=>{
        this.dialog.closeAll();
        this.messageService.add({ severity: "success", summary: "", detail: 'Se actualizo la solicitud.' });
        setTimeout(()=> { this.router.navigate(['/planificacion/bandeja-solicitud-acceso']);},2000);
      },
      () =>{
        this.dialog.closeAll();
      }
    );
  }

  validarRegistroSolicitudAcceso(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if(this.solicitudAcceso.observacion==null || this.solicitudAcceso.observacion=="")
    {
      if(this.solicitudAcceso.codigoAccion == "Denegar"){
        validar = false;
        mensaje = mensaje +="(*) Debe ingresar una observación/comentario.\n";
      }
    }


    if(mensaje!="")
    //this.ErrorMensaje(mensaje);
    alert(mensaje);

    return validar;
  }

  enviarCorreoValidacion(): any {

    var params ={
      content: "Su solicitud de acceso fue aprobada. Sus credenciales son las siguientes.",
      email: this.solicitudAcceso.email,//'soporte_sharepoint@valtx.pe', // USUARIO CORREO / CONTRASENA NRO DOCUMENTO
      subject: "Mensaje de Aprobación"
    }

    this.servSA.enviarCorreo(params).subscribe(
      (result : any)=>{
        //alert(result.message);
      }
    );

  }


}
