import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import { SolicitudAccesoService } from 'src/app/service/solicitudAcceso.service';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from '../../../components/loading/loading.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalPlanOperativoComponent } from './components/modal/modal-plan-operativo.component';
import { BandejaPlanOperativoService } from 'src/app/service/bandeja-plan-operativo/bandeja-plan-operativo.service';

@Component({
  selector: 'bandeja-plan-operativo',
  templateUrl: './bandeja-plan-operativo.component.html',
})
export class BandejaPlanOperativo implements OnInit {
  solicitudAcceso = {} as SolicitudAccesoModel;
  usuario = {} as UsuarioModel;
  ref!: DynamicDialogRef;
  lstEstado: any[] = [];
  lstSolicituAcceso: any[] = [];
  totalRegistrosSA: number = 0;

  constructor(
    private solicitudAccesoService: SolicitudAccesoService,
    private parametroValorService: ParametroValorService,
    private dialog: MatDialog,
    public dialogService: DialogService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private bandejaPlanOperativoService: BandejaPlanOperativoService
  ) {}

  //Init
  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem('usuario')?.toString());
    this.listarEstado();
    this.listarPlanOperativo();
  }

  listarEstado() {
    var params = { prefijo: 'ESAC' };
    //acá debe obtener de tabla estados
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((result: any) => {
        this.lstEstado = result.data;
      });
  }

  listarPlanOperativo() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    let params = {
      idContrato: 1,
      nroAnio: 1,
      nroParcela: 1,

      tipoPlan: {
        idTipoPlan: 1,
      },
      tipoProceso: {
        idTipoProceso: 1,
      },
    };

    this.bandejaPlanOperativoService.listarPlanManejo(params).subscribe(
      (result: any) => {
        this.lstSolicituAcceso = result.data;
        this.totalRegistrosSA = result.data.length;
        this.dialog.closeAll();
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  }

  limpiar() {
    this.solicitudAcceso.codigoTipoPersona = '';
    this.solicitudAcceso.codigoTipoDocumento = '';
    this.solicitudAcceso.numeroDocumento = '';
    this.solicitudAcceso.nombres = '';
    this.solicitudAcceso.numeroRucEmpresa = '';
    this.solicitudAcceso.codigoTipoActor = '';
    this.solicitudAcceso.codigoTipoCncc = '';
    this.solicitudAcceso.estadoSolicitud = '';
    this.solicitudAcceso.razonSocialEmpresa = '';
  }

  modalPlanOperativo = (typeConsult: number, item: any = null) => {
    this.ref = this.dialogService.open(ModalPlanOperativoComponent, {
      header:
        typeConsult === 1
          ? 'Registrar Plan Operativo'
          : 'Editar Plan Operativo',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        isNew: typeConsult === 1 ? true : false,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if (resp) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se guardó correctamente '.concat(
            ' ',
            typeConsult === 1 ? resp : ''
          ),
        });
        this.listarPlanOperativo();
      }
    });
  };

  editarPlanOperativo = (item: any) => {
    this.modalPlanOperativo(2, item);
  };

  eliminarPlanOperativo = (event: Event, obj: any) => {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el plan operativo?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        let params = {
          idContrato: obj.idContrato,
          idPlanManejo: obj.idPlanManejo,
          idUsuarioElimina: this.usuario.idusuario,
        };
        this.dialog.open(LoadingComponent, { disableClose: true });

        this.bandejaPlanOperativoService.eliminarPlanManejo(params).subscribe(
          (result: any) => {
            this.dialog.closeAll();
            if (result.success) {
              this.SuccessMensaje(result.message);
              this.listarPlanOperativo();
            } else this.ErrorMensaje(result.message);
          },
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          }
        );
      },
      reject: () => {
        //reject action
      },
    });
  };

  SuccessMensaje = (mensaje: any) => {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  };

  ErrorMensaje = (mensaje: any) => {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  };
}
