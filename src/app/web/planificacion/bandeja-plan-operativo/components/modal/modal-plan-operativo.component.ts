import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { BandejaPlanOperativoService } from 'src/app/service/bandeja-plan-operativo/bandeja-plan-operativo.service';

@Component({
  selector: 'modal-plan-operativo',
  templateUrl: './modal-plan-operativo.component.html',
})
export class ModalPlanOperativoComponent implements OnInit {
  listaContratoTh: any[] = [];
  listaPlanGeneral: any[] = [];
  listaNumeroParcelas: any[] = [];
  listaNumeroAnios: any[] = [];

  solicitudRequestEntity: any = {};

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private dialog: MatDialog,
    private messageService: MessageService,
    private bandejaPlanOperativoService: BandejaPlanOperativoService
  ) {}

  ngOnInit() {
    this.cargarCombos();
    if (!this.config.data.item) this.obtenerPlanOperativo();
  }

  cargarCombos = () => {};

  obtenerPlanOperativo = () => {
    this.config.data.item.idPlanOperativo;
  };

  registrarPlanOperativo = () => {
    let params = {
      aspectoComplementario: 'as',

      descripcion: 'descripcion',
      idContrato: 1,
      idPlanManejo: null,
      idPlanManejoPadre: null,
      idUsuarioRegistro: 1,
      nroAnio: 3,
      nroParcela: 3,
      perEmpadronada: 2,
      perTrabajan: 2,
      solicitud: {
        idSolicitud: 1,
      },
      tipoEscala: {
        tipoEscala: 1,
      },
      tipoPlan: {
        idTipoPlan: 1,
      },
      tipoProceso: {
        idTipoProceso: 1,
      },
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.bandejaPlanOperativoService.registrarPlanManejo(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();
        if (result.success) {
          this.ref.close(result.codigo);
        } else this.ErrorMensaje(result.message);
      },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );
  };

  ErrorMensaje = (mensaje: any) => {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  };
}
