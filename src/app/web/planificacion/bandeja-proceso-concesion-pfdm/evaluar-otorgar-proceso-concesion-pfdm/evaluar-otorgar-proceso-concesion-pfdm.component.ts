import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';

@Component({
  selector: 'app-evaluar-otorgar-proceso-concesion-pfdm',
  templateUrl: './evaluar-otorgar-proceso-concesion-pfdm.component.html',
  styleUrls: ['./evaluar-otorgar-proceso-concesion-pfdm.component.scss'],
})
export class EvaluarOtorgarProcesoConcesionPfdmComponent implements OnInit {
  stylesClass: any = {
    w100px: 'evalTu',
  };

  montoAdicional: number = 10;

  asisTec_val1: number = 0;
  asisTec_val2: number = 0;
  asisTec: number = 0;

  combo__0 = [
    { value: null, text: '-' },
    { value: '0', text: '0' },
  ];

  items!: MenuItem[];
  home!: MenuItem;

  idProcesoPostulacion!: number;

  evaluar: any = {
    idProcesoPostulacion: null,
    idUsuarioRegistro: null,
    puntaje11: null,
    puntaje12: null,
    puntaje21: null,
    puntaje22: null,
    puntajeAdecMeta311: null,
    puntajeAdecMeta3121: null,
    puntajeAdecMeta3122: null,
    puntajeAdecMeta3123: null,
    puntajeAdecMeta3131: null,
    puntajeAdecMeta3132: null,
    puntajeAdecMeta3141: null,
    puntajeAdecMeta3142: null,
    puntajeAdecMeta3211: null,
    puntajeAdecMeta3212: null,
    puntajeAdecMeta3213: null,
    puntajeAdecMeta3221: null,
    puntajeAdecMeta3222: null,
    puntajeCoherente4110: null,
    puntajeCoherente4111: null,
    puntajeCoherente4112: null,
    puntajeCoherente412: null,
    puntajeCoherente413: null,
    puntajeCoherente414: null,
    puntajeCoherente415: null,
    puntajeCoherente416: null,
    puntajeCoherente417: null,
    puntajeCoherente418: null,
    puntajeCoherente419: null,
    puntajeCoherente421: null,
    puntajeCoherente4221: null,
    puntajeCoherente4222: null,
    puntajeNoAdecMeta311: null,
    puntajeNoAdecMeta3121: null,
    puntajeNoAdecMeta3122: null,
    puntajeNoAdecMeta3123: null,
    puntajeNoAdecMeta3131: null,
    puntajeNoAdecMeta3132: null,
    puntajeNoAdecMeta3141: null,
    puntajeNoAdecMeta3142: null,
    puntajeNoAdecMeta3211: null,
    puntajeNoAdecMeta3212: null,
    puntajeNoAdecMeta3213: null,
    puntajeNoAdecMeta3221: null,
    puntajeNoAdecMeta3222: null,
    puntajeNoCoherente4110: null,
    puntajeNoCoherente4111: null,
    puntajeNoCoherente4112: null,
    puntajeNoCoherente412: null,
    puntajeNoCoherente413: null,
    puntajeNoCoherente414: null,
    puntajeNoCoherente415: null,
    puntajeNoCoherente416: null,
    puntajeNoCoherente417: null,
    puntajeNoCoherente418: null,
    puntajeNoCoherente419: null,
    puntajeNoCoherente421: null,
    puntajeNoCoherente4221: null,
    puntajeNoCoherente4222: null,
    puntajeNoConsigna4110: null,
    puntajeNoConsigna4111: null,
    puntajeNoConsigna4112: null,
    puntajeNoConsigna412: null,
    puntajeNoConsigna413: null,
    puntajeNoConsigna414: null,
    puntajeNoConsigna415: null,
    puntajeNoConsigna416: null,
    puntajeNoConsigna417: null,
    puntajeNoConsigna418: null,
    puntajeNoConsigna419: null,
    puntajeNoConsigna421: null,
    puntajeNoConsigna4221: null,
    puntajeNoConsigna4222: null,
    puntajeNoTiene311: null,
    puntajeNoTiene3121: null,
    puntajeNoTiene3122: null,
    puntajeNoTiene3123: null,
    puntajeNoTiene3131: null,
    puntajeNoTiene3132: null,
    puntajeNoTiene3141: null,
    puntajeNoTiene3142: null,
    puntajeNoTiene3211: null,
    puntajeNoTiene3212: null,
    puntajeNoTiene3213: null,
    puntajeNoTiene3221: null,
    puntajeNoTiene3222: null,
  };

  idProcesoOferta!: number;
  idResultadoPP!: number;
  adicional: boolean = false;
  isEdit: boolean = false;
  calificacionTotal!: number;

  constructor(
    private resultadoPPService: ResultadoPPService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.items = [
      {
        label: 'Bandeja evaluación y aprobación de otorgamiento de TH',
        routerLink: '/planificacion/bandeja-proceso-concesion-pfdm',
      },
      { label: 'Criterios de evaluación y calificación' },
    ];

    this.home = { icon: 'pi pi-home', routerLink: '/inicio' };

    this.evaluar.idProcesoPostulacion = JSON.parse(
      '' + localStorage.getItem('evaluar-otorgar-proceso-concesion-pfdm')
    ).idProcesoPostulacion;

    this.idProcesoOferta = JSON.parse(
      '' + localStorage.getItem('evaluar-otorgar-proceso-concesion-pfdm')
    ).idProcesoOferta;

    this.idResultadoPP = JSON.parse(
      '' + localStorage.getItem('evaluar-otorgar-proceso-concesion-pfdm')
    ).idResultadoPP;

    if (this.idResultadoPP > 0) this.obtenerNotas();
    this.obtenerPrimerPostulante();
  }

  obtenerNotas = () => {
    this.resultadoPPService
      .obtenerNotas(this.idResultadoPP)
      .subscribe((resp: any) => {
        this.isEdit = true;
        resp.data.forEach((element: any) => {
          this.calificacionTotal = element.calificacionTotal;
          if (element.codigoSubSeccion === '1.1') {
            this.evaluar.puntaje11 = element.puntajeOtorgado;
          } else if (element.codigoSubSeccion === '1.2') {
            this.evaluar.puntaje12 = element.puntajeOtorgado;
          } else if (element.codigoSubSeccion === '2.1') {
            this.evaluar.puntaje21 = element.puntajeOtorgado;
          } else if (element.codigoSubSeccion === '2.2') {
            this.evaluar.puntaje22 = element.puntajeOtorgado;
          } else if (element.codigoSubSeccion === '3.1.1') {
            this.evaluar.puntajeAdecMeta311 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta311 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene311 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.2.1') {
            this.evaluar.puntajeAdecMeta3121 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3121 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3121 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.2.2') {
            this.evaluar.puntajeAdecMeta3122 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3122 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3122 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.2.3') {
            this.evaluar.puntajeAdecMeta3123 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3123 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3123 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.3.1') {
            this.evaluar.puntajeAdecMeta3131 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3131 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3131 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.3.2') {
            this.evaluar.puntajeAdecMeta3132 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3132 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3132 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.4.1') {
            this.evaluar.puntajeAdecMeta3141 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3141 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3141 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.1.4.2') {
            this.evaluar.puntajeAdecMeta3141 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3142 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3142 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.2.1.1') {
            this.evaluar.puntajeAdecMeta3211 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3211 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3211 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.2.1.2') {
            this.evaluar.puntajeAdecMeta3212 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3212 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3212 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.2.1.3') {
            this.evaluar.puntajeAdecMeta3213 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3213 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3213 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.2.2.1') {
            this.evaluar.puntajeAdecMeta3221 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3221 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3221 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '3.2.2.2') {
            this.evaluar.puntajeAdecMeta3222 = element.puntajeAdecuadoMeta;
            this.evaluar.puntajeNoAdecMeta3222 = element.puntajeNoAdecuadoMeta;
            this.evaluar.puntajeNoTiene3222 = element.puntajeNoTiene;
          } else if (element.codigoSubSeccion === '4.1.1.1') {
            this.evaluar.puntajeCoherente4111 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente4111 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna4111 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.1.2') {
            this.evaluar.puntajeCoherente4112 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente4112 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna4112 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.2') {
            this.evaluar.puntajeCoherente412 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente412 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna412 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.3') {
            this.evaluar.puntajeCoherente413 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente413 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna413 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.4') {
            this.evaluar.puntajeCoherente414 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente414 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna414 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.5') {
            this.evaluar.puntajeCoherente415 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente415 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna415 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.6') {
            this.evaluar.puntajeCoherente416 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente416 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna416 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.7') {
            this.evaluar.puntajeCoherente417 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente417 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna417 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.8') {
            this.evaluar.puntajeCoherente418 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente418 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna418 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.9') {
            this.evaluar.puntajeCoherente419 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente419 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna419 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.1.10') {
            this.evaluar.puntajeCoherente4110 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente4110 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna4110 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.2.1') {
            this.evaluar.puntajeCoherente421 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente421 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna421 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.2.2.1') {
            this.evaluar.puntajeCoherente4221 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente4221 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna4221 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.2.2.2') {
            this.evaluar.puntajeCoherente4222 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente4222 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna4222 = element.puntajeNoConsigna;
          } else if (element.codigoSubSeccion === '4.2.2.1') {
            this.evaluar.puntajeCoherente4221 = element.puntajeCoherente;
            this.evaluar.puntajeNoCoherente4221 = element.puntajeNoCoherente;
            this.evaluar.puntajeNoConsigna4221 = element.puntajeNoConsigna;
          }
        });
      });
  };

  obtenerPrimerPostulante = () => {
    this.resultadoPPService
      .obtenerPrimerPostulante(this.idProcesoOferta)
      .subscribe((resp: any) => {
        if (
          resp?.data?.idProcesoPostulacion === this.evaluar.idProcesoPostulacion
        ) {
          this.adicional = true;
          this.calificacionTotal =
            this.calificacionTotal + 0 + this.montoAdicional;
        } else this.adicional = false;
      });
  };

  registrarNotas = () => {
    if (this.calificacionTotal > 100) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Calificación Total, es mayor a 100',
      });
    } else {
      this.evaluar.idUsuarioRegistro = JSON.parse(
        '' + localStorage.getItem('usuario')
      ).idusuario;
      this.resultadoPPService
        .registrarNotas(this.evaluar)
        .subscribe((result: any) => {
          if (result.success) {
            this.messageService.add({
              key: 'tl',
              severity: 'success',
              summary: 'CORRECTO',
              detail: 'Se guardó correctamente',
            });
            this.router.navigate([
              '/planificacion/bandeja-proceso-concesion-pfdm',
            ]);
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });
    }
  };

  asisTecSelected(event: any): void {
    if (this.asisTec == 1) this.evaluar.puntaje22 = null;
    else this.evaluar.puntaje21 = null;
  }

  onInputExperiencia = (event: any, posicion: number) => {
    if (posicion === 1) this.evaluar.puntaje11 = event.value | event;
    else if (posicion === 2) this.evaluar.puntaje12 = event.value | event;
    this.sumaTotal();
  };

  onInputAsistenciaTecnica = (event: any, posicion: number) => {
    if (posicion === 1) this.evaluar.puntaje21 = event.value | event;
    else if (posicion === 2) this.evaluar.puntaje22 = event.value | event;
    this.sumaTotal();
  };

  onInputMaquinaria = (event: any, posicion: number) => {
    // ;
    if (posicion === 1) this.evaluar.puntajeAdecMeta311 = event.value | event;
    else if (posicion === 2)
      this.evaluar.puntajeNoAdecMeta311 = event.value | event;
    else if (posicion === 3)
      this.evaluar.puntajeAdecMeta3121 = event.value | event;
    else if (posicion === 4)
      this.evaluar.puntajeNoAdecMeta3121 = event.value | event;
    else if (posicion === 5)
      this.evaluar.puntajeAdecMeta3122 = event.value | event;
    else if (posicion === 6)
      this.evaluar.puntajeNoAdecMeta3122 = event.value | event;
    else if (posicion === 7)
      this.evaluar.puntajeAdecMeta3123 = event.value | event;
    else if (posicion === 8)
      this.evaluar.puntajeNoAdecMeta3123 = event.value | event;
    else if (posicion === 9)
      this.evaluar.puntajeAdecMeta3131 = event.value | event;
    else if (posicion === 10)
      this.evaluar.puntajeNoAdecMeta3131 = event.value | event;
    else if (posicion === 11)
      this.evaluar.puntajeAdecMeta3132 = event.value | event;
    else if (posicion === 12)
      this.evaluar.puntajeNoAdecMeta3132 = event.value | event;
    else if (posicion === 13)
      this.evaluar.puntajeAdecMeta3141 = event.value | event;
    else if (posicion === 14)
      this.evaluar.puntajeNoAdecMeta3141 = event.value | event;
    else if (posicion === 15)
      this.evaluar.puntajeAdecMeta3142 = event.value | event;
    else if (posicion === 16)
      this.evaluar.puntajeNoAdecMeta3142 = event.value | event;
    else if (posicion === 17)
      this.evaluar.puntajeAdecMeta3211 = event.value | event;
    else if (posicion === 18)
      this.evaluar.puntajeNoAdecMeta3211 = event.value | event;
    else if (posicion === 19)
      this.evaluar.puntajeAdecMeta3212 = event.value | event;
    else if (posicion === 20)
      this.evaluar.puntajeNoAdecMeta3212 = event.value | event;
    else if (posicion === 21)
      this.evaluar.puntajeAdecMeta3213 = event.value | event;
    else if (posicion === 22)
      this.evaluar.puntajeNoAdecMeta3213 = event.value | event;
    else if (posicion === 23)
      this.evaluar.puntajeAdecMeta3221 = event.value | event;
    else if (posicion === 24)
      this.evaluar.puntajeNoAdecMeta3221 = event.value | event;
    else if (posicion === 25)
      this.evaluar.puntajeAdecMeta3222 = event.value | event;
    else if (posicion === 26)
      this.evaluar.puntajeNoAdecMeta3222 = event.value | event;
    this.sumaTotal();
  };

  onInputInformacionTecnica = (event: any, posicion: number) => {
    if (posicion === 1) this.evaluar.puntajeCoherente4111 = event.value | event;
    else if (posicion === 2)
      this.evaluar.puntajeNoCoherente4111 = event.value | event;
    else if (posicion === 3)
      this.evaluar.puntajeCoherente4112 = event.value | event;
    else if (posicion === 4)
      this.evaluar.puntajeNoCoherente4112 = event.value | event;
    else if (posicion === 5)
      this.evaluar.puntajeCoherente412 = event.value | event;
    else if (posicion === 6)
      this.evaluar.puntajeNoCoherente412 = event.value | event;
    else if (posicion === 7)
      this.evaluar.puntajeCoherente413 = event.value | event;
    else if (posicion === 8)
      this.evaluar.puntajeNoCoherente413 = event.value | event;
    else if (posicion === 9)
      this.evaluar.puntajeCoherente414 = event.value | event;
    else if (posicion === 10)
      this.evaluar.puntajeNoCoherente414 = event.value | event;
    else if (posicion === 11)
      this.evaluar.puntajeCoherente415 = event.value | event;
    else if (posicion === 12)
      this.evaluar.puntajeNoCoherente415 = event.value | event;
    else if (posicion === 13)
      this.evaluar.puntajeCoherente416 = event.value | event;
    else if (posicion === 14)
      this.evaluar.puntajeNoCoherente416 = event.value | event;
    else if (posicion === 15)
      this.evaluar.puntajeCoherente417 = event.value | event;
    else if (posicion === 16)
      this.evaluar.puntajeNoCoherente417 = event.value | event;
    else if (posicion === 17)
      this.evaluar.puntajeCoherente418 = event.value | event;
    else if (posicion === 18)
      this.evaluar.puntajeNoCoherente418 = event.value | event;
    else if (posicion === 19)
      this.evaluar.puntajeCoherente419 = event.value | event;
    else if (posicion === 20)
      this.evaluar.puntajeNoCoherente419 = event.value | event;
    else if (posicion === 21)
      this.evaluar.puntajeCoherente4110 = event.value | event;
    else if (posicion === 22)
      this.evaluar.puntajeNoCoherente4110 = event.value | event;

    this.sumaTotal();
  };

  onInputInformacionNegocios = (event: any, posicion: number) => {
    if (posicion === 1) this.evaluar.puntajeCoherente421 = event.value | event;
    else if (posicion === 2)
      this.evaluar.puntajeNoCoherente421 = event.value | event;
    else if (posicion === 3)
      this.evaluar.puntajeCoherente4221 = event.value | event;
    else if (posicion === 4)
      this.evaluar.puntajeNoCoherente4221 = event.value | event;
    else if (posicion === 5)
      this.evaluar.puntajeCoherente4222 = event.value | event;
    else if (posicion === 6)
      this.evaluar.puntajeNoCoherente4222 = event.value | event;
    this.sumaTotal();
  };

  sumarProyecto = (posicion: number) => {
    // ;
    if (
      this.evaluar.puntajeCoherente4222 +
        0 +
        this.evaluar.puntajeCoherente4221 +
        0 >
      12
    ) {
      if (posicion === 1) {
        this.evaluar.puntajeCoherente4221 =
          12 - this.evaluar.puntajeCoherente4222;
      } else if (posicion === 2) {
        this.evaluar.puntajeCoherente4222 =
          12 - this.evaluar.puntajeCoherente4221;
      } else if (posicion === 3) {
        this.evaluar.puntajeNoCoherente4221 =
          12 - this.evaluar.puntajeNoCoherente4222;
      } else if (posicion === 4) {
        this.evaluar.puntajeNoCoherente4222 =
          12 - this.evaluar.puntajeNoCoherente4221;
      }
    }
  };

  sumaTotal = () => {
    this.calificacionTotal =
      this.evaluar.puntaje11 +
      0 +
      (this.evaluar.puntaje12 + 0) +
      (this.evaluar.puntaje21 + 0) +
      (this.evaluar.puntaje22 + 0) +
      (this.evaluar.puntajeAdecMeta311 + 0) +
      (this.evaluar.puntajeNoAdecMeta311 + 0) +
      (this.evaluar.puntajeAdecMeta3121 + 0) +
      (this.evaluar.puntajeNoAdecMeta3121 + 0) +
      (this.evaluar.puntajeAdecMeta3122 + 0) +
      (this.evaluar.puntajeNoAdecMeta3122 + 0) +
      (this.evaluar.puntajeAdecMeta3123 + 0) +
      (this.evaluar.puntajeNoAdecMeta3123 + 0) +
      (this.evaluar.puntajeAdecMeta3131 + 0) +
      (this.evaluar.puntajeNoAdecMeta3131 + 0) +
      (this.evaluar.puntajeAdecMeta3132 + 0) +
      (this.evaluar.puntajeNoAdecMeta3132 + 0) +
      (this.evaluar.puntajeAdecMeta3141 + 0) +
      (this.evaluar.puntajeNoAdecMeta3141 + 0) +
      (this.evaluar.puntajeAdecMeta3142 + 0) +
      (this.evaluar.puntajeNoAdecMeta3142 + 0) +
      (this.evaluar.puntajeAdecMeta3211 + 0) +
      (this.evaluar.puntajeNoAdecMeta3211 + 0) +
      (this.evaluar.puntajeAdecMeta3212 + 0) +
      (this.evaluar.puntajeNoAdecMeta3212 + 0) +
      (this.evaluar.puntajeAdecMeta3213 + 0) +
      (this.evaluar.puntajeNoAdecMeta3213 + 0) +
      (this.evaluar.puntajeAdecMeta3221 + 0) +
      (this.evaluar.puntajeNoAdecMeta3221 + 0) +
      (this.evaluar.puntajeNoAdecMeta3222 + 0) +
      (this.evaluar.puntajeCoherente4111 + 0) +
      (this.evaluar.puntajeNoCoherente4111 + 0) +
      (this.evaluar.puntajeCoherente4112 + 0) +
      (this.evaluar.puntajeNoCoherente4112 + 0) +
      (this.evaluar.puntajeCoherente412 + 0) +
      (this.evaluar.puntajeNoCoherente412 + 0) +
      (this.evaluar.puntajeCoherente413 + 0) +
      (this.evaluar.puntajeNoCoherente413 + 0) +
      (this.evaluar.puntajeCoherente414 + 0) +
      (this.evaluar.puntajeNoCoherente414 + 0) +
      (this.evaluar.puntajeCoherente415 + 0) +
      (this.evaluar.puntajeNoCoherente415 + 0) +
      (this.evaluar.puntajeCoherente416 + 0) +
      (this.evaluar.puntajeNoCoherente416 + 0) +
      (this.evaluar.puntajeCoherente417 + 0) +
      (this.evaluar.puntajeNoCoherente417 + 0) +
      (this.evaluar.puntajeCoherente418 + 0) +
      (this.evaluar.puntajeNoCoherente418 + 0) +
      (this.evaluar.puntajeCoherente419 + 0) +
      (this.evaluar.puntajeNoCoherente419 + 0) +
      (this.evaluar.puntajeCoherente4110 + 0) +
      (this.evaluar.puntajeNoCoherente4110 + 0) +
      (this.evaluar.puntajeCoherente421 + 0) +
      (this.evaluar.puntajeNoCoherente421 + 0) +
      (this.evaluar.puntajeCoherente4221 + 0) +
      (this.evaluar.puntajeCoherente4222 + 0) +
      (this.evaluar.puntajeNoCoherente4221 + 0) +
      (this.evaluar.puntajeNoCoherente4222 + 0);

    if (this.adicional) {
      this.calificacionTotal =
        this.calificacionTotal + (this.calificacionTotal * 10) / 100;
    }
  };
}
