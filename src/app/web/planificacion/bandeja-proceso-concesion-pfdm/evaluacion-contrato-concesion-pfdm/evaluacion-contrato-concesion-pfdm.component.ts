import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-evaluacion-contrato-concesion-pfdm',
  templateUrl: './evaluacion-contrato-concesion-pfdm.component.html',
  styleUrls: ['./evaluacion-contrato-concesion-pfdm.component.scss']
})
export class EvaluacionContratoConcesionPfdmComponent implements OnInit {
  listItems!: MenuItem[];
  homes!: MenuItem;

  stylesClass: any = {
    w100px: 'evalTu',
  };
  constructor() { }

  ngOnInit(): void {

    this.listItems = [
      {
        label: 'Bandeja evaluación y aprobación de otorgamiento de TH',
        routerLink: '/planificacion/bandeja-proceso-concesion-pfdm',
      },
      { label: 'Generar Resolución' },
    ];

    this.homes = { icon: 'pi pi-home', routerLink: '/inicio' };

  }

}
