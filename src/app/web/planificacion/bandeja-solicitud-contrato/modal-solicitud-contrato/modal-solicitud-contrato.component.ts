import { Component, OnInit } from '@angular/core';
import { ConvertDateToString, ConvertNumberToDate, DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FiscalizacionService } from 'src/app/service/fiscalizacion/fiscalizacion.service';
import { ContratoService } from 'src/app/service/contrato.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { ArchivoService } from '@services';
import { GenericoService } from 'src/app/service/generico.service';
import { CodigoEstadoContrato } from 'src/app/model/util/CodigoEstadoContrato';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-modal-solicitud-contrato',
  templateUrl: './modal-solicitud-contrato.component.html',
})
export class ModalSolicitudContratoComponent implements OnInit {
  TipoArchivo = TiposArchivos;
  codigoEstadoContrato = CodigoEstadoContrato;

  comboTipoDocumento: any[] = [];
  listEstadoContrato: any[] = [];

  expediente: any = {};
  listadoAdjuntos: any[] = [];

  fiscalizacion: any = {};
  fileFiscalizacion: any = {};

  fileArchivosOtro: any = {};
  fileOtros: any[] = [];
  listaTablaOtros: any[] = [];
  otrosDocumentos: any = {}

  fiscalizacionCheck: boolean = false;
  minDate!: Date;

  constructor(
    private toast: ToastService,
    public config: DynamicDialogConfig,
    private dialog: MatDialog,
    public ref: DynamicDialogRef,
    private confirmationService: ConfirmationService,
    private fiscalizacionService: FiscalizacionService,
    private contratoService: ContratoService,
    private archivoServ: ArchivoService,
    private genericoService: GenericoService,
  ) { }

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear, 0, 1);

    this.fiscalizacion.emite = false;
    this.fiscalizacion.idFiscalizacion = this.config.data.item.idFiscalizacion;
    this.fiscalizacion.idContrato = this.config.data.item.idContrato;
    this.fiscalizacion.idResultadoPP = this.config.data.item.idResultadoPP;
    this.fiscalizacion.idUsuarioRegistro = this.config.data.idUsuario;
    this.fiscalizacion.estadoContrato = this.config.data.item.codigoEstadoContrato;

    this.listarComboTipoDocumentos();
    this.listarEstadoContrato();
    this.obtenerFiscalizacion();
    this.listarContratoArchivo();
  }

  listarComboTipoDocumentos() {
    const params = { prefijo: 'TDOCGE' }
    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success) {
        result.data.splice(0, 0, { codigo: null, valorPrimario: '-- Seleccione --' });
        this.comboTipoDocumento = result.data;
      }
    });
  }

  listarEstadoContrato() {
    const auxEtados: string[] = [
      CodigoEstadoContrato.VIGENTE, CodigoEstadoContrato.CADUCADO, CodigoEstadoContrato.RENOVADO,
      CodigoEstadoContrato.AMPLIADO, CodigoEstadoContrato.EXTINGUIDO
    ];
    const params = { prefijo: 'ECONTR' };
    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success) {
        const newData = result.data.filter((item: any) => auxEtados.includes(item.codigo));
        newData.splice(0, 0, { codigo: null, valorPrimario: '-- Seleccione --' });
        this.listEstadoContrato = newData;
      }
    });
  }

  listarContratoArchivo() {
    const params = { "idContrato": this.fiscalizacion.idContrato, "prefijo": 'TDOCGE' };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.listarContratoArchivo(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success) {
        this.listadoAdjuntos = result.data;
      }
    }, () => {
      this.dialog.closeAll();
    });
  };

  obtenerFiscalizacion() {
    const params = { "idFiscalizacion": this.fiscalizacion.idFiscalizacion, "idContrato": this.fiscalizacion.idContrato };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.fiscalizacionService.obtenerFiscalizacion(params).subscribe((result) => {
      this.dialog.closeAll();

      // EXPEDIENTE
      this.expediente.numero = result.data.numeroExpediente ? result.data.numeroExpediente : "";
      this.expediente.fecha = result.data.fechaPresentacion ? ConvertDateToString(ConvertNumberToDate(result.data.fechaPresentacion)) : "";

      // INFORME FISCALIZACIÓN
      this.fiscalizacion.numeroInforme = result.data.numeroInforme;
      this.fiscalizacion.asunto = result.data.asunto;
      this.fiscalizacion.fechaInforme = result.data.fechaInforme ? ConvertNumberToDate(result.data.fechaInforme) : null;

      // RESULTADOS
      this.fiscalizacionCheck = result.data.resultado ? true : false;
      this.fileFiscalizacion.comentario = result.data.comentario;
      this.fiscalizacion.emite = result.data.emite ? result.data.emite : false;

      // ARCHICO FISCAL
      let auxFiscal = result.data.listaFiscalizacionArchivo.find((item: any) => item.tipoDocumento === TiposArchivos.FISCALIZACION);
      if (auxFiscal) {
        this.fileFiscalizacion.nombre = auxFiscal.nombre;
        this.fileFiscalizacion.idArchivo = auxFiscal.idArchivo;
        this.fileFiscalizacion.idFiscalizacionArchivo = auxFiscal.idFiscalizacionArchivo;
      }

      // ARCHICO OTROS
      let auxOtros: any[] = result.data.listaFiscalizacionArchivo.filter((item: any) => item.tipoDocumento !== TiposArchivos.FISCALIZACION);
      if (auxOtros.length > 0) {
        auxOtros.forEach(archivo => {
          archivo.desTipodoc = this.comboTipoDocumento.find(item => item.codigo === archivo.tipoDocumento.trim()).valorPrimario;
        })
        this.listaTablaOtros = auxOtros;
      }

    }, (error) => this.errorMensaje(error, true));
  };

  eliminarArchivoRegistrado(idArchivo: number, isFileFiscal: boolean) {
    const params = { "idArchivo": idArchivo, "idUsuarioElimina": this.fiscalizacion.idUsuarioRegistro };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.fiscalizacionService.eliminarFiscalizacionArchivo(params).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok('Se eliminó el archivo correctamente.');
        if (isFileFiscal) {
          this.fileFiscalizacion = {};
        } else {
          this.listaTablaOtros = this.listaTablaOtros.filter(item => item.idArchivo !== idArchivo);
        }
        this.eliminarArchivoGeneral(idArchivo, "");
      } else {
        this.toast.warn('Ocurrió un error al realizar la operación.');
      }
    }, (error) => this.errorMensaje(error, true));
  }

  eliminarArchivoGeneral(idAdjunto: number, codigo: string) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.eliminarArchivo(idAdjunto, this.fiscalizacion.idUsuarioRegistro).subscribe((result) => {
      this.dialog.closeAll();
      if (codigo !== "") {
        if (result.success) {
          this.toast.ok('Se eliminó el archivo correctamente.');
          if (codigo === this.TipoArchivo.FISCALIZACION) {
            this.fileFiscalizacion = {};
          } else if (codigo === this.TipoArchivo.FISCA_OTROS) {
            this.fileArchivosOtro = {};
          } else if (codigo === "tablaOtros") {
            this.listaTablaOtros = this.listaTablaOtros.filter(item => item.idArchivo !== idAdjunto);
          }
        } else {
          this.toast.warn('Ocurrió un error al realizar la operación.');
        }
      }
    }, (error) => {
      this.dialog.closeAll();
      if (codigo !== "") this.errorMensaje(error)
    });
  }

  registrarArchivo(file: any, codigo: string) {
    if (!codigo) {
      this.toast.warn('(*) Debe Seleccionar un tipo de documento.');
      this.fileArchivosOtro = {};
      return;
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.config.data.idUsuario, codigo, file.file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok('Se registró el archivo correctamente.');
        if (codigo === this.TipoArchivo.FISCALIZACION) {
          this.fileFiscalizacion.idArchivo = result.data;
        } else {
          this.fileArchivosOtro.idArchivo = result.data;
        }
      } else {
        this.toast.warn(result.message);
        if (codigo === this.TipoArchivo.FISCALIZACION) {
          this.fileFiscalizacion = {};
        } else {
          this.fileArchivosOtro = {};
        }
      }
    }, (error) => {
      if (codigo === this.TipoArchivo.FISCALIZACION) {
        this.fileFiscalizacion = {};
      } else {
        this.fileArchivosOtro = {};
      }
      this.errorMensaje(error, true)
    });
  }

  actualizarEstadoContrato() {
    let params = {
      "idContrato": this.fiscalizacion.idContrato,
      "codigoEstadoContrato": this.fiscalizacion.estadoContrato,
      "idUsuarioModificacion": this.config.data.idUsuario
    };
    this.contratoService.actualizarEstadoContrato(params).subscribe((result: any) => {
      if (result.success) {
        this.ref.close(true);
      }
    }, (error) => this.errorMensaje(error));
  };

  //INICIO REGISTRAR
  validarRegistarFisca(): boolean {
    let validado = true;

    if (!this.fiscalizacion.numeroInforme) {
      this.toast.warn('(*) Debe ingresar número de informe.');
      validado = false;
    }
    if (!this.fiscalizacion.fechaInforme) {
      this.toast.warn('(*) Debe ingresar fecha de informe.');
      validado = false;
    }
    if (!this.fiscalizacion.asunto) {
      this.toast.warn('(*) Debe ingresar asunto de informe.');
      validado = false;
    }
    if (!this.fileFiscalizacion.nombre) {
      this.toast.warn('(*) Debe adjuntar informe de fiscalización.');
      validado = false;
    }

    return validado;
  };

  registrarFiscalizacion(isMensajeEmitir: boolean) {
    let auxArchivos = [{
      "idFiscalizacion": this.fiscalizacion.idFiscalizacion,
      "idFiscalizacionArchivo": this.fileFiscalizacion.idFiscalizacionArchivo ? this.fileFiscalizacion.idFiscalizacionArchivo : null,
      "idArchivo": this.fileFiscalizacion.idArchivo,
      "descripcion": "",
      "asunto": "",
      "idUsuarioRegistro": this.fiscalizacion.idUsuarioRegistro
    }];

    if (this.listaTablaOtros.length > 0) {
      const auxOtros = this.listaTablaOtros.filter(item => item.idFiscalizacionArchivo === null);
      auxArchivos.push(...auxOtros);
    }

    const params = {
      "idFiscalizacion": this.fiscalizacion.idFiscalizacion,
      "idContrato": this.fiscalizacion.idContrato,
      "asunto": this.fiscalizacion.asunto,
      "numeroInforme": this.fiscalizacion.numeroInforme,
      "fechaInforme": this.fiscalizacion.fechaInforme,
      "idResultadoPP": this.fiscalizacion.idResultadoPP,
      "idUsuarioRegistro": this.fiscalizacion.idUsuarioRegistro,
      "listaFiscalizacionArchivo": auxArchivos,
      "emite": this.fiscalizacion.emite,
      //Datos para emitir resultados
      "resultado": isMensajeEmitir ? this.fiscalizacionCheck : null,
      "comentario": this.fiscalizacionCheck ? this.fiscalizacion.comentario : null,
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.fiscalizacionService.registrarFiscalizacion(params).subscribe((result) => {
      if (result.success) {
        this.dialog.closeAll();
        if (isMensajeEmitir) {
          this.toast.ok('Se emitió los resultados correctamente');
          this.actualizarEstadoContrato();
        } else {
          this.toast.ok(result.message);
          this.ref.close(true);
        }
      }
    }, (error) => this.errorMensaje(error, true));
  };
  //FIN REGISTRAR

  //INICIO OTROS DOCUMENTOS
  validarAgregar() {
    let validado = true;
    if (!this.otrosDocumentos.tipoDoc) {
      this.toast.warn('(*) Debe ingresar tipo de documento.');
      validado = false;
    }

    if (!this.otrosDocumentos.asunto) {
      this.toast.warn('(*) Debe ingresar asunto.');
      validado = false;
    }

    if (!this.otrosDocumentos.descripcion) {
      this.toast.warn('(*) Debe ingresar descripción.');
      validado = false;
    }

    if (!this.fileArchivosOtro.nombre) {
      this.toast.warn('(*) Debe ingresar documento.');
      validado = false;
    }

    return validado;
  }

  btnAgregar() {
    if (!this.validarAgregar()) return;

    this.listaTablaOtros.push({
      idFiscalizacionArchivo: null,
      idFiscalizacion: this.fiscalizacion.idFiscalizacion,
      idArchivo: this.fileArchivosOtro.idArchivo,
      descripcion: this.otrosDocumentos.descripcion,
      asunto: this.otrosDocumentos.asunto,
      idUsuarioRegistro: this.fiscalizacion.idUsuarioRegistro,
      desTipodoc: this.comboTipoDocumento.find(item => item.codigo === this.otrosDocumentos.tipoDoc).valorPrimario,
      nombre: this.fileArchivosOtro.nombre,
    });

    this.otrosDocumentos.tipoDoc = null;
    this.otrosDocumentos.asunto = '';
    this.otrosDocumentos.descripcion = '';
    this.fileArchivosOtro = {};
  };
  //FIN OTROS DOCUMENTOS



  btnRegistrarFiscalizacion(isMensajeEmitir: boolean) {
    if (!this.validarRegistarFisca()) return;
    this.registrarFiscalizacion(isMensajeEmitir);
  }

  btnEmitirResult() {
    if (!this.validarEmitir()) return;
    this.fiscalizacion.emite = true;
    this.btnRegistrarFiscalizacion(true);
  }

  validarEmitir(): boolean {
    let validado = true;

    if (this.fiscalizacionCheck && !this.fiscalizacion.comentario) {
      this.toast.warn('(*) Debe ingresar el comentario.');
      validado = false;
    }

    if (!this.fiscalizacion.estadoContrato) {
      this.toast.warn('(*) Debe seleccionar el estado del contrato.');
      validado = false;
    }

    return validado;
  }

  btnEliminarArchivoAdjunto(file: any, codigo: string) {
    if (!file.idArchivo) return;

    if (!file.idFiscalizacionArchivo) {
      this.eliminarArchivoGeneral(file.idArchivo, codigo);
    } else {
      this.eliminarArchivoRegistrado(file.idArchivo, true);
    }
  }

  btnEliminarArchivo(event: any, fila: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (!fila.idFiscalizacionArchivo) {
          this.eliminarArchivoGeneral(fila.idArchivo, "tablaOtros");
        } else {
          this.eliminarArchivoRegistrado(fila.idArchivo, false);
        }
      },
    });
  }

  btnDescargarArchivoOtros(fila: any) {
    let params = { "idArchivo": fila.idArchivo };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data) DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
      else this.toast.warn("No se encontro el documento.");
    }, (error) => this.errorMensaje(error, true))
  }

  btnDescargarArchivoContrato(fila: any) {
    let params = { "idArchivo": fila.idArchivo };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.success && result.data) DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
      else this.toast.warn("No se encontro el documento.");
    }, (error) => this.errorMensaje(error, true));
  };

  btnDescargarPlantFiscal() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.fiscalizacionService.descagarPlantillaFormatoFiscalizacion().subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, (error) => this.errorMensaje(error, true));
  }

  changeCheck() {
    if (this.fiscalizacionCheck) {
      this.fiscalizacion.estadoContrato = CodigoEstadoContrato.EXTINGUIDO;
    } else {
      this.fiscalizacion.estadoContrato = this.config.data.item.codigoEstadoContrato;
      this.fiscalizacion.comentario = null;
    }
  }

  cerrarModal() {
    this.ref.close();
  }

  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
