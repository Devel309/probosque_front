import { Component, OnInit } from '@angular/core';
import { ConvertDateToString, ConvertNumberToDate, ToastService } from '@shared';
import { SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FiscalizacionService } from 'src/app/service/fiscalizacion/fiscalizacion.service';
import { ModalSolicitudContratoComponent } from './modal-solicitud-contrato/modal-solicitud-contrato.component';
import { GenericoService } from 'src/app/service/generico.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-bandeja-solicitud-contrato',
  templateUrl: './bandeja-solicitud-contrato.component.html',
})
export class BandejaSolicitudContratoComponent implements OnInit {
  lstFiscalizacion: any[] = [];
  procesoPostulacionSelect!: String | null;
  selectedestadocontrato!: SelectItem | null;
  ref!: DynamicDialogRef;

  listEstadoContrato: any[] = [];
  optionPage: any = { pageNum: 1, pageSize: 10 };
  totalRecords: number = 0;

  usuario!: UsuarioModel;

  constructor(
    private toast: ToastService,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private fiscalizacionService: FiscalizacionService,
    private genericoService: GenericoService,
    private usuarioServ: UsuarioService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit() {
    this.listarFiscalizacion();
    this.listarEstadoContrato();
  }

  listarEstadoContrato() {
    let params = { prefijo: 'ECONTR' }
    this.genericoService.listarPorFiltroParametro(params).subscribe((result: any) => {
      if (result.success) {
        result.data.splice(0, 0, { codigo: null, valorPrimario: '-- Seleccione --' });
        this.listEstadoContrato = result.data;
      }
    });
  }

  listarFiscalizacion = () => {
    const params = {
      "codigoTituloH": null,
      "conObservacion": null,
      "idFiscalizacion": null,
      "idProcesoPostulacion": this.procesoPostulacionSelect ? this.procesoPostulacionSelect : null,
      "codigoEstadoContrato": this.selectedestadocontrato ? this.selectedestadocontrato : null,
      "pageNum": this.optionPage.pageNum,
      "pageSize": this.optionPage.pageSize
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.fiscalizacionService.listarFiscalizacion(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success && resp.data.length > 0) {
        resp.data.forEach((element: any) => {
          if (element.fechaInicioContrato != null) {
            element.fechaInicioContrato = ConvertDateToString(ConvertNumberToDate(element.fechaInicioContrato));
          }
          if (element.fechaFinContrato != null) {
            element.fechaFinContrato = ConvertDateToString(ConvertNumberToDate(element.fechaFinContrato));
          }
        });
        this.lstFiscalizacion = resp.data;
        this.totalRecords = resp.totalRecord;
      } else {
        this.lstFiscalizacion = [];
        this.totalRecords = 0;
        this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      }
    }, (error) => this.errorMensaje(error, true));
  };

  verDetalle = (item: any) => {
    this.ref = this.dialogService.open(ModalSolicitudContratoComponent, {
      header: 'Contrato: '.concat(item.codigoTituloH),
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        idUsuario: this.usuario.idusuario
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.listarFiscalizacion();
      }
    });
  };

  btnBuscar() {
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.listarFiscalizacion();
  }

  btnLmipiar() {
    this.procesoPostulacionSelect = null;
    this.selectedestadocontrato = null;
    this.optionPage = { pageNum: 1, pageSize: 10 };
    this.listarFiscalizacion();
  }

  loadData(event: any) {
    this.optionPage.pageNum = event.first + 1;
    this.optionPage.pageSize = event.rows;
    this.listarFiscalizacion();
  }

  errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }
}
