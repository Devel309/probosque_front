import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { ConvertDateToString, ConvertNumberToDate } from 'src/app/shared/util';

@Component({
  selector: 'app-bandeja-otorgamiento-pfdm',
  templateUrl: './bandeja-otorgamiento-pfdm.component.html',
  styleUrls: ['./bandeja-otorgamiento-pfdm.component.scss'],
})
export class BandejaOtorgamientoPfdmComponent implements OnInit {
  listaResultadoPP: any[] = [];
  procesoPostulacionSelect!: String;
  estadoevaluacion: SelectItem[];
  selectestadoevaluacion!: SelectItem;
  ref!: DynamicDialogRef;

  constructor(
    private procesoPostulaionService: ProcesoPostulaionService,
    private route: Router,
    private resultadoPPService: ResultadoPPService,
    public dialogService: DialogService
  ) {
    this.estadoevaluacion = [
      { label: '-- Seleccione --', value: null },
      { label: 'Ganador', value: true },
      { label: 'Pendiente envio resolución', value: false },
    ];
  }

  procesoPostulacion: any[] = [];

  ngOnInit(): void {
    this.listarProcesoPostulacion();
    localStorage.removeItem('evaluacion-otorgamiento-tu');
    this.cargarPP();
  }

  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: 8,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          idProcesoPostulacion: '-- Seleccione --',
        });
        this.procesoPostulacion = resp.data;
      });
  };

  cargarPP = () => {
    var form = {
      idProcesoOferta: null,
      idProcesoPostulacion:
        this.procesoPostulacionSelect == null ||
        this.procesoPostulacionSelect == '-- Seleccione --'
          ? null
          : this.procesoPostulacionSelect,
      idUsuarioPostulacion: null,
      ganador: this.selectestadoevaluacion,
    };
    this.resultadoPPService.listarPPEvaluacion(form).subscribe((resp: any) => {
      resp.data.forEach((element: any) => {
        if (element.fechaPostulacion != null)
          element.fechaPostulacionString = ConvertDateToString(
            ConvertNumberToDate(element.fechaPostulacion)
          );
      });

      this.listaResultadoPP = resp.data;
    });
  };

  generarResolucion = () => {
    this.route.navigate([
      '/planificacion/generar-resolucion-otorgamiento-pfdm',
    ]);
  };
}
