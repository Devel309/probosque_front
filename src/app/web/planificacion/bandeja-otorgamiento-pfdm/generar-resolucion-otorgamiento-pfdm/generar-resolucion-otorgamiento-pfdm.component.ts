import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { ModalResolucionOtorgamientoPfdmComponent } from '../modal-resolucion-otorgamiento-pfdm/modal-resolucion-otorgamiento-pfdm.component';

@Component({
  selector: 'app-generar-resolucion-otorgamiento-pfdm',
  templateUrl: './generar-resolucion-otorgamiento-pfdm.component.html',
})
export class GenerarResolucionOtorgamientoPfdmComponent implements OnInit {
  idProcesoOferta!: number;

  listItems!: MenuItem[];
  homes!: MenuItem;

  procesosOfertas: any[] = [];

  listaResultado: any[] = [];

  ref!: DynamicDialogRef;
  listaProcesos: any[] = [];

  constructor(
    private resultadoPPService: ResultadoPPService,
    private messageService: MessageService,
    public dialogService: DialogService,
    private serv: PlanificacionService
  ) {}

  ngOnInit() {

    this.listItems = [
      {
        label: 'Bandeja evaluación y aprobación de otorgamiento de TH',
        routerLink: '/planificacion/bandeja-otorgamiento-pfdm',
      },
      { label: 'Generar Resolución' },
    ];

    this.homes = { icon: 'pi pi-home', routerLink: '/inicio' };

    this.listarProcesosOferta();
  }

  listarProcesosOferta = () => {
    this.resultadoPPService.listarProcesosOferta().subscribe((resp: any) => {
      this.procesosOfertas = resp.data;
      this.idProcesoOferta = resp.data[0].idProcesoOferta;
      this.listarPPEvaluacion();
    });
  };

  enviar = () => {
    var nota = this.listaResultado.filter((x) => x.notaTotal === 0);

    let cantidadPP = this.listaResultado.length;
    let cantidadProceso = this.listaProcesos.length;
    ;
    if (cantidadPP != cantidadProceso) {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'Alerta',
        detail:
          'Procesos de Postulación pendientes en la bandeja de postulación',
      });
      return;
    } else if (nota.length > 0) {
      this.messageService.add({
        key: 'tl',
        severity: 'warn',
        summary: 'Alerta',
        detail:
          'Existen procesos de postulación pendientes de evaluación técnica',
      });
      return;
    }
    this.modalResolucion();
  };

  modalResolucion = () => {
    this.ref = this.dialogService.open(ModalResolucionOtorgamientoPfdmComponent, {
      header: 'Enviar Resolución',
      width: '30%',
      contentStyle: { 'min-height': '400px', overflow: 'auto' },
      data: {
        listaResultado: this.listaResultado,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if (resp) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se envió correctamente',
        });
        this.listarProcesosOferta();
      }
    });
  };

  listarPPEvaluacion = () => {
    var parms = {
      idProcesoOferta: this.idProcesoOferta,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
      ganador: null,
    };
    this.resultadoPPService.listarPPEvaluacion(parms).subscribe((resp: any) => {
      var objMaxim: any = {};
      resp.data.forEach((element: any) => {
        ;
        if (element.notaTotal > 0) {
          if (objMaxim.notaTotal < element.notaTotal) objMaxim = element;
          else if(objMaxim.notaTotal > element.notaTotal) objMaxim = objMaxim;
          else objMaxim = element;
        }
      });

      resp.data.forEach((element: any) => {
        if (element.idProcesoPostulacion === objMaxim.idProcesoPostulacion)
          element.background = '#bdf1a87a';
      });
      this.listaResultado = resp.data;
      this.listarProcesoPostulacion();
    });
  };

  listarProcesoPostulacion = () => {
    var parms = {
      // idProcesoOferta: this.idProcesoOferta,
      // idProcesoPostulacion: null,
      // idUsuarioPostulacion: null,
      // ganador: null,

      idPerfil: null,
      idProcesoOferta: this.idProcesoOferta,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };
    this.serv.listarProcesoPostulacion(parms).subscribe((resp: any) => {
      this.listaProcesos = resp.data;
    });
  };
}
