import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { DownloadFile } from 'src/app/shared/util';

@Component({
  selector: 'app-modal-resolucion-otorgamiento-pfdm',
  templateUrl: './modal-resolucion-otorgamiento-pfdm.component.html',
})
export class ModalResolucionOtorgamientoPfdmComponent implements OnInit {
  idDocumentoAdjunto!: number;
  file!: any;
  autoResize: boolean = true;
  j: number = 0;
  asunto: string = '';
  contenidoEmail: string = '';

  nombreArchivo: string = '';
  documento: string = '';

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private resultadoPPService: ResultadoPPService,
    private serv: PlanificacionService,
    private messageService: MessageService,
    private seguridadService: SeguridadService
  ) {}

  ngOnInit() {
    if (this.config.data.isView) this.obtenerResultados();
  }

  obtenerResultados = () => {
    this.resultadoPPService
      .obtenerResultados(this.config.data.item.idResultadoPP)
      .subscribe((resp: any) => {
        this.nombreArchivo = resp.data.nombredocumento;
        this.documento = resp.data.documento;
        this.asunto = resp.data.resultado.asuntoEmail;
        this.contenidoEmail = resp.data.resultado.contenidoEmail;
      });
  };

  descargarArchivo = () => {
    DownloadFile(
      this.documento,
      this.nombreArchivo,
      'application/octet-stream'
    );
  };

  enviar = () => {
    this.enviarArchivo();
  };

  enviarArchivo = () => {
    var params = new HttpParams()
      .set('CodigoAnexo', 'Resolucion')
      .set('IdTipoDocumento', '18')
      // .set(
      //   'IdProcesoPostulacion',
      //   this.solicitudRequestEntity.idProcesoPostulacion
      // )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.file.name);

    const formData = new FormData();
    formData.append('file', this.file);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.buscarUsuario();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  cargarArchivo(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.file = e.srcElement.files[0];
      }
    }
  }

  buscarUsuario = () => {
    this.j = 1;
    this.config.data.listaResultado.forEach((element: any) => {
      var params = {
        idusuario: element.idUsuarioPostulacion,
      };
      this.seguridadService.obtenerUsuario(params).subscribe((resp: any) => {
        element.correoPostulante = resp.data[0].correoElectronico;
        this.registrarResultados(element);
      });
    });
  };

  registrarResultados = (element: any) => {
    // ;
    var parms = {
      asuntoEmail: this.asunto,
      contenidoEmail: this.contenidoEmail,
      correoPostulante: element.correoPostulante,
      ganador: !element.background ? false : true,
      idAutoridadRegional: 33,
      idDocumentoAdjunto: this.idDocumentoAdjunto,
      idResultadoPP: element.idResultadoPP,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
      idProcesoPostulacion: element.idProcesoPostulacion,
    };

    this.resultadoPPService
      .registrarResultados(parms)
      .subscribe((data: any) => {
        // ;
        if (this.config.data.listaResultado.length === this.j) {
          if (data.success) this.ref.close(true);
        } else this.j = this.j + 1;
      });
  };
}
