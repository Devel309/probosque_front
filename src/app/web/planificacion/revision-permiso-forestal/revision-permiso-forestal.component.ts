import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from '@services';
import { MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import { Perfiles } from '../../../model/util/Perfiles';
import { HttpParams } from '@angular/common/http';
import { PermisoForestalService } from '../../../service/permisoForestal.service';
import { CodigoPermisoForestal } from '../../../model/util/CodigoPermisoForestal';

@Component({
  selector: 'revision-permiso-forestal',
  templateUrl: './revision-permiso-forestal.component.html',
  styleUrls: ['./revision-permiso-forestal.component.scss'],
  providers: [MessageService],
})
export class RevisionPermisosForestales implements OnInit {
  idPermisoForestal: number = 0;
  tabIndex: number = 0;
  usuario!: UsuarioModel;
  //isPerfilArffs: boolean = false;
  comunidad: string = '';
  codigoEstadoPermisoForestal: string = '';
  nroDocumento: string = '';

  sunat: boolean = false;
  sunarp: boolean = false;

  CodigoPermisoForestal = CodigoPermisoForestal;
  Perfiles = Perfiles;
  showRegresar: boolean = true;

  constructor(
    private route: ActivatedRoute,
    public dialogService: DialogService,
    private userSer: UsuarioService,
    private permisoForestalService: PermisoForestalService
  ) {
    this.usuario = userSer.usuario;
    this.showRegresar = (this.usuario.sirperfil === Perfiles.OSINFOR || this.usuario.sirperfil === Perfiles.SERFOR) ? false : true;
  }

  ngOnInit(): void {
    this.idPermisoForestal = Number(this.route.snapshot.paramMap.get('idPlan'));

    /*if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilArffs = true;
    } else {
      this.isPerfilArffs = false;
    }*/

    //this.obtenerEstadoPermisoForestal();
  }

  obtenerEstadoPermisoForestal() {
    let params = new HttpParams()
      .set('idPermisoForestal', this.idPermisoForestal.toString())
      .set('idTipoProceso', '2')
      .set('idTipoPermiso', '1')
      .set('pageNumber', '1')
      .set('pageSize', '1')
      .set('sortType', 'DESC');

    this.permisoForestalService.ListarPermisosForestales(params).subscribe(
      (result: any) => {
        if (result.success) {
          if (result.data.length > 0) {
            this.codigoEstadoPermisoForestal = result.data[0].codigoEstado;
          }
        }
      },
      (error) => {}
    );
  }

  siguienteTab() {
    this.tabIndex++;
  }

  regresarTab() {
    this.tabIndex--;
  }

  informChange(event: any) {
    this.tabIndex = event;
  }
}
