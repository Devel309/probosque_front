import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarEntidadesComponent } from './consultar-entidades.component';

describe('ButtonRemisionOsinforComponent', () => {
  let component: ConsultarEntidadesComponent;
  let fixture: ComponentFixture<ConsultarEntidadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsultarEntidadesComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarEntidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
