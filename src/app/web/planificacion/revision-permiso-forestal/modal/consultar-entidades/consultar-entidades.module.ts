import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from '@shared';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { ButtonsFilePermisoForestalModule } from 'src/app/shared/components/buttons-file-permiso-forestal/buttons-file-permiso-forestal.module';
import { ConsultarEntidadesComponent } from './consultar-entidades.component';

@NgModule({
  declarations: [ConsultarEntidadesComponent],
  imports: [
    CommonModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    ButtonModule,
    ButtonsFilePermisoForestalModule,
    ToastModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule
  ],
  exports: [ConsultarEntidadesComponent],
})
export class ConsultarEntidadesPFModule {}
