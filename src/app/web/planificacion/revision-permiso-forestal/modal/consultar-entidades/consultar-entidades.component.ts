import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ParametroValorService, UsuarioService } from '@services';
import { isNullOrEmpty } from '@shared';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { OpinionesService } from '../../../../../service/opiniones/opiniones.service';
@Component({
  selector: 'consultar-entidades',
  templateUrl: './consultar-entidades.component.html',
  styleUrls: ['./consultar-entidades.component.scss'],
})
export class ConsultarEntidadesComponent implements OnInit {
  @Input() idPermisoForestal!: number;

  @Input() disabled!: boolean;
  isShowModal2_2: boolean = false;

  lstTipoEntidad: any[] = [];
  entidad: Entidad = {} as Entidad;
  lstTipoDocumento: any[] = [];
  minDate = moment(new Date()).format('YYYY-MM-DD');
  idArchivo : number = 0;

  constructor(
    private parametroService: ParametroValorService,
    private toast: ToastModule,
    private solicitudService: OpinionesService,
    private messageService: MessageService,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    this.entidad = new Entidad();
    this.listarCatalogoEntidades();
    this.listarTipoDocumento();
    // this.listar();
  }


  listarTipoDocumento() {
    var params = { prefijo: 'TDOCI' };
    this.parametroService.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoDocumento = result.data;
    });
  }

  verPDF() {
    this.isShowModal2_2 = true;
  }

  listar() {
    const params = {
      idPlanManejo: this.idPermisoForestal,
      codSolicitud: 'PFCR',
      subCodSolicitud: '',
    };

    this.solicitudService
      .listarSolicitudOpinionEvaluacion(params)
      .subscribe((response: any) => {
        if (response.success) {
          this.entidad = new Entidad(response.data[0]);
        }
      });
  }

  guardar() {
    if (this.validar()) {
      const obj = new Entidad(this.entidad);
      obj.idPlanManejo = this.idPermisoForestal;
      obj.idUsuarioRegistro = this.usuarioService.idUsuario;
  
      this.solicitudService.registrarSolicitudOpinionEvaluacion([obj]).subscribe(
        (response: any) => {
          if (response.success) {
            this.successMensaje(response.message);
            this.isShowModal2_2 = false;
  
            this.entidad = new Entidad();
            // this.listar();
          } else {
            this.errorMensaje(response.message);
          }
        },
        (response: any) => {
          this.errorMensaje('Ocurrió un error.');
        }
      );
    }
  }

  validar() {
    let validar: boolean = true;

    if (isNullOrEmpty(this.entidad.entidad)) {
      validar = false;
      this.errorMensaje("(*) Debe seleccionar: Entidad.");
    }
    if (isNullOrEmpty(this.entidad.asunto)) {
      validar = false;
      this.errorMensaje("(*) Debe ingresar: Asunto.");
    }
    if (isNullOrEmpty(this.entidad.descripcion)) {
      validar = false;
      this.errorMensaje("(*) Debe ingresar: Descripción.");
    }
    if (isNullOrEmpty(this.entidad.numDocumento)) {
      validar = false;
      this.errorMensaje("(*) Debe ingresar: Número de documento.");
    }
    if (isNullOrEmpty(this.entidad.tipoDocumento)) {
      validar = false;
      this.errorMensaje("(*) Debe seleccionar: Tipo Documento.");
    }
    if (isNullOrEmpty(this.entidad.fechaDocumento)) {
      validar = false;
      this.errorMensaje("(*) Debe ingresar: Fecha.");
    }
    if(this.idArchivo == 0){
      validar = false;
      this.errorMensaje("(*) Debe adjuntar: Documento.")
    }

    return validar;
  }

  registroArchivo(idArchivo: number) {
    this.idArchivo = idArchivo;
  }

  listarCatalogoEntidades() {
    var params = { prefijo: 'AUTSAN' };

    this.parametroService.listarPorCodigoParametroValor(params).subscribe(
      (result: any) => {
        this.lstTipoEntidad = result.data;
      },
      (error: any) => {}
    );
  }

  successMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'warn',
      summary: '',
      detail: mensaje,
    });
  }
}

export class Entidad {
  idSolOpinion: number;
  idPlanManejo: number;
  codSolicitud: string;
  subCodSolicitud: string;
  numDocumento: string;
  fechaDocumento: Date;
  tipoDocumento: string;
  asunto: string;
  entidad: string;
  descripcion: string;
  idUsuarioRegistro: number;

  constructor(obj?: Partial<Entidad>) {
    this.idSolOpinion = 0;
    this.idPlanManejo = 0;
    this.codSolicitud = 'PFCR';
    this.subCodSolicitud = '';
    this.numDocumento = '';
    this.tipoDocumento = '';
    this.fechaDocumento = new Date();
    this.asunto = '';
    this.entidad = '';
    this.descripcion = '';
    this.idUsuarioRegistro = 0;

    if (obj) Object.assign(this, obj);
  }
}
