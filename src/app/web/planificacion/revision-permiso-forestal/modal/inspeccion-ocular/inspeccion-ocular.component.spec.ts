import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspeccionOcularComponent } from './inspeccion-ocular.component';

describe('InspeccionOcularComponent', () => {
  let component: InspeccionOcularComponent;
  let fixture: ComponentFixture<InspeccionOcularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspeccionOcularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspeccionOcularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
