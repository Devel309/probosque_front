import { Component, Input, OnInit } from '@angular/core';
import { UsuarioService } from '@services';
import { MessageService } from 'primeng/api';
import { CodigoPermisoForestal } from 'src/app/model/util/CodigoPermisoForestal';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import {ToastService} from '@shared';

@Component({
  selector: 'inspeccion-ocular',
  templateUrl: './inspeccion-ocular.component.html',
  styleUrls: ['./inspeccion-ocular.component.scss'],
})
export class InspeccionOcularComponent implements OnInit {
  @Input() idPermisoForestal: any;
  @Input() estadoEvaluacion: any;
  @Input() tipoEvaluacion: any;
  @Input() disabled!: boolean;

  idArchivo : number = 0;

  isShowModalInspeccion: boolean = false;

  inspeccion: ListarEvaluacionPermisoDetalle =
    {} as ListarEvaluacionPermisoDetalle;

  constructor(
    private inspeccionService: EvaluacionService,
    private messageService: MessageService,
    private usuarioService: UsuarioService,
    private toast: ToastService,
  ) {}

  ngOnInit(): void {
    // this.listar();
  }

  verInspeccion() {
    this.isShowModalInspeccion = true;
  }

  listar() {
    const params = {
      idPlanManejo: this.idPermisoForestal,
      codigoEvaluacionDet: 'PFCR',
      codigoEvaluacionDetSub: 'GOPFPFIG',
      tipoEvaluacion: 'EIPF',
    };

    this.inspeccionService
      .obtenerEvaluacionPermisoForestal(params)
      .subscribe((response: any) => {
        console.log(params);
        if (response.success) {
          this.inspeccion = new ListarEvaluacionPermisoDetalle(
            response.data[0]
          );
        }
      });
  }

  registroArchivo(idArchivo:number){
    this.idArchivo = idArchivo;
  }


  validar() {
    let mensaje: string = '';
    let resl:boolean = true;
    if(this.inspeccion.observacion == undefined){
      mensaje = mensaje += "(*) Debe ingresar: Inspector.\n"
      resl = false;
    }
    if(this.inspeccion.detalle == undefined){
      mensaje = mensaje += "(*) Debe ingresar: Asunto.\n"
      resl = false;
    }

    // if(this.idArchivo == 0){
    //   mensaje = mensaje += "(*) Debe adjuntar: Archivo."
    //   resl = false;
    // }

    if (!resl) this.toast.warn(mensaje);

    return resl;
  }

  guardar() {


    if(this.validar()){

      const inspeccionOcular = new InspeccionOcular();
      inspeccionOcular.idPermisoForestal = this.idPermisoForestal;
      inspeccionOcular.idUsuarioRegistro = this.usuarioService.idUsuario;
      inspeccionOcular.tipoEvaluacion = this.tipoEvaluacion;
      inspeccionOcular.estadoEvaluacion = this.estadoEvaluacion;
      const obj = new ListarEvaluacionPermisoDetalle(this.inspeccion);
      inspeccionOcular.listarEvaluacionPermisoDetalle.push(obj);

      this.inspeccionService
        .registrarEvaluacionPermisoForestal(inspeccionOcular)
        .subscribe((response: any) => {
          if (response.success) {
            this.successMensaje(response.message);
            this.isShowModalInspeccion = false;
            // this.listar();
            this.inspeccion = new ListarEvaluacionPermisoDetalle();
          }
        });
    }

  }

  successMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }
}

export class InspeccionOcular {
  codigoEvaluacion: string;
  idEvaluacionPermiso: number;
  estadoEvaluacion: string;
  fechaEvaluacionInicial: Date;
  idPermisoForestal: number;
  idUsuarioRegistro: number;
  tipoEvaluacion: string;
  listarEvaluacionPermisoDetalle: ListarEvaluacionPermisoDetalle[];

  constructor(obj?: Partial<InspeccionOcular>) {
    this.codigoEvaluacion = 'PFCR';
    this.idEvaluacionPermiso = 0;
    this.estadoEvaluacion = 'EPSFVAL';
    this.fechaEvaluacionInicial = new Date();
    this.idPermisoForestal = 0;
    this.idUsuarioRegistro = 0;
    this.tipoEvaluacion = CodigoPermisoForestal.TIPO_EVALUACION;
    this.listarEvaluacionPermisoDetalle = [];

    if (obj) Object.assign(this, obj);
  }
}

export class ListarEvaluacionPermisoDetalle {
  codigoEvaluacionDet: string;
  codigoEvaluacionDetSub: string;
  codigoEvaluacionDetPost: string;
  conforme: string;
  descripcion: string;
  observacion: string;
  detalle: string;

  constructor(obj?: Partial<ListarEvaluacionPermisoDetalle>) {
    this.codigoEvaluacionDet = 'PFCR';
    this.codigoEvaluacionDetSub = 'GOPFPFIG';
    this.codigoEvaluacionDetPost = 'EIPF';
    this.conforme = 'EIPFPRES';
    this.descripcion = 'Inspección Ocular';
    this.observacion = '';
    this.detalle = '';

    if (obj) Object.assign(this, obj);
  }
}
