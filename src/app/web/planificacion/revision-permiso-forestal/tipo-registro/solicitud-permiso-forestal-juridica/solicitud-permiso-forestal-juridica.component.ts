import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParametroValorService } from 'src/app/service/parametro.service';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ValidarCondicionMinimaComponent} from '../../../../../shared/components/validar-condicion-minima/validar-condicion-minima.component';
import {CodigoPermisoForestal} from '../../../../../model/util/CodigoPermisoForestal';

@Component({
  selector: 'solicitud-permiso-forestal-juridica',
  templateUrl: './solicitud-permiso-forestal-juridica.component.html',
  styleUrls: ['./solicitud-permiso-forestal-juridica.component.scss'],
})
export class SolicitudPermisoForestalJuridica implements OnInit {
  constructor(private serv: ParametroValorService, private router: Router, public dialogService: DialogService,) {}

  lstTipoDocumento: any[] = [];
  ref!: DynamicDialogRef;

  @Input('dataBase') dataBase: any = {
    disabled: false,
  };
  @Input('solicitudAcceso') solicitudAcceso: any = {};
  @Input() isEdit: boolean = true;
  @Input('observado') observado: boolean = false;

  //@Input() isPerfilArffs!: boolean;
  @Input() codigoEstadoPermisoForestal!: string;

  CodigoPermisoForestal = CodigoPermisoForestal;

  ngOnInit(): void {
    this.listarTipoDocumento();
  }

  listarTipoDocumento() {
    var params = { prefijo: 'TDOCI' };
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      this.lstTipoDocumento = result.data;
    });
  }


  validarCondiciones() {
    let param = {
      nombre: '',
      apellidoPaterno: '',
      apellidoMaterno: '',
      tipoDocumento: this.solicitudAcceso.codigoEstadoSolicitud != null ? this.solicitudAcceso.codigoTipoDocumentoE: this.solicitudAcceso.codTipoDocumento,
      numeroDocumento: this.solicitudAcceso.codigoEstadoSolicitud != null ? this.solicitudAcceso.numeroRucEmpresa: this.solicitudAcceso.rucComunidad,
    };

    

    if(param.tipoDocumento == 'TDOCRUC'){
      param.tipoDocumento = 'RUC';
    }else{
      param.tipoDocumento = 'DNI';
    }

    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: 'Validar Condiciones Mínimas',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        idCondicionMinima: null,
        obj: param
      },
    });
  }

}
