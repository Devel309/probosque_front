import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ParametroValorService, UsuarioService } from '@services';
import { descargarArchivo, DownloadFile, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ResultadosEvaluacionDetalle } from 'src/app/model/resultadosEvaluacion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { CondicionMinimaService } from 'src/app/service/bandeja-postulacion/condicion.minima.service';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { ResultadosEvaluacionService } from 'src/app/service/evaluacion/resultadosEvaluacion.service';
import { PfcrDatosOtorgamientoService } from 'src/app/service/pfcr-datos-otorgamiento.service';
import { CodigoPermisoForestal } from '../../../../../model/util/CodigoPermisoForestal';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { PermisoForestalService } from '../../../../../service/permisoForestal.service';
import { ResumenService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service";

@Component({
  selector: 'permiso-resultado-evaluacion',
  templateUrl: './permiso-resultado-evaluacion.component.html',
  styleUrls: ['./permiso-resultado-evaluacion.component.scss'],
})
export class PermisoResultadoEvaluacionComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Input() codigoPlan!: string;
  @Input() numeracion!: string;
  @Input() sunarp: boolean = false;
  @Input() sunat: boolean = false;
  @Output() public regresar = new EventEmitter();

  estadoResultadoEvaluacion: string | null = null;
  justificacionResultadoEvaluacion: string | null = null;
  classEstado: string = '';
  archivoRecurso: any = {};
  listDocumentos: any[] = [];
  perfiles: any[] = [];
  perfilesAgregados: any[] = [];
  perfilObj: any;

  listaDetalle: ResultadosEvaluacionDetalle[] = [];
  idEvalResultadoCarga: number = 0;
  idEvalResultadoEnvio: number = 0;
  datos: any;

  CodigoPermisoForestal = CodigoPermisoForestal;

  listaDetalleEnviar: ResultadosEvaluacionDetalle[] = [];

  favorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  desFavorableObj: ResultadosEvaluacionDetalle =
    new ResultadosEvaluacionDetalle();
  observadoObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();

  criteriosEvaluacion: any = [];

  @Input() disabled!: boolean;

  //isDisabledEnviar :boolean = true;
  listaRequisitos: any[] = [];

  evaluacion: any[] = [];

  codigoProceso = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoTab = CodigosTabEvaluacion.PFCR_TAB_5;
  codigoAcordeonVal: string = CodigosTabEvaluacion.PFCR_TAB_VAL_5_1;

  idArchivoResultEval: number = 0;
  codigoEstadoPermiso: string = '';
  condiciones: any = {};

  isJuridico: boolean = false;
  usuario!: UsuarioModel;
  Perfiles = Perfiles;

  constructor(
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private user: UsuarioService,
    private resultadosEvaluacionService: ResultadosEvaluacionService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private permisoForestalService: PermisoForestalService,
    private router: Router,
    private datosOtorgamientoService: PfcrDatosOtorgamientoService,
    private condicionMinimaService: CondicionMinimaService,
    private resumenService: ResumenService
  ) {}

  ngOnInit() {
    /*this.criteriosEvaluacion.push({
      criterio:'SUNARP',
      resultado:'Favorable'

    });

    this.criteriosEvaluacion.push({
      criterio:'SUNAT',
      observacion:"RUC no existente",
      resultado:'Desfavorable'
    });

    this.criteriosEvaluacion.push({
      criterio:'Registro nacional de infractores',
      resultado:'Favorable'
    });

    this.criteriosEvaluacion.push({
      criterio:'Opiniones de Otras Entidades (DERNAMP, ANA, ETC)',
      resultado:'Desfavorable'
    });

    this.criteriosEvaluacion.push({
      criterio:'Resultado de Inspección Ocular',
      resultado:'Desfavorable'
    });

    this.criteriosEvaluacion.push({
      criterio:'Resultado de Informe Plan General, PO o DEMA',
      resultado:'Favorable'
    });
*/
    this.obtenerEvaluacion();
    this.listTipoDocumento();
    this.listPerfiles();
    this.listadoCargaInforme();
    this.listadoEnviar();
    this.listarDatosOtorgamiento();
    this.listarInfGeneral();
    this.usuario = this.user.usuario;
  }

  listarDatosOtorgamiento() {
    var params = {
      idPermisoForestal: this.idPermisoForestal,
      codPermiso: 'PFCR',
    };

    this.datosOtorgamientoService
      .listarInformacionOtorgamiento(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];
          this.sunarp = element.nroPartidaRegistral != '';
          this.sunat = element.nroRuc != '';
        }
      });
  }

  listarInfGeneral() {
    var params = {
      idInfGeneral: null,
      codTipoInfGeneral: 'PFCR',
      idPermisoForestal: this.idPermisoForestal,
    };
    this.permisoForestalService
      .listarInformacionGeneralPF(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];
          this.codigoEstadoPermiso = element.codEstadoPF;

          if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE){
            this.estadoResultadoEvaluacion = CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE

          }else if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE){
            this.estadoResultadoEvaluacion = CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE

            this.justificacionResultadoEvaluacion= element.observacion;
          }

          let param: any = {
            tipoDocumento: element.codTipoPersona == 'TPERJURI' ? element.codTipoDocumento: element.tipoDocumentoElaborador,
            nroDocumento: element.codTipoPersona == 'TPERJURI' ? element.rucComunidad: element.documentoElaborador,
            nombres: element.codTipoPersona == 'TPERJURI' ? element.federacionComunidad : element.nombreElaborador,
            apellidoPaterno: element.codTipoPersona == 'TPERJURI' ? "" : element.apellidoPaternoElaborador,
            apellidoMaterno: element.codTipoPersona == 'TPERJURI' ? "" : element.apellidoMaternoElaborador,
          };
          
          if (param.tipoDocumento == 'TDOCRUC') {
            param.tipoDocumento = 'RUC';
          } else {
            param.tipoDocumento = 'DNI';
          }
          this.validarRequisitos(param);

          this.isJuridico = element.codTipoPersona == 'TPERJURI';
        }
      });
  }

  regresarTab() {
    this.regresar.emit();
  }

  listadoCargaInforme() {
    var params = {
      idPlanManejo: this.idPermisoForestal,
      codResultado: this.codigoPlan,
      subCodResultado: 'ENINEVA',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoCarga = element.idEvalResultado;
              if (element.codResultadoDet == 'FAVO') {
                this.favorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == 'DESF') {
                this.desFavorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == 'OBSE') {
                this.observadoObj = new ResultadosEvaluacionDetalle(element);
              }
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listadoEnviar() {
    this.perfilesAgregados = [];
    var params = {
      idPlanManejo: this.idPermisoForestal,
      codResultado: this.codigoPlan,
      subCodResultado: 'ENIN',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoEnvio = element.idEvalResultado;
              this.filtrarPerfil(element);
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  filtrarPerfil(element: any) {
    this.datos = this.perfiles.filter((x) => x.codigo === element.perfil);
    this.datos.forEach((item: any) => {
      this.perfilesAgregados.push({
        ...item,
        idEvalResultadoDet: element.idEvalResultadoDet,
      });
    });
  }

  generarDescargarReporte() {
    const payload = {
      codigoProceso: this.codigoPlan,
      idPlanManejo: this.idPermisoForestal,
    };
    this.evaluacionService
      .generarInformacionEvaluacion(payload)
      .subscribe((data: any) => {
        this.toast.ok('Se generó el Reporte de Evaluación correctamente.');

        DownloadFile(data.archivo, data.nombeArchivo, '');
      });
  }

  listTipoDocumento() {
    var params = {
      prefijo: 'TDRESULTEVAL',
    };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listDocumentos = [...response.data];
      });
  }

  listPerfiles() {
    var params = {
      prefijo: 'AUTSAN',
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.perfiles = [...response.data];
      });
  }

  onChange(event: any) {
    this.listaDetalle = [];
    var obj = new ResultadosEvaluacionDetalle(event.obj);
    obj.codResultadoDet = event.codResultadoDet;
    obj.idUsuarioRegistro = this.user.idUsuario;

    this.listaDetalle.push(obj);

    var params = [
      {
        idEvalResultado: this.idEvalResultadoCarga,
        idPlanManejo: this.idPermisoForestal,
        codResultado: this.codigoPlan,
        subCodResultado: 'ENINEVA',
        descripcionCab: 'Carga de Informe de Evaluación',
        codInforme: 'radioSelect',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalle,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoCargaInforme();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  agregarPerfil() {
    let idPerfilAgregar = this.perfilObj.codigo;
    let encontrado: boolean = false;

    this.perfilesAgregados.forEach((item: any) => {
      if (item.codigo == idPerfilAgregar) {
        encontrado = true;
        return;
      }
    });

    if (!encontrado) {
      this.perfilesAgregados.push({ ...this.perfilObj, idEvalResultadoDet: 0 });
    } else {
      this.toast.warn(
        'El perfil ' +
          this.perfilObj.valorPrimario +
          ' ya se encuentra agregado.'
      );
    }
  }

  guardarEnviar() {
    this.perfilesAgregados.forEach((item) => {
      var obj = new ResultadosEvaluacionDetalle();
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.perfil = item.codigo;
      obj.codResultadoDet = 'NOPER';
      obj.idEvalResultadoDet = item.idEvalResultadoDet
        ? item.idEvalResultadoDet
        : 0;
      this.listaDetalleEnviar.push(obj);
    });

    var params = [
      {
        idEvalResultado: this.idEvalResultadoEnvio,
        idPlanManejo: this.idPermisoForestal,
        codResultado: this.codigoPlan,
        subCodResultado: 'ENIN',
        descripcionCab: 'Envio de información',
        codInforme: '',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalleEnviar,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoEnviar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openEliminarPerfil(event: Event, index: number, perfil: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (perfil.idEvalResultadoDet != 0) {
          var params = {
            idEvalResultado: this.idEvalResultadoEnvio,
            idEvalResultadoDet: perfil.idEvalResultadoDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.resultadosEvaluacionService
            .eliminarEvaluacionesultado(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.perfilesAgregados.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.perfilesAgregados.splice(index, 1);
        }
      },
    });
  }

  obtenerEvaluacion() {
    //obtener datos de Otorgamiento
    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      //codigoEvaluacionDetSub : CodigosTabEvaluacion.PFCR_TAB_2,
      tipoEvaluacion: CodigoPermisoForestal.TIPO_EVALUACION,
    };

    this.listaRequisitos.push({
      descripcion: '1. Información General',
      detalle: '-',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });
    this.listaRequisitos.push({
      descripcion: '2. Datos de Otorgamiento',
      detalle: '-',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });
    this.listaRequisitos.push({
      detalle: '1. Resultado de Busqueda en Registro nacional de infractores',
      descripcion: '2. Datos de Otorgamiento',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });
    this.listaRequisitos.push({
      detalle: '2. Resultado de Opiniones de otras Entidades (SENAMP)',
      descripcion: '2. Datos de Otorgamiento',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });
    this.listaRequisitos.push({
      detalle: '3. Resultado de Inspección Ocular',
      descripcion: '2. Datos de Otorgamiento',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });
    this.listaRequisitos.push({
      detalle: '4. Resultado de Informe Plan General PO o DEMA',
      descripcion: '2. Datos de Otorgamiento',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });
    this.listaRequisitos.push({
      descripcion: '3. Anexos',
      detalle: '-',
      conforme: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
    });

    this.evaluacionService
      .obtenerEvaluacionPermisoForestal(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            let evaluacion = result.data[0].listarEvaluacionPermisoDetalle;

            let item = evaluacion.find(
              (e: any) => e.descripcion == '1. Información General'
            );

            if (item) {
              this.listaRequisitos[0].conforme = item.conforme;
              this.listaRequisitos[0].observacion = item.observacion;
            }

            let item2 = evaluacion.find(
              (e: any) =>
                e.codigoEvaluacionDetPost ==
                CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1
            );
            if (item2) {
              this.listaRequisitos[1].conforme = item2.conforme;
              this.listaRequisitos[1].observacion = item2.observacion;
            }


            let item2_1 = evaluacion.find(
              (e: any) =>
                e.codigoEvaluacionDetPost ==
                CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_1
            );
            if (item2_1) {
              this.listaRequisitos[2].conforme = item2_1.conforme;
              this.listaRequisitos[2].observacion = item2_1.observacion;
            }

            let item2_2 = evaluacion.find(
              (e: any) =>
                e.codigoEvaluacionDetPost ==
                CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_2
            );
            if (item2_2) {
              this.listaRequisitos[3].conforme = item2_2.conforme;
              this.listaRequisitos[3].observacion = item2_2.observacion;
            }

            let item2_3 = evaluacion.find(
              (e: any) =>
                e.codigoEvaluacionDetPost ==
                CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_3
            );
            if (item2_3) {
              this.listaRequisitos[4].conforme = item2_3.conforme;
              this.listaRequisitos[4].observacion = item2_3.observacion;
            }

            let item2_4 = evaluacion.find(
              (e: any) =>
                e.codigoEvaluacionDetPost ==
                CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_4
            );
            if (item2_4) {
              this.listaRequisitos[5].conforme = item2_4.conforme;
              this.listaRequisitos[5].observacion = item2_4.observacion;
            }

            let item3 = evaluacion.find(
              (e: any) => e.descripcion == '3. Anexos'
            );
            if (item3) {
              this.listaRequisitos[6].conforme = item3.conforme;
              this.listaRequisitos[6].observacion = item3.observacion;
            }
          }
        }
      });
  }

  cargarIdArchivo(idArchivo: any) {
    console.log('-><<<', idArchivo);
    if (idArchivo && idArchivo > 0) {
      this.idArchivoResultEval = idArchivo;
      //this.isDisabledEnviar = false;
    }
  }

  eliminarArchivo(idArchivo: any) {
    console.log('-><<<', idArchivo);
    this.idArchivoResultEval = 0;
  }

  validarEnvio() {
    let flag = true;
    let flagPendiente = false;

    this.listaRequisitos.forEach((e: any) => {
      if (e.conforme == CodigoPermisoForestal.ESTADO_VAL_PENDIENTE) {
        flagPendiente = true;
      }
    });

    if (flagPendiente) {
      this.toast.warn('Tiene al menos un requisito pendiente de evaluación');
      flag = false;
    }

    if (
      this.estadoResultadoEvaluacion == undefined ||
      this.estadoResultadoEvaluacion == null
    ) {
      this.toast.warn(
        'Seleccione un resultado final, ya sea favorable o desfavorable'
      );
      flag = false;
    }

    if (this.idArchivoResultEval == 0) {
      this.toast.warn('Debe cargar el archivo Reporte de Evaluación Firmado');
      flag = false;
    }

    if (this.estadoResultadoEvaluacion == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE && (this.justificacionResultadoEvaluacion == null || this.justificacionResultadoEvaluacion == "")) {
      this.toast.warn('El Campo Observación es Obligatorio.');
      flag = false;
    }

    return flag;
  }

  enviarResultados() {
    if (this.validarEnvio()) {
      let param = {
        idPermisoForestal: this.idPermisoForestal,
        codigoEstado: this.estadoResultadoEvaluacion,
        observacion: !!this.justificacionResultadoEvaluacion? this.justificacionResultadoEvaluacion: null,
        idUsuarioRegistro: this.user.idUsuario,
      };

      this.permisoForestalService
        .actualizarEstadoPermisoForestal(param)
        .subscribe((response: any) => {
          if (response.success == true) {
            this.toast.ok(response?.message);
            //this.isDisabledEnviar = true;

            this.toast.ok('Envío Realizado Correctamente.');
            this.router.navigate([
              '/planificacion/evaluacion/otorgamiento-permiso',
            ]);
          } else {
            this.toast.error(response?.message);
          }
        });
    }
  }

  private validarRequisitos(param: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.condicionMinimaService
      .validarRequisitos(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.success) {
          this.condiciones.resultadoInfractor = result.data.resultadoInfractor;
        }
      });
  }

  generarPFCR() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resumenService
      .consolidadoPFCR(this.idPermisoForestal)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          descargarArchivo(res);
        } else {
          this.toast.error(res?.message);
        }
      });
  }
}
