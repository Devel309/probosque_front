import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ResultadosEvaluacionDetalle } from 'src/app/model/resultadosEvaluacion';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { ResultadosEvaluacionService } from 'src/app/service/evaluacion/resultadosEvaluacion.service';
import {CodigoProceso} from '../../../../../model/util/CodigoProceso';
import {CodigosTabEvaluacion} from '../../../../../model/util/CodigosTabEvaluacion';
import {CodigoPermisoForestal} from '../../../../../model/util/CodigoPermisoForestal';
import {PermisoForestalService} from '../../../../../service/permisoForestal.service';
import {Router} from '@angular/router';
import {EvaluacionPermisoForestalModel} from '../../../../../model/Comun/EvaluacionPermisoForestalModel';
import {EvaluacionUtils} from '../../../../../model/util/EvaluacionUtils';
import {Mensajes} from '../../../../../model/util/Mensajes';
import * as moment from 'moment';

@Component({
  selector: 'recepcion-documentos-fisicos',
  templateUrl: './recepcion-documentos-fisicos.component.html',
  styleUrls: ['./recepcion-documentos-fisicos.component.scss'],
})
export class RecepcionDocumentosFisicosComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Input() codigoPlan!: string;
  @Input() numeracion!: string;
  @Output() public regresar = new EventEmitter();

  radioSelect: string = 'FAVO';
  classEstado: string = '';
  archivoRecurso: any = {};
  listDocumentos: any[] = [];
  perfiles: any[] = [];
  perfilesAgregados: any[] = [];
  perfilObj: any;

  minDate = moment(new Date()).format('YYYY-MM-DD');

  listaDetalle: ResultadosEvaluacionDetalle[] = [];
  idEvalResultadoCarga: number = 0;
  idEvalResultadoEnvio: number = 0;
  datos: any;

  listaDetalleEnviar: ResultadosEvaluacionDetalle[] = [];

  favorableObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();
  desFavorableObj: ResultadosEvaluacionDetalle =
    new ResultadosEvaluacionDetalle();
  observadoObj: ResultadosEvaluacionDetalle = new ResultadosEvaluacionDetalle();

  criteriosEvaluacion :any =[];

  codigoProceso = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoTab = CodigosTabEvaluacion.PFCR_TAB_4;
  codigoAcordeonVal: string = CodigosTabEvaluacion.PFCR_TAB_VAL_4_1;

  listaRequisitos:any[] = [];
  CodigoPermisoForestal=CodigoPermisoForestal;

  isDisabledEnviar: boolean = true;

  fecha:Date = new Date();

  idArchivo!:number;

  //isGuardado: boolean= false;

  @Input() disabled!: boolean;

  evaluacion:any;
  evaluacionDet!: EvaluacionPermisoForestalModel;

  constructor(
    private evaluacionService: EvaluacionService,
    private toast: ToastService,
    private parametroValorService: ParametroValorService,
    private user: UsuarioService,
    private resultadosEvaluacionService: ResultadosEvaluacionService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private permisoForestalService: PermisoForestalService,private router: Router,
  ) {

    this.evaluacion = {
      codigoEvaluacion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idEvaluacionPermiso: 0,
      estadoEvaluacion: 'EEVAPRES',
      fechaEvaluacionInicial: new Date().toISOString(),
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.user.idUsuario,
      tipoEvaluacion:CodigoPermisoForestal.TIPO_VALIDACION,
      listarEvaluacionPermisoDetalle: [],
    };
  }

  ngOnInit() {

    this.obtenerListaRequisitos();

    this.evaluacionDet = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab,
      codigoEvaluacionDetPost : this.codigoAcordeonVal,
      detalle :'4. Recepción de Documentos Físicos',
      conforme: CodigoPermisoForestal.ESTADO_VAL_VALIDADO
      //fechaEvaluacionDetInicial: new Date().toISOString(),
    });
    this.obtenerEvaluacion();
  }

  regresarTab() {
    this.regresar.emit();
  }

  obtenerEvaluacion() {
    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab,
      tipoEvaluacion : CodigoPermisoForestal.TIPO_VALIDACION
    }

    this.evaluacionService.obtenerEvaluacionPermisoForestal(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion) {

            this.evaluacionDet = Object.assign(this.evaluacionDet,this.evaluacion.listarEvaluacionPermisoDetalle.find((x: any) =>
              x.codigoEvaluacionDetPost == this.codigoAcordeonVal
            ));

            if(this.evaluacionDet.codigoEvaluacionDetPost == this.codigoAcordeonVal) {
              this.isDisabledEnviar = false;
              if (this.evaluacionDet.fechaEvaluacionDetInicial)
                this.fecha = new Date(this.evaluacionDet.fechaEvaluacionDetInicial);
            }
          }
        }
      }
    })
  }

  registroArchivo(e:any){
    if(e>0){
      this.idArchivo = e;
    }



  }


  guardarValidacion() {
    let fechaActual = moment(new Date())
    .subtract(1, "days")
    .format("YYYY-MM-DD");
    let fechaRegistrada = moment(this.fecha).format("YYYY-MM-DD");

    if (this.evaluacionDet.descripcion == undefined || this.evaluacionDet.descripcion == ''){
      this.toast.warn('Ingrese un número de trámite');
      return;
    }

    if (this.fecha == undefined || !this.fecha){
      this.toast.warn('Seleccione una fecha');
      return;
    }

    if (this.fecha == undefined || !this.fecha){
      this.toast.warn('Seleccione una fecha');
      return;
    }

    if (fechaRegistrada <= fechaActual) {
      this.toast.warn('La Fecha Inicio debe ser posterior o igual a la Fecha Actual.');
    }

    if(this.idArchivo &&  this.idArchivo>0){
      if(EvaluacionUtils.validarPermisoForestal([this.evaluacionDet])) {
        if(this.evaluacion) {
          this.evaluacion.idPermisoForestal = this.idPermisoForestal;
          this.evaluacion.listarEvaluacionPermisoDetalle = [];
          this.evaluacionDet.fechaEvaluacionDetInicial = this.fecha.toISOString();
          this.evaluacionDet.idArchivo = this.idArchivo;
          this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet);
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.evaluacionService.registrarEvaluacionPermisoForestal(this.evaluacion)
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((res:any) => {
              this.toast.ok(res.message);
              //this.isGuardado = true;

              this.obtenerEvaluacion();
            })
        }
      } else {
        this.toast.warn(Mensajes.MSJ_EVALUACIONES)
      }
    }else{
      this.toast.warn('Cargue un Archivo');
      return;
    }



  }


  listadoCargaInforme() {
    var params = {
      idPlanManejo: this.idPermisoForestal,
      codResultado: this.codigoPlan,
      subCodResultado: 'ENINEVA',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data.length != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoCarga = element.idEvalResultado;
              if (element.codResultadoDet == 'FAVO') {
                this.favorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == 'DESF') {
                this.desFavorableObj = new ResultadosEvaluacionDetalle(element);
              } else if (element.codResultadoDet == 'OBSE') {
                this.observadoObj = new ResultadosEvaluacionDetalle(element);
              }
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listadoEnviar() {
    this.perfilesAgregados = [];
    var params = {
      idPlanManejo: this.idPermisoForestal,
      codResultado: this.codigoPlan,
      subCodResultado: 'ENIN',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .listarEvaluacionesultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (response.data != 0) {
            response.data.forEach((element: any) => {
              this.idEvalResultadoEnvio = element.idEvalResultado;
              this.filtrarPerfil(element);
            });
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  filtrarPerfil(element: any) {
    this.datos = this.perfiles.filter((x) => x.codigo === element.perfil);
    this.datos.forEach((item: any) => {
      this.perfilesAgregados.push({
        ...item,
        idEvalResultadoDet: element.idEvalResultadoDet,
      });
    });
  }

  generarDescargarReporte() {
    const payload = {
      codigoProceso: this.codigoPlan,
      idPlanManejo: this.idPermisoForestal,
    };
    this.evaluacionService
      .generarInformacionEvaluacion(payload)
      .subscribe((data: any) => {
        this.toast.ok('Se generó el Reporte de Evaluación correctamente.');

        DownloadFile(data.archivo, data.nombeArchivo, '');
      });
  }

  listTipoDocumento() {
    var params = {
      prefijo: 'TDRESULTEVAL',
    };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listDocumentos = [...response.data];
      });
  }

  listPerfiles() {
    var params = {
      prefijo: 'AUTSAN',
    };
    this.parametroValorService
      .listarParametroPorPrefijo(params)
      .subscribe((response: any) => {
        this.perfiles = [...response.data];
      });
  }

  onChange(event: any) {
    this.listaDetalle = [];
    var obj = new ResultadosEvaluacionDetalle(event.obj);
    obj.codResultadoDet = event.codResultadoDet;
    obj.idUsuarioRegistro = this.user.idUsuario;

    this.listaDetalle.push(obj);

    var params = [
      {
        idEvalResultado: this.idEvalResultadoCarga,
        idPlanManejo: this.idPermisoForestal,
        codResultado: this.codigoPlan,
        subCodResultado: 'ENINEVA',
        descripcionCab: 'Carga de Informe de Evaluación',
        codInforme: 'radioSelect',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalle,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoCargaInforme();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  agregarPerfil() {

    let idPerfilAgregar = this.perfilObj.codigo;
    let encontrado:boolean = false;

    this.perfilesAgregados.forEach((item:any)=>{
      if(item.codigo == idPerfilAgregar){
        encontrado = true;
        return;
      }
    })

    if(!encontrado){
      this.perfilesAgregados.push({ ...this.perfilObj, idEvalResultadoDet: 0 });
    }else{
      this.toast.warn("El perfil " +this.perfilObj.valorPrimario+" ya se encuentra agregado.");
    }

  }

  guardarEnviar() {
    this.perfilesAgregados.forEach((item) => {
      var obj = new ResultadosEvaluacionDetalle();
      obj.idUsuarioRegistro = this.user.idUsuario;
      obj.perfil = item.codigo;
      obj.codResultadoDet = 'NOPER';
      obj.idEvalResultadoDet = item.idEvalResultadoDet
        ? item.idEvalResultadoDet
        : 0;
      this.listaDetalleEnviar.push(obj);
    });

    var params = [
      {
        idEvalResultado: this.idEvalResultadoEnvio,
        idPlanManejo: this.idPermisoForestal,
        codResultado: this.codigoPlan,
        subCodResultado: 'ENIN',
        descripcionCab: 'Envio de información',
        codInforme: '',
        idUsuarioRegistro: this.user.idUsuario,
        listEvaluacionResultado: this.listaDetalleEnviar,
      },
    ];

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.resultadosEvaluacionService
      .registrarEvaluacionResultado(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(response?.message);
          this.listadoEnviar();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  openEliminarPerfil(event: Event, index: number, perfil: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el registro?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (perfil.idEvalResultadoDet != 0) {
          var params = {
            idEvalResultado: this.idEvalResultadoEnvio,
            idEvalResultadoDet: perfil.idEvalResultadoDet,
            idUsuarioElimina: this.user.idUsuario,
          };
          this.resultadosEvaluacionService
            .eliminarEvaluacionesultado(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.perfilesAgregados.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.perfilesAgregados.splice(index, 1);
        }
      },
    });
  }


  obtenerListaRequisitos() {

    //obtener datos de Otorgamiento
    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet : CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      //codigoEvaluacionDetSub : CodigosTabEvaluacion.PFCR_TAB_2,
      tipoEvaluacion : CodigoPermisoForestal.TIPO_VALIDACION
    }

    //por defecto
    this.listaRequisitos.push({
      detalle:'1. Información General',
      conforme:CodigoPermisoForestal.ESTADO_VAL_PENDIENTE
    });
    this.listaRequisitos.push({
      detalle:'2. Datos de Otorgamiento',
      conforme:CodigoPermisoForestal.ESTADO_VAL_PENDIENTE
    });
    this.listaRequisitos.push({
      detalle:'3. Anexos',
      conforme:CodigoPermisoForestal.ESTADO_VAL_PENDIENTE
    });

    this.evaluacionService.obtenerEvaluacionPermisoForestal(params).subscribe((result: any) => {
      if(result.data) {

        if (result.data.length > 0) {
          let evaluacion = result.data[0].listarEvaluacionPermisoDetalle;

          let item = evaluacion.find((e:any)=>e.detalle == '1. Información General');

          if(item) {
            this.listaRequisitos[0].conforme = item.conforme;
            this.listaRequisitos[0].observacion = item.observacion;
          }

          let item2 = evaluacion.find((e:any)=>e.detalle == '2. Datos de Otorgamiento');
          if(item2) {
            this.listaRequisitos[1].conforme = item2.conforme;
            this.listaRequisitos[1].observacion = item2.observacion;
          }

          let item3 = evaluacion.find((e:any)=>e.detalle == '3. Anexos');
          if(item3) {
            this.listaRequisitos[2].conforme = item3.conforme;
            this.listaRequisitos[2].observacion = item3.observacion;
          }

          //this.isDisabledEnviar = false;

        } else if(result.data.length==0){

        }
      }
    })
  }

  validarEnvio(){
    let flag= true;
    let flagPendiente = false;

    this.listaRequisitos.forEach((e:any)=>{
      if(e.conforme == CodigoPermisoForestal.ESTADO_VAL_PENDIENTE){
       // this.toast.warn('Tiene un requisito pendiente de validación');
        flagPendiente = true;
      }
    });

    if(flagPendiente){
      this.toast.warn('Tiene un requisito pendiente de validación');
      flag = false;
    }

    return flag;
  }

  enviarValidacion(){

    if(this.validarEnvio()){

      let param = {
        idPermisoForestal: this.idPermisoForestal,
      }
      if(this.validarEnvio()) {
        this.permisoForestalService.enviarCorreoValidacionPermisoForestal(param).subscribe((result: any) => {
          let estado : string = "";
          if(result) {
            estado = CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO;
          } else {
            //estado = CodigoPermisoForestal.ESTADO_VAL_VALIDADO;
            estado = CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION//---EEVPFEVAL
          }

          let param = {
            idPermisoForestal : this.idPermisoForestal,
            codigoEstado      : estado,
            idUsuarioRegistro : this.user.idUsuario
          };

          this.permisoForestalService.actualizarEstadoPermisoForestal(param)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok('Envío Realizado Correctamente.');
                this.router.navigate(['/planificacion/evaluacion/otorgamiento-permiso']);
              } else {
                this.toast.error(response?.message);
              }
            });

        });

      }
    }

  }

}
