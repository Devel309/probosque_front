import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CoreCentralService, UsuarioService } from "@services";
import { ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EspeciesFauna } from "src/app/model/medioTrasporte";
import { PermisoForestalService } from "src/app/service/permisoForestal.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-dialog-planes",
  templateUrl: "./dialog-planes.component.html",
  styleUrls: ["./dialog-planes.component.scss"],
})
export class DialogPlanesComponent implements OnInit {
  queryPlanManejo: string = "";
  comboListadoPlanManejo: EspeciesFauna[] = [];
  selectedValues: any[] = [];
  filtrados: any[] = [];

  objbuscar = {
    dato: "",
    pageNum: 1,
    pageSize: 10,
  };

  totalRecords: number = 0;
  filtrado: any;
  listPermisoPlanManejo: any[] = [];
  tienePlan: boolean = false;

  constructor(
    private coreCentralService: CoreCentralService,
    private dialog: MatDialog,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private permisoForestalService: PermisoForestalService,
    private user: UsuarioService,
    public config: DynamicDialogConfig,
    private toast: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listaPorFiltroEspecieFauna(this.config.data.codTipoPlan);
  }

  listarPermisoForestalPlanManejo(
    idPermisoForestal: number,
    codTipoPlan: string
  ) {
    var params = {
      idPermisoForestal: idPermisoForestal,
      codTipoPlan: codTipoPlan,
    };
    this.permisoForestalService
      .listarPermisoForestalPlanManejo(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.tienePlan = true;
          this.listPermisoPlanManejo = [...response.data];
          response.data.forEach((element: any) => {
            this.filtrado = [];
            this.filtrar(element.idPlanManejo);
          });
          this.selectedValues = this.filtrados;
        }
      });
  }

  filtrar(idPlanManejo: number) {
    this.filtrado = this.comboListadoPlanManejo.filter(
      (x: any) => x.idPlanManejo === idPlanManejo
    );
    this.filtrado.forEach((element: any) => {
      this.filtrados.push(element);
    });
  }

  onChangeFauna(event: any, data: any) {
    this.listPermisoPlanManejo.forEach((item) => {
      if (event.checked === false && data.idPlanManejo === item.idPlanManejo) {
        let params = {
          idPermisoForestal: this.config.data.idPermisoForestal,
          idPlanManejo: data.idPlanManejo,
        };
        this.permisoForestalService
          .eliminarPermisoForestalPlanManejo(params)
          .subscribe((response: any) => {});
      }
    });
    if (event.checked === true && this.selectedValues.length != 0) {
      this.selectedValues = [data];
    }
  }

  listaPorFiltroEspecieFauna(codTipoPlan: string) {
    let params = {
      // nroDocumento: "04822836",
      // nroDocumento: this.user.nroDocumento,
      nroDocumento: this.config.data.numDocumento,
      codTipoPlan: codTipoPlan,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .listarPorFiltroPlanManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data.length != 0) {
          this.comboListadoPlanManejo = [...result.data];
          this.listarPermisoForestalPlanManejo(
            this.config.data.idPermisoForestal,
            this.config.data.codTipoPlan
          );
        } else {
          this.toast.warn("No se encontraron datos");
        }
      });
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.queryPlanManejo == null || this.queryPlanManejo == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un Plan de manejo.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  agregar() {
    /* if (!this.validarMedidas()) {
      return;
    } */
    this.ref.close(this.selectedValues);
  }
  cerrarModal() {
    this.ref.close();
  }

  redireccionar(tipo: any, idPlanManejo: any) {
    this.cerrarModal();
    switch (tipo) {
      case "PGMFA":
        this.router.navigateByUrl('/planificacion/plan-general-manejo/' + idPlanManejo);
        break;
      case "DEMA":
        this.router.navigateByUrl("/planificacion/generar-declaracion-manejo-dema/" + idPlanManejo);
        break;
      default:
        this.router.navigateByUrl("");
        break;
    }
  }
}
