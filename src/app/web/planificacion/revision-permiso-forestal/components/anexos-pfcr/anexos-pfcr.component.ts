import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { ArchivoService, UsuarioService } from "@services";
import { DowloadFileLocal, DownloadFile, GroupFile, LayerView, MapApi, PGMFArchivoTipo, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { from } from "rxjs";
import { concatMap, finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { PermisoForestalGeometriaModel } from "src/app/model/PFCR/PermisoForestalGeometria";
import { ListArchivo, PmfiDatosOtorgamiento } from "src/app/model/PfcrDatosOtorgamientoModel";
import { SolicitudModel } from "src/app/model/Solicitud";
import { SolicitudPermisoArchivoAdjuntoModel, SolicitudPermisoCargaArchivoModel } from "src/app/model/SolicitudArchivo";
import { CodigosPermisosForestales } from "src/app/model/util/CodigoPermisosForestales";
import { CustomCapaModel } from "src/app/model/util/CustomCapa";
import { FileModel } from "src/app/model/util/File";
import { PermisoForestalService } from "src/app/service/permisoForestal.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { PermisoForestalArchivoService } from "src/app/service/planificacion/permiso-forestal/permiso-forestal-archivo.service";
import { PermisoForestalGeometriaService } from "src/app/service/planificacion/permiso-forestal/permiso-forestal-geometria.service";
import { SolicitudService } from "src/app/service/solicitud.service";
import { CodigoProceso } from "../../../../../model/util/CodigoProceso";
import { CodigoPermisoForestal } from "../../../../../model/util/CodigoPermisoForestal";
import { CodigosTabEvaluacion } from "../../../../../model/util/CodigosTabEvaluacion";
import { ApiGeoforestalService } from "../../../../../service/api-geoforestal.service";
import { UsuarioModel } from "../../../../../model/seguridad/usuario";
import { Perfiles } from "../../../../../model/util/Perfiles";
import { DialogPlanesComponent } from "./dialog-tipo-especie/dialog-planes.component";
import { PfcrDatosOtorgamientoService } from "src/app/service/pfcr-datos-otorgamiento.service";
import { ApiForestalService } from "src/app/service/api-forestal.service";

@Component({
  selector: "anexos-pfcr",
  templateUrl: "./anexos-pfcr.component.html",
  styleUrls: ["./anexos-pfcr.component.scss"],
})
export class AnexosPfcrComponent implements OnInit {
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  @ViewChild("chkInComponenteCatastro", { static: true })
  private chkInComponenteCatastro!: ElementRef;
  @Output() public siguiente = new EventEmitter();
  @Output() public regresar = new EventEmitter();
  @Input() idPermisoForestal!: number;
  //@Input() isPerfilArffs!: boolean;
  codigoEstadoPermiso!: string;
  numDocumento!: string;

  @Input() set codEstadoPF(data: string) {
    this.codigoEstadoPermiso = data;
  }
  @Input() set nroDocumento(data: string) {
    this.numDocumento = data;
  }
  @Input() showRegresar: boolean = true;
  CodigoPermisoForestal = CodigoPermisoForestal;

  labelTituloValidacion: string = "";
  classValidacion: string = "";
  iconEvaluacion: string = "";
  listPlanManejo: any[] = [];

  solicitud: any = {} as SolicitudModel;
  solicitudCargaArchivo!: SolicitudPermisoCargaArchivoModel;

  //idPermisoForestal: number = 0;
  codigoProceso: any = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoTab = CodigosTabEvaluacion.PFCR_TAB_3;
  codigoAcordeonVal = CodigosTabEvaluacion.PFCR_TAB_VAL_3_1;
  codigoAcordeonEval = CodigosTabEvaluacion.PFCR_TAB_EVAL_3_1;

  subCodigoArchivo: any = "PFCR";
  codigoTipoDocumento: any = "PFCROD";
  idSolicitud: number = 0;
  idArchivo: number = 0;
  disabled: boolean = false;
  verEnviar1: boolean = false;
  fileInfGenreal: FileModel = {} as FileModel;
  files: FileModel[] = [];

  isEditedMap = false;
  relacionArchivo!: { idArchivo: number; idPermisoForestalArchivo: number };
  listM: any[] = [];

  CodigosPF = CodigosPermisosForestales;

  file: any = {};

  dataBase: any = {disabled: false};
  listArchivo: any = [];

  solArchivoAdjuntoc = {} as SolicitudPermisoArchivoAdjuntoModel;
  lstSolArchivoAdjunto: SolicitudPermisoArchivoAdjuntoModel[] = [];

  enabledControlGeneral: boolean = false;
  enabledControlOperativo: boolean = false;
  enabledControlDema: boolean = false;
  enabledHabilitarTercero: boolean = false;

  listEscala = [
    { escalaLabel: "Gran escala", escalaValue: "Alta" },
    { escalaLabel: "Mediana escala", escalaValue: "Media" },
    { escalaLabel: "Baja escala", escalaValue: "Baja" },
    { escalaLabel: "Predio privado (PP)", escalaValue: "predioP" },
  ];

  /* mapa inicio*/
  _fileSHP: FileModel = {} as FileModel;
  //_filesSHP: FileModel[] = [];
  _id = this.mapApi.Guid2.newGuid;
  _layers: CustomCapaModel[] = [];
  _layer: any;
  permisoForestalGeometria: PermisoForestalGeometriaModel[] = [];
  view: any = null;
  layersView: LayerView[] = [];
  _files: GroupFile[] = [];
  /* mapa fin */
  _filesSHPProdForestal: FileModel[] = [];

  usuario!: UsuarioModel;
  Perfiles = Perfiles;
  ref!: DynamicDialogRef;
  listPermisoPlanManejo: any[] = [];
  areaTotal!: number;
  otorgamiento: PmfiDatosOtorgamiento = {} as PmfiDatosOtorgamiento;

  constructor(
    private messageService: MessageService,
    private servSol: SolicitudService,
    private dialog: MatDialog,
    private confirmationService: ConfirmationService,
    private router: Router,
    public dialogService: DialogService,
    private anexosService: AnexosService,
    private usuarioService: UsuarioService,
    private toast: ToastService,
    private route: ActivatedRoute,
    private permisoForestalArchivoService: PermisoForestalArchivoService,
    private archivoService: ArchivoService,
    private permisoForestalService: PermisoForestalService,
    private mapApi: MapApi,
    private permisoForestalGeometriaService: PermisoForestalGeometriaService,
    private servicioGeoforestal: ApiGeoforestalService,
    private datosOtorgamientoService: PfcrDatosOtorgamientoService,
    private serviceExternos: ApiForestalService
  ) {}

  ngOnInit(): void {
    this.usuario = this.usuarioService.usuario;

    /*if (this.isPerfilArffs) {
      if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_VALIDADO){
        this.labelTituloValidacion = '';
        this.classValidacion = '';
        this.iconEvaluacion = '';
      }else if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_PROCEDE){
        this.labelTituloValidacion = 'Pendiente';
        this.classValidacion = 'req_anexo_pendiente_class';
        this.iconEvaluacion = 'pi-times-circle';
      }
    }*/

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      if (this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION || this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE ||
          this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE
      ) {
        this.labelTituloValidacion = "";
        this.classValidacion = "";
        this.iconEvaluacion = "";
      }
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      if (
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_PROCEDE || this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_PRESENTADO
      ) {
        this.labelTituloValidacion = "Pendiente";
        this.classValidacion = "req_anexo_pendiente_class";
        this.iconEvaluacion = "pi-times-circle";
      }
    } else if(this.usuario.sirperfil == Perfiles.OSINFOR || this.usuario.sirperfil == Perfiles.SERFOR || this.usuario.sirperfil == Perfiles.COMPONENTE_ESTADISTICO) {
      this.dataBase.disabled = true;
    } else {
      this.labelTituloValidacion = "";
      this.classValidacion = "";
      this.iconEvaluacion = "";
    }

    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";
    this.idPermisoForestal = Number(this.route.snapshot.paramMap.get("idPlan"));
    this.listarArchivos();
    this.initializeMap();
    this.obtenerCapas();
    this.listarPermisoForestalPlanManejo();
    this.listarDatosOtorgamiento();
  }

  // ngAfterViewInit(): void {
  //   this.getInitData();
  // }

  // getInitData() {
  //   this.dialog.open(LoadingComponent, { disableClose: true });
  //   forkJoin({
  //     base64: this.obtenerArchivoMapa(),
  //   })
  //     .pipe(finalize(() => this.dialog.closeAll()))
  //     .subscribe((res) => {
  //       const layers = this.setLayer(this.listM);
  //       if (!isNullOrEmpty(res.base64)) {
  //         this.map.addBase64FileWithConfig(res.base64, layers);
  //       }
  //     });
  // }

  // setLayer(list: any): LayerView[] {
  //   let layers: LayerView[] = [];
  //   for (const item of list) {
  //     if (!isNullOrEmpty(item.actividad) && !isNullOrEmpty(item.descripcion)) {
  //       const layersId = item.actividad.split(';');
  //       const layersName = item.descripcion.split(';');
  //       for (let index = 0; index < layersId.length; index++) {
  //         const layerId = layersId[index];
  //         const title = layersName[index];
  //         const color = item.observacionDetalle;
  //         const groupId = item.actividadesRealizar;
  //         const layer: LayerView = {
  //           color,
  //           groupId,
  //           layerId,
  //           title,
  //           area: 0,
  //           features: [],
  //         };
  //         layers.push(layer);
  //       }
  //     }
  //   }
  //   return layers;
  // }

  // deleteLayer(l: LayerView) {
  //   let item = this.listM.find(
  //     (x) => x.actividadesRealizar == l.groupId
  //   );

  //   if (!isNull(item)) {
  //     item.areaHA = item.areaHA - l.area;
  //     item.areaHA = item.areaHA > 0 ? item.areaHA : 0;
  //     let capas = String(item.descripcion).replace(
  //       this.map.joinTitle(l.title),
  //       ''
  //     );
  //     capas = setOneSemicolon(capas);
  //     capas = onlySemicolons(capas) ? '' : capas;
  //     item.descripcion = capas;
  //     item.observacion = isNullOrEmpty(capas)
  //       ? RespuestaTipo.NO
  //       : RespuestaTipo.SI;
  //     let layerId = String(item.actividad).replace(String(l.layerId), '');
  //     layerId = setOneSemicolon(layerId);
  //     layerId = onlySemicolons(layerId) ? '' : layerId;
  //     item.actividad = layerId;
  //     item.observacionDetalle = isNullOrEmpty(capas)
  //       ? ''
  //       : item.observacionDetalle;
  //   }
  // }

  // deleteAllLayers() {
  //   this.listM.forEach((item) => {
  //     item.areaHA = 0;
  //     item.observacion = RespuestaTipo.NO;
  //     item.descripcion = '';
  //     item.actividad = '';
  //     item.actividadesRealizar = '';
  //     item.observacionDetalle = '';
  //   });
  // }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  enviar() {
    let param: any = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEstado: "EPSFPRES",
      idUsuarioRegistro: this.usuarioService.idUsuario,
    };
    this.permisoForestalService
      .actualizarEstadoPermisoForestal(param)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se finaliza la solicitud del permiso forestal");
          this.router.navigate(["/planificacion/bandeja-permisos-forestales"]);
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  habilitaAlta() {
    this.enabledControlGeneral = true;
    this.enabledControlOperativo = false;
    this.enabledControlDema = false;
  }

  habilitaMedia() {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = true;
    this.enabledControlDema = false;
  }

  habilitaBaja() {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = false;
    this.enabledControlDema = true;
  }

  habilitarTercero(event: any) {
    this.enabledHabilitarTercero = event.value == "Por Terceros";
  }

  redireccionarOperativo() {
    this.router.navigate(["/planificacion/plan-operativo-ccnn-ealta"]);
  }

  redireccionarGeneral() {
    this.router.navigate(["/planificacion/plan-general-manejo"]);
  }

  redireccionarDema() {
    this.router.navigate(["/planificacion/elaboracion-declaracion-mpafpp"]);
    // CAMBIOS
  }

  declaracionJurada() {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: "Se genera declaración jurada.",
    });
  }

  // openEliminarArchivo(
  //   event: Event,
  //   index: number,
  //   data: SolicitudPermisoArchivoAdjuntoModel
  // ) {
  //   this.confirmationService.confirm({
  //     target: event.target || undefined,
  //     message: '¿Esta seguro de querer eliminar?',
  //     icon: 'pi pi-exclamation-triangle',
  //     acceptLabel: 'Si',
  //     rejectLabel: 'No',
  //     accept: () => {
  //       let params = {
  //         idArchivo: data.idArchivo,
  //       };
  //       this.servSol
  //         .eliminarArchivoAdjuntoSolicitud(params)
  //         .subscribe((result: any) => {
  //           this.SuccessMensaje('Se eliminó el archivo');
  //           this.solicitud.listArchivo.splice(index, 1);
  //         });
  //     },
  //     reject: () => {
  //       //reject action
  //     },
  //   });
  // }

  // descargarArchivoAdjunto(data: SolicitudPermisoArchivoAdjuntoModel) {
  //   this.dialog.open(LoadingComponent, { disableClose: true });
  //   if (data.idArchivoSolicitud) {
  //     this.servSol
  //       .descargarArchivoSolicitudAdjunto(
  //         data.idArchivoSolicitud,
  //         this.idSolicitud,
  //         data.tipoDocumento
  //       )
  //       .subscribe((data: any) => {
  //         DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
  //         this.dialog.closeAll();
  //       });
  //   } else {
  //     DowloadFileLocal(data.rutaArchivo, data.nombreArchivo);
  //     this.dialog.closeAll();
  //   }
  // }

  // abrirModal() {
  //   const header = `Adjuntar Declaración Jurada`;
  //   const ref = this.dialogService.open(AdjuntoModalComponent, {
  //     header,
  //     width: '50vw',
  //     closable: true,
  //   });
  //   /*ref.onClose.subscribe(detalle => {
  //     if (detalle) {
  //       if (!CompareObjects(data, detalle)) {
  //         this.pendiente = true;
  //         detalle.enviar = true;
  //       }
  //     }
  //   })*/
  // }

  onFileDowlandDC() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud) {
      let tipoDocumento: string = "DECJUR";
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoDecJur.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoDecJur.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  asignarDocumento(id: number, tipoDocumento: number) {
    this.solArchivoAdjuntoc.idArchivoSolicitud = id != 0 ? id : 0;
    this.solArchivoAdjuntoc.tipoDocumento = tipoDocumento;
  }

  grabarArchivo() {
    if (!this.validarArchivoAdjunto()) return;
    const newArchivo = new ListArchivo();
    newArchivo.idArchivo = this.solArchivoAdjuntoc.idArchivoSolicitud;
    newArchivo.idTipoDocumento = this.solicitud.tipoDocumento;
    newArchivo.descripcion = this.solArchivoAdjuntoc.descripcionArchivo;
    this.solicitud.listArchivo.push(newArchivo);
  }

  validarArchivoAdjunto(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (!this.solArchivoAdjuntoc.descripcionArchivo) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar la descripción del archivo. \n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;

        if (type === "PDF") {
          include = AnexosPfcrComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El tipo de Documento no es Válido.",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB.",
          });
        } else {
          if (type === "PDF") {
            this.fileInfGenreal.url = URL.createObjectURL(e.target.files[0]);
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.fileInfGenreal);
          }
        }
      }
    }
  }

  guardarArchivoGeneral() {
    if (!this.validarArchivoAdjunto()) return;

    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.usuarioService.idUsuario,
          tipoDocumento: this.codigoTipoDocumento,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
      }
    });
  }

  registrarArchivo(id: number) {
    var params = {
      idPermisoForestalArchivo: 0,
      codigoArchivo: this.codigoProceso,
      codigoSubArchivo: this.subCodigoArchivo,
      descripcion: this.solArchivoAdjuntoc.descripcionArchivo,
      idArchivo: id,
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.usuarioService.idUsuario,
      observacion: this.fileInfGenreal.nombreFile,
    };

    let array = [];
    array.push(params);

    this.permisoForestalArchivoService
      .registrarArchivo(array)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok("Se cargó el archivo correctamente.");
          this.fileInfGenreal = {} as FileModel;
          this.files = [];
          this.fileInfGenreal.inServer = false;
          this.fileInfGenreal.descripcion = "PDF";
          this.solArchivoAdjuntoc.descripcionArchivo = "";
          this.listarArchivos();
        } else {
          this.toast.error("Ocurrió un error al realizar la operación.");
        }
      });
  }

  listarArchivos() {
    var params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoArchivo: this.codigoProceso,
      codigoSubArchivo: this.subCodigoArchivo,
      codigoTipoDocumento: this.codigoTipoDocumento,
      idArchivo: null,
    };

    this.permisoForestalArchivoService
      .listarArchivo(params)
      .subscribe((result: any) => {
        if (result.data && result.data.length != 0) {
          this.listArchivo = result.data;
        } else {
          this.listArchivo = [];
        }
      });
  }

  eliminarArchivoGeneral(idArchivo: any) {
    const params = new HttpParams()
      .set("idArchivo", String(idArchivo))
      .set("idUsuarioElimina", String(this.usuarioService.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el archivo correctamente.");
      } else {
        this.toast.error("Ocurrió un error al realizar la operación.");
      }
    });
  }

  descargarArchivo(idArchivo: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idArchivo: idArchivo,
    };
    this.archivoService
      .descargarArchivoGeneral(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.archivo,
            result.data.nombeArchivo,
            result.data.contenTypeArchivo
          );
        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        };
      });
  }

  cambiarEscala(event: any) {
    if (event.value == "Alta") {
      this.habilitaAlta();
    }
    if (event.value == "Media") {
      this.habilitaMedia();
    }
    if (event.value == "Baja") {
      this.habilitaBaja();
    }
  }

  openEliminarArchivo(event: Event, index: number, idArchivo: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el archivo?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        const params = new HttpParams()
          .set("idArchivo", String(idArchivo))
          .set("idUsuarioElimina", String(this.usuarioService.idUsuario));

        this.anexosService
          .eliminarArchivo(params)
          .subscribe((response: any) => {
            if (response.success == true) {
              this.toast.ok("Se eliminó el archivo correctamente.");
              this.listArchivo.splice(index, 1);
            } else {
              this.toast.error("Ocurrió un error al realizar la operación");
            }
          });
      },
      reject: () => {},
    });
  }

  redireccionarPorTipoEscalaManejo() {
    if (this.solicitud.escalaManejo) {
      switch (this.solicitud.escalaManejo) {
        case "Alta":
          // this.router.navigateByUrl('/planificacion/bandeja-pmfi');
          this.openModal("PGMFA");
          break;

        case "Media":
          //  this.router.navigateByUrl("");
          this.openModal("PMFI");
          break;

        case "Baja":
          // this.router.navigateByUrl("/planificacion/bandeja-generacion-dema");
          this.openModal("DEMA");
          break;

        default:
          this.router.navigateByUrl("");
          break;
      }
    }
  }

  openModal(codTipoPlan: string) {
    this.ref = this.dialogService.open(DialogPlanesComponent, {
      header: "Planes de manejo",
      width: "70%",
      contentStyle: { "max-height": "500px", overflow: "auto" },
      data: {
        idPermisoForestal: this.idPermisoForestal,
        codTipoPlan: codTipoPlan,
        numDocumento: this.numDocumento
      },
    });
    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        resp.forEach((element: any) => {
          var params = {
            idPermisoForestal: this.idPermisoForestal,
            idPlanManejo: element.idPlanManejo,
            idUsuarioRegistro: this.usuarioService.idUsuario,
          };
          this.listPlanManejo.push(params);
        });

        this.permisoForestalService
          .registrarPermisoForestalPlanManejo(this.listPlanManejo)
          .subscribe((response: any) => {
            if (response.success) {
              this.toast.ok(response.message);
            } else {
              this.toast.warn(response.message);
            }
            this.listarPermisoForestalPlanManejo();
          });
      }
    });
  }

  //#region ARCHIVO - RELACION ARCHIVO
  // guardarShape() {
  //   this.dialog.open(LoadingComponent, { disableClose: true });
  //   this.saveFileFlow()
  //     .pipe(finalize(() => {
  //       this.dialog.closeAll();
  //       this.toast.ok('Se guardó Shapefile correctamente');
  //     }))
  //     .subscribe();
  // }

  // saveFileFlow() {
  //   if (!this.isEditedMap) return of(null);

  //   const idArchivo = Number(this.relacionArchivo?.idArchivo);
  //   const idPermisoForestalArchivo = Number(this.relacionArchivo?.idPermisoForestalArchivo);

  //   const deleteFile = isNull(this.relacionArchivo)
  //     ? of(null)
  //     : this.deleteFile(idArchivo, idPermisoForestalArchivo).pipe(
  //         tap(() => (this.isEditedMap = false))
  //       );

  //   if (this.map.isEmpty)
  //     return deleteFile.pipe(tap(() => (this.relacionArchivo = undefined as any)));

  //   return this.saveFile()
  //     .pipe(
  //       tap((res: any) => {
  //         this.relacionArchivo = {
  //           idArchivo: res?.data?.idArchivo,
  //           idPermisoForestalArchivo: res?.data?.idPermisoForestalArchivo,
  //         };
  //         this.isEditedMap = false;
  //       })
  //     )
  //     .pipe(concatMap(() => deleteFile));
  // }

  // saveFile() {
  //   return this.map
  //     .getZipFile()
  //     .pipe(tap({ error: (err) => this.toast.warn(err) }))
  //     .pipe(concatMap((blob) => this.guardarArchivo(blob as Blob)))
  //     .pipe(concatMap((idArchivo) => this.guardarRelacionArchivo(idArchivo)));
  // }

  // guardarArchivo(blob: Blob): Observable<number> {
  //   const file = new File([blob], `PFCR-AN-${this.idPermisoForestal}.zip`);
  //   return this.archivoService
  //     .cargar(this.user.idUsuario, ArchivoTipo.SHAPEFILE, file)
  //     .pipe(map((res: any) => res?.data));
  // }

  // guardarRelacionArchivo(idArchivo: number) {
  //   const item = {
  //     codigoArchivo: PGMFArchivoTipo.PFCR,
  //     idPermisoForestal: this.idPermisoForestal,
  //     idUsuarioRegistro: this.user.idUsuario,
  //     idArchivo,
  //   };
  //   let array = [];
  //   array.push(item);
  //   return this.permisoForestalArchivoService.registrarArchivo(array);
  // }

  // deleteFile(idArchivo: number, idPermisoForestalArchivo: number) {
  //   return forkJoin([
  //     this.eliminarArchivo(idArchivo, this.user.idUsuario),
  //     this.eliminarRelacionArchivo(idPermisoForestalArchivo, this.user.idUsuario),
  //   ]).pipe(map(() => null));
  // }

  // eliminarArchivo(idArchivo: number, idUsuarioElimina: number) {
  //   return this.archivoService.eliminarArchivo(idArchivo, idUsuarioElimina);
  // }

  // eliminarRelacionArchivo(idPermisoForestalArchivo: number, idUsuarioElimina: number) {
  //   const item = new PFCRArchivoDto({
  //     idPermisoForestalArchivo,
  //     idUsuarioElimina,
  //   });
  //   return this.permisoForestalArchivoService.eliminarArchivoPermisoForestal(item);
  // }

  // obtenerArchivoMapa(): Observable<string> {
  //   const item = {
  //     codigoTipo: PGMFArchivoTipo.PFCR,
  //     idPermisoForestal: this.idPermisoForestal,
  //     idTipoDocumento: ArchivoTipo.SHAPEFILE,
  //   };
  //   return this.obtenerRelacionArchivo(item)
  //     .pipe(
  //       tap((res) => {
  //         if (!isNull(res)) {
  //           const { idArchivo, idPermisoForestalArchivo } = res;
  //           this.relacionArchivo = { idArchivo, idPermisoForestalArchivo };
  //         }
  //       })
  //     )
  //     .pipe(map((res: any) => (res?.documento ? res.documento : "")));
  // }

  // obtenerRelacionArchivo(item: any) {
  //   return this.permisoForestalArchivoService.obtenerArchivo(item);
  // }
  //#endregion

  /* MAPA INICIO */
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  /*
  onChangeFileSHP(
    e: any,
    withVertice: Boolean,
    code: any,
    codigoSubSeccion: any
  ) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      annex: false,
      codigoSubTipoPGMF: code,
      withVertice: withVertice,
      codigoSubSeccion: codigoSubSeccion,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      //e.target.value = '';
    }
  }
  +/

   */

  onChangeFileProdForestal(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let chkInComponenteCatastro = this.chkInComponenteCatastro.nativeElement;
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: chkInComponenteCatastro.checked,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFileProdForestal(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = "";
    }
  }

  processFileProdForestal(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayersProdForestal(data, config);
      if (config.validate === true) {
        this.validateOverlap(data[0].features[0].geometry.coordinates);
        this.obtenerTiposBosques(data[0].features[0].geometry.coordinates);
      }
      //Calcula área
      this.calculateArea(data[0]);
    });
  }

  obtenerTiposBosques(coordinates: any) {
    let parametros = {
      idClasificacion: "",
      geometria: {
        poligono: coordinates,
      },
    };
    this.serviceExternos
      .identificarTipoBosque(parametros)
      .subscribe((result) => {
        
        if (result.dataService.data.capas.length > 0) {
          result.dataService.data.capas.forEach((t: any) => {
            if (t.geoJson !== null) {
              t.geoJson.crs.type = "name";
              t.geoJson.opacity = 0.4;
              t.geoJson.groupId = this._id;
              t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
              t.geoJson.color = this.mapApi.random();
              t.geoJson.title = t.nombreCapa;
              t.geoJson.service = true;
              this._layer = {} as CustomCapaModel;
              this._layer.codigo = 0;
              this._layer.idLayer = t.geoJson.idLayer;
              this._layer.nombre = t.geoJson.title;
              this._layer.groupId = t.geoJson.groupId;
              this._layer.color = t.geoJson.color;
              this._layer.service = t.geoJson.service;
              this._layers.push(this._layer);
              this.createLayer(t.geoJson);
            }
          });
        }
      });
  }

  validateOverlap(coordinates: any) {
    let item = {
      geometria: {
        poligono: coordinates,
      },
    };

    this.serviceExternos
      .validarSuperposicionAprovechamiento(item)
      .subscribe((result: any) => {
        
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                this._layer = {} as CustomCapaModel;
                this._layer.codigo = 0;
                this._layer.idLayer = t.geoJson.idLayer;
                this._layer.inServer = true;
                this._layer.nombre = t.geoJson.title;
                this._layer.groupId = t.geoJson.groupId;
                this._layer.color = t.geoJson.color;
                this._layer.overlap = t.geoJson.overlap;
                this._layer.service = t.geoJson.service;
                this._layers.push(this._layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === "Si") {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = "name";
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                this._layer = {} as CustomCapaModel;
                this._layer.codigo = 0;
                this._layer.idLayer = t.geoJson.idLayer;
                this._layer.inServer = true;
                this._layer.nombre = t.geoJson.title;
                this._layer.groupId = t.geoJson.groupId;
                this._layer.color = t.geoJson.color;
                this._layer.overlap = t.geoJson.overlap;
                this._layer.service = t.geoJson.service;
                this._layers.push(this._layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
      });
  }

  /*
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      // if(config.codigoSubTipoPGMF === 'PGMFAREA'){
      //   this.calculateArea(data);
      // }
    });
  }*/

  /*createLayers(layers: any, config: any) {
    console.log(layers);
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.withVertice = config.withVertice;
      t.descripcion = config.codigoSubTipoPGMF;
      t.codigoSubSeccion = config.codigoSubSeccion;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layer.annex = config.annex;
      this._layer.descripcion = config.codigoSubTipoPGMF;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this._fileSHP = {} as FileModel;
    this._fileSHP.codigo = config.idArchivo;
    this._fileSHP.file = config.file;
    this._fileSHP.inServer = config.inServer;
    this._fileSHP.idGroupLayer = config.idGroupLayer;
    this._fileSHP.descripcion = config.codigoSubTipoPGMF;
    this._filesSHP.push(this._fileSHP);
  }*/

  createLayersProdForestal(layers: any, config: any) {
    
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this._layer = {} as CustomCapaModel;
      this._layer.codigo = config.idArchivo;
      this._layer.idLayer = t.idLayer;
      this._layer.inServer = config.inServer;
      this._layer.nombre = t.title || config.fileName;
      this._layer.groupId = t.groupId;
      this._layer.color = t.color;
      this._layer.idGroupLayer = config.idGroupLayer;
      this._layers.push(this._layer);
      this.createLayer(t);
    });
    this.file = {} as FileModel;
    this.file.codigo = config.idArchivo;
    this.file.file = config.file;
    this.file.inServer = config.inServer;
    this.file.idGroupLayer = config.idGroupLayer;
    this._filesSHPProdForestal.push(this.file);
  }

  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.descripcion = item.descripcion;
    geoJsonLayer.codigoSubSeccion = item.codigoSubSeccion;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {});
  }

  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.withVertice === true) {
      popupTemplate.content = [
        {
          type: "fields",
          fieldInfos: [
            {
              fieldName: "anexo",
              label: "Anexo",
            },
            {
              fieldName: "vertice",
              label: "Vertice",
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: "este",
              label: "Este (X)",
            },
            {
              fieldName: "norte",
              label: "Norte (Y)",
            },
          ],
        },
      ];
    }
    return popupTemplate;
  }

  onGuardarLayer() {
    let fileUpload: any = [];
    this._filesSHPProdForestal.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.usuarioService.idUsuario,
          codigo: "37",
          codigoTipoPGMF: PGMFArchivoTipo.PFCR,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje("Cargue un archivo para continuar.");
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result) => {
          
          this.registroSolicitud();
          this.SuccessMensaje(result.message);
          this.cleanLayers();
          this.obtenerCapas();
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error.");
        }
      );
  }

  /*onGuardarLayer() {
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: ArchivoTipo.SHAPEFILE,
          codigoTipoPGMF: PGMFArchivoTipo.PFCR,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result) => {
          
          this.SuccessMensaje(result.message);
          this.cleanLayers();
          this.obtenerCapas();
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un error.');
        }
      );
  }*/

  saveFile(item: any) {
    return this.archivoService
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }

  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoArchivo: PGMFArchivoTipo.PFCR,
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.usuarioService.idUsuario,
      idArchivo: result.data,
    };
    let array = [];
    array.push(item2);
    return this.permisoForestalArchivoService
      .registrarArchivo(array)
      .pipe(concatMap(() => this.guardarCapa(item, item2.idArchivo)));
  }

  guardarCapa(itemFile: any, idArchivo: any) {
    
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.permisoForestalGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPermisoForestalGeometria: 0,
        idPermisoForestal: this.idPermisoForestal,
        idArchivo: idArchivo,
        tipoGeometria: t3.descripcion,
        codigoGeometria: null,
        codigoSeccion: CodigosTabEvaluacion.PFCR_TAB_3,
        codigoSubSeccion: CodigosTabEvaluacion.PFCR_TAB_VAL_3_1, //t3.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.usuarioService.idUsuario,
      };
      this.permisoForestalGeometria.push(item);
    });
    
    return this.permisoForestalGeometriaService.registrarPermisoForestalGeometria(
      this.permisoForestalGeometria
    );
  }

  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }

  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }

  obtenerCapas() {
    let item = {
      idPermisoForestal: this.idPermisoForestal,
      codigoSeccion: CodigosTabEvaluacion.PFCR_TAB_3,
      codigoSubSeccion: CodigosTabEvaluacion.PFCR_TAB_VAL_3_1, //'',
    };
    this.permisoForestalGeometriaService
      .listarPermisoForestalGeometria(item)
      .subscribe(
        (result) => {
          
          if (result.data.length) {
            result.data.forEach((t: any) => {
              if (t.geometry_wkt !== null) {
                let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
                
                let groupId = this._id;

                let item = {
                  color: t.colorCapa,
                  title: t.nombreCapa,
                  jsonGeometry: jsonGeometry,
                  properties: {
                    title: t.nombreCapa,
                  },
                };
                this._layer = {} as CustomCapaModel;
                this._layer.codigo = t.idArchivo;
                this._layer.idLayer = this.mapApi.Guid2.newGuid;
                this._layer.inServer = true;
                this._layer.service = false;
                this._layer.nombre = t.nombreCapa;
                this._layer.groupId = groupId;
                this._layer.color = t.colorCapa;
                this._layer.idGroupLayer = this.mapApi.Guid2.newGuid;
                this._layers.push(this._layer);
                let geoJson = this.mapApi.getGeoJson(
                  this._layer.idLayer,
                  groupId,
                  item
                );
                
                this.createLayer(geoJson);
                // if(t.codigoSubSeccion === 'PGMFAIBAMCUAMAMC'){
                //   this.obtenerCordenadaUTM();
                // } else if(t.codigoSubSeccion === 'PGMFAIBAMCUAMAMD'){
                //   this.obtenerCordenadaUTM2();
                // }
              }
            });
          }
        },
        (error) => {
          this.ErrorMensaje("Ocurrió un error");
        }
      );
  }

  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }

  onDownloadFileSHP(id: any) {
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.obtenerArchivo(id).subscribe((result: any) => {
      
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          "application/octet-stream"
        );
      }
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      };
    });
  }

  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: "¿Está seguro de eliminar este grupo de archivos?",
        icon: "pi pi-exclamation-triangle",
        key: "deleteFileSHP",
        acceptLabel: "Si",
        rejectLabel: "No",
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHPProdForestal.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHPProdForestal.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                
                if (this._filesSHPProdForestal.length === 0) {
                  this.cleanLayers();
                }
                this.SuccessMensaje("El archivo se eliminó correctamente.");
              } else {
                this.ErrorMensaje("No se pudo eliminar, vuelve a intertarlo.");
              }
            },
            (error) => {
              
              this.dialog.closeAll();
              this.ErrorMensaje("Ocurrió un problema." + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHPProdForestal.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHPProdForestal.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHPProdForestal.length === 0) {
        this.cleanLayers();
      }
    }
  }

  eliminarArchivoDetalle(idArchivoD: number) {
    let params = {
      idArchivo: idArchivoD,
      idUsuarioElimina: this.usuarioService.idUsuario,
    };
    return this.permisoForestalGeometriaService.eliminarPermisoForestalGeometriaArchivo(
      params
    );
  }

  calculateArea(data: any) {
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    this.areaTotal = Math.abs(sumArea.toFixed(3));
    this.otorgamiento.areaComunidad = this.areaTotal;
  }
  /* MAPA FIN */

  seleccionarValidacion(event: string) {
    if (event == CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO) {
      this.labelTituloValidacion = "Observado";
      this.classValidacion = "req_anexo_observado_class";
      this.iconEvaluacion = "pi-info-circle";
    } else if (event == CodigoPermisoForestal.ESTADO_VAL_VALIDADO) {
      this.labelTituloValidacion = "Validado";
      this.classValidacion = "req_anexo_conforme_class";
      this.iconEvaluacion = "pi-check-circle";
    }
  }
  
  listarPermisoForestalPlanManejo() {
    var params = {
      idPermisoForestal: this.idPermisoForestal
      // codTipoPlan: codTipoPlan,
    };
    this.permisoForestalService
      .listarPermisoForestalPlanManejo(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.listPermisoPlanManejo = [...response.data];
          response.data.forEach((element: any) => {            
            if (element.descripcion == 'PGMFA') {
              this.solicitud.escalaManejo = 'Alta';
            } else if (element.descripcion == 'DEMA') {
              this.solicitud.escalaManejo = 'Baja';
            }
          });
        }
      });
  }

  redireccionar(tipo: any, idPlanManejo: any) {
    switch (tipo) {
      case "PGMFA":
        this.router.navigateByUrl('/planificacion/plan-general-manejo/' + idPlanManejo);
        break;
      case "DEMA":
        this.router.navigateByUrl("/planificacion/generar-declaracion-manejo-dema/" + idPlanManejo);
        break;
      default:
        break;
    }
  }  

  listarDatosOtorgamiento() {
    var params = {
      idPermisoForestal: this.idPermisoForestal,
      codPermiso: 'PFCR',
    };

    this.datosOtorgamientoService
      .listarInformacionOtorgamiento(params)
      .subscribe((response: any) => {
        this.otorgamiento = new PmfiDatosOtorgamiento();
        this.otorgamiento.idPermisoForestal = this.idPermisoForestal;
        if (response.data.length > 0) {
          const element = response.data[ 0 ];
          this.otorgamiento = element;
        }
      });
  }

  registroSolicitud() {
    this.datosOtorgamientoService
      .registrarInformacionOtorgamiento(this.otorgamiento)
      .subscribe((data: any) => {
        if (data.success) {
          this.listarDatosOtorgamiento();
        } else {
          this.ErrorMensaje(data.message);
        }
      });
  }
}

interface Ordenamiento {
  categoria: string;
  areaHA: number;
  observacion: string;
  descripcion: string;
  actividad: string;
  actividadesRealizar: string;
  observacionDetalle: string;
}

class PFCRArchivoDto {
  idPermisoForestalArchivo!: number | null;
  codigoArchivo!: string | null;
  codigoSubArchivo!: string | null;
  descripcion!: string | null;
  detalle!: string | null;
  observacion!: string | null;
  idPermisoForestal!: number | null;
  idArchivo!: number | null;
  codigoTipoDocumento!: string | null;
  nombreArchivo!: string | null;
  extensionArchivo!: string | null;
  idUsuarioRegistro!: number | null;
  idUsuarioElimina!: number | null;

  constructor(obj?: Partial<PFCRArchivoDto>) {
    this.idPermisoForestalArchivo = null;
    this.codigoArchivo = "";
    this.codigoSubArchivo = "";
    this.descripcion = "";
    this.detalle = "";
    this.observacion = "";
    this.idPermisoForestal = null;
    this.idArchivo = null;
    this.codigoTipoDocumento = "";
    this.nombreArchivo = "";
    this.extensionArchivo = "";
    this.idUsuarioRegistro = null;
    this.idUsuarioElimina = null;

    if (obj) Object.assign(this, obj);
  }
}
