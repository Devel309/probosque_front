import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SolicitudAccesoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PfcrInformacionGeneral } from 'src/app/model/pfcr-informacion-general';
import { SolicitudAccesoModel } from 'src/app/model/SolicitudAcceso';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { SolicitudService } from 'src/app/service/solicitud.service';
import { EvaluacionArchivoModel } from '../../../../../model/Comun/EvaluacionArchivoModel';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { EvaluacionUtils } from '../../../../../model/util/EvaluacionUtils';
import { Mensajes } from '../../../../../model/util/Mensajes';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { ValidarRequisitosModal } from '../../../evaluar-permiso-forestal-ccnn/modal/validar-requisitos-modal/validar-requisitos.modal';
import { EvaluacionArchivoPermisoForestalComponent } from '../../../../../shared/components/evaluacion-archivo-permiso-forestal/evaluacion-archivo-permiso-forestal.component';
import { CodigoEstadoEvaluacion } from '../../../../../model/util/CodigoEstadoEvaluacion';
import { CodigoPermisoForestal } from '../../../../../model/util/CodigoPermisoForestal';
import { Perfiles } from '../../../../../model/util/Perfiles';
import { UsuarioModel } from '../../../../../model/seguridad/usuario';
import {ValidarCondicionMinimaComponent} from '../../../../../shared/components/validar-condicion-minima/validar-condicion-minima.component';

@Component({
  selector: 'informacion-general-pfcr',
  templateUrl: './informacion-general-pfcr.component.html',
  styleUrls: ['./informacion-general-pfcr.component.scss'],
})
export class InformacionGeneralPfcrComponent implements OnInit {
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();
  @Input() idPermisoForestal!: number;
  @Input() showRegresar: boolean = true;
  //@Input() isPerfilArffs!: boolean;
  //@Input() codigoEstado!: boolean;
  codigoEstadoPermiso!: string;
  @Output() comunidad = new EventEmitter();
  @Output() codEstadoPF = new EventEmitter();
  @Output() nroDocumento = new EventEmitter();

  labelTituloValidacion: string = '';
  classValidacion: string = '';
  iconEvaluacion: string = '';

  dataBase: any = { tipoPersona: '', disabled: false };
  enabledControl: boolean = false;
  enabledControlOtorgamiento: string = 'false';
  idSolicitudEdit: string | null = null;
  idSolicitud: number = 0;
  validDatosPersona: boolean = false;
  validDatosPlanManejo: boolean = false;
  validDatosArea: boolean = false;
  validDatosAnexo: boolean = false;
  enabledHabilitarContinuar: boolean = false;
  enabledControlGeneral: boolean = false;
  enabledControlOperativo: boolean = false;
  enabledControlDema: boolean = false;

  // solicitudAcceso = {} as SolicitudAccesoModel;
  solicitudAcceso = {} as any;
  edo = {} as any;
  informacionGeneral: PfcrInformacionGeneral = new PfcrInformacionGeneral();
  isEdit: boolean = false;

  numeroDocumento: any;

  tabIndex: number = 0;

  CodigoPermisoForestal = CodigoPermisoForestal;

  codigoProceso = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoTab = CodigosTabEvaluacion.PFCR_TAB_1;
  codigoAcordeonInfGeneralEval: string = CodigosTabEvaluacion.PFCR_TAB_EVAL_1_1;
  codigoAcordeonInfGeneralValidacion: string =
    CodigosTabEvaluacion.PFCR_TAB_VAL_1_1;

  /*evaluacionInfGeneral: EvaluacionArchivoModel = new EvaluacionArchivoModel({
    codigoEvaluacionDet: this.codigoProceso,
    codigoEvaluacionDetSub: this.codigoTab,
    codigoEvaluacionDetPost: this.codigoAcordeonInfGeneralEval,
  });

  evaluacion: any = {
    codigoEvaluacion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
    idEvaluacion: 0,
    estadoEvaluacion: 'EEVAPRES',
    fechaEvaluacionInicial: new Date().toISOString(),
    idPlanManejo: this.idPermisoForestal,
    idUsuarioRegistro: this.user.idUsuario,
    listarEvaluacionDetalle: [],
  };*/
  usuario!: UsuarioModel;
  idInfGeneral: number = 0;
  Perfiles = Perfiles;
  ref!: DynamicDialogRef;
  observado: boolean = false;
  estadoPF!: string;

  constructor(
    private messageService: MessageService,
    private servSA: SolicitudAccesoService,
    private servSol: SolicitudService,
    private route: ActivatedRoute,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private informacionGeneralService: PermisoForestalService,
    private usuarioService: UsuarioService,
    private toastService: ToastService,
    private evaluacionService: EvaluacionService
  ) {
    // this.idPlanManejo = Number(this.route.snapshot.paramMap.get('idPlan'));
  }

  ngOnInit(): void {
    this.usuario = this.usuarioService.usuario;
    this.listarInfGeneral();

    //if (this.isPerfilArffs) this.obtenerEvaluacion();
    //if (this.isPerfilArffs) {

    /*if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_VALIDADO) {
        this.labelTituloValidacion = '';
        this.classValidacion = '';
        this.iconEvaluacion = '';
      }else if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_PROCEDE){
        this.labelTituloValidacion = 'Pendiente';
        this.classValidacion = 'req_pendiente_class';
        this.iconEvaluacion = 'pi-times-circle';
      }
    }*/
  }

  obtenerEvaluacion() {
    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      tipoEvaluacion: CodigoPermisoForestal.TIPO_EVALUACION,
    };

    this.evaluacionService
      .obtenerEvaluacionPermisoForestal(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            let evaluacion = result.data[0].listarEvaluacionPermisoDetalle;

            let item = evaluacion.find(
              (e: any) => e.descripcion == '1. Información General'
            );

            if (item) {
              if (item.conforme == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE
                && this.estadoPF == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE) {
                this.observado = true;
              }
            }
          }
        }
      });
  }

  setearValidacion(){
    if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      if (
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_PROCEDE || this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_PRESENTADO
      ) {
        this.labelTituloValidacion = 'Pendiente';
        this.classValidacion = 'req_pendiente_class';
        this.iconEvaluacion = 'pi-times-circle';
      }
    } else if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      if (
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION
      ) {
        this.labelTituloValidacion = '';
        this.classValidacion = '';
        this.iconEvaluacion = '';
      }
    } else {
      this.labelTituloValidacion = '';
      this.classValidacion = '';
      this.iconEvaluacion = '';
    }
  }

  obtenerSolicitudAcceso() {
    let numeroDoc = '';
    let numeroRucEmp = '';
    if (this.usuarioService.idtipoDocumento == 4) {
      numeroRucEmp = this.usuarioService.nroDocumento;
    } else {
      numeroDoc = this.usuarioService.nroDocumento;
    }
    const params = {
      codigoEstadoSolicitud: 'ESACREGI',
      numeroDocumento: numeroDoc,
      numeroRucEmpresa: numeroRucEmp
    };

    this.servSA.obtenerSolicitudAcceso(params).subscribe((result: any) => {
      if (result.data) {
        this.solicitudAcceso = result.data;
        this.solicitudAcceso.codigoTipoDocumentoE = 'TDOCRUC';

        this.solicitudAcceso.idDistritoPersona = parseInt(
          result.data.idDistritoPersona
        );

        this.edo.idProvinciaPersona = parseInt(result.data.idProvinciaPersona);
        this.edo.idDepartamentoPersona = parseInt(
          result.data.idDepartamentoPersona
        );
        if (result.data.codigoTipoPersona == 'TPERJURI') {
          this.nroDocumento.emit(result.data.numeroRucEmpresa);
        } else {
          this.nroDocumento.emit(result.data.numeroDocumento);
        }

        this.setearValidacion();
      }
    });
  }

  registrarArchivoSolicitud2(
    idSolicitud: number,
    archivo: File,
    tipoArchivo: string
  ) {
    this.servSol
      .registrarArchivoSolicitud(idSolicitud, archivo, tipoArchivo)
      .subscribe((response: any) => {});
  }

  toast(severty: string, msg: string) {
    this.messageService.add({ severity: severty, summary: '', detail: msg });
  }

  // BOTON GUARDAR

  validarRequisitos() {
    const ref = this.dialogService.open(ValidarRequisitosModal, {
      header: 'Validar Requisitos',
      width: '44%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });

    ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.messageService.add({
          severity: 'success',
          detail: 'Se validaron los requisitos',
        });
      }
    });
  }

  // BOTON CONTINUAR

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  listarInfGeneral() {
    var params = {
      idInfGeneral: null,
      codTipoInfGeneral: 'PFCR',
      idPermisoForestal: this.idPermisoForestal,
    };
    this.informacionGeneralService
      .listarInformacionGeneralPF(params)
      .pipe(finalize(() => this.obtenerEvaluacion()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          this.isEdit = true;
          const element = response.data[0];

          this.edo.nombreDepartamento = element.departamentoTitular;
          this.edo.nombreProvincia = element.provinciaTitular;

          const infoGral = new PfcrInformacionGeneral(element);
          infoGral.ubigeoTitular = parseInt(element.ubigeoTitular);

          this.solicitudAcceso = infoGral;

          this.edo.idProvinciaPersona = element.idProvincia;
          this.edo.idDepartamentoPersona = element.idDepartamento;
          this.comunidad.emit(infoGral.codTipoCncc);
          this.codigoEstadoPermiso = element.codEstadoPF;
          this.codEstadoPF.emit(element.codEstadoPF);
          this.estadoPF = element.codEstadoPF;          
          if (element.codTipoPersona == 'TPERJURI') {
            this.nroDocumento.emit(element.rucComunidad);
          } else {
            this.nroDocumento.emit(element.documentoElaborador);
          }

          this.setearValidacion();

          this.obtenerSolicitudAccesoID(element.codTipoPersona, element.documentoElaborador, element.rucComunidad);
        } else {
          this.isEdit = false;

          this.obtenerSolicitudAcceso();
        }
      });
  }  

  obtenerSolicitudAccesoID(codTipoPersona: string, documentoElaborador:string, rucComunidad: string) {
    let numeroDoc = '';
    let numeroRucEmp = '';
    if (codTipoPersona == 'TPERJURI') {
      numeroRucEmp = rucComunidad;
    } else {
      numeroDoc = documentoElaborador;
    }
    const params = {
      codigoEstadoSolicitud: 'ESACREGI',
      numeroDocumento: numeroDoc,
      numeroRucEmpresa: numeroRucEmp
    };

    this.servSA.obtenerSolicitudAcceso(params).subscribe((result: any) => {
      if (result.data) {
        this.solicitudAcceso.idSolicitudAcceso = result.data.idSolicitudAcceso;        
      }
    });
  }

  registrarInformacionGeneral() {
    if (this.isEdit) {
      if (!this.validarEmail(this.solicitudAcceso.correo)) {
        this.toast(
          'warn',
          '(*) El email de la Persona tiene un formato incorrecto.'
        );
        return;
      }

      if (this.solicitudAcceso.codTipoPersona == 'TPERJURI') {
        if (!this.validarEmail(this.solicitudAcceso.correoEmpresa)) {
          this.toast(
            'warn',
            '(*) El email de la Empresa tiene un formato incorrecto.'
          );
          return;
        }
      }
    } else {
      if (!this.validarEmail(this.solicitudAcceso.email)) {
        this.toast(
          'warn',
          '(*) El email de la Persona tiene un formato incorrecto.'
        );
        return;
      }

      if (this.solicitudAcceso.codTipoPersona == 'TPERJURI') {
        if (!this.validarEmail(this.solicitudAcceso.emailEmpresa)) {
          this.toast(
            'warn',
            '(*) El email de la Empresa tiene un formato incorrecto.'
          );
          return;
        }
      }
    }

    let infoGral;
    if (this.isEdit) {
      infoGral = new PfcrInformacionGeneral();
      infoGral.idInfGeneral = this.solicitudAcceso.idInfGeneral;
      infoGral.idUsuarioRegistro = this.usuarioService.idUsuario;
      infoGral.idPermisoForestal = this.solicitudAcceso.idPermisoForestal;
      infoGral.fechaPresentacion = this.solicitudAcceso.fechaPresentacion;
      infoGral.apellidoPaternoElaborador =
        this.solicitudAcceso.apellidoPaternoElaborador;
      infoGral.apellidoMaternoElaborador =
        this.solicitudAcceso.apellidoMaternoElaborador;
      infoGral.nombreElaborador = this.solicitudAcceso.nombreElaborador;
      infoGral.correo = this.solicitudAcceso.correo;
      infoGral.tipoDocumentoElaborador =
        this.solicitudAcceso.tipoDocumentoElaborador;
      infoGral.documentoElaborador = this.solicitudAcceso.documentoElaborador;
      infoGral.esReprLegal = this.solicitudAcceso.esReprLegal;
      infoGral.codTipoPersona = this.solicitudAcceso.codTipoPersona;
      infoGral.codTipoActor = this.solicitudAcceso.codTipoActor;
      infoGral.codTipoCncc =
        this.solicitudAcceso.codTipoActor == 'TACTTCNC'
          ? this.solicitudAcceso.codTipoCncc
          : null;
      infoGral.codTipoDocumento =
        this.solicitudAcceso.codTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.codTipoDocumento;
      infoGral.rucComunidad =
        this.solicitudAcceso.codTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.rucComunidad;
      infoGral.federacionComunidad =
        this.solicitudAcceso.codTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.federacionComunidad;
      infoGral.domicilioLegal =
        this.solicitudAcceso.codTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.domicilioLegal;
      infoGral.correoEmpresa =
        this.solicitudAcceso.codTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.correoEmpresa;
      infoGral.ubigeoTitular = this.solicitudAcceso.ubigeoTitular.toString();
    } else {
      infoGral = new PfcrInformacionGeneral();
      infoGral.idUsuarioRegistro = this.usuarioService.idUsuario;
      infoGral.idPermisoForestal = this.idPermisoForestal;
      infoGral.fechaPresentacion = this.solicitudAcceso.fechaPresentacion;
      infoGral.apellidoPaternoElaborador = this.solicitudAcceso.apellidoPaterno;
      infoGral.apellidoMaternoElaborador = this.solicitudAcceso.apellidoMaterno;
      infoGral.nombreElaborador = this.solicitudAcceso.nombres;
      infoGral.correo = this.solicitudAcceso.email;
      infoGral.tipoDocumentoElaborador =
        this.solicitudAcceso.codigoTipoDocumento;
      infoGral.documentoElaborador = this.solicitudAcceso.numeroDocumento;
      infoGral.esReprLegal = this.solicitudAcceso.esReprLegal;
      infoGral.codTipoPersona = this.solicitudAcceso.codigoTipoPersona;
      infoGral.codTipoActor = this.solicitudAcceso.codigoTipoActor;
      infoGral.codTipoCncc =
        this.solicitudAcceso.codigoTipoActor == 'TACTTCNC'
          ? this.solicitudAcceso.codigoTipoCncc
          : null;
      infoGral.codTipoDocumento =
        this.solicitudAcceso.codigoTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.codigoTipoDocumentoE;
      infoGral.rucComunidad =
        this.solicitudAcceso.codigoTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.numeroRucEmpresa;
      infoGral.federacionComunidad =
        this.solicitudAcceso.codigoTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.razonSocialEmpresa;
      infoGral.domicilioLegal =
        this.solicitudAcceso.codigoTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.direccionEmpresa;
      infoGral.correoEmpresa =
        this.solicitudAcceso.codigoTipoPersona == 'TPERNATU'
          ? ''
          : this.solicitudAcceso.emailEmpresa;
      infoGral.ubigeoTitular =
        this.solicitudAcceso.idDistritoPersona.toString();
    }

    this.informacionGeneralService
      .registrarInformacionGeneralPF(infoGral)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.SuccessMensaje(response.message);
          this.listarInfGeneral();
        } else {
          this.ErrorMensaje(response.message);
        }
      });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  registrarEvaluacion() {
    //this.hijo.registrarEvaluacion();
  }

  validarEmail(valor: any) {
    if (
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
        valor
      )
    ) {
      return true;
    } else {
      return false;
    }
  }

  seleccionarValidacion(event: string) {
    if (event == CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO) {
      this.labelTituloValidacion = 'Observado';
      this.classValidacion = 'req_observado_class';
      this.iconEvaluacion = 'pi-info-circle';
    } else if (event == CodigoPermisoForestal.ESTADO_VAL_VALIDADO) {
      this.labelTituloValidacion = 'Validado';
      this.classValidacion = 'req_conforme_class';
      this.iconEvaluacion = 'pi-check-circle';
    }
  }

  validarCondiciones() {

    console.log("this.solicitudAcceso ",this.solicitudAcceso);

    let param = {
      nombre: "",
      apellidoPaterno: "",
      apellidoMaterno: "",
      tipoDocumento: this.solicitudAcceso.codigoEstadoSolicitud != null ? this.solicitudAcceso.codigoTipoDocumentoE: this.solicitudAcceso.codTipoDocumento,
      numeroDocumento: this.solicitudAcceso.codigoEstadoSolicitud != null ? this.solicitudAcceso.numeroRucEmpresa: this.solicitudAcceso.rucComunidad,
    };

    console.log("param",param);

    if(param.tipoDocumento == 'TDOCRUC'){
      param.tipoDocumento = 'RUC';
      param.nombre= this.solicitudAcceso.federacionComunidad;


    } else {
      param.tipoDocumento = 'DNI';
      param.nombre= this.solicitudAcceso.nombreElaborador;
      param.apellidoPaterno= this.solicitudAcceso.apellidoPaternoElaborador;
      param.apellidoMaterno= this.solicitudAcceso.apellidoMaternoElaborador;
      param.numeroDocumento = this.solicitudAcceso.numeroDocumento?this.solicitudAcceso.numeroDocumento:this.solicitudAcceso.documentoElaborador;
    }

    this.ref = this.dialogService.open(ValidarCondicionMinimaComponent, {
      header: 'Validar Condiciones Mínimas',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
      baseZIndex: 10000,
      data: {
        idCondicionMinima: null,
        obj: param
      },
    });
  }




  /*
  obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPermisoForestal,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionInfGeneral = Object.assign(
                this.evaluacionInfGeneral,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost ==
                    this.codigoAcordeonInfGeneralEval
                )
              );
            }
          }
        }
      });
  }



  registrarEvaluacion() {
    if (EvaluacionUtils.validar([this.evaluacionInfGeneral])) {
      //if (this.evaluacion) {
      this.evaluacion.idPlanManejo = this.idPermisoForestal;
      this.evaluacion.listarEvaluacionDetalle = [];
      this.evaluacion.listarEvaluacionDetalle.push(this.evaluacionInfGeneral);

      this.dialog.open(LoadingComponent, { disableClose: true });

      this.evaluacionService
        .registrarEvaluacionPlanManejo(this.evaluacion)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
          this.toastService.ok(res.message);
          this.obtenerEvaluacion();
        });
      //}
    } else {
      this.toastService.warn(Mensajes.MSJ_EVALUACIONES);
    }
  }

 */
}
