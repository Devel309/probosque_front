import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { SolicitudAccesoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import {
  ListArchivo,
  PmfiDatosOtorgamiento,
} from 'src/app/model/PfcrDatosOtorgamientoModel';
import { SolicitudModel } from 'src/app/model/Solicitud';
import { SolicitudPermisoCargaArchivoModel } from 'src/app/model/SolicitudArchivo';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { PfcrDatosOtorgamientoService } from 'src/app/service/pfcr-datos-otorgamiento.service';
import { SolicitudService } from 'src/app/service/solicitud.service';
import { EvaluacionPermisoForestalModel } from '../../../../../model/Comun/EvaluacionPermisoForestalModel';
import { CodigoEstadoEvaluacion } from '../../../../../model/util/CodigoEstadoEvaluacion';
import { CodigoPermisoForestal } from '../../../../../model/util/CodigoPermisoForestal';
import { CodigoProceso } from '../../../../../model/util/CodigoProceso';
import { CodigosTabEvaluacion } from '../../../../../model/util/CodigosTabEvaluacion';
import { EvaluacionService } from '../../../../../service/evaluacion/evaluacion.service';
import { UsuarioModel } from '../../../../../model/seguridad/usuario';
import { Perfiles } from '../../../../../model/util/Perfiles';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'datos-aprovechamiento-pfcr',
  templateUrl: './datos-aprovechamiento-pfcr.component.html',
  styleUrls: ['./datos-aprovechamiento-pfcr.component.scss'],
})
export class DatosAprovechamientoPfcrComponent implements OnInit {
  @Output() siguiente = new EventEmitter();
  @Output() regresar = new EventEmitter();
  @Output() sunat = new EventEmitter();
  @Output() sunarp = new EventEmitter();
  //@Input() isPerfilArffs!: boolean;
  @Input() idPermisoForestal!: number;
  @Input() comunidad!: string;
  @Input() showRegresar: boolean = true;
  codigoEstadoPermiso!: string;

  @Input() set codEstadoPF(data: string) {
    this.codigoEstadoPermiso = data;
    this.solicitud.estadoSolicitud = data;
  }
  @Input() nroDocumento!: string;

  labelTituloValidacion: string = '';
  classValidacion: string = '';
  iconEvaluacion: string = '';
  idPlanManejoRegente: number = 0

  solicitud = {} as SolicitudModel;
  dataBase: any = { tipoPersona: '', disabled: false };
  solicitudCargaArchivo = {} as SolicitudPermisoCargaArchivoModel;
  validDatosPersona: boolean = false;
  validDatosPlanManejo: boolean = false;
  validDatosArea: boolean = false;
  validDatosAnexo: boolean = false;
  enabledHabilitarContinuar: boolean = false;
  accion: string | null = 'nuevo';
  enabledControlGeneral: boolean = false;
  enabledControlOperativo: boolean = false;
  enabledControlDema: boolean = false;
  enabledControlOtorgamiento: string = 'false';
  idSolAcceso: string | null = '';
  idPersona: string | null = '';

  isEdit: boolean = false;
  idOtorgamiento: number = 0;
  idSolicitudEdit: string | null = null;

  codigoProceso = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoTab = CodigosTabEvaluacion.PFCR_TAB_2;

  codigoAcordeonVal: string = CodigosTabEvaluacion.PFCR_TAB_VAL_2_1;
  codigoAcordeon: string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1;

  codigoAcordeonItem1: string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_1;
  codigoAcordeonItem2: string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_2;
  codigoAcordeonItem3: string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_3;
  codigoAcordeonItem4: string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1_4;
  /*
=======
  codigoAcordeonDatosOtorgEval: string =
    CodigosTabEvaluacion.PFCR_TAB_2_1 + 'VAL';
  /*
>>>>>>> 8b8e2fc118a2efafcea79624f7dd98dc74f073f7
  evaluacionDatosOtorgamiento: EvaluacionArchivoModel =
    new EvaluacionArchivoModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonDatosOtorgEval,
    });

  evaluacion: any = {
    codigoEvaluacion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
    idEvaluacion: 0,
    estadoEvaluacion: 'EEVAPRES',
    fechaEvaluacionInicial: new Date().toISOString(),
    idPlanManejo: this.idPermisoForestal,
    idUsuarioRegistro: this.user.idUsuario,
    listarEvaluacionDetalle: [],
  };*/

  listArchivo: ListArchivo[] = [];
  array1: ListArchivo[] = [];
  array2: ListArchivo[] = [];
  array3: ListArchivo[] = [];
  array4: ListArchivo[] = [];
  array5: ListArchivo[] = [];
  array6: ListArchivo[] = [];
  array7: ListArchivo[] = [];
  array8: ListArchivo[] = [];
  array9: ListArchivo[] = [];
  array10: ListArchivo[] = [];
  array11: ListArchivo[] = [];

  ///**CAMPOS PARA EVALUACION*/
  //CodigoEstadoEvaluacion =  CodigoEstadoEvaluacion;
  CodigoPermisoForestal = CodigoPermisoForestal;
  evaluacion: any;
  evaluacionDet1!: EvaluacionPermisoForestalModel;
  evaluacionDet2!: EvaluacionPermisoForestalModel;
  evaluacionDet3!: EvaluacionPermisoForestalModel;
  evaluacionDet4!: EvaluacionPermisoForestalModel;

  usuario!: UsuarioModel;
  Perfiles = Perfiles;
  isShowModal2_2:boolean=false;

  listaPlanes:any[] = [];
  favorable:string = "";

  constructor(
    private messageService: MessageService,
    private servSA: SolicitudAccesoService,
    private servPf: PermisoForestalService,
    private servSol: SolicitudService,
    private route: ActivatedRoute,
    public dialogService: DialogService,
    private dialog: MatDialog,
    private datosOtorgamientoService: PfcrDatosOtorgamientoService,
    private usuarioService: UsuarioService,
    private evaluacionService: EvaluacionService,
    private toastService: ToastService,
    private toast: ToastService
  ) {
    this.evaluacion = {
      codigoEvaluacion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idEvaluacionPermiso: 0,
      estadoEvaluacion: 'EEVAPRES',
      tipoEvaluacion: CodigoPermisoForestal.TIPO_EVALUACION,
      fechaEvaluacionInicial: new Date().toISOString(),
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.usuarioService.idUsuario,
      listarEvaluacionPermisoDetalle: [],
    };
  }

  ngOnInit(): void {
    this.usuario = this.usuarioService.usuario;

    this.listaPlanes.push({
      nombrePlan:'PGMF',
      codigoPlan:'PGMF',
    })

    this.listaPlanes.push({
      nombrePlan:'PMFI',
      codigoPlan:'PMFI',
    })


    this.listaPlanes.push({
      nombrePlan:'DEMA',
      codigoPlan:'DEMA',
    })


    this.evaluacionDet1 = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonItem1,
      detalle: '1. Resultado de Busqueda en Registro Nacional de infractores',
      descripcion: '2. Datos de Otorgamiento',
    });

    this.evaluacionDet2 = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonItem2,
      detalle: '2. Resultado de Opiniones de otras Entidades (SENAMP)',
      descripcion: '2. Datos de Otorgamiento',
    });

    this.evaluacionDet3 = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonItem3,
      detalle: '3. Resultado de Inspección Ocular',
      descripcion: '2. Datos de Otorgamiento',
    });

    this.evaluacionDet4 = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      codigoEvaluacionDetPost: this.codigoAcordeonItem4,
      detalle: '4. Resultado de Informe Plan General PO o DEMA',
      descripcion: '2. Datos de Otorgamiento',
    });

    if (this.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      if (
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION ||
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE ||
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE
      ) {
        this.obtenerEvaluacion();
        this.labelTituloValidacion = '';
        this.classValidacion = '';
        this.iconEvaluacion = '';
      }
    } else if (this.usuario.sirperfil == Perfiles.MESA_DE_PARTES) {
      if (
        this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_PROCEDE || this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_PRESENTADO
      ) {
        this.labelTituloValidacion = 'Pendiente';
        this.classValidacion = 'req_pendiente_class';
        this.iconEvaluacion = 'pi-times-circle';
      }
    } else if(this.usuario.sirperfil == Perfiles.OSINFOR || this.usuario.sirperfil == Perfiles.SERFOR || this.usuario.sirperfil == Perfiles.COMPONENTE_ESTADISTICO) {
      this.dataBase.disabled = true;
    } else {
      this.labelTituloValidacion = '';
      this.classValidacion = '';
      this.iconEvaluacion = '';
    }

    /*

    if (this.isPerfilArffs) {
      if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_VALIDADO){
        this.obtenerEvaluacion();
        this.labelTituloValidacion = '';
        this.classValidacion = '';
        this.iconEvaluacion = '';
      }else if(this.codigoEstadoPermiso == CodigoPermisoForestal.ESTADO_VAL_PROCEDE){
        this.labelTituloValidacion = 'Pendiente';
        this.classValidacion = 'req_pendiente_class';
        this.iconEvaluacion = 'pi-times-circle';
      }

    }
    */

    /*if (this.isPerfilArffs) {
      this.obtenerEvaluacion();
      this.labelTituloValidacion = 'Pendiente';
      this.classValidacion = 'req_pendiente_class';
      this.iconEvaluacion = 'pi-times-circle';
    }*/

    this.listarDatosOtorgamiento();

    this.solicitud.idSolicitud = Number(
      this.route.snapshot.paramMap.get('idPlan')
    );

    //if (this.isPerfilArffs) this.obtenerEvaluacion();
  }

  obtenerSolicitudPorAccesoSolicitud() {
    let idSolicitudAcceso: number =
      this.idSolAcceso != null ? parseInt(this.idSolAcceso) : 0;
    let idSolicitud: number =
      this.idSolicitudEdit != null ? parseInt(this.idSolicitudEdit) : 0;
    this.servSol
      .obtenerSolicitudPorAccesoSolicitud(idSolicitud, idSolicitudAcceso)
      .subscribe((response: any) => {
        this.solicitud = response.data;
      });
  }

  obtenerSolicitudAcceso() {
    const params = {
      codigoEstadoSolicitud: 'ESACREGI',
      numeroDocumento: this.usuarioService.nroDocumento,
    };

    this.servSA.obtenerSolicitudAcceso(params).subscribe((result: any) => {
      if (result.data) {
        this.idSolAcceso = result.data.idSolicitudAcceso;
        this.solicitud.idSolicitud = result.data.idSolicitudAcceso;
      }
    });
  }

  listarDatosOtorgamiento() {
    var params = {
      idPermisoForestal: this.idPermisoForestal,
      codPermiso: 'PFCR',
    };

    this.datosOtorgamientoService
      .listarInformacionOtorgamiento(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          this.isEdit = true;

          const element = response.data[0];
          this.idOtorgamiento = element.idOtorgamiento;

          this.solicitud.estadoSolicitud = element.estadoSolicitud;
          this.solicitud.validaSunarp =
            element.nroPartidaRegistral != '' ? true : false;
          this.solicitud.validaSunat = element.nroRuc != '' ? true : false;
          this.solicitud.nroRucEmpresa = element.nroRuc;
          this.solicitud.nroPropiedad = element.nroTituloPropiedad;
          this.solicitud.nroPartidaRegistral = element.nroPartidaRegistral;
          this.solicitud.nroActaAsambleaRrll = element.nroActaAsambleaRrll;
          this.solicitud.nroComprobantePago =
            element.nroOperacionComprobantePago;
          this.solicitud.areaComunidad = element.areaComunidad;
          this.solicitud.nroActaAsambleaAcuerdo =
            element.nroActaAcuerdoAsamblea;
          this.solicitud.codigoTipoAprovechamiento = parseInt(
            element.codTipoAprovechamiento
          );
          this.solicitud.nroContratoTercero = element.nroContratoTercero
          if(element.regente){
            this.solicitud.apellidosRegente = element.regente.apellidos;
            this.solicitud.nombresRegente = element.regente.nombres;
            this.solicitud.regenteForestal = element.regente.numeroDocumento;
            this.solicitud.numeroLicenciaRegente = element.regente.numeroLicencia;
            this.solicitud.periodoRegente = element.regente.periodo;
            this.solicitud.nroContratoRegente = element.regente.contratoSuscrito;
            this.solicitud.nombreRegenteForestal = `${element.regente.nombres} ${element.regente.apellidos} - ${element.regente.numeroDocumento}`;
            this.idPlanManejoRegente = element.regente.idPlanManejoRegente;

          }

          this.sunarp.emit(this.solicitud.validaSunarp);
          this.sunat.emit(this.solicitud.validaSunat);
        } else {
          this.isEdit = false;
        }
      });
  }

  validaSolicitud() {
    let validar: boolean = true;
    let mensaje: string = '';

    // if (!this.solicitud.nroRucEmpresa) {
    //   validar = false;
    //   mensaje = mensaje += '(*) Debe ingresar: Número de RUC.\n';
    // }

    if (this.solicitud.nroRucEmpresa && !this.solicitud.validaSunarp) {
      validar = false;
      mensaje = mensaje +=
        '(*) Debe ingresar: Partida Registral válida en SUNARP.\n';
    }

    if (this.solicitud.nroRucEmpresa && !this.solicitud.validaSunat) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: RUG válido en SUNAT.\n';
    }

    // if (!this.solicitud.nroPropiedad) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Debe ingresar: Número de Título de Propiedad.\n';
    // }

    // if (!this.solicitud.nroPartidaRegistral) {
    //   validar = false;
    //   mensaje = mensaje += '(*) Debe ingresar: Número de Partida Registral.\n';
    // }

    // if (!this.solicitud.nroActaAsambleaRrll) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Debe ingresar: Número de Acta de Asamblea RRLL.\n';
    // }

    // if (!this.solicitud.nroComprobantePago) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Debe ingresar: Número de Operación del Comprobante de Pago.\n';
    // }

    // if (!this.solicitud.nroActaAsambleaAcuerdo) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Debe ingresar: Número de Acta de Acuerdo de Asamblea.\n';
    // }

    // if (!this.solicitud.nombreRegenteForestal) {
    //   validar = false;
    //   mensaje = mensaje += '(*) Debe ingresar: Regente Forestal.\n';
    // }

    // if (!this.solicitud.nroContratoRegente) {
    //   validar = false;
    //   mensaje = mensaje +=
    //     '(*) Debe ingresar: Número de Contrato con Regente Forestal.\n';
    // }

    // if (!this.solicitud.codigoTipoAprovechamiento) {
    //   validar = false;
    //   mensaje = mensaje += '(*) Debe ingresar: Tipo de Aprovechamiento.\n';
    // }

    // if (this.solicitud.codigoTipoAprovechamiento == 2) {
    //   if (!this.solicitud.nroContratoTercero) {
    //     validar = false;
    //     mensaje = mensaje +=
    //       '(*) Debe ingresar: Número de Contrato con Terceros.\n';
    //   }
    // }

    // if (!this.solicitud.escalaManejo) {
    //   validar = false;
    //   mensaje = mensaje += '(*) Debe ingresar: Escala de Manejo.\n';
    // }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  hayDocumentos() {
    if (!!this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud) {
      const doc1 = new ListArchivo();
      doc1.idArchivo =
        this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud;
      doc1.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud;
      doc1.idUsuarioRegistro =
        this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud;

      this.array1.push(doc1);
    } else {
      this.array1 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud) {
      const doc2 = new ListArchivo();
      doc2.idArchivo =
        this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud;
      doc2.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoActRepLeg.tipoDocumento;
      this.array2.push(doc2);
    } else {
      this.array2 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud) {
      const doc3 = new ListArchivo();
      doc3.idArchivo =
        this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud;
      doc3.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoLimCol.tipoDocumento;
      this.array3.push(doc3);
    } else {
      this.array3 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud) {
      const doc4 = new ListArchivo();
      doc4.idArchivo =
        this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud;
      doc4.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoComPag.tipoDocumento;
      this.array4.push(doc4);
    } else {
      this.array4 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud) {
      const doc5 = new ListArchivo();
      doc5.idArchivo =
        this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud;
      doc5.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoAreCom.tipoDocumento;

      this.array5.push(doc5);
    } else {
      this.array5 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud) {
      const doc6 = new ListArchivo();
      doc6.idArchivo =
        this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud;
      doc6.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoActAsa.tipoDocumento;

      this.array6.push(doc6);
    }

    if (!!this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud) {
      const doc7 = new ListArchivo();
      doc7.idArchivo =
        this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud;
      doc7.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoRegFore.tipoDocumento;

      this.array7.push(doc7);
    } else {
      this.array7 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud) {
      const doc8 = new ListArchivo();
      doc8.idArchivo =
        this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud;
      doc8.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoArrf.tipoDocumento;

      this.array8.push(doc8);
    } else {
      this.array8 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud) {
      const doc9 = new ListArchivo();
      doc9.idArchivo =
        this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud;
      doc9.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoDecJur.tipoDocumento;

      this.array9.push(doc9);
    } else {
      this.array9 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud) {
      const doc10 = new ListArchivo();
      doc10.idArchivo =
        this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud;
      doc10.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoConTer.tipoDocumento;

      this.array10.push(doc10);
    } else {
      this.array10 = [];
    }

    if (!!this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud) {
      const doc11 = new ListArchivo();
      doc11.idArchivo =
        this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud;
      doc11.idTipoDocumento =
        this.solicitudCargaArchivo.solArchivoAsamAcu.tipoDocumento;

      this.array11.push(doc11);
    } else {
      this.array11 = [];
    }

    this.listArchivo = this.array1.concat(
      this.array2,
      this.array3,
      this.array4,
      this.array5,
      this.array6,
      this.array7,
      this.array8,
      this.array9,
      this.array10,
      this.array11
    );

    
  }

  registroSolicitud() {
    // if (!this.validaSolicitud()) return;

    if (this.solicitud.codigoTipoAprovechamiento == 1) {
      this.solicitud.nroContratoTercero = '';
      this.solicitud.nroActaAprovechamiento = '';
    }

    const newObj = new PmfiDatosOtorgamiento();
    newObj.idOtorgamiento = this.idOtorgamiento;
    newObj.idPermisoForestal = this.idPermisoForestal;
    newObj.estadoSolicitud = this.solicitud.estadoSolicitud
      ? this.solicitud.estadoSolicitud
      : null;
    newObj.nroRuc =
      this.solicitud.validaSunat && this.solicitud.nroRucEmpresa
        ? this.solicitud.nroRucEmpresa
        : '';
    newObj.nroTituloPropiedad = this.solicitud.nroPropiedad
      ? this.solicitud.nroPropiedad
      : '';
    newObj.nroPartidaRegistral =
      this.solicitud.validaSunarp && this.solicitud.nroPartidaRegistral
        ? this.solicitud.nroPartidaRegistral
        : '';
    newObj.nroActaAsambleaRrll = this.solicitud.nroActaAsambleaRrll
      ? this.solicitud.nroActaAsambleaRrll
      : '';
    newObj.nroOperacionComprobantePago = this.solicitud.nroComprobantePago
      ? this.solicitud.nroComprobantePago
      : '';
    newObj.areaComunidad = this.solicitud.areaComunidad
      ? this.solicitud.areaComunidad
      : 0;
    newObj.nroActaAcuerdoAsamblea = this.solicitud.nroActaAsambleaAcuerdo
      ? this.solicitud.nroActaAsambleaAcuerdo
      : '';
    newObj.codTipoAprovechamiento = this.solicitud.codigoTipoAprovechamiento
      ? this.solicitud.codigoTipoAprovechamiento.toString()
      : '';
    newObj.escalaManejo = this.solicitud.escalaManejo
      ? this.solicitud.escalaManejo
      : '';
    newObj.idUsuarioRegistro = this.usuarioService.idUsuario
      ? this.usuarioService.idUsuario.toString()
      : '';
    newObj.regente.apellidos = this.solicitud.nombresRegente
      ? this.solicitud.apellidosRegente
      : '';
    newObj.regente.nombres = this.solicitud.nombresRegente
      ? this.solicitud.nombresRegente
      : '';
    newObj.regente.idPlanManejo = this.solicitud.nombresRegente
      ? this.idPermisoForestal
      : 0;
    newObj.regente.idUsuarioRegistro = this.solicitud.nombresRegente
      ? this.usuarioService.idUsuario
      : 0;

    newObj.regente.numeroDocumento = this.solicitud.nombresRegente
      ? this.solicitud.regenteForestal
      : '';
    newObj.regente.numeroLicencia = this.solicitud.nombresRegente
      ? this.solicitud.numeroLicenciaRegente
      : '';
    newObj.regente.periodo = this.solicitud.nombresRegente
      ? this.solicitud.periodoRegente
      : '';
    newObj.regente.contratoSuscrito = this.solicitud.nombresRegente
      ? this.solicitud.nroContratoRegente
      : '';
      newObj.regente.idPlanManejoRegente =  this.idPlanManejoRegente ? this.idPlanManejoRegente : 0;
      newObj.nroContratoTercero = this.solicitud.nroContratoTercero

    this.datosOtorgamientoService
      .registrarInformacionOtorgamiento(newObj)
      .subscribe((data: any) => {
        if (data.success) {
          this.SuccessMensaje(data.message);
          this.listarDatosOtorgamiento();
        } else {
          this.ErrorMensaje(data.message);
        }
      });
  }

  registrarArchivoSolicitud(idSolicitud: number) {
    //archivo propiedad
    if (
      this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoTitProp.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoTitProp
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoTitProp.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoTitProp.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoTitProp
        )
        .subscribe((result: any) => {});
    }

    //archivo RRLL
    if (
      this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoActRepLeg.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoActRepLeg
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoActRepLeg.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoActRepLeg.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoActRepLeg
        )
        .subscribe((result: any) => {});
    }

    //archivo Limites

    if (
      this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoLimCol.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoLimCol
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoLimCol.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoLimCol.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoLimCol
        )
        .subscribe((result: any) => {});
    }

    //archivo comprobante pago

    if (
      this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoComPag.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoComPag
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoComPag.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoComPag.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoComPag
        )
        .subscribe((result: any) => {});
    }

    //archivo area comunidad pago

    if (
      this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoAreCom.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoAreCom
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoAreCom.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoAreCom.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoAreCom
        )
        .subscribe((result: any) => {});
    }

    //archivo acta asamblea acuerdo
    if (
      this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoActAsa.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoActAsa
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoActAsa.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoActAsa.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoActAsa
        )
        .subscribe((result: any) => {});
    }

    //contrato regente forestal

    if (
      this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoRegFore.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoRegFore
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoRegFore.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoRegFore.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoRegFore
        )
        .subscribe((result: any) => {});
    }

    // archivo ARRF
    if (
      this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoArrf.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoArrf
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoArrf.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoArrf.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoArrf
        )
        .subscribe((result: any) => {});
    }

    // archivo ARRF
    if (
      this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoArrf.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoArrf
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoArrf.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoArrf.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoArrf
        )
        .subscribe((result: any) => {});
    }

    //archivo declaracion jurada
    if (
      this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoDecJur.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoDecJur
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoDecJur.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoDecJur.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoDecJur
        )
        .subscribe((result: any) => {});
    }

    //archivo contrato terceros
    if (
      this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoConTer.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoConTer
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoConTer.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoConTer.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoConTer
        )
        .subscribe((result: any) => {});
    }

    //archivo acta asamblea
    if (
      this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud &&
      this.solicitudCargaArchivo.solArchivoAsamAcu.flagActualiza
    ) {
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoAsamAcu
        )
        .subscribe((result: any) => {});
    }

    if (
      this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud == 0 &&
      !this.solicitudCargaArchivo.solArchivoAsamAcu.flagActualiza
    ) {
      this.solicitudCargaArchivo.solArchivoAsamAcu.idSolicitud = idSolicitud;
      this.servSol
        .registrarPermisoArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoAsamAcu
        )
        .subscribe((result: any) => {});
    }
  }

  editarSolicitud() {
    if (this.solicitud.tipoAprovechamiento == 'Por Comunidad') {
      this.solicitud.nroContratoTercero = '';
      this.solicitud.nroActaAprovechamiento = '';
    }

    let params = {
      ...this.solicitud,
      descDocumentoResidencia: this.solicitud.descDocumentoResidencia
        ? this.solicitud.descDocumentoResidencia
        : '',
      descSolicitudAmpliacion: this.solicitud.descSolicitudAmpliacion
        ? this.solicitud.descSolicitudAmpliacion
        : '',
      descSolicitudReconocimiento: this.solicitud.descSolicitudReconocimiento
        ? this.solicitud.descSolicitudReconocimiento
        : '',
      descSolicitudTitulacion: this.solicitud.descSolicitudTitulacion
        ? this.solicitud.descSolicitudTitulacion
        : '',
    };

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.registrarArchivoSolicitud(this.solicitud.idSolicitud);

    this.servSol
      .editarSolicitudPorAccesoSolicitud(params)
      .subscribe((result: any) => {
        this.SuccessMensaje(result.message);
        this.dialog.closeAll();
      });
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  validatingPlanManejo(event: boolean) {
    this.validDatosPlanManejo = event;
  }
  validatingAnexo(event: boolean) {
    this.validDatosAnexo = event;
  }
  validatingArea(event: boolean) {
    this.validDatosArea = event;
  }
  validating(event: boolean) {
    this.validDatosPersona = event;
  }

  guardarSolicitud() {
    this.messageService.add({
      severity: 'success',
      detail: 'Se guardaron los datos correctamente',
    });
  }

  obtenerSolicitudAccesoPorUsuario() {
    let usuario = JSON.parse(localStorage.getItem('usuario') as any);
    this.servPf
      .obtenerSolicitudAccesoPersonaPorUsuario(usuario.idusuario)
      .subscribe((result: any) => {
        this.idPersona = result.data.idPersona;
      });
  }

  siguienteTab() {
    this.siguiente.emit();
  }

  regresarTab() {
    this.regresar.emit();
  }

  seleccionarValidacion(event: string) {
    if (event == CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO) {
      this.labelTituloValidacion = 'Observado';
      this.classValidacion = 'req_observado_class';
      this.iconEvaluacion = 'pi-info-circle';
    } else if (event == CodigoPermisoForestal.ESTADO_VAL_VALIDADO) {
      this.labelTituloValidacion = 'Validado';
      this.classValidacion = 'req_conforme_class';
      this.iconEvaluacion = 'pi-check-circle';
    }
  }

  /*obtenerEvaluacion() {
    let params = {
      idPlanManejo: this.idPermisoForestal,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
    };

    this.evaluacionService
      .obtenerEvaluacion(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionDatosOtorgamiento = Object.assign(
                this.evaluacionDatosOtorgamiento,
                this.evaluacion.listarEvaluacionDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost ==
                    this.codigoAcordeonDatosOtorgEval
                )
              );
            }
          }
        }
      });
  }*/

  //registrarEvaluacion() {
  /*if (EvaluacionUtils.validar([this.evaluacionDatosOtorgamiento])) {
      if (this.evaluacion) {
        this.evaluacion.listarEvaluacionDetalle = [];
        this.evaluacion.listarEvaluacionDetalle.push(
          this.evaluacionDatosOtorgamiento
        );

        this.dialog.open(LoadingComponent, { disableClose: true });

        this.evaluacionService
          .registrarEvaluacionPlanManejo(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res: any) => {
            this.toastService.ok(res.message);
            this.obtenerEvaluacion();
          });
      }
    } else {
      this.toastService.warn(Mensajes.MSJ_EVALUACIONES);
    }*/
  // }

  registrarEvaluacion() {
    if( !!this.evaluacionDet4.observacion){
    if (this.evaluacion) {
      this.evaluacion.idPermisoForestal = this.idPermisoForestal;
      this.evaluacion.listarEvaluacionPermisoDetalle = [];

      this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet1);
      this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet2);
      this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet3);
      this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet4);

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.evaluacionService
        .registrarEvaluacionPermisoForestal(this.evaluacion)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((res: any) => {
          this.toast.ok(res.message);
          this.obtenerEvaluacion();
        });
    }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }
  }

  enviarValidacionPlan(){
    this.toast.ok("Se envió la evaluación del plan manejo correctamente.");
    this.isShowModal2_2 = false;
  }

  verModal(){

    this.isShowModal2_2 = true;
  }

  obtenerEvaluacion() {
    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoTab,
      tipoEvaluacion: CodigoPermisoForestal.TIPO_EVALUACION,
    };

    this.evaluacionService
      .obtenerEvaluacionPermisoForestal(params)
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
            if (this.evaluacion) {
              this.evaluacionDet1 = Object.assign(
                this.evaluacionDet1,
                this.evaluacion.listarEvaluacionPermisoDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeonItem1
                )
              );
              this.evaluacionDet2 = Object.assign(
                this.evaluacionDet2,
                this.evaluacion.listarEvaluacionPermisoDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeonItem2
                )
              );
              this.evaluacionDet3 = Object.assign(
                this.evaluacionDet3,
                this.evaluacion.listarEvaluacionPermisoDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeonItem3
                )
              );
              this.evaluacionDet4 = Object.assign(
                this.evaluacionDet4,
                this.evaluacion.listarEvaluacionPermisoDetalle.find(
                  (x: any) =>
                    x.codigoEvaluacionDetPost == this.codigoAcordeonItem4
                )
              );
            }
          }
        }
      });
  }
}
