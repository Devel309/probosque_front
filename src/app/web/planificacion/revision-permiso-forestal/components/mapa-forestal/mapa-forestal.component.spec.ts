import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaForestalComponent } from './mapa-forestal.component';

describe('MapaForestalComponent', () => {
  let component: MapaForestalComponent;
  let fixture: ComponentFixture<MapaForestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapaForestalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaForestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
