import {
  Component,
  Input,
  EventEmitter,
  OnInit,
  Output,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ParametroValorService } from '../../../../service/parametro.service';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { SolicitudService } from '../../../../service/solicitud.service';
import { DowloadFileLocal, DownloadFile, MapApi } from '@shared';
import { RegenteExternoModel, RegenteForestalModel } from '@models';
import { BehaviorSubject, forkJoin, from, Observable } from 'rxjs';
import { concatMap, finalize, map, tap } from 'rxjs/operators';
import { RegenteService } from '../../../../service/regente.service';
import { ApiForestalService } from '../../../../service/api-forestal.service';
import Map from '@arcgis/core/Map';
import MapView from '@arcgis/core/views/MapView';
import { SolicitudModel } from '../../../../model/Solicitud';
import {
  SolicitudPermisoArchivoAdjuntoModel,
  SolicitudPermisoArchivoModel,
  SolicitudPermisoCargaArchivoModel,
} from '../../../../model/SolicitudArchivo';
import { Router } from '@angular/router';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AdjuntoModalComponent } from './modal/adjunto.modal.component';
import { ModalEvaluacionAnexosComponent } from './modal/modal-evaluacion-anexo/modal-evaluacion-anexo.component';
import { ModalEvaluarAreaComponent } from './modal/modal-evaluar-area/modal-evaluar-area.component';
import { ModalEvaluarPlanManejoComponent } from './modal/modal-evaluar-plan-manejo/modal-evaluar-plan-manejo.component';
import { FileModel } from 'src/app/model/util/File';
import {
  ArchivoService,
  SolicitudAccesoService,
  UsuarioService,
} from '@services';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { ListArchivo } from 'src/app/model/PfcrDatosOtorgamientoModel';
import { CodigosPermisosForestales } from 'src/app/model/util/CodigoPermisosForestales';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { Perfiles } from '../../../../model/util/Perfiles';
import { UsuarioModel } from '../../../../model/seguridad/usuario';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { PfcrInformacionGeneral } from 'src/app/model/pfcr-informacion-general';

@Component({
  selector: 'datos-otorgamiento',
  templateUrl: './datos-otorgamiento.component.html',
  styleUrls: ['./datos-otorgamiento.component.scss'],
  providers: [MessageService],
})
export class DatosOtorgamientoComponent implements OnInit {
  @Output() isValidatingAnexo: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @Output() isValidatingArea: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @Output() isValidatingPlanManejo: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @Output() registroSolicitud = new EventEmitter<void>();
  @Output() editarSolicitud = new EventEmitter<void>();

  @Input('dataBase') dataBase: any = {};
  //@Input() isPerfilArffs!: boolean;
  @Input('enabledControlOtorgamiento') enabledControlOtorgamiento: any = {};
  @Input('solicitud') solicitud: any = {} as SolicitudModel;

  @Input('solicitudCargaArchivo')
  solicitudCargaArchivo!: SolicitudPermisoCargaArchivoModel;
  @Input('accion') accion: string | null = '';
  @Input() comunidad: string = '';

  @Input() enabledHabilitarContinuar: boolean = false;
  @Input() idPermisoForestal: number = 0;
  @Input() nroDocumento!: string;

  isEdit: boolean = false;

  @Input() set edit(data: boolean) {
    if (data) {
      this.isEdit = data;
      if (this.solicitud.nroPartidaRegistral) {
        this.validaSunarpClass = 'btn btn-sm btn-secondary';
        this.flagvalidaSunaarp = false;
      }

      if (this.solicitud.nroRucEmpresa) {
        this.validaSunatClass = 'btn btn-sm btn-secondary';
        this.flagvalidaSunat = false;
      }
    } else {
      this.validaSunatClass = 'btn btn-sm btn-danger2';
      this.flagvalidaSunat = true;
      this.validaSunarpClass = 'btn btn-sm btn-danger2';
      this.flagvalidaSunaarp = true;
    }
  }

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @ViewChild('mapOtorgamiento', { static: true })
  enabledControlGeneral: boolean = false;
  enabledControlOperativo: boolean = false;
  enabledControlDema: boolean = false;
  enabledHabilitarTercero: boolean = false;
  private mapViewEl2!: ElementRef;
  public view: any = null;
  ref!: DynamicDialogRef;
  isValid: boolean = false;
  isPending: boolean = true;
  isObserved: boolean = false;

  isValidArea: boolean = false;
  isPendingArea: boolean = true;
  isObservedArea: boolean = false;

  isValidPlan: boolean = false;
  isPendingPlan: boolean = true;
  isObservedPlan: boolean = false;

  validaSunatClass: string = 'btn btn-sm btn-danger2';
  flagvalidaSunat: boolean = true;
  validaSunarpClass: string = 'btn btn-sm btn-danger2';
  flagvalidaSunaarp: boolean = true;
  flagAgregaArchivo: boolean = true;
  idSolicitud: number = 0;
  lstEstado: any[] = [];
  lstAprovechamiento = [
    { id: 1, nAprov: 'Por Comunidad' },
    { id: 2, nAprov: 'Por Terceros' },
  ];
  terceros: string = '';

  regenteForestal: RegenteForestalModel = new RegenteForestalModel();
  documentoRegente: RegenteExternoModel = new RegenteExternoModel();

  regentesFiltrados: RegenteExternoModel[] = [];
  regentesForestales: RegenteExternoModel[] = [];

  idUsuario: number = 1;

  mapaBase = [
    { label: '-- Seleccione --', value: null },
    { label: 'Archivos shapefiles', value: true },
    { label: 'PDF', value: false },
  ];

  solicitudAcceso = {} as any;

  //se obtienen datos de informacion general
  //para consulta a la sunarp
  listarInfGeneral() {
    var params = {
      idInfGeneral: null,
      codTipoInfGeneral: 'PFCR',
      idPermisoForestal: this.idPermisoForestal,
    };
    this.informacionGeneralService
      .listarInformacionGeneralPF(params)
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          const element = response.data[0];
          const infoGral = new PfcrInformacionGeneral(element);
          infoGral.ubigeoTitular = parseInt(element.ubigeoTitular);
          this.solicitudAcceso = infoGral;
        } else {
          console.log('No existen datos registrados de informacion general');
        }
      });
  }

  solArchivoAdjuntoc = {} as SolicitudPermisoArchivoAdjuntoModel;
  lstSolArchivoAdjunto: SolicitudPermisoArchivoAdjuntoModel[] = [];

  descripcion: any = {};
  CodigosPF = CodigosPermisosForestales;

  _filesSHP: FileModel[] = [];
  file: FileModel = {} as FileModel;
  _id = this.mapApi.Guid2.newGuid;
  usuario!: UsuarioModel;
  Perfiles = Perfiles;
  codigoProceso: string = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  constructor(
    private messageService: MessageService,
    private serv: ParametroValorService,
    private servSol: SolicitudService,
    private dialog: MatDialog,
    private serv_forestal: ApiForestalService,
    private confirmationService: ConfirmationService,
    private reg: RegenteService,
    private router: Router,
    public dialogService: DialogService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private usuarioService: UsuarioService,
    private serviceGeoforestal: ApiGeoforestalService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private serviceSolicitudArchivo: SolicitudAccesoService,
    private informacionGeneralService: PermisoForestalService,
    private serviceExternos: ApiForestalService
  ) {}

  ngOnInit(): void {
    this.usuario = this.usuarioService.usuario;
    this.solicitudCargaArchivo.solArchivoTitProp =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoActRepLeg =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoLimCol =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoComPag =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoAreCom =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoActAsa =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoRegFore =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoArrf =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoDecJur =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoConTer =
      {} as SolicitudPermisoArchivoModel;
    this.solicitudCargaArchivo.solArchivoAsamAcu =
      {} as SolicitudPermisoArchivoModel;
    this.solArchivoAdjuntoc = {} as SolicitudPermisoArchivoAdjuntoModel;

    this.listarEstado();
    if (this.solicitud.idSolicitud != null && this.accion != 'nuevo') {
      this.idSolicitud = this.solicitud.idSolicitud;
      this.obtenerNombresArchivos();
      this.obtenerArchivosAdjuntos();
      this.obtenerArchivoSolicitudSHP();
    }
    this.asignarRegente();
    // this.initializeMapBase();
    //this.initializeMapOtorgamiento();
  }

  initializeMapBase(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '370px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }

  initializeMapOtorgamiento(): void {
    const container = this.mapViewEl2.nativeElement;
    container.style.height = '370px';
    container.style.width = '500px';
    const map = new Map({
      basemap: 'hybrid',
    });
    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }

  validaRuc() {
    if (
      this.solicitud.nroRucEmpresa == null ||
      this.solicitud.nroRucEmpresa == '' ||
      this.solicitud.nroRucEmpresa == undefined
    ) {
      this.toast('warn', '(*) Se requiere número de ruc.');
      return;
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serviceExternos
        .consultarDatoRuc(this.solicitud.nroRucEmpresa)
        .subscribe(
          (data: any) => {
            this.dialog.closeAll();
            if (data.dataService) {
              if (data.dataService.respuesta.esHabido) {
                this.messageService.add({
                  severity: 'success',
                  summary: '',
                  detail: 'Se validó el RUC con la SUNAT correctamente.',
                });
                this.validaSunatClass = 'btn btn-sm btn-secondary';
                this.flagvalidaSunat = false;
                this.solicitud.validaSunat = true;
              } else {
                this.messageService.add({
                  severity: 'warn',
                  summary: '',
                  detail: 'RUC ingresado no existe en SUNAT.',
                });
                this.validaSunatClass = 'btn btn-sm btn-danger2';
                this.flagvalidaSunat = true;
                this.solicitud.nroRucEmpresa = '';
                this.solicitud.validaSunat = false;
              }
            } else {
              this.messageService.add({
                severity: 'warn',
                summary: '',
                detail:
                  'Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.',
              });
              this.validaSunatClass = 'btn btn-sm btn-danger2';
              this.flagvalidaSunat = true;
              this.solicitud.validaSunat = false;
            }
          },
          (error) => {
            this.dialog.closeAll();
            this.messageService.add({
              severity: 'warn',
              summary: '',
              detail:
                'Hay errores con el servicio de validación de RUC. Contactar con el administrador del sistema.',
            });
            this.validaSunatClass = 'btn btn-sm btn-danger2';
            this.flagvalidaSunat = true;
            this.solicitud.validaSunat = false;
          }
        );
    }
  }

  validaSunarp() {
    if (
      this.solicitud.nroPartidaRegistral == null ||
      this.solicitud.nroPartidaRegistral == '' ||
      this.solicitud.nroPartidaRegistral == undefined
    ) {
      this.toast('warn', '(*) Se requiere número partida registral.');
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.obtenerSolicitudAcceso()
        .pipe(concatMap((item) => this.consultarTitularidadSUNARP(item)))
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe();
    }
  }
  obtenerSolicitudAcceso() {
    const params = {
      codigoEstadoSolicitud: 'ESACREGI',
      numeroDocumento: this.nroDocumento,
    };

    return this.serviceSolicitudArchivo.obtenerSolicitudAcceso(params);
  }
  consultarTitularidadSUNARP(result: any) {
    let item = result.data;

    console.log(item);
    
    let tipoParticipante: any = null;
    if(!!item){
      if (item.tipoPersona === 'Natural') {
        tipoParticipante = 'N';
      } else if (item.tipoPersona === 'Jurídico') {
        tipoParticipante = 'J';
      }


    }
    let param = {
      apellidoMaterno: !!item? item.apellidoMaterno : '',
      apellidoPaterno: !!item? item.apellidoPaterno : '',
      nombres: !!item?  item.nombres : '',
      razonSocial: !!item? item.razonSocialEmpresa : '',
      tipoParticipante: tipoParticipante,
    };

    return this.serviceExternos.consultarTitularidadSUNARP(param).pipe(
      tap((data: any) => {       
        let item=null;
        if(data.dataService!=null){
          console.log('entro a la validacion');
          
          item = data?.dataService?.respuesta.respuestaTitularidad;
        }
        if (item === undefined || item === null) {
          this.ErrorMensaje('El N° de partida ingresado no es válido.');
          return;
        }
        if (Array.isArray(item)) {
          let validPartida = item.find(
            (t: any) => t.numeroPartida === this.solicitud.nroPartidaRegistral
          );
          if (validPartida) {
            this.SuccessMensaje(
              'Se validó la existencia de la Partida Registral en SUNARP'
            );
            this.validaSunarpClass = 'btn btn-sm btn-secondary';
            this.solicitud.validaSunarp = true;
          } else {
            this.ErrorMensaje('El N° de partida ingresado no es válido.');
            this.validaSunarpClass = 'btn btn-sm btn-danger2';
            this.solicitud.validaSunarp = false;
          }
        } else {
          if (
            parseInt(item.numeroPartida) ===
            parseInt(this.solicitud.nroPartidaRegistral)
          ) {
            this.SuccessMensaje(
              'Se validó la existencia de la Partida Registral en SUNARP.'
            );
            this.validaSunarpClass = 'btn btn-sm btn-secondary';
            this.solicitud.validaSunarp = true;
          } else {
            this.ErrorMensaje('El N° de partida ingresado no es válido.');
            this.validaSunarpClass = 'btn btn-sm btn-danger2';
            this.solicitud.validaSunarp = false;
          }
        }
      })
    );
  }

  continuar() {
    /*if (!this.validarRegistroSolicitud()) {
      return;
    }*/
    this.registroSolicitud.emit();
    this.enabledHabilitarContinuar = true;
  }

  editar() {
    this.editarSolicitud.emit();
  }

  listarEstado() {
    var params = { prefijo: 'EPSF' };
    //acá debe obtener de tabla estados
    this.serv.listarPorCodigoParametroValor(params).subscribe((result: any) => {
      result.data.splice(0, 0, {
        codigo: null,
        prefijo: null,
        valor1: '-- Seleccione --',
      });

      this.lstEstado = result.data;
    });
  }

  asignarRegente() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    forkJoin({
      regentesResponse: this.serviceExternos.consultarRegente(),
      // regenteResponse: this.reg.obtenerRegentes()
    })
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe({
        next: (res) => {
          this.regentesForestales = (res.regentesResponse as any)?.dataService;
          // this.regenteForestal = res?.regenteResponse?.data;
          // const regenteForestal = this.regentesForestales.find(x => this.regenteForestal.numeroDocumento == x.numeroDocumento);
          // this.documentoRegente = regenteForestal ? regenteForestal : new RegenteExternoModel({ numeroDocumento: this.regenteForestal.numeroDocumento });
        },
        error: (_err) => {
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: 'Ocurrió un error.',
          });
        },
      });
  }

  clearRegente() {
    this.regenteForestal = new RegenteForestalModel();
    this.documentoRegente = new RegenteExternoModel();

    this.solicitud.regenteForestal = '';
    this.solicitud.nombreRegenteForestal = '';
  }

  selectDocumentoRegente(value: RegenteExternoModel) {
    this.regenteForestal = {
      ...this.regenteForestal,
      numeroLicencia: value.numeroLicencia,
      numeroDocumento: value.numeroLicencia,
      idUsuarioRegistro: this.idUsuario,
    };

    this.solicitud.regenteForestal = value.numeroDocumento;
    this.solicitud.nombreRegenteForestal = value.nombresNroDocumento;
    this.solicitud.nombresRegente = value.nombres;
    this.solicitud.apellidosRegente = value.apellidos;
    this.solicitud.numeroLicenciaRegente = value.numeroLicencia;
    this.solicitud.numeroLicenciaRegente = value.numeroLicencia;
    this.solicitud.periodoRegente = value.periodo.toString();
  }

  filtrarRegente(value: string) {
    value = value.toLowerCase();
    this.regentesFiltrados = [];
    this.regentesFiltrados = this.regentesForestales
      .filter(
        (r) =>
          r.nombres.toLowerCase().includes(value) ||
          r.apellidos.toLowerCase().includes(value) ||
          r.numeroDocumento.includes(value)
      )
      .map((x) => {
        return new RegenteExternoModel({
          ...x,
          nombresNroDocumento: `${x.nombres} ${x.apellidos} - ${x.numeroDocumento}`,
        });
      });
  }

  /* Carga de SHP Area */
  onChangeFileSHP(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      inServer: false,
      service: false,
      annex: false,
      codigo: e.target.id,
      validate: true,
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFileSHP(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  processFileSHP(file: any, config: any) {
    config.file = file;
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.createLayers(data, config);
      if (config.validate === true)
        this.validateOverlap(data[0].features[0].geometry.coordinates);
    });
  }
  validateOverlap(geometry: any) {
    let item = {
      codProceso: '080204',
      geometria: { poligono: geometry },
    };
    this.serviceExternos
      .validarSuperposicionOtorgamiento(item)
      .subscribe((result: any) => {
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.included = false;
                this.file = {} as FileModel;
                this.file.codigo = 0;
                this.file.file = null;
                this.file.idLayer = t.geoJson.idLayer;
                this.file.inServer = true;
                this.file.nombreFile = t.geoJson.title;
                this.file.groupId = t.geoJson.groupId;
                this.file.color = t.geoJson.color;
                this.file.overlap = t.geoJson.overlap;
                this._filesSHP.push(this.file);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                this.file = {} as FileModel;
                this.file.codigo = 0;
                this.file.file = null;
                this.file.idLayer = t.geoJson.idLayer;
                this.file.inServer = true;
                this.file.nombreFile = t.geoJson.title;
                this.file.groupId = t.geoJson.groupId;
                this.file.color = t.geoJson.color;
                this.file.overlap = t.geoJson.overlap;
                this._filesSHP.push(this.file);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
      });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.file = config.file;
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.codZona = config.codZona;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      this.file = {} as FileModel;
      this.file.codigo = config.idArchivo;
      this.file.file = config.file;
      this.file.idLayer = t.idLayer;
      this.file.inServer = config.inServer;
      this.file.nombreFile = t.title || config.fileName;
      this.file.groupId = t.groupId;
      this.file.color = t.color;
      this.file.annex = config.annex;
      this._filesSHP.push(this.file);
      this.createLayer(t);
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = this.setProperties(item);
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = item.groupId;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {
        console.log(error);
      });
    this.view.map.add(geoJsonLayer);
  }
  setProperties(item: any) {
    let popupTemplate: any = {
      title: item.title,
      content: [],
    };
    if (item.isCenso === true) {
      popupTemplate.content = [
        {
          type: 'fields',
          fieldInfos: [
            {
              fieldName: 'nombreComun',
              label: 'Nombre Comun',
            },
            {
              fieldName: 'nombreCientifico',
              label: 'Nombre Cientifico',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'productoTipo',
              label: 'Tipo Producto',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
            {
              fieldName: 'descripcionOtros',
              label: 'Descripcion',
              format: {
                digitSeparator: true,
                places: 0,
              },
            },
          ],
        },
      ];
    }

    return popupTemplate;
  }
  obtenerArchivoSolicitudSHP() {
    this.cleanLayers();
    this._filesSHP = [];
    let item: any = {
      idSolicitud: this.solicitud.idSolicitud,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceSolicitudArchivo
      .obtenerArchivoDetalle(item.idSolicitud)
      .subscribe(
        (result: any) => {
          if (result.isSuccess) {
            this.dialog.closeAll();
            result.data.forEach((t: any) => {
              let blob = this.mapApi.readFileByte(t.file);
              let config = {
                inServer: true,
                service: false,
                fileName: t.nombreArchivo,
                idArchivo: t.idArchivo,
                validate: false,
              };
              this.processFileSHP(blob, config);
            });
          } else {
            this.dialog.closeAll();
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema',
            });
          }
        },
        (error) => {
          this.dialog.closeAll();
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema',
          });
        }
      );
  }
  guardarArchivoCapas() {
    let validate = this._filesSHP.filter(
      (item: any) => item['overlap'] === true
    );
    if (validate.length > 0) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Hay una superposición, modifica el área.',
      });
      return;
    }
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true && t.file !== null) {
        let item: any = {
          idUsuario: this.usuarioService.idUsuario,
          codigo: '37',
          codigoTipoPGMF: 'DATOS',
          subCodigoTipoPGMF: 'SHP',
          file: t.file,
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    observer
      .pipe(
        concatMap((item: any) => {
          return this.serviceArchivo.cargar(
            item.idUsuario,
            item.codigo,
            item.file
          );
        })
      )
      .pipe(
        concatMap((result: any) => {
          let item2 = {
            idArchivo: result.data,
            idSolicitud: this.idSolicitud,
            idUsuarioRegistro: this.usuarioService.idUsuario,
          };
          return this.serviceSolicitudArchivo.registrarArchivoDetalle(item2);
        })
      )
      .subscribe(
        (data) => {
          this.obtenerArchivoSolicitudSHP();
        },
        (error) => {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: error,
          });
        }
      );
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  onDownloadFileSHP(item: FileModel) {
    if (item.file !== null) {
      let url = window.URL.createObjectURL(item.file);
      let name = item.nombreFile;
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.href = url;
      a.download = `${name}.zip`;
      a.click();
    }
  }
  onRemoveFileSHP(file: FileModel) {
    if (file.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          let index = this._filesSHP.findIndex(
            (t: any) => t.idLayer === file.idLayer
          );
          this._filesSHP.splice(index, 1);
          this.mapApi.removeLayer(file.idLayer, this.view);
          this.eliminarArchivoDetalle(file.codigo);
        },
      });
    } else {
      let index = this._filesSHP.findIndex(
        (t: any) => t.idLayer === file.idLayer
      );
      this._filesSHP.splice(index, 1);
      this.mapApi.removeLayer(file.idLayer, this.view);
    }
  }
  cleanLayers() {
    if (this.view === null) return;
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._filesSHP = [];
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    this.serviceSolicitudArchivo
      .eliminarArchivoDetalle(idArchivo, this.usuarioService.idUsuario)
      .subscribe(
        (response: any) => {
          if (response.success) {
            this.messageService.add({
              key: 'tl',
              severity: 'success',
              detail: 'Se eliminó correctamente',
            });
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        },
        (error) => {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: error,
          });
        }
      );
  }

  /*Find carga de SHP Area */
  onFileSelectedTP(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoTitProp.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoTitProp.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoTitProp.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoTitProp.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoTitProp.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoTitProp.tipoDocumento = 'TITPRO';
      this.solicitudCargaArchivo.solArchivoTitProp.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedActRepLeg(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoActRepLeg.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoActRepLeg.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoActRepLeg.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoActRepLeg.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoActRepLeg.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoActRepLeg.tipoDocumento = 'ACTASA';
      this.solicitudCargaArchivo.solArchivoActRepLeg.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedLC(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoLimCol.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoLimCol.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoLimCol.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoLimCol.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoLimCol.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoLimCol.tipoDocumento = 'LIMCON';
      this.solicitudCargaArchivo.solArchivoLimCol.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedCP(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoComPag.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoComPag.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoComPag.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoComPag.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoComPag.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoComPag.tipoDocumento = 'COMPAG';
      this.solicitudCargaArchivo.solArchivoComPag.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedAreCom(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoAreCom.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoAreCom.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoAreCom.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoAreCom.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoAreCom.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoAreCom.tipoDocumento = 'ARECOM';
      this.solicitudCargaArchivo.solArchivoAreCom.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedAsamAcu(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoActAsa.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoActAsa.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoActAsa.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoActAsa.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoActAsa.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoActAsa.tipoDocumento = 'ASAACU';
      this.solicitudCargaArchivo.solArchivoActAsa.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedRegFore(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoRegFore.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoRegFore.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoRegFore.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoRegFore.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoRegFore.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoRegFore.tipoDocumento = 'CONREGFOR';
      this.solicitudCargaArchivo.solArchivoRegFore.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedArrf(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoArrf.rutaArchivo = URL.createObjectURL(
      event.target.files[0]
    );
    this.solicitudCargaArchivo.solArchivoArrf.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoArrf.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoArrf.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoArrf.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoArrf.tipoDocumento = 'ARRF';
      this.solicitudCargaArchivo.solArchivoArrf.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedDC(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoDecJur.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoDecJur.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoDecJur.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoDecJur.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoDecJur.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoDecJur.tipoDocumento = 'DECJUR';
      this.solicitudCargaArchivo.solArchivoDecJur.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedCT(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoConTer.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoConTer.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoConTer.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoConTer.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoConTer.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoConTer.tipoDocumento = 'CONTTERC';
      this.solicitudCargaArchivo.solArchivoConTer.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileSelectedAA(event: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.solicitudCargaArchivo.solArchivoAsamAcu.rutaArchivo =
      URL.createObjectURL(event.target.files[0]);
    this.solicitudCargaArchivo.solArchivoAsamAcu.file = event.target.files[0];
    this.solicitudCargaArchivo.solArchivoAsamAcu.nombreArchivo =
      event.target.files[0].name;

    if (this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud) {
      this.solicitudCargaArchivo.solArchivoAsamAcu.flagActualiza = true;
    } else {
      this.solicitudCargaArchivo.solArchivoAsamAcu.idSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud = 0;
      this.solicitudCargaArchivo.solArchivoAsamAcu.tipoDocumento = 'ACASAM';
      this.solicitudCargaArchivo.solArchivoAsamAcu.flagActualiza = false;
    }

    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }

  onFileDowlandTP() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud) {
      let tipoDocumento: string = 'TITPRO';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoTitProp.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoTitProp.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandActaRepLegal() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud) {
      let tipoDocumento: string = 'ACTASA';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoActRepLeg.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoActRepLeg.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandDC() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud) {
      let tipoDocumento: string = 'DECJUR';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoDecJur.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoDecJur.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandLC() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud) {
      let tipoDocumento: string = 'LIMCON';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoLimCol.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoLimCol.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandAreCom() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let tipoDocumento: string = 'ARECOM';
    if (this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud) {
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoAreCom.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoAreCom.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandAsamAcu() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud) {
      let tipoDocumento: string = 'ASAACU';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoActAsa.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoActAsa.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandRegFore() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud) {
      let tipoDocumento: string = 'CONREGFOR';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoRegFore.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoRegFore.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandCP() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud) {
      let tipoDocumento: string = 'COMPAG';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoComPag.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoComPag.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandAA() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud) {
      let tipoDocumento: string = 'ACASAM';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoAsamAcu.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoAsamAcu.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandCT() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud) {
      let tipoDocumento: string = 'CONTTERC';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoConTer.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoConTer.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  onFileDowlandArrf() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud) {
      let tipoDocumento: string = 'ARRF';
      this.servSol
        .descargarArchivoSolicitud(
          this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud,
          this.idSolicitud,
          tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(
        this.solicitudCargaArchivo.solArchivoArrf.rutaArchivo,
        this.solicitudCargaArchivo.solArchivoArrf.nombreArchivo
      );
      this.dialog.closeAll();
    }
  }

  obtenerNombresArchivos() {
    this.servSol
      .obtenerArchivoSolicitud(this.solicitud.idSolicitud)
      .subscribe((response: any) => {
        for (let item of response) {
          if (item.tipoDocumento == 'TITPRO') {
            this.solicitudCargaArchivo.solArchivoTitProp = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'ACTASA') {
            this.solicitudCargaArchivo.solArchivoActRepLeg = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'LIMCON') {
            this.solicitudCargaArchivo.solArchivoLimCol = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'COMPAG') {
            this.solicitudCargaArchivo.solArchivoComPag = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'ARECOM') {
            this.solicitudCargaArchivo.solArchivoAreCom = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'ASAACU') {
            this.solicitudCargaArchivo.solArchivoActAsa = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'CONREGFOR') {
            this.solicitudCargaArchivo.solArchivoRegFore = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'ARRF') {
            this.solicitudCargaArchivo.solArchivoArrf = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'DECJUR') {
            this.solicitudCargaArchivo.solArchivoDecJur = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'CONTTERC') {
            this.solicitudCargaArchivo.solArchivoConTer = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }

          if (item.tipoDocumento == 'ACASAM') {
            this.solicitudCargaArchivo.solArchivoAsamAcu = {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          }
        }
      });
  }

  obtenerArchivosAdjuntos() {
    this.servSol
      .obtenerArchivoSolicitudAdjunto(this.solicitud.idSolicitud)
      .subscribe((result: any) => {
        if (result)
          this.lstSolArchivoAdjunto = result.map((item: any) => {
            return {
              ...item,
              nombreArchivo: item.nombreArchivo.concat(
                '.',
                item.extensionArchivo
              ),
            };
          });
      });
  }

  descargarArchivoAdjunto(data: SolicitudPermisoArchivoAdjuntoModel) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (data.idArchivoSolicitud) {
      this.servSol
        .descargarArchivoSolicitudAdjunto(
          data.idArchivoSolicitud,
          this.idSolicitud,
          data.tipoDocumento
        )
        .subscribe((data: any) => {
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
          this.dialog.closeAll();
        });
    } else {
      DowloadFileLocal(data.rutaArchivo, data.nombreArchivo);
      this.dialog.closeAll();
    }
  }

  habilitarAlta() {
    this.enabledControlGeneral = true;
    this.enabledControlOperativo = false;
    this.enabledControlDema = false;
  }

  habilitaMedia() {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = true;
    this.enabledControlDema = false;
  }

  habilitaBaja() {
    this.enabledControlGeneral = false;
    this.enabledControlOperativo = false;
    this.enabledControlDema = true;
  }

  habilitarTercero(event: any) {
    this.enabledHabilitarTercero = event.value == 'Por Terceros';
  }

  redireccionarOperativo() {
    this.router.navigate(['/planificacion/plan-operativo-ccnn-ealta']);
  }

  redireccionarGeneral() {
    this.router.navigate(['/planificacion/plan-general-manejo']);
  }

  redireccionarDema() {
    this.router.navigate(['/planificacion/elaboracion-declaracion-mpafpp']);
    // CAMBIOS
  }

  declaracionJurada() {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: 'Se genera declaración jurada.',
    });
  }

  private validarRegistroSolicitud() {
    let boolValidar: boolean = true;

    if (
      this.solicitud.nroRucEmpresa == null ||
      this.solicitud.nroRucEmpresa == '' ||
      this.solicitud.nroRucEmpresa == undefined
    ) {
      boolValidar = false;
      this.toast('warn', 'Se requiere número de ruc.');
      return boolValidar;
    }

    if (this.flagvalidaSunat) {
      boolValidar = false;
      this.toast('warn', 'Se requiere validar ruc en sunat.');
      return boolValidar;
    }

    if (
      this.solicitud.nroPropiedad == null ||
      this.solicitud.nroPropiedad == '' ||
      this.solicitud.nroPropiedad == undefined
    ) {
      boolValidar = false;
      this.toast('warn', 'Se requiere número de propiedad.');
      return boolValidar;
    }

    // if (this.solArchivoTitProp.archivo == null || this.solArchivoTitProp.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo titulo de propiedad.');
    //   return boolValidar;
    // }

    if (
      this.solicitud.nroPartidaRegistral == null ||
      this.solicitud.nroPartidaRegistral == '' ||
      this.solicitud.nroPartidaRegistral == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número de partida registral.');
      return boolValidar;
    }

    if (
      this.solicitud.nroActaAsamblea == null ||
      this.solicitud.nroActaAsamblea == '' ||
      this.solicitud.nroActaAsamblea == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número de acta de asamblea.');
      return boolValidar;
    }

    // if (this.solArchivoActRepLeg.archivo == null || this.solArchivoActRepLeg.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo acta de asamblea RRLL.');
    //   return boolValidar;
    // }

    // if (this.solArchivoLimCol.archivo == null || this.solArchivoLimCol.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo limites y colindancia.');
    //   return boolValidar;
    // }

    if (
      this.solicitud.nroComprobantePago == null ||
      this.solicitud.nroComprobantePago == '' ||
      this.solicitud.nroComprobantePago == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número del comprobante de pago.');
      return boolValidar;
    }

    // if (this.solArchivoComPag.archivo == null || this.solArchivoComPag.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo comprobante de pago.');
    //   return boolValidar;
    // }

    // if (this.solArchivoAreCom.archivo == null || this.solArchivoAreCom.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo area comunidad.');
    //   return boolValidar;
    // }

    if (
      this.solicitud.nroActaAsambleaAcuerdo == null ||
      this.solicitud.nroActaAsambleaAcuerdo == '' ||
      this.solicitud.nroActaAsambleaAcuerdo == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere número de acta de acuerdo asamblea.');
      return boolValidar;
    }

    // if (this.solArchivoAsamAcu.archivo == null || this.solArchivoAsamAcu.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo asamblea acuerdo.');
    //   return boolValidar;
    // }

    if (
      this.solicitud.regenteForestal == null ||
      this.solicitud.regenteForestal == '' ||
      this.solicitud.regenteForestal == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere regente forestal.');
      return boolValidar;
    }

    if (
      this.solicitud.nroContratoRegente == null ||
      this.solicitud.nroContratoRegente == '' ||
      this.solicitud.nroContratoRegente == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere numero de contrato.');
      return boolValidar;
    }

    // if (this.solArchivoRegFore.archivo == null || this.solArchivoRegFore.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo contrato regente forestal.');
    //   return boolValidar;
    // }

    if (
      this.solicitud.tipoAprovechamiento == null ||
      this.solicitud.tipoAprovechamiento == '' ||
      this.solicitud.tipoAprovechamiento == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere tipo de aprovechamiento.');
      return boolValidar;
    }

    // if (this.solArchivoArrf.archivo == null || this.solArchivoArrf.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo Arrf.');
    //   return boolValidar;
    // }

    // if (this.solArchivoDecJur.archivo == null || this.solArchivoDecJur.archivo == undefined) {
    //   boolValidar = false;
    //   this.toast('warn', 'Se requiere archivo declaracion jurada.');
    //   return boolValidar;
    // }

    if (
      this.solicitud.tipoAprovechamiento != null ||
      this.solicitud.tipoAprovechamiento != '' ||
      this.solicitud.tipoAprovechamiento != undefined
    ) {
      if (this.solicitud.tipoAprovechamiento == 'Por Terceros') {
        if (
          this.solicitud.nroContratoTercero == null ||
          this.solicitud.nroContratoTercero == '' ||
          this.solicitud.nroContratoTercero == undefined
        ) {
          boolValidar = false;
          this.toast('warm', 'Se requiere numero de contrato tercero.');
          return boolValidar;
        }

        // if (this.solArchivoConTer.archivo == null || this.solArchivoConTer.archivo == undefined) {
        //   boolValidar = false;
        //   this.toast('warn', 'Se requiere archivo contrato tercero.');
        //   return boolValidar;
        // }

        if (
          this.solicitud.nroActaAprovechamiento == null ||
          this.solicitud.nroActaAprovechamiento == '' ||
          this.solicitud.nroActaAprovechamiento == undefined
        ) {
          boolValidar = false;
          this.toast('warm', 'Se requiere numero de acta asamblea.');
          return boolValidar;
        }

        // if (this.solArchivoActAsa.archivo == null || this.solArchivoActAsa.archivo == undefined) {
        //   boolValidar = false;
        //   this.toast('warn', 'Se requiere archivo acta asamblea.');
        //   return boolValidar;
        // }
      }
    }

    if (
      this.solicitud.escalaManejo == null ||
      this.solicitud.escalaManejo == '' ||
      this.solicitud.escalaManejo == undefined
    ) {
      boolValidar = false;
      this.toast('warm', 'Se requiere escala de manejo.');
      return boolValidar;
    }

    return boolValidar;
  }

  otroAdjunto() {}

  abrirModal() {
    const header = `Adjuntar Declaración Jurada`;
    const ref = this.dialogService.open(AdjuntoModalComponent, {
      header,
      width: '50vw',
      closable: true,
    });
    /*ref.onClose.subscribe(detalle => {
      if (detalle) {
        if (!CompareObjects(data, detalle)) {
          this.pendiente = true;
          detalle.enviar = true;
        }
      }
    })*/
  }

  toast(severty: string, msg: string) {
    this.messageService.add({ severity: severty, summary: '', detail: msg });
  }

  openModalAnexos() {
    this.ref = this.dialogService.open(ModalEvaluacionAnexosComponent, {
      header: 'Evaluación Anexo',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObserved = resp;
          this.isValid = false;
          this.isPending = false;
        } else {
          this.isValidatingAnexo.emit(true);
          this.isObserved = false;
          this.isValid = true;
          this.isPending = false;
        }
      }
    });
  }

  openModalArea() {
    this.ref = this.dialogService.open(ModalEvaluarAreaComponent, {
      header: 'Validación Superposición',
      width: '80%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObservedArea = resp;
          this.isValidArea = false;
          this.isPendingArea = false;
        } else {
          this.isValidatingArea.emit(true);
          this.isObservedArea = false;
          this.isValidArea = true;
          this.isPendingArea = false;
        }
      }
    });
  }

  openModalPlanManejo() {
    this.ref = this.dialogService.open(ModalEvaluarPlanManejoComponent, {
      header: '¿Anexo tiene observaciones?',
      width: '50%',
      contentStyle: { 'max-height': '500px', overflow: 'auto' },
    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        if (resp == true) {
          this.isObservedPlan = resp;
          this.isValidPlan = false;
          this.isPendingPlan = false;
        } else {
          this.isValidatingPlanManejo.emit(true);
          this.isObservedPlan = false;
          this.isValidPlan = true;
          this.isPendingPlan = false;
        }
      }
    });
  }

  grabarArchivo() {
    if (!this.validarArchivoAdjunto()) return;
    const newArchivo = new ListArchivo();
    newArchivo.idArchivo = this.solArchivoAdjuntoc.idArchivoSolicitud;
    newArchivo.idTipoDocumento = this.solicitud.tipoDocumento;
    newArchivo.descripcion = this.solArchivoAdjuntoc.descripcionArchivo;
    this.solicitud.listArchivo.push(newArchivo);
  }

  onFileSelectedAdjunto(event: any) {
    this.solArchivoAdjuntoc.nombreArchivo = event.target.files[0].name;
    this.solArchivoAdjuntoc.idSolicitud = this.solicitud.idSolicitud;
    this.solArchivoAdjuntoc.idArchivoSolicitud = '';
    this.solArchivoAdjuntoc.tipoDocumento = 'OTRARC';
    this.solArchivoAdjuntoc.rutaArchivo = URL.createObjectURL(
      event.target.files[0]
    );
    this.solArchivoAdjuntoc.file = event.target.files[0];
    this.solArchivoAdjuntoc.flagActualiza = false;

    this.flagAgregaArchivo = false;
  }

  openEliminarArchivo(
    event: Event,
    index: number,
    data: SolicitudPermisoArchivoAdjuntoModel
  ) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        let params = {
          idArchivo: data.idArchivo,
        };
        this.servSol
          .eliminarArchivoAdjuntoSolicitud(params)
          .subscribe((result: any) => {
            this.SuccessMensaje('Se eliminó el archivo');
            this.solicitud.listArchivo.splice(index, 1);
          });
      },
      reject: () => {
        //reject action
      },
    });
  }

  eliminarArchivoTituloPropiedad(event: Event) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoTitProp.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoTitProp.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoTitProp.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoTitProp =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoTitProp =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoRLL(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoActRepLeg.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoActRepLeg.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoActRepLeg.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoActRepLeg =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoActRepLeg =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoLimites(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoLimCol.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoLimCol.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoLimCol.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoLimCol =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoLimCol =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoComprobante(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoComPag.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoComPag.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoComPag.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoComPag =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoComPag =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoAreaComunidad(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoAreCom.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoAreCom.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoAreCom.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoAreCom =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoAreCom =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoActaAcuerdo(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoActAsa.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoActAsa.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoActAsa.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoActAsa =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoActAsa =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoRegenteForestal(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoRegFore.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoRegFore.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoRegFore.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoRegFore =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoRegFore =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoARFF(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoArrf.idArchivoSolicitud,
            idSolicitud: this.solicitudCargaArchivo.solArchivoArrf.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoArrf.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoArrf =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoArrf =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoDeclaracionJurada(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoDecJur.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoDecJur.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoDecJur.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoDecJur =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoDecJur =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoContratoTerceros(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoConTer.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoConTer.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoConTer.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoConTer =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoConTer =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  eliminarArchivoActaAsamblea(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Esta seguro de querer eliminar?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.dialog.open(LoadingComponent, { disableClose: true });

        if (this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud) {
          let params = {
            idArchivoSolicitud:
              this.solicitudCargaArchivo.solArchivoAsamAcu.idArchivoSolicitud,
            idSolicitud:
              this.solicitudCargaArchivo.solArchivoAsamAcu.idSolicitud,
            tipoArchivo:
              this.solicitudCargaArchivo.solArchivoAsamAcu.tipoDocumento,
          };

          this.servSol
            .eliminarArchivoPermisoSolicitud(params)
            .subscribe((result: any) => {
              this.solicitudCargaArchivo.solArchivoAsamAcu =
                {} as SolicitudPermisoArchivoModel;
              this.SuccessMensaje('Se eliminó el archivo');
              this.dialog.closeAll();
            });
        } else {
          this.solicitudCargaArchivo.solArchivoAsamAcu =
            {} as SolicitudPermisoArchivoModel;
          this.dialog.closeAll();
        }
      },
    });
  }

  validarArchivoAdjunto(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.solArchivoAdjuntoc.idArchivoSolicitud) {
      validar = false;
      mensaje = mensaje += '(*) Debe el agregar un archivo. \n';
    }
    if (!this.solArchivoAdjuntoc.descripcionArchivo) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar la descripción del archivo. \n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  asignarDocumento(id: number, tipoDocumento: number) {
    this.solArchivoAdjuntoc.idArchivoSolicitud = id != 0 ? id : 0;
    this.solArchivoAdjuntoc.tipoDocumento = tipoDocumento;
  }
}
