import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";


@Component({
  selector: "app-modal-evaluar-plan-manejo",
  templateUrl: "./modal-evaluar-plan-manejo.component.html",
})
export class ModalEvaluarPlanManejoComponent implements OnInit {
  isObserved: boolean = false
  obj = {};
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
  }

  agregar = () => {
    this.ref.close(this.obj);
  };
  cancelar(){
    this.isObserved = true;
    this.ref.close(this.isObserved);
  }
}
