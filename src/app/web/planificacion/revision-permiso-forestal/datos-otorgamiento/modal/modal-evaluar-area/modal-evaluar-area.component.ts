import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import MapView from "@arcgis/core/views/MapView";
import Map from "@arcgis/core/Map";

@Component({
  selector: "app-modal-evaluar-area",
  templateUrl: "./modal-evaluar-area.component.html",
})
export class ModalEvaluarAreaComponent implements OnInit {
  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  public view: any = null;
  isObserved: boolean = false
  isConsulting: boolean = false;
  obj = {
    pide: "",
    informacion: "",
  };

  entidades = [
    {entidad: "entidad1"},
    {entidad: "entidad2"},
    {entidad: "entidad3"}
  ]

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.initializeMap();
  }
  consulta() {
    this.isConsulting = true;
  }
  cancelar(){
    this.isObserved = true;
    this.ref.close(this.isObserved);
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "390px";
    container.style.width = "100%";
    const map = new Map({
      basemap: "hybrid",
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    this.view = view;
  }

  agregar = () => {
    this.ref.close(this.obj);
  };
}
