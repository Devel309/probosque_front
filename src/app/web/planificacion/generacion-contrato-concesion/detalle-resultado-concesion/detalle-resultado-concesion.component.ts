import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConvertDateToString, ConvertNumberToDate } from '@shared';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ResultadoPPService } from 'src/app/service/resultadoPP/resultado-pp.service';
import { ModalGeneracionContratoConcesionComponent } from '../modal-generacion-contrato-concesion/modal-generacion-contrato-concesion.component';

@Component({
  selector: 'app-detalle-resultado-concesion',
  templateUrl: './detalle-resultado-concesion.component.html',
})
export class DetalleResultadoConcesionComponent implements OnInit {
  idProcesoOferta!: number;

  listItems!: MenuItem[];
  homes!: MenuItem;

  listaResultadoPP: any[] = [];

  procesosOfertas: any[] = [];

  listaResultado: any[] = [];

  ref!: DynamicDialogRef;

  constructor(
    private resultadoPPService: ResultadoPPService,
    private messageService: MessageService,
    public dialogService: DialogService,
    private router: Router
  ) {}

  ngOnInit() {
    this.listItems = [
      {
        label: 'Lista de Postulación PFDM con Generación de Contrato Concesion',
        routerLink: '/planificacion/generacion-contrato-concesion',
      },
      { label: 'Generar Contrato' },
    ];

    this.homes = { icon: 'pi pi-home', routerLink: '/inicio' };

    this.cargarPP();
  }

  modalGenerarContrato = (item: any = null, typeConsult: number = 1) => {
    this.ref = this.dialogService.open(ModalGeneracionContratoConcesionComponent, {
      header: 'Documentos para Generación de Contrato de conseción',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        isNew: typeConsult === 1 ? true : false,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if (resp) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se guardó correctamente '.concat(
            ' ',
            typeConsult === 1 ? resp : ''
          ),
        });
        this.router.navigate(['planificacion/generacion-contrato-concesion']);
      }
    });
  };

  cargarPP = () => {
    var form = {
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null, //JSON.parse('' + localStorage.getItem('usuario')).idusuario,
      ganador: true,
    };
    this.resultadoPPService.listarPPEvaluacion(form).subscribe((resp: any) => {
      resp.data.forEach((element: any) => {
        if (element.fechaPostulacion != null)
          element.fechaPostulacionString = ConvertDateToString(
            ConvertNumberToDate(element.fechaPostulacion)
          );
      });

      this.listaResultadoPP = resp.data;
    });
  };
}
