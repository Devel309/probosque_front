import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, SelectItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ContratoService } from 'src/app/service/contrato.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { ModalGeneracionContratoConcesionComponent } from './modal-generacion-contrato-concesion/modal-generacion-contrato-concesion.component';

@Component({
  selector: 'app-generacion-contrato-concesion',
  templateUrl: './generacion-contrato-concesion.component.html',
})
export class GeneracionContratoConcesionComponent implements OnInit {
  ref!: DynamicDialogRef;
  listContrato!: any[];
  procesoPostulacionSelect!:String;
  estadoContrato: SelectItem[];
  selectedestadocontrato!: SelectItem;

  constructor(
    public dialogService: DialogService,
    private contratoService: ContratoService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private router: Router,
    private messageService: MessageService
  ) {
    this.estadoContrato = [
      { label:'-- Seleccione --', value:null},
      { label: 'Firmado', value: true },
      { label: 'Pendiente de firma', value: false },
    ];
  }

  ngOnInit() {
    this.listarContrato();
    this.listarProcesoPostulacion();
  }

  procesoPostulacion: any[] = [];
  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: null,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          idProcesoPostulacion: "-- Seleccione --"
        });
        this.procesoPostulacion = resp.data;
      });
  };


  listarContrato = () => {
    var params = {
      codigoContrato: null,
      firmado: this.selectedestadocontrato,
      idContrato: null,
      idProcesoPostulacion: this.procesoPostulacionSelect == null || this.procesoPostulacionSelect == '-- Seleccione --' ? null : this.procesoPostulacionSelect,
    };
    this.contratoService
      .listarContrato(params)
      .subscribe((resp: any) => {
        this.listContrato = resp.data;
      });
  };

  verModalGeneracionContrato = (item: any = null, typeConsult: number = 1) => {
    this.router.navigate(['planificacion/generacion-contrato-resultado-concesion']);
  };

  verDetalle = (item: any) => {
    this.ref = this.dialogService.open(ModalGeneracionContratoConcesionComponent, {
      header: 'Documentos para Generación de Contrato de conseción',
      width: '50%',
      contentStyle: { overflow: 'auto' },
      data: {
        item: item,
        isNew: false,
      },
    });

    this.ref.onClose.subscribe((resp: any) => {
      
      if (resp) {
        this.messageService.add({
          key: 'tl',
          severity: 'success',
          summary: 'CORRECTO',
          detail: 'Se guardó correctamente',
        });
        this.listarContrato();
      }
    });
  };
}
