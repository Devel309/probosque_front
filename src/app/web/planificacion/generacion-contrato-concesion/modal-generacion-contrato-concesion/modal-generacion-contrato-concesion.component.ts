import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  ConvertDateToString,
  ConvertNumberToDate,
  DownloadFile,
} from '@shared';
import { MessageService } from 'primeng/api';
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from 'primeng/dynamicdialog';
import { AmpliacionSolicitudService } from 'src/app/service/ampliacion-solicitud/ampliacion-solicitud.service';
import { ContratoService } from 'src/app/service/contrato.service';
import { PlanificacionService } from 'src/app/service/planificacion.service';
import { ProcesoPostulaionService } from 'src/app/service/proceso-postulacion/proceso-postulacion.service';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { ModalInformacionConcesionComponent } from '../modal-informacion-concesion/modal-informacion-concesion.component';

@Component({
  selector: 'app-modal-generacion-contrato-concesion',
  templateUrl: './modal-generacion-contrato-concesion.component.html',
})
export class ModalGeneracionContratoConcesionComponent implements OnInit {
  contratoRequestEntity: any = {};
  procesoPostulacion: any[] = [];
  idContrato!: number;
  fileLiteral!: any;
  fileCumplimiento!: any;
  fileBienes!: any;
  fileContrato!: any;

  idDocumentoAdjunto!: number;
  j: number = 0;

  documentosLiteral: any = {};
  documentosCumplimiento: any = {};
  documentosBienes: any = {};
  documentosContratoFirmado: any = {};
  existeBienes: boolean = false;
  existeContratadoFirmado: boolean = false;

  tipoValidacion: string = 'Validación faltante';
  esValido: boolean = false;
  esActualizadoContrato: boolean = false;

  codigoTh: string | null = null;
  listaValidada: any[] = [];

  vigenciaDesde: any = null;
  vigenciaHasta: any = null;

  seGenero: boolean = false;
  firmado: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public ref1: DynamicDialogRef,
    public dialogService: DialogService,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private procesoPostulaionService: ProcesoPostulaionService,
    private contratoService: ContratoService,
    private serv: PlanificacionService,
    private seguridadService: SeguridadService,
    private ampliacionSolicitudService: AmpliacionSolicitudService
  ) {}

  ngOnInit() {
    this.contratoRequestEntity.idProcesoPostulacion =
      this.config.data.item.idProcesoPostulacion;
    this.listarProcesoPostulacion();

    if (
      this.config.data.item.idContrato != null &&
      this.config.data.item.idContrato != undefined &&
      this.config.data.item.idContrato != 0
    ) {
      this.idContrato = this.config.data.item.idContrato;
      this.obtenerContrato();
      this.obtenerArchivosContrato();
    }
  }

  obtenerContrato = () => {
    this.contratoService
      .obtenerContrato(this.config.data.item.idContrato)
      .subscribe((result: any) => {
        
        if (result.data.datosValidados) {
          this.esValido = this.esActualizadoContrato =
            result.data.datosValidados;
          this.tipoValidacion = 'Validación correcta';
        }

        if (result.data.fechaInicioContrato != null) {
          this.vigenciaDesde = ConvertDateToString(
            ConvertNumberToDate(result.data.fechaInicioContrato)
          );
        }

        if (result.data.fechaFinContrato != null) {
          this.vigenciaHasta = ConvertDateToString(
            ConvertNumberToDate(result.data.fechaFinContrato)
          );
        }

        this.contratoRequestEntity = result.data;
        this.contratoRequestEntity.dniRucRepresentanteLegal =
          result.data.dniRepresentanteLegal;
        this.contratoRequestEntity.nombreRaSoRepresentanteLegal =
          result.data.nombreRepresentanteLegal;
        this.codigoTh = result.data.codigoTituloH;
        this.firmado = this.contratoRequestEntity.firmado;
      });
  };

  obtenerArchivosContrato = () => {
    this.contratoService
      .obtenerArchivosContrato(this.config.data.item.idContrato)
      .subscribe((result: any) => {
        
        result.data.forEach((element: any) => {
          if (element.idTipoDocumento === 22) this.documentosLiteral = element;
          else if (element.idTipoDocumento === 23)
            this.documentosCumplimiento = element;
          else if (element.idTipoDocumento === 24) {
            this.existeBienes = true;
            this.documentosBienes = element;
          } else if (element.idTipoDocumento === 28) {
            this.documentosContratoFirmado = element;
            this.existeContratadoFirmado = true;
          }
        });
      });
  };

  listarProcesoPostulacion = () => {
    var obj = {
      idStatus: null,
      idProcesoOferta: null,
      idProcesoPostulacion: null,
      idUsuarioPostulacion: null,
    };

    this.procesoPostulaionService
      .listarProcesoPostulacion(obj)
      .subscribe((resp: any) => {
        resp.data.splice(0, 0, {
          idProcesoPostulacion: '-- SELECCIONE --',
        });
        this.procesoPostulacion = [...resp.data];
      });
  };

  cargarAchivo(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileLiteral = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoCumplimiento(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileCumplimiento = e.srcElement.files[0];
      }
    }
  }

  cargarAchivoBienes(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileBienes = e.srcElement.files[0];
      }
    }
  }

  cargarContrato(e: any) {
    if (e.srcElement) {
      if (e.srcElement.files.length) {
        this.fileContrato = e.srcElement.files[0];
      }
    }
  }

  registrarContrato = () => {
    ;
    this.contratoRequestEntity.idUsuarioRegistro = JSON.parse(
      '' + localStorage.getItem('usuario')
    ).idusuario;
    this.contratoRequestEntity.idResultadoPP =
      this.config.data.item.idResultadoPP;
    this.contratoService
      .registrarContrato(this.contratoRequestEntity)
      .subscribe((data: any) => {
        if (data.success) {
          this.idContrato = data.codigo;
          this.j = 1;
          this.enviarArchivo('22', this.fileLiteral);
          this.enviarArchivo('23', this.fileCumplimiento);
        } else {
          this.messageService.add({
            key: 'tl',
            severity: 'error',
            summary: 'ERROR',
            detail: 'Ocurrió un problema, intente nuevamente',
          });
        }
      });
  };

  enviarArchivo = (tipo: string, archivo: any) => {
    let params = new HttpParams()
      .set('CodigoAnexo', 'Generar contrato')
      .set('IdTipoDocumento', tipo)
      .set(
        'IdProcesoPostulacion',
        this.contratoRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', archivo.name);

    const formData = new FormData();
    formData.append('file', archivo);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.adjuntarArchivosContrato();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  adjuntarArchivosContrato = () => {
    let params = {
      idContrato: this.idContrato,
      idDocumentoAdjunto: this.idDocumentoAdjunto,
      idUsuarioRegistro: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.contratoService
      .adjuntarArchivosContrato(params)
      .subscribe((data: any) => {
        if (data.success && this.j === 2) this.ref.close(this.idContrato);
        else if (data.success && this.j === 3) {
          this.messageService.add({
            key: 'tl',
            severity: 'success',
            summary: 'CORRECTO',
            detail: 'Se guardó correctamente',
          });
          this.ref.close(this.idContrato);
        } else if (data.success && this.j === 4) {
          this.actualizarStatusContrato();
        } else this.j = this.j + 1;
      });
  };

  actualizarStatusContrato = () => {
    let params = {
      idContrato: this.idContrato,
      idSatusContrato: 23,
      idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
        .idusuario,
    };
    this.contratoService
      .actualizarStatusContrato(params)
      .subscribe((result: any) => {

        if (result.success) this.actualizarEstadoProcesoPostulacion();
      });
  };

  actualizarEstadoProcesoPostulacion = () => {

    var params = {
      idusuario: this.contratoRequestEntity.idUsuarioPostulacion,
    };
    this.seguridadService.obtenerUsuario(params).subscribe((resp: any) => {
      var params = {
        correoPostulante: resp.data[0].correoElectronico,
        idProcesoPostulacion: this.contratoRequestEntity.idProcesoPostulacion,
        idStatus: 24,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
      };

      this.ampliacionSolicitudService
        .actualizarEstadoProcesoPostulacion(params)
        .subscribe((resp: any) => {

          if (resp.success) {
            this.messageService.add({
              key: 'tl',
              severity: 'success',
              summary: 'CORRECTO',
              detail: 'Se guardó correctamente',
            });
            this.ref.close(this.idContrato);
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });
    });
  };

  descargarLiteral = () => {
    DownloadFile(
      this.documentosLiteral.documento,
      this.documentosLiteral.nombredocumento,
      'application/octet-stream'
    );
  };

  descargarCumplimiento = () => {
    DownloadFile(
      this.documentosCumplimiento.documento,
      this.documentosCumplimiento.nombredocumento,
      'application/octet-stream'
    );
  };

  descargarBienes = () => {
    DownloadFile(
      this.documentosBienes.documento,
      this.documentosBienes.nombredocumento,
      'application/octet-stream'
    );
  };

  descargarContratoFirmado = () => {
    DownloadFile(
      this.documentosContratoFirmado.documento,
      this.documentosContratoFirmado.nombredocumento,
      'application/octet-stream'
    );
  };

  descargarContrato = () => {
    this.contratoService
      .descargarContrato(this.config.data.item.idContrato)
      .subscribe((data: any) => {
        if (data.success) {
          this.seGenero = true;
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
        }
      });
  };

  generarCodigo = () => {
    this.codigoTh = '25-UCA-ATA/CON-PFDM-2018-001';
  };

  validarInformacion = () => {
    this.ref1 = this.dialogService.open(ModalInformacionConcesionComponent, {
      header: 'Validar Información',
      width: '40%',
      contentStyle: { 'min-height': '300px', overflow: 'auto' },
      data: {},
    });

    this.ref1.onClose.subscribe((resp: any) => {
      if (resp) {
        this.tipoValidacion = 'Validación correcta';
        this.esValido = true;
      } else {
        this.tipoValidacion = 'Validación faltante';
        this.esValido = false;
      }
    });
  };

  guardarInformacionValida = () => {
    ;
    if (!this.esValido) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Validación faltante.',
      });
    }
    else if (this.fileBienes === undefined && !this.existeBienes && !this.firmado) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe adjuntar documento de informe favorables.',
      });
    } else if (this.vigenciaDesde === null) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe ingresar Fecha Vigencia Desde',
      });
    } else if (this.vigenciaHasta === null) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe ingresar Fecha Vigencia Hasta',
      });
    } else if (this.codigoTh === '' || this.codigoTh === null) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe Generar codigo de TH',
      });
    } else if (this.firmado && this.fileContrato === undefined) {
      this.messageService.add({
        key: 'tl',
        severity: 'error',
        summary: 'ERROR',
        detail: 'Debe adjuntar el contrato firmado',
      });
    } else {
      var params = {
        codigoTituloH: this.codigoTh,
        datosValidados: this.esValido,
        fechaFinContrato: this.vigenciaHasta,
        fechaInicioContrato: this.vigenciaDesde,
        firmado: this.firmado,
        idContrato: this.contratoRequestEntity.idContrato,
        idUsuarioModificacion: JSON.parse('' + localStorage.getItem('usuario'))
          .idusuario,
        nombreRaSoRepresentanteLegal:
          this.contratoRequestEntity.nombreRaSoRepresentanteLegal,
        dniRucRepresentanteLegal:
          this.contratoRequestEntity.dniRucRepresentanteLegal,
      };
      this.contratoService
        .actualizarContrato(params)
        .subscribe((result: any) => {
          if (result.success) {
            ;
            this.esActualizadoContrato = true;
            if (!this.existeBienes && !this.firmado) this.enviarBienes();
            else if (this.firmado && this.fileContrato != null) {
              this.enviarContratoFirmado();
            } else {
              this.ref.close(this.idContrato);
              this.messageService.add({
                key: 'tl',
                severity: 'success',
                summary: 'CORRECTO',
                detail: 'Se guardó correctamente',
              });
            }
          } else {
            this.messageService.add({
              key: 'tl',
              severity: 'error',
              summary: 'ERROR',
              detail: 'Ocurrió un problema, intente nuevamente',
            });
          }
        });
    }
  };

  enviarContratoFirmado = () => {
    var params = new HttpParams()
      .set('CodigoAnexo', 'Contrato firmado')
      .set('IdTipoDocumento', '28')
      .set(
        'IdProcesoPostulacion',
        this.contratoRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileContrato.name);

    const formData = new FormData();
    formData.append('file', this.fileContrato);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.j = 4;
        this.adjuntarArchivosContrato();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  enviarBienes = () => {
    var params = new HttpParams()
      .set('CodigoAnexo', 'Inform favorable de bienes')
      .set('IdTipoDocumento', '24')
      .set(
        'IdProcesoPostulacion',
        this.contratoRequestEntity.idProcesoPostulacion
      )
      .set(
        'IdUsuarioAdjunta',
        JSON.parse('' + localStorage.getItem('usuario')).idusuario
      )
      .set('NombreArchivo', this.fileBienes.name);

    const formData = new FormData();
    formData.append('file', this.fileBienes);

    this.serv.adjuntarAnexo(params, formData).subscribe((result: any) => {
      if (result.success) {
        this.idDocumentoAdjunto = result.codigo;
        this.j = 3;
        this.adjuntarArchivosContrato();
      } else {
        this.messageService.add({
          key: 'tl',
          severity: 'error',
          summary: 'ERROR',
          detail: 'Ocurrió un problema, intente nuevamente',
        });
      }
    });
  };

  seleccionarFirma = (event: any) => {
    this.firmado = event.checked;
  };
}
