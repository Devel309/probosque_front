import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DownloadFile } from '@shared';
import { ConfirmationService, MessageService, SelectItemGroup } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { SeguridadService } from 'src/app/service/seguridad.service';


@Component({
  selector: 'app-mantenimiento-usuarios',
  templateUrl: './mantenimiento-usuarios.component.html',
  styleUrls: ['./mantenimiento-usuarios.component.scss'],
  providers: [MessageService]
})
export class MantenimientoUsuariosComponent implements OnInit {

  @ViewChild('numeroDocumento') inputNumeroDocumento: ElementRef | undefined;
  usuario = {} as UsuarioModel;
  MantenedorFormulario: FormGroup;

  estados: ComboModel[] = DataCombos.EstadosMaestas;
  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accionMantenimiento: string = "";

  listInternos: any[] = [];
  listExternos: any[] = [];
  lstDepartamentos: ComboModel[] = [];
  comboEntidad: SelectItemGroup[] = [];
  ListaAutoridad: any;
  ListaEntidad: any;
  ListaEmpresa: any;
  ListaDocumento: any;
  totalRecords: number = 0;

  listaBandejaUsuario: UsuarioModel[] = [];
  usuarioContext: UsuarioModel = new UsuarioModel();

  objbuscar = {
    "dato": "",
    "idestado": '1',
    "indice": 1,
    "limite": 10
  };

  displayConfirm: boolean = false;

  openModalMantenimiento(data: any, tipo: string): void {
    this.accionMantenimiento = tipo;
    if (tipo == "C") {
      this.MantenedorFormulario.reset();
      this.usuarioContext = new UsuarioModel();
      this.MantenedorFormulario.patchValue(this.usuarioContext);
      this.tituloModalMantenimiento = "Registrar Usuario";
    }
    else if (tipo == "E") {
      this.tituloModalMantenimiento = "Editar Usuario";
      this.ObtenerUsuario(data.idusuario);
    }
    else if (tipo == "R") {
      this.MantenedorFormulario.reset();
      this.usuarioContext = new UsuarioModel(data);
      this.MantenedorFormulario.patchValue(this.usuarioContext);
      this.tituloModalMantenimiento = "Visualizar Usuario";
    }

    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event, usuario: UsuarioModel): void {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.EliminarUsuario(usuario)
      },
      reject: () => {
        //reject action
      }
    });
  }

  constructor(private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private serUsuario: SeguridadService,
    private serCoreCentral: CoreCentralService,
    private fb: FormBuilder,
    private utilSev: UtilitariosService,
    private dialog: MatDialog) {

    this.MantenedorFormulario = this.fb.group({
      idusuario: [0],
      codigoUsuario: [''],
      usuario: new FormControl(),
      correoElectronico: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      contrasenia: new FormControl('',  [
        Validators.required,
        Validators.minLength(8),
        ValidatorsExtend.passwordValid(),
        ValidatorsExtend.passwordValidReverseSame()
      ],),
      idtipoDocumento: new FormControl('idtipoDocumento', Validators.compose([ValidatorsExtend.comboRequired()])),
      numeroDocumento: new FormControl(0, Validators.compose([Validators.required])),
      nombres: new FormControl('', Validators.compose([Validators.required])),
      apellidoPaterno: new FormControl('', Validators.compose([Validators.required])),
      apellidoMaterno: new FormControl('', Validators.compose([Validators.required])),
      identidad: new FormControl('identidad', Validators.compose([ValidatorsExtend.comboRequired()])),
      idempresa: new FormControl('idempresa', Validators.compose([ValidatorsExtend.comboRequired()])),
      idautoridad: new FormControl('idautoridad', Validators.compose([ValidatorsExtend.comboRequired()])),
      region: new FormControl('region', Validators.compose([Validators.required])),
      estado: new FormControl('estado')
    });

    this.MantenedorFormulario.get('correoElectronico')?.valueChanges.subscribe(value => {
      //  this.MantenedorFormulario.controls['usuario'].setValue(value);

    });


    this.MantenedorFormulario.get('idtipoDocumento')?.valueChanges.subscribe(value => {
      this.MantenedorFormulario.get('numeroDocumento')?.clearValidators();

      if (value == 3) {
        this.inputNumeroDocumento?.nativeElement.setAttribute('maxlength', '8');
        this.MantenedorFormulario.get('numeroDocumento')?.setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(8)]);
        this.MantenedorFormulario.controls['numeroDocumento'].setValue(undefined);
      } else if (value == 4) {
        this.inputNumeroDocumento?.nativeElement.setAttribute('maxlength', '11');
        this.MantenedorFormulario.get('numeroDocumento')?.setValidators([Validators.required, Validators.minLength(11), Validators.maxLength(11)]);
        this.MantenedorFormulario.controls['numeroDocumento'].setValue(undefined);
      } else {
        this.inputNumeroDocumento?.nativeElement.setAttribute('maxlength', '12');
        this.MantenedorFormulario.get('numeroDocumento')?.setValidators([Validators.required, Validators.minLength(12), Validators.maxLength(12)]);
        this.MantenedorFormulario.controls['numeroDocumento'].setValue(undefined);
      }


    });


  }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarBandeja(this.objbuscar);

    this.listaComboRegion();
    this.listaComboEntidad(null);
    this.listaComboAutoridad(null);

    this.listaComboGenerico(2);
    this.listaComboGenerico(3);


  }
  listaComboEntidad(obj: any) {
    var params = obj;
    this.serUsuario.listaComboEntidad1(params).subscribe(
      (result: any) => {
        result.data.forEach((element: any, index: number) => {

          if (element.codigoEntidad == "TEINT") {
            this.listInternos.push({ label: element.text, value: element.value, id: index + 1, });

          }
          if (element.codigoEntidad == "TEEXT") {
            this.listExternos.push({ label: element.text, value: element.value, id: index + 1, });
          }
        });

        this.comboEntidad.push({ label: "I.Entidad Interna", value: "Entidad", items: this.listInternos, });
        this.comboEntidad.push({ label: "II.Entidad Externa", value: "Entidad", items: this.listExternos, });

        
      },(error) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      });


  }

  entidadChange(event: any) {
    let rubroMap: any[] = [];

    this.comboEntidad.forEach((rubros) => {
      rubros.items.forEach((item: any) => { rubroMap.push(item); });
    });

    rubroMap.forEach((rubro) => {
      if (rubro.value == event.value) {
      //  this.rentabilidadManejo.id = rubro.id;
        //this.rentabilidadManejo.rubro = rubro.value;
      }
    });
  }
  listaComboGenerico(obj: any) {
    var params = obj;

    this.serUsuario.listaComboGenerico(params).subscribe(
      (result: any) => {
        if (result.success) {
          // this.listaBandejaUsuario= result.data;
          if (obj == 2) {
            this.ListaDocumento = result.data;
          }
          else if (obj == 3) {
            this.ListaEmpresa = result.data;
          }

        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }
  listaComboRegion() {
    var params = {};

    this.serCoreCentral.listarPorFiltroDepartamento(params).subscribe(
      (result: any) => {

        if (result.success) {
          this.lstDepartamentos.push({
            code: '',
            name: '-seleccionar-'
          });
          result.data.forEach((d: any) => {
            this.lstDepartamentos.push({
              code: d.codDepartamento,
              name: d.nombreDepartamento
            });
          });


        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }
  listaComboAutoridad(obj: any) {
    var params = obj;

    this.serUsuario.listaComboAutoridad(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.ListaAutoridad = result.data;

        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  loadData(event: any) {
    this.objbuscar.indice = event.first + 1;
    this.objbuscar.limite = event.rows;
    this.listarBandeja(this.objbuscar);
  }

  listarBandeja(obj: any) {
    var params = obj;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serUsuario.bandejaUsuario(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaBandejaUsuario = result.data;
          this.totalRecords = result.totalRegistro;
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  ObtenerUsuario(idusuario: any) {
    var params = {
      "idusuario": idusuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serUsuario.obtenerUsuario(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.MantenedorFormulario.reset();
          this.usuarioContext = new UsuarioModel(result.data[0]);
          this.MantenedorFormulario.patchValue({
            ...this.usuarioContext, contrasenia: result.data[0].contrasenia
          });
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  registrarUsuario(data: UsuarioModel) {
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
    if (this.MantenedorFormulario.valid) {
      this.displayConfirm = true;
    }
  }

  confirmarEnvioRegistro() {
    this.displayConfirm = false;
    this.dialog.open(LoadingComponent, { disableClose: true });
    var params = this.MantenedorFormulario.value;
    params.idUsuarioRegistro = this.usuario.idusuario;
    
    this.serUsuario.registrarUsuario(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          this.listarBandeja(this.objbuscar);
          this.verModalMantenimiento = false;
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  EliminarUsuario(data: UsuarioModel) {
    var params = {
      "idusuario": data.idusuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serUsuario.eliminarUsuario(params).subscribe(
      (result: any) => {
        if (result.success) {
          // ;
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          this.objbuscar.indice = 1;
          this.listarBandeja(this.objbuscar);

          this.verModalMantenimiento = false;
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  btnLimpiar(){
    this.objbuscar = {
      "dato": "",
      "idestado": '1',
      "indice": 1,
      "limite": 10
    };

    this.listarBandeja(this.objbuscar);
  }

  btnDescargarExcel(){
    let params={
      dato: this.objbuscar.dato,
      idestado: this.objbuscar.idestado
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serUsuario.exportarUsuariosExcel(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data.length > 0) {
          let auxArchivo=result.data[0];
          DownloadFile(
            auxArchivo.archivo,
            auxArchivo.nombeArchivo,
            'application/octet-stream'
          );

      }

    }, () => {
      this.dialog.closeAll();
    });
  }

}


