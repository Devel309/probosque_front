import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { remove } from 'lodash';
import {ConfirmationService, MessageService} from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EntidadModel } from 'src/app/model/seguridad/entidad';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';
import { CoreCentralService } from 'src/app/service/coreCentral.service';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-mantenimiento-entidades',
  templateUrl: './mantenimiento-entidades.component.html',
  styleUrls: ['./mantenimiento-entidades.component.scss'],
  providers: [MessageService]
})
export class MantenimientoEntidadesComponent implements OnInit {

  MantenedorFormulario: FormGroup;
  usuario={} as UsuarioModel;
  estados:ComboModel[] = DataCombos.EstadosMaestas;
  verModalMantenimiento:boolean = false;
  tituloModalMantenimiento:string = "";
  accionMantenimiento:string="";
  lstDepartamentos:ComboModel[] = [];
  lstProvincias:ComboModel[] = [];
  lstDistrito:ComboModel[] = [];
  lstEntidadPadre:ComboModel[] = [];
  lstEntidadPadreData:ComboModel[] = [];
  totalRecords: number=0;
  listaBandejaEntidad :EntidadModel[] = [];
  entidadContext:EntidadModel = new EntidadModel();
  comparador:number=0;

   objbuscar={
    "dato": "",
    "idestado": 1,
    "indice": 1,
    "limite": 10
  };
  openModalMantenimiento(tipo:string,data:any):void{
    const lstEntidadPadreFiltro:ComboModel[] = [];
    this.accionMantenimiento = tipo;
    this.lstProvincias = [];
      this.lstDistrito = [];
      this.comparador=0;
    if(tipo == "C")
    {
      this.MantenedorFormulario.reset();
      if(!this.MantenedorFormulario.controls['flagTipoEntidad']) {
        this.MantenedorFormulario.addControl('flagTipoEntidad', new FormControl(false,Validators.compose([Validators.required])));
      }
      this.entidadContext = new EntidadModel();
      this.MantenedorFormulario.patchValue(this.entidadContext);
      this.addValidacionRuc();
      this.listaComboDepartamento();
      this.tituloModalMantenimiento = "Registrar Entidad";
    }    else if(tipo == "E"){
      this.tituloModalMantenimiento = "Editar Entidad";
      this.MantenedorFormulario.reset();
      this.ObtenerEntidad(data.identidad);
      //this.listaComboEntidadPadre(data.identidad);
      this.lstEntidadPadreData.forEach((d:any) => {
        if(d.code !== data.identidad){
          lstEntidadPadreFiltro.push(d);
        }
      });
      this.lstEntidadPadre = lstEntidadPadreFiltro;

    }else if(tipo == "R")
    {
      this.MantenedorFormulario.reset();
      if(!this.MantenedorFormulario.controls['flagTipoEntidad']) {
        this.MantenedorFormulario.addControl('flagTipoEntidad', new FormControl(false,Validators.compose([Validators.required])));
      }
      this.entidadContext = new EntidadModel(data);
      this.listaComboDepartamento();
      this.MantenedorFormulario.patchValue(this.entidadContext);
      this.tituloModalMantenimiento = "Visualizar Entidad";
    }

    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event,perfil:any):void{

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Sí',
      rejectLabel:'No',
      accept: () => {
         this.EliminarEntidad(perfil);
      },
      reject: () => {
          //reject action
      }
   });
  }

  lstaComboDistrito(isChange: boolean = false) {
    this.lstDistrito = [];

    const params = { codProvincia: this.MantenedorFormulario.get("idProvincia")?.value };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serCoreCentral.listarPorFilroDistrito(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (isChange) this.MantenedorFormulario.controls.idDistrito.setValue("");
      if (result.success) {
        this.lstDistrito = result.data.map((d: any) => { return { code: d.codDistrito, name: d.nombreDistrito } });
        if (!this.MantenedorFormulario.get("idDistrito")?.value) {
          this.MantenedorFormulario.controls.idDistrito.setValue(this.lstDistrito[0].code);
        }
      } else {
        this.messageService.add({ severity: "warn", summary: "", detail: result.message });
      }
    }, (error) => {
      this.dialog.closeAll();
      this.messageService.add({ severity: "warn", summary: "", detail: error });
    }
    );
  }

  listaComboProvincia(isChange: boolean = false) {
    this.lstProvincias = [];
    const params = { codDepartamento: this.MantenedorFormulario.get("idDepartamento")?.value };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serCoreCentral.listarPorFilroProvincia(params).subscribe((result: any) => {
      this.dialog.closeAll();
      if (isChange) this.MantenedorFormulario.controls.idProvincia.setValue("");
      if (result.success) {
        this.lstProvincias = result.data.map((d: any) => { return { code: d.codProvincia, name: d.nombreProvincia } });
        if (!this.MantenedorFormulario.get("idProvincia")?.value) {
          this.MantenedorFormulario.controls.idProvincia.setValue(this.lstProvincias[0].code);
        }
        this.lstaComboDistrito(isChange);
      } else {
        this.messageService.add({ severity: "warn", summary: "", detail: result.message });
      }
    }, (error) => {
      this.dialog.closeAll();
      this.messageService.add({ severity: "warn", summary: "", detail: error });
    }
    );
  }

  listaComboDepartamento() {
    this.serCoreCentral.listarPorFiltroDepartamento({}).subscribe((result: any) => {
      if (result.success) {
        this.lstDepartamentos = result.data.map((d: any) => { return { code: d.codDepartamento, name: d.nombreDepartamento } });
        if (!this.MantenedorFormulario.get("idDepartamento")?.value) {
          this.MantenedorFormulario.controls.idDepartamento.setValue(this.lstDepartamentos[0].code);
        }
        this.listaComboProvincia();
      } else {
        this.messageService.add({ severity: "warn", summary: "", detail: result.message });
      }
    }, (error) => {
      this.messageService.add({ severity: "warn", summary: "", detail: error });
    }
    );

  }

  listaComboEntidadPadre(idusuario:any){
    var params = {
      "identidad": idusuario
    };
    this.lstEntidadPadre = [];
    this.lstEntidadPadreData = [];
    this.serEntidad.listaComboEntidad(params).subscribe(
      (result : any)=>{

        if(result.success){
          result.data.forEach((d:any) => {
            this.lstEntidadPadre.push({
              code:d.value,
              name:d.text
            });
            this.lstEntidadPadreData.push({
              code:d.value,
              name:d.text
            });
          });
        }else{
          
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }

      },
      (error)=>{
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );

  }

  loadData(event:any){

    this.objbuscar.indice=event.first+1;
    this.objbuscar.limite=event.rows;
     this.listarBandeja(this.objbuscar);
   }

  listarBandeja(obj:any){
    var params = obj;
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.serEntidad.bandejaEntidad(params).subscribe(
      (result : any)=>{
        this.listaBandejaEntidad = [];
        if(result.success){
          this.listaBandejaEntidad= result.data;
          this.totalRecords=result.totalRegistro;
        }else{
          
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
          this.totalRecords=0;
        }
       this.dialog.closeAll();
      },
      (error)=>{
        this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );

}

ObtenerEntidad(idusuario:any){
  this.MantenedorFormulario.addControl('flagTipoEntidad', new FormControl(false,Validators.compose([Validators.required])));
  var params ={
      "identidad": idusuario
    }
    this.dialog.open(LoadingComponent,{ disableClose: true });
  this.serEntidad.obtenerEntidad(params).subscribe(
    (result : any)=>{
      if(result.success){
        this.entidadContext = new EntidadModel(result.data[0]);
        this.MantenedorFormulario.patchValue(this.entidadContext);
        this.changeFlagEntidad();
        this.listaComboDepartamento();
      }else{
        this.messageService.add({severity:"warn", summary: "", detail: result.message});
      }
      this.dialog.closeAll();
    },
    (error)=>{
      this.dialog.closeAll();
      this.messageService.add({severity:"warn", summary: "", detail: error});
    }
  );
}

registrarEntidad(data:EntidadModel){

   this.utilSev.markFormGroupTouched(this.MantenedorFormulario);

  if(this.MantenedorFormulario.valid){

    this.MantenedorFormulario.controls['ubigeo'].setValue(this.MantenedorFormulario.controls['idDistrito'].value);
    

    if(this.MantenedorFormulario.value.flagTipoEntidad==true){

      this.MantenedorFormulario.controls['ruc'].setValue("0");
      this.MantenedorFormulario.removeControl('flagTipoEntidad');
      var params= this.MantenedorFormulario.value;
      params.idUsuarioRegistro =  this.usuario.idusuario;
      params.codigoEntidad="TEINT";
      var valor=params.identidad;
      this.dialog.open(LoadingComponent,{ disableClose: true });
      this.serEntidad.registrarEntidad(params).subscribe((result : any)=>{
          this.dialog.closeAll();

            if(result.success){
              this.messageService.add({severity:"success", summary: "", detail: result.message});
              this.listarBandeja(this.objbuscar);
              this.listaComboEntidadPadre(valor);
              this.verModalMantenimiento = false;
            }else{
              this.dialog.closeAll();
              this.messageService.add({severity:"warn", summary: "", detail: result.message});
            }
          },
          (error)=>{
            this.dialog.closeAll();
            this.messageService.add({severity:"warn", summary: "", detail: error});
          }
        );
    }else{

      this.MantenedorFormulario.removeControl('flagTipoEntidad');
      this.MantenedorFormulario.value.codigoEntidad="TEEXT";
      var params = this.MantenedorFormulario.value;
      params.idUsuarioRegistro =  this.usuario.idusuario;
      
        this.dialog.open(LoadingComponent,{ disableClose: true });
        this.serEntidad.registrarEntidad(params).subscribe(
          (result : any)=>{
            if(result.success){
              this.dialog.closeAll();
              this.messageService.add({severity:"success", summary: "", detail: result.message});
              this.listarBandeja(this.objbuscar);
              this.listaComboEntidadPadre(valor);
              this.verModalMantenimiento = false;
            }else{
              this.dialog.closeAll();
              this.messageService.add({severity:"warn", summary: "", detail: result.message});
            }
          },
          (error)=>{
            this.dialog.closeAll();
            
            this.messageService.add({severity:"warn", summary: "", detail: error});
          }
        );

    }

  } else {
    if(!this.MantenedorFormulario.value.sigla){
      this.messageService.add({severity:"warn", summary: "",detail: "(*) Debe Ingresar la Sigla."});
    }

    if(this.MantenedorFormulario.value.flagTipoEntidad){
      /*if(!this.MantenedorFormulario.value.identidadPadre) {
        this.messageService.add({severity:"warn", summary: "",detail: "(*) Debe seleccionar la Entidad Padre."});
      }*/
    } else {
      if(!this.MantenedorFormulario.value.ruc){
        this.messageService.add({severity:"warn", summary: "",detail: "(*) Debe Ingresar el RUC."});
      }
    }

    if(!this.MantenedorFormulario.value.direccion){
      this.messageService.add({severity:"warn", summary: "",detail: "(*) Debe Ingresar la Dirección."});
    }

  }

}

private EliminarEntidad(data:EntidadModel){
  var params ={
      "identidad": data.identidad
    }
  this.dialog.open(LoadingComponent,{ disableClose: true });
  this.serEntidad.eliminarEntidad(params).subscribe(
    (result : any)=>{
      if(result.success){
       this.messageService.add({severity:"success", summary: "", detail: result.message});
       this.listarBandeja(this.objbuscar);
       this.listaComboEntidadPadre("");
       this.verModalMantenimiento = false;
      }else{
        this.messageService.add({severity:"warn", summary: "", detail: result.message});
      }
      this.dialog.closeAll();
    },
    (error)=>{
      this.dialog.closeAll();
      
      this.messageService.add({severity:"warn", summary: "", detail: error});
    }
  );
}

btnLimpiar(){
  this.objbuscar = {
    "dato": "",
    "idestado": 1,
    "indice": 1,
    "limite": 10
  };

  this.listarBandeja(this.objbuscar);
}

changeFlagEntidad() {
  if(this.MantenedorFormulario.controls['flagTipoEntidad'].value) this.addValidacionIdEntPadre();
  else this.addValidacionRuc();
}

addValidacionIdEntPadre() {
  //this.MantenedorFormulario.controls['identidadPadre'].setValidators(Validators.compose([ValidatorsExtend.comboRequired()]));
  this.MantenedorFormulario.controls['ruc'].clearValidators();
  this.MantenedorFormulario.controls['ruc'].setValue("");
}
addValidacionRuc() {
  this.MantenedorFormulario.controls['ruc'].setValidators([Validators.required, Validators.minLength(11), Validators.maxLength(11)]);
  this.MantenedorFormulario.controls['identidadPadre'].clearValidators();
  this.MantenedorFormulario.controls['identidadPadre'].setValue(0);
}

constructor(private confirmationService: ConfirmationService
  ,private messageService: MessageService
  ,private serEntidad :SeguridadService
  ,private serCoreCentral:CoreCentralService
  ,private fb: FormBuilder
  ,private utilSev:UtilitariosService
  ,private dialog: MatDialog) {

    this.MantenedorFormulario = this.fb.group({
      identidad: [0] ,
      sigla: new FormControl('',Validators.compose([Validators.required])),
      identidadPadre: new FormControl('identidadPadre'),
      idDepartamento: new FormControl('',Validators.compose([Validators.required])),
      idProvincia: new FormControl('',Validators.compose([Validators.required])),
      idDistrito: new FormControl('',Validators.compose([Validators.required])),
      ruc: new FormControl(''),
      estado: new FormControl('estado'),
      codigoDependencia: new FormControl(''),
      direccion: new FormControl('',Validators.compose([Validators.required])),
      ubigeo:[''],
      codigoEntidad : [''],
      descripcion: [''],
      flagTipoEntidad: false,
    });
  }

ngOnInit(): void {
  this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
  this.listarBandeja(this.objbuscar);
  this.listaComboEntidadPadre("");
}




}


