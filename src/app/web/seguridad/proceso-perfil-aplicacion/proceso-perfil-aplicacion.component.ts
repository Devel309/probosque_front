import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PerfilAplicacionModel } from 'src/app/model/seguridad/perfilAplicacion';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { Paginator } from 'primeng/paginator';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';

@Component({
  selector: 'app-proceso-perfil-aplicacion',
  templateUrl: './proceso-perfil-aplicacion.component.html',
  styleUrls: ['./proceso-perfil-aplicacion.component.scss']
})
export class ProcesoPerfilAplicacionComponent implements OnInit {

  @ViewChild('paginator', { static: true }) paginator: Paginator | undefined;

    
  usuario={} as UsuarioModel;
  estados:ComboModel[] = DataCombos.EstadosMaestas;
  estadosDetalle:ComboModel[] = DataCombos.EstadosMaestas2 ;
  verModalMantenimiento:boolean = false;
  tituloModalMantenimiento:string = "";
  accionMantenimiento:string="";
  listaBandejaPerfilAplicacion:PerfilAplicacionModel[] = [];
  totalRecords: number=0;
 
  perfilContext:number = 0;
  lstDetallePerfilAplicacion:PerfilAplicacionModel[] = [];
  lstComboPerfiles:any[] = [];
  lstComboAplicacion:any[] = [];

  objbuscar={
    "dato": "",
    "idestado": '1',
    "indice": 1,
    "limite": 10
  };

  openModalMantenimiento(tipo:string,data:any):void{
    this.accionMantenimiento = tipo;
    this.lstDetallePerfilAplicacion=[];
    if(tipo == "C")
    {
      this.perfilContext = 0;
      this.tituloModalMantenimiento = "Registrar Aplicaciones por Perfil";
    }    else if(tipo == "E"){
      this.perfilContext = data.idperfil;
      this.obtenerPerfilAplicacion({value : this.perfilContext});
      this.tituloModalMantenimiento = "Editar Aplicaciones por Perfil";
    }else if(tipo == "R")
    {
      this.perfilContext = data.idperfil;
      this.obtenerPerfilAplicacion({value : this.perfilContext});
      this.tituloModalMantenimiento = "Visualizar Aplicaciones por Perfil";
    }
   
    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event,data:any,tipo:string):void{

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Sí',
      rejectLabel:'No',
      accept: () => {
          this.removerPerfilAplicacion(data,tipo);
      },
      reject: () => {
          
      }
   });
  }

  loadData(event:any){
      this.objbuscar.indice=event.first+1;
      this.objbuscar.limite=event.rows;
      this.listarBandeja(this.objbuscar, false);
  }


  listarBandeja(obj:any, reload: boolean = false){
  var params = obj;
  if(reload) this.listaBandejaPerfilAplicacion = [];
  this.dialog.open(LoadingComponent,{ disableClose: true });
  this.serSeguridad.bandejaPerfilAplicacion(params).subscribe(
    (result : any)=>{
      if(result.success){
        this.listaBandejaPerfilAplicacion = result.data;
        this.totalRecords=result.totalRegistro;

      }else{
        
        if(!reload) this.messageService.add({severity:"warn", summary: "", detail: result.message});
      }
      this.dialog.closeAll();
    },
    (error)=>{
      this.dialog.closeAll();
      
      this.messageService.add({severity:"warn", summary: "", detail: error});
    }
  );

  }

  listarComboPerfil()
  {
    this.serSeguridad.listaComboPerfil({}).subscribe(
      (result : any)=>{
        if(result.success){
          this.lstComboPerfiles = result.data;
  
        }else{
          
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
        
      },
      (error)=>{
        
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );
  }

  listarComboAplicacion()
  {
    this.serSeguridad.listaComboAplicacion({}).subscribe(
      (result : any)=>{
        if(result.success){
          this.lstComboAplicacion = result.data;
  
        }else{
          
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
        
      },
      (error)=>{
        
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );
  }

  obtenerPerfilAplicacion(e:any):void
  {
    if(e.value > 0)
    {
      this.lstDetallePerfilAplicacion = [];
      var params ={idperfil:  e.value};
      this.dialog.open(LoadingComponent,{ disableClose: true });
      this.serSeguridad.obtenerPerfilAplicacion(params).subscribe(
        (result : any)=>{
          if(result.success){
            this.lstDetallePerfilAplicacion = result.data;
    
          }else{
            
            this.messageService.add({severity:"warn", summary: "", detail: result.message});
          }
          this.dialog.closeAll();
        },
        (error)=>{
          this.dialog.closeAll();
          
          this.messageService.add({severity:"warn", summary: "", detail: error});
        }
      );
    }else{
      this.lstDetallePerfilAplicacion = [];
    }
    

  }

  agregarNuevoPerfilAplicacion():void{
    let item = new PerfilAplicacionModel();
    item.local = true;
    item.idperfil = this.perfilContext;
    item.sysusuario =  this.usuario.idusuario;
    this.lstDetallePerfilAplicacion.push(item);
  }

  private removerPerfilAplicacion(data:PerfilAplicacionModel,tipo:string):void{
      if(data.local && tipo == 'D')
      {
        const index = this.lstDetallePerfilAplicacion.indexOf(data, 0);
        this.lstDetallePerfilAplicacion.splice(index,1);
      }else{
        this.dialog.open(LoadingComponent,{ disableClose: true });
        let params = data;
        this.serSeguridad.eliminarPerfilAplicacion(params).subscribe(
          (result : any)=>{
            if(result.success){
              this.messageService.add({severity:"success", summary: "", detail: result.message});
              if(tipo == 'D'){
                this.obtenerPerfilAplicacion({value : this.perfilContext});
              }else{
                this.paginator?.changePage(0);              
                this.listarBandeja(this.objbuscar, true);
              }

      
            }else{
              
              this.messageService.add({severity:"warn", summary: "", detail: result.message});
            }
            this.dialog.closeAll();
          },
          (error)=>{
            this.dialog.closeAll();
            
            this.messageService.add({severity:"warn", summary: "", detail: error});
          }
        );

      }
  }

  guardarPerfilAplicacion()
  {
    let params = this.lstDetallePerfilAplicacion;
    this.dialog.open(LoadingComponent,{ disableClose: true });
        this.serSeguridad.registrarPerfilAplicacion(params).subscribe(
          (result : any)=>{
            if(result.success){
              this.messageService.add({severity:"success", summary: "", detail: result.message});
                   this.obtenerPerfilAplicacion({value : this.perfilContext});
            }else{
              
              this.messageService.add({severity:"warn", summary: "", detail: result.message});
            }
            this.dialog.closeAll();
          },
          (error)=>{
            this.dialog.closeAll();
            
            this.messageService.add({severity:"warn", summary: "", detail: error});
          }
        );
  }

  btnLimpiar(){
    this.objbuscar = {
      "dato": "",
      "idestado": '1',
      "indice": 1,
      "limite": 10
    };
    
    this.listarBandeja(this.objbuscar, false);
  }

  constructor(private confirmationService: ConfirmationService
    ,private messageService: MessageService
    ,private serSeguridad :SeguridadService
    ,private dialog: MatDialog) { }

  ngOnInit(): void {

    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarComboPerfil();   
    this.listarComboAplicacion();
    this.listarBandeja(this.objbuscar, false);

  }
/*
  filterCountry(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    let query = event.query;

    for(let i = 0; i < this.countries.length; i++) {
        let country = this.countries[i];
        if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(country);
        }
    }
    
    this.filteredCountries = filtered;
}*/

}
