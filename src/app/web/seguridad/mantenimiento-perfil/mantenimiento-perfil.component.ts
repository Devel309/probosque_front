import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PerfilModel } from 'src/app/model/seguridad/perfil';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-mantenimiento-perfil',
  templateUrl: './mantenimiento-perfil.component.html',
  styleUrls: ['./mantenimiento-perfil.component.scss'],
  providers: [MessageService]
})
export class MantenimientoPerfilComponent implements OnInit {

  usuario={} as UsuarioModel;
  MantenedorFormulario: FormGroup;

  estados:ComboModel[] = DataCombos.EstadosMaestas;
  verModalMantenimiento:boolean = false;
  tituloModalMantenimiento:string = "";
  accionMantenimiento:string="";

  listacontacto:any;
  totalRecords: number=0;
  listaBandejaPerfil:PerfilModel[] =[];
  perfilContext:PerfilModel = new PerfilModel();
  
  objbuscar={
    "dato": "",
    "idestado": '1',
    "indice": 1,
    "limite": 10
  };

  btnLimpiar(){
    this.objbuscar = {
      "dato": "",
      "idestado": "1",
      "indice": 1,
      "limite": 10
    };
    this.listarBandeja(this.objbuscar);
  }

  openModalMantenimiento(tipo:string,data:any):void{
    this.accionMantenimiento = tipo;
    if(tipo == "C")
    {
      this.MantenedorFormulario.reset();
      this.perfilContext = new PerfilModel();
      this.MantenedorFormulario.patchValue(this.perfilContext);
      this.tituloModalMantenimiento = "Registrar Perfil";
      this.verModalMantenimiento = true;
    }    else if(tipo == "E"){
      this.ObtenerPerfil(data);
      this.tituloModalMantenimiento = "Editar Perfil";
    }else if(tipo == "R")
    {
      this.perfilContext = new PerfilModel(data);
      this.MantenedorFormulario.patchValue(this.perfilContext);
      this.tituloModalMantenimiento = "Visualizar Perfil";
      this.verModalMantenimiento = true;
    }
   
    
  }

  private confirmarEliminarPerfil(perfil:PerfilModel):void
  {
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.serSeguridad.eliminarPerfil(perfil).subscribe(
      (result : any)=>{
        if(result.success){         
          this.messageService.add({severity:"success", summary: "", detail: result.message});
          this.listarBandeja(this.objbuscar, false);
          
        }else{
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
        this.dialog.closeAll();
      },
      (error)=>{
        this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );
  }

  openEliminarRegistro(event: Event,perfil:any):void{
    
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Si',
      rejectLabel:'No',
      accept: () => {
          this.confirmarEliminarPerfil(perfil);
      },
      reject: () => {
          //reject action
      }
   });
  }


  constructor(private confirmationService: ConfirmationService
    ,private messageService: MessageService
    ,private serSeguridad :SeguridadService
    ,private fb: FormBuilder
    ,private utilSev:UtilitariosService
    ,private dialog: MatDialog) { 

      this.MantenedorFormulario = this.fb.group({
        idperfil: [0] ,
        codigoPerfil: new FormControl(''),
        perfil: new FormControl('',Validators.compose([Validators.required])),
        idtipoContacto: new FormControl('idtipoContacto',Validators.compose([Validators.required,Validators.min(1)])),
        descripcion: new FormControl('',[Validators.required]),
        estado: new FormControl('estado')
      });
    }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listaComboGenerico(1);
    this.listarBandeja(this.objbuscar, false);
  }

  listaComboGenerico(obj:any){
    var params = obj;
  
    this.serSeguridad.listaComboGenerico(params).subscribe(
      (result : any)=>{
        if(result.success){
          if(obj==1){
            this.listacontacto=result.data;
          }         
       
        }else{
          
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
       
      },
      (error)=>{
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );
  
  }

  loadData(event:any){
//   ;
      this.objbuscar.indice=event.first+1;
      this.objbuscar.limite=event.rows;
      this.listarBandeja(this.objbuscar, false);
  }

  listarBandeja(obj:any, reload: boolean = false){
    var params = obj;
    if(reload) this.listaBandejaPerfil = [];
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.serSeguridad.bandejaPerfil(params).subscribe(
      (result : any)=>{
        if(result.success){
          this.listaBandejaPerfil = result.data;
          this.totalRecords=result.totalRegistro;
         
        }else{
         
          
          if(!reload) this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
        this.dialog.closeAll();
      },
      (error)=>{
        this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );

}

ObtenerPerfil(objPerfil:PerfilModel){
  var params ={
      "idperfil": objPerfil.idperfil
    };
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.serSeguridad.obtenerPerfil(params).subscribe(
    (result : any)=>{
      if(result.success){
        
        this.perfilContext = new PerfilModel(result.data[0]);
        this.MantenedorFormulario.patchValue(this.perfilContext);
        this.verModalMantenimiento = true;
        
      }else{
        this.messageService.add({severity:"warn", summary: "", detail: result.message});
      }
      this.dialog.closeAll();
    },
    (error)=>{
      this.dialog.closeAll();
      
      this.messageService.add({severity:"warn", summary: "", detail: error});
    }
  );
}



registrarPerfil(data:PerfilModel){
  this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
  if(this.MantenedorFormulario.valid){
    this.dialog.open(LoadingComponent,{ disableClose: true });
    var params =this.MantenedorFormulario.value; 
    params.idUsuarioRegistro =  this.usuario.idusuario;
      this.serSeguridad.registrarPerfil(params).subscribe(
        (result : any)=>{
          if(result.success){
            
            this.messageService.add({severity:"success", summary: "", detail: result.message});
            this.listarBandeja(this.objbuscar, true);
            this.verModalMantenimiento = false;
           
          }else{
            this.messageService.add({severity:"warn", summary: "", detail: result.message});
          }
          this.dialog.closeAll();
        },
        (error)=>{
          this.dialog.closeAll();
          
          this.messageService.add({severity:"warn", summary: "", detail: error});
        }
      );
  }

}

}


