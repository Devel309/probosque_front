import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { AccionModel } from 'src/app/model/seguridad/accion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-mantenimiento-acciones',
  templateUrl: './mantenimiento-acciones.component.html',
  styleUrls: ['./mantenimiento-acciones.component.scss'],
  providers: [MessageService]
})
export class MantenimientoAccionesComponent implements OnInit {

  MantenedorFormulario: FormGroup;
  usuario = {} as UsuarioModel;
  estados: ComboModel[] = DataCombos.EstadosMaestas;
  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accionMantenimiento: string = "";

  totalRecords: number = 0;
  listaBandejaAccion: any;

  accionContext: AccionModel = new AccionModel();
  objbuscar = {
    "dato": "",
    "idestado": "1",
    "indice": 1,
    "limite": 10
  };


  openModalMantenimiento(tipo: string, data: any): void {
    this.accionMantenimiento = tipo;
    if (tipo == "C") {
      this.MantenedorFormulario.reset();
      this.accionContext = new AccionModel();
      this.MantenedorFormulario.patchValue(this.accionContext);
      this.tituloModalMantenimiento = "Registrar Acción";
    } else if (tipo == "E") {
      this.tituloModalMantenimiento = "Editar Acción";
      this.ObtenerAccion(data.idaccion);
    } else if (tipo == "R") {
      this.MantenedorFormulario.reset();
      this.accionContext = new AccionModel(data);
      this.MantenedorFormulario.patchValue(this.accionContext);
      this.tituloModalMantenimiento = "Visualizar Acción";
    }

    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event, accion: AccionModel): void {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.EliminarAccion(accion);
      },
      reject: () => {
        //reject action
      }
    });
  }

  constructor(private confirmationService: ConfirmationService,
    private messageService: MessageService
    , private serAccion: SeguridadService,
    private fb: FormBuilder,
    private utilSev: UtilitariosService,
    private dialog: MatDialog) {


    this.MantenedorFormulario = this.fb.group({
      idaccion: [0],
      accion: new FormControl('', Validators.compose([Validators.required])),
      descripcion: new FormControl('', Validators.compose([Validators.required])),
      estado: new FormControl('estado'),
      sysusuario: new FormControl('sysusuario')
    });



  }

  ngOnInit(): void {

    this.listarBandeja(this.objbuscar, false);

    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());

  }


  loadData(event: any) {
    //this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.objbuscar.indice = event.first + 1;
    this.objbuscar.limite = event.rows;
    this.listarBandeja(this.objbuscar,false);
  }


  listarBandeja(obj: any, reload: boolean = false) {
    var params = obj;
    if(reload) this.listaBandejaAccion = [];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serAccion.bandejaAccion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaBandejaAccion = result.data;
          this.totalRecords = result.totalRegistro;

        } else {
          
          if(!reload) this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  ObtenerAccion(idusuario: any) {
    var params = {
      "idaccion": idusuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serAccion.obtenerAccion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.accionContext = new AccionModel(result.data[0]);
          this.MantenedorFormulario.patchValue(this.accionContext);
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  registrarAccion(data: AccionModel) {

    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
    if (this.MantenedorFormulario.valid) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      var params = this.MantenedorFormulario.value;
      //params.idUsuarioRegistro =  this.usuario.idusuario;
      params.sysusuario = this.usuario.idusuario;
      this.serAccion.registrarAccion(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.messageService.add({ severity: "success", summary: "", detail: result.message });
            this.listarBandeja(this.objbuscar, true);
            this.verModalMantenimiento = false;
          } else {
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          
          this.messageService.add({ severity: "warn", summary: "", detail: error });
        }
      );
    }

  }

  EliminarAccion(data: AccionModel) {
    var params = {
      "idaccion": data.idaccion,
      //"idUsuarioRegistro" :  this.usuario.idusuario
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serAccion.eliminarAccion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          this.listarBandeja(this.objbuscar, true);
          this.verModalMantenimiento = false;
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  btnLimpiar(){
    this.objbuscar = {
      "dato": "",
      "idestado": "1",
      "indice": 1,
      "limite": 10
    };
    
    this.listarBandeja(this.objbuscar, false);
  }

}
