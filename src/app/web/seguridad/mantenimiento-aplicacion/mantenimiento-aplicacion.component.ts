import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { AplicacionModel } from 'src/app/model/seguridad/aplicacion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-mantenimiento-aplicacion',
  templateUrl: './mantenimiento-aplicacion.component.html',
  styleUrls: ['./mantenimiento-aplicacion.component.scss'],
  providers: [MessageService]
})
export class MantenimientoAplicacionComponent implements OnInit {

  MantenedorFormulario: FormGroup;
  usuario={} as UsuarioModel;
  estados:ComboModel[] = DataCombos.EstadosMaestas;
  verModalMantenimiento:boolean = false;
  tituloModalMantenimiento:string = "";
  accionMantenimiento:string="";

  listaBandejaAplicacion :AplicacionModel[] = [];
  totalRecords: number=0;
  aplicacionContext:AplicacionModel = new   AplicacionModel();

  objbuscar={
    "dato": "",
    "idestado": '1',
    "indice": 1,
    "limite": 10
  };


  openModalMantenimiento(tipo:string,data:any):void{
    this.accionMantenimiento = tipo;
    if(tipo == "C")
    {
      this.MantenedorFormulario.reset();
      this.aplicacionContext =new AplicacionModel();
      this.MantenedorFormulario.patchValue(this.aplicacionContext);
      this.tituloModalMantenimiento = "Nueva Aplicación";

    }    else if(tipo == "E"){
      this.tituloModalMantenimiento = "Editar Aplicación";
      this.ObtenerAplicacion(data.idaplicacion);
    }else if(tipo == "R")
    {
      this.MantenedorFormulario.reset();
      this.aplicacionContext = new AplicacionModel(data);
      this.MantenedorFormulario.patchValue(this.aplicacionContext);
      this.tituloModalMantenimiento = "Visualizar Aplicación";
    }
   
    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event,aplicacion:AplicacionModel):void{

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Sí',
      rejectLabel:'No',
      accept: () => {
        this.EliminarAplicacion(aplicacion)
      },
      reject: () => {
          
      }
   });
  }


  constructor(private confirmationService: ConfirmationService
   , private messageService: MessageService
    ,private serAplicacion :SeguridadService,
    private fb: FormBuilder,
    private utilSev:UtilitariosService,
    private dialog: MatDialog) {

      this.MantenedorFormulario = this.fb.group({
        idaplicacion: [0] ,
        codigoAplicacion: [''],//new FormControl('',Validators.compose([Validators.required])),
        aplicacion: new FormControl('',Validators.compose([Validators.required])),
        detalleAplicacion: new FormControl('',Validators.compose([Validators.required])),
        observacionAplicacion: new FormControl('',[Validators.required]),
        tipoAplicacion: new FormControl('',[Validators.required]),
        estado: new FormControl('estado')
      });

     }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarBandeja(this.objbuscar);
  }


  loadData(event:any){
 //   ;
       this.objbuscar.indice=event.first+1;
       this.objbuscar.limite=event.rows;
        this.listarBandeja(this.objbuscar);
      }
    

  listarBandeja(obj:any){
    var params = obj;
    this.dialog.open(LoadingComponent,{ disableClose: true });
    this.serAplicacion.bandejaAplicacion(params).subscribe(
      (result : any)=>{
        if(result.success){
          this.listaBandejaAplicacion= result.data;
          this.totalRecords=result.totalRegistro;
        }else{
          
          this.messageService.add({severity:"warn", summary: "", detail: result.message});
        }
        this.dialog.closeAll();
      },
      (error)=>{
        this.dialog.closeAll();
        
        this.messageService.add({severity:"warn", summary: "", detail: error});
      }
    );

}

ObtenerAplicacion(idusuario:any){
  var params ={
      "idaplicacion": idusuario
    }
  this.dialog.open(LoadingComponent,{ disableClose: true });
  this.serAplicacion.obtenerAplicacion(params).subscribe(
    (result : any)=>{
      if(result.success){        
        this.aplicacionContext = new AplicacionModel(result.data[0]);
        this.MantenedorFormulario.patchValue(this.aplicacionContext);
      }else{
        this.messageService.add({severity:"warn", summary: "", detail: result.message});
      }
      this.dialog.closeAll();
    },
    (error)=>{
      this.dialog.closeAll();
      
      this.messageService.add({severity:"warn", summary: "", detail: error});
    }
  );
}

registrarAplicacion(data:any){

  this.utilSev.markFormGroupTouched(this.MantenedorFormulario);

  if(this.MantenedorFormulario.valid){
    this.dialog.open(LoadingComponent,{ disableClose: true });
    var params = this.MantenedorFormulario.value; 
    params.idUsuarioRegistro =  this.usuario.idusuario;
      this.serAplicacion.registrarAplicacion(params).subscribe(
        (result : any)=>{
          if(result.success){
           
            this.messageService.add({severity:"success", summary: "", detail: result.message});
            this.listarBandeja(this.objbuscar);
            this.verModalMantenimiento = false;
          }else{
            this.messageService.add({severity:"warn", summary: "", detail: result.message});
          }
          this.dialog.closeAll();
        },
        (error)=>{
          this.dialog.closeAll();
          
          this.messageService.add({severity:"warn", summary: "", detail: error});
        }
      );
  }

}


private EliminarAplicacion(data:AplicacionModel){
  var params ={
      "idaplicacion": data.idaplicacion
    }
    this.dialog.open(LoadingComponent,{ disableClose: true });
  this.serAplicacion.eliminarAplicacion(params).subscribe(
    (result : any)=>{
      if(result.success){
       this.listarBandeja(this.objbuscar);
       this.messageService.add({severity:"success", summary: "", detail: result.message});
       this.verModalMantenimiento = false;
      }else{
        this.messageService.add({severity:"warn", summary: "", detail: result.message});
      } 
      this.dialog.closeAll();
    },
    (error)=>{
      this.dialog.closeAll();
      
      this.messageService.add({severity:"warn", summary: "", detail: error});
    }
  );
}

}
