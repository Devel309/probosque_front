import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ParametroModel } from 'src/app/model/seguridad/parametro';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-mantenimiento-parametros',
  templateUrl: './mantenimiento-parametros.component.html',
  styleUrls: ['./mantenimiento-parametros.component.scss']
})
export class MantenimientoParametrosComponent implements OnInit {
  objbuscar = {
    "codigo": null,
    "descripcionValorPrimario": null,
    "valorPrimario": null,
  };

  totalRecords: number = 0;
  listaBandeja: any[] = [];

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  disabledModal: boolean = false;

  MantenedorFormulario!: FormGroup;
  requestModel = new ParametroModel();
  usuario!: UsuarioModel;

  constructor(
    private fb: FormBuilder,
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private seguridadService: SeguridadService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.crearFormu();
  }

  crearFormu() {
    this.MantenedorFormulario = this.fb.group({
      idParametro: [0],
      idTipoParametro: [0],
      nombre: [''],
      descripcion: [''],
      codigo: [''],
      valorPrimario: ['', [Validators.required, Validators.maxLength(100)]],
      idUsuarioModificacion: [null],
    });
  }

  ngOnInit(): void {
    this.listarBandeja();
  }
 
  listarBandeja() {
    this.listaBandeja = [];
    this.dialog.open(LoadingComponent, { disableClose: true });
    
    this.seguridadService.tipoParametroListar(this.objbuscar).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success && resp.data.length > 0) {
        this.listaBandeja = resp.data;
      } else {
        this.toast.warn("Información no encontrada.");
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  actualizarParametro(params: ParametroModel) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.seguridadService.parametroActualizar(params).subscribe((resp) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarBandeja();
        this.verModalMantenimiento = false;
      } else {
        this.toast.warn(resp.message);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  btnLimpiar() {
    this.objbuscar = {
      "codigo": null,
      "descripcionValorPrimario": null,
      "valorPrimario": null,
    };
    this.listarBandeja();
  }

  btnBuscar() {
    this.listarBandeja();
  }

  btnGuardar() {
    if (this.MantenedorFormulario.valid) {
      this.MantenedorFormulario.controls['idUsuarioModificacion'].setValue(this.usuario.idusuario);
      this.actualizarParametro(this.MantenedorFormulario.value);
    } else {
      this.MantenedorFormulario.markAllAsTouched();
    }
  }

  // loadData(event: any) {
  //   this.objbuscar.indice = event.first + 1;
  //   this.objbuscar.limite = event.rows;
  //   this.listarBandeja(this.objbuscar);
  // }

  openModalMantenimiento(tipo: string, datos: any): void {
    this.MantenedorFormulario.reset();
    this.requestModel.setDataInit();
    if (tipo == "E") {
      this.tituloModalMantenimiento = "Editar Parámetro";
      this.disabledModal = false;
      this.requestModel.setData(datos);
      this.MantenedorFormulario.patchValue(this.requestModel);
    } else if (tipo == "R") {
      this.tituloModalMantenimiento = "Visualizar Parámetro";
      this.disabledModal = true;
      this.requestModel.setData(datos);
      this.MantenedorFormulario.patchValue(this.requestModel);
    }

    this.verModalMantenimiento = true;
  }

}
