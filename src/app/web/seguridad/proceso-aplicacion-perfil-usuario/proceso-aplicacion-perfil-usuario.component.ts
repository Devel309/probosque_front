import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { aplicacionPerfilUsaurioModel } from 'src/app/model/seguridad/aplicacionPerfilUsuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { Paginator } from 'primeng/paginator';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';

@Component({
  selector: 'app-proceso-aplicacion-perfil-usuario',
  templateUrl: './proceso-aplicacion-perfil-usuario.component.html',
  styleUrls: ['./proceso-aplicacion-perfil-usuario.component.scss'],
  providers: [MessageService]
})
export class ProcesoAplicacionPerfilUsuarioComponent implements OnInit {

  @ViewChild('paginator', { static: true }) paginator: Paginator | undefined;

  usuario = {} as UsuarioModel;
  estados: ComboModel[] = DataCombos.EstadosMaestas;
  estadosDetalle: ComboModel[] = DataCombos.EstadosMaestas2;

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accionMantenimiento: string = "";

  totalRecords: number = 0;
  listaBandeja: aplicacionPerfilUsaurioModel[] = [];

  lstComboPerfilAplicacion: any[] = [];
  lstComboPerfilAplicacionFiltro: any[] = [];
  lstDetalle: aplicacionPerfilUsaurioModel[] = [];

  objbuscar = {
    "dato": "",
    "codigoAplicacion": "MC",
    "idperfilAplicacion": 0,
    "indice": 1,
    "limite": 10
  };

  dataFilaModal: any = {};

  openModalMantenimiento(data: any, tipo: string, idusuario: number): void {
    this.accionMantenimiento = tipo;
    this.lstDetalle = [];
    this.dataFilaModal = data;

    if (tipo == "C") {
      this.tituloModalMantenimiento = "Registrar Aplicaciones-Perfil por Usuario";
    } else if (tipo == "E") {
      this.listarUsuarioPerfil(data.idusuario);
      this.tituloModalMantenimiento = "Editar Aplicaciones-Perfil por Usuario";
    } else if (tipo == "R") {
      this.listarUsuarioPerfil(data.idusuario);
      this.tituloModalMantenimiento = "Visualizar Aplicaciones-Perfil por Usuario";
    }

    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event, data: any): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarAplicacionPerfilUsuario(data);
      },
      reject: () => {
        //reject action
      }
    });
  }

  loadData(event: any) {
    this.objbuscar.indice = event.first + 1;
    this.objbuscar.limite = event.rows;
    this.listarBandeja(this.objbuscar);

  }

  listarBandeja(obj: any) {
    var params = obj;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSeguridad.bandejaAplicacionPerfilUsuario(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaBandeja = result.data;
          this.totalRecords = result.totalRegistro;
        } else {
          this.listaBandeja = [];
          this.totalRecords = 0;
          this.mostrarToats("warn", result.message);
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.listaBandeja = [];
        this.totalRecords = 0;
        this.mostrarError("Ocurrió un error");
      }
    );

  }

  listarComboPerfilAplicacion() {
    this.serSeguridad.listaComboPerfilAplicacion({}).subscribe(
      (result: any) => {
        if (result.success) {
          this.lstComboPerfilAplicacion = result.data;
          this.lstComboPerfilAplicacionFiltro = [...result.data];
        } else {
          this.mostrarToats("warn", result.message);
        }
      },
      (error) => this.mostrarError("Ocurrió un error")
    );
  }

  listarUsuarioPerfil(idUser: any) {
    if (idUser !== 0) {
      this.lstDetalle = [];
      var params = { idusuario: idUser, codigoAplicacion: '' };

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serSeguridad.listarUsuarioPerfil(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.lstDetalle = result.data;
          } else {
            this.mostrarToats("warn", result.message);
          }
          this.dialog.closeAll();
        },
        (error) => this.mostrarError("Ocurrió un error")
      );
    } else {
      this.lstDetalle = [];
    }
  }

  AgregarDetalle() {
    let item = new aplicacionPerfilUsaurioModel();
    item.idusuario = this.dataFilaModal.idusuario;
    item.sysusuario = this.usuario.idusuario;
    this.lstDetalle.push(item);
  }

  registrarAplicacionPerfilUsuario() {
    // Validar que se seleccionen los perfiles de la tabla detalle
    for (const item of this.lstDetalle) {
      if (item.idperfilAplicacion === null || item.idperfilAplicacion === 0) {
        this.mostrarToats("warn", '(*) Debe seleccionar un perfil');
        return;
      }
    }

    let valido: boolean = true;
    //Validar que no se ingrese más de 1 vez el mismo perfil
    for(let i=0;i<this.lstDetalle.length; i++){
      let idPerfil = this.lstDetalle[i].idperfilAplicacion;
      for(let j=i+1;j<this.lstDetalle.length; j++){
        let _idPerfil = this.lstDetalle[j].idperfilAplicacion;
        if(idPerfil == _idPerfil){
          valido = false;
          break;
        }
      }

      if(!valido) break;
    }
    if(!valido){
      this.mostrarToats("warn", 'No se puede ingresar el mismo perfil varias veces.');
      return;
    }

    let params = this.lstDetalle;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSeguridad.registrarAplicacionPerfilUsuario(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.mostrarToats("success", result.message);
          this.listarUsuarioPerfil(this.dataFilaModal.idusuario);
        } else {
          this.mostrarToats("warn", result.message);
        }
        this.dialog.closeAll();
      },
      (error) => this.mostrarError("Ocurrió un error")
    );
  }

  private eliminarAplicacionPerfilUsuario(data: aplicacionPerfilUsaurioModel) {
    if (data.idperfilAplicacionUsuario === 0) {
      this.eliminarIndexTablaDetalle(data);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      let params = data;
      this.serSeguridad.eliminarAplicacionPerfilUsuario(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.mostrarToats('success', result.message);
            this.eliminarIndexTablaDetalle(data);
          } else {
            this.mostrarToats("warn", result.message)
          }
          this.dialog.closeAll();
        },
        (error) => this.mostrarError("Ocurrió un error")
      );
    }
  }

  eliminarIndexTablaDetalle(data: any) {
    const index = this.lstDetalle.indexOf(data, 0);
    this.lstDetalle.splice(index, 1);
  }

  cerrarModal(){
    this.verModalMantenimiento = false;
    this.listarBandeja(this.objbuscar);
  }

  mostrarToats(tipo: string, mensaje: string) {
    this.messageService.add({ severity: tipo, summary: "", detail: mensaje });
  }

  mostrarError(errorIn: string) {
    this.mostrarToats("warn", errorIn);
    this.dialog.closeAll();
  }

  btnLimpiar() {
    this.objbuscar = {
      "dato": "",
      "codigoAplicacion": "MC",
      "idperfilAplicacion": 0,
      "indice": 1,
      "limite": 10
    };

    this.listarBandeja(this.objbuscar);
  }

  constructor(
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private serSeguridad: SeguridadService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarComboPerfilAplicacion();
    this.listarBandeja(this.objbuscar);
  }

}




