import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { OpcionModel } from 'src/app/model/seguridad/opcion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { UtilitariosService } from 'src/app/model/util/util.service';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-mantenimiento-opciones',
  templateUrl: './mantenimiento-opciones.component.html',
  styleUrls: ['./mantenimiento-opciones.component.scss'],
  providers: [MessageService]
})
export class MantenimientoOpcionesComponent implements OnInit {

  MantenedorFormulario: FormGroup;

  usuario = {} as UsuarioModel;
  estados: ComboModel[] = DataCombos.EstadosMaestas;
  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accionMantenimiento: string = "";
  disableOpcionPadre: boolean = true;
  totalRecords: number = 0;

  listaComboOpcion: any;
  listaComboAplicacion: any;
  listaBandejaOpcion: OpcionModel[] = [];
  opcionContext: OpcionModel = new OpcionModel();

  filteredOpciones: any[] = [];

  objbuscar = {
    "dato": "",
    "idestado": 1,
    "indice": 1,
    "limite": 10
  };

  openModalMantenimiento(tipo: string, data: any): void {
    this.accionMantenimiento = tipo;
    if (tipo == "C") {
      this.MantenedorFormulario.reset();
      this.listaComboOpcion = [];
      this.opcionContext = new OpcionModel();
      this.MantenedorFormulario.patchValue(this.opcionContext);
      this.tituloModalMantenimiento = "Registrar Opción";
    } else if (tipo == "E") {
      this.listaComboOpcion = [];
      this.listarOcpcionPadre({ value: data.idaplicacion });
      this.disableOpcionPadre = data.gerarquia == 1;
      this.tituloModalMantenimiento = "Editar Opción";
      this.ObtenerOpcion(data);
    } else if (tipo == "R") {
      this.MantenedorFormulario.reset();
      this.listaComboOpcion = [];
      this.listarOcpcionPadre({ value: data.idaplicacion });
      this.opcionContext = new OpcionModel(data);
      this.MantenedorFormulario.patchValue(this.opcionContext);
      this.tituloModalMantenimiento = "Visualizar Opción";
    }

    this.verModalMantenimiento = true;
  }

  selecinarJeranquia(g: string) {
    if (g == 'M') {
      this.disableOpcionPadre = true;
    } else {
      this.disableOpcionPadre = false;
    }
  }

  openEliminarRegistro(event: Event, opcion: OpcionModel): void {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        this.EliminarOpcion(opcion);
      },
      reject: () => {
      }
    });
  }


  constructor(private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private serOpcion: SeguridadService
    , private fb: FormBuilder
    , private utilSev: UtilitariosService,
    private dialog: MatDialog) {

    this.MantenedorFormulario = this.fb.group({
      idopcion: [0],
      idaplicacion: new FormControl('idaplicacion', Validators.compose([ValidatorsExtend.comboRequired()])),
      codigoOpcion: [""],//new FormControl('codigoOpcion',Validators.compose([Validators.required])),
      gerarquia: new FormControl('gerarquia'),
      idopcionPadre: new FormControl('idopcionPadre'),
      opcion: new FormControl('', Validators.compose([Validators.required])),
      orden: new FormControl(''),
      estado: new FormControl('estado'),
      descripcion: new FormControl('', Validators.compose([Validators.required])),
      url: new FormControl(''),
      icono: new FormControl('')
    });

    this.MantenedorFormulario.get('gerarquia')?.valueChanges.subscribe(value => {
      if (value == 1) {
        this.MantenedorFormulario.get('idopcionPadre')?.clearValidators();
        this.MantenedorFormulario.controls['idopcionPadre'].setValue(0);

        this.listaComboOpcion = [{
          text: "--Seleccione--",
          value: 0
        }];
      }
      else {
        if (value) {
          this.MantenedorFormulario.get('idopcionPadre')?.setValidators(ValidatorsExtend.comboRequired());
          if (this.accionMantenimiento == 'C') {
            this.MantenedorFormulario.controls['idopcionPadre'].setValue(0);
          }


        }

      }

    })

  }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarBandeja(this.objbuscar,false);
    this.listarCbAplicacion();
  }

  filterOpciones(event: any) {

    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.listaComboOpcion.length; i++) {
      let item = this.listaComboOpcion[i];
      if (item.text.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(item);
      }
    }

    this.filteredOpciones = filtered;
  }

  loadData(event: any) {
    this.objbuscar.indice = event.first + 1;
    this.objbuscar.limite = event.rows;
    this.listarBandeja(this.objbuscar,false);
  }

  listarBandeja(obj: any, reload: boolean = false) {
    var params = obj;
    if(reload)this.listaBandejaOpcion = [];
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serOpcion.bandejaOpcion(params).subscribe(
      (result: any) => {

        if (result.success) {
          this.listaBandejaOpcion = result.data;
          this.totalRecords = result.totalRegistro;
        } else {
          
          if(!reload) this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  ObtenerOpcion(data: OpcionModel) {
    var params = {
      "idopcion": data.idopcion
    }

    this.serOpcion.obtenerOpcion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.MantenedorFormulario.reset();
          this.opcionContext = new OpcionModel(result.data[0]);
          this.MantenedorFormulario.patchValue(this.opcionContext);
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
      },
      (error) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  registrarOpcion(data: OpcionModel) {
    this.utilSev.markFormGroupTouched(this.MantenedorFormulario);
    if (this.MantenedorFormulario.valid) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      var params = this.MantenedorFormulario.value;
      params.idUsuarioRegistro = this.usuario.idusuario;
      this.serOpcion.registrarOpcion(params).subscribe(
        (result: any) => {

          if (result.success) {
            this.messageService.add({ severity: "success", summary: "", detail: result.message });
            this.listarBandeja(this.objbuscar,true);
            this.verModalMantenimiento = false;
          } else {
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          
          this.messageService.add({ severity: "warn", summary: "", detail: error });
        }
      );
    }

  }


  EliminarOpcion(data: OpcionModel) {
    var params = data;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serOpcion.eliminarOpcion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          this.listarBandeja(this.objbuscar,true);
          this.verModalMantenimiento = false;
        } else {
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  listarCbAplicacion() {
    var params = {};

    this.serOpcion.listaComboAplicacion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaComboAplicacion = result.data;
        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  listarOcpcionPadre(e: any) {
    var params = {
      idaplicacion: e.value
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.listaComboOpcion = [];
    this.serOpcion.listaComboOpcion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaComboOpcion = result.data;
        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  btnLimpiar(){
    this.objbuscar = {
      "dato": "",
      "idestado": 1,
      "indice": 1,
      "limite": 10
    };
    
    this.listarBandeja(this.objbuscar,false);
  }

}


