import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SeguridadService } from 'src/app/service/seguridad.service';

@Component({
  selector: 'app-proceso-permiso-perfil',
  templateUrl: './proceso-permiso-perfil.component.html',
  styleUrls: ['./proceso-permiso-perfil.component.scss']
})
export class ProcesoPermisoPerfilComponent implements OnInit {
  usuario!: UsuarioModel;
  comboPerfil: any[] = [];
  objbuscar: { idPerfil: number | null, opcion: string | null, idPerfilOpcionAccion: number | null } = { idPerfil: null, opcion: null, idPerfilOpcionAccion: null }
  listaBandeja: any[] = [];

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private seguridadService: SeguridadService,
  ) {
    this.usuario = this.usuarioServ.usuario;
  }

  ngOnInit(): void {
    this.listarComboPerfiles();
  }

  //SERVICIOS
  listarComboPerfiles() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.seguridadService.listaComboPerfilAplicacion({}).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success) {
        this.comboPerfil = resp.data.filter((x: any) => x.value !== 0);
        this.objbuscar = { idPerfil: this.comboPerfil[0].value, opcion: null, idPerfilOpcionAccion: null };
        this.listarBandeja();
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  codPermisos: any = {
    1: "reg",
    2: "edit",
    3: "ver",
    4: "eli",
    5: "otro"
  }
  obtenerPermisos() {
    let auxLista = this.listaBandeja.filter(x => x.idOpcion === 17).map(y => {
      let aux = { accion: y.accion, permitir: y.permitir, idAccion: y.idAccion, codPermiso: this.codPermisos[y.idAccion] }
      return aux;
    });
    let auxPermisos: any = {};
    for (const item of auxLista) {
      auxPermisos[item.codPermiso] = item.permitir
    }


  }

  listarBandeja() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.seguridadService.perfilOpcionAccionListar(this.objbuscar).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaBandeja = resp.data.map((x: any) => { return { ...x, isChange: false } });
        this.obtenerPermisos();
      } else {
        this.listaBandeja = [];
      }
    }, () => {
      this.listaBandeja = [];
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  registrarPermisos(lista: any) {
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.seguridadService.perfilOpcionAccionRegistrar(lista).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarBandeja();
      } else {
        this.toast.warn(resp.message);
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error(Mensajes.MSJ_ERROR_CATCH);
    });
  }

  //BOTNES
  btnLimpiar() {
    this.objbuscar = { idPerfil: this.comboPerfil[0].value, opcion: null, idPerfilOpcionAccion: null };
    this.listarBandeja();
  }

  btnBuscar() {
    this.listarBandeja();
  }

  btnGuardar() {
    let aux = this.listaBandeja.filter(x => x.isChange);
    if (aux.length > 0) {
      this.registrarPermisos(aux);
    } else {
      this.toast.warn("No hay cambios por registrar.");
    }
  }

  //FUNCIONES

}
