import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { Paginator } from 'primeng/paginator';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { OpcionAccionAppPerfilModel } from 'src/app/model/seguridad/opcionAccionAplicacionPerfil';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';

@Component({
  selector: 'app-proceso-accion-opcion-aplicacion-perfil',
  templateUrl: './proceso-accion-opcion-aplicacion-perfil.component.html',
  styleUrls: ['./proceso-accion-opcion-aplicacion-perfil.component.scss'],
  providers: [MessageService]
})
export class ProcesoAccionOpcionAplicacionPerfilComponent implements OnInit {

  @ViewChild('paginator', { static: true }) paginator: Paginator | undefined;


  usuario = {} as UsuarioModel;
  estados: ComboModel[] = DataCombos.EstadosMaestas;
  estadosDetalle: ComboModel[] = DataCombos.EstadosMaestas2;


  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accionMantenimiento: string = "";

  totalRecords: number = 0;
  listaBandeja: any[] = [];

  perfilAplicacionContext: number = 0;
  lstComboOpcionAccion: any[] = [];
  lstComboPerfilAplicacion: any[] = [];
  lstDetalle: any[] = [];

  objbuscar = {
    "dato": "",
    "idestado": "1",
    "indice": 1,
    "limite": 10
  };

  openModalMantenimiento(data: any, tipo: string): void {
    this.accionMantenimiento = tipo;
    this.lstDetalle = [];
    if (tipo == "C") {
      this.perfilAplicacionContext = 0;
      this.tituloModalMantenimiento = "Registrar Aplicaciones-Perfil por Opción-Acción";
    } else if (tipo == "E") {
      this.perfilAplicacionContext = data.idperfilAplicacion;
      this.obtenerOpcionAccionAplicacionPerfil({ value: this.perfilAplicacionContext });
      this.tituloModalMantenimiento = "Editar Aplicaciones-Perfil por Opción-Acción";
    } else if (tipo == "R") {
      this.perfilAplicacionContext = data.idperfilAplicacion;
      this.obtenerOpcionAccionAplicacionPerfil({ value: this.perfilAplicacionContext });
      this.tituloModalMantenimiento = "Visualizar Aplicaciones-Perfil por Opción-Acción";
    }

    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event, data: any, tipo: string): void {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarOpcionAccionAplicacionPerfil(data, tipo);
      },
      reject: () => {
      }
    });
  }

  loadData(event: any) {
    this.objbuscar.indice = event.first + 1;
    this.objbuscar.limite = event.rows;
    this.listarBandeja(this.objbuscar);
  }

  listarBandeja(obj: any) {
    var params = obj;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSeguridad.bandejaOpcionAccionAplicacionPerfil(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaBandeja = result.data;
          this.totalRecords = result.totalRegistro;

        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        
        this.dialog.closeAll();
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  listarComboPerfilAplicacion() {
    this.serSeguridad.listaComboPerfilAplicacion({}).subscribe(
      (result: any) => {
        if (result.success) {
          this.lstComboPerfilAplicacion = result.data;

        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {

        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  listarComboOpcionAccion() {
    this.serSeguridad.listaComboOpcionAccion({}).subscribe(
      (result: any) => {
        if (result.success) {
          this.lstComboOpcionAccion = result.data;

        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {

        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  obtenerOpcionAccionAplicacionPerfil(e: any) {
    if (e.value > 0) {
      this.lstDetalle = [];
      var params = { idperfilAplicacion: e.value };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serSeguridad.obtenerOpcionAccionAplicacionPerfil(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.lstDetalle = result.data;

          } else {
            
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          
          this.messageService.add({ severity: "warn", summary: "", detail: error });
        }
      );
    } else {
      this.lstDetalle = [];
    }
  }

  AgregarDetalle() {
    let item = new OpcionAccionAppPerfilModel();
    item.local = true;
    item.idperfilAplicacion = this.perfilAplicacionContext;
    item.sysusuario = this.usuario.idusuario;
    this.lstDetalle.unshift(item);
  }

  registrarOpcionAccionAplicacionPerfil() {
    let params = this.lstDetalle;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSeguridad.registrarOpcionAccionAplicacionPerfil(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.messageService.add({ severity: "success", summary: "", detail: result.message });
          this.listarBandeja(this.objbuscar);
          this.obtenerOpcionAccionAplicacionPerfil({ value: this.perfilAplicacionContext });
        } else {
          
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  private eliminarOpcionAccionAplicacionPerfil(data: OpcionAccionAppPerfilModel, tipo: string) {
    if (data.local && tipo == 'D') {
      const index = this.lstDetalle.indexOf(data, 0);
      this.lstDetalle.splice(index, 1);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      let params = data;
      this.serSeguridad.eliminarOpcionAccionAplicacionPerfil(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.messageService.add({ severity: "success", summary: "", detail: result.message });
            const index = this.lstDetalle.indexOf(data, 0);
            this.lstDetalle.splice(index, 1);

            if (tipo == 'D') {
              // this.obtenerOpcionAccionAplicacionPerfil({value : this.perfilAplicacionContext});
            } else {
              this.paginator?.changePage(0);
              this.listarBandeja(this.objbuscar);
            }


          } else {
            
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          
          this.messageService.add({ severity: "warn", summary: "", detail: error });
        }
      );
    }
  }

  btnLimpiar() {
    this.objbuscar = {
      "dato": "",
      "idestado": "1",
      "indice": 1,
      "limite": 10
    };

    this.listarBandeja(this.objbuscar);
  }

  constructor(private confirmationService: ConfirmationService
    , private messageService: MessageService
    , private serSeguridad: SeguridadService
    , private dialog: MatDialog) { }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarComboPerfilAplicacion();
    this.listarComboOpcionAccion();
    this.listarBandeja(this.objbuscar);

  }

}
