import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { AccionOpcionModel } from 'src/app/model/seguridad/acccionOpcion';
import { ComboModel, DataCombos } from 'src/app/model/util/Combo';
import { SeguridadService } from 'src/app/service/seguridad.service';
import { Paginator } from 'primeng/paginator';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';



@Component({
  selector: 'app-proceso-accion-opcion',
  templateUrl: './proceso-accion-opcion.component.html',
  styleUrls: ['./proceso-accion-opcion.component.scss']
})
export class ProcesoAccionOpcionComponent implements OnInit {

  @ViewChild('paginator', { static: true }) paginator: Paginator | undefined;

  usuario = {} as UsuarioModel;
  estados: ComboModel[] = DataCombos.EstadosMaestas;
  estadosDetalle: ComboModel[] = DataCombos.EstadosMaestas2;

  verModalMantenimiento: boolean = false;
  tituloModalMantenimiento: string = "";
  accionMantenimiento: string = "";

  totalRecords: number = 0;
  listaBandeja: AccionOpcionModel[] = [];

  opcionContext: number = 0;
  lstCoomboOpciones: any[] = [];
  lstComboAcciones: any[] = [];
  lstaDetalleOpcionAccion: AccionOpcionModel[] = [];

  objbuscar = {
    "dato": "",
    "idestado": '1',
    "indice": 1,
    "limite": 10
  };


  openModalMantenimiento(data: any, tipo: string): void {
    this.accionMantenimiento = tipo;
    this.lstaDetalleOpcionAccion = [];

    if (tipo == "C") {
      this.opcionContext = 0;
      this.tituloModalMantenimiento = "Registrar Acciones por Opción";
    } else if (tipo == "E") {
      this.opcionContext = data.idopcion;
      this.obtenerOpcionAccion({ value: this.opcionContext });
      this.tituloModalMantenimiento = "Editar Acciones por Opción";
    } else if (tipo == "R") {
      this.opcionContext = data.idopcion;
      this.obtenerOpcionAccion({ value: this.opcionContext });
      this.tituloModalMantenimiento = "Visualizar Acciones por Opción";
    }

    this.verModalMantenimiento = true;
  }

  openEliminarRegistro(event: Event, data: any, tipo: string): void {

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de querer eliminar esté registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.removerAccionOpcion(data, tipo);
      },
      reject: () => {

      }
    });
  }

  loadData(event: any) {
    this.objbuscar.indice = event.first + 1;
    this.objbuscar.limite = event.rows;
    this.listarBandeja(this.objbuscar);
  }

  listarBandeja(obj: any) {
    var params = obj;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serSeguridad.bandejaOpcionAccion(params).subscribe(
      (result: any) => {
        if (result.success) {
          this.listaBandeja = result.data;
          this.totalRecords = result.totalRegistro;

        } else {
         
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }
        this.dialog.closeAll();
      },
      (error) => {
        this.dialog.closeAll();
        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );

  }

  listarComboOpciones() {
    this.serSeguridad.listaComboOpcion({ idaplicacion: -1 }).subscribe(
      (result: any) => {
        if (result.success) {
          this.lstCoomboOpciones = result.data;

        } else {
         
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {

        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }

  listarComboAcciones() {
    this.serSeguridad.listaComboAccion({}).subscribe(
      (result: any) => {
        if (result.success) {
          this.lstComboAcciones = result.data;

        } else {
         
          this.messageService.add({ severity: "warn", summary: "", detail: result.message });
        }

      },
      (error) => {

        
        this.messageService.add({ severity: "warn", summary: "", detail: error });
      }
    );
  }



  obtenerOpcionAccion(e: any): void {

    if (e.value > 0) {
      this.lstaDetalleOpcionAccion = [];
      var params = { idopcion: e.value };
      this.opcionContext = e.value;
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serSeguridad.obtenerOpcionAccion(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.lstaDetalleOpcionAccion = result.data;

          } else {
           
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          
          this.messageService.add({ severity: "warn", summary: "", detail: error });
        }
      );
    } else {
      this.lstaDetalleOpcionAccion = [];
    }


  }

  agregarNuevoOpcionAccion(): void {
    let item = new AccionOpcionModel();
    item.local = true;
    item.idopcion = this.opcionContext;
    item.sysusuario = this.usuario.idusuario;
    this.lstaDetalleOpcionAccion.push(item);
  }

  guardarOpcionAccion() {
    let existeReg=false;
    let duplicado: number[]=[];
    this.lstaDetalleOpcionAccion.map((item: any) => {
      let i=item.idaccion;
        if(duplicado[i]==null){
          duplicado[i]=0;
        }

        duplicado[i]=++duplicado[i];
      
    });

    for(let elemento of duplicado){
      if(elemento>1){
        existeReg=true;
      }
    }


    if(!existeReg){
          let params = this.lstaDetalleOpcionAccion;

          this.dialog.open(LoadingComponent, { disableClose: true });
          this.serSeguridad.registrarOpcionAccion(params).subscribe(
            (result: any) => {
              if (result.success) {
                this.messageService.add({ severity: "success", summary: "", detail: result.message });
                this.obtenerOpcionAccion({ value: this.opcionContext });
              } else {
               
                this.messageService.add({ severity: "warn", summary: "", detail: result.message });
              }
              this.dialog.closeAll();
            },
            (error) => {
              this.dialog.closeAll();
              
              this.messageService.add({ severity: "warn", summary: "", detail: error });
            }
          );

        }else{
      this.messageService.add({ severity: "warn", summary: "", detail: "Existe acciones duplicadas" });
    }
  }


  private removerAccionOpcion(data: AccionOpcionModel, tipo: string): void {

    if (data.local && tipo == 'D') {
      const index = this.lstaDetalleOpcionAccion.indexOf(data, 0);
      this.lstaDetalleOpcionAccion.splice(index, 1);
    } else {
      this.dialog.open(LoadingComponent, { disableClose: true });
      let params = data;
      this.serSeguridad.eliminarOpcionAccion(params).subscribe(
        (result: any) => {
          if (result.success) {
            this.messageService.add({ severity: "success", summary: "", detail: result.message });
            if (tipo == 'D') {
              this.obtenerOpcionAccion({ value: this.opcionContext });
            } else {
              this.paginator?.changePage(0);
              this.listarBandeja(this.objbuscar);
            }


          } else {
           
            this.messageService.add({ severity: "warn", summary: "", detail: result.message });
          }
          this.dialog.closeAll();
        },
        (error) => {
          this.dialog.closeAll();
          
          this.messageService.add({ severity: "warn", summary: "", detail: error });
        }
      );

    }
  }

  btnLimpiar() {
    this.objbuscar = {
      "dato": "",
      "idestado": '1',
      "indice": 1,
      "limite": 10
    };

    this.listarBandeja(this.objbuscar);
  }

  constructor(private confirmationService: ConfirmationService
    , private messageService: MessageService
    , private serSeguridad: SeguridadService
    , private dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.usuario = JSON.parse('' + localStorage.getItem("usuario")?.toString());
    this.listarComboOpciones();
    this.listarComboAcciones();
    this.listarBandeja(this.objbuscar);
  }

}

