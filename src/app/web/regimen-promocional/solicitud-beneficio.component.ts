import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-solicitud-beneficio',
  templateUrl: './solicitud-beneficio.component.html',
  styleUrls: ['./solicitud-beneficio.component.scss']
})
export class SolicitudBeneficioComponent implements OnInit {
  numeroTHSelect!: String;
  mostrarModalBuscarTH: boolean= false; 
  codigoTituloHabilitante: String='';

  lstNumeroTH = [
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    }
  ]



  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.listarnumeroTH();
  }

  listarnumeroTH(){
    console.log("se listó el N° de TH");


  }


  nuevoBeneficio(){
    console.log("se creó un nuevo beneficio")
  }

  limpiarFiltro(){
    this.numeroTHSelect = '';
    this.listarnumeroTH();
  }

  abrirModalBuscarTH(){
    this. mostrarModalBuscarTH=true;
    
  }
  seleccionadoTH(data: any){
    
    this.codigoTituloHabilitante=data;
    this. mostrarModalBuscarTH=false;
  }
  closeModalTH(){
    this. mostrarModalBuscarTH=false;
  }
}
