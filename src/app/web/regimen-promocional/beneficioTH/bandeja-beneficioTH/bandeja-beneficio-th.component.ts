import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bandeja-beneficio-th',
  templateUrl: './bandeja-beneficio-th.component.html',
  styleUrls: ['./bandeja-beneficio-th.component.scss']
})
export class BandejaBeneficioTHComponent implements OnInit {
  numeroTHSelect!: String;
  mostrarModalBuscarTH: boolean= false; 
  codigoTituloHabilitante: String='';

  lstNumeroTH = [
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    },
    {
      idTH: '4589',
      titularTH: 'prueba',
      idAprovechamiento: 'prueba',
      descripcion: 'prueba',
      porcentaje: 'prueba',
      numeroResolucion: 'prueba',
      vigenciaBeneficio: 'prueba',
      fechaInicio: 'prueba',
      fechaFin: 'prueba',
      estado: 'prueba'
    }
  ]



  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.listarnumeroTH();
  }

  listarnumeroTH(){
  }


  nuevoBeneficio(){
    
  }

  limpiarFiltro(){
    this.numeroTHSelect = '';
    this.listarnumeroTH();
  }

  abrirModalBuscarTH(){
    this. mostrarModalBuscarTH=true;
    
  }
  seleccionadoTH(data: any){
    
    this.codigoTituloHabilitante=data;
    this. mostrarModalBuscarTH=false;
  }
  closeModalTH(){
    this. mostrarModalBuscarTH=false;
  }

}
