import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoLO } from 'src/app/model/aprovechamiento/CodigoEstadoLO';
import { IPermisoOpcion } from 'src/app/model/Comun/IPermisoOpcion';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { LibroOperacionesService } from 'src/app/service/aprovechamiento/libro-operaciones.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ModalSolicitarNumeroLoComponent } from '../components/modal-solicitar-numero-lo/modal-solicitar-numero-lo.component';

@Component({
  selector: 'app-bandeja-libro-operaciones',
  templateUrl: './bandeja-libro-operaciones.component.html',
  styleUrls: ['./bandeja-libro-operaciones.component.scss']
})
export class BandejaLibroOperacionesComponent implements OnInit {
  usuario!: UsuarioModel;
  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  codEstLO = CodigoEstadoLO;

  objbuscar = {
    idLibroOperaciones: null,
    nombreSolicitanteUnico: null,
    numeroDocumento: null,
    estadoLO: null,
    pageNum: 1,
    pageSize: 10
  };
  totalRecords: number = 0;

  comboEstado: any[] = [];
  listaBandeja: any[] = [];

  isPerTitular: boolean = false;
  isPerArffs: boolean = false;

  constructor(
    private activaRoute: ActivatedRoute,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioServ: UsuarioService,
    private genericoService: GenericoService,
    private libroOperacionesService: LibroOperacionesService,
  ) {
    this.usuario = this.usuarioServ.usuario;
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
    this.isPerTitular = this.usuario.sirperfil === Perfiles.TITULARTH;
    this.isPerArffs = this.usuario.sirperfil === Perfiles.AUTORIDAD_REGIONAL;
  }

  ngOnInit(): void {
    this.listarComboEstados();
    this.listarBandeja();
  }

  //SERVICIOS
  listarComboEstados() {
    this.genericoService.listarPorFiltroParametro({ prefijo: "ELIBOP" }).subscribe((resp: any) => {
      if (resp.success && resp.data) this.comboEstado = resp.data;
    });
  }

  listarBandeja() {
    this.listaBandeja = [];
    this.totalRecords = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.listarLibroOperaciones(this.objbuscar).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.listaBandeja = resp.data || [];
        this.totalRecords = resp.totalRecord || 0;
        if (this.listaBandeja?.length === 0) this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnBuscar() {
    this.objbuscar.pageNum = 1;
    this.objbuscar.pageSize = 10;
    this.listarBandeja();
  }

  btnLimpiar() {
    this.objbuscar = {
      idLibroOperaciones: null,
      nombreSolicitanteUnico: null,
      numeroDocumento: null,
      estadoLO: null,
      pageNum: 1,
      pageSize: 10
    };
    this.listarBandeja();
  }

  btnModal(datos: any, isNew: boolean) {
    const respRef = this.dialogService.open(ModalSolicitarNumeroLoComponent, {
      header: 'Solicitar Número de LO',
      width: '650px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {
        datos: datos, isNew: isNew, idUser: this.usuario.idusuario, perfil: this.usuario.sirperfil
      }
    });

    respRef.onClose.subscribe((resp: any) => {
      if (resp) this.listarBandeja();
    });
  }

  //FUNCIONES
  loadData(event: any) {
    this.objbuscar.pageNum = event.first + 1;
    this.objbuscar.pageSize = event.rows;
    this.listarBandeja();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
