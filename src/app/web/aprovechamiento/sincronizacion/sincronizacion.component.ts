import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';
import { ModalNuevaSincronizacionComponent } from '../components/modal-nueva-sincronizacion/modal-nueva-sincronizacion.component';
import {UsuarioService} from '@services';
import { HistoricoGuiaService } from 'src/app/service/historico-guia.service';

@Component({
  selector: 'app-sincronizacion',
  templateUrl: './sincronizacion.component.html',
  styleUrls: ['./sincronizacion.component.scss']
})
export class SincronizacionComponent implements OnInit {
  requestFiltro: {
    fechaInicial: any;
    fechaFinal: any;
    idSincronizacionHistorico: null | number;
    idArchivoBackup: null | number;
    idUsuarioCarga: null | number;
    pageNum: number;
    pageSize: number;
  } = {
      fechaInicial: null, fechaFinal: null,
      idSincronizacionHistorico: null, idArchivoBackup: null, idUsuarioCarga: null,
      pageNum: 1, pageSize: 10
    };

  listaBandeja: any[] = [];
  totalRecords: number = 0;

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private sincronizacionService: SincronizacionService,
    private historicoGuiaService: HistoricoGuiaService,

  ) { }

  ngOnInit(): void {
    //console.log("USUARIO ", this.usuarioService.usuario);
    this.listarBandeja();
  }

  //SERVICIOS
  listarBandeja() {
    /*this.listaBandeja = [
      {id: 1, nombre: "prueba1", fecha: "01/01/2022"},
      {id: 2, nombre: "prueba2", fecha: "01/02/2022"},
    ];*/
    this.totalRecords = this.listaBandeja.length;
     this.listaBandeja = [];
     this.totalRecords = 0;
     this.dialog.open(LoadingComponent, { disableClose: true });
     this.sincronizacionService.listarSincronizacionHistorico(this.requestFiltro).subscribe(resp => {
       this.dialog.closeAll();
       if (resp.success && resp.data) {
         this.listaBandeja = resp.data;
         this.totalRecords = resp.totalRecords || 0;
       }
     }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnDetalle(fila: any) {
    
    this.router.navigate(["/aprovechamiento/detalle-sincronizacion", fila.idSincronizacionHistorico])
  }

  btnBuscar() {
    this.requestFiltro.pageNum = 1;
    this.requestFiltro.pageSize = 10;
    this.listarBandeja();
  }

  btnLimpiar() {
    this.requestFiltro = {
      fechaInicial: null, fechaFinal: null,
      idSincronizacionHistorico: null, idArchivoBackup: null, idUsuarioCarga: null,
      pageNum: 1, pageSize: 10
    }
    this.listarBandeja();
  }

  btnNuevo() {
    const respRef = this.dialogService.open(ModalNuevaSincronizacionComponent, {
      header: 'Nueva Sincronización',
      width: '550px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {},
    });

    respRef.onClose.subscribe((resp: any) => {
      this.btnBuscar();
    });
  }


  btnSincronizar() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.historicoGuiaService.sincronizarHistoricoGuia().subscribe(resp => {
      this.dialog.closeAll();
      if (resp) {
        this.toast.ok('Se realizó el proceso correctamente');
      }
    }, (error) => this.errorMensaje(error, true));

  }

  //FUNCIONES
  loadData(event: any) {

    

    this.requestFiltro.pageNum = event.pageCount;
    this.requestFiltro.pageSize = event.rows;
    this.listarBandeja();

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
