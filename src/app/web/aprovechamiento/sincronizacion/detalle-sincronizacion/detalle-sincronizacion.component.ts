import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle-sincronizacion',
  templateUrl: './detalle-sincronizacion.component.html',
  styleUrls: ['./detalle-sincronizacion.component.scss']
})
export class DetalleSincronizacionComponent implements OnInit {
  tabIndex: number = 0;
  urlBandeja = "/aprovechamiento/sincronizacion";
  idSinc: number = 0;

  constructor(
    private router: Router,
    private activaRoute: ActivatedRoute,
  ) {
    this.idSinc = this.activaRoute.snapshot.paramMap.get('idParam') ? Number(this.activaRoute.snapshot.paramMap.get('idParam')) : 0;
    if (!this.idSinc) this.router.navigate([this.urlBandeja]);
  }

  ngOnInit(): void {
  }

  //BOTONES
  tabSiguiente(): void {
    this.tabIndex = this.tabIndex + 1;
  }

  tabAnterior(): void {
    this.tabIndex = this.tabIndex - 1;
  }

  //FUNCIONES
  tabChange(event: any) {
    this.tabIndex = event;
  }

}
