import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';
import { ModalDetalleTrozadoComponent } from '../../../components/modal-detalle-trozado/modal-detalle-trozado.component';

@Component({
  selector: 'tab-trozado',
  templateUrl: './tab-trozado.component.html',
  styleUrls: ['./tab-trozado.component.scss']
})
export class TabTrozadoComponent implements OnInit {
  @Input() idSincronizacion: number = 0;

  requestTabla : {
    idPlanManejo: number | null; 
    idSincronizacion: number | null;
    tipoConsulta: string;
    pageNum: number;
    pageSize: number;
  } = {
    idPlanManejo: null, idSincronizacion: null,
    tipoConsulta: "TROZADO", pageNum: 1, pageSize: 10
  };

  comboPlanManejo: any[] = [];
  listaTrozado: any[] = [];
  totalRecordsTrozado: number = 0;

  constructor(
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { }

  ngOnInit(): void {
    this.requestTabla.idSincronizacion = this.idSincronizacion;
    this.listarPlanManejo();
    this.listarTrozado();
  }

  //SERVICIOS
  listarPlanManejo() {
    const params = { idPlanManejo: null, idSincronizacion:null,tipoConsulta: "TROZADO"};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarPlanTrozado(params).subscribe(resp => {
      this.dialog.closeAll();
      
      if (resp.success && resp.data) {
        this.comboPlanManejo = resp.data;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarTrozado() {
    this.listaTrozado = [];
    this.totalRecordsTrozado = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarTrozado(this.requestTabla).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.listaTrozado = resp.data;
        this.totalRecordsTrozado = resp.totalrecord || 0;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnDetalle(fila: any) {
    const respRef = this.dialogService.open(ModalDetalleTrozadoComponent, {
      header: 'Detalle de Trozado',
      width: '80%',
      contentStyle: { overflow: 'auto' },
      data: { idTala: fila?.idTala, idSincronizacion: this.idSincronizacion },
    });

    respRef.onClose.subscribe((resp: any) => {

    });
  };

  //FUNCIONES
  changePlanManejo() {
    this.requestTabla.pageNum = 1;
    this.requestTabla.pageSize = 10;
    this.listarTrozado();
  }

  loadDataTrozado(event: any) {
    this.requestTabla.pageNum = event.first + 1;
    this.requestTabla.pageSize = event.rows;
    this.listarTrozado();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
