import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';
import { ModalDespachoProductoComponent } from '../../../components/modal-despacho-producto/modal-despacho-producto.component';
import { ModalDespachoTrozaComponent } from '../../../components/modal-despacho-troza/modal-despacho-troza.component';

@Component({
  selector: 'tab-guiaForestal',
  templateUrl: './tab-guiaForestal.component.html',
  styleUrls: ['./tab-guiaForestal.component.scss']
})
export class TabGuiaForestalComponent implements OnInit {
  @Input() idSincronizacion: number = 0;

  requestTabla: {
    
    idSincronizacion: number | null;
    pageNum: number;
    pageSize: number;
  } = {
     idSincronizacion: null, pageNum: 1, pageSize: 10
    };
    requestTabla1 : {
      idSincronizacion: number | null;
      pageNum: number;
      pageSize: number;
    } = {
       idSincronizacion: null, pageNum: 1, pageSize: 10
    };

  comboPlanManejo: any[] = [];
  listaTrozas: any[] = [];
  totalRecordsTroza: number = 0;
  listaProductos: any[] = [];
  totalRecordsProd: number = 0;

  constructor(
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { }

  ngOnInit(): void {
    this.requestTabla.idSincronizacion = this.idSincronizacion;
    this.requestTabla1.idSincronizacion = this.idSincronizacion;
    this.listarGuia();
    this.listarPlanManejo();
    this.listarGuiaPersona();
  }
  
  listarPlanManejo() {
    const params = { idPlanManejo: null , idSincronizacion:null,tipoConsulta: "TROZADO"};
   
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarPlanTrozado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.comboPlanManejo = resp.data;
      }
    }, (error) => this.toast.error(error?.error?.message || Mensajes.MSJ_ERROR_CATCH));
  }
  //SERVICIOS
  listarGuia() {
    this.listaTrozas = [];
    this.totalRecordsTroza = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarGuiaTransporte(this.requestTabla).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.listaTrozas = resp.data;
        
       /* this.listaTrozas.forEach((t:any) =>{ 
          if(t.idGuiaTransForestal === 0 || t.idGuiaTransForestal === null){
            t.idGuiaTransForestal = '-';
          }
        });*/
      }
    }, (error) => {this.toast.error(error?.error?.message || Mensajes.MSJ_ERROR_CATCH); this.dialog.closeAll();});
    this.totalRecordsTroza = this.listaTrozas.length;
  }

  listarGuiaPersona() {
    this.listaProductos = [];
    this.totalRecordsProd = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarGuiaTransportePersona(this.requestTabla1).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.listaProductos = resp.data;
        
        this.totalRecordsProd = this.listaProductos.length;
      }
    }, (error) => {this.toast.error(error?.error?.message || Mensajes.MSJ_ERROR_CATCH); this.dialog.closeAll();});
    this.totalRecordsProd = this.listaProductos.length;
  }

  //BOTONES
  btnDetalleTroza(fila: any) {
    const respRef = this.dialogService.open(ModalDespachoTrozaComponent, {
      header: 'Detalle de Lista de Troza',
      width: '80%',
      contentStyle: { overflow: 'auto' },
      data: {idNumeroListaTrozaDesktop: fila?.idNumeroListaTrozaDesktop, idSincronizacion: this.idSincronizacion},
    });

    respRef.onClose.subscribe((resp: any) => {

    });
  };

  btnDetalleProducto(fila: any) {
    const respRef = this.dialogService.open(ModalDespachoProductoComponent, {
      header: 'Detalle de Lista de Producto',
      width: '80%',
      contentStyle: { overflow: 'auto' },
      data: {idListaProductoTerminado: fila?.idListaProductoTerminado,idSincronizacion: this.idSincronizacion},
    });

    respRef.onClose.subscribe((resp: any) => {

    });
  };

  //FUNCIONES
  changePlanManejo() {
    this.requestTabla.pageNum = 1;
    this.requestTabla.pageSize = 10;
    this.listarGuia();
  }

  loadDataTroza(event: any) {
    this.requestTabla.pageNum = event.first + 1;
    this.requestTabla.pageSize = event.rows;
    this.listarGuia();
  }

  loadDataProd(event: any) {
    this.requestTabla.pageNum = event.first + 1;
    this.requestTabla.pageSize = event.rows;
    this.listarGuiaPersona();

  }
  changePlanManejo1() {
    this.requestTabla.pageNum = 1;
    this.requestTabla.pageSize = 10;
    this.listarGuiaPersona();
  }

}
