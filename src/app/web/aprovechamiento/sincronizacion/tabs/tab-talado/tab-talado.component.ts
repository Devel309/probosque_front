import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';
import { ModalDetalleTaladoComponent } from '../../../components/modal-detalle-talado/modal-detalle-talado.component';

@Component({
  selector: 'tab-talado',
  templateUrl: './tab-talado.component.html',
  styleUrls: ['./tab-talado.component.scss']
})
export class TabTaladoComponent implements OnInit {
  @Input() idSincronizacion: number = 0;

  requestTabla : {
    idPlanManejo: number | null; 
    idSincronizacion: number | null;
    tipoConsulta: string;
    pageNum: number;
    pageSize: number;
  } = {
    idPlanManejo: null, idSincronizacion: null,
    tipoConsulta: "TALA", pageNum: 1, pageSize: 10

  };

  comboPlanManejo: any[] = [];
  listaTalado: any[] = [];
  totalRecordsTalado: number = 0;

  constructor(
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { }

  ngOnInit(): void {
    this.requestTabla.idSincronizacion = this.idSincronizacion;
    this.listarPlanManejo();
    this.listarTalado();
  }

  //SERVICIOS
  listarPlanManejo() {
    const params = { idPlanManejo: null , idSincronizacion:null,tipoConsulta: "TALA"};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarPlanTrozado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.comboPlanManejo = resp.data;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarTalado() {
    // this.listaTalado = [
    //   { id: "prueba1" }, 
    //   { id: "prueba2" },
    //   { id: "prueba3" },
    //   { id: "prueba4" },
    // ];
    this.listaTalado = [];
    this.totalRecordsTalado = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarTrozado(this.requestTabla).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.listaTalado = resp.data;
        this.totalRecordsTalado = resp.totalrecord || 0;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnDetalle(fila: any) {
    const respRef = this.dialogService.open(ModalDetalleTaladoComponent, {
      header: 'Detalle de Talado',
      width: '80%',
      contentStyle: { overflow: 'auto' },
      data: { idTala: fila?.idTala, idSincronizacion: this.idSincronizacion },
    });

    respRef.onClose.subscribe((resp: any) => {

    });
  };

  //FUNCIONES
  changePlanManejo() {
    this.requestTabla.pageNum = 1;
    this.requestTabla.pageSize = 10;
    this.listarTalado();
  }

  loadDataTalado(event: any) {
    this.requestTabla.pageNum = event.first + 1;
    this.requestTabla.pageSize = event.rows;
    this.listarTalado();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
