import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DialogService } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';
import { ModalDetalleTransformacionComponent } from '../../../components/modal-detalle-transformacion/modal-detalle-transformacion.component';

@Component({
  selector: 'tab-transformacion',
  templateUrl: './tab-transformacion.component.html',
  styleUrls: ['./tab-transformacion.component.scss']
})
export class TabTransformacionComponent implements OnInit {
  @Input() idSincronizacion: number = 0;

  requestTabla : {
    idPlanManejo: number | null; 
    idSincronizacion: number | null;
    pageNum: number;
    pageSize: number;
  } = {
    idPlanManejo: null, idSincronizacion: null, pageNum: 1, pageSize: 10
  };

  comboPlanManejo: any[] = [];
  listaOrdenProd: any[] = [];
  totalRecordsOrdProd: number = 0;

  constructor(
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { }

  ngOnInit(): void {
    this.requestTabla.idSincronizacion = this.idSincronizacion;
    this.listarPlanManejo();
    //this.listarOrdenProd();
  }

  //SERVICIOS
  listarPlanManejo() {
    const params = { idPlanManejo: null, idSincronizacion:null,tipoConsulta: "TROZADO"};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarPlanTrozado(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data){
        this.comboPlanManejo = resp.data;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarOrdenProd() {
    this.listaOrdenProd = [];
    this.totalRecordsOrdProd = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    
    this.sincronizacionService.listarTransformacion(this.requestTabla).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.listaOrdenProd = resp.data;
        
        this.totalRecordsOrdProd = resp.totalrecord || 0;
      }
    }, (error) => this.errorMensaje(error, true));
    // this.dialog.open(LoadingComponent, { disableClose: true });
    // this.sincronizacionService.listarOrdenProduccion(params).subscribe(resp => {
    //   this.dialog.closeAll();
    //   if (resp.success && resp.data) {
    //     this.listaOrdenProd = resp.data;
    //     this.totalRecordsOrdProd = resp.totalRecord || 0;
    //   }
    // }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnDetalle(idOrdProd: number) {
    const respRef = this.dialogService.open(ModalDetalleTransformacionComponent, {
      header: 'Detalle de Transformación',
      width: '80%',
      contentStyle: { overflow: 'auto' },
      data: { idOrdProd: idOrdProd, idSincronizacion:this.idSincronizacion },
    });

    respRef.onClose.subscribe((resp: any) => {

    });
  };

  //FUNCIONES
  changePlanManejo() {
    this.requestTabla.pageNum = 1;
    this.requestTabla.pageSize = 10;
    this.listarOrdenProd();
  }

  loadDataOrdProd(event: any) {
    this.requestTabla.pageNum = event.first + 1;
    this.requestTabla.pageSize = event.rows;
    this.listarOrdenProd();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
