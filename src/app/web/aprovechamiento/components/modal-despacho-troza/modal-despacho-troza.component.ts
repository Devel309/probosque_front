import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';

@Component({
  selector: 'app-modal-despacho-troza',
  templateUrl: './modal-despacho-troza.component.html',
  styleUrls: ['./modal-despacho-troza.component.scss']
})
export class ModalDespachoTrozaComponent implements OnInit {
  detalleListaTrozas: any[] = [];
  totalRecordsDetTroza: number = 0;

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { }

  ngOnInit(): void {
    this.listarDetalleTrozas();
  }

  //SERVICIOS
  listarDetalleTrozas() {
    const params = {idSincronizacion: this.config.data?.idSincronizacion, idNumeroListaTrozaDesktop: this.config.data?.idNumeroListaTrozaDesktop, idPlanManejo: this.config.data?.idPlanManejo,pageNum: 1, pageSize:10};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarListaTrozaDetalle(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.detalleListaTrozas = resp.data;
        this.detalleListaTrozas.forEach((t:any) =>{
          if(t.unidadMedida === null){
            t.unidadMedida = '-';
          }
        });
        this.totalRecordsDetTroza = this.detalleListaTrozas.length;
      }
    }, (error) => {this.errorMensaje(error, true); this.dialog.closeAll();});
  }

  //BOTONES
  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  loadDataDetTroza(event: any) {

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
