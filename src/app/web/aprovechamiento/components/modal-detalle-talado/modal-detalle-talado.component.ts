import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';

@Component({
  selector: 'app-modal-detalle-talado',
  templateUrl: './modal-detalle-talado.component.html',
  styleUrls: ['./modal-detalle-talado.component.scss']
})
export class ModalDetalleTaladoComponent implements OnInit {
  listaFuste: any[] = [];
  totalRecordsFuste: number = 0;
  listaRama: any[] = [];
  totalRecordsRama: number = 0;
  idSincronizacion: number = 0;
  idTala:number = 0;

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { 
    this.idSincronizacion = this.config.data?.idSincronizacion;
    this.idTala =  this.config.data?.idTala;
  }

  ngOnInit(): void {
    this.listarTalaDetalle();
    //this.listarFuste();
    //this.listarRama();
  }

  //SERVICIOS
  listarTalaDetalle() {
    const params = {"idSincronizacion": this.idSincronizacion, "idTala": this.idTala,"pageNum": 1,
      "pageSize":10};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarTalaDetalle(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
          resp.data.forEach((t:any) => {
            if(t.tipoTalaDetalle === "1"){
              this.listaFuste.push(t);

            }else if(t.tipoTalaDetalle === "2"){
              this.listaRama.push(t);
            }
          });
      }
    }, (error) => {this.errorMensaje(error, true);this.dialog.closeAll();});
  }

  listarFuste() {
    this.listaFuste = [{ id: "prueba1" }, { id: "prueba2" }];
    this.totalRecordsFuste = this.listaFuste.length;
  }

  listarRama() {
    this.listaRama = [{ id: "prueba1" }, { id: "prueba2" }];
    this.totalRecordsRama = this.listaRama.length;
  }

  //BOTONES
  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  loadDataFuste(event: any) {

  }

  loadDataRama(event: any) {

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
