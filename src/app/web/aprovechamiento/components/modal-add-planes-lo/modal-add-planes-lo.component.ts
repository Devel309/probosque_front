import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroValorService } from '@services';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { LibroOperacionesService } from 'src/app/service/aprovechamiento/libro-operaciones.service';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'app-modal-add-planes-lo',
  templateUrl: './modal-add-planes-lo.component.html',
  styleUrls: ['./modal-add-planes-lo.component.scss']
})
export class ModalAddPlanesLoComponent implements OnInit {
  objbuscarPlan = {
    idLibroOperaciones: null,
    idTipoProceso: 2,
    idPlanManejo: null,
    codigoTipoPlan: null,
    pageNum: 1,
    pageSize: 10
  }
  totalRecords: number = 0;

  listaPlanesTotal: any[] = [];
  comboPlanManejo: any[] = [];

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private genericoService: GenericoService,
    private parametroValorService: ParametroValorService,
    private libroOperacionesService: LibroOperacionesService,
  ) {
    this.objbuscarPlan.idLibroOperaciones = this.config.data.idSol;
  }

  ngOnInit(): void {
    this.listarPlanesTotal();
    this.listarComboPlanes();
  }

  //SERVICIOS
  listarComboPlanes() {
    this.parametroValorService.listarPorCodigoParametroValor({}).subscribe((resp: any) => {
      if (resp.success && resp.data) this.comboPlanManejo = resp.data;
    });
  }

  listarPlanesTotal() {
    this.listaPlanesTotal = [];
    this.totalRecords = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.listarLibroOperacionesPlanManejo(this.objbuscarPlan).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        if (resp.data && resp.data.length > 0) {
          this.listaPlanesTotal = resp.data.map((x: any) => {
            return { ...x, check: false }
          });
          this.totalRecords = resp.totalRecord || 0;
        } else {
          this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
        }
      }

    }, (error) => this.errorMensaje(error, true));
  }

  registrarPlanesLO(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.registrarLibroOperacionesPlanManejo(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success) {
        this.toast.ok(resp.message);
        this.listarPlanesTotal();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnGuardar() {
    const auxParams = this.listaPlanesTotal.filter(x => x.check).map(y => {
      return {
        idLibroOperaciones: this.config.data.idSol, idPlanManejo: y.idPlanManejo, idUsuarioRegistro: this.config.data?.idUser
      }
    });
    if (auxParams.length === 0) this.toast.warn("(*) Debe seleccionar al menos un Plan de Manejo.");
    else this.registrarPlanesLO(auxParams);
  }

  btnCancelar() {
    this.ref.close();
  }

  btnLimpiar() {
    this.objbuscarPlan = {
      idLibroOperaciones: null,
      idTipoProceso: 2,
      idPlanManejo: null,
      codigoTipoPlan: null,
      pageNum: 1,
      pageSize: 10
    }
    this.listarPlanesTotal();
  }

  btnBuscar() {
    this.objbuscarPlan.pageNum = 1;
    this.objbuscarPlan.pageSize = 10;
    this.listarPlanesTotal();
  }

  //FUNCIONES
  validarAgregar(): boolean {
    let valido = true;

    return valido;
  }

  loadData(event: any) {
    this.objbuscarPlan.pageNum = event.first + 1;
    this.objbuscarPlan.pageSize = event.rows;
    this.listarPlanesTotal();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
