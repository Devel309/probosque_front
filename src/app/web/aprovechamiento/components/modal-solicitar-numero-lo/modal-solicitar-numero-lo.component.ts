import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoLO } from 'src/app/model/aprovechamiento/CodigoEstadoLO';
import { RegistroLibroOperacion } from 'src/app/model/aprovechamiento/RegistroLibroOperacion';
import { CodigosTiposPersona } from 'src/app/model/util/CodigosTiposPerona';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { LibroOperacionesService } from 'src/app/service/aprovechamiento/libro-operaciones.service';
import { GenericoService } from 'src/app/service/generico.service';
import { ModalAddPlanesLoComponent } from '../modal-add-planes-lo/modal-add-planes-lo.component';

@Component({
  selector: 'app-modal-solicitar-numero-lo',
  templateUrl: './modal-solicitar-numero-lo.component.html',
  styleUrls: ['./modal-solicitar-numero-lo.component.scss']
})
export class ModalSolicitarNumeroLoComponent implements OnInit {
  requestModal = new RegistroLibroOperacion();
  infoPersona = {numeroDocumento: "", solicitante: ""};
  tipoArchivoEvidencia: string = "TDOCLIBOPEVISOL";
  tipoArchivoNotificacion: string = "TDOCLIBOPNOTSOL"

  listaPlanes: any [] = [];
  totalRecords: number = 0;

  isNew: boolean = false;
  isPerTitular: boolean = false;
  isPerArffs: boolean = false;
  isEstPendiente: boolean = false;
  isEstCompletado: boolean = false;
  showRegistro: boolean = false;
  isValidNunReg: boolean = false;
  tieneNumReg: boolean = false;
  agregarFolioTomo!: boolean;
  libroFisico!:boolean;
  isClicValidar: boolean = false;
  numeroFolio!: number;
  numeroTomo!: number;
  mostrarGrilla: boolean = false;


  objbuscarPlan = {
    idLibroOperaciones: 0,
    idTipoProceso: 1,
    pageNum: 1,
    pageSize: 10
  }

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private genericoService: GenericoService,
    private libroOperacionesService: LibroOperacionesService,
  ) {
    this.isNew = this.config.data?.isNew;
    this.isPerTitular = this.config.data.perfil === Perfiles.TITULARTH;
    this.isPerArffs = this.config.data.perfil === Perfiles.AUTORIDAD_REGIONAL;



    if(this.isNew) {
      this.requestModal.estadoLO = CodigoEstadoLO.PENDIENTE;
      this.requestModal.idUsuarioRegistro = this.config.data?.idUser;
    } else {

      this.requestModal.setData(this.config.data.datos);
      this.requestModal.agregarFolioTomo = this.config.data.datos.agregarFolioTomo;
      this.requestModal.libroFisico = this.config.data.datos.libroFisico;
      this.requestModal.numeroFolio = this.config.data.datos.numeroFolio;
      this.requestModal.numeroTomo = this.config.data.datos.numeroTomo;
      this.requestModal.validarSolicitud = this.config.data.datos.validarSolicitud;

      

      this.requestModal.idUsuarioModificacion = this.config.data?.idUser;

      this.infoPersona.numeroDocumento = this.config.data.datos.numeroDocumento;
      this.infoPersona.solicitante = this.config.data.datos.nombreSolicitanteUnico;
      this.isValidNunReg = !!this.config.data.datos.nroRegistro;
      this.tieneNumReg = !!this.config.data.datos.nroRegistro;

      this.isEstPendiente = this.requestModal.estadoLO === CodigoEstadoLO.PENDIENTE;
      this.isEstCompletado = this.requestModal.estadoLO === CodigoEstadoLO.COMPLETADO;
      this.showRegistro = (this.isPerArffs) || (this.isEstCompletado && this.isPerTitular);

      this.objbuscarPlan.idLibroOperaciones = this.requestModal.idLibroOperaciones || 0;
    }
  }

  ngOnInit(): void {
    
    if(this.isNew) {
      this.obtenerPersonaServ();
    } else {
      this.listarPlanes();
      // this.isValidNunReg = this.requestModal.validarSolicitud;
      if(this.requestModal.validarSolicitud){
        this.isValidNunReg = true;
      }else if(this.requestModal.validarSolicitud !=null){
        this.isValidNunReg = false;
      }
    
    }
  }

  //SERVICIOS
  obtenerPersonaServ() {
    const params= {idUsuario: this.config.data.idUser};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.genericoService.listarPorFiltroPersona(params).subscribe((resp: any) => {
      this.dialog.closeAll();
      if(resp.isSuccess && resp.data) {
        this.infoPersona.numeroDocumento = resp.data[0].numeroDocumento;
        this.infoPersona.solicitante = resp.data[0].codigo_tipo_persona === CodigosTiposPersona.NATURAL ? resp.data[0].nombres : resp.data[0].razonSocialEmpresa;
      }

    }, (error) => this.errorMensaje(error, true));
  }

  registrarLibroOperServ() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.registrarLibroOperaciones(this.requestModal).subscribe(resp => {
      this.dialog.closeAll();
      if(resp.success) {
        this.toast.ok(resp.message);
        this.ref.close(true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  actualizarLibroOperServ(isEnviar: boolean) {
    // this.requestModal.numeroFolio = this.numeroFolio;
    // this.requestModal.numeroTomo = this.numeroTomo;
    // this.requestModal.libroFisico = this.libroFisico;
    // this.requestModal.agregarFolioTomo = this.agregarFolioTomo;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.actualizarLibroOperaciones(this.requestModal).subscribe(resp => {
      this.dialog.closeAll();
      if(resp.success) {
        this.toast.ok("Código de Número de Registro de Libro de Operaciones Otorgado.");
        if(isEnviar) this.ref.close(true);
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  validarRegistro() {
    
    const params = {nroRegistro: this.requestModal.nroRegistro,
      idLibroOperaciones: this.objbuscarPlan.idLibroOperaciones};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.validarNroRegistroLibroOperaciones(params).subscribe(resp => {
      this.dialog.closeAll();
      if(resp.validateBusiness) {
        this.isValidNunReg = true;
        this.toast.ok(resp.message);
        this.requestModal.validarSolicitud = true;
        this.mostrarGrilla = true;
      } else {
        this.isValidNunReg = false;
        this.mostrarGrilla = false;
        this.toast.warn(resp.message);
        this.requestModal.validarSolicitud = false

      }
    }, (error) => this.errorMensaje(error, true));
  }

  listarPlanes() {
    this.listaPlanes = [];
    this.totalRecords = 0;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.listarLibroOperacionesPlanManejo(this.objbuscarPlan).subscribe(resp => {
      this.dialog.closeAll();
      if(resp.success && resp.data) {
        this.listaPlanes = resp.data;
        this.totalRecords = resp.totalRecord || 0;
        if(this.listaPlanes.length === 0 ) this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      }

    }, (error) => this.errorMensaje(error, true));
  }

  eliminarPlanMAnejoLO(idPlanManejo: number, idPlanLO: number) {
    const params = {
      idLibroOperaciones: this.requestModal.idLibroOperaciones,
      idLibroOperacionesPlanManejo: idPlanLO,
      idPlanManejo: idPlanManejo,
      idUsuarioElimina: this.config.data?.idUser
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.libroOperacionesService.eliminarLibroOperacionesPlanManejo(params).subscribe(resp => {
      this.dialog.closeAll();
      if(resp.success) {
        this.toast.ok(resp.message);
        this.listarPlanes();
      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnGuardar() {
    this.registrarLibroOperServ();
  }

  btnGuardarPlanManejo() {

    //if(!this.validarGuardar()) return;
    this.actualizarLibroOperServ(false);
  }

  btnActualizar(event: any) {
    if(!this.validarActualizar()) return;
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de Enviar la Información?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.requestModal.estadoLO = CodigoEstadoLO.COMPLETADO;
        this.actualizarLibroOperServ(true);
      },
    });
  }

  btnCancelar() {
    this.ref.close();
  }

  btnValidarReg() {
    if(!this.requestModal.nroRegistro) {
      this.toast.warn("(*) Debe ingresar un Número de Registro.");
      return;
    }
    this.validarRegistro();

  }

  registrarArchivo(id: number, adjunto: any) {
    adjunto.idArchivoEvidencia = id;
  }
  registrarArchivoNotificacion(id: number, adjunto: any) {
    adjunto.idArchivoNotificacion = id;
  }

  eliminarArchivoNew(ok: boolean, adjunto: any) {
    if(!ok) return;
    adjunto.idArchivoEvidencia = 0;
  }
  eliminarArchivoNewNotificacion(ok: boolean, adjunto: any) {
    if(!ok) return;
    adjunto.idArchivoNotificacion = 0;
  }


  btnAgregar() {
    const respRef = this.dialogService.open(ModalAddPlanesLoComponent, {
      header: 'Seleccionar Plan Manejo',
      width: '950px',
      style: { margin: '15px' },
      contentStyle: { overflow: 'auto' },
      data: {idSol: this.requestModal.idLibroOperaciones, idUser: this.config.data?.idUser}
    });

    respRef.onClose.subscribe((resp: any) => {
      this.listarPlanes();
    });
  }

  btnEliminarPlan(event: any, idPlanManejo: number, idPlanLO: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.eliminarPlanMAnejoLO(idPlanManejo, idPlanLO);
      },
    });
  }

  //FUNCIONES
  validarGuardar(): boolean {
    let valido = true;
    if(!this.requestModal.nroRegistro) {
      valido = false;
      this.toast.warn("(*) Debe ingresar un Número de Registro.");
    } else if(!this.isValidNunReg) {
      valido = false;
      this.toast.warn("(*) Debe de Validar el Número de Registro.");
    }
    return valido;
  }

  validarActualizar(): boolean {
    let valido = true;
    if(!this.requestModal.nroRegistro) {
      valido = false;
      this.toast.warn("(*) Debe ingresar un Número de Registro.");
    } else if(!this.isValidNunReg) {
      valido = false;
      this.toast.warn("(*) Debe de Validar el Número de Registro.");
    }

    if(this.listaPlanes.length === 0) {
      valido = false;
      this.toast.warn("(*) Debe de registrar al menos un Plan Manejo.");
    }
    return valido;
  }

  // changeNumReg() {
  //   this.isValidNunReg = false;
  // (ngModelChange)="changeNumReg()"
  // }

  loadData(event: any) {
    this.objbuscarPlan.pageNum = event.first + 1;
    this.objbuscarPlan.pageSize = event.rows;
    this.listarPlanes();
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
