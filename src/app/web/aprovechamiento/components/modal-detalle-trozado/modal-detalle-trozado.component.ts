import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';

@Component({
  selector: 'app-modal-detalle-trozado',
  templateUrl: './modal-detalle-trozado.component.html',
  styleUrls: ['./modal-detalle-trozado.component.scss']
})
export class ModalDetalleTrozadoComponent implements OnInit {
  listaFuste: any[] = [];
  totalRecordsFuste: number = 0;
  listaRama: any[] = [];
  totalRecordsRama: number = 0;
  idSincronizacion: number = 0;
  idTala:number = 0;

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { 
    this.idSincronizacion = this.config.data?.idSincronizacion;
    this.idTala =  this.config.data?.idTala;
  }

  ngOnInit(): void {
    this.listarDetalleTrozado();
    //this.listarFuste();
    //this.listarRama();
  }

  //SERVICIOS
  listarFuste() {
    this.listaFuste = [{ id: "prueba1" }, { id: "prueba2" }];
    this.totalRecordsFuste = this.listaFuste.length;
  }

  listarRama() {
    this.listaRama = [{ id: "prueba1" }, { id: "prueba2" }];
    this.totalRecordsRama = this.listaRama.length;
  }

  listarDetalleTrozado() {
    const params = {
      "idSincronizacion": this.idSincronizacion, "idTala": this.idTala, numTrozado: 1 , "pageNum": 1,
      "pageSize": 10
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarTalaDetallePorFiltro(params).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        resp.data.forEach((t: any) => {
          if (t.tipoTalaDetalle === "1") {
            if(t.nivelTrozado ===0){
              t.nivelTrozado  = 'Nivel 1 - Tala';
            }else if(t.nivelTrozado ===1){
              t.nivelTrozado  = 'Nivel 2 - Trozado';
            }else if(t.nivelTrozado ===2){
              t.nivelTrozado  = 'Nivel 3 - Retrozado';
            }
            this.listaFuste.push(t);

          } else if (t.tipoTalaDetalle === "2") {
            if(t.nivelTrozado ===0){
              t.nivelTrozado  = 'Nivel 1 - Tala';
            }else if(t.nivelTrozado ===1){
              t.nivelTrozado  = 'Nivel 2 - Trozado';
            }else if(t.nivelTrozado ===2){
              t.nivelTrozado  = 'Nivel 3 - Retrozado';
            }
            this.listaRama.push(t);
          }
        });
      }
    }, (error) => { this.errorMensaje(error, true); this.dialog.closeAll(); });
  }

  //BOTONES
  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  loadDataFuste(event: any) {

  }

  loadDataRama(event: any) {

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
