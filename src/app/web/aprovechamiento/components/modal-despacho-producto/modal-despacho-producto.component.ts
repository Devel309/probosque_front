import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { Console } from 'console';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';

@Component({
  selector: 'app-modal-despacho-producto',
  templateUrl: './modal-despacho-producto.component.html',
  styleUrls: ['./modal-despacho-producto.component.scss']
})
export class ModalDespachoProductoComponent implements OnInit {
  detalleListaProductos: any[] = [];
  totalRecordsDetProd: number = 0;
  requestTabla : {
    idPlanManejo: number | null; 
    idSincronizacion: number | null;
    idListaProductoTerminado:number  | null;
    tipoConsulta: string;
    pageNum: number;
    pageSize: number;
  } = {
    idPlanManejo: null, idSincronizacion: null,idListaProductoTerminado:null,
    tipoConsulta: "TROZADO", pageNum: 1, pageSize: 10
  };
  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) { 
    this.requestTabla.idListaProductoTerminado =  this.config.data?.idListaProductoTerminado;  
    this.requestTabla.idSincronizacion = this.config.data?.idSincronizacion;
    this.requestTabla.idPlanManejo = this.config.data?.idPlanManejo;
  }
  ngOnInit(): void {
    this.listarDetalleProductos();
  }

  //SERVICIOS
  listarDetalleProductos() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    
    this.sincronizacionService.listarOrdenProduccionProductoTerminadoweb(this.requestTabla).subscribe(resp => {
      this.dialog.closeAll();
      if (resp.success && resp.data) {
        this.detalleListaProductos = resp.data;
        this.detalleListaProductos.forEach((t:any) =>{
          if(t.unidadMedida === null){
            t.unidadMedida = '-';
          }
        })
        this.totalRecordsDetProd = this.detalleListaProductos.length;
        
      }
    }, (error) => {this.errorMensaje(error, true); this.dialog.closeAll();});
  }

  //BOTONES
  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  loadDataDetProd(event: any) {
    this.requestTabla.pageNum = event.first + 1;
    this.requestTabla.pageSize = event.rows;
    this.listarDetalleProductos();
  }


 
  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
