import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';

@Component({
  selector: 'app-modal-detalle-transformacion',
  templateUrl: './modal-detalle-transformacion.component.html',
  styleUrls: ['./modal-detalle-transformacion.component.scss']
})
export class ModalDetalleTransformacionComponent implements OnInit {
  idOrdenProduccion: number = 0;
  listaTrozaConsumida: any[] = [];
  totalRecordsTrozaConsumida: number = 0;
  listaProdTerm: any[] = [];
  totalRecordsProdTerm: number = 0;
  idSincronizacion: number = 0;

  constructor(
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private dialog: MatDialog,
    private toast: ToastService,
    private sincronizacionService: SincronizacionService,
  ) {
    this.idOrdenProduccion = this.config.data.idOrdProd;
    this.idSincronizacion=this.config.data.idSincronizacion;
  }

  ngOnInit(): void {
    this.listarTrozaConsumida();
    this.listarProdTerm();
  }

  //SERVICIOS
  listarTrozaConsumida() {
    //this.listaTrozaConsumida = [{id: "prueba1"}, {id: "prueba2"}];
   
    const params = {idOrdProduccion: this.idOrdenProduccion, idSincronizacion: this.idSincronizacion,pageNum: 1,
      pageSize:10};
      
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.sincronizacionService.listarTrozaConsumida(params).subscribe(resp => {
        this.dialog.closeAll();
        
        if (resp.success && resp.data) {
          this.listaTrozaConsumida = resp.data;
          this.totalRecordsTrozaConsumida = this.listaTrozaConsumida.length;

        }
      }, (error) => this.errorMensaje(error, true));
  }

  listarProdTerm() {
    this.listaProdTerm = [];
    this.totalRecordsProdTerm = 0;
    const params = {idOrdProduccion: this.idOrdenProduccion, idProdTerminado: null, idSincronizacion: this.idSincronizacion,pageNum: 1,
    pageSize:10};
    
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.listarOrdenProduccionProductoTerminado(params).subscribe(resp => {
      this.dialog.closeAll();
      
      if (resp.success && resp.data) {
        this.listaProdTerm = resp.data;
        this.totalRecordsProdTerm = resp.totalRecord || 0;
      }
    }, (error) => this.errorMensaje(error, true));
  }

  //BOTONES
  btnCancelar() {
    this.ref.close();
  }

  //FUNCIONES
  loadDataTrozaConsumida(event: any) {

  }

  loadDataProdTerm(event: any) {

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
