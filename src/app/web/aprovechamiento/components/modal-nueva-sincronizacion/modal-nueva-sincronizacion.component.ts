import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SincronizacionService } from 'src/app/service/aprovechamiento/sincronizacion.service';
import {UsuarioService} from '@services';

@Component({
  selector: 'app-modal-nueva-sincronizacion',
  templateUrl: './modal-nueva-sincronizacion.component.html',
  styleUrls: ['./modal-nueva-sincronizacion.component.scss']
})
export class ModalNuevaSincronizacionComponent implements OnInit {
  adjunto: {file: any; tipoArchivo: string; idArchivo: number} = {
    tipoArchivo: `TDOCSINCRO`,
    idArchivo: 0,
    file: null
  };

  constructor(
    private ref: DynamicDialogRef,
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private toast: ToastService,
    private usuarioService: UsuarioService,
    private sincronizacionService: SincronizacionService,
  ) { }

  ngOnInit(): void {
  }

  sincronizar() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.sincronizacionService.sincronizarServ(
      this.adjunto.idArchivo,
      this.usuarioService.idUsuario,
      this.adjunto.file).subscribe(resp => {
      this.dialog.closeAll();
      if(resp.success) {
        this.toast.ok(resp.message);
        this.ref.close();

      } else {
        this.toast.warn(resp.message);
      }
    }, (error) => this.errorMensaje(error, true));
  }

  btnGuardar(event: any) {
    if(!this.validarSincronizar()) return;
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de realizar la Sincronización?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        
        this.sincronizar();
      },
    });
  }

  btnCancelar() {
    this.ref.close();
  }

  registrarArchivoFile(fileIn: any) {
    this.adjunto.idArchivo = fileIn.idArchivo;
    this.adjunto.file = fileIn.file;
  }

  eliminarArchivo(event: any) {
    this.adjunto.idArchivo = 0;
    this.adjunto.file = null;
  }

  validarSincronizar(): boolean {
    let valido = true;

    if(!this.adjunto.file) {
      valido = false
      this.toast.warn("(*) Debe de adjuntar el archivo de Sincronización.");
    }

    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
