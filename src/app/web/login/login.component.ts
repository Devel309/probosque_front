import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/service/config.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { datosUsuario_parcial } from 'src/app/model/user';
import { MessageService } from 'primeng/api';
import { NgRecaptcha3Service } from 'ng-recaptcha3';
// import { Md5 } from "ts-md5/dist/md5";
//import {Md5} from "md5-typescript";
import * as CryptoJS from 'crypto-js';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ValidatorsExtend } from 'src/app/model/util/ValidatorsExtend';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService],
})
export class LoginComponent implements OnInit {
  verPassword: boolean = false;
  btnShowverLogin = false;
  private siteKey = '6LfHrd4ZAAAAAK2MBAmkJZ8-VNlCbiuUENCWemKf';
  requiereCambioContrasenia: boolean = false;
  // md5 = new Md5();

  paramsLogin: FormGroup;
  hide: boolean = false;
  constructor(
    public _configService: ConfigService,
    private _router: Router,
    private servAuth: AuthService,
    private messageService: MessageService,
    private fb: FormBuilder,
    private recaptcha3: NgRecaptcha3Service
  ) {
    this._configService.config = {
      useLayout: false,
    };

    this.paramsLogin = this.fb.group({
      username: ['soporte_sharepoint@valtx.pe'],
      password: ['123'],
      codApp: ['MC'],
      tokenRecaptcha: [null],
      token: null,
      passwordNew: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        ValidatorsExtend.passwordValid(),
        ValidatorsExtend.passwordValidReverseSame(),
      ]),
      confirmPasswordNew: [''],
      requiereCambioContrasenia: [false],
    });
  }

  param = {
    username: '',
    password: '',
    codApp: '',
    tokenRecaptcha: null,
    token: null,
    passwordNew: '',
    confirmPasswordNew: '',
    requiereCambioContrasenia: false,
  };

  validarUsuario() {
    const paramsLogin = this.paramsLogin.value;
    this._configService.config = {
      useLayout: false,
    };
    this.servAuth.validarLogin(paramsLogin).subscribe(
      (result: any) => {
        let succes = result.success == true ? 'success' : 'warn';
        this.messageService.add({
          severity: succes,
          summary: '',
          detail: result.message,
        });
        this.verPassword = result.success;
      },
      (error: any) => {
        this.messageService.add({
          severity: 'warn',
          summary: error.name,
          detail: 'Problemas de Conexión ó Comuníquese con Soporte',
        });
      }
    );
  }

  login() {
    const paramsLogin = this.paramsLogin.value;
    this._configService.config = {
      useLayout: false,
    };

    this.btnShowverLogin = true;
    this.recaptcha3.getToken().then((token) => {
      paramsLogin.tokenRecaptcha = token;
      this.param = paramsLogin;
      this.param.password = paramsLogin.password.trim().toString();
      this.servAuth.login(this.param).subscribe(
        (result: any) => {
          localStorage.setItem('usuario', JSON.stringify(result.loginDto));
          if (result.success) {
            this.requiereCambioContrasenia =
              result.segEntity.requiereCambioContrasenia;

            if (this.requiereCambioContrasenia) this.btnShowverLogin = false;
            else {
              // sessionStorage.setItem('token', result.segEntity.token);
              localStorage.setItem('token', result.segEntity.token);
              this._configService.config = { useLayout: true };
              //this._router.navigateByUrl('/inicio');
              window.location.href = './inicio';
            }
          } else {
            this.messageService.add({
              severity: 'warn',
              summary: '',
              detail: result.message,
            });
            this.btnShowverLogin = false;
          }
        },
        (error: any) => {
          this.messageService.add({
            severity: 'warn',
            summary: error.name,
            detail: 'Problemas de Conexión ó Comuníquese con Soporte',
          });
          this.btnShowverLogin = false;
        }
      );
    }); //fin recapchat
  }

  guardarCambioContrasenialogin() {
    const paramsLogin = this.paramsLogin.value;
    this._configService.config = {
      useLayout: false,
    };

    if (paramsLogin.passwordNew.trim() == '') {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Ingrese la nueva contraseña.',
      });
      return;
    }
    if (paramsLogin.confirmPasswordNew.trim() == '') {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Ingrese la confirmación de su nueva contraseña.',
      });
      return;
    }
    if (
      paramsLogin.passwordNew.trim() != paramsLogin.confirmPasswordNew.trim()
    ) {
      this.messageService.add({
        severity: 'warn',
        summary: '',
        detail: 'Las contraseñas deben ser iguales.',
      });
      return;
    }

    paramsLogin.requiereCambioContrasenia = false;
    this.param = paramsLogin;
    this.param.password = paramsLogin.password.trim().toString();
    this.param.passwordNew = paramsLogin.passwordNew.trim().toString();

    this.btnShowverLogin = true;

    this.servAuth.guardarCambioContraseniaLogin(this.param).subscribe(
      (result: any) => {
        if (result.success) {
          paramsLogin.password = paramsLogin.passwordNew;
          this.login();
        } else {
          this.messageService.add({
            severity: 'warn',
            summary: '',
            detail: result.message,
          });
          this.btnShowverLogin = false;
        }
      },
      (error: any) => {
        this.messageService.add({
          severity: 'warn',
          summary: error.name,
          detail: 'Problemas de Conexión ó Comuníquese con Soporte',
        });
        this.btnShowverLogin = false;
      }
    );
  }

  ngOnInit() {
    this.recaptcha3.init(this.siteKey);
  }
  onConfirm() {
    this.messageService.clear('c');
  }

  onReject() {
    this.messageService.clear('c');
  }

  clear() {
    this.messageService.clear();
  }
}
