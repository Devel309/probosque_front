import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastService } from '@shared';
import { DataTablesModule } from 'angular-datatables';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AvatarModule } from 'primeng/avatar';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { DialogService, DynamicDialogModule } from 'primeng/dynamicdialog';
import { FieldsetModule } from 'primeng/fieldset';
import { InputMaskModule } from 'primeng/inputmask';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MenuModule } from 'primeng/menu';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { BandejaEvaluacionOtorgamientoTuModule } from '././module/planificacion/bandeja-evaluacion-otorgamiento-tu.module';
import { OposicionModule } from '././module/planificacion/oposicion.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ModalAutorizacionComponent } from './components/modal/modal-autorizacion/modal-autorizacion.component';
import { ModalFileComponent } from './components/modal/modal-file/modal-file.component';
import { ModalObservacionesComponent } from './components/modal/modal-observaciones/modal-observaciones.component';
import { SidebarNavGroupComponent } from './components/sidebar/sidebar-nav-group/sidebar-nav-group.component';
import { SidebarNavItemComponent } from './components/sidebar/sidebar-nav-item/sidebar-nav-item.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { HttpConfigInterceptor } from './interceptor/HttpConfig';
import { CalificacionConcesionPFDMModule } from './module/concesion/calificacion-concesion-pfdm.module';
import { SolicitudConcesionPFDMModule } from './module/concesion/solicitud-concesion-pfdm.module';
import { MecanismoPagoModule } from './module/derecho-aprovechamiento/mecanismo-pago/mecanismo-pago.module';
import { ElaboracionPgmfModule } from './module/elaboracion-pgmf.module';
import { EvaluacionAprobacionPmModule } from './module/evaluacion-aprobacion-pm.module';
import { InicioModule } from './module/inicio.module';
import { LoginModule } from './module/login.module';
import { MantenimientoPerfilModule } from './module/mantenimiento-perfil.module';
import { PlanificacionModule } from './module/planificacion.module';
import { AmpliacionSolicitudModule } from './module/planificacion/ampliacion-solicitud.module';
import { AmpliacionSolicitudesModule } from './module/planificacion/ampliacion-solicitudes.module';
import { BandejaContratoConcesionPfdmModule } from './module/planificacion/bandeja-contrato-concesion-pfdm.module';
import { BandejaContratoConcesionPostulacionModule } from './module/planificacion/bandeja-contrato-concesion-postulacion.module';
import { BandejaContratoModule } from './module/planificacion/bandeja-contrato.module';
import { BandejaEvaluacionSolicitudModule } from './module/planificacion/bandeja-evaluacion-impugnacion.module';
import { BandejaEvaluacionTecnicaModule } from './module/planificacion/bandeja-evaluacion-tecnica.module';
import { BandejaGeneracionDemaModule } from './module/planificacion/bandeja-generacion-dema.module';
import { BandejaOtorgamientoPfdmModule } from './module/planificacion/bandeja-otorgamiento-pfdm.module';
import { BandejaPermisosForestalesModule } from './module/planificacion/bandeja-permisos-forestales.module';
import { BandejaPgmfConsolidadoModule } from './module/planificacion/bandeja-pgmf-consolidado.module';
import { BandejaPGMFModule } from './module/planificacion/bandeja-PGMF.module';
import { BandejaPlanOperativoModule } from './module/planificacion/bandeja-plan-operativo.module';
import { BandejaPmficModule } from './module/planificacion/bandeja-pmfic.module';
import { BandejaPopacModule } from './module/planificacion/bandeja-popac.module';
// import { BandejaPlantacionForestalModule } from './module/planificacion/bandeja-plantacion-forestal.module';
import { BandejaProcesoConcesionPfdmModule } from './module/planificacion/bandeja-proceso-concesion-pfdm.module';
import { BandejaSolicitudAccesoModule } from './module/planificacion/bandeja-solicitud-acceso.module';
import { BandejaSolicitudContratoModule } from './module/planificacion/bandeja-solicitud-contrato.module';
import { BandejaSolicitudModule } from './module/planificacion/bandeja-solicitud.module';
import { ConcesionForestalMaderablesModule } from './module/planificacion/concesion-forestal-maderables.module';
import { ConsideracionesEvalucionPgmfModule } from './module/planificacion/consideraciones-evalucion-pgmf.module';
import { ContratoElaboracionPgmfModule } from './module/planificacion/contrato-elaboracion-pgmf.module';
import { ElaboracionDeclaracionMpafppModule } from './module/planificacion/elaboracion-declaracion-mpafpp.module';
import { EnvioInformacionOtorgamientoModule } from './module/planificacion/envio-informacion-otorgamiento.module';
import { EvaluacionCampoModule } from './module/planificacion/evaluacion-campo.module';
import { EvaluacionContratoConcesionModule } from './module/planificacion/evaluacion-contrato-concesion.module';
import { EvaluacionPgmfModule } from './module/planificacion/evaluacion-pgmf.module';
import { EvaluarOtorgarTuModule } from './module/planificacion/evaluar-otorgar-tu.module';
import { EvaluarPermisoForestalCCNNModule } from './module/planificacion/evaluar-permiso-forestal-ccnn.module';
import { EvaluarPermisoForestalModule } from './module/planificacion/evaluar-permiso-forestal.module';
import { FormulacionPMFIConcesionPFDMModule } from './module/planificacion/formulacion-pmfi-concesion-pfdm.module';
import { GeneracionContratoConcesionModule } from './module/planificacion/generacion-contrato-concesion.module';
import { GeneracionContratoModule } from './module/planificacion/generacion-contrato.module';
import { GeneracionDeclaracionManejoDemaModule } from './module/planificacion/generacion-declaracion-manejo-dema.module';
import { GenerarResolucionModule } from './module/planificacion/generar-resolucion.module';
import { LineamientosEvaluacionPgmfModule } from './module/planificacion/lineamientos-evaluacion-pgmf.module';
import { PlanGeneralManejoModule } from './module/planificacion/plan-general-manejo.module';
import { PlanManejoForestalIntermedioModule } from './module/planificacion/plan-manejo-forestal-intermedio.module';
import { PlanOperativoPMFICnccModule } from './module/planificacion/plan-manejo-PMFI-cncc.module';
import { PlanOperativoModule } from './module/planificacion/plan-operativo-ccnn-ealta.module';
import { PlanOperativoConcesionesMaderablesModule } from './module/planificacion/plan-operativo-concesiones-maderables.module';
import { RegistroPlantacionForestalModule } from './module/planificacion/registro-plantacion-forestal.module';
import { RevicionPlantacionForestalModule } from './module/planificacion/revicion-plantacion-forestal.module';
import { RevisionPermisosForestalesModule } from './module/planificacion/revision-permisos-forestales.module';
import { RevisionSolicitudAccesoModule } from './module/planificacion/revision-solicitud-acceso.module';
import { SolicitudOpinionModule } from './module/planificacion/solicitud-opinion.module';
import { SolicitudPermisoForestalCCNNModule } from './module/planificacion/solicitud-permiso.forestal-ccnn.module';
import { SolicitudPermisoForestalModule } from './module/planificacion/solicitud-permiso.forestal.module';
import { BandejaPlantacionForestalModule } from './module/plantacion/bandeja-plantacion-forestal.module';
import { RecuperarContrasenaModule } from './module/recuperar-contrasena.module';
import { RegimenPromocionalModule } from './module/regimen-promocional/regimen-promocional.module';
import { RegistrarSolicitudAccesoModule } from './module/registrar-solicitud-acceso.module';
import { GestionEvaluacionPlanManejoModule } from './module/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-manejo.module';
import { GestionEvaluacionPlanOperativojoModule } from './module/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-operativo.module';
import { TramitesModule } from './module/tramites.module';
import { ValidarCondicionMinimaModule } from './shared/components/validar-condicion-minima/validar-condicion-minima.module';
import { EventEmitterService } from './shared/services/event-emitter.service';
import { BandejaEvalPmfiDemaModule } from './web/planificacion/bandeja-eval-pmfi-dema/bandeja-eval-pmfi-dema.module';
import { LayoutComponent } from './web/layout/layout.component';
import { EvaluacionEvalPmfiDemaModule } from './web/evaluacion/evaluacion-eval-pmfi-dema/evaluacion-eval-pmfi-dema.module';
import { BandejaEstablecimientoBlModule } from './module/bosque-local/bandeja-establecimiento-bl.module';
import { SolicitudOpinionEntidadModule } from './module/planificacion/solicitud-opinion-entidad.module';
import { GenericModalAutorizacionModule } from './shared/components/generic-modal-autorizacion/generic-modal-autorizacion.module';
import { BandejaLibroOperacionesModule } from './module/aprovechamiento/bandeja-libro-operaciones.module';
import { BandejaDocumentosDigitalizadosModule } from './module/planificacion/bandeja-documentos-digitalizados';
import { SincronizacionModule } from './module/aprovechamiento/sincronizacion.module';
import { RequisitosMdpModule } from './web/planificacion/bandeja-eval-pgmfa/requisitos-mdp/requisitos-mdp.module';
import { RequisitosMdpPoacModule } from './web/planificacion/bandeja-eval-poac/requisitos-mdp/requisitos-mdp.module';
import { RequisitoPreviosPoacModule } from './web/planificacion/bandeja-eval-poac/requisitos-previos/requisitos-previos-poac.module';
import { RequisitoPreviosPgmfaModule } from './web/planificacion/bandeja-eval-pgmfa/requisitos-previos/requisitos-previos-pgmfa.module';


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    SidebarComponent,
    TopbarComponent,
    SidebarNavGroupComponent,
    SidebarNavItemComponent,
    ModalAutorizacionComponent,
    ModalFileComponent,
    LoadingComponent,
    ModalObservacionesComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    //global component's
    MenuModule,
    AvatarModule,
    AutoCompleteModule,
    // modulos
    LoginModule,
    RecuperarContrasenaModule,

    InicioModule,
    TramitesModule,
    PlanificacionModule,
    ElaboracionPgmfModule,
    EvaluacionAprobacionPmModule,
    PlanGeneralManejoModule,
    BandejaPmficModule,
    BandejaPopacModule,
    BandejaSolicitudAccesoModule,
    BandejaGeneracionDemaModule,
    BandejaPGMFModule,
    RevisionSolicitudAccesoModule,
    RegistrarSolicitudAccesoModule,
    BandejaEvaluacionOtorgamientoTuModule,
    EvaluarOtorgarTuModule,
    BandejaDocumentosDigitalizadosModule,

    MantenimientoPerfilModule,

    BandejaPermisosForestalesModule,

    RevisionPermisosForestalesModule,
    ConcesionForestalMaderablesModule,
    ConsideracionesEvalucionPgmfModule,
    LineamientosEvaluacionPgmfModule,
    AmpliacionSolicitudModule,
    AmpliacionSolicitudesModule,
    OposicionModule,
    DynamicDialogModule,
    InputTextareaModule,
    InputMaskModule,
    ToastModule,
    ConfirmDialogModule,
    PlanOperativoModule,
    RegistroPlantacionForestalModule,
    BandejaPlantacionForestalModule,
    RevicionPlantacionForestalModule,
    PlanManejoForestalIntermedioModule,
    GenerarResolucionModule,

    PlanOperativoConcesionesMaderablesModule,
    PlanOperativoPMFICnccModule,
    ElaboracionDeclaracionMpafppModule,
    GeneracionContratoModule,
    BandejaContratoConcesionPfdmModule,
    BandejaContratoModule,
    DataTablesModule,

    BandejaSolicitudContratoModule,
    BandejaContratoConcesionPostulacionModule,
    EvaluacionContratoConcesionModule,

    BandejaEvaluacionTecnicaModule,

    SolicitudPermisoForestalModule,
    EvaluarPermisoForestalModule,

    SolicitudPermisoForestalCCNNModule,
    EvaluarPermisoForestalCCNNModule,

    BandejaProcesoConcesionPfdmModule,
    BandejaOtorgamientoPfdmModule,

    GeneracionContratoConcesionModule,
    EnvioInformacionOtorgamientoModule,
    ContratoElaboracionPgmfModule,
    BandejaPgmfConsolidadoModule,
    GeneracionDeclaracionManejoDemaModule,

    BandejaPlanOperativoModule,
    FormulacionPMFIConcesionPFDMModule,

    ValidarCondicionMinimaModule,
    EvaluacionCampoModule,
    EvaluacionPgmfModule,
    SolicitudOpinionModule,
    BandejaSolicitudModule,
    BandejaEvaluacionSolicitudModule,
    GestionEvaluacionPlanManejoModule,
    GestionEvaluacionPlanOperativojoModule,

    MecanismoPagoModule,
    RegimenPromocionalModule,
    FieldsetModule,
    BreadcrumbModule,
    TableModule,
    DropdownModule,
    MatIconModule,

    /* CONCESION */
    SolicitudConcesionPFDMModule,
    CalificacionConcesionPFDMModule,
    EvaluacionEvalPmfiDemaModule,
    /* BOSQUE LOCAL */
    BandejaEstablecimientoBlModule,
    SolicitudOpinionEntidadModule,
    GenericModalAutorizacionModule,
    /* LIBRO OPERACIONES */
    BandejaLibroOperacionesModule,
    SincronizacionModule,
    RequisitosMdpModule,
    RequisitosMdpPoacModule,
    RequisitoPreviosPoacModule,
    RequisitoPreviosPgmfaModule,
  ],
  exports: [LayoutComponent, SidebarComponent, TopbarComponent],
  providers: [
    //{ provide: HTTP_INTERCEPTORS, useClass: PlanificacionService, multi: true },
    //{ provide: HTTP_INTERCEPTORS, useClass: ApiForestalService, multi: true },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
    DialogService,
    MessageService,
    ToastService,
    EventEmitterService,
    DecimalPipe,
    ConfirmationService
  ],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
