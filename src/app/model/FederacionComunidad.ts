
import { AuditoriaModel } from "./auditoria";
export interface FederacionComunidadModel extends AuditoriaModel{ 
    idFedComunidad: number;
    descripcion: String;
}
