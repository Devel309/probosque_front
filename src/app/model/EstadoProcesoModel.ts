export interface EstadoProcesoModel { 
    tipoStatus:String;
    idStatusProceso: number,
    descripcion: String,
}