import { SolicitudModel } from "./Solicitud";
import { TipoProcesoModel } from "./TipoProceso";
import { TipoPlanModel } from "./TipoPlan";
import { TipoEscalaModel } from "./TipoEscala";
import { AuditoriaModel } from "./auditoria";
export interface PlanManejoModel extends AuditoriaModel{
    idPlanManejo: number;
    descripcion: String;
    solicitud:SolicitudModel;
    tipoProceso:TipoProcesoModel;
    tipoPlan:TipoPlanModel;
    tipoEscala:TipoEscalaModel;
    perEmpadronada:number;
    perTrabajan:number;
    aspectoComplementario: String;
    codigoParametro: String;
}


export const PlanManejoMSG = {
    OK: {
        CREATE: 'Plan registrado correctamente',
        UPDATE: 'Plan actualizado correctamente',
        DELETE: 'Plan eliminado correctamente',
    },
    ERR: {
        LIST: 'Ocurrió un error al listar los Planes',
        CREATE: 'Ocurrió un error al registrar el Plan',
        UPDATE: 'Ocurrió un error al actualizar el Plan',
        DELETE: 'Ocurrió un error al eliminar el Plan',
    }
}