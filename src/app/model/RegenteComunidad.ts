import { AuditoriaModel } from "./auditoria";
import { PersonaModel } from "./Persona";
export interface RegenteComunidadModule extends AuditoriaModel{
    idRegenteComunidad: number;
    persona:PersonaModel;
    codInrena:String;

}