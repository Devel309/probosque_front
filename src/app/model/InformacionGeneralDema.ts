
import { AuditoriaDto } from "./auditoria";
import { InformacionGeneralDemaDetalle } from "./InformacionGeneralDemaDetalle";

export class InformacionGeneralDema extends AuditoriaDto {
  idPlanManejo?: number | null;
  idPersonaComunidad?: number | null;
  codigoProceso?: string | null;
  idInformacionGeneralDema?: number | null;
  nombreComunidad?: string | null;
  nombresJefeComunidad?: string | null;
  apellidoMaternoJefeComunidad?: string | null;
  apellidoPaternoJefeComunidad?: string | null;
  dniJefeComunidad?: string | null;
  federacionComunidad?: string | null;
  idDepartamento?: number | null;
  idProvincia?: number | null;
  idDistrito?: number | null;
  cuenca?: string | null;
  subCuenca?: string | null;
  nroAnexosComunidad?: number | null;
  nroResolucionComunidad?: string | null;
  nroTituloPropiedadComunidad?: string | null;
  nroTotalFamiliasComunidad?: number | null;
  nroRucComunidad?: string | null;
  fechaInicioDema?: Date | null;
  fechaFinDema?: Date | null;
  nombreElaboraDema?: string | null;
  apellidoPaternoElaboraDema?: string | null;
  apellidoMaternoElaboraDema?: string | null;
  dniElaboraDema?: string | null;
  nroPersonasInvolucradas?: number | null;
  nroArbolesMaderables?: number | null;
  nroArbolesMaderablesSemilleros?: number | null;
  superficieHaMaderables?: string | null;
  volumenM3rMaderables?: string | null;
  nroArbolesNoMaderables?: number | null;
  nroArbolesNoMaderablesSemilleros?: number | null;
  superficieHaNoMaderables?: string | null;
  volumenM3rNoMaderables?: string | null;
  totalIngresosEstimado?: number | null;
  totalCostoEstimado?: number | null;
  totalUtilidadesEstimado?: number | null;
  aprovechamientoNoMaderable?: number | null;
  lstUmf?: InformacionGeneralDemaDetalle[] | null;
  idPermisoForestal?: number | null;
  areaTotal = null;
  codTipoPersona = null;
  tipoPersona = null;
  codTipoDocumentoElaborador = null;
  tipoDocumentoElaborador = null;
  codTipoDocumentoRepresentante = null;
  tipoDocumentoRepresentante = null;
  documentoRepresentante = null;
  nombreRepresentante = null;
	apellidoPaternoRepresentante = null;
	apellidoMaternoRepresentante = null;
  nombreCompletoTitular = null;
  nombreCompletoRepresentante = null;
  esReprLegal!: string;
  idDistritoRepresentante = null;

  constructor(obj?: Partial<InformacionGeneralDema>) {
    super();

    this.idPlanManejo = null;
    this.idPersonaComunidad = null;
    this.codigoProceso = null;
    this.idInformacionGeneralDema = null;
    this.nombreComunidad = null;
    this.nombresJefeComunidad = null;
    this.apellidoMaternoJefeComunidad = null;
    this.apellidoPaternoJefeComunidad = null;
    this.dniJefeComunidad = null;
    this.federacionComunidad = null;
    this.idDepartamento = null;
    this.idProvincia = null;
    this.idDistrito = null;
    this.cuenca = null;
    this.subCuenca = null;
    this.nroAnexosComunidad = null;
    this.nroResolucionComunidad = null;
    this.nroTituloPropiedadComunidad = null;
    this.nroTotalFamiliasComunidad = null;
    this.nroRucComunidad = null;
    this.fechaInicioDema = null;
    this.fechaFinDema = null;
    this.nombreElaboraDema = null;
    this.apellidoPaternoElaboraDema = null;
    this.apellidoMaternoElaboraDema = null;
    this.dniElaboraDema = null;
    this.nroPersonasInvolucradas = null;
    this.nroArbolesMaderables = null;
    this.nroArbolesMaderablesSemilleros = null;
    this.superficieHaMaderables = null;
    this.volumenM3rMaderables = null;
    this.nroArbolesNoMaderables = null;
    this.nroArbolesNoMaderablesSemilleros = null;
    this.superficieHaNoMaderables = null;
    this.volumenM3rNoMaderables = null;
    this.totalIngresosEstimado = null;
    this.totalCostoEstimado = null;
    this.totalUtilidadesEstimado = null;
    this.aprovechamientoNoMaderable = null;
    this.lstUmf = [];
    this.idPermisoForestal = null;
    this.areaTotal = null;
    this.codTipoPersona = null;
    this.tipoPersona = null;
    this.codTipoDocumentoElaborador = null;
    this.tipoDocumentoElaborador = null;
    this.codTipoDocumentoRepresentante = null;
    this.tipoDocumentoRepresentante = null;
    this.documentoRepresentante = null;
    this.nombreRepresentante = null;
    this.apellidoPaternoRepresentante = null;
    this.apellidoMaternoRepresentante = null;
    this.nombreCompletoTitular = null;
    this.nombreCompletoRepresentante = null;
    this.esReprLegal = "";
    this.idDistritoRepresentante = null;

    if (obj) Object.assign(this, obj);
  }

}

export const IGD_DETALLE: { codigo: string, descripcion: string, detalle: string, }[] = [
  {
    codigo: '001',
    descripcion: `La UMF se desarrolla investigación alineada al Programa de Investigación Forestal aprobado por el SERFOR.`,
    detalle: 'NO',
  },
  {
    codigo: '002',
    descripcion: `La UMF se ubica en la zona prioritaria para el Estado.`,
    detalle: 'NO',
  },
  {
    codigo: '003',
    descripcion: `La UMF se realizan proyectos integrales de transformación.`,
    detalle: 'NO',
  },
  {
    codigo: '004',
    descripcion: `La UMF cuenta con la certificación forestal voluntaria de buenas prácticas.`,
    detalle: 'NO',
  },
  {
    codigo: '005',
    descripcion: `La UMF se protege, conserva y/o recupera áreas no destinadas al aprovechamiento forestal.`,
    detalle: 'NO',
  },
  {
    codigo: '006',
    descripcion: `La UMF se encuentra bajo manejo de uso múltiple.`,
    detalle: 'NO',
  }
];