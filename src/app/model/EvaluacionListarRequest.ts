export class EvaluacionListarRequest {

  dniElaborador: string | null;
  rucComunidad: string | null;
  nombreElaborador: string | null
  codigoEstado: string | null
  idPlanManejo: number | null;
  pageNum: number | null;
  pageSize: number | null;
  perfil: string | null;
  filtroEstado: string | null;

    constructor(obj?: Partial<EvaluacionListarRequest>) {
        this.dniElaborador = null;
        this.rucComunidad = null;
        this.nombreElaborador = null;
        this.idPlanManejo = null;
        this.codigoEstado = null;
        this.pageNum = 1;
        this.pageSize = 10;
        this.perfil = null;
        this.filtroEstado = null;
        if (obj) Object.assign(this, obj);
    }


}

