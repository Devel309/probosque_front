export class Anexo2CensoComercialModelPOAC {
  idCensoForestalDetalle:string='0';
  alturaComercial: string = '';
  calidadFuste: string = '';
  categoria: string = '';
  codigo: string = '';
  dap: string = '';
  este: string = '';
  nArbol: string = '';
  nombreEspecies: string = '';
  norte: string = '';
  numeroFaja: string = '';
  unidadTrabajo: string = '';
  volumen: string = '';
  condicionArbol: string = '';
  bosqueSecundario: string = '';

  constructor(obj?: Partial<Anexo2CensoComercialModelPOAC>) {
    if (obj) Object.assign(this, obj);
  }
}
