import { RegenteForestalModel } from "@models";

export class PmficInformacionGeneral {
  aprovechamientoNoMaderable = null;
  apellidoMaternoElaboraDema = null;
  apellidoPaternoElaboraDema = null;
  codigoProceso = "PMFIC";
  cuenca!: string | null;
  dniElaboraDema = "";
  fechaFinDema: Date | any = "";
  fechaInicioDema: Date | any = "";
  fechaElaboracionPmfi: Date | any = "";
  idInformacionGeneralDema?: number;
  idPersonaComunidad = null;
  idPlanManejo?: number;
  idUsuarioRegistro?: number;
  lstUmf: infoGeneralDetalle[] = [];
  nombreElaboraDema = "";
  nroAnexosComunidad?: number;
  nroArbolesMaderables?: number | null;
  nroArbolesMaderablesSemilleros?: number | null;
  nroArbolesNoMaderables?: number | null;
  nroArbolesNoMaderablesSemilleros?: number | null;
  nroPersonasInvolucradas = null;
  nroResolucionComunidad = "";
  nroRucComunidad = "";
  nroTituloPropiedadComunidad = null;
  nroTotalFamiliasComunidad = null;
  observacion = "";
  subCuenca!: string | null;
  superficieHaMaderables = "";
  superficieHaNoMaderables?: string | number | null;
  totalCostoEstimado = "";
  totalIngresosEstimado = "";
  totalUtilidadesEstimado = "";
  volumenM3rMaderables?: string | number | null;
  volumenM3rNoMaderables?: string | number | null;
  idDistritoRepresentante = null;
  representanteLegal = "";
  federacionComunidad?: string;
  areaTotal = null;
  detalle = "";
  descripcion = null;
  conRegente = true;
  regente!: RegenteForestal;
  direccionLegalRepresentante = "";
  direccionLegalTitular = "";
  idContrato: any = null;
  idDistritoTitular?: number;
  vigencia?: number;
  celularTitular = "";
  correoTitular = "";
  departamentoRepresentante? = "";
  provinciaRepresentante? = "";
  distritoRepresentante? = "";
  codTipoPersona = "";
  tipoPersona = "";
  codTipoDocumentoElaborador = "";
  tipoDocumentoElaborador = "";
  codTipoDocumentoRepresentante = "";
  tipoDocumentoRepresentante = "";
  documentoRepresentante = "";
  nombreRepresentante = "";
	apellidoPaternoRepresentante = "";
	apellidoMaternoRepresentante = "";
  nombreCompletoTitular = "";
  nombreCompletoRepresentante = "";
  idPermisoForestal?: number;
  areaBosqueProduccionForestal = null;
  esReprLegal!: string;

  constructor(data?: any) {
    if (data) {
      this.aprovechamientoNoMaderable = data.aprovechamientoNoMaderable ? data.aprovechamientoNoMaderable : null;
      this.apellidoMaternoElaboraDema = data.apellidoMaternoElaboraDema ? data.apellidoMaternoElaboraDema : null;
      this.apellidoPaternoElaboraDema = data.apellidoPaternoElaboraDema ? data.apellidoPaternoElaboraDema : null;
      this.codigoProceso = "PMFIC";
      this.cuenca = data.cuenca ? data.cuenca : null;
      this.detalle = data.detalle ? data.detalle : "";
      this.fechaElaboracionPmfi = data.fechaElaboracionPmfi ? data.fechaElaboracionPmfi : "";
      this.fechaInicioDema = data.fechaInicioDema ? data.fechaInicioDema : "";
      this.fechaFinDema = data.fechaFinDema ? data.fechaFinDema : "";
      this.dniElaboraDema = data.dniElaboraDema ? data.dniElaboraDema : "";
      this.idInformacionGeneralDema = data.idInformacionGeneralDema ? data.idInformacionGeneralDema : null;
      this.idPersonaComunidad = data.idPersonaComunidad ? data.idPersonaComunidad : null;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : null;
      this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro : null;
      this.lstUmf = data.lstUmf ? data.lstUmf : [];
      this.descripcion = data.descripcion ? data.descripcion : null;
      this.areaTotal = data.areaTotal ? data.areaTotal : null;
      this.federacionComunidad = data.federacionComunidad ? data.federacionComunidad : null;
      this.nombreElaboraDema = data.nombreElaboraDema ? data.nombreElaboraDema : "";
      this.nroAnexosComunidad = data.nroAnexosComunidad ? data.nroAnexosComunidad : null;
      this.nroArbolesMaderables = data.nroArbolesMaderables === 0 ? 0 : data.nroArbolesMaderables ? data.nroArbolesMaderables : null;
      this.nroArbolesMaderablesSemilleros = data.nroArbolesMaderablesSemilleros === 0 ? 0 : data.nroArbolesMaderablesSemilleros ? data.nroArbolesMaderablesSemilleros : null;
      this.nroArbolesNoMaderables = data.nroArbolesNoMaderables === 0 ? 0 : data.nroArbolesNoMaderables ? data.nroArbolesNoMaderables : null;
      this.nroArbolesNoMaderablesSemilleros = data.nroArbolesNoMaderablesSemilleros == 0 ? 0 : data.nroArbolesNoMaderablesSemilleros ? data.nroArbolesNoMaderablesSemilleros : null;
      this.nroPersonasInvolucradas = data.nroPersonasInvolucradas ? data.nroPersonasInvolucradas : null;
      this.nroResolucionComunidad = data.nroResolucionComunidad ? data.nroResolucionComunidad : "";
      this.nroRucComunidad = data.nroRucComunidad ? data.nroRucComunidad : "";
      this.nroTituloPropiedadComunidad = data.nroTituloPropiedadComunidad ? data.nroTituloPropiedadComunidad : null;
      this.nroTotalFamiliasComunidad = data.nroTotalFamiliasComunidad ? data.nroTotalFamiliasComunidad : null;
      this.observacion = data.observacion ? data.observacion : "";
      this.subCuenca = data.subCuenca ? data.subCuenca : null;
      this.superficieHaMaderables = data.superficieHaMaderables ? data.superficieHaMaderables : "";
      this.superficieHaNoMaderables = data.superficieHaNoMaderables ? data.superficieHaNoMaderables : null;
      this.totalCostoEstimado = data.totalCostoEstimado ? data.totalCostoEstimado : "";
      this.totalIngresosEstimado = data.totalIngresosEstimado ? data.totalIngresosEstimado : "";
      this.totalUtilidadesEstimado = data.totalUtilidadesEstimado ? data.totalUtilidadesEstimado : "";
      this.volumenM3rMaderables = data.volumenM3rMaderables ? data.volumenM3rMaderables : null;
      this.volumenM3rNoMaderables = data.volumenM3rNoMaderables ? data.volumenM3rNoMaderables : null;
      this.conRegente = data.conRegente ? data.conRegente : true;
      this.regente = new RegenteForestal(data.regente);
      this.direccionLegalRepresentante = data.direccionLegalRepresentante ? data.direccionLegalRepresentante : "";
      this.direccionLegalTitular = data.direccionLegalTitular ? data.direccionLegalTitular : "";
      this.idContrato = data.idContrato ? data.idContrato : null;
      this.idDistritoRepresentante = data.idDistritoRepresentante ? data.idDistritoRepresentante : null;
      this.idDistritoTitular = data.idDistritoTitular ? data.idDistritoTitular : null;
      this.vigencia = data.vigencia ? data.vigencia : null;
      this.representanteLegal = data.representanteLegal ? data.representanteLegal : "";
      this.celularTitular = data.celularTitular ? data.celularTitular : "";
      this.correoTitular = data.correoTitular ? data.correoTitular : "";
      this.distritoRepresentante = data.distritoRepresentante ? data.distritoRepresentante : "";
      this.codTipoPersona = data.codTipoPersona ? data.codTipoPersona : "";
      this.tipoPersona = data.tipoPersona ? data.tipoPersona : "";
      this.codTipoDocumentoElaborador = data.codTipoDocumentoElaborador ? data.codTipoDocumentoElaborador : "";
      this.tipoDocumentoElaborador = data.tipoDocumentoElaborador ? data.tipoDocumentoElaborador : "";
      this.codTipoDocumentoRepresentante = data.codTipoDocumentoRepresentante ? data.codTipoDocumentoRepresentante : "";
      this.tipoDocumentoRepresentante = data.tipoDocumentoRepresentante ? data.tipoDocumentoRepresentante : "";
      this.documentoRepresentante = data.documentoRepresentante ? data.documentoRepresentante : "";
      this.nombreRepresentante = data.nombreRepresentante ? data.nombreRepresentante : "";
      this.apellidoPaternoRepresentante = data.apellidoPaternoRepresentante ? data.apellidoPaternoRepresentante : "";
      this.apellidoMaternoRepresentante = data.apellidoMaternoRepresentante ? data.apellidoMaternoRepresentante : "";
      this.nombreCompletoTitular = data.nombreCompletoTitular ? data.nombreCompletoTitular : "";
      this.nombreCompletoRepresentante = data.nombreCompletoRepresentante ? data.nombreCompletoRepresentante : "";
      this.idPermisoForestal = data.idPermisoForestal ? data.idPermisoForestal : null;
      this.areaBosqueProduccionForestal = data.areaBosqueProduccionForestal ? data.areaBosqueProduccionForestal : null;
      this.esReprLegal = data.esReprLegal ? data.esReprLegal : '';
    }
  }
}

export class infoGeneralDetalle {
  codigo: string = "";
  descripcion: string = "";
  detalle: string = "";
  codigoInformacionDet: string = "";
  tipoInformacionDet: string = "";
  vigenciaDet: number = 0;
  vigenciaFinalDet: number = 0;
  area: any;

  constructor(data?: infoGeneralDetalle) {
    if (data) {
      this.codigo = data.codigo ? data.codigo : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.codigoInformacionDet = data.codigoInformacionDet ? data.codigoInformacionDet : "";
      this.tipoInformacionDet = data.tipoInformacionDet ? data.tipoInformacionDet : "";
      this.vigenciaDet = data.vigenciaDet ? data.vigenciaDet : 0;
      this.vigenciaFinalDet = data.vigenciaFinalDet ? data.vigenciaFinalDet : 0;
      this.area = data.area ? data.area : 0;
    }
  }
}

export class RegenteForestal {
  adjuntoCertificado: string;
  apellidos: string;
  certificadoHabilitacion: string | null;
  codigoProceso: string;
  codigoTipoRegente: string | null;
  contratoSuscrito: string | null;
  domicilioLegal: string | null;
  estadoRegente: string;
  idPlanManejo: number;
  idPlanManejoRegente: number;
  idUsuarioRegistro: number;
  nombres: string;
  numeroDocumento: string;
  numeroInscripcion: string;
  numeroLicencia: string;
  periodo: string;
  idRegente!: number | null;

  constructor(obj?: Partial<RegenteForestal>) {
    this.idRegente = 0;
    this.codigoTipoRegente = "";
    this.numeroLicencia = "";
    this.domicilioLegal = "";
    this.contratoSuscrito = "";
    this.certificadoHabilitacion = "";
    this.numeroInscripcion = "";
    this.nombres = "";
    this.apellidos = "";
    this.numeroDocumento = "";
    this.estadoRegente = "";
    this.adjuntoCertificado = "";
    this.idPlanManejoRegente = 0;
    this.idPlanManejo = 0;
    this.idUsuarioRegistro = 0;
    this.periodo = "";
    this.codigoProceso = "PMFIC";

    if (obj) Object.assign(this, obj);
  }
}
