export abstract class UrlFormatos {
  static readonly POAC_17: string = "/assets/plantillas/POAC/Formato5.docx";
  static readonly CARGA_MASIVA_PMFI: string = "Plantilla_CargaInventario_PMFI_03-05-2022_121448.xlsx";
  static readonly CARGA_MASIVA_DEMA: string = "Plantilla_CargaInventario_DEMA_04-05-2022_181115.xlsx";

  static readonly CARGA_MASIVA_POCC: string = "Plantilla_CargaCenso_Aprov_POCC_13-05-2022_224940.xlsx"; 
  static readonly CARGA_POCC_6_2_3: string = "plantilla-POCC-ActividadAprovechamiento-ListaEspecies_14-05-2022_180231.xlsx"; 
  static readonly CARGA_POCC_CRONOGRAMA: string = "plantilla-POCC-Cronograma_13-05-2022_230542.xlsx"; 
  static readonly CARGA_POCC_ANEXO2: string = "plantilla-POCC-Anexo2_13-05-2022_231428.xlsx"; 

  static readonly CARGA_MASIVA_PGMFA: string = "Plantilla_CargaCenso_Potencial_PGMFA_13-05-2022_231019.xlsx"; 
  static readonly CARGA_PGMFA_RENTABILIDAD: string = "plantilla-Rentabilidad-PGMFA_13-05-2022_232218.xlsx"; 
  static readonly CARGA_PGMFA_CRONOGRAMA: string = "plantilla-PGMFA-cronograma_13-05-2022_232431.xlsx"; 
  static readonly CARGA_PGMFA_INVENTARIO_ESPECIES: string = "plantilla-PGMFA-InventarioEspeciesMaderables_13-05-2022_233831.xlsx"; 
  static readonly CARGA_PGMFA_INVENTARIO_FUSTALES: string = "plantilla-PGMFA-InventarioFrutales_13-05-2022_234012.xlsx"; 
  static readonly CARGA_PGMFA_ANEXO2: string = "plantilla_Anexo2_PGMFA_13-05-2022_233243.xlsx"; 
  static readonly CARGA_PGMFA_ANEXO3: string = "plantilla_Anexo3_PGMFA_15-05-2022_001206.xlsx"; 
  static readonly CARGA_PGMFA_ANEXO4: string = "Plantilla_CargaInventario_PGMFA_Anexo4_15-05-2022_003447.xlsx"; //
  static readonly CARGA_PGMFA_7_1_2: string = "plantilla-PGMFA-ManejoBosque_14-05-2022_180355.xlsx"; 

  static readonly CARGA_MASIVA_POAC: string = "Plantilla_CargaInventario_POAC_Aprovecha_13-05-2022_234452.xlsx"; 
  static readonly CARGA_POAC_7_2_1: string = "plantilla-POAC-ProteccionBosque-MatrizImpactosAmbientales_13-05-2022_234816.xlsx"; 
  static readonly CARGA_POAC_RENTABILIDAD: string = "plantilla-POAC-Rentabilidad_13-05-2022_235304.xlsx"; 

  static readonly CARGA_MASIVA_PMFIC: string = "Plantilla_CargaInventario_PMFIC_Potencial_13-05-2022_235503.xlsx"; 
  static readonly CARGA_PMFIC_EVALUACION: string = "Plantilla formato Plan de Gestion Ambiental PMFIC_14-05-2022_235316.xlsx"; //
 
  static readonly CARGA_MASIVA_POPAC: string = "Plantilla_CargaInventario_POPAC_Potencial_14-05-2022_000046.xlsx"; 
}
