import { AuditoriaModel } from "./auditoria";
export interface RegenteModel extends AuditoriaModel{ 
    idRegente: number;
    codTipoRegente: string;
    numeroLicencia: string;
    domicilioLegal: string;
    contratoSuscrito: string;
    certificadoHabilitacion: string;
    numeroInscripcion: string;
    adjuntoContrato: string;
    adjuntoCertificado: string;
    idPersona: number;
    idPlanManejo: number;

    idProcesoPostulacion: number;
    idRegenteForestal : number;
    idContrato :number;
    codigoTh :string;
    descripcion :string;
    idSolicitud :number;
    idProceso :number;
    idTipoPlan:number;
    
}
