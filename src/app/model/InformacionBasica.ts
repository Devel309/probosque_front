import { AuditoriaModel } from "./auditoria";
export interface InformacionBasicaModel extends AuditoriaModel{
    idInfBasica: number;
}