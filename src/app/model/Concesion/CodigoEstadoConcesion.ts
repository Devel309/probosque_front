export abstract class CodigoEstadoConcesion {
    static readonly BORRADOR: string = "SCESTBORR";
    static readonly PRESENTADA: string = "SCESTPRESN";
    static readonly OBSERVADO_REQ: string = "SCESTREQO";
    static readonly NO_ADMITIDO: string = "SCESTNADM";
    static readonly EVALUACION: string = "SCESTEEVAL";
    static readonly OBSERVADO_SOL: string = "SCESTSOLO";
    static readonly PUBLICADO: string = "SCESTPUB";
    static readonly FIN_OPOSICION: string = "SCESTFINO";
}
