export class SolicitudConcesionGeometriaModel {
  
    idSolicitudConcesionGeometria: number | null;
    idSolicitudConcesion: number | null;
    idArchivo: number | null;
    tipoGeometria: string | null;
    codigoGeometria: string | null;
    codigoSeccion: string | null;
    codigoSubSeccion: string | null;
    nombreCapa:string  | null;
    colorCapa:string | null;
    geometry_wkt:string | null;
    srid:number  | null;
    idUsuarioRegistro:number | null;
  
  
    constructor(obj?: Partial<SolicitudConcesionGeometriaModel>) {
      this.idSolicitudConcesionGeometria = null;
      this.idSolicitudConcesion = null;
      this.idArchivo = null;
      this.tipoGeometria = null;
      this.codigoGeometria = null;
      this.codigoSeccion = null;
      this.codigoSubSeccion = null;
      this.nombreCapa = null;
      this.colorCapa = null;
      this.geometry_wkt = null;
      this.srid = null;
      this.idUsuarioRegistro = null;
  
      if (obj) Object.assign(this, obj);
    }
  
  
  }