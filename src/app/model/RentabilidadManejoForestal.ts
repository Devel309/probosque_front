import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
import { RentabilidadManejoForestalDetalleModel } from './RentabilidadManejoForestalDetalle'

export interface RentabilidadManejoForestalModel extends AuditoriaModel {
  idRentManejoForestal: number;
  idTipoRubro: number;
  rubro: String;
  descripcion: String;
  planManejo: PlanManejoModel;
  listRentabilidadManejoForestalDetalle: RentabilidadManejoForestalDetalleModel[];
  idPlanManejo:number;
  id: number
  indice: number;
}
