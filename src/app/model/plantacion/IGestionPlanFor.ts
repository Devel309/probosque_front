interface optionPage {
  pageNum: number | null;
  pageSize: number | null;
}

export interface IFiltroBjaSolPla extends optionPage{
  idSolicitudPlantacion: number | null;
  tipoModalidad: string | null;
  nombreSolicitante: string | null;
  numeroDocumentoSolicitante: string | null;
  estadoSolicitud: string | null;
  remitir: Boolean | null;
  codigoPerfil : string | null;
}

export interface IFiltroBjsFiscaPlan extends optionPage{
  idSolicitudPlantacion: number | null;
  tipoModalidad: string | null;
  nombreSolicitante: string | null;
  numeroDocumentoSolicitante: string | null;
  estadoSolicitud: string | null;
  codigoPerfil : string | null;
}

export interface IfamiliarPersonaBpf {
  idSolPlantacionForestal: number;
  idSolPlantacionForestalPersona: number
  dni: string;
  nombres: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  idUsuarioRegistro: number;
}
