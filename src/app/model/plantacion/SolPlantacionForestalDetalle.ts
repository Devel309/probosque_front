export class SolPlantacionForestalDetalle {
    constructor(data?: any) {

        if (data) {
            this.idSolPlantForest = data.idSolPlantForest;
            this.idSolPlantForestDetalle = data.idSolPlantForestDetalle;
            this.coordenadaEste = data.coordenadaEste;
            this.coordenadaNorte = data.coordenadaNorte;
            this.especieNombreCientifico = data.especieNombreCientifico;

            this.idEspecie = data.idEspecie;
            this.especieNombreComun = data.especieNombreComun;
            this.totalArbolesExistentes = data.totalArbolesExistentes;
            this.produccionEstimada = data.produccionEstimada;
            this.numVertice = data.numVertice;
            this.idUsuarioRegistro = data.idUsuarioRegistro;
            this.unidadMedida=data.unidadMedida;
            this.isChange = data.isChange;
            return;
        }
    }
 
    idSolPlantForest: number = 0;
    idSolPlantForestDetalle: number = 0;
    coordenadaEste: any = null;
    coordenadaNorte: any = null;
    especieNombreComun: string = "";
    especieNombreCientifico: string = "";
    produccionEstimada: number | null = 0;
    totalArbolesExistentes: number | null = 0;
    idEspecie :number = 0;
    numVertice:any = null;
    idUsuarioRegistro: any = null;
    unidadMedida:string="";
    isChange: any = false;
}