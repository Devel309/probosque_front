export abstract class CodigoEstadoPlantFor {
    static readonly BORRADOR: string = "SPFESTBORR";
    static readonly PRESENTADA: string = "SPFESTPRES";
    static readonly OBSERVADO_REQ: string = "SPFESTREQO";
    static readonly EVALUACION: string = "SPFESTEVAL";
    static readonly OBSERVADO_SOL: string = "SPFESTSOLO";
    static readonly APROBADA_SOL: string = "SPFESTSOLA";
    static readonly DENEGADA_SOL: string = "SPFESTSOLD";
}
