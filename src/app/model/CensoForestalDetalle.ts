import { AuditoriaDto } from "./auditoria";

export class CensoForestalDetalle extends AuditoriaDto {

  idCensoForestalDetalle: number | null;
  parcelaCorte: number | null;
  bloque: number | null;
  faja: number | null;
  idTipoRecurso: string;
  idTipoBosque: number | null;
  idTipoEvaluacion: number | null;
  correlativo: string;
  idCodigoEspecie: number | null;
  idNombreComun: string;
  idNombreCientifico: string;
  descripcionOtros: string;
  idEspecieCodigo: number | null;
  idTipoArbol: string;
  dap: number | null;
  alturaComercial: number | null;
  alturaTotal: number | null;
  codigoArbolCalidad: string;
  volumen: number | null;
  factorForma: number | null;
  categoriaDiametrica: number | null;
  este: any;  //Geometry;
  norte: any; //Geometry;
  zona: number | null;
  nota: string;
  cantidad: number | null;
  codigoUnidadMedida: string;
  productoTipo: string;
  idCensoForestal: number | null;

  constructor(obj?: Partial<CensoForestalDetalle>) {
    super();

    this.idCensoForestalDetalle = null;
    this.parcelaCorte = null;
    this.bloque = null;
    this.faja = null;
    this.idTipoRecurso = '';
    this.idTipoBosque = null;
    this.idTipoEvaluacion = null;
    this.correlativo = '';
    this.idCodigoEspecie = null;
    this.idNombreComun = '';
    this.idNombreCientifico = '';
    this.descripcionOtros = '';
    this.idEspecieCodigo = null;
    this.idTipoArbol = '';
    this.dap = null;
    this.alturaComercial = null;
    this.alturaTotal = null;
    this.codigoArbolCalidad = '';
    this.volumen = null;
    this.factorForma = null;
    this.categoriaDiametrica = null;
    this.este = null;
    this.norte = null;
    this.zona = null;
    this.nota = '';
    this.cantidad = null;
    this.codigoUnidadMedida = '';
    this.productoTipo = '';
    this.idCensoForestal = null;
    
    if (obj) Object.assign(this, obj);
  }


}