export class PotencialProduccionCabeceraModel {
  constructor(data?: any) {
    if (data) {
      this.idPotProdForestal = data.idPotProdForestal;
      this.idPlanManejo = data.idPlanManejo;
      this.codigoTipoPotProdForestal = data.codigoTipoPotProdForestal;
      this.idTipoBosque = data.idTipoBosque;
      this.tipoBosque = data.tipoBosque;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.codigoSubTipoPotencialProdForestal = data.codigoSubTipoPotencialProdForestal
      this.listPotencialProduccion = data.listPotencialProduccion;
      this.disenio = data.disenio;
      this.diametroMinimoInventariadaCM = data.diadiametroMinimoInventariadaCM;
      this.intensidadMuestreoPorcentaje = data.intensidadMuestreoPorcentaje;
      this.tamanioParcela = data.tamanioParcela;
      this.distanciaParcela = data.distanciaParcela;
      this.nroParcela = data.nroParcela;
      this.totalAreaInventariada = data.totalAreaInventariada;
      this.metodoMuestreo = data.metodoMuestreo;
      this.rangoDiametroCM = data.rangoDiametroCM;
      this.areaMuestreada = data.areaMuestreada;
      this.anexo = data.anexo;
      this.errorMuestreo = data.errorMuestreo;
      return;
    }
  }

  idPotProdForestal: number = 0;
  idPlanManejo: number = 0;
  codigoTipoPotProdForestal: string = "";
  idTipoBosque: number = 0;
  tipoBosque: string = "";
  idUsuarioRegistro: number = 0;
  codigoSubTipoPotencialProdForestal: string = '';
  listPotencialProduccion: potencialProduccionDetalleModel[] = [];
  disenio!: string;
  diametroMinimoInventariadaCM!: number;
  intensidadMuestreoPorcentaje!: number;
  tamanioParcela!: number;
  distanciaParcela!: number;
  nroParcela!: number;
  totalAreaInventariada!: number;
  metodoMuestreo!: string;
  rangoDiametroCM!: number;
	areaMuestreada!: number;
  anexo!: string;
  errorMuestreo!: number;
}

export class potencialProduccionDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.idPotProdForestalDet = data.idPotProdForestalDet
        ? data.idPotProdForestalDet
        : 0;
      this.codigoTipoPotencialProdForestalDet = data.codigoTipoPotencialProdForestalDet
        ? data.codigoTipoPotencialProdForestalDet
        : "";
      this.idEspecie = data.idEspecie;
      this.especie = data.especie;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.codigoSubTipoPotencialProdForestalDet = data.codigoSubTipoPotencialProdForestalDet;
      this.listPotencialProduccionVariable = data.listPotencialProduccionVariable;
      return;
    }
  }

  idPotProdForestalDet: number = 0;
  codigoTipoPotencialProdForestalDet: string = "";
  idEspecie: number = 0;
  especie: string = "";
  idUsuarioRegistro: number = 0;
  codigoSubTipoPotencialProdForestalDet: string = '';
  listPotencialProduccionVariable: VariablesModel[] = [];
}

export class VariablesModel {
  constructor(data?: any) {
    if (data) {
      this.accion = data.accion
      this.totalHa = data.totalHa ? data.totalHa : 0;
      this.variable = data.variable ? data.variable : "";
      this.nuTotalTipoBosque = data.nuTotalTipoBosque;
      this.idVariable = data.idVariable;
      this.idUsuarioRegistro = data.idUsuarioRegistro
      return;
    }
  }
  accion:number = 0;
  variable: string = "";
  totalHa: number = 0;
  nuTotalTipoBosque: number = 0;
  idVariable: number = 0;
  idUsuarioRegistro: number = 0;
}
