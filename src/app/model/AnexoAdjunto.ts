import { AuditoriaModel } from "./auditoria";

export interface AnexoAdjuntoModel extends AuditoriaModel{
    idAnexoAdjunto: number;
    idAnexo: number;
    codigoTab:string;
    adjuntaAnexo:boolean;
    idPlanManejo:number;

}