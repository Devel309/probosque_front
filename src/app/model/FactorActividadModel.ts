export class FactorActividadModel {


  idEvaluacionAmbiental: number;
  idEvalAprovechamiento: number;
  idEvalActividad: number;
  idPlanManejo: number;
  valorEvaluacion: string;
  descripcion: string;
  medidasMitigacion: string;
  responsableSecundario: string;
  contingencia: string;
  auxiliar: string;
  adjunto: string;
  idUsuarioRegistro: number;
  fechaRegistro: string;
  idUsuarioModificacion: number;
  fechaModificacion: string;
  idUsuarioElimina: number;
  fechaElimina: string;

  constructor(obj?: Partial<FactorActividadModel>) {
    this.idEvaluacionAmbiental = 0;
    this.idEvalAprovechamiento = 0;
    this.idEvalActividad = 0;
    this.idPlanManejo = 0;
    this.valorEvaluacion = "N";
    this.descripcion = '';
    this.medidasMitigacion = '';
    this.responsableSecundario = '';
    this.contingencia = '';
    this.auxiliar = '';
    this.adjunto = '';
    this.idUsuarioRegistro = 0;
    this.fechaRegistro = '';
    this.idUsuarioModificacion = 0;
    this.fechaModificacion = '';
    this.idUsuarioElimina = 0;
    this.fechaElimina = '';

    if (obj) Object.assign(this, obj);
  }
}