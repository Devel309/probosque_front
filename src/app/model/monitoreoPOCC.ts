export class MonitoreoPOCCModel {
  constructor(data?: any) {
    if (data) {
      this.codigoMonitoreo = data.codigoMonitoreo ? data.codigoMonitoreo : " ";
      this.codigoMonitoreoDet = data.codigoMonitoreoDet
        ? data.codigoMonitoreoDet
        : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.idMonitoreo = data.idMonitoreo ? data.idMonitoreo : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion
        ? data.idUsuarioModificacion
        : 0;
      this.observacion = data.observacion ? data.observacion : "";
      this.monitoreo = data.monitoreo ? data.monitoreo : "";
      this.indicador = data.indicador ? data.indicador : "";
      this.frecuencia = data.frecuencia ? data.frecuencia : "";
      this.responsable = data.responsable ? data.responsable : "";
      this.lstDetalle = data.lstDetalle;
      return;
    }
  }

  codigoMonitoreo: string = "";
  codigoMonitoreoDet: string = "";
  descripcion: string = "";
  idMonitoreo: number = 0;
  idMonitoreoDet: number = 0;
  idPlanManejo: number = 0;
  idUsuarioModificacion: number = 0;
  idUsuarioRegistro: number = 0;
  observacion: string = "";
  monitoreo: string = "";
  indicador: string = "";
  frecuencia: string = "";
  responsable: string = "";
  lstDetalle: lstDetalleModel[] = [];
}

export class lstDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.idMonitoreoDetalle = data.idMonitoreoDetalle
        ? data.idMonitoreoDetalle
        : 0;
      this.actividad = data.actividad ? data.actividad : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.codigoMonotoreoDet = data.codigoMonotoreoDet
        ? data.codigoMonotoreoDet
        : "";
      this.responsable = data.responsable ? data.responsable : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idUsuarioModificacion = data.idUsuarioModificacion
        ? data.idUsuarioModificacion
        : 0;
      this.observacion = data.observacion ? data.observacion : "";
      this.responsable = data.responsable ? data.responsable : "";
      this.operacion = data.operacion ? data.operacion : "<";
      return;
    }
  }

  idMonitoreoDetalle: number = 0;
  actividad: string = "";
  codigoMonotoreoDet: string = "";
  descripcion: string = "";
  idUsuarioModificacion: number = 0;
  idUsuarioRegistro: number = 0;
  observacion: string = "";
  responsable: string = "";
  operacion: string = "";
}
