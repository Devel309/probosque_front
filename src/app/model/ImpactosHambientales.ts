
import { AuditoriaModel } from "./auditoria";

export interface ImpactosHambientalesModel extends AuditoriaModel{
    factores:String;
    impactosIdentificados: String;
    censo:string;
    demarcacionLinderos:string;
    construccionCampamentos:string;
    construccionCaminos:string;
    tala:string;
    arrastre:string;
    otros:string;
}