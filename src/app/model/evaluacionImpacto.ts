export interface AprovechamientoResponse {
  codTipoAprovechamiento?: string;
  estado?: string;
  fechaElimina?: string;
  fechaModificacion?: string;
  fechaRegistro?: string;
  idEvalAprovechamiento?: number;
  idUsuarioElimina?: string;
  idUsuarioModificacion?: string;
  idUsuarioRegistro?: string;
  nombreAprovechamiento?: string;
  pageNum?: string;
  pageSize?: string;
  search?: string;
  startIndex?: string;
  tipoAprovechamiento?: string;
  tipoNombreAprovechamiento?: string;
  totalPage?: string;
  totalRecord?: string;
  valorEvaluacion?: boolean | string;
  idEvaluacionAmbiental?: number;
}

