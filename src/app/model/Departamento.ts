export interface DepartamentoModel { 
    idDepartamento:number;
    nombreDepartamento:String;
    codDepartamento:number;
    codDepartamentoInei:number;
    codDepartamentoReniec:String;
    codDepartamentoSunat:String;
    
}