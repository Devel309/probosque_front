
import { AuditoriaModel } from "./auditoria";

export interface OrganizacionDesarrolloModel extends AuditoriaModel {
    idOrganizacionDesarrollo: number;
    actividadRealizada: string;
    formaOrganizacion:string;
}