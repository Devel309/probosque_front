import { AprovechamientoResponse } from "@models";

export class FactorModel {

  idEvalActividad: number;
  codTipoActividad: string;
  tipoActividad: string;
  tipoNombreActividad: string;
  nombreActividad: string;
  medidasControl: string;
  medidasMonitoreo: string;
  frecuencia: string;
  acciones: string;
  responsable: string;

  estado: string;
  idUsuarioRegistro: number;
  fechaRegistro: string;
  idUsuarioModificacion: number;
  fechaModificacion: string;
  idUsuarioElimina: number;
  fechaElimina: string;

  aprovechamiento?: AprovechamientoResponse[];
  preAprovechamiento?: AprovechamientoResponse[];
  postAprovechamiento?: AprovechamientoResponse[];

  constructor(obj?: Partial<FactorModel>) {
    this.idEvalActividad = 0;
    this.codTipoActividad = "";
    this.tipoActividad = "";
    this.tipoNombreActividad = "";
    this.nombreActividad = "";
    this.medidasControl = "";
    this.medidasMonitoreo = "";
    this.frecuencia = "";
    this.acciones = "";
    this.responsable = "";

    this.estado = '';
    this.idUsuarioRegistro = 0;
    this.fechaRegistro = "";
    this.idUsuarioModificacion = 0;
    this.fechaModificacion = "";
    this.idUsuarioElimina = 0;
    this.fechaElimina = "";

    this.aprovechamiento = [];
    this.preAprovechamiento = [];
    this.postAprovechamiento = [];

    if (obj) Object.assign(this, obj);
  }
}