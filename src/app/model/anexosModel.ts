export interface Anexos {
  altura: number;
  codArbol: string;
  diametro: number;
  estado: string;
  este: string;
  fechaElimina: string;
  fechaModificacion: string;
  fechaRegistro: string;
  idUsuarioElimina: number;
  idUsuarioModificacion: number;
  idUsuarioRegistro: number;
  nombreComun: number;
  norte: string;
  pageNum: number;
  pageSize: number;
  search: string;
  startIndex: number;
  tipoArbol: string;
  totalPage: number;
  totalRecord: number;
  volumen: number;
  observaciones?: string
}
