export interface ProvinciaModel { 
    idProvincia:number;
    nombreProvincia:String;
    codProvincia:String;
    codDepartamento:String;
    codProvinciaInei:String;
    codProvinciaReniec:String;
    codProvinciaSunat:String;
    idDepartamento:number;
}