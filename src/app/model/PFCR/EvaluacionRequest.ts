export class EvaluacionRequest {

  fecha :string | null;
  fechaRegistro :Date | null;
  codigoEstado: string | null;
  idPermisoForestal: number | null;
  pageNum: number | null;
  pageSize: number | null;
  perfil: string | null;
  filtroEstado: string | null;

    constructor(obj?: Partial<EvaluacionRequest>) {
        this.fecha = null;
      this.fechaRegistro = null;
        this.idPermisoForestal = null;
        this.codigoEstado = null;
        this.pageNum = 1;
        this.pageSize = 10;
        this.perfil = null;
        this.filtroEstado = null;
        if (obj) Object.assign(this, obj);
    }


}

