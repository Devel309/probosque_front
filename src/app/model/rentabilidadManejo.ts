export class RentabilidadPGMFA {
  constructor(data?: any) {
    if (data) {
      this.codigoRentabilidad = data.codigoRentabilidad
        ? data.codigoRentabilidad
        : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.estado = data.estado ? data.estado : "A";
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idRentManejoForestal = data.idRentManejoForestal
        ? data.idRentManejoForestal
        : 0;
      this.idTipoRubro = data.idTipoRubro ? data.idTipoRubro : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.listRentabilidadManejoForestalDetalle =
        data.listRentabilidadManejoForestalDetalle;
      this.rubro = data.rubro ? data.rubro : "";
    }
  }

  codigoRentabilidad?: string;
  observacion?: string;
  descripcion?: string;
  estado?: string;
  idPlanManejo?: number;
  idRentManejoForestal?: number;
  idTipoRubro?: number;
  idUsuarioRegistro?: number;
  listRentabilidadManejoForestalDetalle?: ListRentabilidadManejoForestalDetalle[];
  rubro?: string;
}

export class ListRentabilidadManejoForestalDetalle {
  constructor(data?: any) {
    if (data) {
      this.idRentManejoForestalDet = data.idRentManejoForestalDet
        ? data.idRentManejoForestalDet
        : 0;
      this.anio = data.anio ? data.anio : 0;
      this.estado = data.estado ? data.estado : "A";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.monto = data.monto ? data.monto : 0;
      this.mes = data.mes ? data.mes : 0;
    }
  }
  idRentManejoForestalDet?: number;
  anio?: number;
  estado?: string;
  idUsuarioRegistro?: number;
  monto?: number;
  mes?: number;
}
