export class ResultadosEvaluacionDetalle {
  constructor(data?: any) {
    if (data) {
      this.idEvalResultadoDet = data.idEvalResultadoDet
        ? data.idEvalResultadoDet
        : 0;
      this.codResultadoDet = data.codResultadoDet ? data.codResultadoDet : "";
      this.tipoBosque = data.tipoBosque ? data.tipoBosque : 0;
      this.nuAccionInforme = data.nuAccionInforme ? data.nuAccionInforme : 0;
      this.tipoDocumento = data.tipoDocumento ? data.tipoDocumento : "";
      this.nuDocumento = data.nuDocumento ? data.nuDocumento : 0;
      this.nombreFirmante = data.nombreFirmante ? data.nombreFirmante : "";
      this.paternoFirmante = data.paternoFirmante ? data.paternoFirmante : "";
      this.maternoFirmante = data.maternoFirmante ? data.maternoFirmante : "";
      this.cargoFirmante = data.cargoFirmante ? data.cargoFirmante : "";
      this.fechaNotificacion = data.fechaNotificacion
        ? data.fechaNotificacion
        : null;
      this.fechaResolucion = data.fechaResolucion ? data.fechaResolucion : null;
      this.fechaConsentimiento = data.fechaConsentimiento ? data.fechaConsentimiento : null;
      this.asunto = data.asunto ? data.asunto : "";
      this.perfil = data.perfil ? data.perfil : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.nuDocumentoFirmante = data.nuDocumentoFirmante
        ? data.nuDocumentoFirmante
        : 0;
      return;
    }
  }

  idEvalResultadoDet: number = 0;
  codResultadoDet: string = "";
  tipoBosque: number = 0;
  nuAccionInforme: number = 0;
  tipoDocumento: string = "";
  nuDocumento: number = 0;
  nombreFirmante: string = "";
  paternoFirmante: string = "";
  maternoFirmante: string = "";
  cargoFirmante: string = "";
  fechaNotificacion!: Date | null;
  fechaResolucion!: Date | null;
  fechaConsentimiento!: Date | null;
  asunto: string = "";
  perfil: string = "";
  observacion: string = "";
  detalle: string = "";
  descripcion: string = "";
  idUsuarioRegistro: number = 0;
  nuDocumentoFirmante: number = 0;
}
