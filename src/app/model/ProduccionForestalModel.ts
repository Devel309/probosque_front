export class PotencialForestal {
    constructor(data?: any) {
      if (data) {
        this.nombreComun = data.nombreComun;
        return;
      }
    }
    nombreComun: string = "";
    array: Arboles[] = [];
  }
  
  export class Arboles {
    constructor(data?: any) {
      if (data) {
        return;
      }
    }
    var: string = "";
    Dap30a39: number = 0;
    Dap40a49: number = 0;
    Dap50a59: number = 0;
    Dap60a69: number = 0;
    Dap70a79: number = 0;
    Dap80a89: number = 0;
    Dap90aMas: number = 0;
    TotalTipoBosque: number = 0;
    TotalPorArea: number = 0;
    porcentaje: number = 0;
  }
  