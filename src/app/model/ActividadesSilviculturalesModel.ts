export interface Actividades {
  actividad: string;
  anexo: string;
  codActividad: string;
  descripcion: string;
  detalle: Detalle[];
  estado: string;
  fechaElimina: string;
  fechaModificacion: string;
  fechaRegistro: string;
  idActividadSilvicultural: string;
  idPlanManejo: number;
  idUsuarioElimina: string;
  idUsuarioModificacion: string;
  idUsuarioRegistro: string;
  monitoreo: string;
  observacion: string;
  pageNum: string;
  pageSize: string;
  search: string;
  startIndex: string;
  totalPage: string;
  totalRecord: string;
}

export interface Detalle {
  accion?: boolean;
  descripcionDetalle?: string;
  estado?: string;
  fechaElimina?: string;
  fechaModificacion?: string;
  fechaRegistro?: string;
  idActividadSilviculturalDet?: number;
  idTipo?: string;
  idTipoTratamiento?: string;
  idUsuarioElimina?: string;
  idUsuarioModificacion?: string;
  idUsuarioRegistro?: string;
  justificacion?: string;
  pageNum?: string;
  pageSize?: string;
  search?: string;
  startIndex?: string;
  tipoTratamiento?: string;
  totalPage?: string;
  totalRecord?: string;
  tratamiento?: string;
  descripcion?: string;
  codActividad?: string;
  idActividadSilvicultural?: number;
  personal?: string;
  insumo?: string;
  equipo?: string;
  actividad?: string;
  observacionDetalle?: string;
}

export class ActividadSilvicultural {
  constructor(data?: any) {
    if (data) {
      this.idTipo = data.idTipo ? data.idTipo : "";
      this.accion = data.accion ? data.accion : "";
      this.descripcionDetalle = data.descripcionDetalle
        ? data.descripcionDetalle
        : "";
      this.equipo = data.equipo
        ? data.equipo
        : "0";
      this.actividad = data.actividad ? data.actividad : 0;
      this.idActividadSilviculturalDet = data.idActividadSilviculturalDet
        ? data.idActividadSilviculturalDet
        : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.observacionDetalle = data.observacionDetalle
        ? data.observacionDetalle
        : "";
      this.personal = data.personal
      ? data.personal
      : null;
      this.detalle = data.detalle
      ? data.detalle
      : null;

      return;
    }
  }
  idTipo?: string;
  accion?: boolean;
  descripcionDetalle?: string;
  actividad?: string;
  idActividadSilviculturalDet?: number;
  idUsuarioRegistro?: number;
  observacionDetalle?: string;
  personal?: string;
  equipo?: string;
  detalle?: string;
}


export class ActividadSilviculturales {
  constructor(data?: any) {
    if (data) {
      this.idTipo = data.idTipo ? data.idTipo : "";
      this.idTipoTratamiento = data.idTipoTratamiento ? data.idTipoTratamiento : "";
      this.accion = data.checked ? data.checked : false;
      this.descripcionDetalle = data.descripcionDetalle
        ? data.descripcionDetalle
        : "";
      this.actividad = data.actividad ? data.actividad : 0;
      this.idActividadSilviculturalDet = data.idActividadSilviculturalDet
        ? data.idActividadSilviculturalDet
        : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.observacionDetalle = data.observacionDetalle
        ? data.observacionDetalle
        : "";

      return;
    }
  }
  idTipo?: string;
  idTipoTratamiento?: string;
  accion?: boolean;
  descripcionDetalle?: string;
  actividad?: string;
  idActividadSilviculturalDet?: number;
  idUsuarioRegistro?: number;
  observacionDetalle?: string;
}

