export class CensoMaderable {
  constructor(data?: any) {
    if (data) {
      this.idCensoForestalDetalle = data.idCensoForestalDetalle
        ? data.idCensoForestalDetalle
        : 0;
      this.parcelaCorte = data.parcelaCorte ? data.parcelaCorte : 0;
      this.faja = data.faja ? data.faja : 0;
      this.bloque = data.bloque ? data.bloque : 0;
      this.idTipoBosque = data.idTipoBosque ? data.idTipoBosque : 0;
      this.idCodigoEspecie = data.idCodigoEspecie ? data.idCodigoEspecie : 0;
      this.idnombreComun = data.idnombreComun ? data.idnombreComun : 0;
      this.idnombreCientifico = data.idnombreCientifico
        ? data.idnombreCientifico
        : 0;
      this.dap = data.dap ? data.dap : 0;
      this.alturaComercial = data.alturaComercial ? data.alturaComercial : 0;
      this.alturaTotal = data.alturaTotal ? data.alturaTotal : 0;
      this.volumen = data.volumen ? data.volumen : 0;
      this.factorForma = data.factorForma ? data.factorForma : 0;
      this.categoriaDiametrica = data.categoriaDiametrica
        ? data.categoriaDiametrica
        : 0;
      this.zona = data.zona ? data.zona : 0;
      this.cantidad = data.cantidad ? data.cantidad : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.idCensoForestal = data.idCensoForestal ? data.idCensoForestal : 0;
      this.superficieComercial = data.superficieComercial
        ? data.superficieComercial
        : 0;
      this.numeroCorrelativoArbol = data.numeroCorrelativoArbol
        ? data.numeroCorrelativoArbol
        : 0;
      this.volumenTala = data.volumenTala ? data.volumenTala : 0;
      this.volumenTrozado = data.volumenTrozado ? data.volumenTrozado : 0;

      this.idTipoRecurso = data.idTipoRecurso ? data.idTipoRecurso : "";
      this.correlativo = data.correlativo ? data.correlativo : "";
      this.descripcionOtros = data.descripcionOtros
        ? data.descripcionOtros
        : "";
      this.codigoTipoArbol = data.codigoTipoArbol ? data.codigoTipoArbol : "";
      this.codigoArbolCalidad = data.codigoArbolCalidad
        ? data.codigoArbolCalidad
        : "";
      this.nota = data.nota ? data.nota : "";
      this.codigoUnidadMedida = data.codigoUnidadMedida
        ? data.codigoUnidadMedida
        : "";
      this.productoTipo = data.productoTipo ? data.productoTipo : "";
      this.nombreComun = data.nombreComun ? data.nombreComun : "";
      this.nombreCientifico = data.nombreCientifico
        ? data.nombreCientifico
        : "";
      this.nombreNativo = data.nombreNativo ? data.nombreNativo : "";

      this.nombreComercial = data.nombreComercial ? data.nombreComercial : "";
      this.nombreAlterno = data.nombreAlterno ? data.nombreAlterno : "";
      this.familia = data.familia ? data.familia : "";
      this.estadoArbol = data.estadoArbol ? data.estadoArbol : "";
      this.condicionArbol = data.condicionArbol ? data.condicionArbol : "";
      this.unidadTrabajo = data.unidadTrabajo ? data.unidadTrabajo : "";
      this.codigoDeArbol = data.codigoDeArbol ? data.codigoDeArbol : "";
      this.bloqueQuinquenal = data.bloqueQuinquenal
        ? data.bloqueQuinquenal
        : "";
      this.parcelaCorta = data.parcelaCorta ? data.parcelaCorta : "";

      this.coorEste = data.coorEste ? data.coorEste : "";
      this.coorNorte = data.coorNorte ? data.coorNorte : "";
      this.fechaTala = data.fechaTala ? data.fechaTala : "";
      this.fechaTrozado = data.fechaTrozado ? data.fechaTrozado : "";

      this.codigoEstadoArbol = data.codigoEstadoArbol
        ? data.codigoEstadoArbol
        : "";
      this.tipoArbol = data.tipoArbol ? data.tipoArbol : "";
      this.productoDescr = data.productoDescr ? data.productoDescr : "";
      this.fechaRealizacion = data.fechaRealizacion ? data.fechaRealizacion : null;
    }
  }

  idCensoForestalDetalle: number | null = 0;
  parcelaCorte: number = 0;
  bloque: number = 0;
  faja: number = 0;
  idTipoRecurso: string = "";
  idTipoBosque: number = 0;
  correlativo: string = "";
  idCodigoEspecie: number = 0;
  idnombreComun: number = 0;
  idnombreCientifico: number = 0;
  descripcionOtros: string = "";
  codigoTipoArbol: string = "";
  dap: number = 0;
  alturaComercial: number = 0;
  alturaTotal: number = 0;
  codigoArbolCalidad: string = "";
  volumen: number = 0;
  factorForma: number = 0;
  categoriaDiametrica: number = 0;
  zona: number = 0;
  nota: string = "";
  cantidad: number = 0;
  codigoUnidadMedida: string = "";
  productoTipo: string = "";
  idUsuarioRegistro: number = 0;
  idCensoForestal: number = 0;
  superficieComercial: number = 0;
  nombreComun: string = "";
  nombreCientifico: string = "";
  nombreNativo: string = "";
  nombreComercial: string = "";
  nombreAlterno: string = "";
  familia: string = "";
  estadoArbol: string = "";
  condicionArbol: string = "";
  numeroCorrelativoArbol: number = 0;
  unidadTrabajo: string = "";
  codigoDeArbol: string = "";
  mortandad: boolean = true;
  bloqueQuinquenal: string = "";
  parcelaCorta: string = "";
  idPlanManejo: number = 0;
  coorEste: string = "";
  coorNorte: string = "";
  fechaTala: string = "";
  volumenTala: number = 0;
  fechaTrozado: string = "";
  volumenTrozado: number = 0;
  codigoEstadoArbol: string = "";
  tipoArbol: string = "";
  productoDescr: string = "";
  fechaRealizacion: Date | any = "";
}
