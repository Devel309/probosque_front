import { AuditoriaModel } from './auditoria';
import { PersonaModel } from './Persona';
import { DistritoModel } from './Distrito';

export interface SolicitudModel extends AuditoriaModel {
  idSolicitud: number;
  superficieComunidad: number;
  areaManejoForestal: number;
  nroSectorAnexo: string;
  lstAprovechamiento: string;
  codigoTipoAprovechamiento: number;
  codigoEstadoSolicitud: string;
  estadoSolicitud: string;
  persona: PersonaModel;
  nombresRegente: string;
  apellidosRegente: string;
  numeroLicenciaRegente: string;
  periodoRegente: string;
  distrito: DistritoModel;
  nroPropiedad: string;
  nroPartidaRegistral: string;
  nroActaAsamblea: string;
  nroActaAsambleaRrll: string;
  nroActaAsambleaAcuerdo: string;
  regenteForestal: string;
  nombreRegenteForestal: string;
  nroContratoRegente: string;
  escalaManejo: string;
  validaSunat: boolean;
  validaSunarp: boolean;

  nroRucEmpresa: string;
  nroComprobantePago: string;
  areaComunidad: number;
  tipoAprovechamiento: string;
  nroContratoTercero: string;
  nroActaAprovechamiento: string;

  idSolicitudAcceso: number;
  idPersonaRepresentante: number;

  rat?: boolean;

  descSolicitudReconocimiento: string;
  descSolicitudAmpliacion: string;
  descSolicitudTitulacion: string;
  descDocumentoResidencia: string;
}


export class SolicitudPlantacionForestalModel {
  constructor(data?: any) {
    if (data) {
      this.solPlantacionForestal = new solPlantacionForestalModel(
        data.solPlantacionForestal
      );
      this.solPlantacionForestalArea = new solPlantacionForestalAreaModel(
        data.solPlantacionForestal
      );
      // this.solPlantacionForestalAreaPlantada  =new solPlantacionForestalAreaPlantadaModel( data.solPlantacionForestal);
      // this.solPlantacionForestalDetCoord  =new solPlantacionForestalDetCoordModel( data.solPlantacionForestal);
      // this.solPlantacionForestalDet  =new solPlantacionForestalDetModel( data.solPlantacionForestal);
    } else {
      this.solPlantacionForestal = new solPlantacionForestalModel();
      this.solPlantacionForestalArea = new solPlantacionForestalAreaModel();
      // this.solPlantacionForestalAreaPlantada =  new solPlantacionForestalAreaPlantadaModel();
      // this.solPlantacionForestalDetCoord =  new solPlantacionForestalDetCoordModel();
      // this.solPlantacionForestalDet =  new solPlantacionForestalDetModel();
    }
  }

  // lstDetalle:DetalleDetallePlantacion1[] = [];
  solPlantacionForestal: solPlantacionForestalModel;
  solPlantacionForestalArea: solPlantacionForestalAreaModel;
  solPlantacionForestalAreaPlantada: solPlantacionForestalAreaPlantadaModel[] =
    [];
  solPlantacionForestalDetCoord: solPlantacionForestalDetCoordModel[] = [];
  solPlantacionForestalDet: solPlantacionForestalDetModel[] = [];
}

export class solPlantacionForestalModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      this.id_solplantforest = data.id_solplantforest;
      this.id_sol_persona = data.id_sol_persona;
      this.id_sol_representante = data.id_sol_representante;
      this.observacion = data.observacion;
      this.persona = new PersonaSolPlantacion(data.persona);
      this.estadoSolPlanta = new EstadoSolPlantacion(data.estadoSolPlanta);

      this.readonly = data.readonly;
    } else {
      this.persona = new PersonaSolPlantacion();
      this.estadoSolPlanta = new EstadoSolPlantacion();

      this.id_solplantforest = 0;
      this.id_sol_persona = 0;
      this.id_sol_representante = 0;
      this.observacion = '';
      this.readonly = false;
    }
  }
  fechaRegistro: Date = new Date();
  idUsuarioModificacion: number = 0;
  fechaModificacion: Date = new Date();
  idUsuarioElimina: number = 0;
  fechaElimina: Date = new Date();
  idUsuarioRegistro: number = 0;

  estado: string = '';

  id_solplantforest: number = 0;
  id_sol_persona: number = 0;
  id_sol_representante: number = 0;
  observacion: string = '';
  persona: PersonaSolPlantacion;
  estadoSolPlanta: EstadoSolPlantacion;

  readonly: boolean = false;
}

export class PersonaSolPlantacion {
  constructor(data?: any) {
    if (data) {
      this.idTipoPersona = data.tipoPersona;
      this.apellidoMaterno = data.apellidoMaterno;
      this.apellidoPaterno = data.apellidoPaterno;
      this.nombres = data.nombres;
      this.celular = data.celular;
      this.telefono = data.telefono;
      this.sector = data.sector;
      this.codigo_tipo_documento = data.codigo_tipo_documento;
      this.codigo_tipo_persona = data.codigo_tipo_persona;
      this.correo = data.correo;
      this.direccion = data.direccion;
      this.direccion_numero = data.direccion_numero;
      this.es_repr_legal = data.es_repr_legal;
      this.id_departamento = data.id_departamento;

      this.id_distrito = data.id_distrito;
      this.id_provincia = data.id_provincia;
      this.id_persona_empresa_repr_legal = data.id_persona_empresa_repr_legal;
      this.id_provincia_empresa = data.id_provincia_empresa;
      this.id_distrito_empresa = data.id_distrito_empresa;
      this.id_departamento_empresa = data.id_departamento_empresa;
      this.direccion_empresa = data.direccion_empresa;
      this.direccion_numero_empresa = data.direccion_numero_empresa;
      this.email_empresa = data.email_empresa;
      this.numero_documento = data.numero_documento;

      this.numero_documento_empresa = data.numero_documento_empresa;
      this.persona_empresa_repr_legal = data.persona_empresa_repr_legal;
      this.razon_social_empresa = data.razon_social_empresa;
      this.ruc = data.ruc;
      this.sector_empresa = data.sector_empresa;
      this.telefono_empresa = data.telefono_empresa;
      this.celular_empresa = data.celular_empresa;

      return;
    }
  }

  idTipoPersona: number = 0;
  apellidoMaterno: string = '';
  apellidoPaterno: string = '';
  celular: string = '';
  nombres: string = '';
  telefono: string = '';
  sector: string = '';
  codigo_tipo_documento: string = '';
  codigo_tipo_persona: string = '';
  correo: string = '';
  direccion: string = '';
  direccion_numero: string = '';
  es_repr_legal: string = '';
  id_departamento: number = 0;
  id_distrito: number = 0;
  id_provincia: number = 0;

  id_persona_empresa_repr_legal: string = '';
  id_provincia_empresa: number = 0;
  id_distrito_empresa: number = 0;
  id_departamento_empresa: number = 0;

  direccion_empresa: string = '';
  direccion_numero_empresa: string = '';
  email_empresa: string = '';

  numero_documento: string = '';
  numero_documento_empresa: string = '';
  persona_empresa_repr_legal: string = '';
  razon_social_empresa: string = '';
  ruc: string = '';
  sector_empresa: string = '';
  telefono_empresa: string = '';
  celular_empresa: string = '';
}

export class EstadoSolPlantacion {
  constructor(data?: any) {
    if (data) {
      this.descripcion = data.descripcion;
      this.fecha_registro = data.fecha_registro;
      this.id_solplantforestal_estado = data.id_solplantforestal_estado;
      this.id_usuario_registro = data.id_usuario_registro;
      this.estado = data.estado;
    }
  }
  descripcion: string = '';
  fecha_registro: Date = new Date();
  id_solplantforestal_estado: number = 0;
  id_usuario_registro: number = 0;
  estado: string = '';
}

export class solPlantacionForestalAreaModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      this.area = data.area;
      this.caserio_comunidad = data.caserio_comunidad;
      this.doc_autoriza_plantacion = data.doc_autoriza_plantacion;
      this.estado = data.estado;
      this.id_distrito = data.id_distrito;
      this.id_provincia = data.id_provincia;
      this.id_departamento = data.id_departamento;
      this.id_doc_autoriza_plantacion = data.id_doc_autoriza_plantacion;
      this.id_solplantforest = data.id_solplantforest;
      this.num_cesion_agroforestal = data.num_cesion_agroforestal;
      this.num_concesion_agroforestal = data.num_concesion_agroforestal;
      this.num_documento = data.num_documento;
      this.ruc = data.ruc;
      this.predio = data.predio;
      this.pro_doc_acredita = data.pro_doc_acredita;
      this.propietario_predio = data.propietario_predio;
      this.ubi_geografica = data.ubi_geografica;

      this.fileNamePropietario = data.fileNamePropietario;
      this.fileNameInversionista = data.fileNameInversionista;
      this.readonly = data.readonly;
      this.condicionPropietario = data.condicionPropietario;
      this.condicionInversionista = data.condicionInversionista;
      this.tipo = data.tipo;
      this.tipoNumero = data.tipoNumero;
    }
  }
  idUsuarioRegistro: number = 0;
  fechaRegistro: Date = new Date();
  idUsuarioModificacion: number = 0;
  fechaModificacion: Date = new Date();
  idUsuarioElimina: number = 0;
  fechaElimina: Date = new Date();

  area: number = 0;
  caserio_comunidad: string = '';
  doc_autoriza_plantacion: string = '';
  estado: string = '';

  id_distrito: number = 0;
  id_provincia: number = 0;
  id_departamento: number = 0;
  id_doc_autoriza_plantacion: number = 0;
  id_solplantforest: number = 0;
  id_solplantforest_area: number = 0;
  inv_doc_acredita: string = '';
  num_cesion_agroforestal: string = '';
  num_concesion_agroforestal: string = '';
  num_documento: string = '';
  ruc: string = '';
  predio: string = '';
  pro_doc_acredita: string = '';
  propietario_predio: string = '';
  ubi_geografica: string = '';

  fileNamePropietario: string = '';
  fileNameInversionista: string = '';
  readonly: boolean = true;
  condicionPropietario: boolean = false;
  condicionInversionista: boolean = false;
  tipo: string = '';
  tipoNumero: string = '';
}
export class solPlantacionForestalAreaPlantadaModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      this.area_total_plantacion = data.area_total_plantacion;
      this.especies_establecidas = data.especies_establecidas;
      this.estado = data.estado;
      this.fines = data.fines;
      this.id_sistemaplantacion = data.id_sistemaplantacion;
      this.id_solplantforest = data.id_solplantforest;
      this.id_solplantforest_areaplantada = data.id_solplantforest_areaplantada;
      this.mes_anio_plantacion = data.mes_anio_plantacion;
      this.superficie_cantidad = data.superficie_cantidad;
      this.superficie_medida = data.superficie_medida;
    }
  }

  fechaRegistro!: Date;
  idUsuarioModificacion!: number;
  fechaModificacion!: Date;
  idUsuarioElimina!: number;
  fechaElimina!: Date;
  idUsuarioRegistro: number = 0;

  area_total_plantacion: number = 0;
  especies_establecidas: string = '';
  estado: string = '';
  fines: number = 0;

  id_sistemaplantacion: number = 0;
  id_solplantforest: number = 0;
  id_solplantforest_areaplantada: number = 0;
  mes_anio_plantacion: Date = new Date();
  superficie_cantidad: number = 0;
  superficie_medida: string = '';
}

export class solPlantacionForestalDetCoordModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      this.areabloque = data.areabloque;
      this.areabloque_unidad = data.areabloque_unidad;
      this.bloquesector = data.bloquesector;
      this.especies_establecidas = data.especies_establecidas;
      this.estado = data.estado;
      this.id_solplantforest = data.id_solplantforest;
      this.id_solplantforest_coord_det = data.id_solplantforest_coord_det;
      this.detalle = data.detalle;

      return;
    }
  }
  idUsuarioRegistro: number = 0;
  fechaRegistro: Date = new Date();
  idUsuarioModificacion: number = 0;
  fechaModificacion: Date = new Date();
  idUsuarioElimina: number = 0;
  fechaElimina: Date = new Date();

  areabloque: number = 0;
  areabloque_unidad: string = '';
  especies_establecidas: string = '';
  estado: string = '';
  bloquesector: string = '';
  id_bloquesector: number = 0;
  id_solplantforest: number = 0;
  id_solplantforest_coord_det: number = 0;
  detalle: DetCoordModel[] = [];
}
export interface DetCoordModel {
  id_bloquesector: number;
  id_solplantforest_coord_det: number;
  observaciones: string;
  vertice: number;
  zonaeste: string;
  zonanorte: string;
}
export class solPlantacionForestalDetModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      this.coord_este = data.coord_este;
      this.coord_norte = data.coord_norte;
      this.espcs_nom_cientifico = data.espcs_nom_cientifico;
      this.espcs_nom_comun = data.espcs_nom_comun;
      this.estado = data.estado;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.id_solplantforest = data.id_solplantforest;
      this.id_solplantforest_det = data.id_solplantforest_det;
      this.produccion_estimada = data.produccion_estimada;
      this.producion_estimada_und_medida = data.producion_estimada_und_medida;
      this.total_arbol_matas_espcs_existentes =
        data.total_arbol_matas_espcs_existentes;
      this.especie = new Especies();
      this.especie_cient = new Especies();
      return;
    }
  }
  fechaRegistro: Date = new Date();
  idUsuarioModificacion: number = 0;
  fechaModificacion: Date = new Date();
  idUsuarioElimina: number = 0;
  fechaElimina: Date = new Date();
  idUsuarioRegistro: number = 0;

  coord_este: string = '';
  coord_norte: string = '';
  espcs_nom_cientifico: string = '';
  espcs_nom_comun: string = '';
  estado: string = '';

  id_solplantforest: number = 0;
  id_solplantforest_det: number = 0;
  produccion_estimada: number = 0;
  producion_estimada_und_medida: string = '';
  total_arbol_matas_espcs_existentes: number = 0;
  especie: Especies = new Especies();
  especie_cient: Especies = new Especies();
}

export class Especies {
  constructor(data?: any) {
    if (data) {
      this.autor = data.autor;
      this.categoria = data.categoria;
      this.cites = data.autor;
      this.familia = data.familia;
      this.fuente = data.fuente;
      this.idEspecie = data.idEspecie;
      this.idFuenteOficial = data.idFuenteOficial;
      this.nombreCientifico = data.nombreCientifico;
      this.nombreComun = data.nombreComun;
    }
  }

  autor: string = '';
  categoria: string = '';
  cites: string = '';
  // codfor: string="";
  // dmc: string="";
  familia: string = '';
  fuente: string = '';
  habitoCrecimiento: string = '';
  idEspecie: number = 0;
  idFuenteOficial: number = 0;
  nombreCientifico: string = '';
  nombreComun: string = '';
  nombrecomercial: string = '';
  // numero_lista: string="";
  // oficial: string="";
  // sinonimia: string="";
  // tipouso: string="";
}
