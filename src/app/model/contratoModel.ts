import { AuditoriaDto } from "./auditoria";

export class ContratoModel extends AuditoriaDto {
    //Archivos
    archivoLegal: number;
    archivoGarantia: number;
    archivoBienes: number;
    archivoContrato: number;
    archivoPLcontrato: number;
    archivoFirmadoTitularTH: number;
    // Empresa
    tipoDocumentoTitular: string;
    numeroDocumentoTitular: string;
    nombreTitular: string;
    tipoPersonaTitular: string;
    apellidoPaternoTitular : string;
    apellidoMaternoTitular:string;
    // Representante
    tipoDocumentoRepresentante: string
    tipoPersonaRepresentante: string;
    dniRucRepresentanteLegal: string;
    nombreRaSoRepresentanteLegal: string;
    nombresRepresentante: string;
    apePaternoRepresentante: string;
    apeMAternoRepresentante: string;

    codigoPartidaRegistral: string;
    idResultadoPP: number | null;
    idUsuarioRegistro: number;
    idStatusContrato: number;

    titular: string;
    codigoTituloH: string;
    datosValidados: boolean;
    descripcion: string;
    firmado: boolean;
    idContrato: number;
    idUsuarioElimina: number;
    idUsuarioModificacion: number;
    nroContratoConsecion: string;
    tipoDocumentoGestionDescripcion: string;

    //***********************************
    correoElectronico: string;
    idUsuarioPostulacion: number;
    idProcesoPostulacion: number;
    tipoDocumentoGestion: string;
    documentoGestion: number;

    constructor(obj?: Partial<ContratoModel>) {
        super();

        this.archivoLegal = 0;
        this.archivoGarantia = 0;
        this.archivoBienes = 0;
        this.archivoContrato = 0;
        this.archivoPLcontrato = 0;
        this.archivoFirmadoTitularTH = 0;
        this.codigoPartidaRegistral = "";
        this.codigoTituloH = "";
        this.datosValidados = false;
        this.descripcion = "";
        this.dniRucRepresentanteLegal = "";
        this.firmado = false;
        this.idContrato = 0;
        this.idResultadoPP = 0;
        this.idStatusContrato = 0;
        this.idUsuarioElimina = 0;
        this.idUsuarioModificacion = 0;
        this.idUsuarioRegistro = 0;
        this.nombreRaSoRepresentanteLegal = "";
        this.nroContratoConsecion = "";
        this.numeroDocumentoTitular = "";
        this.tipoDocumentoTitular = "";
        this.titular = "";
        //***********************************
        this.tipoDocumentoRepresentante = "";
        this.nombreTitular = "";
        this.apellidoPaternoTitular = "";
        this.apellidoMaternoTitular = "";
        this.tipoPersonaTitular = "";
        this.tipoPersonaRepresentante = "";
        this.nombresRepresentante = "";
        this.apePaternoRepresentante = "";
        this.apeMAternoRepresentante = "";

        this.idProcesoPostulacion = 0;
        this.idUsuarioPostulacion = 0;
        this.correoElectronico = "";
        this.tipoDocumentoGestion = "";
        this.tipoDocumentoGestionDescripcion = "";
        this.documentoGestion = 0;


        if (obj) Object.assign(this, obj);
    }
}
