import { SortType } from "../shared/enums";

export class Page {

    /**
     * Número de página
     */
    pageNumber: number;

    /**
     * Tamaño de página
     */
    pageSize: number;

    /**
     * Campo por el que se desea ordenar
     */
    sortField?: SortType | null;

    /**
     * Tipo de ordenamiento ASC o DESC
     */
    sortType?: string | null;

    /**
     * Campo de base de datos(ignorar)
     */
    offset?: number | null;

    /**
     * Número total de registros
     */
    totalRecords!: number;


    constructor(obj?: Partial<Page>) {
        this.pageNumber = 1;
        this.pageSize = 10;
        this.sortField = null;
        this.sortType = SortType.DESC;
        this.totalRecords = 0;
        if (obj) Object.assign(this, obj);
    }

}
