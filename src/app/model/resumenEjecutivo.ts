export class ResumenEjecutivo {
    constructor(data?: any) {
        if (data) {
            this.aprovechamientoNoMaderable = data.aprovechamientoNoMaderable;
            this.apellidoMaternoElaboraDema = data.apellidoMaternoElaboraDema;
            this.apellidoPaternoElaboraDema = data.apellidoPaternoElaboraDema;
            this.codigoProceso = data.codigoProceso;
            this.cuenca = data.cuenca;
            this.dniElaboraDema = data.dniElaboraDema;
            this.fechaInicioDema = data.fechaInicioDema;
            this.fechaFinDema = data.fechaFinDema;
            this.fechaElaboracionPmfi = data.fechaElaboracionPmfi;
            this.idInformacionGeneralDema = data.idInformacionGeneralDema;
            this.idPersonaComunidad = data.idPersonaComunidad;
            this.idPlanManejo = data.idPlanManejo;
            this.idUsuarioRegistro = data.idUsuarioRegistro;
            this.lstUmf = data.lstUmf;
            this.nombreElaboraDema = data.nombreElaboraDema;

            this.nombreRepresentante = data.nombreRepresentante;
             this.apellidoPaternoRepresentante = data.apellidoPaternoRepresentante;
             this.apellidoMaternoRepresentante = data.apellidoMaternoRepresentante;
          this.regente = data.regente;
          this.departamento = data.departamento;
          this.provincia = data.provincia;
          this.distrito = data.distrito;
          this.idPermisoForestal = data.idPermisoForestal;
          this.codTipoPersona = data.codTipoPersona;


            this.nroAnexosComunidad = data.nroAnexosComunidad;
            this.nroArbolesMaderables = data.nroArbolesMaderables;
            this.nroArbolesMaderablesSemilleros = data.nroArbolesMaderablesSemilleros;
            this.nroArbolesNoMaderables = data.nroArbolesNoMaderables;
            this.nroArbolesNoMaderablesSemilleros = data.nroArbolesNoMaderablesSemilleros;
            this.nroPersonasInvolucradas = data.nroPersonasInvolucradas;
            this.nroResolucionComunidad = data.nroResolucionComunidad;
            this.nroRucComunidad = data.nroRucComunidad;
            this.nroTituloPropiedadComunidad = data.nroTituloPropiedadComunidad;
            this.nroTotalFamiliasComunidad = data.nroTotalFamiliasComunidad;
            this.subCuenca = data.subCuenca;
            this.superficieHaMaderables= data.superficieHaMaderables? data.superficieHaMaderables: '';
            this.superficeHaNoMaderables = data.superficeHaNoMaderables ? data.superficeHaNoMaderables : null;
            this.totalCostoEstimado = data.totalCostoEstimado;
            this.totalIngresosEstimado = data.totalIngresosEstimado;
            this.totalUtilidadesEstimado = data.totalUtilidadesEstimado;
            this.volumenM3rMaderables = data.volumenM3rMaderables;
            this.volumenM3rNoMaderables = data.volumenM3rNoMaderables;
            this.idDistritoRepresentante = data.idDistritoRepresentante;
            this.representanteLegal = data.representanteLegal;
            this.federacionComunidad = data.federacionComunidad;
            this.areaTotal = data.areaTotal;
            this.detalle = data.detalle ? data.detalle :'';
            this.observacion = data.observacion ? data.observacion : null;
            this.descripcion = data.descripcion ? data.descripcion : null;
            this.vigencia = data.vigencia ? data.vigencia: 0;

        }
      }
  aprovechamientoNoMaderable?: null;
  apellidoMaternoElaboraDema?: null;
  apellidoPaternoElaboraDema?: string;
  codigoProceso?: string;
  cuenca?: null;
  dniElaboraDema?: null;
  fechaFinDema?: Date;
  fechaInicioDema?: Date;
  fechaElaboracionPmfi?: Date;
  idInformacionGeneralDema?: number;
  idPersonaComunidad?: null;
  idPlanManejo?: number;
  idUsuarioRegistro?: number;
  lstUmf?: null;
  nombreElaboraDema?: string;
  nroAnexosComunidad?: number;
  nroArbolesMaderables?: null;
  nroArbolesMaderablesSemilleros?: null;
  nroArbolesNoMaderables?: null;
  nroArbolesNoMaderablesSemilleros?: null;
  nroPersonasInvolucradas?: null;
  nroResolucionComunidad?: string;
  nroRucComunidad?: string;
  nroTituloPropiedadComunidad?: null;
  nroTotalFamiliasComunidad?: null;
  subCuenca?: null;
  superficieHaMaderables?: string;
  superficeHaNoMaderables?: null;
  totalCostoEstimado?: null;
  totalIngresosEstimado?: null;
  totalUtilidadesEstimado?: null;
  volumenM3rMaderables?: string;
  volumenM3rNoMaderables?: null;
  idDistritoRepresentante?: number;
  representanteLegal?: string;
  federacionComunidad?: string;
  areaTotal?: number;
  detalle?: string;
  observacion?: string;
  descripcion?: string;
  vigencia?: number;

  nombreRepresentante ?: string;
  apellidoPaternoRepresentante ?: string;
  apellidoMaternoRepresentante ?: string;
  regente ?: any;
  departamento?: string;
  provincia ?: string;
  distrito ?: string;
  idPermisoForestal?: number;
  codTipoPersona?: string;
}
