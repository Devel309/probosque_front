import { AuditoriaModel } from "@models";


export interface OrdenInternoModel extends AuditoriaModel{
    
    idOrdenamientoProteccionDet : Number;
    idOrdenamientoProteccion :Number;
    espacio :String;
    supeficie: Number;
}