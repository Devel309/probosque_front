import { DetalleAnexo } from "../util/dataDemoMPAFPP";

export class SistemaManejoPMFIC {
  search: any = null;
  pageNum: any = null;
  pageSize: any = null;
  totalRecord: any = null;
  totalPage: any = null;
  startIndex: any = null;
  estado: any = null;
  idUsuarioRegistro: number = 0;
  fechaRegistro: any = null;
  idUsuarioModificacion: any = null;
  fechaModificacion: any = null;
  idUsuarioElimina: any = null;
  fechaElimina: any = null;
  idSistemaManejoForestal: number = 0;
  idPlanManejo: number = 0;
  codigoProceso: string = "PMFIC";
  descripcionFinMaderable: any = null;
  descripcionCicloCorta: any = null;
  descripcionFinNoMaderable: any = null;
  descripcionCicloAprovechamiento: any = null;
  seccion: string = "";
  subSeccion: string = "";
  detalle: DetalleSistemaManejo[] = [];

  constructor(obj: Partial<SistemaManejoPMFIC>) {
    if (obj) {
      this.search = obj.search;
      this.pageNum = obj.pageNum;
      this.pageSize = obj.pageSize;
      this.totalRecord = obj.totalRecord;
      this.totalPage = obj.totalPage;
      this.startIndex = obj.startIndex;
      this.estado = obj.estado;
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
      this.fechaRegistro = obj.fechaRegistro;
      this.idUsuarioModificacion = obj.idUsuarioModificacion;
      this.fechaModificacion = obj.fechaModificacion;
      this.idUsuarioElimina = obj.idUsuarioElimina;
      this.fechaElimina = obj.fechaElimina;
      this.idSistemaManejoForestal = obj.idSistemaManejoForestal
        ? obj.idSistemaManejoForestal
        : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.codigoProceso = "PMFIC";
      this.descripcionFinMaderable = obj.descripcionFinMaderable;
      this.descripcionCicloCorta = obj.descripcionCicloCorta;
      this.descripcionFinNoMaderable = obj.descripcionFinNoMaderable;
      this.descripcionCicloAprovechamiento =
        obj.descripcionCicloAprovechamiento;
      this.seccion = obj.seccion ? obj.seccion : "";
      this.subSeccion = obj.subSeccion ? obj.subSeccion : "";
      this.detalle = obj.detalle ? obj.detalle : [];
    }
  }
}

export class DetalleSistemaManejo {
  search?: any = null;
  pageNum?: any = null;
  pageSize?: any = null;
  totalRecord?: any = null;
  totalPage?: any = null;
  startIndex?: any = null;
  estado?: any = null;
  idUsuarioRegistro: number = 0;
  fechaRegistro?: any = null;
  idUsuarioModificacion?: any = null;
  fechaModificacion?: any = null;
  idUsuarioElimina?: any = null;
  fechaElimina?: any = null;
  idSistemaManejoForestalDetalle?: any = 0;
  idSistemaManejoForestal?: any = 0;
  codigoTipoDetalle: string = "";
  categoriaZona?: any = null;
  usoPotencial?: any = null;
  usoPotencialNumerico: number = 0;
  actividadRealizar?: any = null;
  actividades: string = "";
  descripcionSistema: string = "";
  maquinariasInsumos?: any = null;
  personalRequerido?: any = null;
  observacion?: any = null;
  codigoTipoSilvicultural?: any = null;
  nombreTipoSilvicultural?: any = null;
  descripcionTipoSilvicultural?: any = null;
  codigoProceso: string = "PMFIC";
  editable: boolean = false;
  enviar: boolean = false;

  constructor(obj?: Partial<DetalleSistemaManejo>) {
    if (obj) {
      this.search = obj.search;
      this.pageNum = obj.pageNum;
      this.pageSize = obj.pageSize;
      this.totalRecord = obj.totalRecord;
      this.totalPage = obj.totalPage;
      this.startIndex = obj.startIndex;
      this.estado = obj.estado;
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
      this.fechaRegistro = obj.fechaRegistro;
      this.idUsuarioModificacion = obj.idUsuarioModificacion;
      this.fechaModificacion = obj.fechaModificacion;
      this.idUsuarioElimina = obj.idUsuarioElimina;
      this.fechaElimina = obj.fechaElimina;
      this.idSistemaManejoForestalDetalle = !!obj.idSistemaManejoForestalDetalle
        ? obj.idSistemaManejoForestalDetalle
        : 0;
      this.idSistemaManejoForestal = !!obj.idSistemaManejoForestal
        ? obj.idSistemaManejoForestal
        : 0;
      this.codigoTipoDetalle = obj.codigoTipoDetalle
        ? obj.codigoTipoDetalle
        : "";
      this.categoriaZona = obj.categoriaZona;
      this.usoPotencial = obj.usoPotencial;
      this.usoPotencialNumerico = !!obj.usoPotencialNumerico
        ? obj.usoPotencialNumerico
        : 0;
      this.actividadRealizar = obj.actividadRealizar;
      this.actividades = obj.actividades ? obj.actividades : "";
      this.descripcionSistema = obj.descripcionSistema
        ? obj.descripcionSistema
        : "";
      this.maquinariasInsumos = obj.maquinariasInsumos;
      this.personalRequerido = obj.personalRequerido;
      this.observacion = obj.observacion;
      this.codigoTipoSilvicultural = obj.codigoTipoSilvicultural;
      this.descripcionTipoSilvicultural = obj.descripcionTipoSilvicultural;
      this.nombreTipoSilvicultural = obj.nombreTipoSilvicultural;
      this.codigoProceso = "PMFIC";
      this.editable = obj.editable ? obj.editable : false;
      this.enviar = obj.enviar ? obj.enviar : false;
    }
  }
}
