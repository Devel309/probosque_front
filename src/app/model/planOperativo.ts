export interface resumenActividades {
  idResumenAct: number;
  codTipoResumen: string;
  nroResolucion: number;
  nroPcs: number;
  areaHa: number;
  idPlanManejo: number;
  idUsuarioRegistro: number;
  listResumenEvaluacion: ListResumenEvaluacion[];
}

export interface ListResumenEvaluacion {
  idResumenActDet: number;
  tipoResumen: string;
  actividad: string;
  indicador: string;
  programado: string;
  realizado: string;
  resumen: string;
  idUsuarioRegistro: number;
}

export class PlanOpResumenActModel {
  constructor(data?: any) {
    if (data) {
      this.idResumenAct = data.idResumenAct;
      this.codTipoResumen = data.codTipoResumen;
      this.nroResolucion = data.nroResolucion;
      this.nroPcs = data.nroPcs;
      this.areaHa = data.areaHa;
      this.idPlanManejo = data.idPlanManejo;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.listResumenEvaluacion = data.listResumenEvaluacion;

      return;
    }
  }
  idResumenAct: number = 0;
  codTipoResumen: string = '';
  nroResolucion: number = 0;
  nroPcs: number = 0;
  areaHa: number = 0;
  idPlanManejo: number = 0;
  idUsuarioRegistro: number = 0;
  listResumenEvaluacion: PlanOPResumenActDetModel[] = [];
}

export class PlanOPResumenActDetModel {
  constructor(data?: any) {
    if (data) {
      this.idResumenActDet = data.idResumenActDet;
      this.tipoResumen = data.tipoResumen;
      this.actividad = data.actividad;
      this.indicador = data.indicador;
      this.programado = data.programado;
      this.realizado = data.realizado;
      this.resumen = data.resumen;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.idTmp = data.idTmp;

      return;
    }
  }
  idResumenActDet: number = 0;
  tipoResumen: number = 0;
  actividad: string = '';
  indicador: string = '';
  programado: string = '';
  realizado: string = '';
  resumen: string = '';
  idUsuarioRegistro: number = 0;
  idTmp: string = new Date().toISOString();
}
