import { AuditoriaModel } from "../../auditoria";
import { ResultArchivoModel } from "../../ResultArchivo";
import { ZonaAnexoModel } from "./ZonaAnexoModel";

export interface ZonaModel extends AuditoriaModel{
    idZona: Number;
    idPlanManejo: Number;
    codigoZona:String;
    idZonaPadre:Number;
    nombre:String;
    total:Number;
    porcentaje:Number;
    zonaAnexo: ZonaAnexoModel[];
    archivo: ResultArchivoModel;
    idLayer: String
    inServer: Boolean;
    idArchivo:Number;
}