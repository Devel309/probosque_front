import { AuditoriaModel } from "../../auditoria";

export interface ZonaAnexoModel extends AuditoriaModel{

    idZonaAnexo: Number;
    idZonaDetalle: Number;
    nombre:String;
    valor:String;

}