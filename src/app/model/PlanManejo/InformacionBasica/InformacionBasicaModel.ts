import {InformacionBasicaDetalleModel} from './InformacionBasicaDetalleModel';


export class InformacionBasicaModel {

  idInfBasica: number | null = 0;
  codInfBasica: string | null = null;
  codSubInfBasica: string | null = null;
  codNombreInfBasica: string | null = null;
  idDepartamento: number | null = null;
  idProvincia: number | null = null;
  idDistrito: number | null = null;
  departamento: string | null = null;
  provincia: string | null = null;
  distrito: string | null = null;
  cuenca: string | null = null;
  anexo: string | null = null;
  idPlanManejo: number | null = null;
  numeroPc: number | null = null;
  areaTotal: number | null = null;
  nroHojaCatastral: string | null = null;
  nombreHojaCatastral: string | null = null;
  idHidrografia: number | null = null;
  frenteCorta: string | null = null;
  comunidad: string | null = null;
  idUsuarioRegistro: number | null = null;
  listInformacionBasicaDet : InformacionBasicaDetalleModel[] = []
  listInformacionBasicaDistrito? : any[] = []
  subCuenca: string | null = null;

  /*constructor(obj?: Partial<InformacionBasicaModel>) {
    this.idInfBasica = obj.idInfBasica;
    this.codInfBasica = null;
    this.peronasTrabajan = null;
    this.personasEmpadronada = null;
    if (obj) Object.assign(this, obj);
  }*/

  constructor(data?: any) {
    if (data) {
      Object.assign(this, data)
      /*this.idInfBasica = data.idInfBasica;
      this.codInfBasica = data.codInfBasica;
      this.codNombreInfBasica = data.codNombreInfBasica;
      this.departamento = data.departamento;
      this.provincia = data.provincia;
      this.distrito = data.distrito;
      this.comunidad = data.comunidad;
      this.cuenca = data.cuenca;
      this.idPlanManejo = data.idPlanManejo;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.listInformacionBasicaDet = data.listInformacionBasicaDet;
      return;*/
    }
  }


}

