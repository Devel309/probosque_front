export class InformacionBasicaDetalleModel {
  idInfBasicaDet: number | null = 0;
  codInfBasicaDet: string | null = null;
  codSubInfBasicaDet: string | null = null;
  puntoVertice: string | null = null;
  coordenadaEsteIni: number | null = 0;
  coordenadaNorteIni: number | null = 0;
  referencia: string | null = null;
  distanciaKm: number | null = null;
  tiempo: number | null = null;
  medioTransporte: string | null = null;
  idUsuarioRegistro: any | null = null;
  epoca: string | null = null;
  idRios: number | null = null;
  descripcion: string | null = null;
  areaHa: string | null = null;
  areaHaPorcentaje: string | null = null;
  zonaVida: string | null = null;
  idFauna: number | null = null;
  idFlora: number | null = null;
  nombreRio: string | null = null;
  nombreLaguna: string | null = null;
  nombreQuebrada: string | null = null;
  isEdit: boolean = false;
  nombreComun: string | null = null;
  nombreCientifico: string | null = null;
  familia: string | null = null;
  observaciones: any = '';
  coordenadaEsteFin: number | null = 0;
  coordenadaNorteFin: number | null = 0;
  coordenadaEste: number | null = 0;
  coordenadaNorte: number | null = 0;
  nombre: string | null = null;
  acceso: string | null = null;
  numeroFamilia: number | null = 0;
  actividad: string | null = null;
  subsistencia: boolean | null = null;
  perenne: boolean | null = null;
  ganaderia: boolean | null = null;
  caza: boolean | null = null;
  pesca: boolean | null = null;
  madera: boolean | null = null;
  otroProducto: boolean | null = null;
  ampliarAnexo: boolean | null = null;
  justificacion: string | null = null;
  marcar: boolean | null = null;
  especieExtraida: string | null = null;
  

  constructor(data?: any) {
    if (data) {
      Object.assign(this, data);
      /*this.idInfBasicaDet = data.idInfBasicaDet;
      this.codInfBasicaDet = data.codInfBasicaDet;
      this.puntoVertice = data.puntoVertice;
      this.coordenadaEsteIni = data.coordenadaEsteIni;
      this.coordenadaNorteIni = data.coordenadaNorteIni;
      this.referencia = data.referencia;
      this.distanciaKm = data.distanciaKm;
      this.tiempo = data.tiempo;
      this.medioTransporte = data.medioTransporte;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.epoca = data.epoca;
      this.idRios = data.idRios;
      this.descripcion = data.descripcion;
      this.areaHa = data.areaHa;
      this.areaHaPorcentaje = data.areaHaPorcentaje;
      this.zonaVida = data.zonaVida;
      this.idFauna = data.idFauna;
      this.idFlora = data.idFlora;
      this.nombreRio = data.nombreRio;
      this.nombreLaguna = data.nombreLaguna;
      return;*/
    }
  }

  limpiarJustificacion(): void {
    this.justificacion = null;
  }
}

