
import {OrdenamientoProteccionDet} from './OrdenamientoProteccionDet';

export class OrdenamientoProteccion {

  idOrdenamientoProteccion: number | null = 0;
  codTipoOrdenamiento: string | null = null;
  subCodTipoOrdenamiento: string | null = null;
  idPlanManejo: number | null = null;
  idUsuarioRegistro: number | null = null;
  listOrdenamientoProteccionDet: OrdenamientoProteccionDet[] = []

  constructor(data?: any) {
    if (data) {
      Object.assign(this, data)

    }
  }
}
