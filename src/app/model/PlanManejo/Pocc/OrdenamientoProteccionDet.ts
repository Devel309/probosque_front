export class OrdenamientoProteccionDet {
  idOrdenamientoProteccionDet: number | null = 0;
  codigoTipoOrdenamientoDet: string | null = null;
  categoria: string | null = null;
  areaHA: number | null = null;
  areaHAPorcentaje: number | null = null;
  descripcion: string | null = null;
  observacion: string | null = null;
  observacionDetalle: string | null = null;
  actividad: string | null = null;
  actividadesRealizar: string | null = null;
  idUsuarioRegistro: number | null = null;
  // isEdit :boolean = true;

  constructor(data?: any) {
    if (data) {
      Object.assign(this, data);
    }
  }
}
