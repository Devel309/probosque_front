import { AuditoriaDto } from "../auditoria";


export class PlanManejoAspEconomico extends AuditoriaDto {

  idInformacionSocioEconomica: number | null;
  idPlanManejo: number | null;
  peronasTrabajan: number | null;
  personasEmpadronada: number | null;

  constructor(obj?: Partial<PlanManejoAspEconomico>) {
    super();

    this.idInformacionSocioEconomica = null;
    this.idPlanManejo = null;
    this.peronasTrabajan = null;
    this.personasEmpadronada = null;

    if (obj) Object.assign(this, obj);
  }


}

export class PlanManejoActEconomica extends AuditoriaDto {

  idInformacionSocioEconomica: number | null;
  idInfoeconomicaActiEconomica: number | null;
  idTipoactividadEconomica: number | null;
  tipoActividadEconomica: string | null;
  activo: number | null;
  descripcion: string | null;

  constructor(obj?: Partial<PlanManejoActEconomica>) {
    super();

    this.idInformacionSocioEconomica = null;
    this.idInfoeconomicaActiEconomica = null;
    this.idTipoactividadEconomica = null;
    this.tipoActividadEconomica = null;
    this.activo = null;
    this.descripcion = null;
    if (obj) Object.assign(this, obj);
  }


}

export class PlanManejoInfraestructura extends AuditoriaDto {
  idInfoEconomicaInfraestructura: number | null;
  idInformacionSocioEconomica: number | null;
  idTipoinfraestructura: number | null;
  activo: number | null;
  norte: number | null;
  este: number | null;
  referencia: string | null;
  tipoInfraestructura: string | null;
  zonautm: string | null;


  constructor(obj?: Partial<PlanManejoInfraestructura>) {
    super();

    this.idInfoEconomicaInfraestructura = null;
    this.idInformacionSocioEconomica = null;
    this.idTipoinfraestructura = null;
    this.activo = null;
    this.norte = null;
    this.este = null;
    this.referencia = null;
    this.tipoInfraestructura = null;
    this.zonautm = null;


    if (obj) Object.assign(this, obj);
  }


}

export class PlanManejoActividades extends AuditoriaDto {

  idInfoeconomicaActividades: number | null;
  idInformacionSocioEconomica: number | null;
  tipoActividad: string | null;
  idTipoActividad: number | null;
  especiesExtraidas: string | null;
  observacion: string | null;
  activo: number | null;

  constructor(obj?: Partial<PlanManejoActividades>) {
    super();

    this.idInfoeconomicaActividades = null;
    this.idInformacionSocioEconomica = null;
    this.tipoActividad = null;
    this.idTipoActividad = null;
    this.especiesExtraidas = null;
    this.observacion = null;
    this.activo = null;

    if (obj) Object.assign(this, obj);
  }


}


export class PlanManejoConficto extends AuditoriaDto {


  idInfoEconomicaConfictos: number | null;
  idInformacionSocioEconomica: number | null;
  propuestaSolucion: string | null;
  tipoConficto: string | null;
  activo: number | null;


  constructor(obj?: Partial<PlanManejoConficto>) {
    super();

    this.idInfoEconomicaConfictos = null;
    this.idInformacionSocioEconomica = null;
    this.propuestaSolucion = null;
    this.tipoConficto = null;
    this.activo = null;

    if (obj) Object.assign(this, obj);
  }


}
