import {ResumenActividadDet} from './ResumenActividadDet';

export class ResumenActividad {

  idResumenAct: number | null = 0;
  codTipoResumen: string | null = null;
  codigoResumen: string | null = null;
  codigoSubResumen: string | null = null;
  nroResolucion: number | null = null;
  nroPcs: number | null = null;
  areaHa: number | null = null;
  idPlanManejo: number | null = null;
  idUsuarioRegistro: string | null = null;
  listResumenEvaluacion: ResumenActividadDet[] = []

  constructor(data?: any) {
    if (data) {
      Object.assign(this, data)

    }
  }
}
