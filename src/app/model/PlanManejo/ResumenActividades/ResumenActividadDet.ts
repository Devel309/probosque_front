
export class ResumenActividadDet {

  idResumenActDet: number | null = 0;
  tipoResumen: string | null = null;
  actividad: string | null = null;
  indicador: string | null = null;
  programado: string | null = null;
  realizado: string | null = null;
  resumen: string | null = null;
  idUsuarioRegistro: number | null = 0;
  aspectoRecomendacion: string | null = null;
  aspectoPositivo: string | null = null;
  aspectoNegativo: string | null = null;
  estadoVar: string | null = null;


  constructor(data?: any) {
    if (data) {
      Object.assign(this, data)

    }
  }
}
