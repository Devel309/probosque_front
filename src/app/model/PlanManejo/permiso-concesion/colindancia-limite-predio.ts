export class ColindanciaLimitePredio {
  idColindancia: number = 0;
  idPlanManejo: number = 0;
  codTipoColindancia: string = "";
  tipoColindancia: string = "";
  descripcionColindante: string = "";
  descripcionLimite: string = "";
  idUsuarioRegistro: number = 0;

  constructor() {

  }
}