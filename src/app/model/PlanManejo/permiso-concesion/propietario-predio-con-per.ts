export class PropietarioPredioConPer {
    idPlanManejo: number = 0;
    idInformacionGeneralDema: number = 0;
    nombreElaborador: string = "";
    apellidoPaternoElaborador: string = "";
    apellidoMaternoElaborador: string = "";
    nombreCompleto: string = "";
    idUsuarioRegistro: number | null = null;
    idUsuarioModificacion: number | null = null;
    nombreElaboraDema: string = "";
    constructor() {}

    setData(obj: any) {
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.nombreElaboraDema = obj.nombreElaboraDema ? obj.nombreElaboraDema : "";
      this.idInformacionGeneralDema = obj.idInformacionGeneralDema ? obj.idInformacionGeneralDema : 0;
      this.nombreElaborador = obj.nombreElaborador ? obj.nombreElaborador : "";
      this.apellidoPaternoElaborador = obj.apellidoPaternoElaboraDema ? obj.apellidoPaternoElaboraDema : "";
      this.apellidoMaternoElaborador = obj.apellidoMaternoElaboraDema ? obj.apellidoMaternoElaboraDema : "";
      this.nombreCompleto = `${obj.nombreElaboraDema} ${obj.apellidoPaternoElaboraDema} ${obj.apellidoMaternoElaboraDema}`;
      this.idUsuarioRegistro = obj.idUsuarioRegistro ? obj.idUsuarioRegistro : null;
      this.idUsuarioModificacion = obj.idUsuarioModificacion ? obj.idUsuarioModificacion : null;
    }
  }
