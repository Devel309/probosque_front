export class UbicacionPredioConPer {
    idPlanManejo: number = 0;
    idInformacionGeneralDema: number = 0;
    idDepartamento: number | null = null;
    idProvincia: number | null = null;
    idDistrito: number | null = null;
    idDistritoTitular: number | null = null;
    idDepartamentoTitular: number | null = null;
    idProvinciaTitular: number | null = null;
    cuenca: string = "";
    idUsuarioRegistro: number | null = null;
    idUsuarioModificacion: number | null = null;
    zona: string = "";
    datum: string = "";
    horizontal: string = "";

    constructor() {}

    setData(obj: any) {
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.idInformacionGeneralDema = obj.idInformacionGeneralDema ? obj.idInformacionGeneralDema : 0;
      this.idDepartamento = obj.idDepartamento ? obj.idDepartamento : 0;
      this.idProvincia = obj.idProvincia ? obj.idProvincia : 0;
      this.idDistrito = obj.idDistrito ? obj.idDistrito : 0;
      this.idDistritoTitular = obj.idDistritoTitular ? obj.idDistritoTitular : 0;
      this.idDepartamentoTitular  = obj.idDepartamentoTitular  ? obj.idDepartamentoTitular  : 0;
      this.idProvinciaTitular  = obj.idProvinciaTitular  ? obj.idProvinciaTitular  : 0;
      this.cuenca = obj.cuenca ? obj.cuenca : "";
      this.zona = obj.zona ? obj.zona : "";
      this.datum = obj.datum ? obj.datum : "";
      this.horizontal = obj.horizontal ? obj.horizontal : "";
      this.idUsuarioRegistro = obj.idUsuarioRegistro ? obj.idUsuarioRegistro : null;
      this.idUsuarioModificacion = obj.idUsuarioModificacion ? obj.idUsuarioModificacion : null;
    }
  }
