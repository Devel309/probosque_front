export class RegenteForestalModel {
    idRegente!: number | null;
    codTipoRegente: string;
    numeroLicencia: string;
    domicilioLegal: string;
    contratoSuscrito: string;
    certificadoHabilitacion: string;
    numeroInscripcion: string;
    numeroDocumento: string;
    nombres: string;
    apellidos: string;
    adjuntoContrato: string;
    adjuntoCertificado: string;
    idPersona!: number | null;
    idPlanManejo: number;
    idUsuarioRegistro: number;
    estado: string;

    constructor(obj?: Partial<RegenteForestalModel>) {

        this.idRegente = 0;
        this.codTipoRegente = '';
        this.numeroLicencia = '';
        this.domicilioLegal = '';
        this.contratoSuscrito = '';
        this.certificadoHabilitacion = '';
        this.numeroInscripcion = '';
        this.nombres = '';
        this.apellidos = '';
        this.numeroDocumento = '';
        this.adjuntoContrato = '';
        this.adjuntoCertificado = '';
        this.idPersona = 0;
        this.idPlanManejo = 0;
        this.idUsuarioRegistro = 0;
        this.estado = "";

        if (obj) Object.assign(this, obj);
    }
}
