import { PGMFArchivoDto } from "./PGMFArchivoDto";
import { AuditoriaDto } from "./auditoria";

export class InformacionGeneralDto extends AuditoriaDto {
    idInformacionGeneral: number;
    idPlanManejo: number;
    nombreTitular: string;
    nombreRepresentanteLegal: string;
    dni: string;
    domicilioLegal: string;
    nroContratoConcesion: string;
    distrito: string;
    departamento: string;
    provincia: string;
    fechaPresentacion!: Date;
    duracion: number;
    fechaInicio!: Date;
    fechaFin!: Date;
    areaTotalConcesion: number;
    areaBosqueProduccionForestal: number;
    areaProteccion: number;
    numeroBloquesQuinquenales: string;
    potencialMaderable: number;
    volumenCortaAnualPermisible: number;
    idRegenteForestal: number | null;
    nombreRegenteForestal: string;
    domicilioLegalRegente: string;
    contratoSuscritoTitularTituloHabilitante: string;
    certificadoHabilitacionProfesionalRegenteForestal: string;
    numeroInscripcionRegistroRegente: string;
    nombreDepartamentoContrato:string;
    nombreProvinciaContrato:string;
    nombreDistritoContrato:string;

    idContrato: number | null;
    tipoPersonaTitularTH: string;
    tipoPersonaTitularTHDesc: string;
    tipoDocumentoTitularTH: string;
    tipoDocumentoTitularTHDesc: string;
    numeroDocumentoTitularTH: string;
    nombreTitularTH: string;
    apellidoPaternoTitularTH: string;
    apellidoMaternoTitularTH: string;
    razonSocialTitularTH: string;
    emailTitularTH: string;
    direccionTitularTH: string;
    tipoPersonaRepLegal: string;
    tipoPersonaRepLegalDesc: string;
    tipoDocumentoRepLegal: string;
    tipoDocumentoRepLegalDesc: string;
    numeroDocumentoRepLegal: string;
    nombreRepLegal: string;
    apellidoPaternoRepLegal: string;
    apellidoMaternoRepLegal: string;
    emailRepLegal: string;

    tipoDocElaborador: string;
    dniElaborador: string;
    nombreElaborador: string;
    apellidoPaternoElaborador: string;
    apellidoMaternoElaborador: string;


    tipoDocRepresentante: string;
    docRepresentante: string;
    nombreRepresentante: string;
    apellidoPaternoRepresentante: string;
    apellidoMaternoRepresentante: string;
    nroContrato: string;

		archivos:PGMFArchivoDto[]

    constructor(obj?: Partial<InformacionGeneralDto>) {
        super()

        this.idContrato = 0;
        this.tipoPersonaTitularTH= '';
        this.tipoPersonaTitularTHDesc= '';
        this.tipoDocumentoTitularTH= '';
        this.tipoDocumentoTitularTHDesc= '';
        this.numeroDocumentoTitularTH= '';
        this.nombreTitularTH= '';
        this.apellidoPaternoTitularTH= '';
        this.apellidoMaternoTitularTH= '';
        this.razonSocialTitularTH= '';
        this.emailTitularTH= '';
        this.direccionTitularTH = '';
        this.nombreDepartamentoContrato='';
        this.nombreProvinciaContrato='';
        this.nombreDistritoContrato='';

        this.tipoPersonaRepLegal= '';
        this.tipoPersonaRepLegalDesc= '';
        this.tipoDocumentoRepLegal= '';
        this.tipoDocumentoRepLegalDesc= '';
        this.numeroDocumentoRepLegal= '';
        this.nombreRepLegal= '';
        this.apellidoPaternoRepLegal= '';
        this.apellidoMaternoRepLegal= '';
        this.emailRepLegal= '';



        this.idInformacionGeneral = 0;
        this.idPlanManejo = 0;
        this.nombreTitular = '';
        this.nombreRepresentanteLegal = '';
        this.dni = '';
        this.domicilioLegal = '';
        this.nroContratoConcesion = '';
        this.distrito = '';
        this.departamento = '';
        this.provincia = '';
        // this.fechaPresentacion = 0;
        this.duracion = 0;
        // this.fechaInicio = 0;
        // this.fechaFin = 0;
        this.areaTotalConcesion = 0;
        this.areaBosqueProduccionForestal = 0;
        this.areaProteccion = 0;
        this.numeroBloquesQuinquenales = '';;
        this.potencialMaderable = 0;
        this.volumenCortaAnualPermisible = 0;
        this.idRegenteForestal = 0;
        this.nombreRegenteForestal = '';
        this.domicilioLegalRegente = '';
        this.contratoSuscritoTitularTituloHabilitante = '';
        this.certificadoHabilitacionProfesionalRegenteForestal = '';
        this.numeroInscripcionRegistroRegente = '';
				this.archivos=[];
        this.tipoDocElaborador='';
        this.dniElaborador= '';
        this.nombreElaborador= '';
        this.apellidoPaternoElaborador= '';
        this.apellidoMaternoElaborador='';


        this.tipoDocRepresentante= '';
        this.docRepresentante= '';
        this.nombreRepresentante= '';
        this.apellidoPaternoRepresentante='';
        this. apellidoMaternoRepresentante= '';
        this.nroContrato='';
        if (obj) Object.assign(this, obj);
    }

}
