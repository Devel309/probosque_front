import { AuditoriaModel } from "./auditoria";
import { MonitoreoModel } from "./Monitoreo";
export interface MonitoreoPgmfeaModel extends AuditoriaModel  {
    idMonitoreoPgmfea:number;
    actividad:String;
    descripcion:String;
    responsable:String;
    monitoreo:MonitoreoModel;
}
