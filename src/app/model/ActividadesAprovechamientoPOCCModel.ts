export class ActividadesAprovechamientoPOCCModel {
  constructor(data?: any) {
    if (data) {
      this.anexo = data.anexo ? data.anexo : '';
      this.areaTotal = data.areaTotal ? data.areaTotal : 0;
      this.codActvAprove = data.codActvAprove ? data.codActvAprove : '';
      this.codSubActvAprove = data.codSubActvAprove
        ? data.codSubActvAprove
        : '';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.detalle = data.detalle ? data.detalle : '';
      this.dimension = data.dimension ? data.dimension : 0;
      this.distancia = data.distancia ? data.distancia : 0;
      this.idActvAprove = data.idActvAprove ? data.idActvAprove : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioElimina = data.idUsuarioElimina ? data.idUsuarioElimina : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.limites = data.limites ? data.limites : '';
      this.lstactividadDet = data.lstactividadDet ? data.lstactividadDet : [];
      this.metodologia = data.metodologia ? data.metodologia : '';
      this.nroArboles = data.nroArboles ? data.nroArboles : 0;
      this.nroEspecies = data.nroEspecies ? data.nroEspecies : 0;
      this.numero = data.numero ? data.numero : 0;
      this.observacion = data.observacion ? data.observacion : '';
      this.postes = data.postes ? data.postes : '';
      this.subdivision = data.subdivision ? data.subdivision : '';
      this.superficie = data.superficie ? data.superficie : 0;
      this.trochas = data.trochas ? data.trochas : '';
      return;
    }
  }

  anexo: string = '';
  areaTotal!: number;
  codActvAprove: string = '';
  codSubActvAprove: string = '';
  descripcion: string = '';
  detalle: string = '';
  dimension!: number;
  distancia!: number;
  idActvAprove!: number;
  idPlanManejo!: number;
  idUsuarioElimina!: number;
  idUsuarioRegistro!: number;
  limites: string = '';
  lstactividadDet: lstactividadDet[] = [];
  metodologia: string = '';
  nroArboles!: number;
  nroEspecies!: number;
  numero!: number;
  observacion: string = '';
  postes: string = '';
  subdivision: string = '';
  superficie!: number;
  trochas: string = '';
}

export class lstactividadDet {
  constructor(data?: any) {
    if (data) {
      this.area = data.area ? data.area : 0;
      this.carcteristicaTec = data.carcteristicaTec
        ? data.carcteristicaTec
        : '';
      this.codActvAproveDet = data.codActvAproveDet
        ? data.codActvAproveDet
        : '';
      this.codSubActvAproveDet = data.codSubActvAproveDet
        ? data.codSubActvAproveDet
        : '';
      this.dcm = data.dcm ? data.dcm : 0;
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.detalle = data.detalle ? data.detalle : '';
      this.familia = data.familia ? data.familia : '';
      this.idActvAproveDet = data.idActvAproveDet ? data.idActvAproveDet : 0;
      this.idUsuarioElimina = data.idUsuarioElimina ? data.idUsuarioElimina : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.individuosHA = data.individuosHA ? data.individuosHA : '';
      this.lineaProduccion = data.lineaProduccion ? data.lineaProduccion : '';
      this.lstactividadDetSub = data.lstactividadDetSub
        ? data.lstactividadDetSub
        : [];
      this.maquinariaMaterial = data.maquinariaMaterial
        ? data.maquinariaMaterial
        : '';
      this.nombreCientifica = data.nombreCientifico
        ? data.nombreCientifico
        : '';
      this.nombreComun = data.nombreComun ? data.nombreComun : '';
      this.numeracion = data.numeracion ? data.numeracion : 0;
      this.observacion = data.observacion ? data.observacion : '';
      this.operaciones = data.operaciones ? data.operaciones : '';
      this.personal = data.personal ? data.personal : '';
      this.sector = data.sector ? data.sector : '';
      this.tipoDetalle = data.tipoDetalle ? data.tipoDetalle : '';
      this.totalIndividuos = data.totalIndividuos ? data.totalIndividuos : 0;
      this.unidad = data.unidad ? data.unidad : '';
      this.volumen = data.volumen ? data.volumen : '';
      this.gc = data.gc ? data.gc : '';
      return;
    }
  }

  area!: number;
  carcteristicaTec: string = '';
  codActvAproveDet: string = '';
  codSubActvAproveDet: string = '';
  dcm!: number;
  descripcion: string = '';
  detalle: string = '';
  familia: string = '';
  idActvAproveDet!: number;
  idUsuarioElimina!: number;
  idUsuarioRegistro!: number;
  individuosHA: string = '';
  lineaProduccion: string = '';
  lstactividadDetSub: lstactividadDetSub[] = [];
  maquinariaMaterial: string = '';
  nombreCientifica: string = '';
  nombreComun: string = '';
  numeracion!: number;
  observacion: string = '';
  operaciones: string = '';
  personal: string = '';
  sector: string = '';
  tipoDetalle: string = '';
  totalIndividuos!: number;
  unidad: string = '';
  volumen: string = '';
  nombreIdiomaNativo: any;
  este: any;
  norte: any;
  gc: string = '';
}

export class lstactividadDetSub {
  constructor(data?: any) {
    if (data) {
      this.area = data.area ? data.area : 0;
      this.codActvAproveDetSub = data.codActvAproveDetSub
        ? data.codActvAproveDetSub
        : '';
      this.codSubActvAproveDetSub = data.codSubActvAproveDetSub
        ? data.codSubActvAproveDetSub
        : '';
      this.dapCM = data.dapCM ? data.dapCM : '';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.detalle = data.detalle ? data.detalle : '';
      this.idActvAproveDetSub = data.idActvAproveDetSub
        ? data.idActvAproveDetSub
        : 0;
      this.idUsuarioElimina = data.idUsuarioElimina ? data.idUsuarioElimina : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.nroArbol = data.nro ? data.nroPC : 0;
      this.observacion = data.observacion ? data.observacion : '';
      this.totalHa = data.totalHa ? data.totalHa : 0;
      this.totalPC = data.totalPC ? data.totalPC : 0;
      this.var = data.var ? data.var : '';
      this.volM = data.volM ? data.volM : 0;
      return;
    }
  }

  area!: number;
  codActvAproveDetSub: string = '';
  codSubActvAproveDetSub: string = '';
  dapCM: string = '';
  descripcion: string = '';
  detalle: string = '';
  idActvAproveDetSub!: number;
  idUsuarioElimina!: number;
  idUsuarioRegistro!: number;
  nroArbol!: number;
  nroPC!: number;
  observacion: string = '';
  totalHa!: number;
  totalPC!: number;
  var: string = '';
  volM!: number;
}

export abstract class CodigosActividadesAprovechamiento {
  static readonly ACTIVIDAD: string = 'POCC';
  static readonly ACORDEON1: string = 'POCCAAPROCC';
  static readonly ACORDEON2: string = 'POCCAAPROVC';
  static readonly ACORDEON3: string = 'POCCAAPROOA';
  static readonly SUBACORDEON1_1: string = 'POCCAAPROCCME';
  static readonly SUBACORDEON1_2: string = 'POCCAAPROVCDD';
  static readonly SUBACORDEON1_3: string = 'POCCAAPROOALE';
  static readonly SUBACORDEON1_4: string = 'POCCAAPROCCVI';
  static readonly SUBACORDEON1_5: string = 'POCCAAPROVCML';
  static readonly SUBACORDEON1_6: string = 'POCCAAPROOARR';
  static readonly SUBACORDEON1_7: string = 'POCCAAPROCCAR';
  static readonly SUBACORDEON2_1: string = 'POCCAAPROVC';
  static readonly SUBACORDEON3_1: string = 'POCCAAPROOA';
}
