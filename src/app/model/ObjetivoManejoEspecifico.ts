import { SelectItem } from 'primeng/api';

export interface ObjetivoEspecificoManejoModel {
  desObjEspecifico: string;
  detalleOtro: string;
  estado: string;
  idObjEspecificoManejo: number;
  idTipoObjEspecifico: string;
  idUsuarioModificacion: number;
  idUsuarioRegistro: number;
}

export interface ListadoObjetivos {
  label: string;
  value?: any;
  items: SelectItem[];
}
export interface MaderablesNoMaderablesResponse {
  activo: string;
  codigoObjetivo: null;
  descripcion: null;
  descripcionDetalle: string;
  detalle: string;
  estado: null;
  fechaElimina: null;
  fechaModificacion: null;
  fechaRegistro: null;
  idObjManejo: null;
  idObjetivoDet: string;
  idPlanManejo: null;
  idUsuarioElimina: null;
  idUsuarioModificacion: null;
  idUsuarioRegistro: null;
  pageNum: null;
  pageSize: null;
  search: null;
  startIndex: null;
  totalPage: null;
  totalRecord: null;
}

export class DetalleObjetivo {
  idObjEspecificoManejo?: number = 0;
  codTipo?: string = '';
  tipoObjetivo?: string = '';
  descripcion?: string = '';
  detalle?: string = '';
  observacion?: string = '';
  activo?: string = '';
  idObjetivo?: number = 0;
  idUsuarioRegistro?: number = 0;

  constructor(data?: any) {
    if (data) {
      this.idObjEspecificoManejo = data.idObjetivoDet ? data.idObjetivoDet : 0;
      this.codTipo = data.codigoObjetivo ? data.codigoObjetivo : '';
      this.tipoObjetivo = data.detalle ? data.detalle : '';
      this.descripcion = data.descripcionDetalle ? data.descripcionDetalle : '';
      this.detalle = data.descripcion ? data.descripcion : '';
      this.observacion = data.observacion ? data.observacion : '';
      this.activo = data.activo ? data.activo : '';
      this.idObjetivo = data.idObjetivo ? data.idObjetivo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
    }
  }
}
