export class HidrografiaModel {

  idHidrografia: number;
  tipoHidrografia: string;
  nombre: string;
  descripcion: string;

  // estado: string;
  // idUsuarioRegistro: number;
  // fechaRegistro: string;
  // idUsuarioModificacion: number;
  // fechaModificacion: string;
  // idUsuarioElimina: number;
  // fechaElimina: string;


  constructor(obj?: Partial<HidrografiaModel>) {
    this.idHidrografia = 0;
    this.tipoHidrografia = "";
    this.nombre = "";
    this.descripcion = "";

    // this.estado = '';
    // this.idUsuarioRegistro = 0;
    // this.fechaRegistro = "";
    // this.idUsuarioModificacion = 0;
    // this.fechaModificacion = "";
    // this.idUsuarioElimina = 0;
    // this.fechaElimina = "";

    if (obj) Object.assign(this, obj);
  }
}