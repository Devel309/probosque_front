import { AuditoriaModel } from './auditoria';

export interface CronogramaActividad{ 
 estado:String    ,
 idUsuarioRegistro: Number,
 fechaRegistro: Date,
 idUsuarioModificacion: Number,
 fechaModificacion: Date,
 idUsuarioElimina: Number,
 fechaElimina: Date,
 id_CRON_ACTIVIDAD: Number,
 id_USUARIO_REGISTRO: Number,
 id_USUARIO_MODIFICACION: Number,
 fecha_MODIFICACION: Date,
 id_USUARIO_ELIMINA: Number,
 id_PLAN_MANEJO: Number,
 actividad: String
 fecha_REGISTRO: Date,
 ano: Number,
 fecha_ELIMINA: Date,
 importe: Number,
 a1:Number,
 a2:Number,
 a3:Number,
 a4:Number,
 a5:Number,
 a6:Number,
 a7:Number,
 a8:Number,
 a9:Number,
 a10:Number, 
 a11:Number

 a12:Number,
 a13:Number,
 a14:Number,
 a15:Number,
 a16:Number,
 a17:Number,
 a18:Number,
 a19:Number,
 a20:Number

}


 