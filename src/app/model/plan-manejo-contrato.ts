export class InformacionGeneralPlanManejoContrato {
  idContrato: number;
  idUsuarioTitular: number;
  nombreTitular: string;
  dniTitular: string;
  rucTitular: string;
  distritoTitular: number;
  provinciaTitular: number;
  departamentoTitular: number;
  domicilioLegalTitular: string;
  dniRepresentanteLegal: string;
  nombreRepresentanteLegal: string;
  tituloHabilitante: string;
  superficieUA: string;
  idUA: string;
  idPlanManejo: number;
  idPlanManejoContrato: number;
  nombresNroDocumento: string

  constructor(obj?: Partial<InformacionGeneralPlanManejoContrato>) {
    this.idContrato = 0;
    this.idUsuarioTitular = 0;
    this.nombreTitular = "";
    this.dniTitular = "";
    this.rucTitular = "";
    this.distritoTitular = 0;
    this.provinciaTitular = 0;
    this.departamentoTitular = 0;
    this.domicilioLegalTitular = "";
    this.dniRepresentanteLegal = "";
    this.nombreRepresentanteLegal = "";
    this.tituloHabilitante = "";
    this.superficieUA = "";
    this.idUA = "";
    this.idPlanManejo = 0;
    this.idPlanManejoContrato = 0;
    this.nombresNroDocumento = ''

    if (obj) Object.assign(this, obj);
  }
}
