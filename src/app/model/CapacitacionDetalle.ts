
import { AuditoriaModel } from "./auditoria";
import { CapacitacionModel } from "./Capacitacion";
export interface CapacitacionDetalleModel extends AuditoriaModel{
  idCapacitacionDet: number;
  descripcion: String;
  capacitacion:CapacitacionModel;
  actividad?: string
}
