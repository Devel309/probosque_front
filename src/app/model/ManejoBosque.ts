import { AuditoriaModel } from './auditoria';
import { PlanManejoModel } from './PlanManejo';
export interface ManejoBosqueAprovechamiento extends AuditoriaModel {
  idManBosqueAprove: number;
  idTipo: string;
  idTipoMetodoAprove: string;
  idTipoTransMaderable: string;
  idTipoTransAreaDestino: string;
  caminoAcceso: number;
  caminoPrincipal: number;
  descripcion: String;
  planManejo: PlanManejoModel;
  caminoDescripcion: string;
}

export interface ManBosqueAproveCamino extends AuditoriaModel {
  idManBosqueAproveCamino: number;
  manejoBosqueAprovechamiento: ManejoBosqueAprovechamiento;
  infraestructura: string;
  tipoInfraestructura: string;
  caracteristicasTecnicas: string;
  metodoConstruccion: string;
  manoObra: string;
  maquina: string;
}

export interface ManBosqueEspecieProteger extends AuditoriaModel {
  idManBosqueEspPro: number;
  idPlanManejo: number;
  idTipoEspecie: string;
  tipoEspecie: string;
  idEspecie: number;
  especie: string;
  justificacionEspecie: string;
}

export class ManejoBosqueCabecera {
  constructor(data?: any) {
    if (data) {
      this.idManejoBosque = data.idManejoBosque ? data.idManejoBosque : 0;
      this.tipo = data.tipo ? data.tipo : '';
      this.codigoManejo = data.codigoManejo ? data.codigoManejo : '';
      this.subCodigoManejo = data.subCodigoManejo ? data.subCodigoManejo : '';
      this.descripcionCab = data.descripcionCab ? data.descripcionCab : '';
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.listManejoBosqueDetalle = data.listManejoBosqueDetalle
        ? data.listManejoBosqueDetalle
        : [];
    }
  }
  idManejoBosque: number = 0;
  tipo: string = '';
  codigoManejo: string = '';
  subCodigoManejo: string = '';
  descripcionCab: string = '';
  idPlanManejo: number = 0;
  idUsuarioRegistro: number = 0;
  listManejoBosqueDetalle: ManejoBosqueDetalleModel[] = [];
}

export class ManejoBosqueDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.idManejoBosqueDet = data.idManejoBosqueDet
        ? data.idManejoBosqueDet
        : 0;
      this.codtipoManejoDet = data.codtipoManejoDet
        ? data.codtipoManejoDet
        : ''; //PGMFAIMP?
      this.catOrdenamiento = data.catOrdenamiento ? data.catOrdenamiento : '';
      this.ordenamiento = data.ordenamiento ? data.ordenamiento : '';
      this.actividad = data.actividad ? data.actividad : '';
      this.codOpcion = data.codOpcion ? data.codOpcion : '';
      this.descripcionOrd = data.descripcionOrd ? data.descripcionOrd : '';
      this.idtipoMetodoAprov = data.idtipoMetodoAprov
        ? data.idtipoMetodoAprov
        : '';
      this.tipoMetodoAprov = data.tipoMetodoAprov ? data.tipoMetodoAprov : '';
      this.idtipoTransMaderable = data.idtipoTransMaderable
        ? data.idtipoTransMaderable
        : '';
      this.tipoTransMaderable = data.tipoTransMaderable
        ? data.tipoTransMaderable
        : '';
      this.idtipoTransDestino = data.idtipoTransDestino
        ? data.idtipoTransDestino
        : '';
      this.tipoTransDestino = data.tipoTransDestino
        ? data.tipoTransDestino
        : '';
      this.nuCaminoAcceso = data.nuCaminoAcceso ? data.nuCaminoAcceso : 0;
      this.nuCaminoPrincipal = data.nuCaminoPrincipal
        ? data.nuCaminoPrincipal
        : 0;
      this.descripcionCamino = data.descripcionCamino
        ? data.descripcionCamino
        : '';
      this.descripcionOtro = data.descripcionOtro ? data.descripcionOtro : '';
      this.nuIdOperaciones = data.nuIdOperaciones ? data.nuIdOperaciones : '';
      this.manoObra = data.manoObra ? data.manoObra : '';
      this.nuIdEspecie = data.nuIdEspecie ? data.nuIdEspecie : 0;
      this.especie = data.especie ? data.especie : '';
      this.justificacionEspecie = data.justificacionEspecie
        ? data.justificacionEspecie
        : '';
      this.tratamientoSilvicultural = data.tratamientoSilvicultural
        ? data.tratamientoSilvicultural
        : '';
      this.descripcionTratamiento = data.descripcionTratamiento
        ? data.descripcionTratamiento
        : '';
      this.descripcionManoObra = data.descripcionManoObra
        ? data.descripcionManoObra
        : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.infraestructura = data.infraestructura ? data.infraestructura : '';
      this.caracteristicasTecnicas = data.caracteristicasTecnicas
        ? data.caracteristicasTecnicas
        : '';
      this.metodoConstruccion = data.metodoConstruccion
        ? data.metodoConstruccion
        : '';

      this.tipoEspecie = data.tipoEspecie ? data.tipoEspecie : '';
      this.maquina = data.maquina ? data.maquina : '';
      this.idOperacion = data.idOperacion ? data.idOperacion : '';
      this.descripcionOtros = data.descripcionOtros
        ? data.descripcionOtros
        : '';
      this.lineaProduccion = data.lineaProduccion ? data.lineaProduccion : '';
      this.nuDiametro = data.nuDiametro ? data.nuDiametro : 0;
      this.accion = data.accion ? data.accion : 0;
      this.subCodtipoManejoDet = data.subCodtipoManejoDet
        ? data.subCodtipoManejoDet
        : '';
      this.reforestacion = data.reforestacion ? data.reforestacion : '';
      this.puntoVertice =  data.puntoVertice ? data.puntoVertice : ''
      this.coordenadaEsteIni =  data.coordenadaEsteIni ? data.coordenadaEsteIni : ''
      this.coordenadaNorteIni =  data.coordenadaNorteIni ? data.coordenadaNorteIni : ''
      this.referencia =  data.referencia ? data.referencia : ''
      this.nuTotalVcp =  data.nuTotalVcp ? data.nuTotalVcp : 0
      this.equipo = data.equipo ? data.equipo : '';
      this.insumo = data.insumo ? data.insumo : '';
      this.personal = data.personal ? data.personal : '';
      this.observacion = data.observacion ? data.observacion : '';
    }
  }

  idManejoBosqueDet: number = 0;
  codtipoManejoDet: string = '';
  subCodtipoManejoDet: string = '';
  catOrdenamiento: string = '';
  ordenamiento: string = '';
  actividad: string = '';
  codOpcion: string = '';
  descripcionOrd: string = '';
  idtipoMetodoAprov: number = 0;
  tipoMetodoAprov: string = '';
  idtipoTransMaderable: number = 0;
  tipoTransMaderable: string = '';
  idtipoTransDestino: number = 0;
  tipoTransDestino: string = '';
  nuCaminoAcceso: string = '';
  nuCaminoPrincipal: string = '';
  descripcionCamino: string = '';
  descripcionOtro: string = '';
  nuIdOperaciones: string = '';
  manoObra: string = '';
  maquina: string = '';
  descripcionManoObra: string = '';
  nuIdEspecie: string = '';
  especie: string = '';
  justificacionEspecie: string = '';
  tratamientoSilvicultural: string = '';
  descripcionTratamiento: string = '';
  Reforestacion: string = '';
  infraestructura: string = '';
  caracteristicasTecnicas: string = '';
  metodoConstruccion: string = '';
  descripcionOtros: string = '';
  idOperacion: number = 0;
  lineaProduccion: string = '';
  nuDiametro: number = 0;
  accion: number = 0;
  tipoEspecie: string = '';
  idUsuarioRegistro: number = 0;
  reforestacion: string = '';
  puntoVertice: string = ''
  coordenadaEsteIni: string = ''
  coordenadaNorteIni: string = ''
  referencia: string = ''
  nuTotalVcp : number = 0;

  //
  equipo! : string;
  insumo! : string;
  personal! : string;
  observacion! : string;

}

export abstract class CodigosManejoBosque {
  static readonly CODIGO_PROCESO: string = 'PGMFA';

  static readonly CODIGO_TAB: string = 'PGMFAMB';

  static readonly ACORDEON_7_1: string = 'PGMFAMBAPO'; //acordeon 1 Principales según Ordenamiento
  static readonly ACORDEON_7_1_1: string = 'PGMFAMBDSM'; // Detalle Sistemas de manejo
  static readonly ACORDEON_7_1_2: string = 'PGMFAMBDPF'; // Detalle Producción Forestal
  static readonly ACORDEON_7_1_2_1: string = 'PGMFAACPORESP'; // especies aprovechar
  static readonly ACORDEON_7_1_2_0: string = 'PGMFAACPORESPVE ';
  static readonly ACORDEON_7_1_2_2: string = 'PGMFAACPORCOA';
  static readonly ACORDEON_7_1_2_3: string = 'PGMFAACPORESP';
  static readonly ACORDEON_7_1_2_4: string = 'PGMFAACPORBLO'; // Corta anual permisible
  static readonly ACORDEON_7_1_3: string = CodigosManejoBosque.ACORDEON_7_1 + 'BQ'; // Detalle Producción Forestal
  static readonly ACORDEON_7_1_4: string = CodigosManejoBosque.ACORDEON_7_1 + 'PC'; // Detalle Producción Forestal
  static readonly ACORDEON_7_1_5: string = CodigosManejoBosque.ACORDEON_7_1 + 'FC'; // Detalle Producción Forestal

  static readonly ACORDEON_7_2: string = 'PGMFAAPRO'; //acordeon 2  falta - codigo principal de acordeon
  static readonly ACORDEON_7_2_1: string = 'PGMFAMBMA '; // Método de Aprovechamiento
  static readonly ACORDEON_7_2_2: string = 'PGMFAMBIAT '; // Infraestructura para Aprovechamiento y Transporte
  static readonly ACORDEON_7_2_2_1: string = 'PGMFAAPROCA'; // espesificaciones sobre el camino
  static readonly ACORDEON_7_2_3: string = 'PGMFAMBOCA'; //Operaciones de Corta y Arrastre

  static readonly ACORDEON_7_3: string = 'PGMFAMBEP'; // acordeon 3 Especies a Proteger
  static readonly ACORDEON_7_3_1: string = 'PGMFAESPRFLO'; //  flora
  static readonly ACORDEON_7_3_2: string = 'PGMFAESPRFAU'; // fauna

  static readonly ACORDEON_7_4: string = 'PGMFAMBTS'; //acordeon 4 Tratamientos Silviculturales
  static readonly ACORDEON_7_4_1: string = 'PGMFATRASILT'; //  listado 1 Tratamientos previos a aplicar
  static readonly ACORDEON_7_4_2: string = 'PGMFATRASILR'; // listado 2 Reforestacion
}
