import { ObjetivoManejoModel } from "./ObjetivoManejo";
import { AuditoriaModel } from "./auditoria";
export interface ObjetivoEspecificoManejoModel extends AuditoriaModel
{
    idObjEspecificoManejo:number;
    objetivoManejo:ObjetivoManejoModel;
    desObjEspecifico:String;
    detalleOtro:String;
    idTipoObjEspecifico :String;
}
