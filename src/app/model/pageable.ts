import { Page } from "./page";

export class Pageable<T> extends Page {

    data!: T;
    success?: boolean | null;
    message?: string | null;

    constructor(obj?: Partial<Pageable<T>>) {
        super(obj);

        if (obj) Object.assign(this, obj);
    }
}
