export class ListarSolicitudOpinionRequest {

    siglaEntidad: string | null;
    tipoDocGestion: string | null;
    estSolicitudOpinion: string | null;
    nroDocGestion: number | null;
    tipoDocumento: number | null;
    fechaDocGestion: Date | null;
    pageNum: number;
    pageSize: number;
    idUsuarioRegistro: number | null;
  //  tipoBandeja?: number

    constructor(obj?: Partial<ListarSolicitudOpinionRequest>) {
        this.siglaEntidad = null;
        this.tipoDocGestion = null;
        this.estSolicitudOpinion = null;
        this.nroDocGestion = null;
        this.fechaDocGestion = null;
    //    this.tipoBandeja = 1;
        this.pageNum = 1;
        this.pageSize = 10;
        this.tipoDocumento = null
        this.idUsuarioRegistro = null
        if (obj) Object.assign(this, obj);
    }


}

export interface SolicitudOpinionRequest {
    idSolicitudOpinion: number,
    estSolicitudOpinion: string,
    idUsuarioModificacion: number,
    esFavorable: boolean,
    descripcion: string,
    tipoDoc: string,
    fechaEmision: Date,
    tipoDocumento: string
}

export interface SolicitudOpinionRegistro {
    siglaEntidad: string;
    tipoDocGestion: string;
    nroDocGestion: number;
    fechaDocGestion: Date;
    estSolicitudOpinion: string;
    tipoDocumento: string;
    numeroDocumento: string;
    asunto: string;
    idUsuarioRegistro: number;
    //fileDocumentoGestion: File;
    documentoGestion: number;
}

export interface ProcesoOfertaSolicitudOpinion {
    idProcesoOferta: number;
    idSolicitudOpinion: number;
    idUsuarioRegistro: number;
}