export interface ApiRegenteModel { 
    apellidos:String;
    id:number;
    nombres:String;
    numeroDocumento:String;
    numeroLicencia:String;
    periodo:number;
}
