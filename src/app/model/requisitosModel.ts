import { RequisitoSimpleModel } from "../shared/components/requisito-pgmf-simple/requisito-pgmf-simple.component";

export class Requisito1Model extends RequisitoSimpleModel {
  constructor(data?: any) {
    super(data);
  }
  validarTitular?: boolean = undefined;
  validarregente?: boolean = undefined;
}

export class RequisitoModel {
  constructor(data?: any) {
    if (data) {
      this.area = data.area;
      this.codigoRequisito = data.codigoRequisito;
      this.comentarios = data.comentarios;
      this.comunidad = data.comunidad;
      this.descripcion = data.descripcion;
      this.detalle = data.detalle;
      this.evaluacion = data.evaluacion;
      this.evaluacionDetalle = data.evaluacionDetalle;
      this.idPlanManejo = data.idPlanManejo;
      this.idRequisito = data.idRequisito;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.observacion = data.observacion;
      this.partidaRegistral = data.partidaRegistral;
      this.regente = data.regente;
      this.regenteHabilitado = data.regenteHabilitado;
      this.isRegenteHabilitado = data.isRegenteHabilitado;
      this.ruc = data.ruc;
      this.tipoRequisito = data.tipoRequisito;
      this.titular = data.titular;
      this.titularConsecion = data.titularConsecion;
      this.titularHabilitado = data.titularHabilitado;
      this.isTitularHabilitado = data.isTitularHabilitado;
      this.tituloHabilitante = data.tituloHabilitante;
      this.numeroDocumentoRegente = data.numeroDocumentoRegente;
      this.numeroDocumentoTitular = data.numeroDocumentoTitular;
      this.numeroDocumentoComunidad = data.numeroDocumentoComunidad;
      this.numeroDocumento = data.numeroDocumento;

      return;
    }
  }
  area: string | null = null;
  codigoRequisito: string | null = null;
  comentarios: string | null = null;
  comunidad: string | null = null;
  descripcion: string | null = null;
  detalle: string | null = null;
  evaluacion: string | null = null;
  idPlanManejo: number = 0;
  idRequisito: number = 0;
  idUsuarioRegistro: number = 0;
  observacion: string | null = null;
  partidaRegistral: string | null = null;
  regente: string | null = null;
  regenteHabilitado: string | null = null;
  isRegenteHabilitado: boolean | null = null;
  ruc: string | null = null;
  tipoRequisito: string | null = null;
  titular: string | null = null;
  titularConsecion: string | null = null;
  titularHabilitado: string | null = null;
  isTitularHabilitado: boolean | null = null;
  tituloHabilitante: string | null = null;
  numeroDocumentoRegente: string | null = null;
  numeroDocumentoTitular: string | null = null;
  numeroDocumentoComunidad: string | null = null;
  numeroDocumento: string | null = null;
  evaluacionDetalle: RequisitoDetalleModel[] = [];
}

export class RequisitoDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.codigoRequisitoDet = data.codigoRequisitoDet;
      this.conformidad = data.conformidad;
      this.isConformidad = data.isConformidad;
      this.descripcion = data.descripcion;
      this.detalle = data.detalle;
      this.idRequisitoDet = data.idRequisitoDet;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.medida = data.medida;
      this.observacion = data.observacion;
      this.fechaIni = data.fechaIni;
      this.fechaFin = data.fechaFin;
      return;
    }
  }
  codigoRequisitoDet: string | null = null;
  conformidad: string | null = null;
  isConformidad: boolean | null = null;
  descripcion: string | null = null;
  detalle: string | null = null;
  idRequisitoDet: number = 0;
  idUsuarioRegistro: number = 0;
  medida: string | null = null;
  observacion: string | null = null;
  fechaIni!: Date | string;
  fechaFin!: Date | string;
}
