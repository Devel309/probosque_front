import { AuditoriaModel } from "./auditoria";
import { ParametroModel } from "./Parametro";
import { PlanManejoModel } from "./PlanManejo";

export interface ProteccionBosqueDemarcacionModel extends AuditoriaModel {
  idProBosqueDem: number;
  planManejo: PlanManejoModel;
  parametro: ParametroModel;
  accion: Boolean;
  descripcion: String;
  implementacion: String;
}

export class ProteccionBosquePGMFAModel {
  constructor(data?: any) {
    if (data) {
  this.idProBosqueDet =  data.idProBosqueDet ? data.idProBosqueDet : 0;
  this.codPlanGeneralDet =  data.codPlanGeneralDet ? data.codPlanGeneralDet : ''; 
  this.subCodPlanGeneralDet =  data.subCodPlanGeneralDet ? data.subCodPlanGeneralDet : ''; 
  this.tipoMarcacion =  data.tipoMarcacion ? data.tipoMarcacion : '';
  this.nuAccion =  data.nuAccion ? data.nuAccion : false;
  this.implementacion =  data.implementacion ? data.implementacion :'';
  this.factorAmbiental = data.factorAmbiental ? data.factorAmbiental :'';
  this.impacto = data.impacto ? data.impacto :'';
  this.nuCenso =  data.nuCenso ? data.nuCenso : 0;
  this.nuDemarcacionLineal = data.nuDemarcacionLineal ? data.nuDemarcacionLineal : 0;
  this.nuConstruccionCampamento = data.nuConstruccionCampamento ? data.nuConstruccionCampamento : 0;
  this.nuConstruccionCamino = data.nuConstruccionCamino ? data.nuConstruccionCamino : 0;
  this.nuTala = data.nuTala ? data.nuTala : 0;
  this.nuArrastre = data.nuArrastre ? data.nuArrastre : 0;
  this.nuOtra = data.nuOtra ? data.nuOtra : 0;
  this.descOtra =  data.descOtra ? data.descOtra :'';
  this.tipoPrograma = data.tipoPrograma ? data.tipoPrograma :'';
  this.actividad = data.actividad ? data.actividad :'';
  this.mitigacionAmbiental = data.mitigacionAmbiental ? data.mitigacionAmbiental :'';
  this.impactoGestion =  data.impactoGestion ? data.impactoGestion :'';
  this.listProteccionBosqueActividad = data.listProteccionBosqueActividad ? data.listProteccionBosqueActividad:[]
    }
  }

  idProBosqueDet?: number;
  codPlanGeneralDet?: string; //PGMFAIMP? 
  subCodPlanGeneralDet?: string;
  tipoMarcacion?: string;
  nuAccion?: boolean;
  implementacion?: string;
  factorAmbiental?: string;
  impacto?: string;
  nuCenso?: number;
  nuDemarcacionLineal?: number;
  nuConstruccionCampamento?: number;
  nuConstruccionCamino?: number;
  nuTala?: number;
  nuArrastre?: number;
  nuOtra?: number;
  descOtra?: string;
  tipoPrograma?: string;
  actividad?: string;
  mitigacionAmbiental?: string;
  impactoGestion?: string;
  listProteccionBosqueActividad?: ListProteccionBosqueActividad[]
}

export class ListProteccionBosqueActividad {
  constructor(data?: any) {
    if (data) {
  this.idProBosqueActividad =  data.idProBosqueActividad ? data.idProBosqueActividad : 0;
  this.codigo =  data.codigo ? data.codigo : ''; 
  this.descripcion =  data.descripcion ? data.descripcion : '';
  this.accion =  data.accion ? data.accion : 0;
    }
  }

  idProBosqueActividad?:number;
  codigo?:string;
  descripcion?: string;
  accion?:number
}


export class ProteccionBosquePGMFAModel2 {
  constructor(data?: any) {
    if (data) {
  this.idProBosqueDet =  data.idProBosqueDet ? data.idProBosqueDet : 0;
  this.codPlanGeneralDet =  data.codPlanGeneralDet ? data.codPlanGeneralDet : ''; 
  this.subCodPlanGeneralDet =  data.subCodPlanGeneralDet ? data.subCodPlanGeneralDet : ''; 
  this.tipoMarcacion =  data.tipoMarcacion ? data.tipoMarcacion : '';
  this.nuAccion =  data.nuAccion ? data.nuAccion : false;
  this.implementacion =  data.implementacion ? data.implementacion :'';
  this.factorAmbiental = data.factorAmbiental ? data.factorAmbiental :'';
  this.impacto = data.impacto ? data.impacto :'';
  this.nuCenso =  data.nuCenso ? data.nuCenso : 0;
  this.nuDemarcacionLineal = data.nuDemarcacionLineal ? data.nuDemarcacionLineal : 0;
  this.nuConstruccionCampamento = data.nuConstruccionCampamento ? data.nuConstruccionCampamento : 0;
  this.nuConstruccionCamino = data.nuConstruccionCamino ? data.nuConstruccionCamino : 0;
  this.nuTala = data.nuTala ? data.nuTala : 0;
  this.nuArrastre = data.nuArrastre ? data.nuArrastre : 0;
  this.nuOtra = data.nuOtra ? data.nuOtra : 0;
  this.descOtra =  data.descOtra ? data.descOtra :'';
  this.tipoPrograma = data.tipoPrograma ? data.tipoPrograma :'';
  this.actividad = data.actividad ? data.actividad :'';
  this.mitigacionAmbiental = data.mitigacionAmbiental ? data.mitigacionAmbiental :'';
  this.impactoGestion =  data.impactoGestion ? data.impactoGestion :'';
  this.listProteccionBosqueActividad = data.listProteccionBosqueActividad ? data.listProteccionBosqueActividad:[]
    }
  }

  idProBosqueDet?: number;
  codPlanGeneralDet?: string; //PGMFAIMP? 
  subCodPlanGeneralDet?: string;
  tipoMarcacion?: string;
  nuAccion?: boolean;
  implementacion?: string;
  factorAmbiental?: string;
  impacto?: string;
  nuCenso?: number;
  nuDemarcacionLineal?: number;
  nuConstruccionCampamento?: number;
  nuConstruccionCamino?: number;
  nuTala?: number;
  nuArrastre?: number;
  nuOtra?: number;
  descOtra?: string;
  tipoPrograma?: string;
  actividad?: string;
  mitigacionAmbiental?: string;
  impactoGestion?: string;
  listProteccionBosqueActividad?: ListProteccionBosqueActividad2[]
}

export class ListProteccionBosqueActividad2 {
  constructor(data?: any) {
    if (data) {
  this.idProBosqueActividad =  data.idProBosqueActividad ? data.idProBosqueActividad : 0;
  this.codigo =  data.codigo ? data.codigo : ''; 
  this.descripcion =  data.descripcion ? data.descripcion : '';
  this.accion =  data.accion ? data.accion : 0;
    }
  }

  idProBosqueActividad?:number;
  codigo?:string;
  descripcion?: string;
  accion?:any
}