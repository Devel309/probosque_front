export class ListarEvaluacionCampoRequest {

    apellidoTitular: string;
    idEvaluacionCampo: number | null;
    idEvaluacionCampoEstado: number;
    documentoGestion: number | null;
    idUsuarioElimina: number;
    idUsuarioModificacion: number;
    idUsuarioRegistro: number;
    numThActoAdmin: string;
    search: string;
    startIndex: number;
    tipoDocumentoGestion: string;
    tipoDocumentoGestionTexto: string;
    titular: string;
    pageNum: number;
    pageSize: number;
    idEvaluacionCampoVersion: number | null;
    siglaEntidad: string | null;
    nombreTitular: string | null;




    constructor(obj?: Partial<ListarEvaluacionCampoRequest>) {

        this.apellidoTitular = "";
        this.idEvaluacionCampo = null;
        this.idEvaluacionCampoEstado = 0;
        this.documentoGestion = null;

        this.tipoDocumentoGestionTexto =  "";
        this.idUsuarioElimina = 0;
        this.idUsuarioModificacion = 0;
        this.idUsuarioRegistro = 0;
        this.numThActoAdmin = "";
        this.search = "";
        this.startIndex = 0;
        this.tipoDocumentoGestion = "";
        this.titular = "";
        this.pageNum = 1;
        this.pageSize = 10;
        this.idEvaluacionCampoVersion = null;
        this.siglaEntidad = "";
        this.nombreTitular = null;
        if (obj) Object.assign(this, obj);
    }


}


export interface EvaluacionCampo {
    apellidoTitular: string;
    cites: boolean;
    comunidadNatidadCampesina: boolean;
    evaluacionCampoEstado: string;
    fechaElimina: Date;
    fechaInicioEvaluacion: Date;
    fechaFinEvaluacion: Date;
    fechaInicioImpedimentos: Date;
    fechaFinImpedimentos: Date;
    fechaModificacion: Date;
    fechaRegistro: Date;
    idEvaluacionCampo: number;
    idEvaluacionCampoEstado: number;
    idEvaluacionCampoVersion: number;
    documentoGestion: number;
    tipoDocumentoGestion: string;
    idUsuarioRegistro: number;
    notificacion: boolean;
    numThActoAdmin: string;
    resultadoFavorable: boolean;
    tipoDocumentoGestionTexto: string;
    titular: string;
}


export interface EvaluacionCampoRegistro {
    cites: boolean;
    comunidadNatidadCampesina: boolean;
    fechaFinEvaluacion: Date;
    fechaFinImpedimentos: Date;
    fechaInicioEvaluacion: Date;
    fechaInicioImpedimentos: Date;
    idEvaluacionCampo: number;
    idEvaluacionCampoEstado: number;
  //  documentoGestion: number;
   // tipoDocumentoGestion: string;
    idUsuarioRegistro: number;
    notificacion: boolean;
    resultadoFavorable: boolean;
}

export interface EvaluacionCampoAutoridad {
    idEvaluacionCampo: number;
    idEvaluacionCampoAutoridad: number;
    idTipoAutoridad: number;
    tipoAutoridad: number;
    idUsuarioRegistro: number;
}
