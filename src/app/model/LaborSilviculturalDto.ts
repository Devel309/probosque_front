import { AccionTipo } from "../shared/enums";
import { Actividades, Detalle } from "./ActividadesSilviculturalesModel";
import { AuditoriaDto } from "./auditoria";


export class LaborSilviculturalDto extends AuditoriaDto {
    accion!: AccionTipo;
    marca?: Boolean;
    actividad?: string = "";
    anexo: string = "";
    codActividad!: string;
    descripcion?: string = "";
    descripcionDetalle: string = "";
    idActividadSilvicultural!: number;
    idActividadSilviculturalDet!: number;
    idPlanManejo!: number;
    idTipo!: string;
    idTipoTratamiento!: string;
    justificacion!: string;
    monitoreo !: string;
    observaciones?: string = "";
    tipoTratamiento: string = "";
    tratamiento!: string;

    tipoActividad: number = 0;
    equipos: string = "";
    insumos: string = "";
    personal: string = "";
    id: string = "";

    constructor(data?: Detalle) {
        super();

        if (data) {
            this.id = data.idActividadSilviculturalDet === undefined || data.idActividadSilviculturalDet == null ? "" : data.idActividadSilviculturalDet.toString();
            this.tipoActividad = parseInt(data.idTipoTratamiento === undefined || data.idTipoTratamiento == null ? "0" : data.idTipoTratamiento);
            this.marca = (data.accion) ?? true;
            this.actividad = data.actividad;
            this.descripcion = data.descripcionDetalle;
            this.equipos = data.equipo === undefined || data.equipo == null ? '' : data.equipo;
            this.insumos = data.insumo === undefined || data.insumo == null ? '' : data.insumo;
            this.personal = data.personal === undefined || data.personal == null ? '' : data.personal;
            this.observaciones = data.observacionDetalle;
            return;
        }
    }
    
}