export interface TipoProcesoModel{
    idTipoProceso:number;
    descripcion:String;
    nivel:number;
    codigo:String;
    codigoPadre:String;
}