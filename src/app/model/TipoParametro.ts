import { AuditoriaModel } from "./auditoria";
export interface TipoParametroModel extends AuditoriaModel  { 
    idTipoParametro:number;
    prefijo:String;
    nombre:String;
    descripcion:String;
   
}