
import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
import { ResultArchivoModel } from "./ResultArchivo";

export interface ProduccionRecursoForestalModel extends AuditoriaModel {
    planManejo: PlanManejoModel;
    idPotProdForestal: number;
    idTipoBosque: number;
    tipoBosque: string;
    codigoTipoPotProdForestal: number;
    estado: string;
    listproduccionRecursoForestalEspecieDto: ProduccionRecursoForestalDetalleModel[];
}

export interface ProduccionRecursoForestalDetalleModel {
    idPotProdForestalDet: number;
    idEspecie: number;
    especie: string;
    listProduccionRecursoForestalDetalle: VariablersoForestalDetModel[];
}

export interface VariablersoForestalDetModel {
    idVariable: number;
    variable: string;
    nuTotalTipoBosque: number;
    totalHa: number;
    accion: boolean;
}