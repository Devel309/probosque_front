import { AuditoriaDto } from "./auditoria";

export class ContratoExternoModel extends AuditoriaDto {
  areaBosqueProduccionForestal: number;
  areaProteccion: number;
  areaTotalConsecion: number;
  certificadoHabilitacionProfecionalRegenteForestal: string;
  contratoSuscritoTitularTituloHabilitante: string;
  departamento: string;
  distrito: string;
  dni: string;
  domicilioLegal: string;
  domicilioLegalRegente: string;
  duracionPGMF: number;
  fechaFinPGMF: string;
  fechaInicioPGMF: string;
  fechaPrecentacionPGMF: string;
  idPgmf: number;
  idRegenteForestal: number;
  nombreRegenteForestal: string;
  nombreReprecentanteLegal: string;
  nombreTitular: string;
  nroContratoConcesion: string;
  numeroBlouesQuinquenales: string;
  numeroInsccripcion: number;
  potencialMaderable: number;
  provincia: string;
  volumenCortaAnualPermisible: number;

  constructor(obj?: Partial<ContratoExternoModel>) {
    super();
    this.areaBosqueProduccionForestal = 0;
    this.areaProteccion = 0;
    this.areaTotalConsecion = 0;
    this.certificadoHabilitacionProfecionalRegenteForestal = '';
    this.contratoSuscritoTitularTituloHabilitante = '';
    this.departamento = '';
    this.distrito = '';
    this.dni = '';
    this.domicilioLegal = '';
    this.domicilioLegalRegente = '';
    this.duracionPGMF = 0;
    this.fechaFinPGMF = '';
    this.fechaInicioPGMF = '';
    this.fechaPrecentacionPGMF = '';
    this.idPgmf = 0;
    this.idRegenteForestal = 0;
    this.nombreRegenteForestal = '';
    this.nombreReprecentanteLegal = '';
    this.nombreTitular = '';
    this.nroContratoConcesion = '';
    this.numeroBlouesQuinquenales = '';
    this.numeroInsccripcion = 0;
    this.potencialMaderable = 0;
    this.provincia = '';
    this.volumenCortaAnualPermisible = 0;
    if (obj) Object.assign(this, obj);
  }
}