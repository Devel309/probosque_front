import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
import { ResultArchivoModel } from "./ResultArchivo";
export interface OrdenamientoAreaManejoModel extends AuditoriaModel  {
    idOrdAreaManejo:number;
    planManejo:PlanManejoModel;
    catOrdenamiento:String;
    area:number;
    porcentaje:number;
    accion:Boolean;
    file:Boolean;
    modificar:Boolean;
    resultArchivo:ResultArchivoModel;
    idLayer:String;
    inServer:Boolean;
}
