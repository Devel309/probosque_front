export class PmfiDatosOtorgamiento {
  idPermisoForestal: number;
  idOtorgamiento: number;
  codPermiso: string;
  codOtorgamiento?: any;
  codTipo?: any;
  estadoSolicitud?: any;
  nroRuc: string;
  nroTituloPropiedad: string;
  nroPartidaRegistral: string;
  nroActaAsambleaRrll: string;
  nroOperacionComprobantePago: string;
  areaComunidad: number;
  nroActaAcuerdoAsamblea: string;
  codTipoAprovechamiento: string;
  nroContratoTercero: string;
  nroActaAsamblea: string;
  escalaManejo: string;
  descripcion?: any;
  detalle?: any;
  observacion?: any;
  idUsuarioRegistro: string;
  regente: PfcrRegente;

  constructor(obj?: Partial<PmfiDatosOtorgamiento>) {
    this.idPermisoForestal = 0;
    this.idOtorgamiento = 0;
    this.codPermiso = 'PFCR';
    this.codOtorgamiento = null;
    this.codTipo = null;
    this.estadoSolicitud = null;
    this.nroRuc = '';
    this.nroTituloPropiedad = '';
    this.nroPartidaRegistral = '';
    this.nroActaAsambleaRrll = '';
    this.nroOperacionComprobantePago = '';
    this.areaComunidad = 0;
    this.nroActaAcuerdoAsamblea = '';
    this.codTipoAprovechamiento = '';
    this.nroContratoTercero = '';
    this.nroActaAsamblea = '';
    this.escalaManejo = '';
    this.descripcion = null;
    this.detalle = null;
    this.observacion = null;
    this.idUsuarioRegistro = '';
    this.regente = new PfcrRegente();

    if (obj) Object.assign(this, obj);
  }
}

export class PfcrRegente {
  adjuntoCertificado?: any;
  adjuntoContrato?: any;
  apellidos: string;
  certificadoHabilitacion: string;
  codigoProceso: string;
  codigoTipoRegente?: any;
  contratoSuscrito?: any;
  domicilioLegal?: any;
  estadoRegente?: any;
  idPlanManejo: number;
  idPlanManejoRegente: number;
  idUsuarioRegistro: number;
  nombres: string;
  numeroDocumento: string;
  numeroInscripcion?: any;
  numeroLicencia: string;
  periodo: string;
  estado?: any;

  constructor(obj?: Partial<PfcrRegente>) {
    this.adjuntoCertificado = null;
    this.adjuntoContrato = null;
    this.apellidos = '';
    this.certificadoHabilitacion = '';
    this.codigoProceso = 'PFCR';
    this.codigoTipoRegente = null;
    this.contratoSuscrito = null;
    this.domicilioLegal = null;
    this.estadoRegente = null;
    this.idPlanManejo = 0;
    this.idPlanManejoRegente = 0;
    this.idUsuarioRegistro = 0;
    this.nombres = '';
    this.numeroDocumento = '';
    this.numeroInscripcion = null;
    this.numeroLicencia = '';
    this.periodo = '';
    this.estado = null;

    if (obj) Object.assign(this, obj);
  }
}

export class ListArchivo {
  idOtorgamientoArchivo: number;
  idOtorgamiento: number;
  codOtorgamientoArchivo?: any;
  idArchivo: number;
  idTipoDocumento: number;
  descripcion?: any;
  detalle?: any;
  observacion?: any;
  idUsuarioRegistro: number;

  constructor(obj?: Partial<ListArchivo>) {
    this.idOtorgamientoArchivo = 0;
    this.idOtorgamiento = 0;
    this.codOtorgamientoArchivo = null;
    this.idArchivo = 0;
    this.idTipoDocumento = 0;
    this.descripcion = null;
    this.detalle = null;
    this.observacion = null;
    this.idUsuarioRegistro = 0;

    if (obj) Object.assign(this, obj);
  }
}
