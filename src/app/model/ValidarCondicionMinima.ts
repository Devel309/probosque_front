export interface ValidarCondicionMinima {
    descripcion: string;
    fechaEvaluacion: string;
    idUsuarioRegistro: number;
    nroDocumento: string;
    resultadoEvaluacion: string;
    tipoDocumento: string;
    condicionMinimaDetalle: CondicionMinimaDetalle[];
}

export interface CondicionMinimaDetalle {
    codigoTipoCondicionMinimaDet: string;
    fechaIngreso: string;
    fechaVigencia: string;
    idUsuarioRegistro: number;
    resultado: string;
}