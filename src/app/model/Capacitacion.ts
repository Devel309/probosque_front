
import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
export interface CapacitacionModel extends AuditoriaModel{
    idCapacitacion: number;
    codTipoCapacitacion: String;
    tema:string;
    personaCapacitar:string;
    idTipoModalidad:any;
    lugar:string;
    periodo:string;
    responsable:string;
    lugarCapacitacion:string;
    planManejo:PlanManejoModel;
}
