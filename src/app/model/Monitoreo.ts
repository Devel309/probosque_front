import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
export interface MonitoreoModel extends AuditoriaModel  {
    idMonitoreo:number;
    planManejo:PlanManejoModel;
    descripcion:String;

}
