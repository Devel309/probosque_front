export abstract class CodigoEstadoBL {
  static readonly BORRADOR: string = "SEBESTBORR";            //Borrador
  static readonly EVAL_BENEF: string = "SEBESTEVAB";          //En Evaluación de Beneficiarios
  static readonly EVAL_DESFAVORABLE: string = "SEBESTEVAD";          //Evaluación Desfavorable
  static readonly PUBLICA_BENEF: string = "SEBESTPUBB";       //Publicación de Beneficiarios
  static readonly PATRON_FINAL_BENEF: string = "SEBESTPAFB";  //Con Padrón Final de Beneficiarios
  static readonly CREACION_COMITE: string = "SEBESTCOMI";        //Creacion comite Técnico
  static readonly EVAL_EXPED_TEC: string = "SEBESTEVET";      //Evaluación de Expediente Técnico
  static readonly OBS_EXPED_TEC: string = "SEBESTOBS";        //Expediente Técnico Observado
  static readonly FAV_EXPED_TEC: string = "SEBESTEFAV";       //Expediente Técnico Favorable
  static readonly DESFAV_EXPED_TEC: string = "SEBESTEDFA";       //Expediente Técnico Desfavorable
  static readonly APROBADA: string = "SEBESTAPR";        //Solicitud Aprobada
  static readonly DENEGADA: string = "SEBESTDEN";        //Solicitud Denegada
  static readonly SOL_TH: string = "SEBESTTH";        //Solicitud con Título Habilitante
}
