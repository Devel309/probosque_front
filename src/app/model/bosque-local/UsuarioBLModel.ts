

export class UsuarioBLModel {
   idComite!:number|null;
   idSolBosqueLocal!:number|null;
   idPersona!: number | null;
   idUsuario!: number | null;
   nombres!: string;
   apellidoPaterno!: string;
   apellidoMaterno!: string;
   telefono!: string;
   numeroDocumento!: string;
   emailUsuario!: string;
   perfil!:string;
   departamento!:String;
   idDocAcreditacion!:Number;


   constructor(obj?: Partial<UsuarioBLModel>) {

       this.idComite = 0;
       this.idSolBosqueLocal = 0;
       this.idPersona = 0;
       this.idUsuario = 0;
       this.nombres = '';
       this.apellidoPaterno = '';
       this.apellidoMaterno = '';
       this.telefono = '';
       this.numeroDocumento = '';
       this.emailUsuario = '';
       this.perfil = "";
       this.departamento = "";
       this.idDocAcreditacion = 0;


       if (obj) Object.assign(this, obj);

   }
}