export class ModelBeneficiarioBL {
  constructor() { }

  idSolBosqueLocal: number = 0;
  idBosqueLocalBeneficiario: number = 0;
  condicion: string | null = null;
  condicionDescripcion: string | null = "";
  tipoDocumento: string = "DNI";
  numeroDocumento: string = "";
  nombreCompleto: string = "";
  residencia: string = "";
  flagActualizarResidencia: boolean = false;
  telefono: string = "";
  email: string = "";
  cargaFamiliar: string = "";
  idCertificado: number | null = null;
  declaracionJurada: number | null = null;
  idUsuario: number | null = null;
  ubigeoTexto: string | null = "";

  setData(obj: any) {
    this.idSolBosqueLocal = obj.idSolBosqueLocal || 0;
    this.idBosqueLocalBeneficiario = obj.idBosqueLocalBeneficiario || 0;
    this.condicion = obj.condicion || "";
    this.condicionDescripcion = obj.condicionDescripcion || "";
    this.tipoDocumento = obj.tipoDocumento || "";
    this.numeroDocumento = obj.numeroDocumento || "";
    this.nombreCompleto = obj.nombreCompleto || "";
    this.residencia = obj.residencia || "";
    this.flagActualizarResidencia = obj.flagActualizarResidencia || false;
    this.telefono = obj.telefono || "";
    this.email = obj.email || "";
    this.cargaFamiliar = obj.cargaFamiliar || "";
    this.idCertificado = obj.idCertificado || null;
    this.declaracionJurada = obj.declaracionJurada || null;
    this.idUsuario = obj.idUsuario || null;
    this.ubigeoTexto = obj.ubigeoTexto || "";
  }
}