export class InformacionBasicaPGMFIC {
  constructor(data?: any) {
    if (data) {
      this.idInfBasicaDet = data.idInfBasicaDet ? data.idInfBasicaDet : 0;
      this.codInfBasicaDet = data.codInfBasicaDet ? data.codInfBasicaDet : "";
      this.codSubInfBasicaDet = data.codSubInfBasicaDet
        ? data.codSubInfBasicaDet
        : "";
      this.areaHa = data.areaHa ? data.areaHa : 0;
      this.areaHaPorcentaje = data.areaHaPorcentaje ? data.areaHaPorcentaje : 0;
      this.nombre = data.nombre ? data.nombre : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.observaciones = data.observaciones ? data.observaciones : "";
      this.idTipoBosque = data.idTipoBosque ? data.idTipoBosque : 0;
    }
  }

  idInfBasicaDet: number = 0;
  codInfBasicaDet: string = "";
  codSubInfBasicaDet: string = "";
  areaHa: number = 0;
  areaHaPorcentaje: number = 0;
  nombre: string = "";
  descripcion: string = "";
  observaciones: string = "";
  idTipoBosque: number = 0;
}
