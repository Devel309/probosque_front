import { AuditoriaModel } from './auditoria';
import { PlanManejoModel } from './PlanManejo';

export class ManejoForestalCabecera {


  constructor(data?: any) {


    if (data) {
      this.idManejoBosque = data.idManejoBosque ? data.idManejoBosque : 0;
      this.tipo = data.tipo ? data.tipo : '';
      this.codigoManejo = data.codigoManejo ? data.codigoManejo : '';
      this.subCodigoManejo = data.subCodigoManejo ? data.subCodigoManejo : '';
      this.descripcionCab = data.descripcionCab ? data.descripcionCab : '';
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;

      this.txDetalle = data.txDetalle ? data.txDetalle : '';
      this.txObservacion = data.txObservacion ? data.txObservacion : '';

      this.listManejoBosqueDetalle = data.listManejoBosqueDetalle
        ? data.listManejoBosqueDetalle
        : [];
    }
  }
  idManejoBosque: number = 0;
  tipo: string = '';
  codigoManejo: string = '';
  subCodigoManejo: string = '';
  descripcionCab: string = '';
  idPlanManejo: number = 0;
  idUsuarioRegistro: number = 0;
  txDetalle: string = '';
  txObservacion: string = '';
  anio:number | null=null;
  codigoEscala:string='';
  listManejoBosqueDetalle: ManejoForestalDetalleModel[] = [];
}

export class ManejoForestalDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.idArchivo = data.idArchivo ? data.idArchivo : null;
      this.idManejoBosqueDet = data.idManejoBosqueDet
        ? data.idManejoBosqueDet
        : 0;
      this.codtipoManejoDet = data.codtipoManejoDet
        ? data.codtipoManejoDet
        : ''; //PGMFAIMP?
      this.catOrdenamiento = data.catOrdenamiento ? data.catOrdenamiento : '';
      this.ordenamiento = data.ordenamiento ? data.ordenamiento : '';
      this.actividad = data.actividad ? data.actividad : '';
      this.codOpcion = data.codOpcion ? data.codOpcion : '';
      this.descripcionOrd = data.descripcionOrd ? data.descripcionOrd : '';
      this.idtipoMetodoAprov = data.idtipoMetodoAprov
        ? data.idtipoMetodoAprov
        : '';
      this.tipoMetodoAprov = data.tipoMetodoAprov ? data.tipoMetodoAprov : '';
      this.idtipoTransMaderable = data.idtipoTransMaderable
        ? data.idtipoTransMaderable
        : '';
      this.tipoTransMaderable = data.tipoTransMaderable
        ? data.tipoTransMaderable
        : '';
      this.idtipoTransDestino = data.idtipoTransDestino
        ? data.idtipoTransDestino
        : '';
      this.tipoTransDestino = data.tipoTransDestino
        ? data.tipoTransDestino
        : '';
      this.nuCaminoAcceso = data.nuCaminoAcceso ? data.nuCaminoAcceso : 0;
      this.nuCaminoPrincipal = data.nuCaminoPrincipal
        ? data.nuCaminoPrincipal
        : 0;
      this.descripcionCamino = data.descripcionCamino
        ? data.descripcionCamino
        : '';
      this.descripcionOtro = data.descripcionOtro ? data.descripcionOtro : '';
      this.nuIdOperaciones = data.nuIdOperaciones ? data.nuIdOperaciones : '';
      this.manoObra = data.manoObra ? data.manoObra : '';
      this.nuIdEspecie = data.nuIdEspecie ? data.nuIdEspecie : 0;
      this.especie = data.especie ? data.especie : '';
      this.justificacionEspecie = data.justificacionEspecie
        ? data.justificacionEspecie
        : '';
      this.tratamientoSilvicultural = data.tratamientoSilvicultural
        ? data.tratamientoSilvicultural
        : '';
      this.descripcionTratamiento = data.descripcionTratamiento
        ? data.descripcionTratamiento
        : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.infraestructura = data.infraestructura ? data.infraestructura : '';
      this.caracteristicasTecnicas = data.caracteristicasTecnicas
        ? data.caracteristicasTecnicas
        : '';
      this.metodoConstruccion = data.metodoConstruccion
        ? data.metodoConstruccion
        : '';

      this.tipoEspecie = data.tipoEspecie ? data.tipoEspecie : '';
      this.maquina = data.maquina ? data.maquina : '';
      this.idOperacion = data.idOperacion ? data.idOperacion : '';
      this.descripcionOtros = data.descripcionOtros
        ? data.descripcionOtros
        : '';
      this.lineaProduccion = data.lineaProduccion ? data.lineaProduccion : '';
      this.nuDiametro = data.nuDiametro ? data.nuDiametro : 0;
      this.accion = data.accion ? data.accion : false;
      this.subCodtipoManejoDet = data.subCodtipoManejoDet
        ? data.subCodtipoManejoDet
        : '';
      this.reforestacion = data.reforestacion ? data.reforestacion : '';

      this.nuTotalNroArboles = data.nuTotalNroArboles
        ? data.nuTotalNroArboles
        : 0;
      this.nuTotalVcp = data.nuTotalVcp ? data.nuTotalVcp : 0;
      this.nuValorVcpProd = data.nuValorVcpProd ? data.nuValorVcpProd : 0;
      this.nuValorVcap = data.nuValorVcap ? data.nuValorVcap : 0;

      this.listManejoBosqueAnios = data.listManejoBosqueAnios
        ? data.listManejoBosqueAnios
        : [];
    }
  }

  idArchivo: number | null = null;
  idManejoBosqueDet: number = 0;
  codtipoManejoDet: string = '';
  subCodtipoManejoDet: string = '';
  catOrdenamiento: string = '';
  ordenamiento: string = '';
  actividad: string = '';
  codOpcion: string = '';
  descripcionOrd: string = '';
  idtipoMetodoAprov: number = 0;
  tipoMetodoAprov: string = '';
  idtipoTransMaderable: number = 0;
  tipoTransMaderable: string = '';
  idtipoTransDestino: number = 0;
  tipoTransDestino: string = '';
  nuCaminoAcceso: string = '';
  nuCaminoPrincipal: string = '';
  descripcionCamino: string = '';
  descripcionOtro: string = '';
  nuIdOperaciones: string = '';
  manoObra: string = '';
  maquina: string = '';
  descripcionManoObra: string = '';
  nuIdEspecie: string = '';
  especie: string = '';
  justificacionEspecie: string = '';
  tratamientoSilvicultural: string = '';
  descripcionTratamiento: string = '';
  Reforestacion: string = '';
  infraestructura: string = '';
  caracteristicasTecnicas: string = '';
  metodoConstruccion: string = '';
  descripcionOtros: string = '';
  idOperacion: number = 0;
  lineaProduccion: string = '';
  nuDiametro: number = 0;
  accion: boolean = false;
  tipoEspecie: string = '';
  idUsuarioRegistro: number = 0;
  reforestacion: string = '';
  nuTotalNroArboles: number = 0;
  nuTotalVcp: number = 0;
  nuValorVcpProd: number = 0;
  nuValorVcap: number = 0;
  listManejoBosqueAnios: ManejoForestalAnios[] = [];
}

export class ManejoForestalAnios {
  constructor(data?: any) {
    this.idManejoBosqueAnios = data.idManejoBosqueAnios
      ? data.idManejoBosqueAnios
      : 0;
    this.idUsuarioRegistro = data.idUsuarioRegistro
      ? data.idUsuarioRegistro
      : 0;
    this.codigo = data.codigo ? data.codigo : '';
    this.descripcion = data.descripcion ? data.descripcion : '';
    this.detalle = data.detalle ? data.detalle : '';
    this.observacion = data.observacion ? data.observacion : '';
  }

  idManejoBosqueAnios: number = 0;
  codigo: string = '';
  descripcion: string = '';
  detalle: string = '';
  observacion: string = '';
  idUsuarioRegistro: number = 0;
}
export abstract class CodigosManejoBosquePGMF {
  static readonly CODIGO_PROCESO: string = 'PGMF';

  static readonly CODIGO_TAB: string = `PGMFCFFMMF`;

  static readonly ACORDEON_6_1: string = `PGMFCFFMMFUPCO`; // Uso potencial categoria ordenamiento

  static readonly ACORDEON_6_2: string = `PGMFCFFMMFST`; // Detalle Sistemas de manejo

  static readonly ACORDEON_6_3: string = `PGMFCFFMMFCC`; // Detalle Producción Forestal

  static readonly ACORDEON_6_4: string = `PGMFCFFMMFEMDMC`; // Especies a Proteger

  static readonly ACORDEON_6_5: string = `PGMFCFFMMFEFP`; // Especies a Proteger

  static readonly ACORDEON_6_6: string = `PGMFCFFMMFCAP`; // Corta anual permisible
  static readonly ACORDEON_6_6_1: string = `PGMFCFFMMFCAPVCPHA`; // Corta anual permisible
  static readonly ACORDEON_6_6_2: string = `PGMFCFFMMFCAPVCAP`; // Corta anual permisible

  static readonly ACORDEON_6_7: string = `PGMFCFFMMFPC`; // Corta anual permisible
  static readonly ACORDEON_6_8: string = `PGMFCFFMMFESA`; // Corta anual permisible

  static readonly ACORDEON_6_9: string = `PGMFCFFMMFEPS`; // Corta anual permisible
  static readonly ACORDEON_6_9_1: string = `PGMFCFFMMFEPSDIS`; // Corta anual permisible
  static readonly ACORDEON_6_9_2: string = `PGMFCFFMMFEPSTSA`; // Corta anual permisible
  static readonly ACORDEON_6_9_3: string = `PGMFCFFMMFEPSMEAD`; // Corta anual permisible
}
