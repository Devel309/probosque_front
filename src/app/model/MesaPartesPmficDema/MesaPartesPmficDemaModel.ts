export class MesaPartesPmficDema {
  search?: any = null;
  pageNum?: any = null;
  pageSize?: any = null;
  totalRecord?: any = null;
  totalPage?: any = null;
  startIndex?: any = null;
  codigoPerfil?: any = null;
  estado: string = '';
  idUsuarioRegistro?: any = null;
  fechaRegistro?: any = null;
  idUsuarioModificacion?: any = null;
  fechaModificacion?: any = null;
  idUsuarioElimina?: any = null;
  fechaElimina?: any = null;
  token?: any = null;
  nuAlerta?: any = null;
  idMesaPartes: number = 0;
  idPlanManejo: number = 0;
  codigo: string = '';
  conforme: string = '';
  comentario: string = '';
  observacion: string = '';
  detalle: string = '';
  fechaTramite: number = 0;
  descripcion: string = '';
  listarMesaPartesDetalle: ListarMesaPartesDetallePmficDema[] = [];
  correo?: any = null;
  listarMesaPartesRequisitosTUPA: ListarMesaPartesDetallePmficDema[] = [];


  constructor(obj?:any){
    
      if(obj) {
        this.search = obj.search ? obj.search : null;
        this.pageNum = obj.pageNum ? obj.pageNum : null;
        this.pageSize = obj.pageSize ? obj.pageSize : null;
        this.totalRecord = obj.totalRecord ? obj.totalRecord : null;
        this.totalPage = obj.totalPage ? obj.totalPage : null;
        this.startIndex = obj.startIndex ? obj.startIndex : null;
        this.codigoPerfil = obj.codigoPerfil ? obj.codigoPerfil : null;
        this.estado = obj.estado ? obj.estado : '';
        this.idUsuarioRegistro = obj.idUsuarioRegistro
          ? obj.idUsuarioRegistro
          : null;
        this.fechaRegistro = obj.fechaRegistro ? obj.fechaRegistro : null;
        this.idUsuarioModificacion = obj.idUsuarioModificacion
          ? obj.idUsuarioModificacion
          : null;
        this.fechaModificacion = obj.fechaModificacion
          ? obj.fechaModificacion
          : null;
        this.idUsuarioElimina = obj.idUsuarioElimina ? obj.idUsuarioElimina : null;
        this.fechaElimina = obj.fechaElimina ? obj.fechaElimina : null;
        this.token = obj.token ? obj.token : null;
        this.nuAlerta = obj.nuAlerta ? obj.nuAlerta : null;
        this.idMesaPartes = obj.idMesaPartes ? obj.idMesaPartes : 0;
        this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
        this.codigo = obj.codigo ? obj.codigo : '';
        this.conforme = obj.conforme ? obj.conforme : '';
        this.comentario = obj.comentario ? obj.comentario : '';
        this.observacion = obj.observacion ? obj.observacion : '';
        this.detalle = obj.detalle ? obj.detalle : '';
        this.fechaTramite = obj.fechaTramite;
        this.descripcion = obj.descripcion ? obj.descripcion : 0;
        this.listarMesaPartesDetalle = obj.listarMesaPartesDetalle
          ? obj.listarMesaPartesDetalle
          : [];
        this.correo = obj.correo ? obj.correo : null;
        this.listarMesaPartesRequisitosTUPA = obj.listarMesaPartesRequisitosTUPA
          ? obj.listarMesaPartesRequisitosTUPA
          : [];
      }

  }
}

export class ListarMesaPartesDetallePmficDema {
  search?: any = null;
  pageNum?: any = null;
  pageSize?: any = null;
  totalRecord?: any = null;
  totalPage?: any = null;
  startIndex?: any = null;
  codigoPerfil?: any = null;
  estado: string = '';
  idUsuarioRegistro?: any = null;
  fechaRegistro?: any = null;
  idUsuarioModificacion?: any = null;
  fechaModificacion?: any = null;
  idUsuarioElimina?: any = null;
  fechaElimina?: any = null;
  token?: any = null;
  nuAlerta?: any = null;
  idMesaPartesDet: number = 0;
  codigo: string = '';
  conforme: string = '';
  idArchivo: number = 0;
  archivo: any = null;
  requisito: string = '';
  idMesaPartes: number = 0;
  observacion: string = '';
  fecha: any = '';
  fechaDocumento: any = '';
  detalle: string = '';
  comentario: string = '';
  descripcion: string = '';
  justificacion: string = '';
  tipoRequisito: string = '';


  constructor(obj?:any){
    if(obj) {
      this.search = obj.search;
      this.pageNum = obj.pageNum;
      this.pageSize = obj.pageSize ? obj.pageSize : null;
      this.totalRecord = obj.totalRecord ? obj.totalRecord : null;
      this.totalPage = obj.totalPage ? obj.totalPage : null;
      this.startIndex = obj.startIndex ? obj.startIndex : null;
      this.codigoPerfil = obj.codigoPerfil ? obj.codigoPerfil : null;
      this.estado = obj.estado ? obj.estado : '';
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : null;
      this.fechaRegistro = obj.fechaRegistro ? obj.fechaRegistro : null;
      this.idUsuarioModificacion = obj.idUsuarioModificacion
        ? obj.idUsuarioModificacion
        : null;
      this.fechaModificacion = obj.fechaModificacion
        ? obj.fechaModificacion
        : null;
      this.idUsuarioElimina = obj.idUsuarioElimina ? obj.idUsuarioElimina : null;
      this.fechaElimina = obj.fechaElimina ? obj.fechaElimina : null;
      this.token = obj.token ? obj.token : null;
      this.nuAlerta = obj.nuAlerta ? obj.nuAlerta : null;
      this.idMesaPartesDet = obj.idMesaPartesDet ? obj.idMesaPartesDet : 0;
      this.codigo = obj.codigo ? obj.codigo : '';
      this.conforme = obj.conforme ? obj.conforme : '';
      this.idArchivo = obj.idArchivo ? obj.idArchivo : '';
      this.archivo = obj.archivo ? obj.archivo : null;
      this.requisito = obj.requisito ? obj.requisito : '';
      this.idMesaPartes = obj.idMesaPartes ? obj.idMesaPartes : 0;
      this.observacion = obj.observacion ? obj.observacion : '';
      this.comentario = obj.comentario ? obj.comentario : '';
      this.fecha = obj.fecha ? obj.fecha : '';
      this.fechaDocumento = obj.fechaDocumento ? obj.fechaDocumento : '';
      this.detalle = obj.detalle ? obj.detalle : '';
      this.descripcion = obj.descripcion ? obj.descripcion : '';
      this.justificacion = obj.justificacion ? obj.justificacion : '';
      this.tipoRequisito = obj.tipoRequisito ? obj.tipoRequisito : '';
    }

  }

}
