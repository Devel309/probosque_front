import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";

export interface PotencialRecursoForestalModel extends AuditoriaModel {
    planManejo: PlanManejoModel;
    idPotProdForestal: number;
    codigoTipoPotProdForestal:String;
    especie:String;
    variable:String;
    anexo:String;
    disenio:String;
    diametroMinimoInventariadaCM:Number;
    tamanioParcela:Number;
    nroParcela:Number;
    distanciaParcela:Number;
    totalAreaInventariada:Number;
    rangoDiametroCM :Number;
    areaMuestreada :Number;
    metodoMuestreo:String;
    intensidadMuestreoPorcentaje:Number;
    errorMuestreoPorcentaje:Number;
}