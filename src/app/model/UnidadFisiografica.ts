import { AuditoriaModel } from "./auditoria";
import { InformacionBasicaModel } from "./InformacionBasica";
import { ResultArchivoModel } from "./ResultArchivo";
export interface UnidadFisiograficaModel extends AuditoriaModel  {
    idUnidadFisiografica:number;
    informacionBasica:InformacionBasicaModel;
    area:number;
    porcentaje:number;
    accion:Boolean;
    file:Boolean;
    modificar:Boolean;
    archivo:ResultArchivoModel;
    idLayer:String;
    inServer:Boolean;
}