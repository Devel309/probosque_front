export class SolPlantacionForestalGeometriaModel {
  
    idSolPlantacionForestalGeometria: number | null;
    idSolPlantacionForestal: number | null;
    idArchivo: number | null;
    tipoGeometria: string | null;
    codigoGeometria: string | null;
    codigoSeccion: string | null;
    codigoSubSeccion: string | null;
    nombreCapa:string  | null;
    colorCapa:string | null;
    geometry_wkt:string | null;
    properties:String | null;
    srid:number  | null;
    idUsuarioRegistro:number | null;
  
  
    constructor(obj?: Partial<SolPlantacionForestalGeometriaModel>) {
      this.idSolPlantacionForestalGeometria = null;
      this.idSolPlantacionForestal = null;
      this.idArchivo = null;
      this.tipoGeometria = null;
      this.codigoGeometria = null;
      this.codigoSeccion = null;
      this.codigoSubSeccion = null;
      this.nombreCapa = null;
      this.colorCapa = null;
      this.geometry_wkt = null;
      this.properties = null;
      this.srid = null;
      this.idUsuarioRegistro = null;
  
      if (obj) Object.assign(this, obj);
    }
  
  
  }