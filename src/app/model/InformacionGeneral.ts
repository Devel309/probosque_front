import { PlanManejoModel } from "./PlanManejo";
import { RegenteComunidadModule } from "./RegenteComunidad";
import { AuditoriaModel } from "./auditoria";
import { PersonaModel } from "./Persona";
export interface InformacionGeneralModel extends AuditoriaModel{
    idInfGeneral: number;
    planManejo:PlanManejoModel;
    persona:PersonaModel;
    certiHabilita: string;
    fechaPresentacion: Date;
    duracion: number;
    fechaInicio: Date;
    fechaFin: Date;
    potencial: number;
    nombreArchivo: string;
}
