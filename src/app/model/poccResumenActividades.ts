export class PoccResumenActRecModel {
  constructor(data?: any) {
    if (data) {
      this.idResumenActDet = data.idResumenActDet ? data.idResumenActDet : 0;
      this.tipoResumen = data.tipoResumen ? data.tipoResumen : 0;
      this.actividad = data.actividad ? data.actividad : '';
      this.indicador = data.indicador ? data.indicador : '';
      this.programado = data.programado ? data.programado : '';
      this.realizado = data.realizado ? data.realizado : '';
      this.resumen = data.resumen ? data.resumen : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.aspectoRecomendacion = data.aspectoRecomendacion
        ? data.aspectoRecomendacion
        : '';
      this.aspectoPositivo = data.aspectoPositivo ? data.aspectoPositivo : '';
      this.aspectoNegativo = data.aspectoNegativo ? data.aspectoNegativo : '';
      this.estadoVar = data.estadoVar ? data.estadoVar : 'A';
    }
  }
  idResumenActDet: number = 0;
  tipoResumen: number = 0;
  actividad: string = '';
  indicador: string = '';
  programado: string = '';
  realizado: string = '';
  resumen: string = '';
  idUsuarioRegistro: number = 0;
  aspectoRecomendacion: string = '';
  aspectoPositivo: string = '';
  aspectoNegativo: string = '';
  estadoVar: string = 'A';
}

export class PoccResumenActModel {
  constructor(data?: any) {
    if (data) {
      this.idResumenAct = data.idResumenAct ? data.idResumenAct : 0;
      this.codTipoResumen = data.codTipoResumen ? data.codTipoResumen : '';
      this.nroResolucion = data.nroResolucion ? data.nroResolucion : 0;
      this.nroPcs = data.nroPcs ? data.nroPcs : 0;
      this.areaHa = data.areaHa ? data.areaHa : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.listResumenEvaluacion = data.listResumenEvaluacion;
    }
  }
  idResumenAct: number = 0;
  codTipoResumen: string = '';
  nroResolucion: number = 0;
  nroPcs: number = 0;
  areaHa: number = 0;
  idPlanManejo: number = 0;
  idUsuarioRegistro: number = 0;
  listResumenEvaluacion: PoccResumenActRecModel[] = [];
}
