import { RegenteForestalModel } from "@models";

export class PoccInformacionGeneral {
  aprovechamientoNoMaderable = null;
  apellidoMaternoElaboraDema = null;
  apellidoPaternoElaboraDema = null;
  codigoProceso = "POCC";
  cuenca = null;
  dniElaboraDema = "";
  fechaFinDema: Date | any = "";
  fechaInicioDema: Date | any = "";
  fechaElaboracionPmfi: Date | any = "";
  idInformacionGeneralDema?: number;
  idPersonaComunidad = null;
  idPlanManejo = 0;
  idUsuarioRegistro = 0;
  lstUmf: infoGenralDetalle[] = [];
  nombreElaboraDema = "";
  nroAnexosComunidad: number = 0;
  nroArbolesMaderables = null;
  nroArbolesMaderablesSemilleros = null;
  nroArbolesNoMaderables = null;
  nroArbolesNoMaderablesSemilleros = null;
  nroPersonasInvolucradas = null;
  nroResolucionComunidad = "";
  nroRucComunidad = "";
  nroTituloPropiedadComunidad = null;
  nroTotalFamiliasComunidad = null;
  subCuenca = null;
  superficieHaMaderables = "";
  superficieHaNoMaderables = "";
  totalCostoEstimado = "";
  totalIngresosEstimado = "";
  totalUtilidadesEstimado = "";
  volumenM3rMaderables = "";
  volumenM3rNoMaderables = "";
  idDistritoRepresentante = null;
  representanteLegal = "";
  federacionComunidad = null;
  areaTotal: number | null = null;
  detalle = "";
  observacion = "";
  descripcion = null;
  conRegente = true;
  regente!: RegenteForestal;
  direccionLegalRepresentante = "";
  direccionLegalTitular = "";
  idContrato: any = null;
  idDistritoTitular = 0;
  vigencia = 0;
  celularTitular = "";
  correoTitular = "";
  departamentoRepresentante? = "";
  provinciaRepresentante? = "";
  distritoRepresentante? = "";
  codTipoPersona = "";
  tipoPersona = "";
  codTipoDocumentoElaborador = "";
  tipoDocumentoElaborador = "";
  codTipoDocumentoRepresentante = "";
  tipoDocumentoRepresentante = "";
  documentoRepresentante = "";
  nombreRepresentante = "";
	apellidoPaternoRepresentante = "";
	apellidoMaternoRepresentante = "";
  nombreCompletoTitular = "";
  nombreCompletoRepresentante = "";

  constructor(data?: any) {
    if (data) {
      this.aprovechamientoNoMaderable = data.aprovechamientoNoMaderable
        ? data.aprovechamientoNoMaderable
        : null;
      this.apellidoMaternoElaboraDema = data.apellidoMaternoElaboraDema
        ? data.apellidoMaternoElaboraDema
        : null;
      this.apellidoPaternoElaboraDema = data.apellidoPaternoElaboraDema
        ? data.apellidoPaternoElaboraDema
        : null;
      this.codigoProceso = "POCC";
      this.cuenca = data.cuenca ? data.cuenca : null;
      this.detalle = data.detalle ? data.detalle : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.fechaElaboracionPmfi = data.fechaElaboracionPmfi
        ? data.fechaElaboracionPmfi
        : "";
      this.dniElaboraDema = data.dniElaboraDema ? data.dniElaboraDema : "";
      this.idInformacionGeneralDema = data.idInformacionGeneralDema ? data.idInformacionGeneralDema : null;
      this.idPersonaComunidad = data.idPersonaComunidad
        ? data.idPersonaComunidad
        : null;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.lstUmf = data.lstUmf ? data.lstUmf : [];
      this.descripcion = data.descripcion ? data.descripcion : null;
      this.areaTotal = data.areaTotal ? data.areaTotal : null;
      this.federacionComunidad = data.federacionComunidad
        ? data.federacionComunidad
        : null;
      this.nombreElaboraDema = data.nombreElaboraDema
        ? data.nombreElaboraDema
        : "";
      this.nroAnexosComunidad = data.nroAnexosComunidad
        ? data.nroAnexosComunidad
        : 0;
      this.nroArbolesMaderables = data.nroArbolesMaderables
        ? data.nroArbolesMaderables
        : null;
      this.nroArbolesMaderablesSemilleros = data.nroArbolesMaderablesSemilleros
        ? data.nroArbolesMaderablesSemilleros
        : null;
      this.nroArbolesNoMaderables = data.nroArbolesNoMaderables
        ? data.nroArbolesNoMaderables
        : null;
      this.nroArbolesNoMaderablesSemilleros = data.nroArbolesNoMaderablesSemilleros
        ? data.nroArbolesNoMaderablesSemilleros
        : null;
      this.nroPersonasInvolucradas = data.nroPersonasInvolucradas
        ? data.nroPersonasInvolucradas
        : null;
      this.nroResolucionComunidad = data.nroResolucionComunidad
        ? data.nroResolucionComunidad
        : "";
      this.nroRucComunidad = data.nroRucComunidad ? data.nroRucComunidad : "";
      this.nroTituloPropiedadComunidad = data.nroTituloPropiedadComunidad
        ? data.nroTituloPropiedadComunidad
        : null;
      this.nroTotalFamiliasComunidad = data.nroTotalFamiliasComunidad
        ? data.nroTotalFamiliasComunidad
        : null;
      this.subCuenca = data.subCuenca ? data.subCuenca : null;
      this.superficieHaMaderables = data.superficieHaMaderables
        ? data.superficieHaMaderables
        : "";
      this.superficieHaNoMaderables = data.superficieHaNoMaderables
        ? data.superficieHaNoMaderables
        : "";
      this.totalCostoEstimado = data.totalCostoEstimado
        ? data.totalCostoEstimado
        : "";
      this.totalIngresosEstimado = data.totalIngresosEstimado
        ? data.totalIngresosEstimado
        : "";
      this.totalUtilidadesEstimado = data.totalUtilidadesEstimado
        ? data.totalUtilidadesEstimado
        : "";
      this.volumenM3rMaderables = data.volumenM3rMaderables
        ? data.volumenM3rMaderables
        : "";
      this.volumenM3rNoMaderables = data.volumenM3rNoMaderables
        ? data.volumenM3rNoMaderables
        : "";
      this.conRegente = data.conRegente ? data.conRegente : true;
      this.regente = new RegenteForestal(data.regente);
      this.direccionLegalRepresentante = data.direccionLegalRepresentante
        ? data.direccionLegalRepresentante
        : "";
      this.direccionLegalTitular = data.direccionLegalTitular
        ? data.direccionLegalTitular
        : "";
      this.idContrato = data.idContrato ? data.idContrato : null;
      this.idDistritoRepresentante = data.idDistritoRepresentante
        ? data.idDistritoRepresentante
        : null;
      this.idDistritoTitular = data.idDistritoTitular
        ? data.idDistritoTitular
        : 0;
      this.vigencia = data.vigencia ? data.vigencia : 0;
      this.representanteLegal = data.representanteLegal
        ? data.representanteLegal
        : "";
      this.celularTitular = data.celularTitular ? data.celularTitular : "";
      this.correoTitular = data.correoTitular ? data.correoTitular : "";
      this.distritoRepresentante = data.distritoRepresentante
        ? data.distritoRepresentante
        : "";
        this.codTipoPersona = data.codTipoPersona ? data.codTipoPersona : "";
        this.tipoPersona = data.tipoPersona ? data.tipoPersona : "";
        this.codTipoDocumentoElaborador = data.codTipoDocumentoElaborador ? data.codTipoDocumentoElaborador : "";
        this.tipoDocumentoElaborador = data.tipoDocumentoElaborador ? data.tipoDocumentoElaborador : "";
        this.codTipoDocumentoRepresentante = data.codTipoDocumentoRepresentante ? data.codTipoDocumentoRepresentante : "";
        this.tipoDocumentoRepresentante = data.tipoDocumentoRepresentante ? data.tipoDocumentoRepresentante : "";
        this.documentoRepresentante = data.documentoRepresentante ? data.documentoRepresentante : "";
        this.nombreRepresentante = data.nombreRepresentante ? data.nombreRepresentante : "";
        this.apellidoPaternoRepresentante = data.apellidoPaternoRepresentante ? data.apellidoPaternoRepresentante : "";
        this.apellidoMaternoRepresentante = data.apellidoMaternoRepresentante ? data.apellidoMaternoRepresentante : "";
        this.nombreCompletoTitular = data.nombreCompletoTitular ? data.nombreCompletoTitular : "";
        this.nombreCompletoRepresentante = data.nombreCompletoRepresentante ? data.nombreCompletoRepresentante : "";
    }
  }
}

export class infoGenralDetalle {
  codigo: string = "";
  descripcion: string = "";
  detalle: string = "";
  codigoInformacionDet: string = "";
  tipoInformacionDet: string = "";
  vigenciaDet: number = 0;
  vigenciaFinalDet: number = 0;
  area: any;

  constructor(data?: infoGenralDetalle) {
    if (data) {
      this.codigo = data.codigo ? data.codigo : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.codigoInformacionDet = data.codigoInformacionDet
        ? data.codigoInformacionDet
        : "";
      this.tipoInformacionDet = data.tipoInformacionDet
        ? data.tipoInformacionDet
        : "";
      this.vigenciaDet = data.vigenciaDet ? data.vigenciaDet : 0;
      this.vigenciaFinalDet = data.vigenciaFinalDet ? data.vigenciaFinalDet : 0;
      this.area = data.area ? data.area : 0;
    }
  }
}

export class RegenteForestal {
  adjuntoCertificado: string;
  apellidos: string;
  certificadoHabilitacion: string | null;
  codigoProceso: string;
  codigoTipoRegente: string | null;
  contratoSuscrito: string | null;
  domicilioLegal: string | null;
  estadoRegente: string;
  idPlanManejo: number;
  idPlanManejoRegente: number;
  idUsuarioRegistro: number;
  nombres: string;
  numeroDocumento: string;
  numeroInscripcion: string;
  numeroLicencia: string;
  periodo: string;
  idRegente!: number | null;

  constructor(obj?: Partial<RegenteForestal>) {
    this.idRegente = 0;
    this.codigoTipoRegente = "";
    this.numeroLicencia = "";
    this.domicilioLegal = "";
    this.contratoSuscrito = "";
    this.certificadoHabilitacion = "";
    this.numeroInscripcion = "";
    this.nombres = "";
    this.apellidos = "";
    this.numeroDocumento = "";
    this.estadoRegente = "";
    this.adjuntoCertificado = "";
    this.idPlanManejoRegente = 0;
    this.idPlanManejo = 0;
    this.idUsuarioRegistro = 0;
    this.periodo = "";
    this.codigoProceso = "POCC";

    if (obj) Object.assign(this, obj);
  }
}
