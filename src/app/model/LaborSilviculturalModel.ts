import { LaborSilviculturalDto } from "./LaborSilviculturalDto";

export class LaborSilviculturalModel {
    listaObligatorias: LaborSilviculturalDto[];
    listaOpcionales: LaborSilviculturalDto[];
    
    constructor() {
        this.listaObligatorias = [];
        this.listaOpcionales = [];
    }
}
