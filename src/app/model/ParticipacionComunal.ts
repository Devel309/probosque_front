import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
export interface ParticipacionComunalModel extends AuditoriaModel {
  idPartComunal: number;
  actividad: string;
  responsable: string;
  seguimientoActividad: string;
  planManejo: PlanManejoModel;
}

export class ParticipacionComunalDto implements ParticipacionComunalModel {
  idPartComunal: number;
  actividad: string;
  responsable: string;
  seguimientoActividad: string;
  planManejo: PlanManejoModel;
  codTipoPartComunal: string;
  lugar: string;
  fecha: string;

  estado: string;
  idUsuarioRegistro: number;
  fechaRegistro: Date;
  idUsuarioModificacion: number;
  fechaModificacion: Date;
  idUsuarioElimina: number;
  fechaElimina: Date;

  constructor(obj?: Partial<ParticipacionComunalDto>) {
    this.idPartComunal = 0;
    this.actividad = "";
    this.responsable = "";
    this.seguimientoActividad = "";
    this.planManejo = { idPlanManejo: 0 } as PlanManejoModel;
    this.codTipoPartComunal = "";
    this.lugar = "";
    this.fecha = "";

    this.estado = "";
    this.idUsuarioRegistro = 0;
    this.fechaRegistro = new Date();
    this.idUsuarioModificacion = 0;
    this.fechaModificacion = new Date();
    this.idUsuarioElimina = 0;
    this.fechaElimina = new Date();

    if (obj) Object.assign(this, obj);
  }
}

export class ParticipacionComunalDetalleModel {
  constructor(data?: any) {
    if (data) {
      this.actividad = data.actividad ? data.actividad : "";
      this.codPartComunalDet = data.codPartComunalDet
        ? data.codPartComunalDet
        : "";
      this.codigoPartComunal = data.codigoPartComunal
        ? data.codigoPartComunal
        : "";
      this.descripcion = data.descripcion ? data.descripcion : "";
      this.detalle = data.detalle ? data.detalle : "";
      this.fecha = data.fecha ? data.fecha : "";
      this.lugar = data.lugar ? data.lugar : "";
      this.idPartComunalDet = data.idPartComunalDet ? data.idPartComunalDet : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.mecanismos = data.mecanismos ? data.mecanismos : "";
      this.metodologia = data.metodologia ? data.metodologia : "";
      this.observacion = data.observacion ? data.observacion : "";
      this.participacion = data.participacion ? data.participacion : "";
      return;
    }
  }

  actividad: string = "";
  codPartComunalDet: string = "";
  codigoPartComunal: string = "";
  descripcion: string = "";
  detalle: string = "";
  fecha: Date | string = "";
  idPartComunalDet: number = 0;
  idUsuarioRegistro: number = 0;
  lugar: string = "";
  mecanismos: string = "";
  metodologia: string = "";
  observacion: string = "";
  participacion: string = "";
}
