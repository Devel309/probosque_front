export class ListarPlanManejoEvaluacionRequest {

    idPlanManejoEval: number | null;
    idPlanManejo: number | null;
    titularRegente: string | null;
    fechaPresentacion: Date | null;
    estadoPlanManejoEvaluacion: string | null;
    search: string;
    startIndex: number;
    pageNum: number;
    pageSize: number;
    perfil: string | null;
    idUsuarioRegistro: number | null;

    constructor(obj?: Partial<ListarPlanManejoEvaluacionRequest>) {
        this.idPlanManejo = null;
        this.idPlanManejoEval = null;
        this.estadoPlanManejoEvaluacion = null;
        this.titularRegente = null;
        this.fechaPresentacion = null;
        this.search = "";
        this.startIndex = 0;
        this.pageNum = 1;
        this.pageSize = 10;
        this.perfil = null;
        this.idUsuarioRegistro = null;
        if (obj) Object.assign(this, obj);
    }


}
