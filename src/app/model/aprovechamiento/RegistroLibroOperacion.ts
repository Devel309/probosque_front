export class RegistroLibroOperacion {
    idLibroOperaciones: number | null = null;
    nroRegistro: string | null = null;
    idArchivoEvidencia: number = 0;
    estadoLO: string | null = null;
    permisoOtorgado: boolean | null = null;
    idUsuarioRegistro: number | null = null;
    idUsuarioModificacion: number | null = null;
    idArchivoNotificacion: number = 0;
    agregarFolioTomo: boolean | null = null;
    libroFisico:boolean | null = null;
    numeroFolio: number | null = null;
    numeroTomo: number | null = null;
    validarSolicitud: boolean | null = null;
  
    constructor() {}
  
    setData(obj: any) {
      this.idLibroOperaciones = obj.idLibroOperaciones || null;
      this.nroRegistro = obj.nroRegistro || null;
      this.idArchivoEvidencia = obj.idArchivoEvidencia || 0;
      this.idArchivoNotificacion = obj.idArchivoNotificacion || 0;
      this.estadoLO = obj.estadoLO || null;
      this.permisoOtorgado = obj.permisoOtorgado || null;
      this.numeroFolio = this.numeroFolio || 0;
      this.numeroTomo = this.numeroTomo || 0;
      this.libroFisico = this.libroFisico || null;
      this.agregarFolioTomo = this.agregarFolioTomo || null;

    }
  }