export abstract class CodigoEstadoLO {
  static readonly PENDIENTE: string = "ELIBOPPEND";    //Pendiente
  static readonly COMPLETADO: string = "ELIBOPCOMP";    //Completado
}
