export class ListarImpugnacionRequest {

    nroResolucion: number | null;
    fechaImpugnacion: Date | null;
    estadoSolicitud: string | null;
    tipoDocGestion?: string | null;
    nroDocGestion: number | null;
    pageNum: number | null;
    pageSize: number | null;
    perfil: string | null; //se manda solo en la pantalla de inicio
    idUsuarioRegistro: number | null;

    constructor(obj?: Partial<ListarImpugnacionRequest>) {
        this.nroResolucion = null;
        this.fechaImpugnacion = null;
        this.estadoSolicitud = null;
        this.tipoDocGestion = null;
        this.nroDocGestion = null;
        this.pageNum = 1;
        this.pageSize = 10;
        this.perfil = null;
        this.idUsuarioRegistro = null;
        if (obj) Object.assign(this, obj);
    }


}


export interface ImpugnacionRegistrar {
    idResolucion: number;
    asunto: string;
    descripcion: string;
    fechaImpugnacion: Date;
    idUsuarioRegistro: number;
    region: number;
    fileRecurso: File;
    fileSustento: File;

}

export interface ImpugnacionActualizar {
    idImpugnacion: number;
    tipoImpugnacion: string;
    esfundado: boolean;
    idUsuarioModificacion: number;
    fileEvaluacion: File;
}