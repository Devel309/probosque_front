
import { PlanManejoModel } from "./PlanManejo";

export class CapacitacionMaderableModel {

    idCapacitacion: number | null;
    idPlanManejo: number | null;
    adjunto: string | null;
    codTipoCapacitacion: string | null;
    idUsuarioRegistro: number | null;
    idUsuarioElimina: number | null;
    idUsuarioModificacion: number | null;
    listCapacitacionDetalle: CapacitacionDetalleMaderableModel[];
    planManejo?: PlanManejoModel;

    constructor(obj?: Partial<CapacitacionMaderableModel>) {
        this.codTipoCapacitacion = null;
        this.idCapacitacion = 0;
        this.idUsuarioRegistro = 0;
        this.idUsuarioElimina = 0;
        this.idUsuarioModificacion = 0;
        this.idPlanManejo = 0;
        this.adjunto = null;
        this.listCapacitacionDetalle = [];

        if (obj) Object.assign(this, obj);
    }
}

export class CapacitacionDetalleMaderableModel {
    actividad?: string | null;
    descripcion?: string | null;
    idCapacitacionDet?: number | null;
    lugar?: string | null;
    modalidad?: string | null;
    periodo?: string | null;
    personal?: string | null;
    responsable?: string | null;
    idUsuarioRegistro?: number | null;
    idUsuarioElimina: number | null;
    idUsuarioModificacion: number | null;

    enviar?: boolean | null;

    constructor(obj?: Partial<CapacitacionDetalleMaderableModel>) {
        this.actividad = null;
        this.descripcion = "";
        this.idCapacitacionDet = 0;
        this.idUsuarioRegistro = 0;
        this.idUsuarioElimina = 0;
        this.idUsuarioModificacion = 0;
        this.lugar = null;
        this.modalidad = null
        this.periodo = null;
        this.personal = null;
        this.responsable = null;
        this.enviar = null;

        if (obj) Object.assign(this, obj);
    }
}