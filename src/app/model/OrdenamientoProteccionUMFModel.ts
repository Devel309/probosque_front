export class OrdenamientoProteccionCabeceraModel {
  constructor(obj?: any) {
    if (obj) {
      this.idOrdenamientoProteccion = obj.idOrdenamientoProteccion
        ? obj.idOrdenamientoProteccion
        : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.codTipoOrdenamiento = obj.codTipoOrdenamiento
        ? obj.codTipoOrdenamiento
        : "";
      this.subCodTipoOrdenamiento = obj.subCodTipoOrdenamiento
        ? obj.subCodTipoOrdenamiento
        : "";
      this.anexo = obj.anexo ? obj.anexo : "";
      this.listOrdenamientoProteccionDet = obj.listOrdenamientoProteccionDet
        ? obj.listOrdenamientoProteccionDet
        : [];
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
    }
  }
  idOrdenamientoProteccion: number = 0;
  codTipoOrdenamiento: string = "";
  subCodTipoOrdenamiento: string = "";
  idPlanManejo: number = 0;
  anexo: string = "";
  idUsuarioRegistro: number = 0;
  listOrdenamientoProteccionDet: OrdenamientoProteccionDetalleModel[] = [];
}

export class OrdenamientoProteccionDetalleModel {
  constructor(obj?: any) {
    if (obj) {
      this.idOrdenamientoProteccionDet = obj.idOrdenamientoProteccionDet
        ? obj.idOrdenamientoProteccionDet
        : 0;
      this.codigoTipoOrdenamientoDet = obj.codigoTipoOrdenamientoDet
        ? obj.codigoTipoOrdenamientoDet
        : "";
      this.categoria = obj.descripcionSistema ? obj.descripcionSistema : "";
      this.areaHA = obj.areaHA ? obj.areaHA : 0;
      this.areaHAPorcentaje = obj.areaHAPorcentaje ? obj.areaHAPorcentaje : 0;
      this.descripcion = obj.descripcion ? obj.descripcion : "";
      this.observacion = obj.observacion ? obj.observacion : "";
      this.observacionDetalle = obj.observacionDetalle
        ? obj.observacionDetalle
        : "";
      this.actividad = obj.actividades ? obj.actividades : "";
      this.actividadesRealizar = obj.actividadesRealizar
        ? obj.actividadesRealizar
        : "";
      this.accion = obj.accion ? obj.accion : 0;
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
      this.verticeBloque = obj.verticeBloque ? obj.verticeBloque : "";
      this.zonaUTM = obj.zonaUTM ? obj.zonaUTM : "";
      this.coordenadaEste = obj.coordenadaEste ? obj.coordenadaEste : 0;
      this.coordenadaNorte = obj.coordenadaNorte ? obj.coordenadaNorte : 0;
    }
  }
  idOrdenamientoProteccionDet: number = 0;
  codigoTipoOrdenamientoDet: string = "";
  categoria: string = "";
  areaHA: number = 0;
  areaHAPorcentaje: number = 0;
  descripcion: string = "";
  observacion: string = "";
  observacionDetalle: string = "";
  actividad: string = "";
  actividadesRealizar: string = "";
  accion: number = 0;
  idUsuarioRegistro: number = 0;
  verticeBloque: string = "";
  zonaUTM: string = "";
  coordenadaEste: number = 0;
  coordenadaNorte: number = 0;
}

export class SistemaOrdenamientoodel {
  constructor(obj?: any) {
    if (obj) {
      this.idSistemaManejoForestal = obj.idSistemaManejoForestal
        ? obj.idSistemaManejoForestal
        : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.codigoProceso = obj.codigoProceso ? obj.codigoProceso : "";
      this.detalle = obj.detalle ? obj.detalle : [];
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
    }
  }
  idSistemaManejoForestal: number = 0;
  codigoProceso: string = "";
  idPlanManejo: number = 0;
  detalle: SistemaOrdenamientoDetalle[] = [];
  idUsuarioRegistro: number = 0;
}

export class SistemaOrdenamientoDetalle {
  constructor(obj?: any) {
    if (obj) {
      this.idSistemaManejoForestalDetalle = obj.idSistemaManejoForestalDetalle
        ? obj.idSistemaManejoForestalDetalle
        : 0;
      this.codigoTipoDetalle = obj.codigoTipoDetalle
        ? obj.codigoTipoDetalle
        : "";
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
      this.actividades = obj.actividades ? obj.actividades : "";
      this.descripcionSistema = obj.descripcionSistema
        ? obj.descripcionSistema
        : "";
      this.codigoTipoDetalle = obj.codigoTipoDetalle
        ? obj.codigoTipoDetalle
        : "";
      this.codigoProceso = obj.codigoProceso ? obj.codigoProceso : "";
      this.editable = obj.editable ? obj.editable : false;
    }
  }
  idSistemaManejoForestalDetalle: number = 0;
  codigoTipoDetalle: string = "";
  actividades: string = "";
  descripcionSistema: string = "";
  codigoProceso: string = "";
  idUsuarioRegistro: number = 0;
  editable: boolean = false;
}
