export class InformacionBasicaModelPMFIC {
  idInfBasica: number = 0;
  codInfBasica: string = '';
  codSubInfBasica: string = '';
  codNombreInfBasica: string = '';
  idDepartamento?: any = null;
  idProvincia?: any = null;
  idDistrito?: any = null;
  departamento?: any = null;
  provincia?: any = null;
  distrito?: any = null;
  cuenca?: any = null;
  anexo: string = '';
  idPlanManejo: number = 0;
  numeroPc?: any = null;
  areaTotal?: any = null;
  nroHojaCatastral?: any = null;
  nombreHojaCatastral?: any = null;
  idHidrografia?: any = null;
  frenteCorta?: any = null;
  comunidad: string = '';
  idUsuarioRegistro: number = 0;
  listInformacionBasicaDet: InformacionBasicaDetalleModelPMFIC[] = [];

  constructor(data?: any) {
    if (data) {
      this.idInfBasica = data.idInfBasica;
      this.codInfBasica = data.codInfBasica;
      this.codSubInfBasica = data.codSubInfBasica;
      this.codNombreInfBasica = data.codNombreInfBasica;
      this.idDepartamento = data.idDepartamento;
      this.idProvincia = data.idProvincia;
      this.idDistrito = data.idDistrito;
      this.departamento = data.departamento;
      this.provincia = data.provincia;
      this.distrito = data.distrito;
      this.cuenca = data.cuenca;
      this.anexo = data.anexo;
      this.idPlanManejo = data.idPlanManejo;
      this.numeroPc = data.numeroPc;
      this.areaTotal = data.areaTotal;
      this.nroHojaCatastral = data.nroHojaCatastral;
      this.nombreHojaCatastral = data.nombreHojaCatastral;
      this.idHidrografia = data.idHidrografia;
      this.frenteCorta = data.frenteCorta;
      this.comunidad = data.comunidad;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.listInformacionBasicaDet = data.listInformacionBasicaDet? data.listInformacionBasicaDet:[];
    }
  }
}

export class InformacionBasicaDetalleModelPMFIC {
  search?: any = null;
  pageNum?: any = null;
  pageSize?: any = null;
  totalRecord?: any = null;
  totalPage?: any = null;
  startIndex?: any = null;
  estado?: any = null;
  idUsuarioRegistro: number = 0;
  fechaRegistro?: any = null;
  idUsuarioModificacion?: any = null;
  fechaModificacion?: any = null;
  idUsuarioElimina?: any = null;
  fechaElimina?: any = null;
  idInfBasica: number = 0;
  codInfBasica: string = '';
  codNombreInfBasica: string = '';
  departamento?: any = null;
  provincia?: any = null;
  distrito?: any = null;
  comunidad?: any = null;
  cuenca?: any = null;
  idPlanManejo: number = 0;
  idInfBasicaDet: number = 0;
  codInfBasicaDet: string = '';
  codSubInfBasicaDet: string = '';
  puntoVertice?: any = null;
  nombre?: any = null;
  acceso?: any = null;
  numeroFamilia: number = 0;
  actividad?: any = null;
  subsistencia?: any = null;
  perenne?: any = null;
  ganaderia?: any = null;
  caza?: any = null;
  pesca?: any = null;
  madera?: any = null;
  otroProducto?: any = null;
  ampliarAnexo?: any = null;
  justificacion?: any = null;
  especieExtraida?: any = null;
  marcar?: any = null;
  valor?: any = null;
  referencia?: any = null;
  distanciaKm?: any = null;
  tiempo?: any = null;
  medioTransporte?: any = null;
  epoca?: any = null;
  idRios?: any = null;
  descripcion: string = '';
  areaHa?: any = null;
  areaHaPorcentaje?: any = null;
  zonaVida: string = '';
  idFauna: number = 0;
  idFlora: number = 0;
  nombreRio?: any = null;
  nombreLaguna?: any = null;
  nombreQuebrada?: any = null;
  anexo: string = '';
  numeroPc?: any = null;
  areaTotal?: any = null;
  nroHojaCatastral?: any = null;
  nombreHojaCatastral?: any = null;
  idTipoBosque?: any = null;
  nombreComun?: any = null;
  nombreCientifico?: any = null;
  familia?: any = null;
  observaciones: string = '';
  coordenadaUtm?: any = null;
  coordenadaEsteIni: number = 0;
  coordenadaNorteIni: number = 0;
  coordenadaEsteFin: number = 0;
  coordenadaNorteFin: number = 0;
  conflicto?: any = null;
  solucion?: any = null;
  idZona : number = 0;
  codigoZona: string = '';
  idLayer: string = '';
  inServer:boolean = false;
  idArchivo : number = 0;
  listInformacionBasicaDetSub: ListInformacionBasicaDetSubPMFIC[] = [];

  constructor(data?: any) {
    if (data) {
      // Object.assign(this, data);

      this.search = data.search;
      this.pageNum = data.pageNum;
      this.pageSize = data.pageSize;
      this.totalRecord = data.totalRecord;
      this.totalPage = data.totalPage;
      this.startIndex = data.startIndex;
      this.estado = data.estado;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.fechaRegistro = data.fechaRegistro;
      this.idUsuarioModificacion = data.idUsuarioModificacion;
      this.fechaModificacion = data.fechaModificacion;
      this.idUsuarioElimina = data.idUsuarioElimina;
      this.fechaElimina = data.fechaElimina;
      this.idInfBasica = data.idInfBasica;
      this.codInfBasica = data.codInfBasica;
      this.codNombreInfBasica = data.codNombreInfBasica;
      this.departamento = data.departamento;
      this.provincia = data.provincia;
      this.distrito = data.distrito;
      this.comunidad = data.comunidad;
      this.cuenca = data.cuenca;
      this.idPlanManejo = data.idPlanManejo;
      this.idInfBasicaDet = data.idInfBasicaDet;
      this.codInfBasicaDet = data.codInfBasicaDet;
      this.codSubInfBasicaDet = data.codSubInfBasicaDet;
      this.puntoVertice = data.puntoVertice;
      this.nombre = data.nombre;
      this.acceso = data.acceso;
      this.numeroFamilia = data.numeroFamilia;
      this.actividad = data.actividad;
      this.subsistencia = data.subsistencia;
      this.perenne = data.perenne;
      this.ganaderia = data.ganaderia;
      this.caza = data.caza;
      this.pesca = data.pesca;
      this.madera = data.madera;
      this.otroProducto = data.otroProducto;
      this.ampliarAnexo = data.ampliarAnexo;
      this.justificacion = data.justificacion;
      this.especieExtraida = data.especieExtraida;
      this.marcar = data.marcar;
      this.valor = data.valor;
      this.referencia = data.referencia;
      this.distanciaKm = data.distanciaKm;
      this.tiempo = data.tiempo;
      this.medioTransporte = data.medioTransporte;
      this.epoca = data.epoca;
      this.idRios = data.idRios;
      this.descripcion = data.descripcion;
      this.areaHa = data.areaHa;
      this.areaHaPorcentaje = data.areaHaPorcentaje;
      this.zonaVida = data.zonaVida;
      this.idFauna = data.idFauna;
      this.idFlora = data.idFlora;
      this.nombreRio = data.nombreRio;
      this.nombreLaguna = data.nombreLaguna;
      this.nombreQuebrada = data.nombreQuebrada;
      this.anexo = data.anexo;
      this.numeroPc = data.numeroPc;
      this.areaTotal = data.areaTotal;
      this.nroHojaCatastral = data.nroHojaCatastral;
      this.nombreHojaCatastral = data.nombreHojaCatastral;
      this.idTipoBosque = data.idTipoBosque;
      this.nombreComun = data.nombreComun;
      this.nombreCientifico = data.nombreCientifico;
      this.familia = data.familia;
      this.observaciones = data.observaciones;
      this.coordenadaUtm = data.coordenadaUtm;
      this.coordenadaEsteIni = data.coordenadaEsteIni;
      this.coordenadaNorteIni = data.coordenadaNorteIni;
      this.coordenadaEsteFin = data.coordenadaEsteFin;
      this.coordenadaNorteFin = data.coordenadaNorteFin;
      this.conflicto = data.conflicto;
      this.solucion = data.solucion;
      this.idZona = data.idZona;
      this.idLayer = data.idLayer;
      this.codigoZona = data.codigoZona;
      this.inServer = data.inServer;
      this.idArchivo = data.idArchivo;

      this.listInformacionBasicaDetSub = data.listInformacionBasicaDetSub? data.listInformacionBasicaDetSub:[];
    }
  }
}

export class ListInformacionBasicaDetSubPMFIC {
  idInfBasicaDetSub: number = 0;
  idInfBasicaDet: number = 0;
  codInfBasicaDet: string = '';
  codSubInfBasicaDet: string = '';
  descripcion: string = '';
  detalle: string = '';
  observaciones: string = '';
  codTipoInfBasicaDet: string = '';
  codSubTipoInfBasicaDet: string = '';

  constructor(data?: any) {
    if (data) {
      this.idInfBasicaDetSub = data.idInfBasicaDetSub;
      this.idInfBasicaDet = data.idInfBasicaDet;
      this.codInfBasicaDet = data.codInfBasicaDet;
      this.codSubInfBasicaDet = data.codSubInfBasicaDet;
      this.descripcion = data.descripcion;
      this.detalle = data.detalle;
      this.observaciones = data.observaciones;
      this.codTipoInfBasicaDet = data.codTipoInfBasicaDet;
      this.codSubTipoInfBasicaDet = data.codSubTipoInfBasicaDet;
    }
  }
}
