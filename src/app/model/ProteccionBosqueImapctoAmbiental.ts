import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";

export interface ProteccionBosqueImpactoAmbientalModel extends  AuditoriaModel{
  idProBosqueImpacAmb:number;
  planManejo:PlanManejoModel;
  factorAmbiental:String;
  impacto:String;
  censo:Boolean;
  demarcacionLineal:Boolean;
  construccionCampamento:Boolean;
  construccionCamino:Boolean;
  tala:Boolean;
  arrastre:Boolean;
  otra:Boolean;
  descripccionOtra:String;
  actividades:string[];
}

