import { ComunidadModel } from "./Comunidad";
import { TipoPersonaModel } from "./TipoPersona";
import { AuditoriaModel } from "./auditoria";
export interface PersonaModel extends AuditoriaModel{

    idPersona:Number;
    nombres:String;
    apellidoPaterno:String;
    apellidoMaterno:String;
    comunidad: ComunidadModel;
    tipoPersona:TipoPersonaModel;
    correo:String;
    telefono:String;
    ruc:String;
    codInrena:String;
    desTipoPersona:String;
    rasonSocial:String;
    representaLegal:String;

}