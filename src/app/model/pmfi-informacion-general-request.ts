import { PGMFArchivoDto } from "./PGMFArchivoDto";

export class PmfiInformacionGeneralRequest {
  idInformacionGeneral: number;
  idPlanManejo: number;
  nombreTitular: string;
  nombreRepresentanteLegal: string;
  dni: string;
  domicilioLegal: string;
  nroContratoConcesion: string;
  distrito: string;
  departamento: string;
  provincia: string;
  fechaElavoracionPMFI!: Date;
  duracion: number;
  fechaInicio!: Date;
  fechaFin!: Date;
  idRegenteForestal: number | null;
  nombreRegenteForestal: string;
  domicilioLegalRegente: string;
  nroTelefonoCelular: number;
  correoElectronico: string;
  domicilioLegalTitular: string;
  nombreRegentePMFI: string;
  nroLicenciaRegente: number;
  idUA: string;
  archivos: PGMFArchivoDto[];
  regente: PmfiRegente;
  dniRepresentanteLegal:string

  constructor(obj?: Partial<PmfiInformacionGeneralRequest>) {
    this.idInformacionGeneral = 0;
    this.idPlanManejo = 0;
    this.nombreTitular = "";
    this.nombreRepresentanteLegal = "";
    this.dni = "";
    this.domicilioLegal = "";
    this.nroContratoConcesion = "";
    this.distrito = "";
    this.departamento = "";
    this.provincia = "";
    this.duracion = 0;
    this.idRegenteForestal = 0;
    this.nombreRegenteForestal = "";
    this.domicilioLegalRegente = "";
    this.nroTelefonoCelular = 0;
    this.correoElectronico = "";
    this.domicilioLegalTitular = "";
    this.nombreRegentePMFI = "";
    this.nroLicenciaRegente = 0;
    this.dniRepresentanteLegal =""
    this.idUA = "";
    this.archivos = [];

    this.regente = new PmfiRegente();

    if (obj) Object.assign(this, obj);
  }
}

export class PmfiRegente {
  adjuntoCertificado: string;
  adjuntoContrato: string;
  apellidos: string;
  certificadoHabilitacion: string | null;
  codigoProceso: string;
  codigoTipoRegente: string | null;
  contratoSuscrito: string | null;
  domicilioLegal: string;
  estadoRegente: string;
  idPlanManejo: number;
  idPlanManejoRegente: number;
  idUsuarioRegistro: number;
  nombres: string;
  numeroDocumento: string;
  numeroInscripcion: string | null;
  numeroLicencia: string;
  periodo: string;
  nombreCompleto: string;

  constructor(obj?: Partial<PmfiRegente>) {
    this.adjuntoCertificado = "";
    this.adjuntoContrato = "";
    this.apellidos = "";
    this.certificadoHabilitacion = "";
    this.codigoProceso = "";
    this.codigoTipoRegente = "";
    this.contratoSuscrito = "";
    this.domicilioLegal = "";
    this.estadoRegente = "";
    this.idPlanManejo = 0;
    this.idPlanManejoRegente = 0;
    this.idUsuarioRegistro = 0;
    this.nombres = "";
    this.numeroDocumento = "";
    this.numeroInscripcion = "";
    this.numeroLicencia = "";
    this.periodo = "";
    this.nombreCompleto = "";

    if (obj) Object.assign(this, obj);
  }
}
