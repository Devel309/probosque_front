

export class ImpugnacionSan {

  idSolicitudSAN :number | null;
  codSolicitudSAN :string | null;
  subCodSolicitudSAN: string | null;
  tipoSolicitud: string | null;
  idPermisoForestal: number | null;
  nroGestion: number | null;
  tipoPostulacion: string | null;
  procesoPostulacion: string | null;
  motivoSolicitud: string | null;
  asunto: string | null;
  descripcion: string | null;
  detalle: string | null;
  observacion: string | null;
  idUsuarioRegistro: number | null;
  idArchivo: number | null;
  diasRestantes: number | null;
  diasRestantesPlan: number | null;
  diasAdicionales : number | null;

  constructor(obj?: Partial<ImpugnacionSan>) {
    this.idSolicitudSAN = null;
    this.codSolicitudSAN = null;
    this.subCodSolicitudSAN = null;
    this.tipoSolicitud = null;
    this.idPermisoForestal = null;
    this.nroGestion = null;
    this.tipoPostulacion = null;
    this.procesoPostulacion = null;
    this.motivoSolicitud = null;
    this.asunto = null;
    this.descripcion = null;
    this.detalle = null;
    this.observacion = null;
    this.idUsuarioRegistro = null;
    this.idArchivo = null;
    this.diasRestantes = null;
    this.diasRestantesPlan = null;
    this.diasAdicionales = null;

    if (obj) Object.assign(this, obj);
  }


}
