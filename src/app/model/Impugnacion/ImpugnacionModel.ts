
import { AuditoriaModel } from "@models"
import { ArchivoModel } from "../Comun/ArchivoModel";



export class ImpugnacionDto implements AuditoriaModel {
    constructor(data?: any) {
      if (data) {
        //   this.areabloque = data.areabloque;
          this.search= data.search;
          this.pageNum= data.pageNum;
          this.pageSize= data.pageSize;
          this.totalRecord=data.totalRecord;
          this.totalPage= data.totalPage;
          this.startIndex= data.startIndex;
          this.idTipoSolicitud= data.idTipoSolicitud;
          this.idImpugnacion= data.idImpugnacion;
          this.idSolicitud= data.idSolicitud;
          this.diaAmpliacion=data.diaAmpliacion;
          this.fechaPresentacion= data.fechaPresentacion;
          this.calificacionImpugnacion= data.calificacionImpugnacion;
          this.asunto= data.asunto;
          this.fechaInicio= data.fechaInicio;
          this.fechaFin= data.fechaFin;
          this.fundadoInfundado=data.fundadoInfundado;
          this.idTipoEvaluacion= data.idTipoEvaluacion;
          this.idEtapaImpugnacion= data.idEtapaImpugnacion;
          this.idEtapaImpugnacionTx=data.idEtapaImpugnacionTx;
          this.tipoSolicitud= data.tipoSolicitud;
          this.descripcion=data.descripcion;
  
  
          return;
      }
  }
    estado: String="";
    idUsuarioRegistro: number=0;
    fechaRegistro: Date=new Date();
    idUsuarioModificacion: number=0;
    fechaModificacion: Date=new Date();
    idUsuarioElimina: number=0;
    fechaElimina: Date=new Date();

search: string="";
pageNum: number=0;
pageSize: number=0;
totalRecord:number=0;
totalPage: number=0;
startIndex: number=0;
idTipoSolicitud: number=0;
idImpugnacion: number=0;
idSolicitud: number=0;
diaAmpliacion: number=0;
fechaPresentacion: Date=new Date();
calificacionImpugnacion: boolean=false;
asunto: string="";
fechaInicio: Date=new Date();
fechaFin: Date=new Date();
fundadoInfundado:Boolean=false;
idTipoEvaluacion: number=0;
idEtapaImpugnacion: number=0;
idEtapaImpugnacionTx: string="";
tipoSolicitud: string="";
descripcion:string="";
//impugnacionArchivoModel:ImpugnacionArchivoModel=[];
}



export class ImpugnacionModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      //   this.areabloque = data.areabloque;      
       
        this.asunto= data.asunto;
        this.calificacionImpugnacion= data.calificacionImpugnacion;
        this.descripcion=data.descripcion;
        this.fechaFin= data.fechaFin;
        this.fechaInicio= data.fechaInicio;
        this.fechaPresentacion= data.fechaPresentacion;
        this.fundadoInfundado= data.fundadoInfundado;
        this.idImpugnacion= data.idImpugnacion;
        this.idSolicitud= data.idSolicitud;
        this.diaAmpliacion = data.diaAmpliacion;
        this.idTipoEvaluacion= data.idTipoEvaluacion;
        this.idTipoSolicitud= data.idTipoSolicitud;
        this.region= data.region;

        return;
    }
}
  estado: String="";
  idUsuarioRegistro: number=0;
  fechaRegistro: Date=new Date();
  idUsuarioModificacion: number=0;
  fechaModificacion: Date=new Date();
  idUsuarioElimina: number=0;
  fechaElimina: Date=new Date();

  asunto: string="";
  calificacionImpugnacion: Boolean| null=true;
  idCalificacionImpugnacion: number=1;
  descripcion: string="";
  fechaFin: Date = new Date();
  fechaInicio: Date=new Date();
  fechaPresentacion: Date = new Date();
  fundadoInfundado: Boolean| null=true;
  idImpugnacion: number=0;
  idSolicitud: number=0;
  diaAmpliacion: number=0;
  idTipoEvaluacion: number=0;
  idTipoSolicitud: number=0;
  region: string="";
   

}


export class ImpugnacionEtapaModel implements AuditoriaModel {
  constructor(data?: any) {
    if (data) {
      //   this.areabloque = data.areabloque;
       
       
      this.etapaImpugnacion= data.etapaImpugnacion;  
      this.idEtapaImpugnacion=data.idEtapaImpugnacion;
      this.idImpugnacion= data.idImpugnacion;
      this.idImpugnacionEtapa= data.idImpugnacionEtapa;
      this.idUsuarioResponsable= data.idUsuarioResponsable;

        return;
    }
}
  estado: String="";
  idUsuarioRegistro: number=0;
  fechaRegistro: Date=new Date();
  idUsuarioModificacion: number=0;
  fechaModificacion: Date=new Date();
  idUsuarioElimina: number=0;
  fechaElimina: Date=new Date();

  
  etapaImpugnacion: string="";  
    idEtapaImpugnacion:number=0;
    idImpugnacion: number=0;
    idImpugnacionEtapa: number=0;  
    idUsuarioResponsable: number=0;


}



export class ImpugnacionArchivoModel implements AuditoriaModel {
  constructor(data?: any) {
    // super();
    if (data) {
      //   this.areabloque = data.areabloque;
      this.idImpugnacionArchivo = data.idImpugnacionArchivo;
      this.idImpugnacion = data.idImpugnacion;
      this.idArchivo = data.idArchivo;
      this.Parametro = data.Parametro;
      this.idTipoArchivo = data.idTipoArchivo;
      this.archivo = data.archivo;
        return;
    }
}
  estado: String="";
  idUsuarioRegistro: number=0;
  fechaRegistro: Date=new Date();
  idUsuarioModificacion: number=0;
  fechaModificacion: Date=new Date();
  idUsuarioElimina: number=0;
  fechaElimina: Date=new Date();

   idImpugnacionArchivo :number=0;
   idImpugnacion:number=0;
   idArchivo :number=0;
   Parametro :String="";
   idTipoArchivo :number=0;
   archivo :ArchivoModel=new ArchivoModel();

}
