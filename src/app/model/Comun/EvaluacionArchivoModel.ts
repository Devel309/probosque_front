export class EvaluacionArchivoModel {
    constructor(data?:any ) {
        if(data)
        {
          this.idEvaluacionDet = data.idEvaluacionDet;
          this.codigoEvaluacion = data.codigoEvaluacion;
          this.codigoEvaluacionDet = data.codigoEvaluacionDet;
          this.codigoEvaluacionDetSub = data.codigoEvaluacionDetSub;
          this.codigoEvaluacionDetPost = data.codigoEvaluacionDetPost;
          this.codigoTipo = data.codigoTipo;
          this.conforme = data.conforme;
          this.observacion = data.observacion;
          this.idArchivo = data.idArchivo;
          this.descArchivo = data.descArchivo;
          this.detalle = data.detalle;
          this.descripcion = data.descripcion;

          if (data.idArchivo!==null && data.idArchivo!==undefined &&
            data.descArchivo!==null && data.descArchivo!==undefined) {
            this.archivo = {idArchivo:data.idArchivo, nombre:data.descArchivo}
          }
          return;
        }
        /*else{
          this.idEvaluacionDet = 0
          this.codigoEvaluacionDet = null;
          this.codigoEvaluacionDetSub = null;
          this.codigoEvaluacionDetPost = null;
          this.codigoTipo = null;
          this.conforme = null;
          this.observacion = null;
          this.idArchivo = null;
          this.descArchivo = null;
          this.archivo={};
        }*/
    }
    idEvaluacionDet:number = 0;
    codigoEvaluacion:any = null;
    codigoEvaluacionDet:any = null;
    codigoEvaluacionDetSub:any = null;
    codigoEvaluacionDetPost:any = null;
    codigoTipo:any = null;
    conforme:any=null;
    observacion:any = null;
    idArchivo:any = null;
    descArchivo:any = null;
    archivo:any={};
    detalle:any= null;
    descripcion:any = null;
    fechaEvaluacionDetInicial :any = null;
    fechaEvaluacionDetFinal :any = null;

    limpiarObservacion():void{
      this.observacion = null;
    }


}
