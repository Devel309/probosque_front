import { Byte } from "@angular/compiler/src/util";
import { AuditoriaModel } from "@models";

export class ArchivoModel implements AuditoriaModel {
    constructor(data?: any) {
      if (data) {
        //   this.areabloque = data.areabloque;
        this.idArchivo= data.idArchivo;
        this.ruta= data.ruta;
        this.nombre= data.nombre;
        this.nombreGenerado= data.nombreGenerado;
        this.descripcionArchivo= data.descripcionArchivo;
        this.extension= data.extension;
        this.tipoDocumento= data.tipoDocumento;
        this.file= data.file;
        this.contenType= data.contenType;
  
          return;
      }
  }
    estado: String="";
    idUsuarioRegistro: number=0;
    fechaRegistro: Date=new Date();
    idUsuarioModificacion: number=0;
    fechaModificacion: Date=new Date();
    idUsuarioElimina: number=0;
    fechaElimina: Date=new Date();
  
      idArchivo: number=0;
      ruta: String="";
      nombre: String="";
      nombreGenerado: String="";
      descripcionArchivo: String="";
      extension: String="";
      tipoDocumento: String="";
      file:String="";
      contenType: String="";
  
  
  }
  