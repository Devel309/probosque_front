export interface IPermisoOpcion {
  isPerRegistrar: boolean;
  isPerEditar: boolean;
  isPerEliminar: boolean;
  isPerVisualizar: boolean;
  isPerOtros: boolean;
}
