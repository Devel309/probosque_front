export class LineamientoInnerModel {
  constructor(data?: any) {
    if (data) {
      this.idPlanManejoEvaluacion = data.idPlanManejoEvaluacion;
      this.idPlanManejoEvalDet = data.idPlanManejoEvalDet;
      this.codigoTipo = data.codigoTipo;
      this.conforme = data.conforme;
      this.observacion = data.observacion;
      if (
        data.idArchivo !== null &&
        data.idArchivo !== undefined &&
        data.descArchivo !== null &&
        data.descArchivo !== undefined
      ) {
        this.archivo = { idArchivo: data.idArchivo, nombre: data.descArchivo };
      }

      return;
    } else {
      this.idPlanManejoEvaluacion = undefined;
      this.idPlanManejoEvalDet = undefined;
      this.codigoTipo = undefined;
      this.conforme = undefined;
      this.observacion = undefined;
      this.archivo = {};
    }
  }
  idPlanManejoEvaluacion: any = undefined;
  idPlanManejoEvalDet: any = undefined;
  codigoTipo: any = undefined;
  conforme: any = false;
  observacion: any = undefined;
  descripcion: any = undefined;
  idArchivo: any = undefined;
  descArchivo: any = undefined;
  archivo: any = {};

  limpiarObservacion(): void {
    this.observacion = undefined;
  }
}
