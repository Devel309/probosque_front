export class PfcrInformacionGeneral {
  idInfGeneral: number;
  codTipoInfGeneral: string;
  idPermisoForestal: number;
  fechaPresentacion: Date;
  nombreElaborador: string;
  apellidoPaternoElaborador: string;
  apellidoMaternoElaborador: string;
  tipoDocumentoElaborador: string;
  documentoElaborador: string;
  rucComunidad: string;
  federacionComunidad: string;
  representanteLegal?: any;
  domicilioLegal: string;
  domicilioLegalRegente?: any;
  codTipoDocumento: string;
  codTipoPersona: string;
  codTipoActor: string;
  codTipoCncc?: any;
  esReprLegal: string;
  ubigeoTitular: any;
  ubigeo?: any;
  correo: string;
  correoEmpresa: string;
  descripcion?: any;
  detalle?: any;
  observacion?: any;
  idUsuarioRegistro: number;
  idSolicitudAcceso: number;

  constructor(obj?: Partial<PfcrInformacionGeneral>) {
    this.idInfGeneral = 0;
    this.codTipoInfGeneral = 'PFCR';
    this.idPermisoForestal = 0;
    this.fechaPresentacion = new Date();
    this.nombreElaborador = '';
    this.apellidoPaternoElaborador = '';
    this.apellidoMaternoElaborador = '';
    this.tipoDocumentoElaborador = '';
    this.documentoElaborador = '';
    this.rucComunidad = '';
    this.federacionComunidad = '';
    this.representanteLegal = null;
    this.domicilioLegal = '';
    this.domicilioLegalRegente = null;
    this.codTipoDocumento = '';
    this.codTipoPersona = '';
    this.codTipoActor = '';
    this.codTipoCncc = null;
    this.esReprLegal = '';
    this.ubigeoTitular = '';
    this.ubigeo = null;
    this.correo = '';
    this.correoEmpresa = '';
    this.descripcion = null;
    this.detalle = null;
    this.observacion = null;
    this.idUsuarioRegistro = 0;
    this.idSolicitudAcceso = 0;

    if (obj) Object.assign(this, obj);
  }
}
