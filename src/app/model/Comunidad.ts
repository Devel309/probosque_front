import { AuditoriaModel } from "./auditoria";
import { FederacionComunidadModel } from "./FederacionComunidad";
export interface ComunidadModel extends AuditoriaModel{ 
    idComunidad:number;
    ruc:String;
    idTipoComunidad:number;
    idFedComunidad:number;
    nombre:String;
    credencial:String;
    federacionComunidad:FederacionComunidadModel;
}