export class accordeon_3 {
  title: string = "";
  body: accordionDes[] = [];
  array: accordionDes[] = [];

  constructor(obj?: any) {
    if (obj) {
      this.title = obj.title ? obj.title : "";
      this.array = obj.subAccordeon1 ? obj.subAccordeon1 : [];
    }
  }
}

export class accordionDes {
  title: string = "";
  lastbody1: any;
  parrafo: any;
  array: any = [];
  body: any = [];

  constructor(obj?: any) {
    if (obj) {
      this.title = obj.title ? obj.title : "";
      this.body = obj.body ? obj.body : [];
      this.array = obj.array ? obj.array : [];
    }
  }
}

export class ConsideracionesGenerales {
  idParametro: number = 0;
  codigo: string = "";
  valorPrimario: string = "";
  valorSecundario: string = "";
  valorTerciario: string = "";
  codigoSniffs: string = "";
  codigoSniffsDescripcion: string = "";
  prefijo: string = "";
  idPlanManejo: number = 0;
  idEvaluacionDet: number = 0;
  listNivel3: [] = [];
  observacion: string = "";

  constructor(obj?: any) {
    if (obj) {
      this.codigo = obj.codigo ? obj.codigo : "";
      this.valorPrimario = obj.valorPrimario ? obj.valorPrimario : "";
      this.valorSecundario = obj.valorSecundario ? obj.valorSecundario : "";
      this.valorTerciario = obj.valorTerciario ? obj.valorTerciario : "";
      this.codigoSniffs = obj.codigoSniffs ? obj.codigoSniffs : "";
      this.codigoSniffsDescripcion = obj.codigoSniffsDescripcion
        ? obj.codigoSniffsDescripcion
        : "";
      this.prefijo = obj.prefijo ? obj.prefijo : "";
      this.observacion = obj.observacion ? obj.observacion : "";
      this.idParametro = obj.idParametro ? obj.idParametro : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.idEvaluacionDet = obj.idEvaluacionDet ? obj.idEvaluacionDet : 0;
      this.listNivel3 = obj.listNivel3 ? obj.listNivel3 : [];
    }
  }
}

export class EvaluacionObj {
  idEvaluacion: number = 0;
  idPlanManejo: number = 0;
  codigoEvaluacion: string = "";
  idUsuarioRegistro: number = 0;

  listarEvaluacionDetalle: [] = [];

  constructor(obj?: any) {
    if (obj) {
      this.idEvaluacion = obj.idEvaluacion ? obj.idEvaluacion : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.codigoEvaluacion = obj.codigoEvaluacion ? obj.codigoEvaluacion : "";
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
      this.listarEvaluacionDetalle = obj.listarEvaluacionDetalle
        ? obj.listarEvaluacionDetalle
        : [];
    }
  }
}

export interface ConsideracionesGenerales {
  idParametro: number;
  codigo: string;
  valorPrimario: string;
  valorSecundario: string;
  valorTerciario: string;
  codigoSniffs: string;
  codigoSniffsDescripcion: string;
  prefijo: string;
  idPlanManejo: number;
  idEvaluacionDet: number;
  listNivel2: [];
  observacion: string;
}
