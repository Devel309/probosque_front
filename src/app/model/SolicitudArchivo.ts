export interface SolicitudArchivoModel {
  idSolicitud: number;
  archivo: File;
  nombreArchivo: string;
  tipoArchivo: string;
}

export interface SolicitudCargaArchivoModel {
  solArchivoTitProp: SolicitudArchivoModel; // TITULO DE PROPIEDAD
  solArchivoDecJur: SolicitudArchivoModel; // DECLARACION JURADA
  solArchivoLimCol: SolicitudArchivoModel; // LIMITES Y COLINDANCIAS
  solArchivoActRepLeg: SolicitudArchivoModel; // ACTA DE ASAMBLEA RRLL
  solArchivoAreCom: SolicitudArchivoModel; /// AREA COMUNIDAD
  solArchivoAsamAcu: SolicitudArchivoModel; // ASAMBLEA ACUERDO
  solArchivoRegFore: SolicitudArchivoModel; // CONTRATO REGENTE FORESTAL
  solArchivoArrf: SolicitudArchivoModel; //  ARRF
  solArchivoComPag: SolicitudArchivoModel; // COMPROBANTE DE PAGO - PENDIENTE
  solArchivoConTer: SolicitudArchivoModel; // CONTRATO TERCERO
  solArchivoActAsa: SolicitudArchivoModel; // ACTA ASAMBLEA
}

export interface SolicitudArchivoAdjuntoModel {
  idSolicitud: string;
  idArchivoSolicitud: string;
  nombreArchivo: string;
  tipoArchivo: string;
  file: File | null;
}

export interface SolicitudPermisoArchivoAdjuntoModel {
  idSolicitud: string;
  idArchivo: any;
  idArchivoSolicitud: any;
  nombreArchivo: string;
  tipoDocumento: any;
  descripcionArchivo: string;
  rutaArchivo: string;
  file: File | null;
  flagActualiza?: boolean;
}

export interface SolicitudPermisoArchivoModel {
  idSolicitud: number;
  idArchivoSolicitud: number;
  file: File;
  nombreArchivo: string;
  tipoDocumento: any;
  rutaArchivo: string;
  flagActualiza: boolean;
}

export interface SolicitudPermisoCargaArchivoModel {
  solArchivoTitProp: SolicitudPermisoArchivoModel; // TITULO DE PROPIEDAD
  solArchivoActRepLeg: SolicitudPermisoArchivoModel; // ACTA DE ASAMBLEA RRLL
  solArchivoLimCol: SolicitudPermisoArchivoModel; // LIMITES Y COLINDANCIAS
  solArchivoComPag: SolicitudPermisoArchivoModel; // COMPROBANTE DE PAGO - PENDIENTE
  solArchivoAreCom: SolicitudPermisoArchivoModel; /// AREA COMUNIDAD
  solArchivoActAsa: SolicitudPermisoArchivoModel; // ACTA ASAMBLEA
  solArchivoRegFore: SolicitudPermisoArchivoModel; // CONTRATO REGENTE FORESTAL
  solArchivoArrf: SolicitudPermisoArchivoModel; //  ARRF
  solArchivoDecJur: SolicitudPermisoArchivoModel; // DECLARACION JURADA
  solArchivoConTer: SolicitudPermisoArchivoModel; // CONTRATO TERCERO
  solArchivoAsamAcu: SolicitudPermisoArchivoModel; // ASAMBLEA ACUERDO
}
