import { AuditoriaModel } from "./auditoria";
import { TipoParametroModel } from "./TipoParametro";
export interface ParametroModel extends AuditoriaModel {
    idParametro: number;
    codigo: string;
    valorPrimario: string;
    valorSecundario: string;
    tipoParametro: TipoParametroModel;
    idParametroPadre: number;
    codigoParametro?: string;
    idPlanManejo?: number


}

export interface ParametroValor {
    codigo: string;
    valor1: string;
    prefijo: string;
}