import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";
export interface OrganizacionActividadModel extends AuditoriaModel {
  idOrgActividad: number;
  planManejo: PlanManejoModel;
  actividad: String;
  descripcionDetalle: string;
  observacionDetalle:string;
  organizacion?: string;
  equipo:string;
  idActividadSilviculturalDet: number
}
