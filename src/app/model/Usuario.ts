export interface UsuarioModel {
    apellidoMaterno: string;
    apellidoPaterno: string;
    correoElectronico: string;
    idusuario: number;
    nombres: string;
    numeroDocumento: string;
    sirperfil: string;
    region: string;
    usuarioDescripcion: string;
    sysusuario: string;
    codigoEmpleado: string;
    idtipoDocumento: number;
    idempresa: number;
    idautoridad: number;
    tipoDocumento: string;
    estado: string;
    identidad: number;
    codigoUsuario: string;
    usuario: string;
    contraseña: string;
    sysfecha: number;
    descripcionARFFS: string;
    codigoDepartamentoARFFS: string;
    token: string;
    estadoDescripcion: string;
    perfiles: Perfil[];
    razonSocial: string;
}

export interface Perfil {
    codigoAplicacion: string,
    codigoPerfil: string,
    descripcion: string,
    idPerfil: number
}