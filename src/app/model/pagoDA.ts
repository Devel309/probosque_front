export class PagoDA {
    accion: boolean | null = null;
    areHA: number | null = null;
    codTipoPago: string | null = null;
    codigoEstado: string | null = null;
    codigoTH: string | null = null;
    conforme: string | null = null;
    descripcion: string | null = null;
    detalle: string | null = null;
    fechaFin: Date | any | null = null;
    fechaInicio: Date | any | null = null;
    idPago: number = 0;
    idUsuarioRegistro: number | null = null;
    listaCronograma: CronogramaPA[] = [];
    maternoRepresentante: string | null = null;
    maternoTitular: string | null = null;
    mecanismoPago: string | null = null;
    modalidadTH: string | null = null;
    montoPagoAnual: number | null = null;
    montoPagoAnualCalculado: number | null = null;
    montoPagoHA: number | null = null;
    nombreRepresentante: string | null = null;
    nombreTitular: string | null = null;
    nrDocumentoGestion: number | null = null;
    nroDocumento: string | null = null;
    nroDocumentoRepresentante: string | null = null;
    observacion: string | null = null;
    paternoRepresentante: string | null = null;
    paternoTitular: string | null = null;
    perfil: string | null = null;
    tipoDocumento: string | null = null;
    tipoDocumentoGestion: string | null = null;
    tipoDocumentoRepresentante: string | null = null;
    tipoMoneda: string | null = null;
    tipoPersona: string | null = null;
    ubigeo: string | null = null;
    vigencia: number | null = null;

    constructor(data?: any) {
        if (data) {
            this.accion = data.accion ? data.accion : null;
            this.areHA = data.areHA ? data.areHA : null;
            this.codTipoPago = data.codTipoPago ? data.codTipoPago : null;
            this.codigoEstado = data.codigoEstado ? data.codigoEstado : null;
            this.codigoTH = data.codigoTH ? data.codigoTH : null;
            this.conforme = data.conforme ? data.conforme : null;
            this.descripcion = data.descripcion ? data.descripcion : null;
            this.detalle = data.detalle ? data.detalle : null;
            this.fechaFin = data.fechaFin ? data.fechaFin : null;
            this.fechaInicio = data.fechaInicio ? data.fechaInicio : null;
            this.idPago = data.idPago ? data.idPago : 0;
            this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro : null;
            this.listaCronograma = data.listaCronograma ? data.listaCronograma : [];
            this.maternoRepresentante = data.maternoRepresentante ? data.maternoRepresentante : null;
            this.maternoTitular = data.maternoTitular ? data.maternoTitular : null;
            this.mecanismoPago = data.mecanismoPago ? data.mecanismoPago : null;
            this.modalidadTH = data.modalidadTH ? data.modalidadTH : null;
            this.montoPagoAnual = data.montoPagoAnual ? data.montoPagoAnual : null;
            this.montoPagoAnualCalculado = data.montoPagoAnualCalculado ? data.montoPagoAnualCalculado : null;
            this.montoPagoHA = data.montoPagoHA ? data.montoPagoHA : null;
            this.nombreRepresentante = data.nombreRepresentante ? data.nombreRepresentante : null;
            this.nombreTitular = data.nombreTitular ? data.nombreTitular : null;
            this.nrDocumentoGestion = data.nrDocumentoGestion ? data.nrDocumentoGestion : null;
            this.nroDocumento = data.nroDocumento ? data.nroDocumento : null;
            this.nroDocumentoRepresentante = data.nroDocumentoRepresentante ? data.nroDocumentoRepresentante : null;
            this.observacion = data.observacion ? data.observacion : null;
            this.paternoRepresentante = data.paternoRepresentante ? data.paternoRepresentante : null;
            this.paternoTitular = data.paternoTitular ? data.paternoTitular : null;
            this.perfil = data.perfil ? data.perfil : null;
            this.tipoDocumento = data.tipoDocumento ? data.tipoDocumento : null;
            this.tipoDocumentoGestion = data.tipoDocumentoGestion ? data.tipoDocumentoGestion : null;
            this.tipoDocumentoRepresentante = data.tipoDocumentoRepresentante ? data.tipoDocumentoRepresentante : null;
            this.tipoMoneda = data.tipoMoneda ? data.tipoMoneda : null;
            this.tipoPersona = data.tipoPersona ? data.tipoPersona : null;
            this.ubigeo = data.ubigeo ? data.ubigeo : null;
            this.vigencia = data.vigencia ? data.vigencia : null;
        }
    }
}

export class CronogramaPA {
    anio: number | null = null;
    codTipoPagoCronograma: string | null = null;
    descripcion: string | null = null;
    detalle: string | null = null;
    fechaFin: Date | any | null = null;
    fechaInicio: Date | any | null = null;
    idPagoCronograma: number = 0;
    idUsuarioRegistro: number | null = null;
    listaPeriodo: PeriodoPA[] = [];
    observacion: string | null = null;
    perfil: string | null = null;
    verPeriodo: boolean | null = null;

    constructor(obj?: any) {
        if (obj) {
            this.anio = obj.anio ? obj.anio : null;
            this.codTipoPagoCronograma = obj.codTipoPagoCronograma ? obj.codTipoPagoCronograma : null;
            this.descripcion = obj.descripcion ? obj.descripcion : null;
            this.detalle = obj.detalle ? obj.detalle : null;
            this.fechaFin = obj.fechaFin ? obj.fechaFin : null;
            this.fechaInicio = obj.fechaInicio ? obj.fechaInicio : null;
            this.idPagoCronograma = obj.idPagoCronograma ? obj.idPagoCronograma : 0;
            this.idUsuarioRegistro = obj.idUsuarioRegistro ? obj.idUsuarioRegistro : null;
            this.listaPeriodo = obj.listaPeriodo ? obj.listaPeriodo : [];
            this.observacion = obj.observacion ? obj.observacion : null;
            this.perfil = obj.perfil ? obj.perfil : null;
            this.verPeriodo = obj.verPeriodo ? obj.verPeriodo : null;
        }
    }
}

export class PeriodoPA {
    balancePago: number | null = null;
    codTipoPagoPeriodo: string | null = null;
    descripcion: string | null = null;
    detalle: string | null = null;
    fechaComprobante: Date | any | null = null;
    fechaLimite: Date | any | null = null;
    idPagoPeriodo: number = 0;
    idUsuarioRegistro: number | null = null;
    monto: number | null = null;
    montoFinal: number | null = null;
    observacion: string | null = null;
    perfil: string | null = null;

    constructor(obj2?: any) {
        if (obj2) {
            this.balancePago = obj2.balancePago ? obj2.balancePago : null;
            this.codTipoPagoPeriodo = obj2.codTipoPagoPeriodo ? obj2.codTipoPagoPeriodo : null;
            this.descripcion = obj2.descripcion ? obj2.descripcion : null;
            this.detalle = obj2.detalle ? obj2.descripcion : null;
            this.fechaComprobante = obj2.fechaComprobante ? obj2.fechaComprobante : null;
            this.fechaLimite = obj2.fechaLimite ? obj2.fechaLimite : null;
            this.idPagoPeriodo = obj2.idPagoPeriodo ? obj2.idPagoPeriodo : 0;
            this.idUsuarioRegistro = obj2.idUsuarioRegistro ? obj2.idUsuarioRegistro : null;
            this.monto = obj2.monto ? obj2.monto : null;
            this.montoFinal = obj2.montoFinal ? obj2.montoFinal : null;
            this.observacion = obj2.observacion ? obj2.observacion : null;
            this.perfil = obj2.perfil ? obj2.perfil : null;
        }
    }
}
