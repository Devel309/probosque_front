export interface ArchivoDema {
  codigoProceso?: string;
  codigoSubTipo?: null;
  descripcion?: string;
  documento: string;
  extension?: null;
  extensionArchivo?: string;
  idArchivo?: number;
  idPlanManejo?: number;
  idPlanManejoArchivo?: number;
  idTipoDocumento?: string;
  nombre?: string;
  nombreArchivo: string;
  observacion?: string;
}
