export class InformacionBasicaParcelaCortaPOCCModel {
  constructor(data?: any) {
    if (data) {
      this.idInfBasica = data.idInfBasica ? data.idInfBasica : 0;
      this.idInfBasicaDet = data.idInfBasicaDet ? data.idInfBasicaDet : 0;

      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.codInfBasicaDet = data.codInfBasicaDet ? data.codInfBasicaDet : '';

      this.coordenadaEsteIni = data.coordenadaEsteIni
        ? data.coordenadaEsteIni
        : '0';
      this.coordenadaNorteIni = data.coordenadaNorteIni
        ? data.coordenadaNorteIni
        : '0';
      this.referencia = data.referencia ? data.referencia : null;
      this.puntoVertice = data.puntoVertice ? data.puntoVertice : '0';
      this.descripcion = data.descripcion ? data.descripcion : '';
      this.areaHa = data.areaHa ? data.areaHa : 0;
      this.codSubInfBasicaDet = data.codSubInfBasicaDet
        ? data.codSubInfBasicaDet
        : '';

      this.idTipoBosque = data.idTipoBosque ? data.idTipoBosque : 0;
      this.distanciaKm = data.distanciaKm ? data.distanciaKm : 0;
      this.tiempo = data.tiempo ? data.tiempo : 0;
      this.medioTransporte = data.medioTransporte ? data.medioTransporte : '';

      this.areaHaPorcentaje = data.areaHaPorcentaje ? data.areaHaPorcentaje : 0;

      this.epoca = data.epoca ? data.epoca : '';
      this.idRios = data.idRios ? data.idRios : 0;
      this.zonaVida = data.zonaVida ? data.zonaVida : '';
      this.idFauna = data.idFauna ? data.idFauna : 0;
      this.idFlora = data.idFlora ? data.idFlora : 0;
      this.nombreRio = data.nombreRio ? data.nombreRio : '';
      this.nombreLaguna = data.nombreLaguna ? data.nombreLaguna : '';
      this.nombreQuebrada = data.nombreQuebrada ? data.nombreQuebrada : '';
    }
  }

  idInfBasica: number = 0;
  idInfBasicaDet: number = 0;
  codInfBasicaDet: string = '';
  puntoVertice: string = '0;';
  coordenadaEsteIni: any = '0';
  coordenadaNorteIni: any = ' 0';
  referencia: any = null;
  distanciaKm: number = 0;
  tiempo: number = 0;
  medioTransporte: string = '';
  idUsuarioRegistro: number = 0;
  epoca: string = '';
  idRios: number = 0;
  descripcion: string = '';
  areaHa: number = 0;
  areaHaPorcentaje: number = 0;

  zonaVida: string = '';
  idFauna: number = 0;
  idFlora: number = 0;
  nombreRio = '';
  nombreLaguna = '';
  nombreQuebrada = '';

  codSubInfBasicaDet: string = '';
  idTipoBosque: number = 0;
  nombreComun: string = '';
  nombreCientifico: string = '';
  familia: string = '';

  // idInfBasica: number = 0;
}

export class InformacionBasicaModel {
  constructor(data?: any) {
    if (data) {
      this.numeroPc = data.numeroPc ? data.numeroPc : 0;
      this.areaTotal = data.areaTotal ? data.areaTotal : 0;
      this.nroHojaCatastral = data.nroHojaCatastral ? data.nroHojaCatastral : 0;
      this.nombreHojaCatastral = data.nombreHojaCatastral ? data.nombreHojaCatastral : '';
      this.codSubInfBasica = data.codSubInfBasica ? this.codSubInfBasica : '';
      this.codNombreInfBasica = data.codNombreInfBasica ? data.codNombreInfBasica : '';
      this.masPc = data.masPc ? data.masPc : false;
      this.detalle = data.detalle ? this.detalle : '';
      this.comunidad = data.comunidad ? this.comunidad : '';
      this.nroPc = data.nroPc ? data.nroPc : "";
    }
  }
  numeroPc: number = 0;
  areaTotal: number = 0;
  nroHojaCatastral: number = 0;
  nombreHojaCatastral: string = '';
  codSubInfBasica: string = '';
  codNombreInfBasica: string = '';
  masPc: boolean = false;
  detalle: string = '';
  comunidad: string = "";
  nroPc: string = "";
}
