export interface Entidad {
  codigo: string;
  prefijo: string;
  valor1: string;
}

export class OpinionModel {
  constructor(obj?: any) {
    if (obj) {
      this.idSolOpinion = obj.idSolOpinion ? obj.idSolOpinion : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.idPlanManejo = obj.idPlanManejo ? obj.idPlanManejo : 0;
      this.idArchivo = obj.idArchivo ? obj.idArchivo : "";
      this.subCodSolicitud = obj.subCodSolicitud ? obj.subCodSolicitud : "";
      this.tipoDocumento = obj.tipoDocumento ? obj.tipoDocumento : "";
      this.numDocumento = obj.numDocumento ? obj.numDocumento : "";
      this.fechaDocumento = obj.fechaDocumento ? obj.fechaDocumento : "";
      this.asunto = obj.asunto ? obj.asunto : "";
      this.entidad = obj.entidad ? obj.entidad : "";
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
    }
  }
  idSolOpinion?: number;
  idPlanManejo?: number;
  codSolicitud?: string;
  subCodSolicitud?: string;
  tipoDocumento?: string;
  numDocumento?: string;
  fechaDocumento?: Date;
  asunto?: string;
  entidad?: string;
  idUsuarioRegistro?: number;
  idArchivo?: number
}

export interface ListaSolicitudModel {
  asunto: string;
  codSolicitud: string;
  entidad: string;
  estado: null;
  fechaDocumento: string;
  fechaElimina: null;
  fechaModificacion: null;
  fechaRegistro: null;
  idPlanManejo: number;
  idSolOpinion: number;
  idUsuarioElimina: null;
  idUsuarioModificacion: null;
  idUsuarioRegistro: null;
  numDocumento: string;
  pageNum: null;
  pageSize: null;
  search: null;
  startIndex: null;
  subCodSolicitud: string;
  tipoDocumento: string;
  totalPage: null;
  totalRecord: null;
}
