import { AuditoriaDto } from "./auditoria";

export class InformacionGeneralDemaDetalle extends AuditoriaDto {

  idInformacionGeneralDetalle?: number | null;
  idInformacionGeneral?: number | null;
  codigo?: string | null;
  descripcion?: string | null;
  detalle?: string | null;
  observacion?: string | null;

  constructor(obj?: Partial<InformacionGeneralDemaDetalle>) {
    super();

    this.idInformacionGeneralDetalle = null;
    this.idInformacionGeneral = null;
    this.codigo = null;
    this.descripcion = null;
    this.detalle = null;
    this.observacion = null;

    if (obj) Object.assign(this, obj);
  }

}
