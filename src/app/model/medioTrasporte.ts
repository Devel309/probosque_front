export interface MedioTrasporte {
  search: string;
  pageNum: string;
  pageSize: string;
  totalRecord: string;
  totalPage: string;
  startIndex: string;
  estado: string;
  idUsuarioRegistro: string;
  fechaRegistro: string;
  idUsuarioModificacion: string;
  fechaModificacion: string;
  idUsuarioElimina: string;
  fechaElimina: string;
  idMedioTransporte: number;
  codTipoMedio: string;
  medioTransporte: string;
}

export interface Rios {
  search: string;
  pageNum: string;
  pageSize: string;
  totalRecord: string;
  totalPage: string;
  startIndex: string;
  estado: string;
  idUsuarioRegistro: string;
  fechaRegistro: string;
  idUsuarioModificacion: string;
  fechaModificacion: string;
  idUsuarioElimina: string;
  fechaElimina: string;
  idHidrografia: number;
  tipoHidrografia: string;
  nombre: string;
  descripcion: string;
  desembocadura: string;
  region: string;
}

export interface EspecieForestal {
  idEspecie: number;
  codfor: string;
  nombreCientifico: string;
  nombreComun: string;
  autor: string;
  sinonimia: string;
  familia: string;
  numero_lista: string;
  fuente: string;
  categoria: string;
  oficial: string;
  habitoCrecimiento: string;
  tipouso: string;
  cites: string;
  nombrecomercial: string;
  dmc: string;
  idFuenteOficial: string;
}

export interface EspeciesFauna {
  idEspecie: number;
  codfor: string;
  nombreCientifico: string;
  nombreComun: string;
  autor: string;
  sinonimia: string;
  familia: string;
  numero_lista: string;
  fuente: string;
  categoria: string;
  oficial: string;
  habitoCrecimiento: string;
  tipouso: string;
  cites: string;
  nombrecomercial: string;
  dmc: string;
  idFuenteOficial: string;
  nombreNativo: string;
  categoriaAmenaza: string;
}
