export class OpcionAccionAppPerfilModel {
    constructor(data?:any) {
        if(data)
        {
            this.estado = data.estado;
            this.idopcionAccion = data.idopcionAccion;
            this.idperfilAplicacion = data.idperfilAplicacion;
            this.idperfilAplicacionOpcionAccion = data.idperfilAplicacionOpcionAccion;
            this.opcionAccion = data.opcionAccion;
            this.perfilAplicacion = data.perfilAplicacion;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;

            return;
        }
    }

    estado: string = "1";
    idopcionAccion:number = 0;
    idperfilAplicacion:number = 0;
    idperfilAplicacionOpcionAccion:number = 0;
    opcionAccion:string = "";
    perfilAplicacion:string = "";
    sysfecha:any = null;
    sysusuario:any = null;

    local:boolean = false;

}