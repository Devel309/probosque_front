export class ParametroModel {
  constructor() { }

  idParametro: number | null = 0;
  idTipoParametro: number | null = 0;
  nombre: string | null = "";
  descripcion: string | null = "";
  codigo: string | null = "";
  valorPrimario: string | null = "";
  idUsuarioModificacion?: number | null = null;

  setDataInit() {
    this.idParametro = 0;
    this.idTipoParametro = 0;
    this.nombre = "";
    this.descripcion = "";
    this.codigo = "";
    this.valorPrimario = "";
    this.idUsuarioModificacion = null;
  }

  setData(obj: any) {
    this.idParametro = obj.idParametro ? obj.idParametro : 0;
    this.idTipoParametro = obj.idTipoParametro ? obj.idTipoParametro : 0;
    this.nombre = obj.nombre ? obj.nombre : "";
    this.descripcion = obj.descripcion ? obj.descripcion : "";
    this.codigo = obj.codigo ? obj.codigo : "";
    this.valorPrimario = obj.valorPrimario ? obj.valorPrimario : "";
    this.idUsuarioModificacion = obj.idUsuarioModificacion ? obj.idUsuarioModificacion : null;
  }
}