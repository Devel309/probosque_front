export class AccionOpcionModel {
    constructor(data?:any) {
        
        if(data)
        {
            this.accion = data.accion;
            this.estado = data.estado;
            this.idaccion = data.idaccion;
            this.idopcion = data.idopcion;
            this.idopcionAccion = data.idopcionAccion;
            this.opcion = data.opcion;
            this.opcionPrincipal = data.opcionPrincipal;
            this.opcionSecundaria = data.opcionSecundaria;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;

            return ;
        }

    }


    accion:string = "";
    estado:string = "1";
    idaccion:number = 0;
    idopcion:number = 0;
    idopcionAccion:number = 0;
    opcion:string = "";
    opcionPrincipal:any = null;
    opcionSecundaria:any = null;
    sysfecha:any = null;
    sysusuario:any = null;


    local:boolean = false;

}