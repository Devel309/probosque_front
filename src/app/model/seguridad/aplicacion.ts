export class AplicacionModel
{

    constructor(data?:any)
    {
        if(data)
        {
            this.aplicacion= data.aplicacion;
            this.codigoAplicacion = data.codigoAplicacion;
            this.detalleAplicacion = data.detalleAplicacion
            this.estado = data.estado;
            this.idaplicacion = data.idaplicacion;
            this.observacionAplicacion = data.observacionAplicacion;
            this.sysfecha = data.sysfecha;
            this.sysusuario  = data.sysusuario;
            this.tipoAplicacion = data.tipoAplicacion;
            return;
        }


    }


    aplicacion:string = "";
    codigoAplicacion:string = "";
    detalleAplicacion:string = "";
    estado:string = "1";
    idaplicacion:number = 0;
    observacionAplicacion:string = "";
    sysfecha:any = null;
    sysusuario:any = null;
    tipoAplicacion:string = "";
}