export class AccionModel {
    constructor(data?:any) {
    
        if(data)
        {
            this.accion = data.accion;
            this.descripcion = data.descripcion;
            this.estado = data.estado;
            this.estadoDescripcion = data.estadoDescripcion;
            this.idaccion = data.idaccion;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;
            this.usuario = data.usuario;
            
            return;
        }
    
    }

    accion:string ="";
    descripcion:string ="";
    estado:string = "1";
    estadoDescripcion:string = "";
    idaccion:number = 0;
    sysfecha:any = null;
    sysusuario:any =  null;
    usuario:any = null;


}