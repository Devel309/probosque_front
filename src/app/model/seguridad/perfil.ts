export class PerfilModel
{
    constructor(data?:any){

        if(data)
        {
            this.codigoPerfil = data.codigoPerfil;
            this.descripcion = data.descripcion;
            this.estado = data.estado;
            this.idperfil = data.idperfil;
            this.idtipoContacto = data.idtipoContacto;
            this.perfil = data.perfil;
            this.sysusuario = data.sysusuario;
            this.sysfecha = data.sysfecha;
            return;
        }
        this.codigoPerfil = "";
        this.descripcion = "";
        this.estado = "1";
        this.idperfil = 0;
        this.idtipoContacto = 0;
        this.perfil = "";
        this.sysfecha = null;
        this.sysusuario = null;
    }

    codigoPerfil: string;
    descripcion: string;
    estado: string;
    idperfil:number ;
    idtipoContacto:number;
    perfil:string ;
    sysfecha:any;
    sysusuario:any;
}