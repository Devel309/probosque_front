export class OpcionModel {
    constructor(data?:any) {
        if(data)
        {
            this.aplicacion = data.aplicacion;
            this.codigoOpcion = data.codigoOpcion;
            this.codigoOpcionPadre = data.codigoOpcionPadre;
            this.descripcion = data.descripcion;
            this.esPadre = data.esPadre;
            this.estado = data.estado;
            this.gerarquia = data.gerarquia;
            this.icono = data.icono;
            this.idaplicacion = data.idaplicacion;
            this.idopcion = data.idopcion;
            this.idopcionPadre = data.idopcionPadre;
            this.opcion = data.opcion;
            this.orden = data.orden;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;
            this.url = data.url;

            return;
        }
    }

    aplicacion:string = "";
    codigoOpcion:string = "";
    codigoOpcionPadre:any = null;
    descripcion:string = "";
    esPadre:boolean = false;
    estado:string = "1";
    gerarquia:number = 1;
    icono:string = "";
    idaplicacion:number = 0;
    idopcion:number = 0;
    idopcionPadre:number = 0;
    opcion:string = "";
    orden:number = 1;
    sysfecha:any = null;
    sysusuario:any = null;
    url:any = null;

}