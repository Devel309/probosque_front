export class aplicacionPerfilUsaurioModel {
    constructor(data?:any) {
        if(data)
        {
            this.estado = data.estado;
            this.idperfilAplicacion = data.idperfilAplicacion;
            this.idperfilAplicacionUsuario = data.idperfilAplicacionUsuario;
            this.idusuario = data.idusuario;
            this.perfilAplicacion = data.perfilAplicacion;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;
            this.usuario = data.usuario;
    
            return;
        }
        
    }

    estado:string = "1";
    idperfilAplicacion:number =  0;
    idperfilAplicacionUsuario:number = 0;
    idusuario:number = 0;
    perfilAplicacion:string = "";
    sysfecha:any = null;
    sysusuario:any = null;
    usuario:string = "";

    local:boolean=false;

}