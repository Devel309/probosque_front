export class PerfilAplicacionModel {
    constructor(data?:any) {
        
        if(data)
        {
            this.aplicacion = data.aplicacion;
            this.descripcion = data.descripcion;
            this.estado = data.estado;
            this.idaplicacion = data.idaplicacion;
            this.idperfil = data.idperfil;
            this.idperfilAplicacion = data.idperfilAplicacion;
            this.perfil = data.perfil;
            this.perfilAplicacion = data.perfilAplicacion;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;

            return;
        }

    }

    aplicacion:string = "";
    descripcion:string = "";
    estado:string = "1";
    idaplicacion:number = 0;
    idperfil:number = 0;
    idperfilAplicacion:number = 0;
    perfil:string = "";
    perfilAplicacion:any = null;
    sysfecha:any = null;
    sysusuario:any = null;

    local:boolean=false;

}