export class EntidadModel
{
    constructor(data?:any){
        if(data){
            this.codigoDependencia = data.codigoDependencia;
            this.direccion = data.direccion;
            this.estado = data.estado;
            this.identidad = data.identidad;
            this.identidadPadre = data.identidadPadre;
            this.idorganizacion = data.idorganizacion;
            this.idrepresentante = data.idrepresentante;
            this.organizacion = data.organizacion;
            this.representate = data.representate;
            this.ruc = data.ruc;
            this.sigla = data.sigla;
            this.ubigeo = data.ubigeo;
            this.flagTipoEntidad = data?.flagTipoEntidad || false;
            this.descripcion = data?.descripcion || "";
            
            if(this.ubigeo){
                this.idDepartamento = this.ubigeo.substr(0,2);
                this.idProvincia = this.ubigeo.substr(0,4);
                this.idDistrito = this.ubigeo;
            }
            
    
            return;
        }
       
    }


    codigoDependencia:string = "";
    direccion:string = "";
    estado:string = "1";
    identidad:number = 0;
    identidadPadre:number = 0;
    idorganizacion:number = 0;
    idrepresentante:number = 0;
    organizacion:string = "";
    idDepartamento:string = "";
    idProvincia:string = ""
    idDistrito:string = "";
    representate:string = "";
    ruc:string = "";
    sigla:string = "";
    ubigeo:string = "";
    flagTipoEntidad: boolean = false;
    descripcion: string = "";

    setUbigeo():void{
        this.ubigeo = this.idDistrito;
    }
    

}