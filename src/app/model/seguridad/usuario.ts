export class UsuarioModel {
    constructor(data?: any) {

        if (data) {
            this.apellidoMaterno = data.apellidoMaterno;
            this.apellidoPaterno = data.apellidoPaterno;
            this.codigoEmpleado = data.codigoEmpleado;
            this.codigoUsuario = data.codigoUsuario;
            this.contrasenia = data.contrasenia;
            this.correoElectronico = data.correoElectronico;
            this.empresa = data.empresa;
            this.entidad = data.entidad;
            this.estado = data.estado;
            this.idautoridad = data.idautoridad;
            this.idempresa = data.idempresa;
            this.identidad = data.identidad;
            this.idtipoDocumento = data.idtipoDocumento;
            this.idusuario = data.idusuario;
            this.nombres = data.nombres;
            this.numeroDocumento = data.numeroDocumento;
            this.region = data.region;
            this.sysfecha = data.sysfecha;
            this.sysusuario = data.sysusuario;
            this.tipoDocumento = data.tipoDocumento;
            this.usuario = data.usuario;
            this.usuarioDescripcion = data.usuarioDescripcion;
            this.perfiles = data.perfiles;
            this.sirperfil = data.sirperfil;
            this.idPerfil = data.idPerfil;
            return;
        }

    }

    apellidoMaterno: string = "";
    apellidoPaterno: string = "";
    codigoEmpleado: any = null;
    codigoUsuario: string = "";
    contrasenia: any = null;
    correoElectronico: string = "";
    empresa: string = "";
    entidad: string = "";
    estado: string = "1";
    idautoridad: any = 0;
    idempresa: number = 0;
    identidad: number = 0;
    idtipoDocumento: number = 3;
    idusuario: number = 0;
    nombres: string = "";
    numeroDocumento: string = "";
    region: any = "";
    sysfecha: any = null;
    sysusuario: any = null;
    tipoDocumento: string = "";
    usuario: any = "";
    usuarioDescripcion: string = "";
    perfiles: PerfilModel[] = [];
    sirperfil: string = "";
    idPerfil: number = 0;

}


export class PerfilModel {
    constructor(data?: any) {

        if (data) {
            this.codigoAplicacion = data.codigoAplicacion;
            this.codigoPerfil = data.codigoPerfil;
            this.descripcion = data.descripcion;
            this.idPerfil = data.idPerfil;

            return;
        }

    }

    codigoAplicacion: string = "";
    codigoPerfil: String = "";
    descripcion: String = "";
    idPerfil: number = 0;

}


