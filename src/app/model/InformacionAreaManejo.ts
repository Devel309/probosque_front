import { AuditoriaModel } from "./auditoria";
export interface InformacionUnidadManejo extends AuditoriaModel {
    idUnidadManejo: number;
    cuenca: string;
    idDistrito: number;
    idPlanManejo: number;
}

export interface InformacionCoordenadaUtm {
    anexoSector: string | null;
    estado: string;
    este: number;
    idCoordUtm: number;
    idUnidadManejo: number;
    idUsuarioModificacion: number | null;
    idUsuarioRegistro: number | null;
    referencia: string;
    tipo: number;
    vertice: string;
    norte: number;
}

export interface InformacionBasicaDetalle {
    idInfBasica: number | 0;
    idInfBasicaDet:number;
    estado: string;
    coordenadaEsteIni: number;
    codInfBasicaDet:String;
    codSubInfBasicaDet:String;
    idUnidadManejo: number;
    idUsuarioModificacion: number | null;
    idUsuarioRegistro: number | null;
    referencia: string;
    puntoVertice: string;
    coordenadaNorteIni: number;
    descripcion:string;
}

export interface InformacionAccesibilidadmanejomatriz {
    anexosector: string;
    estado: string;
    idAcceManejo: number;
    idAcceManejomatriz: number;
    idUsuarioRegistro: number;
    listAccesibilidadManejoMatrizDetalle: listAccesibilidadManejoMatrizDetalle[];
}

export interface listAccesibilidadManejoMatrizDetalle {
    idAcceManejomatriz: number;
    idAcceManejomatrizdetalle: number;
    distancia: number;
    referencia: string;
    subparcela: number;
    tiempo: number;
    tipovehiculo: string;
    via: string;
    estado: string;
}


export interface InfobioFaunaSilvestre {
    idInfobioFaunaSilvestre: number;
    idPlanManejo: number;
    idEspecieForestal: number;
    nombreComun: string;
    nombreCientifico: string;
    categoriaAmenaza: string;
    nombreNativo: string;
    cites: string;
    observacion: string;
    estatus: string;
    idUsuarioRegistro: number;
    idUsuarioModificacion: number | null;
    estado: string;
}

export interface InfoAspectofisicofisiografia {
    accion: boolean;
    area: number;
    descripcion: string;
    especificacion: string;
    idApectofisicofisiografia: number;
    idPlanManejo: number;
    idTipounidadfisiografica: string;
    idUsuarioRegistro: number;
    tipounidadfisiografica: string;
    estado: string;
}

export interface InfoEconomicaPgmf {
    estado: string;
    idInformacionSocioEconomica: number;
    idPlanManejo: number;
    idUsuarioModificacion: number;
    idUsuarioRegistro: number;
    peronasTrabajan: number;
    personasEmpadronada: number;

}