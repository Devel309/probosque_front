
import { AuditoriaModel } from "./auditoria";
import { PlanManejoModel } from "./PlanManejo";

export interface ProteccionBosqueGestionAmbientalModel extends  AuditoriaModel{
  idProBosqueGesAmb:number;
  planManejo:PlanManejoModel;
  idTipoPrograma:any;
  actividad:String;
  impacto:String;
  mitigacionAmbiental:String;

}

