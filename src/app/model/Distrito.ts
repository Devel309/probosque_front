export interface DistritoModel { 
    idDistrito:number;
    nombreDistrito:String;
    codDistrito:String;
    codDistritoInei:String;
    codDistritoReniec:String;
    codDistritoSunat:String;
    idProvincia:number;
    idDepartamento:number;
    
}