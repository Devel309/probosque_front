export class PlanManejoArchivo {

	idPlanManejoArchivo: number | null;
	codigoTipoPgmf: string | null;
	codigoSubTipoPgmf: string | null;
	descripcion: string | null;
	observacion: string | null;
	idPlanManejo: number | null;
	idArchivo: number | null;
	idUsuarioElimina: number | null;

	constructor(obj?: Partial<PlanManejoArchivo>) {

		this.idPlanManejoArchivo = null;
		this.codigoTipoPgmf = null;
		this.codigoSubTipoPgmf = null;
		this.descripcion = null;
		this.observacion = null;
		this.idPlanManejo = null;
		this.idArchivo = null;
		this.idUsuarioElimina = null;

		if (obj) Object.assign(this, obj);
	}
}