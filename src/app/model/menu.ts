export class MenuNavegacionItem
{
    constructor(data?:any)
    {
        this.groupOpen = false;
        if(!data)
        {
            this.id = '';
            this.title = '';
            this.type = '';
            this.icon = '';
            this.url = '';
            this.classes = '';
           return;
        }
        this.id = data.id;
        this.title = data.title;
        this.type = data.type;
        this.icon = data.icon;
        this.url = data.url;
        this.classes = data.classes;
    }

    id?: string;
    title: string;
    type:string;
    translate?: string;
    icon?: string;
    hidden?: boolean;
    url: string;
    classes: string;
    groupOpen:boolean;
    exactMatch?: boolean;
    externalUrl?: boolean;
    openInNewTab?: boolean;
    function?: any;
    badge?: {
        title?: string;
        translate?: string;
        bg?: string;
        fg?: string;
    };
    children?: MenuNavegacionItem[];
}

export class MenuNavegacion extends MenuNavegacionItem
{
    children?: MenuNavegacionItem[];
}