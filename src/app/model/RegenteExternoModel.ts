export class RegenteExternoModel {
    apellidos: string;
    estado: string;
    id: number;
    nombres: string;
    numeroDocumento: string;
    numeroLicencia: string;
    periodo: number;
    nombresNroDocumento: string;

    constructor(obj?: Partial<RegenteExternoModel>) {
        this.apellidos = '';
        this.estado = '';
        this.id = 0;
        this.nombres = '';
        this.numeroDocumento = '';
        this.numeroLicencia = '';
        this.periodo = 0;
        this.nombresNroDocumento = '';

        if (obj) Object.assign(this, obj);
    }
}