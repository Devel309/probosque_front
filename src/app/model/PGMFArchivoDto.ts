export class PGMFArchivoDto {

	idPGMFArchivo: number | null;
	idPlanManejo: number | null;
	idArchivo: number | null;
	codigoTipoPGMF: string;
	codigoSubTipoPGMF: string;
	descripcion: string;
	observacion: string;
	nombre: string;
	extension: string;
	tipoDocumento: string;
	archivo: File | string | null | Blob;
	idUsuarioRegistro: number | null;

	constructor(obj?: Partial<PGMFArchivoDto>) {

		this.idPGMFArchivo = null;
		this.idPlanManejo = null;
		this.idArchivo = null;
		this.codigoTipoPGMF = '';
		this.codigoSubTipoPGMF = '';
		this.descripcion = '';
		this.observacion = '';
		this.nombre = '';
		this.extension = '';
		this.tipoDocumento = '';
		this.archivo = null;
		this.idUsuarioRegistro = null;

		if (obj) Object.assign(this, obj);
	}
}