import { AuditoriaDto } from "./auditoria";

export class SistemaManejoForestalDetalle extends AuditoriaDto {
    idSistemaManejoForestalDetalle: number | null;
    idSistemaManejoForestal: number | null;
    codigoTipoDetalle: string | null;
    categoriaZona: string | null;
    usoPotencial: string | null;
    usoPotencialNumerico: number | null;
    actividadRealizar: string | null;
    actividades: string | null;
    descripcionSistema: string | null;
    maquinariasInsumos: string | null;
    personalRequerido: string | null;
    observacion: string | null;
    codigoTipoSilvicultural: string | null;
    nombreTipoSilvicultural: string | null;
    descripcionTipoSilvicultural: string | null;
    codigoProceso: string | null;
    editable: boolean | null;

    enviar?: boolean | null;//flag fronted

    constructor(obj?: Partial<SistemaManejoForestalDetalle>) {
        super();

        this.idSistemaManejoForestalDetalle = null;
        this.idSistemaManejoForestal = null;
        this.codigoTipoDetalle = null;
        this.categoriaZona = null;
        this.usoPotencial = null;
        this.usoPotencialNumerico = null;
        this.actividadRealizar = null;
        this.actividades = null;
        this.descripcionSistema = null;
        this.maquinariasInsumos = null;
        this.personalRequerido = null;
        this.observacion = null;
        this.codigoTipoSilvicultural = null;
        this.nombreTipoSilvicultural = null;
        this.descripcionTipoSilvicultural = null;
        this.codigoProceso = null;
        this.editable = null;

        if (obj) Object.assign(this, obj);
    }

}

export const KEY_SMF_DETALLE: any = {
    categoriaZona: 'Categoría de zonificación',
    usoPotencialNumerico: 'Uso potencial',
    actividadRealizar: 'Actividad a realizar',
}

export const KEY_DEMA_DETALLE: any = {
    actividades: 'Actividad',
    descripcionSistema: 'Descripción',
    codigoTipoDetalle: 'Tipo Actividad',
    maquinariasInsumos: 'Equipos e Insumos',
    personalRequerido: 'Personal',
    observacion: 'Observaciones',
}