export class CapacitacionPGMFA {
  constructor(data?: any) {
    if (data) {
      this.idCapacitacion = data.idCapacitacion ? data.idCapacitacion : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo: 0;
      this.adjunto = data.adjunto ? data.adjunto : ''; // no tiene
      this.codTipoCapacitacion = data.codTipoCapacitacion ? data.codTipoCapacitacion : ''; // crearlo
      this.tema = data.tema ? data.tema : '';
      this.personaCapacitar = data.personaCapacitar ? data.personaCapacitar: '';
      this.idTipoModalidad = data.idTipoModalidad ? data.idTipoModalidad : 0;
      this.lugar = data.lugar ? data.lugar : '';
      this.periodo = data.periodo ? data.periodo : '';
      this.responsable = data.responsable ? data.responsable : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro :0;
      this.listCapacitacionDetalle = data.listCapacitacionDetalle ? data.listCapacitacionDetalle : '';
    }
  }

  idCapacitacion?: number;
  idPlanManejo?: number;
  adjunto?: string;
  codTipoCapacitacion?: string;
  tema?: string;
  personaCapacitar?: string;
  idTipoModalidad?: number | string;
  lugar?: string;
  periodo?: string;
  responsable?: string;
  idUsuarioRegistro?: number;
  listCapacitacionDetalle?: ListCapacitacionDetalle[];
}

export class ListCapacitacionDetalle {
  constructor(data?: any) {
    if (data) {
      this.idCapacitacionDet = data.idCapacitacionDet ? data.idCapacitacionDet : 0;
      this.actividad = data.actividad ? data.actividad : '';
      this.descripcion = data.descripcion ?  data.descripcion :'';
      this.idUsuarioRegistro = data.idUsuarioRegistro ? data.idUsuarioRegistro : 0;
    }
  }
  idCapacitacionDet?: number;
  actividad?: string;
  descripcion?: string;
  idUsuarioRegistro?: number;
}
