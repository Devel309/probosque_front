export abstract class CodigoPGMF {
    static readonly CODIGO_PROCESO: string = "PGMF";

    static readonly TAB_1: string = "PGMFCFFMIG";
    static readonly TAB_2: string = "PGMFCFFMOBJM";
    static readonly ACORDEON_2_1: "PGMFCFFMOBJMOG";
    static readonly ACORDEON_2_2: string = "PGMFCFFMOBJMOE";
  
    static readonly TAB_3: string = "PGMFCFFMIBUMF";
    static readonly ACORDEON_3_1: string = "PGMFCFFMIBUMFUE";
    static readonly ACORDEON_3_1_TAB_1: string = "PGMFCFFMIBUMFUEUPC";
    static readonly ACORDEON_3_1_TAB_2: string = "PGMFCFFMIBUMFUECCC";
    static readonly ACORDEON_3_2: string = "PGMFCFFMIBUMFAC";
    static readonly ACORDEON_3_2_TAB_1: string = "PGMFCFFMIBUMFACRVATF";
    static readonly ACORDEON_3_2_TAB_2: string = "PGMFCFFMIBUMFACNCSTF";
    static readonly ACORDEON_3_3: string = "PGMFCFFMIBUMFAFHF"
    static readonly ACORDEON_3_3_TAB_1: string = "PGMFCFFMIBUMFAFHFRQL"
    static readonly ACORDEON_3_3_TAB_2: string = "PGMFCFFMIBUMFAFHFUFA"
    static readonly ACORDEON_3_3_TAB_3: string = "PGMFCFFMIBUMFAFHFLCB"
    static readonly ACORDEON_3_3_TAB_4: string = "PGMFCFFMIBUMFAFHFISC"
    static readonly ACORDEON_3_4: string = "PGMFCFFMIBUMFABIO"
    static readonly ACORDEON_3_4_TAB_1: string = "PGMFCFFMIBUMFABIOFS"
    static readonly ACORDEON_3_4_TAB_2: string = "PGMFCFFMIBUMFABIOTB"
    static readonly ACORDEON_3_5: string = "PGMFCFFMIBUMFASECO"
    static readonly ACORDEON_3_5_TAB_1: string = "PGMFCFFMIBUMFASECOCP"
    static readonly ACORDEON_3_5_TAB_2: string = "PGMFCFFMIBUMFASECOIS"
    static readonly ACORDEON_3_6: string = "PGMFCFFMIBUMFAUIC"
    static readonly ACORDEON_3_6_TAB_1: string = "PGMFCFFMIBUMFAUICAUR"
    static readonly ACORDEON_3_6_TAB_2: string = "PGMFCFFMIBUMFAUICIMC"

    static readonly TAB_4: string = "PGMFCFFMOPUMF"
    static readonly ACORDEON_4_1: string = "PGMFCFFMOPUMFCO"
    static readonly ACORDEON_4_2: string = "PGMFCFFMOPUMFDAB"
    static readonly ACORDEON_4_2_TAB_1: string = "PGMFCFFMOPUMFDABBQ"
    static readonly ACORDEON_4_2_TAB_2: string = "PGMFCFFMOPUMFDABPC"
    static readonly ACORDEON_4_2_TAB_3: string = "PGMFCFFMOPUMFDABFC"
    static readonly ACORDEON_4_3: string = "PGMFCFFMOPUMFPV"
    static readonly ACORDEON_4_3_TAB_1: string = "PGMFCFFMOPUMFPVUMV"
    static readonly ACORDEON_4_3_TAB_2: string = "PGMFCFFMOPUMFPVS"
    static readonly ACORDEON_4_3_TAB_3: string = "PGMFCFFMOPUMFPVDML"
    static readonly ACORDEON_4_3_TAB_4: string = "PGMFCFFMOPUMFPVVUMF"

    static readonly TAB_5: string = "PGMFCFFMPPRFM"
    static readonly ACORDEON_5_1: string = "PGMFCFFMPPRFMCIF"
    static readonly ACORDEON_5_1_TAB_1: string = "PGMFCFFMPPRFMCIFPM"
    static readonly ACORDEON_5_1_TAB_2: string = "PGMFCFFMPPRFMCIFRF"
    static readonly ACORDEON_5_2: string = "PGMFCFFMPPRFMRPM"
    static readonly ACORDEON_5_3: string = "PGMFCFFMPPRFMRF"
    static readonly ACORDEON_5_4: string = "PGMFCFFMPPRFMPPRFFS"

    static readonly TAB_6: string = "PGMFCFFMMF"
    static readonly ACORDEON_6_1: string = "PGMFCFFMMFUPCO"
    static readonly ACORDEON_6_2: string = "PGMFCFFMMFST"
    static readonly ACORDEON_6_3: string = "PGMFCFFMMFCC"
    static readonly ACORDEON_6_4: string = "PGMFCFFMMFEMDMC"
    static readonly ACORDEON_6_5: string = "PGMFCFFMMFEFP"
    static readonly ACORDEON_6_6: string = "PGMFCFFMMFCAP"
    static readonly ACORDEON_6_6_TAB_1: string = "PGMFCFFMMFCAPVCPHA"
    static readonly ACORDEON_6_6_TAB_2: string = "PGMFCFFMMFCAPVCAP"
    static readonly ACORDEON_6_7: string = "PGMFCFFMMFPC"
    static readonly ACORDEON_6_8: string = "PGMFCFFMMFESA"
    static readonly ACORDEON_6_9: string = "PGMFCFFMMFEPS"
    static readonly ACORDEON_6_9_TAB_1: string = "PGMFCFFMMFEPSDIS"
    static readonly ACORDEON_6_9_TAB_2: string = "PGMFCFFMMFEPSTSA"
    static readonly ACORDEON_6_9_TAB_3: string = "PGMFCFFMMFEPSMEAD"

    static readonly TAB_7: string = "PGMFCFFMEIA"
    static readonly ACORDEON_7_1: string = "PGMFCFFMEIAAIA"
    static readonly ACORDEON_7_2: string = "PGMFCFFMEIAPGA"
    static readonly ACORDEON_7_2_1: string = "PGMFCFFMEIAPGAPAPC"
    static readonly ACORDEON_7_2_2: string = "PGMFCFFMEIAPGAPVS"
    static readonly ACORDEON_7_2_3: string = "PGMFCFFMEIAPGAPCA"

    static readonly TAB_8: string = "PGMFCFFMMNT"
    static readonly ACORDEON_8_1: string = "PGMFCFFMMNTSMNT"
    static readonly ACORDEON_8_2: string = "PGMFCFFMMNTAMNT"

    static readonly TAB_9: string = "PGMFCFFMPC"
    static readonly ACORDEON_9_1: string = "PGMFCFFMPCFPMF"
    static readonly ACORDEON_9_2: string = "PGMFCFFMPCIPMF"
    static readonly ACORDEON_9_3: string = "PGMFCFFMPCCGB"
    static readonly ACORDEON_9_4: string = "PGMFCFFMPCPRC"

    static readonly TAB_10: string = "PGMFCFFMCAP"
    static readonly ACORDEON_10_1: string = "PGMFCFFMCAPOPC"
    static readonly ACORDEON_10_2: string = "PGMFCFFMCAPAPP"

    static readonly TAB_11: string = "PGMFCFFMOM"
    static readonly ACORDEON_11_1: string = "PGMFCFFMOMFRPC"
    static readonly ACORDEON_11_2: string = "PGMFCFFMOMRQPAM"
    static readonly ACORDEON_11_3: string = "PGMFCFFMOMRQME"

    static readonly TAB_12: string = "PGMFCFFMPI"
    static readonly ACORDEON_12_1: string = "PGMFCFFMPIFC"
    static readonly ACORDEON_12_2: string = "PGMFCFFMPIFIN"

    static readonly TAB_13: string = "PGMFCFFMCA"
    
    static readonly TAB_14: string = "PGMFCFFMANEXO"
    static readonly ACORDEON_14_1: string = "PGMFCFFMANEXO1"
    static readonly ACORDEON_14_2: string = "PGMFCFFMANEXO2"
    static readonly ACORDEON_14_3: string = "PGMFCFFMANEXO3"

    static readonly TAB_15: string = "PGMFCFFMCED"
  }
  