import { ValidationErrors } from '@angular/forms';
import { AbstractControl, ValidatorFn } from '@angular/forms';

export class ValidatorsExtend {
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    const config: any = {
      required: 'Campo obligatorio.',
      minlength: `La longitud mínima (${validatorValue.requiredLength} caracteres)`,
      maxlength: `Excede longitud máxima (${validatorValue.requiredLength} caracteres)`,
      min: `Debe seleccionar un valor.`,
      requireobject: '* Debe seleccionar un item.',
      isMultiAlphanumeric: 'Campo solo admite letras y números.',
      isMultiAlpha: 'Campo solo admite letras.',
      number: 'Campo admite números y/o símbolos válidos',
      isNumeric: 'Campo admite solo números',
      passwordValid:
        'El password debe tener:<br/> (*) mínimo 8 caracteres <br/> (*) al menos una minúscula, una mayúscula y un número <br/>',
      pattern: 'Campo admite solo letras, numeros y/o símbolos válidos',
      isMultiAlphanumericSimbol:
        'Campo admite solo letras, numeros y/o símbolos válidos',
      email: 'Formato de correo no es válido',
      isEmail: 'Formato de correo no es válido',
      hasWhitespace: 'No se permite espacios en blanco.',
      isPhoneNumber: 'El numero ingresado no es válido.',
      isURL: 'La URL de la página ingresada no es correcta.',
      isMultiAlphaWithExceptions:
        'Campo admite letras, nros. y punto(.) y coma(,) .',
      isMultiAlphaWithHyphen: 'Campo no válido.', // 'Campo no admite carácteres especiales, excepto guión(-).',
      isNumericWithExceptions: 'Campo admite nros., (), #, *, - y +',
      isDecimal: 'Campo solo admite números decimales',
      isOnlyNumberAndScript: 'El numero ingresado no es válido.',
      isRUC: 'El RUC ingresado no valido.',
    };

    return config[validatorName];
  }

  static comboRequired(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (isEmptyInputValue(control.value)) {
        return null;
      }

      return control.value == 0 || control.value == '0' || control.value == ''
        ? { required: true }
        : null;
    };
  }

  reverse(s: any) {
    return s.split('').reverse().join('');
  }

  static passwordValid(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (isEmptyInputValue(control.value)) {
        return null;
      }

      let strongRegex = new RegExp(
        '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})'
      );

      const fullusername = localStorage.getItem('username')?.valueOf();
      let isReverseValueR = false;
      let isReverseValueN = false;
      const contrasena = control.value;
      if (fullusername) {
        const lastIndex = fullusername.indexOf('@');
        const username = fullusername.substring(0, lastIndex);
        const usernameReverse = username.split('').reverse().join('');
        isReverseValueR = contrasena.includes(usernameReverse);
        isReverseValueN = contrasena.includes(username);
      }   

      if (!(isReverseValueR || isReverseValueN ) && strongRegex.test(control.value)) {
        return null;
      } else {
        return {
          passwordValid: {
            requiredPattern: 'password poco seguro.',
            actualValue: control.value,
          },
        };
      }

      return control.value == 0 || control.value == '0' || control.value == ''
        ? { required: true }
        : null;
    };
  }


  static passwordValidReverseSame(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (isEmptyInputValue(control.value)) {
        return null;
      }

      const fullusername = localStorage.getItem('username')?.valueOf();
      let isReverseValueR = false;
      let isReverseValueN = false;
      const contrasena = control.value;
      if (fullusername) {
        const lastIndex = fullusername.indexOf('@');
        const username = fullusername.substring(0, lastIndex);
        const usernameReverse = username.split('').reverse().join('');
        isReverseValueR = contrasena.includes(usernameReverse);
        isReverseValueN = contrasena.includes(username);
      }      

      return isReverseValueR||isReverseValueN ? { passwordValidReverseSame: true } : null;
    };
  }
}

function isEmptyInputValue(value: any) {
  return value == null || (typeof value === 'string' && value.length === 0);
}
