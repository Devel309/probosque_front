export abstract class CodigoPrerequisitos {
  static readonly EVAL_PRESENTADO: string = "EPREPRES";
  static readonly EVAL_OBSERVADO: string = "EPREOBS";
  static readonly EVAL_NO_PRESENTADO: string = "EPRENPRE";
  static readonly EVAL_EN_EVALUACION: string = "EPREEVAL";
  static readonly EVAL_COMPLETADO: string = "EPRECOMP";

  static readonly PGMFA_ACORDEON_SUSCRIPCION: string = "PGMFARPPRS";
  static readonly PGMFA_ACORDEON_MEDIDAS_CAUTELARES: string = "PGMFARPPRM";
  static readonly PGMFA_ACORDEON_RESTRICCIONES: string = "PGMFARPPRR";
  static readonly PGMFA_ACORDEON_MEDIDAS_CORRECTIVAS: string = "PGMFARPPRMC";
  static readonly PGMFA_ACORDEON_VIGENCIA_DE_PODER: string = "PGMFARPPRVP";
  static readonly PGMFA_ACORDEON_VIGENCIA_JUNTA_DIRECTIVA: string = "PGMFARPPRVJD";
  static readonly PGMFA_ACORDEON_SUPERFICIE_AREA: string = "PGMFARPPSA";

  static readonly POAC_ACORDEON_MESA_DE_PARTES: string = "POACMPEV";
  static readonly POAC_ACORDEON_TUPA: string = "POACRPORT";
  static readonly POAC_ACORDEON_SUSCRIPCION: string = "POACRPORS";
  static readonly POAC_ACORDEON_MEDIDAS_CAUTELARES: string = "POACRPORMC";
  static readonly POAC_ACORDEON_RESTRICCIONES: string = "POACRPORR";
  static readonly POAC_ACORDEON_MEDIDAS_CORRECTIVAS: string = "POACRPORMCO";
  static readonly POAC_ACORDEON_SUPERFICIE_AREA: string = "POACRPOSA";


  static readonly ACORDEON_MEDIDAS_CAUTELARES: string = "RPPRM";
  static readonly ACORDEON_RESTRICCIONES: string = "RPPRR";
  static readonly ACORDEON_MEDIDAS_CORRECTIVAS: string = "RPPRMC";
}
