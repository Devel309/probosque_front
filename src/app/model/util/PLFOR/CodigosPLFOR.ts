export abstract class CodigosPLFOR {
    static readonly CODIGO_PROCESO: string = "PLFOR";
    static readonly TAB_1: string = "PLFORT1";          //T1 = tab1
    static readonly TAB_1_1: string = "PLFORT1_SS1";    //SS1 = sección 1 o bloque 1 o acordión 1
    static readonly TAB_1_2: string = "PLFORT1_SS2";    //SS2 = sección 2 o bloque 2 o acordión 2
    static readonly TAB_2: string = "PLFORT2";
    static readonly TAB_2_1: string = "PLFORT2_SS1";
    static readonly TAB_2_2: string = "PLFORT2_SS2";
    static readonly TAB_3: string = "PLFORT3";
    static readonly TAB_3_1: string = "PLFORT3_SS1";
    static readonly TAB_4: string = "PLFORT4";
    static readonly TAB_4_1: string = "PLFORT4_SS1";
    static readonly TAB_5: string = "PLFORT5";
    static readonly TAB_5_1: string = "PLFORT5_SS1";
    static readonly TAB_5_2: string = "PLFORT5_SS2";
    static readonly TAB_5_3: string = "PLFORT5_SS3";
    static readonly TAB_6: string = "PLFORT6";
    static readonly TAB_6_1: string = "PLFORT6_SS1";
}
