
import { ComboModel, ComboModel2 } from "./Combo";

export class DataDemoMPAFPP {
    static UnidadOtorgamientoUMF: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Área de producción forestal',
            value: 1
        },
        {
            text: 'Áreas de protección',
            value: 2
        },
        {
            text: 'Áreas de conservación',
            value: 3
        }
    ];

    static Activiades06: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Selección de árboles con diámetros mayores al DMC',
            value: 1
        },
        {
            text: 'Identificación y manejo de árboles semilleros',
            value: 2
        },
        {
            text: 'Limpieza del sotobosque',
            value: 3
        },
        {
            text: 'Eliminación de lianas y sogas',
            value: 4
        },
        {
            text: 'Manejo de regeneración natural',
            value: 5
        }
    ];

    static Medidas07: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Proteger el área de incendios forestales',
            value: 1
        },
        {
            text: 'Evitar la introducción de especies exóticas',
            value: 2
        },
        {
            text: 'Prohibir la caza ilegal de fauna silvestre y protegerla frente a terceros',
            value: 3
        },
        {
            text: 'Prohibir la tala o extracción ilegal de flora silvestre y protegerla frente a terceros',
            value: 4
        },
        {
            text: 'Restringir el acceso a personas no autorizadas a la UMF',
            value: 5
        },
        {
            text: 'Prohibir el cambio de uso dentro de tierras dentro de la UMF',
            value: 6
        },
        {
            text: 'Monitoreo de la UMF',
            value: 7
        }
    ];

    static TipoAprovechamiento08: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Aprovechamiento maderable',
            value: 1
        },
        {
            text: 'Aprovechamiento de productos forestales diferentes a la madera',
            value: 2
        }
    ];

    static Actividades08_A: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Tumbado',
            value: 1
        },
        {
            text: 'Trozado',
            value: 2
        },
        {
            text: 'Cuartoneado',
            value: 3
        },
        {
            text: 'Transporte de madera rolliza y/o cuartoneada',
            value: 4
        },
        {
            text: 'Acopio de madera rolliza y/o cuartoneada',
            value: 5
        },
        {
            text: 'Cubicación de madera rolliza y/o cuartoneada',
            value: 6
        }
    ];

    static Actividades08_B: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Extracción o recolección',
            value: 1
        },
        {
            text: 'Procesamiento o transformación',
            value: 2
        },
        {
            text: 'Transporte',
            value: 3
        },
        {
            text: 'Acopio',
            value: 4
        }
    ];

    static Actividades09: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Censos',
            value: 1
        },
        {
            text: 'Acondicionamiento de áreas para patios de acopio.',
            value: 2
        },
        {
            text: 'Apertura y mantenimiento de trochas y otros.',
            value: 3
        },
        {
            text: 'Actividades de aprovechamiento',
            value: 4
        },
        {
            text: 'Transporte en la UMF',
            value: 5
        }
    ];

    static Anhios10: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Año 1',
            value: 1
        },
        {
            text: 'Año 2',
            value: 2
        },
        {
            text: 'Año 3',
            value: 3
        }
    ];

    static TipoAprovechamiento10: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Aprovechamiento maderable',
            value: 1
        },
        {
            text: 'Aprovechamiento de productos forestales diferentes a la madera',
            value: 2
        }
    ];

    static Actividades10_A: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Tumbado',
            value: 1
        },
        {
            text: 'Trozado',
            value: 2
        },
        {
            text: 'Cuartoneado',
            value: 3
        },
        {
            text: 'Transporte de madera rolliza y/o cuartoneada',
            value: 4
        },
        {
            text: 'Acopio de madera rolliza y/o cuartoneada',
            value: 5
        }
    ];

    static Actividades10_B: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Extracción o recolección',
            value: 1
        },
        {
            text: 'Procesamiento o transformación',
            value: 2
        },
        {
            text: 'Transporte',
            value: 3
        },
        {
            text: 'Acopio',
            value: 4
        }
    ];

    static TipoPersona: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: ''
        },
        {
            text: 'Persona Natural',
            value: 1
        },
        {
            text: 'Persona Juridica',
            value: 2
        }
    ];

    static ubigeoCombo: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        }
    ];

    static ActividadesAnexo1Erika: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Labores de Vigilancia de Área',
            value: 1
        },
        {
            text: 'Caracterización del Área',
            value: 2
        },
        {
            text: 'Inventario de Recursos',
            value: 3
        },
        {
            text: 'Formulación del Plan de Majeto',
            value: 4
        },
        {
            text: 'Infraestructura',
            value: 5
        },
        {
            text: 'Aliados Estratégicos',
            value: 6
        }
    ];

    static EstadosH4Erika: ComboModel2[] = [
        {
            text: '-Seleccionar-',
            value: 0
        },
        {
            text: 'Favorable',
            value: 1
        },
        {
            text: 'Desfavorable',
            value: 2
        },
        {
            text: 'Observado',
            value: 3
        }
    ];

}

export class InformacionSolicitantePFModel {
    constructor(data?: any) {

        if (data) {
            this.idTipoPersona = data.tipoPersona;
            this.nombre = data.nombre;
            this.numero_documento = data.dni;
            this.ruc = data.direccion;
            this.numero_ruc_empresa = data.direccion;
            this.direccion_empresa = data.direccion_empresa;
            this.nro = data.nro;
            this.sector = data.sector;
            this.telFijo = data.telFijo;
            this.telefono = data.telCelular;
            this.correo = data.correo;

            this.id_departamento = data.iddepartamento;
            this.id_provincia = data.idprovincia;
            this.id_distrito = data.iddistrito;
            //******************************** */
            this.rNombre = data.rNombre;
            this.rDni = data.rDni;
            this.rDireccion = data.rDireccion;
            this.rNro = data.rNro;
            this.rSector = data.rSector;
            this.rTelFijo = data.rTelFijo;
            this.rTelCelular = data.rTelCelular;
            this.rCorreo = data.rCorreo;
            this.readonly = data.readonly;
            return;
        }

    }
    idTipoPersona: string = "";
    nombre: string = "";
    numero_documento: string = "";
    ruc: string = "";
    numero_ruc_empresa: string = "";
    nro: string = "";
    sector = "";
    telFijo: string = "";
    telefono: string = "";
    correo: string = "";
    direccion_empresa: string = "";

    id_departamento: number = 0;
    id_provincia: number = 0;
    id_distrito: number = 0;
//*** */
   
    rNombre: string = "";
    rDni: string = "";
    rDireccion: string = "";
    rNro: string = "";
    rSector = "";
    rTelFijo: string = "";
    rTelCelular: string = "";
    rCorreo: string = "";  

    id_departamento_empresa: number = 0;
    id_provincia_empresa: number = 0;
    id_distrito_empresa: number = 0; 

    readonly: boolean = false;
}



export class AreaPFModel {
    constructor(data?: any) {
        if (data) {
            this.predio = data.predio;
            this.area = data.area;
            this.propietario_predio = data.propietario_predio;
            this.num_documento = data.num_documento;
            this.ruc = data.ruc;
            this.ubi_geografica = data.ubi_geografica;
            this.caserio_comunidad = data.caserio_comunidad;
            this.condicionPropietario = data.condicionPropietario;
            this.condicionInversionista = data.condicionInversionista;
            this.fileNamePropietario = data.fileNamePropietario;
            this.fileNameInversionista = data.fileNameInversionista;
            this.condicionNumero = data.condicionNumero;
            this.inv_doc_acredita = data.inv_doc_acredita;
            this.pro_doc_acredita = data.pro_doc_acredita;
            this.tipo = data.tipo
            this.tipoNumero = data.tipoNumero;

            this.id_departamento = data.departamento;
            this.id_provincia = data.distrito;
            this.id_distrito = data.provincia;

            this.readonly = data.readonly;
            return;
        }
    }
    id_departamento: number = 0;
    id_provincia: number = 0;
    id_distrito: number = 0;

    predio: string = "";
    area: any = null;
    propietario_predio: string = "";
    num_documento: string = "";
    ruc: string = "";
    ubi_geografica: string = "";
    caserio_comunidad: string = "";
    condicionPropietario: boolean = false;
    condicionInversionista: boolean = false;
    fileNamePropietario: string = "";
    fileNameInversionista: string = "";
    condicionNumero: string = "";
    inv_doc_acredita: string = "";
    pro_doc_acredita: string = "";
    tipo: string = "";
    tipoNumero: string = "";
    
    readonly: boolean = false;
}

export class DetalleAreaPlantadaModel {
    constructor(data?: any) {
        if (data) {
            this.id_solplantforest_areaplantada = data.id_solplantforest_areaplantada;
            this.id_sistemaplantacion = data.id_sistemaplantacion;
            this.superficie_medida = data.superficie_medida;
            this.superficie_cantidad = data.superficie_cantidad;
            this.fines = data.fines;
            this.especies_establecidas = data.especies_establecidas;
            return;
        }
        else {
            this.id_solplantforest_areaplantada = this.id_solplantforest_areaplantada;
        }
    }

    id_solplantforest_areaplantada: number = 0;
    id_sistemaplantacion: number = 0;
    superficie_medida: string = "";
    superficie_cantidad: any = null;
    fines: any = "";
    especies_establecidas: string = "";
}

export class AreaPlantadaModel {
    constructor(data?: any) {
        if (data) {
            this.areaTotal = data.areaTotal;
            this.mesAnhio = data.mesAnhio;
            this.lstDetalle = data.lstDetalle;
            this.readonly = data.readonly;
            return;
        }
    }
    readonly: boolean = false;
    areaTotal: any = null;
    mesAnhio: string = '';
    lstDetalle: DetalleAreaPlantadaModel[] = [];
}

export class DetalleDetallePlantacion1 {
    constructor(data?: any) {

        if (data) {
            this.idEspecie = data.idEspecie;
            this.id_solplantforest_det = data.id;
            this.espcs_nom_comun = data.espcs_nom_comun;
            this.espcs_nom_cientifico = data.espcs_nom_cientifico;
            this.total_arbol_matas_espcs_existentes = data.total_arbol_matas_espcs_existentes;
            this.produccion_estimada = data.produccion_estimada;
            this.coord_este = data.coord_este;
            this.coord_norte = data.coord_norte;
            this.especie = data.especie;
            this.especie_cient = data.especie_cient;
            this.producion_estimada_und_medida= data.producion_estimada_und_medida;
            this.idVertice = data.idVertice;
            return;
        }
        else {
            this.id_solplantforest_det = this.id_solplantforest_det;//+ 1;//new Date().toISOString();
        }

    }
 
    especie:Especies = new Especies();
    especie_cient:Especies = new Especies();
    idEspecie :number = 0;
    id_solplantforest_det: number = 0;
    espcs_nom_comun: string = "";
    espcs_nom_cientifico: string = "";
    total_arbol_matas_espcs_existentes: number = 0;
    produccion_estimada: number = 0;
    coord_este: any = null;
    coord_norte: any = null;
    idVertice:any = null;
    producion_estimada_und_medida:string="";
}

export class Especies {
    constructor(data?:any){
      if(data){
        this.autor= data.autor;
        this.categoria= data.categoria;
        this.cites= data.autor;
        this.familia= data.familia;
        this.fuente= data.fuente;
        this.idEspecie= data.idEspecie;
        this.idFuenteOficial= data.idFuenteOficial;
        this.nombreCientifico= data.nombreCientifico;
        this.nombreComun= data.nombreComun;
      }
    }
    
    autor: string="";
    categoria:string="";
    cites:string="";
    // codfor: string="";
    // dmc: string="";
    familia:string="";
    fuente: string="";
    habitoCrecimiento: string="";
    idEspecie: number=0;
    idFuenteOficial: number=0;
    nombreCientifico:string="";
    nombreComun: string="";
    nombrecomercial: string="";
    // numero_lista: string="";
    // oficial: string="";
    // sinonimia: string="";
    // tipouso: string="";
    
    }
export class DetallePlantacion1Model {
    constructor(data?: any) {
        if (data) {
            this.alturaPromedio = data.alturaPromedio;
            this.lstDetalle = data.lstDetalle;
            this.readonly = data.readonly;
            return;
        }

    }
    readonly: boolean = false;
    alturaPromedio: any = null;
    lstDetalle: DetalleDetallePlantacion1[] = [];

}

export class DetalleVerticesModel {
    constructor(data?: any) {
        if (data) {
            this.id_bloquesector = data.id;
            this.vertice = data.vertice;
            this.zonaeste = data.este;
            this.zonanorte = data.norte;
            this.observaciones = data.observacion;

            return;
        }
        else {
            this.id_bloquesector = this.id_bloquesector;//+1; //new Date().toISOString();
        }
    }
    id_bloquesector: number = 0;
    vertice: number = 0;
    zonaeste: any = null;
    zonanorte: any = null;
    observaciones: string = "";
}

export class DetalleDetallePlanteacion2model {
    constructor(data?: any) {
        if (data) {
            this.id_solplantforest_coord_det = data.id;
            this.areabloque_unidad = data.areabloque_unidad;
            this.areabloque = data.areabloque;
            this.bloquesector = data.bloquesector;
            this.detalle = data.detalle;

            return;
        }
        else {
            this.id_solplantforest_coord_det = this.id_solplantforest_coord_det;
        }
    }

    id_solplantforest_coord_det: number = 0;
    areabloque_unidad: string = "";
    areabloque: any = null;
    bloquesector:string ="";
    
    detalle: DetalleVerticesModel[] = [];

}

export class DetallePlantacion2Model {
    constructor(data?: any) {
        if (data) {
            this.lstDetalle = data.lstDetalle;
            this.readonly = data.readonly;
            return;
        }
    }
    readonly: boolean = false;
    lstDetalle: DetalleDetallePlanteacion2model[] = [];
}

export class ObservacionesPFModel {
    constructor(data?: any) {
        if (data) {
            this.creado = data.creado;
            this.observacion = data.observacion;
            this.estado = data.estado;
        } else {
            this.creado = new Date();
        }
    }
    estado: string = "";
    observacion: string = "";
    creado: Date;

}

export class PlantacionForestalModel {
    constructor(data?: any) {
        if (data) {
            this.informacionSolicitante = new InformacionSolicitantePFModel(data.informacionSolicitante);
            this.area = new AreaPFModel(data.area);
            this.areaPlantada = new AreaPlantadaModel(data.areaPlantada);
            this.detalleAreaPlanteada1 = new DetallePlantacion1Model(data.detalleAreaPlanteada1);
            this.detalleAreaPlanteada2 = new DetallePlantacion2Model(data.detalleAreaPlanteada2);
            this.id = data.id;
            this.observacion = data.observacion;
            this.observaciones = data.observaciones || [];
            this.creado = data.creado;
            this.estado = data.estado;
            this.detalleAnexo = new SolDetalleAnexo(data.detalleAnexo);
            return
        } else {
            this.id = this.id + 1;//new Date().toISOString();
            this.informacionSolicitante = new InformacionSolicitantePFModel();
            this.area = new AreaPFModel();
            this.areaPlantada = new AreaPlantadaModel();
            this.detalleAreaPlanteada1 = new DetallePlantacion1Model();
            this.detalleAreaPlanteada2 = new DetallePlantacion2Model();
            this.creado = new Date();
            this.observaciones = [];
            this.detalleAnexo = new SolDetalleAnexo()
        }
    }

    id: number = 0;
    observacion: string = "";
    observaciones: ObservacionesPFModel[] = [];
    estado: string = "Pendiente";
    creado: Date;
    informacionSolicitante: InformacionSolicitantePFModel;
    area: AreaPFModel;
    areaPlantada: AreaPlantadaModel;
    detalleAreaPlanteada1: DetallePlantacion1Model;
    detalleAreaPlanteada2: DetallePlantacion2Model;
    detalleAnexo: SolDetalleAnexo;
}




export class SolBandejaPlantacionForestalModel {
    constructor(data?: any) {
        // if (data) {

        // }
        this.id_solplantforest = this.id_solplantforest;
        this.des_estado = this.des_estado;
        this.fechaRegistro = this.fechaRegistro;
        this.id_des_estado = this.id_des_estado;
        this.persona = new PersonaModel();
    }


    id_solplantforest: number = 0;
    des_estado: String = "";
    fechaRegistro: Date = new Date();
    persona: PersonaModel;
    id_des_estado: number= 0;
}

export class PersonaModel {

    idPersona: Number = 0;
    nombres: String = "";
    apellidoPaterno: String = "";
    apellidoMaterno: String = "";
    // comunidad: ComunidadModel;
    // tipoPersona:TipoPersonaModel;
    correo: String = "";
    telefono: String = "";
    ruc: String = "";
    // codInrena:String;    
    desTipoPersona: String = "";
    rasonSocial: String = "";
    representaLegal: String = "";

}


/*************************************************************** */

export class CCEstados {
    static creacionSolicitud: ComboModel2 = { value: -1, text: 'En Creación' };
    static pendienteValidacion: ComboModel2 = { value: 0, text: 'Pendiente' };
    static ValidadoAnexo: ComboModel2 = { value: 1, text: 'Validado' };
    static observadoSolicitud: ComboModel2 = { value: 2, text: 'Observado' };
    static subsanadoSolicitud: ComboModel2 = { value: 3, text: 'Subsanado' };
    static evaluacionSolicitud: ComboModel2 = { value: 4, text: 'Evaluación' };
    static publicacionSolicitud: ComboModel2 = { value: 5, text: 'Publicación' };
    static evaluacionTecnicaSolicitud: ComboModel2 = { value: 6, text: 'Evaluacion Técnica' };
    static rechazarSolicitud: ComboModel2 = { value: 7, text: 'Solicitiud Rechazada' };
    static observadorEvalSolicitud: ComboModel2 = { value: 8, text: 'Observado' };
    static subsanadoEvalSolicitud: ComboModel2 = { value: 9, text: 'Subsanado' };
    static evalAnexoSolicitud: ComboModel2 = { value: 10, text: 'Anexo Evaluado' };
}

export class CCAnexoModel {
    constructor(data?: any) {
        if (data) {
            this.nombre = data.nombre;
            this.estado = data.estado;
            this.estadoId = data.estadoId;
            this.index = data.index;
            this.h2Observacion = data.h2Observacion;
            this.h2Validado = data.h2Validado;
            this.h2EstadoClass = data.h2EstadoClass;
            this.data = data.data;
            this.isShow = data?.isShow;
            return;
        } else {

        }
    }
    nombre: string = "";
    estado: String = CCEstados.pendienteValidacion.text;
    estadoId: number = CCEstados.pendienteValidacion.value;
    index: number = 0;
    h2Observacion: string = "";

    h2Validado?: any = null;

    h2EstadoClass = "text-warning";
    data: any;
    isShow?: boolean = true;
}

export class Anexo1Model {
    constructor(data?: any) {
        if (data) {
            this.zona = data.zona;
            return;
        }
    }
    zona: string = "";
}

export class ContratoConcesionModel {

    constructor(data?: any) {
        if (data) {
            this.id = data.id;
            this.anexo1 = data.anexo1;
            this.anexo2 = data.anexo2;
            this.anexo3 = data.anexo3;
            this.anexo4 = data.anexo4;
            this.anexo5 = data.anexo5;
            this.anexo6 = data.anexo6;
            this.estadoId = data.estadoId;
            this.estado = data.estado;
            this.cntObservacionH2 = data.cntObservacionH2;
            this.recepcionDocumentosFisico = data.recepcionDocumentosFisico;
            this.creado = data.creado;
            this.tipoUsuario = data.tipoUsuario;
            this.resultadoEvalH4 = data.resultadoEvalH4;
            return;
        } else {
            this.id = new Date().toISOString();
        }
    }

    id: string = "";
    estadoId: number = CCEstados.creacionSolicitud.value;
    estado: String = CCEstados.creacionSolicitud.text;
    cntObservacionH2: number = 0;
    recepcionDocumentosFisico: boolean = false;
    creado: Date = new Date();
    tipoUsuario: string = "S";
    resultadoEvalH4: number = 0;
    ObservacionEvaluacionAnexo: string = "";
    ValidadoEvaluacionAnexo: any = "0";
    tabIndex: number = 0;
    anexo1: CCAnexoModel = new CCAnexoModel({
        nombre: 'Información general.',
        estado: CCEstados.creacionSolicitud.text,
        index: 0,
        estadoId: CCEstados.creacionSolicitud.value,
        h2EstadoClass: "text-warning",
        data: new Anexo1Model()
    });
    anexo2: CCAnexoModel = new CCAnexoModel({
        nombre: 'Capacidad técnica.',
        estado: CCEstados.creacionSolicitud.text,
        index: 1,
        estadoId: CCEstados.creacionSolicitud.value,
        h2EstadoClass: "text-warning"
    });
    anexo3: CCAnexoModel = new CCAnexoModel({
        nombre: 'Declaración jurada de ingresos, cuentas y bienes.',
        estado: CCEstados.creacionSolicitud.text,
        index: 2,
        estadoId: CCEstados.creacionSolicitud.value,
        h2EstadoClass: "text-warning"
    });
    anexo4: CCAnexoModel = new CCAnexoModel({
        nombre: 'Formato para presentar el estudio técnico.',
        estado: CCEstados.creacionSolicitud.text,
        index: 3,
        estadoId: CCEstados.creacionSolicitud.value,
        h2EstadoClass: "text-warning",
        isShow: false
    });
    anexo5: CCAnexoModel = new CCAnexoModel({
        nombre: 'Formato para la publicación.',
        estado: CCEstados.creacionSolicitud.text,
        index: 4,
        estadoId: CCEstados.creacionSolicitud.value,
        h2EstadoClass: "text-warning"
    });
    anexo6: CCAnexoModel = new CCAnexoModel({
        nombre: 'Formato para la elaboración de propuestas.',
        estado: CCEstados.creacionSolicitud.text,
        index: 5,
        estadoId: CCEstados.creacionSolicitud.value,
        h2EstadoClass: "text-warning"
    });

}



export class SolDetalleAnexo {
    constructor(data?: any) {
        if (data) {

            this.id_solplantforest = data.id_solplantforest;
            this.readonly = data.readonly;
            return;
        } else {
            this.id_solplantforest = 0;//new Date().toISOString();
        }
    }
    readonly: boolean = false;
    id_solplantforest: number = 0;
    lsDetalleAnexo: DetalleAnexo[] = [];
}

export class DetalleAnexo {
    constructor(data?: any) {
        if (data) {
            this.id_solplantacionforest_archivo = data.id_solplantacionforest_archivo;
            //this.id_solplantforest = data.id_solplantforest;
            this.id_tipodetalleanexo = data.id_tipodetalleanexo;
            this.tipodetalleanexo = data.tipodetalleanexo;
            this.id_tipodetalleseccion = data.id_tipodetalleseccion;
            this.estado = data.estado;
            this.descripcion = data.descripcion;
            this.fecha_registro = data.fecha_registro;
            this.usuario_registro = data.usuario_registro;
            this.url = data.url;
            this.readonly = data.readonly;
            return;
        } else {
            this.id_solplantacionforest_archivo = 0;//new Date().toISOString();
        }
    }
    readonly: boolean = false;
    id_solplantacionforest_archivo: number;
    // id_solplantforest:number =0;
    id_archivo: number = 0;
    id_tipodetalleseccion: number = 0;
    id_tipodetalleanexo: number = 0;
    tipodetalleanexo: string = "";
    descripcion: string = "";
    estado: string = "";
    id_usuario_registro: number = 0;
    usuario_registro: string = "";
    fecha_registro: Date = new Date();
    url: string = "";
}

/************************************************************** */