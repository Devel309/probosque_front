export abstract class CodigosDEMAC {

  static readonly CODIGO_PROCESO: string = "DEMAC";
  static readonly ID_TIPO_PLAN: number = 14;
  static readonly TAB_1: string = "DEMAC1";
  static readonly TAB_1_1: string = "DEMAC1_SS1";
  static readonly TAB_1_2: string = "DEMAC1_SS2";
  static readonly TAB_1_3: string = "DEMAC1_SS3";
  static readonly TAB_1_4: string = "DEMAC1_SS4";
  static readonly TAB_2: string = "DEMAC2";
  static readonly TAB_2_1: string = "DEMAC2_SS1";
  static readonly TAB_3: string = "DEMAC3";
  static readonly TAB_4: string = "DEMAC4";
  static readonly TAB_5: string = "DEMAC5";
  static readonly TAB_6: string = "DEMAC6";
  static readonly TAB_6_1: string = "DEMAC6_SS1";
  static readonly TAB_7: string = "DEMAC7";
  static readonly TAB_7_1: string = "DEMAC7_SS1";
  static readonly TAB_8: string = "DEMAC8";
  static readonly TAB_9: string = "DEMAC9";
  static readonly TAB_10: string = "DEMAC10";
  static readonly TAB_11: string = "DEMAC11";
  static readonly TAB_11_1: string = "DEMAC11_SS1";
  static readonly TAB_11_2: string = "DEMAC11_SS2";
  static readonly TAB_11_3: string = "DEMAC11_SS3";
  static readonly TAB_12: string = "DEMAC12";
  static readonly TAB_12_1: string = "DEMAC12_SS1";
  static readonly TAB_12_2: string = "DEMAC12_SS2";
  static readonly TAB_12_3: string = "DEMAC12_SS3";
  static readonly TAB_12_4: string = "DEMAC12_SS4";
}
