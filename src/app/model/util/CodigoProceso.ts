export abstract class CodigoProceso{
  static readonly PLAN_GENERAL: string = "PGMFA";
  static readonly PLAN_GENERAL_MANEJO_FORESTAL: string = "PGMF";
  static readonly PLAN_OPERATIVO_PO: string = "PO";

  static readonly PLAN_OPERATIVO: string = "POAC";
  static readonly PLAN_OPERATIVO_CONCESIONES: string = "POCC";

  static readonly SOLICITUD_PERMISOS_FORESTALES: string = "PFCR";

  static readonly PLAN_MANEJO_FORESTAL_INTERMEDIO : string = "PMFIC";
  static readonly PLAN_OPERATIVO_DEMAP: string = "DEMAP";
  
  static readonly PLAN_OPERATIVO_PMFI_CNCC        : string = "POPAC";

  static readonly PLAN_OPERATIVO_DEMACU: string = "DEMACU";
  static readonly PLAN_OPERATIVO_PMFIP: string = "PMFIP";
  static readonly PLAN_OPERATIVO_POPMIFP: string = "POPMIFP";
  static readonly PLAN_OPERATIVO_DEMAC: string = "DEMAC";


}
