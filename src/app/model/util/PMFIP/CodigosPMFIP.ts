export abstract class CodigosPMFIP {

  static readonly CODIGO_PROCESO: string = "PMFIP";
  static readonly ID_TIPO_PLAN: number = 12;
  static readonly TAB_1: string = "PMFIP1";
  static readonly TAB_1_1: string = "PMFIP1_SS1";
  static readonly TAB_1_2: string = "PMFIP1_SS2";
  static readonly TAB_1_3: string = "PMFIP1_SS3";
  static readonly TAB_1_4: string = "PMFIP1_SS4";
  static readonly TAB_2: string = "PMFIP2";
  static readonly TAB_2_1: string = "PMFIP2_SS1";
  static readonly TAB_3: string = "PMFIP3";
  static readonly TAB_4: string = "PMFIP4";
  static readonly TAB_5: string = "PMFIP5";
  static readonly TAB_6: string = "PMFIP6";
  static readonly TAB_6_1: string = "PMFIP6_SS1";
  static readonly TAB_7: string = "PMFIP7";
  static readonly TAB_7_1: string = "PMFIP7_SS1";
  static readonly TAB_8: string = "PMFIP8";
  static readonly TAB_9: string = "PMFIP9";
  static readonly TAB_10: string = "PMFIP10";
  static readonly TAB_11: string = "PMFIP11";
  static readonly TAB_11_1: string = "PMFIP11_SS1";
  static readonly TAB_11_2: string = "PMFIP11_SS2";
  static readonly TAB_11_3: string = "PMFIP11_SS3";
  static readonly TAB_12: string = "PMFIP12";
  static readonly TAB_12_1: string = "PMFIP12_SS1";
  static readonly TAB_12_2: string = "PMFIP12_SS2";
  static readonly TAB_12_3: string = "PMFIP12_SS3";
  static readonly TAB_12_4: string = "PMFIP12_SS4";
}
