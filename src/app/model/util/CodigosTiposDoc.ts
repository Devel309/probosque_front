export abstract class CodigosTiposDoc {
    static readonly DNI: string = "TDOCDNI";
    static readonly RUC: string = "TDOCRUC";
    static readonly CARNET_EXT: string = "TDOCEXTR";
}