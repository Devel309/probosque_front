import { ValidationErrors } from '@angular/forms';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import {EvaluacionArchivoModel} from '../Comun/EvaluacionArchivoModel';
import {CodigoEstadoEvaluacion} from './CodigoEstadoEvaluacion';
import {LineamientoInnerModel} from '../Comun/LineamientoInnerModel';
import {EvaluacionPermisoForestalModel} from '../Comun/EvaluacionPermisoForestalModel';
import { CodigoPermisoForestal } from './CodigoPermisoForestal';
import { isNullOrEmpty } from '@shared';

  export abstract class EvaluacionUtils {

    public static validar(evaluaciones : EvaluacionArchivoModel[]): boolean  {

      for (var item of evaluaciones) {
        if( isNullOrEmpty(item.conforme)){
          return false;
        }
        if(item.conforme == CodigoEstadoEvaluacion.OBSERVADO && isNullOrEmpty(item.observacion)){
          return false;
        }
      }

      return true;
    }

    public static validarPermisoForestal(evaluaciones : EvaluacionPermisoForestalModel[]): boolean  {


      for (var item of evaluaciones) {
        if(item.conforme == CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO && isNullOrEmpty(item.observacion)){
          return false;
        }
      }

      return true;
    }

    public static validarPermisoForestalEvaluacion(evaluaciones : EvaluacionPermisoForestalModel[]): boolean  {


      for (var item of evaluaciones) {
        if(item.conforme == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE && isNullOrEmpty(item.observacion)){
          return false;
        }
      }

      return true;
    }

    public static validarLineamientoModel(evaluaciones : LineamientoInnerModel[]): boolean  {


      for (var item of evaluaciones) {
        if(item.conforme == false && isNullOrEmpty(item.observacion)){
          return false;
        }
      }

      return true;
    }

}
