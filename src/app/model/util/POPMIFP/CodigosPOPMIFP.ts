export abstract class CodigosPOPMIFP {

  static readonly CODIGO_PROCESO: string = "POPMIFP";
  static readonly ID_TIPO_PLAN: number = 13;
  static readonly TAB_1: string = "POPMIFP1";
  static readonly TAB_1_1: string = "POPMIFP1_SS1";
  static readonly TAB_1_2: string = "POPMIFP1_SS2";
  static readonly TAB_1_3: string = "POPMIFP1_SS3";
  static readonly TAB_1_4: string = "POPMIFP1_SS4";
  static readonly TAB_2: string = "POPMIFP2";
  static readonly TAB_2_1: string = "POPMIFP2_SS1";
  static readonly TAB_3: string = "POPMIFP3";
  static readonly TAB_4: string = "POPMIFP4";
  static readonly TAB_5: string = "POPMIFP5";
  static readonly TAB_6: string = "POPMIFP6";
  static readonly TAB_6_1: string = "POPMIFP6_SS1";
  static readonly TAB_7: string = "POPMIFP7";
  static readonly TAB_7_1: string = "POPMIFP7_SS1";
  static readonly TAB_8: string = "POPMIFP8";
  static readonly TAB_9: string = "POPMIFP9";
  static readonly TAB_10: string = "POPMIFP10";
  static readonly TAB_11: string = "POPMIFP11";
  static readonly TAB_11_1: string = "POPMIFP11_SS1";
  static readonly TAB_11_2: string = "POPMIFP11_SS2";
  static readonly TAB_11_3: string = "POPMIFP11_SS3";
  static readonly TAB_12: string = "POPMIFP12";
  static readonly TAB_12_1: string = "POPMIFP12_SS1";
  static readonly TAB_12_2: string = "POPMIFP12_SS2";
  static readonly TAB_12_3: string = "POPMIFP12_SS3";
  static readonly TAB_12_4: string = "POPMIFP12_SS4";
}
