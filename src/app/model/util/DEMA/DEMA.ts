export abstract class CodigosDEMA {

  static readonly CODIGO_PROCESO: string = "DEMA";

  /*static readonly TAB_1: string = "CPMINFG";
  static readonly ACORDEON_1_1: string = "CPMINFGAC";

  static readonly TAB_2: string = "CPMZONO";
  static readonly ACORDEON_2_1: string = "CPMZONOAC";

  static readonly TAB_3: string = "CPMRFM";
  static readonly ACORDEON_3_1: string = "CPMRFMAC";

  static readonly TAB_4: string = "CPMRFN";
  static readonly ACORDEON_4_1: string = "CPMRFNAC";

  static readonly TAB_5: string = "CPMSMLS";
  static readonly ACORDEON_5_1: string = "CPMSMLSAC";


  static readonly TAB_6: string = "CPMMPU";
  static readonly ACORDEON_6_1: string = "CPMMPUAC";

  static readonly TAB_7: string = "CPMDAAE";
  static readonly ACORDEON_7_1: string = "CPMDAAEAC";

  static readonly TAB_8: string = "CPMDAA2";
  static readonly ACORDEON_8_1: string = "CPMDAA2AC";

  static readonly TAB_9: string = "CPMCRA";
  static readonly ACORDEON_9_1: string = "CPMCRAAC";

  static readonly TAB_10: string = "CPMA4";
  static readonly ACORDEON_10_1: string = "CPMA4AC";

  static readonly TAB_11: string = "CPMCARDOC";
  static readonly ACORDEON_11_1: string = "CPMCARDOCAC";*/



  static readonly TAB_1: string = "DEMAIG";
  static readonly ACORDEON_1_1: string = "DEMAIGDC";
  static readonly ACORDEON_1_2: string = "DEMAIGDD";
  static readonly ACORDEON_1_3: string = "DEMAIGDRPAAF";

  static readonly TAB_2: string = "DEMAZOIA";
  static readonly ACORDEON_2_1: string = "DEMAZOIAZO";

  static readonly TAB_3: string = "DEMARFM";
  static readonly ACORDEON_3_1: string = "DEMARFMARFM";
  static readonly ACORDEON_3_2: string = "DEMARFMRES";

  static readonly TAB_4: string = "DEMARFNM";
  static readonly ACORDEON_4_1: string = "DEMARFNMARFNM";
  static readonly ACORDEON_4_2: string = "DEMARFNMRES";

  static readonly TAB_5: string = "DEMASMLS";
  static readonly ACORDEON_5_1: string = "DEMASMLS";


  static readonly TAB_6: string = "DEMAMPUMF";
  static readonly ACORDEON_6_1: string = "DEMAMPUMF";

  static readonly TAB_7: string = "DEMAAAE";
  static readonly ACORDEON_7_1: string = "DEMAAAE";

  static readonly TAB_8: string = "DEMAIAN";
  static readonly ACORDEON_8_1: string = "DEMAIAN";

  static readonly TAB_9: string = "DEMACA";
  static readonly ACORDEON_9_1: string = "DEMACA";

  static readonly TAB_10: string = "DEMAA";
  static readonly ACORDEON_10_1: string = "DEMAA1";
  static readonly ACORDEON_10_2: string = "DEMAA2";
  static readonly ACORDEON_10_3: string = "DEMAA3";
  static readonly ACORDEON_10_4: string = "DEMAA4";

  static readonly TAB_11: string = "DEMACED";
  static readonly ACORDEON_11_1: string = "DEMACED";
}
