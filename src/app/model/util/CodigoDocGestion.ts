export abstract class CodigoDocGestion {
  static readonly TH_PROCED_ABREV: string = "TPMTHPA";
  static readonly PFDM_CONCE_DIRECTA: string = "TPMPFDM";
  static readonly PERMISO_FORESTAL: string = "TPMPF";
}

export const FILTRO1_TIPO_DOC_GESTION = [CodigoDocGestion.TH_PROCED_ABREV, CodigoDocGestion.PFDM_CONCE_DIRECTA];
