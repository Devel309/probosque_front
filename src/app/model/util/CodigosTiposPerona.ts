export abstract class CodigosTiposPersona {
    static readonly NATURAL: string = "TPERNATU";
    static readonly JURIDICA: string = "TPERJURI";
}