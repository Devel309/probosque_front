export abstract class CodigosPermisosForestales {
  static readonly TITULO_PROPIEDAD: string = 'PFCRTP';
  static readonly ACTA_ASAMBLEA_RRLL: string = 'PFCRAR';
  static readonly LIMITES_COLINDANCIAS: string = 'PFCRLC';
  static readonly COMROBANTE_PAGO: string = 'PFCRCP';
  static readonly AREA_COMUNIDAD: string = 'PFCRAC';
  static readonly ACTA_ASAMBLEA_ACUERDO: string = 'PFCRAA';
  static readonly CONTRATO_REGENTE_FORESTAL: string = 'PFCRCR';
  static readonly ADJUNTAR_ARRF: string = 'PFCRRF';
  static readonly DECLARACION_JURADA: string = 'PFCRDJ';
  static readonly CONTRATO_TERCEROS: string = 'PFCRCT';
  static readonly ACTA_ASAMBLEA: string = 'PFCRAS';
  static readonly OTROS: string = 'PFCROD';
  static readonly ADJUNTAR_DECLARACION_JURADA_FIRMADA: any = 'PFCRDJFIR';
  static readonly SOLICITUD_RECONOCIMIENTO: any = 'PFCRSR';
  static readonly SOLICITUD_AMPLIACION: any = 'PFCRSA';
  static readonly SOLICITUD_TITULACION: any = 'PFCRST';
  static readonly OPINION_TECNICO_SOL_RECONOCIMEINTO: any = 'PFCROTSR';
  static readonly OPINION_TECNICO_SOL_AMPLIACION: any = 'PFCROTSA';
  static readonly OPINION_TECNICO_SOL_TITULACION: any = 'PFCROTST';
  static readonly RECIBO_RESIDENCIA: any = 'PFCRRC';
}
