export abstract class TipoPlanManejo {
    static readonly PGMF_CONCESION_MADERABLE: string = "TPMPGMF";
    static readonly PGMFI: string = "TPMPGMFI";
    static readonly PGMF_PERMISO_APROVE: string = "TPMPGMFA";
    static readonly PO_CONCESION_MAD: string = "TPMPOCC";
    static readonly PO_ANUAL_COMERCIA: string = "TPMPOAC"; 
    static readonly PO_PERMISO_APROVE: string = "TPMPOC";
    static readonly DEMA_PERMISO_APROVE: string = "TPMDEMA";
    static readonly PMFI_PERMISO_APROVE: string = "TPMPMFIC";
    static readonly PMFI_CONCESION_PFDM: string = "TPMPMFI";
    static readonly DEMA_CONCESION_PFDM: string = "TPMDEMAP";
    static readonly DEMA_CONTRATO_CESION: string = "TPMDEMACU";
    static readonly PFDM_CONCESION_DIRECTA: string = "TPMPFDM";
    static readonly PERMISO_FORESTAL: string = "TPMPF";
    static readonly TH_PROCED_ABREV: string = "TPMTHPA";
    static readonly PLANTACIONES_FORESTALES: string = "TPMPLANF";

    static readonly PMFIC: string = "PMFIC";
    static readonly DEMA: string = "DEMA";
    static readonly PMFI: string = "PMFI";
    static readonly PGMFA: string = "PGMFA";
    static readonly POAC: string = "POAC";
}