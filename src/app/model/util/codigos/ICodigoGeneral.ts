export interface ICodigoGeneral {
    CODIGO_PROCESO: string;
    ID_TIPO_PLAN: number;
    TAB_1: string;
    TAB_1_1: string;
    TAB_1_2: string;
    TAB_1_3: string;
    TAB_1_4: string;
    TAB_2: string;
    TAB_2_1: string;
    TAB_3: string;
    TAB_4: string;
    TAB_5: string;
    TAB_6: string;
    TAB_6_1: string;
    TAB_7: string;
    TAB_7_1: string;
    TAB_8: string;
    TAB_9: string;
    TAB_10: string;
    TAB_11: string;
    TAB_11_1: string;
    TAB_11_2: string;
    TAB_11_3: string;
    TAB_12: string;
    TAB_12_1: string;
    TAB_12_2: string;
    TAB_12_3: string;
    TAB_12_4: string;
  
  }