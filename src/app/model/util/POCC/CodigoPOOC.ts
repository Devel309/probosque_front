export abstract class CodigoPOOC {
  static readonly CODIGO_PROCESO: string = "POCC";

  static readonly TAB_2: string = CodigoPOOC.CODIGO_PROCESO + "RARPOA"; 
  static readonly ACORDEON_2_1: string = CodigoPOOC.TAB_2+"RARPO";
  static readonly ACORDEON_2_2: string = CodigoPOOC.TAB_2+"RRPOE";

  static readonly TAB_3: string = CodigoPOOC.CODIGO_PROCESO + "OBJ";
  static readonly ACORDEON_3_1: string = CodigoPOOC.TAB_3 + "OE";

  static readonly TAB_4: string = CodigoPOOC.CODIGO_PROCESO + "IBPC";
  static readonly ACORDEON_4_1: string = CodigoPOOC.TAB_4 + "UEPC";
  static readonly ACORDEON_4_2: string = CodigoPOOC.TAB_4 + "TIBO";
  static readonly ACORDEON_4_3: string = CodigoPOOC.TAB_4 + "ACCE";

  static readonly TAB_5: string = CodigoPOOC.CODIGO_PROCESO + "OPCPUMF";
  static readonly ACORDEON_5_1: string = CodigoPOOC.TAB_5 + "CO";//POCCOPCPUMFCO   POCCOPCPUMFCO
  static readonly ACORDEON_5_2: string = CodigoPOOC.TAB_5 + "FC";
  static readonly ACORDEON_5_3: string = CodigoPOOC.TAB_5 + "PV";//POCCOPCPUMFPV   POCCOPCPUMFPV

  static readonly TAB_6: string = CodigoPOOC.CODIGO_PROCESO + "AAPRO";
  static readonly ACORDEON_6_1: string = CodigoPOOC.TAB_6 + "CC";
  static readonly ACORDEON_6_1_1: string = CodigoPOOC.ACORDEON_6_1 + "ME";
  static readonly ACORDEON_6_1_2: string = CodigoPOOC.ACORDEON_6_1 + "DD";
  static readonly ACORDEON_6_1_3: string = CodigoPOOC.ACORDEON_6_1 + "LE";
  static readonly ACORDEON_6_1_4: string = CodigoPOOC.ACORDEON_6_1 + "VI";
  static readonly ACORDEON_6_1_5: string = CodigoPOOC.ACORDEON_6_1 + "ML";
  static readonly ACORDEON_6_1_6: string = CodigoPOOC.ACORDEON_6_1 + "RR";
  static readonly ACORDEON_6_1_7: string = CodigoPOOC.ACORDEON_6_1 + "AR";
  static readonly ACORDEON_6_2: string = CodigoPOOC.TAB_6 + "VC";
  static readonly ACORDEON_6_3: string = CodigoPOOC.TAB_6 + "OA";

  static readonly TAB_7: string = CodigoPOOC.CODIGO_PROCESO + "ASILV";
  static readonly ACORDEON_7_1: string = CodigoPOOC.TAB_7 + "NIS";
  static readonly ACORDEON_7_2: string = CodigoPOOC.TAB_7 + "TS";
  static readonly ACORDEON_7_3: string = CodigoPOOC.TAB_7 + "EAD";

  static readonly TAB_8: string = CodigoPOOC.CODIGO_PROCESO + "EIA";
  static readonly ACORDEON_8_0: string = CodigoPOOC.TAB_8 + "EIA";
  static readonly ACORDEON_8_1: string = CodigoPOOC.TAB_8 + "IIA";
  static readonly ACORDEON_8_2: string = CodigoPOOC.TAB_8 + "PAPC";
  static readonly ACORDEON_8_3: string = CodigoPOOC.TAB_8 + "PVSA";
  static readonly ACORDEON_8_4: string = CodigoPOOC.TAB_8 + "PCA";

  static readonly TAB_9: string = CodigoPOOC.CODIGO_PROCESO + "MON";

  static readonly TAB_11: string = CodigoPOOC.CODIGO_PROCESO + "CAPA";
}

export abstract class CodigoPOCCTabs {
  static readonly CODIGO_TAB1: string = "POCC1";
  static readonly CODIGO_TAB2: string = "POCC2";
  static readonly CODIGO_TAB3: string = "POCC3";
  static readonly CODIGO_TAB4: string = "POCC4";
  static readonly CODIGO_TAB5: string = "POCC5";
  static readonly CODIGO_TAB6: string = "POCC6";
  static readonly CODIGO_TAB6_1: string ="POCC6_1" 
  static readonly CODIGO_TAB6_1_6: string ="POCC6_1_6"
  static readonly CODIGO_TAB6_1_7: string ="POCC6_1_7"
  static readonly CODIGO_TAB6_3: string ="POCC6_3"
  static readonly CODIGO_TAB6_7_1: string = "POCC7_1";
  static readonly CODIGO_TAB6_7_2: string = "POCC7_2";
  static readonly CODIGO_TAB6_7_3: string = "POCC7_3";
  static readonly CODIGO_TAB8: string = "POCC8";
  static readonly CODIGO_TAB9: string = "POCC9";
  static readonly CODIGO_TAB10: string = "POCC10";
  static readonly CODIGO_TAB11: string = "POCC11";

}