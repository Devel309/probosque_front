export interface FileModel {
  file:File | null;
  nombreFile:string;
  tipoArchivo:string;
  descripcion:string;
  codigo:Number;
  inServer:Boolean;
  idLayer:string;
  groupId:String;
  idGroupLayer:Number;
  color:String;
  geoJson: {};
  id:Number;
  annex:Boolean;
  url: string;
  overlap:Boolean;
  justificacion?: string | null | undefined;
  service:Boolean;
}