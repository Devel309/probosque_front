export abstract class CodigoEstadoPagos{
    static readonly BORRADOR: string = 'PAGOBOR';
    static readonly PENDIENTE: string = "PAGOPEND"; 
    static readonly PAGADO: string = "PAGOPAG"; 
    static readonly EXTORNADO: string = "PAGOEXTOR";
    static readonly PAGO_OBSERVADO: string = "PAGOOBS"; 
    static readonly PAGO_VALIDO: string = "PAGOVALD";
    static readonly APROBADO: string = 'EPLMAPROB';
  }
  