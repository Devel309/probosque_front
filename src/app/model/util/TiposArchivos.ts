export abstract class TiposArchivos {
    //Contrato
    static readonly LEGAL: string = "TDOCLIRL";
    static readonly GARANTIA: string = "TDOCGFC";
    static readonly BIENES: string = "TDOCBIEM";
    static readonly FIRMA_ARFFS: string = "TDOCFARF";
    static readonly FIRMA_POSTULANTE: string = "TDOCFPOS";
    static readonly CONTRATO_PLANTILLA: string = "TDOCPLCONT";
    //Fiscalizacion
    static readonly FISCALIZACION: string = "TDOCFISCAL";
    static readonly FISCA_OTROS: string = "TDOCFOTROS";
    //CONSECION DE OPOSICION
    static readonly OPOSICION: string = "TDOCOPO";
    static readonly RESP_OPOSICION: string = "TDOCRESPOPO";
    static readonly OPOS_TIPO_DOC: string = "OPOTIPODOC";
    //EVALUACION OTORGAMIENTO PERMISO
    static readonly EOP_APROBADO: string = "TDOCRESOLAPROB";
    static readonly EOP_DESAPROBADO: string = "TDOCRESOLDESPROB";
    static readonly EOP_PERMISO: string = "TDOCRESOLFPERM";

    //TAB INFORMACION BASICA
    static readonly TDOCGDB: string = "TDOCGDB";

    //TAB MANEJO FORESTAL
    static readonly TDOCTABLA692: string = "TDOCTSILCULT";

    //TAB ANEXOS - CONCESION MANEJO FORESTAL
    static readonly TDOCSFMAPGE: string = "TDOCSFMAPGE";
    static readonly TDOCEVIMAPGE: string = "TDOCEVIMAPGE";
    static readonly TDOCSFMAPORDF: string = "TDOCSFMAPORDF";
    static readonly TDOCEVIMAPORDF: string = "TDOCEVIMAPORDF";
    static readonly TDOCSFMAPDIVADM: string = "TDOCSFMAPDIVADM";
    static readonly TDOCEVIMAPDIVADM: string = "TDOCEVIMAPDIVADM";
}