export abstract class CodigosDEMAP {

  static readonly CODIGO_PROCESO: string = "DEMAP";
  static readonly ID_TIPO_PLAN: number = 8;
  static readonly TAB_1: string = "DEMAP1";
  static readonly TAB_1_1: string = "DEMAP1_SS1";
  static readonly TAB_1_2: string = "DEMAP1_SS2";
  static readonly TAB_1_3: string = "DEMAP1_SS3";
  static readonly TAB_1_4: string = "DEMAP1_SS4";
  static readonly TAB_2: string = "DEMAP2";
  static readonly TAB_2_1: string = "DEMAP2_SS1";
  static readonly TAB_3: string = "DEMAP3";
  static readonly TAB_4: string = "DEMAP4";
  static readonly TAB_5: string = "DEMAP5";
  static readonly TAB_6: string = "DEMAP6";
  static readonly TAB_6_1: string = "DEMAP6_SS1";
  static readonly TAB_7: string = "DEMAP7";
  static readonly TAB_7_1: string = "DEMAP7_SS1";
  static readonly TAB_8: string = "DEMAP8";
  static readonly TAB_9: string = "DEMAP9";
  static readonly TAB_10: string = "DEMAP10";
  static readonly TAB_11: string = "DEMAP11";
  static readonly TAB_11_1: string = "DEMAP11_SS1";
  static readonly TAB_11_2: string = "DEMAP11_SS2";
  static readonly TAB_11_3: string = "DEMAP11_SS3";
  static readonly TAB_12: string = "DEMAP12";
  static readonly TAB_12_1: string = "DEMAP12_SS1";
  static readonly TAB_12_2: string = "DEMAP12_SS2";
  static readonly TAB_12_3: string = "DEMAP12_SS3";
  static readonly TAB_12_4: string = "DEMAP12_SS4";
}
