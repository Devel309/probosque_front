export abstract class CodigosPFDM {
  static readonly CODIGO_PROCESO: string = "PFDM";
  static readonly TAB_1: string = "PFDMA1";
  static readonly TAB_1_1: string = "PFDMA1_SS1";
  static readonly TAB_1_2: string = "PFDMA1_SS2";
  static readonly TAB_1_3: string = "PFDMA1_SS3";
  static readonly TAB_1_4: string = "PFDMA1_SS4";
  static readonly TAB_1_5: string = "PFDMA1_SS5";
  static readonly TAB_1_6: string = "PFDMA1_SS6";
  static readonly TAB_1_7: string = "PFDMA1_SS7";
  static readonly TAB_2: string = "PFDMA2";
  static readonly TAB_2_1: string = "PFDMA2_SS1";
  static readonly TAB_2_2: string = "PFDMA2_SS2";
  static readonly TAB_2_3: string = "PFDMA2_SS3";
  static readonly TAB_3: string = "PFDMA3";
  static readonly TAB_3_1: string = "PFDMA3_SS1";
  static readonly TAB_4: string = "PFDMA4";
  static readonly TAB_4_1: string = "PFDMA4_SS1";
  static readonly TAB_4_2: string = "PFDMA4_SS2";
  static readonly TAB_4_3: string = "PFDMA4_SS3";
  static readonly TAB_4_3_2: string = "PFDMA4_SS3_2";

  static readonly TAB_5: string = "PFDMA5";
  static readonly TAB_5_1: string = "PFDMA5_SS1";
  static readonly TAB_6: string = "PFDMA6";
  static readonly TAB_6_1: string = "PFDMA6_SS1";
  static readonly TAB_6_2: string = "PFDMA6_SS2";
  static readonly TAB_6_3: string = "PFDMA6_SS3";
  static readonly TAB_6_4: string = "PFDMA6_SS4";
  static readonly TAB_6_5: string = "PFDMA6_SS5";
  static readonly TAB_6_6: string = "PFDMA6_SS6";
  static readonly TAB_6_7: string = "PFDMA6_SS7";
  static readonly TAB_6_8: string = "PFDMA6_SS8";
  static readonly TAB_7: string = "PFDMA7";
  static readonly TAB_7_1: string = "PFDMA7_SSS1";
  static readonly TAB_8: string = "PFDMA8";
  static readonly TAB_8_1: string = "PFDMA8_SSS1";
}
