export abstract class Mensajes{
  //static readonly MSJ_EVALUACIONES: string = "El Campo Observación es Obligatorio.";

  static readonly MSJ_EVALUACIONES: string = "Debe seleccionar una opción (Conforme, Observado). \n" +
                                             "Al seleccionar observado El Campo Observación es Obligatorio. ";

  static readonly MSJ_SIN_REGISTROS: string = "No se encontraron registros.";

  static readonly MSJ_ERROR_CATCH: string = "Ocurrió un problema, intente nuevamente.";

  // ARCHIVOS ADJUNTOS
  static readonly MSJ_REGISTRO_ARCHIVO: string = "Se registró el archivo correctamente.";
  static readonly MSJ_ELIMINO_ARCHIVO: string = "Se eliminó el archivo correctamente.";

  // INFORMACIÓN
  static readonly GUADAR_DATA_LOCAL: string = "(Los cambios locales se guardarán pulsando el botón 'Guardar'.)";

}
