export abstract class CodigoEstadoPlanManejo {
  static readonly BORRADOR: string = 'EPLMBOR';
  static readonly REGISTRADO: string = 'EPLMREG';
  static readonly COMPLETADO: string = 'EPLMCOMP';
  static readonly PRESENTADO: string = 'EPLMPRES';
  static readonly OBSERVADO_MESA_DE_PARTES: string = 'EPLMOBSM';
  static readonly RECIBIDO: string = 'EPLMRECI';
  static readonly BORRADOR_NO_PRESENTADO: string = 'EPLMBORN';
  static readonly EN_EVALUACION: string = 'EPLMEEVAL';
  static readonly APROBADO: string = 'EPLMAPROB';
  static readonly BORRADOR_DENEGADO: string = 'EPLMBORD';
  static readonly OBSERVADO: string = 'EPLMOBSG';
  static readonly APROBADO_NOTIFICADO: string = 'EPLMAPRON';
  static readonly IMPUGNADO_SAN: string = 'EPLMIMP';
  static readonly PROCEDE: string = 'EPLMPROC';
  static readonly NO_PROCEDE: string = 'EPLMNOPROC';
  static readonly FAVORABLE: string = 'EPLMFAVO';
  static readonly DESFAVORABLE: string = 'EPLMDESFAVO';
  static readonly EVALUACION_OBSERVADA: string = 'EEVAOBS';
  static readonly FORMULADO: string = 'EPLMFORM';
}

export abstract class CodigoEstadoMesaPartes {
  static readonly PRESENTADOMP: string = 'EPLMPRES';

  static readonly COMPLETADOMP: string = 'EMDCOMP';
  static readonly OBSERVADOMP: string = 'EMDOBS';
  static readonly NOPRESENTADOMP: string = 'EMDNPRE';
  static readonly PRESENTADOSUBSANADOMP: string = 'EMDPRSU';
  static readonly NO_PRESENTADO: string = 'EMDNPRE';
  static readonly PRESENTADOMDP: string = 'EMDPRES';

  static readonly COMPLETADO_MED_CORREC  : string = 'EMCOCOMP';
  static readonly COMPLETADO_PREREQUISITO : string = 'EPRECOMP';
  static readonly OBSERVADO_PREREQUISITO: string = 'EPREOBS';
  static readonly PRESENTADO_PREREQUISITO: string = 'EPREPRES';

  static readonly EN_EVALUACION: string = 'EEVAEVAL';
  static readonly EN_EVALUACIONMDP: string = 'EMDEVAL';
  static readonly APROBADO: string = 'EPLMAPROB';
}
