export abstract class CodigosPOAC {

  static readonly CODIGO_PROCESO       : string = "POAC";

  static readonly POAC_TAB_4       : string = "POACCFFMOPUMF";
  static readonly POAC_TAB_4_1     : string = CodigosPOAC.POAC_TAB_4 + "CO";//4.1. Categorías de Ordenamiento

  static readonly POAC_TAB_4_2     : string = CodigosPOAC.POAC_TAB_4 + "DAB";//4.2. División Administrativa del Bosque

  static readonly POAC_TAB_4_2_1   : string = CodigosPOAC.POAC_TAB_4_2 + "BQ";//4.2.1. Bloques Quinquenales(opcional)
  static readonly POAC_TAB_4_2_2   : string = CodigosPOAC.POAC_TAB_4_2 + "PC";//4.2.2. Parcelas de Corta (PC)
  static readonly POAC_TAB_4_2_3   : string = CodigosPOAC.POAC_TAB_4_2 + "FC";//4.2.3. Frentes de Corta


  static readonly POAC_TAB_4_3     : string = CodigosPOAC.POAC_TAB_4 + "PV";//4.3. Protección y Vigilancia
  static readonly POAC_TAB_4_3_1   : string = CodigosPOAC.POAC_TAB_4_3 + "UMV";//4.3.1. Ubicación y Marcado de Vértices
  static readonly POAC_TAB_4_3_2   : string = CodigosPOAC.POAC_TAB_4_3 + "S";//4.3.2. Señalización
  static readonly POAC_TAB_4_3_3   : string = CodigosPOAC.POAC_TAB_4_3 + "DML";//4.3.3. Demarcación y Mantenimiento de Linderos
  static readonly POAC_TAB_4_3_4   : string = CodigosPOAC.POAC_TAB_4_3 + "VUMF";//4.3.4. Vigilancia de la UMF

  static readonly POAC_TAB_5      : string = "POACCFFMPPRFM";	                  //5. Potencial de Producción del Recurso Forestal Maderable
  static readonly POAC_TAB_5_1    : string = CodigosPOAC.POAC_TAB_5 + "CIF";    //5.1 Características del Inventario Forestal
  static readonly POAC_TAB_5_1_1  : string = CodigosPOAC.POAC_TAB_5_1 + "PM";	  //5.1.1. Potencial Maderable
  static readonly POAC_TAB_5_1_2  : string = CodigosPOAC.POAC_TAB_5_1 + "RF";	  //5.1.2. Regeneración de Fustales
  static readonly POAC_TAB_5_2    : string = CodigosPOAC.POAC_TAB_5 + "RPM";	  //5.2 Resultados para el Potencial Maderable
  static readonly POAC_TAB_5_2_B  : string = CodigosPOAC.POAC_TAB_5_2 + "B";
  static readonly POAC_TAB_5_2_D  : string = CodigosPOAC.POAC_TAB_5_2 + "D";
  static readonly POAC_TAB_5_2_E  : string = CodigosPOAC.POAC_TAB_5_2 + "E";
  static readonly POAC_TAB_5_3    : string = CodigosPOAC.POAC_TAB_5 + "RF";     //5.3 Resultados para los Fustales
  static readonly POAC_TAB_5_3_C  : string = CodigosPOAC.POAC_TAB_5_3 + "C";
  static readonly POAC_TAB_5_3_D  : string = CodigosPOAC.POAC_TAB_5_3 + "D";
  static readonly POAC_TAB_5_4    : string = CodigosPOAC.POAC_TAB_5 + "PPRFFS"; //5.4 Potencial de Producción de Recursos Forestales No Maderables y Fauna Silvestre (Opcional)



  static readonly TAB_7             : string = "POACPB";
  static readonly ACORDEON_7_71     : string = CodigosPOAC.TAB_7+"DML";
  static readonly ACORDEON_7_721    : string = CodigosPOAC.TAB_7+"AIA";
  static readonly ACORDEON_7_722    : string = CodigosPOAC.TAB_7+"PGA";

  static readonly ACORDEON_7_722_TABLA_1  : string = CodigosPOAC.ACORDEON_7_722+"PPC";
  static readonly ACORDEON_7_722_TABLA_2  : string = CodigosPOAC.ACORDEON_7_722+"PVS";
  static readonly ACORDEON_7_722_TABLA_3  : string = CodigosPOAC.ACORDEON_7_722+"PCA";

}
