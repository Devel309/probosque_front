
export class ProteccionBosqueActividad {
  constructor(data?: any) {
    if (data) {
      this.idProBosqueActividad = data.idProBosqueActividad;
      this.idProBosqueDet = data.idProBosqueDet;
      this.codigo = data.codigo;
      this.descripcion = data.descripcion;
      this.accion = data.accion;
      return;
    }
  }

  idProBosqueActividad  : number = 0;
  idProBosqueDet        : number = 0;
  codigo                : string | null = null;
  descripcion           : string | null = null;
  accion                : boolean = false;

}
