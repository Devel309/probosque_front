import {ProteccionBosqueActividad} from './ProteccionBosqueActividad';

export class ProteccionBosqueDetalle {
  constructor(data?: any) {
    if (data) {
      this.idProBosqueDet = data.idProBosqueDet;
      this.codPlanGeneralDet = data.codPlanGeneralDet;
      this.subCodPlanGeneralDet = data.subCodPlanGeneralDet;
      this.tipoMarcacion = data.tipoMarcacion;
      this.descripcion = data.descripcion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.nuAccion = data.nuAccion;
      this.nuAccionb = data.nuAccionb;
      this.implementacion = data.implementacion;
      this.descOtra = data.descOtra;
      this.actividad = data.actividad;
      this.factorAmbiental = data.factorAmbiental;
      this.impacto = data.impacto;
      this.nuTala = data.nuTala;
      this.nuArrastre = data.nuArrastre;
      this.nuOtra = data.nuOtra;
      this.nuCenso = data.nuCenso;
      this.nuDemarcacionLineal= data.nuDemarcacionLineal;
      this.nuConstruccionCampamento= data.nuConstruccionCampamento;
      this.nuConstruccionCamino= data.nuConstruccionCamino;
      this.listProteccionBosqueActividad = data.listProteccionBosqueActividad;
      this.actividades = data.actividades;
      this.mitigacionAmbiental = data.mitigacionAmbiental;
      this.tipoPrograma = data.tipoPrograma;
      return;
    }
  }

  idProBosqueDet: number = 0;
  codPlanGeneralDet: string | null = null;
  subCodPlanGeneralDet: string | null = null;
  tipoMarcacion: string | null = null;
  idUsuarioRegistro: number = 0;
  descripcion: string | null = null;
  actividad: string | null = "";
  nuAccion:number | null = null;
  nuAccionb:boolean | null = null;
  implementacion: string | null = null;
  factorAmbiental: string | null = null;
  descOtra: string | null = null;
  impacto: string | null = null;
  listProteccionBosqueActividad:ProteccionBosqueActividad[] = [];
  actividades:string[] = [];
  nuTala:boolean | null = null;
  nuArrastre:boolean | null = null;
  nuOtra:boolean | null = null;
  nuCenso:boolean | null = null;
  nuDemarcacionLineal:boolean | null = null;
  nuConstruccionCampamento:boolean | null = null;
  nuConstruccionCamino:boolean | null = null;
  mitigacionAmbiental: string | null = null;
  tipoPrograma: number | null = null;
}
