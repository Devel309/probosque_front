import {ProteccionBosqueDetalle} from './ProteccionBosqueDetalle';

export class ProteccionBosque {
  constructor(data?: any) {
    if (data) {
      this.idProBosque = data.idProBosque;
      this.idPlanManejo = data.idPlanManejo;
      this.codPlanGeneral = data.codPlanGeneral;
      this.subCodPlanGeneral = data.subCodPlanGeneral;
      this.descripcion = data.descripcion;
      this.idUsuarioRegistro = data.idUsuarioRegistro;
      this.listProteccionBosque = data.listProteccionBosque;
      this.listProteccionGestion = data.listProteccionGestion;
      this.listProteccionAnalisis = data.listProteccionAnalisis;
      return;
    }
  }

  idProBosque: number = 0;
  idPlanManejo: number = 0;
  codPlanGeneral: string | null = null;
  subCodPlanGeneral: string | null = null;
  descripcion: string = '';
  idUsuarioRegistro: number = 0;
  listProteccionBosque: ProteccionBosqueDetalle[] = [];
  listProteccionGestion : ProteccionBosqueDetalle[] = [];
  listProteccionAnalisis : ProteccionBosqueDetalle[] = [];

}
