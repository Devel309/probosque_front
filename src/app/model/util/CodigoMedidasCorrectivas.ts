export abstract class CodigoMedidasCorrectivas {
  static readonly EVAL_PRESENTADO: string = "EMCODPRES";
  static readonly EVAL_OBSERVADO: string = "EMCOOBS";
  static readonly EVAL_NO_PRESENTADO: string = "EMCONPRE";
  static readonly EVAL_EN_EVALUACION: string = "EMCOEVAL";
  static readonly EVAL_COMPLETADO: string = "EMCOCOMP";
}
