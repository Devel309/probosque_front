export abstract class Perfiles {
  static readonly AUTORIDAD_REGIONAL: string = "ARFFS";
  static readonly AUTORIDAD_REGIONAL_CAJA: string = "ARFFS_CAJA";
  static readonly MESA_DE_PARTES: string = "MDP";
  static readonly TITULARTH: string = "TITULARTH";
  static readonly POSTULANTE: string = "POSTULANTE";
  static readonly OSINFOR: string = "OSINFOR";
  static readonly SERFOR: string = "SERFOR";
  static readonly COMPONENTE_ESTADISTICO: string = "COMESTADIST";
  static readonly MUNICIPIO: string = "MUNICIPIO";  //Gobierno Local
  static readonly BENEFICIARIO: string = "BENEFICIARIO";
}
