export abstract class CodigosPMFI {

  static readonly CODIGO_PROCESO: string = "PMFI";

  static readonly TAB_1: string = "PMFIIG";//Información General

  static readonly TAB_2: string = "PMFIO";//Objetivos

  static readonly TAB_3: string = "PMFIIA";//Información del Área
  static readonly ACORDEON_3_1: string = "PMFIIAUB";//Ubicación
  static readonly ACORDEON_3_2: string = "PMFIIAACC";//Accesibilidad
  static readonly ACORDEON_3_3: string = "PMFIIAAFHF";//Aspectos Físicos (Hidrografía y Fisiografía)
  static readonly ACORDEON_3_4: string = "PMFIIAZOVI";//Zonas de Vida
  static readonly ACORDEON_3_5: string = "PMFIIAAB";//Aspectos Biológicos
  static readonly ACORDEON_3_6: string = "PMFIIAASA";//Aspectos Sociales para el Aprovechamiento

  static readonly TAB_4: string = "PMFIOI";

  static readonly TAB_5: string = "PMFIIERS";//5. Información de Especies Recursos y Servicios
  static readonly ACORDEON_5_1: string = "PMFIIERSIEA";//5.1 Identificación de la Especie a Aprovechar
  static readonly ACORDEON_5_2: string = "PMFIIERSINV"; //Inventario

  static readonly TAB_6: string = "PMFISMALS";//Sistema de Manejo, Aprovechamiento y Labores Silviculturales
  static readonly ACORDEON_6_1: string = "PMFISMALSEA";//Etapa de Aprovechamiento
  static readonly ACORDEON_6_2: string = "PMFISMALSCCDA";//Ciclo de Corta y División  Administrativa (Solo en Caso el Aprovechamiento Implique la Muerte del Individuo)
  static readonly ACORDEON_6_3: string = "PMFISMALSLS";//Labores Silviculturales

  static readonly TAB_7: string = "PMFIIMPUMF";//Información de las Medidas de Protección de la Unidad de Manejo Forestal

  static readonly TAB_8: string = "PMFIIIANEMPM";//Identificación de Impactos Ambientales Negativos y Establecimiento de Medidas de Prevención y Mitigación

  static readonly TAB_9: string = "PMFICA";//Cronograma de Actividades

  static readonly TAB_10: string = "PMFIAN";//Anexos

  static readonly TAB_11: string = "DEMACED";//Carga y Envío de Documentación

}
