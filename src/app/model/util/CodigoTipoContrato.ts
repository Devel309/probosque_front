export abstract class CodigoTipoContrato {
    static readonly ABREVIADO: string = "TCONTPROABR";
    static readonly PROGRAMADO_ABREVIADO: string = "TCONTPROABR";
    static readonly PFDM_CONC_DIRECTA: string = "TCONTPFDM";
}
