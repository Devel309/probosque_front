export abstract class CodigosPGMF {

  static readonly PGMF_TAB_4       : string = "PGMFCFFMOPUMF";
  static readonly PGMF_TAB_4_1     : string = CodigosPGMF.PGMF_TAB_4 + "CO";//4.1. Categorías de Ordenamiento

  static readonly PGMF_TAB_4_2     : string = CodigosPGMF.PGMF_TAB_4 + "DAB";//4.2. División Administrativa del Bosque

  static readonly PGMF_TAB_4_2_1   : string = CodigosPGMF.PGMF_TAB_4_2 + "BQ";//4.2.1. Bloques Quinquenales(opcional)
  static readonly PGMF_TAB_4_2_2   : string = CodigosPGMF.PGMF_TAB_4_2 + "PC";//4.2.2. Parcelas de Corta (PC)
  static readonly PGMF_TAB_4_2_3   : string = CodigosPGMF.PGMF_TAB_4_2 + "FC";//4.2.3. Frentes de Corta


  static readonly PGMF_TAB_4_3     : string = CodigosPGMF.PGMF_TAB_4 + "PV";//4.3. Protección y Vigilancia
  static readonly PGMF_TAB_4_3_1   : string = CodigosPGMF.PGMF_TAB_4_3 + "UMV";//4.3.1. Ubicación y Marcado de Vértices
  static readonly PGMF_TAB_4_3_2   : string = CodigosPGMF.PGMF_TAB_4_3 + "S";//4.3.2. Señalización
  static readonly PGMF_TAB_4_3_3   : string = CodigosPGMF.PGMF_TAB_4_3 + "DML";//4.3.3. Demarcación y Mantenimiento de Linderos
  static readonly PGMF_TAB_4_3_4   : string = CodigosPGMF.PGMF_TAB_4_3 + "VUMF";//4.3.4. Vigilancia de la UMF

  static readonly PGMF_TAB_5      : string = "PGMFCFFMPPRFM";	                  //5. Potencial de Producción del Recurso Forestal Maderable
  static readonly PGMF_TAB_5_1    : string = CodigosPGMF.PGMF_TAB_5 + "CIF";    //5.1 Características del Inventario Forestal
  static readonly PGMF_TAB_5_1_1  : string = CodigosPGMF.PGMF_TAB_5_1 + "PM";	  //5.1.1. Potencial Maderable
  static readonly PGMF_TAB_5_1_2  : string = CodigosPGMF.PGMF_TAB_5_1 + "RF";	  //5.1.2. Regeneración de Fustales
  static readonly PGMF_TAB_5_2    : string = CodigosPGMF.PGMF_TAB_5 + "RPM";	  //5.2 Resultados para el Potencial Maderable
  static readonly PGMF_TAB_5_2_B  : string = CodigosPGMF.PGMF_TAB_5_2 + "B";	  
  static readonly PGMF_TAB_5_2_D  : string = CodigosPGMF.PGMF_TAB_5_2 + "D";
  static readonly PGMF_TAB_5_2_E  : string = CodigosPGMF.PGMF_TAB_5_2 + "E";	  
  static readonly PGMF_TAB_5_3    : string = CodigosPGMF.PGMF_TAB_5 + "RF";     //5.3 Resultados para los Fustales
  static readonly PGMF_TAB_5_3_C  : string = CodigosPGMF.PGMF_TAB_5_3 + "C";
  static readonly PGMF_TAB_5_3_D  : string = CodigosPGMF.PGMF_TAB_5_3 + "D";
  static readonly PGMF_TAB_5_4    : string = CodigosPGMF.PGMF_TAB_5 + "PPRFFS"; //5.4 Potencial de Producción de Recursos Forestales No Maderables y Fauna Silvestre (Opcional)
}
