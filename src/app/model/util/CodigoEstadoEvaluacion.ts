export abstract class CodigoEstadoEvaluacion{
  static readonly RECIBIDO: string = "EEVARECI";
  static readonly OBSERVADO: string = "EEVAOBS";
  static readonly PRESENTADO: string = "EEVAPRES";
  static readonly FINALIZADO: string = "EEVALFIN";
}
