export abstract class CodigoPermisoForestal {

  static readonly ESTADO_BORRADOR: string = "EPSFBOR";
  static readonly ESTADO_REGISTRADO: string = "EPSFREG";
  static readonly ESTADO_COMPLETADO: string = "EPSFCOMP";
  static readonly ESTADO_PRESENTADO: string = "EPSFPRES";
  static readonly ESTADO_IMPUGNADO: string = "EPSFIMP";


  static readonly ESTADO_VAL_PROCEDE: string = "EPSFPROC";
  static readonly ESTADO_VAL_NOPROCEDE: string = "EPSFNOPROC";

  static readonly ESTADO_EVAL_FAVORABLE: string = "EPSFFAV";
  static readonly ESTADO_EVAL_DESFAVORABLE: string = "EPSFDESF";

  static readonly ESTADO_EVAL_EMITIDO: string = "EPSFEMI";//cuando esta observado caso, desfavorable
  static readonly ESTADO_EVAL_APROBADO: string = "EPSFAPRO";
  static readonly ESTADO_EVAL_FIRME: string = "EPSFFIRM";



  static readonly ESTADO_VAL_VALIDADO_OBSERVADO: string = "EPSFVALOB"; 
  static readonly ESTADO_VAL_VALIDADO: string = "EPSFVAL";

  static  readonly ESTADO_VAL_EN_EVALUACION :string = "EEVPFEVAL";

  static readonly ESTADO_VAL_PENDIENTE: string = "EPSFPEND";

  static readonly TIPO_EVALUACION: string = "EEVPF";
  static readonly TIPO_VALIDACION: string = "EVPF";

/*
  static readonly TIPO_EVALUACION: string = "EEVPF";
  static readonly TIPO_EVALUACION: string = "EEVPF";
  static readonly TIPO_EVALUACION: string = "EEVPF";
  static readonly TIPO_EVALUACION: string = "EEVPF";
  static readonly TIPO_EVALUACION: string = "EEVPF";
  ESISANPRES	Presentado
  ESISANIMP	Impugnado
  ESISANDESF	Desfavorable
  ESISANFAV	Favorable
  ESISANHAB	Habilitado
  ESISANDESHA	Deshabilitado
  ESISANPROC	Procede
  ESISANNOPROC	No Procede

*/

}
