export abstract class CodigoPMFIC {
  static readonly CODIGO_PROCESO: string = "PMFIC";

  static readonly TAB_1: string = CodigoPMFIC.CODIGO_PROCESO + "INFG";
  static readonly TAB_1_1: string = CodigoPMFIC.TAB_1 + "DGCP";
  static readonly TAB_1_2: string = CodigoPMFIC.TAB_1 + "DP";
  static readonly TAB_1_3: string = CodigoPMFIC.TAB_1 + "DR";
  static readonly TAB_1_4: string = CodigoPMFIC.TAB_1 + "IAM";
  static readonly TAB_1_5: string = CodigoPMFIC.TAB_1 + "IANM";
  static readonly TAB_1_6: string = CodigoPMFIC.TAB_1 + "RPAF";
  static readonly TAB_1_7: string = CodigoPMFIC.TAB_1 + "CTAF";

  static readonly TAB_2: string = CodigoPMFIC.CODIGO_PROCESO + "OBJ";
  static readonly ACORDEON_2_1: string = CodigoPMFIC.TAB_2 + "G";
  static readonly ACORDEON_2_2: string = CodigoPMFIC.TAB_2 + "E";

  static readonly TAB_3: string = CodigoPMFIC.CODIGO_PROCESO + "INFBA";
  static readonly TAB_3_0: string = CodigoPMFIC.TAB_3 + "TIM";
  static readonly TAB_3_1: string = CodigoPMFIC.TAB_3 + "UC";
  static readonly TAB_3_2: string = CodigoPMFIC.TAB_3 + "SUUMF";
  static readonly TAB_3_3: string = CodigoPMFIC.TAB_3 + "ZIUMF";
  static readonly TAB_3_3_TOTAL: string = CodigoPMFIC.TAB_3 + "ZIUMFTOT";
  static readonly TAB_3_4: string = CodigoPMFIC.TAB_3 + "AUMF";

  static readonly TAB_4: string = CodigoPMFIC.CODIGO_PROCESO + "INFBAAF";
  static readonly TAB_4_1: string = CodigoPMFIC.TAB_4 + "HUMF";
  static readonly TAB_4_2: string = CodigoPMFIC.TAB_4 + "FUMF";

  static readonly TAB_5: string = CodigoPMFIC.TAB_3 + "AB";
  static readonly TAB_5_1: string = CodigoPMFIC.TAB_5 + "FS";
  static readonly TAB_5_2: string = CodigoPMFIC.TAB_5 + "TB";

  static readonly TAB_6: string = "PMFICIS";// Informacion socioeconómica
  static readonly ACORDEON_6_1: string = "PMFICISCC";
  static readonly ACORDEON_6_3: string = "PMFICISAUEC";
  static readonly ACORDEON_6_2: string = "PMFICISIS";

  static readonly TAB_7: string = "PMFICOP";
  static readonly ACORDEON_7_1: string = CodigoPMFIC.TAB_7 + "SUB";
  static readonly ACORDEON_7_2: string = CodigoPMFIC.TAB_7 + "SUAAPB";
  static readonly ACORDEON_7_3: string = CodigoPMFIC.TAB_7 + "SUPCAO";
  static readonly ACORDEON_7_4: string = CodigoPMFIC.TAB_7 + "MPUMF";

  static readonly TAB_8: string = "PMFICPPF";
  static readonly ACORDEON_8_1: string = "PMFICPPFPFAFM";	//8.1 Potencial con fines de aprovechamiento forestal maderable
  static readonly ACORDEON_8_2: string = "PMFICPPFPFAFNM";	//8.2 Potencial con fines de aprovechamiento forestal no maderable

  static readonly TAB_9: string = "PMFICSMFUM";// "PMFICSMF";
  static readonly ACORDEON_9_0: string = "PMFICSMFUMCZUMF";//Categorìa de zonificaciòn de la UMF
  static readonly ACORDEON_9_1: string = "PMFICSMFUMMFFM";
  static readonly ACORDEON_9_1_1: string = "PMFICSMFUMMFFMSMFCFM";
  static readonly ACORDEON_9_1_2: string = "PMFICSMFUMMFFMCC";
  static readonly ACORDEON_9_1_3: string = "PMFICSMFUMMFFMAAEUFM";
  static readonly ACORDEON_9_2: string = "PMFICSMFUMMFFNM";
  static readonly ACORDEON_9_2_1: string = "PMFICSMFUMMFFNMSMFNM";
  static readonly ACORDEON_9_2_2: string = "PMFICSMFUMMFFNMCAPRO";
  static readonly ACORDEON_9_2_3: string = "PMFICSMFUMMFFNMAAEFN";
  static readonly ACORDEON_9_3: string = "PMFICSMFUMLS";


  static readonly ACORDEON_9_3_OBLIGATORIO: string = "OBLI";
  static readonly ACORDEON_9_3_OPCIONALES: string = "OPC";

  static readonly TAB_10: string = CodigoPMFIC.CODIGO_PROCESO + "EA";
  static readonly ACORDEON_10_1: string = CodigoPMFIC.TAB_10 + "IM";
  static readonly ACORDEON_10_1_1: string = CodigoPMFIC.TAB_10 + "AC";
  static readonly ACORDEON_10_1_2: string = CodigoPMFIC.TAB_10 + "PV";
  static readonly ACORDEON_10_3: string = CodigoPMFIC.TAB_10 + "CA";

  static readonly TAB_11: string = CodigoPMFIC.CODIGO_PROCESO + "ORG";
  static readonly ACORDEON_11_1: string = CodigoPMFIC.TAB_11 + "IM";

  static readonly TAB_12: string ="PMFICORGCA";
  static readonly TAB_13: string = "PMFICORGRMF";
  static readonly TAB_14: string = "PMFICORGAC";
  static readonly TAB_15: string = "PMFICORGA";
}
