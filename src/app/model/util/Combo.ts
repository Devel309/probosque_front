export interface ComboModel { 
    name:String;
    code?:String;
}

export interface ComboModel2
{
    text:String;
    value?:any;
}

export class DataCombos
{
    static EstadosMaestas:ComboModel[]=[
        {
            name:'Todos',
            code:'2'
        },
        {
            name:'Activo',
            code:'1'
        },
        {
            name:'Inactivo',
            code:'0'
        }
    ];

    static EstadosMaestas2:ComboModel[]=[
        {
            name:'Activo',
            code:'1'
        },
        {
            name:'Inactivo',
            code:'0'
        }
    ];
}