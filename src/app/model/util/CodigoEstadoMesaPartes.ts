export abstract class CodigoEstadoMesaPartesEvaluacion {
  static readonly PRESENTADO: string = 'EMDPRES';
  static readonly OBSERVADO: string = 'EMDOBS';
  static readonly NO_PRESENTADO: string = 'EMDNPRE';
  static readonly EVALUACION: string = 'EMDEVAL';
  static readonly COMPLETADO: string = 'EMDCOMP';
}
