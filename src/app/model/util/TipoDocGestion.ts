export abstract class TipoDocGestion{
    static readonly PROCESO_OFERTA: string = "TDGPROCOFERTA";
    static readonly PERMISOS_FORESTALES: string = "TDGPERMFOR";
    static readonly PLAN_MANEJO: string = "TDGPLMAN";
    static readonly CONCESIONPFDM: string = "TDGCONPFDM";
}
