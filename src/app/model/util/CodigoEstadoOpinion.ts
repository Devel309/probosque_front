export abstract class CodigoEstadoOpiniones {
  static readonly PENDIENTE: string = 'ESOPPEND';
  static readonly ACEPTADO: string = 'ESOPA';
  static readonly OBSERVADO: string = 'ESOPOBS';
  static readonly FAVORABLE: string = 'ESOPFAV';
  static readonly DESFAVORABLE: string = 'ESOPDENE';

}

