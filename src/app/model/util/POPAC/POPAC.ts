export abstract class CodigoPOPAC {
  static readonly CODIGO_PROCESO: string = "POPAC";

  static readonly TAB_1: string = CodigoPOPAC.CODIGO_PROCESO + "INFG";
  static readonly TAB_1_1: string = CodigoPOPAC.TAB_1 + "DGCP";
  static readonly TAB_1_2: string = CodigoPOPAC.TAB_1 + "DP";
  static readonly TAB_1_3: string = CodigoPOPAC.TAB_1 + "DR";
  static readonly TAB_1_4: string = CodigoPOPAC.TAB_1 + "IAM";
  static readonly TAB_1_5: string = CodigoPOPAC.TAB_1 + "IANM";
  static readonly TAB_1_6: string = CodigoPOPAC.TAB_1 + "RPAF";
  static readonly TAB_1_7: string = CodigoPOPAC.TAB_1 + "CTAF";

  static readonly TAB_2: string = CodigoPOPAC.CODIGO_PROCESO + "OBJ";
  static readonly ACORDEON_2_1: string = CodigoPOPAC.TAB_2 + "OE";

  static readonly TAB_3: string = CodigoPOPAC.CODIGO_PROCESO + "INFBA";
  static readonly TAB_3_0: string = CodigoPOPAC.TAB_3 + "TIM";
  static readonly TAB_3_1: string = CodigoPOPAC.TAB_3 + "UC";
  static readonly TAB_3_2: string = CodigoPOPAC.TAB_3 + "SUUMF";
  static readonly TAB_3_3: string = CodigoPOPAC.TAB_3 + "ZIUMF";
  static readonly TAB_3_3_TOTAL: string = CodigoPOPAC.TAB_3 + "ZIUMFTOT";
  static readonly TAB_3_4: string = CodigoPOPAC.TAB_3 + "AUMF";

  static readonly TAB_4: string = CodigoPOPAC.CODIGO_PROCESO + "INFBAAF";
  static readonly TAB_4_1: string = CodigoPOPAC.TAB_4 + "HUMF";
  static readonly TAB_4_2: string = CodigoPOPAC.TAB_4 + "FUMF";

  static readonly TAB_5: string = CodigoPOPAC.TAB_3 + "AB";
  static readonly TAB_5_1: string = CodigoPOPAC.TAB_5 + "FS";
  static readonly TAB_5_2: string = CodigoPOPAC.TAB_5 + "TB";

  static readonly ACORDEON_6_1: string = "POPACISCC";
  static readonly ACORDEON_6_2: string = "POPACISAUEC";
  static readonly ACORDEON_6_3: string = "POPACISIS";

  static readonly TAB_7: string = CodigoPOPAC.CODIGO_PROCESO + "OP";
  static readonly ACORDEON_7_1: string = CodigoPOPAC.TAB_7 + "SUB";
  static readonly ACORDEON_7_2: string = CodigoPOPAC.TAB_7 + "SUAAPB";
  static readonly ACORDEON_7_3: string = CodigoPOPAC.TAB_7 + "SUPCAO";
  static readonly ACORDEON_7_4: string = CodigoPOPAC.TAB_7 + "MPUMF";

  static readonly TAB_9: string = "POPACSMF";
  static readonly ACORDEON_9_0: string = "POPACSMFUMCZUMF";
  static readonly ACORDEON_9_1: string = "POPACSMFUMMFFM";
  static readonly ACORDEON_9_1_1: string = "POPACSMFUMMFFMSMFCFM";
  static readonly ACORDEON_9_1_2: string = "POPACSMFUMMFFMCC";
  static readonly ACORDEON_9_1_3: string = "POPACSMFUMMFFMAAEUFM";
  static readonly ACORDEON_9_2: string = "POPACSMFUMMFFNM";
  static readonly ACORDEON_9_2_1: string = "POPACSMFUMMFFNMSMFNM";
  static readonly ACORDEON_9_2_2: string = "POPACSMFUMMFFNMCAPRO";
  static readonly ACORDEON_9_2_3: string = "POPACSMFUMMFFNMAAEFN";
  static readonly ACORDEON_9_3: string = "POPACSMFUMLS";
  static readonly ACORDEON_9_3_OBLIGATORIO: string = "OBLI";
  static readonly ACORDEON_9_3_OPCIONALES: string = "OPC";

  static readonly TAB_10: string = CodigoPOPAC.CODIGO_PROCESO + "EVA";
  static readonly ACORDEON_10_1: string = CodigoPOPAC.TAB_10 + "IM";
  static readonly ACORDEON_10_1_1: string = CodigoPOPAC.TAB_10 + "AC";
  static readonly ACORDEON_10_1_2: string = CodigoPOPAC.TAB_10 + "PV";
  static readonly ACORDEON_10_3: string = CodigoPOPAC.TAB_10 + "CA";

  static readonly TAB_11: string = CodigoPOPAC.CODIGO_PROCESO + "ORG";
  static readonly ACORDEON_11_1: string = CodigoPOPAC.TAB_11 + "IM";
  //static readonly TAB_11: string = CodigoPOPAC.CODIGO_PROCESO + "ODA";
}
