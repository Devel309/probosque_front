export interface CustomCapaModel {
    nombre:string;
    descripcion:string;
    codigo:Number;
    inServer:Boolean;
    idLayer:string;
    service:Boolean;
    groupId:String;
    idGroupLayer:Number;
    color:String;
    geoJson: {};
    id:Number;
    annex:Boolean;
    url: string;
    overlap:Boolean;
    idsCatastro:string;
    idPlanManejoGeometria:Number;
    codProceso:string;
  }