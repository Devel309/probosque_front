export abstract class LeyendaCargaDescargaArchivos {

  static readonly DEMA3_1_4: string = "DEMA";
  static readonly PMFI3_1_4: string = "PMFI";
  static readonly PO_PMFI3_1_4: string = "PO de PMFI";
}