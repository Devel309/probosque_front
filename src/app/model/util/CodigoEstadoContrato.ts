export abstract class CodigoEstadoContrato {
    static readonly BORRADOR: string = "ECONTRBOR";
    static readonly PENDIENTE_FIRMA: string = "ECONTRPENFIR";
    static readonly FIRMADO: string = "ECONTRFIR";
    static readonly VIGENTE: string = "ECONTRVIGEN";
    static readonly CADUCADO: string = "ECONTRCAD";
    static readonly EXTINGUIDO: string = "ECONTREXTI";
    static readonly RENOVADO: string = "ECONTRRENOV";
    static readonly AMPLIADO: string = "ECONTRAMPL";
}