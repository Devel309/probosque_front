export abstract class CodigosTabEvaluacion{

  static readonly PGMFA_TAB_1  : string = "PGMFARE";//RESUMEN EJECUTIVO
  static readonly PGMFA_TAB_1_DEL_PLAN_GENERAL : string = CodigosTabEvaluacion.PGMFA_TAB_1 + "EV";

  static readonly PGMFA_TAB_2           : string = "PGMFAOM";//OBJETIVO_MANEJO
  static readonly PGMFA_TAB_2_OBJETIVOS : string = CodigosTabEvaluacion.PGMFA_TAB_2+"EV";

  static readonly PGMFA_TAB_3 : string = "PGMFADRP";//DURACION_REVISION_PLAN
  static readonly PGMFA_TAB_3_DURACION_PLAN : string = CodigosTabEvaluacion.PGMFA_TAB_3+"EV";//DURACION_REVISION_PLAN

  static readonly PGMFA_TAB_4     : string = "PGMFAIBAM";//INFORMACION_BASICA
  static readonly PGMFA_TAB_4_ACREDITACION     : string = CodigosTabEvaluacion.PGMFA_TAB_4+"ATC";
  static readonly PGMFA_TAB_4_UBICPOLITCA     : string = CodigosTabEvaluacion.PGMFA_TAB_4+"UP";
  static readonly PGMFA_TAB_4_COORUTM     : string = CodigosTabEvaluacion.PGMFA_TAB_4+"CUAM";
  static readonly PGMFA_TAB_4_ACCESIBILIDAD     : string = CodigosTabEvaluacion.PGMFA_TAB_4+"A";
  static readonly PGMFA_TAB_4_ASPECT_FISICOS     : string = CodigosTabEvaluacion.PGMFA_TAB_4+"AF";
  static readonly PGMFA_TAB_4_ASPECT_BIOLOGICOS    : string = CodigosTabEvaluacion.PGMFA_TAB_4+"AB";
  static readonly PGMFA_TAB_4_ASPECT_SOCIECONOMICOS    : string = CodigosTabEvaluacion.PGMFA_TAB_4+"AS";

  static readonly PGMFA_TAB_4_6_2    : string = CodigosTabEvaluacion.PGMFA_TAB_4_ASPECT_BIOLOGICOS + "TB";

  static readonly PGMFA_TAB_5    : string = "PGMFAOAM";
  static readonly PGMFA_TAB_5_CUADRO_CATEGORIA    : string = CodigosTabEvaluacion.PGMFA_TAB_5+"OAM";

  static readonly PGMFA_TAB_6    : string = "PGMFAPPRF";
  static readonly PGMFA_TAB_6_INVENTARIO_MADERABLES    : string = CodigosTabEvaluacion.PGMFA_TAB_6+"CRIEMM";
  static readonly PGMFA_TAB_6_INVENTARIO_FRUTALES    : string = CodigosTabEvaluacion.PGMFA_TAB_6+"CRIF";


  static readonly PGMFA_TAB_7     : string = "PGMFAMB";
  static readonly PGMFA_TAB_7_71  : string = CodigosTabEvaluacion.PGMFA_TAB_7+"APO";//Actividades principales
  static readonly PGMFA_TAB_7_711 : string = CodigosTabEvaluacion.PGMFA_TAB_7+"DSM";//Sistema de manejo
  static readonly PGMFA_TAB_7_712 : string = CodigosTabEvaluacion.PGMFA_TAB_7+"DPF";//Proteccion forestal
  static readonly PGMFA_TAB_7_721 : string = CodigosTabEvaluacion.PGMFA_TAB_7+"MA";//metodo de aprovechamiento
  static readonly PGMFA_TAB_7_722 : string = CodigosTabEvaluacion.PGMFA_TAB_7+"IAT";//infraestructura para aprovechamiento y transporte
  static readonly PGMFA_TAB_7_723 : string = CodigosTabEvaluacion.PGMFA_TAB_7+"OCA";//operaciones de corts y arrastre
  static readonly PGMFA_TAB_7_73  : string = CodigosTabEvaluacion.PGMFA_TAB_7+"EP";// especies a proteger
  static readonly PGMFA_TAB_7_74  : string = CodigosTabEvaluacion.PGMFA_TAB_7+"TS";//tratamientos silviculturales

  static readonly PGMFA_TAB_8    : string = "PGMFAPB";
  static readonly PGMFA_TAB_8_81    : string = CodigosTabEvaluacion.PGMFA_TAB_8+"DML";//demarcacion y mantenimiento de linderos
  static readonly PGMFA_TAB_8_821    : string = CodigosTabEvaluacion.PGMFA_TAB_8+"AIA";//analisis de impacto ambiental
  static readonly PGMFA_TAB_8_822    : string = CodigosTabEvaluacion.PGMFA_TAB_8+"PGA";//plan de gestion ambiental


  static readonly PGMFA_TAB_9   : string = "PGMFAM";
  static readonly PGMFA_TAB_9_1    : string = CodigosTabEvaluacion.PGMFA_TAB_9+"EV";

  static readonly PGMFA_TAB_10   : string = "PGMFAPC";
  static readonly PGMFA_TAB_10_1    : string = CodigosTabEvaluacion.PGMFA_TAB_10+"PC";

  static readonly PGMFA_TAB_11   : string = "PGMFAC";
  static readonly PGMFA_TAB_11_1    : string = CodigosTabEvaluacion.PGMFA_TAB_11+"CA";

  static readonly PGMFA_TAB_12   : string = "PGMFAODA";
  static readonly PGMFA_TAB_12_1    : string = CodigosTabEvaluacion.PGMFA_TAB_12+"ODA";

  static readonly PGMFA_TAB_13   : string = "PGMFARMF";
  static readonly PGMFA_TAB_13_1    : string = CodigosTabEvaluacion.PGMFA_TAB_13+"I";
  static readonly PGMFA_TAB_13_2   : string = CodigosTabEvaluacion.PGMFA_TAB_13+"E";

  static readonly PGMFA_TAB_14   : string = "PGMFACA";
  static readonly PGMFA_TAB_14_1   : string = CodigosTabEvaluacion.PGMFA_TAB_14+"EV";

  static readonly PGMFA_TAB_15   : string = "PGMFAAC";
  static readonly PGMFA_TAB_15_1   : string = CodigosTabEvaluacion.PGMFA_TAB_15+"AC";


  static readonly PGMFA_TAB_16   : string = "PGMFAA";
  static readonly PGMFA_TAB_16_1   : string = CodigosTabEvaluacion.PGMFA_TAB_16+"A1";
  static readonly PGMFA_TAB_16_2   : string = CodigosTabEvaluacion.PGMFA_TAB_16+"A2";
  static readonly PGMFA_TAB_16_3   : string = CodigosTabEvaluacion.PGMFA_TAB_16+"A3";


  static readonly PGMFA_TAB_17   : string = "PGMFAO";

  static readonly PGMFA_TAB_18   : string = "PGMFAEC";
  static readonly PGMFA_TAB_18_1   : string = CodigosTabEvaluacion.PGMFA_TAB_18+"EV";//para la evaluacion
  static readonly PGMFA_TAB_18_2   : string = CodigosTabEvaluacion.PGMFA_TAB_18+"INS";//para el inspecto


  static readonly POAC_TAB_1   : string = "POACIG";
  static readonly POAC_TAB_1_APROVECHAMIENTO   : string = CodigosTabEvaluacion.POAC_TAB_1+"DA";

  static readonly POAC_TAB_2   : string = "POACRARPA";
  static readonly POAC_TAB_2_21   : string = CodigosTabEvaluacion.POAC_TAB_2+"RA";
  static readonly POAC_TAB_2_22   : string = CodigosTabEvaluacion.POAC_TAB_2+"RPA";

  static readonly POAC_TAB_3   : string = "POACO";
  static readonly POAC_TAB_3_31   : string = CodigosTabEvaluacion.POAC_TAB_3+"BOE";

  static readonly POAC_TAB_4   : string = "POACIBAAA";
  static readonly POAC_TAB_4_41   : string = CodigosTabEvaluacion.POAC_TAB_4+"UP";
  static readonly POAC_TAB_4_42   : string = CodigosTabEvaluacion.POAC_TAB_4+"CUAM";
  static readonly POAC_TAB_4_43   : string = CodigosTabEvaluacion.POAC_TAB_4+"TB";
  static readonly POAC_TAB_4_44   : string = CodigosTabEvaluacion.POAC_TAB_4+"A";

  static readonly POAC_TAB_5   : string = "POACAA";//actividades de aprovechamiento
  static readonly POAC_TAB_5_51   : string = CodigosTabEvaluacion.POAC_TAB_5+"DAAA";
  static readonly POAC_TAB_5_52   : string = CodigosTabEvaluacion.POAC_TAB_5+"CC";
  static readonly POAC_TAB_5_53   : string = CodigosTabEvaluacion.POAC_TAB_5+"R";
  static readonly POAC_TAB_5_54   : string = CodigosTabEvaluacion.POAC_TAB_5+"PCIA";
  static readonly POAC_TAB_5_55   : string = CodigosTabEvaluacion.POAC_TAB_5+"OC";
  static readonly POAC_TAB_5_56   : string = CodigosTabEvaluacion.POAC_TAB_5+"OAT";
  static readonly POAC_TAB_5_57   : string = CodigosTabEvaluacion.POAC_TAB_5+"PL";

  static readonly POAC_TAB_6      : string = "POACAS";
  static readonly POAC_TAB_6_61   : string = CodigosTabEvaluacion.POAC_TAB_6+"ATS";
  static readonly POAC_TAB_6_62   : string = CodigosTabEvaluacion.POAC_TAB_6+"R";

  static readonly POAC_TAB_7   : string = "POACPB";
  static readonly POAC_TAB_7_71   : string = CodigosTabEvaluacion.POAC_TAB_7+"DML";
  static readonly POAC_TAB_7_721   : string = CodigosTabEvaluacion.POAC_TAB_7+"AIA";
  static readonly POAC_TAB_7_722   : string = CodigosTabEvaluacion.POAC_TAB_7+"PGA";

  static readonly POAC_TAB_8   : string = "POACM";//Monitoreo
  static readonly POAC_TAB_8_81  : string = CodigosTabEvaluacion.POAC_TAB_8+"M";

  static readonly POAC_TAB_9   : string = "POACPC";//participacion comunal
  static readonly POAC_TAB_9_91  : string = CodigosTabEvaluacion.POAC_TAB_9+"PC";

  static readonly POAC_TAB_10   : string = "POACC";//capacitacion
  static readonly POAC_TAB_10_1  : string = CodigosTabEvaluacion.POAC_TAB_10+"CA";

  static readonly POAC_TAB_11   : string = "POACODA";//organizacion para el desarrollo de actividades
  static readonly POAC_TAB_11_1  : string = CodigosTabEvaluacion.POAC_TAB_11+"ODA";

  static readonly POAC_TAB_12   : string = "POACRMF";//rentabilidad del manejo forestal
  static readonly POAC_TAB_12_1  : string = CodigosTabEvaluacion.POAC_TAB_12+"I";
  static readonly POAC_TAB_12_2  : string = CodigosTabEvaluacion.POAC_TAB_12+"E";

  static readonly POAC_TAB_13   : string = "POACCA";//cronograma
  static readonly POAC_TAB_13_1  : string = CodigosTabEvaluacion.POAC_TAB_13+"CAPPE";

  static readonly POAC_TAB_14   : string = "POACAC";//aspectos complementarios
  static readonly POAC_TAB_14_1  : string = CodigosTabEvaluacion.POAC_TAB_14+"AC";

  static readonly POAC_TAB_15   : string = "POACAN";//anexos
  static readonly POAC_TAB_15_1  : string = CodigosTabEvaluacion.POAC_TAB_15+"AN1";
  static readonly POAC_TAB_15_2  : string = CodigosTabEvaluacion.POAC_TAB_15+"AN2";

  static readonly POAC_TAB_16   : string = "POACGPPP";
  static readonly POAC_TAB_17   : string = "POACRE";



  static readonly PFCR_TAB_1   : string = "GOPFSDG";//Informacion general
  static readonly PFCR_TAB_VAL_1_1  : string = CodigosTabEvaluacion.PFCR_TAB_1+"VAL";
  static readonly PFCR_TAB_EVAL_1_1  : string = CodigosTabEvaluacion.PFCR_TAB_1+"EV";

  static readonly PFCR_TAB_2  : string = "GOPFSDO";//Otorgamiento
  static readonly PFCR_TAB_EVAL_2_1  : string = CodigosTabEvaluacion.PFCR_TAB_2+"EV";

  static readonly PFCR_TAB_EVAL_2_1_1  : string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1 + "REG";
  static readonly PFCR_TAB_EVAL_2_1_2  : string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1 + "OPI";
  static readonly PFCR_TAB_EVAL_2_1_3  : string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1 + "OCU";
  static readonly PFCR_TAB_EVAL_2_1_4  : string = CodigosTabEvaluacion.PFCR_TAB_EVAL_2_1 + "PLA";


  static readonly PFCR_TAB_VAL_2_1  : string = CodigosTabEvaluacion.PFCR_TAB_2+"VAL";

  static readonly PFCR_TAB_3  : string = "GOPFSAN";//Anexos
  static readonly PFCR_TAB_EVAL_3_1  : string = CodigosTabEvaluacion.PFCR_TAB_3+"EV";
  static readonly PFCR_TAB_VAL_3_1  : string = CodigosTabEvaluacion.PFCR_TAB_3+"VAL";

  static readonly PFCR_TAB_4  : string = "GOPFSRDF";//Recepción de documentos físicos
  static readonly PFCR_TAB_EVAL_4_1  : string = CodigosTabEvaluacion.PFCR_TAB_4+"EV";
  static readonly PFCR_TAB_VAL_4_1  : string = CodigosTabEvaluacion.PFCR_TAB_4+"VAL";

  static readonly PFCR_TAB_5  : string = "GOPFSRE";// Resultado de evaluación
  static readonly PFCR_TAB_EVAL_5_1  : string = CodigosTabEvaluacion.PFCR_TAB_5+"EV";
  static readonly PFCR_TAB_VAL_5_1  : string = CodigosTabEvaluacion.PFCR_TAB_5+"VAL";


}
