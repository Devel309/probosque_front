import { PlanManejoModel } from "./PlanManejo";
import { AuditoriaModel } from "./auditoria";

export interface ObjetivoManejoModel extends AuditoriaModel {
    idObjManejo: number;
    general: String;
    detalle: String;
    planManejo: PlanManejoModel;
    observacion: String;
}
