export interface AuditoriaModel {
    estado: String;
    idUsuarioRegistro: number;
    fechaRegistro: Date;
    idUsuarioModificacion: number;
    fechaModificacion: Date;
    idUsuarioElimina: number;
    fechaElimina: Date;
}

export class AuditoriaDto implements AuditoriaModel {
    estado: String;
    idUsuarioRegistro: number;
    fechaRegistro: Date;
    idUsuarioModificacion: number;
    fechaModificacion: Date;
    idUsuarioElimina: number;
    fechaElimina: Date;

    constructor(obj?: Partial<AuditoriaDto>) {
        this.estado = '';
        this.idUsuarioRegistro = 0;
        this.fechaRegistro = new Date();
        this.idUsuarioModificacion = 0;
        this.fechaModificacion = new Date();
        this.idUsuarioElimina = 0;
        this.fechaElimina = new Date();
        if (obj) Object.assign(this, obj);
    }
}
