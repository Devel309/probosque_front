import { AuditoriaModel } from "./auditoria";
import { RentabilidadManejoForestalModel } from "./RentabilidadManejoForestal";
export interface RentabilidadManejoForestalDetalleModel extends AuditoriaModel{
  idRentManejoForestalDet: number;
  anio: number;
  monto: number|any;
  rentabilidadManejoForestal: RentabilidadManejoForestalModel;
  checked:boolean;
  mes: number;
}
