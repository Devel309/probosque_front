
import { AuditoriaModel } from "./auditoria";

export interface PlanGestionModel extends AuditoriaModel{
    idPlanGestion: number;
    descripcionImpacto: String;
    actividades:string;
    medidasMitigacion:string;
    tipoPlan:string;
}