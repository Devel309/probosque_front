import { AuditoriaDto } from "./auditoria";
import { SistemaManejoForestalDetalle } from "./SistemaManejoForestalDetalle";

export class SistemaManejoForestal extends AuditoriaDto {
    idSistemaManejoForestal: number | null;
    idPlanManejo: number | null;
    descripcionFinMaderable: string | null;
    descripcionCicloCorta: string | null;
    descripcionFinNoMaderable: string | null;
    descripcionCicloAprovechamiento: string | null;
    codigoProceso: string | null;
    detalle: SistemaManejoForestalDetalle[] | null;
    seccion: string | null;
    subSeccion: string | null;

    constructor(obj?: Partial<SistemaManejoForestal>) {
        super();

        this.idSistemaManejoForestal = null;
        this.idPlanManejo = null;
        this.descripcionFinMaderable = null;
        this.descripcionCicloCorta = null;
        this.descripcionFinNoMaderable = null;
        this.descripcionCicloAprovechamiento = null;
        this.codigoProceso = null;
        this.detalle = [];
        this.seccion = null;
        this.subSeccion = null;

        if (obj) Object.assign(this, obj);
    }

}
