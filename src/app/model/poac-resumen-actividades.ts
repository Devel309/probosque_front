export class ResumenActModelPOAC {
  constructor(data?: any) {
    if (data) {
      this.idResumenAct = data.idResumenAct ? data.idResumenAct : 0;
      this.codTipoResumen = data.codTipoResumen ? data.codTipoResumen : 'POAC';
      this.codigoResumen = data.codigoResumen
        ? data.codigoResumen
        : 'POACRARPA';
      this.nroResolucion = data.nroResolucion ? data.nroResolucion : 0;
      this.nroPcs = data.nroPcs ? data.nroPcs : 0;
      this.areaHa = data.areaHa ? data.areaHa : 0;
      this.idPlanManejo = data.idPlanManejo ? data.idPlanManejo : 0;
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.listResumenEvaluacion = data.listResumenEvaluacion
        ? data.listResumenEvaluacion
        : [];

      return;
    }
  }
  idResumenAct: number = 0;
  codTipoResumen: string = 'POAC';
  codigoResumen: string = 'POACRARPA';
  codigoSubResumen: string = '';
  nroResolucion: number = 0;
  nroPcs: number = 0;
  areaHa: number = 0;
  idPlanManejo: number = 0;
  idUsuarioRegistro: number = 0;
  listResumenEvaluacion: ResumenActDetModelPOAC[] = [];
}

export class ResumenActDetModelPOAC {
  constructor(data?: any) {
    if (data) {
      this.idResumenActDet = data.idResumenActDet ? data.idResumenActDet : 0;
      this.tipoResumen = data.tipoResumen ? data.tipoResumen : 0;
      this.actividad = data.actividad ? data.actividad : '';
      this.indicador = data.indicador ? data.indicador : '';
      this.programado = data.programado ? data.programado : '';
      this.realizado = data.realizado ? data.realizado : '';
      this.resumen = data.resumen ? data.resumen : '';
      this.idUsuarioRegistro = data.idUsuarioRegistro
        ? data.idUsuarioRegistro
        : 0;
      this.aspectoRecomendacion = data.aspectoRecomendacion
        ? data.aspectoRecomendacion
        : '';
      this.aspectoPositivo = null;
      this.aspectoNegativo = null;
      this.estadoVar = data.estadoVar ? data.estadoVar : '';

      return;
    }
  }
  idResumenActDet: number = 0;
  tipoResumen: number = 0;
  actividad: string = '';
  indicador: string = '';
  programado: string = '';
  realizado: string = '';
  resumen: string = '';
  idUsuarioRegistro: number = 0;
  aspectoRecomendacion: string = '';
  aspectoPositivo: any = null;
  aspectoNegativo: any = null;
  estadoVar: string = '';
}

export abstract class CodigosResumenActividadesPOAC {
  static readonly CODIGO_PROCESO: string = 'POAC';

  static readonly CODIGO_TAB: string = `POACRARPA`;

  static readonly ACORDEON_2_1: string = `${CodigosResumenActividadesPOAC.CODIGO_PROCESO}RA`;

  static readonly ACORDEON_2_2: string = `${CodigosResumenActividadesPOAC.CODIGO_PROCESO}RPA`;

  static readonly ACORDEON_2_2_1: string = `${CodigosResumenActividadesPOAC.CODIGO_PROCESO}RPAAN`;

  static readonly ACORDEON_2_2_3: string = `${CodigosResumenActividadesPOAC.CODIGO_PROCESO}RPAR `;
}
