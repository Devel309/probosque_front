import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContratoElaboracionPgmfComponent } from 'src/app/web/planificacion/contrato-elaboracion-pgmf/contrato-elaboracion-pgmf.component';
import { PermisosOpcionResolver } from './resolvers/permisos-opcion.resolver';
import { BandejaPlanManejoSharedComponent } from './shared/components/bandeja-plan-manejo-shared/bandeja-plan-manejo-shared.component';
import { BandejaLibroOperacionesComponent } from './web/aprovechamiento/bandeja-libro-operaciones/bandeja-libro-operaciones.component';
import { EvaluacionLibroOperacionesComponent } from './web/aprovechamiento/evaluacion-libro-operaciones/evaluacion-libro-operaciones.component';
import { DetalleSincronizacionComponent } from './web/aprovechamiento/sincronizacion/detalle-sincronizacion/detalle-sincronizacion.component';
import { SincronizacionComponent } from './web/aprovechamiento/sincronizacion/sincronizacion.component';
import { BandejaEstablecimientoBlComponent } from './web/bosque-local/bandeja-establecimiento-bl/bandeja-establecimiento-bl.component';
import { EvaluacionComiteTecnicoComponent } from './web/bosque-local/bandeja-establecimiento-bl/evaluacion-comite-tecnico/evaluacion-comite-tecnico.component';
import { RegistroComiteTecnicoBlComponent } from './web/bosque-local/bandeja-establecimiento-bl/registro-comite-tecnico-bl/registro-comite-tecnico-bl.component';
import { RegistroEstablecimientoBlComponent } from './web/bosque-local/bandeja-establecimiento-bl/registro-establecimiento-bl/registro-establecimiento-bl.component';
import { BandejaEvaluacionComiteBlComponent } from './web/bosque-local/bandeja-evaluacion-comite-bl/bandeja-evaluacion-comite-bl.component';
import { CalificacionConcesionPFDMComponent } from './web/concesion/calificacion-concesion-pfdm/calificacion-concesion-pfdm.component';
import { EvaluarPropuestaCalificacionConcesionPfdmComponent } from './web/concesion/calificacion-concesion-pfdm/evaluar-propuesta-calificacion/evaluar-propuesta-calificacion-concesion-pfdm.component';
import { EvaluacionPropuestaComponent } from './web/concesion/solicitud-concesion-pfdm/evaluacion-propuesta/evaluacion-propuesta.component';
import { AnexoFormatoPropuestaComponent } from './web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/anexo-formato-propuesta/anexo-formato-propuesta.component';
import { RegistroSolicitudConcesionPfdmComponent } from './web/concesion/solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm/registro-solicitud-concesion-pfdm.component';
import { SolicitudConcesionPFDMComponent } from './web/concesion/solicitud-concesion-pfdm/solicitud-concesion-pfdm.component';
import { MecanismoPagoComponent } from './web/derecho-aprovechamiento/mecanismo-pago/mecanismo-pago.component';
import { RegistroMecanismoPagoComponent } from './web/derecho-aprovechamiento/mecanismo-pago/registro-mecanismo-pago/registro-mecanismo-pago.component';
import { EvaluacionEvalPmfiDemaComponent } from './web/evaluacion/evaluacion-eval-pmfi-dema/evaluacion-eval-pmfi-dema.component';
import { EvaluacionOtorgamientoPermisoComponent } from './web/evaluacion/evaluacion-otorgamiento-permiso/evaluacion-otorgamiento-permiso.component';
import { EvaluacionPlanGeneralManejoComponent } from './web/evaluacion/evaluacion-plan-general-manejo/evaluacion-plan-general-manejo.component';
import { EvaluacionPlanOperativoCcnnComponent } from './web/evaluacion/evaluacion-plan-operativo-ccnn/evaluacion-plan-operativo-ccnn.component';
import { InicioComponent } from './web/inicio/inicio.component';
import { AuthGuard } from './web/login/AuthGuard';
import { LoginComponent } from './web/login/login.component';
import { ConsultaSolicitudComponent } from './web/planificacion/ampliacion-solicitud/ampliacion-solicitud.component';
import { ConsultaSolicitudesComponent } from './web/planificacion/ampliacion-solicitudes/ampliacion-solicitudes.component';
import { BandejaContratoConcesionPfdmComponent } from './web/planificacion/bandeja-contrato-concesion-pfdm/bandeja-contrato-concesion-pfdm.component';
import { BandejaContratoConcesionPostulacionComponent } from './web/planificacion/bandeja-contrato-concesion-postulacion/bandeja-contrato-concesion-postulacion.component';
import { BandejaContratoComponent } from './web/planificacion/bandeja-contrato/bandeja-contrato.component';
import { BandejaDocumentosDigitalizadosComponent } from './web/planificacion/bandeja-documentos-digitalizados/bandeja-documentos-digitalizados.component';
import { RequisitosMdpComponentPgmfa } from './web/planificacion/bandeja-eval-pgmfa/requisitos-mdp/requisitos-mdp.component';
import { RequisitosPreviosPgmfaComponent } from './web/planificacion/bandeja-eval-pgmfa/requisitos-previos/requisitos-previos-pgmfa.component';
import { RequisitosMesaPartesComponent } from './web/planificacion/bandeja-eval-pmfi-dema/requisitos-mesa-partes/requisitos-mesa-partes.component';
import { RequisitosPreviosPmfiDemaComponent } from './web/planificacion/bandeja-eval-pmfi-dema/requisitos-previos/requisitos-previos-pmfi-dema.component';
import { RequisitosMdpComponentPoac } from './web/planificacion/bandeja-eval-poac/requisitos-mdp/requisitos-mdp.component';
import { RequisitosPreviosPoacComponent } from './web/planificacion/bandeja-eval-poac/requisitos-previos/requisitos-previos-poac.component';
import { BandejaEvaluacionOtorgamientoTuComponent } from './web/planificacion/bandeja-evaluacion-otorgamiento-tu/bandeja-evaluacion-otorgamiento-tu.component';
import { EvaluarOtorgarTuComponent } from './web/planificacion/bandeja-evaluacion-otorgamiento-tu/evaluar-otorgar-tu/evaluar-otorgar-tu.component';
import { GenerarResolucionComponent } from './web/planificacion/bandeja-evaluacion-otorgamiento-tu/generar-resolucion/generar-resolucion.component';
import { BandejaEvaluacionTecnicaComponent } from './web/planificacion/bandeja-evaluacion-tecnica/bandeja-evaluacion-tecnica.component';
import { GenerarResolucionComponent as GenerarResolucionEvaluacionTecnica } from './web/planificacion/bandeja-evaluacion-tecnica/generar-resolucion/generar-resolucion.component';
import { BandejaOtorgamientoPfdmComponent } from './web/planificacion/bandeja-otorgamiento-pfdm/bandeja-otorgamiento-pfdm.component';
import { GenerarResolucionOtorgamientoPfdmComponent } from './web/planificacion/bandeja-otorgamiento-pfdm/generar-resolucion-otorgamiento-pfdm/generar-resolucion-otorgamiento-pfdm.component';
import { BandejaPermisosForestales } from './web/planificacion/bandeja-permisos-forestales/bandeja-permisos-forestales.component';
import { NotificacionOtorgamientoPermisoDetalleComponent } from './web/planificacion/bandeja-permisos-forestales/notificacion-otorgamiento-permiso-detalle/notificacion-otorgamiento-permiso-detalle.component';
import { ResultadoEvaluacionSolicitudOtorgamientoDetalleComponent } from './web/planificacion/bandeja-permisos-forestales/resultado-evaluacion-solicitud-otorgamiento-detalle/resultado-evaluacion-solicitud-otorgamiento-detalle.component';
import { BandejaPgmfConsolidadoComponent } from './web/planificacion/bandeja-pgmf-consolidado/bandeja-pgmf-consolidado.component';
import { BandejaPlanOperativo } from './web/planificacion/bandeja-plan-operativo/bandeja-plan-operativo.component';
import { BandejaPmficComponent } from './web/planificacion/bandeja-pmfic/bandeja-pmfic.component';
import { BandejaPopacComponent } from './web/planificacion/bandeja-popac/bandeja-popac.component';
import { BandejaPostulacionComponent } from './web/planificacion/bandeja-postulacion/bandeja-postulacion.component';
import { RevisarComponent } from './web/planificacion/bandeja-postulacion/revisar/revisar.component';
import { BandejaProcesoConcesionPfdmComponent } from './web/planificacion/bandeja-proceso-concesion-pfdm/bandeja-proceso-concesion-pfdm.component';
import { EvaluacionContratoConcesionPfdmComponent } from './web/planificacion/bandeja-proceso-concesion-pfdm/evaluacion-contrato-concesion-pfdm/evaluacion-contrato-concesion-pfdm.component';
import { EvaluarOtorgarProcesoConcesionPfdmComponent } from './web/planificacion/bandeja-proceso-concesion-pfdm/evaluar-otorgar-proceso-concesion-pfdm/evaluar-otorgar-proceso-concesion-pfdm.component';
import { GenerarResolucionProcesoConcesionPfdmComponent } from './web/planificacion/bandeja-proceso-concesion-pfdm/generar-resolucion-proceso-concesion-pfdm/generar-resolucion-proceso-concesion-pfdm.component';
import { BandejaSolicitudAcceso } from './web/planificacion/bandeja-solicitud-acceso/bandeja-solicitud-acceso.component';
import { BandejaSolicitudContratoComponent } from './web/planificacion/bandeja-solicitud-contrato/bandeja-solicitud-contrato.component';
import { BandejaSolicitudOpinionPmfiDemaComponent } from './web/planificacion/bandeja-solicitud-opinion/bandeja-solicitud-opinion.component';
import { BandejaPGMFComponent } from './web/planificacion/concesion-forestal-maderables/bandeja-PGMF/bandeja-PGMF.component';
import { ConcesionForestalMaderablesComponent } from './web/planificacion/concesion-forestal-maderables/concesion-forestal-maderables.component';
import { ConsideracionesEvalucionPgmfComponent } from './web/planificacion/concesion-forestal-maderables/consideraciones-evalucion-pgmf/consideraciones-evalucion-pgmf.component';
import { LineamientosEvaluacionPgmfComponent } from './web/planificacion/concesion-forestal-maderables/lineamientos-evaluacion-pgmf/lineamientos-evaluacion-pgmf.component';
import { ElaboracionDeclaracionMpafppComponent } from './web/planificacion/elaboracion-declaracion-mpafpp/elaboracion-declaracion-mpafpp.component';
import { ElaboracionPgmfComponent } from './web/planificacion/elaboracion-pgmf/elaboracion-pgmf.component';
import { EnvioInformacionOtorgamientoComponent } from './web/planificacion/envio-informacion-otorgamiento/envio-informacion-otorgamiento.component';
import { EvaluacionAprobacionPmComponent } from './web/planificacion/evaluacion-aprobacion-pm/evaluacion-aprobacion-pm.component';
import { BandejaEvaluacionCampoComponent } from './web/planificacion/evaluacion-campo/bandeja-evaluacion-campo/bandeja-evaluacion-campo.component';
import { RegistroEvaluacionCampoComponent } from './web/planificacion/evaluacion-campo/registro-evaluacion-campo/registro-evaluacion-campo.component';
import { EvaluacionContratoConcesionComponent } from './web/planificacion/evaluacion-contrato-concesion/evaluacion-contrato-concesion.component';
import { BandejaEvaluacionPgmfComponent } from './web/planificacion/evaluacion-pgmf/bandeja-evaluacion-pgmf/bandeja-evaluacion-pgmf.component';
import { EvaluarPermisoForestalCCNNComponent } from './web/planificacion/evaluar-permiso-forestal-ccnn/evaluar-permiso-forestal-ccnn.component';
import { EvaluarPermisoForestalComponent } from './web/planificacion/evaluar-permiso-forestal/evaluar-permiso-forestal.component';
import { BandejaPmfi } from './web/planificacion/formulacion-pmfi-concesion-pfdm/bandeja-pmficomponent';
import { FormulacionPMFIConcesionPFDMComponent } from './web/planificacion/formulacion-pmfi-concesion-pfdm/formulacion-pmfi-concesion-pfdm.component';
import { DetalleResultadoConcesionComponent } from './web/planificacion/generacion-contrato-concesion/detalle-resultado-concesion/detalle-resultado-concesion.component';
import { GeneracionContratoConcesionComponent } from './web/planificacion/generacion-contrato-concesion/generacion-contrato-concesion.component';
import { DetalleResultadoComponent } from './web/planificacion/generacion-contrato/detalle-resultado/detalle-resultado.component';
import { GeneracionContratoComponent } from './web/planificacion/generacion-contrato/generacion-contrato.component';
import { BandejaGeneracionDema } from './web/planificacion/generacion-declaracion-manejo-dema/bandeja-generacion-dema.component';
import { GeneracionDeclaracionManejoDemaComponent } from './web/planificacion/generacion-declaracion-manejo-dema/generacion-declaracion-manejo-dema.component';
import { BandejaEvaluacionImpugnacionComponent } from './web/planificacion/Impugnacion/bandeja-evaluacion-impugnacion/bandeja-evaluacion-impugnacion.component';
import { BandejaSolicitudComponent } from './web/planificacion/Impugnacion/bandeja-solicitud/bandeja-solicitud.component';
import { EvaluacionRecursoInpugnacionComponent } from './web/planificacion/Impugnacion/evaluacion-recurso-inpugnacion/evaluacion-recurso-inpugnacion.component';
import { SolicitudRecursoInpugnacionComponent } from './web/planificacion/Impugnacion/solicitar-recurso-inpugnacion/solicitar-recurso-inpugnacion.component';
import { LanzarPostulacionComponent } from './web/planificacion/lanzar-postulacion/lanzar-postulacion.component';
import { ListaOposicionComponent } from './web/planificacion/oposicion/lista-oposicion/lista-oposicion.component';
import { OposicionComponent } from './web/planificacion/oposicion/oposicion.component';
import { NuevoOtorgamientoDerechoComponent } from './web/planificacion/otorgamiento-derechos/nuevo-otorgamiento-derecho/nuevo-otorgamiento-derecho.component';
import { OtorgamientoDerechosComponent } from './web/planificacion/otorgamiento-derechos/otorgamiento-derechos.component';
import { BandejaPgmfa } from './web/planificacion/plan-general-manejo/bandeja-pgmfa.component';
import { PlanGeneralManejoComponent } from './web/planificacion/plan-general-manejo/plan-general-manejo.component';
import { DetallePlanManejoForestalIntermedioComponent } from './web/planificacion/plan-manejo-forestal-intermedio/detalle/detalle-forestal-intermedio.component';
import { BandejaPoac } from './web/planificacion/plan-operativo-ccnn-ealta/bandeja-poac.component';
import { PlanOperativoComponent } from './web/planificacion/plan-operativo-ccnn-ealta/plan-operativo-ccnn-ealta.component';
import { BandejaPOCCComponent } from './web/planificacion/plan-operativo-concesiones-maderables/bandeja-POCC.component';
import { PlanOperativoConcesionesMaderablesComponent } from './web/planificacion/plan-operativo-concesiones-maderables/plan-operativo-concesiones-maderables.component';
import { DetallePlanOperativoPMFICnccComponent } from './web/planificacion/plan-operativo-PMFI-cncc/detalle/detalle-plan-operativo-PMFI-cncc.component';
import { PostularOfertaComponent } from './web/planificacion/procesos-ofertados/postular-oferta/postular-oferta.component';
import { ProcesosOfertadosComponent } from './web/planificacion/procesos-ofertados/procesos-ofertados.component';
import { RevicionPlantacionForestalComponent } from './web/planificacion/revicion-plantacion-forestal/revicion-plantacion-forestal.component';
import { RevisionPermisosForestales } from './web/planificacion/revision-permiso-forestal/revision-permiso-forestal.component';
import { RevisionSolicitudAccesoComponent } from './web/planificacion/revision-solicitud-acceso/revision-solicitud-acceso.component';
import { BandejaSolicitudOpinionComponent } from './web/planificacion/solicitud-opinion/bandeja-solicitud-opinion/bandeja-solicitud-opinion.component';
import { SolicitudPermisoForestalCCNNComponent } from './web/planificacion/solicitud-permiso-forestal-ccnn/solicitud-permiso-forestal-ccnn.component';
import { SolicitudPermisoForestalComponent } from './web/planificacion/solicitud-permiso-forestal/solicitud-permiso-forestal.component';
// import { BandejaPlantacionForestalComponent } from './web/planificacion/bandeja-plantacion-forestal/bandeja-plantacion-forestal.component';
import { BandejaPlantacionForestalComponent } from './web/plantacion/bandeja-plantacion-forestal/bandeja-plantacion-forestal.component';
// import { RegistroPlantacionForestalComponent } from './web/planificacion/registro-plantacion-forestal/registro-plantacion-forestal.component';
import { RegistroPlantacionForestalComponent } from './web/plantacion/bandeja-plantacion-forestal/registro-plantacion-forestal/registro-plantacion-forestal.component';
import { ResultadosEvaluacionComponent } from './web/plantacion/bandeja-plantacion-forestal/tab/resultados-evaluacion/resultados-evaluacion.component';
import { EvaluacionCampoFpfComponent } from './web/plantacion/fiscalizacion-plantacion-forestal/evaluacion-campo-fpf/evaluacion-campo-fpf.component';
import { FiscalizacionPlantacionForestalComponent } from './web/plantacion/fiscalizacion-plantacion-forestal/fiscalizacion-plantacion-forestal.component';
import { RecuperarContrasenaComponent } from './web/recuperar-contrasena/recuperar-contrasena.component';
import { BandejaBeneficioTHComponent } from './web/regimen-promocional/beneficioTH/bandeja-beneficioTH/bandeja-beneficio-th.component';
import { BandejaSolicitudBeneficioComponent } from './web/regimen-promocional/solicitud-beneficio/bandeja-solicitud-beneficio/bandeja-solicitud-beneficio.component';
import { RegistroSolicitudBeneficioComponent } from './web/regimen-promocional/solicitud-beneficio/registro-solicitud-beneficio/registro-solicitud-beneficio.component';
import { RegistrarSolicitudAcessoComponent } from './web/registrar-solicitud-acesso/registrar-solicitud-acesso.component';
import { GestionEvaluacionPlanManejoComponent } from './web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-manejo/gestion-evaluacion-plan-manejo.component';
import { GestionEvaluacionPlanOperativoComponent } from './web/requisitros-preliminares-evaluacion/gestion-evaluacion-plan-operativo/gestion-evaluacion-plan-operativo.component';
import { TramitesComponent } from './web/tramites/tramites.component';





const routes: Routes = [
  {
    path: '',
    component: InicioComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'login',
    component: LoginComponent,
  },
  // {
  //   path: '',
  //   component: LoginComponent,
  // },
  {
    path: 'recuperar-contrasena',
    component: RecuperarContrasenaComponent,
  },
  {
    path: 'inicio',
    component: InicioComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'mis-tramites',
    component: TramitesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-pgmf',
    component: ElaboracionPgmfComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-solicitud-acceso',
    component: BandejaSolicitudAcceso,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/revision-solicitud-acceso',
    component: RevisionSolicitudAccesoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'registrar-solicitud-acceso',
    component: RegistrarSolicitudAcessoComponent,
  },
  {
    path: 'planificacion/evaluacion-aprobacion-pm',
    component: EvaluacionAprobacionPmComponent,
    canActivate: [AuthGuard],
  },
  // {
  //   path: 'planificacion/registro-plantacion-forestal',
  //   component: RegistroPlantacionForestalComponent,
  //   canActivate: [AuthGuard],
  // },
  // {
  //   path: 'planificacion/registro-plantacion-forestal/:id',
  //   component: RegistroPlantacionForestalComponent,
  //   canActivate: [AuthGuard],
  // },
  {
    path: 'planificacion/revision-plantacion-forestal',
    component: RevicionPlantacionForestalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/plan-general-manejo/:idPlan',
    component: PlanGeneralManejoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-pgmfa',
    component: BandejaPgmfa,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/bandeja-pmfic',
    component: BandejaPmficComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/bandeja-popac',
    component: BandejaPopacComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  /******************* Solicitud Opinión ***************/

  {
    path: 'planificacion/bandeja-solicitud-opinion-entidad',
    component: BandejaSolicitudOpinionPmfiDemaComponent,
    canActivate: [AuthGuard],
  },

  /******************* Evaluación ***************/
  {
    path: 'planificacion/evaluacion/plan-operativo-ccnn',
    component: EvaluacionPlanOperativoCcnnComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/evaluacion/plan-general-manejo',
    component: EvaluacionPlanGeneralManejoComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'planificacion/evaluacion/bandeja-eval-pmfi-dema',
    component: EvaluacionEvalPmfiDemaComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'planificacion/evaluacion/requisitos-mesa-partes/:idPlan/:tipoPlan',
    component: RequisitosMesaPartesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion/requisitos-mesa-partes-pgmf/:idPlan/:tipoPlan',
    component: RequisitosMdpComponentPgmfa,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion/requisitos-mesa-partes-poac/:idPlan/:tipoPlan',
    component: RequisitosMdpComponentPoac,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion/requisitos-previos/:idPlan/:tipoPlan',
    component: RequisitosPreviosPmfiDemaComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion/requisitos-previos-poac/:idPlan/:tipoPlan',
    component: RequisitosPreviosPoacComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion/requisitos-previos-pgmfa/:idPlan/:tipoPlan',
    component: RequisitosPreviosPgmfaComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/evaluacion/otorgamiento-permiso',
    component: EvaluacionOtorgamientoPermisoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-documentos-digitalizados',
    component: BandejaDocumentosDigitalizadosComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/elaboracion-declaracion-mpafpp',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-mpafpp/:idPlan',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-demap',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-demap/:idPlan',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-pmfip',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-pmfip/:idPlan',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-popmifp',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-popmifp/:idPlan',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-demac',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/elaboracion-declaracion-demac/:idPlan',
    component: ElaboracionDeclaracionMpafppComponent,
    canActivate: [AuthGuard],
  },


  {
    path: 'planificacion/plan-operativo-ccnn-ealta/:idPlan',
    component: PlanOperativoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-poac',
    component: BandejaPoac,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/otorgamiento-derechos',
    component: OtorgamientoDerechosComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'planificacion/postulacion',
    component: LanzarPostulacionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/otorgamiento-derechos/nuevo',
    component: NuevoOtorgamientoDerechoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/procesos-oferta',
    component: ProcesosOfertadosComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'planificacion/procesos-oferta/postular',
    component: PostularOfertaComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-evaluacion-otorgamiento-tu',
    component: BandejaEvaluacionOtorgamientoTuComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion-otorgaminento-tu',
    component: EvaluarOtorgarTuComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-postulacion',
    component: BandejaPostulacionComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'planificacion/bandeja-postulacion/revisar',
    component: RevisarComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/concesion-forestal-maderables',
    component: ConcesionForestalMaderablesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/concesion-forestal-maderables/:idPlan',
    component: ConcesionForestalMaderablesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/consideraciones-pgmf',
    component: ConsideracionesEvalucionPgmfComponent,
  },

  {
    path: 'planificacion/consideraciones-pgmf/:idPlan',
    component: ConsideracionesEvalucionPgmfComponent,
  },

  {
    path: 'planificacion/lineamientos-pgmf',
    component: LineamientosEvaluacionPgmfComponent,
  },

  {
    path: 'planificacion/lineamientos-pgmf/:idEval',
    component: LineamientosEvaluacionPgmfComponent,
  },

  {
    path: 'planificacion/bandeja-PGMF',
    component: BandejaPGMFComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/bandeja-POCC',
    component: BandejaPOCCComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/ampliacion-solicitudes',
    component: ConsultaSolicitudesComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/ampliacion-solicitud',
    component: ConsultaSolicitudComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/consulta-oposicion',
    component: OposicionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-postulacion/oposicion',
    component: ListaOposicionComponent,
    canActivate: [AuthGuard],
  },

  /*******************permisos forestales***************/
  {
    path: 'planificacion/bandeja-permisos-forestales',
    component: BandejaPermisosForestales,
    canActivate: [AuthGuard],
  },
  // {
  //   path: 'planificacion/revision-permisos-forestales/:action/:idSolicitudAcceso',
  //   component: RevisionPermisosForestales, canActivate: [AuthGuard]
  // },
  {
    path: 'planificacion/revision-permisos-forestales/:idPlan',
    component: RevisionPermisosForestales,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/revision-permisos-forestales/:action/:idSolicitudAcceso/:idSolicitud',
    component: RevisionPermisosForestales,
    canActivate: [AuthGuard],
  },
  /*{
    path: 'planificacion/plan-manejo-forestal-intermedio',
    component: PlanManejoForestalIntermedioComponent,
    canActivate: [AuthGuard],
  },*/
  {
    path: 'planificacion/plan-manejo-forestal-intermedio/:idPlan',
    component: DetallePlanManejoForestalIntermedioComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/generar-resolucion',
    component: GenerarResolucionComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/solicitud-permiso-forestal',
    component: SolicitudPermisoForestalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluar-permiso-forestal/:action/:idSolicitudAcceso',
    component: EvaluarPermisoForestalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluar-permiso-forestal/:action/:idSolicitudAcceso/:idSolicitud',
    component: EvaluarPermisoForestalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/notificacion-otorgamiento-permiso-detalle',
    component: NotificacionOtorgamientoPermisoDetalleComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/solicitud-permiso-forestal-ccnn',
    component: SolicitudPermisoForestalCCNNComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluar-permiso-forestal-ccnn/:action/:idSolicitudAcceso',
    component: EvaluarPermisoForestalCCNNComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluar-permiso-forestal-ccnn/:action/:idSolicitudAcceso/:idSolicitud',
    component: EvaluarPermisoForestalCCNNComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/resultado-evaluacion-solicitud-otorgamiento-detalle',
    component: ResultadoEvaluacionSolicitudOtorgamientoDetalleComponent,
    canActivate: [AuthGuard],
  },

  /********************* Contrato de Concesion PFDM por Concesion Direacta***********************/
  {
    path: 'planificacion/bandeja-contrato-concesion',
    component: BandejaContratoConcesionPfdmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-contrato-concesion-postulacion',
    component: BandejaContratoConcesionPostulacionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion-contrato-concesion',
    // component: EvaluacionContratoConcesionComponent
    component: BandejaEvaluacionTecnicaComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion-tecnica',
    component: EvaluacionContratoConcesionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/generar-resolucion-evaluacion-tecnica',
    component: GenerarResolucionEvaluacionTecnica,
    canActivate: [AuthGuard],
  },
  /*******************plan operativo para Concesiones Maderables***************/

  {
    path: 'planificacion/plan-operativo-concesiones-maderables/:idPlan',
    component: PlanOperativoConcesionesMaderablesComponent,
    canActivate: [AuthGuard],
  },

  /*******************plan operativo PMFI en cncc***************/
  /*{
    path: 'planificacion/plan-operativo-PMFI-cncc',
    component: PlanOperativoPMFICnccComponent,
    canActivate: [AuthGuard],
  },*/
  {
    path: 'planificacion/plan-operativo-pmfi-cncc/:idPlan',
    component: DetallePlanOperativoPMFICnccComponent,
    canActivate: [AuthGuard],
  },

  /*******************contrato***************/
  {
    path: 'planificacion/generacion-contrato',
    component: GeneracionContratoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/generacion-contrato-resultado',
    component: DetalleResultadoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-contrato',
    component: BandejaContratoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-solicitud-contrato',
    component: BandejaSolicitudContratoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion-recurso-inpugnacion',
    component: EvaluacionRecursoInpugnacionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/solicitud-recurso-inpugnacion',
    component: SolicitudRecursoInpugnacionComponent,
    canActivate: [AuthGuard],
  },

  /******************* BANDEJA PROCESO CONCESION PFDM ***************/
  {
    path: 'planificacion/bandeja-proceso-concesion-pfdm',
    component: BandejaProcesoConcesionPfdmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluacion-tecnica-proceso-concesion-pfdm',
    component: EvaluacionContratoConcesionPfdmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/generar-resolucion-proceso-concesion-pfdm',
    component: GenerarResolucionProcesoConcesionPfdmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/evaluar-otorgar-proceso-concesion-pfdm',
    component: EvaluarOtorgarProcesoConcesionPfdmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-otorgamiento-pfdm',
    component: BandejaOtorgamientoPfdmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/generar-resolucion-otorgamiento-pfdm',
    component: GenerarResolucionOtorgamientoPfdmComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/generacion-contrato-concesion',
    component: GeneracionContratoConcesionComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/generacion-contrato-resultado-concesion',
    component: DetalleResultadoConcesionComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/envio-informacion-otorgamiento',
    component: EnvioInformacionOtorgamientoComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/contrato-elaboracion-pgmf',
    component: ContratoElaboracionPgmfComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/contrato-elaboracion-pgmf/:idPlan',
    component: ContratoElaboracionPgmfComponent,
    canActivate: [AuthGuard],
  },

  /******************* PGMF ***************/

  {
    path: 'planificacion/bandeja-pgmf-consolidado',
    component: BandejaPgmfConsolidadoComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-pgmf-consolidado/nuevo',
    component: ContratoElaboracionPgmfComponent,
    canActivate: [AuthGuard],
  },

  /******************* Plan Operativo ***************/

  {
    path: 'planificacion/bandeja-plan-operativo',
    component: BandejaPlanOperativo,
    canActivate: [AuthGuard],
  },
  /******************* Generar la Declaración de Manejo (DEMA) ******************* */

  {
    path: 'planificacion/generar-declaracion-manejo-dema/:idPlan',
    component: GeneracionDeclaracionManejoDemaComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/bandeja-generacion-dema',
    component: BandejaGeneracionDema,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  {
    path: 'planificacion/formulacion-PMFI/:idPlan',
    component: FormulacionPMFIConcesionPFDMComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/bandeja-pmfi',
    component: BandejaPmfi,
    canActivate: [AuthGuard],
  },

  /******************* Evaluación Campo ***************/

  {
    path: 'planificacion/bandeja-evaluacion-campo',
    component: BandejaEvaluacionCampoComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'planificacion/registro-evaluacion-campo/:idEvaluacionCampo',
    component: RegistroEvaluacionCampoComponent,
    canActivate: [AuthGuard],
  },

  /******************* Evaluación Pgmf ***************/

  {
    path: 'planificacion/bandeja-evaluacion-pgmf',
    component: BandejaEvaluacionPgmfComponent,
    canActivate: [AuthGuard],
  },

  /******************* Solicitud Opinión ***************/

  {
    path: 'planificacion/bandeja-solicitud-opinion',
    component: BandejaSolicitudOpinionComponent,
    canActivate: [AuthGuard],
  },

  /************************** Impugnación ************************* */
  {
    path: 'planificacion/bandeja-solicitud-impugnacion',
    component: BandejaSolicitudComponent,
  },
  {
    path: 'planificacion/bandeja-evaluacion-impugnacion',
    component: BandejaEvaluacionImpugnacionComponent,
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'requisitos-preliminares/gestion-evaluacion/:idPlan',
    component: GestionEvaluacionPlanManejoComponent,
  },
  {
    path: 'requisitos-preliminares/gestion-evaluacion-plan-general/:idPlan',
    component: GestionEvaluacionPlanOperativoComponent,
  },

  /*******************Derecho de Aprovechamiento***************/
  {
    path: 'DerechoAprovechamiento/BandejaDA',
    component: MecanismoPagoComponent,
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'DerechoAprovechamiento/RegistroDA/:idPago',
    component: RegistroMecanismoPagoComponent,
  },

  /*******************Regimen Promocional***************/
  {
    path: 'regimen-promocional/beneficioTH/bandeja-beneficioTH/bandeja-beneficio-th',
    component: BandejaBeneficioTHComponent,
  },
  {
    path: 'regimen-promocional/solicitud-beneficio/bandeja-solicitud-beneficio/bandeja-solicitud-beneficio',
    component: BandejaSolicitudBeneficioComponent,
  },
  {
    path: 'regimen-promocional/solicitud-beneficio/registro-solicitud-beneficio/registro-solicitud-beneficio',
    component: RegistroSolicitudBeneficioComponent,
  },

  /*******************MODULO CONCESION***************/
  /********Solicitud concesion PFDM********/
  {
    path: 'concesion/solicitud-concesion-PFDM',
    component: SolicitudConcesionPFDMComponent, canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'concesion/solicitud-contrato-concesion/:idParam',
    component: RegistroSolicitudConcesionPfdmComponent, canActivate: [AuthGuard],
  },
  {
    path: 'concesion/calificacion-concesion-PFDM',
    component: CalificacionConcesionPFDMComponent, canActivate: [AuthGuard],
  },
  {
    path: 'concesion/calificacion-propuesta-concesion/:idParam',
    component: EvaluarPropuestaCalificacionConcesionPfdmComponent, canActivate: [AuthGuard],
  },
  {
    path: 'concesion/evaluacion-propuesta/:idParam',
    component: EvaluacionPropuestaComponent, canActivate: [AuthGuard],
  },
  {
    path: 'concesion/formato-propuesta/:idParam',
    component: AnexoFormatoPropuestaComponent, canActivate: [AuthGuard],
  },

  /*******************MODULO PLANTACIONES***************/
    //Bandeja Solicitud Plantaciones
  {
    path: 'plantacion/bandeja-plantacion-forestal',
    component: BandejaPlantacionForestalComponent, 
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'plantacion/registro-plantacion-forestal',
    component: RegistroPlantacionForestalComponent, canActivate: [AuthGuard],
  },
  {
    path: 'plantacion/registro-plantacion-forestal/:idParam',
    component: RegistroPlantacionForestalComponent, canActivate: [AuthGuard],
  },
    //Fiscalización Plantacion Forestal
  {
    path: 'plantacion/fiscalizacion-plantacion-forestal',
    component: FiscalizacionPlantacionForestalComponent, canActivate: [AuthGuard],
  },
  {
    path: 'plantacion/evaluacion-campo-plantacion-forestal/:idParam',
    component: EvaluacionCampoFpfComponent, canActivate: [AuthGuard],
  },
  {
    path: 'plantacion/resultado-evaluacion'
    ,component: ResultadosEvaluacionComponent
    //,canActivate: [AuthGuard],
  },

  /*******************MODULO BOSQUE LOCAL***************/
  /********Bandeja establecimiento********/
  {
    path: 'bosque-local/bandeja-establecimiento-BL',
    component: BandejaEstablecimientoBlComponent, 
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'bosque-local/bandeja-evauacion-comite-BL',
    component: BandejaEvaluacionComiteBlComponent, 
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'bosque-local/registro-establecimiento-BL',
    component: RegistroEstablecimientoBlComponent, canActivate: [AuthGuard],
  },
  {
    path: 'bosque-local/registro-establecimiento-BL/:idParam',
    component: RegistroEstablecimientoBlComponent, canActivate: [AuthGuard],
  },
  {
    path: 'bosque-local/registro-comite-tecnico-BL',
    component: RegistroComiteTecnicoBlComponent, canActivate: [AuthGuard],
  },
  {
    path: 'bosque-local/registro-comite-tecnico-BL/:idParam',
    component: RegistroComiteTecnicoBlComponent, canActivate: [AuthGuard],
  },
  {
    path: 'bosque-local/evaluacion-comite-tecnico-BL',
    component: EvaluacionComiteTecnicoComponent, canActivate: [AuthGuard],
  },
  {
    path: 'bosque-local/evaluacion-comite-tecnico-BL/:idParam',
    component: EvaluacionComiteTecnicoComponent, canActivate: [AuthGuard],
  },

  /*******************MODULO LIBRO DE OPERACIONES***************/
  /********Bandeja Libro de Operaciones********/
  {
    path: 'aprovechamiento/bandeja-libro-operaciones',
    component: BandejaLibroOperacionesComponent, 
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  /********Evaluación Libro de Operaciones********/
  {
    path: 'aprovechamiento/evaluacion-libro-operaciones',
    component: EvaluacionLibroOperacionesComponent, 
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  /********Sincronización********/
  {
    path: 'aprovechamiento/sincronizacion',
    component: SincronizacionComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'aprovechamiento/detalle-sincronizacion',
    component: DetalleSincronizacionComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },
  {
    path: 'aprovechamiento/detalle-sincronizacion/:idParam',
    component: DetalleSincronizacionComponent,
    canActivate: [AuthGuard],
    resolve: {permisos: PermisosOpcionResolver}
  },

  /********************** Plan Manejo ****************************/
  {
    path: 'planificacion/bandeja-pmp-dema'
    ,component: BandejaPlanManejoSharedComponent
    //,canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-pmp-pmfi'
    ,component: BandejaPlanManejoSharedComponent
    //,canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-pmp-po-pmfi'
    ,component: BandejaPlanManejoSharedComponent
    //,canActivate: [AuthGuard],
  },
  {
    path: 'planificacion/bandeja-pmc-dema'
    ,component: BandejaPlanManejoSharedComponent
    //,canActivate: [AuthGuard],
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
