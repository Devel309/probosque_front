export enum varLocalStor {
  BJA_PLANT_FORE = "BPFData",
  BJA_SOL_CONCESION = "SCPFDMdata",
  BJA_SOL_BL = "SOLBLdata",
  BJA_SOL_EEVAL_CT_BL = "SOLEvalCTBLdata",  //Evaluacion Comité Técnico - Bosque Local
  PERMISO_OPCION = "PermisoOpcion", //guardar opcion seleccionada del menu
}
