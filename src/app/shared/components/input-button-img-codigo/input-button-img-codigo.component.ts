import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import {
  descargarArchivo,
  DowloadFileLocal,
  DownloadFile,
  ToastService,
} from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { FileModel } from "src/app/model/util/File";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";

@Component({
  selector: "input-button-img-codigo",
  templateUrl: "./input-button-img-codigo.component.html",
  styleUrls: ["./input-button-img-codigo.component.scss"],
})
export class InputButtonImgCodigoComponent implements OnInit {
  @Input() idPlanManejo!: number;

  @Input() file: any = {};
  @Input() label: string = "";
  @Input() disabled: boolean = false;
  @Input() accept: string = "";
  @Input() idArchivoModal: string = "";

  @Input() idTipoDocumento!: any;
  @Input() codigoProceso!: string;
  @Input() disabledDescarga: boolean = false

  @Output() idArchivoEmit = new EventEmitter();

  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;

  /*static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }*/
  static get EXTENSIONSAUTHORIZATION2() {
    return ["image/png", "image/jpg"];
  }

  constructor(
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private toast: ToastService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";

    if(this.idArchivoModal){
      this.idArchivo = Number(this.idArchivoModal?this.idArchivoModal:0);
    }
    this.listarArchivoPMFI();

  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;
        if (type === "PDF") {
          include = InputButtonImgCodigoComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El tipo de Documento no es Válido.",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB.",
          });
        } else {
          if (type === "PDF") {
            this.fileInfGenreal.url = URL.createObjectURL(e.target.files[0]);
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.fileInfGenreal);
            this.guardarArchivoGeneral();
          }
        }
      }
    }
  }

  guardarArchivoGeneral() {
    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: this.idTipoDocumento,
        };

        let load= this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => load.close()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
      }
    });
  }

  registrarArchivo(id: number) {
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: this.codigoProceso,
      descripcion: "",
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: "",
    };

    this.idArchivo = id;
    this.registrarArchivoId.emit(this.idArchivo);

    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se cargó el archivo correctamente.");
        this.listarArchivoPMFI();
      } else {
        this.toast.error("Ocurrió un error al realizar la operación.");
      }
    });


  }

  listarArchivoPMFI() {

    var params = {
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: this.idTipoDocumento,
      codigoProceso: this.codigoProceso,
      idArchivo: this.idArchivo
    };

    // this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .listarPlanManejoArchivo(params)
      // .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result) {
          this.cargarArchivo = true;
          this.eliminarArchivo = false;
          this.fileInfGenreal.nombreFile = result.nombre;
          /*this.idArchivo = result.idArchivo;
          this.registrarArchivoId.emit(this.idArchivo);*/
        } else {
          this.eliminarArchivo = true;
          this.cargarArchivo = false;
        }
      });
  }

  eliminarArchivoGeneral() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivo))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó  el Archivo correctamente");
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = "";
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.idArchivo) {
      let params = {
        idArchivo: this.idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
