import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonImgCodigoComponent } from './input-button-img-codigo.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonImgCodigoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonImgCodigoComponent],
})
export class InputButtonImgCodigoModule {}
