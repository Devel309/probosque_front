import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonsZipCodigoComponent } from './input-buttons-zip.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonsZipCodigoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonsZipCodigoComponent],
})
export class InputButtonsZipCodigoModule {}
