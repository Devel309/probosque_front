import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoadingComponent} from '../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {ArchivoService, UsuarioService} from '@services';
import {MatDialog} from '@angular/material/dialog';
import {AmpliacionSolicitudService} from '../../../service/ampliacion-solicitud/ampliacion-solicitud.service';
import {ImpugnacionSan} from '../../../model/Impugnacion/ImpugnacionSan';
import {DownloadFile, ToastService} from '@shared';
import {ImpugnacionSanService} from '../../../service/planificacion/permiso-forestal/impugnacionSan.service';
import {CodigosTiposSolicitud} from '../../../model/util/CodigosTiposSolicitud';
import {CodigoProceso} from '../../../model/util/CodigoProceso';
import {CodigoPermisoForestal} from '../../../model/util/CodigoPermisoForestal';
import {PermisoForestalService} from '../../../service/permisoForestal.service';
import {CodigoEstadoPlanManejo} from '../../../model/util/CodigoEstadoPlanManejo';
import {EvaluacionService} from '../../../service/evaluacion/evaluacion.service';

@Component({
  selector: 'evaluar-impugnacion-shared',
  templateUrl: './evaluar-impugnacion-shared.component.html',
  styleUrls: ['./evaluar-impugnacion-shared.component.scss']
})
export class EvaluarImpugnacionSharedComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() estado!: string;
  @Input() codigoPlan!: string;
  @Output() registroImpugnacionEmiter: EventEmitter<number> = new EventEmitter();
  pdfSrc :string = "";
  isShowModal2_2:boolean=false;

  idTipoSolicitud:number = 0;
  idProcesoPostulacion:number=0;
  autoResize: boolean = true;
  idArchivoSAN: number = 0;
  idArchivoSANFirmado: number = 0;
  idArchivoSANInforme: number = 0;
  showFormatoSan:boolean = false;

  CodigosTiposSolicitud = CodigosTiposSolicitud;

  //codigoProceso :string = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;

  /*codigoArchivoSan :string = "PFCRIMP";
  codigoArchivoSanFirmado :string = "PFCRIMPFIRMA";
  codigoArchivoInforme :string = "PFCRIMPINFO";*/
  codigoArchivoSan :string = "";
  codigoArchivoSanFirmado :string = "";
  codigoArchivoInforme :string = "";

  impugnacionSan:ImpugnacionSan = new ImpugnacionSan({
    idSolicitudSAN:0,
    //idPermisoForestal:this.idPermisoForestal,
    nroGestion: this.idPlanManejo,
    codSolicitudSAN : this.codigoPlan,//this.codigoImpugnacion,
    subCodSolicitudSAN : CodigosTiposSolicitud.IMPUGNACION_SAN,
    tipoSolicitud : 'IMPUGNACIÓN AL SAN',
    tipoPostulacion : "PLAN",
    idUsuarioRegistro: this.usuariServ.idUsuario,
    diasAdicionales :0
  });

  tipoSolicitud: any[] = [];

  idArchivoEvidencia: number = 0;

  flagBloqueoDias :boolean = false;

  codigoArchivoEvidencia: string = "";

  constructor(private archivoService: ArchivoService,
              private dialog: MatDialog,
              private ampliacionSolicitudService: AmpliacionSolicitudService,
              private usuariServ: UsuarioService,private evaluacionService: EvaluacionService,
              private impugnacionSanService : ImpugnacionSanService,private toast: ToastService,private permisoForestalService : PermisoForestalService) {

  }

  ngOnInit(): void {
    this.codigoArchivoSan        = this.codigoPlan + "SAN";
    this.codigoArchivoSanFirmado = this.codigoPlan + "IMPFIRMA";
    this.codigoArchivoInforme    = this.codigoPlan + "IMPINFO";
    this.codigoArchivoEvidencia = this.codigoPlan + "EVID";
  }

  cargarEvidencia(idArchivo: number) {
    if (idArchivo !=null && idArchivo > 0) {
      this.idArchivoEvidencia = idArchivo;

    }
  }



  editarCalendario() {
    if(!this.validacionDiasCalendario()) return;
    let params: any[] = [];
    this.impugnacionSan.nroGestion = this.idPlanManejo;
    //this.impugnacionSan.idArchivo = this.idArchivo;
    this.impugnacionSan.codSolicitudSAN= this.codigoPlan;

    params.push(this.impugnacionSan);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionSanService
      .registrarSolicitudSAN(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok('Se actualizó los días adicionales a la solicitud');
          this.listarImpugnacion();
        } else {
          this.toast.error(res?.message);
        }
      });
  }


  cargarIdArchivo(idArchivo: any) {
    this.idArchivoSAN = idArchivo;
  }

  cargarIdArchivo2(idArchivo: any) {
    this.idArchivoSANFirmado = idArchivo;
  }

  cargarIdArchivo3(idArchivo: any) {
    this.idArchivoSANInforme = idArchivo;
  }

  /*listarTipoSolicitud () {
    this.ampliacionSolicitudService.listarTipoSolicitud()
      .subscribe((resp: any) => {
        this.tipoSolicitud = resp.data;
      });
  };*/


  getSolicitudSAN(){
    if(this.estado == CodigoEstadoPlanManejo.IMPUGNADO_SAN || this.estado == CodigoEstadoPlanManejo.PRESENTADO){
      this.isShowModal2_2 = true;
      this.showFormatoSan = true;
      //this.listarTipoSolicitud();
      this.listarImpugnacion();
    }
  }

  listarImpugnacion() {
    let param = {
      //idPermisoForestal: this.idPlanManejo,
      nroGestion:this.idPlanManejo,
      pageNum: 1,
      pageSize: 1,
    };

    this.impugnacionSanService
      .listarSolicitudSAN(param)
      .subscribe((res: any) => {
        if (res.success == true) {
          if (res.data) {
            if (res.data.length > 0) {
              let response = res.data[0];
              this.impugnacionSan.asunto = response.asunto;
              this.impugnacionSan.descripcion = response.descripcion;
              this.impugnacionSan.codSolicitudSAN = response.codSolicitudSAN;
              //this.impugnacionSan.idPermisoForestal = response.idPermisoForestal;
              this.impugnacionSan.nroGestion = response.nroGestion;
              this.impugnacionSan.idSolicitudSAN = response.idSolicitudSAN;
              this.impugnacionSan.procesoPostulacion = response.procesoPostulacion;
              this.impugnacionSan.diasRestantesPlan = response.diasRestantesPlan;
              if(res.diasAdicionales != null){
                this.impugnacionSan.diasAdicionales = response.diasAdicionales;
              }

              if( this.impugnacionSan.diasRestantesPlan !=null ) {

                this.flagBloqueoDias = true;
                if(this.impugnacionSan.diasRestantesPlan<=0)
                  this.flagBloqueoDias = false;//activa el editar calendario

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan == 50)
                  this.toast.ok("Falta 50 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==40)
                  this.toast.ok("Falta 40 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==30)
                  this.toast.ok("Falta 30 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==20)
                  this.toast.ok("Falta 20 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==10)
                  this.toast.ok("Falta 10 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==5)
                  this.toast.ok("Falta 5 días para resolver las observaciones.");

              } else {

                this.flagBloqueoDias = true;

              }

            }
          }
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  /*listarImpugnacion(){
    let param = {
      idPermisoForestal:this.idPlanManejo,
      pageNum:1,
      pageSize:1
    }

    this.impugnacionSanService.listarSolicitudSAN(param)
      .subscribe((res: any) => {
        if (res.success == true) {
          if (res.data){
           if(res.data.length >0){

             let response = res.data[0];
             this.impugnacionSan.asunto = response.asunto;
             this.impugnacionSan.descripcion = response.descripcion;
             this.impugnacionSan.codSolicitudSAN = response.codSolicitudSAN;
             this.impugnacionSan.idPermisoForestal = response.idPermisoForestal;
             this.impugnacionSan.idSolicitudSAN = response.idSolicitudSAN;
             this.impugnacionSan.procesoPostulacion = response.procesoPostulacion;
             this.impugnacionSan.diasRestantes = response.diasRestantes;
           }
          }
        } else {
          this.toast.error(res?.message);
        }
      });


  }*/

  validarEnvio(){

    let resul :boolean = true;
    if(this.impugnacionSan.detalle == null){
      this.toast.warn('Debe seleccionar el resultado para la solicitud : Procede o No Procede.');
      resul = false;

    }

    if(this.impugnacionSan.observacion == null){
      this.toast.warn('Debe ingresar una observación');
      resul = false;
    }

    if(this.idArchivoSANFirmado == 0){
      this.toast.warn('Debe seleccionar un archivo para el Formato de Resolución Firmado');
      resul = false;
    }

    if(this.idArchivoSANInforme == 0){
      this.toast.warn('Debe seleccionar un archivo Carga de Informe');
      resul = false;
    }

    return resul;
  }

  validacionDiasCalendario() {

    let validar: boolean = true;

    if (this.impugnacionSan.diasAdicionales == 0 || this.impugnacionSan.diasAdicionales == null) {
      validar = false;
      this.toast.warn('(*) Debe ingresar: Días adicionales.');
    }

    if (this.idArchivoEvidencia == null || this.idArchivoEvidencia == 0) {
      validar = false;
      this.toast.warn('(*) Debe cargar un Archivo Evidencia.\n');
    }
    //if (!validar) this.ErrorMensaje(mensaje);
    return validar;
  }



  guardarImpugnacion() {

  if(this.validarEnvio()){

    let params:any[]= [];
    this.impugnacionSan.nroGestion= this.idPlanManejo;

    params.push(this.impugnacionSan);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionSanService.registrarSolicitudSAN(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {

          let estado = CodigoEstadoPlanManejo.NO_PROCEDE;
          if(this.impugnacionSan.detalle == 'PROCEDE'){
            estado = CodigoEstadoPlanManejo.PROCEDE;
          }

          let param = {
            idPlanManejo: this.idPlanManejo,
            codigoEstado: estado,
            idUsuarioModificacion: this.usuariServ.idUsuario,
          };

          this.evaluacionService.actualizarPlanManejo(param)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok('Se registró la resolución impugnación SAN correctamente.');
                this.isShowModal2_2 = false;
                this.registroImpugnacionEmiter.emit(1);
              } else {
                this.toast.error(response?.message);
              }
            });

        } else {
          this.toast.error(res?.message);
        }
      });
  }
  }



}


