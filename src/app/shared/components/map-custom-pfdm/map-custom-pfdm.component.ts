import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SolicitudConcesionGeometriaModel } from 'src/app/model/Concesion/SolicitudConcesionGeometria';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { SolicitudConcesionGeometriaService } from 'src/app/service/concesion/solicitud-concesion-geometria.service';
import { SolicitudConcesionService } from 'src/app/service/concesion/solicitud-concesion.service';
import { MapApi } from '../../mapApi';
import { DownloadFile } from '../../util';

@Component({
  selector: 'app-map-custom-pfdm',
  templateUrl: './map-custom-pfdm.component.html',
  styleUrls: ['./map-custom-pfdm.component.scss']
})
export class MapCustomPFDMComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private serviceSolicitudConcesionGeometria: SolicitudConcesionGeometriaService,
    private serviceSolicitudConcesion: SolicitudConcesionService,
    private serviceGeoforestal: ApiGeoforestalService,
    private serviceExternos: ApiForestalService) { }

  @ViewChild('map2', { static: true }) private mapViewEl!: ElementRef;
  @Input() idPFDM!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() tipoArchivoArea!: string;
  @Input() tipoArchivoGeneral!: string;
  @Input() tipoArchivoPunto!: string;
  @Input() isAnexo: boolean = false;
  @Input() printPDF: boolean = false;
  @Input() showBotonCargarGeneral: boolean = true;
  @Input() showBotonCargarCustom: boolean = false;
  @Input() showBotonGuardar: boolean = true;
  @Input() identificarBosque: boolean = false;
  @Input() validaSuperposicionOtorgar: boolean = false;
  @Input() validaSuperposicionAprovechar: boolean = false;
  @Input() calcularArea: boolean = false;
  @Input() isDisbledFormu: boolean = false;
  @Output() areaTotal = new EventEmitter<number>();
  @Output() listVertices = new EventEmitter<any>();
  public view: any = null;
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
  public _layers: CustomCapaModel[] = [];
  _listaCapas: [] = [];
  TPAREA = "TPAREA";
  TPPUNTO = "TPPUNTO";
  solicitudConcesionGeometria: SolicitudConcesionGeometriaModel[] = [];
  showMensajeSuperposicion: boolean = false;

  ngOnInit(): void {
    if (this.printPDF === true) {
      this.initializeMapCustom();
    } else {
      this.initializeMap();
    }
    this.obtenerCapas();
  }
  initializeMapCustom() {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMapCustom(container);
    this.view = view;
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)))
      .pipe(
        concatMap((result: any) => this.guardarCapa(item, result.data.idArchivo)));
  }
  saveFileRelation(result: any, item: any) {
    let item2 = [{
      idSolicitudConcesionArchivo: 0,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
      idArchivo: result.data,
      idSolicitudConcesion: this.idPFDM,
      tipoArchivo: item.tipoArchivo,
      idUsuarioRegistro: this.user.idUsuario,
    }];
    return this.serviceSolicitudConcesion.registrarInfoAdjuntosSolicitudConcesion(item2)
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.solicitudConcesionGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item: any = {
        idSolicitudConcesionGeometria: 0,
        idSolicitudConcesion: this.idPFDM,
        idArchivo: idArchivo,
        tipoGeometria: t3.tipo,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.solicitudConcesionGeometria.push(item);
    });
    return this.serviceSolicitudConcesionGeometria.registrarGeometria(
      this.solicitudConcesionGeometria
    );
  }
  onChangeFile(e: any, tipo: string, tipoArchivo: string) {
    e.preventDefault();
    e.stopPropagation();
    if (tipo === this.TPAREA) {
      let item = this._layers.find(
        (e: any) => e.descripcion == this.TPAREA
      );
      if (item) {
        this.toast.warn('Ya existe un archivo de área');
        return;
      }
    } else if (tipo === this.TPPUNTO) {
      let item = this._layers.find(
        (e: any) => e.descripcion == this.TPPUNTO
      );
      if (item) {
        this.toast.warn('Ya existe un archivo de vértices');
        return;
      }
    }
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      tipo: tipo,
      tipoArchivo: tipoArchivo
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            this.toast.warn(controls.message);
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  onGuardarLayer() {
    let codigoTipo: any = this.codigoProceso;
    let fileUpload: any = [];
    let validateOverlap: any = false;
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: codigoTipo,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    this._layers.forEach((t: any) => {
      if (t.overlap === true) {
        validateOverlap = true;
      }
    });

    if (fileUpload.length === 0) {
      this.toast.warn('Cargue un archivo para continuar.');
      return;
    }
    if (validateOverlap === true) {
      this.confirmationService.confirm({
        message: 'El Área está superpuesta ¿Desea continuar?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.saveLayer(fileUpload);
        }
      });
    } else {
      this.saveLayer(fileUpload);
    }
  }
  saveLayer(fileUpload: any) {
    let observer = from(fileUpload);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => {
        this.dialog.closeAll();
        this.cleanLayers();
        this.obtenerCapas();
      }))
      .subscribe(
        (result: any) => {
          this.toast.ok(result.message);
        },
        (error) => {
          this.toast.error('Ocurrió un error. por favor comunique al Administrador');
        }
      );
  }
  obtenerCapas() {
    let item = null;

    if (this.isAnexo === true) {
      item = {
        idSolicitudConcesion: this.idPFDM,
      };
    } else {
      item = {
        idSolicitudConcesion: this.idPFDM,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
      };
    }
    this.serviceSolicitudConcesionGeometria.listarGeometria(item).subscribe(
      (result: any) => {
        if (result.data.length) {
          this._listaCapas = result.data;
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              let layer: any = {} as CustomCapaModel;
              layer.codigo = t.idArchivo;
              layer.idLayer = this.mapApi.Guid2.newGuid;
              if (this.isAnexo === true) {
                layer.inServer = false;
                layer.service = true;
              } else {
                layer.inServer = true;
                layer.service = false;
              }
              layer.nombre = t.nombreCapa;
              layer.groupId = groupId;
              layer.color = t.colorCapa;
              layer.annex = false;
              layer.descripcion = t.tipoGeometria;
              this._layers.push(layer);
              let geoJson = this.mapApi.getGeoJson(
                layer.idLayer,
                groupId,
                item
              );
              if (t.tipoGeometria === this.TPPUNTO && this.showBotonCargarCustom === true) {
                this.obtenerVerticesMultiPoint(geoJson);
              }
              this.createLayer(geoJson);
            }
          });
        }
      },
      (error: any) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
      (error: HttpErrorResponse) => {
        this.toast.error(error.message);
        this.dialog.closeAll();
      };
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._filesSHP.length === 0) {
                  this.cleanLayers();
                  this.obtenerCapas();
                }
                this.toast.ok('El archivo se eliminó correctamente.');
              } else {
                this.toast.error('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error: any) => {
              this.dialog.closeAll();
              this.toast.error('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
        this.obtenerCapas();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    let item = {
      idArchivo: idArchivo,
      idUsuarioElimina: this.user.idUsuario
    }
    return this.serviceSolicitudConcesionGeometria.eliminarGeometriaArchivo(item);
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (this.calcularArea === true && config.tipo === this.TPAREA) {
        this.calculateArea(data[0]);
      } else if (config.tipo === this.TPPUNTO) {
        this.obtenerVertices(data[0]);
      }
      if (this.validaSuperposicionAprovechar === true && config.tipo === this.TPAREA) {

        let coordinates: any = [];
        data[0].features.forEach((t: any) => {
          coordinates.push(t.geometry.coordinates[0]);
        });
        this.servicioSuperposicionPlanificacion(coordinates);
      }
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.tipo = config.tipo;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer: any = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.tipo;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.tipoArchivo = config.tipoArchivo;
    this._filesSHP.push(file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.tipo = item.tipo;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
    this._filesSHP = [];
  }
  calculateArea(data: any) {
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    let areaTotal = Math.abs(sumArea.toFixed(3));
    this.areaTotal.emit(areaTotal);
    return areaTotal;
  }
  obtenerVertices(data: any) {
    let listVertices: any = [];
    data.features.forEach((t: any, index: any) => {
      listVertices.push({
        vertice: index + 1,
        este: t.geometry.coordinates[0],
        norte: t.geometry.coordinates[1],
      });
    });
    this.listVertices.emit(listVertices);
  }
  obtenerVerticesMultiPoint(data: any) {
    let listVertices: any = [];
    data.features[0].geometry.coordinates.forEach((t: any, index: any) => {
      listVertices.push({
        vertice: index + 1,
        este: t[0],
        norte: t[1],
      });
    });
    this.listVertices.emit(listVertices);
  }
  servicioSuperposicionPlanificacion(geometry: any) {
    let index = this._layers.findIndex(
      (t: any) => t.overlap === true
    );
    if (index >= 0) {
      this._layers.splice(index, 1);
    }

    let params = {
      codProceso: '110102',
      geometria: {
        poligono: geometry,
      },
    };
    this.showMensajeSuperposicion = true;
    this.serviceExternos
      .validarSuperposicionPlanificacion(params)
      .subscribe((result: any) => {
        this.showMensajeSuperposicion = false;
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            let item = this._layers.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof item === 'object') return;

            if (t.seSuperpone === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.included = false;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.overlap = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layers.push(layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        } else {
          this.toast.info('No se encontró áreas superpuestas');
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            let item = this._layers.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof item === 'object') return;
            if (t.contenida === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layers.push(layer);

                this.createLayer(t.geoJson);
              }
            }
          });
        }
      }, (error: HttpErrorResponse) => {
        this.showMensajeSuperposicion = false;
        this.toast.error(error.message);
      });
  }
}
