import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { EvaluacionSimpleModule } from '../evaluacion-simple/evaluacion-simple.module';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { RequisitoPgmfSimpleComponent } from './requisito-pgmf-simple.component';
import { AccordionModule } from 'primeng/accordion';



@NgModule({
  declarations: [RequisitoPgmfSimpleComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material angular
    RadioButtonModule,
    TableModule,
    EvaluacionSimpleModule,
    PanelModule,
    DropdownModule,
    AccordionModule
  ],
  exports:[RequisitoPgmfSimpleComponent]
})
export class RequisitoPgmfSimpleModule { }
