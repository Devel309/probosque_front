import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from "@angular/core";
import { LineamientoInnerModel } from "src/app/model/Comun/LineamientoInnerModel";

@Component({
  selector: "requisito-pgmf-simple-shared",
  templateUrl: "./requisito-pgmf-simple.component.html",
  styleUrls: ["./requisito-pgmf-simple.component.scss"],
})
export class RequisitoPgmfSimpleComponent implements OnInit {
  @ContentChild("conatinerinner") conatinerinnerRef!: TemplateRef<any>;
  @Input("base") base: RequisitoSimpleModel = new RequisitoSimpleModel();
  @Input() default: boolean = true;
  @Input() disabled!: boolean;
  @Output() onChange: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  evaluation(conforme: any) {
    this.onChange.emit(conforme);
  }
}

export class RequisitoSimpleModel {
  constructor(data?: any) {
    if (data) {
      this.titulo = data.titulo;
      this.evaluacion = data.evaluacion;
      this.classEstado = data.classEstado;
      this.icon = data.icon;
      return;
    }
  }
  titulo: string = "";
  icon: string = "";
  classEstado: string = "req_pendiente_class";
  evaluacion: LineamientoInnerModel = new LineamientoInnerModel();
}
