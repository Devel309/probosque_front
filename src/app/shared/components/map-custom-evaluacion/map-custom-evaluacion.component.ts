import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { OgcGeometryType } from '../../enums';
import { MapApi } from '../../mapApi';
import { ToastService } from '../../services/toast.service';
import { DownloadFile } from '../../util';

@Component({
  selector: 'app-map-custom-evaluacion',
  templateUrl: './map-custom-evaluacion.component.html',
  styleUrls: ['./map-custom-evaluacion.component.scss']
})
export class MapCustomEvaluacionComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private user: UsuarioService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private toast: ToastService,
    private serviceExternos: ApiForestalService,
    private servicePlanificacion: PlanificacionService
  ) { }

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() isShow?: boolean = true;
  @Input() identificarBosque: boolean = false;
  @Input() validaSuperposicionOtorgar: boolean = false;
  @Input() validaSuperposicionAprovechar: boolean = false;
  @Input() isAnexo: boolean = false;
  @Input() printPDF: boolean = false;
  @Input() disabled!: boolean;

  view: any = null;
  _id = this.mapApi.Guid2.newGuid;
  _layers: CustomCapaModel[] = [];
  _layersCatastro: CustomCapaModel[] = [];
  showMensajeSuperposicion: boolean = false;
  objetoCC: any = {};

  ngOnInit(): void {
    if (this.printPDF === true) {
      this.initializeMapCustom();
    } else {
      this.initializeMap();
    }
    this.obtenerCapas();
    this.listarDivisionAdministrativa();
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '460px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  initializeMapCustom() {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMapCustom(container);
    this.view = view;
  }
  listarDivisionAdministrativa() {
    let item = {
      idPlanManejo: this.idPlanManejo
    }
    this.servicePlanificacion.listarDivisionAdministrativa(item).subscribe((response: any) => {
      this.objetoCC = response.data[0];
    });
  }
  obtenerCapas() {
    this.cleanLayers();
    this.cleanLayersService();
    let item = {
      idPlanManejo: this.idPlanManejo,
    };
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              if (t.tipoGeometria === 'TPPARC' || t.tipoGeometria === 'TPQUIN' || t.tipoGeometria === 'TPBLOQ') {
                if (t.codigoGeometria.toUpperCase() === OgcGeometryType.POLYGON) {
                  let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
                  let groupId = this._id;

                  let layer: any = {} as CustomCapaModel;
                  layer.codigo = t.idArchivo;
                  layer.idLayer = this.mapApi.Guid2.newGuid;
                  layer.inServer = true;
                  layer.service = false;
                  layer.nombre = t.nombreCapa;
                  layer.groupId = groupId;
                  layer.color = t.colorCapa;
                  layer.annex = false;
                  layer.descripcion = t.tipoGeometria;
                  layer.idsCatastro = t.idsCatastro;
                  layer.idPlanManejoGeometria = t.idArchivo;
                  let codProceso = '';
                  if (t.tipoGeometria === 'TPPARC') {
                    codProceso = '110102';
                  } else if (t.tipoGeometria === 'TPQUIN' || t.tipoGeometria === 'TPBLOQ') {
                    codProceso = '110101';
                  }
                  layer.codProceso = codProceso;
                  let properties = JSON.parse(t.propiedad)  || {}
                  if (typeof properties === 'object'){
                    properties = properties
                  }else {
                    properties = {}
                  }
                  let item = {
                    color: t.colorCapa,
                    title: t.nombreCapa,
                    jsonGeometry: jsonGeometry,
                    properties: properties,
                    service: layer.service,
                    tipoGeometria: t.tipoGeometria,
                    codProceso: codProceso,
                    idPlanManejoGeometria: layer.idPlanManejoGeometria,
                    idsCatastro: layer.idsCatastro
                  };
                  this._layers.push(layer);
                  let geoJson = this.mapApi.getGeoJsonCustom(
                    layer.idLayer,
                    groupId,
                    item
                  );
                  this.createLayer(geoJson);
                }
              }
            }

          });

          /* if (result.data.length) {
            //COD1
            const filtered1 = result.data.filter((t: any) => {
              return t.descripcion === "COD1" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered1, "TPAREA");
            const filteredPunto1 = result.data.filter((t: any) => {
              return t.descripcion === "COD1" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto1, "TPPUNTO");
            //COD2
            const filtered2 = result.data.filter((t: any) => {
              return t.descripcion === "COD2" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered2, "TPAREA");
            const filteredPunto2 = result.data.filter((t: any) => {
              return t.descripcion === "COD2" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto2, "TPPUNTO");
            //COD3
            const filtered3 = result.data.filter((t: any) => {
              return t.descripcion === "COD3" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered3, "TPAREA");
            const filteredPunto3 = result.data.filter((t: any) => {
              return t.descripcion === "COD3" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto3, "TPPUNTO");
            //COD4
            const filtered4 = result.data.filter((t: any) => {
              return t.descripcion === "COD4" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered4, "TPAREA");
            const filteredPunto4 = result.data.filter((t: any) => {
              return t.descripcion === "COD4" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto4, "TPPUNTO");
            //COD5
            const filtered5 = result.data.filter((t: any) => {
              return t.descripcion === "COD5" && t.tipoGeometria === "TPRUTA";
            });
            this.createFeature(filtered5, "TPAREA");
            const filteredPunto5 = result.data.filter((t: any) => {
              return t.descripcion === "COD5" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto5, "TPPUNTO");
            //COD6
            const filtered6 = result.data.filter((t: any) => {
              return t.descripcion === "COD6" && t.tipoGeometria === "TPHIDRO";
            });
            this.createFeature(filtered6, "TPAREA");
            const filteredPunto6 = result.data.filter((t: any) => {
              return t.descripcion === "COD6" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto6, "TPPUNTO");
            //COD7
            const filtered7 = result.data.filter((t: any) => {
              return t.descripcion === "COD7" && t.tipoGeometria === "TPFISIO";
            });
            this.createFeature(filtered7, "TPAREA");
            const filteredPunto7 = result.data.filter((t: any) => {
              return t.descripcion === "COD7" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto7, "TPPUNTO");
            //COD8
            const filtered8 = result.data.filter((t: any) => {
              return t.descripcion === "COD8" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered8, "TPAREA");
            const filteredPunto8 = result.data.filter((t: any) => {
              return t.descripcion === "COD8" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto8, "TPPUNTO");
            //COD9
            const filtered9 = result.data.filter((t: any) => {
              return t.descripcion === "COD9" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered9, "TPAREA");
            const filteredPunto9 = result.data.filter((t: any) => {
              return t.descripcion === "COD9" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto9, "TPPUNTO");
            //COD10
            const filtered10 = result.data.filter((t: any) => {
              return t.descripcion === "COD10" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered10, "TPAREA");
            const filteredPunto10 = result.data.filter((t: any) => {
              return t.descripcion === "COD10" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto10, "TPPUNTO");
            //COD11
            const filtered11 = result.data.filter((t: any) => {
              return t.descripcion === "COD11" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered11, "TPAREA");
            const filteredPunto11 = result.data.filter((t: any) => {
              return t.descripcion === "COD11" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto11, "TPPUNTO");
            //COD12
            const filtered12 = result.data.filter((t: any) => {
              return t.descripcion === "COD12" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered12, "TPAREA");
            const filteredPunto12 = result.data.filter((t: any) => {
              return t.descripcion === "COD12" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto12, "TPPUNTO");
            //COD13
            const filtered13 = result.data.filter((t: any) => {
              return t.descripcion === "COD13" && t.tipoGeometria === "TPAREA";
            });
            this.createFeature(filtered13, "TPAREA");
            const filteredPunto13 = result.data.filter((t: any) => {
              return t.descripcion === "COD13" && t.tipoGeometria === "TPPUNTO";
            });
            this.createFeature(filteredPunto13, "TPPUNTO");
          } */
        }
      },
      (error) => {
        this.toast.error('Ocurrió un error. Comunique al Administrador.');
      }
    );
  }
  createFeature(filtered: any, tipo: any) {
    if (filtered.length) {
      let idLayer = this.mapApi.Guid2.newGuid;
      let groupId = this._id;
      let features1: any = [];
      let element = filtered[0];
      filtered.forEach((t: any) => {
        if (t.geometry_wkt !== null) {
          let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
          let area = 0;
          if (jsonGeometry.type === "Polygon") {
            let geometry: any = null;
            geometry = { spatialReference: { wkid: 4326 } };
            geometry.rings = jsonGeometry.coordinates;
            area = this.mapApi.calculateArea(geometry, "hectares");
            features1.push({
              type: "Feature",
              properties: JSON.parse(t.propiedad),
              geometry: jsonGeometry,
            });
          } else {
            features1.push({
              type: "Feature",
              properties: JSON.parse(t.propiedad),
              geometry: jsonGeometry,
            });
          }
        }
      });

      let layer: any = {} as CustomCapaModel;
      layer.codigo = element.idArchivo;
      layer.idLayer = idLayer;
      if (this.isAnexo === true) {
        layer.inServer = false;
        layer.service = true;
      } else {
        layer.inServer = true;
        layer.service = false;
      }
      layer.nombre = element.nombreCapa;
      layer.groupId = groupId;
      layer.color = element.colorCapa;
      layer.annex = false;
      layer.descripcion = element.descripcion;

      this._layers.push(layer);
      let item = {
        color: element.colorCapa,
        title: element.nombreCapa,
        features: features1,
        service: layer.service,
        tipoGeometria: element.tipoGeometria
      };
      let geoJson = this.mapApi.getGeoJson2(layer.idLayer, groupId, item);
      this.createLayer(geoJson);
    }
  }
  servicioSuperposicionPlanificacion(geometry: any) {
    let params = {
      codProceso: '110102',
      geometria: {
        poligono: geometry,
      },
    };
    this.showMensajeSuperposicion = true;
    this.serviceExternos
      .validarSuperposicionPlanificacion(params)
      .subscribe((result: any) => {
        if (result.dataService === null) return;
        this.showMensajeSuperposicion = false;
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            let item = this._layersCatastro.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof item === 'object') return;
            if (t.seSuperpone === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.included = false;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.overlap = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layersCatastro.push(layer);
                this.createLayer(t.geoJson);
              }
            }
          });
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            let item = this._layersCatastro.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof item === 'object') return;
            if (t.contenida === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layersCatastro.push(layer);

                this.createLayer(t.geoJson);
              }
            }
          });
        }
      }, (error: HttpErrorResponse) => {
        this.showMensajeSuperposicion = false;
        this.toast.error(error.message);
      });
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
    }, (error: HttpErrorResponse) => {
      this.toast.error(error.message);
      this.dialog.closeAll();
    });
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.service = item.service;
    geoJsonLayer.tipoGeometria = item.tipoGeometria;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.codProceso = item.codProceso;
    geoJsonLayer.idPlanManejoGeometria = item.idPlanManejoGeometria;
    geoJsonLayer.idsCatastro = item.idsCatastro;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  cleanLayersService() {
    let layers = this.mapApi.getLayersService(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layersCatastro = [];
  }
  calculateArea(data: any) {
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    let areaTotal = Math.abs(sumArea.toFixed(3));
    return areaTotal;
  }
  validarSuperposicion() {
    this.cleanLayersService();
    let layers = this.getLayers();
    layers.items.forEach((t: any) => {
      if (t.geometryType.toUpperCase() === OgcGeometryType.POLYGON) {
        t.attributes.forEach((t2: any) => {
          if (t2.geometry.type.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
            t2.geometry.coordinates.forEach((t3: any) => {
              this.servicioSuperposicionPlanificacion(t3);
            });
          } else {

            this.servicioSuperposicionPlanificacion(t2.geometry.coordinates);
          }
        });
      }
    })
  }
  enviarComponenteCatastro() {

    if(this.objetoCC === null || this.objetoCC === undefined){
      this.toast.warn('No hay información del titulo habilitante (TH).')
      return;
    }
    let layers = this.getLayers();

    let subLayers = layers.items.filter(
      (t: any) => t.tipoGeometria === "TPPARC" || t.tipoGeometria === "TPBLOQ"
    );
    let items: any = [];
    subLayers.forEach((t: any) => {
      if (t.idsCatastro === null) {
        let item: any = {
          codProceso: t.codProceso,
          idPlanManejoGeometria: t.idPlanManejoGeometria,
          objetos: []
        };
        t.attributes.forEach((t2: any) => {
          if (t2.geometry.type.toUpperCase() === OgcGeometryType.POLYGON) {
            item.objetos.push({
              geometria: {
                poligono: t2.geometry.coordinates
              },
              atributos: {
                "fuente": this.objetoCC.fuente,
                "docreg": this.objetoCC.docreg,
                "fecreg": this.objetoCC.fecreg,
                "observ": this.objetoCC.observ,
                "zonutm": 18,
                "origen": parseInt(this.objetoCC.origen),
                "numpc": t2.properties.pc || this.objetoCC.numpc,
                "tipoth": parseInt(this.objetoCC.tipoth),
                "nrotth": this.objetoCC.nrotth,
                "numblo": parseInt(t2.properties.bloque) || this.objetoCC.numblo,
                "docleg": this.objetoCC.docleg,
                "fecleg": this.objetoCC.fecleg,
                "supafp": parseInt(this.objetoCC.supafp),
                "nomrgt": this.objetoCC.nomrgt,
                "estado": parseInt(this.objetoCC.estado),
                "tiempo": 1,
                "fecest": this.objetoCC.fecest,
                "numrgt": this.objetoCC.numrgt
              }
            })
          } else if (t2.geometry.type.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
            t2.geometry.coordinates.forEach((t2: any) => {
              item.objetos.push({
                geometria: {
                  poligono: t2
                },
                atributos: {
                  "fuente": this.objetoCC.fuente,
                  "docreg": this.objetoCC.docreg,
                  "fecreg": this.objetoCC.fecreg,
                  "observ": this.objetoCC.observ,
                  "zonutm": 18,
                  "origen": parseInt(this.objetoCC.origen),
                  "numpc":  this.objetoCC.numpc,
                  "tipoth": parseInt(this.objetoCC.tipoth),
                  "nrotth": this.objetoCC.nrotth,
                  "numblo": this.objetoCC.numblo,
                  "docleg": this.objetoCC.docleg,
                  "fecleg": this.objetoCC.fecleg,
                  "supafp": parseInt(this.objetoCC.supafp),
                  "nomrgt": this.objetoCC.nomrgt,
                  "estado": parseInt(this.objetoCC.estado),
                  "tiempo": 1,
                  "fecest": this.objetoCC.fecest,
                  "numrgt": this.objetoCC.numrgt
                }
              })
            })
          }
        })
        items.push(item);
      }
    });
    if(!items.length){
      this.toast.warn('No hay capas activas para guardar en el componente catastro.');
      return;
    }
    let observer = from(items);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((response) => this.insertarDivisionAdministrativa(response)))
      .pipe(finalize(() => {
        this.dialog.closeAll();
        this.obtenerCapas();
      })).subscribe((response: any) => {
        this.toast.ok(response.message);
      },(error:HttpErrorResponse) =>{
          this.toast.error(error.statusText);
      })
  }
  insertarDivisionAdministrativa(item: any) {
    return this.serviceExternos.insertarDivisionAdministrativa(item)
      .pipe(concatMap((response) => this.actualizarCatastroPlanManejoGeometria(response, item)));
  }
  actualizarCatastroPlanManejoGeometria(response: any, item: any) {
    let item2 = {
      idPlanManejo: this.idPlanManejo,
      idsCatastro: JSON.stringify(response.dataService.data.objetos),
      idUsuarioRegistro: this.user.idUsuario,
      idPlanManejoGeometria: item.idPlanManejoGeometria
    }
    return this.servicePlanManejoGeometria.actualizarCatastroPlanManejoGeometria(item2);
  }
}
