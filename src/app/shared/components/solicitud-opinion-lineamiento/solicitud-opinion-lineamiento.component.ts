import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-solicitud-opinion-lineamiento',
  templateUrl: './solicitud-opinion-lineamiento.component.html',
  styleUrls: ['./solicitud-opinion-lineamiento.component.scss']
})
export class SolicitudOpinionLineamientoComponent implements OnInit {

  
  minDate = moment(new Date()).format('YYYY-MM-DD');
  @ContentChild('footer') accionesRef!: TemplateRef<any>;

  @Input() comboEntidad: any[] = [];
  @Input() comboTipoDocumento: any[] = [];
  @Input() verModalSolicitud: boolean = false
  @Input() opinion: any = {};
  @Input() archivoAdjunto: any = {};
  @Input() disabledComboEntidad: boolean = true;
  @Input() disabled: boolean = false;
  
  @Output() closeModal = new EventEmitter();
  @Output() registrarAdjunto = new EventEmitter();
  @Output() eliminarAdjunto = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  close() {
    this.closeModal.emit();
  }

  registrarArchivoAdjunto(event: any) {
    
    this.registrarAdjunto.emit(event);
  }

  eliminarArchivoAdjunto() {
    this.eliminarAdjunto.emit();
  }

}
