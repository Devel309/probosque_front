import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitudOpinionLineamientoComponent } from './solicitud-opinion-lineamiento.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadInputButtonsModule } from '../upload-input-buttons/upload-input-buttons.module';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DropdownModule } from 'primeng/dropdown';
import { CargaArchivoGeneralModule } from '../formulario/carga-archivo-general/carga-archivo-general.module';



@NgModule({
  declarations: [
    SolicitudOpinionLineamientoComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UploadInputButtonsModule, 
    DropdownModule,
    ButtonModule,
    DialogModule,
    MatDatepickerModule,
    CargaArchivoGeneralModule
  ],
  exports:[
    SolicitudOpinionLineamientoComponent
  ]
})
export class SolicitudOpinionLineamientoModule { }
