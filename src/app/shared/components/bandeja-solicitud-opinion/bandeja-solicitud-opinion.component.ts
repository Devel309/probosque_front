import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ListarSolicitudOpinionRequest } from "src/app/model/solicitud-opinion";
import { CodigoEstadoOpiniones } from "src/app/model/util/CodigoEstadoOpinion";
import { ModalSolicitudOpinionComponent } from "../modal-solicitud-opinion/modal-solicitud-opinion.component";

@Component({
  selector: "app-bandeja-solicitud-opinion-entidad",
  templateUrl: "./bandeja-solicitud-opinion.component.html",
  styleUrls: ["./bandeja-solicitud-opinion.component.scss"],
})
export class BandejaSolicitudOpinionComponent implements OnInit {

  @Input() solicitudes: any ;
  @Input() comboEntidad: any ;
  @Input() comboTipoDocumento: any ;
  @Input() comboDocumentos: any ;
  @Input() comboEstado: any ;
  @Input() totalRecords!: number ;
  @Input() usuario!: string ;

  @Input() listarSolicitudOpinionRequest: ListarSolicitudOpinionRequest = new ListarSolicitudOpinionRequest();

  ref!: DynamicDialogRef;

  @Output() load = new EventEmitter();

  @Output() buscar = new EventEmitter();

  @Output() limpiar = new EventEmitter();

  CodigoEstadoOpiniones = CodigoEstadoOpiniones

  constructor(
    private dialog: MatDialog,
    public dialogService: DialogService,
  ) {}

  ngOnInit(): void {
  }

  buscarOpinion() {
    this.buscar.emit();
  }

  limpiarOpinion() {
    this.limpiar.emit();
  }

  loadOpinion(event: any) {
    this.load.emit(event);
  }

  verDetalle(data: any) {
    this.ref = this.dialogService.open(ModalSolicitudOpinionComponent, {
      header: "Respuesta de Solicitud de Opinión",
      width: "600px",
      contentStyle: { "max-height": "600px", overflow: "auto" },
      baseZIndex: 10000,
      data: {
        idSolicitudOpinion: data.idSolicitudOpinion,
        disabled:
          data.estadoSolicitudOpinion == "ESOPA" ||
          this.usuario == "ARFFS" ||  data.estadoSolicitudOpinion == "ESOPDENE",

          comboDocumentos: this.comboDocumentos
          },

    });

    this.ref.onClose.subscribe((resp: any) => {
      if (resp) {
        this.buscar.emit();
      }
    });
  }
}
