import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaSolicitudOpinionComponent } from './bandeja-solicitud-opinion.component';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { PaginatorModule } from 'primeng/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ModalSolicitudOpinionModule } from '../modal-solicitud-opinion/modal-solicitud-opinion.module';



@NgModule({
  declarations: [
    BandejaSolicitudOpinionComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TableModule,
    ToastModule,
    DropdownModule,
    FieldsetModule,
    PaginatorModule,
    MatDatepickerModule,
    ModalSolicitudOpinionModule
  ],
  exports:[BandejaSolicitudOpinionComponent]
})
export class BandejaSolicitudOpinionpmfiDemaPModule { }
