import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoadingComponent} from '../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {ArchivoService, UsuarioService} from '@services';
import {MatDialog} from '@angular/material/dialog';
import {AmpliacionSolicitudService} from '../../../service/ampliacion-solicitud/ampliacion-solicitud.service';
import {ImpugnacionSan} from '../../../model/Impugnacion/ImpugnacionSan';
import {DownloadFile, ToastService} from '@shared';
import {ImpugnacionSanService} from '../../../service/planificacion/permiso-forestal/impugnacionSan.service';
import {CodigosTiposSolicitud} from '../../../model/util/CodigosTiposSolicitud';
import {CodigoProceso} from '../../../model/util/CodigoProceso';
import {CodigoPermisoForestal} from '../../../model/util/CodigoPermisoForestal';
import {PermisoForestalService} from '../../../service/permisoForestal.service';

@Component({
  selector: 'button-evaluar-impugnacion-shared',
  templateUrl: './button-evaluar-impugnacion.component.html',
  styleUrls: ['./button-evaluar-impugnacion.component.scss']
})
export class ButtonEvaluarImpugnacionComponent implements OnInit {

  @Input() idPermisoForestal!: number;
  @Input() estado!: string;

  @Output() registroImpugnacionEmiter: EventEmitter<number> = new EventEmitter();
  pdfSrc :string = "";
  isShowModal2_2:boolean=false;

  idTipoSolicitud:number = 0;
  idProcesoPostulacion:number=0;
  autoResize: boolean = true;
  idArchivo: number = 0;
  idArchivo2: number = 0;
  idArchivo3: number = 0;
  showFormatoSan:boolean = false;

  CodigosTiposSolicitud = CodigosTiposSolicitud;

  codigoProceso :string = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;

  codigoArchivoSan :string = "PFCRIMP";
  codigoArchivoSanFirmado :string = "PFCRIMPFIRMA";
  codigoArchivoInforme :string = "PFCRIMPINFO";

  impugnacionSan:ImpugnacionSan = new ImpugnacionSan({
    idSolicitudSAN:0,
    //idPermisoForestal:this.idPermisoForestal,
    nroGestion: this.idPermisoForestal,
    codSolicitudSAN : this.codigoProceso,//this.codigoImpugnacion,
    subCodSolicitudSAN : CodigosTiposSolicitud.IMPUGNACION_SAN,
    tipoSolicitud : 'IMPUGNACIÓN AL SAN',
    tipoPostulacion : "PERMISO",
    idUsuarioRegistro: this.usuariServ.idUsuario
  });

  tipoSolicitud: any[] = [];

  constructor(private archivoService: ArchivoService,
              private dialog: MatDialog,
              private ampliacionSolicitudService: AmpliacionSolicitudService,
              private usuariServ: UsuarioService,
              private impugnacionSanService : ImpugnacionSanService,private toast: ToastService,private permisoForestalService : PermisoForestalService) {

  }

  ngOnInit(): void {

  }

  cargarIdArchivo(idArchivo: any) {
    this.idArchivo = idArchivo;
  }

  cargarIdArchivo2(idArchivo: any) {
    this.idArchivo2 = idArchivo;
  }

  cargarIdArchivo3(idArchivo: any) {
    this.idArchivo3 = idArchivo;
  }

  listarTipoSolicitud () {
    this.ampliacionSolicitudService.listarTipoSolicitud()
      .subscribe((resp: any) => {
        this.tipoSolicitud = resp.data;
      });
  };

  /*descargarImpSAN = () => {
    DownloadFile(
      this.documentoImpSAN,
      this.nombredocumentoImpSAN,
      'application/octet-stream'
    );
  };*/

  verPDF(){

    if(this.estado == CodigoPermisoForestal.ESTADO_IMPUGNADO || this.estado == CodigoPermisoForestal.ESTADO_PRESENTADO){
      this.isShowModal2_2 = true;
      this.showFormatoSan = true;
      this.listarTipoSolicitud();
      this.listarImpugnacion();
    }
  }


  listarImpugnacion(){
    let param = {
      //idPermisoForestal:this.idPermisoForestal,
      nroGestion:this.idPermisoForestal,
      pageNum:1,
      pageSize:1
    }

    this.impugnacionSanService.listarSolicitudSAN(param)
      .subscribe((res: any) => {
        if (res.success == true) {
          if (res.data){
           if(res.data.length >0){

             let response = res.data[0];
             this.impugnacionSan.asunto = response.asunto;
             this.impugnacionSan.descripcion = response.descripcion;
             this.impugnacionSan.codSolicitudSAN = response.codSolicitudSAN;
             //this.impugnacionSan.idPermisoForestal = response.idPermisoForestal;
             this.impugnacionSan.nroGestion = response.nroGestion;
             this.impugnacionSan.idSolicitudSAN = response.idSolicitudSAN;
             this.impugnacionSan.procesoPostulacion = response.procesoPostulacion;
             this.impugnacionSan.diasRestantes = response.diasRestantes;
           }


          }
        } else {
          this.toast.error(res?.message);
        }
      });


  }

  validarEnvio(){

    let resul :boolean = true;
    if(this.impugnacionSan.detalle == null){
      this.toast.warn('Debe seleccionar el resultado para la solicitud : Procede o No Procede.');
      resul = false;

    }

    if(this.impugnacionSan.observacion == null){
      this.toast.warn('Debe ingresar una observación');
      resul = false;
    }

    if(this.idArchivo2 == 0){
      this.toast.warn('Debe seleccionar un archivo para el Formato de Resolución Firmado');
      resul = false;
    }

    if(this.idArchivo3 == 0){
      this.toast.warn('Debe seleccionar un archivo Carga de Informe');
      resul = false;
    }

    return resul;
  }

  guardarImpugnacion() {

  if(this.validarEnvio()){

    let params:any[]= [];
    //this.impugnacionSan.idPermisoForestal = this.idPermisoForestal;
    this.impugnacionSan.nroGestion= this.idPermisoForestal;

    params.push(this.impugnacionSan);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionSanService.registrarSolicitudSAN(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          let estado = CodigoPermisoForestal.ESTADO_VAL_NOPROCEDE;

          if(this.impugnacionSan.detalle == 'PROCEDE'){
            estado = CodigoPermisoForestal.ESTADO_VAL_PROCEDE;
          }

          let param = {
            idPermisoForestal : this.idPermisoForestal,
            codigoEstado      : estado,
            idUsuarioRegistro : this.usuariServ.idUsuario
          };

          this.permisoForestalService.actualizarEstadoPermisoForestal(param)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok('Se registró la impugnación SAN correctamente.');
                this.isShowModal2_2 = false;
                this.registroImpugnacionEmiter.emit(1);
              } else {
                this.toast.error(response?.message);
              }
            });

        } else {
          this.toast.error(res?.message);
        }
      });
  }
  }



}


