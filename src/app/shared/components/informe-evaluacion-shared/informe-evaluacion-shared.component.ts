import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ResultadosEvaluacionDetalle } from 'src/app/model/resultadosEvaluacion';
import { PideService } from 'src/app/service/evaluacion/pide.service';

@Component({
  selector: 'informe-evaluacion-shared',
  templateUrl: './informe-evaluacion-shared.component.html',
  styleUrls: ['./informe-evaluacion-shared.component.scss'],
})
export class InformeEvaluacionSharedComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() codigoPlan!: string;
  @Input() listDocumentos!: any[];

  @Input() set favorableObj(data: ResultadosEvaluacionDetalle) {
    if (!!data) {
      this.informeEvaluacionObj = data;
      this.fechaNotificacion = data.fechaNotificacion;
      this.fechaResolucion = data.fechaResolucion;
      this.fechaConsentimiento = data.fechaConsentimiento;
    }
  }
  @Input() set desFavorableObj(data: ResultadosEvaluacionDetalle) {
    if (!!data) {
      this.informeEvaluacionObj = data;
      this.fechaNotificacion = data.fechaNotificacion;
      this.fechaResolucion = data.fechaResolucion;
      this.fechaConsentimiento = data.fechaConsentimiento;
    }
  }
  @Input() set observadoObj(data: ResultadosEvaluacionDetalle) {
    if (!!data) {
      this.informeEvaluacionObj = data;
      this.fechaNotificacion = data.fechaNotificacion;
      this.fechaResolucion = data.fechaResolucion;
      this.fechaConsentimiento = data.fechaConsentimiento;
    }
  }

  informeEvaluacionObj: ResultadosEvaluacionDetalle =
    new ResultadosEvaluacionDetalle();

  @Input('titulo') titulo: string = '';
  @Input('resolucion') resolucion: string = '';

  archivoDocSubsanacion: any = {};
  archivoDocAprueba: any = {};
  archivoDocDesaprueba: any = {};

  archivoResolucion: any = {};
  archivoNotificacion: any = {};

  archivoResolucionDesaprueba: any = {};
  archivoNotificacionDesaprueba: any = {};

  minDate = moment(new Date()).format('YYYY-MM-DD');
  @Output() onChange: EventEmitter<any> = new EventEmitter();
  fechaNotificacion!: Date | null;
  fechaResolucion!: Date | null;
  fechaConsentimiento!: Date | null;

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private pideService: PideService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  validarTitular() {
    var params = {
      numDNIConsulta: this.informeEvaluacionObj.nuDocumentoFirmante,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.pideService
      .consultarDNI(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success == true) {
          if (!!response.dataService.datosPersona) {
            this.toast.ok(response?.dataService.deResultado);

            this.informeEvaluacionObj.nombreFirmante =
              response.dataService.datosPersona.prenombres;

            this.informeEvaluacionObj.paternoFirmante =
              response.dataService.datosPersona.apPrimer;

            this.informeEvaluacionObj.maternoFirmante =
              response.dataService.datosPersona.apSegundo;
          } else {
            this.toast.warn(response?.dataService.deResultado);
            this.informeEvaluacionObj.nombreFirmante = '';
            this.informeEvaluacionObj.paternoFirmante = '';
            this.informeEvaluacionObj.maternoFirmante = '';
          }
        } else {
          this.toast.warn(response?.dataService.deResultado);
        }
      });
  }
  guardarNotificar(codigoAcordeon: string) {
    if (!this.validarMedidas()) {
      return;
    }
    this.informeEvaluacionObj.fechaNotificacion = new Date(
      this.fechaNotificacion!
    );
    this.informeEvaluacionObj.fechaResolucion = new Date(this.fechaResolucion!);
    this.informeEvaluacionObj.fechaConsentimiento = new Date(
      this.fechaConsentimiento!
    );
    var params = {
      obj: this.informeEvaluacionObj,
      codResultadoDet: codigoAcordeon,
    };
    //return;
    this.onChange.emit(params);
  }
  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.informeEvaluacionObj.tipoDocumento == null ||
      this.informeEvaluacionObj.tipoDocumento == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Tipo documento.\n';
    }
    if (
      this.informeEvaluacionObj.nuDocumento == null ||
      this.informeEvaluacionObj.nuDocumento == 0
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: N° documento.\n';
    }
    if (
      this.informeEvaluacionObj.nuDocumentoFirmante == null ||
      this.informeEvaluacionObj.nuDocumentoFirmante == 0
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe Validar: DNI.\n';
    }
    if (
      this.informeEvaluacionObj.nombreFirmante == null ||
      this.informeEvaluacionObj.nombreFirmante == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Nombres.\n';
    }
    if (
      this.informeEvaluacionObj.paternoFirmante == null ||
      this.informeEvaluacionObj.paternoFirmante == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Apellido Paterno.\n';
    }
    if (
      this.informeEvaluacionObj.cargoFirmante == null ||
      this.informeEvaluacionObj.cargoFirmante == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Cargo.\n';
    }
    if (this.fechaNotificacion == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Fecha de documento.\n';
    }
    if (
      this.informeEvaluacionObj.asunto == null ||
      this.informeEvaluacionObj.asunto == ''
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Asunto.\n';
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}
