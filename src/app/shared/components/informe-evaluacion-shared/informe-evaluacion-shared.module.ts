import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformeEvaluacionSharedComponent } from './informe-evaluacion-shared.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MaterialModule } from 'src/app/module/material/material.module';
import { UploadInputButtonsModule } from '../upload-input-buttons/upload-input-buttons.module';
import { PanelModule } from 'primeng/panel';
import { InputButtonsModule } from '../input-button/input-buttons.module';
import {InputButtonsCodigoModule} from '../input-button-codigo/input-buttons.module';



@NgModule({
  declarations: [InformeEvaluacionSharedComponent],
    imports: [
        CommonModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        DropdownModule,
        RadioButtonModule,
        MaterialModule,
        UploadInputButtonsModule,
        PanelModule,
        InputButtonsModule,
        InputButtonsCodigoModule
    ],
  exports:[InformeEvaluacionSharedComponent]
})
export class InformeEvaluacionSharedModule { }
