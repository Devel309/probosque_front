import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {ResultadoEvaluacionSharedComponent} from './resultado-evaluacion-shared.component';
import {InputButtonsModule} from '../input-button/input-buttons.module';
import {RadioButtonModule} from 'primeng/radiobutton';
import {InformeEvaluacionSharedModule} from '../informe-evaluacion-shared/informe-evaluacion-shared.module';
import {DropdownModule} from 'primeng/dropdown';
import {AccordionModule} from 'primeng/accordion';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';

@NgModule({
  declarations: [
    ResultadoEvaluacionSharedComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    ConfirmPopupModule,
    InputButtonsModule,
    RadioButtonModule,
    InformeEvaluacionSharedModule,
    DropdownModule,
    AccordionModule,
    TableModule,
    ToastModule
  ],
  exports:[ResultadoEvaluacionSharedComponent]
})
export class ResultadoEvaluacionSharedModule { }
