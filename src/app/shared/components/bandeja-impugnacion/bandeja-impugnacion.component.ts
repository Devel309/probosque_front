import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { ListarImpugnacionRequest } from 'src/app/model/Impugnacion';

@Component({
  selector: 'app-bandeja-impugnacion',
  templateUrl: './bandeja-impugnacion.component.html',
  styleUrls: ['./bandeja-impugnacion.component.scss']
})
export class BandejaImpugnacionComponent implements OnInit {

  @ContentChild('acciones') accionesRef!: TemplateRef<any>;

  @Input() lstData: any[] = [];

  @Input() comboEstado: any[] = [];
  @Input() comboTipodocGestion: any[] = [];

  @Input() totalRecords = 0;

  @Input() impugnacionRequest: ListarImpugnacionRequest = new ListarImpugnacionRequest();

  @Input() verAgregar: boolean = false;

  @Output() changePage = new EventEmitter();

  @Output() buscar = new EventEmitter();

  @Output() limpiar = new EventEmitter();

  @Output() agregar = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
 
  }

  loadData(event: any) {
    this.changePage.emit(event);
  }

  buscarImpugnacion() {
    this.buscar.emit();
  }

  limpiarImpugnacion() {
    this.limpiar.emit();
  }

  agregarImpugnacion() {
    this.agregar.emit();
  }

}
