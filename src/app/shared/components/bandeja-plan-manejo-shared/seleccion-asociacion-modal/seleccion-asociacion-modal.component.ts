import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DataBasePMSharedModel } from 'src/app/model/FakeData';
import { handlerSaveResult, ToastService } from '@shared';
import { TipoProcesoContratoPermiso } from '../bandeja-plan-manejo-shared.component';
import { TipoDocumentoPlan } from '../../../const';
import { Page, PlanManejoContrato, PlanManejoMSG as MSG } from '@models';
import { PlanManejoService } from 'src/app/service/plan-manejo.service';
import { CensoForestalService } from 'src/app/service/censoForestal';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BandejaPlanOperativoService } from 'src/app/service/bandeja-plan-operativo/bandeja-plan-operativo.service';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { PlanificacionService, UsuarioService } from '@services';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { HttpParams } from '@angular/common/http';
import { ConsoleLogger } from '@angular/compiler-cli/src/ngtsc/logging';

@Component({
  selector: 'app-seleccion-asociacion-modal',
  templateUrl: './seleccion-asociacion-modal.component.html',
  styleUrls: ['./seleccion-asociacion-modal.component.scss']
})
export class SeleccionAsociacionModalComponent implements OnInit {
  // dataBase: DataBasePMSharedModel;
  lstModal: any[] = [];
  tipoProcesoContratoPermiso: TipoProcesoContratoPermiso;
  tipoProcesoPlanManejo!: number;
  idTipoPlan: number;
  urlNuevo: string = '/inicio';
  selectModal: any[] = [];
  loading = false;
  totalRecords: number = 0;
  codigoProceso: string = '';
	contrato!: PlanManejoContrato;
  queryHabilitante: string = "";

  isTipoPlanConcesion: boolean = false;

  constructor(private router: Router,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private toast: ToastService,
    private censoForestal: CensoForestalService,
    private bandejaPlanOperativoService: BandejaPlanOperativoService,
    private user: UsuarioService,
    private apiPlanificacion: PlanificacionService,
    private dialog: MatDialog,
    private permisoForestalService: PermisoForestalService,
  ) {

    // this.dataBase = new DataBasePMSharedModel();
    let data = this.config.data;

    this.tipoProcesoContratoPermiso = data.tipoProcesoContratoPermiso;
    this.idTipoPlan = data.idTipoPlan;
    this.tipoProcesoPlanManejo = data.idtipoProceso;
  }


  ngOnInit(): void {
    this.queryHabilitante = '';
    switch (this.idTipoPlan) {
      case TipoDocumentoPlan.DEMAP:
        this.urlNuevo = "/planificacion/elaboracion-declaracion-demap";
        this.codigoProceso = CodigoProceso.PLAN_OPERATIVO_DEMAP;
        this.isTipoPlanConcesion = false;
        break;
      case TipoDocumentoPlan.PMFIP:
        this.urlNuevo = "/planificacion/elaboracion-declaracion-pmfip";
        this.codigoProceso = CodigoProceso.PLAN_OPERATIVO_PMFIP;
        this.isTipoPlanConcesion = false;
        break;
      case TipoDocumentoPlan.POPMIFP:
        this.urlNuevo = "/planificacion/elaboracion-declaracion-popmifp";
        this.codigoProceso = CodigoProceso.PLAN_OPERATIVO_POPMIFP;
        this.isTipoPlanConcesion = false;
        break;
      case TipoDocumentoPlan.DEMAC:
        this.urlNuevo = "/planificacion/elaboracion-declaracion-demac";
        this.codigoProceso = CodigoProceso.PLAN_OPERATIVO_DEMAC;
        this.isTipoPlanConcesion = true;
        break;
      default:
        break;
    }

    this.listarBandeja().subscribe();
    
  }

  filtrarContratoHabilitante() {
    this.listarBandeja().subscribe();
  }

  listarBandeja(page?: Page) {
    const r = {
      pageNum: this.pageNumber,
      pageSize: this.pageSize,
      tipoProceso: this.tipoProcesoContratoPermiso,
      codigoTituloTh: this.queryHabilitante?(this.queryHabilitante.trim()!= ""? this.queryHabilitante.trim(): null):null,
      idUsuarioRegistro: JSON.parse("" + localStorage.getItem("usuario")).idusuario
    }

    this.loading = true;
    return this.listaTHContratoPorFiltro(r)
      .pipe(finalize(() => this.loading = false))
      .pipe(tap(res => {
        
        this.lstModal = res.data;
        this.totalRecords = res.totalRecord;
      }));
  }

  pageNumber = 1;
  pageSize = 10;
  load(e: LazyLoadEvent) {
    this.pageSize = Number(e.rows);
    this.pageNumber = (Number(e.first) / this.pageSize) + 1;
    this.listarBandeja().subscribe();
  }

  listaTHContratoPorFiltro(request: any, page?: Page): Observable<any> {
    return this.censoForestal.listaTHContratoPorFiltro(request, page)
      .pipe(tap({ error: () => this.toast.error(MSG.ERR.LIST) }));
  }

  registrarPlanConcesion() {
    let contratoSelecionado: any = this.selectModal;
    let param = {
      idContrato: contratoSelecionado.idContrato,
      idUsuarioTitularTH: null,
      // codigoEstadoContrato: this.codigoEstadoContrato.VIGENTE,
      // codigoTipoContrato: this.codigoTipoContrato.PROGRAMADO_ABREVIADO
    }

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiPlanificacion.obtenerContrato(param).subscribe(
      (result: any) => {
        if (result.success) {
          if (result.data && result.data.length > 0) {
            this.contrato = result.data[0];
             let params = {
              idPlanManejo: null,
              descripcion: this.codigoProceso,
              idContrato: contratoSelecionado.idContrato,
              idSolicitud: null,
              idPlanManejoPadre: null,
              idTipoPlan: this.idTipoPlan,
              idTipoProceso: this.tipoProcesoPlanManejo,
              consolidado: null,
              idUsuarioRegistro: this.user.idUsuario,
              contratoPrincipal: null,
              listaPlanManejoContrato:[{
                idContrato: contratoSelecionado.idContrato,
                idPlanManejo:null,
                idUsuarioRegistro:this.user.idUsuario,
                principal:true

              }],

              infoGeneral: {
                apeMaternoTitular: null,
                codigoTipoPersonaTitular: null,
                descripcionTipoPersonaTitular: '',
                codigoTipoDocIdentidadTitular: null,
                descripcionTipoDocIdentidadTitular: null,
                nombreTitular: null,
                apePaternoTitular: null,
                numeroDocumentoTitular: null,
                rucTitular: null,
                correoTitular: null,
                razonSocialTitular: null,
                domicilioLegalTitular: null,
                departamento: null,
                provincia: null,
                distrito: null,
                codigoTipoDocIdentidadRepLegal: null,
                descripcionTipoDocIdentidadRepLegal: null,
                numeroDocumentoRepLegal: null,
                nombreRepresentanteLegal: this.contrato.nombreRepresentanteLegal,
                apePaternoRepLegal: this.contrato.apePaternoRepLegal,
                apeMaternoRepLegal: this.contrato.apeMaternoRepLegal,
                correoRepLegal: null,
                rucRepLegal: null,
                tipoPersonaTitular:this.contrato.codigoTipoPersonaTitular,//'TPERNATU', //agregado
                tipoPersonaRepre: this.contrato.codigoTipoDocIdentidadRepLegal//'TPERNATU' //agregado
              }
            }

            this.bandejaPlanOperativoService.registrarPlanManejo(params)
              .pipe(tap(handlerSaveResult(this.toast)))
              .pipe(finalize(() => this.dialog.closeAll()))
              .subscribe((res) => {
                this.router.navigate([this.urlNuevo, res.data.idPlanManejo], { state: { data: this.idTipoPlan } });
                this.ref.close();
              });
          } else {
            this.dialog.closeAll();
          }
        } else {
          this.dialog.closeAll();
          this.toast.warn(result.message);
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.toast.error('Ocurrió un error al traer los datos del contrato seleccionado');
      }
    );
  }

  registrarPlanPermiso() {
    let contratoSelecionado: any = this.selectModal;

    let paramsListar = new HttpParams()
      .set('idPermisoForestal', contratoSelecionado.idContrato.toString())
      .set('pageNumber', '1')
      .set('pageSize', '1')
      .set('sortType', 'DESC');

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.ListarPermisosForestales(paramsListar).subscribe((result: any) => {
      if (result.success) {
        if (result.data.length > 0) {
          let auxDataListar = result.data[0];
          let auxIdPermisoForestal = auxDataListar.idPermisoForestal;
          let paramsReg1 = {
            idPlanManejo: null,
            descripcion: this.codigoProceso,
            idContrato: contratoSelecionado.idContrato,
            idSolicitud: null,
            idPlanManejoPadre: null,
            idTipoPlan: this.idTipoPlan,
            idTipoProceso: this.tipoProcesoPlanManejo,
            consolidado: null,
            idUsuarioRegistro: this.user.idUsuario,
            contratoPrincipal: null,
            infoGeneral: {
              apeMaternoTitular: null,
              codigoTipoPersonaTitular: null,
              descripcionTipoPersonaTitular: '',
              codigoTipoDocIdentidadTitular: null,
              descripcionTipoDocIdentidadTitular: null,
              nombreTitular: null,
              apePaternoTitular: null,
              numeroDocumentoTitular: null,
              rucTitular: null,
              correoTitular: null,
              razonSocialTitular: null,
              domicilioLegalTitular: null,
              departamento: null,
              provincia: null,
              distrito: null,
              codigoTipoDocIdentidadRepLegal: null,
              descripcionTipoDocIdentidadRepLegal: null,
              numeroDocumentoRepLegal: null,
              nombreRepresentanteLegal: null,
              apePaternoRepLegal: null,
              apeMaternoRepLegal: null,
              correoRepLegal: null,
              rucRepLegal: null,
              tipoPersonaTitular: auxDataListar?.informacionGeneral?.codTipoPersona,
              tipoPersonaRepre: null
            }
          }
          this.bandejaPlanOperativoService.registrarPlanManejo(paramsReg1).subscribe((res: any) => {
            if (res.success) {
              let idNuevoPlan = res.data.idPlanManejo;
              let mensaje = res.message;
              const paramsReg2 = [{
                idPermisoForestal: auxIdPermisoForestal,
                idPlanManejo: idNuevoPlan,
                idUsuarioRegistro: this.user.idUsuario,
              }];

              this.permisoForestalService.registrarPermisoForestalPlanManejo(paramsReg2).subscribe((response: any) => {
                this.dialog.closeAll();
                if (response.success) {
                  this.toast.ok(mensaje);
                  this.router.navigate([this.urlNuevo, idNuevoPlan], { state: { data: this.idTipoPlan } });
                  this.ref.close();
                } else {
                  this.toast.warn(response.message);
                }
              }, (error) => {
                this.dialog.closeAll();
                this.toast.error('Ocurrió un error al registrar el plan');
              });
            } else {
              this.dialog.closeAll();
              this.toast.warn(result.message);
            }
          }, (error) => {
            this.dialog.closeAll();
            this.toast.error('Ocurrió un error al registrar el plan');
          });
        } else {
          this.dialog.closeAll();
        }
      } else {
        this.dialog.closeAll();
        this.toast.warn(result.message);
      }
    }, (error) => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un error al traer los datos del contrato seleccionado');
    }
    );
  }

  aceptar(): void {
    if (this.selectModal.length === 0) {
      this.toast.error('Debe seleccionar una opción');
      return;
    }

    if(this.isTipoPlanConcesion) this.registrarPlanConcesion();
    else this.registrarPlanPermiso();
  }

  cancelar() {
    this.ref.close();
  }
  limpiar(){
    this.queryHabilitante = '';
    this.filtrarContratoHabilitante();

  }

}
