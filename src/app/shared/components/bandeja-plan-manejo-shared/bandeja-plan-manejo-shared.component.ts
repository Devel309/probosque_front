import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/base/usuario.service';
import { PlanManejoService } from 'src/app/service/plan-manejo.service';
import { UsuarioModel } from '../../../model/seguridad/usuario';
import { ToastService } from '../../services/toast.service';
import { PlanManejoMSG as MSG } from '@models';
import { TipoDocumentoPlan } from '../../const';
import { DialogService } from 'primeng/dynamicdialog';
import { SeleccionAsociacionModalComponent } from './seleccion-asociacion-modal/seleccion-asociacion-modal.component';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from 'src/app/model/util/Mensajes';
@Component({
  selector: 'app-bandeja-plan-manejo-shared',
  templateUrl: './bandeja-plan-manejo-shared.component.html',
  styleUrls: ['./bandeja-plan-manejo-shared.component.scss']
})
export class BandejaPlanManejoSharedComponent implements OnInit {
  filtro: any = {};
  tipoPMTitulo: string = "";
  tipoPMAbrev: string = "";
  tipoPM: string = "C";
  totalRecords: number = 0;
  usuario!: UsuarioModel;
  planes: any[] = [];

  PLAN_MANEJO = {
    idTipoProceso: 3,
    idTipoPlan: 0,
  }
  tipoProcesoContratoPermiso = TipoProcesoContratoPermiso.Permisos;
  tituloModal = '';
  url = '/inicio';
  pageOptions: any = { pageNumber: 1, pageSize: 10 };

  constructor(private router: Router,
    private apiPlanManejo: PlanManejoService,
    private user: UsuarioService,
    private toast: ToastService,
    public dialogService: DialogService,
    private dialog: MatDialog
  ) {
    this.usuario = this.user.usuario;
  }

  ngOnInit(): void {
    this.tipoPM = "P";
    this.tipoProcesoContratoPermiso = TipoProcesoContratoPermiso.Permisos;
    this.tituloModal = ' SOLICITUD DE PERMISO FORESTAL';
    
    let ruta = this.router.url;
    if (ruta.indexOf('pmp-dema') > -1) {
      this.tipoPMTitulo = "Permisos DEMA";
      this.tipoPMAbrev = "DEMA"
      this.PLAN_MANEJO.idTipoPlan = TipoDocumentoPlan.DEMAP;
      this.url = '/planificacion/elaboracion-declaracion-demap';
    } else if (ruta.indexOf('pmp-pmfi') > -1) {
      this.tipoPMTitulo = "Permisos PMFI";
      this.tipoPMAbrev = "PMFI"
      this.PLAN_MANEJO.idTipoPlan = TipoDocumentoPlan.PMFIP;
      this.url = '/planificacion/elaboracion-declaracion-pmfip';
    } else if (ruta.indexOf('pmp-po-pmfi') > -1) {
      this.tipoPMTitulo = "Permisos PO PMFI";
      this.tipoPMAbrev = "PO-PMFI"
      this.PLAN_MANEJO.idTipoPlan = TipoDocumentoPlan.POPMIFP;
      this.url = '/planificacion/elaboracion-declaracion-popmifp';
    } else if (ruta.indexOf('pmc-dema') > -1) {
      this.tipoPMTitulo = "Concesiones DEMA";
      this.tipoPMAbrev = "DEMA"
      this.tipoProcesoContratoPermiso = TipoProcesoContratoPermiso.Contratos;
      this.tipoPM = "C";
      this.PLAN_MANEJO.idTipoPlan = TipoDocumentoPlan.DEMAC;
      this.tituloModal = ' CONTRATO / PM'
      this.url = '/planificacion/elaboracion-declaracion-demac';
    }

    this.listarBandeja();
  }

  buscar() {
    this.listarBandeja();
  }

  limpiar(): void {
    this.filtro = {};
    this.pageOptions = { pageNumber: 1, pageSize: 10 }
    this.listarBandeja();
  }

  verPlan(idPlan: number): void {
    this.router.navigate([this.url, idPlan], { state: { data: this.PLAN_MANEJO.idTipoPlan } });
  }

  listarBandeja() {
    this.planes = [];
    this.totalRecords = 0;
    const request = { ...this.PLAN_MANEJO, ...this.filtro };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiPlanManejo.filtrar(request, this.pageOptions).subscribe(resp=> {
      this.dialog.closeAll();
      if(resp.success && resp.data.length > 0){
        this.planes = resp.data;
        this.totalRecords = resp.totalRecords;
      } else {
        this.toast.warn(Mensajes.MSJ_SIN_REGISTROS);
      }
    }, () => {
      this.toast.error(MSG.ERR.LIST);
      this.dialog.closeAll();
    });
  }

  loadData(e: any) {
    const pageSize = Number(e.rows);
    const pageNumber = (Number(e.first) / pageSize) + 1;
    this.pageOptions = { pageNumber, pageSize };
    this.listarBandeja();
  }

  nuevo() {
    const header = `SELECCIONE${this.tituloModal} `;

    let data = {
      idtipoProceso: this.PLAN_MANEJO.idTipoProceso,
      idTipoPlan: this.PLAN_MANEJO.idTipoPlan,
      tipoProcesoContratoPermiso: this.tipoProcesoContratoPermiso,
    }

    const config = { header, data, width: '60vw', closable: true };
    const ref = this.dialogService.open(SeleccionAsociacionModalComponent, config);

    ref.onClose.subscribe((res: any) => {
    });
  }

}


export enum TipoProcesoContratoPermiso {
  Permisos = 2,
  Contratos = 1
}
