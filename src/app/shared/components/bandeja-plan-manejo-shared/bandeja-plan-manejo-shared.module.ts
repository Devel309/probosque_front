import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaPlanManejoSharedComponent } from './bandeja-plan-manejo-shared.component';
import { SeleccionAsociacionModalComponent } from './seleccion-asociacion-modal/seleccion-asociacion-modal.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    BandejaPlanManejoSharedComponent,
    SeleccionAsociacionModalComponent
  ],
  imports: [
    CommonModule,
    TableModule
  ],
  exports:[
    BandejaPlanManejoSharedComponent,
    SeleccionAsociacionModalComponent
    
  ]
})
export class BandejaPlanManejoSharedModule { }
