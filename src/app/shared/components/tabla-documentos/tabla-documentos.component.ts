import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ParametroModel } from '@models';
import { ArchivoService, PlanManejoService, UsuarioService } from '@services';
import { DowloadFileLocal, DownloadFile } from '@shared';

import { ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { GenericoService } from 'src/app/service/generico.service';

@Component({
  selector: 'tabla-documentos',
  templateUrl: './tabla-documentos.component.html',
  styleUrls: ['./tabla-documentos.component.scss']
})
export class TablaDocumentosComponent implements OnInit {

  @Input() fileList: any[] = [];
  file: any = {};
  display: boolean = false;

  @Input() canLoad: boolean = false;
  @Input() descripcion: boolean = false;
  @Input() idPlanManejo: any = null;
  @Input() label: string = "";
  @Input() disabled: boolean = false;
  @Input() edit: boolean = true;
  @Input() verObservacion: boolean = false;

  @Output() registrarArchivo = new EventEmitter();
  @Output() actualizarArchivo = new EventEmitter();
  @Output() elimArchivo = new EventEmitter();

  listaTipoDocumento: any[] = [];
  idTipoDocumento: any;
  usuario = {} as UsuarioModel;

  displayModificar: boolean = false;
  archivoModificar: any = {};

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService,
    private genericoService: GenericoService,
    private planManejoService: PlanManejoService,
    private dialog: MatDialog,
    private usuarioSrv: UsuarioService
  ) {

  }

  ngOnInit(): void {
    this.usuario = this.usuarioSrv.usuario;
    this.listarTipoDocumento();
  }

  abrirModal() {
    this.idTipoDocumento = undefined;
    this.file = {};
    this.display = true;
  }

  cerrarModal() {
    this.display = false;
    this.displayModificar = false;
  }

  private listarTipoDocumento() {
    let parametro: any = {};
    parametro.idTipoParametro = 79;

    this.genericoService.listarPorFiltroParametro(parametro).subscribe((result: any) => {
      this.listaTipoDocumento = result.data;
    })
  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.file = {};

    this.file.url = URL.createObjectURL(event.target.files[0]);
    this.file.file = event.target.files[0];
    this.file.nombre = event.target.files[0].name;

    if (this.displayModificar) {

    }
  }

  verArchivo(d: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (d.idArchivo) {
      let params = {
        idArchivo: d.idArchivo
      }

      this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);

        }
      },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      );

    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }

  openEliminarRegistro(event: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      rejectLabel: 'No',
      accept: () => {
        let params = {
          idPlanManejoArchivo: this.fileList[index].idPlanManejoArchivo,
          idUsuarioElimina: this.usuario.idusuario
        };
        this.eliminarArchivo(params);
      }
    });
  }
  editarArchivo(params: any) {
    this.displayModificar = true;
    this.file = { idArchivo: params.idArchivo, nombre: params.nombre };
    this.archivoModificar = params;
  }

  private eliminarArchivo(params: any) {

    this.planManejoService.eliminarPlanManejoArchivo(params).subscribe((result: any) => {
      this.dialog.closeAll();
      this.SuccessMensaje(result.message);

      this.elimArchivo.emit();
    },
      (error: HttpErrorResponse) => {
        this.ErrorMensaje(error.message);
        this.dialog.closeAll();
      }
    );

  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  agregarArchivo() {
    if (!this.validarArchivo()) return;
    let param = { file: this.file.file, idTipoDocumento: this.idTipoDocumento, descripcion: this.file.descripcion };
    this.registrarArchivo.emit(param);
    this.display = false;
  }

  modificarArchivo() {
    let param = { file: this.file.file, archivoModificar: this.archivoModificar };
    this.actualizarArchivo.emit(param);
    this.displayModificar = false;
  }

  validarArchivo() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (!this.file.file) {
      validar = false;
      mensaje = mensaje += '(*) Debe adjuntar el archivo.\n';
    }

    if (this.descripcion && !this.file.descripcion) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar la descripción.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

}


