import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaDocumentosComponent } from './tabla-documentos.component'
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import {DialogModule} from 'primeng/dialog';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { MatDialogModule } from '@angular/material/dialog';
import { DropdownModule } from 'primeng/dropdown';




@NgModule({
    declarations: [
        TablaDocumentosComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        TableModule,
        ToastModule,
        DialogModule,
        ConfirmPopupModule,
        ButtonModule,
        MatDialogModule,
        DropdownModule,
    ],
    exports: [TablaDocumentosComponent]
})
export class TablaDocumentosModule { }
