import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import {ConfirmPopupModule} from 'primeng/confirmpopup';

import {InputButtonsModule} from '../input-button/input-buttons.module';
import {RadioButtonModule} from 'primeng/radiobutton';
import {InformeEvaluacionSharedModule} from '../informe-evaluacion-shared/informe-evaluacion-shared.module';
import {DropdownModule} from 'primeng/dropdown';
import {AccordionModule} from 'primeng/accordion';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {OsinforSharedComponent} from './osinfor-shared.component';
import {RequisitoPgmfSimpleModule} from '../requisito-pgmf-simple/requisito-pgmf-simple.module';
import {DialogModule} from 'primeng/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  declarations: [
    OsinforSharedComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    ConfirmPopupModule,
    InputButtonsModule,
    RadioButtonModule,
    InformeEvaluacionSharedModule,
    DropdownModule,
    AccordionModule,
    TableModule,
    ToastModule,
    RequisitoPgmfSimpleModule,
    DialogModule,
    MatDatepickerModule
  ],
  exports:[OsinforSharedComponent]
})
export class OsinforSharedModule { }
