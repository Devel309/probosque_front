import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { ToastService } from '@shared';
import { MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { EvaluacionObj } from 'src/app/model/evaluacionInfoDoc';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { ResumenService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service';
import { RequisitoSimpleModel } from 'src/app/shared/components/requisito-pgmf-simple/requisito-pgmf-simple.component';
import { LoadingComponent } from '../../../components/loading/loading.component';
import { observados } from '../../util';
@Component({
  selector: 'osinfor-shared',
  templateUrl: './osinfor-shared.component.html',
  styleUrls: ['./osinfor-shared.component.scss'],
})
export class OsinforSharedComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() disabled!: boolean;
  @Input() evaluacion!: EvaluacionObj;
  @Input() set listarEvaluacionDetalle(response: any) {
    if (response) {

      console.log(response);

      this.evaluacionMedidasCautelares.titulo =
        'Requisitos de Medidas Cautelares';
      this.evaluacionMedidasCautelares.evaluacion.codigoTipo = 'REQMEDCAU';
      this.evaluacionMedidasCorrectivas.titulo =
        'Requisitos de Medidas Correctivas';
      this.evaluacionMedidasCorrectivas.evaluacion.codigoTipo = 'REQMEDCOR';

      const mediCautelares: any[] = response.filter(
        (data: any) => data.codigoEvaluacionDetPost == 'REQMEDCAU'
      );
      const mediCorrectivas: any[] = response.filter(
        (data: any) => data.codigoEvaluacionDetPost == 'REQMEDCOR'
      );

      console.log(mediCautelares, mediCorrectivas);

      if (mediCautelares.length > 0) {
        mediCautelares.map((data: any) => {
          this.evaluacionMedidasCautelares.evaluacion.idPlanManejoEvalDet =
            data.idEvaluacionDet;
          this.evaluacionMedidasCautelares.evaluacion.idPlanManejoEvaluacion =
            data.idEvaluacion;
          this.evaluacionMedidasCautelares.evaluacion.conforme =
            data.conforme == 'EEVARECI' ? true : false;
          this.evaluacionSeleccionada(
            data.conforme == 'EEVARECI',
            'medidasCautelares'
          );
          this.evaluacionMedidasCautelares.evaluacion.observacion =
            data.observacion;
        });
      } else {
        this.evaluacionSeleccionada(false, 'defaultMedCau');
      }

      if (mediCorrectivas.length > 0) {
        mediCorrectivas.map((data: any) => {
          this.evaluacionMedidasCorrectivas.evaluacion.idPlanManejoEvalDet =
            data.idEvaluacionDet;
          this.evaluacionMedidasCorrectivas.evaluacion.idPlanManejoEvaluacion =
            data.idEvaluacion;
          this.evaluacionMedidasCorrectivas.evaluacion.conforme =
            data.conforme == 'EEVARECI' ? true : false;

          this.evaluacionSeleccionada(
            data.conforme == 'EEVARECI',
            'medidasCorrectivas'
          );
          this.evaluacionMedidasCorrectivas.evaluacion.observacion =
            data.observacion;
        });
      } else {
        this.evaluacionSeleccionada(false, 'defaultMedCor');
      }
    }
  }

  @Output() updateList = new EventEmitter();

  showTablePlanes: boolean = false;
  showTableFinPAU: boolean = false;

  numeroDocumento: string | null = null;

  evaluacionMedidasCautelares: RequisitoSimpleModel =
    new RequisitoSimpleModel();
  evaluacionMedidasCorrectivas: RequisitoSimpleModel =
    new RequisitoSimpleModel();

  tituloHabilitante: any;
  listOsinforPlanes: any;
  listResolucionFinPAU: any;
  isDisabledRequisitos: boolean = false;
  noValido: boolean = false;

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private evaluacionService: EvaluacionService,
    private messageService: MessageService,
    private resumenService: ResumenService,
    private apiForestalService: ApiForestalService,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    // this.evaluacionMedidasCorrectivas.titulo =
    //   'Requisitos de Medidas Correctivas';
    // this.evaluacionMedidasCorrectivas.classEstado = 'req_nothing_class';
    // this.evaluacionMedidasCorrectivas.icon = 'pi-times-circle';
    // this.evaluacionMedidasCorrectivas.evaluacion.codigoTipo = 'REQMEDCOR';
    // this.listarPlanManejo();
  }

  openTab(e: any) {
    if (e.index == 0) {
      this.openMedidasCautelares();
    } else if (e.index == 1) {
      this.openMedidasCorrectivas();
    }
  }

  openMedidasCautelares() {
    this.consultarPlanes();
  }
  openMedidasCorrectivas() {
    this.consultarResolucionFinPAU();
  }

  listarPlanManejo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: this.codigoProceso,
    };
    this.resumenService
      .obtenerInformacionPlan(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        this.tituloHabilitante = response.data[0].tituloHabilitante;
      });
  }
  consultarPlanes() {
    let param = {
      pageNumber: 1,
      pageSize: 10,
      titular: '',
      tituloHabilitante: this.tituloHabilitante,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiForestalService
      .consultarPlanesOsinfor(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.dataService.items.length) {
            this.listOsinforPlanes = response.dataService.items;
            this.showTablePlanes = true;
          } else {
            this.toast.warn('No se encuentran datos en web service OSINFOR.');
          }
        },
        () => {
          this.toast.warn('No se encuentran datos en web service OSINFOR.');
        }
      );
  }
  consultarResolucionFinPAU() {
    let param = {
      pageNumber: 1,
      pageSize: 10,
      titular: '',
      tituloHabilitante: this.tituloHabilitante,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.apiForestalService
      .consultarResolFinPAU(param)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (response: any) => {
          if (response.dataService.items.length) {
            this.listResolucionFinPAU = response.dataService.items;
            this.showTableFinPAU = true;
          } else {
            this.toast.warn('No se encuentran datos en web service OSINFOR.');
          }
        },
        () => {
          this.toast.warn('No se encuentran datos en web service OSINFOR.');
        }
      );
  }

  registrarRequisitosPlan() {
    let evaluacionDet: any = [];

    const objDet = new EvaluacionDetalle();
    objDet.idEvaluacion =
      this.evaluacionMedidasCautelares.evaluacion.idPlanManejoEvaluacion;
    objDet.idEvaluacionDet =
      this.evaluacionMedidasCautelares.evaluacion.idPlanManejoEvalDet;
    objDet.codigoEvaluacionDet = this.codigoProceso;
    objDet.codigoEvaluacionDetPost =
      this.evaluacionMedidasCautelares.evaluacion.codigoTipo;
    objDet.codigoEvaluacionDetSub = this.codigoProceso + 'REEVRIIC';
    objDet.idUsuarioRegistro = this.usuarioService.idUsuario;

    switch (this.evaluacionMedidasCautelares.evaluacion.conforme) {
      case true: {
        objDet.conforme = 'EEVARECI';
        objDet.observacion = '';
        evaluacionDet.push(objDet);
        break;
      }
      case false: {
        objDet.conforme = 'EEVAOBS';
        objDet.observacion =
          this.evaluacionMedidasCautelares.evaluacion.observacion;

        evaluacionDet.push(objDet);
        break;
      }
    }

    // MEDIDAS CORRECTIVAS
    const objDetMC = new EvaluacionDetalle();
    objDet.idEvaluacion =
      this.evaluacionMedidasCorrectivas.evaluacion.idPlanManejoEvaluacion;
    objDet.idEvaluacionDet =
      this.evaluacionMedidasCorrectivas.evaluacion.idPlanManejoEvalDet;
    objDetMC.codigoEvaluacionDet = this.codigoProceso;
    objDetMC.codigoEvaluacionDetPost =
      this.evaluacionMedidasCorrectivas.evaluacion.codigoTipo;
    objDetMC.codigoEvaluacionDetSub = this.codigoProceso + 'REEVRIIC';
    objDetMC.idUsuarioRegistro = this.usuarioService.idUsuario;

    switch (this.evaluacionMedidasCorrectivas.evaluacion.conforme) {
      case true: {
        objDetMC.conforme = 'EEVARECI';
        objDetMC.observacion = '';
        evaluacionDet.push(objDetMC);
        break;
      }
      case false: {
        objDetMC.conforme = 'EEVAOBS';
        objDetMC.observacion =
          this.evaluacionMedidasCorrectivas.evaluacion.observacion;

        evaluacionDet.push(objDetMC);
        break;
      }
    }

    this.evaluacion.listarEvaluacionDetalle = [];
    this.evaluacion.listarEvaluacionDetalle = evaluacionDet;
    if(this.evaluacion.listarEvaluacionDetalle.length != 0){

      if (!observados(this.evaluacion.listarEvaluacionDetalle, this.toast)) {
        return;
      }

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.evaluacionService
        .registrarEvaluacionPlanManejo(this.evaluacion)
        .pipe(finalize(() => this.dialog.closeAll()))
        .subscribe((response: any) => {
          if (response.success == true) {
            this.toast.ok(response.message);
            this.updateList.emit();
          } else {
            this.toast.warn(response.message);
          }
        });
    } else {
      this.toast.warn("(*) No ha seleccionado una evaluación.\n");
    }
  }

  evaluacionSeleccionada(conforme: boolean, type: string) {
    console.log(conforme, type);

    switch (type) {
      case 'medidasCautelares':
        this.evaluacionMedidasCautelares.icon = !!conforme
          ? 'pi-check-circle'
          : 'pi-info-circle';
        this.evaluacionMedidasCautelares.classEstado = !!conforme
          ? 'req_conforme_class'
          : 'req_observado_class';
        break;
      case 'medidasCorrectivas':
        this.evaluacionMedidasCorrectivas.icon = !!conforme
          ? 'pi-check-circle'
          : 'pi-info-circle';
        this.evaluacionMedidasCorrectivas.classEstado = !!conforme
          ? 'req_conforme_class'
          : 'req_observado_class';
        break;
      case 'defaultMedCor':
        this.evaluacionMedidasCorrectivas.classEstado = 'req_nothing_class';
        this.evaluacionMedidasCorrectivas.icon = 'pi-times-circle';
        break;
      case 'defaultMedCau':
        this.evaluacionMedidasCautelares.classEstado = 'req_nothing_class';
        this.evaluacionMedidasCautelares.icon = 'pi-times-circle';
        break;
    }
  }

  listarEvaluacion() {
    let params = {
      idPlanManejo: this.idPlanManejo,
      codigoEvaluacionDet: this.codigoProceso,
      codigoEvaluacionDetSub: this.codigoProceso + 'REEVRIIC',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService
      .obtenerEvaluacion(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data) {
          if (result.data.length > 0) {
            this.evaluacion = result.data[0];
          } else {
            this.evaluacion.idEvaluacion = 0;
            this.evaluacion.idPlanManejo = this.idPlanManejo;
            this.evaluacion.codigoEvaluacion = this.codigoProceso;
            this.evaluacion.idUsuarioRegistro = this.usuarioService.idUsuario;
            this.evaluacion.listarEvaluacionDetalle = [];
          }
        }
      });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}

export class EvaluacionDetalle {
  idEvaluacionDet: any;
  idEvaluacion: any;
  codigoEvaluacionDet: any;
  codigoEvaluacionDetSub: any;
  codigoEvaluacionDetPost: any;
  conforme: any;
  observacion: any;
  idUsuarioRegistro: any;

  constructor(obj?: any) {
    if (obj) {
      this.idEvaluacion = obj.idEvaluacion ? obj.idEvaluacion : 0;
      this.idEvaluacionDet = obj.idEvaluacionDet ? obj.idEvaluacionDet : 0;
      this.codigoEvaluacionDet = obj.codigoEvaluacionDet
        ? obj.codigoEvaluacionDet
        : '';
      this.codigoEvaluacionDetSub = obj.codigoEvaluacionDetSub
        ? obj.codigoEvaluacionDetSub
        : '';
      this.codigoEvaluacionDetPost = obj.codigoEvaluacionDetPost
        ? obj.codigoEvaluacionDetPost
        : '';
      this.conforme = obj.conforme ? obj.conforme : false;
      this.observacion = obj.observacion ? obj.observacion : '';
      this.idUsuarioRegistro = obj.idUsuarioRegistro
        ? obj.idUsuarioRegistro
        : 0;
    }
  }
}
