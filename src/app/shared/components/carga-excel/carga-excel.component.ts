import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { FileModel } from 'src/app/model/util/File';
import { environment } from '../../../../environments/environment';
import { ToastService } from '../../services/toast.service';
import { DownloadFile } from '../../util';

@Component({
  selector: 'app-carga-excel',
  templateUrl: './carga-excel.component.html',
  styleUrls: ['./carga-excel.component.scss']
})
export class CargaExcelComponent implements OnInit {
  @Input() disabled: boolean = false;
  @Input() isDisbledObjFormu!: boolean;
  @Input() urlFomato!: string;
  @Input() NombrePlantilla: string = "";
  isNombrePlantilla: boolean = false;

  @Output() cargarArchivo = new EventEmitter();

  file: FileModel = {} as FileModel;
  files: FileModel[] = [];

  varAssets = `${environment.varAssets}`;
  urlFormatoNew: string = "";

  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private archivoService: ArchivoService,
  ) { }

  ngOnInit(): void {
    this.isNombrePlantilla = !!this.NombrePlantilla;

    if (this.varAssets && this.varAssets != "") {
      this.urlFormatoNew = "/" + this.varAssets + this.urlFomato;
    } else {
      this.urlFormatoNew = this.urlFomato;
    }

  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();

    this.files = [];
    this.file.url = URL.createObjectURL(e.target.files[0]);
    this.file.nombreFile = e.target.files[0].name;
    this.file.file = e.target.files[0];
    this.files.push(this.file);
  }

  cargarFormato() {
    this.cargarArchivo.emit(this.files);
    this.file = {} as FileModel;
    this.files = [];
  }

  btnDescargarFormato() {
    const NombreGenerado: string = this.NombrePlantilla;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.descargarPlantilla(NombreGenerado).subscribe((data: any) => {
      this.dialog.closeAll();
      if (data.isSuccess) {
        DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
      } else {
        this.toast.error('Ocurrió un problema, intente nuevamente');
      }
    }, () => {
      this.dialog.closeAll();
      this.toast.error('Ocurrió un problema, intente nuevamente');
    });
  }
}
