import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "listado-observaciones",
  templateUrl: "./listado-observaciones.component.html",
  styleUrls: ["./listado-observaciones.component.scss"],
})
export class ListadoObservacionesComponent implements OnInit {
  @Input() listaEvaluacion: any;
  @Input() item: any;
  @Output() btnRedirigir: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  btnDireccion(data: any) {
    this.btnRedirigir.emit(data);
  }
}
