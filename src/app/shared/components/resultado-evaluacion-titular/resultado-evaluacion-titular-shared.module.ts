import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { InformeEvaluacionSharedModule } from '../informe-evaluacion-shared/informe-evaluacion-shared.module';
import { InputButtonsModule } from '../input-button/input-buttons.module';
import { ResultadoEvaluacionTitularSharedComponent } from './resultado-evaluacion-titular-shared.component';


import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DialogModule } from "primeng/dialog";
import { MatTabsModule } from '@angular/material/tabs';
import { ListadoObservacionesComponent } from './components/listado-observaciones/listado-observaciones.component';


@NgModule({
  declarations: [
    ResultadoEvaluacionTitularSharedComponent,
    ListadoObservacionesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    ConfirmPopupModule,
    InputButtonsModule,
    RadioButtonModule,
    InformeEvaluacionSharedModule,
    DropdownModule,
    AccordionModule,
    TableModule,
    ToastModule,
    ConfirmDialogModule,
    BrowserModule,
    BrowserAnimationsModule,
    DialogModule,
    ButtonModule,
    MatTabsModule
  ],
  exports:[ResultadoEvaluacionTitularSharedComponent]
})
export class ResultadoEvaluacionTitularSharedModule { }
