import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from '@shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { CodigoEstadoMesaPartes, CodigoEstadoPlanManejo } from 'src/app/model/util/CodigoEstadoPlanManejo';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { ConfirmationService } from 'primeng/api';
import { PrimeNGConfig } from "primeng/api";
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { NotificacionService } from 'src/app/service/notificacion';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { UsuarioService } from '@services';
import { UsuarioModel } from '../../../model/seguridad/usuario';


@Component({
  selector: 'resultado-evaluacion-titular-shared',
  templateUrl: './resultado-evaluacion-titular-shared.component.html',
  styleUrls: ['./resultado-evaluacion-titular-shared.component.scss'],
})


export class ResultadoEvaluacionTitularSharedComponent implements OnInit {

  listaEvaluacion: any = [];
  listaEvaluacionTabs: any = [];
  item: any = {};
  @ContentChild('acciones') accionesRef!: TemplateRef<any>;
  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;

  CodigoProceso = CodigoProceso;
  Perfiles= Perfiles;
  descripcionPlan: any;
  usuario!: UsuarioModel;
  idUsuario: number = 0
  perfil: string = ''
  @Input() disabled!: boolean;
  constructor(
    private toast: ToastService,
    private dialog: MatDialog,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmationService: ConfirmationService,
    private evaluacionService: EvaluacionService,
    private primengConfig: PrimeNGConfig,
    private notificacionService: NotificacionService,
    private usuarioService: UsuarioService,
    private userSer: UsuarioService,
  ) {
    this.item = this.config.data.item;

  }

  ngOnInit() {
    this.listadoEvaluacion();
    //this.mostrarAviso();
    this.primengConfig.ripple = true;
    this.usuario = this.userSer.usuario;
    this.perfil=this.usuario.sirperfil
    this.idUsuario = this.userSer.idUsuario;
    this.listadoEvaluacionTabs()
  }

  geeks: boolean = true;

  listadoEvaluacion() {
    var params = {
      idPlanManejo: this.item.idPlanManejo,
      codigoEvaluacion: 'EVAL'
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService.listarEvaluacionResumido(params).pipe(finalize(() => this.dialog.closeAll())).subscribe((response: any) => {
        if (response.success) {
          this.listaEvaluacion = response.data[0]?.listarEvaluacionDetalle;
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  listadoEvaluacionTabs() {
    var params = {
      idPlanManejo: this.item.idPlanManejo,
      codigoEvaluacion: 'PLAN',
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService.listarEvaluacionResumido(params).pipe(finalize(() => this.dialog.closeAll())).subscribe((response: any) => {
        if (response.success) {
          this.listaEvaluacionTabs = response.data[0]?.listarEvaluacionDetalle;
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  mostrarAviso() {
    this.confirmationService.confirm({
      message: 'La no presentación de subsanaciones dentro del plazo hará considerar el documento de gestión (PMFI/DEMA) como "No presentado" conforme al literal 4 del articulo 136 del TUO de la LGPA ley 27444.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Aceptar',
      accept: () => {
        this.ref.close
      },

    });

  }

  btnRedirigir(data: any, tipo?:string) {
    let params = {
      data: data,
      tipo:tipo
    }
    this.ref.close(params);
  }

  btnCancelar() {
    this.ref.close();
  }

  btnRemitir() {

    this.confirmationService.confirm({
      message: '¿ Está seguro de Remitir la Solicitud sobre el levantanmiento de Obervaciones ?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.actualizarPlanManejo();
      },

    });

  }

  actualizarPlanManejo(){
    var param = {
      idPlanManejo: this.item.idPlanManejo,
      codigoEstado: CodigoEstadoMesaPartes.EN_EVALUACION,
      idUsuarioModificacion: this.idUsuario
    };
    //console.log("Params...",param);

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.evaluacionService.actualizarPlanManejo(param).pipe(finalize(() => this.dialog.closeAll())).subscribe((resp: any)=> {

      if(resp.success) {
        var obj: any = { remitir: true};
        this.btnRedirigir(obj);
        this.toast.ok(resp?.message);
        this.registrarNotificacion();
      }
      else this.toast.error(resp?.message)
    });

  }



  private registrarNotificacion() {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: this.CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO,
      numDocgestion: this.item.idPlanManejo,
      mensaje: 'Se informa que se ha registrado el plan '
        + this.CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO
        + ' Nro. '
        + this.item.idPlanManejo
        + ' para evaluación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.usuario.idusuario,
      //codigoPerfil: this.user.usuario.sirperfil,
      codigoPerfil: 'ARFFS',
      fechaInicio: new Date(),
      cantidadDias: 10,
      url: '/planificacion/evaluacion/bandeja-eval-pmfi-dema',
      idUsuarioRegistro: this.usuario.idusuario
    }
    this.notificacionService
      .registrarNotificacion(params)
      .subscribe()
  }

}
