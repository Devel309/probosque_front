import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CoreCentralService } from "@services";
import { DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";

@Component({
  selector: "app-dialog-tipo-bosque",
  templateUrl: "./dialog-tipo-bosque.component.html",
  styleUrls: ["./dialog-tipo-bosque.component.scss"],
})
export class DialogTipoBosqueComponent implements OnInit {
  queryBosque: string = "";
  comboListBosques: any[] = [];
  selectedValues: any[] = [];

  constructor(
    private coreCentralService: CoreCentralService,
    private dialog: MatDialog,
    public ref: DynamicDialogRef
  ) {}

  ngOnInit(): void {
    this.listaPorFiltroEspecieFauna();
  }

  filtrarFauna() {
    if (this.queryBosque) {
      this.comboListBosques = this.comboListBosques.filter((r) =>
        r.descripcion.toLowerCase().includes(this.queryBosque.toLowerCase())
      );
    } else {
      this.listaPorFiltroEspecieFauna();
    }
  }

  listaPorFiltroEspecieFauna() {
    let params = {};
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.coreCentralService
      .listarPorFiltroTipoBosque(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.comboListBosques = [...result.data];
      });
  }

  /* onChangeBosque(event: any, data: any) {
  this.listaFauna.forEach((item) => {
    if (event.checked === false && data.idEspecie === item.idFauna) {
      let params = {
        idInfBasica: 0,
        idInfBasicaDet: item.idInfBasicaDet,
        codInfBasicaDet: "PGMFAFAU",
        idUsuarioElimina: this.user.idUsuario,
      };
      this.informacionAreaPmfiService
        .eliminarInformacionBasica(params)
        .subscribe((response: any) => {});
    }
  });
} */

agregar() {
  this.ref.close(this.selectedValues);
  
}
cerrarModal() {
  this.ref.close();
}

}
