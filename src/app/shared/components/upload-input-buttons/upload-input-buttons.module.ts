import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadInputButtonsComponent } from './upload-input-buttons.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import {ConfirmPopupModule} from 'primeng/confirmpopup';



@NgModule({
  declarations: [
    UploadInputButtonsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    ConfirmPopupModule
  ],
  exports:[UploadInputButtonsComponent]
})
export class UploadInputButtonsModule { }
