import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { DowloadFileLocal } from '@shared';
import { Confirmation, ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { DownloadFile } from '../../util';

@Component({
  selector: 'upload-input-buttons',
  templateUrl: './upload-input-buttons.component.html',
  styleUrls: ['./upload-input-buttons.component.scss']
})
export class UploadInputButtonsComponent implements OnInit {

  @ViewChild("fileInput") fileInputVariable: any;

  @Input() file: any = {};
  @Input() label: string = "";
  @Input() disabled: boolean = false;
	@Input() accept: string = '';
  @Input() disableSubir: boolean = false;

  @Output() registrarArchivo = new EventEmitter();
  @Output() eliminarArchivo = new EventEmitter();

  constructor(private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,

  ) { }

  ngOnInit(): void {

  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.file.url = URL.createObjectURL(event.target.files[0]);
    this.file.file = event.target.files[0];
    this.file.nombre = event.target.files[0].name;

    this.registrarArchivo.emit(this.file);
    this.fileInputVariable.nativeElement.value = "";
  }

  eliminar(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      key: "fileDelete",
      accept: () => {
        this.eliminarArchivo.emit(this.file);
        this.fileInputVariable.nativeElement.value = "";
      },

    });

  }

  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });

    if (this.file.idArchivo) {
      let params = {
        idArchivo: this.file.idArchivo
      }

      this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      })

    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }

  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }


}
