import { HttpErrorResponse } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Page } from '@models';
import {
  ArchivoService,
  ParametroValorService,
  UsuarioService,
} from '@services';
import { DownloadFile, ToastService } from '@shared';
import { data } from 'jquery';
import * as moment from 'moment';
import {
  ConfirmationService,
  LazyLoadEvent,
  MessageService,
} from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import {
  ListarMesaPartesDetallePmficDema,
  MesaPartesPmficDema,
} from 'src/app/model/MesaPartesPmficDema/MesaPartesPmficDemaModel';
import { CodigoEstadoMesaPartesEvaluacion } from 'src/app/model/util/CodigoEstadoMesaPartes';
import { FileModel } from 'src/app/model/util/File';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { MesaPartesService } from 'src/app/service/evaluacion/evaluacion-plan-operativo/mesa-partes.service';
import { EvaluacionService } from 'src/app/service/evaluacion/evaluacion.service';
import { InformacionGeneralService } from 'src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { FirmaPadComponent } from '../firma-pad/firma-pad.component';

@Component({
  selector: 'tab-requisitos-tupa',
  templateUrl: './requisitos-tupa.component.html',
  styleUrls: ['./requisitos-tupa.component.scss'],
})
export class TabRequisitosTupaComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() tipo!: string;
  @Input() filtro: any = {
    fechaTramite: null,
    detalle: null,
  };

  @Input() codigoProceso!: string;
  @Input() idTipoDocumento!: string;
  @Input() idTipoDocumentoFirma!: string;
  @Input() disabled: boolean = false;
  @Output() longitudTUPA = new EventEmitter();
  @Output('idMesaPartes') emitMesaPartes = new EventEmitter();
  @Output('haveInfoMesaPartes') infoMesaPartes = new EventEmitter();

  @ViewChild('firma', { static: true }) firma!: FirmaPadComponent;

  evaluacion: LineamientoInnerModel = new LineamientoInnerModel();
  verModalMantenimiento: boolean = false;

  queryRequisito: string = '';

  idMesaPartes: number = 0;

  listProcedimientosAdministrativos: any[] = [];

  isEditarFechaDePartes: boolean = false;
  isEditarEvaluacionArffs: boolean = false;
  verModalRequisito: boolean = false;
  selectedValues: string = '';

  editar: boolean = false;

  idArchivoEmit: number = 0;
  idArchivoFirma: number = 0;
  nombreArchivo: string = '';

  mesaPartes = new MesaPartesPmficDema();

  listadoTupa: any = [];

  lineamiento: ListarMesaPartesDetallePmficDema =
    new ListarMesaPartesDetallePmficDema();
  archivoAdjunto: any = {};

  edit: boolean = false;
  editJustificacion: boolean = false;
  indexLineamiento!: any;

  pendiente: boolean = false;

  perfil: string = '';

  isJustificacion: boolean = false;

  /* mesaPartesObj: MesaPartesCabeceraModel = new MesaPartesCabeceraModel();
  mesaPartesDetalleObj: MesaPartesDetalleModel = new MesaPartesDetalleModel();
  listMesaPartesDetalle: MesaPartesDetalleModel[] = []; */
  codigoEdoMesaPartesEvaluacion = CodigoEstadoMesaPartesEvaluacion;
  codEstadoConforme: string = '';

  static get EXTENSIONSAUTHORIZATION2() {
    return ['.pdf', 'image/png', 'image/jpeg', 'image/jpeg', 'application/pdf'];
  }

  files: FileModel[] = [];
  filePMFI: FileModel = {} as FileModel;
  cargarPMFI: boolean = false;
  eliminarPMFI: boolean = true;
  idArchivoPMFI: number = 0;
  tieneArchivo: boolean = false;
  verEnviar1: boolean = false;
  minDate = moment(new Date()).format('YYYY-MM-DD');
  fechaDocumento: any;
  fechaDocumentoMdp: any;
  listTipos: any[] = [];
  perfiles = Perfiles;
  tamList: number = 0;

  verAdjuntarTupa: boolean = false;
  verAdjuntarFirma: boolean = false;
  idValuesAdjuntarFirma: number = 0;
  valuesAdjuntarFirma: ListarMesaPartesDetallePmficDema =
    new ListarMesaPartesDetallePmficDema();

  selectAll: boolean = false;

  selectedCustomers: any[] = [];

  isEvaluacionArffs: boolean = false;
  isNewEvaluacionArffs: boolean = false;
  isCodigoEstadoPlanCompletadoMdp: boolean = false;


  codigoPlanRequisitoTupa=''


  isActualizarMDP:boolean=false

  constructor(
    //  private evaluacionPlanManejoService: EvaluacionPlanManejoCcnnService,
    private confirmationService: ConfirmationService,
    private mesaPartesService: MesaPartesService,
    private toast: ToastService,
    private anexosService: AnexosService,
    private messageService: MessageService,
    private dialog: MatDialog,
    private user: UsuarioService,
    private archivoServ: ArchivoService,
    private parametroValorService: ParametroValorService,
    private route: ActivatedRoute,
    private informacionGeneralService: InformacionGeneralService,
    private evaluacionService: EvaluacionService
  ) {}

  ngOnInit(): void {
    this.filePMFI.inServer = false;
    this.filePMFI.descripcion = 'PDF';
    // this.tipoDocumento();
    let params = {
      pageNum: 1,
      pageSize: 10,
      ubigeo: '000000',
    };
    //this.listarProcedimientosAdministrativos(params);

    this.perfil = this.user.usuario.sirperfil;

    this.obtenerEstadoPlan();
  }

  obtenerEstadoPlan() {
    const body = {
      idPlanManejo: this.idPlanManejo,
    };
    this.informacionGeneralService
      .listarInfGeneralResumido(body)
      .subscribe((_res: any) => {
        if (_res.data.length > 0) {
          const estado = _res.data[0].codigoEstado;

          this.isCodigoEstadoPlanCompletadoMdp =
            estado == 'EMDCOMP' &&
            this.perfil == this.perfiles.AUTORIDAD_REGIONAL
              ? false
              : true;

            this.isActualizarMDP= estado == 'EMDCOMP' &&
            this.perfil == this.perfiles.AUTORIDAD_REGIONAL;

              

        }
      });
  }

  tipoDocumento() {
    var params = {
      prefijo: 'TDOCGE',
    };
    this.parametroValorService
      .listarPorCodigoParametroValor(params)
      .subscribe((response: any) => {
        this.listTipos = response.data;
      });
  }

  loadMesaPartes(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNumber = Number(e.first) / pageSize + 1;
    const page = new Page({ pageNumber, pageSize });

    const allItems =
      this.listadoTupa.filter((data: any) => data.conforme == 'true').length ==
      this.listadoTupa.length;

    if (!!allItems) {
      this.evaluacion.conforme = true;
    } else {
      this.evaluacion.conforme = false;
    }
    this.listMesaPartes(page);
  }

  listMesaPartesFiltro(filtro: any) {
    if (filtro.fechaTramite != null || filtro.detalle != null) {
      this.filtro = filtro;
      this.listMesaPartes();
    }
  }

  listMesaPartes(page?: Page) {
    const mesaDePartes = this.listadoTupa.filter(
      (data: any) => data.fecha != null && data.observacion != null
    );
    this.infoMesaPartes.emit(mesaDePartes.length > 0);

    this.isJustificacion = false;
    var params = {
      idPlanManejo: this.idPlanManejo,
      fechaTramite: this.filtro.fechaTramite
        ? new Date(this.filtro.fechaTramite).toISOString()
        : null,
      detalle: this.filtro.detalle ? this.filtro.detalle : null,
      // pageNum: page?.pageNumber,
      // pageSize: page?.pageSize,
    };

    this.mesaPartesService
      .listarMesaPartesPmficDema(params)
      .subscribe((response: any) => {
        this.isEditarFechaDePartes = false;
        this.isEditarEvaluacionArffs = false;
        this.verModalMantenimiento = false;

        if (response.data.length != 0) {
          response.data.forEach((element: any) => {
            this.mesaPartes = new MesaPartesPmficDema(element);

            this.evaluacion.observacion = response.data[0].descripcion;
            this.codEstadoConforme = response.data[0].conforme;
            this.idMesaPartes = element.idMesaPartes;
            this.emitMesaPartes.emit(this.idMesaPartes);

            if (this.tipo == 'TUPA') {
              if (element.listarMesaPartesRequisitosTUPA.length != 0) {
                this.longitudTUPA.emit(
                  element.listarMesaPartesRequisitosTUPA.length
                );
                this.tamList = element.listarMesaPartesRequisitosTUPA.length;
                this.listadoTupa = [];
                element.listarMesaPartesRequisitosTUPA.forEach(
                  (element: any) => {
                    if (element.conforme == 'false') {
                      this.isJustificacion = true;
                    }

                    this.listadoTupa.push(element);
                  }
                );
                const allItems =
                  this.listadoTupa.filter(
                    (data: any) => data.conforme == 'true'
                  ).length == this.listadoTupa.length;

                const mesaDePartes = this.listadoTupa.filter(
                  (data: any) => data.fecha != null && data.observacion != null
                );
                this.infoMesaPartes.emit(mesaDePartes.length > 0);
                if (!!allItems) {
                  this.evaluacion.conforme = true;
                } else {
                  this.evaluacion.conforme = false;
                }
              } else {
                this.listadoTupa = [];
              }
            }
          });
          this.editar = false;
        } else {
          this.editar = true;
        }
      });
  }

  lazyLoadProcedimiento(e: LazyLoadEvent) {
    const pageSize = Number(e.rows);
    const pageNum = Number(e.first) / pageSize + 1;

    let params = {
      pageNum: pageNum,
      pageSize: pageSize,
      ubigeo: '000000',
    };

    this.listarProcedimientosAdministrativos(params);
  }

  listarProcedimientosAdministrativos(params: any) {
    this.listProcedimientosAdministrativos = [];

    /* this.evaluacionPlanManejoService
      .listarProcedimientoAdministrativo(params)
      .subscribe((result: any) => {
        result.data.forEach((ele: any) => {
          let obje = {
            codigoProcedimiento: ele.codigoProcedimiento,
            idProcedimiento: ele.idProcedimiento,
            procedimientoAdministrativo: ele.procedimientoAdministrativo,
            ubigeo: ele.ubigeo,
          };

          this.listProcedimientosAdministrativos.push(obje);
        });
      }); */
  }

  abrirModalRequisitos() {
    this.selectedValues = '';
    this.verModalRequisito = true;
  }

  editarLineamiento(data: any, index: number) {
    if (this.perfil == this.perfiles.AUTORIDAD_REGIONAL) {
      this.isEvaluacionArffs = true;
      this.isEditarEvaluacionArffs = true;
    }
    this.tipoDocumento();

    this.fechaDocumento = '';

    this.filePMFI.nombreFile = '';
    this.lineamiento = new ListarMesaPartesDetallePmficDema();
    this.archivoAdjunto = {};
    this.idArchivoEmit = 0;
    this.indexLineamiento = null;
    this.edit = true;

    this.lineamiento = new ListarMesaPartesDetallePmficDema(data);
    this.fechaDocumento = new Date(data.fechaDocumento);
    this.fechaDocumentoMdp = new Date(data.fecha);

    this.indexLineamiento = index;
    this.listarArchivoDemaFirmado(data.idArchivo);

    this.verModalMantenimiento = true;
  }

  adjuntarTupa() {
    if (
      this.codigoProceso == 'PMFIC' ||
      this.codigoProceso == 'PMFI' ||this.codigoProceso == 'PGMFA' ||this.codigoProceso == 'POAC' ||
      this.codigoProceso == 'DEMA'
    ) {
      this.tipoDocumento();
      this.verAdjuntarTupa = true;
      this.codigoPlanRequisitoTupa='EPLMPRES'
    } else {
      this.abrirModalMantenimiento();
    }
  }

  editarFechaDePartes() {
    if (!this.validarCamposTupa()) {
      return;
    }

    this.tipoDocumento();
    let objeto = this.lineamiento;
    objeto.fecha = new Date(this.fechaDocumentoMdp).toISOString();
    objeto.observacion = this.lineamiento.observacion;
    const mesaPartes = this.mesaPartes;
    mesaPartes.idPlanManejo = this.idPlanManejo;
    mesaPartes.idUsuarioRegistro = this.user.idUsuario;
    mesaPartes.listarMesaPartesRequisitosTUPA = [];
    let listarMesaPartesRequisitosTUPA = [];
    const obj = new ListarMesaPartesDetallePmficDema(objeto);

    listarMesaPartesRequisitosTUPA.push(obj);

    mesaPartes.listarMesaPartesRequisitosTUPA = listarMesaPartesRequisitosTUPA;

    this.mesaPartesService
      .registrarMesaPartesPmficDema(mesaPartes)
      .subscribe(() => {
        this.toast.ok('Se registró, exitosamente.');
        this.listMesaPartes();
      });
  }

  openModalFechaMdp(data: any, index: number) {
    this.tipoDocumento();
    this.edit = true;
    this.indexLineamiento = 0;
    this.fechaDocumento = '';
    this.fechaDocumentoMdp = '';
    this.lineamiento = new ListarMesaPartesDetallePmficDema();

    this.verModalMantenimiento = true;
    this.isEditarFechaDePartes = true;

    this.lineamiento = new ListarMesaPartesDetallePmficDema(data);

    this.fechaDocumento = new Date(this.lineamiento.fechaDocumento);

    if (this.lineamiento.fecha) {
      this.fechaDocumentoMdp = new Date(this.lineamiento.fecha);
    }
    this.indexLineamiento = index;
  }

  modalAdjuntarFirma(data: any, index: number) {
    this.valuesAdjuntarFirma = new ListarMesaPartesDetallePmficDema();
    this.idValuesAdjuntarFirma = 0;

    this.verAdjuntarFirma = true;
    this.valuesAdjuntarFirma = new ListarMesaPartesDetallePmficDema(data);
    this.idValuesAdjuntarFirma = index;
  }

  registrarFirmaLineamiento(idFirma: number) {
    let objeto = new ListarMesaPartesDetallePmficDema(this.valuesAdjuntarFirma);

    objeto = this.listadoTupa[this.idValuesAdjuntarFirma];
    objeto.archivo = String(idFirma);
    this.fechaDocumento = new Date(
      this.valuesAdjuntarFirma.fechaDocumento
    ).toISOString();

    const mesaPartes = this.mesaPartes;
    mesaPartes.idPlanManejo = this.idPlanManejo;
    mesaPartes.idUsuarioRegistro = this.user.idUsuario;
    mesaPartes.listarMesaPartesRequisitosTUPA = [];
    let listarMesaPartesRequisitosTUPA = [];
    const obj = new ListarMesaPartesDetallePmficDema(objeto);

    listarMesaPartesRequisitosTUPA.push(obj);

    mesaPartes.listarMesaPartesRequisitosTUPA = listarMesaPartesRequisitosTUPA;

    this.mesaPartesService
      .registrarMesaPartesPmficDema(mesaPartes)
      .subscribe(() => {
        this.toast.ok('Se registró, exitosamente.');
        this.listMesaPartes();
      });
  }

  abrirModalMantenimiento() {
    if (this.perfil == this.perfiles.AUTORIDAD_REGIONAL) {
      this.isNewEvaluacionArffs = true;
      this.isEvaluacionArffs = true;
    }

    this.verAdjuntarTupa = false;
    this.queryRequisito = '';
    this.filePMFI.nombreFile = '';
    this.lineamiento = new ListarMesaPartesDetallePmficDema();
    this.archivoAdjunto = {};
    this.edit = false;
    this.fechaDocumento = '';
    this.fechaDocumentoMdp = '';
    this.verModalMantenimiento = true;
    this.idArchivoEmit = 0;
    this.nombreArchivo = '';
  }

  registrarLineamiento() {
    if (!this.validarCamposTupa()) {
      return;
    }
    let objeto = new ListarMesaPartesDetallePmficDema();
    if (this.edit) {
      this.filePMFI.nombreFile = '';
      objeto = this.listadoTupa[this.indexLineamiento];
      if (this.idArchivoEmit > 0) {
        objeto.descripcion = this.nombreArchivo;
        objeto.idArchivo = this.idArchivoEmit;
      }

      objeto.codigo = this.lineamiento.codigo;
      objeto.conforme = this.lineamiento.conforme;
      objeto.fechaDocumento = new Date(this.fechaDocumento).toISOString();
      objeto.comentario = this.lineamiento.comentario;
      objeto.detalle = this.lineamiento.detalle;
      this.pendiente = true;

      if (objeto.conforme == 'false') {
        objeto.justificacion = this.lineamiento.justificacion;
      }
    } else {
      this.lineamiento.descripcion = this.nombreArchivo;
      this.lineamiento.fechaDocumento = new Date(
        this.fechaDocumento
      ).toISOString();
      this.listadoTupa.push(this.lineamiento);
      this.pendiente = true;
      this.lineamiento.idArchivo = this.idArchivoEmit;
      this.lineamiento.conforme= 'EMDBOR'
      this.lineamiento.tipoRequisito = 'REQTUPA';
    }

    const mesaPartes = this.mesaPartes;
    mesaPartes.idPlanManejo = this.idPlanManejo;
    mesaPartes.idUsuarioRegistro = this.user.idUsuario;
    mesaPartes.conforme= 'EMDBOR'
    mesaPartes.listarMesaPartesRequisitosTUPA = [];
    let listarMesaPartesRequisitosTUPA = [];
    let obj;

    if (this.edit) {
      obj = new ListarMesaPartesDetallePmficDema(objeto);
    } else {
      obj = new ListarMesaPartesDetallePmficDema(this.lineamiento);
    }

    listarMesaPartesRequisitosTUPA.push(obj);

    this.dialog.open(LoadingComponent, { disableClose: true });
    mesaPartes.listarMesaPartesRequisitosTUPA = listarMesaPartesRequisitosTUPA;
    this.mesaPartesService
      .registrarMesaPartesPmficDema(mesaPartes)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(() => {
        this.listMesaPartes();
      });

    this.verModalMantenimiento = false;

    this.isNewEvaluacionArffs = false;
    this.isEvaluacionArffs = false;
  }

  eliminarLineamiento(event: any, data: any, index: number): void {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        if (!!data.idMesaPartesDet) {
          var params = [
            {
              idMesaPartesDet: data.idMesaPartesDet,
              idUsuarioElimina: this.user.idUsuario,
            },
          ];
          this.mesaPartesService
            .eliminarMesaPartesDetalle(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.toast.ok(response?.message);
                this.listadoTupa.splice(index, 1);
              } else {
                this.toast.error(response?.message);
              }
            });
        } else {
          this.listadoTupa.splice(index, 1);
        }
        this.tamList -= 1;
        this.longitudTUPA.emit(this.tamList);
      },
    });
  }

  descargaArchivo(idArchivo: number) {
    //this.listarArchivoDemaFirmado(idArchivo);

    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.toast.error(error.message);
            this.dialog.closeAll();
          };
        });
    }
  }

  seleccionarProcedimiento(data: any) {
    this.queryRequisito = data.codigoProcedimiento;
    this.verModalRequisito = false;
  }

  filtrarRequisitos() {
    if (this.queryRequisito) {
      this.listProcedimientosAdministrativos =
        this.listProcedimientosAdministrativos.filter((r) =>
          r.procedimientoAdministrativo
            .toLowerCase()
            .includes(this.queryRequisito.toLowerCase())
        );
    } else {
      let params = {
        pageNum: 1,
        pageSize: 10,
        ubigeo: '000000',
      };
      this.listarProcedimientosAdministrativos(params);
    }
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  listarArchivoDemaFirmado(id: number) {
    var params = {
      idArchivo: id ? id : null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: this.idTipoDocumento,
      codigoProceso: this.codigoProceso,
    };

    // this.anexosService
    // .listarPlanManejoArchivo(params)
    //   .subscribe((result: any) => {
    //     if (result) {
    //       this.cargarPMFI = true;
    //       this.eliminarPMFI = false;
    //       this.nombreArchivo = result.nombre;
    //       this.idArchivoEmit = result.idArchivo;
    //     } else {
    //       this.cargarPMFI = true;
    //       this.eliminarPMFI = false;
    //     }
    //   });
  }

  changeConforme(value: string) {
    const allItems =
      this.listadoTupa.filter((data: any) => data.conforme == 'true').length ==
      this.listadoTupa.length;

    if (value == 'SI' && !!allItems) {
      this.evaluacion.conforme = true;
    } else {
      this.evaluacion.conforme = false;
    }
  }

  descargarArchivo(id: any) {
    const idArchivo = parseInt(id);
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.errorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.errorMensaje('No se ha encontrado un documento vinculado.');
      this.dialog.closeAll();
    }
  }

  onSelectionChange(value = []) {
    this.selectAll = value.length === this.listadoTupa.length;
    this.selectedCustomers = value;
  }

  onSelectAllChange(event: any) {
    const checked = event.checked;

    if (checked) {
      this.listadoTupa.map((res: any) => {
        this.selectedCustomers = res.customers;
        this.selectAll = true;
      });
    } else {
      this.selectedCustomers = [];
      this.selectAll = false;
    }
  }

  updateEvaluacion() {
    // actualiza la informacion del evaluador
    if (!this.validarCamposTupa()) {
      return;
    }
    let objeto = this.lineamiento;

    if (objeto.conforme == 'true') {
      objeto.justificacion = '';
    }

    const mesaPartes = this.mesaPartes;
    mesaPartes.idPlanManejo = this.idPlanManejo;
    mesaPartes.idUsuarioRegistro = this.user.idUsuario;
    mesaPartes.listarMesaPartesRequisitosTUPA = [];
    let listarMesaPartesRequisitosTUPA = [];
    const obj = new ListarMesaPartesDetallePmficDema(objeto);

    listarMesaPartesRequisitosTUPA.push(obj);

    mesaPartes.listarMesaPartesRequisitosTUPA = listarMesaPartesRequisitosTUPA;

    this.mesaPartesService
      .registrarMesaPartesPmficDema(mesaPartes)
      .subscribe(() => {
        this.toast.ok('Se adjuntó la firma correctamente.”');
        this.listMesaPartes();
        this.isJustificacion = false;
        this.isEvaluacionArffs = false;
        this.isNewEvaluacionArffs = false;

        if (this.isActualizarMDP) {
          this.cambiarEstadoEvaluacion();
        }
      });
  }

  cambiarEstadoEvaluacion() {
    const body = {
      idMesaPartes: this.idMesaPartes,
      idPlanManejo: null,
      conforme: 'EEVAEVAL',
    };


    this.mesaPartesService
      .registrarMesaPartesMdp(body)
      .subscribe((data: any) => {});
  }

  validarCamposTupa() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.lineamiento.codigo == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar: Tipo de documento.\n';
    }
    if (this.fechaDocumento == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar: Fecha de documento.\n';
    }
    // if (this.lineamiento.detalle == '') {
    //   validar = false;
    //   mensaje = mensaje += '(*) Debe ingresar: No. Trámite.\n';
    // }
    if (
      this.lineamiento.conforme == 'false' &&
      this.lineamiento.justificacion == '' &&
      this.isEvaluacionArffs
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: justificación.\n';
    }
    if (this.lineamiento.archivo == '' && this.isEvaluacionArffs) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: firma electrónica.\n';
    }
    if (this.lineamiento.conforme == '' && this.isEvaluacionArffs) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Evaluación.\n';
    }

    if (
      this.fechaDocumentoMdp == '' &&
      this.perfil == this.perfiles.MESA_DE_PARTES
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar: Fecha mesa de partes.\n';
    }
    if (
      this.lineamiento.observacion == '' &&
      this.perfil == this.perfiles.MESA_DE_PARTES
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Observaciones mesa de partes.\n';
    }

    if (!validar) this.errorMensaje(mensaje);

    return validar;
  }
}
