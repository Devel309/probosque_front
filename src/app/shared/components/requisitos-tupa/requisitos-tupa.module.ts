import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { EvaluacionSimpleModule } from '../evaluacion-simple/evaluacion-simple.module';
import { FirmaPadModule } from '../firma-pad/firma-pad.module';
import { InputButtonsCodigoIdModule } from '../input-button-codigo-id/input-button-codigo-id.module';
import { InputButtonsModule } from '../input-button/input-buttons.module';
import { UploadInputButtonsModule } from '../upload-input-buttons/upload-input-buttons.module';
import { TabRequisitosTupaComponent } from './requisitos-tupa.component';



@NgModule({
  declarations: [TabRequisitosTupaComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    RadioButtonModule,
    // material angular
    TableModule,
    EvaluacionSimpleModule,
    PanelModule,
    DropdownModule,
    AccordionModule,
    ToastModule,
    DialogModule,
    ButtonModule,
    UploadInputButtonsModule,
    ConfirmPopupModule,
    InputButtonsModule,
    MatDatepickerModule,
    InputButtonsCodigoIdModule,
    FirmaPadModule
  ],
  exports:[TabRequisitosTupaComponent]
})
export class RequisitoPgmfSimpleModule { }
