import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { isNullOrEmpty, ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";

@Component({
  selector: "app-modal-info-permiso",
  templateUrl: "./modal-info-permiso.component.html",
})
export class ModalInfoPermisoComponent implements OnInit {
  queryPermiso: string = "";
  listPermiso: any[] = [];
  selectPermiso: any;
  selectedValues: any[] = [];
  queryIdPermiso: string = "";
  permiso: any[] = [];
  idPermisoForestal: number = 0;
  buscar: boolean = false;
  related: boolean = false;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private informacionGeneralService: InformacionGeneralService,
    private toast: ToastService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.buscar = this.config.data.buscar
    this.idPermisoForestal = this.config.data.idPermisoForestal
    this.listarPermiso();
  }

  onChangePermiso(event: any, data: any){
    if (event.checked === true && this.selectedValues.length != 0) {
      this.selectedValues = [data];
      this.selectedValues.forEach(response => {
        this.selectPermiso = response
      })
    }
  }

  filtrarIdPermiso(idPermisoForestal: number) {
    this.selectedValues = []
    this.permiso = this.listPermiso.filter(
      (x) => idPermisoForestal == x.idPermisoForestal
    );
    this.permiso.forEach((response) => {
      this.selectPermiso = response;
      this.selectedValues.push(this.selectPermiso)
    });
  }

  filtrarPermiso() {
    if (this.queryPermiso) {
      this.listPermiso = this.listPermiso.filter(
        (r) => r.idPermisoForestal == this.queryPermiso
      );
    } else if (this.queryIdPermiso) {
      this.filtrarId();
    } else {
      this.listarPermiso();
    }
  }

  filtrarId() {
    this.selectedValues = []
    if (this.queryIdPermiso) {
      this.listPermiso = this.listPermiso.filter(
        (r) => r.idPermiso == this.queryIdPermiso
      );
      this.permiso.forEach((response) => {
        this.selectPermiso = response;
        this.selectedValues.push(this.selectPermiso)
      });
    } else {
      this.listarPermiso();
    }
  }

  listarPermiso() {    
    let numeroDoc = '';
    let nroRucEmp = '';
    if (this.config.data.idTipoDocumento == 4) {
      nroRucEmp = this.config.data.nroDocumento;
    } else {
      numeroDoc = this.config.data.nroDocumento;
    }    
    if (this.buscar) {
      if (this.idPermisoForestal != 0) {
        numeroDoc = '';
        nroRucEmp = '';
        this.related = true;
      }
    }        
    let params = {
      nroDocumento: numeroDoc,
      nroRucEmpresa: nroRucEmp,
      idPlanManejo: this.config.data.idPlanManejo
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarPorFiltroOtorgamiento(params)
      .subscribe(
      (response: any) => {
        this.dialog.closeAll();
        if (response.success == true) {
          response.data.forEach((item: any) => {
            if (item.informacionGeneral.codTipoPersona == 'TPERJURI') {
              item.nombreComunidad = item.informacionGeneral.federacionComunidad;
              item.documento = item.informacionGeneral.rucComunidad;
            } else {
              item.nombreComunidad = '';
              item.documento = item.informacionGeneral.documentoElaborador;
            }            
            item.nombreElaborador = !isNullOrEmpty(item.informacionGeneral.nombreElaborador) ? item.informacionGeneral.nombreElaborador : '';
            item.apellidoPaternoElaborador = !isNullOrEmpty(item.informacionGeneral.apellidoPaternoElaborador) ? item.informacionGeneral.apellidoPaternoElaborador : '';
            item.apellidoMaternoElaborador = !isNullOrEmpty(item.informacionGeneral.apellidoMaternoElaborador) ? item.informacionGeneral.apellidoMaternoElaborador : '';
            item.jefeComunidad = item.nombreElaborador + ' ' + item.apellidoPaternoElaborador + ' ' + item.apellidoMaternoElaborador;
            
            this.listPermiso.push(item);
          });

          this.filtrarIdPermiso(this.idPermisoForestal);
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    );
  }

  validarPermiso(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectPermiso == null || this.selectPermiso == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un permiso.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }
  
  redireccionar(idPermisoForestal: any) {
    this.cerrarModal();
    this.router.navigateByUrl('/planificacion/revision-permisos-forestales/' + idPermisoForestal);
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  agregar = () => {
    if (!this.validarPermiso()) return;
    this.ref.close(this.selectPermiso);
  };

  cerrarModal() {
    this.ref.close();
  }
}
