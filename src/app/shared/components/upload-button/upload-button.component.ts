import { Component, ElementRef, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'upload-button',
  templateUrl: './upload-button.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploadButtonComponent),
      multi: true
    }
  ]
})
export class UploadButtonComponent implements ControlValueAccessor {

  @Input() label: string = 'Adjuntar Archivo';
  @Input() icon: string = 'upload';
  @Input() showIcon: boolean = true
  @Input() accept: string = '';
  @Input() style: string = '';
  @Input() disabled: boolean = false;

  @ViewChild('fileInput') el!: ElementRef<HTMLInputElement>;

  constructor() { }

  onChange: any = () => { }
  onTouch: any = () => { }

  file: File | null | string = null;


  set value(file: File | null | string) {
    if (file !== undefined && this.file !== file) {
      this.file = file;
      this.onChange(file);
      this.onTouch(file);
    } else {
      this.file = null;
      this.onChange(file);
      this.onTouch(file);
    }
  }

  changeFile(e: Event) {
    const target = e.target as HTMLInputElement;
    const file = target.files?.item(0);
    this.onChange(file);
  }

  writeValue(value: File | FileList) {

    if (this.el?.nativeElement) {
      if (value instanceof FileList) {
        this.el.nativeElement.files = value;
      } else if (value instanceof File) {
        this.el.nativeElement.files = this.fileListItems([value]);
      } else if (value === null) {
        this.el.nativeElement.files = null;
        this.el.nativeElement.value = '';
      }
    }
  }

  fileListItems(files: File[]) {
    const b = new ClipboardEvent("").clipboardData || new DataTransfer();
    for (var i = 0, len = files.length; i < len; i++) b.items.add(files[i])
    return b.files
  }

  registerOnChange(fn: any) {
    this.onChange = fn
  }

  registerOnTouched(fn: any) {
    this.onTouch = fn
  }

}
