import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import Polygon from '@arcgis/core/geometry/Polygon';
import SpatialReference from '@arcgis/core/geometry/SpatialReference';
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { ConfirmationService } from 'primeng/api';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';
import { EsriGeometryType, OgcGeometryType, PGMFArchivoTipo } from '../../enums';
import { MapApi } from '../../mapApi';
import { UnitMetric } from '../../models/const';
import { ToastService } from '../../services/toast.service';
import { DownloadFile } from '../../util';

@Component({
  selector: 'app-map-custom-uf',
  templateUrl: './map-custom-uf.component.html',
  styleUrls: ['./map-custom-uf.component.scss']
})
export class MapCustomUFComponent implements OnInit {

  constructor(private dialog: MatDialog,
    private toast: ToastService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private serviceGeoforestal: ApiGeoforestalService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService) { }

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() isAnexo: boolean = false;
  @Input() printPDF: boolean = false;
  @Input() showBotonCargarGeneral: boolean = true;
  @Input() showBotonGuardar: boolean = true;
  @Input() identificarBosque: boolean = false;
  @Input() validaSuperposicionOtorgar: boolean = false;
  @Input() validaSuperposicionAprovechar: boolean = false;
  @Input() calcularArea: boolean = false;
  @Output() areaTotal = new EventEmitter<number>();
  @Output() listUF = new EventEmitter<any>();
  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  public view: any = null;
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  planManejoGeometria: PlanManejoGeometriaModel[] = [];

  ngOnInit(): void {
    if (this.printPDF === true) {
      this.initializeMapCustom();
    } else {
      this.initializeMap();
    }
    this.obtenerCapas();
  }
  initializeMapCustom() {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMapCustom(container);
    this.view = view;
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) =>  this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {
    this.registrarArchivoId.emit(result.data);
    let codigoTipo: any = PGMFArchivoTipo.PFMIC;
    let item2 = {
      codigoTipoPGMF: codigoTipo,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
    };
    return this.servicePostulacionPFDM.registrarArchivoDetalle(item2).pipe(
      concatMap((response: any) => {
        return this.guardarCapa(item, item2.idArchivo);
      })
    );
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      t3.attributes.forEach((feature:any) => {
        let geometryWKT: any = this.mapApi.getFeature(
          feature,
          this.view.spatialReference.wkid
        );
        let item: any = {
          idPlanManejoGeometria: 0,
          idPlanManejo: this.idPlanManejo,
          idArchivo: idArchivo,
          tipoGeometria: t3.tipo,
          codigoGeometria: feature.geometry.type,
          codigoSeccion: this.codigoProceso,
          codigoSubSeccion: this.codigoSubSeccion,
          geometry_wkt: geometryWKT,
          srid: 4326,
          nombreCapa: `${feature.properties.Simbolo} - ${feature.properties.Fisiogr}`,
          colorCapa: t3.color,
          idUsuarioRegistro: this.user.idUsuario,
        };
        this.planManejoGeometria.push(item);
      });
    });
    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  onChangeFile(e: any, tipo: string) {
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      tipo: tipo
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  onGuardarLayer() {
    let codigoTipo: any = this.codigoProceso;
    let fileUpload: any = [];
    let validateOverlap: any = false;
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: codigoTipo,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
        };
        fileUpload.push(item);
      }
    });
    this._layers.forEach((t: any) => {
      if (t.overlap === true) {
        validateOverlap = true;
      }
    });

    if (fileUpload.length === 0) {
      this.toast.error('Cargue un archivo para continuar.');
      return;
    }
    if (validateOverlap === true) {
      this.confirmationService.confirm({
        message: 'El Área está superpuesta ¿Está seguro de guardar?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.saveLayer(fileUpload);
        }
      });
    } else {
      this.saveLayer(fileUpload);
    }
  }
  saveLayer(fileUpload: any) {
    let observer = from(fileUpload);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result: any) => {
          this.toast.ok(result.message);
          this.cleanLayers();
          this.obtenerCapas();
        },
        (error) => {
          this.toast.error('Ocurrió un error.');
        }
      );
  }
  obtenerCapas() {
    let item = null;

    if (this.isAnexo === true) {
      item = {
        idPlanManejo: this.idPlanManejo,
      };
    } else {
      item = {
        idPlanManejo: this.idPlanManejo,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
      };
    }
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result: any) => {
        if (result.data.length) {
          let features:any = [];
          let groupId = this._id;
          let item = {
            title: 'Unidad Fisiográficas',
            color: result.data[0].colorCapa
          };
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              features.push( {
                type: "Feature",
                properties: {
                  title: t.nombreCapa,
                },
                geometry: jsonGeometry
              });
            }
          });
          

          let layer: any = {} as CustomCapaModel;
          layer.codigo = result.data[0].idArchivo;
          layer.idLayer = this.mapApi.Guid2.newGuid;
          if (this.isAnexo === true) {
            layer.inServer = false;
            layer.service = true;
          } else {
            layer.inServer = true;
            layer.service = false;
          }
          layer.nombre = item.title;
          layer.groupId = groupId;
          layer.color = item.color;
          layer.annex = false;
          this._layers.push(layer);
          let geoJson = this.getGeoJson(
            layer.idLayer,
            groupId,
            features,
            item
          );
          this.obtenerLista(geoJson);
          this.createLayer(geoJson);
        }
      },
      (error: any) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }
  getGeoJson(guid:any,groupId:any,features:any,item:any){
    let geoJson: any = {
      type: "FeatureCollection",
      features: features,
      opacity: 0.4,
      color: item.color,
      title: item.title,
      service: true,
      inServer: true,
      idLayer: guid,
      groupId: groupId,
      crs: {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      }
    }
    return geoJson;
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
      (error: HttpErrorResponse) => {
        this.toast.error(error.message);
        this.dialog.closeAll();
      };
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._filesSHP.length === 0) {
                  this.cleanLayers();
                  this.obtenerCapas();
                  this.listUF.emit([]);
                }
                this.toast.ok('El archivo se eliminó correctamente.');
              } else {
                this.toast.error('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error: any) => {
              this.dialog.closeAll();
              this.toast.error('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
        this.listUF.emit([]);
        this.obtenerCapas();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      //this.calculateArea(data[0]);
      this.obtenerLista(data[0]);
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.tipo = config.tipo;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer: any = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    this._filesSHP.push(file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
      content: [{
        type: "fields",
        fieldInfos: [{
          fieldName: "title",
          label: "Capa",
          format: {
            digitSeparator: false
          }
        }]
      }]
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.tipo = item.tipo;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  calculateArea(feature: any) {
    if ((feature.geometry.type == EsriGeometryType.POLYGON)) {
      let polygon: any = new Polygon();
      polygon = { spatialReference: { wkid: 4326 } };
      polygon.rings = feature.geometry.coordinates;
      return this.mapApi.calculateArea(polygon, UnitMetric.HA);
    }
    if ((feature.geometry.type == EsriGeometryType.MULTIPOLYGON)) {
      const features = feature?.geometry?.coordinates;
      let totalArea = 0;
      features.forEach((coordinates:any) => {
        let polygon: any = new Polygon();
        polygon = { spatialReference: { wkid: 4326 } };
        polygon.rings = coordinates;
        let area = this.mapApi.calculateArea(polygon, UnitMetric.HA);
        totalArea += area;
      });
      return totalArea;
    }

    return 0;
  }
  obtenerLista(data: any) {
    let listUF: any = [];
    data.features.forEach((t: any, index: any) => {
      listUF.push({
        vertice: index + 1,
        nombreUF:  t.properties.title || `${t.properties.Simbolo} - ${t.properties.Fisiogr}`,
        area: this.calculateArea(t),
        porcentaje: 0
      });
    });
    this.listUF.emit(listUF);
  }
}
