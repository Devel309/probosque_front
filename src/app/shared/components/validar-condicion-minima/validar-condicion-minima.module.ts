import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidarCondicionMinimaComponent } from './validar-condicion-minima.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FieldsetModule } from 'primeng/fieldset';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';



@NgModule({
  declarations: [
    ValidarCondicionMinimaComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    FieldsetModule,
    FormsModule,
    MatDatepickerModule,
  ],
  exports:[ValidarCondicionMinimaComponent],
  entryComponents:[ValidarCondicionMinimaComponent],
})
export class ValidarCondicionMinimaModule { }
