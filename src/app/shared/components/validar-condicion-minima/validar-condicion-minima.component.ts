import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '@services';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ValidarCondicionMinima } from 'src/app/model/ValidarCondicionMinima';
import { CondicionMinimaService } from 'src/app/service/bandeja-postulacion/condicion.minima.service';

@Component({
  selector: 'app-validar-condicion-minima',
  templateUrl: './validar-condicion-minima.component.html',
  styleUrls: ['./validar-condicion-minima.component.scss']
})
export class ValidarCondicionMinimaComponent implements OnInit {

  condiciones: any = {};
  //idCondicionMinima = 9;
  minimaDetalle: any[] = [];

  constructor(public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    private condicionMinimaService: CondicionMinimaService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService) {



  }

  ngOnInit(): void {

    if (this.config.data.idCondicionMinima) {
      this.obtenerCondicionMinima(this.config.data.idCondicionMinima);
    } else {
      this.validar();
    }
  }

  validar() {

/*     if (localStorage.getItem("usuario") != null) {

      let usuario: any = this.usuarioServ.usuario;

      let param: any = {};
      param.apellidoPaterno = usuario.apellidoPaterno;
      param.apellidoMaterno = usuario.apellidoMaterno;
      param.nombres = usuario.nombres;
      if(usuario.idtipoDocumento==3){
        param.tipoDocumento = "DNI";
      }else{
        param.tipoDocumento = "RUC";
      }
      param.nroDocumento = usuario.numeroDocumento;

      param.apellidoPaterno = "Maquera"
      param.apellidoMaterno = "Acero"
      param.nombres = "Rogel Joel"
      param.nroDocumento = "44822913";
      //param.ruc = usuario.numeroDocumento;

      this.validarRequisitos(param);
    } */
    if (this.config.data.obj!==null && this.config.data.obj!==undefined ) {
      let param: any = {};
      param.apellidoPaterno = this.config.data.obj.apellidoPaterno;
      param.apellidoMaterno = this.config.data.obj.apellidoMaterno;
      param.nombres = this.config.data.obj.nombre;
      /*if(this.config.data.obj.tipoDocumento===3){
        param.tipoDocumento = "DNI";
      }else{
        param.tipoDocumento = "RUC";
      }*/
      param.tipoDocumento = this.config.data.obj.tipoDocumento;
      param.nroDocumento = this.config.data.obj.numeroDocumento;
      this.validarRequisitos(param);
    }


  }


  private validarRequisitos(param: any) {

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.condicionMinimaService
      .validarRequisitos(param)
      .subscribe(
        (result: any) => {
          this.dialog.closeAll();
          if (result.success) {
            if(param.nombres === undefined){
              param.nombres = '';
            }
            if(param.apellidoPaterno === undefined){
              param.apellidoPaterno = '';
            }
            if(param.apellidoMaterno === undefined){
              param.apellidoMaterno = '';
            }
            this.condiciones.tipoDocumento = param.tipoDocumento;
            this.condiciones.numeroDocumento = param.nroDocumento;
            this.condiciones.descripcion = param.nombres + " " + param.apellidoPaterno + " " + param.apellidoMaterno
            this.condiciones.resultadoEvaluacion = result.data.resultadoGeneral;
            this.condiciones.fechaEvaluacion = result.data.fechaEvaluacion;

            this.condiciones.resultadoAntecedentePenal = result.data.resultadoAntecedentePenal;
            this.condiciones.resultadoAntecedenteJudicial = result.data.resultadoAntecedenteJudicial;
            this.condiciones.resultadoAntecedentePolicial = result.data.resultadoAntecedentePolicial;
            this.condiciones.resultadoSancionVigenteOsce = result.data.resultadoSancionVigenteOsce;
            this.condiciones.resultadoInfractor = result.data.resultadoInfractor;

          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      );
  }

  private obtenerCondicionMinima(param: number) {

    this.dialog.open(LoadingComponent, { disableClose: true });

    this.condicionMinimaService
      .obtenerCondicionMinima(param)
      .subscribe(
        (result: any) => {
          
          this.dialog.closeAll();
          if (result.success) {

            this.condiciones.tipoDocumento = result.data.tipoDocumento;
            this.condiciones.numeroDocumento = result.data.nroDocumento;
            this.condiciones.descripcion = result.data.descripcion;
            this.condiciones.resultadoEvaluacion = result.data.resultadoEvaluacion;
            this.condiciones.fechaEvaluacion = result.data.fechaEvaluacion;

            result.data.listaCondicionMinimaDetalle.forEach((element: any) => {




              if (element.codigoTipoCondicionMinimaDet === 'NOFRNI') {
                this.condiciones.resultadoInfractor = element.resultado;
              }else if (element.codigoTipoCondicionMinimaDet === 'NOCOAP') {
                this.condiciones.resultadoAntecedentePenal = element.resultado;
              }else if (element.codigoTipoCondicionMinimaDet === 'NOCOAJ') {
                this.condiciones.resultadoAntecedenteJudicial = element.resultado;
              }else if (element.codigoTipoCondicionMinimaDet === 'NAPPNP') {
                this.condiciones.resultadoAntecedentePolicial = element.resultado;
              }else if (element.codigoTipoCondicionMinimaDet === 'NOCPAU') {

              }else if (element.codigoTipoCondicionMinimaDet === 'NOIMES') {
                this.condiciones.resultadoSancionVigenteOsce = element.resultado;

              }
            });


          }
        },
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      );
  }

  guardarValidacion() {
    // if (!this.condiciones.fechaEvaluacion) {
    //   this.ErrorMensaje("Seleccione Fecha Evaluación")
    //   return;
    // }

    let strFechaHora = this.condiciones.fechaEvaluacion.split(' ');
    let fecha = strFechaHora[0].split("/");
    let hora = strFechaHora[1].split(":");

    //let fechaFinal=new Date(fecha[2], (fecha[1] - 1), fecha[0], hora[0], hora[1], hora[2]).toUTCString();
    let fechaFinal=fecha[2]+"-"+fecha[1]+"-"+fecha[0]+"T"+hora[0]+":"+hora[1]+":"+hora[2];

    this.crearListadoDetalle();

    let params: ValidarCondicionMinima = {
      tipoDocumento: this.condiciones.tipoDocumento,
      resultadoEvaluacion: this.condiciones.resultadoEvaluacion,
      nroDocumento: this.condiciones.numeroDocumento,
      idUsuarioRegistro: this.usuarioServ.idUsuario,

      fechaEvaluacion: fechaFinal,
      descripcion: this.condiciones.descripcion,
      condicionMinimaDetalle: this.minimaDetalle
    }

    this.condicionMinimaService.registrarCondicionMinima(params).subscribe((result: any) => {
      if (result.success) {
        this.SuccessMensaje(result.message);
        this.ref.close();
      } else {
        this.ErrorMensaje(result.message);
      }
    })

  }

  crearListadoDetalle() {

    this.minimaDetalle = [
      {
        codigoTipoCondicionMinimaDet: "NOFRNI",
        fechaIngreso: "",
        fechaVigencia: "",
        idUsuarioRegistro: this.usuarioServ.usuario.idusuario,
        resultado: this.condiciones.resultadoInfractor
      },

      {
        codigoTipoCondicionMinimaDet: "NOCOAP",
        fechaIngreso: "",
        fechaVigencia: "",
        idUsuarioRegistro: this.usuarioServ.usuario.idusuario,
        resultado: this.condiciones.resultadoAntecedentePenal
      },

      {
        codigoTipoCondicionMinimaDet: "NOCOAJ",
        fechaIngreso: "",
        fechaVigencia: "",
        idUsuarioRegistro: this.usuarioServ.usuario.idusuario,
        resultado: this.condiciones.resultadoAntecedenteJudicial
      },

      {
        codigoTipoCondicionMinimaDet: "NAPPNP",
        fechaIngreso: "",
        fechaVigencia: "",
        idUsuarioRegistro: this.usuarioServ.usuario.idusuario,
        resultado: this.condiciones.resultadoAntecedentePolicial
      },

      {
        codigoTipoCondicionMinimaDet: "NOCPAU",
        fechaIngreso: "",
        fechaVigencia: "",
        idUsuarioRegistro: this.usuarioServ.usuario.idusuario,
        resultado: "CONFORME"
      },

      {
        codigoTipoCondicionMinimaDet: "NOIMES",
        fechaIngreso: "",
        fechaVigencia: "",
        idUsuarioRegistro: this.usuarioServ.usuario.idusuario,
        resultado: this.condiciones.resultadoSancionVigenteOsce
      },

    ]

  }


  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'warn',
      key: 'tl',
      summary: '',
      detail: mensaje,
    });
  }

  cerrar() {
    this.ref.close();
  }

}
