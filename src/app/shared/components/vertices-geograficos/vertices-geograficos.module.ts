import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerticesGeograficosComponent } from './vertices-geograficos.component';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [
    VerticesGeograficosComponent
  ],
  imports: [
    CommonModule,
    TableModule
  ],
  exports:[VerticesGeograficosComponent]
})
export class VerticesGeograficosModule { }
