import { Component, Input, OnInit } from "@angular/core";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";
import { InformacionAreaPmfiService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-area.service";
import { MapApi } from "../../mapApi";

@Component({
  selector: "app-vertices-geograficos",
  templateUrl: "./vertices-geograficos.component.html",
  styleUrls: ["./vertices-geograficos.component.scss"],
})
export class VerticesGeograficosComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  @Input() disabled!: boolean;
  @Input() listCoordinatesAnexo: any[] = [];
  @Input() listAnexoArea: any[] = [];
  @Input() totalAnexoArea!: number;
  constructor(
  ) {}

  ngOnInit(): void {
  }


}
