import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluacionArchivoComponent } from './evaluacion-archivo.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { UploadInputButtonsModule} from '../upload-input-buttons/upload-input-buttons.module';
import { InputButtonsModule } from '../input-button/input-buttons.module';
import { InputButtonsCodigoModule } from '../input-button-codigo/input-buttons.module';


@NgModule({
  declarations: [EvaluacionArchivoComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    UploadInputButtonsModule,
    InputButtonsModule,
    InputButtonsCodigoModule
  ],
  exports:[EvaluacionArchivoComponent]
})
export class EvaluacionArchivoModule { }
