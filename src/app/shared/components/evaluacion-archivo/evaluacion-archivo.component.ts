import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import {CodigoEstadoEvaluacion} from '../../../model/util/CodigoEstadoEvaluacion';
import {EvaluacionArchivoModel} from '../../../model/Comun/EvaluacionArchivoModel';

@Component({
  selector: 'evaluacion-archivo-shared',
  templateUrl: './evaluacion-archivo.component.html',
  styleUrls: ['./evaluacion-archivo.component.scss']
})
export class EvaluacionArchivoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() cadigoTab!: string;
  codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;


  @ContentChild('conatinertop') conatinertopRef!: TemplateRef<any>;
  @ContentChild('conatinerbottom') conatinerbottomRef!: TemplateRef<any>;



  @Input('base') base: EvaluacionArchivoModel = new EvaluacionArchivoModel();

  constructor() { }

  ngOnInit(): void {
    //this.base.archivo = this.archivoResolucionDesaprueba
  }

  cargarIdArchivo(idArchivo: any) {
    this.base.idArchivo = idArchivo;
  }

}
