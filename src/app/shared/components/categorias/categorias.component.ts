import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CoreCentralService } from "@services";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EspeciesFauna } from "src/app/model/medioTrasporte";

@Component({
  selector: "categoria",
  templateUrl: "./categorias.component.html",
  styleUrls: ["./categorias.component.scss"],
})
export class CategoriasComponent implements OnInit {
  categorias: any[] = [];

  constructor(
    private coreCentralService: CoreCentralService,
    private dialog: MatDialog,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig
  ) {}

  ngOnInit(): void {
    this.categorias = this.config.data.data;
  }
  cerrarModal() {
    this.ref.close();
  }
}
