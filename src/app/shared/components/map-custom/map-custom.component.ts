import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {
  ArchivoService,
  PlanificacionService,
  UsuarioService,
} from '@services';
import { DownloadFile, MapApi, PGMFArchivoTipo, ToastService } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { CodigosPGMF } from 'src/app/model/util/PGMF/CodigosPGMF';
import { CodigoPMFIC } from 'src/app/model/util/PMFIC/CodigoPMFIC';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';

@Component({
  selector: 'app-map-custom',
  templateUrl: './map-custom.component.html',
  styleUrls: ['./map-custom.component.scss'],
})
export class MapCustomComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private user: UsuarioService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private serviceGeoforestal: ApiGeoforestalService,
    private planificacionService: PlanificacionService,
    private toast: ToastService,
    private serviceExternos: ApiForestalService
  ) { }

  @ViewChild('map2', { static: true }) private mapViewEl!: ElementRef;
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() isShow?: boolean = true;
  @Input() identificarBosque: boolean = false;
  @Input() validaSuperposicionOtorgar: boolean = false;
  @Input() validaSuperposicionAprovechar: boolean = false;
  @Input() isAnexo: boolean = false;
  @Input() printPDF: boolean = false;
  @Input() isListContrato: boolean = false;
  @Input() opcionCheckParcela: boolean = false;
  @Input() valueCheckParcela!: string;
  @Output() areaTotal = new EventEmitter<number>();
  @Output() listTipoBosque = new EventEmitter<any>();
  @Output() listTipoBosqueParcelaOpcion1 = new EventEmitter<any>();
  @Output() listTipoBosqueParcelaOpcion2 = new EventEmitter<any>();
  @Output() deleteRegisterTable = new EventEmitter();
  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  @Input() ShowBotonCargar: boolean = true;
  @Input() ShowBotonGuardar: boolean = true;
  @Input() mostrarBotonCapturarMapa: boolean = false;
  @Input() listBloqueQuinquenal!: [];
  @Input() listParcelaOpcion1!: [];
  @Input() listParcelaOpcion2!: [];
  @Input() idOrdenamientoProteccion!: number;
  @Input() disabled: boolean = false;
  public view: any = null;
  public geoJsonLayer: any = null;
  _id = this.mapApi.Guid2.newGuid;
  file: FileModel = {} as FileModel;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  listQuinquenal: any = [];
  layerInternal: any = [];
  geoJsonInternal: any = [];
  newLayerInternal: any = [];
  newGeojsonInternal: any = [];
  CodigosPGMF = CodigosPGMF;

  ngOnInit(): void {
    if (this.printPDF === true) {
      this.initializeMapCustom();
    } else {
      this.initializeMap();
    }
    this.obtenerCapas();
    if (this.isListContrato === true) {
      this.listarContratos();
    }
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  initializeMapCustom() {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMapCustom(container);
    this.view = view;
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {
    this.registrarArchivoId.emit(result.data);

    let codigoTipo: any = PGMFArchivoTipo.PGMFA;
    if (this.codigoSubSeccion == 'POCCIBPCTIBO') {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion === CodigosPGMF.PGMF_TAB_5_1 || this.codigoSubSeccion === CodigosPGMF.PGMF_TAB_5_2) {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion === CodigoPMFIC.TAB_1_1 || this.codigoSubSeccion === CodigoPMFIC.TAB_1_2 || this.codigoSubSeccion === CodigoPMFIC.TAB_1_4) {
      codigoTipo = this.codigoSubSeccion;
    }
    let item2 = {
      codigoTipoPGMF: codigoTipo,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      descripcion: item.descripcion
    };
    return this.servicePostulacionPFDM.registrarArchivoDetalle(item2).pipe(
      concatMap((response: any) => {
        if (response.success) {
          this.SuccessMensaje(response.message);
        }
        return this.guardarCapa(item, item2.idArchivo);
      })
    );
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: this.tipoGeometria,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  obtenerCapas() {
    let item = null;

    if (this.isAnexo === true) {
      item = {
        idPlanManejo: this.idPlanManejo,
      };
    } else {
      item = {
        idPlanManejo: this.idPlanManejo,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
      };
    }
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              let layer: any = {} as CustomCapaModel;
              layer.codigo = t.idArchivo;
              layer.idLayer = this.mapApi.Guid2.newGuid;
              if (this.isAnexo === true) {
                layer.inServer = false;
                layer.service = true;
              } else {
                layer.inServer = true;
                layer.service = false;
              }
              layer.nombre = t.nombreCapa;
              layer.groupId = groupId;
              layer.color = t.colorCapa;
              layer.annex = false;
              layer.descripcion = t.descripcion;
              if (this.isAnexo === true) {
                if (t.codigoSeccion !== 'POCCANEX') {
                  layer.service = true;
                }
              }

              this._layers.push(layer);
              let geoJson = this.mapApi.getGeoJson(
                layer.idLayer,
                groupId,
                item
              );
              this.createLayer(geoJson);
              if (this.identificarBosque === true) {
                //if (geoJson.coordinates !== null)
                //this.consultarTiposBosque(geoJson.features[0].geometry.coordinates[0]);
              }
            }
          });
        }
      },
      (error) => {
        this.ErrorMensaje('Ocurrió un error');
      }
    );
  }
  consultarTiposBosque(geometry: any) {
    let params = {
      idClasificacion: '',
      geometria: {
        poligono: geometry,
      },
    };
    this.serviceExternos
      .identificarTipoBosque(params)
      .subscribe((result: any) => {
        if (result.dataService.data.capas.length > 0) {
          let listaBosque: any = [];
          result.dataService.data.capas.forEach((t: any) => {
            let itemLayer = this._layers.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof itemLayer === 'object') return;
            if (t.geoJson !== null) {
              t.geoJson.crs.type = 'name';
              t.geoJson.opacity = 0.4;
              t.geoJson.color = this.mapApi.random();
              t.geoJson.title = t.nombreCapa;
              t.geoJson.service = true;
              t.geoJson.groupId = this._id;
              t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
              let layer: any = {} as CustomCapaModel;
              layer.codigo = null;
              layer.idLayer = t.geoJson.idLayer;
              layer.inServer = false;
              layer.service = true;
              layer.nombre = t.nombreCapa;
              layer.groupId = t.geoJson.groupId;
              layer.color = t.geoJson.color;
              layer.annex = false;
              this._layers.push(layer);
              this.createLayer(t.geoJson);
              let coordinates: any = [];
              t.geoJson.features.forEach((t: any) => {
                t.geometry.coordinates.forEach((t2: any) => {
                  if (t2.length === 1) {
                    coordinates.push(t2[0]);
                  } else {
                    coordinates.push(t2)
                  }
                });
              });
              listaBosque.push({
                idBosque: t.codigoCapa,
                descripcion: t.nombreCapa,
                areaHa: this.mapApi.calculateIntersection(
                  geometry,
                  coordinates
                ), //this.calculateArea(t.geoJson),
                areaHaPorcentaje: '',
              });
            }
          });

          this.listTipoBosque.emit(listaBosque);
        }
      });
  }

  consultarTiposBosquePorPoligono(items: any) {
    this.listQuinquenal = [];
    let observer = from(items);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.servicioIdentificarTipoBosque(item)))
      .pipe(
        finalize(() => {
          this.dialog.closeAll();
          this.newLayerInternal.forEach((t: any) => {
            this._layers.push(t);
          });
          this.newGeojsonInternal.forEach((t: any) => {
            this.createLayer(t);
          });
          let newItems = this.listQuinquenal.sort((a: any, b: any) => {
            if (a.label < b.label) {
              return -1;
            }
            if (a.label > b.label) {
              return 1;
            }
            return 0;
          });
          if (this.valueCheckParcela === 'OPCION1') {
            this.listTipoBosqueParcelaOpcion1.emit(newItems);
          } else if (this.valueCheckParcela === 'OPCION2') {
            this.listTipoBosqueParcelaOpcion2.emit(newItems);
          } else {
            this.listTipoBosque.emit(newItems);
          }
        })
      )
      .subscribe();
  }
  servicioSuperposicionPlanificacion(geometry: any) {
    let params = {
      codProceso: '110102',
      geometria: {
        poligono: geometry,
      },
    };
    this.serviceExternos
      .validarSuperposicionPlanificacion(params)
      .subscribe((result: any) => {
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            let itemLayer = this._layers.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof itemLayer === 'object') return;
            if (t.seSuperpone === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.included = false;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.overlap = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layers.push(layer);
                this.createLayer(t.geoJson);
              }
            }
          });
          let message = "Se encontraron capas superpuestas";
          if(this.valueCheckParcela === "OPCION1"){
              message = "Se encontraron capas superpuestas para la opción 1"
          }else if (this.valueCheckParcela === "OPCION2"){
            message = "Se encontraron capas superpuestas para la opción 2 "
          }
          this.toast.ok(message);
        }else {
          this.toast.warn("No se encontraton capas superpuestas");
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            let itemLayer = this._layers.find(
              (t2: any) => t2.nombre === t.nombreCapa
            );
            if (typeof itemLayer === 'object') return;
            if (t.contenida === 'Si') {
              if (t.geoJson !== null) {
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = t.nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.nombre = t.nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                this._layers.push(layer);

                this.createLayer(t.geoJson);
              }
            }
          });
          let message = "Se encontraron capas contenidas";
          if(this.valueCheckParcela === "OPCION1"){
              message = "Se encontraron capas contenidas para la opción 1"
          }else if (this.valueCheckParcela === "OPCION2"){
            message = "Se encontraron capas contenidas para la opción 2 "
          }
          this.toast.ok(message);
        }
      });
  }
  servicioIdentificarTipoBosque(item: any) {
    let params = {
      idClasificacion: '',
      geometria: {
        poligono: item.geometry.coordinates,
      },
    };
    return this.serviceExternos
      .identificarTipoBosque(params)
      .pipe(concatMap(async (item2) => this.listarQuinquenal(item2, item)));
  }
  listarQuinquenal(result: any, item: any) {
    let listBosquesService: any = {};
    if (this.valueCheckParcela === 'OPCION1') {
      listBosquesService = {
        parcela: item.properties.ET_ID_1,
        label: item.properties.BQ,
        tipoBosque: [],
      };
    } else {
      listBosquesService = {
        label: item.properties.Name_1 || item.properties.Name,
        tipoBosque: [],
      };
    }

    let tiposBosques: any = [];
    if (result.dataService.data.capas.length > 0) {
      result.dataService.data.capas.forEach((t: any) => {
        let itemLayer = this._layers.find(
          (t2: any) => t2.nombre === t.nombreCapa
        );
        if (typeof itemLayer === 'object') return;
        if (t.geoJson !== null) {
          t.geoJson.crs.type = 'name';
          t.geoJson.opacity = 0.4;
          t.geoJson.color = this.mapApi.random();
          t.geoJson.title = t.nombreCapa;
          t.geoJson.service = true;
          t.geoJson.groupId = this._id;
          t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
          let layer: any = {} as CustomCapaModel;
          layer.codigo = null;
          layer.idLayer = t.geoJson.idLayer;
          layer.inServer = false;
          layer.service = true;
          layer.nombre = t.nombreCapa;
          layer.groupId = t.geoJson.groupId;
          layer.color = t.geoJson.color;
          layer.annex = false;
          this.layerInternal.push(layer);
          //this._layers.push(this._layer);
          this.geoJsonInternal.push(t.geoJson);
          //this.createLayer(t.geoJson);
          let coordinates: any = [];
          t.geoJson.features.forEach((t: any) => {
            t.geometry.coordinates.forEach((t2: any) => {
              coordinates.push(t2);
            });
          });
          tiposBosques.push({
            idBosque: t.codigoCapa,
            descripcion: t.nombreCapa,
            areaHA: this.mapApi.calculateIntersection(
              item.geometry.coordinates,
              coordinates
            ), //this.calculateArea(t.geoJson),
            areaHAPorcentaje: '',
          });
        }
      });
      listBosquesService.tipoBosque = tiposBosques;
    }
    if (listBosquesService.tipoBosque.length) {

      this.listQuinquenal.push(listBosquesService);
      this.newLayerInternal = this.removeDuplicates(this.layerInternal, 'nombre');
      this.newGeojsonInternal = this.removeDuplicates(
        this.geoJsonInternal,
        'title'
      );
    }
  }
  removeDuplicates(items: any, nodo: string) {
    let newArray = [];
    let uniqueObject: any = {};
    for (let i in items) {
      let objTitle: any = items[i][nodo];
      uniqueObject[objTitle] = items[i];
    }
    for (let i in uniqueObject) {
      newArray.push(uniqueObject[i]);
    }

    return newArray;
  }
  SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: 'success',
      summary: '',
      detail: mensaje,
    });
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
  onChangeFile(e: any) {
    if (this.codigoSubSeccion === "PGMFCFFMOPUMFDABBQ") {
      if (this.listBloqueQuinquenal.length >= 1) {
        this.toast.warn("Ya se encuentra datos registrados.");
        return;
      }
    }
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      opcion: e.target.dataset.zone || null
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  getBloqueQuinquenal() {

  }
  onGuardarLayer() {
    let codigoTipo: any = PGMFArchivoTipo.PGMFA;
    if (this.codigoSubSeccion == 'POCCIBPCTIBO') {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion === CodigosPGMF.PGMF_TAB_5_1 || this.codigoSubSeccion === CodigosPGMF.PGMF_TAB_5_2) {
      codigoTipo = this.codigoSubSeccion;
    } else if (this.codigoSubSeccion === CodigoPMFIC.TAB_1_1 || this.codigoSubSeccion === CodigoPMFIC.TAB_1_2 || this.codigoSubSeccion === CodigoPMFIC.TAB_1_4) {
      codigoTipo = this.codigoSubSeccion;
    }
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: codigoTipo,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
          descripcion: t.descripcion
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.ErrorMensaje('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe(
        (result) => {
          this.SuccessMensaje(result.message);
          this.cleanLayers();
          this.obtenerCapas();
          if (this.isListContrato === true) {
            this.listarContratos();
          }
        },
        (error) => {
          this.ErrorMensaje('Ocurrió un error.');
        }
      );
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
    }, (error: HttpErrorResponse) => {
      this.ErrorMensaje(error.message);
      this.dialog.closeAll();
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                if (this.codigoSubSeccion === 'PGMFCFFMOPUMFDABBQ') {
                  this.deleteRegisterTable.emit('click');
                } else if (this.codigoSubSeccion === 'PGMFCFFMOPUMFDABPC') {
                  if (layer.descripcion === 'OPCION1') {
                    this.deleteRegisterTable.emit('OPCION1');
                  } else if (layer.descripcion === 'OPCION2') {
                    this.deleteRegisterTable.emit('OPCION2');
                  }
                }
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._layers.length === 0) {
                  this.cleanLayers();
                }
                this.SuccessMensaje('El archivo se eliminó correctamente.');
              } else {
                this.ErrorMensaje('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.ErrorMensaje('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      if (this.codigoSubSeccion === 'PGMFCFFMOPUMFDABBQ') {
        this.listTipoBosque.emit([]);
      } else if (this.codigoSubSeccion === 'PGMFCFFMOPUMFDABPC') {
        if (layer.descripcion === 'OPCION1') {
          this.listTipoBosqueParcelaOpcion1.emit([]);
        } else if (layer.descripcion === 'OPCION2') {
          this.listTipoBosqueParcelaOpcion2.emit([]);
        }
      }
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._layers.length === 0) {
        this.cleanLayers();
      }
    }
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (this.codigoSubSeccion === 'POCCIBPCACCE') {
        this.calculateArea(data[0]);
      } else if (this.codigoSubSeccion === CodigosPGMF.PGMF_TAB_5_1 || this.codigoSubSeccion === CodigosPGMF.PGMF_TAB_5_2) {
        this.calculateArea(data[0]);
      } else if (this.codigoSubSeccion === CodigoPMFIC.TAB_1_1 || this.codigoSubSeccion === CodigoPMFIC.TAB_1_2 || this.codigoSubSeccion === CodigoPMFIC.TAB_1_4) {
        this.calculateArea(data[0]);
      }
      if (
        this.identificarBosque === true &&
        this.validaSuperposicionOtorgar === true
      ) {
        let coordinates: any = [];
        data[0].features.forEach((t: any) => {
          coordinates.push(t.geometry.coordinates[0]);
        });
        this.consultarTiposBosque(coordinates);
      }
      if (
        this.identificarBosque === true &&
        this.validaSuperposicionAprovechar === true
      ) {
        this.consultarTiposBosquePorPoligono(data[0].features);
        let coordinates: any = [];
        data[0].features.forEach((t: any) => {
          coordinates.push(t.geometry.coordinates[0]);
        });
        this.servicioSuperposicionPlanificacion(coordinates);
      }
      if (
        this.validaSuperposicionAprovechar === true &&
        this.identificarBosque === false
      ) {
        let coordinates: any = [];
        data[0].features.forEach((t: any) => {
          coordinates.push(t.geometry.coordinates[0]);
        });
        this.servicioSuperposicionPlanificacion(coordinates);
      }
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer: any = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.opcion;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.opcion;
    this._filesSHP.push(file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  calculateArea(data: any) {
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    let areaTotal = Math.abs(sumArea.toFixed(3));
    this.areaTotal.emit(areaTotal);
    return areaTotal;
  }

  listarContratos() {
    let params = {
      idPlanManejoContrato: null,
      idPlanManejo: this.idPlanManejo,
      idContrato: null,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planificacionService.obtenerPlanTrabajoContrato(params).subscribe(
      (result: any) => {
        this.dialog.closeAll();

        if (result.success) {
          let contratosSeleccionados = [...result?.data];
          let idsContratos: string = '';
          contratosSeleccionados.forEach((x: any) => {
            if (idsContratos.length > 0)
              idsContratos = idsContratos.concat(',');
            idsContratos = idsContratos.concat(x.idContrato.toString());
          });

          this.obtenerContratosPoligonos(idsContratos);
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }
  obtenerContratosPoligonos(idsContratos: string) {
    this.planificacionService.obtenerContratoPoligono(idsContratos).subscribe(
      (result: any) => {
        this.dialog.closeAll();

        if (result.success) {
          //this.mapApi.cleanLayers(this._id, this.view);
          let coordinates: any = [];
          result.data.forEach((t: any) => {
            if (t.geometry !== null) {
              let geoJsonGeometry: any = this.mapApi.wktParse(t.geometry);

              let item = {
                color: this.mapApi.random(),
                title: 'UA' + ' ' + t.idUnidadAprovechamiento,
                jsonGeometry: geoJsonGeometry,
                properties: {
                  title: 'UA' + ' ' + t.idUnidadAprovechamiento,
                },
              };
              let idLayer = this.mapApi.Guid2.newGuid;
              let groupId = this._id;
              let layer = {} as CustomCapaModel;

              let geoJson = this.mapApi.getGeoJson(idLayer, groupId, item);
              geoJson.features.forEach((t: any) => {
                coordinates.push(t.geometry.coordinates[0]);
              });
              layer.codigo = 0;
              layer.idLayer = idLayer;
              layer.inServer = false;
              layer.service = true;
              layer.nombre = item.title;
              layer.groupId = groupId;
              layer.color = item.color;
              layer.idGroupLayer = 0;
              this._layers.push(layer);
              this.mapApi.createLayer(geoJson, this.view);
            }
          });
          if (this.identificarBosque === true) {
            this.consultarTiposBosque(coordinates);
          }
        }
      },
      (error: HttpErrorResponse) => {
        this.dialog.closeAll();
        this.ErrorMensaje(error.message);
      }
    );
  }
  capturarMapa() {
    this.view.takeScreenshot().then((data: any) => {
      fetch(data.dataUrl)
        .then((data) => data.blob())
        .then((data) => {
          const file = new File([data], 'Anexo Mapas.png', {
            type: 'image/png',
          });
          const formData = new FormData();
          formData.append('file', file);
          this.serviceArchivo.cargar(this.user.idUsuario, '19', file)
            .pipe(concatMap((result: any) => {
              let item2: any = {
                codigoTipoPGMF: 'Anexo',
                idArchivo: result.data,
                idPlanManejo: this.idPlanManejo,
                idUsuarioRegistro: this.user.idUsuario,
              }
              return this.servicePostulacionPFDM.registrarArchivoDetalle(item2)
            }))
            .pipe(finalize(() => this.dialog.closeAll()))
            .subscribe((data) => {
            });

        });
    });

  }
}
