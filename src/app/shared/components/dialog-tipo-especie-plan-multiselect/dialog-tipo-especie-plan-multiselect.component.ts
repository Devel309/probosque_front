import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import {
  CoreCentralService,
  ParametroValorService,
  UsuarioService,
} from "@services";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { EspeciesFauna } from "src/app/model/medioTrasporte";
import { Perfiles } from "src/app/model/util/Perfiles";
import { ManejoBosqueService } from "src/app/service/manejoBosque.service";

@Component({
  selector: "app-dialog-tipo-especie-plan-multiselect",
  templateUrl: "./dialog-tipo-especie-plan-multiselect.component.html",
  styleUrls: ["./dialog-tipo-especie-plan-multiselect.component.scss"],
})
export class DialogTipoEspeciePlanMultiselectComponent implements OnInit {
  queryFauna: string = "";
  comboListEspeciesFauna: EspeciesFauna[] = [];
  selectedValues: any[] = [];
  totalRecords: number = 0;
  objbuscar = {
    dato: "",
    pageNum: 1,
    pageSize: 10,
  };
  idPlanManejo: number = 0;
  tipoPlan: string = "";
  isPerfilARFFS: boolean = false;
  perfiles = Perfiles;

  constructor(
    private coreCentralService: CoreCentralService,
    private dialog: MatDialog,
    public ref: DynamicDialogRef,
    private messageService: MessageService,
    public config: DynamicDialogConfig,
    private manejoService: ManejoBosqueService,
    private userService: UsuarioService
  ) {}

  ngOnInit(): void {
    this.idPlanManejo = this.config.data.idPlanManejo;
    this.tipoPlan = this.config.data.tipoPlan;

    // this.listaPorFiltroEspecieFauna();
    this.listarActividadesDeAprovechamiento();
    if (this.userService.usuario.sirperfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilARFFS = true;
    }

    let idUsuarioA = this.userService.usuario.idusuario;
    if (idUsuarioA == 496) {
      this.isPerfilARFFS = false;
    }
  }

  listarActividadesDeAprovechamiento() {
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.tipoPlan,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.manejoService
      .actividadesDeAprovechamiento(params)
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          this.dialog.closeAll();
          this.totalRecords =
            response.data.censoComercialDto.listaEspecieDtos.length;
          this.comboListEspeciesFauna = [
            ...response.data.censoComercialDto.listaEspecieDtos,
          ];
        }
      });
  }

  loadData(e: any) {
    this.selectedValues = [];
    const pageSize = Number(e.rows);
    this.objbuscar.pageNum = Number(e.first) / pageSize + 1;
    this.objbuscar.pageSize = pageSize;
    this.listaPorFiltroEspecieFauna();
  }

  filtrarFauna() {
    this.listaPorFiltroEspecieFauna();
  }

  listaPorFiltroEspecieFauna() {
    let params = {
      idEspecie: null,
      nombreComun: null,
      nombreCientifico: this.queryFauna ? this.queryFauna : null,
      autor: null,
      familia: null,
      pageNum: this.objbuscar.pageNum,
      pageSize: this.objbuscar.pageSize,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.coreCentralService
      .ListaPorFiltroEspeciesForestales(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.totalRecords = result.totalrecord;
        this.comboListEspeciesFauna = [...result.data];
      });
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.queryFauna == null || this.queryFauna == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Una Especie.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  agregar() {
    /* if (!this.validarMedidas()) {
      return;
    } */
    this.ref.close(this.selectedValues);
  }
  cerrarModal() {
    this.selectedValues = [];
    this.ref.close();
  }
}
