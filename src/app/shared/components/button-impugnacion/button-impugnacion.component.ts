import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoadingComponent } from '../../../components/loading/loading.component';
import { finalize } from 'rxjs/operators';
import { ArchivoService, UsuarioService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { AmpliacionSolicitudService } from '../../../service/ampliacion-solicitud/ampliacion-solicitud.service';
import { ImpugnacionSan } from '../../../model/Impugnacion/ImpugnacionSan';
import { DownloadFile, ToastService } from '@shared';
import { ImpugnacionSanService } from '../../../service/planificacion/permiso-forestal/impugnacionSan.service';
import { CodigosTiposSolicitud } from '../../../model/util/CodigosTiposSolicitud';
import { CodigoProceso } from '../../../model/util/CodigoProceso';
import { CodigoPermisoForestal } from '../../../model/util/CodigoPermisoForestal';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'button-impugnacion-shared',
  templateUrl: './button-impugnacion.component.html',
  styleUrls: ['./button-impugnacion.component.scss'],
})
export class ButtonImpugnacionComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Input() disabled!: boolean;
  @Input() estado!: string;

  @Output() registroImpugnacionEmiter: EventEmitter<number> =
    new EventEmitter();

  pdfSrc: string = '';
  isShowModal2_2: boolean = false;

  idTipoSolicitud: number = 0;
  idProcesoPostulacion: number = 0;
  autoResize: boolean = true;
  idArchivo: number = 0;
  showFormatoSan: boolean = false;

  isLoadFile: boolean = false;
  CodigosTiposSolicitud = CodigosTiposSolicitud;

  codigoProceso: string = CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoArchivoSan: string = 'PFCRIMP';

  impugnacionSan: ImpugnacionSan = new ImpugnacionSan({
    idSolicitudSAN: 0,
    nroGestion: this.idPermisoForestal,
    //idPermisoForestal: this.idPermisoForestal,
    codSolicitudSAN: this.codigoProceso, //codigoProceso
    subCodSolicitudSAN: CodigosTiposSolicitud.IMPUGNACION_SAN,
    tipoSolicitud: 'IMPUGNACIÓN AL SAN',
    tipoPostulacion : "PERMISO",
    idUsuarioRegistro: this.usuariServ.idUsuario,
  });

  tipoSolicitud: any[] = [];

  constructor(
    private archivoService: ArchivoService,
    private dialog: MatDialog,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuariServ: UsuarioService,
    private impugnacionSanService: ImpugnacionSanService,
    private toast: ToastService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  cargarIdArchivo(idArchivo: any) {
    if (idArchivo && idArchivo > 0) {
      this.idArchivo = idArchivo;
      this.isLoadFile = true;
    }
  }

  eliminarArchivo(idArchivo: any) {
    this.isLoadFile = false;
  }

  listarTipoSolicitud() {
    this.ampliacionSolicitudService
      .listarTipoSolicitud()
      .subscribe((resp: any) => {
        this.tipoSolicitud = resp.data;
      });
  }

  /*descargarImpSAN = () => {
    DownloadFile(
      this.documentoImpSAN,
      this.nombredocumentoImpSAN,
      'application/octet-stream'
    );
  };*/

  verPDF() {
    if (this.estado == CodigoPermisoForestal.ESTADO_PRESENTADO) {
      this.isShowModal2_2 = true;
      this.showFormatoSan = true;
      //this.listarTipoSolicitud();
      this.listarImpugnacion();
    }
  }

  listarImpugnacion() {
    let param = {
      //idPermisoForestal: this.idPermisoForestal,
      nroGestion:this.idPermisoForestal,
      pageNum: 1,
      pageSize: 1,
    };

    this.impugnacionSanService
      .listarSolicitudSAN(param)
      .subscribe((res: any) => {
        if (res.success == true) {
          if (res.data) {
            if (res.data.length > 0) {
              let response = res.data[0];
              this.impugnacionSan.asunto = response.asunto;
              this.impugnacionSan.descripcion = response.descripcion;
              this.impugnacionSan.codSolicitudSAN = response.codSolicitudSAN;
              //this.impugnacionSan.idPermisoForestal = response.idPermisoForestal;
              this.impugnacionSan.nroGestion = response.nroGestion;
              this.impugnacionSan.idSolicitudSAN = response.idSolicitudSAN;
              this.impugnacionSan.procesoPostulacion = response.procesoPostulacion;
            }
          }
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  guardarImpugnacion() {
    if(!this.validacionFormulario()) return;
    let params: any[] = [];
    //this.impugnacionSan.idPermisoForestal = this.idPermisoForestal;
    this.impugnacionSan.nroGestion = this.idPermisoForestal;
    this.impugnacionSan.idArchivo = this.idArchivo;

    params.push(this.impugnacionSan);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionSanService
      .registrarSolicitudSAN(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok('Se registró la impugnación SAN correctamente.');
          this.isShowModal2_2 = false;
          this.registroImpugnacionEmiter.emit(1);
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  validacionFormulario() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (
      this.impugnacionSan.asunto == '' ||
      this.impugnacionSan.asunto == null
    ) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Asunto.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}
