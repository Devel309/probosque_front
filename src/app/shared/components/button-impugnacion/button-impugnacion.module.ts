import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonImpugnacionComponent } from './button-impugnacion.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {DialogModule} from 'primeng/dialog';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputButtonsModule} from '../input-button/input-buttons.module';
import {InputButtonsCodigoModule} from '../input-button-codigo/input-buttons.module';
import {ButtonsFilePermisoForestalModule} from '../buttons-file-permiso-forestal/buttons-file-permiso-forestal.module';


@NgModule({
  declarations: [ButtonImpugnacionComponent],
    imports: [
        CommonModule,
        PdfViewerModule,
        DialogModule,
        FormsModule,
        DropdownModule,
        InputTextareaModule,
        InputButtonsModule,
        InputButtonsCodigoModule,
        ButtonsFilePermisoForestalModule
    ],
  exports:[ButtonImpugnacionComponent]
})
export class ButtonImpugnacionModule { }
