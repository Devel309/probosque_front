import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DialogModule } from 'primeng/dialog';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';

import { TableModule } from "primeng/table";
import { PaginatorModule } from "primeng/paginator";

import { BuscarThComponent } from './buscar-th.component';

@NgModule({
  declarations: [
    BuscarThComponent
  ],
  imports: [
    CommonModule,
    DialogModule,
    RadioButtonModule,
    TableModule,
    PaginatorModule,
    FormsModule,
    ButtonModule,
    RippleModule
  ],
  exports:[BuscarThComponent]
})
export class BuscarThModule { }
