import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { PlanManejoContrato } from '@models';
import { PlanificacionService } from '@services';


@Component({
  selector: 'app-buscar-th',
  templateUrl: './buscar-th.component.html',
  styleUrls: ['./buscar-th.component.scss']
})
export class BuscarThComponent implements OnInit {

    contratos: PlanManejoContrato[] = [];
    queryIdContratos: string = "";
    queryContratos: string = "";
    selectContrato!: PlanManejoContrato;

    @Input() verModalBuscarTH: boolean = false;
    @Output() seleccionadoTH = new EventEmitter();
    @Output() closeModal = new EventEmitter();
  constructor(
    private apiPlanificacion: PlanificacionService,
  ) { }

  ngOnInit(): void {
    this.listarContratosVigentes();
  }

  listarContratosVigentes() {
    this.contratos = [];
    this.apiPlanificacion
      .listarContratosVigentes(0)
      .subscribe((res) => (this.contratos = [...res.data]));
  }

  filtrarId() {
    
    if (this.queryIdContratos) {
      this.contratos = this.contratos.filter((r) =>
        r.idContrato.toString()
          .toLowerCase()
          .includes(this.queryIdContratos.toLowerCase())
      );
    } else {
      this.listarContratosVigentes();
    }
  }
  
  filtrarContrato() {
    if (this.queryContratos) {
      this.contratos = this.contratos
        .filter((r) => (r.nombreTitular && r.nombreTitular.toLowerCase().includes(this.queryContratos.toLowerCase())) ||
          (r.dniTitular && r.dniTitular.toLowerCase().includes(this.queryContratos.toLowerCase()))
          )
    } else if(this.queryIdContratos){
      this.filtrarId()
    }else {
      this.listarContratosVigentes();
    }
  }

  aceptarTH() {

    this.seleccionadoTH.emit(this.selectContrato.idContrato);

  }
  close(){
    this.closeModal.emit();
  }

}
