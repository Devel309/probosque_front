import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { descargarArchivo, DowloadFileLocal, DownloadFile, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { FileModel } from "src/app/model/util/File";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";


@Component({
  selector: "input-img-buttons",
  templateUrl: "./input-img-buttons.component.html",
  styleUrls: ["./input-img-buttons.component.scss"],
})
export class InputImgButtonsComponent implements OnInit {
  @Input() idPlanManejo!: number;

  @Input() file: any = {};
  @Input() label: string = "";
  @Input() disabled: boolean = false;
  @Input() accept: string = "";
  @Input() idArchivoModal: number = 0;

  @Input() idTipoDocumento!: number;
  @Input() codigoProceso!: string;

  @Output() idArchivoEmit = new EventEmitter();


  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;
  disabledDescarga: boolean = true;

  static get EXTENSIONSAUTHORIZATION2() {
    return ["image/png","application/pdf","pdf","image/jpeg"];
  }

  constructor(
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private toast: ToastService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "IMG";
    this.listarArchivoPMFI();
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        let include = null;

        //console.log("TYPE ",type);
        if (type === "IMG") {
          include = InputImgButtonsComponent.EXTENSIONSAUTHORIZATION2.includes(
            file.type
          );
        } else {
          return;
        }

        if (include !== true) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El tipo de Documento no es Válido.",
          });
        } else if (file.size > 3000000) {
          this.messageService.add({
            key: "tl",
            severity: "warn",
            summary: "",
            detail: "El archivo no debe tener más de  3MB.",
          });
        } else {
          if (type === "IMG") {
            this.fileInfGenreal.url = URL.createObjectURL(e.target.files[0]);
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.fileInfGenreal);
            this.disabledDescarga = true;
            this.guardarArchivoGeneral();
          }
        }
      }
    }
  }

  guardarArchivoGeneral() {
    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: this.idTipoDocumento,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
      }
    });
  }

  registrarArchivo(id: number) {
    // this.idArchivoEmit.emit(id);
    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: this.codigoProceso,
      descripcion: "",
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: "",
    };
    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se cargó el archivo correctamente.");
        this.listarArchivoPMFI();
      } else {
        this.toast.error("Ocurrió un error al realizar la operación.");
      }
    });
  }

  listarArchivoPMFI() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: this.idTipoDocumento,
      codigoProceso: this.codigoProceso,
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarArchivo = true;
        this.eliminarArchivo = false;
        result.data.forEach((element: any) => {
          this.fileInfGenreal.nombreFile = element.nombreArchivo;
          this.idArchivo = element.idArchivo;
          this.registrarArchivoId.emit(this.idArchivo);
          this.disabledDescarga = false;
        });
      } else {
        this.eliminarArchivo = true;
        this.cargarArchivo = false;
      }
    });
  }

  eliminarArchivoGeneral() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivo))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok("Se eliminó el archivo correctamente.");
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = "";
        this.files = [];
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.idArchivo) {
      let params = {
        idArchivo: this.idArchivo
      }

      this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      })

    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
