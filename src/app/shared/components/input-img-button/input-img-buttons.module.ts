import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputImgButtonsComponent } from './input-img-buttons.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputImgButtonsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputImgButtonsComponent],
})
export class InputImgButtonsModule {}
