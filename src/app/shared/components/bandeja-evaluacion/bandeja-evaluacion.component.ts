import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { UsuarioService } from "@services";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { IPermisoOpcion } from "src/app/model/Comun/IPermisoOpcion";
import { EvaluacionService } from "src/app/service/evaluacion/evaluacion.service";
import { EvaluacionListarRequest } from "../../../model/EvaluacionListarRequest";
import { UsuarioModel } from "../../../model/seguridad/usuario";
import { CodigoEstadoEvaluacion } from "../../../model/util/CodigoEstadoEvaluacion";
import {
  CodigoEstadoMesaPartes,
  CodigoEstadoPlanManejo,
} from "../../../model/util/CodigoEstadoPlanManejo";
import { Perfiles } from "../../../model/util/Perfiles";
import { ToastService } from "../../services/toast.service";
import { ResultadoEvaluacionTitularSharedComponent } from "../resultado-evaluacion-titular/resultado-evaluacion-titular-shared.component";

@Component({
  selector: "app-bandeja-evaluacion",
  templateUrl: "./bandeja-evaluacion.component.html",
  styleUrls: ["./bandeja-evaluacion.component.scss"],
})
export class BandejaEvaluacionComponent implements OnInit {
  @ContentChild("acciones") accionesRef!: TemplateRef<any>;
  @ContentChild("verContrato") verContratoRef!: TemplateRef<any>;
  @ContentChild("requisitosPrevios") requisitosPreviosRef!: TemplateRef<any>;
  @ContentChild("requisitosMesaDePartes")
  requisitosMesaDePartes!: TemplateRef<any>;
  @ContentChild("verPermiso") verPermisoRef!: TemplateRef<any>;
  @ContentChild("impugnacionSAN") impugnacionSAN!: TemplateRef<any>;
  @ContentChild("remitiendo") remitiendoRef!: TemplateRef<any>;

  @Input() planes: any[] = [];

  @Input() isBandejaEvaluacion!: boolean;

  @Input() loading: boolean = false;

  @Input() pmficDema?: boolean = false;

  @Input() isPgmfa?: boolean = false;

  CodigoEstadoEvaluacion = CodigoEstadoEvaluacion;

  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;

  CodigoEstadoMesaPartes = CodigoEstadoMesaPartes;

  ref: DynamicDialogRef = new DynamicDialogRef();

  //@Input() isNuevo: boolean = false;
  @Input() descripcionPlan: string = "";
  @Input() perfil: string = "";

  isPerfilArffs: boolean = false;
  Perfiles = Perfiles;

  isShowColumnRequisitosPrevios: boolean = false;
  isShowColumnRequisitosMesaDePartes: boolean = false;
  isShowColumnEvaluacion: boolean = false;
  isShowColumnAcciones: boolean = false;
  isShowButtonNew: boolean = false;
  isShowColumnImpugnacion: boolean = false;
  isShowColumnResolucion: boolean = false;
  isShowColumnRemitir: boolean = false;

  @Input() comboEstado: any[] = [];

  @Input() totalRecords = 0;

  //@Input() f!: FormGroup;

  @Input()
  evaluacionRequest: EvaluacionListarRequest = new EvaluacionListarRequest();

  //@Input() verAgregar: boolean = false;

  @Output() load = new EventEmitter();

  @Output() buscar = new EventEmitter();

  @Output() limpiar = new EventEmitter();

  @Output() nuevo = new EventEmitter();

  @Output() ver = new EventEmitter();
  usuario!: UsuarioModel;

  permisos: IPermisoOpcion = {} as IPermisoOpcion;
  estados: any[] = [];

  constructor(
    private user: UsuarioService,
    private evaluacionService: EvaluacionService,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private toast: ToastService,
    private router: Router,
    private activaRoute: ActivatedRoute
  ) {
    this.usuario = this.user.usuario;
    this.permisos = this.activaRoute.snapshot.data?.permisos || {};
  }

  ngOnInit(): void {
    if (this.perfil == Perfiles.TITULARTH) {
      this.isPerfilArffs = false;
      this.isShowColumnRequisitosPrevios = false;
      this.isShowColumnEvaluacion = false;
      this.isShowColumnAcciones = true;
      this.isShowButtonNew = true;
      this.isShowColumnImpugnacion = false;
      this.isShowColumnResolucion = false;
      this.isShowColumnRemitir = false;
    } else if (this.perfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilArffs = true;
      this.isShowColumnRequisitosPrevios = true;
      this.isShowColumnEvaluacion = true;
      this.isShowColumnAcciones = false;
      this.isShowColumnImpugnacion = true; //TODO cambiar a false
      this.isShowButtonNew = false;
      this.isShowColumnResolucion = true;
      this.isShowColumnRemitir = true;
    } else if (this.perfil == Perfiles.MESA_DE_PARTES) {
      this.isPerfilArffs = false;
      this.isShowColumnRequisitosPrevios = false;
      this.isShowColumnRequisitosMesaDePartes = true;
      this.isShowColumnEvaluacion = false;
      this.isShowColumnAcciones = false;
      this.isShowButtonNew = false;
      this.isShowColumnResolucion = false;
      this.isShowColumnRemitir = false;
    } else if (this.perfil == Perfiles.SERFOR) {
      this.isShowColumnRequisitosPrevios = true;
      this.isShowColumnEvaluacion = false;
      this.isPerfilArffs = false;
      this.isShowColumnRequisitosMesaDePartes = false;
      this.isShowColumnAcciones = false;
      this.isShowButtonNew = false;
      this.isShowColumnResolucion = false;
      this.isShowColumnRemitir = false;
    }  else if (this.perfil == Perfiles.COMPONENTE_ESTADISTICO) {
      this.isShowColumnRequisitosPrevios = true;
      this.isShowColumnEvaluacion = false;
      this.isPerfilArffs = false;
      this.isShowColumnRequisitosMesaDePartes = false;
      this.isShowColumnAcciones = false;
      this.isShowButtonNew = false;
      this.isShowColumnResolucion = false;
      this.isShowColumnRemitir = false;
    } else {
      this.isPerfilArffs = false;
      this.isShowColumnAcciones = true;
      this.isShowColumnEvaluacion = false;
      if (this.isBandejaEvaluacion) {
        this.isShowButtonNew = false;
      } else {
        this.isShowButtonNew = true;
      }
    }

    switch (this.perfil) {
      case Perfiles.AUTORIDAD_REGIONAL:
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.APROBADO,
          estado: "APROBADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.COMPLETADOMP,
          estado: "COMPLETADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.BORRADOR_DENEGADO,
          estado: "DESAPROBADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.DESFAVORABLE,
          estado: "DESFAVORABLE",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.EN_EVALUACION,
          estado: "EN EVALUACIÓN",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.EVALUACION_OBSERVADA,
          estado: "EVALUACIÓN OBSERVADA",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.FAVORABLE,
          estado: "FAVORABLE",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.IMPUGNADO_SAN,
          estado: "IMPUGNADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO,
          estado: "NO PRESENTADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.NO_PROCEDE,
          estado: "NO PROCEDE",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.PROCEDE,
          estado: "PROCEDE",
        });
        break;
      case Perfiles.MESA_DE_PARTES:
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.COMPLETADOMP,
          estado: "COMPLETADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.EN_EVALUACIONMDP,
          estado: "EN EVALUACIÓN MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.NO_PRESENTADO,
          estado: "NO PRESENTADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.OBSERVADOMP,
          estado: "OBERVADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.PRESENTADOMDP,
          estado: "PRESENTADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.PRESENTADOSUBSANADOMP,
          estado: "SUBSANADO MESA DE PARTES",
        });
        break;

      case Perfiles.SERFOR:

        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.APROBADO,
          estado: "APROBADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.COMPLETADOMP,
          estado: "COMPLETADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.BORRADOR_DENEGADO,
          estado: "DESAPROBADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.DESFAVORABLE,
          estado: "DESFAVORABLE",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.EN_EVALUACION,
          estado: "EN EVALUACIÓN",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.EVALUACION_OBSERVADA,
          estado: "EVALUACIÓN OBSERVADA",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.FAVORABLE,
          estado: "FAVORABLE",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.IMPUGNADO_SAN,
          estado: "IMPUGNADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO,
          estado: "NO PRESENTADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.NO_PROCEDE,
          estado: "NO PROCEDE",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.PROCEDE,
          estado: "PROCEDE",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.COMPLETADOMP,
          estado: "COMPLETADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.EN_EVALUACIONMDP,
          estado: "EN EVALUACIÓN MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.NO_PRESENTADO,
          estado: "NO PRESENTADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.OBSERVADOMP,
          estado: "OBERVADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.PRESENTADOMDP,
          estado: "PRESENTADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.PRESENTADOSUBSANADOMP,
          estado: "SUBSANADO MESA DE PARTES",
        });
        break;
      case Perfiles.COMPONENTE_ESTADISTICO:

        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.APROBADO,
          estado: "APROBADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.COMPLETADOMP,
          estado: "COMPLETADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.BORRADOR_DENEGADO,
          estado: "DESAPROBADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.DESFAVORABLE,
          estado: "DESFAVORABLE",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.EN_EVALUACION,
          estado: "EN EVALUACIÓN",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.EVALUACION_OBSERVADA,
          estado: "EVALUACIÓN OBSERVADA",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.FAVORABLE,
          estado: "FAVORABLE",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.IMPUGNADO_SAN,
          estado: "IMPUGNADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.BORRADOR_NO_PRESENTADO,
          estado: "NO PRESENTADO",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.NO_PROCEDE,
          estado: "NO PROCEDE",
        });
        this.estados.push({
          codEstado: CodigoEstadoPlanManejo.PROCEDE,
          estado: "PROCEDE",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.COMPLETADOMP,
          estado: "COMPLETADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.EN_EVALUACIONMDP,
          estado: "EN EVALUACIÓN MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.NO_PRESENTADO,
          estado: "NO PRESENTADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.OBSERVADOMP,
          estado: "OBERVADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.PRESENTADOMDP,
          estado: "PRESENTADO MESA DE PARTES",
        });
        this.estados.push({
          codEstado: CodigoEstadoMesaPartes.PRESENTADOSUBSANADOMP,
          estado: "SUBSANADO MESA DE PARTES",
        });
        break;
      default:
        break;
    }
  }

  verDetalle(item: any) {
    var params = {
      idPlanManejo: item.idPlanManejo,
      codigoEvaluacion: "EVAL",
    };
    this.evaluacionService
      .listarEvaluacionResumido(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.success) {
          if (response.data.length != 0) {
            this.ref = this.dialogService.open(
              ResultadoEvaluacionTitularSharedComponent,
              {
                header: "RESULTADO EVALUACIÓN",
                width: "60%",
                contentStyle: { overflow: "auto" },
                data: {
                  item: item,
                },
              }
            );

            this.ref.onClose.subscribe((resp: any) => {
              if (resp.data.remitir) this.buscar.emit();
              else if (resp) {
                if (
                  resp.tipo == "EVAL" &&
                  resp.data.codigoEvaluacionDet == "POAC"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );

                  this.router.navigate([
                    "/planificacion/evaluacion/requisitos-previos-poac/" +
                      item.idPlanManejo +
                      "/" +
                      resp.data.codigoEvaluacionDet,
                  ]);
                } else if (
                  resp.tipo == "EVAL" &&
                  resp.data.codigoEvaluacionDet == "PGMFA"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/evaluacion/requisitos-previos-pgmfa/" +
                      item.idPlanManejo +
                      "/" +
                      resp.data.codigoEvaluacionDet,
                  ]);
                } else if (resp.tipo == "EVAL" &&  (resp.data.codigoEvaluacionDet == "PMFIC" || resp.data.codigoEvaluacionDet == "DEMA" || resp.data.codigoEvaluacionDet == "PMFI")  ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/evaluacion/requisitos-previos/" +
                      item.idPlanManejo +
                      "/" +
                      resp.data.codigoEvaluacionDet,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "PMFIC"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/plan-manejo-forestal-intermedio/" +
                      item.idPlanManejo,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "DEMA"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/generar-declaracion-manejo-dema/" +
                      item.idPlanManejo,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "PMFI"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/formulacion-PMFI/" + item.idPlanManejo,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "POAC"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/plan-operativo-ccnn-ealta/" +
                      item.idPlanManejo,
                  ]);
                } else if (
                  resp.tipo == "PLAN" &&
                  resp.data.codigoEvaluacionDet == "PGMFA"
                ) {
                  localStorage.setItem(
                    "EvalResuDet",
                    JSON.stringify(resp.data)
                  );
                  this.router.navigate([
                    "/planificacion/plan-general-manejo/" + item.idPlanManejo,
                  ]);
                }
              }
            });
          } else {
            this.toast.warn("El Plan Seleccionado No Tiene Observaciones");
          }
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  //loadData(event: any) {
  //this.changePage.emit(event);
  //}

  registroNotificacionEmit(event: any) {
    this.buscarEvaluacion();
  }

  buscarEvaluacion() {
    this.buscar.emit();
  }

  limpiarEvaluacion() {
    this.limpiar.emit();
  }

  nuevaEvaluacion() {
    this.nuevo.emit();
  }

  loadEvaluacion(event: any) {
    this.load.emit(event);
  }

  verEvaluacion(idPlanEvaluacion: number) {
    this.ver.emit(idPlanEvaluacion);
  }
}
