import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmaPadComponent } from './firma-pad.component';

describe('FirmaPadComponent', () => {
  let component: FirmaPadComponent;
  let fixture: ComponentFixture<FirmaPadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirmaPadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmaPadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
