import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ArchivoService, UsuarioService } from '@services';
import { SignaturePad } from 'angular2-signaturepad';
import { MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { FileModel } from 'src/app/model/util/File';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { ToastService } from '../../services/toast.service';
import { DownloadFile } from '../../util';

@Component({
  selector: 'firma-pad',
  templateUrl: './firma-pad.component.html',
  styleUrls: ['./firma-pad.component.scss'],
})
export class FirmaPadComponent {
  @ViewChild(SignaturePad) signaturePad!: SignaturePad;
  @Input() idPlanManejo!: number;
  @Input() idArchivo: any=0;
  @Input() label: string = '';
  @Input() idTipoDocumento!: any;
  @Input() codigoProceso!: string;
  @Input() disabled: boolean = false;
  signatureImg: string = '';
  @Output() idArchivoFirma = new EventEmitter();

  file:any={};

  signaturePadOptions: Object = {
    minWidth: 2,
    canvasWidth: 600,
    canvasHeight: 300,
  };

  fileInfGenreal: FileModel = {} as FileModel;

  verAdjuntarFirma: boolean= false;

  
  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  deshabilita: boolean = false;
  disabledDescarga: boolean = true;


  constructor( private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private toast: ToastService,
    private route:ActivatedRoute,
    private archivoServ: ArchivoService,
    private messageService: MessageService
    ) {}

  ngOnInit(): void {
        if(parseInt(this.idArchivo)>0){
          this.fileInfGenreal.nombreFile= "firma electronica.png"
          this.disabledDescarga= false;
        }

  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 2);
    this.signaturePad.clear();
  }

  drawComplete() {}

  drawStart() {}

  clearSignature() {
    this.signaturePad.clear();
  }

  savePad() {
    const base64Data = this.signaturePad.toDataURL();
    this.urltoFile(base64Data)
    .then( (_file:any) =>{ 
      this.guardarArchivoGeneral(_file)
  })
}

  cerrarModal() {
    this.verAdjuntarFirma=false;
  }

  urltoFile(url: any) {
    return fetch(url)
      .then(function (res) {
        return res.arrayBuffer();
      })
      .then(function (buf) {
        return new File([buf], 'firma electronica.png', { type: 'image/png' });
      });
  }



  
  guardarArchivoGeneral(file:File) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: this.idTipoDocumento,
        };

        let load = this.dialog.open(LoadingComponent, { disableClose: true });
        
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, file)
          .pipe(finalize(() => load.close()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
  }

  registrarArchivo(id: number) {
    this.idArchivoFirma.emit(id);

    var params = {
      codigoSubTipoPGMF: null,
      codigoTipoPGMF: this.codigoProceso,
      descripcion: '',
      idArchivo: id,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: '',
    };    

    this.anexosService.registrarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.fileInfGenreal.nombreFile= "firma electronica.png"
        this.disabledDescarga= false;
        this.cerrarModal();
      } else {
        this.toast.error('Ocurrió un error al realizar la operación.');
      }
    });
  }



  descargarArchivo(){
    const idArchivo = parseInt(this.idArchivo);
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.errorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.errorMensaje('No se ha encontrado un documento vinculado.');
      this.dialog.closeAll();
    }
  }


  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }


  eliminarArchivoGeneral() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idArchivo))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok('Se eliminó el archivo correctamente.');
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = '';
        this.disabledDescarga = false;
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }
}
