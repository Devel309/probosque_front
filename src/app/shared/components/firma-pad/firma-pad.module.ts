import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SignaturePadModule } from 'angular2-signaturepad';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { FirmaPadComponent } from './firma-pad.component';


@NgModule({
  declarations: [
    FirmaPadComponent
  ],
  exports: [
    FirmaPadComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    ButtonModule,
    SignaturePadModule,
    DialogModule

  ]
})
export class FirmaPadModule { }
