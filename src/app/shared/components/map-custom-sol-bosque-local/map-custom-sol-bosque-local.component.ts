import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ConfirmationService } from 'primeng/api';
import { from, Observable } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SolicitudBosqueLocalGeometriaModel } from 'src/app/model/Solicitud/SolicitudBosqueLocalGeometria';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { ApiForestalService } from 'src/app/service/api-forestal.service';
import { SolicitudBosqueLocalGeometriaService } from 'src/app/service/solicitud/solicitud-bosque-local-geometria.service';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { OgcGeometryType } from '../../enums';
import { MapApi } from '../../mapApi';
import { ToastService } from '../../services/toast.service';
import { DownloadFile } from '../../util';

@Component({
  selector: 'app-map-custom-sol-bosque-local',
  templateUrl: './map-custom-sol-bosque-local.component.html',
  styleUrls: ['./map-custom-sol-bosque-local.component.scss']
})
export class MapCustomSolBosqueLocalComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private toast: ToastService,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private serviceSolBosqueLocal: SolicitudBosqueLocalService,
    private serviceSolBosqueLocalGeometria: SolicitudBosqueLocalGeometriaService,
    private serviceExternos: ApiForestalService
  ) { }

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() tipoArchivoArea!: string;
  @Input() tipoArchivoGeneral!: string;
  @Input() tipoArchivoPunto!: string;
  @Input() isShow?: boolean = true;
  @Input() isShowBotonCargar: boolean = true;
  @Input() showBotonCargarGeneral: boolean = true;
  @Input() showBotonCargarCustom: boolean = false;
  @Input() showOnlyVertice: boolean = false;
  @Input() showOnlyArea: boolean = false;
  @Input() showOnlyAreaCustom: boolean = false;
  @Input() showOnlyVerticeCustom: boolean = false;
  @Input() showValidSuperPos: boolean = false;
  @Input() showBotonGuardar: boolean = true;
  @Input() identificarBosque: boolean = false;
  @Input() validaSuperposicionOtorgar: boolean = false;
  @Input() validaSuperposicionAprovechar: boolean = false;
  @Input() validaSuperposicionPlanificar: boolean = false;
  @Input() isAnexo: boolean = false;
  @Input() printPDF: boolean = false;
  @Input() isListContrato: boolean = false;
  @Input() isGuardarSuperposicion: boolean = false;
  @Input() labelArea: string = 'Área de Est. bosque local';
  @Input() labelVertice: string = 'Vértice de Est. bosque local';
  @Input() labelAreaCustom: string = 'Área de Zonificación Interna';
  @Input() labelVerticeCustom: string = 'Vértices de Zonificación Interna';
  @Input() isLeyenda :boolean = false;
  @Input() leyenda !: string;
  @Output() areaTotal = new EventEmitter<number>();
  @Output() listVertices = new EventEmitter<any>();
  @Input() calcularArea: boolean = false;
  @Input() isDisbledFormu: boolean = false;
  @Output() eliminarDetalleVertice = new EventEmitter;
  @Output() contieneSuperposicion = new EventEmitter<boolean>();


  view: any = null;
  geoJsonLayer: any = null;
  solPlantacionForestalGeometria: SolicitudBosqueLocalGeometriaModel[] = [];
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];
  showMensajeSuperposicion: boolean = false;

  ngOnInit(): void {
    if (this.printPDF === true) {
      this.initializeMapCustom();
    } else {
      this.initializeMap();
    }
    this.obtenerCapas();
  }
  static get TipoCodigo() {
    return {
      TPAREA: 'TPAREA',
      TPPUNTO: 'TPPUNTO',
      TPCATASTRO: 'TPCATASTRO'
    };
  }
  initializeMapCustom() {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMapCustom(container);
    this.view = view;
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  saveFile(item: any) {
    if (item.service === true) {
      return this.guardarCapa(item, 0)
    } else {
      return this.serviceArchivo
        .cargar(item.idUsuario, item.codigo, item.file)
        .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
    }

  }
  saveFileRelation(result: any, item: any) {
    let item2 = {
      idSolBosqueLocalArchivo: 0,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
      idArchivo: result.data,
      idSolBosqueLocal: this.idPlanManejo,
      tipoArchivo: item.tipoArchivo,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: item.descripcion,
      conforme: true
    };
    return this.serviceSolBosqueLocal.registrarInfoAdjuntoSolicitudBosqueLocal(item2).pipe(
      concatMap((response: any) => {
        return this.guardarCapa(item, result.data);
      })
    );
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.solPlantacionForestalGeometria = [];
    let attributes: any = [];
    layerGroup.items.forEach((t3: any) => {
      t3.attributes.forEach((feature: any) => {
        let area = 0;
        if (feature.geometry.type.toUpperCase() === OgcGeometryType.POLYGON || feature.geometry.type.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
          let geometry: any = null;
          geometry = { spatialReference: { wkid: 4326 } };
          geometry.rings = feature.geometry.coordinates;
          area = this.mapApi.calculateArea(geometry, 'hectares');
        }
        feature.properties.area = area;
        attributes.push(feature.properties)
      });

      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item: any = {
        idSolBosqueLocalGeometria: 0,
        idSolBosqueLocal: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.tipo,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        properties: JSON.stringify(attributes),
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.solPlantacionForestalGeometria.push(item);
    });
    return this.serviceSolBosqueLocalGeometria.registrarGeometria(
      this.solPlantacionForestalGeometria
    );
  }
  onChangeFile(e: any) {
    e.preventDefault();
    e.stopPropagation();
    let tipo = e.target.dataset.tipo;
    if (this.codigoSubSeccion !== 'SOLBL3_SS3') {
      if (tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {
        let item = this._layers.find(
          (e: any) => e.descripcion == MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA
        );
        if (item) {
          this.toast.warn('Ya existe un archivo de área');
          return;
        }
      } else if (tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO) {
        let item = this._layers.find(
          (e: any) => e.descripcion == MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO
        );
        if (item) {
          this.toast.warn('Ya existe un archivo de vértices');
          return;
        }
      }
    }

    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      idLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      tipo: tipo,
      tipoArchivo: e.target.dataset.tipoArchivo
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            this.toast.warn(controls.message);
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  guardar() {
    let codigoTipo: any = this.codigoProceso;
    let fileUpload: any = [];
    let validateOverlap: any = false;
    if (this.isGuardarSuperposicion === true) {
      
      this._filesSHP.forEach((t: any) => {
        if (t.inServer !== true || t.service === true) {
          let item: any = {
            idUsuario: this.user.idUsuario,
            codigo: '37',
            codigoTipoPGMF: codigoTipo,
            file: t.file,
            idGroupLayer: t.idGroupLayer,
            tipoArchivo: t.tipoArchivo,
            descripcion: t.descripcion,
            service: t.service
          };
          fileUpload.push(item);
        }
      });
    } else {
      
      this._filesSHP.forEach((t: any) => {
        if (t.inServer !== true && t.file !== null) {
          let item: any = {
            idUsuario: this.user.idUsuario,
            codigo: '37',
            codigoTipoPGMF: codigoTipo,
            file: t.file,
            idGroupLayer: t.idGroupLayer,
            tipoArchivo: t.tipoArchivo,
            descripcion: t.descripcion,
          };
          fileUpload.push(item);
        }
      });
    }

    // this._layers.forEach((t: any) => {
    //   if (t.overlap === true) {
    //     validateOverlap = true;
    //   }
    // });
    let item = this._layers.find(
      (e: any) => e.overlap == true
    );
    
    if (this.isGuardarSuperposicion === true) {
      if (fileUpload.length === 0) {
        this.toast.error('No hay capas nuevas a guardar.');
        return;
      }
    } else {
      if (fileUpload.length === 0) {
        this.toast.error('Cargue un archivo para continuar.');
        return;
      }
    }
    if (typeof item === 'object') {
      this.confirmationService.confirm({
        message: 'El Área está superpuesta ¿Desea continuar?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.saveLayer(fileUpload);
        }
      });
    } else {
      this.saveLayer(fileUpload);
    }
  }
  saveLayer(fileUpload: any) {
    let observer = from(fileUpload);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)
      ))
      .pipe(finalize(() => {
        this.dialog.closeAll();
        this.cleanLayers();
        this.obtenerCapas();
      }))
      .subscribe(
        (result: any) => {
          this.toast.ok(result.message);
        },
        (error) => {
          this.toast.error('Ocurrió un error.');
        }
      );
  }
  obtenerCapas() {
    let item = null;

    if (this.isAnexo === true) {
      item = {
        idSolBosqueLocal: this.idPlanManejo,
      };
    } else {
      item = {
        idSolBosqueLocal: this.idPlanManejo,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
      };
    }
    this.serviceSolBosqueLocalGeometria.listarGeometria(item).subscribe(
      (result: any) => {
        
        if (result.data.length) {
          let item = result.data.find(
            (e: any) => e.tipoGeometria == MapCustomSolBosqueLocalComponent.TipoCodigo.TPCATASTRO
          );
          if (typeof item === 'object') {
            this.labelAreaCustom = 'Área Cat. Zonificación';
            this.labelVerticeCustom = 'Vértice Cat. Zonificación';
            this.contieneSuperposicion.emit(true);
          }

          let sumArea: any = 0;
          //this._listaCapas = result.data;
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;
              let properties = JSON.parse(t.properties)

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                tipoGeometria: t.tipoGeometria,
                jsonGeometry: jsonGeometry,
                properties: properties[0]
              };
              let layer: any = {} as CustomCapaModel;
              layer.codigo = t.idArchivo;
              layer.id = t.idSolBosqueLocalGeometria
              layer.idLayer = this.mapApi.Guid2.newGuid;
              layer.inServer = true;
              layer.nombre = t.nombreCapa;
              layer.groupId = groupId;
              layer.color = t.colorCapa;
              layer.annex = false;
              layer.descripcion = t.tipoGeometria;
              let geoJson = this.mapApi.getGeoJson(
                layer.idLayer,
                groupId,
                item
              );
              layer.geoJson = geoJson;
              if (this.isAnexo === true) {
                if (this.codigoSubSeccion === 'SOLBL2_SS2') {
                  if (t.codigoSubSeccion === 'SOLBL2_SS2') {
                    layer.service = false;
                  } else if (t.codigoSubSeccion === 'SOLBL1_SS3') {
                    layer.service = true;
                  }
                } else if (this.codigoSubSeccion === 'SOLBL3_SS3') {
                  if (t.codigoSubSeccion === 'SOLBL3_SS3') {
                    layer.service = false;
                  } else if (t.codigoSubSeccion === 'SOLBL1_SS3' || t.codigoSubSeccion === 'SOLBL2_SS2') {
                    layer.service = true;
                  }
                } else if (this.codigoSubSeccion === 'SOLBL3_SS8') {
                  if (t.codigoSubSeccion === 'SOLBL3_SS8') {
                    layer.service = false;
                  } else if (t.codigoSubSeccion === 'SOLBL1_SS3' || t.codigoSubSeccion === 'SOLBL2_SS2') {
                    layer.service = true;
                  } else {
                    return;
                  }
                }

              }
              this._layers.push(layer);
              if (t.tipoGeometria === MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO && this.showBotonCargarCustom === true) {
                this.obtenerVerticesMultiPoint(geoJson);
              }
              this.createLayer(geoJson);
              if (t.tipoGeometria === "TPUA" || t.tipoGeometria === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {
                let area: number = 0;
                if (jsonGeometry.type.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
                  jsonGeometry.coordinates.forEach((t: any) => {
                    let geometry: any = null;
                    geometry = { spatialReference: { wkid: 4326 } };
                    geometry.rings = t;
                    area = this.mapApi.calculateArea(geometry, 'hectares');
                    sumArea += area;
                  });
                } else if (jsonGeometry.type.toUpperCase() === OgcGeometryType.POLYGON) {
                  let geometry: any = null;
                  geometry = { spatialReference: { wkid: 4326 } };
                  geometry.rings = jsonGeometry.coordinates;
                  area = this.mapApi.calculateArea(geometry, 'hectares');
                  sumArea += area;
                }

              }
            }
          });
          let areaTotal = Math.abs(sumArea.toFixed(3));
          this.areaTotal.emit(areaTotal);
        }
      },
      (error: any) => {
        let message = null;
        if (error.status === 404) {
          message = 'No se encontró el servicio de listar Geometria.';
        } else {
          message = 'Consulte con el administrador.'
        }
        this.toast.error('Ocurrió un error: ' + message);
      }
    );
  }
  onDownloadFileSHP(item: any) {
    
    if (item.descripcion === MapCustomSolBosqueLocalComponent.TipoCodigo.TPCATASTRO) {
      let nombreFile = item.nombre.replace(/\s+/g, "");
      let options = {
        folder: nombreFile,
        types: {
          point: nombreFile,
          polygon: nombreFile,
          line: nombreFile
        }
      }
      this.mapApi.exportToSHPFile(item.geoJson, options);
    } else {

      this.dialog.open(LoadingComponent, { disableClose: true });
      this.serviceArchivo.obtenerArchivo(item.codigo).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.file,
            result.data.nombre,
            'application/octet-stream'
          );
        }
        (error: HttpErrorResponse) => {
          this.toast.error(error.message);
          this.dialog.closeAll();
        };
      });
    }

  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                if (layer.descripcion === MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO) {
                  this.listVertices.emit([]);
                } else if (layer.descripcion === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {
                  this.areaTotal.emit(0);
                }
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._filesSHP.length === 0) {
                  this.cleanLayers();
                  this.obtenerCapas();
                }
                this.toast.ok('El archivo se eliminó correctamente.');
              } else {
                this.toast.error('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error: any) => {
              this.dialog.closeAll();
              this.toast.error('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      if (layer.descripcion === MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO) {
        this.listVertices.emit([]);
      } else if (layer.descripcion === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {
        this.areaTotal.emit(0);
      }
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._filesSHP.length === 0) {
        this.cleanLayers();
        this.obtenerCapas();
      }
    }
  }
  eliminarArchivoDetalle(layer: any) {
    let item = {
      idSolBosqueLocalGeometria: layer.id,
      idArchivo: layer.codigo,
      idUsuarioElimina: this.user.idUsuario
    }
    return this.serviceSolBosqueLocalGeometria.eliminarGeometriaArchivo(item);
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (this.calcularArea === true && config.tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {
        this.calculateArea(data[0]);
      } else if (config.tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO) {
        this.obtenerVertices(data[0]);
      }
      if (this.validaSuperposicionPlanificar === true && config.tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {

        let coordinates: any = [];
        data[0].features.forEach((t: any) => {
          coordinates.push(t.geometry.coordinates[0]);
        });
        this.servicioSuperposicionPlanificacion(coordinates);
      }
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      
      let title: string = '';
      if (config.tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPAREA) {
        title = `${this.labelArea} (${t.title.replace(/(\w+\/)*(\w+)/gi, '$2')})`
      } else if (config.tipo === MapCustomSolBosqueLocalComponent.TipoCodigo.TPPUNTO) {
        title = `${this.labelVertice} (${t.title.replace(/(\w+\/)*(\w+)/gi, '$2')})`
      } else {
        title = `${t.title.replace(/(\w+\/)*(\w+)/gi, '$2')}`
      }

      t.title = title;
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.tipo = config.tipo;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer: any = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.tipo;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.tipoArchivo = config.tipoArchivo;
    file.descripcion = config.tipo;
    this._filesSHP.push(file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.tipo = item.tipo;
    geoJsonLayer.tipoGeometria = item.tipoGeometria;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
    this._filesSHP = [];
  }
  calculateArea(data: any) {
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    let areaTotal = Math.abs(sumArea.toFixed(3));
    this.areaTotal.emit(areaTotal);
    return areaTotal;
  }
  obtenerVertices(data: any) {
    let listVertices: any = [];
    data.features.forEach((t: any, index: any) => {
      listVertices.push({
        vertice: index + 1,
        este: t.geometry.coordinates[0],
        norte: t.geometry.coordinates[1],
      });
    });
    this.listVertices.emit(listVertices);
  }
  obtenerVerticesMultiPoint(data: any) {
    let listVertices: any = [];
    data.features[0].geometry.coordinates.forEach((t: any, index: any) => {
      listVertices.push({
        vertice: index + 1,
        este: t[0],
        norte: t[1],
      });
    });
    this.listVertices.emit(listVertices);
  }
  servicioSuperposicionPlanificacion(geometry: any) {
    let index = this._layers.findIndex(
      (t: any) => t.overlap === true
    );
    if (index >= 0) {
      this._layers.splice(index, 1);
    }

    let params = {
      codProceso: '080501',
      geometria: {
        poligono: geometry,
      },
    };
    this.showMensajeSuperposicion = true;
    this.serviceExternos
      .validarSuperposicionOtorgamiento(params)
      .subscribe((result: any) => {
        
        this.showMensajeSuperposicion = false;
        if (result.dataService.data.noSuperpuesta.length > 0) {
          result.dataService.data.noSuperpuesta.forEach((t: any) => {
            if (t.seSuperpone === 'Si') {
              if (t.geoJson !== null) {
                let nombreCapa = `Superposición (${t.nombreCapa})`;
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = true;
                t.geoJson.included = false;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.idGroupLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.tipo = MapCustomSolBosqueLocalComponent.TipoCodigo.TPCATASTRO;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.overlap = true;
                layer.nombre = nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                layer.geoJson = t.geoJson;
                layer.file = null;
                this._layers.push(layer);
                this.createLayer(t.geoJson);
                let file = {} as FileModel;
                file.codigo = 0;
                file.file = null;
                file.inServer = false;
                file.service = true;
                file.idGroupLayer = t.geoJson.idGroupLayer;
                file.tipoArchivo = 'Catastro';
                file.descripcion = MapCustomSolBosqueLocalComponent.TipoCodigo.TPCATASTRO;
                this._filesSHP.push(file);
              }
            }
          });
          this.toast.info('Se han encontrado capas superpuestas al área evaluada');
        } else {
          this.toast.info('No se encontró áreas superpuestas.');
        }
        if (result.dataService.data.contenida.length > 0) {
          result.dataService.data.contenida.forEach((t: any) => {
            if (t.contenida === 'Si') {
              if (t.geoJson !== null) {
                let nombreCapa = `Superposición (${t.nombreCapa})`;
                t.geoJson.crs.type = 'name';
                t.geoJson.opacity = 0.4;
                t.geoJson.color = this.mapApi.random();
                t.geoJson.title = nombreCapa;
                t.geoJson.groupId = this._id;
                t.geoJson.service = true;
                t.geoJson.overlap = false;
                t.geoJson.included = true;
                t.geoJson.idLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.idGroupLayer = this.mapApi.Guid2.newGuid;
                t.geoJson.tipo = MapCustomSolBosqueLocalComponent.TipoCodigo.TPCATASTRO;
                let layer: any = {} as CustomCapaModel;
                layer.codigo = null;
                layer.idLayer = t.geoJson.idLayer;
                layer.inServer = false;
                layer.service = true;
                layer.nombre = nombreCapa;
                layer.groupId = t.geoJson.groupId;
                layer.color = t.geoJson.color;
                layer.annex = false;
                layer.geoJson = t.geoJson;
                layer.file = null;
                this._layers.push(layer);

                this.createLayer(t.geoJson);
                let file = {} as FileModel;
                file.codigo = 0;
                file.file = null;
                file.inServer = false;
                file.service = true;
                file.idGroupLayer = t.geoJson.idGroupLayer;
                file.tipoArchivo = 'Catastro';
                file.descripcion = MapCustomSolBosqueLocalComponent.TipoCodigo.TPCATASTRO;
                this._filesSHP.push(file);
              }
            }
          });
          this.toast.info('Se han encontrado capas contenidas al área evaluada');
        }else {
          this.toast.info('No se encontró capas contenidas');
        }
      }, (error) => {
        this.showMensajeSuperposicion = false;
        this.toast.error("El servicio de Superposición no se encuentra disponible. Consulte con el administrador.");
      });
  }
  validarSuperposicion() {
    this.mapApi.cleanLayersService(this._id, this.view);

    let layers: any = this.mapApi.getLayers(this._id, this.view);
    let item = layers.items.find(
      (e: any) => e.tipoGeometria == 'TPAREA'
    );
    if (item !== null) {
      this.servicioSuperposicionPlanificacion(item.attributes[0].geometry.coordinates);
    }

  }
}

