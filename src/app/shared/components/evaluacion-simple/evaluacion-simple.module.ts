import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluacionSimpleComponent } from './evaluacion-simple.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TableModule } from 'primeng/table';



@NgModule({
  declarations: [EvaluacionSimpleComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    TableModule,
  ],
  exports:[EvaluacionSimpleComponent]
})
export class EvaluacionSimpleModule { }
