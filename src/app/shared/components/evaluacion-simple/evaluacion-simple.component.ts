import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';

@Component({
  selector: 'evaluacion-simple-shared',
  templateUrl: './evaluacion-simple.component.html',
  styleUrls: ['./evaluacion-simple.component.scss']
})
export class EvaluacionSimpleComponent implements OnInit {

  @ContentChild('conatinertop') conatinertopRef!: TemplateRef<any>;
  @ContentChild('conatinerbottom') conatinerbottomRef!: TemplateRef<any>;

  @Input('base') base: LineamientoInnerModel = new LineamientoInnerModel();
  @Input() default: boolean = true;
  @Input() disabled!: boolean;

  @Output() onChange: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  update(){
    this.base.limpiarObservacion();
    this.onChange.emit(this.base.conforme)
  }

}
