import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import {
  descargarArchivo,
  DowloadFileLocal,
  DownloadFile,
  ToastService,
} from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { FileModel } from 'src/app/model/util/File';
import { AnexosService } from 'src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service';
import { PermisoForestalArchivoService } from '../../../service/planificacion/permiso-forestal/permiso-forestal-archivo.service';

@Component({
  selector: 'buttons-file-permiso-forestal-shared',
  templateUrl: './buttons-file-permiso-forestal.component.html',
  styleUrls: ['./buttons-file-permiso-forestal.component.scss'],
})
export class ButtonsFilePermisoForestalComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Input() loadFile: boolean = true;
  @Input() file: any = {};
  @Input() label: string = '';
  @Input() disabled: boolean = false;
  @Input() descripcionArchivo: string = '';
  @Input() accept: string = '';
  @Input() fileDescripcion: string = '';
  @Input() codigoTipoDocumento!: any;
  @Input() codigoProceso!: string;
  @Input() subCodigoArchivo!: string;
  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  @Output() eliminarArchivoEmit: EventEmitter<number> = new EventEmitter();
  @Output() descripcionArc: EventEmitter<string> = new EventEmitter();

  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;
  isLoaded: boolean = false;
  verDescargar: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;

  static get EXTENSIONSAUTHORIZATION2() {
    //return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
    return ['.pdf', 'application/pdf'];
  }

  constructor(
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private toast: ToastService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService,
    private permisoForestalArchivoService: PermisoForestalArchivoService
  ) {}

  ngOnInit(): void {

    if(this.accept) {
      //se seteo un ´pdf
    }else{
      this.accept = '.pdf, application/pdf'
    }

    this.fileInfGenreal.inServer = false;
    //this.fileInfGenreal.descripcion = 'PDF';
    if(this.fileDescripcion){
      this.fileInfGenreal.descripcion = this.fileDescripcion;
    }else{
      this.fileInfGenreal.descripcion = 'PDF';
    }


    if (this.loadFile) {
      this.listarArchivos();
    }
  }

  onFileChange(e: any) {
    e.preventDefault();
    e.stopPropagation();
    if (e.target) {
     // console.log("TARGET dataset" , e.target.dataset);
      if (e.target.files.length) {
        let file = e.target.files[0];
        let type = e.target.dataset.type;
        /*let include = null;
        if (type === 'PDF') {
          include = ButtonsFilePermisoForestalComponent.EXTENSIONSAUTHORIZATION2.includes(file.type);
        } else {
          return;
        }*/

        //let include = ButtonsFilePermisoForestalComponent.EXTENSIONSAUTHORIZATION2.includes(file.type);

        /*if (include !== true) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El tipo de Documento no es Válido.',
          });
        } else*/

          if (file.size > 3000000) {
          this.messageService.add({
            key: 'tl',
            severity: 'warn',
            summary: '',
            detail: 'El archivo no debe tener más de  3MB.',
          });
        } else {
          //if (type === 'PDF') {
            this.fileInfGenreal.url = URL.createObjectURL(e.target.files[0]);
            this.fileInfGenreal.nombreFile = e.target.files[0].name;
            this.fileInfGenreal.file = e.target.files[0];
            this.fileInfGenreal.descripcion = type;
            this.fileInfGenreal.inServer = false;
            this.verEnviar1 = true;
            this.files.push(this.fileInfGenreal);
            this.guardarArchivoGeneral();
          //}
        }
      }
    }
  }

  guardarArchivoGeneral() {
    this.verEnviar1 = false;
    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: this.codigoTipoDocumento,
        };

        this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
          });
      }
    });
  }

  registrarArchivo(id: number) {
    var params = {
      idPermisoForestalArchivo: 0,
      codigoArchivo: this.codigoProceso,
      codigoSubArchivo: this.subCodigoArchivo,
      descripcion: this.descripcionArchivo,
      idArchivo: id,
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.user.idUsuario,
      observacion: '',
    };

    let array = [];
    array.push(params);

    this.permisoForestalArchivoService
      .registrarArchivo(array)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se cargó el archivo correctamente.');
          this.listarArchivos();
        } else {
          this.toast.error('Ocurrió un error al realizar la operación.');
        }
      });
  }

  listarArchivos() {
    var params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoArchivo: this.codigoProceso,
      codigoSubArchivo: this.subCodigoArchivo,
      codigoTipoDocumento: this.codigoTipoDocumento,
      idArchivo: null,
    };

    this.permisoForestalArchivoService
      .listarArchivo(params)
      .subscribe((result: any) => {
        if (!!result.data.length) {
          this.cargarArchivo = true;
          this.eliminarArchivo = false;
          result.data.forEach((element: any) => {
            this.fileInfGenreal.nombreFile = element.nombreArchivo;
            this.idArchivo = element.idArchivo;
            this.registrarArchivoId.emit(this.idArchivo);
            this.descripcionArc.emit(element.descripcion);

            this.verEnviar1 = false;
            this.isLoaded = true;
          });
        } else {
          this.eliminarArchivo = true;
          this.cargarArchivo = false;
        }
      });
    //this.registrarArchivoId.emit(this.idArchivo);  ncoqc
  }

  eliminarArchivoGeneral() {
    const params = new HttpParams()
      .set('idArchivo', String(this.idArchivo))
      .set('idUsuarioElimina', String(this.user.idUsuario));

    this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
      if (response.success == true) {
        this.toast.ok('Se eliminó el archivo correctamente');
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = '';
        this.files = [];
        this.isLoaded = false;
        this.eliminarArchivoEmit.emit(1);
      } else {
        this.toast.error('Ocurrió un error al realizar la operación');
      }
    });
  }

  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.idArchivo) {
      let params = {
        idArchivo: this.idArchivo,
      };
      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }
}
