import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsFilePermisoForestalComponent } from './buttons-file-permiso-forestal.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [ButtonsFilePermisoForestalComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [ButtonsFilePermisoForestalComponent],
})
export class ButtonsFilePermisoForestalModule {}
