import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { TableModule } from 'primeng/table';
import { GenericModalAutorizacionComponent } from './generic-modal-autorizacion.component';

@NgModule({
  declarations: [GenericModalAutorizacionComponent],
  exports: [GenericModalAutorizacionComponent],
  imports: [CommonModule, ButtonModule, TableModule, ConfirmPopupModule],
})
export class GenericModalAutorizacionModule {}
