import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericModalAutorizacionComponent } from './generic-modal-autorizacion.component';

describe('GenericModalAutorizacionComponent', () => {
  let component: GenericModalAutorizacionComponent;
  let fixture: ComponentFixture<GenericModalAutorizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenericModalAutorizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericModalAutorizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
