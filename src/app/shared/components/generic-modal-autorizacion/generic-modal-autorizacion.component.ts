import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UsuarioService } from "@services";
import { ConfirmationService } from "primeng/api";
import { DynamicDialogConfig } from "primeng/dynamicdialog";
import { UsuarioModel } from "src/app/model/seguridad/usuario";
import { NotificacionService } from "src/app/service/notificacion";

@Component({
  selector: "app-generic-modal-autorizacion",
  templateUrl: "./generic-modal-autorizacion.component.html",
  styleUrls: ["./generic-modal-autorizacion.component.scss"],
})
export class GenericModalAutorizacionComponent implements OnInit {
  usuario!: UsuarioModel;
  notificaciones: any[] = [];

  isPerfilARFFS: boolean = false;
  isPerfilTitularTH: boolean = false;

  constructor(
    public config: DynamicDialogConfig,
    private usuarioServ: UsuarioService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private notificacionService: NotificacionService
  ) {}

  ngOnInit() {
    this.usuario = this.usuarioServ.usuario;
    this.notificaciones = this.config.data.notificaciones;
  }

  openEliminar(event: Event, data: any, index: number) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el registro?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        if (data.idNotificacion != 0) {
          var params = {
            idNotificacion: data.idNotificacion,
            idUsuarioElimina: this.usuarioServ.idUsuario,
          };
          this.notificacionService
            .eliminarNotificacion(params)
            .subscribe((response: any) => {
              if (response.success == true) {
                this.notificaciones.splice(index, 1);
              }
            });
        } else {
          this.notificaciones.splice(index, 1);
        }
      },
    });
  }

  verDetalle(data: any) {
    this.router.navigate([data]);
  }
}
