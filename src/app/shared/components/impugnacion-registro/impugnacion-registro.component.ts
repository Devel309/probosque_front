import {HttpErrorResponse} from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { finalize, tap } from 'rxjs/operators';
import {ToastService} from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { ImpugnacionService } from 'src/app/service/impugnacion.service';
import * as moment from 'moment';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { UsuarioService } from '@services';
@Component({
  selector: 'app-impugnacion-registro',
  templateUrl: './impugnacion-registro.component.html',
  styleUrls: ['./impugnacion-registro.component.scss']
})
export class ImpugnacionRegistroComponent implements OnInit {

  @Input("ReadOnly") ReadOnly: boolean = false;

  @Input() resolusiones: any[] = [];

  @Input() impugnacion: any = {};

  @Input() archivoRecurso: any = {};

  @Input() archivoSustento: any = {};

  @Output() eliminarFileRecurso = new EventEmitter();
  @Output() eliminarFileSustento = new EventEmitter();

  minDate = moment(new Date()).format('YYYY-MM-DD');
  usuario!: UsuarioModel;
  constructor(
    private impugnacionService: ImpugnacionService,
    private dialog: MatDialog,
    private usuarioServ: UsuarioService,
    private toast: ToastService) { }

  ngOnInit(): void {
    const currentYear = new Date().getFullYear();

    this.usuario = this.usuarioServ.usuario;
  }

  elimnarFileRecurso() {
    this.eliminarFileRecurso.emit();
  }

  elimnarFileSustento() {
    this.eliminarFileSustento.emit();
  }

  validarPlazoImpugnacion(idResolucion: number) {

    var request = {
      idResolucion: idResolucion,
      idUsuarioRegistro: this.usuario .idusuario
    };


    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionService.validarPlazoImpugnacionResolucion(request)
    .pipe(finalize(() => this.dialog.closeAll()))
    .subscribe((result: any )=> {
      if (result.success) {
        if(result.message && result.message.trim() != '') {
          this.impugnacion.idResolucion = null;
          this.toast.warn(result.message);
        }
      }
    }, (error: HttpErrorResponse) => this.errorMensaje(error, true));

  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
