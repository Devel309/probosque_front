import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImpugnacionRegistroComponent } from './impugnacion-registro.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { UploadInputButtonsModule } from '../upload-input-buttons/upload-input-buttons.module';



@NgModule({
  declarations: [ImpugnacionRegistroComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UploadInputButtonsModule,
    DropdownModule,
    MatDatepickerModule,
  ],
  exports:[ImpugnacionRegistroComponent]
})
export class ImpugnacionRegistroModule { }
