import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import {ButtonsFilePagoComponent} from './buttons-file-pago.component';

@NgModule({
  declarations: [ButtonsFilePagoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [ButtonsFilePagoComponent],
})
export class ButtonsFilePagoModule {}
