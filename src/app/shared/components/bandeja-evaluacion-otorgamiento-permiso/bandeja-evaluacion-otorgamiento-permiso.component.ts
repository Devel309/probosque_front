import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { ListarImpugnacionRequest } from '../../../model/Impugnacion';
import { EvaluacionListarRequest } from '../../../model/EvaluacionListarRequest';
import { Perfiles } from '../../../model/util/Perfiles';
import { EvaluacionRequest } from '../../../model/PFCR/EvaluacionRequest';
import { CodigoEstadoEvaluacion } from '../../../model/util/CodigoEstadoEvaluacion';

import { CodigoPermisoForestal } from '../../../model/util/CodigoPermisoForestal';

@Component({
  selector: 'bandeja-evaluacion-otorgamiento-permiso',
  templateUrl: './bandeja-evaluacion-otorgamiento-permiso.component.html',
  styleUrls: ['./bandeja-evaluacion-otorgamiento-permiso.component.scss'],
})
export class BandejaEvaluacionOtorgamientoPermisoComponent implements OnInit {
  @ContentChild('acciones') accionesRef!: TemplateRef<any>;
  @ContentChild('validacion') validacionRef!: TemplateRef<any>;
  @ContentChild('evaluacion') evaluacionRef!: TemplateRef<any>;
  @ContentChild('remitiendo') remitiendoRef!: TemplateRef<any>;
  @ContentChild('verdetalle') verdetalleRef!: TemplateRef<any>;
  @ContentChild('infoEspacial') infoEspacialRef!: TemplateRef<any>;

  @ContentChild('requisitosPrevios') requisitosPreviosRef!: TemplateRef<any>;

  @Input() planes: any[] = [];

  @Input() isBandejaEvaluacion!: boolean;

  @Input() loading: boolean = false;

  //@Input() isNuevo: boolean = false;
  @Input() descripcionPlan: string = '';
  @Input() perfil: string = '';

  isPerfilArffs: boolean = false;
  CodigoPermisoForestal = CodigoPermisoForestal;

  isShowColumnRequisitosPrevios: boolean = false;
  isShowColumnEvaluacion: boolean = false;
  isShowColumnAcciones: boolean = false;
  isShowColumnValidacion: boolean = false;
  isShowButtonNew: boolean = false;

  @Input() comboEstado: any[] = [];

  @Input() totalRecords = 0;

  //@Input() f!: FormGroup;

  @Input() evaluacionRequest: EvaluacionRequest = new EvaluacionRequest();

  //@Input() verAgregar: boolean = false;

  @Output() load = new EventEmitter();

  @Output() buscar = new EventEmitter();

  @Output() limpiar = new EventEmitter();

  @Output() nuevo = new EventEmitter();

  @Output() ver = new EventEmitter();

  estados: any[] = [];

  estadoSelect: string = '';
  fechaPresentacion: string = '';
  isPerOSinforAndCompEstad: boolean = false;

  Perfiles = Perfiles;

  constructor() {}

  ngOnInit(): void {
    if (this.perfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.isPerfilArffs = true;
      this.isShowColumnRequisitosPrevios = false;
      this.isShowColumnEvaluacion = true;
      this.isShowColumnValidacion = false;
      this.isShowColumnAcciones = false;
      this.isShowButtonNew = false;
    } else if (this.perfil == Perfiles.MESA_DE_PARTES) {
      this.isPerfilArffs = false;
      this.isShowColumnRequisitosPrevios = false;
      this.isShowColumnEvaluacion = false;
      this.isShowColumnAcciones = false;
      this.isShowColumnValidacion = true;
      this.isShowButtonNew = false;
    }else if(this.perfil == Perfiles.OSINFOR || this.perfil == Perfiles.SERFOR){
      this.isPerOSinforAndCompEstad = true;
    } else {
      this.isPerfilArffs = false;
      this.isShowColumnAcciones = true;
      this.isShowColumnEvaluacion = false;
      this.isShowColumnValidacion = false;
      if (this.isBandejaEvaluacion) {
        this.isShowButtonNew = false;
      } else {
        this.isShowButtonNew = true;
      }
    }

    if (this.perfil == Perfiles.AUTORIDAD_REGIONAL) {
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_REGISTRADO,
        estado: '  REGISTRADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION,
        estado: '  EN EVALUACIÓN',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE,
        estado: '  FAVORABLE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE,
        estado: '  DESFAVORABLE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_PROCEDE,
        estado: '  PROCEDE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_IMPUGNADO,
        estado: '  IMPUGNADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO,
        estado: '  VAL OBSERVADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_EMITIDO,
        estado: '  EMITIDO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_NOPROCEDE,
        estado: '  NO PROCEDE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_COMPLETADO,
        estado: '  COMPLETADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
        estado: '  PENDIENTE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_VALIDADO,
        estado: '  VALIDADO',
      });
    } else if (this.perfil == Perfiles.MESA_DE_PARTES) {
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_REGISTRADO,
        estado: '  REGISTRADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_PRESENTADO,
        estado: '  PRESENTADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION,
        estado: '  EN EVALUACIÓN',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE,
        estado: '  FAVORABLE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE,
        estado: '  DESFAVORABLE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_PROCEDE,
        estado: '  PROCEDE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_IMPUGNADO,
        estado: '  IMPUGNADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO,
        estado: '  VAL OBSERVADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_EMITIDO,
        estado: '  EMITIDO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_NOPROCEDE,
        estado: '  NO PROCEDE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_COMPLETADO,
        estado: '  COMPLETADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_PENDIENTE,
        estado: '  PENDIENTE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_VALIDADO,
        estado: '  VALIDADO',
      });
    } else {
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_PRESENTADO,
        estado: '  PRESENTADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_IMPUGNADO,
        estado: '  IMPUGNADO',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_PROCEDE,
        estado: 'PROCEDE',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_NOPROCEDE,
        estado: 'NO PROCEDE',
      });
      this.estados.push({
        //codEstado: CodigoPermisoForestal.ESTADO_VAL_VALIDADO,
        codEstado: CodigoPermisoForestal.ESTADO_VAL_EN_EVALUACION,
        estado: 'EN EVALUACIÓN',
      });
      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO,
        estado: 'VAL OBSERVADO',
      });

      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE,
        estado: 'FAVORABLE',
      });

      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE,
        estado: 'DESFAVORABLE',
      });

      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_APROBADO,
        estado: 'APROBADO',
      });

      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_EMITIDO,
        estado: 'EMITIDO',
      });

      this.estados.push({
        codEstado: CodigoPermisoForestal.ESTADO_EVAL_FIRME,
        estado: 'FIRMEZA',
      });
    }

  }



  //loadData(event: any) {
  //this.changePage.emit(event);
  //}
  registroNotificacionEmit(event: any) {
    this.buscarEvaluacion();
  }

  buscarEvaluacion() {
    this.buscar.emit();
  }

  limpiarEvaluacion() {
    this.limpiar.emit();
  }

  nuevaEvaluacion() {
    this.nuevo.emit();
  }

  loadEvaluacion(event: any) {
    this.load.emit(event);
  }

  verEvaluacion(idPlanEvaluacion: number) {
    this.ver.emit(idPlanEvaluacion);
  }
}
