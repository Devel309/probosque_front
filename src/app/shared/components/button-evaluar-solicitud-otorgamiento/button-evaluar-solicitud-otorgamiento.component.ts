import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigoPermisoForestal } from 'src/app/model/util/CodigoPermisoForestal';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { TipoDocGestion } from 'src/app/model/util/TipoDocGestion';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { ContratoService } from 'src/app/service/contrato.service';
import { NotificacionService } from 'src/app/service/notificacion';
import { CodigoProceso } from '../../../model/util/CodigoProceso';
import { PermisoForestalService } from '../../../service/permisoForestal.service';
import { PlantillaConst } from '../../plantilla.const';
import { DownloadFile } from '../../util';

@Component({
  selector: 'button-evaluar-solicitud-otorgamiento',
  templateUrl: './button-evaluar-solicitud-otorgamiento.component.html',
  styleUrls: ['./button-evaluar-solicitud-otorgamiento.component.scss'],
})
export class ButtonEvaluarSolicitudOtorgamientoComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Input() estado!: any;
  @Input() codigoUnico!: any;
  @Input() idUsuarioRegistro!: any;
  @Input() idSolicitud!: any;

  @Output() registroNotificacionEmit: EventEmitter<number> = new EventEmitter(); //para recargar la pagina principal

  pdfSrc: string = '';
  isShowModal2_2: boolean = false;
  isShowModalFirmeza: boolean = false;

  CodigoPermisoForestal = CodigoPermisoForestal;
  CodigoProceso = CodigoProceso;

  codigoArchivoResolAprobacion: string = '';
  codigoArchivoFormatoPermiso: string = '';

  codigoArchivoResolDesaprobacion: string = '';
  codigoArchivoFirmezaResolucion: string = '';

  idArchivoResolAprobacion!: number;
  idArchivoFormatoPermiso!: number;

  idArchivoResolDesprobacion!: number;
  idArchivoFirmezaResolucion!: number;

  disabled: boolean = false;

  usuario!: UsuarioModel;
  tiposArchivos = TiposArchivos;

  fileResolAprobado: any = {};
  fileResolDesAprobado: any = {};
  fileResolPermiso: any = {};

  isDisabledGuardar: boolean = false;

  constructor(
    private dialog: MatDialog,
    private usuariServ: UsuarioService,
    private toast: ToastService,
    private permisoForestalService: PermisoForestalService,
    private archivoService: ArchivoService,
    private notificacionService: NotificacionService,
    private contratoService: ContratoService
  ) {
    this.usuario = this.usuariServ.usuario;
  }

  ngOnInit(): void {
    if (
      this.usuario.sirperfil === Perfiles.OSINFOR ||
      this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO
    ) {
      this.isDisabledGuardar = true;
    }
  }

  btnModal1() {
    if (
      this.estado == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE ||
      this.estado == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE
      // this.estado == CodigoPermisoForestal.ESTADO_EVAL_EMITIDO
    ) {
      this.isShowModal2_2 = true;
      this.obtenerResolucion();
    }

    if (this.estado == CodigoPermisoForestal.ESTADO_EVAL_EMITIDO)
      this.disabled = true;
  }

  obtenerResolucion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.obtenerResolucion(this.idPermisoForestal).subscribe(
      (result) => {
        this.dialog.closeAll();

        if (result.success && result.data.length > 0) {
          const auxData = result.data[0];
          if (auxData?.idResolucion) this.isDisabledGuardar = true;

          auxData.listaResolucionArchivo.forEach((item: any) => {
            this.setIInfoArchivo(item.tipoDocumento, item);
          });
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  btnGuardar(caso: any) {
    let auxArchivos: any[] = [];

    if (caso == CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE) {
      if (!this.fileResolAprobado.nombre) {
        this.toast.warn('Debe adjuntar la Resolución de Aprobación');
        return;
      }
      if (!this.fileResolPermiso.nombre) {
        this.toast.warn('Debe adjuntar el Formato de Permiso');
        return;
      }

      auxArchivos = [
        { idArchivo: this.fileResolAprobado.idArchivo },
        { idArchivo: this.fileResolPermiso.idArchivo },
      ];
    } else if (caso == CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE) {
      if (!this.fileResolDesAprobado.nombre) {
        this.toast.warn(
          'Debe adjuntar la Resolución de Desaprobación o en Abandono'
        );
        return;
      }
      auxArchivos = [{ idArchivo: this.fileResolDesAprobado.idArchivo }];
    } else if (caso == CodigoPermisoForestal.ESTADO_EVAL_EMITIDO) {
      if (this.idArchivoFirmezaResolucion == undefined) {
        this.toast.warn('Debe adjuntar el Formato de Permiso');
        return;
      }
    }

    const params = {
      nroDocumentoGestion: this.idPermisoForestal,
      tipoDocumentoGestion: TipoDocGestion.PERMISOS_FORESTALES,
      estadoResolucion: null,
      asunto: null,
      idUsuarioRegistro: this.usuariServ.idUsuario,
      listaResolucionArchivo: auxArchivos,
    };

   this.registrarResolucionGuardar(params);
  this.registrarNotificacion(this.idPermisoForestal);

  }

  registrarNotificacion(Int : any) {
    let params = {
      idNotificacion: 0,
      codigoDocgestion: "TDOCESTBOSRESOL",
      numDocgestion: Int,
      mensaje: 'La resolución '+Int+' fue remitida , tendrá un periodo de 15 dias para realizar la impugnación.',
      codigoTipoMensaje: 'TNOTIPERF',
      idUsuario: this.usuario.idusuario,
      //codigoPerfil: this.user.usuario.sirperfil,
      codigoPerfil: 'TITULARTH',
      fechaInicio: new Date(),
      cantidadDias: 15,
      url: '/planificacion/evaluacion/otorgamiento-permiso',
      idUsuarioRegistro: this.usuario.idusuario
    }
    
    this.notificacionService
      .registrarNotificacion(params)
      .subscribe()
  }

  registrarResolucionGuardar(paramsIn: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.registrarResolucion(paramsIn).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok(result.message);

          this.isShowModal2_2 = false;
          this.registroNotificacionEmit.emit(1);
          this.notificarResolucion();
        } else {
          this.toast.warn(result.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  notificarResolucion() {
    const params = {
      idSolicitud: this.idSolicitud,
      idUsuarioRegistro: this.idUsuarioRegistro,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.notificarResolucion(params).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
        } else {
          this.toast.warn(result.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  showFirmeza() {
    this.isShowModalFirmeza = true;
  }

  openFirmeza() {
    if (this.estado == CodigoPermisoForestal.ESTADO_EVAL_EMITIDO) {
      if (this.idArchivoFirmezaResolucion == undefined) {
        this.toast.warn('Debe adjuntar el Firmeza de Resolución');
        return;
      }
    }

    let param = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEstado: CodigoPermisoForestal.ESTADO_EVAL_FIRME,
      idUsuarioRegistro: this.usuariServ.idUsuario,
    };

    this.permisoForestalService
      .actualizarEstadoPermisoForestal(param)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok(
            'El permiso forestal se actualizo a un estado de Firmeza de Resolución'
          );
          this.isShowModal2_2 = false;
          this.registroNotificacionEmit.emit(1);
        } else {
          this.toast.error(response?.message);
        }
      });
    // this.isShowModalFirmeza = true;
  }

  guardarFirmeza(caso: any) {
    this.isShowModalFirmeza = false;

    let param = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEstado: CodigoPermisoForestal.ESTADO_EVAL_FIRME,
      idUsuarioRegistro: this.usuariServ.idUsuario,
    };

    this.permisoForestalService
      .actualizarEstadoPermisoForestal(param)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se actualizó a un estado de Firmeza de Resolución');
          this.isShowModal2_2 = false;
          this.registroNotificacionEmit.emit(1);
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  registrarArchivoAdjunto(file: any, codigo: string) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService
      .cargar(this.usuario.idusuario, codigo, file.file)
      .subscribe(
        (result) => {
          this.dialog.closeAll();
          if (result.success) {
            this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
            this.setIdArchivoRegistrado(codigo, result.data);
          } else {
            this.toast.warn(result.message);
            this.limpiarArchivoFile(codigo);
          }
        },
        (error) => {
          this.limpiarArchivoFile(codigo);
          this.errorMensaje(error, true);
        }
      );
  }

  eliminarArchivoAdjunto(idAdjuntoOut: number, codigo: string) {
    if (!idAdjuntoOut) return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService
      .eliminarArchivo(idAdjuntoOut, this.usuario.idusuario)
      .subscribe(
        (result) => {
          this.dialog.closeAll();
          if (result.success) {
            this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
            this.limpiarArchivoFile(codigo);
          } else {
            this.toast.warn(result.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  setIInfoArchivo(codigoIn: string, fileInfo: any) {
    if (codigoIn === TiposArchivos.EOP_APROBADO)
      this.fileResolAprobado = fileInfo;
    else if (codigoIn === TiposArchivos.EOP_DESAPROBADO)
      this.fileResolDesAprobado = fileInfo;
    else if (codigoIn === TiposArchivos.EOP_PERMISO)
      this.fileResolPermiso = fileInfo;
  }

  setIdArchivoRegistrado(codigoIn: string, idArchivoIn: number) {
    if (codigoIn === TiposArchivos.EOP_APROBADO)
      this.fileResolAprobado.idArchivo = idArchivoIn;
    else if (codigoIn === TiposArchivos.EOP_DESAPROBADO)
      this.fileResolDesAprobado.idArchivo = idArchivoIn;
    else if (codigoIn === TiposArchivos.EOP_PERMISO)
      this.fileResolPermiso.idArchivo = idArchivoIn;
  }

  limpiarArchivoFile(codigoIn: string) {
    if (codigoIn === TiposArchivos.EOP_APROBADO) this.fileResolAprobado = {};
    else if (codigoIn === TiposArchivos.EOP_DESAPROBADO)
      this.fileResolDesAprobado = {};
    else if (codigoIn === TiposArchivos.EOP_PERMISO) this.fileResolPermiso = {};
  }

  descargarPlantResolAproba() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.descargarPlantillaResolAprobacion().subscribe(
      (data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  /*descargarPlantResolDesAproba() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.descargarPlantillaResolDesAprobacion().subscribe((data: any) => {
      this.dialog.closeAll();
      DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
    }, (error) => this.errorMensaje(error, true));
  }*/
  descargarPlantResolDesAproba() {
    const NombreGenerado: string = PlantillaConst.PlantillaResolucionDeNegacion;

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService.descargarPlantilla(NombreGenerado).subscribe(
      (data: any) => {
        this.dialog.closeAll();
        if (data.isSuccess) {
          DownloadFile(data.archivo, data.informacion, data.contenTypeArchivo);
        } else {
          this.toast.warn('Ocurrió un problema, intente nuevamente');
        }
      },
      () => {
        this.dialog.closeAll();
        this.toast.warn('Ocurrió un problema, intente nuevamente');
      }
    );
  }

  descargarPlantResolPermiso() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.descargarPlantillaFormatoPermiso().subscribe(
      (data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  registrarArchivoFirmezaResolucion(eve: any) {
    if (eve > 0) {
      this.idArchivoFirmezaResolucion = eve;
    }
  }

  generarCodigo() {}
}
