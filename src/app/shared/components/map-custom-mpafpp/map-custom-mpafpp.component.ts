import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, PlanificacionService, UsuarioService } from '@services';
import { DownloadFile, MapApi, PGMFArchivoTipo, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { PlanManejoGeometriaModel } from 'src/app/model/PlanManejo/PlanManejoGeometria';
import { CustomCapaModel } from 'src/app/model/util/CustomCapa';
import { FileModel } from 'src/app/model/util/File';
import { CodigosPGMF } from 'src/app/model/util/PGMF/CodigosPGMF';
import { ApiGeoforestalService } from 'src/app/service/api-geoforestal.service';
import { PlanManejoGeometriaService } from 'src/app/service/plan-manejo-geometria.service';
import { PostulacionPFDMService } from 'src/app/service/postulacionPFDM/postulacion-pfdm.service';

@Component({
  selector: 'app-map-custom-mpafpp',
  templateUrl: './map-custom-mpafpp.component.html',
  styleUrls: ['./map-custom-mpafpp.component.scss']
})
export class MapCustomMPAFPPComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private user: UsuarioService,
    private confirmationService: ConfirmationService,
    private mapApi: MapApi,
    private serviceArchivo: ArchivoService,
    private servicePostulacionPFDM: PostulacionPFDMService,
    private servicePlanManejoGeometria: PlanManejoGeometriaService,
    private serviceGeoforestal: ApiGeoforestalService,
    private planificacionService: PlanificacionService,
    private toast: ToastService) { }

  @ViewChild('map', { static: true }) private mapViewEl!: ElementRef;
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() tipoArchivoArea!: string;
  @Input() tipoArchivoGeneral!: string;
  @Input() tipoArchivoPunto!: string;
  @Input() isShow?: boolean = true;
  @Input() isShowBotonCargar: boolean = true;
  @Input() showBotonCargarGeneral: boolean = true;
  @Input() showBotonCargarCustom: boolean = false;
  @Input() showOnlyVertice: boolean = false;
  @Input() showOnlyArea: boolean = false;
  @Input() showBotonGuardar: boolean = true;
  @Input() identificarBosque: boolean = false;
  @Input() validaSuperposicionOtorgar: boolean = false;
  @Input() validaSuperposicionAprovechar: boolean = false;
  @Input() isAnexo: boolean = false;
  @Input() printPDF: boolean = false;
  @Input() isListContrato: boolean = false;
  @Output() areaTotal = new EventEmitter<number>();
  @Output() listVertices = new EventEmitter<any>();
  @Input() calcularArea: boolean = false;
  @Input() isDisbledFormu: boolean = false;
  @Input() showHU01: boolean = false;
  @Output() eliminarDetalleVertice = new EventEmitter;
  TPAREA = "TPAREA";
  TPPUNTO = "TPPUNTO";

  view: any = null;
  geoJsonLayer: any = null;
  planManejoGeometria: PlanManejoGeometriaModel[] = [];
  _id = this.mapApi.Guid2.newGuid;
  _filesSHP: FileModel[] = [];
  _layers: CustomCapaModel[] = [];


  ngOnInit(): void {
    if (this.printPDF === true) {
      this.initializeMapCustom();
    } else {
      this.initializeMap();
    }
    this.obtenerCapas();
    if (this.isListContrato === true) {
      // this.listarContratos();
    }
  }
  initializeMap(): void {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMap(container);
    this.view = view;
  }
  initializeMapCustom() {
    const container = this.mapViewEl.nativeElement;
    container.style.height = '390px';
    container.style.width = '100%';
    const view = this.mapApi.initializeMapCustom(container);
    this.view = view;
  }
  guardar() {
    let codigoTipo: any = PGMFArchivoTipo.PGMFA;
    let fileUpload: any = [];
    this._filesSHP.forEach((t: any) => {
      if (t.inServer !== true) {
        let item: any = {
          idUsuario: this.user.idUsuario,
          codigo: '37',
          codigoTipoPGMF: codigoTipo,
          file: t.file,
          idGroupLayer: t.idGroupLayer,
          descripcion: t.opcion
        };
        fileUpload.push(item);
      }
    });
    let observer = from(fileUpload);
    if (fileUpload.length === 0) {
      this.toast.warn('Cargue un archivo para continuar.');
      return;
    }
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)))
      .pipe(finalize(() => {
        this.dialog.closeAll();
        this.cleanLayers();
        this.obtenerCapas();
      }))
      .subscribe(
        (result) => {
          this.toast.ok(result.message);
          if (this.isListContrato === true) {
            //this.listarContratos();
          }
        },
        (error) => {
          this.toast.error('Ocurrió un error. por favor comunique al Administrador');
        }
      );
  }
  saveFile(item: any) {
    return this.serviceArchivo
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));
  }
  saveFileRelation(result: any, item: any) {
    let item2 = {
      codigoTipoPGMF: item.codigoTipoPGMF,
      idArchivo: result.data,
      idPlanManejo: this.idPlanManejo,
      idUsuarioRegistro: this.user.idUsuario,
      descripcion: item.descripcion
    };
    return this.servicePostulacionPFDM.registrarArchivoDetalle(item2).pipe(
      concatMap((response: any) => {
        return this.guardarCapa(item, item2.idArchivo);
      })
    );
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    let layers = this.getLayers();
    let layerGroup = layers.filter(
      (t2: any) => t2.idGroupLayer === itemFile.idGroupLayer
    );
    this.planManejoGeometria = [];
    layerGroup.items.forEach((t3: any) => {
      let geometryWKT: any = this.mapApi.getGeometry(
        t3,
        this.view.spatialReference.wkid
      );
      let item = {
        idPlanManejoGeometria: 0,
        idPlanManejo: this.idPlanManejo,
        idArchivo: idArchivo,
        tipoGeometria: t3.tipo,
        codigoGeometria: t3.geometryType,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
        geometry_wkt: geometryWKT,
        srid: 4326,
        nombreCapa: t3.title,
        colorCapa: t3.color,
        idUsuarioRegistro: this.user.idUsuario,
      };
      this.planManejoGeometria.push(item);
    });

    return this.servicePlanManejoGeometria.registrarPlanManejoGeometria(
      this.planManejoGeometria
    );
  }
  obtenerCapas() {
    let item = null;

    if (this.isAnexo === true) {
      item = {
        idPlanManejo: this.idPlanManejo,
      };
    } else if (this.showHU01 === true) {
      item = {
        idPlanManejo: this.idPlanManejo,
        codigoSeccion: this.codigoProceso
      };
    } else {
      item = {
        idPlanManejo: this.idPlanManejo,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
      };
    }
    this.servicePlanManejoGeometria.listarPlanManejoGeometria(item).subscribe(
      (result) => {
        let area: number = 0;
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let groupId = this._id;

              let item = {
                color: t.colorCapa,
                title: t.nombreCapa,
                jsonGeometry: jsonGeometry,
                properties: {
                  title: t.nombreCapa,
                },
              };
              let layer: any = {} as CustomCapaModel;
              layer.codigo = t.idArchivo;
              layer.idLayer = this.mapApi.Guid2.newGuid;
              if (this.isAnexo === true || t.tipoGeometria === 'TPUA') {
                layer.inServer = false;
                layer.service = true;
              } else {
                layer.inServer = true;
                layer.service = false;
              }
              layer.nombre = t.nombreCapa;
              layer.groupId = groupId;
              layer.color = t.colorCapa;
              layer.annex = false;
              layer.descripcion = t.tipoGeometria;
              if (this.isAnexo === true) {
                if (t.codigoSeccion !== 'POCCANEX') {
                  layer.service = true;
                }
              } else if (this.showHU01 === true) {
                if (t.codigoSubSeccion === 'DEMAP2_SS1' || t.codigoSubSeccion === 'DEMAP1_SS3') {
                  if (t.codigoSubSeccion !== 'DEMAP2_SS1') {
                    layer.service = true;
                  }
                } else if (t.codigoSubSeccion === 'PMFIP2_SS1' || t.codigoSubSeccion === 'PMFIP1_SS3') {
                  if (t.codigoSubSeccion !== 'PMFIP2_SS1') {
                    layer.service = true;
                  }
                }  else if (t.codigoSubSeccion === 'POPMIFP2_SS1' || t.codigoSubSeccion === 'POPMIFP1_SS3') {
                  if (t.codigoSubSeccion !== 'POPMIFP2_SS1') {
                    layer.service = true;
                  }
                }else if (t.codigoSubSeccion === 'DEMAC2_SS1' || t.codigoSubSeccion === 'DEMAC1_SS3') {
                  if (t.codigoSubSeccion !== 'DEMAC2_SS1') {
                    layer.service = true;
                  }
                }else {
                  return;
                }

              }

              this._layers.push(layer);
              let geoJson = this.mapApi.getGeoJson(
                layer.idLayer,
                groupId,
                item
              );
              this.createLayer(geoJson);
              if (t.tipoGeometria === 'TPAREA' || t.tipoGeometria === 'TPUA') {
                let geometry: any = null;
                geometry = { spatialReference: { wkid: 4326 } };
                geometry.rings = jsonGeometry.coordinates;
                area = + this.mapApi.calculateArea(geometry, 'hectares');

              }
            }
          });
        }
        this.areaTotal.emit(Math.abs(area));
      },
      (error) => {
        this.toast.error('Ocurrió un error');
      }
    );
  }
  eliminarArchivoDetalle(idArchivo: Number) {
    return this.servicePlanManejoGeometria.eliminarPlanManejoGeometriaArchivo(
      idArchivo,
      this.user.idUsuario
    );
  }
  processFile(file: any, config: any) {
    config.file = file;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.dialog.closeAll();
      this.createLayers(data, config);
      if (this.calcularArea === true && config.tipo === this.TPAREA) {
        this.calculateArea(data[0]);
      } else if (config.tipo === this.TPPUNTO) {
        this.obtenerVertices(data[0]);
      }
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idLayer = this.mapApi.Guid2.newGuid;
      t.groupId = this._id;
      t.idGroupLayer = config.idGroupLayer;
      t.tipo = config.tipo;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };
      let layer: any = {} as CustomCapaModel;
      layer.codigo = config.idArchivo;
      layer.idLayer = t.idLayer;
      layer.inServer = config.inServer;
      layer.nombre = t.title || config.fileName;
      layer.groupId = t.groupId;
      layer.color = t.color;
      layer.idGroupLayer = config.idGroupLayer;
      layer.descripcion = config.tipo;
      this._layers.push(layer);
      this.createLayer(t);
    });
    let file = {} as FileModel;
    file.codigo = config.idArchivo;
    file.file = config.file;
    file.inServer = config.inServer;
    file.idGroupLayer = config.idGroupLayer;
    file.descripcion = config.opcion;
    file.tipoArchivo = config.tipoArchivo;
    this._filesSHP.push(file);
  }
  createLayer(item: any) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.mapApi.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = 'vector';
    geoJsonLayer.groupId = this._id;
    geoJsonLayer.idGroupLayer = item.idGroupLayer;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.tipo = item.tipo;
    this.view.map.add(geoJsonLayer);
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.mapApi.changeLayerStyle(
          geoJsonLayer.id,
          geoJsonLayer.color,
          this.view
        );
        this.view.goTo({ target: data.fullExtent.extent });
      })
      .catch((error: any) => { });
  }
  getLayers() {
    let layers = this.view.map.allLayers.filter(
      (t: any) => t.groupId === this._id
    );
    return layers;
  }
  toggleLayer(idLayer: any, e: any) {
    this.mapApi.toggleLayer(idLayer, e.currentTarget.checked, this.view);
  }
  cleanLayers() {
    let layers = this.mapApi.getLayers(this._id, this.view);
    layers.forEach((t: any) => {
      this.view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
    this._layers = [];
  }
  calculateArea(data: any) {
    let sumArea: any = null;
    data.features.forEach((t: any) => {
      let geometry: any = null;
      geometry = { spatialReference: { wkid: 4326 } };
      geometry.rings = t.geometry.coordinates;
      let area = this.mapApi.calculateArea(geometry, 'hectares');
      sumArea += area;
    });
    let areaTotal = Math.abs(sumArea.toFixed(3));
    this.areaTotal.emit(areaTotal);
    return areaTotal;
  }
  obtenerVertices(data: any) {
    let listVertices: any = [];
    data.features.forEach((t: any, index: any) => {
      listVertices.push({
        vertice: t.properties.vertice || t.properties.ORIG_FID,
        este: t.properties.este || t.properties.POINT_X,
        norte: t.properties.norte || t.properties.POINT_Y,
      });
    });
    this.listVertices.emit(listVertices);
  }
  onChangeFile(e: any, tipo: string, tipoArchivo: string) {
    if (tipo === this.TPAREA) {
      let item = this._layers.find(
        (e: any) => e.descripcion == this.TPAREA
      );
      if (item) {
        e.target.value = '';
        this.toast.warn('Ya existe un archivo de área');
        return;
      }
    } else if (tipo === this.TPPUNTO) {
      let item = this._layers.find(
        (e: any) => e.descripcion == this.TPPUNTO
      );
      if (item) {
        e.target.value = '';
        this.toast.warn('Ya existe un archivo de vértices');
        return;
      }
    }
    e.preventDefault();
    e.stopPropagation();
    let config = {
      idGroupLayer: this.mapApi.Guid2.newGuid,
      inServer: false,
      service: false,
      validate: false,
      opcion: e.target.dataset.zone || null,
      tipo: tipo,
      tipoArchivo: tipoArchivo
    };
    if (e.target) {
      if (e.target.files.length) {
        let i = 0;
        while (i < e.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(e.target.files);
          if (controls.success == false) {
            return;
          } else {
            this.processFile(e.target.files[i], config);
          }
          i++;
        }
      }
      e.target.value = '';
    }
    e.target.value = '';
  }
  onDownloadFileSHP(id: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.serviceArchivo.obtenerArchivo(id).subscribe((result: any) => {
      this.dialog.closeAll();
      if (result.data !== null && result.data !== undefined) {
        DownloadFile(
          result.data.file,
          result.data.nombre,
          'application/octet-stream'
        );
      }
    }, (error: HttpErrorResponse) => {
      this.toast.error(error.message);
      this.dialog.closeAll();
    });
  }
  onRemoveFileSHP(layer: any) {
    if (layer.inServer === true) {
      this.confirmationService.confirm({
        message: '¿Está seguro de eliminar este grupo de archivo?',
        icon: 'pi pi-exclamation-triangle',
        key: 'deleteFileSHP',
        acceptLabel: 'Si',
        rejectLabel: 'No',
        accept: () => {
          this.dialog.open(LoadingComponent, { disableClose: true });
          this.eliminarArchivoDetalle(layer.codigo).subscribe(
            (response: any) => {
              this.dialog.closeAll();
              if (response.success) {
                if (layer.descripcion === this.TPPUNTO) {
                  this.eliminarDetalleVertice.emit(this.TPPUNTO);
                } else if (layer.descripcion === this.TPAREA) {
                  this.areaTotal.emit(0);
                }
                let index = this._layers.findIndex(
                  (t: any) => t.idLayer === layer.idLayer
                );
                let indexFile = this._filesSHP.findIndex(
                  (t: any) => t.idGroupLayer === layer.idGroupLayer
                );
                this._filesSHP.splice(indexFile, 1);
                this._layers.splice(index, 1);
                this.mapApi.removeLayer(layer.idLayer, this.view);
                if (this._layers.length === 0) {
                  this.cleanLayers();
                }
                this.toast.ok('El archivo se eliminó correctamente.');
              } else {
                this.toast.error('No se pudo eliminar, vuelve a intertarlo.');
              }
            },
            (error) => {
              this.dialog.closeAll();
              this.toast.error('Ocurrió un problema.' + error.statusText);
            }
          );
        },
      });
    } else {
      if (layer.descripcion === this.TPPUNTO) {
        this.listVertices.emit([]);
      } else if (layer.descripcion === this.TPAREA) {
        this.areaTotal.emit(0);
      }
      let index = this._layers.findIndex(
        (t: any) => t.idLayer === layer.idLayer
      );
      let indexFile = this._filesSHP.findIndex(
        (t: any) => t.idGroupLayer === layer.idGroupLayer
      );
      this._filesSHP.splice(indexFile, 1);
      this._layers.splice(index, 1);
      this.mapApi.removeLayer(layer.idLayer, this.view);
      if (this._layers.length === 0) {
        this.cleanLayers();
      }
    }
  }

}
