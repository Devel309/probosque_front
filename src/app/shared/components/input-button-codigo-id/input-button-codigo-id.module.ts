import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonsCodigoIdComponent } from './input-button-codigo-id.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonsCodigoIdComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonsCodigoIdComponent],
})
export class InputButtonsCodigoIdModule {}
