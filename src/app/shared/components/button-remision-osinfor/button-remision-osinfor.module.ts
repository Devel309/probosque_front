import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonRemisionOsinforComponent } from './button-remision-osinfor.component';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { MatDatepickerModule } from '@angular/material/datepicker';

@NgModule({
  declarations: [ButtonRemisionOsinforComponent],
  imports: [
    CommonModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    ButtonModule,
    MatDatepickerModule,
  ],
  exports: [ButtonRemisionOsinforComponent],
})
export class ButtonRemisionOsinforModule {}
