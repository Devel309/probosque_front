import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonRemisionOsinforComponent } from './button-remision-osinfor.component';

describe('ButtonRemisionOsinforComponent', () => {
  let component: ButtonRemisionOsinforComponent;
  let fixture: ComponentFixture<ButtonRemisionOsinforComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonRemisionOsinforComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonRemisionOsinforComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
