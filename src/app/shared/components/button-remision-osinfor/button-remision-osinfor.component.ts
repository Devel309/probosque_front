import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DepartamentoModel, DistritoModel, ProvinciaModel } from '@models';
import { CoreCentralService, UsuarioService } from '@services';
import { CodigoPermisoForestal } from 'src/app/model/util/CodigoPermisoForestal';
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { PermisoForestalService } from 'src/app/service/permisoForestal.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'button-remision-osinfor',
  templateUrl: './button-remision-osinfor.component.html',
  styleUrls: ['./button-remision-osinfor.component.scss'],
})
export class ButtonRemisionOsinforComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Output() registroNotificacionEmit: EventEmitter<number> = new EventEmitter(); //para recargar la pagina principal

  isShowModal2_2: boolean = false;
  departamento = {} as DepartamentoModel;
  listDepartamento: DepartamentoModel[] = [];
  provincia = {} as ProvinciaModel;
  listProvincia: ProvinciaModel[] = [];
  distrito = {} as DistritoModel;
  listDistrito: DistritoModel[] = [];
  lstEstado: any[] = [];

  constructor(
    private dialog: MatDialog,
    private usuariServ: UsuarioService,
    private toast: ToastService,
    private servCoreCentral: CoreCentralService
  ) {}

  ngOnInit(): void {
    this.listarPorFiltroDepartamento(this.departamento);
  }

  verPDF() {
    this.isShowModal2_2 = true;
  }

  guardar() {}

  listarPorFiltroDepartamento(departamento: DepartamentoModel) {
    this.servCoreCentral.listarPorFiltroDepartamento(departamento).subscribe(
      (result: any) => {
        this.listDepartamento = result.data;
      },
      (error: any) => {}
    );
  }

  onSelectedProvincia(param: any) {
    this.listProvincia = [];
    this.listDistrito = [];
    const provincia = {} as ProvinciaModel;
    provincia.idDepartamento = param.value;
    this.servCoreCentral
      .listarPorFilroProvincia(provincia)
      .subscribe((result: any) => {
        this.listProvincia = result.data;
      });
  }

  onSelectedDistrito(param: any) {
    this.listDistrito = [];
    const distrito = {} as ProvinciaModel;
    distrito.idProvincia = param.value;
    this.servCoreCentral
      .listarPorFilroDistrito(distrito)
      .subscribe((result: any) => {
        this.listDistrito = result.data;
      });
  }
}
