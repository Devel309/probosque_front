import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { DowloadFileLocal } from '@shared';
import { Confirmation, ConfirmationService, MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { DownloadFile } from '@shared';

@Component({
  selector: 'anexo-upload-input-buttons',
  templateUrl: './anexo-upload-input-buttons.component.html',
  styleUrls: ['./anexo-upload-input-buttons.component.scss']
})
export class AnexoUploadInputButtonsComponent implements OnInit {

  @ViewChild("fileInput") fileInputVariable: any;

  @Input() file: any = {};
  @Input() label: string = "";
  @Input() disabled: boolean = false;
  @Input() accept: string = '';

  @Output() registrarArchivo = new EventEmitter();
  @Output() eliminarArchivo = new EventEmitter();

  constructor(private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,

  ) { }

  ngOnInit(): void {
  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.file.url = URL.createObjectURL(event.target.files[0]);
    this.file.file = event.target.files[0];
    this.file.nombre = event.target.files[0].name;

    this.registrarArchivo.emit(this.file);
  }

  eliminar(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      accept: () => {
        this.fileInputVariable.nativeElement.value = "";
        this.eliminarArchivo.emit(this.file);
      },

    });

  }

  descargarArchivo() {
    if (!this.file.file?.size) {
      DownloadFile(this.file.file, this.file.nombre, 'application/octet-stream');
    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
    }

    /*
    var blob=new Blob([this.file], {type: "application/octet-stream"});// change resultByte to bytes

    var link=document.createElement('a');
    link.href=window.URL.createObjectURL(blob);
    link.download=this.file.nombre;
    link.click();*/
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }


}
