import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnexoUploadInputButtonsComponent } from './anexo-upload-input-buttons.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import {ConfirmPopupModule} from 'primeng/confirmpopup';



@NgModule({
  declarations: [
    AnexoUploadInputButtonsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    ConfirmPopupModule
  ],
  exports:[AnexoUploadInputButtonsComponent]
})
export class AnexoUploadInputButtonsModule { }
