import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ArchivoService, UsuarioService } from "@services";
import { DownloadFile } from "@shared";
import * as moment from "moment";
import { ConfirmationService, MessageService } from "primeng/api";
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef,
} from "primeng/dynamicdialog";
import { finalize, tap } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { SolicitudOpinionRequest } from "src/app/model/solicitud-opinion";
import { GenericoService } from "src/app/service/generico.service";
import { SolicitudOpinionService } from "src/app/service/solicitud-opinion.service";
import { ToastService } from "../../services/toast.service";

@Component({
  selector: "app-modal-solicitud-opinion",
  templateUrl: "./modal-solicitud-opinion.component.html",
  styleUrls: ["./modal-solicitud-opinion.component.scss"],
})
export class ModalSolicitudOpinionComponent implements OnInit {
  disabled: boolean = false;
  mostrar: boolean = true;
  opinion: any = {};

  comboTipoDocumento: any[] = [];

  archivoOficio: any = {};

  archivoRespuesta: any = {};

  archivoGestion: any = {};

  loading: boolean = false;

  refLoad!: DynamicDialogRef;

  displayOk: boolean = false;
  displayConfirm: boolean = false;

  @ViewChild("fileInput") inputOficio!: ElementRef;
  @ViewChild("fileInput2") inputResp!: ElementRef;

  minDate = moment(new Date()).format("YYYY-MM-DD");

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private genericoService: GenericoService,
    private solicitudOpinionServ: SolicitudOpinionService,
    private archivoServ: ArchivoService,
    private confirmationService: ConfirmationService,
    private usuarioServ: UsuarioService,
    private messageService: MessageService,
    public dialogService: DialogService,
    public toastService: ToastService
  ) {}

  ngOnInit(): void {
    this.disabled = this.config.data.disabled;
    this.comboTipoDocumento = this.config.data.comboDocumentos;

    this.listarsolicitud(true);
  }

  listarsolicitud(load: boolean) {
    this.loading = load;

    let params = {
      idSolicitudOpinion: this.config.data.idSolicitudOpinion,
    };

    let f = this.opinion.esFavorable;
    let x = this.opinion.descripcion;
    let y = this.opinion.tipoDocumento;
    let z = this.opinion.fechaEmision;

    this.solicitudOpinionServ.obtenerSolicitudOpinion(params).subscribe(
      (result: any) => {
        if (result.data) {
          this.opinion = {
            ...result.data,
            fechaEmision: result.data.fechaEmision
              ? new Date(result.data.fechaEmision)
              : result.data.fechaEmision,
          };

          if (
            result.data.esFavorable == null ||
            result.data.descripcion == null ||
            result.data.tipoDocumento == null ||
            result.data.fechaEmision == null
          ) {
            this.opinion.esFavorable = f;
            this.opinion.descripcion = x;
            this.opinion.tipoDocumento = y;
            this.opinion.fechaEmision = z;
          }

          this.archivoOficio.nombre = result.data.nombreDocumentoOficio;
          this.archivoOficio.idArchivo = result.data.documentoOficio;
          this.opinion.tipoDocumento = result.data.tipoDocumento;

          this.archivoRespuesta.nombre = result.data.nombreDocumentoRespuesta;
          this.archivoRespuesta.idArchivo = result.data.documentoRespuesta;

          this.opinion.idArchivo =
            result.data.documentoGestion || this.opinion.idArchivo;
          if (this.opinion.idArchivo > 0) {
            this.mostrar = false;
          }
        }
        this.loading = false;
      },
      (error) => {
        this.ErrorMensaje("Ocurrió un error");
        this.loading = false;
      }
    );
  }

  obtenerArchivoRespuesta() {
    this.refLoad = this.dialogService.open(LoadingComponent, {
      closable: false,
    });

    this.archivoServ
      .obtenerArchivo(this.opinion.documentoRespuesta)
      .pipe(
        tap(
          (res: any) => {
            if (res.data) {
              this.archivoRespuesta = res.data;
              this.descargarArchivoRespuesta();
            }
          },
          (error) => {}
        )
      )
      .pipe(finalize(() => this.refLoad.close()))
      .subscribe();
  }

  obtenerArchivoOficio() {
    this.refLoad = this.dialogService.open(LoadingComponent, {
      closable: false,
    });

    this.archivoServ
      .obtenerArchivo(this.opinion.documentoOficio)
      .pipe(
        tap(
          (res: any) => {
            if (res.data) {
              this.archivoOficio = res.data;
              this.descargarArchivoOficio();
            }
          },
          (error) => {}
        )
      )
      .pipe(finalize(() => this.refLoad.close()))
      .subscribe();
  }

  obtenerArchivoGestion() {
    this.refLoad = this.dialogService.open(LoadingComponent, {
      closable: false,
    });
    
    this.archivoServ
      .obtenerArchivo(this.opinion.idArchivo)
      .pipe(
        tap(
          (res: any) => {
            if (res.data) {
              this.archivoGestion = res.data;
              this.descargarArchivoGestion();
            }
          },
          (error) => {}
        )
      )
      .pipe(finalize(() => this.refLoad.close()))
      .subscribe();
  }

  onFileChangeOficio(event: any) {
    event.preventDefault();
    if (!this.opinion.tipoDocumento) {
      this.ErrorMensaje("Seleccione tipo Documento");
      return;
    }
    this.refLoad = this.dialogService.open(LoadingComponent, {
      closable: false,
    });

    this.archivoOficio.url = URL.createObjectURL(event.target.files[0]);
    this.archivoOficio.file = event.target.files[0];
    this.archivoOficio.nombre = event.target.files[0].name;

    let params = {
      idTipoDocumento: this.opinion.tipoDocumento,
      idSolicitudOpinion: this.opinion.idSolicitudOpinion,
      documento: 1,
      idUsuarioModificacion: this.usuarioServ.usuario.idusuario,
      file: this.archivoOficio.file,
    };

    this.solicitudOpinionServ
      .actualizarSolicitudOpinionArchivo(params)
      .subscribe(
        (result: any) => {
          if (result?.body?.success) {
            this.SuccessMensaje(
              "Se adjuntó el documento de oficio correctamente."
            );
            this.listarsolicitud(false);
            this.refLoad.close();
          }
        },
        (err) => {
          this.ErrorMensaje("Ocurrió un error");
          this.refLoad.close();
        }
      );
  }

  onFileChangeOResp(event: any) {
    event.preventDefault();
    if (!this.opinion.tipoDocumento) {
      this.ErrorMensaje("Seleccione Tipo Documento.");
      return;
    }

    this.refLoad = this.dialogService.open(LoadingComponent, {
      closable: false,
    });

    this.archivoRespuesta.url = URL.createObjectURL(event.target.files[0]);
    this.archivoRespuesta.file = event.target.files[0];
    this.archivoRespuesta.nombre = event.target.files[0].name;

    let params = {
      idTipoDocumento: this.opinion.tipoDocumento,
      idSolicitudOpinion: this.opinion.idSolicitudOpinion,
      documento: 2,
      idUsuarioModificacion: this.usuarioServ.usuario.idusuario,
      file: this.archivoRespuesta.file,
    };

    this.solicitudOpinionServ
      .actualizarSolicitudOpinionArchivo(params)
      .subscribe(
        (result: any) => {
          if (result?.body?.success) {
            this.SuccessMensaje(
              "Se adjuntó el documento de respuesta correctamente."
            );
            this.listarsolicitud(false);
            this.refLoad.close();
          }
        },
        (err) => {
          this.ErrorMensaje("Ocurrió un error");
          this.refLoad.close();
        }
      );
  }

  descargarArchivoOficio() {
    DownloadFile(
      this.archivoOficio.file,
      this.archivoOficio.nombre,
      "application/octet-stream"
    );
  }

  descargarArchivoRespuesta() {
    DownloadFile(
      this.archivoRespuesta.file,
      this.archivoRespuesta.nombre,
      "application/octet-stream"
    );
  }

  descargarArchivoGestion() {
    if (this.archivoGestion)
      DownloadFile(
        this.archivoGestion.file,
        this.archivoGestion.nombre,
        "application/octet-stream"
      );
  }

  eliminarArchivoOficio(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el archivo?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        this.refLoad = this.dialogService.open(LoadingComponent, {
          closable: false,
        });

        let params = {
          idSolicitudOpinion: this.opinion.idSolicitudOpinion,
          documentoOficio: this.archivoOficio.idArchivo,
          idUsuarioElimina: this.usuarioServ.usuario.idusuario,
        };

        this.solicitudOpinionServ
          .eliminarSolicitudOpinionArchivo(params)
          .subscribe(
            (result: any) => {
              this.SuccessMensaje(
                "Se eliminó el documento de oficio correctamente."
              );
              this.archivoOficio = {};
              this.inputOficio.nativeElement.value = "";
              this.refLoad.close();
            },
            (error) => {
              this.ErrorMensaje("Ocurrió un error");
              this.refLoad.close();
            }
          );
      },
    });
  }

  eliminarArchivoRespuesta(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de eliminar el archivo?.",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Sí",
      rejectLabel: "No",
      accept: () => {
        this.refLoad = this.dialogService.open(LoadingComponent, {
          closable: false,
        });

        let params = {
          idSolicitudOpinion: this.opinion.idSolicitudOpinion,
          documentoRespuesta: this.archivoRespuesta.idArchivo,
          idUsuarioElimina: this.usuarioServ.usuario.idusuario,
        };

        this.solicitudOpinionServ
          .eliminarSolicitudOpinionArchivo(params)
          .subscribe(
            (result: any) => {
              this.SuccessMensaje(
                "Se eliminó el documento de respuesta correctamente."
              );
              this.archivoRespuesta = {};
              this.inputResp.nativeElement.value = "";
              this.refLoad.close();
            },
            (error) => {
              this.ErrorMensaje("Ocurrió un error");
              this.refLoad.close();
            }
          );
      },
    });
  }

  enviar() {
    this.displayConfirm = false;

    this.refLoad = this.dialogService.open(LoadingComponent, {
      closable: false,
    });

    let params: SolicitudOpinionRequest = {
      tipoDoc: this.opinion.tipoDoc,
      tipoDocumento: this.opinion.tipoDocumento,
      fechaEmision: this.opinion.fechaEmision,
      esFavorable: this.opinion.esFavorable,
      descripcion: this.opinion.descripcion,
      idSolicitudOpinion: this.opinion.idSolicitudOpinion,
      idUsuarioModificacion: this.usuarioServ.usuario.idusuario,
      estSolicitudOpinion:
        this.opinion.esFavorable == true ? "ESOPA" : "ESOPDENE",
    };

    this.solicitudOpinionServ
      .actualizarSolicitudOpinion(params)
      .subscribe((result: any) => {
        if (result.success) {
          this.listarsolicitud(true);
          this.toastService.ok(
            "Se actualizó la solicitud de opinion correctamente."
          );
          this.refLoad.close();
          this.ref.close(true);
        } else {
          this.ErrorMensaje("Ocurrió un error.");
        }
      });
  }

  finalizar() {
    this.ref.close(true);
  }

  abrirModalConfirmacion() {
    if (!this.validarOpinion()) return;
    // this.messageService.add({
    //   severity: 'success',
    //   summary: '',
    //   detail: 'Respuesta enviada correctamente.',
    // });
    // this.finalizar();
    this.displayConfirm = true;
  }

  cancelar() {
    this.ref.close(false);
  }

  validarOpinion() {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.opinion.esFavorable == null) {
      validar = false;
      mensaje = mensaje +=
        "(*) Seleccione si la solicitud de opinión es favorable o desfavorable. \n";
    }

    if (!this.opinion.descripcion) {
      validar = false;
      mensaje = mensaje += "(*) Ingrese descripción. \n";
    }

    if (!this.opinion.tipoDocumento) {
      validar = false;
      mensaje = mensaje += "(*) Seleccione el tipo de documento. \n";
    }

    if (!this.opinion.fechaEmision) {
      validar = false;
      mensaje = mensaje += "(*) Ingrese fecha de emision. \n";
    }

    if (!this.archivoOficio.nombre) {
      validar = false;
      mensaje = mensaje += "(*) Adjunte Documento Oficio.\n";
    }

    if (!this.archivoRespuesta.nombre) {
      validar = false;
      mensaje = mensaje += "(*) Adjunte Archivo Respuesta.\n";
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  private SuccessMensaje(mensaje: any) {
    this.messageService.add({
      severity: "success",
      summary: "",
      detail: mensaje,
    });
  }

  private ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
