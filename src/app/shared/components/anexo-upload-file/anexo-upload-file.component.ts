import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { DowloadFileLocal, DownloadFile, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { FileModel } from "src/app/model/util/File";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { PostulacionPFDMService } from "src/app/service/postulacionPFDM/postulacion-pfdm.service";
import { CodigoPOOC } from "src/app/model/util/POCC/CodigoPOOC";

@Component({
  selector: "app-anexo-upload-file",
  templateUrl: "./anexo-upload-file.component.html",
  styleUrls: ["./anexo-upload-file.component.scss"],
})
export class AnexoUploadFileComponent implements OnInit {
  @Input() idPlanManejo!: number;

  @Input() file: any = {};
  @Input() label: string = "";
  @Input() disabled: boolean = false;
  @Input() accept: string = "";
  @Input() idArchivoModal: number = 0;
  //@Input() ocultarJustificacion: boolean = true;
  @Input() ocultarActualizar: boolean = false;
  @Input() disableEliminar: boolean = false;
  @Input() mensajeGuardar: string = '';
  @Input() mensajeEliminar: string = '';

  @Input() idTipoDocumento!: any;

  @Input() codigoProceso!: string;

  @Input() isDisbledObjFormu: boolean = false;

  @Input() fileInfGenreal: FileModel = {} as FileModel;

  @Output() idArchivoEmit = new EventEmitter();

  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  @Output() justificacionEmiter: EventEmitter<string> = new EventEmitter();

  files: FileModel[] = [];
  verEnviar1: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;
  idPGMFArchivo: number = 0;
  justificacion: string = "";
  pendiente: boolean = false;
  actualizar: boolean = false;
  deshabilita: boolean = false;
  CodigoPOCC = CodigoPOOC;

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "application/pdf",".doc",".docx","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"];
  }

  constructor(
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private toast: ToastService,
    private messageService: MessageService,
    private archivoServ: ArchivoService,
    private postulacionPFDMService: PostulacionPFDMService,
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";
    this.listarArchivoPMFI();
  }

  onFileChange(e: any) {
    

      e.preventDefault();
      e.stopPropagation();
      if (e.target) {
        if (e.target.files.length) {
          let file = e.target.files[0];
          let type = e.target.dataset.type;

          let include = null;
          if (type === "PDF") {
            include = AnexoUploadFileComponent.EXTENSIONSAUTHORIZATION2.includes(
              file.type
            );
          } else {
            return;
          }

          if (include !== true) {
            this.messageService.add({
              key: "tl",
              severity: "warn",
              summary: "",
              detail: "El tipo de Documento no es Válido.",
            });
          } else if (file.size > 3000000) {
            this.messageService.add({
              key: "tl",
              severity: "warn",
              summary: "",
              detail: "El archivo no debe tener más de  3MB.",
            });
          } else {
            if (type === "PDF") {

              //if ( this.justificacion == undefined || this.justificacion == '' ){
             //   this.toast.warn("Ingrese una justificación.");
             //   return;
             // }else{
                this.fileInfGenreal.url = URL.createObjectURL(e.target.files[0]);
                this.fileInfGenreal.nombreFile = e.target.files[0].name;
                this.fileInfGenreal.file = e.target.files[0];
                this.fileInfGenreal.descripcion = type;
                this.fileInfGenreal.inServer = false;
                this.verEnviar1 = true;
                this.pendiente = true;
                this.files.push(this.fileInfGenreal);
                this.guardarArchivoGeneral();
             // }
            }
          }
        }
      }



  }

  guardarArchivoGeneral() {

    /*if ( this.justificacion == undefined || this.justificacion == '' ){
      this.toast.warn("Ingrese una justificación.");
      return;
    }*/

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.deshabilita = true;
    this.files.forEach((t: any) => {
      if (t.inServer !== true) {
        let item = {
          id: this.user.idUsuario,
          tipoDocumento: this.idTipoDocumento,
        };

        // this.dialog.open(LoadingComponent, { disableClose: true });
        this.anexosService
          .cargarAnexos(item.id, item.tipoDocumento, t.file)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((result: any) => {
            this.registrarArchivo(result.data);
            
            this.idArchivo = result.data;

            this.registrarArchivoId.emit(result.data);
          });
      }
    });
  }

  emitJustificacion() {
    this.fileInfGenreal.justificacion = this.justificacion;
    this.justificacionEmiter.emit(this.justificacion);
  }

  registrarArchivo(id: number) {
    var params = {
      idPGMFArchivo: this.idPGMFArchivo,
      codigoTipoPGMF: this.codigoProceso,
      codigoSubTipoPGMF: null,
      descripcion: this.justificacion,
      observacion: null,
      idPlanManejo: this.idPlanManejo,
      idArchivo: id,
      idUsuarioRegistro: this.user.idUsuario,
    };

    
    this.postulacionPFDMService
      .registrarArchivoDetalle(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          if (this.codigoProceso == this.CodigoPOCC.TAB_11) {
            this.toast.ok('Se adjuntó el archivo de evidencia correctamente.');
          } else if(this.mensajeGuardar.length>1){
            this.toast.ok(this.mensajeGuardar);
          }
          else {
            this.toast.ok('Se registró la evidencia correctamente.');
          }
          this.listarArchivoPMFI();
        } else {
          this.toast.error(response?.message);
        }
      });
    this.deshabilita = false;
  }

  listarArchivoPMFI() {
    let load = this.dialog.open(LoadingComponent, { disableClose: true });

    this.postulacionPFDMService
      .obtenerArchivoDetalle(this.idPlanManejo, this.codigoProceso)
      .pipe(finalize(() => load.close()))
      .subscribe((response: any) => {

        if (response.data && response.data.length != 0) {
          this.cargarArchivo = true;
          this.eliminarArchivo = false;
          response.data.forEach((element: any) => {
            this.fileInfGenreal.nombreFile = element.nombre;
            this.fileInfGenreal.justificacion = element.descripcion;
            this.idArchivo = element.idArchivo;
            this.justificacion = element.descripcion;
            this.idPGMFArchivo = element.idPGMFArchivo;
            this.pendiente = false;
            this.actualizar = true;
            this.registrarArchivoId.emit(element);
            this.emitJustificacion();
          });
        } else {
          this.eliminarArchivo = true;
          this.cargarArchivo = false;
        }
      });
  }

  eliminarArchivoGeneral() {
    const params = new HttpParams()
      .set("idArchivo", String(this.idArchivo))
      .set("idUsuarioElimina", String(this.user.idUsuario));

    // this.anexosService.eliminarArchivo(params).subscribe((response: any) => {
    this.postulacionPFDMService.eliminarArchivoDetalle(this.idArchivo, this.user.idUsuario).subscribe((response: any) => {
      if (response.success == true) {
        this.idPGMFArchivo = 0;
        this.files = [];
        if(this.mensajeEliminar.length>1){
          this.toast.ok(this.mensajeEliminar);
        }else{
          this.toast.ok("Se eliminó la evidencia correctamente.");
        }
        this.cargarArchivo = false;
        this.eliminarArchivo = true;
        this.fileInfGenreal.nombreFile = "";
      } else {
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    });
  }

  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.idArchivo) {
      let params = {
        idArchivo: this.idArchivo,
      };

      this.archivoServ
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.ErrorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }

  editar() {
    var params = {
      idPGMFArchivo: this.idPGMFArchivo,
      codigoTipoPGMF: this.codigoProceso,
      codigoSubTipoPGMF: null,
      descripcion: this.justificacion,
      observacion: null,
      idPlanManejo: this.idPlanManejo,
      idArchivo: this.idArchivo,
      idUsuarioRegistro: this.user.idUsuario,
    };
    this.postulacionPFDMService
      .registrarArchivoDetalle(params)
      .subscribe((response: any) => {
        if (response.success == true) {
          this.toast.ok('Se actualizó la información correctamente.');
          this.listarArchivoPMFI();
        } else {
          this.toast.error(response?.message);
        }
      });
  }

  ErrorMensaje(mensaje: any) {
    this.toast.warn(mensaje);
  }
}
