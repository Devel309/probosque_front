import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AnexoUploadFileComponent } from "./anexo-upload-file.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ButtonModule } from "primeng/button";
import { ConfirmPopupModule } from "primeng/confirmpopup";
import { InputTextareaModule } from "primeng/inputtextarea";
import { SharedModule } from "../../shared.module";

@NgModule({
  declarations: [AnexoUploadFileComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
    InputTextareaModule,
    SharedModule
  ],
  exports: [AnexoUploadFileComponent],
})
export class AnexoUploadFileModule {}
