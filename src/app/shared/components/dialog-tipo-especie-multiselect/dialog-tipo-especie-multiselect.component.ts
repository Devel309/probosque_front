import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CoreCentralService } from '@services';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { EspeciesFauna } from 'src/app/model/medioTrasporte';

@Component({
  selector: 'app-dialog-tipo-especie-multiselect',
  templateUrl: './dialog-tipo-especie-multiselect.component.html',
  styleUrls: ['./dialog-tipo-especie-multiselect.component.scss'],
})
export class DialogTipoEspecieMultiselectComponent implements OnInit {
  queryFauna: string = '';
  comboListEspeciesFauna: EspeciesFauna[] = [];
  selectedValues: any[] = [];
  totalRecords: number = 0;
  objbuscar = {
    dato: '',
    pageNum: 1,
    pageSize: 10,
  };

  constructor(
    private coreCentralService: CoreCentralService,
    private dialog: MatDialog,
    public ref: DynamicDialogRef,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.listaPorFiltroEspecieFauna();
  }

  loadData(e: any) {
    this.selectedValues = [];
    const pageSize = Number(e.rows);
    this.objbuscar.pageNum = Number(e.first) / pageSize + 1;
    this.objbuscar.pageSize = pageSize;
    this.listaPorFiltroEspecieFauna();
  }

  filtrarFauna() {
    this.listaPorFiltroEspecieFauna();
  }

  listaPorFiltroEspecieFauna() {
    let params = {
      idEspecie: null,
      nombreComun: null,
      nombreCientifico: this.queryFauna ? this.queryFauna : null,
      autor: null,
      familia: null,
      pageNum: this.objbuscar.pageNum,
      pageSize: this.objbuscar.pageSize,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.coreCentralService
      .ListaPorFiltroEspeciesForestales(params)
      .subscribe((result: any) => {
        this.dialog.closeAll();
        this.totalRecords = result.totalrecord;
        this.comboListEspeciesFauna = [...result.data];
      });
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.queryFauna == null || this.queryFauna == '') {
      validar = false;
      mensaje = mensaje += '(*) Debe seleccionar: Una Especie.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  agregar() {
    /* if (!this.validarMedidas()) {
      return;
    } */
    this.ref.close(this.selectedValues);
  }
  cerrarModal() {
    this.selectedValues = [];
    this.ref.close();
  }
}
