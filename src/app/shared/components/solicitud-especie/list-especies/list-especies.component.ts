import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService } from '@services';
import { DownloadFile } from '@shared';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';

@Component({
  selector: 'app-list-especies',
  templateUrl: './list-especies.component.html',
  styleUrls: ['./list-especies.component.scss']
})
export class ListEspeciesComponent implements OnInit {

  @Input() listAspectosBiologicos!: any;

  @Input() disabled!:boolean;

  @Output() dataEditar = new EventEmitter();
  @Output() indexEditar = new EventEmitter();

  @Output() dataEliminar = new EventEmitter();
  @Output() indexEliminar = new EventEmitter();

  constructor(
    private confirmationService: ConfirmationService,
    private dialog: MatDialog,
    private archivoService: ArchivoService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
  }

  editar(index: number, data: any){
    this.dataEditar.emit(data)
    this.indexEditar.emit(index)

  }



  eliminar(event: any, index: number, data: any){

    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar este registro?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Si',
      rejectLabel:'No',
      accept: () => {

        this.dataEliminar.emit(data)
        this.indexEliminar.emit(index)

      },
      reject: () => {

      }
   });

  }

  descargarArchivo(idArchivo: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    let params = {
      idArchivo: idArchivo,
    };
    this.archivoService
      .descargarArchivoGeneral(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(
            result.data.archivo,
            result.data.nombeArchivo,
            result.data.contenTypeArchivo
          );
        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
        };
      });
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

}
