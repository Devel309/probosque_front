import { Component, Input, OnInit } from "@angular/core";
import { CodigoPMFIC } from "src/app/model/util/PMFIC/CodigoPMFIC";

@Component({
  selector: "tabla-tipo-bosque",
  templateUrl: "./tabla-tipo-bosque.component.html",
  styleUrls: ["./tabla-tipo-bosque.component.scss"],
})
export class TableTipoBosqueComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;
  @Input() listaBosque: any;
  @Input() totalAreaBosque: any;
  @Input() totalPorcentajeBosque: any;
  @Input() isVisible!: boolean;
  

  CodigoPMFIC = CodigoPMFIC;

  constructor() {}

  ngOnInit(): void {}

  // ---------- Aspectos Biológicos Fauna------------------- //
}
