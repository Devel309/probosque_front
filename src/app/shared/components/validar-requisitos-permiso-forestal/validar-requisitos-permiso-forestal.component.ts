import {Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {CodigoEstadoEvaluacion} from '../../../model/util/CodigoEstadoEvaluacion';
import {EvaluacionArchivoModel} from '../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionPermisoForestalModel} from '../../../model/Comun/EvaluacionPermisoForestalModel';
import {EvaluacionService} from '../../../service/evaluacion/evaluacion.service';
import {MatDialog} from '@angular/material/dialog';
import {ToastService} from '@shared';
import {UsuarioService} from '@services';
import {CodigoProceso} from '../../../model/util/CodigoProceso';
import {EvaluacionUtils} from '../../../model/util/EvaluacionUtils';
import {LoadingComponent} from '../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {Mensajes} from '../../../model/util/Mensajes';
import {CodigoPermisoForestal} from '../../../model/util/CodigoPermisoForestal';

@Component({
  selector: 'validar-requisitos-permiso-forestal-shared',
  templateUrl: './validar-requisitos-permiso-forestal.component.html',
  styleUrls: ['./validar-requisitos-permiso-forestal.component.scss']
})
export class ValidarRequisitosPermisoForestalComponent implements OnInit {
  @Input() idPermisoForestal!: number;
  @Input() codigoProceso!    : string;
  @Input() codigoTab!        : string;
  @Input() codigoAcordeon!   : string;
  @Input() detalle! :string;
  @Output() seleccionarValidacion = new EventEmitter();
  //codigoObservado            : string = CodigoEstadoEvaluacion.OBSERVADO;
  //codigoConforme             : string = CodigoEstadoEvaluacion.RECIBIDO;

  codigoValObs            : string = CodigoPermisoForestal.ESTADO_VAL_VALIDADO_OBSERVADO;
  codigoValidado             : string = CodigoPermisoForestal.ESTADO_VAL_VALIDADO;

  evaluacion:any;

  @ContentChild('conatinertop') conatinertopRef!: TemplateRef<any>;
  @ContentChild('conatinerbottom') conatinerbottomRef!: TemplateRef<any>;


  evaluacionDet!: EvaluacionPermisoForestalModel;

  constructor(private evaluacionService: EvaluacionService,
              private dialog: MatDialog,
              private toast: ToastService,
              private user: UsuarioService,) {

    this.evaluacion = {
      codigoEvaluacion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idEvaluacionPermiso: 0,
      estadoEvaluacion: 'EEVAPRES',
      fechaEvaluacionInicial: new Date().toISOString(),
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.user.idUsuario,
      tipoEvaluacion:CodigoPermisoForestal.TIPO_VALIDACION,
      listarEvaluacionPermisoDetalle: [],
    };
  }

  ngOnInit(): void {
    //this.base.archivo = this.archivoResolucionDesaprueba
    this.evaluacionDet = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab,
      codigoEvaluacionDetPost : this.codigoAcordeon,
      detalle :this.detalle
    });
    this.obtenerEvaluacion();
  }

  cargarIdArchivo(idArchivo: any) {
  
    this.evaluacionDet.idArchivo = idArchivo;
  }

  obtenerEvaluacion() {
    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab,
      tipoEvaluacion : CodigoPermisoForestal.TIPO_VALIDACION
    }

    this.evaluacionService.obtenerEvaluacionPermisoForestal(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion) {
            this.evaluacionDet = Object.assign(this.evaluacionDet,this.evaluacion.listarEvaluacionPermisoDetalle.find((x: any) =>
              x.codigoEvaluacionDetPost == this.codigoAcordeon //&& x.tipoEvaluacion == CodigoPermisoForestal.TIPO_VALIDACION
            ));
            this.emitirEvaluacion();
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validarPermisoForestal([this.evaluacionDet])) {

      if(this.evaluacion) {
        this.evaluacion.idPermisoForestal = this.idPermisoForestal;
        this.evaluacion.listarEvaluacionPermisoDetalle = [];
        this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet);
        this.dialog.open(LoadingComponent, { disableClose: true });
        this.evaluacionService.registrarEvaluacionPermisoForestal(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }

  }

  emitirEvaluacion(){
    this.seleccionarValidacion.emit(this.evaluacionDet.conforme);
  }

}
