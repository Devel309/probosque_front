import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValidarRequisitosPermisoForestalComponent } from './validar-requisitos-permiso-forestal.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { UploadInputButtonsModule} from '../upload-input-buttons/upload-input-buttons.module';
import { InputButtonsModule } from '../input-button/input-buttons.module';


@NgModule({
  declarations: [ValidarRequisitosPermisoForestalComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    UploadInputButtonsModule,
    InputButtonsModule
  ],
  exports:[ValidarRequisitosPermisoForestalComponent]
})
export class ValidarRequisitosPermisoForestalModule { }
