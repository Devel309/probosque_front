import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { AnexosService } from "src/app/service/plan-operativo-concesion-maderable/anexos.service";
import { ToastService } from "../../services/toast.service";

@Component({
  selector: "especies-censadas",
  templateUrl: "./especies-censadas.component.html",
  styleUrls: ["./especies-censadas.component.scss"],
})
export class EspeciesCensadasComponent implements OnInit {
  listAnexo8Especies: any[] = [];
  @Input() idPlanManejo!: number;
  @Input() codigoEpica!: string;
  constructor(
    private toast: ToastService,
    private anexosService: AnexosService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.listAnexo8();
  }

  listAnexo8() {
    this.listAnexo8Especies = [];
    var params = {
      idPlanDeManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .AnexoListaEspeciesInventariadasMaderablesObtener(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length != 0) {
          response.data.forEach((element: any, index: number) => {
            this.listAnexo8Especies.push(element);
          });
        } else {
          this.toast.warn(response.message);
        }
      });
  }
}
