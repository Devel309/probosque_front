import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EspeciesCensadasComponent } from "./especies-censadas.component";
import { TableModule } from "primeng/table";

@NgModule({
  declarations: [EspeciesCensadasComponent],
  imports: [CommonModule, TableModule],
  exports: [EspeciesCensadasComponent],
})
export class EspeciesCensadasModule {}
