import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { ArchivoService, PlanManejoService } from "@services";
import { isNullOrEmpty, ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";
import { DownloadFile } from "../../util";

@Component({
  selector: "app-modal-documentos-digitalizados",
  templateUrl: "./modal-documentos-digitalizados.component.html",
})
export class ModalDocumentosDigitalizadosComponent implements OnInit {
  item: any = null;
  idPlanManejo: number = 0;
  tipoPlan: string = "";
  codigoUnico: string = "";
  listDocumentos: any = [];

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private archivoService: ArchivoService
  ) { }

  ngOnInit(): void {
    

    this.item = this.config.data.item;
    this.idPlanManejo = this.item.idPlanManejo;
    this.tipoPlan = this.item.descripcion;
    this.codigoUnico = this.item.codigoUnico;
    this.listarArchivos();
  }

  listarArchivos() {
    var params = {
      idPlanManejo: this.idPlanManejo,
      extension: 'pdf'
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.anexosService
      .listarArchivosPlanManejo(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        if (response.data.length > 0) {
          response.data.forEach((item: any) => {
            if (item.extension === '.pdf') {
              this.listDocumentos.push(item);
            }
          });
        }
      });
  }

  descargarArchivo(idArchivo: number) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (idArchivo) {
      let params = {
        idArchivo: idArchivo,
      };

      this.archivoService
        .descargarArchivoGeneral(params)
        .subscribe((result: any) => {
          this.dialog.closeAll();
          if (result.data !== null && result.data !== undefined) {
            DownloadFile(
              result.data.archivo,
              result.data.nombeArchivo,
              result.data.contenTypeArchivo
            );
          }
          (error: HttpErrorResponse) => {
            this.errorMensaje(error.message);
            this.dialog.closeAll();
          };
        });
    } else {
      this.errorMensaje('No se ha encontrado un documento vinculado.');
      this.dialog.closeAll();
    }
  }

  errorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

  cerrarModal() {
    this.ref.close();
  }
}
