import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonEvaluarResolucionComponent } from './button-evaluar-resolucion.component';
import { DialogModule } from 'primeng/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputButtonsModule } from 'src/app/shared/components/input-button/input-buttons.module';
import { InputButtonsCodigoModule } from "src/app/shared/components/input-button-codigo/input-buttons.module";
import { RadioButtonModule } from 'primeng/radiobutton';
import { ButtonsDownloadFileModule } from 'src/app/shared/components/button-download-file/buttons-download-file.module';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import { ButtonsFilePermisoForestalModule } from 'src/app/shared/components/buttons-file-permiso-forestal/buttons-file-permiso-forestal.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { UploadInputButtonsModule } from '../upload-input-buttons/upload-input-buttons.module';

@NgModule({
  declarations: [ButtonEvaluarResolucionComponent],
  imports: [
    CommonModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    InputButtonsModule,
    RadioButtonModule,
    ButtonsDownloadFileModule,
    DropdownModule,
    InputTextareaModule,
    ButtonModule,
    ButtonsFilePermisoForestalModule,
    MatDatepickerModule,
    UploadInputButtonsModule,
    InputButtonsCodigoModule
  ],
  exports: [ButtonEvaluarResolucionComponent],
})
export class ButtonEvaluarResolucionModule {}
