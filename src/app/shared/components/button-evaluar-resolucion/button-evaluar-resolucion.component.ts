import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ToastService } from '@shared';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { CodigoEstadoPlanManejo } from 'src/app/model/util/CodigoEstadoPlanManejo';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { Perfiles } from 'src/app/model/util/Perfiles';
import { TipoDocGestion } from 'src/app/model/util/TipoDocGestion';
import { TiposArchivos } from 'src/app/model/util/TiposArchivos';
import { ContratoService } from 'src/app/service/contrato.service';
import { CodigoProceso } from '../../../model/util/CodigoProceso';
import { PermisoForestalService } from '../../../service/permisoForestal.service';
import { DownloadFile } from '../../util';

@Component({
  selector: 'button-evaluar-resolucion',
  templateUrl: './button-evaluar-resolucion.component.html',
  styleUrls: ['./button-evaluar-resolucion.component.scss'],
})
export class ButtonEvaluarResolucionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() tipoPlan!: any;
  @Input() estado!: any;
  @Input() codigoUnico!: any;
  @Input() idSolicitud!: any;

  @Output() registroNotificacionEmit: EventEmitter<number> = new EventEmitter(); //para recargar la pagina principal

  pdfSrc: string = '';
  isShowModal2_2: boolean = false;
  isShowModalFirmeza: boolean = false;

  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  CodigoProceso = CodigoProceso;

  codigoArchivoResolAprobacion: string = '';
  codigoArchivoFormatoPermiso: string = '';

  codigoArchivoResolDesaprobacion: string = '';
  codigoArchivoFirmezaResolucion: string = '';

  idArchivoResolAprobacion!: number;
  idArchivoFormatoPermiso!: number;

  idArchivoResolDesprobacion!: number;
  idArchivoFirmezaResolucion!: number;

  disabled: boolean = false;

  usuario!: UsuarioModel;
  tiposArchivos = TiposArchivos;

  fileResolAprobado: any = {};
  fileResolDesAprobado: any = {};
  fileResolPermiso: any = {};

  isDisabledGuardar: boolean = false;
  idArchivoEmit: number = 0;

  constructor(
    private dialog: MatDialog,
    private usuariServ: UsuarioService,
    private toast: ToastService,
    private permisoForestalService: PermisoForestalService,
    private archivoService: ArchivoService,
    private contratoService: ContratoService
  ) {
    this.usuario = this.usuariServ.usuario;
  }

  ngOnInit(): void {
    if (
      this.usuario.sirperfil === Perfiles.OSINFOR ||
      this.usuario.sirperfil === Perfiles.COMPONENTE_ESTADISTICO
    ) {
      this.isDisabledGuardar = true;
    }
  }

  btnModal1() {
    if (
      this.estado == CodigoEstadoPlanManejo.FAVORABLE ||
      this.estado == CodigoEstadoPlanManejo.DESFAVORABLE
    ) {
      this.isShowModal2_2 = true;
      this.obtenerResolucion();
    }
  }

  obtenerResolucion() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.obtenerResolucion(this.idPlanManejo).subscribe(
      (result) => {
        this.dialog.closeAll();

        if (result.success && result.data.length > 0) {
          const auxData = result.data[0];
          if (auxData?.idResolucion) this.isDisabledGuardar = true;

          auxData.listaResolucionArchivo.forEach((item: any) => {
            this.setIInfoArchivo(item.tipoDocumento, item);
          });
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  btnGuardar(caso: any) {
    let auxArchivos: any[] = [];

    if (caso == CodigoEstadoPlanManejo.FAVORABLE) {
      // if (!this.fileResolAprobado.nombre) {
      if (this.idArchivoEmit == 0) {
        this.toast.warn('Debe adjuntar la Resolución de Aprobación');
        return;
      }
      // auxArchivos = [{ "idArchivo": this.fileResolAprobado.idArchivo }]
    } else if (caso == CodigoEstadoPlanManejo.DESFAVORABLE) {
      // if (!this.fileResolDesAprobado.nombre) {
      if (this.idArchivoEmit == 0) {
        this.toast.warn(
          'Debe adjuntar la Resolución de Desaprobación o en Abandono'
        );
        return;
      }
      // auxArchivos = [{ "idArchivo": this.fileResolDesAprobado.idArchivo }]
    }

    const params = {
      nroDocumentoGestion: this.idPlanManejo,
      tipoDocumentoGestion: TipoDocGestion.PLAN_MANEJO,
      estadoResolucion: null,
      asunto: null,
      idUsuarioRegistro: this.usuariServ.idUsuario,
      listaResolucionArchivo: auxArchivos,
    };

    this.registrarResolucionGuardar(params);
  }

  registrarResolucionGuardar(paramsIn: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.registrarResolucion(paramsIn).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
          this.toast.ok(result.message);

          this.isShowModal2_2 = false;
          this.registroNotificacionEmit.emit(1);
          this.notificarResolucion();
        } else {
          this.toast.warn(result.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  notificarResolucion() {
    const params = {
      idSolicitud: this.idPlanManejo,
      idUsuarioRegistro: this.usuariServ.idUsuario,
    };

    this.dialog.open(LoadingComponent, { disableClose: true });
    this.contratoService.notificarResolucion(params).subscribe(
      (result) => {
        this.dialog.closeAll();
        if (result.success) {
        } else {
          this.toast.warn(result.message);
        }
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  registrarArchivoAdjunto(file: any, codigo: string) {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService
      .cargar(this.usuario.idusuario, codigo, file.file)
      .subscribe(
        (result) => {
          this.dialog.closeAll();
          if (result.success) {
            this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
            this.setIdArchivoRegistrado(codigo, result.data);
          } else {
            this.toast.warn(result.message);
            this.limpiarArchivoFile(codigo);
          }
        },
        (error) => {
          this.limpiarArchivoFile(codigo);
          this.errorMensaje(error, true);
        }
      );
  }

  eliminarArchivoAdjunto(idAdjuntoOut: number, codigo: string) {
    if (!idAdjuntoOut) return;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoService
      .eliminarArchivo(idAdjuntoOut, this.usuario.idusuario)
      .subscribe(
        (result) => {
          this.dialog.closeAll();
          if (result.success) {
            this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
            this.limpiarArchivoFile(codigo);
          } else {
            this.toast.warn(result.message);
          }
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  setIInfoArchivo(codigoIn: string, fileInfo: any) {
    if (codigoIn === TiposArchivos.EOP_APROBADO)
      this.fileResolAprobado = fileInfo;
    else if (codigoIn === TiposArchivos.EOP_DESAPROBADO)
      this.fileResolDesAprobado = fileInfo;
    else if (codigoIn === TiposArchivos.EOP_PERMISO)
      this.fileResolPermiso = fileInfo;
  }

  setIdArchivoRegistrado(codigoIn: string, idArchivoIn: number) {
    if (codigoIn === TiposArchivos.EOP_APROBADO)
      this.fileResolAprobado.idArchivo = idArchivoIn;
    else if (codigoIn === TiposArchivos.EOP_DESAPROBADO)
      this.fileResolDesAprobado.idArchivo = idArchivoIn;
    else if (codigoIn === TiposArchivos.EOP_PERMISO)
      this.fileResolPermiso.idArchivo = idArchivoIn;
  }

  limpiarArchivoFile(codigoIn: string) {
    if (codigoIn === TiposArchivos.EOP_APROBADO) this.fileResolAprobado = {};
    else if (codigoIn === TiposArchivos.EOP_DESAPROBADO)
      this.fileResolDesAprobado = {};
    else if (codigoIn === TiposArchivos.EOP_PERMISO) this.fileResolPermiso = {};
  }

  descargarPlantResolAproba() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.descargarPlantillaResolAprobacion().subscribe(
      (data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  descargarPlantResolDesAproba() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService
      .descargarPlantillaResolDesAprobacion()
      .subscribe(
        (data: any) => {
          this.dialog.closeAll();
          DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
        },
        (error) => this.errorMensaje(error, true)
      );
  }

  descargarPlantResolPermiso() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.permisoForestalService.descargarPlantillaFormatoPermiso().subscribe(
      (data: any) => {
        this.dialog.closeAll();
        DownloadFile(data.archivo, data.nombeArchivo, data.contenTypeArchivo);
      },
      (error) => this.errorMensaje(error, true)
    );
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

  registrarArchivoFirmezaResolucion(eve: any) {
    if (eve > 0) {
      this.idArchivoFirmezaResolucion = eve;
    }
  }

  generarCodigo() {}
}
