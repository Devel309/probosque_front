import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CargaArchivoGeneralComponent } from './carga-archivo-general.component';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { CargaArchivoGeneralCustomComponent } from '../carga-archivo-general-custom/carga-archivo-general-custom.component';


@NgModule({
  declarations: [
    CargaArchivoGeneralComponent,
    CargaArchivoGeneralCustomComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    ConfirmPopupModule
  ],
  exports: [
    CargaArchivoGeneralComponent,
    CargaArchivoGeneralCustomComponent
  ]
})
export class CargaArchivoGeneralModule { }
