import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { DownloadFile, ToastService } from '@shared';
import { ConfirmationService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { Mensajes } from 'src/app/model/util/Mensajes';

@Component({
  selector: 'app-carga-archivo-general',
  templateUrl: './carga-archivo-general.component.html',
  styleUrls: ['./carga-archivo-general.component.scss'],
})
export class CargaArchivoGeneralComponent implements OnInit, OnChanges {
  @ViewChild("fileInput") fileInputVariable: any;

  @Output() registrarArchivo = new EventEmitter();
  @Output() registrarArchivoFile = new EventEmitter();
  @Output() eliminarArchivo = new EventEmitter();

  @Input() label: string = "";
  @Input() accept: string = '';
  @Input() disabled: boolean = false;
  @Input() tipoDoc: string = "";
  @Input() idArchivo: number = 0;
  @Input() showMsjEliminar: boolean = true;
  @Input() disabledDescargar: boolean = false;
  @Input() ocultarNombre: boolean = false;

  isCargaInicial: boolean = true;
  fileOut: { idArchivo: number, nombre: string, file: any, } = { idArchivo: 0, nombre: "", file: null };

  constructor(
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private usuarioServ: UsuarioService,
  ) {

  }

  ngOnInit(): void {
    if (this.idArchivo) this.obtener(this.idArchivo);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.isCargaInicial) {
      if (!changes?.idArchivo?.firstChange) {
        let aux = Number(changes.idArchivo?.currentValue);
        if (aux) this.obtener(aux);
      }
    } else {
      //Para limoiar el file si se le envia id = 0
      if (changes?.idArchivo) {
        if (changes.idArchivo?.currentValue === 0) this.fileOut = { idArchivo: 0, nombre: "", file: null };
      }
    }
  }

  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();

    if (this.validarExtAndTamanio(event.target.files)) {
      this.fileOut.file = event.target.files[0];
      this.fileOut.nombre = event.target.files[0].name;
      this.registrar(this.fileOut);
    }

    this.fileInputVariable.nativeElement.value = "";
    this.isCargaInicial = false;
  }

  obtener(id: any) {
    this.archivoServ.obtenerArchivoGeneral(id).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.idArchivo) {
        this.fileOut.idArchivo = id;
        this.fileOut.nombre = resp.data.nombre;
      } else {
        this.fileOut.idArchivo = 0;
      }
    }, (error) => {
      this.fileOut.idArchivo = 0;
      this.errorMensaje(error)
    });
  }

  registrar(file: any) {
    const codigo = this.tipoDoc;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuarioServ.idUsuario, codigo, file.file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        file.idArchivo = result.data;
        this.registrarArchivo.emit(result.data);
        this.registrarArchivoFile.emit(file);
      } else {
        this.resetFileOut();
        this.toast.warn(result.message);
      }
    }, (error) => {
      this.resetFileOut();
      this.errorMensaje(error, true);
    });
  }

  descargar() {
    if (this.fileOut.idArchivo) {
      let params = { idArchivo: this.fileOut.idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  eliminar() {
    if (this.fileOut.idArchivo) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.eliminarArchivo(this.fileOut.idArchivo, this.usuarioServ.idUsuario).subscribe((resp) => {
        this.dialog.closeAll();
        if (resp.success) {
          if(this.showMsjEliminar) this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
          this.resetFileOut();
          this.eliminarArchivo.emit(true);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  confirmarEliminar(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      key: "cargaDelete",
      accept: () => {
        this.eliminar();
      },
    });
  }

  resetFileOut() {
    this.fileOut = { nombre: "", idArchivo: 0, file: null };
  }

  MB_5 = {desc: "5MB", num: 5242880, exts: [".pdf", ".jpg", ".png", ".xls", ".xlsx", ".doc", ".docx"]};
  MB_15 = {desc: "15MB", num: 15728640, exts: [".zip", ".gdb"]};

  validarExtAndTamanio(files: any): boolean {
    let valido = true;

    if (files && files.length > 0) {
      const auxFile = files[0];
      const auxName: string[] = auxFile.name.split(".");
      const ext = "." + auxName[auxName.length - 1].toLocaleLowerCase();
      if(this.accept) {
        let auxAcep = this.accept.toLocaleLowerCase();
        if (!auxAcep.includes(ext)) {
          valido = false;
          this.toast.warn(`Formato no valido, solo se permite: (${auxAcep})`);
        }
      }

      if(valido) {
        if(this.MB_5.exts.includes(ext) && auxFile.size > this.MB_5.num) {
          valido = false;
          this.toast.warn(`Tamaño máximo para los archivos ( ${ext} ) es de ${this.MB_5.desc}`);
        } else if(this.MB_15.exts.includes(ext) && auxFile.size > this.MB_15.num) {
          valido = false;
          this.toast.warn(`Tamaño máximo para los archivos ( ${ext} ) es de ${this.MB_15.desc}`);
        } else if(auxFile.size > this.MB_5.num) {
          valido = false;
          this.toast.warn(`Tamaño máximo para los archivos ( ${ext} ) es de ${this.MB_5.desc}`);
        }
      }
    } else {
      valido = false;
      this.toast.warn("No se pudo obtener el archivo.");
    }
    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
