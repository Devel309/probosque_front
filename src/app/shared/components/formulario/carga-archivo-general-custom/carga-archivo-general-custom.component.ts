import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArchivoService, UsuarioService } from '@services';
import { ConfirmationService } from 'primeng/api';
import { from } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { SolicitudBosqueLocalGeometriaModel } from 'src/app/model/Solicitud/SolicitudBosqueLocalGeometria';
import { Mensajes } from 'src/app/model/util/Mensajes';
import { SolicitudBosqueLocalGeometriaService } from 'src/app/service/solicitud/solicitud-bosque-local-geometria.service';
import { SolicitudBosqueLocalService } from 'src/app/service/solicitud/solicitud-bosque-local.service';
import { MapApi } from 'src/app/shared/mapApi';
import { ToastService } from 'src/app/shared/services/toast.service';
import { DownloadFile } from 'src/app/shared/util';

@Component({
  selector: 'app-carga-archivo-general-custom',
  templateUrl: './carga-archivo-general-custom.component.html',
  styleUrls: ['./carga-archivo-general-custom.component.scss']
})
export class CargaArchivoGeneralCustomComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private archivoServ: ArchivoService,
    private toast: ToastService,
    private confirmationService: ConfirmationService,
    private usuarioServ: UsuarioService,
    private mapApi: MapApi,
    private serviceSolBosqueLocal: SolicitudBosqueLocalService,
    private serviceSolBosqueLocalGeometria: SolicitudBosqueLocalGeometriaService,
  ) { }

  @ViewChild("fileInput") fileInputVariable: any;

  @Output() registrarArchivo = new EventEmitter();
  @Output() registrarArchivoFile = new EventEmitter();
  @Output() eliminarArchivo = new EventEmitter();

  @Input() label: string = "";
  @Input() accept: string = '';
  @Input() disabled: boolean = false;
  @Input() tipoDoc: string = "";
  @Input() idArchivo: number = 0;
  @Input() showMsjEliminar: boolean = true;
  @Input() disabledDescargar: boolean = false;
  @Input() ocultarNombre: boolean = false;

  @Input() guardarGeometria: boolean = false;
  @Input() idPlanManejo!: number;
  @Input() codigoProceso!: string;
  @Input() codigoSubSeccion!: string;
  @Input() tipoGeometria!: string;
  @Input() tipoArchivoGeneral!: string;
  @Output() listVertices = new EventEmitter<any>();

  isCargaInicial: boolean = true;;
  fileOut: { idArchivo: number, nombre: string, file: any, } = { idArchivo: 0, nombre: "", file: null };
  solBosqueLocalGeometria: SolicitudBosqueLocalGeometriaModel[] = [];

  ngOnInit(): void {
    this.obtenerCapas();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.isCargaInicial) {
      if (!changes?.idArchivo?.firstChange) {
        let aux = Number(changes.idArchivo?.currentValue);
        //if (aux) this.obtener(aux);
      }
    } else {
      //Para limoiar el file si se le envia id = 0
      if (changes?.idArchivo) {
        if (changes.idArchivo?.currentValue === 0) this.fileOut = { idArchivo: 0, nombre: "", file: null };
      }
    }
  }
  onFileChange(event: any) {
    event.preventDefault();
    event.stopPropagation();
    let config = {
      inServer: false,
      service: false,
      validate: false,
      tipo: event.target.dataset.tipo,
      tipoArchivo: event.target.dataset.tipoArchivo
    };
    if (event.target) {
      if (event.target.files.length) {
        this.fileOut.file = event.target.files[0];
        this.fileOut.nombre = event.target.files[0].name;
        let i = 0;
        while (i < event.target.files.length) {
          let controls = this.mapApi.validateFileInputSHP(event.target.files);
          if (controls.success == false) {
            this.toast.warn(controls.message);
            return;
          } else {
            this.processFile(event.target.files[i], config);
          }
          i++;
        }
      }
      this.fileInputVariable.nativeElement.value = "";
      this.isCargaInicial = false;
    }

  }
  processFile(file: any, config: any) {
    config.file = file;
    this.mapApi.processFileSHP(file).then((data: any) => {
      this.createLayers(data, config);
      if (config.tipo === "TPVERTICES") {
        this.obtenerVertices(data[0]);
      }
    });
  }
  createLayers(layers: any, config: any) {
    layers.forEach((t: any) => {
      let title: string = t.title.replace(/(\w+\/)*(\w+)/gi, '$2');
      t.title = title;
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.mapApi.random();
      t.fileName = config.fileName;
      t.service = config.service;
      t.inServer = config.inServer;
      t.idArchivo = config.idArchivo;
      t.idGroupLayer = config.idGroupLayer;
      t.tipo = config.tipo;
      t.crs = {
        type: 'name',
        properties: {
          name: `epsg:4326`,
        },
      };

      this.guardar(config, t);
    });
  }

  guardar(config: any, layer: any) {
    let codigoTipo: any = this.codigoProceso;
    let fileUpload: any = [];
    if (config.file !== null) {
      let item: any = {
        idUsuario: this.usuarioServ.idUsuario,
        codigo: '37',
        codigoTipoPGMF: codigoTipo,
        file: config.file,
        tipoArchivo: config.tipoArchivo,
        layer: layer
      };
      fileUpload.push(item);
    }

    if (fileUpload.length === 0) {
      this.toast.error('Cargue un archivo para continuar.');
      return;
    }
    this.saveLayer(fileUpload);
  }
  saveLayer(fileUpload: any) {
    let observer = from(fileUpload);
    this.dialog.open(LoadingComponent, { disableClose: true });
    observer
      .pipe(concatMap((item: any) => this.saveFile(item)
      ))
      .pipe(finalize(() => {
        this.dialog.closeAll();
        this.obtenerCapas();
      }))
      .subscribe(
        (result: any) => {
          this.toast.ok(result.message);
        },
        (error: HttpErrorResponse) => {
          this.toast.error('Ocurrió un error.');
        }
      );
  }
  saveFile(item: any) {
    return this.archivoServ
      .cargar(item.idUsuario, item.codigo, item.file)
      .pipe(concatMap((result: any) => this.saveFileRelation(result, item)));

  }
  saveFileRelation(result: any, item: any) {
    let item2 = {
      idSolBosqueLocalArchivo: 0,
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
      idArchivo: result.data,
      idSolBosqueLocal: this.idPlanManejo,
      tipoArchivo: item.tipoArchivo,
      idUsuarioRegistro: this.usuarioServ.idUsuario,
      observacion: item.descripcion,
      conforme: true
    };
    return this.serviceSolBosqueLocal.registrarInfoAdjuntoSolicitudBosqueLocal(item2).pipe(
      concatMap((response: any) => {
        return this.guardarCapa(item, result.data);
      })
    );
  }
  guardarCapa(itemFile: any, idArchivo: any) {
    this.solBosqueLocalGeometria = [];
    let attributes: any = [];
    let geometryWKT: any = this.mapApi.getGeometryNotView(
      itemFile.layer.features,
      4326
    );
    itemFile.layer.features.forEach((t3: any) => {
      attributes.push(t3.properties)
    });
    let item: any = {
      idSolBosqueLocalGeometria: 0,
      idSolBosqueLocal: this.idPlanManejo,
      idArchivo: idArchivo,
      tipoGeometria: itemFile.layer.tipo,
      codigoGeometria: 'Puntos',
      codigoSeccion: this.codigoProceso,
      codigoSubSeccion: this.codigoSubSeccion,
      geometry_wkt: geometryWKT,
      srid: 4326,
      properties: JSON.stringify(attributes),
      nombreCapa: itemFile.layer.title,
      colorCapa: itemFile.layer.color,
      idUsuarioRegistro: this.usuarioServ.idUsuario,
    };
    this.solBosqueLocalGeometria.push(item);
    return this.serviceSolBosqueLocalGeometria.registrarGeometria(
      this.solBosqueLocalGeometria
    );
  }
  obtenerCapas() {
    let  item = {
        idSolBosqueLocal: this.idPlanManejo,
        codigoSeccion: this.codigoProceso,
        codigoSubSeccion: this.codigoSubSeccion,
      };
    this.serviceSolBosqueLocalGeometria.listarGeometria(item).subscribe(
      (result: any) => {
        if (result.data.length) {
          result.data.forEach((t: any) => {
            if (t.geometry_wkt !== null) {
              let jsonGeometry: any = this.mapApi.wktParse(t.geometry_wkt);
              let properties = JSON.parse(t.properties);
              this.obtenerVertices(properties);
              this.fileOut.idArchivo = t.idArchivo;
              this.fileOut.nombre = t.nombreCapa;
            }
          });
        }
      },
      (error: any) => {
        let message = null;
        if (error.status === 404) {
          message = 'No se encontró el servicio de listar Geometria.';
        } else {
          message = 'Consulte con el administrador.'
        }
        this.toast.error('Ocurrió un error: ' + message);
      }
    );
  }
  obtenerVertices(data: any) {
    let listVertices: any = [];
    data.forEach((t: any, index: any) => {
      listVertices.push({
        vertice: t.vertice,
        este: t.este,
        norte: t.norte,
      });
    });
    this.listVertices.emit(listVertices);
  }

  obtener(id: any) {
    this.archivoServ.obtenerArchivoGeneral(id).subscribe((resp: any) => {
      this.dialog.closeAll();
      if (resp.success && resp.data && resp.data.idArchivo) {
        this.fileOut.idArchivo = id;
        this.fileOut.nombre = resp.data.nombre;
      } else {
        this.fileOut.idArchivo = 0;
      }
    }, (error) => {
      this.fileOut.idArchivo = 0;
      this.errorMensaje(error)
    });
  }

  registrar(file: any) {
    const codigo = this.tipoDoc;
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.archivoServ.cargar(this.usuarioServ.idUsuario, codigo, file.file).subscribe((result) => {
      this.dialog.closeAll();
      if (result.success) {
        this.toast.ok(Mensajes.MSJ_REGISTRO_ARCHIVO);
        file.idArchivo = result.data;
        this.registrarArchivo.emit(result.data);
        this.registrarArchivoFile.emit(file);
      } else {
        this.resetFileOut();
        this.toast.warn(result.message);
      }
    }, (error) => {
      this.resetFileOut();
      this.errorMensaje(error, true);
    });
  }

  descargar() {
    if (this.fileOut.idArchivo) {
      let params = { idArchivo: this.fileOut.idArchivo };
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.descargarArchivoGeneral(params).subscribe((resp: any) => {
        this.dialog.closeAll();
        if (resp.success && resp.data && resp.data.archivo) {
          DownloadFile(resp.data.archivo, resp.data.nombeArchivo, resp.data.contenTypeArchivo);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  eliminar() {
    if (this.fileOut.idArchivo) {
      this.dialog.open(LoadingComponent, { disableClose: true });
      this.archivoServ.eliminarArchivo(this.fileOut.idArchivo, this.usuarioServ.idUsuario).subscribe((resp) => {
        this.dialog.closeAll();
        if (resp.success) {
          if (this.showMsjEliminar) this.toast.ok(Mensajes.MSJ_ELIMINO_ARCHIVO);
          this.resetFileOut();
          this.eliminarArchivo.emit(true);
        } else {
          this.toast.warn(resp.message);
        }
      }, (error) => this.errorMensaje(error, true));
    }
  }

  confirmarEliminar(event: any) {
    this.confirmationService.confirm({
      target: event.target || undefined,
      message: '¿Está seguro de eliminar el archivo?.',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Sí',
      rejectLabel: 'No',
      key: "cargaDelete",
      accept: () => {
        this.eliminar();
      },
    });
  }

  resetFileOut() {
    this.fileOut = { nombre: "", idArchivo: 0, file: null };
  }

  validarExt(files: any): boolean {
    let valido = false;

    if (this.accept) {
      if (files && files.length > 0) {
        const auxName: string[] = files[0].name.split(".");
        const ext = "." + auxName[auxName.length - 1].toLocaleLowerCase();
        if (this.accept.includes(ext)) {
          valido = true;
        } else {
          this.toast.warn(`Formato no valido, solo se permite: (${this.accept})`);
        }
      } else {
        this.toast.warn("No se pudo obtener el archivo.");
      }
    } else {
      valido = true;
    }

    return valido;
  }

  private errorMensaje(error: any, isLoad: boolean = false) {
    if (isLoad) this.dialog.closeAll();
    let mensajeError = error?.error?.message || Mensajes.MSJ_ERROR_CATCH;
    this.toast.error(mensajeError);
  }

}
