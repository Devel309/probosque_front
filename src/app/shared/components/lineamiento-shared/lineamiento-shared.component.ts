import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PlanManejoService, UsuarioService } from '@services';
import { MessageService } from 'primeng/api';
import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { LineamientoInnerModel } from 'src/app/model/Comun/LineamientoInnerModel';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';
import { PlanManejoEvaluacionService } from 'src/app/service/plan-manejo-evaluacion.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'lineamiento-inner',
  templateUrl: './lineamiento-shared.component.html',
  styleUrls: ['./lineamiento-shared.component.scss']
})
export class LineamientoSharedComponent implements OnInit {

  @ContentChild('descripcion') descripcionRef!: TemplateRef<any>;

  @Input('base') base: LineamientoInnerModel = new LineamientoInnerModel();
  @Input('titulo') titulo: string = "";
  @Input('hideControls') hideControls!: boolean;

  @Input() archivo: any = {};
  @Input() codigoEstado!: string;

  @Input() isShowObjEval!: boolean;
	@Input() isDisabledObjEval!: boolean;
  @Input() isRequiredObsEval!: boolean;

  usuario!: UsuarioModel;

  disabled: boolean = false;

  constructor(private planManejoService: PlanManejoService,
    private planManejoEvaluacionService: PlanManejoEvaluacionService,
    private dialog: MatDialog,
    private messageService: MessageService,
    private usuarioSrv: UsuarioService,
    private toast: ToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.usuario = this.usuarioSrv.usuario;
    this.disabled = this.usuario.sirperfil == "TITULARTH";
  }

  guardarLineamiento() {

    if(this.base.conforme !== true && this.base.conforme !== false) {
      this.toast.warn("(*) Debe de seleccionar Conforme u Observado.");
      return;
    }

    if(this.base.conforme == false && (this.base.observacion == null || this.base.observacion == undefined || this.base.observacion.trim() == "")) {
      this.toast.warn("(*) Debe ingresar la observación.");
      return;
    }

    let idPlanEvalDet = '';
    if (this.base.idPlanManejoEvalDet !== null && this.base.idPlanManejoEvalDet !== undefined) {
      idPlanEvalDet = this.base.idPlanManejoEvalDet;
    }
    let obs = '';
    if (this.base.observacion !== null && this.base.observacion !== undefined) {
      obs = this.base.observacion;
    }
    let idArchivo = '';
    if (this.base.archivo.idArchivo !== null && this.base.archivo.idArchivo !== undefined) {
      idArchivo = this.base.archivo.idArchivo;
    }


    var p = new HttpParams()
      .set('idPlanManejoEvaluacion', this.base.idPlanManejoEvaluacion)
      .set('idPlanManejoEvalDet', idPlanEvalDet)
      .set('conforme', this.base.conforme)
      .set('codigoTipo', this.base.codigoTipo)
      .set('observacion', obs)
      .set('idUsuario', this.usuario.idusuario.toString())
      .set('idTipoDocumento', '')
      .set('idArchivo', idArchivo);


    this.registrarEvaluacion(p, this.base.archivo.file);

  }

  eliminarArchivo(file: any) {

    this.base.archivo = {};
  }

  registrarArchivoFile(auxFile: any) {
    this.base.archivo = auxFile;
  }

  // registrarArchivo(event: any) {
  //   this.base.archivo.idArchivo = undefined;
  // }


  private registrarEvaluacion(params: any, file: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    const formData = new FormData();
    formData.append("file", file);

    this.planManejoEvaluacionService.regitrarPlanManejoEvaluacionArchivo(params, formData).subscribe((result: any) => {
      this.dialog.closeAll();

      if (result.success) {
        this.toast.ok(result.message);

      }
    },
      (error: HttpErrorResponse) => {
        this.toast.error(error.message);
        this.dialog.closeAll();
      }
    )
  }

  private actualizarEvaluacion(params: any) {
    this.dialog.open(LoadingComponent, { disableClose: true });

    this.planManejoEvaluacionService.actualizarPlanManejoEvaluacionDetalle(params).subscribe((result: any) => {
      this.dialog.closeAll();

      if (result.success) {
        this.toast.ok(result.message);

      }
    },
      (error: HttpErrorResponse) => {
        this.toast.error(error.message);
        this.dialog.closeAll();
      }
    )
  }

}
