import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineamientoSharedComponent } from 'src/app/shared/components/lineamiento-shared/lineamiento-shared.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FieldsetModule} from 'primeng/fieldset';
import { RadioButtonModule } from 'primeng/radiobutton';
import { UploadInputButtonsModule } from 'src/app/shared/components/upload-input-buttons/upload-input-buttons.module';
import { CargaArchivoGeneralModule } from '../formulario/carga-archivo-general/carga-archivo-general.module';


@NgModule({
  declarations: [LineamientoSharedComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UploadInputButtonsModule,
    //primeng
    FieldsetModule,
    RadioButtonModule,
    CargaArchivoGeneralModule
  ],
  exports:[LineamientoSharedComponent]
})
export class LineamientoSharedModule { }
