import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SolicitarOpicionEvaluacionComponent } from './solicitar-opicion-evaluacion.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadInputButtonsModule } from '../upload-input-buttons/upload-input-buttons.module';
import { AccordionModule } from 'primeng/accordion';
import { InputButtonsModule } from '../input-button/input-buttons.module';
import { InputButtonsCodigoModule } from '../input-button-codigo/input-buttons.module';


@NgModule({
  declarations: [
    SolicitarOpicionEvaluacionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UploadInputButtonsModule, 
    DropdownModule,
    MatDatepickerModule,
    PanelModule,
    AccordionModule,
    InputButtonsModule,
    InputButtonsCodigoModule
  ],
  exports:[
    SolicitarOpicionEvaluacionComponent
  ]
})
export class SolicitarOpicionEvaluacionModule { }
