import {
  Component,
  ContentChild,
  Input,
  OnInit,
  Output,
  TemplateRef,
  EventEmitter,
} from "@angular/core";
import * as moment from "moment";
import { MessageService } from "primeng/api";
import { OpinionModel } from "src/app/model/opinionModel";

@Component({
  selector: "app-solicitar-opicion-evaluacion",
  templateUrl: "./solicitar-opicion-evaluacion.component.html",
  styleUrls: ["./solicitar-opicion-evaluacion.component.scss"],
})
export class SolicitarOpicionEvaluacionComponent implements OnInit {
  @Input() idPlanManejo!: number;
  @Input() disabled!: boolean;

  @ContentChild("containerExtra") contenidoExtra!: TemplateRef<any>;

  @Input() codigoEpica!: string;

  @Input("opinion") opinion: string = "";

  @Input() listEntidades: any;
  @Input() lisTiposArchivo: any;
  @Input("titulo") titulo: string = "";

  @Output() changeObj = new EventEmitter();

  @Input() set listaSERNANP(data: OpinionModel) {
    if (!!data) {
      this.opinionObj = data;
      this.fechaDocumento = data.fechaDocumento;
    }
  }
  @Input() set listaANA(data: OpinionModel) {
    if (!!data) {
      this.opinionObj = data;
      this.fechaDocumento = data.fechaDocumento;
    }
  }
  @Input() set listaMINCUL(data: OpinionModel) {
    if (!!data) {
      this.opinionObj = data;
      this.fechaDocumento = data.fechaDocumento;
    }
  }
  @Input() set listaOTRA(data: OpinionModel) {
    if (!!data) {
      this.opinionObj = data;
      this.fechaDocumento = data.fechaDocumento;
    }
  }

  opinionObj: OpinionModel = {};

  idTipoDocumento: any;

  fechaDocumento: any;

  minDate = moment(new Date()).format("YYYY-MM-DD");

  IdSERNANP: number = 0;
  IdANA: number = 0;
  IdMINCUL: number = 0;

  codigoSERNANP: string = "";
  codigoANA: string = "";
  codigoMINCUL: string = "";

  idArchivo: number = 0;

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {}

  solicitarOpinion(codigo: string) {
    this.idArchivo = 0;
    if (!this.validarMedidas()) {
      return;
    }

    if (codigo == "SERNANP") {
      this.idArchivo = this.IdSERNANP;
    } else if (codigo == "ANA") {
      this.idArchivo = this.IdANA;
    } else if (codigo == "MINCUL") {
      this.idArchivo = this.IdMINCUL;
    }
    this.opinionObj.fechaDocumento = new Date(this.fechaDocumento);
    var params = {
      opinionObj: this.opinionObj,
      subCodigo: codigo,
      idArchivo: this.idArchivo
    };

    this.changeObj.emit(params);
  }

  validarMedidas(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";
    let fechaActual = moment(new Date())
      .subtract(1, "days")
      .format("YYYY-MM-DD");
    let fechaInicio = new Date(this.fechaDocumento);
    let fecha = moment(fechaInicio).format("YYYY-MM-DD");

    if (this.opinion == "OTRA") {
      if (this.opinionObj.entidad == null || this.opinionObj.entidad == "") {
        validar = false;
        mensaje = mensaje += "(*) Debe ingresar: Entidad.\n";
      }
    }
    if (
      this.opinionObj.tipoDocumento == null ||
      this.opinionObj.tipoDocumento == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un Tipo de documento.\n";
    }
    if (
      this.opinionObj.numDocumento == null ||
      this.opinionObj.numDocumento == ""
    ) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Número de documento.\n";
    }
    if (this.fechaDocumento == null) {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Fecha de Documento.\n";
    }
    if (this.opinionObj.asunto == null || this.opinionObj.asunto == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe ingresar: Asunto.\n";
    }
    if (fecha <= fechaActual) {
      validar = false;
      mensaje = mensaje +=
        "(*)La Fecha de Documento debe ser posterior a la Fecha Actual.\n";
    }
    if (!validar) this.ErrorMensaje(mensaje);

    return validar;
  }
  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
  registrarArchivoId(event: any, tipo: string) {
    if (tipo == "SERNANP") {
      this.IdSERNANP = event;
      this.codigoSERNANP = tipo;
    } else if (tipo == "ANA") {
      this.IdANA = event;
      this.codigoANA = tipo;
    } else if (tipo == "MINCUL") {
      this.IdMINCUL = event;
      this.codigoMINCUL = tipo;
    }
  }
}
