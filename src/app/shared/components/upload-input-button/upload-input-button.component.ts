import { Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'upload-input-button',
	templateUrl: './upload-input-button.component.html',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => UploadInputButtonComponent),
			multi: true
		}
	]
})
export class UploadInputButtonComponent implements ControlValueAccessor {

	@Input() labelButton: string = 'Seleccionar Archivo';
	@Input() deleteButton: boolean = false;
	@Input() deleteIcon: string = 'trash';
	@Input() placeHolder: string = '';
	@Input() accept: string = '';
	@Output() clear = new EventEmitter();
	@Input() disabled: boolean = false;

	@ViewChild('fileInput') el!: ElementRef<HTMLInputElement>;

	constructor() { }

	onChange: any = () => { }
	onTouch: any = () => { }

	fileName: string | null | undefined = '';
	file: File | null | string = null;


	set value(file: File | null | string) {
		if (file !== undefined && this.file !== file) {
			this.file = file;
			this.onChange(file);
			this.onTouch(file);
		} else {
			this.file = null;
			this.onChange(file);
			this.onTouch(file);
		}
	}

	changeFile(e: Event) {
		const target = e.target as HTMLInputElement;
		const file = target.files?.item(0);
		this.fileName = file?.name ? file?.name : '';
		this.onChange(file);
		this.el.nativeElement.value = "";
	}

	delete() {
		this.fileName = null;
		this.value = null;
		this.onChange(null);
		this.clear.emit();
	}

	writeValue(value: File | any) {

		if (this.el?.nativeElement) {
			if (value instanceof FileList) {
				this.el.nativeElement.files = value;
			} else if (value instanceof File) {
				this.fileName = value.name;
				this.el.nativeElement.files = this.fileListItems([value]);
			} else if (typeof value === 'string' || value instanceof String) {
				this.el.nativeElement.files = null;
				this.fileName = value.toString();
			} else if (value === null) {
				this.el.nativeElement.files = null;
			}
		}
	}

	fileListItems(files: File[]) {
		const b = new ClipboardEvent("").clipboardData || new DataTransfer();
		for (var i = 0, len = files.length; i < len; i++) b.items.add(files[i])
		return b.files
	}

	registerOnChange(fn: any) {
		this.onChange = fn
	}

	registerOnTouched(fn: any) {
		this.onTouch = fn
	}

}
