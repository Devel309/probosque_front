import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonTypeCodigoComponent } from './input-button-type-codigo.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonTypeCodigoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonTypeCodigoComponent],
})
export class InputButtonTypeCodigoModule {}
