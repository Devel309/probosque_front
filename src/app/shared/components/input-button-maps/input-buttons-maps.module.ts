import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonsMapsCodigoComponent } from './input-buttons-maps.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonsMapsCodigoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonsMapsCodigoComponent],
})
export class InputButtonsMapsCodigoModule {}
