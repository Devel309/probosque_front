import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonDocCodigoComponent } from './input-button-doc-codigo.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonDocCodigoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonDocCodigoComponent],
})
export class InputButtonDocCodigoModule {}
