import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { defer, forkJoin, Observable, of, throwError } from "rxjs";
import { map, mergeMap, tap } from "rxjs/operators";

import MapView from "@arcgis/core/views/MapView";
import Layer from "@arcgis/core/layers/Layer";

import { ConfirmationService } from "primeng/api";

import { Base64toBlob, isNull } from "../../util";
import { ToastService } from "../../services/toast.service";
import { MapService } from "../../services/map.service";
import { ZipService } from "../../services/zip.service";
import { CustomLayerView, LayerView } from "../../models/layer-view";
import { GroupFile } from "../../models/group-file";
import { ZipEntry } from "../../models/zip";
import { Feature, FeatureCollection } from "../../models/feature-collection";
@Component({
  selector: "app-mapa",
  templateUrl: "./app-mapa.component.html",
  styleUrls: ['./app-mapa.component.scss']
})
export class AppMapaComponent implements OnInit, OnDestroy {

  @Output() changeLayer = new EventEmitter<LayerView>();
  @Output() isEdited = new EventEmitter<boolean>();
  @Output() deleteLayer = new EventEmitter();
  @Output() deleteAll = new EventEmitter();
  @Output() loadFile = new EventEmitter<LayerView[] | CustomLayerView<any>[]>();

  @Input() disabled!: boolean;


  @ViewChild("map", { static: true }) private mapViewEl!: ElementRef;
  view!: MapView;
  mapId = this.mapService.newGuid;

  files: GroupFile[] = [];
  layersView: LayerView[] = [];


  /**
   * Propiedad para ver si el mapa esta vacío o no(con o sin capas)
   */
  get isEmpty(): boolean {
    return this.layersView.length == 0;
  }

  constructor(
    private toast: ToastService,
    private zip: ZipService,
    private mapService: MapService,
    private confirm: ConfirmationService,
  ) { }

  ngOnInit(): void {
    this.initializeMap().then(() => console.log('loaded map'));
  }

  ngOnDestroy(): void {
    if (this.view) {
      this.view.destroy();
    }
  }

  initializeMap(): Promise<any> {
    const container = this.mapViewEl.nativeElement;
    container.style.height = "340px";
    this.view = this.mapService.initializeMap(container);
    return this.view.when();
  }

  /**
   * Sirve para cargar un shapefile
   * @param e Event
   * @param groupId Id de grupo para las capas del archivo
   * @param color color del grupo de capas
   */
  addFile(e: any, groupId?: string | number, color?: string) {
    const files = e?.target?.files as FileList
    if (files && files.length > 0) {
      const _groupId = groupId ? groupId : this.mapService.newGuid;
      const file = files[0];

      file.arrayBuffer().then(buffer => {
        const features = this.mapService.getDataShp(buffer);
        if (this.validLayerExistAndShowToast(features)) return;
        this.zip.getEntries(file)
          .subscribe(entries => this.files.push({ groupId: _groupId, file, entries }));
        forkJoin(this.addLayer(features, _groupId, color))
          .subscribe(layers => {
            layers.forEach(item => this.layersView.push(item));
            this.loadFile.emit(layers);
          });
      })
    } else {
      this.toast.warn('Archivo no válido');
    }
  }

  addBlob(blob: Blob, groupId?: string | number, color?: string) {
    const _groupId = groupId ? groupId : this.mapService.newGuid;

    blob.arrayBuffer().then(buffer => {
      const features = this.mapService.getDataShp(buffer);

      if (this.validLayerExistAndShowToast(features)) return;

      this.zip.getEntries(blob).subscribe(entries => this.files.push({ groupId: _groupId, file: blob, entries }));

      forkJoin(this.addLayer(features, _groupId, color))
        .subscribe(layers => layers.forEach(item => this.layersView.push(item)));
    })
  }

  /**
   * Sirve para cargar un shapefile desde un archivo base 64
   * @param base64 archivo shapefile en base54
   * @param list lista de capas asociadas al archivo
   */
  addBase64FileWithConfig(base64: string, list: LayerView[]) {
    const blob = Base64toBlob(base64, 'application/octet-stream');

    blob.arrayBuffer().then(buffer => {
      const features = this.mapService.getDataShp(buffer);

      if (this.validLayerExistAndShowToast(features)) return;

      this.syncBlobAndFeatures(blob, features).subscribe(res => {
        let entries = res.entries;

        const layers = this.mapService.getAllLayers(null, this.view);

        for (const layer of layers) {
          const item = list.find(x => x.title == layer.title) || null;

          entries.filter(e => e.filename.split('.')[0] == layer.title)
            .forEach(entry => {
              entry.groupId = String(item?.groupId)
              entry.title = layer.title
            });

          if (item != null) {
            layer.id = String(item.layerId);
            layer.set('ID', item.layerId);
            layer.set('layerName', item.title);
            layer.set('groupId', item.groupId);
            layer.set('color', item.color);
            layer.title = String(item.title);
            this.mapService.setLayerColor(layer, item.color);
            item.title = this.detachTitle(item.title);
            item.area = layer.get('areaHa');
            this.layersView.push(item);
          } else {
            const layerView: LayerView = {
              area: layer.get('areaHa'),
              groupId: layer.get('groupId'),
              color: layer.get('color'),
              layerId: layer.id,
              title: this.detachTitle(layer.title),
              features: layer.get('attributes')
            }
            this.layersView.push(layerView);
          }
        }
        this.files.push({ groupId: null, file: blob, entries });
      })
    })
  }

  /**
 * Carga un shapefile desde un archivo base 64
 * @param base64 archivo shapefile en base54
 */
  addBase64File(base64: string) {
    const blob = Base64toBlob(base64, 'application/octet-stream');
    return this.mapService.fileRead(blob).pipe(mergeMap(buffer => {
      const features = this.mapService.getDataShp(buffer);
      if (this.validLayerExistAndShowToast(features)) return throwError('La capa ya existe');

      this.zip.getEntries(blob).subscribe(entries => this.files.push({ groupId: null, file: blob, entries }));

      return forkJoin(this.addLayer(features, null))
        .pipe(tap(layers => layers.forEach(item => this.layersView.push(item))))
    })
    )
  }

  syncBlobAndFeatures(blob: Blob, features: FeatureCollection[]) {
    return forkJoin({
      entries: this.zip.getEntries(blob),
      layers: forkJoin(this.addLayer(features, null))
    })
  }

  /**
   * Carga un shapefile, devuelve un observable con las capas.
   * @param e Evento input file
   * @param groupId id de grupo para las capas del archivo
   * @param color color del grupo de capas
   * @returns
   */
  addLayerFile(e: any, groupId?: string | number, color?: string): Observable<LayerView[]> {
    const files = e?.target?.files as FileList
    if (files && files.length > 0) {
      const _groupId = groupId ? groupId : this.mapService.newGuid;
      const file = files[0];

      return this.mapService.fileRead(file).pipe(mergeMap(buffer => {
        const features = this.mapService.getDataShp(buffer);

        if (this.validLayerExistAndShowToast(features)) return throwError('La capa ya existe');

        this.zip.getEntries(file).subscribe(entries => this.files.push({ groupId: _groupId, file, entries }));

        return forkJoin(this.addLayer(features, _groupId, color))
          .pipe(tap(layers => layers.forEach(item => this.layersView.push(item))))
      })
      )
    }

    this.toast.warn('Archivo no válido');
    return throwError('Archivo no válido');
  }

  addLayer(features: FeatureCollection[], groupId: string | number | null, color?: string): Observable<LayerView>[] {

    const layersSubs: Observable<LayerView>[] = [];
    features.forEach(feature => {
      const layerSubs = this.mapService.addLayer(this.view, feature, groupId, color)
        .pipe(map(layer => {
          const item: LayerView = {
            groupId,
            layerId: layer.id,
            title: this.detachTitle(layer.title),
            color: layer.get('color'),
            area: Number(layer.get('areaHa')),
            features: layer.get('attributes')
          }
          this.changeLayer.emit(item);
          return item;
        }));
      layersSubs.push(layerSubs);
    })
    return layersSubs;
  }

  getLayer(id: string): Layer {
    return this.mapService.getLayer(id, this.view);
  }
  getLayerByTitle(title: string): Layer {
    return this.mapService.getLayerByTitle(title, this.view);
  }


  toggleView(id: string | null, e: any) {
    if (id != null) {
      this.mapService.toggleLayer(id, e.checked, this.view);
    }
  }

  download(item: LayerView) {
    const file = this.files.find(f => f.groupId == item.groupId) || null;
    const title = this.joinTitle(item.title);
    let entries: ZipEntry[] = [];
    if (isNull(file)) {
      const files = this.files.filter(f => f.groupId == null);
      files.forEach(f => {
        const lista = f?.entries.filter(e => e.title == this.joinTitle(item.title));
        entries = [...entries, ...lista]
      })
    } else {
      entries = file?.entries.filter(e => e.filename.split('.')[0] == title) || [];
    }
    if (entries.length == 0) {
      this.toast.warn('No existe la capa a descargar.');
      return;
    }
    this.zip.downloadZipFile(title, entries);
  }

  downloadGroup(groupId: string, fileTitle: string) {
    const file = this.files.find(f => f.groupId == groupId) || null;

    let groupEntries: ZipEntry[] = [];
    if (isNull(file)) {
      const files = this.files.filter(f => f.groupId == null);
      files.forEach(f => {
        const lista = f?.entries.filter(e => e.groupId == groupId);
        groupEntries = [...groupEntries, ...lista]
      });
    } else {
      const layers = this.layersView.filter(l => l.groupId = groupId);
      for (const layer of layers) {
        const title = this.joinTitle(layer.title);
        const entries = file?.entries.filter(e => e.filename.split('.')[0] == title) || [];
        groupEntries = [...groupEntries, ...entries];
      }
    }

    if (groupEntries.length == 0) {
      this.toast.warn('No existe el grupo de capas a descargar.');
      return;
    }
    this.zip.downloadZipFile(fileTitle, groupEntries);
  }


  deleteUnion(index: number, item: LayerView) {

    this.delete(index, item);

    this.isEdited.emit();
  }

  delete(index: number, item: LayerView) {

    const file = this.files.find(f => f.groupId == item.groupId) || null;
    const title = this.joinTitle(item.title);
    if (file != null) {
      const entries = file?.entries.filter(e => e.filename.split('.')[0] != title);
      file.entries = entries;
    } else {
      const files = this.files.filter(f => f.groupId == null);
      files.forEach(f => {
        const entries = f?.entries.filter(e => e.filename.split('.')[0] != title);
        f.entries = entries;
      });
    }
    if (item.layerId != null) {
      this.layersView.splice(index, 1);
      this.mapService.removeLayer(item.layerId, this.view);
      this.deleteLayer.emit(item);
    }
  }

  deleteGroupLayer(groupId: string) {

    const file = this.files.find(f => f.groupId == groupId) || null;
    if (file != null) {
      const entries = file?.entries.filter(e => e.groupId != groupId);
      file.entries = entries;
    } else {
      const files = this.files.filter(f => f.groupId == null);
      files.forEach(f => {
        const entries = f?.entries.filter(e => e.groupId != groupId);
        f.entries = entries;
      });
    }
    const items = this.layersView.filter(l => l.groupId == groupId);
    items.forEach(item => {
      if (item.layerId != null) {
        this.mapService.removeLayer(item.layerId, this.view);
        this.deleteLayer.emit(item);
      }
    })
    this.layersView = this.layersView.filter(l => l.groupId != groupId);
  }

  clearUnion(event: any) {
    this.clear(event);
    this.isEdited.emit();
  }

  clear(event: any) {
    this.confirm.confirm({
      target: event.target || undefined,
      message: "¿Está seguro de todas las capas del mapa?",
      icon: "pi pi-exclamation-triangle",
      acceptLabel: "Si",
      rejectLabel: "No",
      accept: () => {
        this.files = [];
        this.layersView = [];
        this.view.map.removeAll();
        this.deleteAll.emit();
      }
    });
  }

  getZipFile(): Observable<Blob | File | null> {
    let entries: ZipEntry[] = [];
    this.files.forEach(file => entries = [...entries, ...file.entries])

    if (entries.length == 0) return of(null);

    return defer(() => this.zip.createZipFile(entries))
  }

  genColor(): string {
    return this.mapService.randomHex();
  }
  genId(): string {
    return this.mapService.newGuid;
  }

  validLayerExistAndShowToast(features: FeatureCollection[]): boolean {
    for (const feature of features) {
      const index = this.layersView.findIndex(l => l.title == this.detachTitle(feature.fileName));
      if (index >= 0) {
        this.toast.warn(`Ya existe la capa: ${feature.fileName}`);
        return true;
      }
    }
    return false;
  }

  joinTitle(title: string) {
    return title.replace(/ /g, '_');
  }
  detachTitle(title: string) {
    return title.replace(/_/g, ' ');
  }

  calculateAreaHaPoints(features: Feature[]) {
    return this.mapService.calculateAreaPoints(features);
  }

  calculateAreaHaPolygon(feature: Feature) {
    return this.mapService.calculateAreaPolygon(feature);
  }

}




