import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluacionArchivoPermisoForestalComponent } from './evaluacion-archivo-permiso-forestal.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { UploadInputButtonsModule} from '../upload-input-buttons/upload-input-buttons.module';
import { InputButtonsModule } from '../input-button/input-buttons.module';
import {ButtonsFilePermisoForestalModule} from '../buttons-file-permiso-forestal/buttons-file-permiso-forestal.module';
import {ToastModule} from 'primeng/toast';


@NgModule({
  declarations: [EvaluacionArchivoPermisoForestalComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RadioButtonModule,
    UploadInputButtonsModule,
    InputButtonsModule,
    ButtonsFilePermisoForestalModule,
    ToastModule
  ],
  exports:[EvaluacionArchivoPermisoForestalComponent]
})
export class EvaluacionArchivoPermisoForestalModule { }
