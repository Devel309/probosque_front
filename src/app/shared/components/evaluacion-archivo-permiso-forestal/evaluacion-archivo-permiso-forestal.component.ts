import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import {CodigoEstadoEvaluacion} from '../../../model/util/CodigoEstadoEvaluacion';
import {EvaluacionArchivoModel} from '../../../model/Comun/EvaluacionArchivoModel';
import {EvaluacionUtils} from '../../../model/util/EvaluacionUtils';
import {LoadingComponent} from '../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {Mensajes} from '../../../model/util/Mensajes';
import {EvaluacionService} from '../../../service/evaluacion/evaluacion.service';
import {MatDialog} from '@angular/material/dialog';
import {ToastService} from '@shared';
import {EvaluacionPermisoForestalModel} from '../../../model/Comun/EvaluacionPermisoForestalModel';
import {UsuarioService} from '@services';
import {CodigoProceso} from '../../../model/util/CodigoProceso';
import {CodigoPermisoForestal} from '../../../model/util/CodigoPermisoForestal';

@Component({
  selector: 'evaluacion-archivo-permiso-forestal-shared',
  templateUrl: './evaluacion-archivo-permiso-forestal.component.html',
  styleUrls: ['./evaluacion-archivo-permiso-forestal.component.scss']
})
export class EvaluacionArchivoPermisoForestalComponent implements OnInit {

  @Input() idPermisoForestal!: number;
  @Input() codigoProceso!: string;
  @Input() codigoTab!: string;
  @Input() codigoAcordeon!: string;
  @Input() tipoEvaluacion!: string;
  @Input() descripcion! :string;
  @Input() detalle! :string;
  @Input() disabled! :boolean;


  //codigoObservado: string = CodigoEstadoEvaluacion.OBSERVADO;
  //codigoConforme: string = CodigoEstadoEvaluacion.RECIBIDO;
  codigoObservado: string = CodigoPermisoForestal.ESTADO_EVAL_DESFAVORABLE;
  codigoConforme: string = CodigoPermisoForestal.ESTADO_EVAL_FAVORABLE;

  evaluacion:any;

  evaluacionDet!: EvaluacionPermisoForestalModel;

  constructor(
    private evaluacionService: EvaluacionService,
              private dialog: MatDialog,
              private toast: ToastService,
              private user: UsuarioService,) {
    this.evaluacion = {
      codigoEvaluacion: CodigoProceso.SOLICITUD_PERMISOS_FORESTALES,
      idEvaluacionPermiso: 0,
      estadoEvaluacion: 'EEVAPRES',
      tipoEvaluacion:CodigoPermisoForestal.TIPO_EVALUACION,
      fechaEvaluacionInicial: new Date().toISOString(),
      idPermisoForestal: this.idPermisoForestal,
      idUsuarioRegistro: this.user.idUsuario,
      listarEvaluacionPermisoDetalle: [],
    };


  }

  ngOnInit(): void {

    this.evaluacionDet = new EvaluacionPermisoForestalModel({
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab,
      codigoEvaluacionDetPost : this.codigoAcordeon,
      descripcion: this.descripcion,
      detalle:this.detalle
    });

    this.obtenerEvaluacion();

  }

  cargarIdArchivo(idArchivo: any) {
    
    this.evaluacionDet.idArchivo = idArchivo;
  }

  eliminarArchivo(idArchivo: any) {
    
    //this.evaluacionArchivoModel.idArchivo = idArchivo;
  }

  obtenerEvaluacion() {

    let params = {
      idPermisoForestal: this.idPermisoForestal,
      codigoEvaluacionDet : this.codigoProceso,
      codigoEvaluacionDetSub : this.codigoTab,
      tipoEvaluacion : CodigoPermisoForestal.TIPO_EVALUACION
    }

    this.evaluacionService.obtenerEvaluacionPermisoForestal(params).subscribe((result: any) => {
      if(result.data) {
        if(result.data.length>0) {
          this.evaluacion = result.data[0];
          if(this.evaluacion) {
            this.evaluacionDet = Object.assign(this.evaluacionDet,this.evaluacion.listarEvaluacionPermisoDetalle.find((x: any) =>
              x.codigoEvaluacionDetPost == this.codigoAcordeon
              //&& x.tipoEvaluacion == CodigoPermisoForestal.TIPO_EVALUACION
            ));
          }
        }
      }
    })
  }

  registrarEvaluacion() {
    if(EvaluacionUtils.validarPermisoForestalEvaluacion([this.evaluacionDet])){
      if(this.evaluacion) {
        this.evaluacion.idPermisoForestal = this.idPermisoForestal;
        this.evaluacion.listarEvaluacionPermisoDetalle = [];
        this.evaluacion.listarEvaluacionPermisoDetalle.push(this.evaluacionDet);
        this.dialog.open(LoadingComponent, { disableClose: true });

        this.evaluacionService.registrarEvaluacionPermisoForestal(this.evaluacion)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe((res:any) => {
            this.toast.ok(res.message);
            this.obtenerEvaluacion();
          })
      }
    } else {
      this.toast.warn(Mensajes.MSJ_EVALUACIONES)
    }

  }


}
