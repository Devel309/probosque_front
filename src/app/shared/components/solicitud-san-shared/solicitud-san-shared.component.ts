import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoadingComponent} from '../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {ArchivoService, UsuarioService} from '@services';
import {MatDialog} from '@angular/material/dialog';
import {AmpliacionSolicitudService} from '../../../service/ampliacion-solicitud/ampliacion-solicitud.service';
import {ImpugnacionSan} from '../../../model/Impugnacion/ImpugnacionSan';
import {ToastService} from '@shared';
import {ImpugnacionSanService} from '../../../service/planificacion/permiso-forestal/impugnacionSan.service';
import {CodigosTiposSolicitud} from '../../../model/util/CodigosTiposSolicitud';
import {MessageService} from 'primeng/api';
import {CodigoEstadoMesaPartes, CodigoEstadoPlanManejo} from '../../../model/util/CodigoEstadoPlanManejo';

@Component({
  selector: 'solicitud-san-shared',
  templateUrl: './solicitud-san-shared.component.html',
  styleUrls: ['./solicitud-san-shared.component.scss']
})
export class SolicitudSanSharedComponent implements OnInit {

  @Input() idPlanManejo!: number;
  @Input() codigoPlan!: string;
  @Input() disabled!: boolean;
  @Input() estadoPlanManejo!: string;
  @Output() registroImpugnacionEmiter: EventEmitter<number> = new EventEmitter();

  isShowModal2_2: boolean = false;

  idTipoSolicitud: number = 0;
  idProcesoPostulacion: number = 0;
  autoResize: boolean = true;
  idArchivo: number = 0;
  showFormatoSan: boolean = false;

  isLoadFile: boolean = false;
  CodigosTiposSolicitud = CodigosTiposSolicitud;

  //codigoProceso: string = this.codigoPlan;//CodigoProceso.SOLICITUD_PERMISOS_FORESTALES;
  codigoArchivoSan: string = "";//'PFCRIMP'; DEMAIMP

  impugnacionSan: ImpugnacionSan = new ImpugnacionSan({
    idSolicitudSAN: 0,
    //idPermisoForestal: this.idPlanManejo,
    nroGestion: this.idPlanManejo,
    codSolicitudSAN: this.codigoPlan, //PFCR    DEMA    PMFI    PMFIC
    subCodSolicitudSAN: CodigosTiposSolicitud.IMPUGNACION_SAN,
    tipoSolicitud: 'IMPUGNACIÓN AL SAN',
    tipoPostulacion : "PLAN",//  "PERMISO"
    idUsuarioRegistro: this.usuariServ.idUsuario,
    diasAdicionales :0

  });

  CodigoEstadoPlanManejo = CodigoEstadoPlanManejo;
  CodigoEstadoMesaPartes = CodigoEstadoMesaPartes;

  tipoSolicitud: any[] = [];

  codigoArchivoEvidencia: string = "";

  idArchivoEvidencia: number = 0;

  flagBloqueoDias :boolean = false;

  constructor(
    private archivoService: ArchivoService,
    private dialog: MatDialog,
    private ampliacionSolicitudService: AmpliacionSolicitudService,
    private usuariServ: UsuarioService,
    private impugnacionSanService: ImpugnacionSanService,
    private toast: ToastService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.codigoArchivoSan = this.codigoPlan + "SAN";
    this.codigoArchivoEvidencia = this.codigoPlan + "EVID";
    //this.listarImpugnacion();
  }

  verPDF() {
    if(this.estadoPlanManejo ==  CodigoEstadoMesaPartes.PRESENTADOMDP){
      this.isShowModal2_2 = true;
      this.showFormatoSan = true;
      //this.listarTipoSolicitud();
      this.listarImpugnacion();
    }

  }

  editarCalendario() {
    if(!this.validacionDiasCalendario()) return;
    let params: any[] = [];
    this.impugnacionSan.nroGestion = this.idPlanManejo;
    this.impugnacionSan.idArchivo = this.idArchivo;
    this.impugnacionSan.codSolicitudSAN= this.codigoPlan;

    params.push(this.impugnacionSan);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionSanService
      .registrarSolicitudSAN(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok('Se actualizó los días adicionales a la solicitud');
          this.listarImpugnacion();
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  cargarEvidencia(idArchivo: number) {
    if (idArchivo !=null && idArchivo > 0) {
      this.idArchivoEvidencia = idArchivo;

    }
  }

  cargarIdArchivo(idArchivo: number) {
    if (idArchivo !=null && idArchivo > 0) {
      this.idArchivo = idArchivo;
      this.isLoadFile = true;
    }
  }

  eliminarArchivo(idArchivo: any) {
    this.isLoadFile = false;
  }

  listarTipoSolicitud() {
    this.ampliacionSolicitudService
      .listarTipoSolicitud()
      .subscribe((resp: any) => {
        this.tipoSolicitud = resp.data;
      });
  }

  /*verPDF() {
    console.log(this.estado);
    if (this.estado == CodigoPermisoForestal.ESTADO_PRESENTADO) {
      this.isShowModal2_2 = true;
      this.showFormatoSan = true;
      this.listarImpugnacion();
    }
  }*/

  listarImpugnacion() {
    let param = {
      //idPermisoForestal: this.idPlanManejo,
      nroGestion:this.idPlanManejo,
      pageNum: 1,
      pageSize: 1,
    };

    this.impugnacionSanService
      .listarSolicitudSAN(param)
      .subscribe((res: any) => {
        if (res.success == true) {
          if (res.data) {
            if (res.data.length > 0) {
              let response = res.data[0];
              this.impugnacionSan.asunto = response.asunto;
              this.impugnacionSan.descripcion = response.descripcion;
              this.impugnacionSan.codSolicitudSAN = response.codSolicitudSAN;
              this.impugnacionSan.subCodSolicitudSAN = response.subCodSolicitudSAN,
              //this.impugnacionSan.idPermisoForestal = response.idPermisoForestal;
              this.impugnacionSan.nroGestion = response.nroGestion;
              this.impugnacionSan.idSolicitudSAN = response.idSolicitudSAN;
              this.impugnacionSan.procesoPostulacion = response.procesoPostulacion;
              this.impugnacionSan.diasRestantesPlan = response.diasRestantesPlan;
              if(res.diasAdicionales != null){
                this.impugnacionSan.diasAdicionales = response.diasAdicionales;
              }

              if( this.impugnacionSan.diasRestantesPlan !=null ) {

                this.flagBloqueoDias = true;
                if(this.impugnacionSan.diasRestantesPlan==0)
                  this.flagBloqueoDias = false;


                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==50)
                this.toast.ok("Falta 50 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==40)
                  this.toast.ok("Falta 40 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==30)
                  this.toast.ok("Falta 30 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==20)
                  this.toast.ok("Falta 20 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==10)
                  this.toast.ok("Falta 10 días para resolver las observaciones.");

                if(this.impugnacionSan.diasRestantesPlan>0 && this.impugnacionSan.diasRestantesPlan ==5)
                  this.toast.ok("Falta 5 días para resolver las observaciones.");

              } else {

                this.flagBloqueoDias = true;

              }

            }
          }
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  guardarImpugnacion() {
    if(!this.validacionFormulario()) return;
    let params: any[] = [];
    //this.impugnacionSan.idPermisoForestal = this.idPlanManejo;
    this.impugnacionSan.nroGestion = this.idPlanManejo;
    this.impugnacionSan.idArchivo = this.idArchivo;
    this.impugnacionSan.codSolicitudSAN= this.codigoPlan;
    this.impugnacionSan.subCodSolicitudSAN =  CodigosTiposSolicitud.IMPUGNACION_SAN,

    params.push(this.impugnacionSan);
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.impugnacionSanService
      .registrarSolicitudSAN(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((res: any) => {
        if (res.success == true) {
          this.toast.ok('Se registró la impugnación SAN correctamente.');
          this.isShowModal2_2 = false;
          this.listarImpugnacion();
          this.registroImpugnacionEmiter.emit(1);
        } else {
          this.toast.error(res?.message);
        }
      });
  }

  validacionFormulario() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.impugnacionSan.asunto == '' || this.impugnacionSan.asunto == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Asunto.\n';
    }

    if (this.idArchivo == null || this.idArchivo == 0) {
      validar = false;
      mensaje = mensaje += '(*) Debe cargar un Archivo.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;

  }

  validacionDiasCalendario() {
    let validar: boolean = true;
    let mensaje: string = '';

    if (this.impugnacionSan.diasAdicionales == 0 || this.impugnacionSan.diasAdicionales == null) {
      validar = false;
      mensaje = mensaje += '(*) Debe ingresar: Días adicionales.\n';
    }

    if (this.idArchivoEvidencia == null || this.idArchivoEvidencia == 0) {
      validar = false;
      mensaje = mensaje += '(*) Debe cargar un Archivo Evidencia.\n';
    }

    if (!validar) this.ErrorMensaje(mensaje);
    return validar;

  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: 'warn', summary: '', detail: mensaje });
  }

}


