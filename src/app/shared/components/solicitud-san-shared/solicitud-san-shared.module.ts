import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SolicitudSanSharedComponent } from './solicitud-san-shared.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {DialogModule} from 'primeng/dialog';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputButtonsModule} from '../input-button/input-buttons.module';
import {RadioButtonModule} from 'primeng/radiobutton';
import {ButtonsDownloadFileModule} from '../button-download-file/buttons-download-file.module';
import {ButtonsFilePermisoForestalModule} from '../buttons-file-permiso-forestal/buttons-file-permiso-forestal.module';
import {InputButtonsCodigoModule} from '../input-button-codigo/input-buttons.module';
import {SharedModule} from '@shared';


@NgModule({
  declarations: [SolicitudSanSharedComponent],
  imports: [
    CommonModule,
    PdfViewerModule,
    DialogModule,
    FormsModule,
    DropdownModule,
    InputTextareaModule,
    InputButtonsModule,
    RadioButtonModule,
    ButtonsDownloadFileModule,
    ButtonsFilePermisoForestalModule,
    InputButtonsCodigoModule,
    SharedModule
  ],
  exports:[SolicitudSanSharedComponent]
})
export class SolicitudSanSharedModule { }
