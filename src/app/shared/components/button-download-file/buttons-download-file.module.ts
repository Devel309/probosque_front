import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsDownloadFileComponent } from './buttons-download-file.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [ButtonsDownloadFileComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [ButtonsDownloadFileComponent],
})
export class ButtonsDownloadFileModule {}
