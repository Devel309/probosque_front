import { HttpErrorResponse, HttpParams } from "@angular/common/http";
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ArchivoService, UsuarioService } from "@services";
import { descargarArchivo, DowloadFileLocal, DownloadFile, ToastService } from "@shared";
import { ConfirmationService, MessageService } from "primeng/api";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { FileModel } from "src/app/model/util/File";
import { AnexosService } from "src/app/service/planificacion/generacion-declaracion-manejo-dema/anexo.service";


@Component({
  selector: "buttons-download-file",
  templateUrl: "./buttons-download-file.component.html",
  styleUrls: ["./buttons-download-file.component.scss"],
})
export class ButtonsDownloadFileComponent implements OnInit {
  @Input() idPlanManejo!: number;

  @Input() file: any = {};
  @Input() label: string = "";
  @Input() disabled: boolean = false;
  @Input() accept: string = "";
  @Input() idArchivoModal: number = 0;

  @Input() idTipoDocumento!: number;
  @Input() codigoProceso!: string;

  @Output() idArchivoEmit = new EventEmitter();


  @Output() registrarArchivoId: EventEmitter<number> = new EventEmitter();
  files: FileModel[] = [];
  fileInfGenreal: FileModel = {} as FileModel;
  verEnviar1: boolean = false;

  cargarArchivo: boolean = false;
  eliminarArchivo: boolean = true;
  idArchivo: number = 0;

  static get EXTENSIONSAUTHORIZATION2() {
    return [".pdf", "image/png", "image/jpeg", "image/jpeg", "application/pdf"];
  }

  constructor(
    private dialog: MatDialog,
    private anexosService: AnexosService,
    private user: UsuarioService,
    private toast: ToastService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private archivoServ: ArchivoService
  ) {}

  ngOnInit(): void {
    this.fileInfGenreal.inServer = false;
    this.fileInfGenreal.descripcion = "PDF";
    this.listarArchivoPMFI();
  }



  listarArchivoPMFI() {
    var params = {
      idArchivo: null,
      idPlanManejo: this.idPlanManejo,
      idTipoDocumento: this.idTipoDocumento,
      codigoProceso: this.codigoProceso,
    };
    this.anexosService.listarArchivoDema(params).subscribe((result: any) => {
      if (result.data && result.data.length != 0) {
        this.cargarArchivo = true;
        this.eliminarArchivo = false;
        result.data.forEach((element: any) => {
          this.fileInfGenreal.nombreFile = element.nombreArchivo;
          this.idArchivo = element.idArchivo;
          this.registrarArchivoId.emit(this.idArchivo);
        });
      } else {
        this.eliminarArchivo = true;
        this.cargarArchivo = false;
      }
    });
    //this.registrarArchivoId.emit(this.idArchivo);  ncoqc
  }



  descargarArchivo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    if (this.idArchivo) {
      let params = {
        idArchivo: this.idArchivo
      }

      this.archivoServ.descargarArchivoGeneral(params).subscribe((result: any) => {
        this.dialog.closeAll();
        if (result.data !== null && result.data !== undefined) {
          DownloadFile(result.data.archivo, result.data.nombeArchivo, result.data.contenTypeArchivo);
        }
        (error: HttpErrorResponse) => {
          this.ErrorMensaje(error.message);
          this.dialog.closeAll();
        }
      })

    } else {
      DowloadFileLocal(this.file.url, this.file.nombre);
      this.dialog.closeAll();
    }
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }
}
