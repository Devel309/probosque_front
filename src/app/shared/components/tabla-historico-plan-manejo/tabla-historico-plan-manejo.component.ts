import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { PlanManejoService } from "@services";
import { finalize } from "rxjs/operators";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { ResumenService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/resumen.service";

@Component({
  selector: "tabla-historico-plan-manejo",
  templateUrl: "./tabla-historico-plan-manejo.component.html",
  styleUrls: ["./tabla-historico-plan-manejo.component.scss"],
})
export class TableHistoricoPlanManejoComponent implements OnInit {
  @Input() idPlanManejo!: number;
  codigoEpica: string = "";
  listaInfoPlan: any[] = [];

  constructor(
    private resumenService: ResumenService,
    private dialog: MatDialog,
    private router: Router,
    private planManejoService: PlanManejoService
  ) {}

  ngOnInit(): void {
    this.obtenerPlan();
  }
  obtenerPlan() {
    let params = {
      idPlanManejo: this.idPlanManejo,
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.planManejoService
      .obtenerPlanManejo(params)
      .pipe(finalize(() => {
        this.dialog.closeAll()
        this.listaPlanManejo()
      }))
      .subscribe((result: any) => {
        this.codigoEpica = result.data.descripcion;
      });
  }
  listaPlanManejo() {
    this.dialog.open(LoadingComponent, { disableClose: true });
    var params = {
      idPlanManejo: this.idPlanManejo,
      tipoPlan: this.codigoEpica,
    };
    this.resumenService
      .obtenerInformacionPlan(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((response: any) => {
        this.listaInfoPlan = response.data;
      });
  }
  verDetalle(idPlanManejo: number, tipoPlan: string) {
    if (tipoPlan == "PMFI") {
      this.router.navigateByUrl(
        "/planificacion/formulacion-PMFI/" + idPlanManejo
      );
    } else if (tipoPlan == "DEMA") {
      this.router.navigateByUrl(
        "/planificacion/generar-declaracion-manejo-dema/" + idPlanManejo
      );
    } else if (tipoPlan == "PMFIC") {
      this.router.navigateByUrl(
        "/planificacion/plan-manejo-forestal-intermedio/" + idPlanManejo
      );
    }
  }
}
