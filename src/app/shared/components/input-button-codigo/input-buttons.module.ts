import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputButtonsCodigoComponent } from './input-buttons.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

@NgModule({
  declarations: [InputButtonsCodigoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    ConfirmPopupModule,
  ],
  exports: [InputButtonsCodigoComponent],
})
export class InputButtonsCodigoModule {}
