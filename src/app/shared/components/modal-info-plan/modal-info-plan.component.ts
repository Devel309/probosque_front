import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { PlanManejoService } from "@services";
import { isNullOrEmpty, ToastService } from "@shared";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { LoadingComponent } from "src/app/components/loading/loading.component";
import { CodigoProceso } from 'src/app/model/util/CodigoProceso';
import { InformacionGeneralService } from "src/app/service/planificacion/formulacion-pmfi-concesion-pfdm/informacion-general.service";

@Component({
  selector: "app-modal-info-plan",
  templateUrl: "./modal-info-plan.component.html",
})
export class ModalInfoPlanComponent implements OnInit {
  queryPlan: string = "";
  listPlan: any[] = [];
  selectPlan: any;
  selectedValues: any[] = [];
  queryIdPlan: string = "";
  plan: any[] = [];
  idPlanManejoPadre: number = 0;
  buscar: boolean = false;
  related: boolean = false;
  codTipoPlan!: string;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private planManejoService: PlanManejoService,
    private informacionGeneralService: InformacionGeneralService,
    private toast: ToastService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.buscar = this.config.data.buscar
    this.codTipoPlan = this.config.data.codTipoPlan;
    this.idPlanManejoPadre = this.config.data.idPlanManejoPadre      
    this.listarPlan();
  }

  onChangePlan(event: any, data: any){
    if (event.checked === true && this.selectedValues.length != 0) {
      this.selectedValues = [data];
      this.selectedValues.forEach(response => {
        this.selectPlan = response
      })
    }
  }

  filtrarIdPlanPadre(idPlanManejoPadre: number) {
    this.selectedValues = []
    this.plan = this.listPlan.filter(
      (x) => idPlanManejoPadre == x.idPlanManejo
    );
    this.plan.forEach((response) => {
      this.selectPlan = response;
      this.selectedValues.push(this.selectPlan)
    });
  }

  filtrarPlan() {
    if (this.queryPlan) {
      this.listPlan = this.listPlan.filter(
        (r) => r.idPlanManejo == this.queryPlan
      );
    } else if (this.queryIdPlan) {
      this.filtrarId();
    } else {
      this.listarPlan();
    }
  }

  filtrarId() {
    this.selectedValues = []
    if (this.queryIdPlan) {
      this.listPlan = this.listPlan.filter(
        (r) => r.idPlan == this.queryIdPlan
      );
      this.plan.forEach((response) => {
        this.selectPlan = response;
        this.selectedValues.push(this.selectPlan)
      });
    } else {
      this.listarPlan();
    }
  }

  listarPlan() {
    let numeroDoc = '';
    let nroRucEmp = '';
    if (this.config.data.idTipoDocumento == 4) {
      nroRucEmp = this.config.data.nroDocumento;
    } else {
      numeroDoc = this.config.data.nroDocumento;
    }    
    if (!this.buscar) {
      numeroDoc = '';
      nroRucEmp = '';
    } else if (this.buscar) {
      if (this.idPlanManejoPadre != 0) {
        numeroDoc = '';
        nroRucEmp = '';
        this.related = true;
      }
    }
    let params = {
      nroDocumento: numeroDoc,
      nroRucEmpresa: nroRucEmp,
      codTipoPlan: this.codTipoPlan,
      idPlanManejo: this.idPlanManejoPadre
    };
    this.dialog.open(LoadingComponent, { disableClose: true });
    this.informacionGeneralService
      .listarPorFiltroInformacionGeneral(params)
      .subscribe(
      (response: any) => {
        this.dialog.closeAll();
        if (response.success == true) {
          response.data.forEach((item: any) => {
            if (item.codTipoPersona == 'TPERJURI') {
              item.nombreComunidad = item.nombreElaboraDema;
              item.nombreRepresentante = !isNullOrEmpty(item.nombreRepresentante) ? item.nombreRepresentante : '';
              item.apellidoPaternoRepresentante = !isNullOrEmpty(item.apellidoPaternoRepresentante) ? item.apellidoPaternoRepresentante : '';
              item.apellidoMaternoRepresentante = !isNullOrEmpty(item.apellidoMaternoRepresentante) ? item.apellidoMaternoRepresentante : '';
              item.jefeComunidad = item.nombreRepresentante + ' ' + item.apellidoPaternoRepresentante + ' ' + item.apellidoMaternoRepresentante;
              item.documento = item.nroRucComunidad;
            } else {
              item.nombreComunidad = '';
              item.nombreElaboraDema = !isNullOrEmpty(item.nombreElaboraDema) ? item.nombreElaboraDema : '';
              item.apellidoPaternoElaboraDema = !isNullOrEmpty(item.apellidoPaternoElaboraDema) ? item.apellidoPaternoElaboraDema : '';
              item.apellidoMaternoElaboraDema = !isNullOrEmpty(item.apellidoMaternoElaboraDema) ? item.apellidoMaternoElaboraDema : '';
              item.jefeComunidad = item.nombreElaboraDema + ' ' + item.apellidoPaternoElaboraDema + ' ' + item.apellidoMaternoElaboraDema;
              item.documento = item.dniElaboraDema;
            }

            this.listPlan.push(item);
          });
          
          this.filtrarIdPlanPadre(this.idPlanManejoPadre);
        }
      },
      (error) => {
        this.dialog.closeAll();
        this.toast.error("Ocurrió un error al realizar la operación");
      }
    );
  }

  validarPlan(): boolean {
    let validar: boolean = true;
    let mensaje: string = "";

    if (this.selectPlan == null || this.selectPlan == "") {
      validar = false;
      mensaje = mensaje += "(*) Debe seleccionar: Un plan.\n";
    }

    if (!validar) this.toast.warn(mensaje);

    return validar;
  }
  
  redireccionar(idPlanManejoPadre: any) {
    this.cerrarModal();
    if (this.codTipoPlan == CodigoProceso.PLAN_GENERAL) {
      this.router.navigateByUrl('/planificacion/plan-general-manejo/' + idPlanManejoPadre);
    } else if (this.codTipoPlan == CodigoProceso.PLAN_MANEJO_FORESTAL_INTERMEDIO) {
      this.router.navigateByUrl('/planificacion/plan-manejo-forestal-intermedio/' + idPlanManejoPadre);
    }    
  }

  ErrorMensaje(mensaje: any) {
    this.messageService.add({ severity: "warn", summary: "", detail: mensaje });
  }

  agregar = () => {
    if (!this.validarPlan()) return;
    this.ref.close(this.selectPlan);
  };

  cerrarModal() {
    this.ref.close();
  }
}
