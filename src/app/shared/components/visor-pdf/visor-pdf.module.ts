import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisorPdfComponent } from './visor-pdf.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {DialogModule} from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [VisorPdfComponent],
  imports: [
    CommonModule,
    PdfViewerModule,
    DialogModule,
    ButtonModule
  ],
  exports:[VisorPdfComponent]
})
export class VisorPdfModule { }
