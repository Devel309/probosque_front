import { Component, Input, OnInit } from '@angular/core';
import {LoadingComponent} from '../../../components/loading/loading.component';
import {finalize} from 'rxjs/operators';
import {ArchivoService} from '@services';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'visor-pdf-shared',
  templateUrl: './visor-pdf.component.html',
  styleUrls: ['./visor-pdf.component.scss']
})
export class VisorPdfComponent implements OnInit {
  @Input() idArchivo!: number;

  pdfSrc :string = "";
  isShowModal2_2:boolean=false;

  constructor(private archivoService: ArchivoService,private dialog: MatDialog,) { }

  ngOnInit(): void {

  }


  verPDF(){

    //this.pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";

    this.isShowModal2_2 = true;
    this.dialog.open(LoadingComponent, { disableClose: true });

    var params = {
      idArchivo: this.idArchivo
    };

    this.archivoService.descargarArchivoGeneral(params)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe((result: any) => {


        

        if(result.data){
          let reader = new FileReader();
          reader.onload = (e:any) => {
            
            this.pdfSrc = e.target.result;
          }
          const blob = this.base64toBlob(result.data.archivo, 'application/pdf')
          reader.readAsArrayBuffer(blob);
        }


      });


  }


  base64toBlob(base64Data:any, contentType:any) {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      var begin = sliceIndex * sliceSize;
      var end = Math.min(begin + sliceSize, bytesLength);

      var bytes = new Array(end - begin);
      for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }

}
