import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextColorEstadoComponent } from './text-color-estado.component';



@NgModule({
  declarations: [
    TextColorEstadoComponent
  ],
  exports: [
    TextColorEstadoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TextColorEstadoModule { }
