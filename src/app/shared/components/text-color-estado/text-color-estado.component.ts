import { Component, Input, OnInit } from '@angular/core';
import { CodigoEstadoMesaPartes, CodigoEstadoPlanManejo } from 'src/app/model/util/CodigoEstadoPlanManejo';
import { CodigoEstadoPagos } from 'src/app/model/util/CodigoPagos';
import {CodigoEstadoEvaluacion} from '../../../model/util/CodigoEstadoEvaluacion';

@Component({
  selector: 'text-color-estado',
  templateUrl: './text-color-estado.component.html',
  styleUrls: ['./text-color-estado.component.scss']
})
export class TextColorEstadoComponent implements OnInit {

  @Input() codEstado?:string;
  @Input() codigoEstado?:string;

  codigoEstadoPlanManejo = CodigoEstadoPlanManejo
  codigoEstadoMesaPartes = CodigoEstadoMesaPartes
  codigoEstadoEvaluacion = CodigoEstadoEvaluacion;
  CodigoEstadoPagos = CodigoEstadoPagos;
  constructor() { }

  ngOnInit(): void {
  }

}
