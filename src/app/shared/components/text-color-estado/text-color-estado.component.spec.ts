import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextColorEstadoComponent } from './text-color-estado.component';

describe('TextColorEstadoComponent', () => {
  let component: TextColorEstadoComponent;
  let fixture: ComponentFixture<TextColorEstadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextColorEstadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextColorEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
