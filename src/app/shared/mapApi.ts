import { Injectable, OnInit } from '@angular/core';
import GeoJSONLayer from "@arcgis/core/layers/GeoJSONLayer";
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer";
import Collection from "@arcgis/core/core/Collection";
import Layer from '@arcgis/core/layers/Layer';

import Graphic from "@arcgis/core/Graphic";
import Map from "@arcgis/core/Map";
import MapView from "@arcgis/core/views/MapView";
import Compass from "@arcgis/core/widgets/Compass";
import Home from "@arcgis/core/widgets/Home";
import ScaleBar from "@arcgis/core/widgets/ScaleBar";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol";
import SimpleLineSymbol from "@arcgis/core/symbols/SimpleLineSymbol";
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol";
import SimpleRenderer from "@arcgis/core/renderers/SimpleRenderer";
import Color from "@arcgis/core/Color";
import Print from "@arcgis/core/widgets/Print";
import * as geometryEngine from '@arcgis/core/geometry/geometryEngine';
import * as wkt from "terraformer-wkt-parser";
import { OgcGeometryType } from '@shared';
import Polygon from '@arcgis/core/geometry/Polygon';
import Polyline from "@arcgis/core/geometry/Polyline";
import { EsriGeometryType } from './enums';
import { AnyARecord } from 'dns';


declare const proj4: any;
declare const shp: any;
declare const window: any;
declare const shpwrite: any;

@Injectable({
  providedIn: 'root'
})
export class MapApi {
  constructor() {

  }
  initializeMap(container: any): MapView {
    const map = new Map({
      basemap: "hybrid",
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    let compass = new Compass({
      view: view,
    });
    view.ui.add(compass, "top-right");
    let homeWidget = new Home({
      view: view
    });
    view.ui.add(homeWidget, "top-left");
    const scaleBar = new ScaleBar({
      view: view,
      unit: "dual"
    });
    view.ui.add(scaleBar, {
      position: "bottom-left"
    });
    return view;
  }
  initializeMapCustom(container: any) {
    const map = new Map({
      basemap: "hybrid",
    });

    const view = new MapView({
      container,
      map: map,
      center: [-75, -8.2],
      zoom: 6,
    });
    let compass = new Compass({
      view: view,
    });
    view.ui.add(compass, "top-right");
    let homeWidget = new Home({
      view: view
    });
    view.ui.add(homeWidget, "top-left");
    const scaleBar = new ScaleBar({
      view: view,
      unit: "dual"
    });
    view.ui.add(scaleBar, {
      position: "bottom-left"
    });
    let btnView = document.createElement("button");
    btnView.classList.add("btn-primary2");
    btnView.setAttribute("title", "Mostrar/Ocultar Exportar mapa");
    let i = document.createElement("span");
    i.classList.add("esri-icon-documentation");
    btnView.appendChild(i);
    view.ui.add(btnView, "bottom-right");
    btnView.addEventListener("click", (e) => {
      let print = container.querySelector(".esri-widget--panel");
      if (print.style.display === "none") {
        print.style.display = "block";
      } else {
        print.style.display = "none";
      }
    });
    view.when(() => {
      let print = this.print(view);
      view.ui.add(print, "top-right");
    });

    return view;
  }
  print(view: MapView) {
    return new Print({
      view: view,
      printServiceUrl:
        "https://utility.arcgisonline.com/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task"
    });
  }
  geoJsonLayer(url: any) {
    return new GeoJSONLayer({
      url: url
    });
  }
  graphicsLayer() {
    return new GraphicsLayer();
  }
  graphic(geometry: any, symbol: any) {
    return new Graphic({
      geometry: geometry,
      symbol: symbol
    });
  }
  simpleMarkerSymbol() {
    return new SimpleMarkerSymbol();
  }
  simpleFillSymbol() {
    return new SimpleFillSymbol();
  }
  simpleLineSymbol() {
    return new SimpleLineSymbol();
  }
  simpleRenderer(style: any) {
    return new SimpleRenderer({ symbol: style });
  }
  esriColor(color: any) {
    return Color.fromHex(color);
  }
  get Guid2() {
    return class Guid2 {
      constructor() { }
      static get newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
          var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });

      }
    }
  }
  random() {
    var length = 6;
    var chars = "0123456789ABCDEF";
    var hex = "#";
    while (length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }
  proj4j() {
    if (window.proj4 !== undefined && window.proj4 !== null) {
      proj4.spatialReferences = [];
      proj4.addDef = function (srid: any, wkt: any, jsFormat: any) {
        try {
          proj4("EPSG:" + srid);
        }
        catch (error) {
          proj4.defs("EPSG:" + srid, jsFormat);
        }
        proj4.spatialReferences.push({ srid: srid, wkt: wkt });
      };
      proj4.getDef = function (srid: any) {
        return proj4.spatialReferences.find(function (t: any) { return t.srid === srid }) || null;
      };
      proj4.addDef(3857, 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Mercator"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]', "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs");
      proj4.addDef(4326, 'GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]]', "+proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees");
      proj4.addDef(24877, 'PROJCS["PSAD56 / UTM zone 17S",GEOGCS["PSAD56",DATUM["D_Provisional_S_American_1956",SPHEROID["International_1924",6378388,297]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-81],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["Meter",1]]', "+proj=utm +zone=17 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs");
      proj4.addDef(24878, 'PROJCS["PSAD56 / UTM zone 18S",GEOGCS["PSAD56",DATUM["D_Provisional_S_American_1956",SPHEROID["International_1924",6378388,297]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-75],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["Meter",1]]', "+proj=utm +zone=18 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs");
      proj4.addDef(24879, 'PROJCS["PSAD56 / UTM zone 19S",GEOGCS["PSAD56",DATUM["D_Provisional_S_American_1956",SPHEROID["International_1924",6378388,297]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-69],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["Meter",1]]', "+proj=utm +zone=19 +south +ellps=intl +towgs84=-288,175,-376,0,0,0,0 +units=m +no_defs");
      proj4.addDef(32717, 'PROJCS["WGS_1984_UTM_Zone_17S",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-81],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["Meter",1]]', "+proj=utm +zone=17 +south +datum=WGS84 +units=m +no_defs");
      proj4.addDef(32718, 'PROJCS["WGS_1984_UTM_Zone_18S",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-75],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["Meter",1]]', "+proj=utm +zone=18 +south +datum=WGS84 +units=m +no_defs");
      proj4.addDef(32719, 'PROJCS["WGS_1984_UTM_Zone_19S",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137,298.257223563]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",-69],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],UNIT["Meter",1]]', "+proj=utm +zone=19 +south +datum=WGS84 +units=m +no_defs");
    }
  }
  transform(coordinates: any, sridFrom: any, sridTo: any) {
    if (sridFrom === sridTo) return coordinates;
    let toProj = proj4('EPSG:' + sridTo);
    let fromProj = proj4('EPSG:' + sridFrom);
    this._transform(coordinates, fromProj, toProj);
    return coordinates;
  }
  _transform(coordinates: any, fromProj: any, toProj: any) {
    if (Array.isArray(coordinates) === true && Array.isArray(coordinates[0]) === false) {
      let coordinates2 = proj4(fromProj, toProj).forward(coordinates);
      coordinates[0] = coordinates2[0];
      coordinates[1] = coordinates2[1];
      return;
    }
    coordinates.forEach((t: any, i: any) => {
      if (Array.isArray(t) === true && Array.isArray(t[0]) === true)
        this._transform(t, fromProj, toProj);
      else {
        let coordinates2 = proj4(fromProj, toProj).forward(t);
        t[0] = coordinates2[0];
        t[1] = coordinates2[1];
      }
    });
  }
  loadShapeFile(item: any) {
    return this.getItem(item).then(data => {
      return new Promise((resolve, reject) => {
        let fileReader = new FileReader();
        fileReader.onload = (e: any) => {
          resolve(e.target.result);
        };
        fileReader.onerror = fileReader.onabort = reject;
        fileReader.readAsArrayBuffer(data);
      });
    }).then(data => {
      return shp(data);
    }).then(data => {
      if (Array.isArray(data) === false)
        data = [data];
      data.forEach((t: any) => t.title = t.fileName);
      return data;
    }).catch(error => {
      throw error;
    });
  }
  getItem(item: any) {
    let promise = Promise.resolve(item);
    if (typeof item === "string")
      promise = fetch(item).then(data => { return data.blob(); });
    return promise;
  }
  exportToSHPFile(geoJson: any, options: any) {
    shpwrite.download(geoJson, options);
  }
  wktParse(geometry: any) {
    if (geometry === null || geometry === undefined) return null;
    return wkt.parse(geometry);
  }
  readFileByte(file: any) {
    let byteArrays = [];
    file = window.atob(file);
    let buffer = new ArrayBuffer(file.length),
      view = new Uint8Array(buffer);
    for (let i = 0; i < file.length; i++) {
      view[i] = file.charCodeAt(i);
    }
    let byteArray = new Uint8Array(buffer);
    byteArrays.push(byteArray);
    let blob = new Blob(byteArrays, { type: 'application/octet-stream' });
    return blob;
  }
  calculateArea(geometry: any, measure: any) {
    return geometryEngine.geodesicArea(geometry, measure);
  }
  calculateLenght(geometry: any, measure: any) {
    return geometryEngine.geodesicLength(geometry, measure);
  }
  validateFileInputSHP(file: any) {
    let controls = {
      message: "",
      success: true,
    };
    if (file[0] === undefined || file[0].length === 0) {
      controls.message = "Seleccione un archivo";
      controls.success = false;
    } else if (file[0].name.endsWith(".zip") === false) {
      controls.message = "Seleccione un archivo .zip";
      controls.success = false;
    }
    return controls;
  }
  processFileSHP(file: any) {
    let promise = Promise.resolve([]);
    try {
      promise = this.loadShapeFile(file);
    } catch (error) {
      console.log(error);
    }
    return promise
  }
  processFile(file: any, config: any, id: string, view: MapView) {
    let item = file;
    try {
      let promise = Promise.resolve([]);
      promise = this.loadShapeFile(item);
      promise.then((data) => {
        this.createLayers(data, file, config, id, view);
      })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      console.log(error);
    }
  }

  createLayers(layers: any, file: any, config: any, id: string, view: MapView) {
    layers.forEach((t: any) => {
      t.title = t.title.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t.features.length;
      t.opacity = 0.8;
      t.color = this.random();
      t.file = file;
      t.groupId = id;
      t.id = this.Guid2.newGuid;
      t.crs = {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      };
      this.createLayer(t, view);
      /* 
            this.calculateArea(t, config); */
    });
  }

  //#region UTILS
  crearCapas(layers: any[], config: any) {
    layers.forEach(t => {
      t.title = t?.title?.replace(/(\w+\/)*(\w+)/gi, "$2");
      t.totalFeatures = t?.features.length;
      t.opacity = 0.8;
      t.color = this.random();

      t.file = config.file;
      t.fileName = config.fileName;
      t.idArchivo = config.idArchivo;
      t.idLayer = config.idLayer;
      t.groupId = config.mapId;

      t.crs = { type: "name", properties: { name: `epsg:4326`, }, };
      this.createLayer(t, config.view);
    });
  }
  createCapa(item: any, view: MapView) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = item.groupId;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.fileName = item.fileName;
    geoJsonLayer.idArchivo = item.idArchivo;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.changeLayerStyle(geoJsonLayer.id, geoJsonLayer.color, view);
        view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {
        console.log(error);
      });
    view.map.add(geoJsonLayer);
  }
  getDataShpFile(file: File | Blob): Promise<Layer[]> {
    return new Promise((resolve, reject) => {
      this.readFile(file)
        .then(arrayBuffer => {
          shp(arrayBuffer)
            .then((data: (Collection<Layer> | any)) => {
              if (!Array.isArray(data)) resolve([data]);
              resolve(data);
            })
            .catch((err: any) => reject(err))
        })
        .catch(err => reject(err))
    });
  }

  readFile(file: File | Blob): Promise<ArrayBuffer> {
    return new Promise((resolve, reject) => {
      let fileReader = new FileReader();
      fileReader.onloadend = (e: any) => resolve(e.target.result);
      fileReader.onerror = fileReader.onabort = reject;
      fileReader.readAsArrayBuffer(file);
    });
  }

  //#endregion
  createLayer(item: any, view: MapView) {
    item.features.forEach((t: any, i: any) => {
      t.properties.OBJECTID = i + 1;
    });
    let blob = new Blob([JSON.stringify(item)], { type: "application/json" });
    let url = URL.createObjectURL(blob);
    let popupTemplate = {
      title: item.title,
    };
    let geoJsonLayer: any = this.geoJsonLayer(url);
    geoJsonLayer.visible = true;
    geoJsonLayer.id = item.idLayer;
    geoJsonLayer.ID = geoJsonLayer.id;
    geoJsonLayer.title = item.title;
    geoJsonLayer.layerType = "vector";
    geoJsonLayer.groupId = item.groupId;
    geoJsonLayer.color = item.color;
    geoJsonLayer.opacity = item.opacity;
    geoJsonLayer.attributes = item.features;
    geoJsonLayer.file = item.file;
    geoJsonLayer.popupTemplate = popupTemplate;
    geoJsonLayer.fileName = item.fileName;
    geoJsonLayer.idArchivo = item.idArchivo;
    geoJsonLayer
      .when((data: any) => {
        URL.revokeObjectURL(url);
        this.changeLayerStyle(geoJsonLayer.id, geoJsonLayer.color, view);
        view.goTo({ target: data.fullExtent });
      })
      .catch((error: any) => {
        console.log(error);
      });
    view.map.add(geoJsonLayer);
  }

  changeLayerStyle(id: any, color: any, view: MapView) {
    let layer: any = view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer === null) return;
    layer.color = color;
    let type = layer.geometryType;
    let style: any = null;
    if (type === "point" || type === "multipoint") {
      style = this.simpleMarkerSymbol();
      style.size = 10;
    } else if (type === "polyline") {
      style = this.simpleLineSymbol();
      style.width = 2;
    } else if (type === "polygon" || type === "extent") {
      style = this.simpleFillSymbol();
      style.outline = this.simpleLineSymbol();
      style.outline.width = 2;
    }
    style.color = this.esriColor(color);
    layer.renderer = this.simpleRenderer(style);
  }
  getLayers(id: any, view: MapView): Collection<__esri.Layer> {
    let layers = view.map.allLayers.filter(
      (t: any) => t.groupId === id
    );
    return layers;
  }
  getLayersService(id: any, view: MapView): Collection<__esri.Layer> {
    let layers = view.map.allLayers.filter(
      (t: any) => t.groupId === id && t.service === true
    );
    return layers;
  }
  removeLayer(id: any, view: MapView) {
    let layer: any = view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null) {
      view.map.layers.remove(layer);
      layer.visible = false;
      layer.forRemove = true;
    }
  }
  toggleLayer(id: any, visible: any, view: MapView) {
    let layer: any = view.map.allLayers.find((t: any) => t.ID === id) || null;
    if (layer !== null) layer.visible = visible;
  }
  getLayerById(id: any, view: MapView) {
    let layer: any = view.map.allLayers.find((t: any) => t.ID === id) || null;
    return layer;
  }
  cleanLayers(groupId: any, view: MapView) {
    let layers = this.getLayers(groupId, view);
    layers.forEach((t: any) => {
      view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
  }
  cleanLayersService(groupId: any, view: MapView) {
    let layers = this.getLayersService(groupId, view);
    layers.forEach((t: any) => {
      view.map.layers.remove(t);
      t.visible = false;
      t.forRemove = true;
    });
  }
  async getDatosbyAGS(options: any) {
    let url =
      "https://geoservidorperu.minam.gob.pe/arcgis/rest/services/ServicioBase/MapServer";
    let options2: any = {
      method: "post",
      data: {
        where: options.where,
        outFields: "*",
        returnGeometry: true,
        f: "json",
      },
    };
    url = url.replace("?", "");
    url = `${url}/${options.layerId}/query`;
    let headers = new Headers();
    let data = null;
    if (options2.data !== undefined && options2.data !== null) {
      data = new FormData();
      for (let i in options2.data) data.append(i, options2.data[i]);
    }
    let request = new Request(url, {
      method: "post",
      headers: headers,
      body: data,
    });

    try {
      const dataPromise = await fetch(request);
      return await dataPromise.json();
    } catch (error) {
      console.log(error);
    }
  }
  async getDatosbyAGSIntersect(options: any) {
    let url = options.url; //https://geoservidorperu.minam.gob.pe/arcgis/rest/services/ServicioBase/MapServer
    let geometry = {
      rings: options.geometry, //[[[-8046769.452578636,-944080.4843859308],[-8044323.467673511,-944080.4843859308],[-8046769.452578636,-941634.4994808058],[-7811954.901686637,-831565.1787501812],[-7745913.309248263,-975878.2881525557],[-7794833.007350762,-1230260.7182855546],[-8046769.452578636,-944080.4843859308]]]
    };
    let options2: any = {
      method: "post",
      data: {
        geometry: JSON.stringify(geometry),
        geometryType: "esriGeometryPolygon",
        where: "1=1",
        outFields: "*",
        inSR: 4326,
        outSR: 4326,
        spatialRel: "esriSpatialRelIntersects",
        returnGeometry: true,
        f: "json",
      },
    };
    url = url.replace("?", "");
    url = `${url}/${options.layerId}/query`;
    let headers = new Headers();
    let data = null;
    if (options2.data !== undefined && options2.data !== null) {
      data = new FormData();
      for (let i in options2.data) data.append(i, options2.data[i]);
    }
    let request = new Request(url, {
      method: "post",
      headers: headers,
      body: data,
    });

    try {
      const dataPromise = await fetch(request);
      return await dataPromise.json();
    } catch (error) {
      console.log(error);
    }
  }

  getGeometry(t3: any, srid: number) {
    let geometryWKT: any = null;
    if (t3.geometryType.toUpperCase() === OgcGeometryType.POLYGON || t3.geometryType.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
      if (t3.attributes[0].geometry.type.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
        geometryWKT = this.getMultiPolygonWKT2(t3.attributes, srid);
      } else if (t3.attributes.length >= 2) {
        geometryWKT = this.getMultiPolygonWKT(t3.attributes, srid);
      }
      else {
        geometryWKT = this.getPolygonWKT(t3.attributes, srid);
      }
    } else if (t3.geometryType.toUpperCase() === OgcGeometryType.LINESTRING || t3.geometryType.toUpperCase() === OgcGeometryType.MULTILINESTRING || t3.geometryType.toUpperCase() === OgcGeometryType.POLYLINE) {
      geometryWKT = this.getMultiLineStringWKT(t3.attributes, srid);
    } else if (t3.geometryType.toUpperCase() === OgcGeometryType.POINT || t3.geometryType.toUpperCase() === OgcGeometryType.MULTIPOINT) {
      geometryWKT = this.getMultiPointWKT(t3.attributes, srid);
    }
    return geometryWKT;
  }
  getGeometryNotView(t3: any, srid: number) {
    let geometryWKT: any = null;
     if (t3[0].geometry.type.toUpperCase() === OgcGeometryType.POINT || t3[0].geometry.type.toUpperCase() === OgcGeometryType.MULTIPOINT) {
      geometryWKT = this.getMultiPointWKT(t3, srid);
    }
    return geometryWKT;
  }
  getFeature(feature: any, srid: number) {
    let geometryWKT: any = null;
    if (feature.geometry.type.toUpperCase() === OgcGeometryType.POLYGON) {
      geometryWKT = this.getFeaturePolygonWKT(feature, srid);
    } else if (feature.geometry.type.toUpperCase() === OgcGeometryType.MULTIPOLYGON) {
      geometryWKT = this.getFeatureMultyPolygonWKT(feature, srid);
    } else if (feature.geometry.type.toUpperCase() === OgcGeometryType.POINT) {
      geometryWKT = this.getFeaturePointWKT(feature, srid);
    } else if (feature.geometry.type.toUpperCase() === OgcGeometryType.POLYLINE || feature.geometry.type.toUpperCase() === OgcGeometryType.LINESTRING) {
      geometryWKT = this.getFeatureLineStringWKT(feature, srid);
    }
    return geometryWKT;
  }
  getMultiPointWKT(attributes: any, srid: number) {
    let coordinates: any = [];
    attributes.forEach((t2: any) => {
      coordinates.push(t2.geometry.coordinates);
    });
    let wkt_str3: any = [];
    coordinates.forEach((t: any) => {
      let geojsonGeometry2 = {
        type: 'Point',
        coordinates: t
      };
      let wkt_str2 =
        '(' + geojsonGeometry2.coordinates[0] + ' ' + geojsonGeometry2.coordinates[1] +
        ')';
      wkt_str3.push(wkt_str2);
    });
    let ini = 'MULTIPOINT(';
    let fin = ')';
    return ini.concat(wkt_str3.toString(), fin);
  }
  getPolygonWKT(attributes: any, srid: number) {
    let geojsonGeometry = {
      type: 'Polygon',
      coordinates: attributes[0].geometry.coordinates,
      srid: srid,
    };
    let wkt_str =
      'POLYGON(' +
      geojsonGeometry.coordinates
        .map((ring: any) => {
          return (
            '(' +
            ring
              .map((p: any) => {
                return p[0] + ' ' + p[1];
              })
              .join(', ') +
            ')'
          );
        })
        .join(', ') +
      ')';
    return wkt_str;
  }
  getMultiLineStringWKT(attributes: any, srid: number) {
    let paths: any = [];
    attributes.forEach((t2: any) => {
      paths.push(t2.geometry.coordinates);
    });
    let geojsonGeometry = {
      type: 'Polyline',
      coordinates: paths,
      srid: srid,
    };
    let wkt_str =
      'MULTILINESTRING(' +
      geojsonGeometry.coordinates
        .map((path: any) => {
          if (path.length === 2) {
            return (

              path
                .map((p: any) => {
                  return (
                    '(' +
                    p
                      .map((coord: any) => {
                        return coord[0] + ' ' + coord[1];
                      })
                      .join(', ') +
                    ')'
                  )
                })
            );
          } else {
            return (
              '(' +
              path
                .map((p: any) => {
                  return p[0] + ' ' + p[1];
                })
                .join(', ') +
              ')'
            );
          }

        })
        .join(', ') +
      ')';
    return wkt_str;
  }
  getMultiPolygonWKT2(attributes: any, srid: number) {
    let rings: any = [];
    attributes.forEach((t2: any) => {
      rings.push(t2.geometry.coordinates);
    });
    let wkt_str3: any = [];
    rings.forEach((t: any) => {
      let geojsonGeometry2 = {
        type: 'Polygon',
        coordinates: t,
        srid: srid,
      };
      let wkt_str2 =
        '(' +
        geojsonGeometry2.coordinates
          .map((ring: any) => {
            return (
              '(' +
              ring
                .map((p: any) => {

                  return p.map((r: any) => {
                    return r[0] + ' ' + r[1];
                  })
                })
                .join(', ') +
              ')'
            );
          })
          .join(', ') +
        ')';
      wkt_str3.push(wkt_str2)
    });
    let ini = 'MULTIPOLYGON(';
    let fin = ')';
    return ini.concat(wkt_str3.toString(), fin);
  }
  getMultiPolygonWKT(attributes: any, srid: number) {
    let rings: any = [];
    attributes.forEach((t2: any) => {
      rings.push(t2.geometry.coordinates);
    });
    let wkt_str3: any = [];
    rings.forEach((t: any) => {
      let geojsonGeometry2 = {
        type: 'Polygon',
        coordinates: t,
        srid: srid,
      };
      let wkt_str2 =
        '(' +
        geojsonGeometry2.coordinates
          .map((ring: any) => {
            return (
              '(' +
              ring
                .map((p: any) => {
                  return p[0] + ' ' + p[1];
                })
                .join(', ') +
              ')'
            );
          })
          .join(', ') +
        ')';
      wkt_str3.push(wkt_str2);
    });
    let ini = 'MULTIPOLYGON(';
    let fin = ')';
    return ini.concat(wkt_str3.toString(), fin);
  }
  getFeaturePointWKT(feature: any, srid: number) {
    let geojsonGeometry = {
      type: 'Point',
      coordinates: feature.geometry.coordinates,
      srid: srid,
    };
    let wkt_str = 'POINT(' + geojsonGeometry.coordinates[0] + ' ' + geojsonGeometry.coordinates[1] +
      ')';
    return wkt_str;
  }
  getFeatureLineStringWKT(feature: any, srid: number) {
    let geojsonGeometry = {
      type: 'Polyline',
      coordinates: feature.geometry.coordinates,
      srid: srid,
    };
    let wkt_str =
      'LINESTRING(' +
      geojsonGeometry.coordinates
        .map((path: any) => {
          return path[0] + ' ' + path[1];

        })
        .join(', ') +
      ')';
    return wkt_str;
  }
  getFeaturePolygonWKT(feature: any, srid: number) {
    let geojsonGeometry = {
      type: 'Polygon',
      coordinates: feature.geometry.coordinates,
      srid: srid,
    };
    let wkt_str =
      'POLYGON(' +
      geojsonGeometry.coordinates
        .map((ring: any) => {
          return (
            '(' +
            ring
              .map((p: any) => {
                return p[0] + ' ' + p[1];
              })
              .join(', ') +
            ')'
          );
        })
        .join(', ') +
      ')';
    return wkt_str;
  }
  getFeatureMultyPolygonWKT(feauture: any, srid: number) {
    //let rings: any = [];
    //rings.push(feauture.geometry.coordinates);
    let wkt_str3: any = [];
    feauture.geometry.coordinates.forEach((t: any) => {
      let geojsonGeometry2 = {
        type: 'Polygon',
        coordinates: t,
        srid: srid,
      };
      let wkt_str2 =
        '(' +
        geojsonGeometry2.coordinates
          .map((ring: any) => {
            return (
              '(' +
              ring
                .map((p: any) => {
                  return p[0] + ' ' + p[1];
                })
                .join(', ') +
              ')'
            );
          })
          .join(', ') +
        ')';
      wkt_str3.push(wkt_str2);
    });
    let ini = 'MULTIPOLYGON(';
    let fin = ')';
    return ini.concat(wkt_str3.toString(), fin);
  }
  getGeoJson(guid: any, groupId: any, item: any) {
    let geoJson: any = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          properties: item.properties,
          geometry: item.jsonGeometry
        }
      ],
      opacity: 0.4,
      color: item.color,
      title: item.title,
      service: item.service,
      inServer: true,
      idLayer: guid,
      tipoGeometria: item.tipoGeometria,
      groupId: groupId,
      crs: {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      }
    }
    return geoJson;
  }
  getGeoJsonCustom(guid: any, groupId: any, item: any) {
    let geoJson: any = {
      type: "FeatureCollection",
      features: [
        {
          type: "Feature",
          properties: item.properties,
          geometry: item.jsonGeometry
        }
      ],
      opacity: 0.4,
      color: item.color,
      title: item.title,
      service: item.service,
      inServer: true,
      idLayer: guid,
      tipoGeometria: item.tipoGeometria,
      groupId: groupId,
      codProceso: item.codProceso,
      idPlanManejoGeometria: item.idPlanManejoGeometria,
      idsCatastro: item.idsCatastro,
      crs: {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      }
    }
    return geoJson;
  }
  getGeoJson2(guid: any, groupId: any, item: any) {
    let geoJson: any = {
      type: "FeatureCollection",
      features: item.features,
      opacity: 0.4,
      color: item.color,
      title: item.title,
      service: item.service,
      tipoGeometria: item.tipoGeometria,
      inServer: true,
      idLayer: guid,
      groupId: groupId,
      crs: {
        type: "name",
        properties: {
          name: `epsg:4326`,
        },
      }
    }
    return geoJson;
  }
  calculateIntersection(polygon1: any, polygon2: any) {
    let geometriaAreaTotal = this.createPolygon(polygon1);//turf.polygon(polygon1);

    let geometriaAreaIntersectar = this.createPolygon(polygon2);//turf.polygon(polygon2);;
    let newGeometry = geometryEngine.intersect(geometriaAreaTotal, geometriaAreaIntersectar);
    let area = this.calculateArea(newGeometry, 'hectares');
    return parseFloat(area.toFixed(2));
  }
  validarColindanciaPoligonoConPoligono(geometry1: any, geometry2: any) {
    const poligono1 = this.createPolygon(geometry1);
    const poligono2 = this.createPolygon(geometry2);

    const isTouching = geometryEngine.touches(poligono1, poligono2);
    return isTouching;
  }
  validarColindanciaPoligonoConLinea(geometry1: any, geometry2: any) {
    const poligono1 = this.createPolygon([[[-73.745693396999968, -10.354699965999941], [-73.791355501999988, -10.354874656999925], [-73.791293110999959, -10.371150884999963], [-73.791255383999953, -10.38097991099994], [-73.791208058999985, -10.393295570999953], [-73.79106614799997, -10.430133986999977], [-73.791033818999949, -10.438507164999976], [-73.745359567999969, -10.438331031999951], [-73.745378954999978, -10.433493480999971], [-73.745540386999949, -10.393120217999979], [-73.745657978999986, -10.363606527999934], [-73.745693396999968, -10.354699965999941]]]);
    const polyline = this.createPolyline([[[-73.790894734999938, -10.474448428999949], [-73.715303940999945, -10.47212879999995], [-73.713314275999949, -10.511454152999931], [-73.710107015999938, -10.513701665999974], [-73.707377246999954, -10.510977915999945], [-73.702359583999964, -10.509149014999934], [-73.700992859999985, -10.508239212999968], [-73.699618578999946, -10.509137795999948], [-73.699391107999986, -10.508910810999964], [-73.699497889999975, -10.483358247999945], [-73.699686145999976, -10.438148372999933], [-73.745359567999969, -10.438331031999951], [-73.791033818999949, -10.438507164999976], [-73.790894734999938, -10.474448428999949]]]);

    const isTouching = geometryEngine.touches(poligono1, polyline);
    return isTouching;
  }
  validarColindanciaGeneral(geometry1: any, geometry2: any) {
    const isTouching = geometryEngine.touches(geometry1, geometry2);
    return isTouching;
  }
  validarGeometriasColindancia(geometries: any) {
    let isTouches = [];
    for (let i = 0; i < geometries.length; i++) {
      for (let j = i + 1; j < geometries.length; j++) {
        let isTouching = this.validarColindanciaPoligonoConPoligono(geometries[i].coordinates, geometries[j].coordinates);
        isTouches.push(isTouching);

      }
    }
    return isTouches;
  }
  validarGeometriasColindancia2(geometries: any) {
    let isTouches = [];
    for (let i = 0; i < geometries.length; i++) {
      for (let j = i + 1; j < geometries.length; j++) {
        let isTouching = this.validarColindanciaGeneral(geometries[i], geometries[j]);
        isTouches.push(isTouching);

      }
    }
    return isTouches;
  }
  createPolygon(coordinates: any) {
    const polygon = new Polygon({
      hasZ: true,
      hasM: true,
      rings: coordinates,
      spatialReference: { wkid: 4326 }
    });
    return polygon;
  }
  createPolyline(coordinates: any) {
    let line = new Polyline({
      hasZ: false,
      hasM: true,
      paths: coordinates,
      spatialReference: { wkid: 4326 }
    });
    return line;
  }
  createPolygonFromPoint(features: any): Polygon {
    let ring: number[][] = [];
    features.forEach((f: any) => {
      if ((f.geometry.type == EsriGeometryType.POINT))
        ring.push(f.geometry.coordinates);
    });
    return new Polygon({
      hasZ: true,
      hasM: true,
      rings: [ring],
      spatialReference: { wkid: 4326 }
    });
  }
  // calculateArea(data: any, config: any) {
  //   let feature = data.features[0];
  //   let geometry: any = null;
  //   geometry = { spatialReference: { wkid: 4326 } };
  //   geometry.rings = feature.geometry.coordinates;
  //   let area = this.mapApi.calculateArea(geometry, "hectares");
  //   let name = config.name;
  //   let id = data.id;
  //   if (name === "potencialMaderable") {
  //     this.generarDatosPotencialMadera(feature.geometry.properties, area, id);
  //   } else if (name === "regeneracion") {
  //     this.regeneracionFustalesInvForestal.archivo = data.file;
  //     this.generarDatosRegeneracion(feature.geometry.properties, area, id);
  //   } else {
  //     console.log("No support");
  //   }
  // }

  // generarDatosPotencialMadera(item: any, area: any, id: any) {
  //   let table = this.tblPotencialMaderable.tableViewChild.nativeElement;
  //   let body = table.querySelector("tbody");
  //   let trs = body.querySelectorAll("tr");
  //   this.potencialMaderableInvForestal.areaMuestreada = area;
  //   let tdButton = trs[0].querySelectorAll("td")[2];
  //   let btnCargaSHP = tdButton.querySelector("button");
  //   btnCargaSHP.setAttribute("disabled", true);
  //   let input = tdButton.querySelector("input");
  //   let span = tdButton.querySelector("span");
  //   if (span !== null) span.remove();
  //   let btnDelete = tdButton.appendHTML(
  //     `<span class="float-right"><i class="pi pi-times-circle" id="${id}"></i></span>`
  //   );
  //   btnDelete.addEventListener("click", (e: any) => {
  //     this.removeLayer(e);
  //     input.value = "";
  //     btnCargaSHP.removeAttribute("disabled");
  //     btnDelete.remove();
  //   });
  // }
  // removeLayer(e: any) {
  //   let id = e.srcElement.id;
  //   let layer = this.view.map.allLayers.find((t: any) => t.ID === id) || null;
  //   if (layer !== null) {
  //     this.view.map.layers.remove(layer);
  //     layer.visible = false;
  //     layer.forRemove = true;
  //   }
  // }

  // generarDatosRegeneracion(item: any, area: any, id: any) {
  //   let table = this.tblRegeneracion.tableViewChild.nativeElement;
  //   let body = table.querySelector("tbody");
  //   let trs = body.querySelectorAll("tr");
  //   this.regeneracionFustalesInvForestal.areaMuestreada = area;
  //   let tdButton = trs[0].querySelectorAll("td")[2];
  //   let btnCargaSHP = tdButton.querySelector("button");
  //   btnCargaSHP.setAttribute("disabled", true);
  //   let input = tdButton.querySelector("input");
  //   let span = tdButton.querySelector("span");
  //   if (span !== null) span.remove();
  //   let btnDelete = tdButton.appendHTML(
  //     `<span class="pl-1"><i class="pi pi-times-circle" id="${id}"></i></span>`
  //   );
  //   btnDelete.addEventListener("click", (e: any) => {
  //     this.removeLayer(e);
  //     input.value = "";
  //     btnCargaSHP.removeAttribute("disabled");
  //     btnDelete.remove();
  //   });
  // }
}