
/**
 * DEMATipo tipos de proceso DEMA
 */
export enum DEMATipo {
  /** 
     * DEMA
     *  */
  DEMA = 'DEMA',
  /** 
   * Medidas de Protección de la Unidad de Manejo Forestal
   *  */
  MPUMF = 'MPUMF',
  /**
   * Actividades de Aprovechamiento y Equipos
   */
  AAE = 'AAE',
  AAEA = 'AAEA',
  /**
   * AAE - Aprovechamiento Maderable
   */
  AAEAM = 'AAEAM',
  /**
   * AAE - Aprovechamiento No Maderable
   */
  AAEANM = 'AAEANM',

}


export const tipoAAE: { [key: string]: string | any } = {
  AAEAM: 'Aprovechamiento Maderable',
  AAEANM: 'Aprovechamiento No Maderable',
  AAEOTRO: 'Aprovechamiento Otros',
}

/**
 * Aprovechamiento DEMA, maderable y no maderable
 */
export const aprovDEMA = {
  MADERABLE: 1,
  NO_MADERABLE: 2,
}

