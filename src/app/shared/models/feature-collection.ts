export interface FeatureCollection {
  fileName: string;
  type: string | 'FeatureCollection';
  features: Feature[];
}

export interface Feature {
  geometry: { coordinates: any[], type: string };
  properties: {
    VERTICES?: string,
    ESTE?: string | number,
    NORTE?: string | number,
    OBJECTID?: string | number,
  };
  type: | 'Feature' | string | any;
}

export interface CustomFeature<T> {
  geometry: { coordinates: any[], type: string };
  properties: T;
  type: | 'Feature' | string | any;
}
