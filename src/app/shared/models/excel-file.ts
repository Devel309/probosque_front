import { WorkBook } from 'xlsx';

export class ExcelFile {

  info: WorkBook | null;
  content: { sheetName: string, sheetContent: any[] }[];
  constructor() {
    this.info = null;
    this.content = [];
  }
}