export enum ToastType {
  success = 'success',
  info = 'info',
  warn = 'warn',
  error = 'error',
}

export enum FigureTypes {
  POLYGON = "Polygon",
  POINT = "Point",
  MULTI_POLYGON = "MultiPolygon",
}

export enum UnitMetric {
  HA = "hectares"
}