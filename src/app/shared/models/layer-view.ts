import { CustomFeature, Feature } from "./feature-collection";

export class LayerView {
  groupId: string | number | null;
  title: string;
  layerId: string | null;
  color: string | any;
  area: number;
  features: Feature[];


  constructor(obj?: Partial<LayerView>) {
    this.groupId = null;
    this.title = '';
    this.layerId = null;
    this.color = null;
    this.area = 0;
    this.features = [];
    if (obj) Object.assign(this, obj);
  }
}

export class CustomLayerView<T> {
  groupId: string | number | null;
  title: string;
  layerId: string | null;
  color: string | any;
  area: number;
  features: CustomFeature<T>[];


  constructor(obj?: Partial<CustomLayerView<T>>) {
    this.groupId = null;
    this.title = '';
    this.layerId = null;
    this.color = null;
    this.area = 0;
    this.features = [];
    if (obj) Object.assign(this, obj);
  }
}