import { ZipEntry } from "./zip";

export class GroupFile {
  groupId: string | number | null;
  file: File | Blob | null;
  entries: ZipEntry[];
  color?: string | null;


  constructor(obj?: Partial<GroupFile>) {
    this.groupId = null;
    this.file = null;
    this.color = null;
    this.entries = [];
    if (obj) Object.assign(this, obj);
  }
}