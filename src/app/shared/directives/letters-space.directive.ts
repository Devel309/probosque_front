import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
	 selector: "[lettersAndSpace]",
})
export class LettersAndSpaceDirective {
	private regex: RegExp = new RegExp(/^[A-Za-z\.\s]*$/);

	@Input() max!: number;

	constructor(private el: ElementRef) {}
	@HostListener("keydown", ["$event"])
	onKeyDown(e: KeyboardEvent) {
		e = <KeyboardEvent>event;

		let current: string = this.el.nativeElement.value;
		let next: string = current.concat(e.key);

		if (
			(next && !String(next).match(this.regex)) ||
			(this.max && next.length > this.max)
		)
			e.preventDefault();
	}


}
