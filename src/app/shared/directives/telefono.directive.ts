import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[telefono]'
})
export class TelefonoDirective {
  private regex: RegExp = new RegExp(/^([+,0-9]{1})([0-9][-, ]?)*$/);
  private specialKeys: Array<string> = ["Backspace", "Tab", "End", "'Home", "ArrowLeft", "ArrowRight", "Delete",];
  private isControlV: boolean = false;  //para validar si se hizo control v con el teclado y evitar pegue con el mause.
  @Input() maxLength: number = 0;

  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent) {
    this.isControlV = false;
    if (this.specialKeys.includes(e.key)) return;
    if (e.ctrlKey && e.key === "v") this.isControlV = true;
  }

  @HostListener('keypress', ['$event']) onKeyPress(e: KeyboardEvent) {
    let nuevo = this.el.nativeElement.value + e.key;
    let auxReg = this.regex.test(nuevo);
    if (!auxReg || (this.maxLength && nuevo.length > this.maxLength)) {
      e.preventDefault();
      return;
    }
  }

  @HostListener('keyup', ['$event']) onKeyup(e: KeyboardEvent) {
    let nuevo: string = this.el.nativeElement.value;
    if (e.ctrlKey && e.key === "v") {
      if (!this.regex.test(nuevo)) {
        this.el.nativeElement.value = "";
        e.preventDefault();
        return;
      } else if (this.maxLength && nuevo.length > this.maxLength) {
        this.el.nativeElement.value = nuevo.substring(0,this.maxLength);
        e.preventDefault();
        return;
      }
    }
  }

  @HostListener('paste', ['$event']) onPaste(e: ClipboardEvent) {
    if (this.isControlV) {
      this.isControlV = false;
      return true;
    } else {
      return false;
    }
  }

}
