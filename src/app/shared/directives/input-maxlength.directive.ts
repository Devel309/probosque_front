import {
  AfterViewInit, Directive, ElementRef, HostListener, Input, OnInit, Renderer2, Optional, OnDestroy
} from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({
  selector: '[inputMaxLength]'
})
export class InputMaxlengthDirective implements OnInit {

  maxLengthInput = "20";

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    @Optional() private ngModel: NgModel,
  ) {}



  ngOnInit() {
    this.renderer.setAttribute(this.el.nativeElement, 'maxLength', this.maxLengthInput);
  }


}
