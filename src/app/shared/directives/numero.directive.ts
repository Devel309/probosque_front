import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[numero]'
})
export class NumeroDirective {
    private regex: RegExp = new RegExp(/[0-9]/g);
    private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home'];
    private numP1ad: Array<string> = [
        'Numpad0',
        'Numpad1',
        'Numpad2',
        'Numpad3',
        'Numpad4',
        'Numpad5',
        'Numpad6',
        'Numpad7',
        'Numpad8',
        'Numpad9',

    ];
    @Input() maxLength!: number;
    @Input() min!: number;
    @Input() max!: number;

    constructor(private el: ElementRef) { }
    @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent) {

        if (e.altKey == true && this.numP1ad.includes(e.code)) {
            this.el.nativeElement.value = null;
            e.preventDefault();
        }

        if (this.specialKeys.indexOf(e.key) !== -1) {
            return;
        } else if (
            // to allow numbers
            (e.keyCode >= 48 && e.keyCode <= 57) ||
            // to allow numpad number
            (e.keyCode >= 96 && e.keyCode <= 105)
        ) {
            if (e.altKey == true && this.numP1ad.includes(e.code)) {
                this.el.nativeElement.value = null;
                e.preventDefault();
            }
        } else {
            e.preventDefault();
        }
        let current: string = this.el.nativeElement.value;



        if (e.altKey) {
            this.el.nativeElement.value = null;

            e.preventDefault();
        }

        let next: string = current.concat(e.key);

        if (
            (next && !String(next).match(this.regex)) ||
            (this.maxLength && next.length > this.maxLength) ||
            (this.min && +next < this.min) ||
            (this.max && +next >= this.max)
        ) {
            e.preventDefault();
        }

    }
    @HostListener('keyup', ['$event']) onKeyUp(e: KeyboardEvent) {
        if (e.altKey == true && this.numP1ad.includes(e.code)) {
            this.el.nativeElement.value = null;
            e.preventDefault();
        }
        if (isNaN(Number(this.el.nativeElement.value))) {
            this.el.nativeElement.value = null;
            e.preventDefault();
        }
    }

    @HostListener('keypress', ['$event']) onKeyPress(e: KeyboardEvent) {
        if (e.altKey == true && this.numP1ad.includes(e.code)) {
            this.el.nativeElement.value = null;
            e.preventDefault();
        }

        let next: string = this.el.nativeElement.value;
        if (isNaN(Number(next))) {
            this.el.nativeElement.value = null;
            e.preventDefault();
        }
    }
}
