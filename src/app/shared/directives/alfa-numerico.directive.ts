import { Directive, ElementRef, HostListener, Input } from "@angular/core";

@Directive({
	selector: "[alfaNumerico]",
})
export class AlfaNumericoDirective {
	private regex: RegExp = new RegExp(/^[a-z0-9]+$/i);
	// Allow key codes for special events. Reflect :
	private specialKeys: Array<number> = [
		45, // insert
		46, // del-suprimir
		8, // backspace-retroceso
		9, // tab
		13, // enter
		// 110,//punto
		// 190,// punto
		35, //end - fin
		36, // home inicio
	];
	private caracteresPermitidos: string[] = [];

	@Input() maxLength!: number;

	@Input() set permitir(caracteres: string) {
		if (caracteres)
			[...caracteres].forEach((c) => this.caracteresPermitidos.push(c));
	}

	constructor(private el: ElementRef) {}
	@HostListener("keydown", ["$event"])
	onKeyDown(e: KeyboardEvent) {
		e = <KeyboardEvent>event;

		// tecla alt
		if (e.altKey) e.preventDefault();
		// punto .
		if (e.keyCode == 190 || e.keyCode == 110) e.preventDefault();

		if (
			//caracteres permitidos
			this.caracteresPermitidos.indexOf(e.key) > -1 ||
			// to allow backspace, enter, escape, arrows
			this.specialKeys.indexOf(e.which) > -1 ||
			// Allow: Ctrl+A
			(e.which == 65 && e.ctrlKey == true) ||
			// Allow: Ctrl+C
			(e.which == 67 && e.ctrlKey == true) ||
			// Allow: Ctrl+X
			(e.which == 88 && e.ctrlKey == true) ||
			// Allow: Ctrl+V
			(e.which == 86 && e.ctrlKey == true)
		) {
			return;
		} else if (
			// números
			(48 <= e.which && e.which <= 57) ||
			// to allow numpad number
			(e.which >= 96 && e.which <= 105) ||
			// mayúsculas
			(65 <= e.which && e.which <= 90) ||
			// minúsculas
			(97 <= e.which && e.which <= 122)
		) {
		} else {
			e.preventDefault();
		}

		let current: string = this.el.nativeElement.value;
		let next: string = current.concat(e.key);

		if (
			(next && !String(next).match(this.regex)) ||
			(this.maxLength && next.length > this.maxLength)
		)
			e.preventDefault();
	}
}
