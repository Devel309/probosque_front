import { Directive, ElementRef, HostListener, Input } from "@angular/core";

/**
 * No usar con input type=number, no funcionará,
 * preferentemente usar con input type=text
 */
@Directive({
	selector: "[decimal]",
})
export class DecimalDirective {
	// TECLAS PERMITIDAS
	/*private specialKeys: Array<string> = [
		"Backspace",
		"Tab",
		"Delete",
		"Insert",
	];*/

	@Input() min!: number;
	@Input() max!: number;
	@Input() decimalPlaces!: number;
  @Input("negative") negative: number = 0;
	constructor(private el: ElementRef) { }



  private checkAllowNegative(value: string) {
    if (this.decimalPlaces <= 0) {
      return String(value).match(new RegExp(/^-?\d+$/));
    } else {
      var regExpString =
        "^-?\\s*((\\d+(\\.\\d{0," +
        this.decimalPlaces +
        "})?)|((\\d*(\\.\\d{1," +
        this.decimalPlaces +
        "}))))\\s*$";
      return String(value).match(new RegExp(regExpString));
    }
  }

  private check(value: string) {
    if (this.decimalPlaces <= 0) {
      return String(value).match(new RegExp(/^\d+$/));
    } else {
      var regExpString =
        "^\\s*((\\d+(\\.\\d{0," +
        this.decimalPlaces +
        "})?)|((\\d*(\\.\\d{1," +
        this.decimalPlaces +
        "}))))\\s*$";
      return String(value).match(new RegExp(regExpString));
    }
  }

  private run(oldValue:any) {
    setTimeout(() => {
      let currentValue: string = this.el.nativeElement.value;
      let allowNegative = this.negative > 0 ? true : false;

      if (allowNegative) {
        if (
          !["", "-"].includes(currentValue) &&
          !this.checkAllowNegative(currentValue)
        ) {
          this.el.nativeElement.value = oldValue;
        }
      } else {
        if (currentValue !== "" && !this.check(currentValue)) {
          this.el.nativeElement.value = oldValue;
        }
      }
    });
  }


  @HostListener("keydown", ["$event"])
  onKeyDown(event: KeyboardEvent) {
    this.run(this.el.nativeElement.value);
  }

  @HostListener("paste", ["$event"])
  onPaste(event: ClipboardEvent) {
    this.run(this.el.nativeElement.value);
  }




/*
	@HostListener("keydown", ["$event"]) onKeyDown(event: KeyboardEvent) {
		// Si es una tecla permitida continuar
		if (this.specialKeys.indexOf(event.key) !== -1) {
			return;
		}

		// si no es numero no continuar, excepto "." y "-"
		if (!(event.key === "." || event.key === "-" ) && isNaN(Number(event.key))) {
			event.preventDefault();
			return;
		}

		let current: string = this.el.nativeElement.value;
		let next: string = current.concat(event.key);

		//validar signo menos
		if (current.includes("-") && event.key == "-") event.preventDefault();
		if (current.length >= 1 && event.key == "-") event.preventDefault();

		// validar punto decimal
		if (current.includes(".") && event.key == ".") {
			event.preventDefault();
		} else if (!current.includes(".") && event.key == ".") {
			return;
		}

		// validar que las teclas sean solo números, punto y el signo menos
		if (
			(event.which >= 48 && event.which <= 57) ||
			(event.which >= 96 && event.which <= 105) ||
			event.key == "-" ||
			event.key == "."
		) {
		} else {
			event.preventDefault();
		}

		//validar rango máximo y mínimo
		if ((this.min && +next < this.min) || (this.max && +next > this.max)) {
			event.preventDefault();
		}

		// validar número de decimales
		const numero = next.split(".");
		const parteDecimal = numero[1]?.length;
		if (this.decimalPlaces && this.decimalPlaces < parteDecimal) {
			event.preventDefault();
		}
	}*/
}
