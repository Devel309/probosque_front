import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[sinSpacioInit]'
})
export class SinSpacioInitDirective {

  // private regex: RegExp = new RegExp(/^[A-Za-z\.\s]*$/);

  constructor(private el: ElementRef) { }
  @HostListener("keydown", ["$event"])
  onKeyDown(e: KeyboardEvent) {
		e = <KeyboardEvent>event;

		let current: string = this.el.nativeElement.value;
		let next: string = current.concat(e.key);

    if(current.length === 0) {
      if(next === " ") {
        e.preventDefault();
      }
    }
    
		// if (
		// 	(next && !String(next).match(this.regex)) ||
		// 	(this.max && next.length > this.max)
		// )		e.preventDefault();
	}

}
