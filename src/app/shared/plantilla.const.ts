export abstract class PlantillaConst {
  static readonly PlantillaCargaInventarioCenso: string = "Plantilla_CargaInventarioCenso_v1.00_22-04-2022_203352.xlsx";
  //Epica 3.1.1
  static readonly PGMF_TAB_4_3_1: string = "plantilla-PGMF-ProteccionVigilancia-UbicacionMarcadoVertices_22-04-2022_204711.xlsx";
  static readonly PGMF_TAB_4_3_2: string = "plantilla-PGMF-ProteccionVigilancia-Señalizacion_22-04-2022_204831.xlsx";
  static readonly PGMF_TAB_4_3_3: string = "plantilla-PGMF-ProteccionVigilancia-DemarcacionLinderos_22-04-2022_204935.xlsx";
  static readonly PGMF_TAB_4_3_4: string = "plantilla-PGMF-ProteccionVigilancia-VigilanciaUMF_22-04-2022_205103.xlsx";
  static readonly PGMF_TAB_5_0: string = PlantillaConst.PlantillaCargaInventarioCenso;
  static readonly PGMF_TAB_7_2: string = "Plantilla formato Plan de Gestion Ambiental_22-04-2022_202224.xlsx";
  static readonly PGMF_TAB_7_2_1: string = "Plantilla formato Plan de Accion Preventivo-Corrector_22-04-2022_195653.xlsx";
  static readonly PGMF_TAB_7_2_2: string = "Plantilla formato Plan de Vigilancia y Seguimiento_22-04-2022_202311.xlsx";
  static readonly PGMF_TAB_7_2_3: string = "Plantilla formato Plan de Contingencia Ambiental_22-04-2022_201827.xlsx";
  static readonly PGMF_TAB_12_1: string = "formatoProgramaInversiones_22-04-2022_194303.xlsx";
  //Epica 3.1.4
  static readonly N314_TAB_4_0: string = PlantillaConst.PlantillaCargaInventarioCenso;
  //Epica 2.1
  static readonly PlantillaDeclaracionJuradaPepequenoExtractor: string = "DECLARACIÓN_JURADA_PEQUEÑO_EXTRACTOR_05-05-2022_104158.docx";
  static readonly PlantillaSolicitudInteresBosqueLocal: string ="Solicitud_de_interes_05-05-2022_110531.docx";
  static readonly PlantillaResolucionDeNegacion: string ="RESOLUCION_DENEGACION_12-05-2022_115635.docx";
}
