import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { varLocalStor } from '../util-local-storage';

@Injectable({ providedIn: 'root' })
export class IrPlanService {

  constructor(
    private router: Router,
  ) { }

  /**
   * Ir a la ruta del Plan de manejo Solicitud Concesión y setear variables en el local storage
   * @param isNuevaVentana si se va abrir la ruta en una nueva ventana "TRUE", sino "FALSE"
   * @param irUrl url a redirigir
   * @param idPlan id del plan de manejo
   * @param disabledFormu para bloquear la parte de los formularios enviar "TRUE", sino "FALSE"
   * @param disabledEval para bloquear la parte de evaluación enviar "TRUE", sino "FALSE"
   * @param showEval Para mostrar la parte de evaluación enviar "TRUE", sino "FALSE"
   * @param isSolicitudHija si es una solicitud hija enviar "TRUE", sino "FALSE"
   * @param isAnexo6 si es ver anexo 6 enviar "TRUE", sino "FALSE"
   */
   irFormSolicitudConcesion_LS(
    isNuevaVentana: boolean, irUrl: string,
    idPlan: number, disabledFormu: boolean, disabledEval: boolean, showEval: boolean,
    isSolicitudHija: boolean
  ) {
    let auxData = {
      SCidSolicitud: idPlan,
      SCdisabledForm: disabledFormu,
      SCdisabledEval: disabledEval,
      SCshowEval: showEval,
      isSolicitudHija: isSolicitudHija,
    }
    localStorage.setItem(varLocalStor.BJA_SOL_CONCESION, JSON.stringify(auxData));
    if (isNuevaVentana) this.irNewRutaPlan(irUrl, idPlan)
    else this.irRutaPlan(irUrl, idPlan)
  }

  /**
   * Ir a la ruta del Plan de manejo Solicitud de Plantaciones Forestales y setear variables en el local storage
   * @param isNuevaVentana si se va abrir la ruta en una nueva ventana "TRUE", sino "FALSE"
   * @param idPlan id del plan de manejo
   * @param disabledFormu para bloquear la parte de los formularios enviar "TRUE", sino "FALSE"
   * @param disabledEval para bloquear la parte de evaluación enviar "TRUE", sino "FALSE"
   * @param showEval Para mostrar la parte de evaluación enviar "TRUE", sino "FALSE"
   * @param estadoSolicitud estado de la solicitud
   * @param isTM_Predio si es tipo modalidad Predio Privado enviar "TRUE", sino "FALSE"
   * @param isTM_TH si es tipo modalidad Título Habilitante enviar "TRUE", sino "FALSE"
   * @param isTM_Dominio si es tipo modalidad Dominio Público enviar "TRUE", sino "FALSE"
   * @param isAnexo3 si es anexo 3
   * @param estadoOpcion estado opción
   */
  irFormPlantacionForestal_LS(
    isNuevaVentana: boolean,
    idPlan: number, disabledFormu: boolean, disabledEval: boolean, showEval: boolean, estadoSolicitud: string,
    isTM_Predio: boolean, isTM_TH: boolean, isTM_Dominio: boolean, isAnexo3: boolean, estadoOpcion: string,
    isSolicitudHija: boolean
  ) {
    let auData = {
      BPFidSolicitud: idPlan,
      BPFdisabledForm: disabledFormu,
      BPFdisabledEval: disabledEval,
      BPFshowEval: showEval,
      isTM_Predio: isTM_Predio,
      isTM_TH: isTM_TH,
      isTM_Dominio: isTM_Dominio,
      isAnexo3: isAnexo3,
      estSolicitud: estadoSolicitud,
      estadoOpcion: estadoOpcion,
      isSolicitudHija: isSolicitudHija
    }

    localStorage.setItem(varLocalStor.BJA_PLANT_FORE, JSON.stringify(auData));
    if (isNuevaVentana) this.irNewRutaPlan('/plantacion/registro-plantacion-forestal', idPlan)
    else this.irRutaPlan('/plantacion/registro-plantacion-forestal', idPlan)
  }

  /**
   * Ir a la ruta del Plan de manejo Solicitud de Plantaciones Forestales y setear variables en el local storage
   * @param isNuevaVentana si se va abrir la ruta en una nueva ventana "TRUE", sino "FALSE"
   * @param idPlan id del plan de manejo
   * @param estadoSolicitud estado de la solicitud
   * @param disabledFormu para bloquear la parte de los formularios enviar "TRUE", sino "FALSE"
   * @param disabledEval para bloquear la parte de evaluación (tab2) enviar "TRUE", sino "FALSE"
   * @param showEval Para mostrar la parte de evaluación (tab2) enviar "TRUE", sino "FALSE"
   * @param showEstudioTec Para mostrar campos de estudio tecnico (tab3) enviar "TRUE", sino "FALSE"
   * @param disabledEstudioTec Para bloquear campos de estudio tecnico (tab3) enviar "TRUE", sino "FALSE"
   * @param showIniciativa Para mostrar la parte de iniciativa (tab1) enviar "TRUE", sino "FALSE"
   */
   irFormBosqueLocal_LS(
    isNuevaVentana: boolean,
    idPlan: number, estadoSolicitud: string,
    disabledFormu: boolean,
    disabledEval: boolean, showEval: boolean,
    showEstudioTec: boolean, disabledEstudioTec: boolean,
    showIniciativa: boolean, evaluacionGabinete: boolean
  ) {
    let auData = {
      idSolicitud: idPlan,
      estadoSolicitud: estadoSolicitud,
      disabledForm: disabledFormu,
      disabledEval: disabledEval,
      showIniciativa: showIniciativa,
      showEval: showEval,
      showEstudioTec: showEstudioTec,
      disabledEstudioTec: disabledEstudioTec,
      evaluacionGabinete: evaluacionGabinete,
    }

    localStorage.setItem(varLocalStor.BJA_SOL_BL, JSON.stringify(auData));
    if (isNuevaVentana) this.irNewRutaPlan('/bosque-local/registro-establecimiento-BL', idPlan)
    else this.irRutaPlan('/bosque-local/registro-establecimiento-BL', idPlan)
  }

  /**
   * Ir a la ruta del Plan de manejo Solicitud de Plantaciones Forestales y setear variables en el local storage
   * @param isNuevaVentana si se va abrir la ruta en una nueva ventana "TRUE", sino "FALSE"
   * @param idPlan id del plan de manejo
   * @param estadoSolicitud estado de la solicitud
   * @param disabledEstudioTec Para bloquear campos de estudio tecnico enviar "TRUE", sino "FALSE"
   * @param showEvalComiteTec Para mostrar la parte de evaluación enviar "TRUE", sino "FALSE"
   * @param disabledEvalComiteTec para bloquear la parte de evaluación enviar "TRUE", sino "FALSE"
   * @param evaluacionGabinete
   * @param evaluacionCampo
   * @param showEnvioResultado
   * @param disabledEnvioResultado
   * @param showEstableciBL
   * @param disabledEstableciBL
   */
   irEvalComiteTecnicoBosqueLocal_LS(
    isNuevaVentana: boolean, idPlan: number, estadoSolicitud: string,
    disabledEstudioTec: boolean, showEvalComiteTec: boolean, disabledEvalComiteTec: boolean, evaluacionGabinete: boolean, evaluacionCampo: boolean,
    showEnvioResultado: boolean, disabledEnvioResultado: boolean, showEstableciBL: boolean, disabledEstableciBL: boolean
  ) {
    let auData = {
      idSolicitud: idPlan, estadoSolicitud: estadoSolicitud,
      disabledEstudioTec: disabledEstudioTec, showEvalComiteTec: showEvalComiteTec, disabledEvalComiteTec: disabledEvalComiteTec,
      evaluacionGabinete: evaluacionGabinete, evaluacionCampo: evaluacionCampo, 
      showEnvioResultado: showEnvioResultado, disabledEnvioResultado: disabledEnvioResultado, 
      showEstableciBL: showEstableciBL, disabledEstableciBL: disabledEstableciBL
    }
    localStorage.setItem(varLocalStor.BJA_SOL_EEVAL_CT_BL, JSON.stringify(auData));
    if (isNuevaVentana) this.irNewRutaPlan('/bosque-local/evaluacion-comite-tecnico-BL', idPlan)
    else this.irRutaPlan('/bosque-local/evaluacion-comite-tecnico-BL', idPlan)
  }

  
  /**
   * Ir a la ruta en la misma ventana
   * @param url url del la ruta a redirigir (Ejm: "/nombre/ruta")
   * @param id (opcional) parámetro de la ruta "id numérico"
   */
  private irRutaPlan(url: string, id?: number) {
    if(id) this.router.navigate([url, id])
    else this.router.navigate([url])
  }

  /**
   * Ir a la ruta en una nueva ventana
   * @param url url del la ruta a redirigir
   * @param id (opcional) parámetro de la ruta "id numérico"
   */
  private irNewRutaPlan(url: string, id: number) {
    let auxUrl = "";
    if(id) auxUrl = this.router.serializeUrl(this.router.createUrlTree([url, id]))
    else auxUrl = this.router.serializeUrl(this.router.createUrlTree([url]))
    window.open(auxUrl, '_blank');
  }

}

