import { ElementRef, Injectable } from '@angular/core';
import { defer, Observable, of } from 'rxjs';

import Color from "@arcgis/core/Color";
import Collection from "@arcgis/core/core/Collection";
import * as geometryEngine from '@arcgis/core/geometry/geometryEngine';
import Graphic from "@arcgis/core/Graphic";
import GeoJSONLayer from "@arcgis/core/layers/GeoJSONLayer";
import GraphicsLayer from "@arcgis/core/layers/GraphicsLayer";
import Map from "@arcgis/core/Map";
import SimpleRenderer from "@arcgis/core/renderers/SimpleRenderer";
import SimpleFillSymbol from "@arcgis/core/symbols/SimpleFillSymbol";
import SimpleLineSymbol from "@arcgis/core/symbols/SimpleLineSymbol";
import SpatialReference from "@arcgis/core/geometry/SpatialReference";
import SimpleMarkerSymbol from "@arcgis/core/symbols/SimpleMarkerSymbol";
import MapView from "@arcgis/core/views/MapView";
import Compass from "@arcgis/core/widgets/Compass";

import Layer from '@arcgis/core/layers/Layer';
import Polygon from "@arcgis/core/geometry/Polygon";
import Point from "@arcgis/core/geometry/Point";

import { ToastService } from '../services/toast.service';
import { isNull } from '../util';
import { Feature, FeatureCollection } from '../models/feature-collection';
import { FigureTypes, UnitMetric } from '../models/const';

declare const shp: any;


@Injectable({ providedIn: 'root' })
export class MapService {

  // WGS84 (wkid: 4326)  
  WKID = { wkid: 4326 };
  constructor(private toast: ToastService) { }

  initializeMap(container: ElementRef, center?: any, zoom?: number): MapView {
    center = center ? center : [-75, -8.2];//PERÚ
    zoom = zoom ? zoom : 6;
    const map = new Map({ basemap: "hybrid", });
    const view = new MapView({ container: container as any, map, center, zoom, });
    const compass = new Compass({ view });
    view.ui.add(compass, "top-right");
    return view;
  }

  addLayer(view: MapView, f: FeatureCollection, groupId: number | string | null, color?: string): Observable<__esri.Layer> {
    /* No se pintan todas las capas si no se pone */
    f.features.forEach((t, i) => { t.properties.OBJECTID = i + 1 });

    const blob = new Blob([JSON.stringify(f)], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const _color = color ? color : this.randomHex();
    let geoJson = this.geoJsonLayer(url);
    geoJson.visible = true;
    geoJson.title = f.fileName;
    geoJson.set('ID', geoJson.id);
    geoJson.set('layerName', f.fileName);
    geoJson.set('layerType', 'vector');
    geoJson.set('groupId', groupId);
    geoJson.set('color', _color);
    geoJson.set('opacity', 0.8);
    geoJson.set('attributes', f.features);
    geoJson.set('popupTemplate', { title: f.fileName });
    geoJson.set('areaHa', this.calculateAreaHA(f));
    view.map.add(geoJson);
    return defer(() => geoJson.when((data: any) => {
      URL.revokeObjectURL(url);
      this.setColorLayer(view, geoJson.id, _color);
      try {
        view.goTo({ target: data.fullExtent });
      } catch (error) {
        console.error('No se pudo centrar el mapa');
        console.error(error);
      }
      return data;
    }).catch((error: any) => console.log(error)));
  }

  getLayer(id: string, view: MapView): __esri.Layer {
    return view.map.allLayers.find(l => l.id === id) || null;
  }
  getLayerByTitle(title: string, view: MapView): Layer {
    return view.map.editableLayers.find(x => x.title == title) || null;
  }

  getLayers(id: string, view: MapView): Collection<__esri.Layer> {
    return view.map.allLayers.filter(l => l.id === id);
  }

  getAllLayers(groupId: string | null, view: MapView): Collection<Layer> {
    return view.map.editableLayers.filter(x => x.get('groupId') == groupId);
  }

  removeLayer(id: string, view: MapView) {
    let layer = view.map.allLayers.find(l => l.id === id) || null;
    if (isNull(layer)) return;
    view.map.layers.remove(layer);
    layer.visible = false;
  }
  toggleLayer(id: string, visible: boolean, view: MapView) {
    let layer: any = view.map.allLayers.find(l => l.id === id) || null;
    if (layer !== null) layer.visible = visible;
  }
  calculateArea(polygon: __esri.Polygon, unit: __esri.ArealUnits | number) {
    return geometryEngine.geodesicArea(polygon, unit);
  }

  calculateAreaPoints(features: Feature[]) {
    const polygon = this.createPolygon(features)
    return geometryEngine.geodesicArea(polygon, UnitMetric.HA);
  }
  calculateAreaPoints2(coordinates: any) {
    const polygon = this.createPolygon2(coordinates)
    return geometryEngine.geodesicArea(polygon, UnitMetric.HA);
  }

  calculateAreaHA(feature: FeatureCollection): number {
    const features = feature?.features ? feature.features : [];
    let totalArea = 0;
    features.forEach(f => {
      if (f.geometry.type == FigureTypes.POLYGON) {
        let polygon: Polygon = new Polygon();
        polygon.spatialReference = new SpatialReference({ wkid: 4326 })
        polygon.rings = f.geometry.coordinates;
        let area = this.calculateArea(polygon, UnitMetric.HA);
        totalArea += area;
      } else if (f.geometry.type == FigureTypes.MULTI_POLYGON) {
        const features = f.geometry.coordinates;
        features.forEach(coordinates => {
          let polygon: Polygon = new Polygon();
          polygon.spatialReference = new SpatialReference(this.WKID);
          polygon.rings = coordinates;
          let area = this.calculateArea(polygon, UnitMetric.HA);
          totalArea += area;
        });
      } else {
        totalArea += 0;
      }
    });
    return totalArea;

  }
  calculateAreaPolygon(feature: Feature): number {
    if ((feature.geometry.type == FigureTypes.POLYGON)) {
      let polygon: Polygon = new Polygon();
      polygon.spatialReference = new SpatialReference(this.WKID)
      polygon.rings = feature.geometry.coordinates;
      return this.calculateArea(polygon, UnitMetric.HA);
    }
    if ((feature.geometry.type == FigureTypes.MULTI_POLYGON)) {
      const features = feature?.geometry?.coordinates;
      let totalArea = 0;
      features.forEach(coordinates => {
        let polygon: Polygon = new Polygon();
        polygon.spatialReference = new SpatialReference(this.WKID);
        polygon.rings = coordinates;
        let area = this.calculateArea(polygon, UnitMetric.HA);
        totalArea += area;
      });
      return totalArea;
    }

    return 0;
  }


  validateFileSHPAndShowToast(file?: File): boolean {

    if (isNull(file)) {
      this.toast.warn('Seleccione un archivo');
      return false;
    }
    if (!file?.name.endsWith(".zip")) {
      this.toast.warn("Seleccione un archivo");
      return false;
    }
    return true;
  }

  getDataShp(bytes: ArrayBuffer): FeatureCollection[] {
    const data = shp.parseZip(bytes);
    return (Array.isArray(data) ? data : [data]) as FeatureCollection[];
  }

  /**
   * Setea el color a una capa del mapa,buscando la capa por  id de capa
   * @param view vista mapa
   * @param id id de la capa
   * @param color color    
   */
  setColorLayer(view: MapView, id: string, color: string) {
    let layer = view.map.allLayers.find((t) => t.id === id) || null;
    if (layer === null) return;
    layer.get<SimpleRenderer>('renderer').symbol.color = this.color(color);
  }

  /**
   * Setea el color de una capa, en base a la capa que se le pasa como parámetro
   * @param layer capa en donde se quiere cambiar el color
   * @param color color que se setear a la capa
   */
  setLayerColor(layer: Layer, color: string) {
    layer.get<SimpleRenderer>('renderer').symbol.color = this.color(color);
  }


  //#region UTILITARIOS
  fileRead(file: File | Blob): Observable<ArrayBuffer> {
    return defer(() => this.readFile(file))
  }
  readFile(file: File | Blob): Promise<ArrayBuffer> {
    return new Promise((resolve, reject) => {
      let fileReader = new FileReader();
      fileReader.onloadend = (e: any) => resolve(e.target.result);
      fileReader.onerror = fileReader.onabort = reject;
      fileReader.readAsArrayBuffer(file);
    });
  }

  createPolygon(features: Feature[]): Polygon {
    let ring: number[][] = [];
    features.forEach(f => {
      if ((f.geometry.type == FigureTypes.POINT))
        ring.push(f.geometry.coordinates);
    });
    return new Polygon({
      hasZ: true,
      hasM: true,
      rings: [ring],
      spatialReference: this.WKID
    });
  }
  createPolygon2(coordinates: Feature[]): Polygon {
    let ring: number[][] = [];
    coordinates.forEach((f:any) => {
        ring.push(f);
    });
    return new Polygon({
      hasZ: true,
      hasM: true,
      rings: [ring],
      spatialReference: this.WKID
    });
  }

  geoJsonLayer(url: string) {
    return new GeoJSONLayer({ url });
  }
  graphicsLayer() {
    return new GraphicsLayer();
  }
  graphic(geometry: __esri.Geometry, symbol: __esri.Symbol) {
    return new Graphic({ geometry, symbol });
  }
  simpleMarkerSymbol() {
    return new SimpleMarkerSymbol();
  }
  simpleFillSymbol() {
    return new SimpleFillSymbol();
  }
  simpleLineSymbol() {
    return new SimpleLineSymbol();
  }
  simpleRenderer(symbol: __esri.Symbol) {
    return new SimpleRenderer({ symbol });
  }

  color(color: string): Color {
    return Color.fromHex(color);
  }

  get newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  randomHex() {
    var length = 6;
    var chars = "0123456789ABCDEF";
    var hex = "#";
    while (length--) hex += chars[(Math.random() * 16) | 0];
    return hex;
  }

  //#endregion

}