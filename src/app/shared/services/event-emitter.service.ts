import { EventEmitter, Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  laboresSilviculturalesListarFunction = new EventEmitter();
  aprovechamientoListarFunction = new EventEmitter();

  // subsVar: Subscription = new Subscription();    
  subsLabores !: Subscription;
  subsEtapa !: Subscription;
    
  constructor() { }    
    
  onLlenarLaboresSilviculturales(idPlanManejo:number){
    this.laboresSilviculturalesListarFunction.emit(idPlanManejo);
  }

  onLlenarEtapaAprovechamiento(idPlanManejo: number){
    this.aprovechamientoListarFunction.emit(idPlanManejo);
  }
}
