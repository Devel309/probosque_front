import { Injectable } from '@angular/core';

import { MessageService } from "primeng/api";
import { ToastType } from '../models/const';
import { isNullOrEmpty } from '../util';

@Injectable()
export class ToastService {

  constructor(private messageService: MessageService) {

  }

  /**
   * Crea un toast de tipo success
   * @param mensaje Mensaje del toast
   * @param titulo Título del toast
   */
  ok(mensaje: string = '', titulo: string = '') {
    this.toast(ToastType.success, mensaje, titulo);
  }

  /**
   * Crea un toast de tipo info
   * @param mensaje Mensaje del toast
   * @param titulo Título del toast
   */
  info(mensaje: string = '', titulo: string = '') {
    this.toast(ToastType.info, mensaje, titulo);
  }

  /**
   * Crea un toast de tipo warn
   * @param mensaje Mensaje del toast
   * @param titulo Título del toast
   */
  warn(mensaje: string = '', titulo: string = '') {
    this.toast(ToastType.warn, mensaje, titulo);
  }

  /**
   * Crea un toast de tipo error
   * @param mensaje Mensaje del toast
   * @param titulo Título del toast
   */
  error(mensaje: string = '', titulo: string = '') {
    this.toast(ToastType.error, mensaje, titulo);
  }

  /**
   * 
   * @param type Tipo de toast: ToastType(success, info, warn, error)
   * @param mensaje Mensaje del toast
   * @param titulo Título del toast
   */
  toast(type: ToastType, mensaje: string = '', titulo: string = '') {
    this.messageService.add({
      key: 'global-toast',
      severity: type,
      summary: titulo,
      detail: mensaje,
    });
  }

  /**
   * Función que sirve para validar las propiedades de un objeto, valida si son: null | undefined | "", muestra un
   * toast para cada error.
   * @param keys Propiedades del objeto a validar ej:['nombre','descripcion']
   * @param object Objeto que se desea validar
   * @param dictionary Objeto clave valor, para poner descripción del campo ej:{nombre:'Nombre',descripcion:'Descripción'}
   * @returns Retorna true, si los atributos del objeto son válidos y false si no lo son
   */
  validAndShowError<T>(keys: string[], object: T, dictionary: { [key: string]: string }): boolean {
    let isValid = true;
    for (const _key of keys) {
      const key = _key as keyof T;
      if (isNullOrEmpty(object[key])) {
        isValid = false;
        this.warn(`(*) El campo ${dictionary[_key]}, es obligatorio`);
      }
    }
    return isValid;
  }

  /**
   * Función que sirve para validar las propiedades de un objeto, valida si son: null | undefined | "". 
   * Muestra solo un toast para todos los errores, separado por comas.
   * @param keys Propiedades del objeto a validar ej:['nombre','descripcion']
   * @param object Objeto que se desea validar
   * @param dictionary Objeto clave valor, para poner descripción del campo ej:{nombre:'Nombre',descripcion:'Descripción'}
   * @returns Retorna true, si los atributos del objeto son válidos y false si no lo son
   */
  validAndShowOneToastError<T>(keys: string[], object: T, dictionary: { [key: string]: string }): boolean {
    let isValid = true;
    let errors: string[] = [];
    for (const _key of keys) {
      const key = _key as keyof T;
      if (isNullOrEmpty(object[key])) {
        isValid = false;
        errors.push(_key);
      }
    }
    // if (errors.length == 1) {
    //   this.warn(`(*)El campo ${dictionary[errors[0]]}, es obligatorio`);
    // } else if (errors.length > 1) {
    //   const _errors = errors.map(e => dictionary[e]).join(', ');
    //   // this.warn(`(*)Los campos: ${_errors}; Son obligatorios`,'Campos Obligatorios');
    //   this.warn(`${_errors}`,'(*)Campos Obligatorios:');
    // }
    if (errors.length >= 1) {
      const _errors = errors.map(e => dictionary[e]).join(', ');
      // this.warn(`(*)Los campos: ${_errors}; Son obligatorios`,'Campos Obligatorios');
      this.warn(`${_errors}`, '(*)Campos Obligatorios:');
    }
    return isValid;
  }

}

