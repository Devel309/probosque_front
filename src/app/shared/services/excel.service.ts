import { Injectable } from '@angular/core';

import { WorkBook, read, utils } from 'xlsx';
import { ExcelFile } from '../models/excel-file';


@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  excelToJson(file: File): Promise<ExcelFile> {
    let reader = new FileReader();
    let excelData = new ExcelFile();

    reader.readAsBinaryString(file);

    return new Promise((resolve, reject) => {
      reader.onloadend = () => {
        try {
          let data = reader.result;
          let workBook!: WorkBook;
          workBook = read(data, { type: 'binary' });
          excelData.info = workBook;
          workBook.SheetNames.forEach((sheetName) => {
            const sheet = { sheetName, sheetContent: utils.sheet_to_json(workBook.Sheets[sheetName]) };
            excelData.content.push(sheet);
          });
          resolve(excelData);
        } catch (error) {
          reject(error);
        }
      };
      reader.onerror = reject
    });
  }
}

