import { Injectable } from "@angular/core";
import { defer, forkJoin, Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { ZipEntry, } from "../models/zip";
import { downloadBlobFile } from "../util";

declare const zip: any;

@Injectable({
  providedIn: 'root'
})
export class ZipService {

  constructor() {
    zip.workerScriptsPath = 'assets/lib/zip/';
  }

  getEntries(file: File | Blob): Observable<ZipEntry[]> {
    const reader = new zip.BlobReader(file);
    const zipReader = new zip.ZipReader(reader);
    return defer(() => zipReader.getEntries() as Observable<ZipEntry[]>)
  }

  getBlob(entry: ZipEntry): Observable<Blob> {
    return defer(() => (entry as any).getData(new zip.BlobWriter()) as Observable<Blob>)
  }

  createZip() {
    const zipWriter = new zip.ZipWriter(new zip.BlobWriter("application/zip"));
    return zipWriter;
  }

  downloadZipFile(title: string, entries: ZipEntry[]) {
    const blobsSubs: Observable<Blob>[] = [];
    entries.forEach(entry => blobsSubs.push(this.getBlob(entry)));

    let blobWriter = new zip.BlobWriter("application/zip")
    let zipWriter = new zip.ZipWriter(blobWriter);

    forkJoin(blobsSubs).subscribe(async blobs => {
      for (let index = 0; index < blobs.length; index++) {
        const blob = blobs[index];
        const fileName = entries[index].filename;
        try {
          await zipWriter.add(fileName, new zip.BlobReader(blob), { bufferedWrite: true })
        } catch (error) {
          console.log(error);
        }

      }
      this.zipClose(zipWriter)
        .then(res => downloadBlobFile(res, `${title}.zip`))
        .catch(err => console.log(err));
    });
  }

  createZipFile(entries: ZipEntry[]): Promise<Blob> {

    const blobsSubs: Observable<Blob>[] = [];
    entries.forEach(entry => blobsSubs.push(this.getBlob(entry)));

    let blobWriter = new zip.BlobWriter("application/zip")
    let zipWriter = new zip.ZipWriter(blobWriter);
    return new Promise((resolve, reject) => {
      forkJoin(blobsSubs).subscribe(async blobs => {
        for (let index = 0; index < blobs.length; index++) {
          const blob = blobs[index];
          const fileName = entries[index].filename;
          await zipWriter.add(fileName, new zip.BlobReader(blob), { bufferedWrite: true })
            .catch((err: any) => {
              if (String(err).includes(FileErr.fileExist)) reject(`${FileErr.existeArchivo}: ${fileName}`)
              reject(err)
            })
        }
        resolve(zipWriter.close())
      });
    });
  }


  addFileToZip(zipWriter: any, fileName: string, blob: Blob) {
    return defer(() => zipWriter.add(fileName, new zip.BlobReader(blob), { bufferedWrite: true }))
  }

  async zipClose(zipWriter: any) {
    return await zipWriter.close();
  }
}

const FileErr = {
  fileExist: 'File already exists',
  existeArchivo: 'El archivo ya existe'
}