export enum AccionTipo {
  REGISTRAR = 'Registrar',
  EDITAR = 'Editar',
  ELIMINAR = 'Eliminar',
  LISTAR = 'Listar',
}

export enum ParticipacionTipo {
  IMPLEMENTACION = 'POCCMPC1',
  COMITE = 'POCCMPC2',
  PLAN_OPERATIVO = 'POCCMPC3',
}

export enum ParticipacionTipoPGMF {
  FORMULACION = 'PGMFPC1',
  IMPLEMENTACION = 'PGMFPC2',
  COMITE = 'PGMFPC3',
  PLAN = 'PGMFPC4',
}

export enum AprovechamientoTipo {
  PRE_APROVECHAMIENTO = 'PREAPR',
  APROVECHAMIENTO = 'APROVE',
  POST_APROVECHAMIENTO = 'POSTAP',
}

export enum RespuestaTipo {
  SI = 'S',
  NO = 'N',
}

export enum HidrografiaTipo {
  RIO = 'RIO',
  QUEBRADA = 'QUEBRADA',
  LAGUNA = 'LAGUNA',
}

export enum EstadoTipo {
  APROBADO = 'Aprobado',
  DESAPROBADO = 'Desaprobado',
  VIGENTE = 'Vigente',
}

export enum EstadoTipo {
  FAVORABLE = 'Favorable',
  DESFAVORABLE = 'Desfavorable',
}

/**
 * Tipos de Sistema Manejo Forestal
 */
export enum SMFTipo {
  USO_POTENCIAL = 'SMFUSO',
  FIN_MADERABLE = 'SMFAFM',
  FIN_NO_MADERABLE = 'SMFAFNM',
  SILVICULTURAL = 'SMFASC',
  SILVICULTURAL_OBLIGATORIO = 'ACTOBLI',
}

/**
 * Tipos de ARCHIVO
 */
export enum ArchivoTipo {
  INFO_GENERAL = '33',
  SHAPEFILE = '37',
  /**
   * shapefile 3.1.3 HU03
   */
  SHAPEFILE_03 = '39',
}

export enum ArchivoTipoCodigo {
  SHAPEFILE = 'SHAPE',
}

/**
 * Tipos de PGMF ARCHIVO
 */
export enum PGMFArchivoTipo {
  PGMFIG = 'PGMFIG',
  PMFI = 'PMFI',
  POAC = 'POAC',
  PGMF = 'PGMF',
  PGMFA = 'PGMFA',
  POCC = 'POCC',
  PFCR = 'PFCR',
  PFMIC = 'PFMIC',
  DEMAC = 'DEMAC'
}

/**
 * Tipos de PGMFArchivoSubTipo ARCHIVO
 */
export enum PGMFArchivoSubTipo {
  CONTRATO = 'CONTRATO',
  REGENTE = 'REGENTE',
  ARCHIVO = 'ARCHIVO',
}

export enum SortType {
  ASC = 'ASC',
  DESC = 'DESC',
}

/**
 * Tipos de PMFI:Plan Manejo Intermedio
 */
export enum PMFITipo {
  /**
   * Tipo Plan Manejo Intermedio
   *  */
  PMFI = 'PMFI',
  /**
   * Tipo Ubicación Área Predio
   *  */
  PMFI_AP = 'PMFIAP ',
  /**
   * Tipo Ubicación Área Manejo
   *  */
  PMFI_AM = 'PMFIAM',
  /**
   * Tipo Unidad Fisiográfica
   *  */
  PMFI_UF = 'PMFIUF',
}

/**
 * Tipos de POCC:Plan Operativo para concesiones
 */
export enum POCCTipo {
  POCC = 'POCC',

  POCC_AP = 'POCCAP ',

  POCC_AM = 'POCCAM',

  POCC_UF = 'POCCUF',
}

export enum OgcGeometryType {
  POINT = 'POINT',
  LINESTRING = 'LINESTRING',
  POLYLINE = 'POLYLINE',
  POLYGON = 'POLYGON',
  MULTIPOINT = 'MULTIPOINT',
  MULTILINESTRING = 'MULTILINESTRING',
  MULTIPOLYGON = 'MULTIPOLYGON',
}
export enum EsriGeometryType {
  POINT = 'Point',
  LINESTRING = 'Linestring',
  POLYLINE = 'Polyline',
  POLYGON = 'Polygon',
  MULTIPOINT = 'MultiPoint',
  MULTILINESTRING = 'MultiLinestring',
  MULTIPOLYGON = 'MultiPolygon',
}
