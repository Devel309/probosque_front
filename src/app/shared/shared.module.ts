import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
// moduloes
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { BandejaEvaluacionComponent } from './components/bandeja-evaluacion/bandeja-evaluacion.component';
import {BandejaEvaluacionOtorgamientoPermisoComponent} from './components/bandeja-evaluacion-otorgamiento-permiso/bandeja-evaluacion-otorgamiento-permiso.component';
import { AppMapaComponent } from './components/mapa/app-mapa.component';
import { MessageEvaluationComponent } from './components/message-evaluation/message-evaluation.component';
import { ListEspeciesComponent } from './components/solicitud-especie/list-especies/list-especies.component';
import { UploadButtonComponent } from './components/upload-button/upload-button.component';
// componentes
import { UploadInputButtonComponent } from './components/upload-input-button/upload-input-button.component';
// directivas
import { AlfaNumericoDirective } from './directives/alfa-numerico.directive';
import { DecimalDirective } from './directives/decimal.directive';
import { LettersAndSpaceDirective } from './directives/letters-space.directive';
import { NumeroDirective } from './directives/numero.directive';
import { InputMaxlengthDirective } from './directives/input-maxlength.directive';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MultiSelectModule} from 'primeng/multiselect';

import {ButtonEvaluarSolicitudOtorgamientoModule} from './components/button-evaluar-solicitud-otorgamiento/button-evaluar-solicitud-otorgamiento.module';
import { MapCustomComponent } from './components/map-custom/map-custom.component';
import { CargaExcelComponent } from './components/carga-excel/carga-excel.component';
import { ButtonEvaluarImpugnacionModule } from './components/button-evaluar-impugnacion/button-evaluar-impugnacion.module';
import { MapCustomPFDMComponent } from './components/map-custom-pfdm/map-custom-pfdm.component';
import { MapCustomUFComponent } from './components/map-custom-uf/map-custom-uf.component';
import { BandejaPlanManejoSharedComponent } from './components/bandeja-plan-manejo-shared/bandeja-plan-manejo-shared.component';
import { DialogModule } from 'primeng/dialog';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AccordionModule } from 'primeng/accordion';
import { MapCustomMPAFPPComponent } from './components/map-custom-mpafpp/map-custom-mpafpp.component';
import { SeleccionAsociacionModalComponent } from './components/bandeja-plan-manejo-shared/seleccion-asociacion-modal/seleccion-asociacion-modal.component';
import { SinSpacioInitDirective } from './directives/sin-spacio-init.directive';
import { MapCustomSolPlantacionComponent } from './components/map-custom-sol-plantacion/map-custom-sol-plantacion.component';
import { TableTipoBosqueComponent } from './components/tabla-tipo-bosque/tabla-tipo-bosque.component';
import { TelefonoDirective } from './directives/telefono.directive';
import { MapCustomSolBosqueLocalComponent } from './components/map-custom-sol-bosque-local/map-custom-sol-bosque-local.component';
import { MapCustomEvaluacionComponent } from './components/map-custom-evaluacion/map-custom-evaluacion.component';
import { MapCustomAnexoComponent } from './components/map-custom-anexo/map-custom-anexo.component';
import { MapCustomPMFIComponent } from './components/map-custom-pmfi/map-custom-pmfi.component';
import { ButtonEvaluarResolucionModule } from './components/button-evaluar-resolucion/button-evaluar-resolucion.module';
import { TextColorEstadoModule } from './components/text-color-estado/text-color-estado.module';
import { ModalDocumentosDigitalizadosComponent } from './components/modal-documentos-digitalizados/modal-documentos-digitalizados.component';
import {NumericDirective} from './directives/numeric.directive';

const MODULES: any = [
  CommonModule,
  FormsModule,
  ButtonModule,
  TableModule,
  DialogModule,
  RadioButtonModule,
  AccordionModule,
  PaginatorModule,
  ConfirmPopupModule,
  ConfirmDialogModule
];
const COMPONENTS: any = [
  UploadInputButtonComponent,
  UploadButtonComponent,
  AppMapaComponent,
  MapCustomComponent,
  MapCustomPFDMComponent,
  MapCustomMPAFPPComponent,
  MapCustomUFComponent,
  MapCustomSolPlantacionComponent,
  MapCustomSolBosqueLocalComponent,
  MapCustomEvaluacionComponent,
  ListEspeciesComponent,
  BandejaEvaluacionComponent,
  MessageEvaluationComponent,
  BandejaEvaluacionOtorgamientoPermisoComponent,
  CargaExcelComponent,
  BandejaPlanManejoSharedComponent,
  SeleccionAsociacionModalComponent,
  TableTipoBosqueComponent,
  MapCustomAnexoComponent,
  MapCustomPMFIComponent,
  ModalDocumentosDigitalizadosComponent
];
const DIRECTIVES: any = [
  NumeroDirective,
  InputMaxlengthDirective,
  DecimalDirective,
  NumericDirective,
  AlfaNumericoDirective,
  LettersAndSpaceDirective,
  SinSpacioInitDirective,
  TelefonoDirective,
];
const PROVIDERS: any = [ConfirmationService];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES, MapCustomComponent, MapCustomPFDMComponent, MapCustomUFComponent,MapCustomMPAFPPComponent, MapCustomSolPlantacionComponent, MapCustomSolBosqueLocalComponent, MapCustomEvaluacionComponent, MapCustomAnexoComponent, MapCustomPMFIComponent, ModalDocumentosDigitalizadosComponent],
  imports: [...MODULES, MatDatepickerModule, MultiSelectModule, ButtonEvaluarImpugnacionModule, ButtonEvaluarSolicitudOtorgamientoModule, ButtonEvaluarResolucionModule, TextColorEstadoModule],
  exports: [...COMPONENTS, ...DIRECTIVES],
  providers: [...PROVIDERS],
})
export class SharedModule {}
