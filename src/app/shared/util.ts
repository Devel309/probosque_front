import { MessageService } from "primeng/api";

import { ToastService } from "./services/toast.service";

export function ConvertNumberToDate(date: number): Date {
  return new Date(date);
}

export function ConvertDateStringFormat(date: Date): string {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [day, month, year].join("/");
}

export function ConvertDateToString(date: Date): string {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}

export function Base64toBlob(base64Data: string, contentType: string): Blob {
  contentType = contentType || "";
  var sliceSize = 1024;
  var byteCharacters = atob(base64Data.replace(/['"]+/g, ""));
  var bytesLength = byteCharacters.length;
  var slicesCount = Math.ceil(bytesLength / sliceSize);
  var byteArrays = new Array(slicesCount);

  for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
    var begin = sliceIndex * sliceSize;
    var end = Math.min(begin + sliceSize, bytesLength);

    var bytes = new Array(end - begin);
    for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
      bytes[i] = byteCharacters[offset].charCodeAt(0);
    }
    byteArrays[sliceIndex] = new Uint8Array(bytes);
  }
  return new Blob(byteArrays, { type: contentType });
}

/**
 *
 * @param base64 archivo en base 64
 * @param name nombre del archivo
 * @param mediaType tipo del archivo
 */
export function DownloadFile(base64: string, name: string, mediaType: string) {
  let blob = Base64toBlob(base64, mediaType);
  const link = document.createElement("a");
  const objectUrl = URL.createObjectURL(blob);
  link.href = objectUrl;
  link.download = name;
  link.click();
  URL.revokeObjectURL(objectUrl);
}

/**
 *
 * @param blob archivo File| Blob
 * @param name nombre del archivo
 */
export function downloadBlobFile(blob: Blob | File, name: string) {
  const link = document.createElement("a");
  const objectUrl = URL.createObjectURL(blob);
  link.href = objectUrl;
  link.download = name;
  link.click();
  URL.revokeObjectURL(objectUrl);
}

/**
 *
 * @param archivoResponse Objeto de tipo {
 * archivo: string,
 * codigo: string,
 * contenTypeArchivo: string,
 * informacion: string,
 * innerException: string,
 * isSuccess: boolean
 * message: string,
 * messageExeption: string,
 * nombeArchivo: string,
 * nombeArchivoGenerado: string,
 * stackTrace: string,
 * success: true
 * tipoDocumento: string
 * }
 * @returns
 */
export function descargarArchivo(archivoResponse: any) {
  if (isNullOrEmpty(archivoResponse)) {
    console.error("Archivo nulo");
    return;
  }
  const { archivo, nombeArchivo, contenTypeArchivo } = archivoResponse;
  if (
    isNullOrEmpty(archivo) ||
    isNullOrEmpty(nombeArchivo) ||
    isNullOrEmpty(contenTypeArchivo)
  ) {
    console.error("Archivo o nombre archivo o content type nulo ");
    return;
  }
  DownloadFile(archivo, nombeArchivo, contenTypeArchivo);
}

export function CompareObjects(obj1: any, obj2: any): boolean {
  let keys1 = Object.keys(obj1);
  let keys2 = Object.keys(obj2);

  if (keys1.length !== keys2.length) return false;

  for (let key of keys1) {
    let val1 = obj1[key];
    let val2 = obj2[key];
    let areObjects = isObject(val1) && isObject(val2);

    if (
      (areObjects && !CompareObjects(val1, val2)) ||
      (!areObjects && val1 !== val2)
    )
      return false;
  }

  return true;
}
export function compareObjects(obj1: any, obj2: any): boolean {
  return CompareObjects(obj1, obj2);
}

function isObject(obj: any) {
  return Object.prototype.toString.call(obj) === "[object Object]";
}

export function DowloadFileLocal(url: string, name: string) {
  const link = document.createElement("a");
  link.setAttribute("target", "_blank");
  link.setAttribute("href", url);
  link.setAttribute("download", name);
  document.body.appendChild(link);
  link.click();
  link.remove();
}

//#region Handler Result

/**
 *
 * @param msg Servicio de Mensaje NG Prime
 * @returns Objeto con alertas dependiendo del tipo de resultado de un request.
 */
export function handlerWriteResult(msg: MessageService, key?: string) {
  return {
    next: (res: any) =>
      msg.add({
        severity: "success",
        detail: res?.message ? res.message : "Operación realizada con éxito!",
        key,
      }),
    error: (err: any) => {
      msg.add({
        severity: "error",
        detail: err?.message
          ? err.message
          : "Ocurrió un error al realizar la operación",
        key,
      });
      console.error(err);
    },
  };
}

/**
 *
 * @param toast Servicio Toast Global
 * @returns Objeto con alertas dependiendo del tipo de resultado de un request.
 */
export function handlerSaveResult(toast: ToastService) {
  return {
    next: (res: any) =>
      toast.ok(res?.message ? res.message : "Se actualizó correctamente"),
    error: (err: any) => {
      toast.error(
        err?.message ? err.message : "Ocurrió un error al realizar la operación"
      );
      console.error(err);
    },
  };
}

//#endregion

/**
 *
 * @param value Valor que se quiere saber si es nulo, indefinido o (blanco '')
 * @returns Devuelve true o false
 */
export function isNullOrEmpty(value: any | number | string): boolean {
  return value == null || value == undefined || value == "";
}

/**
 *
 * @param value Valor que se quiere saber si es nulo o indefinido
 * @returns Devuelve true o false
 */
export function isNull(value: any | number | string): boolean {
  return value == null || value == undefined;
}

export function removeTagsEditor(text: string) {
  let regex = /(<([^>]+)>)/gi;
  return text.replace(regex, "");
}

export function onlySemicolons(text: string): boolean {
  let regex = /^[;]*$/gm;
  return regex.test(text);
}

export function setOneSemicolon(text: string) {
  let regex = /;+/g;
  return text.replace(regex, ";");
}

export function noop() {}

/**
 *
 * @param title titulo con caracteres especiales ej. información general
 * @returns titulo normalizado ej. informacion_general
 */
export function normalizeTitle(title: string): string {
  let standardTitle = String(title)
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "");
  return standardTitle.replace(/[^a-z0-9\s]/gi, "").replace(/[-\s]/g, "_");
}

/**
 *
 * @param FwchaInicial type: Date() -> Fecha inicial (desde)
 * @param FwchaFinal type: Date() -> Fecha final (hasta)
 * @param FechaEval type: Date() -> Fecha a ser evaluada
 * @param exacta Si desea que la fecha a evaluar sea incluida (Defecto true)
 * @returns true o false
 */
export function funDateBetween(
  fechaInicial: Date,
  fechaFinal: Date,
  fechaEval: Date,
  exacta: boolean = true
): boolean {
  let auxFechaInicial = new Date(fechaInicial.toDateString());
  let auxFechaFinal = new Date(fechaFinal.toDateString());
  let auxFechaEval = new Date(fechaEval.toDateString());

  let respuesta = false;
  if (exacta)
    respuesta =
      auxFechaEval >= auxFechaInicial && auxFechaEval <= auxFechaFinal;
  else
    respuesta = auxFechaEval > auxFechaInicial && auxFechaEval < auxFechaFinal;

  return respuesta;
}

/**
 *
 * @param email type: string -> Email a evaluar si es un formato de correo valido.
 * @returns Retorna True si el email tienes un formato valido, sino devolverá false.
 */
export function validarEmail(email: string): boolean {
  let respuesta = false;
  const auxExp = new RegExp(/^[a-z0-9_\S]+([.][a-z0-9_]+)*@[a-z0-9_]+([.][a-z0-9_]+)*[.][a-z]{2,5}/);
  if (email && auxExp.test(email.toLocaleLowerCase())) respuesta = true;
  return respuesta;
}

export function observados(data: any, toast: ToastService): boolean {
  let validar: boolean = true;
  let validarObservado: boolean = true;
  let counter = 0;

  data.forEach((response: any) => {
    if (response.conforme == "EEVARECI" || response.conforme == "EEVAOBS") {
      counter++;
    }
    if (
      response.conforme == "EEVAOBS" &&
      (response.observacion == null || response.observacion == "")
    ) {
      validar = false;
      validarObservado = false;
    }
  });

  if (counter == 0) {
    validar = false;
    toast.warn("(*) No ha seleccionado una evaluación.\n");
  }
  if (!validar && !validarObservado) {
    toast.warn(
      "(*) Debe ingresar: Observación en todos los campos observados.\n"
    );
  }

  return validar;
}
