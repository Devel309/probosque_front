export const EstatusProceso = {
  enPublicacion: 2,
  conObservacion: 4,
  enEvaluacion: 5,
  ocurrencia: 7,
  suspendido: 10,
  enEvaluacionTecnica: 8,
  enRevisionDocumentos: 35,
  aprobado: 34
};

export const TipoSolicitud = {
  ampliacionPlazoPublicacion: 3,
  autorizacionExpliracion: 4,
  impugnacion: 5,
  impSAN: 6
};


export const AcceptType = {
  PDF: '.pdf',
  WORD: '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  PDF_WORD: '.pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
}

export const codigoTipoCondicionMinima = {
  Registro_Nacional_Infractores: "NOFRNI",
  Antecedentes_Penales: "NOCOAP",
  Antecedentes_Judiciales: "NOCOAJ",
  Antecedentes_Policiales_PNP: "NAPPNP",
  No_Cuenta_PAU: "NOCPAU",
  No_Cuenta_Impedimento_Estado: "NOIMES",
}


export const TipoDocumentoPlan = {
  PGMF: 1,
  PGMFI: 2,
  PGMFA: 3,
  DEMA: 4,
  PMFI: 5,
  DEMAP: 8,
  DEMACU: 11,
  PMFIP: 12,
  POPMIFP: 13,
  DEMAC: 14
}

export const IDPerfil = {
  ID_ADMIN_MC_SNIFFS: 10,
  ID_POSTULANTE: 55,

}

export const CodigoAplicacion = {
  MC_SNIFFS: "MC",
}
