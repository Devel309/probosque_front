import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class OrganizacionActividadService {
  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) {}

  //ParametroValor
  registrarOrganizacionActividad(params: any) {
    return this.http.post(
      this.base + 'api/organizacionActividad/registrarOrganizacionActividad',
      params
    );
  }
  listarPorFiltroOrganizacionActividad(params: any) {
    return this.http.post(
      this.base +
        'api/organizacionActividad/listarPorFiltroOrganizacionActividad',
      params
    );
  }
  eliminarOrganizacionActividad(params: any) {
    return this.http.post(
      this.base + 'api/organizacionActividad/eliminarOrganizacionActividad',
      params
    );
  }

  registrarOrganizacionActividadArchivo(params: any, formd: FormData) {
    return this.http.post(
      this.base +
        'api/organizacionActividad/registrarOrganizacionActividadArchivo?' +
        params,
      formd
    );
  }
  listarOrganizacionActividadArchivo(params: any) {
    return this.http.post(
      this.base +
        'api/organizacionActividad/listarOrganizacionActividadArchivo',
      params
    );
  }
}
