import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class GenericoService {
  base = environment.urlProcesos; //'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http: HttpClient, private config: ConfigService) {}

  listarParametroLineamiento(params: any) {
    return this.http.post(
      this.base + 'api/generico/listarParametroLineamiento',
      params
    );
  }


  listarPorFiltroParametro(params: any) {
    return this.http.post(
      this.base + 'api/generico/listarPorFiltroParametro',
      params
    );
  }

  listarPorFiltroPersona(params: any) {
    return this.http.post(
      this.base + 'api/generico/listarPorFiltroPersona',
      params
    );
  }

  descargarReporte(params: any) {
    return this.http.post(this.base + 'api/generico/descargarPdf', params);
  }

  listarParametroPorPrefijo() {
    const body = {
      prefijo: 'AUTSAN',
    };

    return this.http.post(
      `${this.base}api/generico/listarParametroPorPrefijo`,
      body
    );
  }

  getParametroPorPrefijo(body: any) {
    return this.http.post(
      `${this.base}api/generico/listarParametroPorPrefijo`,
      body
    );
  }
}
