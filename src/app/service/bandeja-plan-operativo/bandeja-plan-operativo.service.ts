import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class BandejaPlanOperativoService {
  private base: string = '';

  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  actualizarPlanManejo = (params: any) => {
    return this.http.post(
      this.base + 'api/bandejaPlanOperativo/actualizarPlanManejo',
      params
    );
  };

  eliminarPlanManejo = (params: any) => {
    return this.http.post(
      this.base + 'api/bandejaPlanOperativo/eliminarPlanManejo',
      params
    );
  };

  registrarPlanManejo = (params: any) => {
    return this.http.post(
      this.base + 'api/bandejaPlanOperativo/registrarPlanManejo',
      params
    );
  };

  listarPlanManejo = (params: any) => {
    return this.http.post(
      this.base + 'api/bandejaPlanOperativo/listarPlanManejo',
      params
    );
  };
}
