import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SolicitudBosqueLocalGeometriaService {

  constructor(private http: HttpClient) { }

  base = environment.urlProcesos;
  basesolicitudBosqueLocal = `${this.base}api/solicitudBosqueLocalGeometria`;
  registrarGeometria (params: any){
    return this.http.post<ResponseModel<any>>(`${this.basesolicitudBosqueLocal}/registrarSolicitudBosqueLocalGeometria`, params)
    .pipe(tap({ error: err => console.log(err) }));
  };
  listarGeometria (params: any){
    return this.http.post<ResponseModel<any>>(`${this.basesolicitudBosqueLocal}/listarSolicitudBosqueLocalGeometria`, params)
    .pipe(tap({ error: err => console.log(err) }));
  };
  eliminarGeometriaArchivo(params:any){
    return this.http.post(`${this.basesolicitudBosqueLocal}/eliminarSolicitudBosqueLocalGeometriaArchivo`,params)
    .pipe(tap({ error: err => console.log(err) }));
  }
}
