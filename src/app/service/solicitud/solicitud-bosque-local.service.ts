import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})

export class SolicitudBosqueLocalService {
  base = environment.urlProcesos;
  baseSolicitudBosqueLocal= `${this.base}api/solicitudBosqueLocal`;
  baseSolicitudBosqueEvaluacion = `${this.base}api/solicitudBosqueLocalEvaluacion`;

  constructor(private http: HttpClient) { }

  obtenerInfoAdjuntosSolicitudBosqueLocal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocal}/obtenerInfoAdjuntosSolicitudBosqueLocal`, params);
  };

  registrarInfoAdjuntoSolicitudBosqueLocal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocal}/registrarInfoAdjuntoSolicitudBosqueLocal`, params);
  }

  eliminarInfoAdjuntoSolicitudBosqueLocal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocal}/eliminarInfoAdjuntoSolicitudBosqueLocal`, params);
  };

  descargarPlantillaIniciativaBosqueLocal = () => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocal}/descargarPlantillaIniciativaBosqueLocal`, null);
  };

  actualizarEstadoSolicitudBosqueLocal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocal}/actualizarEstadoSolicitudBosqueLocal`, params);
    
  }

  listarSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/listarSolicitudBosqueLocalEvaluacion`, params);
  };

  listarGabinete(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/listarSolicitudBosqueLocalEvaluacionDetalle`, params);
  };

  guardarGabinete(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/registrarInfoEvaluacionDetalleSolicitudBosqueLocal`, params);
  };

  actualizarGabinete(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/actualizarSolicitudBosqueLocalEvaluacionDetalle`, params);
  };

}
