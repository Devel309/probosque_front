import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseModel } from '@models';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class AnexoService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  obtenerUsuarioPostulante = (params: any) => {
    return this.http.post(
      this.base + 'api/anexo/obtenerUsuarioPostulante',
      params
    );
  };

  obtenerUsuario = (params: any) => {
    return this.http.post(
      this.base + 'api/postulante/obtenerUsuario',
      params
    );
  };

  insertarEstatusAnexo = (params: any) => {
    return this.http.post(this.base + 'api/anexo/insertarEstatusAnexo', params);
  };

  obtenerEstatusAnexo = (params: any) => {
    return this.http.post(this.base + 'api/anexo/obtenerEstatusAnexo', params);
  };

  actualizarEstatusAnexo = (params: any) => {
    return this.http.post(
      this.base + 'api/anexo/actualizarEstatusAnexo',
      params
    );
  };

  obtenerAdjuntos = (params: any) => {
    return this.http.post(this.base + 'api/anexo/obtenerAdjuntos', params);
  };

  obtenerDetalleObservacion = (params: any) => {
    return this.http.post(
      this.base + 'api/anexo/obtenerDetalleObservacion',
      params
    );
  };

  listarDocumentos = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + 'api/anexo/listarDocumentos', params);
  };

  guardarObservacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + 'api/anexo/guardarObservacion', params);
  };

  adjuntarListAnexo(params:any) {
    return this.http.post(
      this.base + 'api/anexo/adjuntarListAnexo',params
    );
  }

  generarInformeAnexo(body: any) {
    return this.http.post(
      `${this.base}api/anexo/generarInformeAnexo`,
      body
    );
  }
}
