import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable,from } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiSoapService {

  base =  environment.urlProcesos;

  constructor(private http: HttpClient) { }
  consultarProveedorVigente(params:any):Observable<any>{
    
    return this.http.post(this.base+'api/servicioSoap/consultarProveedorVigente' ,params);
  }
  consultarAntecedentePenal(params:any):Observable<any>{
    
    return this.http.post(this.base+'api/servicioSoap/consultarAntecedentePenal' ,params);
  }
}
