import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators/tap';
import { ResponseModel } from '../model/ResponseModel';

@Injectable({
  providedIn: 'root'
})
export class PlanManejoGeometriaService {

  private base: string;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/planManejoGeometria`;
  }

  registrarPlanManejoGeometria(items: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}/registrarPlanManejoGeometria`
    return this.http.post<ResponseModel<any>>(uri, items)
      .pipe(tap({ error: err => console.log(err) }));
  }
  listarPlanManejoGeometria(item: any): Observable<ResponseModel<any[]>> {
    const uri = `${this.base}/listarPlanManejoGeometria`
    return this.http.post<ResponseModel<any[]>>(uri, item)
      .pipe(tap({ error: err => console.log(err) }));
  }
  eliminarPlanManejoGeometriaArchivo(idArchivo:Number, idUsuario:Number){
    return this.http.get(`${this.base}/eliminarPlanManejoGeometriaArchivo/${idArchivo}/${idUsuario}`);
  }
  actualizarCatastroPlanManejoGeometria(items: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}/actualizarCatastroPlanManejoGeometria`
    return this.http.post<ResponseModel<any>>(uri, items)
      .pipe(tap({ error: err => console.log(err) }));
  }
}
