import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from './config.service';


@Injectable({ providedIn: 'root' })
export class SolicitudOpinionService {

    base = environment.urlProcesos;
    constructor(private http: HttpClient, private config: ConfigService) { }


    listarSolicitudOpinion(params: any) {
        return this.http.post(this.base + 'api/solicitudOpinion/listarSolicitudOpinion', params);
    }

    obtenerSolicitudOpinion(params: any) {
        return this.http.post(this.base + 'api/solicitudOpinion/obtenerSolicitudOpinion', params);
    }

    actualizarSolicitudOpinion(params: any) {
        return this.http.post(this.base + 'api/solicitudOpinion/actualizarSolicitudOpinion', params);
    }

    actualizarSolicitudOpinionArchivo(params: any) {
        let formData = new FormData();
        formData.append("idTipoDocumento", params.idTipoDocumento);
        formData.append("idSolicitudOpinion", params.idSolicitudOpinion);
        formData.append("documento", params.documento);
        formData.append("idUsuarioModificacion", params.idUsuarioModificacion);
        formData.append("file", params.file);

        const req = new HttpRequest('POST', `${this.base}api/solicitudOpinion/actualizarSolicitudOpinionArchivo`, formData);
        return this.http.request(req);
    }

    eliminarSolicitudOpinionArchivo(params: any) {
        return this.http.post(this.base + 'api/solicitudOpinion/eliminarSolicitudOpinionArchivo', params);
    }

    registrarSolicitudOpinion(params: any) {

        return this.http.post(this.base + 'api/solicitudOpinion/registrarSolicitudOpinion', params);
    }

    listarProcesoOfertaSolicitudOpinion(params: any) {
        return this.http.post(this.base + 'api/procesoofertasolicitudopinion/listarProcesoOfertaSolicitudOpinion', params);
    }

    registrarProcesoOfertaSolicitudOpinion(params: any) {
        return this.http.post(this.base + 'api/procesoofertasolicitudopinion/registrarProcesoOfertaSolicitudOpinion', params);
    }

    eliminarProcesoOfertaSolicitudOpinion(params: any) {
        return this.http.post(this.base + 'api/procesoofertasolicitudopinion/eliminarProcesoOfertaSolicitudOpinion', params);
    }


}
