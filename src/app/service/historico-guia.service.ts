import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '@env/environment';
import { isNull } from '@shared';
import { Page, Pageable, ResponseModel } from '@models';
@Injectable({ providedIn: 'root' })

export class HistoricoGuiaService {
  private base: string;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/historicoGuia`;
  }


  sincronizarHistoricoGuia = () => {
    return this.http.post(`${this.base}/sincronizarHistoricoGuia`, '');
  };
}
