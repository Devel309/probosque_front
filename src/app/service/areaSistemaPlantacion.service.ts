import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from "./config.service";
import { environment } from "../../environments/environment";

@Injectable({ providedIn: "root" })
export class AreaSistemaPlantacionService {
  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) { }

  listarAreaSistemaPlantacion(params: any) {
    return this.http.post(
      this.base + "api/areaSistemaPlantacion/listarAreaSistemaPlantacion",
      params
    );
  }

  listarSuperficie(params: any) {
    return this.http.post(
      this.base + "api/solPlantacionForestal/listarSolPlantacacionForestalArea",
      params
    );
  }



  registrarSupMesPlan(params: any) {
    return this.http.post(
      this.base +
      "api/solPlantacionForestal/registrarSolicitudPlantacacionArea",
      params
    );
  }
  listarUnidadMedida(params: any) {
    return this.http.post(
      this.base +
      "api/coreCentral/listarUnidadMedidaSincronizacion",
      params
    );
  }
  registrarAreaSistemaPlantacion(params: any) {
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/registrarAreaSistemaPlantacion",
      params
    );
  }

  eliminarAreaSistemaPlantacion(params: any) {
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/eliminarAreaSistemaPlantacion",
      params
    );
  }

  listarEspecieForestalSistemaPlantacionEspecie(params: any){
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/listarEspecieForestalSistemaPlantacionEspecie",
      params
    );
  }

  listarSistemaPlantacionNuevaEspecie(params: any){
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/listarSistemaPlantacionNuevaEspecie",
      params
    );
  }

  registrarSistemaPlantacionNuevaEspecie(params: any){
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/registrarSistemaPlantacionNuevaEspecie",
      params
    );
  }

  registrarSistemaPlantacionEspecieLista(params: any) {
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/RegistrarSistemaPlantacionEspecieLista",
      params
    );
  }

  eliminarSistemaPlantacionEspecieLista(params: any) {
    return this.http.post(
      this.base +
      "api/areaSistemaPlantacion/EliminarSistemaPlantacionEspecieLista",
      params
    );
  }
  
}
