import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { ResponseModel } from '@models';
import { param } from 'jquery';
@Injectable({
  providedIn: 'root',
})
export class ManejoBosqueService {
  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) {}

  registrarManejoBosqueActividad(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarManejoBosqueActividad',
      params
    );
  }
  listarManejoBosqueActividad(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/listarManejoBosqueActividad',
      params
    );
  }
  eliminarManejoBosqueActividad(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/eliminarManejoBosqueActividad',
      params
    );
  }

  obtenerManejoBosque(body: any) {
    const params = new HttpParams()
      .set('idPlanManejo', String(body.idPlanManejo))
      .set('codigoGeneral', body.codigoGeneral);

    return this.http.post(
      this.base + 'api/manejoBosque/ListarManejoBosque',
      null,
      { params }
    );
  }

  listarManejoBosqueDetalle(body: any) {
    const params = new HttpParams()
      .set('idPlanManejo', String(body.idPlanManejo))
      .set('codigoGeneral', body.codigoGeneral);

    return this.http.post(
      this.base + 'api/manejoBosque/ListarManejoBosqueDetalle',
      null,
      { params }
    );
  }

 listarManejoBosque(body: any) {
    let params = new HttpParams()
      .set('idPlanManejo', String(body.idPlanManejo))
      .set('codigoGeneral', body.codigoGeneral)
      .set('subCodigoGeneral', body.subCodigoGeneral);
    if (body.subCodigoGeneralDet) {
      params = params.set('subCodigoGeneralDet', body.subCodigoGeneralDet);
    }

    return this.http.post(
      this.base + 'api/manejoBosque/ListarManejoBosqueDetalle',
      null,
      { params }
    );
  }
  
  listarManejoBosque1(params : any){
    return this.http.post(this.base +'api/manejoBosque/ListarManejoBosqueDetalle', params);
  }

  sincronizacionCenso(body: any) {
    const params = new HttpParams().set(
      'idPlanManejo',
      String(body.idPlanManejo)
    );

    return this.http.post(
      this.base + 'api/censoForestal/sincronizacionCenso',
      null,
      { params }
    );
  }

  registrarManejoBosque(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarListaManejoBosque',
      params
    );
  }

  eliminarManejoBosque(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/eliminarManejoBosque',
      params
    );
  }

  listarManejoBosqueEspecie(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/listarManejoBosqueEspecie',
      params
    );
  }

  registrarManejoBosqueEspecie(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarManejoBosqueEspecie',
      params
    );
  }

  eliminarManejoBosqueEspecie(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/eliminarManejoBosqueEspecie',
      params
    );
  }

  obtenerManejoBosqueAprovechamiento(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/obtenerManejoBosqueAprovechamiento',
      params
    );
  }

  registrarManejoBosqueAprovechamiento(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarManejoBosqueAprovechamiento',
      params
    );
  }

  registrarManejoBosqueAprovechamientoCamino(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarManejoBosqueAprovechamientoCamino',
      params
    );
  }
  listarManejoBosqueAprovechamientoCamino(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/listarManejoBosqueAprovechamientoCamino',
      params
    );
  }
  eliminarManejoBosqueAprovechamientoCamino(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/eliminarManejoBosqueAprovechamientoCamino',
      params
    );
  }

  registrarManejoBosqueOperacion(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarManejoBosqueOperacion',
      params
    );
  }
  listarManejoBosqueOperacion(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/listarManejoBosqueOperacion',
      params
    );
  }

  listarManejoBosqueEspecieProteger(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/listarManejoBosqueEspecieProteger',
      params
    );
  }

  RegistrarManejoBosqueEspecieProteger(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/registrarManejoBosqueEspecieProteger',
      params
    );
  }

  EliminarManejoBosqueEspecieProteger(params: any) {
    return this.http.post(
      this.base + 'api/manejoBosque/eliminarManejoBosqueEspecieProteger',
      params
    );
  }

  actividadesDeAprovechamiento(data: any) {
    const params = new HttpParams()
      .set('idPlanDeManejo', data.idPlanDeManejo)
      .set('tipoPlan', data.tipoPlan);

    return this.http.post(
      `${environment.urlProcesos}api/censoForestal/actividadesDeAprovechamiento`,
      null,
      { params }
    );
  }

  registrarManejoBosqueExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codigoManejo: string,
    subCodigoManejo: string,
    codtipoManejoDet: string,
    idPlanManejo: number,
    idManejoBosque: number,
    idUsuarioRegistro: number
  ): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codigoManejo', String(codigoManejo))
      .set('subCodigoManejo', String(subCodigoManejo))
      .set('codtipoManejoDet', String(codtipoManejoDet))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idManejoBosque', String(idManejoBosque))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(
      this.base + 'api/manejoBosque/registrarManejoBosqueExcel',
      body,
      { params, reportProgress: true }
    );
  }
}
