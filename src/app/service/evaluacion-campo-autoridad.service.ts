import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from './config.service';

@Injectable({ providedIn: 'root' })
export class EvaluacionCampoAutoridadService {

    base = environment.urlProcesos;
    constructor(private http: HttpClient, private config: ConfigService) { }

    comboPorFiltroAutoridad(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampoAutoridad/comboPorFiltroAutoridad', params);
    }

    registrarEvaluacionCampoAutoridad(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampoAutoridad/registrarEvaluacionCampoAutoridad', params);
    }

    registroEvaluacionCampo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampoAutoridad/registroEvaluacionCampo', params);
    }


}