import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root',
})
export class actividadesSilviculturalService {
  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) {}

  listarActividadSilvicultural(body: any) {
    return this.http.post(
      `${this.base}api/actividadSilvicultural/listarActSilviCulturalDetalle`,
      body
    );
  }

  registrarActividadSilvicultural(body: any) {
    return this.http.post(
      `${this.base}api/actividadSilvicultural/registrarActividadSilvicultural`,
      body
    );
  }

  eliminarActividadSilviculturalPFDM(params: any) {
    return this.http.post(
      `${this.base}api/actividadSilvicultural/eliminarActividadSilviculturalPgmf`,
      params
    );
  }
}
