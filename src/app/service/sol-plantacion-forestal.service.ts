import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { ResponseModel } from '../model/ResponseModel';

@Injectable({
  providedIn: 'root'
})
export class SolPlantacionForestalService {

  base = environment.urlProcesos;

  constructor(private http: HttpClient) { }

  registrarSolPlantacacionForestal(dato: any): Observable<any> {
    return this.http.post(
      this.base + `api/solPlantacionForestal/registrarSolPlantacacionForestal`,
      dato
    );
  }

  obtenerInfoAdjuntosSolPlanFor(params: any): Observable<any> {
    return this.http.post<ResponseModel<any>>(this.base + `api/solPlantacionForestal/obtenerInfoAdjuntosSolPlantacionForestal`, params);
  }

  registrarInfoAdjuntosSolPlanFor(params: any): Observable<any> {
    return this.http.post<ResponseModel<any>>(this.base + `api/solPlantacionForestal/registrarInfoAdjuntosSolPlantacionForestal`, params);
  }

  eliminarInfoAdjuntosSolPlanFor(params: any): Observable<any> {
    return this.http.post<ResponseModel<any>>(this.base + `api/solPlantacionForestal/eliminarInfoAdjuntoSolPlantacionForestal`, params);
  }

  listarEspeciesSistemaPlantacion(id: number): Observable<any> {
    return this.http.get<ResponseModel<any>>(this.base + `api/solPlantacionForestal/obtenerEspeciesSistemaPlantacion/` + id);
  }

  registrarSolPlantacacionForestalDetalle (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/registrarSolPlantacacionForestalDetalle`, params);
  }

  notificarPasSolicitanteServ (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/notificarPasSolicitante`, params);
  }

  notificarPassSerforServ (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/notificarPassSerfor`, params);
  }

  registrarAreaBloque(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/registrarAreaBloque`, params);
  }

  listarAreaBloque(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/listarAreaBloque`, params);
  }

  descargarSolicitudPlantacionesForestales(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/descargarSolicitudPlantacionesForestales`, params);
  }
  descargarSolicitudPlantacionForestal_PDF(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/descargarSolicitudPlantacionForestal_PDF`, params);
  }

  clonarPlantacionForestal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/clonarPlantacionForestal`, params);
  }
  validarClonPlantacionForestal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/validarClonPlantacionForestal`, params);
  }

  //
  registrarSolPlantForPersonaServ(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/registrarSolPlantacacionForestalPersona`, params);
  }
  listarSolPlantForPersonaServ(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/listarSolPlantacacionForestalPersona`, params);
  }
  eliminarSolPlantForPersonaServ(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/eliminarSolPlantacacionForestalPersona`, params);
  }

}
