import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigService } from "./config.service";
import { environment } from "../../environments/environment";

@Injectable({ providedIn: "root" })
export class ProduccionRecursoForestalService {
  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) {}

  listarOrdenamientoAreaManejo(params: any) {
    return this.http.post(
      this.base + "api/produccionRecursoForestal/listarOrdenamientoAreaManejo",
      params
    );
  }

  registrarOrdenamientoAreaManejo(params: any) {
    return this.http.post(
      this.base +
        "api/produccionRecursoForestal/registrarProduccionRecursoForestal",
      params
    );
  }

  eliminarOrdenamientoAreaManejo(params: any) {
    return this.http.post(
      this.base +
        "api/produccionRecursoForestal/eliminarProduccionRecursoForestal",
      params
    );
  }

  eliminarEspecie(params: any) {
    return this.http.post(
      this.base +
        "api/produccionRecursoForestal/eliminarProduccionRecursoForestalEspecie",
      params
    );
  }
}
