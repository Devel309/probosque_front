import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class CoreCentralService {
  base = environment.urlCoreCentral;
  url = environment.urlProcesos;

  constructor(private http: HttpClient, private config: ConfigService) {}

  listarPorFiltroDepartamento(params: any) {
    return this.http.post(
      this.base + 'api/coreCentral/listarPorFiltroDepartamento',
      params
    );
  }
  listarPorFilroProvincia(params: any) {
    return this.http.post(
      this.base + 'api/coreCentral/listarPorFiltroProvincia',
      params
    );
  }
  listarPorFilroDistrito(params: any) {
    return this.http.post(
      this.base + 'api/coreCentral/listarPorFiltroDistrito',
      params
    );
  }

  listarPorFiltroAutoridadForestal(params: any) {
    return this.http.post(
      this.base + 'api/coreCentral/listarPorFiltroAutoridadForestal',
      params
    );
  }

  ListaPorFiltroEspecieForestal(dato: any) {
    return this.http.post(
      this.base + `api/coreCentral/listaPorFiltroEspecieForestal`,
      dato
    );
  }

  listaPorFiltroEspecieFauna(dato: any) {
    return this.http.post(
      this.base + `api/coreCentral/listaPorFiltroEspecieFauna`,
      dato
    );
  }

  listaEspecieFaunaPorFiltro(dato: any) {
    return this.http.post(
      this.url + `api/coreCentral/listaEspecieFaunaPorFiltro`,
      dato
    );
  }

  listarPorFiltroTipoBosque(dato: any) {
    return this.http.post(
      this.base + `api/coreCentral/listarPorFiltroTipoBosque`,
      dato
    );
  }

  ListaPorFiltroEspeciesForestales(dato: any) {
    return this.http.post(
      this.url + `api/coreCentral/ListaEspecieForestalPorFiltro`,
      dato
    );
  }

  listarComboCuenca() {
    return this.http.get(
      this.base + `api/coreCentral/listarComboCuenca`
    );
  }

  listarCuencaSubcuenca() {
    return this.http.get(
      this.base + 'api/coreCentral/listarCuencaSubcuenca'
    );
  }
}
