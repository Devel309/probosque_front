import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AmpliacionSolicitudService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarSolicitudesAmpliacion = (params: any) => {
    return this.http.post(
      this.base + 'api/solicitud/listarSolicitudesAmpliacion',
      params
    );
  };

  obtenerSolicitudAmpliacion = (params: any) => {
    return this.http.post(this.base + 'api/solicitud/obtenerSolicitudAmpliacion', params);
  };

  registrarSolicitudAmpliacion = (params: any) => {
    return this.http.post(this.base + 'api/solicitud/registrarSolicitudAmpliacion', params);
  };

  actualizarSolicitudAmpliacion = (params: any) => {
    return this.http.post(this.base + 'api/solicitud/actualizarSolicitudAmpliacion', params);
  };

  registrarObservacionDocumentoAdjunto = (params: any) => {
    return this.http.post(this.base + 'api/solicitud/registrarObservacionDocumentoAdjunto', params);
  };

  actualizarAprobadoSolicitud = (params: any) => {
    return this.http.post(this.base + 'api/solicitud/actualizarAprobadoSolicitud', params);
  };

  aprobarRechazarSolicitud = (params: any) => {
    return this.http.post(
      this.base + 'api/solicitud/aprobarRechazarSolicitud',
      params
    );
  };

  listarTipoSolicitud = () => {
    return this.http.get(this.base + 'api/solicitud/listarTipoSolicitud');
  };

  descargarDeclaracionJurada = () => {
    return this.http.get(this.base + 'api/solicitud/descagarDeclaracionJurada/');
  };

  descargarPropuestaExploracion = () => {
    return this.http.get(this.base + 'api/solicitud/descargarPropuestaExploracion/');
  };

  descargarResolucionResumen = () => {
    return this.http.get(this.base + 'api/solicitud/descagarPlantillaResolucionResumen/');
  };

  descargarFormatoAprobacion = () => {
    return this.http.get(
      this.base + 'api/solicitud/descargarFormatoAprobacion/'
    );
  };

  registrarDocumentoAdjunto = (params: any) => {
    return this.http.post(this.base + 'api/solicitud/registrarDocumentoAdjunto', params);
  };

  obtenerArchivosSolicitudes = (IdSolicitud: number) => {
    return this.http.get(
      this.base + 'api/solicitud/ObtenerArchivosSolicitudes/' + IdSolicitud
    );
  };

  actualizarEstadoProcesoPostulacion = (params: any) => {
    return this.http.post(
      this.base + 'api/solicitud/actualizarEstadoProcesoPostulacion',
      params
    );
  };

  obtenerNuevoUsuarioGanador = (params: any) => {
    return this.http.post(
      this.base + 'api/solicitud/obtenerNuevoUsuarioGanador',
      params
    );
  };

  listarSolicitudesVencidas() {
    return this.http.get(this.base + 'api/solicitudImpugnacion/listarSolicitudesVencidas');
  };

  solicitudesVencidasEnviarCorreo(params: any, formd: FormData) {
    return this.http.post(this.base + 'api/solicitudImpugnacion/solicitudesVencidasEnviarCorreo?' + params, formd);
  }

  /****************Impugnacion************* */
  ObtenerImpugnacion(param: any): Observable<any> {
    return this.http.post(this.base + 'api/impugnacion/ObtenerImpugnacion', param);
  };
  ObtenerImpugnacionArchivo(param: any): Observable<any> {
    return this.http.post(this.base + 'api/impugnacion/ObtenerImpugnacionArchivo', param);
  };
  ObtenerImpugnacionEtapa(param: any): Observable<any> {
    return this.http.post(this.base + 'api/impugnacion/ObtenerImpugnacionEtapa', param);
  };
  RegistrarImpugnacion(param: any): Observable<any> {
    return this.http.post(this.base + 'api/impugnacion/RegistrarImpugnacion', param);
  };
  registrarImpugnacionArchivo(param: any, fromdata: FormData): Observable<any> {
    return this.http.post<any>(this.base + `api/impugnacion/registrarImpugnacionArchivo`, fromdata);
  };
  ListarImpugnacion(param: any): Observable<any> {
    return this.http.post(this.base + 'api/impugnacion/ListarImpugnacion', param);
  };


}
