import {
  HttpClient,
  HttpClientModule,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpRequest,
  HttpInterceptor,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
import { ParametroValor, ResponseModel } from '@models';
@Injectable({
  providedIn: 'root',
})
export class ParametroValorService {
  base = environment.urlProcesos; // 'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http: HttpClient, private config: ConfigService) {}

  //ParametroValor
  listarPorCodigoParametroValor(params: any) {
    return this.http.post(
      this.base + 'api/parametro/ListarPorCodigoParametroValor',
      params
    );
  }

  listarParametroPorPrefijo(params: any) {
    return this.http.post(
      this.base + 'api/generico/listarParametroPorPrefijo',
      params
    );
  }

  listarParametros(
    codigo: string
  ): Observable<ResponseModel<ParametroValor[]>> {
    const url = this.base + 'api/parametro/ListarPorCodigoParametroValor';
    return this.http.post<ResponseModel<ParametroValor[]>>(url, {
      prefijo: codigo,
    });
  }
}
