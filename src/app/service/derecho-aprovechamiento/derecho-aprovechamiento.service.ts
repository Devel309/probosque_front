import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class DerechoAprovechamientoService {
  base = environment.urlProcesos; // 'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http: HttpClient) { }

  listarDerechoAprovechamiento(params: any) {
    return this.http.post(this.base + "api/derechoAprovechamiento/listarDerechoAprovechamiento", params);
  }

  listarPago(params: any) {
    return this.http.post(this.base + "api/pago/listarPago", params);
  }
  
  registrarPago(params: any) {
    return this.http.post(this.base + "api/pago/registrarPago", params);
  }

  listarTH(params: any) {
    return this.http.post(this.base + "api/pago/listarTH", params);
  }

  listarGuiaPago(params: any) {
    return this.http.post(this.base + "api/pago/listarGuiaPago", params);
  }
  
  registrarGuiaPago(params: any) {
    return this.http.post(this.base + "api/pago/registrarGuiaPago", params);
  }

  listarDescuento(params: any) {
    return this.http.post(this.base + "api/pago/listarDescuento", params);
  }

  listarModalidad(params: any) {
    return this.http.post(this.base + "api/pago/listarModalidad", params);
  }

  listarOperativosPorPGM(params: any) {
    return this.http.post(this.base + "api/pago/listarOperativosPorPGM", params);
  }

  eliminarPagoGuiaEspecie(params: any) {
    return this.http.post(this.base + "api/pago/eliminarPagoGuiaEspecie", params);
  }

  eliminarPago(params: any) {
    return this.http.post(this.base + "api/pago/eliminarPago", params);
  }

}
