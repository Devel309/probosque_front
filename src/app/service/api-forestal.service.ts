import {
  HttpClient,
  HttpClientModule,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpRequest,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {environment} from "src/environments/environment";
import {ConfigService} from "./config.service";

@Injectable({
  providedIn: "root",
})
export class ApiForestalService {
  base = environment.urlSerfor;
  serviciosExternos = environment.urlServiciosExternos;

  constructor(private http: HttpClient, private config: ConfigService) {
  }

  login() {
    var param = {
    };
    return this.http.post("/corews/auth", param);
  }

  consultarRegente() {
    return this.http.post(this.serviciosExternos + "api/serfor/consultarRegente", {});
  }

  consultarDatoRuc(numRUC: String) {
    var bodyData = {numRUC: numRUC};
    return this.http.post(this.serviciosExternos + "api/pide/consultarDatoPrincipal", bodyData);
  }

  consultarDatoSunarp(numPartidaRegistral: String) {
    var bodyData = {numPartidaRegistral: numPartidaRegistral};
    return this.http.post(this.serviciosExternos + "api/pide/consultarDatoPrincipal", bodyData);
  }

  consultarPlanesOsinfor(data: any) {
    return this.http.post(this.serviciosExternos + "api/osinfor/consultarPlanes", data);
  }
  consultarResolFinPAU(data: any) {
    return this.http.post(this.serviciosExternos + "api/osinfor/consultarResolFinPAU", data);
  }

  insertDerechoForestal(data: any) {
     return this.http.post(this.serviciosExternos + "api/geoForestal/insertDerechoForestal", data);
  }
  consultarTitularidadSUNARP(item: any) {
    return this.http.post(this.serviciosExternos + "api/pide/consultarTitularidad", item);
  }
  listarAsientos(item: any) {
    return this.http.post(this.serviciosExternos + "api/pide/listarAsientos", item);
  }
  validarSuperposicionPlanificacion(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/validarSuperposicionPlanificacion',params);
  }
  validarSuperposicionAprovechamiento(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/validarSuperposicionAprovechamiento',params);
  }
  validarSuperposicionOtorgamiento(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/validarSuperposicionOtorgamiento',params);
  }
  identificarTipoBosque(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/identificarTipoBosque',params);
  }
  consultarUnidadAprovechamiento(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/consultarUnidadAprovechamiento',params);
  }
  actualizarUnidadAprovechamiento(params:any):Observable<any>{
    return this.http.put(this.serviciosExternos+'api/geoForestal/actualizarUnidadAprovechamiento',params);
  }
  insertarDivisionAdministrativa(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/insertarDivisionAdministrativa',params);
  }

  consultarDivisionAdmParametrica(params:any):Observable<any>{
    return this.http.post(this.serviciosExternos+'api/geoForestal/consultarDivisionAdmParametrica',params);
  }

  consultarUIT() {
    return this.http.post(this.serviciosExternos + "api/pide/consultarUIT", {});
  }
}
