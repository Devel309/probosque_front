import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class OposicionService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarOposicion = (params: any) => {
    return this.http.post(this.base + 'api/oposicion/listarOposicion', params);
  };

  obtenerOposicion = (idOposicion: any) => {
    return this.http.get(this.base + 'api/oposicion/obtenerOposicion/' + idOposicion);
  };

  obtenerOposicionRespuesta = (IdOposicion: number) => {
    return this.http.get(
      this.base + 'api/oposicion/obtenerOposicionRespuesta/' + IdOposicion
    );
  };

  registrarOposicion = (params: any) => {
    return this.http.post(this.base + 'api/oposicion/registrarOposicion', params);
  };

  registrarRespuestaOposicion = (params: any) => {
    return this.http.post(
      this.base + 'api/oposicion/registrarRespuestaOposicion',
      params
    );
  };

  descargarFormatoResulucion = () => {
    return this.http.get(
      this.base + 'api/oposicion/descargarFormatoResulucion/'
    );
  };

  adjuntarFormatoResolucion = (params: any) => {
    return this.http.post(
      this.base + 'api/oposicion/adjuntarFormatoResolucion',
      params
    );
  };

  actualizarOposicion = (params: any) => {
    return this.http.post(
      this.base + 'api/oposicion/actualizarOposicion',
      params
    );
  };

  obtenerOpositor = (objOpositor: any) => {
    return this.http.post(
      this.base + 'api/oposicion/obtenerOpositor',
      objOpositor
    );
  };

  regitrarOpositor = (params: any) => {
    return this.http.post(this.base + 'api/oposicion/regitrarOpositor', params);
  };

  registrarArchivoDetalle(item: any) {
    return this.http.post(this.base + 'api/oposicion/registrarOposicionArchivo', item);
  }
  eliminarOposicionArchivo = (idArchivo: number, idUsuario: number) => {
    return this.http.get(this.base + `api/oposicion/eliminarOposicionArchivo/${idArchivo}/${idUsuario}`);
  };
}
