import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class AspectoSocioEconomicoPgmfService {


  base = environment.urlProcesos; //'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http : HttpClient, private config : ConfigService) { }
  

  registroInfoEconomicaPgmf(params : any){
    return this.http.post(this.base +'api/informacionAreaManejo/registroInfoEconomicaPgmf', params);
  }
  obtenerInfoEconomicaPgmf(params : any){
    return this.http.post(this.base +'api/informacionAreaManejo/obtenerInfoEconomicaPgmf', params);
  }

  registroInfoEconomicaActEconomicaPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/registroInfoEconomicaActEconomicaPgmf', params);
  }
  obtenerInfoEconomicaActEconomicaPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/obtenerInfoEconomicaActEconomicaPgmf', params);
  }

  registroInfoEconomicaInfraestructuraPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/registroInfoEconomicaInfraestructuraPgmf', params);
  }
  obtenerInfoEconomicaInfraestructuraPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/obtenerInfoEconomicaInfraestructuraPgmf', params);
  }

  registroInfoEconomicaActividadesPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/registroInfoEconomicaActividadesPgmf', params);
  }
  obtenerInfoEconomicaActividadesPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/obtenerInfoEconomicaActividadesPgmf', params);
  }

  registroInfoEconomicaConfictosPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/registroInfoEconomicaConfictosPgmf', params);
  }
  obtenerInfoEconomicaConfictosPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/obtenerInfoEconomicaConfictosPgmf', params);
  }

  EliminarInfoEconomicaConfictosPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/EliminarInfoEconomicaConfictosPgmf', params);
  }
  
  registroInfoBiologicoFaunaSilvestrePgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/registroInfoBiologicoFaunaSilvestrePgmf', params);
  }
  obtenerInfoBiologicoFaunaSilvestrePgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/obtenerInfoBiologicoFaunaSilvestrePgmf', params);
  }
  EliminarBiologicoFaunaSilvestrePgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/EliminarBiologicoFaunaSilvestrePgmf', params);
  }


  EliminarInfoEconomicaInfraestructuraPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/EliminarInfoEconomicaInfraestructuraPgmf', params);
  }
  EliminarInfoEconomicaActEconomicaPgmf(params : any ){
    return this.http.post(this.base +'api/informacionAreaManejo/EliminarInfoEconomicaActEconomicaPgmf', params);
  }
}
