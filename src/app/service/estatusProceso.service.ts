import {
    HttpClient,
  } from '@angular/common/http';
  import { Injectable } from '@angular/core';
  import { ConfigService } from './config.service';
  import { environment } from '../../environments/environment';
  @Injectable({
    providedIn: 'root',
  })
  export class EstatusProcesoService {
    base = environment.urlProcesos;
  
    constructor(private http: HttpClient, private config: ConfigService) {}
  
    listarEstadoEstatusProceso(params: any) {
      return this.http.post(
        this.base + 'api/estatusProceso/listarEstadoEstatusProceso',
        params
      );
    }
    
  }
  