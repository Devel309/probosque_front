import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MonitoreoService   {

  base = environment.urlProcesos;
  constructor(private http : HttpClient, private config : ConfigService) { }

  registrarMonitoreoPgmfea(params : any){
    return this.http.post(this.base +'api/monitoreo/registrarMonitoreoPgmfea', params);
  }
  listarMonitoreoPgmfea(params : any){
    return this.http.post(this.base +'api/monitoreo/listarMonitoreoPgmfea', params);
  }
  eliminarMonitoreoPgmfea(params : any){
    return this.http.post(this.base +'api/monitoreo/eliminarMonitoreoPgmfea', params);
  }
  obtenerMonitoreoPgmfea(params : any){
    return this.http.post(this.base +'api/monitoreo/obtenerMonitoreoPgmfea', params);
  }





}

