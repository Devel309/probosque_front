import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class InformacionAreaManejoService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarAccesibilidadmanejo = (params: any) => {
    return this.http.post(
      this.base + 'api/informacionAreaManejo/listarAccesibilidadmanejo',
      params
    );
  };

  registrarAccesibilidadmanejo = (params: any) => {
    return this.http.post(
      this.base + 'api/informacionAreaManejo/registrarAccesibilidadmanejo',
      params
    );
  };

  actualizarAccesibilidadmanejo = (params: any) => {
    return this.http.post(
      this.base + 'api/informacionAreaManejo/actualizarAccesibilidadmanejo',
      params
    );
  };

  actualizarAccesibilidadmanejorequisito = (params: any) => {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/actualizarAccesibilidadmanejorequisito',
      params
    );
  };

  registrarAccesibilidadmanejorequisito = (params: any) => {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/registrarAccesibilidadmanejorequisito',
      params
    );
  };

  listarAccesibilidadmanejorequisito = (params: any) => {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/listarAccesibilidadmanejorequisito',
      params
    );
  };

  eliminarAccesibilidadmanejorequisito = (params: any) => {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/eliminarAccesibilidadmanejorequisito',
      params
    );
  };

  listarAspectoHidrografico(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/listarAspectoHidrografico',
      params
    );
  }

  eliminarAspectoHidrografico(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/eliminarAspectoHidrografico',
      params
    );
  }

  actualizarAspectoHidrografico(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/actualizarAspectoHidrografico',
      params
    );
  }

  registrarAspectoHidrografico(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/registrarAspectoHidrografico',
      params
    );
  }

  registrarUnidadManejo(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/registrarUnidadManejo',
      params
    );
  }

  obtenerUnidadManejo(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/obtenerUnidadManejo',
      params
    );
  }

  actualizarUnidadManejo(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/actualizarUnidadManejo',
      params
    );
  }

  listarCoordenadaUtm(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/listarCoordenadaUtm',
      params
    );
  }

  registrarCoordenadaUtm(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/registrarCoordenadaUtm',
      params
    );
  }

  eliminarCoordenadaUtm(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/eliminarCoordenadaUtm',
      params
    );
  }

  listarUnidadManejoArchivo(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/listarUnidadManejoArchivo',
      params
    );
  }

  eliminarUnidadManejoArchivo(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/eliminarUnidadManejoArchivo',
      params
    );
  }


  registrarUnidadManejoArchivo(params: any, file1: File, file2: File, file3: File) {
    let formData = new FormData();
    formData.append("idUnidadManejo", params.idUnidadManejo);
    formData.append("idUsuarioRegistro", params.idUsuarioRegistro);
    formData.append("attachments", file1);
    formData.append("attachments", file2);
    formData.append("attachments", file3);

    const req = new HttpRequest('POST', this.base + 'api/informacionAreaManejo/registrarUnidadManejoArchivo', formData, {
      reportProgress: true
    });
    return this.http.request(req);
  }


  registrarAccesibilidadmanejomatriz(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/registrarAccesibilidadmanejomatriz',
      params
    );
  }

  eliminarAccesibilidadmanejomatriz(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/eliminarAccesibilidadmanejomatriz',
      params
    );
  }


  actualizarAccesibilidadmanejomatriz(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/actualizarAccesibilidadmanejomatriz',
      params
    );
  }

  eliminarAccesibilidadmanejomatrizdetalle(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/eliminarAccesibilidadmanejomatrizdetalle',
      params
    );
  }

  listarAccesibilidadmanejomatriz(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/listarAccesibilidadmanejomatriz',
      params
    );
  }

  registroInfoBiologicoFaunaSilvestrePgmf(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/registroInfoBiologicoFaunaSilvestrePgmf',
      params
    );
  }

  obtenerInfoBiologicoFaunaSilvestrePgmf(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/obtenerInfoBiologicoFaunaSilvestrePgmf',
      params
    );
  }

  EliminarBiologicoFaunaSilvestrePgmf(params: any) {
    return this.http.post(
      this.base +
      'api/informacionAreaManejo/EliminarBiologicoFaunaSilvestrePgmf',
      params
    );
  }

  configurarAspectofisicofisiografia(params: any) {
    return this.http.post(
      this.base +
      'api/aspectoFisicoBiologico/configurarAspectofisicofisiografia',
      params
    );
  }

  eliminarAspectofisicofisiografia(params: any) {
    return this.http.post(
      this.base +
      'api/aspectoFisicoBiologico/eliminarAspectofisicofisiografia',
      params
    );
  }

  listarAspectofisicofisiografia(params: any) {
    return this.http.post(
      this.base +
      'api/aspectoFisicoBiologico/listarAspectofisicofisiografia',
      params
    );
  }

  registrarAspectofisicofisiografia(params: any) {
    return this.http.post(
      this.base +
      'api/aspectoFisicoBiologico/registrarAspectofisicofisiografia',
      params
    );
  }

}
