import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from 'src/app/service//config.service';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class SolicitudConcesionResponsableService {
  base = environment.urlProcesos;
  baseSolicitudConcesionResponsable = `${this.base}api/responsableSolicitudConcesion`;

  constructor(private http: HttpClient, private config : ConfigService) { }

  obtenerInformacionResponsableFormacionProfesional = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/obtenerInfoResponsableFormacionProfesional`, params);
  };
  
  eliminarInformacionResponsableFormacionProfesional = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/eliminarInformacionFormacionProfesional`, params);
  };

  registrarInformacionResponsableFormacionProfesional = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/registrarInfoResponsableFormacionProfesional`, params);
  };

  actualizarInformacionResponsableFormacionProfesional = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/actualizarInfoResponsableFormacionProfesional`, params);
  };

  obtenerSolicitudConcesionResponsable = (params: any) => {
    return this.http.post(this.base + 'api/responsableSolicitudConcesion/obtenerSolicitudConcesionResponsable/', params);
  };

  registrarSolicitudConcesionResponsable = (params: any) => {
    return this.http.post(this.base + 'api/responsableSolicitudConcesion/registrarSolicitudConcesionResponsable', params);
  };
  
  actualizarSolicitudConcesionResponsable = (params: any) => {
    return this.http.post(this.base + 'api/responsableSolicitudConcesion/actualizarSolicitudConcesionResponsable',params);
  };

  
}
