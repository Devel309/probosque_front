import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class SolicitudConcesionEvaluacionService {
  private base: string = '';
  private baseSolicitudConcesionEvaluacion: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
    this.baseSolicitudConcesionEvaluacion = `${this.base}api/solicitudConcesionEvaluacion`;
  }

  obtenerInfoEvaluacionSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEvaluacion}/obtenerInfoEvaluacionSolicitudConcesion`, params);
  };

  actualizarInfoEvaluacionSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEvaluacion}/actualizarSolicitudConcesionEvaluacion`, params);
  };
}
