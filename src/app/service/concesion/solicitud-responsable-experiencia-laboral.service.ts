import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class SolicitudExperienciaLaboralService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarExperienciaLaboral = (params: any) => {
    return this.http.post(this.base + 'api/responsableExperienciaLaboral/listarResponsableExperienciaLaboral', params);
  };

  obtenerExperienciaLaboral = (params: any) => {
    return this.http.post(this.base + 'api/responsableExperienciaLaboral/obtenerResponsableExperienciaLaboral', params);
  };

  registrarExperienciaLaboral = (params: any) => {
    return this.http.post(this.base + 'api/responsableExperienciaLaboral/registrarResponsableExperienciaLaboral', params);
  };
  
  actualizarExperienciaLaboral = (params: any) => {
    return this.http.post(this.base + 'api/responsableExperienciaLaboral/actualizarResponsableExperienciaLaboral',params);
  };

  eliminarExperienciaLaboral = (params: any) => {
    return this.http.post(this.base + 'api/responsableExperienciaLaboral/eliminarResponsableExperienciaLaboral', params);
  };
  
}
