import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class SolicitudConcesionService {
  base = environment.urlProcesos;
  baseSolicitudConcesion = `${this.base}api/solicitudConcesion`;
  baseSolicitudConcesionEval = `${this.base}api/solicitudConcesionEvaluacion`;
  baseIngresoCuentaSC = `${this.base}api/ingresoCuentaSolicitudConcesion`;

  constructor(private http: HttpClient) { }

  listarSolicitudConcesionPorFiltro = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/listarSolicitudConcesionPorFiltro`, params);
  };

  obtenerInformacionSolicitanteSolicitado = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/obtenerInformacionSolicitanteSolicitado`, params);
  };

  obtenerInformacionAreaSolicitada = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/obtenerInformacionAreaSolicitada`, params);
  };

  obtenerSolicitudConcesionCalificacionPorFiltro = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/listarSolicitudConcesionCalificacionPorFiltro`, params);
  };

  registrarInformacionSolicitante = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarInformacionSolicitante`, params);
  };

  actualizarInformacionSolicitante = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarInformacionSolicitante`, params);
  };

  registrarInformacionSobreSolicitado = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarInformacionSobreSolicitado`, params);
  };

  actualizarInformacionSobreSolicitado = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarInformacionSobreSolicitado`, params);
  };

  registrarInformacionAreaSolicitada = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarInformacionAreaSolicitada`, params);
  };

  actualizarInformacionAreaSolicitada = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarInformacionAreaSolicitada`, params);
  };

  registrarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarSolicitudConcesion`, params);
  };

  actualizarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarSolicitudConcesion`, params);
  };

  eliminarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/eliminarSolicitudConcesion`, params);
  };

  registrarInformacionActividadFuenteFinanciamiento = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarInformacionActividadFuenteFinanciamiento`, params);
  };

  obtenerInformacionActividadFuenteFinanciamiento = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/obtenerInformacionActividadFuenteFinanciamiento`, params);
  };

  listarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/listarSolicitudConcesion`, params);
  };

  obtenerInformacionAdjuntosSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/obtenerInfoAdjuntosSolicitudConcesion`, params);
  };

  eliminarInformacionAdjuntoSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/eliminarInfoAdjuntoSolicitudConcesion`, params);
  };

  //baseIngresoCuentaSC
  listarSolicitudConcesionIngresoCuenta = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseIngresoCuentaSC}/listarSolicitudConcesionIngresoCuenta`, params);
  };

  registrarSolicitudConcesionIngresoCuenta = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseIngresoCuentaSC}/registrarSolicitudConcesionIngresoCuenta`, params);
  };

  eliminarSolicitudConcesionIngresoCuenta = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseIngresoCuentaSC}/eliminarSolicitudConcesionIngresoCuenta`, params);
  };

  listarCriterioAreaSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/listarCriterioAreaSolicitudConcesion`, params);
  };

  registrarCriterioAreaSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarCriterioAreaSolicitudConcesion`, params);
  };

  actualizarSolicitudConcesionCriterioArea = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarSolicitudConcesionCriterioArea`, params);
  };

  actualizarInfoAreaSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarInfoAreaSolicitudConcesion`, params);
  }

  descargarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/descargarSolicitudConcesionAnexo`, params);
  };
  
  descargarPlantillaCriteriosCalificacionPropuesta = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/descargarPlantillaCriteriosCalificacionPropuesta`, params);
  };

  descargarPlantillaResolucionEvaluacionSolictud = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/descargarPlantillaResolucionEvaluacionSolictud`, params);
  };
  

  listarSolicitudConcesionAnexo = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/listarSolicitudConcesionAnexo`, params);
  };

  registrarInfoAdjuntosSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarInfoAdjuntosSolicitudConcesion`, params);
  }

  actualizarInfoAdjuntosSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/actualizarInfoAdjuntosSolicitudConcesion`, params);
  }

  enviarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/enviarSolicitudConcesion`, params);
  }

  registrarSolicitudConcesionAnexo = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarSolicitudConcesionAnexo`, params);
  }

  //baseSolicitudConcesionEval
  obtenerInfoSolConEval = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEval}/obtenerInfoEvaluacionSolicitudConcesion`, params);
  }

  listarSolConEvalDetalle = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEval}/listarSolicitudConcesionEvaluacionDetalle`, params);
  }

  registrarSolConEvalDetalle = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEval}/registrarSolicitudConcesionEvaluacionDetalle`, params);
  }

  actualizarSolConEvalDetalle = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEval}/actualizarSolicitudConcesionEvaluacionDetalle`, params);
  }

  finalizarCalificacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEval}/actualizarSolicitudConcesionEvaluacion`, params);
  }
  /******************************/

  enviarSolicitudConcesionComunicado = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/enviarComunicado`, params);
  }

  descargarPlantillaManejo = () => {
    return this.http.get<ResponseModel<any>>(`${this.baseSolicitudConcesion}/descargarPlantillaManejo`);
  }
  descargarPlantillaPresupuesto = () => {
    return this.http.get<ResponseModel<any>>(`${this.baseSolicitudConcesion}/descargarPlantillaPresupuesto`);
  }

 


  clonarSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/clonarSolicitudConcesion`, params);
  };

  actualizarSolicitudConcesionEvaluacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEval}/actualizarSolicitudConcesionEvaluacion`, params);
  }
}
