import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SolicitudConcesionGeometriaService {

  constructor(private http: HttpClient) { }
  base = environment.urlProcesos;
  baseSolicitudConcesion = `${this.base}api/SolicitudConcesionGeometria`;

  registrarGeometria (params: any){
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/registrarInformacionAreaGeometriaSolicitada`, params)
    .pipe(tap({ error: err => console.log(err) }));
  };
  listarGeometria (params: any){
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesion}/obtenerInformacionAreaGeometriaSolicitada`, params)
    .pipe(tap({ error: err => console.log(err) }));
  };
  eliminarGeometriaArchivo(params:any){
    return this.http.post(`${this.baseSolicitudConcesion}/eliminarInformacionAreaGeometriaSolicitada`,params)
    .pipe(tap({ error: err => console.log(err) }));
  }
  
}
