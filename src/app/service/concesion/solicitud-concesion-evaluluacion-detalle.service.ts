import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from 'src/app/service//config.service';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class SolicitudConcesionEvaluacionDetalleService {
  base = environment.urlProcesos;
  baseSolicitudConcesionResponsable = `${this.base}api/solicitudConcesionEvaluacion`;

  constructor(private http: HttpClient, private config : ConfigService) { }

  obtenerInfoEvaluacionSolicitudConcesion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/obtenerInfoEvaluacionSolicitudConcesion`, params);
  };

  listarSolicitudConcesionEvaluacionDetalle = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/listarSolicitudConcesionEvaluacionDetalle`, params);
  };
  
  registrarSolicitudConcesionEvaluacionDetalle = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/registrarSolicitudConcesionEvaluacionDetalle`, params);
  };  
  
  actualizarSolicitudConcesionEvaluacionDetalle = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionResponsable}/actualizarSolicitudConcesionEvaluacionDetalle`, params);
  };

}
