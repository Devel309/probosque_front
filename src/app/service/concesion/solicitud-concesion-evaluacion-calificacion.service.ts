import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from 'src/app/service//config.service';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class SolicitudConcesionEvaluacionCalificacionService {
  base = environment.urlProcesos;
  baseSolicitudConcesionEvaluacionCalificacion = `${this.base}api/solicitudConcesionEvaluacionCalificacion`;

  constructor(private http: HttpClient, private config : ConfigService) { }

  listarSolicitudConcesionEvaluacionCalificacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEvaluacionCalificacion}/listarSolicitudConcesionEvaluacionCalificacion`, params);
  };
  
  registrarSolicitudConcesionEvaluacionCalificacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEvaluacionCalificacion}/registrarSolicitudConcesionEvaluacionCalificacion`, params);
  };

  actualizarSolicitudConcesionEvaluacionCalificacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudConcesionEvaluacionCalificacion}/actualizarSolicitudConcesionEvaluacionCalificacion`, params);
  };
  
}
