import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ProcesoOfertaService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  actualizarEstatusProcesoOferta = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoOferta/actualizarEstatusProcesoOferta',
      params
    );
  };

  eliminarEstatusProcesoOferta = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoOferta/eliminarProcesoOferta',
      params
    );
  };

}
