import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EMPTY, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { ResponseModel } from "../model/ResponseModel";
import { isNull } from "../shared/util";
import { Page, Pageable } from '@models';

@Injectable({
  providedIn: "root",
})
export class CensoForestalService {
  base = `${environment.urlProcesos}api/censoForestal`;

  constructor(private http: HttpClient) { }
  

  ResultadosEspecieMuestreo = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "/InfoPotencialProduccionForestal/ResultadosEspecieMuestreo?" + params,
      null
    );
  };

  ResultadosEspecieFrutales = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "/InfoPotencialProduccionForestal/ResultadosEspecieFrutales?" + params,
      null
    );
  };

  AprovechamientoRFNM = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "/AprovechamientoRFNM/Especies?" + params,
      null
    );
  };

  listarResultadosFustales = (body: any) => {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "/infoPotencialProduccionForestal/listarResultadosFustales?" + params,
      null
    );
  };

  listarProyeccionCosecha = (body: any) => {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/listarProyeccionCosecha?" + params, null);
  };

  listarVolumenComercialPromedio = (body: any) => {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan);
     
    return this.http.post(this.base + "/listarVolumenComercialPromedio?" + params, null);
  };

 

  listarEspecieVolumenComercialPromedio = (body: any) => {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan);
    return this.http.post(this.base + "/listarEspecieVolumenComercialPromedio?" + params, null);
  };
 

  listarEspecieVolumenCortaAnualPermisible = (body: any) => {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/listarEspecieVolumenCortaAnualPermisible?" + params, null);
  };

  arbolesAprovechablesMaderables = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanDeManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/PotencialProduccionForestal/ArbolesAprovechablesMaderables?" + params, null);
  };

  arbolesAprovechablesMaderablesFustales = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanDeManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/PotencialProduccionForestal/ArbolesAprovechablesMaderablesFustales?" + params, null);
  };

  censoComercialAprovechamientoMaderable = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanDeManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/CensoComercialAprovechamientoMaderable?" + params, null);
  };

  listaTHContratosObtener(body: any, page?: Page):Observable<Pageable<any[]>>  {
    return this.http.post<any>(this.base + "/listaTHContratosObtener", body);
  };
  listaTHContratoPorFiltro(body: any, page?: Page):Observable<Pageable<any[]>>  {
    return this.http.post<any>(this.base + "/listaTHContratoPorFiltro", body);
  };
  aprovechableForestalNoMaderable = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanDeManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/PotencialProduccionForestal/AprovechableForestalNoMaderable?" + params, null);
  };

  aprovechamientoNoMaderable = (body: any) => {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanDeManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/CensoComercial/AprovechamientoNoMaderable?" + params, null);
  };


  listarAprovechamientoRecursoForestalNoMaderable = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}/listarRecursosDiferentesCensoForestalDetalle`, params);
  }

  registrarCargaMasiva(
    archivo: File,
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}/cargaTipoPlanMasivoExcel`;
    const body = new FormData();

    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(uri, body)

  }

  registrarCargaMasivaTipoPlan(
    archivo: File,
    idPlanManejo: number,
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}/cargaMasivaTipoPlanExcel`;

    const params = new HttpParams()
      .set("idPlanManejo", String(idPlanManejo));

    const body = new FormData();

    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(uri, body, {params})

  }

  listarAnexoC = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}/censoForestalListar`, params);
  }

  registrarCensoForestalDet = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}/registrarCensoForestalDet`, params);
  }

  actualizarPeriodoAprovechamiento = (params: any) => {
    return this.http.post(this.base + "/actualizarPeriodoAprovechamiento", params);
  }
  
  eliminarCensoForestalDetalle = (body: any) => {
    const params = new HttpParams()
      .set("idCensoForestalDetalle", body.idCensoForestalDetalle.toString())
      .set("idUsuarioElimina", body.idUsuarioElimina.toString());

    return this.http.post(this.base + "/eliminarCensoForestalDetalle?" + params, null);
  };

  obtenerCensoForestal = (body: any) => {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "/obtenerCensoForestal?" + params, null);
  };

}
