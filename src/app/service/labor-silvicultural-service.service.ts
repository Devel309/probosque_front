import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

import { ResponseModel } from 'src/app/model/ResponseModel';
import { Observable } from 'rxjs';
import { Actividades, Detalle } from '../model/ActividadesSilviculturalesModel';

@Injectable({
  providedIn: 'root'
})
export class LaborSilviculturalServiceService {
  base = environment.urlProcesos;

  actividadSilvicultural = `${this.base}api/actividadSilvicultural/`

  constructor(private http: HttpClient) { }

  obtenerListaLaborSilvicultural(idPlanManejo: number, idTipo: number = 4): Observable<ResponseModel<Actividades>> {
    const uri = `${this.actividadSilvicultural}listarActividadSilviculturalTipo`;
    const params = new HttpParams()
      .set('idPlanManejo', idPlanManejo.toString())
      .set('idTipo', idTipo.toString());

    return this.http.get<ResponseModel<Actividades>>(uri, { params });
  }

  guardarLaborSilvicultural(laborSilvicultural: any) {
    const uri = `${this.actividadSilvicultural}registrarLaborSilvicultural`;
    return this.http.post(uri, [laborSilvicultural]);
  }

  eliminiarLaborSilvicultural(laborSilvicultural: any) {
    const uri = `${this.actividadSilvicultural}eliminarActividadSilviculturalDema`;
    return this.http.post(uri, laborSilvicultural);
  }

  obtenerListaLaborSilviculturalPMFI(idPlanManejo: number, idTipo: number): Observable<ResponseModel<Actividades>> {
    const uri = `${this.actividadSilvicultural}listarActividadSilviculturalPFDM`;
    const params = new HttpParams()
      .set('idPlanManejo', idPlanManejo.toString())
      .set('idTipo', idTipo.toString());

    return this.http.get<ResponseModel<Actividades>>(uri, { params });
  }
  
  obtenerListaLaborSilviculturalCabecera(idPlanManejo: number): Observable<ResponseModel<Actividades[]>> {
    const uri = `${this.actividadSilvicultural}listarActividadSilviculturalCabecera`;
    const params = new HttpParams()
      .set('idPlanManejo', idPlanManejo.toString());

    return this.http.get<ResponseModel<Actividades[]>>(uri, { params });
  }

  guardarSistemaManejo(param: any) {
    const uri = `${this.actividadSilvicultural}guardarSistemaManejoPFDM`;
    return this.http.post(uri, param);
  }

  eliminarActividadSilviculturalPFDM(param: any) {
    const uri = `${this.actividadSilvicultural}eliminarActividadSilviculturalPFDM`;
    return this.http.post(uri, [param]);
  }

}
