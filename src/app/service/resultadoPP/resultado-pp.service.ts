import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ResultadoPPService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarPPEvaluacion = (form: any) => {
    return this.http.post(
      this.base + 'api/resultadoPP/listarPPEvaluacion',
      form
    );
  };

  listarPorFiltroOtrogamientoProcesoPostulacion = (form: any) => {
    return this.http.post(this.base + 'api/resultadoPP/listarPorFiltroOtrogamientoProcesoPostulacion', form);
  };

  registrarNotas = (form: any) => {
    return this.http.post(this.base + 'api/resultadoPP/registrarNotas', form);
  };

  obtenerPrimerPostulante = (idProcesoOferta: number) => {
    return this.http.get(
      this.base + 'api/resultadoPP/obtenerPrimerPostulante/' + idProcesoOferta
    );
  };

  obtenerNotas = (idResultadoPP: number) => {
    return this.http.get(
      this.base + 'api/resultadoPP/obtenerNotas/' + idResultadoPP
    );
  };

  obtenerResultados = (idResultadoPP: number) => {
    return this.http.get(
      this.base + 'api/resultadoPP/obtenerResultados/' + idResultadoPP
    );
  };

  listarProcesosOferta = () => {
    return this.http.get(this.base + 'api/resultadoPP/listarProcesosOferta');
  };

  registrarResultados = (form: any) => {
    return this.http.post(
      this.base + 'api/resultadoPP/registrarResultados',
      form
    );
  };

  obtenerResultadoPPPorIdPP = (idProcesoPostulacion: number) => {
    return this.http.get(
      this.base + 'api/resultadoPP/obtenerResultadoPPPorIdPP/' + idProcesoPostulacion
    );
  };
}
