import { HttpClient, HttpClientModule, HttpEvent, HttpHandler, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';

import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  base = environment.urlSeguridad;

  constructor(private http: HttpClient, private config: ConfigService) { }

  validarLogin(params: any) {
    return this.http.post(this.base + 'api/autenticacion/ValidaloginApp', params);
  }
  login(params: any) {
    return this.http.post(this.base + 'api/autenticacion/login', params);
  }
  PedirCodigo(params: any) {
    return this.http.post(this.base + 'api/autenticacion/recuperarClave', params);
  }

  ObtenerMenu(params: any) {
    // return this.http.post('http://localhost:8081/mcsniffsseguridad-rest/api/autenticacion' ,params);
    return this.http.post(this.base + 'api/autenticacion/obtenerMenuUsuario', params);
  }

  ObtenerPermisosOpcion(params: any) {
    return this.http.post(this.base + 'api/perfilaccionopcion/perfilOpcionAccionListarPermisos', params).pipe(
      map((resp: any) => {
        let aux = {};
        if (resp.data && resp.data.length > 0) {
          aux = {
            isPerRegistrar: resp.data[0].perRegistrar ? true : false,
            isPerEditar: resp.data[0].perEditar ? true : false,
            isPerEliminar: resp.data[0].perEliminar ? true : false,
            isPerVisualizar: resp.data[0].perVisualizar ? true : false,
            isPerOtros: resp.data[0].perOtros ? true : false,
          }
          return aux;
        } else {
          aux = {
            isPerRegistrar: false,
            isPerEditar: false,
            isPerEliminar: false,
            isPerVisualizar: false,
            isPerOtros: false,
          }
          return aux;
        }
      }),
      catchError(() => {
        let aux = {
          isPerRegistrar: false,
          isPerEditar: false,
          isPerEliminar: false,
          isPerVisualizar: false,
          isPerOtros: false,
        }
        return of(aux);
      })
    );
  }


  private setSession(authResult: any) {
    const expiresAt = moment().add(authResult.expiresIn, 'second');

    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
  }
  logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
  }

  guardarCambioContraseniaLogin(params: any) {
    return this.http.post(this.base + 'api/autenticacion/guardarCambioContrasenialoginApp', params);
  }

  // public isLoggedIn() {
  //   return moment().isBefore(this.getExpiration());
  // }
  // isLoggedOut() {
  //   return !this.isLoggedIn();
  // }

  // getExpiration() {
  //   let expiration = localStorage.getItem("expires_at");
  //   const expiresAt = JSON.parse(expiration);
  //   return moment(expiresAt);
  // }    

}
