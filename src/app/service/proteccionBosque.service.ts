import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProteccionBosqueService  {


  base = environment.urlProcesos; //'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http : HttpClient, private config : ConfigService) { }

  configuracionProteccionBosqueDemarcacion(params : any){
    return this.http.post(this.base +'api/proteccionBosque/configuracionProteccionBosqueDemarcacion', params);
  }
  registrarBosqueDemarcacion(params : any){
    return this.http.post(this.base +'/api/proteccionBosque/registrarProteccionBosqueDemarcacion', params);
  }
  actualizarProteccionBosqueDemarcacion(params : any){
    return this.http.post(this.base +'api/proteccionBosque/actualizarProteccionBosqueDemarcacion', params);
  }
  obtenerProteccionBosqueDemarcacion(params : any){
    return this.http.post(this.base +'api/proteccionBosque/obtenerProteccionBosqueDemarcacion', params);
  }

  registrarProteccionBosqueAmbiental(params : any){
    return this.http.post(this.base +'api/proteccionBosque/registrarProteccionBosqueAmbiental', params);
  }
  listarPorFiltroProteccionBosqueAmbiental(params : any){
    return this.http.post(this.base +'api/proteccionBosque/listarPorFiltroProteccionBosqueAmbiental', params);
  }
  eliminarProteccionBosqueAmbiental(params : any){
    return this.http.post(this.base +'api/proteccionBosque/eliminarProteccionBosqueAmbiental', params);
  }
  registrarProteccionBosqueImpactoAmbiental(params : any){
    return this.http.post(this.base +'api/proteccionBosque/registrarProteccionBosqueImpactoAmbiental', params);
  }
  listarPorFiltroProteccionBosqueImpactoAmbiental(params : any){
    return this.http.post(this.base +'api/proteccionBosque/listarPorFiltroProteccionBosqueImpactoAmbiental', params);
  }
  eliminarProteccionBosqueImpactoAmbiental(params : any){
    return this.http.post(this.base +'api/proteccionBosque/eliminarProteccionBosqueImpactoAmbiental', params);
  }

  registrarProteccionBosque(params : any){
    return this.http.post(this.base +'api/proteccionBosque/registrarProteccionBosque', params);
  }


}
