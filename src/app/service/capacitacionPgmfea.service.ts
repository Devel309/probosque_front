import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CapacitacionPgmfeaService {


  base = environment.urlProcesos;
  constructor(private http : HttpClient, private config : ConfigService) { }

  registrarCapacitacionPgmfea(params : any){
    return this.http.post(this.base +'api/capacitacionPgmfea/registrarCapacitacionPgmfea', params);
  }
  listarCapacitacionPgmfea(params : any){
    return this.http.post(this.base +'api/capacitacionPgmfea/listarCapacitacionPgmfea', params);
  }
  eliminarCapacitacionPgmfea(params : any){
    return this.http.post(this.base +'api/capacitacionPgmfea/eliminarCapacitacionPgmfea', params);
  }

}
