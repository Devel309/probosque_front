import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class CondicionMinimaService {
  private base: string = '';

  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  validarRequisitos(params: any) {
    return this.http.post(
      this.base + 'api/condicionMinima/validarCondicionMinima',
      params
    );
  }

  obtenerCondicionMinima(idCondicionMinima: number) {
    return this.http.get(this.base + 'api/condicionMinima/obtenerCondicionMinima?idCondicionMinima=' + idCondicionMinima);
  }

  registrarCondicionMinima(params: any) {
    return this.http.post(
      this.base + 'api/condicionMinima/registrarCondicionMinima',
      params
    );
  }


}
