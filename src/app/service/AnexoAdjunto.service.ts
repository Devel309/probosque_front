
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AnexoAdjuntoService {


  base = environment.urlProcesos;
  constructor(private http : HttpClient, private config : ConfigService) { }

  marcarParaAmpliacionAnexoAdjunto(params : any){
    return this.http.post(this.base +'api/anexoAdjunto/marcarParaAmpliacionAnexoAdjunto', params);
  }
  listarAnexoAdjunto(params : any){
    return this.http.post(this.base +'api/anexoAdjunto/listarAnexoAdjunto', params);
  }
}
