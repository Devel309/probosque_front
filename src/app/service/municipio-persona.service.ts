import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class MunicipioLocalService {
  private base = environment.urlProcesos;
  private baseMuncipioLocal = `${this.base}api/municipioPersona`;

  constructor(private http: HttpClient) { }

  listarMunicipioPersona(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseMuncipioLocal}/listarMunicipioPersona`, params);
  };
}
