import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@env/environment';
import { ResponseModel } from '../model/ResponseModel';
@Injectable({ providedIn: 'root' })
export class InformacionAreaService {
  private base: string;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/solPlantacionForestal`;
  }

  listarSolPlantacionForestalArea(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/listarSolPlantacacionForestalArea`, params);
  }

  registrarSolicitudPlantacacionArea(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/registrarSolicitudPlantacacionArea`, params);
  }

  
}
