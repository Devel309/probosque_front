import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class SolicitudBosqueLocalComiteTecnicoService {
  base = environment.urlProcesos;
  baseSolicitudBosqueLocalComiteTecnico = `${this.base}api/solicitudBosqueLocalComiteTecnico`;


  constructor(private http: HttpClient) { }

  registrarComiteTecnicoSolicitudBosqueLocal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocalComiteTecnico}/registrarComiteTecnicoSolicitudBosqueLocal`, params);
  };
  listarSolicitudBosqueLocalComiteTecnico = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocalComiteTecnico}/listarSolicitudBosqueLocalComiteTecnico`, params);
  };

  eliminarSolicitudBosqueLocalComiteTecnico = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueLocalComiteTecnico}/eliminarSolicitudBosqueLocalComiteTecnico`, params);
  };





}
