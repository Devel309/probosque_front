import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class SolicitudBosqueLocalService {
  base = environment.urlProcesos;
  baseSolicitudBosque = `${this.base}api/solicitudBosqueLocal`;
  baseSolicitudBosqueEvaluacion = `${this.base}api/solicitudBosqueLocalEvaluacion`;


  constructor(private http: HttpClient) { }

  listarSolicitudBosqueLocal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/listarSolicitudBosqueLocal`, params);
  };
  registrarSolicitudBosqueLocal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/registrarSolicitudBosqueLocal`, params);
  };
  actualizarSolicitudBosqueLocal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/actualizarSolicitudBosqueLocal`, params);
  };
  obtenerSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/obtenerSolicitudBosqueLocal`, params);
  };

  registrarSolicitudBosqueLocalBeneficiario(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/registrarSolicitudBosqueLocalBeneficiario`, params);
  };
  listarSolicitudBosqueLocalBeneficiario(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/listarSolicitudBosqueLocalBeneficiario`, params);
  };
  eliminarSolicitudBosqueLocalBeneficiario(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/eliminarSolicitudBosqueLocalBeneficiario`, params);
  };

  registrarSolicitudBosqueLocalEvaluacion(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/registrarSolicitudBosqueLocalEvaluacion`, params);
  };

  listarSolicitudBosqueLocalEvaluacion(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/listarSolicitudBosqueLocalEvaluacion`, params);
  };

  listarAdjuntosSolicitudBL(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/listarAdjuntosSolicitudBosqueLocal`, params);
  };

  obtenerInfoAdjuntosSolicitudBL(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/obtenerInfoAdjuntosSolicitudBosqueLocal`, params);
  };

  registrarInfoAdjuntosSolicitudBosqueLocal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/registrarInfoAdjuntosSolicitudBosqueLocal`, params);
  }

  eliminarInfoAdjuntoSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/eliminarInfoAdjuntoSolicitudBosqueLocal`, params);
  };

  listarUsuarioSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/listarUsuarioSolicitudBosqueLocal`, params);
  };

  actualizarEstadoSolicitudBosqueLocal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/actualizarEstadoSolicitudBosqueLocal`, params);
  }

  enviarGobiernoLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/enviarGobiernoLocal`, params);
  };

  ArchivoPDFSolicitudBosqueLocalBeneficiario(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/descargarArchivoPDFSolicitudBosqueLocalBeneficiario`, params);
  };

  registrarInfoAdjuntoSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/registrarInfoAdjuntoSolicitudBosqueLocal`, params);
  };
  actualizarInfoAdjuntoSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/actualizarInfoAdjuntoSolicitudBosqueLocal`, params);
  };

  descargarEvaluacionComiteTecnico(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosqueEvaluacion}/descargarEvaluacionComiteTecnico`, params);
  };

  generarTHSolicitudBosqueLocal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/generarTHSolicitudBosqueLocal`, params);
  };

  notificarCreacionTH(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/notificarCreacionTH`, params);
  };

}
