import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class SolicitudBosqueLocalEstudioTecnicoService {
  base = environment.urlProcesos;
  baseSolicitudBosque = `${this.base}api/solicitudBosqueLocalEstudioTecnico`;



  constructor(private http: HttpClient) { }

  listarSolicitudBosqueLocalEstudioTecnico = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/listarSolicitudBosqueLocalEstudioTecnico`, params);
  };

  actualizarSolicitudBosqueLocalEstudioTecnico = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudBosque}/actualizarSolicitudBosqueLocalEstudioTecnico`, params);
  };

}
