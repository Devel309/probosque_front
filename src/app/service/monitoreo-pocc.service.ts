import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root',
})
export class monitoreoPoccService {
  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) {}

  listarMonitoreo(params: any) {
    return this.http.get(
      `${this.base}api/monitoreo/listarMonitoreoPOCC?` + params
    );
  }

  registrarMonitoreo(body: any) {
    return this.http.post(`${this.base}api/monitoreo/listarMonitoreoPOCC`, {
      body,
    });
  }

  eliminarMonitoreo(params: any) {
    return this.http.post(
      `${this.base}api/monitoreo/listarMonitoreoPOCC`,
      params
    );
  }
}
