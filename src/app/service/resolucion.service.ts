import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '../model/ResponseModel';
import { ConfigService } from './config.service';

@Injectable({ providedIn: 'root' })
export class ResolucionService {

  base = environment.urlProcesos;
  constructor(private http: HttpClient, private config: ConfigService) { }

  listarResolucion(params: any) {
    return this.http.post(this.base + 'api/resolucion/listarResolucion', params);
  }

  resolucionValidar(idPO: any) {
    return this.http.get<ResponseModel<any>>(this.base + 'api/resolucion/resolucionValidar/'+idPO);
  }

  registrarResolucionResultado(params: any) {
    return this.http.post<ResponseModel<any>>(this.base + 'api/resolucion/registrarResolucionResultado', params);
  }

  registrarResolucion(params: any) {
    return this.http.post<ResponseModel<any>>(this.base + 'api/resolucion/registrarResolucion', params);
  }
  actualizarResolucion(params: any) {
    return this.http.post<ResponseModel<any>>(this.base + 'api/resolucion/actualizarResolucion', params);
  }

  obtenerResolucion(params: any) {
    return this.http.post<ResponseModel<any>>(this.base + 'api/resolucion/obtenerResolucion', params);
  }


}
