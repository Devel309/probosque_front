import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable,from } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiGeoforestalService {
  base =  environment.urlApiGeoforestal;
  constructor(private http  : HttpClient) { }

  /* validarSuperposicionAprovechamiento(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/superposicion/aprovechamiento' ,params);
  }
  obtenerOrdenamientoForestal(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/ordenamiento/forestal',params);
  }
  identificarTipoBosque(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/identifica/tipos/bosques',params);
  }
  consultarDerechoForestal(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/derechos/otorgados',params);
  }
  validarSuperposicionOtorgamiento(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/superposicion/otorgamiento',params);
  }
  validarSuperposicionPlanificacion(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/superposicion/planificacion',params);
  }
  insertarDerechoForestal(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/modalidad/acceso',params);
  }
  actualizarDerechoForestal(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/modalidad/acceso',params);
  }
  eliminarDerechoForestal(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/modalidad/acceso',params);
  }
  consultarUnidadAprovechamiento(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/unidades/aprovechamiento',params);
  }
  insertarUnidadAprovechamiento(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/unidad/aprovechamiento',params);
  }
  actualizarUnidadAprovechamiento(params:any):Observable<any>{
    return this.http.put(this.base+'apigeoforestal/servicios/unidad/aprovechamiento',params);
  }
  consultarDivisionAdministrativayArbol(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/divisiones/administrativas/parametrica',params);
  }
  consultarDivisionAdministrativayArbolGeoespacial(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/divisiones/administrativas/geometrica',params);
  }
  insertarDivisionAdministrativayArbol(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/divisiones/administrativas',params);
  }
  actualizarDivisionAdministrativayArbol(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/divisiones/administrativas',params);
  }
  eliminarDivisionAdministrativayArbol(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/divisiones/administrativas',params);
  }
  validarCoordenadasUTMArbol(params:any):Observable<any>{
    return this.http.post(this.base+'apigeoforestal/servicios/coordenadas/arbol',params);
  } */
}
