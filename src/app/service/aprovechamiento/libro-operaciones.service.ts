import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class LibroOperacionesService {
  private base = environment.urlProcesos;
  private baseLO = `${this.base}api/libroOperaciones`;
  private baseLOPlanManejo = `${this.base}api/libroOperacionesPlanManejo`;

  constructor(private http: HttpClient) { }

  listarLibroOperaciones(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLO}/listarLibroOperaciones`, params);
  };
  listarEvaluacionLibroOperaciones(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLO}/listarEvaluacionLibroOperaciones`, params);
  };
  registrarLibroOperaciones(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLO}/registrarLibroOperaciones`, params);
  };

  actualizarLibroOperaciones(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLO}/actualizarLibroOperaciones`, params);
  };

  validarNroRegistroLibroOperaciones(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLO}/validarNroRegistroLibroOperaciones`, params);
  };

  listarLibroOperacionesPlanManejo(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLO}/listarLibroOperacionesPlanManejo`, params);
  };

  registrarLibroOperacionesPlanManejo(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLOPlanManejo}/registrarLibroOperacionesPlanManejo`, params);
  };

  eliminarLibroOperacionesPlanManejo(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseLOPlanManejo}/eliminarLibroOperacionesPlanManejo`, params);
  };

}
