import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';

@Injectable({
  providedIn: 'root'
})
export class SincronizacionService {
  private base = environment.urlProcesos;
  private baseSincronizacionCtroller = `${this.base}api/sincronizacion-controller`;
  private baseSincronizacion = `${this.base}api/sincronizacion`;
  private baseTala = `${this.base}api/tala`;
  private baseOrdenProduccion = `${this.base}api/ordenProduccion`;
  private baseListaTroza = `${this.base}api/listaTroza`;
  private baseGuiaTransporte = `${this.base}api/guiaTransporteForestal`;

  constructor(private http: HttpClient) { }

  sincronizarServ(idArchivoBackup:number ,idUsuarioCreacion:number , fileIn: File) {
    const params = new HttpParams()
      .set('idArchivoBackup', String(idArchivoBackup))
      .set('idUsuarioCreacion', String(idUsuarioCreacion))

    const body = new FormData();
		body.append("file", fileIn);
    return this.http.post<ResponseModel<any>>(`${this.baseSincronizacion}/sincronizar`, body,{params});
  };

  listarSincronizacionHistorico(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSincronizacion}/listarSincronizacionHistorico`, params);
  };

  eliminarSincronizacionHistorico(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSincronizacion}/eliminarSincronizacionHistorico`, params);
  };

  listarTala(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseTala}/listarTala`, params);
  };

  listarTalaDetalle(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseTala}/listarTalaDetalle`, params);
  };

  listarTalaDetallePorFiltro(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseTala}/listarTalaDetallePorFiltro`, params);
  };

  listarTrozado(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSincronizacion}/listarTrozado`, params);
  };

  listarPlanTrozado(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSincronizacion}/listarPlanTrozado`, params);
  };

  listarTransformacion(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSincronizacion}/listarTransformacion`, params);
  };

  listarOrdenProduccion(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseOrdenProduccion}/listarOrdenProduccion`, params);
  };

  listarOrdenProduccionProductoTerminado(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseOrdenProduccion}/listarOrdenProduccionProductoTerminado`, params);
  };
  listarListaTroza(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseListaTroza}/listarlistaTroza`, params);
  };
  listarListaTrozaDetalle(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseListaTroza}/listarListaTrozaDetalle`, params);
  };
  listarOrdenProduccionweb(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseOrdenProduccion}/listarOrdenProduccionWeb`, params);
  };
  listarOrdenProduccionProductoTerminadoweb(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseOrdenProduccion}/listarOrdenProduccionProductoTerminadoWeb`, params);
  };

  listarTrozaConsumida(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseListaTroza}/listarTrozaConsumida`, params);
  };

  listarGuiaTransporte(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseGuiaTransporte}/listarGuiaTransporteForestal`, params);
  };

  listarGuiaTransportePersona(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseGuiaTransporte}/listarGuiaTransporteForestalPersona`, params);
  };
}
