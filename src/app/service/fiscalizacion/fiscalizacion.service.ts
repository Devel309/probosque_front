import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class FiscalizacionService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarFiscalizacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}api/fiscalizacion/listarFiscalizacion`, params);
  };

  registrarFiscalizacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}api/fiscalizacion/registrarFiscalizacion`, params);
  };

  obtenerFiscalizacion = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}api/fiscalizacion/obtenerFiscalizacion`, params);
  };

  eliminarFiscalizacionArchivo = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.base}api/fiscalizacion/eliminarFiscalizacionArchivo`, params);
  };



  adjuntarArchivosFiscalizacion = (params: any) => {
    return this.http.post(
      this.base + 'api/fiscalizacion/adjuntarArchivosFiscalizacion',
      params
    );
  };

  obtenerArchivosFiscalizacion = (idFiscalizacion: number) => {
    return this.http.get(
      this.base +
      'api/fiscalizacion/obtenerArchivosFiscalizacion/' +
      idFiscalizacion
    );
  };

  descagarPlantillaFormatoFiscalizacion = () => {
    return this.http.get(this.base + 'api/fiscalizacion/descagarPlantillaFormatoFiscalizacion/');
  };
}
