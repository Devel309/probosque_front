import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '@env/environment';
import { ResponseModel } from '../model/ResponseModel';
@Injectable({ providedIn: 'root' })
export class InformacionBasicaService {
  private base: string;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/informacionBasica`;
  }

  listarFaunaInformacionBasicaDetalle(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/listarFaunaInformacionBasicaDetalle`, params);
  }

  listarPorFiltrosInfBasicaAerea(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/listarPorFiltrosInfBasicaAerea`, params);
  }

  
}
