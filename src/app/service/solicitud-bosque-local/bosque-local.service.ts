import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class BosqueLocalService {
  base = `${environment.urlProcesos}`;

  baseBosqueLocalFinalidad = this.base + 'api/solicitudBosqueLocalFinalidad';

  constructor(private http: HttpClient) {}


  listarSolicitudBosqueLocalFinalidad(params: any) {
    return this.http.post(this.baseBosqueLocalFinalidad + '/listarSolicitudBosqueLocalFinalidad',params);
  }

  registrarSolicitudBosqueLocalFinalidad(params: any) {
    return this.http.post(this.baseBosqueLocalFinalidad + '/registrarSolicitudBosqueLocalFinalidad',params);
  }

}
