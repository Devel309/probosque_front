import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ResponseModel } from '../model/ResponseModel';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class SeguridadService {


  base = environment.urlSeguridad; //environment.urlSeguridad;// 'http://10.6.1.162/mcsniffs-rest/';
  baseMcsniffs = environment.urlProcesos; //environment.urlProcesos;// 'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http : HttpClient, private config : ConfigService) { }
  
  /************Maestras*********** */
  listaComboEntidad(params : any){
    return this.http.post(this.base +'api/maestras/listaComboEntidad', params); 
  }
  listaComboGenerico(params : any){
    return this.http.post(this.base +'api/maestras/listaComboGenerico/' +params,null); 
  }
  listaComboRegion(params : any){
    return this.http.post(this.base +'api/maestras/listaComboRegion', params); 
  }
  listaComboAutoridad(params : any){
    return this.http.post(this.base +'api/maestras/listaComboAutoridad', params); 
  }
   
  listaComboEntidad1(params : any){
    return this.http.post(this.base +'api/entidad/listaComboEntidad', params); 
  }
 /************Usuario*********** */
  registrarUsuario(params : any){
    return this.http.post(this.base +'api/usuario/GuardarUsuario', params); 
  }
  bandejaUsuario(params : any){
    return this.http.post(this.base +'api/usuario/ListarUsuario', params); 
  }
  obtenerUsuario(params : any){
    return this.http.post(this.base +'api/usuario/ObtenerUsuario', params); 
  }
  eliminarUsuario(params : any){
    return this.http.post(this.base +'api/usuario/EliminarUsuario', params); 
  }
  listaComboUsuario(params : any){
    return this.http.post(this.base +'api/usuario/listaComboUsuario', params); 
  }

  ObtenerUsuarioID(params : any){
    return this.http.post(this.base +'api/usuario/ObtenerUsuarioID', params); 
  }

  /************perfil*********** */ 
  registrarPerfil(params : any){
    return this.http.post(this.base +'api/perfil/GuardarPerfil', params); 
  }
  bandejaPerfil(params : any){
    return this.http.post(this.base +'api/perfil/BandejaPerfil', params); 
  }
  obtenerPerfil(params : any){
    return this.http.post(this.base +'api/perfil/ObtenerPerfil', params); 
  }
  eliminarPerfil(params : any){
    return this.http.post(this.base +'api/perfil/EliminarPerfil', params); 
  }

  listaComboPerfil(params : any){
    return this.http.post(this.base +'api/perfil/ListaComboPerfil', params); 
  }

  /************accion*********** */ 
  registrarAccion(params : any){
    return this.http.post(this.base +'api/accion/GuardarAccion', params); 
  }
  bandejaAccion(params : any){
    return this.http.post(this.base +'api/accion/BandejaAccion', params); 
  }
  obtenerAccion(params : any){
    return this.http.post(this.base +'api/accion/ObtenerAccion', params); 
  }
  eliminarAccion(params : any){
    return this.http.post(this.base +'api/accion/EliminarAccion', params); 
  }
  listaComboAccion(params : any){
    return this.http.post(this.base +'api/accion/listaComboAccion', params); 
  }

  /************opciones*********** */ 
  registrarOpcion(params : any){
    return this.http.post(this.base +'api/opcion/GuardarOpcion', params); 
  }
  bandejaOpcion(params : any){
    return this.http.post(this.base +'api/opcion/BandejaOpcion', params); 
  }
  obtenerOpcion(params : any){
    return this.http.post(this.base +'api/opcion/ObtenerOpcion', params); 
  }
  eliminarOpcion(params : any){
    return this.http.post(this.base +'api/opcion/EliminarOpcion', params); 
  }
  listaComboOpcion(params : any){
    return this.http.post(this.base +'api/opcion/listaComboOpcion', params); 
  }
  listaComboOpcionMenuAplicacion(params : any){
    return this.http.post(this.base +'api/opcion/listaComboOpcionMenuAplicacion', params); 
  }
  
  /************entidad*********** */ 
  registrarEntidad(params : any){
    return this.http.post(this.base +'api/entidad/GuardarEntidad', params); 
  }
  bandejaEntidad(params : any){
    return this.http.post(this.base +'api/entidad/BandejaEntidad', params); 
  }
  obtenerEntidad(params : any){
    return this.http.post(this.base +'api/entidad/ObtenerEntidad', params); 
  }
  eliminarEntidad(params : any){
    return this.http.post(this.base +'api/entidad/EliminarEntidad', params); 
  }

  /************parametros*********** */
  tipoParametroListar(params : any){
    return this.http.post<ResponseModel<any>>(this.baseMcsniffs + 'api/tipoParametro/tipoParametroListar', params); 
  }
  parametroActualizar(params : any){
    return this.http.post<ResponseModel<any>>(this.baseMcsniffs + 'api/tipoParametro/parametroActualizar', params); 
  }

  /************aplicacion*********** */ 
  registrarAplicacion(params : any){
    return this.http.post(this.base +'api/aplicacion/GuardarAplicacion', params); 
  }
  bandejaAplicacion(params : any){
    return this.http.post(this.base +'api/aplicacion/BandejaAplicacion', params); 
  }
  obtenerAplicacion(params : any){
    return this.http.post(this.base +'api/aplicacion/ObtenerAplicacion', params); 
  }
  eliminarAplicacion(params : any){
    return this.http.post(this.base +'api/aplicacion/EliminarAplicacion', params); 
  }
  listaComboAplicacion(params : any){
    return this.http.post(this.base +'api/aplicacion/ListaComboAplicacion', params); 
  }
  
  /******acccion-opcion  Apliacacion-Perfil***** */ 
  bandejaOpcionAccionAplicacionPerfil(params : any){
    return this.http.post(this.base +'api/accionopcionperfil/BandejaPerfilOpcionAccion', params); 
  }
  registrarOpcionAccionAplicacionPerfil(params : any){
    return this.http.post(this.base +'api/accionopcionperfil/GuardarPerfilOpcionAccion', params); 
  }
  obtenerOpcionAccionAplicacionPerfil(params : any){
    return this.http.post(this.base +'api/accionopcionperfil/ObtenerPerfilOpcionAccion', params); 
  }
  eliminarOpcionAccionAplicacionPerfil(params : any){
    return this.http.post(this.base +'api/accionopcionperfil/EliminarPerfilOpcionAccion', params); 
  }

 /******aplicacion-perfil-usuario***** */ 
    registrarAplicacionPerfilUsuario(params : any){
      return this.http.post(this.base +'api/perfilaplicacionusuario/GuardarPerfilAplicacionUsuario', params); 
    }
    bandejaAplicacionPerfilUsuario(params : any){
      return this.http.post(this.base +'api/perfilaplicacionusuario/BandejaAplicacionPerfilUsuario', params); 
    }
    obtenerAplicacionPerfilUsuario(params : any){
      return this.http.post(this.base +'api/perfilaplicacionusuario/ObtenerAplicacionPerfilUsuario', params); 
    }
    eliminarAplicacionPerfilUsuario(params : any){
      return this.http.post(this.base +'api/perfilaplicacionusuario/EliminarAplicacionPerfilUsuario', params); 
    }

    listarUsuarioPerfil(params : any){
      return this.http.post(this.base +'api/aplicacion/listarUsuarioPerfil', params); 
    }

   /*****perfil Aplicacion***** */ 

   registrarPerfilAplicacion(params : any){
    return this.http.post(this.base +'api/perfilaplicacion/GuardarPerfilAplicacion', params); 
  }
  bandejaPerfilAplicacion(params : any){
    return this.http.post(this.base +'api/perfilaplicacion/BandejaPerfilAplicacion', params); 
  }
  obtenerPerfilAplicacion(params : any){
    return this.http.post(this.base +'api/perfilaplicacion/ObtenerPerfilAplicacion', params); 
  }
  eliminarPerfilAplicacion(params : any){
    return this.http.post(this.base +'api/perfilaplicacion/EliminarPerfilAplicacion', params); 
  }
  listaComboPerfilAplicacion(params : any){
    return this.http.post(this.base +'api/perfilaplicacion/listaComboPerfilAplicacion', params); 
  }

  
  /*****Accion  Opcion***** */ 

  registrarOpcionAccion(params : any){
    return this.http.post(this.base +'api/accionopcion/GuardarOpcionAccion', params); 
  }
  bandejaOpcionAccion(params : any){
    return this.http.post(this.base +'api/accionopcion/BandejaOpcionAccion', params); 
  }
  obtenerOpcionAccion(params : any){
    return this.http.post(this.base +'api/accionopcion/ObtenerOpcionAccion', params); 
  }
  eliminarOpcionAccion(params : any){
    return this.http.post(this.base +'api/accionopcion/EliminarOpcionAccion', params); 
  }
  listaComboOpcionAccion(params : any){
    return this.http.post(this.base +'api/accionopcion/listaComboOpcionAccion', params); 
  }

  exportarUsuariosExcel(params : any){
    return this.http.post(this.base +'api/usuario/exportarUsuariosExcel', params); 
  }

  /*****Permiso Perfil***** */
  perfilOpcionAccionListar(params : any){
    return this.http.post<ResponseModel<any>>(this.base +'api/perfilaccionopcion/perfilOpcionAccionListar', params); 
  }

  perfilOpcionAccionRegistrar(params : any){
    return this.http.post<ResponseModel<any>>(this.base +'api/perfilaccionopcion/perfilOpcionAccionRegistrar', params); 
  }

}
