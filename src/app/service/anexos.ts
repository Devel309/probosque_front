import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
import { ResponseModel } from "@models";

@Injectable({ providedIn: "root" })
export class AnexosCargaExcelService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  FormatoPGMFFormuladoAnexo2Excel(
    archivo: File,
    tipoPlan: string,
    idPlanDeManejo: number
  ): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(idPlanDeManejo))
      .set("tipoPlan", tipoPlan);

    const body = new FormData();
    body.append("file", archivo);

    return this.http.post<ResponseModel<number>>(
      this.base + "api/censoForestal/FormatoPGMFFormuladoAnexo2Excel",
      body,
      { params, reportProgress: true }
    );
  }

  FormatoPGMFFormuladoAnexo3Excel(
    archivo: File,
    tipoPlan: string,
    idPlanDeManejo: number
  ): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(idPlanDeManejo))
      .set("tipoPlan", tipoPlan);

    const body = new FormData();
    body.append("file", archivo);

    return this.http.post<ResponseModel<number>>(
      this.base + "api/censoForestal/FormatoPGMFFormuladoAnexo3Excel",
      body,
      { params, reportProgress: true }
    );
  }

  cargaTipoPlanMasivoExcel(
    archivo: File,
    tipoPlan: string,
    idPlanDeManejo: number
  ): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(idPlanDeManejo))
      .set("tipoPlan", tipoPlan);

    const body = new FormData();
    body.append("file", archivo);

    return this.http.post<ResponseModel<number>>(
      this.base + "api/censoForestal/cargaTipoPlanMasivoExcel",
      body,
      { params, reportProgress: true }
    );
  }
}
