import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ProcesoPostulaionService {
  base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  obtenerAutorizacion(params: any) {
    return this.http.post(
      this.base + 'api/procesoPostulacion/obtenerAutorizacion', params
    );
  }

  autorizarPublicacion(params: any, formd: FormData) {
    return this.http.post(
      this.base + 'api/procesoPostulacion/autorizarPublicacion?' + params,
      formd
    );
  }

  adjuntarResolucionDenegada(params: any, formd: FormData) {
    return this.http.post(
      this.base + 'api/procesoPostulacion/AdjuntarResolucionDenegada?' + params,
      formd
    );
  }

  listarProcesoPostulacion = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/listarProcesoPostulacion',
      params
    );
  };

  obtenerProcesoPostulacion = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/obtenerProcesoPostulacion',
      params
    );
  };

  tieneObservacion = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/tieneObservacion',
      params
    );
  };

  actualizarProcesoPostulacion = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/actualizarProcesoPostulacion',
      params
    );
  };

  obtenerProcesoPostulacionValidarRequisito = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/obtenerProcesoPostulacionValidarRequisito',
      params
    );
  };

  actualizarObservacionARFFSProcesoPostulacion = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/actualizarObservacionARFFSProcesoPostulacion',
      params
    );
  };

  actualizarResolucionProcesoPostulacion = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/actualizarResolucionProcesoPostulacion',
      params
    );
  };

  guardarAutorizacionPublicacion(params: any) {
    return this.http.post(this.base + 'api/autorizacionPublicacion/guardarAutorizacionPublicacionSolicitud', params);
  }

  enviarAutorizacionPublicacion(params: any) {
    return this.http.post(this.base + 'api/autorizacionPublicacion/enviarAutorizacionPublicacionSolicitud', params);

}

  descargarPlantillaCartaAutorizacion = () => {
    return this.http.get(this.base + 'api/autorizacionPublicacion/descargarPlantillaCartaAutorizacion/');
  };

  actualizarEnvioPrueba = (params: any) => {
    return this.http.post(
      this.base + 'api/procesoPostulacion/actualizarEnvioPrueba',
      params
    );
  };

  validarPublicacionId(params: any) {
    return this.http.post(
      this.base + 'api/procesoPostulacion/validarPublicacionId', params
    );
  }


}
