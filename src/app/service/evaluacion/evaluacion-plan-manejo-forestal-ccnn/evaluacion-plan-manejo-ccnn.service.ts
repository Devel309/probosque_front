import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import {Observable} from 'rxjs';
import {ResponseModel} from '@models';
import {tap} from 'rxjs/operators';

@Injectable({ providedIn: "root" })
export class EvaluacionPlanManejoCcnnService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  listarProcedimientoAdministrativo(params: any) {
    return this.http.post(this.base + "api/procedimientoAdministrativo/listarProcedimientoAdministrativo", params);
  }


  /*registrarEvaluacionPlanManejo(params: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}api/evaluacion/registrarEvaluacion`
    return this.http.post<ResponseModel<any>>(uri, params)
      .pipe(tap({ error: err => console.log(err) }));
  }*/

  registrarEvaluacionPlanManejo(params: any) {
    const uri = `${this.base}api/evaluacion/registrarEvaluacion`;
    return this.http.post(uri, params);
    //return this.http.post<ResponseModel<any>>(uri, params).pipe(tap({ error: err => console.log(err) }));
  }

}
