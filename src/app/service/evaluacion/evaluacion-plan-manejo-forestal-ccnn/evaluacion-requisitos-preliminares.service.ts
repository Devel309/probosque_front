import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class EvaluacionRequisitosPreliminaresService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {

  }

  registrarRequisito(params: any) {
    return this.http.post(this.base + "api/requisitoEvaluacion/registrarRequisito", params);
  }

  listarRequisito(params: any) {
    return this.http.post(this.base + "api/requisitoEvaluacion/listarRequisito", params);
  }

  eliminarRequisitoDetalle(params: any) {
    return this.http.post(this.base + "api/requisitoEvaluacion/eliminarRequisitoDetalle", params);
  }

}
