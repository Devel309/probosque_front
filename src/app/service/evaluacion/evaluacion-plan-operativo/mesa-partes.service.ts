import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class MesaPartesService {

  base = `${environment.urlProcesos}api/mesaPartes/`;

  constructor(private http: HttpClient,) { }

  listarMesaPartes(params: any) {
    return this.http.post(`${this.base}listarMesaPartes`, params );
  }

  listarMesaPartesDetalle(params: any) {
    return this.http.post(`${this.base}listarMesaPartesDetalle`, params );
  }

  registrarMesaPartes(params: any) {
    return this.http.post(`${this.base}registrarMesaPartes`, params );
  }

  actualizarMesaPartes(params: any) {
    return this.http.post(`${this.base}actualizarMesaPartes`, params );
  }

  eliminarMesaPartes(params: any) {
    return this.http.post(`${this.base}eliminarMesaPartes`, params );
  }

  eliminarMesaPartesDetalle(params: any) {
    return this.http.post(`${this.base}eliminarMesaPartesDetalle`, params );
  }


  listarMesaPartesPmficDema(body: any) {
    return this.http.post(`${this.base}listarMesaPartesPMFIDEMA`, body );
  }

  registrarMesaPartesPmficDema(body: any) {
    return this.http.post(`${this.base}registrarMesaPartesPMFIDEMA`, body );
  }


  
  registrarMesaPartesMdp(body: any) {
    return this.http.post(`${this.base}registrarMesaPartes`, body);
  }
}
