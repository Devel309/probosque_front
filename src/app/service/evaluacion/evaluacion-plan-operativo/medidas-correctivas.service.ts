import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class MedidasCorrectivasService {

  base = `${environment.urlProcesos}api/medidaEvaluacion/`;

  constructor(private http: HttpClient,) { }

  listarMedida(params: any) {
    return this.http.post(`${this.base}listarMedida`, params );
  }

  listarMedidaDetalle(params: any) {
    return this.http.post(`${this.base}listarMedidaDetalle`, params );
  }

  registrarMedida(params: any) {
    return this.http.post(`${this.base}registrarMedida`, params );
  }

  actualizarMedida(params: any) {
    return this.http.post(`${this.base}actualizarMedida`, params );
  }

  eliminarMedida(params: any) {
    return this.http.post(`${this.base}eliminarMedida`, params );
  }

  eliminarMedidaDetalle(params: any) {
    return this.http.post(`${this.base}eliminarMedidaDetalle`, params );
  }

}
