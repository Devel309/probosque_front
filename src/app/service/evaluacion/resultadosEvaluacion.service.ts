import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ResultadosEvaluacionService {
  base = `${environment.urlProcesos}api/evaluacionResultado/`;

  constructor(private http: HttpClient) {}

  listarEvaluacionesultado(params: any) {
    

    const param = new HttpParams()
      .set("idPlanManejo", String(params.idPlanManejo))
      .set("codResultado", params.codResultado)
      .set("subCodResultado", params.subCodResultado);
    return this.http.post(
      `${this.base}listarEvaluacionesultado?` + param,
      null
    );
  }

  registrarEvaluacionResultado(params: any) {
    return this.http.post(`${this.base}registrarEvaluacionResultado`, params);
  }

  eliminarEvaluacionesultado(params: any) {
    return this.http.post(`${this.base}eliminarEvaluacionesultado`, params);
  }
}
