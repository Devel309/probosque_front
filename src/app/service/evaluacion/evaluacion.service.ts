import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Page, Pageable, ResponseModel } from '@models';
import { tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { isNull } from '@shared';

@Injectable({ providedIn: 'root' })
export class EvaluacionService {
  base = `${environment.urlProcesos}`;

  baseEvaluacion = this.base + 'api/evaluacion';

  constructor(private http: HttpClient) {}

  listarProcedimientoAdministrativo(params: any) {
    return this.http.post(
      this.base +
        'api/procedimientoAdministrativo/listarProcedimientoAdministrativo',
      params
    );
  }

  registrarEvaluacionPlanManejo(params: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}api/evaluacion/registrarEvaluacion`;
    return this.http
      .post<ResponseModel<any>>(uri, params)
      .pipe(tap({ error: (err) => console.log(err) }));
  }

  registrarEvaluacionPermisoForestal(
    params: any
  ): Observable<ResponseModel<any>> {
    const uri = `${this.base}api/evaluacion/registrarEvaluacionPermisoForestal`;
    return this.http
      .post<ResponseModel<any>>(uri, params)
      .pipe(tap({ error: (err) => console.log(err) }));
  }

  obtenerEvaluacion(params: any) {
    return this.http.post(
      this.base + 'api/evaluacion/listarEvaluacion',
      params
    );
  }

  obtenerEvaluacionDetalle(params: any) {
    return this.http.post(
      this.base + 'api/evaluacion/listarEvaluacionDetalle',
      params
    );
  }

  obtenerEvaluacionPermisoForestal(params: any) {
    return this.http.post(
      this.base + 'api/evaluacion/listarEvaluacionPermisoForestal',
      params
    );
  }

  obtenerInspectorAndTipoDocumento(params: any) {
    return this.http.post(
      this.baseEvaluacion + '/listarInspeccionTipoDocumento',
      params
    );
  }

  listarParametroPorPrefijo(params: any) {
    return this.http.post(
      this.base + 'api/generico/listarParametroPorPrefijo',
      params
    );
  }

  listarParametroLineamiento(params: any) {
    return this.http.post(
      this.base + 'api/generico/listarParametroLineamiento',
      params
    );
  }

  eliminarEvaluacionDetalle(params: any) {
    return this.http.post(
      this.baseEvaluacion + '/eliminarEvaluacionDetalle',
      params
    );
  }

  filtrarEvaluacion(
    r: {
      codigoUnico?: string;
      modalidadTH?: string;
      codigoEstado?: string;
      idTipoPlan?: string;
      dniElaborador?: string;
    },
    page?: Page
  ): Observable<Pageable<any[]>> {
    let p = new HttpParams();

    if (!isNull(r.codigoUnico)) p = p.set('codigoUnico', String(r.codigoUnico));
    if (!isNull(r.modalidadTH))
      p = p.set('modalidadTH', String(r.modalidadTH));
    if (!isNull(r.dniElaborador))
      p = p.set('dniElaborador', String(r.dniElaborador));
    if (!isNull(r.codigoEstado))
      p = p.set('codigoEstado', String(r.codigoEstado));
    if (!isNull(r.idTipoPlan))
      p = p.set('idTipoPlan', String(r.idTipoPlan));

    // pagination and sort
    if (!isNull(page?.pageNumber))
      p = p.set('pageNumber', String(page?.pageNumber));
    if (!isNull(page?.pageSize)) p = p.set('pageSize', String(page?.pageSize));
    if (!isNull(page?.sortField))
      p = p.set('sortField', String(page?.sortField));
    if (!isNull(page?.sortType)) p = p.set('sortType', String(page?.sortType));

    const uri = `${this.base}api/evaluacion/filtrarEval`;
    return this.http.get<Pageable<any[]>>(uri, { params: p }).pipe(
      tap({
        error: (err) => console.log(err),
      })
    );
  }

  filtrarEvaluacionOtorgamientoPermiso(
    r: {
      idPermisoForestal?: number;
      idTipoProceso?: number;
      idTipoPermiso?: number;
      idContrato?: number;
      fechaRegistro?: string;
      codigoEstado?: string;
      codigoPerfil?: string;
    },
    page?: Page
  ): Observable<Pageable<any[]>> {
    let p = new HttpParams();

    if (!isNull(r.idPermisoForestal))
      p = p.set('idPermisoForestal', String(r.idPermisoForestal));
    if (!isNull(r.idTipoProceso))
      p = p.set('idTipoProceso', String(r.idTipoProceso));
    if (!isNull(r.idTipoPermiso))
      p = p.set('idTipoPermiso', String(r.idTipoPermiso));
    if (!isNull(r.idContrato)) p = p.set('idContrato', String(r.idContrato));
    if (!isNull(r.fechaRegistro))
      p = p.set('fechaRegistro', String(r.fechaRegistro));
    if (!isNull(r.codigoEstado))
      p = p.set('codigoEstado', String(r.codigoEstado));
    if (!isNull(r.codigoPerfil))
      p = p.set('codigoPerfil', String(r.codigoPerfil));
    // pagination and sort
    if (!isNull(page?.pageNumber))
      p = p.set('pageNumber', String(page?.pageNumber));
    if (!isNull(page?.pageSize)) p = p.set('pageSize', String(page?.pageSize));
    if (!isNull(page?.sortField))
      p = p.set('sortField', String(page?.sortField));
    if (!isNull(page?.sortType)) p = p.set('sortType', String(page?.sortType));

    const uri = `${this.base}api/permisoForestal/filtrarEval`;
    return this.http.get<Pageable<any[]>>(uri, { params: p }).pipe(
      tap({
        error: (err) => console.log(err),
      })
    );
  }

  generarInformacionEvaluacion(body: any) {
    return this.http.post(
      `${this.base}api/evaluacion/generarInformacionEvaluacion`,
      body
    );
  }

  generarInformeDEMAPMFI(idPlanManejo: number, tipoProceso:string) {

    const params = new HttpParams().set('idPlanManejo', idPlanManejo+"")
        .set('tipoProceso', tipoProceso);
    return this.http.get(`${this.base}api/evaluacion/plantillaInformeEvaluacion`, { params });

  }

  actualizarPlanManejo(body: any) {
    return this.http.post(
      `${this.base}api/evaluacion/evaluacionActualizarPlanManejo`,
      body
    );
  }

  listarPlanManejoEvaluacionPmfiDema(
    plan:any,
    evaluacionRequest: {
      idPlanManejo?: any;
      idContrato?: any;
      dniElaborador?: any;
      rucComunidad?: any;
      nombreElaborador?: any;
    },
    page?: Page
  ): Observable<Pageable<any[]>> {
    let p = new HttpParams();
    if (!isNull(evaluacionRequest.idPlanManejo))
      p = p.set('idPlanManejo', String(evaluacionRequest.idPlanManejo));
    if (!isNull(plan.idTipoProceso))
      p = p.set('idTipoProceso', String(plan.idTipoProceso));
    if (!isNull(plan.idTipoPlan)) p = p.set('idTipoPlan', String(plan.idTipoPlan));
    if (!isNull(evaluacionRequest.idContrato)) p = p.set('idContrato', String(evaluacionRequest.idContrato));
    if (!isNull(evaluacionRequest.dniElaborador))
      p = p.set('dniElaborador', String(evaluacionRequest.dniElaborador));
    if (!isNull(evaluacionRequest.rucComunidad))
      p = p.set('rucComunidad', String(evaluacionRequest.rucComunidad));
    if (!isNull(evaluacionRequest.nombreElaborador))
      p = p.set('nombreTitular', String(evaluacionRequest.nombreElaborador));



    if (!isNull(plan.codigoEstado))
      p = p.set('codigoEstado', String(plan.codigoEstado));

    // pagination and sort
    if (!isNull(page?.pageNumber))
      p = p.set('pageNumber', String(page?.pageNumber));
    if (!isNull(page?.pageSize)) p = p.set('pageSize', String(page?.pageSize));
    if (!isNull(page?.sortField))
      p = p.set('sortField', String(page?.sortField));
    if (!isNull(page?.sortType)) p = p.set('sortType', String(page?.sortType));

    const uri = `${this.base}api/evaluacion/filtrarEvaluacionPlan`;
    return this.http.get<Pageable<any[]>>(uri, { params: p });
  }

  listarEvaluacionResumido(params: any) {
    return this.http.post(
      this.base + 'api/evaluacion/listarEvaluacionResumido',
      params
    );
  }
}
