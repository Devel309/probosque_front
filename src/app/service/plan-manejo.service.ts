import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '@env/environment';
import { isNull } from '@shared';
import { Page, Pageable, ResponseModel } from '@models';
@Injectable({ providedIn: 'root' })
export class PlanManejoService {
  private base: string;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/planManejo`;
  }

  registrarPlanManejo(params: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}/registrarPlanManejo`
    return this.http.post<ResponseModel<any>>(uri, params)
      .pipe(tap({ error: err => console.log(err) }));
  }



  listarPlanManejo(params: any): Observable<ResponseModel<any[]>> {
    const uri = `${this.base}/listarPlanManejo`
    return this.http.post<ResponseModel<any[]>>(uri, params)
      .pipe(tap({ error: err => console.log(err) }));
  }

  filtrar(r: {
    idPlanManejo?: number,
    idTipoProceso?: number,
    idTipoPlan?: number,
    idContrato?: number,
    dniElaborador?: string,
    rucComunidad?: string,
    nombreElaborador?: string,
    codigoEstado?: string
  }, page?: Page): Observable<Pageable<any[]>> {
    let p = new HttpParams();

    if (!isNull(r.idPlanManejo)) p = p.set('idPlanManejo', String(r.idPlanManejo));
    if (!isNull(r.idTipoProceso)) p = p.set('idTipoProceso', String(r.idTipoProceso))
    if (!isNull(r.idTipoPlan)) p = p.set('idTipoPlan', String(r.idTipoPlan));
    if (!isNull(r.idContrato)) p = p.set('idContrato', String(r.idContrato));
    if (!isNull(r.dniElaborador)) p = p.set('dniElaborador', String(r.dniElaborador));
    if (!isNull(r.rucComunidad)) p = p.set('rucComunidad', String((r.rucComunidad)));
    if (!isNull(r.nombreElaborador)) p = p.set('nombreTitular', String((r.nombreElaborador)));
    if (!isNull(r.codigoEstado)) p = p.set('codigoEstado', String((r.codigoEstado)));

    // pagination and sort
    if (!isNull(page?.pageNumber)) p = p.set('pageNumber', String((page?.pageNumber)));
    if (!isNull(page?.pageSize)) p = p.set('pageSize', String((page?.pageSize)));
    if (!isNull(page?.sortField)) p = p.set('sortField', String((page?.sortField)));
    if (!isNull(page?.sortType)) p = p.set('sortType', String((page?.sortType)));

    const uri = `${this.base}`;
    return this.http.get<Pageable<any[]>>(uri, { params: p })
      .pipe(tap({
        error: err => console.log(err)
      }));

  }

  listarPorFiltroPlanManejoArchivo(params: any) {
    return this.http.post(`${this.base}/listarPorFiltroPlanManejoArchivo`, params);
  }

  regitrarArchivosPlanManejo(params: any, file: any) {

    let formData = new FormData();
    formData.append("idUsuarioRegistro", params.idUsuarioRegistro);
    formData.append("idPlanManejo", params.idPlanManejo);
    formData.append("idTipoDocumento", params.idTipoDocumento);
    formData.append("attachments", file);

    const req = new HttpRequest('POST', `${this.base}/regitrarArchivosPlanManejo`, formData, {
      reportProgress: true
    });
    return this.http.request(req);

  }

  regitrarArchivoPlanManejo(params: any, file: any) {
    let formData = new FormData();
    formData.append("idPlanManejoArchivo", params.idPlanManejoArchivo);
    formData.append("idUsuarioRegistro", params.idUsuarioRegistro);
    formData.append("idPlanManejo", params.idPlanManejo);
    formData.append("codTipo", params.codTipo);
    formData.append("subCodTipo", params.subCodTipo);
    formData.append("codTipoDocumento", params.codTipoDocumento);
    formData.append("file", file);

    return this.http.post(`${this.base}/registrarArchivoPlanManejo`, formData, {reportProgress: true});
    // const req = new HttpRequest('POST', `${this.base}/regitrarArchivoPlanManejo`, formData, {
    //   reportProgress: true
    // });
    // return this.http.request(req);
  }

  eliminarPlanManejoArchivo(params: any) {
    return this.http.post(`${this.base}/eliminarPlanManejoArchivo`, params);
  }

  actualizarPlanManejoArchivo(params: any) {
    return this.http.post(`${this.base}/actualizarPlanManejoArchivo`, params);
  }

  obtenerPlanManejoArchivo(params: any) {
    return this.http.post(`${this.base}/obtenerPlanManejoArchivo`, params);
  }

  listarPlanManejoEvaluacionIter(params : any){
    return this.http.post(this.base +'/listarPlanManejoEvaluacionIter', params);
  }

  enviarEvaluacion(params : any){
    return this.http.post(this.base +'/enviarEvaluacion', params);
  }

  listarPlanManejoContrato(idPlanManejo: any) {
    return this.http.get(this.base + '/listarPlanManejoContrato?&idPlanManejo=' + idPlanManejo);
  }

  guardarPlanManejoArchivo(dato: any,fromdata: FormData):Observable<any> {
    return this.http.post<any>(this.base + `/guardarPlanManejoArchivo?`+ dato,fromdata);
  }

  obtenerPlanManejo(params : any){
    return this.http.post(this.base +'/obtenerPlanManejo', params);
  }

  actualizarPlanManejoEstado(params: any) {
    return this.http.post(`${this.base}/actualizarPlanManejoEstado`, params);
  }

  listarPlanManejoAnexoArchivo(params: any) {
    return this.http.post(`${this.base}/listarPlanManejoAnexoArchivo`, params);
  }

  listarPorFiltroPlanManejoContrato(body: any) {
    let params = new HttpParams()
      .set('nroDocumento', body.nroDocumento);
    if (body.codTipoPlan) {
      params = params.set('codTipoPlan', body.codTipoPlan);
    }
    return this.http.post(`${this.base}/listarPorFiltroPlanManejoContrato?`+ params,
    null);
  }

  registrarPlanManejoContrato(params: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}/registrarPlanManejoContrato`
    return this.http.post<ResponseModel<any>>(uri, params)
      .pipe(tap({ error: err => console.log(err) }));
  }

  listarPorPlanManejoTipoBosque(params: any) {
    let formData = new FormData();
    formData.append("idPlanDeManejo", params.idPlanDeManejo);
    formData.append("tipoPlan", params.tipoPlan);
    formData.append("idTipoBosque", params.idTipoBosque);

    return this.http.post(`${this.base}/listarPorPlanManejoTipoBosque`, formData, {reportProgress: true});
  }


  descargarFormatoPlantacionesForestales() {
    return this.http.get(this.base + '/descargarFormatoPlantacionesForestales');
  }

  /* N3.1.4 */
  listarColindanciaPredio(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/listarColindanciaPredio`, params);
  }

  registrarColindanciaPredio(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/registrarColindanciaPredio`, params);
  }

  registrarGrupoEspeciePlanManejo(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/registrarGrupoEspeciePlanManejo`, params);
  }

  descargarDeclaracionManejoProductosForestales_PDF(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}/descargarDeclaracionManejoProductosForestales_PDF`, params);
  }

  /***********/

}
