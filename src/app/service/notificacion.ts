import { HttpClient, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { ResponseModel } from "../model/ResponseModel";

@Injectable({ providedIn: "root" })
export class NotificacionService {
  base: string;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/notificacion`;
  }

  listarNotificacion(params: any) {
    return this.http.post(`${this.base}/listarNotificacion`, params);
  }

  eliminarNotificacion(params: any) {
    return this.http.post(`${this.base}/eliminarNotificacion`, params);
  }

  registrarNotificacion(params: any): Observable<ResponseModel<any>> {
    const   uri = `${this.base}/registrarNotificacion`;
    return this.http
      .post<ResponseModel<any>>(uri, params)
      .pipe(tap({ error: (err) => console.log(err) }));
  }
}
