import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ResponseModel } from '@models';
@Injectable({
  providedIn: 'root',
})
export class PermisoForestalService {
  base = environment.urlProcesos;
  baseSolicitudPlantacionForestal = `${this.base}api/solPlantacionForestal`;
  baseSolicitudPlantacionForestalEvaluacion = `${this.base}api/solPlantacionForestalEvaluacion`;
  constructor(private http: HttpClient) {}

  obtenerSolicitudAccesoPersonaPorUsuario(usuario: any) {
    return this.http.get(
      this.base + `api/permisoForestal/obtenerPersonaPorUsuario/${usuario}`
    );
  }

  ListaComboSolicitudEstado(usuario: any) {
    return this.http.get(
      this.base + `api/solicitudestado/ListaComboSolicitudEstado/`
    );
  }

  registrarPermisoForestal(body: any): Observable<any> {
    return this.http.post(
      this.base + `api/permisoForestal/registrarPermisoForestal`,
      body
    );
  }

  actualizarPermisoForestal(body: any): Observable<any> {
    return this.http.post(this.base + `api/permisoForestal/actualizarPermisoForestal`, body);
  }

  actualizarEstadoPermisoForestal(params: any) {
    return this.http.post(this.base + `api/permisoForestal/actualizarEstadoPermisoForestal`, params);
  }

  listarInformacionGeneralPF(params: any) {
    return this.http.post(
      this.base + 'api/permisoForestal/listarInformacionGeneral',
      params
    );
  }
  
  actualizarResultadoDeEvaluacionForestal(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestalEvaluacion}/actualizarEvaluacionSolicitudPlantacionForestal`, params);
  }

  listarPorFiltroPlanManejo(param: any) {
    const params = new HttpParams()
    .set("nroDocumento",String(param.nroDocumento))
    .set("codTipoPlan", param.codTipoPlan)
    return this.http.post(
      `${this.base}api/permisoForestal/listarPorFiltroPlanManejo?` + params,
      null
    );
  }

  registrarPermisoForestalPlanManejo(params: any) {
    return this.http.post(
      this.base + 'api/permisoForestal/registrarPermisoForestalPlanManejo',
      params
    );
  }

  listarPermisoForestalPlanManejo(params: any) {
    return this.http.post(
      this.base + 'api/permisoForestal/listarPermisoForestalPlanManejo',
      params
    );
  }

  eliminarPermisoForestalPlanManejo(params: any) {
    return this.http.post(
      this.base + 'api/permisoForestal/eliminarPermisoForestalPlanManejo',
      params
    );
  }

  registrarInformacionGeneralPF(params: any) {
    return this.http.post(
      this.base + 'api/permisoForestal/registrarInformacionGeneral',
      params
    );
  }

  ListarPermisosForestales(params: any) {
    return this.http.get(
      this.base + `api/permisoForestal/listarPermisoForestal`,
      { params }
    );
  }

  listarPorFiltroPersona(dato: any) {
    return this.http.post(
      this.base + `api/coreCentral/listarPorFiltroPersona`,
      dato
    );
  }

  permisoForestalActualizarRemitido(params: any) {
    return this.http.post(`${this.base}api/permisoForestal/permisoForestalActualizarRemitido`, params);
  }

  /********Descargar plantillas********/
  descargarPlantillaResolAprobacion = () => {
    return this.http.get(this.base + `api/permisoForestal/descargarPlantillaResolAprobacion`);
  };
  descargarPlantillaResolDesAprobacion = () => {
    return this.http.get(this.base + `api/permisoForestal/descargarPlantillaResolDesAprobacion`);
  };
  descargarPlantillaFormatoPermiso = () => {
    return this.http.get(this.base + `api/permisoForestal/descargarPlantillaFormatoPermiso`);
  };

  /**********SolicitudPermisosForestales***** */

  /******************registrar********************* */
  RegistroSolicitudPermisosForestales(dato: any): Observable<any> {
    return this.http.post(
      this.base + `api/solPlantacionForestal/registrarSolicitudPlantacacion`,
      dato
    );
  }

  registrarSolicitudPlantacacionArea(dato: any): Observable<any> {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/registrarSolicitudPlantacacionArea`,
      dato
    );
  }

  registrarSolicitudPlantacacionAreaPlant(dato: any): Observable<any> {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/registrarSolicitudPlantacacionAreaPlant`,
      dato
    );
  }
  registrarSolicitudPlantacacionDet(dato: any) {
    return this.http.post(
      this.base + `api/solPlantacionForestal/registrarSolicitudPlantacacionDet`,
      dato
    );
  }
  registrarSolicitudPlantacacionDetCoord(dato: any): Observable<any> {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/registrarSolicitudPlantacacionDetCoord`,
      dato
    );
  }

  registrarSolicitudPlantacacionAnexo(
    dato: any,
    fromdata: FormData
  ): Observable<any> {
    return this.http.post<any>(
      this.base +
        `api/solPlantacionForestal/registrarSolicitudPlantacacionAnexo?` +
        dato,
      fromdata
    );
  }

  registrarInfoEvaluacionSolicitudPlantacionForestal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestalEvaluacion}/registrarEvaluacionSolicitudPlantacionForestal`, params);
  };

  registrarInfoDetalleEvaluacionSolicitudPlantacionForestal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestalEvaluacion}/registrarInfoDetalleEvaluacionSolicitudPlantacionForestal`, params);
  };

  /******************obtener********************* */

  obtenerInformacionTitular = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestal}/obtenerInformacionTitular`, params);
  };

  ObtenerPermisosForestales(params: any) {

    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/obtenerSolicitudPlantacacion`, params);
  }

  ObtenerPermisosForestalesArea(dato: any) {
    return this.http.post(
      this.base + `api/solPlantacionForestal/obtenerSolicitudPlantacacionArea`,
      dato
    );
  }
  obtenerSolicitudPlantacacionAreaPant(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/obtenerSolicitudPlantacacionAreaPant`,
      dato
    );
  }

  obtenerSolicitudPlantacacionDet(dato: any) {
    return this.http.post(
      this.base + `api/solPlantacionForestal/obtenerSolicitudPlantacacionDet`,
      dato
    );
  }
  obtenerSolicitudPlantacacionDetCoord(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/obtenerSolicitudPlantacacionDetCoord`,
      dato
    );
  }
  obtenerSolicitudPlantacacionAnexo(dato: any) {
    return this.http.post(
      this.base + `api/solPlantacionForestal/obtenerSolicitudPlantacacionAnexo`,
      dato
    );
  }

  obtenerInfoEvaluacionSolicitudPlantacionForestal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestalEvaluacion}/obtenerInfoEvaluacionSolicitudPlantacionForestal`, params);
  };

  obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestalEvaluacion}/obtenerInfoEvaluacionDetalleSolicitudPlantacionForestal`, params);
  };

  /*************************************** */
  TipoSistemaForestales(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/listarTipoComboSistemaPlantacionForestal`,
      dato
    );
  }

  BandejaSolicitudPermisosForestales(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/listarBandejaSolicitudPlantacionForestal`,
      dato
    );
  }

  EliminarSistemaPlantacionForestal(dato: any) {
    return this.http.post(
      this.base + `api/solPlantacionForestal/EliminarSistemaPlantacionForestal`,
      dato
    );
  }

  EliminarSistemaPlantacionForestalAreaPlant(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/EliminarSistemaPlantacionForestalAreaPlant`,
      dato
    );
  }

  EliminarSistemaPlantacionForestalDet(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/EliminarSistemaPlantacionForestalDet`,
      dato
    );
  }

  EliminarSistemaPlantacionForestalDetCoord(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/EliminarSistemaPlantacionForestalDetCoord`,
      dato
    );
  }

  enviarCorreoValidacionPermisoForestal(dato: any) {
    return this.http.post(
      this.base +
      `api/email/enviarValidacionPermisoForestal`,
      dato
    );
  }



  EliminarSistemaPlantacionForestalAnexo(dato: any) {
    return this.http.post(
      this.base +
        `api/solPlantacionForestal/EliminarSistemaPlantacionForestalAnexo`,
      dato
    );
  }

  actualizarInformacionTitular = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestal}/actualizarInformacionTitular`, params);
  };

  listarSolicitudPlantacionForestal = (params: any) => {
    return this.http.post<ResponseModel<any>>(`${this.baseSolicitudPlantacionForestal}/listarSolicitudPlantacionForestal`, params);
  };

  registrarInfoAdjuntosSolPlantacionForestal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/registrarInfoAdjuntosSolPlantacionForestal`, params);
  }

  obtenerInfoAdjuntosSolPlantacionForestal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/obtenerInfoAdjuntosSolPlantacionForestal`, params);
  }

  actualizarSolicitudPlantacionForestal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/actualizarSolicitudPlantacionForestal`, params);
  }
  actualizarInfoAdjuntosSolPlantacionForestal (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/actualizarInfoAdjuntosSolPlantacionForestal`, params);
  }
  enviarSolicitud (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/enviarSolicitud`, params);
  }

  notificarSolicitante(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/notificarSolicitante`, params);
  }
  generarNroRegistro (params: any) {
    return this.http.post<ResponseModel<any>>(`${this.base}api/solPlantacionForestal/generarNroRegistro`, params);
  }
}
