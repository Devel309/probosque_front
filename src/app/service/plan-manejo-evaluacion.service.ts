import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class PlanManejoEvaluacionService {
  private base: string;
  basePlanManejo = `${environment.urlProcesos}api/planManejo/`;

  constructor(private http: HttpClient) {
    this.base = `${environment.urlProcesos}api/planManejoEvaluacion`;
  }


  registrarPlanManejoEvaluacionDetalle(params: any) {
    return this.http.post(this.base + '/registrarPlanManejoEvaluacionDetalle', params);
  }
  actualizarPlanManejoEvaluacionDetalle(params: any) {
    return this.http.post(this.base + '/actualizarPlanManejoEvaluacionDetalle', params);
  }
  listarPlanManejoEvaluacionDetalle(params: any) {
    return this.http.get(this.base + '/listarPlanManejoEvaluacionDetalle?&idPlanManejoEval=' + params.idPlanManejoEval);
  }
  listarEspecieXCenso(params: any) {
    return this.http.post(environment.urlProcesos + 'api/censoForestal/listarEspecieXCenso', params);
  }

  listarPlanManejoEvaluacion(params: any) {
    return this.http.post(this.base + '/listarPlanManejoEvaluacion', params);
  }
  regitrarPlanManejoEvaluacionArchivo(dato: any, fromdata: FormData): Observable<any> {
    return this.http.post<any>(this.base + `/registrarPlanManejoEvaluacionArchivo?` + dato, fromdata);
  }

  listarPlanManejoEvaluacionLineamiento(params: any) {
    return this.http.post(this.base + '/listarPlanManejoEvaluacionLineamiento', params);
  }

  registrarPlanManejoEvaluacionLineamiento(params: any) {
    return this.http.post(this.base + '/registrarPlanManejoEvaluacionLineamiento', params);
  }

  obtenerUltimaEvaluacion(params: any) {
    return this.http.get(this.base + '/obtenerUltimaEvaluacion?&idPlanManejo=' + params.idPlanManejo);
  }

  actualizarEstadoProcesoEvaluacionPlanManejo(params: any) {
    return this.http.post(this.base + '/actualizarEstadoProcesoEvaluacionPlanManejo', params);
  }
  enviarEvaluacion(params: any) {
    return this.http.post(this.base + '/enviarEvaluacion', params);
  }
  obtenerEvaluacionPlanManejo(params: any) {
    return this.http.post(this.base + '/obtenerEvaluacionPlanManejo', params);
  }
  notificarObservacionesAlTitular(params: any) {
    return this.http.post(this.base + '/notificarObservacionesAlTitular', params);
  }

  actualizarIdSolicitudOpinionPlanManejoEvaluacion(params: any) {
    return this.http.post(this.base + '/actualizarIdSolicitudOpinionPlanManejoEvaluacion', params);
  }

  listarPlanManejoEvaluacionEspecie(params: any) {
    return this.http.post(this.base + '/listarPlanManejoEvaluacionEspecie', params);
  }

  remitirNotificacion(params: any) {
    return this.http.post(this.base + '/remitirNotificacion', params);
  }

  actualizarEvaluacionCampoPlanManejoEvaluacion(params: any) {
    return this.http.post(this.base + '/actualizarEvaluacionCampoPlanManejoEvaluacion', params);
  }

  notificarTitular(params: any) {
    return this.http.post(this.base + '/notificarTitular', params);
  }
  validarLineamiento(params: any) {
    return this.http.post(this.base + '/validarLineamiento', params);
  }

  actualizarPlanManejoArchivo(params: any) {
    return this.http.post<ResponseModel<any>>(`${this.basePlanManejo}actualizarPlanManejoArchivo`, params);
  }
}
