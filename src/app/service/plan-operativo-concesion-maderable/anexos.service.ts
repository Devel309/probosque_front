import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "@env/environment";

@Injectable({ providedIn: "root" })
export class AnexosService {
  private base: string = "";

  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  Anexo2(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/POConcesiones/Anexos/Anexo2?" + params,
      null
    );
  }

  Anexo6(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "api/censoForestal/Anexo6", null, {
      params,
    });
  }

  registrarCensoForestal(param: any, body: any) {
    const params = new HttpParams()
      .set("idPlanManejo", param.idPlanManejo.toString())
      .set("idUsuarioRegistro", param.idUsuarioRegistro);

    return this.http.post(
      this.base + "api/censoForestal/registrarCensoForestalDetalle",
      body,
      { params }
    );
  }

  actualizarCensoForestal(param: any, body: any) {
    const params = new HttpParams()
      .set("idCensoForestalDetalle", param.idCensoForestalDetalle.toString())
      .set("idUsuarioRegistro", param.idUsuarioRegistro);

    return this.http.post(
      this.base + "api/censoForestal/actualizarCensoForestalDetalle",
      body,
      { params }
    );
  }

  eliminarCensoForestal(param: any) {
    const params = new HttpParams()
      .set("idCensoForestalDetalle", param.idCensoForestalDetalle.toString())
      .set("idUsuarioElimina", param.idUsuarioElimina);

    return this.http.post(
      this.base + "api/censoForestal/eliminarCensoForestalDetalle",
      null,
      { params }
    );
  }

  Anexo2PGMFA(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("idTipoBosque", body.idTipoBosque)
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base +
        "api/censoForestal/FormatoPGMFformulado/Anexos/Anexo2?" +
        params,
      null
    );
  }

  Anexo7NoMaderable(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/ListaAnexo7NoMaderable",
      null,
      { params }
    );
  }

  AnexoListaEspeciesInventariadasMaderablesObtener(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base +
        "api/censoForestal/ListaEspeciesInventariadasMaderablesObtener",
      null,
      { params }
    );
  }

  Anexo3PGMFA(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("idTipoBosque", body.idTipoBosque)
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/Anexo3?" + params,
      null
    );
  }

  Anexo3(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/PMFIC/Anexos/Anexo3?" + params,
      null
    );
  }

  Anexo4(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/PMFIC/Anexos/Anexo4?" + params,
      null
    );
  }

  PGMFInventarioExploracionFustales(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("idTipoBosque", body.idTipoBosque)
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base +
        "api/censoForestal/PGMFInventarioExploracionFustales?" +
        params,
      null
    );
  }

  pgmfInventarioExploracionVolumenPotencial(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("idTipoBosque", body.idTipoBosque)
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base +
        "api/censoForestal/pgmfInventarioExploracionVolumenPotencial?" +
        params,
      null
    );
  }

  listarTipoBosque() {
    return this.http.post(this.base + "api/censoForestal/listarTipoBosque", {});
  }

  anexo2POPAC(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/POConcesiones/Anexos/Anexo2?" + params,
      null
    );
  }

  listaAnexo7NoMaderable(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/ListaAnexo7NoMaderable?"+ params, null
    );
  }


  buscarAnexo7NoMaderable(param: any, body:any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(param.idPlanManejo))
      .set("tipoPlan", param.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/BuscarAnexo7NoMaderable?"+ params, body
    );
  }

  listaEspeciesInventariadasMaderablesObtener(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", String(body.idPlanManejo))
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/ListaEspeciesInventariadasMaderablesObtener?"+ params, null
    );
  }

}
