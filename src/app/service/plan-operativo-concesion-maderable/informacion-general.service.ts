import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class InformacionGeneralPOCMService {
  private base: string = '';

  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  registarInformacionGeneralDema = (body: any) => {
    return this.http.post(
      `${this.base}api/informacionGeneralDema/registrarInformacionGeneralDema`,
      body
    );
  };

  actualizarInformacionGeneral = (body: any) => {
    return this.http.post(
      `${this.base}api/informacionGeneralDema/actualizarInformacionGeneralDema`,
      body
    );
  };

  listarInformacionGeneral = (body: any) => {
    return this.http.post(
      `${this.base}api/informacionGeneralDema/listarInformacionGeneralDema`,
      body
    );
  };
  obtenerInformacionProcesoPostulacion = (body: any) => {
    return this.http.post(
      `${this.base}api/procesoPostulacion/obtenerInformacionProcesoPostulacion`,
      body
    );
  };

  obtenerInformacionPlanOperativo = (body: any) => {
    return this.http.post(
      `${this.base}api/informacionGeneral/obtenerInformacionPlanOperativo`,
      body
    );
  };
}
