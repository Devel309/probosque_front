import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class InformacionBasicaSOPCMervice {
  private base: string = '';

  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarInformacionBasica(param: any) {
    const params = new HttpParams()
      .set('idInfBasica', param.idInfBasica)
      .set('idPlanManejo', param.idPlanManejo.toString())
      .set('codCabecera', param.codCabecera);
    return this.http.post(
      this.base + 'api/informacionBasica/listarInfBasicaAerea?' + params,
      null
    );
  }

  registrarInformacionBasica(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/registrarInformacionBasicaDetalle',
      params
    );
  }

  actualizarInformacionBasica(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/actualizarInformacionBasicaDetalle',
      params
    );
  }

  eliminarInformacionBasica(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/eliminarInfBasicaAerea',
      params
    );
  }

  obtenerHidrografia(idHidrografia: any = 0, tipoHidrografia: string = '') {
    const params = new HttpParams()
    .set('idHidrografia', idHidrografia)
    .set('tipoHidrografia', tipoHidrografia)

      const uri = `${this.base}obtenerHidrografia?`;
    return this.http.post(
      this.base + 'api/informacionBasica/obtenerHidrografia?' + params,
      null
    );
  }


  listarInformacionBasicaDetalle(param: any) {
    const params = new HttpParams()
      .set('idInfBasica', param.idInfBasica)
      .set('codTipo', param.codTipo)
      .set('codSubTipo', param.codSubTipo);
    return this.http.post(
      this.base + 'api/informacionBasica/listarInfBasicaAreaDetalle',
      null,
      {params}
    );
  }


  listarInformacionSocioeconomica(param: any) {
    const params = new HttpParams()
      .set('codigoTipoInfBasica', param.codigoTipoInfBasica)
      .set('idPlanManejo', param.idPlanManejo)
      .set('subCodigoTipoInfBasica', param.subCodigoTipoInfBasica)
      
    return this.http.get(
      this.base + 'api/informacionBasica/listarInformacionSocioeconomica',
      {params}
    );
  }

  registrarInformacionBasicaDetalle(body: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/registrarInformacionBasicaDetalle',
      body
    );
  }
}
