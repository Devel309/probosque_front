import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from './config.service';

@Injectable({ providedIn: 'root' })
export class EvaluacionCampoService {

    base = environment.urlProcesos;
    constructor(private http: HttpClient, private config: ConfigService) { }


    listarEvaluacionCampo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/listarEvaluacionCampo', params);
    }

    obtenerEvaluacionCampoArchivo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/obtenerEvaluacionCampoArchivo', params);
    }

    registroEvaluacionCampo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/registroEvaluacionCampo', params);
    }

    obtenerEvaluacionOcular(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/obtenerEvaluacionOcular', params);
    }

    registroEvaluacionCampoInfraccion(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampoInfraccion/registrarEvaluacionCampoInfraccion', params);
    }

    registroEvaluacionCampoArchivo(params: any) {

        let formData = new FormData();
        formData.append("idUsuarioRegistro", params.idUsuarioRegistro);
        formData.append("idEvaluacionCampo", params.idEvaluacionCampo);
        formData.append("idEvaluacionCampoArchivo", params.idEvaluacionCampoArchivo);
        formData.append("idTipoDocumento", params.idTipoDocumento);
        formData.append("idTipoSeccionArchivo", params.idTipoSeccionArchivo);
        formData.append("file", params.file);

        const req = new HttpRequest('POST', `${this.base}api/evaluacionCampo/registroEvaluacionCampoArchivo`, formData);
        return this.http.request(req);
    }

    obtenerEvaluacionCampo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/obtenerEvaluacionCampo', params);
    }

    obtenerEvaluacionCampoInfraccion(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampoInfraccion/obtenerEvaluacionCampoInfraccion', params);
    }

    notificacionEvaluacionCampo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/notificacionEvaluacionCampo', params);
    }


    eliminarEvluacionCampoArchivo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/eliminarEvluacionCampoArchivo', params);
    }

    listarPorSeccionEvaluacionCampoArchivo(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/listarPorSeccionEvaluacionCampoArchivo', params);
    }


    listarEvaluacionCampoArchivoInspeccion(params: any) {
        return this.http.post(this.base + 'api/evaluacionCampo/listarEvaluacionCampoArchivoInspeccion', params);
    }

}
