
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
import { RegenteForestalModel, ResponseModel } from '@models';
@Injectable({
  providedIn: 'root'
})
export class RegenteService {

  base = environment.urlProcesos;// 'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http: HttpClient, private config: ConfigService) { }

  //Información General
  guardarRegente(body: RegenteForestalModel): Observable<ResponseModel<RegenteForestalModel>> {
    return this.http.post<ResponseModel<RegenteForestalModel>>(this.base + 'api/regente/registrarRegente', body);
  }

  obtenerRegente(idPlanManejo: number): Observable<ResponseModel<RegenteForestalModel>> {
    return this.http.post<ResponseModel<RegenteForestalModel>>(this.base + 'api/regente/obtenerRegente', { idPlanManejo });
  }

  obtenerRegentes(): Observable<ResponseModel<RegenteForestalModel>> {
    return this.http.get<ResponseModel<RegenteForestalModel>>(this.base + 'api/regente/obtenerRegentes');
  }

  listarRegenteArchivos(params: any) {
    return this.http.post(`${this.base}api/regente/ListarRegenteArchivos`, params);
  }

}

