import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class OrdenamientoAreaMAnejoService   {

  base = environment.urlProcesos;
  constructor(private http : HttpClient, private config : ConfigService) { }


  listarOrdenamientoAreaManejo(params : any){
    return this.http.post(this.base +'api/ordenamientoAreaManejo/listarOrdenamientoAreaManejo', params);
  }
  eliminarOrdenamientoAreaManejo(params : any){
    return this.http.post(this.base +'api/ordenamientoAreaManejo/eliminarOrdenamientoAreaManejo', params);
  }
  comboCategoriaOrdenamiento(params : any){
    return this.http.post(this.base +'api/ordenamientoAreaManejo/comboCategoriaOrdenamiento', params);
  }
  registrarOrdenamientoAreaManejo(params : any,formd: FormData){
    return this.http.post(this.base +'api/ordenamientoAreaManejo/registrarOrdenamientoAreaManejo?'+params  , formd);
  }
  listarOrdenamientoAreaManejoArchivo(params : any){
    return this.http.post(this.base +'api/ordenamientoAreaManejo/listarOrdenamientoAreaManejoArchivo?',params);
  }
  obtenerOrdenamientoAreaManejoArchivo(params : any){
    return this.http.post(this.base +'api/ordenamientoAreaManejo/obtenerOrdenamientoAreaManejoArchivo?',params);
  }

}

