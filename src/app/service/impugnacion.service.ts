import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from './config.service';

@Injectable({ providedIn: 'root' })

export class ImpugnacionService {

    base = environment.urlProcesos;
    baseImpugnacion = `${this.base}api/impugnacion`;
    constructor(private http: HttpClient, private config: ConfigService) { }


    listarImpugnacion(params: any) {
        return this.http.post(this.base + 'api/impugnacion/listarImpugnacion', params);
    }

    listarResolucionImpugnada(params: any) {
      return this.http.post(this.base + 'api/impugnacion/listarResolucionImpugnada', params);
    }

    notificarSolicitudImpugnacion(params: any) {
        return this.http.post(this.base + 'api/impugnacion/notificarSolicitudImpugnacion', params);
    }

    obtenerImpugnacion(params: any) {
        return this.http.post(this.base + 'api/impugnacion/obtenerImpugnacion', params);
    }

    registrarImpugnacion(params: any) {
        let formData = new FormData();

        formData.append("idResolucion", params.idResolucion);
        formData.append("asunto", params.asunto);
        formData.append("descripcion", params.descripcion);
        formData.append("fechaImpugnacion", params.fechaImpugnacion);
        formData.append("idUsuarioRegistro", params.idUsuarioRegistro);
        formData.append("region", params.region);
        formData.append("fileRecurso", params.fileRecurso);
        formData.append("fileSustento", params.fileSustento==undefined?null:params.fileSustento);


        const req = new HttpRequest('POST', `${this.base}api/impugnacion/registrarImpugnacion`, formData);
        return this.http.request(req);
    }

    actualizarImpugnacion(params: any) {
        let formData = new FormData();
         if( params.retrotraer==null )
         params.retrotraer=false;


         if(params.fileFirme==null){
          formData.append("idImpugnacion", params.idImpugnacion);
          formData.append("tipoImpugnacion", params.tipoImpugnacion);
          formData.append("esfundado", params.esfundado);
          formData.append("idUsuarioModificacion", params.idUsuarioModificacion);
          formData.append("fileEvaluacion", params.fileEvaluacion);
          formData.append("retrotraer", params.retrotraer);
          formData.append("fileFirme", params.fileFirme);
         }
         else{
          formData.append("idImpugnacion", params.idImpugnacion);
          formData.append("esfundado", params.esfundado);
          formData.append("idUsuarioModificacion", params.idUsuarioModificacion);
          formData.append("retrotraer", params.retrotraer);
          formData.append("fileFirme", params.fileFirme);
         }


        const req = new HttpRequest('POST', `${this.base}api/impugnacion/actualizarImpugnacion`, formData);
        return this.http.request(req);
    }

    validarPlazoImpugnacionResolucion = (params: any) => {
        return this.http.post(`${this.baseImpugnacion}/validarPlazoImpugnacionResolucion`, params);
      };
    

}
