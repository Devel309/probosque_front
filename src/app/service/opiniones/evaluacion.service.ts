import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import {Pageable} from '@models';

@Injectable({ providedIn: 'root' })
export class EvaluacionService {

  base = `${environment.urlProcesos}api/evaluacion/`;

  constructor(private http: HttpClient,) { }

  listarInspeccionTipoDocumento(params: any) {
    return this.http.post(`${this.base}listarInspeccionTipoDocumento`, params );
  }

  obtenerRegente(param: any) {
    const params = new HttpParams()
    .set("numeroDocumento", param.numeroDocumento)
    return this.http.get(`${this.base}obtenerRegente?` + params );
  }


  obtenerPlanEvaluacion(idPlanManejo:any){
    const uri = `${this.base}filtrarEval`;
    let p = new HttpParams();
    p = p.set('idPlanManejo', String(idPlanManejo));
    return this.http.get<Pageable<any[]>>(uri, { params: p });
  }

}
