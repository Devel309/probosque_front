import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class OpinionesService {
  base = `${environment.urlProcesos}api/solicitudOpinion`;

  constructor(private http: HttpClient) {}

  listarSolicitudOpinionEvaluacion(param: any) {
    let params = new HttpParams()
      .set('idPlanManejo', String(param.idPlanManejo))
      .set('codSolicitud', param.codSolicitud);
    if (param.subCodSolicitud) {
      params = params.set('subCodSolicitud', param.subCodSolicitud);
    }
    return this.http.post(
      `${this.base}/listarSolicitudOpinionEvaluacion`,
      params
    );
  }

  registrarSolicitudOpinionEvaluacion(params: any) {
    return this.http.post(
      `${this.base}/registrarSolicitudOpinionEvaluacion`,
      params
    );
  }
}
