import {
  HttpClient,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { SolicitudArchivoAdjuntoModel } from "../model/SolicitudArchivo";

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {


  base = environment.urlProcesos;// 'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http: HttpClient) {
  }


  registrarSolicitud(params: any) {
    return this.http.post(this.base + 'api/permisoForestal/registrarSolicitudPorAccesoSolicitud', params);
  }

  registrarArchivoSolicitud(idSolicitud: any, archivo: File, tipoArchivo: any) {
    let formData = new FormData();
    formData.append("idSolicitud", idSolicitud);
    formData.append("file", archivo);
    formData.append("tipoArchivo", tipoArchivo);
    const req = new HttpRequest('POST', this.base + 'api/permisoForestal/registrarArchivoSolicitud', formData, {
      reportProgress: true
    });
    return this.http.request(req);
  }

  registrarPermisoArchivoSolicitud(params: any) {

    let formData = new FormData();
    formData.append("idSolicitud", params.idSolicitud);
    formData.append("file", params.file);
    formData.append("tipoArchivo", params.tipoDocumento);
    formData.append("flagActualiza", params.flagActualiza);

    const req = new HttpRequest('POST', this.base + 'api/permisoForestal/registrarArchivoSolicitud', formData, {
      reportProgress: true
    });
    return this.http.request(req)

  }

  eliminarArchivoPermisoSolicitud(params: any) {
    return this.http.post(this.base + `api/permisoForestal/eliminarArchivo`, params);
  }

  eliminarArchivoAdjuntoSolicitud(params: any) {
    return this.http.post(this.base + `api/permisoForestal/archivoEliminar`, params);
  }

  obtenerArchivoSolicitud(idSolicitud: any) {
    return this.http.get(this.base + `api/permisoForestal/obtenerArchivoSolicitud/${idSolicitud}`);
  }

  obtenerArchivoSolicitudAdjunto(idSolicitud: any) {
    return this.http.get(this.base + `api/permisoForestal/obtenerArchivoSolicitudAdjunto/${idSolicitud}`);
  }

  obtenerSolicitudPorAccesoSolicitud(idSolicitud: number, idSolicitudAcceso: number) {
    return this.http.get(this.base + `api/permisoForestal/obtenerSolicitudPorAccesoSolicitud/${idSolicitud}/${idSolicitudAcceso}`);
  }

  descargarArchivoSolicitud(idArchivoSolicitud: any, idSolicitud: any, tipoDocumento: string) {
    return this.http.get(this.base + `api/permisoForestal/descargarArchivo/${idArchivoSolicitud}/${idSolicitud}/${tipoDocumento}`);

  }

  descargarArchivoSolicitudAdjunto(idArchivoSolicitud: any, idSolicitud: any, tipoDocumento: string) {
    return this.http.get(this.base + `api/permisoForestal/descargarArchivoAdjunto/${idArchivoSolicitud}/${idSolicitud}/${tipoDocumento}`);

  }

  registrarSolicitudMovimientoFlujo(idSolicitud: any, idEstadoSolicitud: any) {
    var data = { idSolicitud: idSolicitud, idEstadoSolicitud: idEstadoSolicitud }
    return this.http.post(this.base + 'api/permisoForestal/registrarSolicitudMovimientoFlujo', data);
  }

  editarSolicitudPorAccesoSolicitud(data: any) {
    return this.http.post(this.base + 'api/permisoForestal/editarSolicitudPorAccesoSolicitud', data);
  }


  registrarArchivoSolicitudAdjunto(params: any) {
    let formData = new FormData();
    if (params.file)
      formData.append("file", params.file);

    if (params.descripcionArchivo)
      formData.append("descripcionArchivo", params.descripcionArchivo);

    formData.append("idSolicitud", params.idSolicitud);
    formData.append("tipoArchivo", params.tipoDocumento);
    formData.append("flagActualiza", params.flagActualiza);

    const req = new HttpRequest('POST', this.base + 'api/permisoForestal/registrarArchivoSolicitudAdjunto', formData, {
      reportProgress: true
    });
    return this.http.request(req);

  }
}

