import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ConfigService } from './config.service';


@Injectable({ providedIn: 'root' })
export class EntidadService {

    base = environment.urlSeguridad;

    constructor(private http: HttpClient, private config: ConfigService) { }


    listaComboEntidad(params: any) {
        return this.http.post(this.base + 'api/entidad/listaComboEntidad', params);
    }
}