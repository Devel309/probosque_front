import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { environment as env } from '@env/environment';
import { InformacionGeneralDto, ResponseModel } from '@models';

@Injectable({ providedIn: 'root' })
export class InformacionGeneralService {

  base = `${env.urlProcesos}api/`;
  infoGeneral = `${this.base}informacionGeneral`;

  constructor(private http: HttpClient) { }


  obtener(idPlanManejo: number): Observable<ResponseModel<InformacionGeneralDto>> {
    const uri = `${this.infoGeneral}`;
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get<ResponseModel<InformacionGeneralDto>>(uri, { params });
  }

  guardar(body: InformacionGeneralDto): Observable<ResponseModel<InformacionGeneralDto>> {
    const uri = `${this.infoGeneral}`;
    return this.http.post<ResponseModel<InformacionGeneralDto>>(uri, body);
  }

}