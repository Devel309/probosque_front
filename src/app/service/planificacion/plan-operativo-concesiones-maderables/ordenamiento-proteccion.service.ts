import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { environment } from '../../../../environments/environment';

import { CapacitacionDetalleMaderableModel, CapacitacionMaderableModel } from 'src/app/model/CapacitacionMaderableModel';
import { ResponseModel } from 'src/app/model/ResponseModel';
import { ListarCapacitacionMaderableRequest } from '@models';

@Injectable({ providedIn: 'root' })
export class OrdenamientoProteccionService {

  base = `${environment.urlProcesos}api/ordenamientoProteccion/`;

  constructor(private http: HttpClient,) { }

  listarOrdenamientoInternoDetalle(params: any) {
    return this.http.post(
      this.base + 'ListarOrdenamientoInternoDetalle',
      params
    );
  }

  registrarOrdenamientoInterno(params: any) {
    return this.http.post(
      this.base + 'registrarOrdenamientoInterno',
      params
    );
  }

  eliminarOrdenamientoInternoDetalle(params: any) {
    return this.http.post(
      this.base + 'eliminarOrdenamientoInterno',
      params
    );
  }



}
