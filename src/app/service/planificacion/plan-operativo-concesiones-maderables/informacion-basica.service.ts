import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ResponseModel } from 'src/app/model/ResponseModel';
import { HidrografiaModel } from '@models';
import { environment as env } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class InformacionBasicaService {

  base = `${env.urlProcesos}api/informacionBasica/`;

  constructor(private http: HttpClient) { }


  obtenerHidrografia(idHidrografia: any = 0, tipoHidrografia: string = '', nombre: string = '', descripcion: string = '')
    : Observable<ResponseModel<HidrografiaModel[]>> {
    const params = new HttpParams()
      .set('idHidrografia', idHidrografia)
      .set('tipoHidrografia', tipoHidrografia)
      .set('nombre', nombre)
      .set('descripcion', descripcion)

    const uri = `${this.base}obtenerHidrografia`;
    return this.http.get<ResponseModel<HidrografiaModel[]>>(uri, { params });

  }
  registrarInfBasicaArchivo(item:any,file:any){
    let  param = new HttpParams()
      .set('idInfBasica',item.informacionBasica.id.toString())
      .set('idUsuarioRegistro',item.idUsuarioRegistro.toString());

    return this.http.post(this.base +'registrarArchivoInfBasica?'+param  , file);
  }
  listarInfBasicaArchivo(id:any){
    return this.http.get(`${this.base}listarInfBasicaArchivo/${id}`);
  }
  eliminarInfBasicaArchivo(item:any){
    return this.http.get(`${this.base}eliminarInfBasicaArchivo/${item.id}/${item.idArchivo}/${item.idUsuario}`);
  }
  listarUnidadFisiografica(id:Number){
    return this.http.get(`${this.base}listarUnidadFisiografica/${id}`);
  }
}