import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ResponseModel } from 'src/app/model/ResponseModel';
import { environment as env } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class UnidadFisiograficaService {

  base = `${env.urlProcesos}api/unidadFisiografica/`;

  constructor(private http: HttpClient) { }
  listarUnidadFisiografica(id:Number){
    return this.http.get(`${this.base}listarUnidadFisiografica/${id}`);
  }
  registrarUnidadFisiograficaArchivo(params:any,file?:any){
    return this.http.post(`${this.base}registrarUnidadFisiograficaArchivo?`+params,file);
  }
  eliminarUnidadFisiograficaArchivo(item:any){
    return this.http.get(`${this.base}eliminarUnidadFisiograficaArchivo/${item.idInfBasica}/${item.idUnidadFisiografica}/${item.idUsuario}`);
  }
}