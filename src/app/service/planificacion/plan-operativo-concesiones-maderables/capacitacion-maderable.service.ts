import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { environment } from '../../../../environments/environment';

import { CapacitacionDetalleMaderableModel, CapacitacionMaderableModel } from 'src/app/model/CapacitacionMaderableModel';
import { ResponseModel } from 'src/app/model/ResponseModel';
import { ListarCapacitacionMaderableRequest } from '@models';

@Injectable({ providedIn: 'root' })
export class CapacitacionMaderableService {

  base = `${environment.urlProcesos}api/capacitacion/`;

  constructor(private http: HttpClient,) { }

  listar(body: ListarCapacitacionMaderableRequest): Observable<ResponseModel<CapacitacionMaderableModel>> {
    const uri = `${this.base}listarCapacitacionDetalle`;
    return this.http.post<ResponseModel<CapacitacionMaderableModel>>(uri, body);
  }

  registrar(body: CapacitacionMaderableModel): Observable<ResponseModel<CapacitacionMaderableModel>> {
    const uri = `${this.base}registrarCapacitacionDetalle`;
    return this.http.post<ResponseModel<CapacitacionMaderableModel>>(uri, [body]);
  }

  actualizar(body: CapacitacionMaderableModel): Observable<ResponseModel<CapacitacionMaderableModel>> {
    const uri = `${this.base}actualizarCapacitacionDetalle`;
    return this.http.post<ResponseModel<CapacitacionMaderableModel>>(uri, [body]);
  }

  eliminar(idCapacitacionDet: number, idUsuarioElimina: number): Observable<ResponseModel<CapacitacionDetalleMaderableModel>> {
    const uri = `${this.base}eliminarCapacitacionDetalle`;
    return this.http.post<ResponseModel<CapacitacionDetalleMaderableModel>>(uri, { idCapacitacionDet, idUsuarioElimina });
  }

}
