import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ResponseModel } from 'src/app/model/ResponseModel';
import { ParticipacionComunalDto } from '@models';
import { environment as env } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class ParticipacionCiudadanaService {

  base = `${env.urlProcesos}api/participacionComunal/`;

  constructor(private http: HttpClient,) { }

  listar(idPlanManejo: number | null): Observable<ResponseModel<ParticipacionComunalDto[]>> {
    const uri = `${this.base}listarParticipacionCiudadana`;

    const body = { planManejo: { idPlanManejo } };
    return this.http.post<ResponseModel<ParticipacionComunalDto[]>>(uri, body);
  }

  registrar(body: ParticipacionComunalDto): Observable<ResponseModel<ParticipacionComunalDto>> {
    const uri = `${this.base}registrarParticipacionCiudadanaList`;
    return this.http.post<ResponseModel<ParticipacionComunalDto>>(uri, [body]);
  }

  actualizar(body: ParticipacionComunalDto): Observable<ResponseModel<ParticipacionComunalDto>> {
    const uri = `${this.base}registrarParticipacionCiudadanaList`;
    return this.http.post<ResponseModel<ParticipacionComunalDto>>(uri, [body]);
  }

  eliminar(idPartComunal: number, idUsuarioElimina: number): Observable<ResponseModel<ParticipacionComunalDto>> {
    const uri = `${this.base}eliminarParticipacionCiudadana`;
    return this.http.post<ResponseModel<ParticipacionComunalDto>>(uri, { idPartComunal, idUsuarioElimina });
  }

}
