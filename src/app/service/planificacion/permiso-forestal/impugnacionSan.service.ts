import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ImpugnacionSanService {
  base = `${environment.urlProcesos}api/solicitudSAN`;

  constructor(private http: HttpClient) {}

  registrarSolicitudSAN(params: any) {
    return this.http.post(`${this.base}/registrarSolicitudSAN`, params);
  }

  listarSolicitudSAN(params: any) {
    return this.http.post(`${this.base}/listarSolicitudSAN`, params);
  }

}
