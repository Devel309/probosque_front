import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ResponseModel } from "@models";
import { environment } from "../../../../environments/environment";
import { map } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class PermisoForestalArchivoService {
  base = `${environment.urlProcesos}api/permisoForestal`;

  constructor(private http: HttpClient) {}

  registrarArchivo(params: any) {
    return this.http.post(`${this.base}/registrarArchivo`, params);
  }

  listarArchivo(params: any) {
    return this.http.post(`${this.base}/listarArchivo`, params);
  }

  obtenerArchivo(body: any) {
    const uri = `${this.base}/listarArchivo`;
    return this.http.post<ResponseModel<any>>(uri, body)
      .pipe(map(res => {
        if (res?.data?.length > 0) return res?.data[0];
        return null;
      }))
  }

  eliminarArchivoPermisoForestal(params: any) {
    return this.http.post(`${this.base}/eliminarArchivoPermisoForestal`, params);
  }

}
