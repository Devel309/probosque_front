import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ResponseModel } from '@models';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators/tap';

@Injectable({
    providedIn: 'root'
})
export class PermisoForestalGeometriaService {
    base = `${environment.urlProcesos}api/permisoForestalGeometria`;

    constructor(private http: HttpClient) { }

    registrarPermisoForestalGeometria(params: any): Observable<ResponseModel<any>> {
        const uri = `${this.base}/registrarPermisoForestalGeometria`;
        return this.http.post<ResponseModel<any>>(uri, params)
            .pipe(tap({ error: err => console.log(err) }));
    }

    listarPermisoForestalGeometria(params: any): Observable<ResponseModel<any[]>> {
        const uri = `${this.base}/listarPermisoForestalGeometria`;
        return this.http.post<ResponseModel<any[]>>(uri, params)
            .pipe(tap({ error: err => console.log(err) }));
    }

    eliminarPermisoForestalGeometriaArchivo(params: any): Observable<ResponseModel<any[]>> {
        const uri = `${this.base}/eliminarPermisoForestalGeometriaArchivo`;
        return this.http.post<ResponseModel<any[]>>(uri, params)
            .pipe(tap({ error: err => console.log(err) }));
    }
}
