import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CapacitacionService {

  base = `${environment.urlProcesos}api/capacitacion/`;

  constructor(private http: HttpClient,) { }

  registrarCapacitacionDetalle(params: any) {
    return this.http.post(`${this.base}registrarCapacitacionDetalle`, params );
  }

  listarCapacitacion(param: any) {
    return this.http.post(`${this.base}listarCapacitacion` , param );
  }

  eliminarCapacitacion(params: any) {
    return this.http.post(`${this.base}eliminarCapacitacion`, params );
  }

  eliminarCapacitacionDetalle(params: any) {
    return this.http.post(`${this.base}eliminarCapacitacionDetalle`, params );
  }




}
