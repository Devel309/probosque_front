import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseModel } from '@models';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })

export class RentabilidadManejoForestalService {

  base = `${environment.urlProcesos}api/rentabilidadManejoForestal/`;

  constructor(private http: HttpClient,) { }

  listarRentabilidadManejoForestal(params: any) {
    return this.http.post(`${this.base}listarRentabilidadManejoForestal`, params );
  }

  registrarRentabilidadManejoForestal(params: any):Observable<any> {
    return this.http.post(`${this.base}registrarRentabilidadManejoForestal`, params );
  }

  eliminarRentabilidadManejoForestal(params: any) {
    return this.http.post(`${this.base}eliminarRentabilidadManejoForestal`, params );
  }

  registrarRentabilidadManejoForestalExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codigoRentabilidad: string,
    idPlanManejo: number,
    idUsuarioRegistro: number,
    vigencia: number): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codigoRentabilidad', String(codigoRentabilidad))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idUsuarioRegistro', String(idUsuarioRegistro))
      .set('vigencia', String(vigencia));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(`${this.base}registrarRentabilidadManejoForestalExcel`, body, { params, reportProgress: true, });
  }
}
