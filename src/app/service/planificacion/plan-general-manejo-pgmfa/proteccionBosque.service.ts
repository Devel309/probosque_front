import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";
import {Observable} from 'rxjs';
import {ResponseModel} from '@models';

@Injectable({ providedIn: "root" })
export class ProteccionDelBosqueService {
  base = `${environment.urlProcesos}api/proteccionBosque/`;

  constructor(private http: HttpClient) {}

  listarProteccionBosque(param: any) {
    const params = new HttpParams()
    .set("idPlanManejo", String(param.idPlanManejo))
    .set("codigoProceso", param.codigoProceso)
    .set("subCodPlanGeneral", param.subCodPlanGeneral);
    return this.http.post(`${this.base}listarProteccionBosque`, params);
  }

  listarProteccionDelBosque(param: any) {
    const params = new HttpParams()
      .set("idPlanManejo", String(param.idPlanManejo))
      .set("codPlanGeneral", param.codPlanGeneral)
      .set("subCodPlanGeneral", param.subCodPlanGeneral);
    return this.http.post(`${this.base}listarProteccionBosque`, params);
  }


  listarProteccionBosqueCodigo(param: any) {
    const params = new HttpParams()
    .set("subCodPlanGeneral", param.subCodPlanGeneral);
    return this.http.post(`${this.base}listarProteccionBosque`, params);
  }

  registrarProteccionBosque(params: any) {
    return this.http.post(`${this.base}registrarProteccionBosque`, params);
  }

  eliminarProteccionBosque(params: any) {
    return this.http.post(`${this.base}eliminarProteccionBosque`, params);
  }

  registrarProteccionBosqueExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codPlanGeneral: string,
    subCodPlanGeneral:string,
    codPlanGeneralDet: string,
    subCodPlanGeneralDet: any,
    idPlanManejo: number,
    idUsuarioRegistro: number): Observable<ResponseModel<number>> {

    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codPlanGeneral', String(codPlanGeneral))
      .set('subCodPlanGeneral', String(subCodPlanGeneral))
      .set('codPlanGeneralDet', String(codPlanGeneralDet))
      .set('subCodPlanGeneralDet', String(subCodPlanGeneralDet))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));

    const body = new FormData();
    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(`${this.base}registrarProteccionBosqueExcel`, body, { params, reportProgress: true, });

  }


}
