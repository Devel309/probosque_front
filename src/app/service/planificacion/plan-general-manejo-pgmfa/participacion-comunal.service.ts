import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ParticipacionComunalService {
  base = `${environment.urlProcesos}api/participacionComunal/`;

  constructor(private http: HttpClient) {}

  listarPorFiltroParticipacionComunal(params: any) {
    return this.http.post(
      `${this.base}listarPorFiltroParticipacionComunal`,
      params
    );
  }

  registrarParticipacionComunal(params: any) {
    return this.http.post(`${this.base}registrarParticipacionComunal`, params);
  }

  eliminarParticipacionComunal(params: any) {
    return this.http.post(`${this.base}eliminarParticipacionComunal`, params);
  }

  registrarParticipacionComunalCaberaDetalle(params: any) {
    return this.http.post(
      `${this.base}registrarParticipacionComunalCaberaDetalle`,
      params
    );
  }

  listarParticipacionComunalCaberaDetalle(params: any) {
    return this.http.post(
      `${this.base}listarParticipacionComunalCaberaDetalle`,
      params
    );
  }
  eliminarParticipacionComunalCaberaDetalle(params: any) {
    return this.http.post(
      `${this.base}eliminarParticipacionComunalCaberaDetalle`,
      params
    );
  }
}
