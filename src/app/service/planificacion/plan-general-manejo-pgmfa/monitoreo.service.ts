import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ObjetivosService {

  base = `${environment.urlProcesos}api/monitoreo/`;

  constructor(private http: HttpClient,) { }

  listarMonitoreo(params: any) {
    return this.http.post(`${this.base}listarMonitoreo`, params );
  }

  registrarMonitoreo(params: any) {
    return this.http.post(`${this.base}registrarMonitoreo`, params );
  }

  ActualizarMonitoreo(params: any) {
    return this.http.post(`${this.base}ActualizarMonitoreo`, params );
  }

  EliminarDetalleMonitoreo(params: any) {
    return this.http.post(`${this.base}EliminarDetalleMonitoreo`, params );
  }

}
