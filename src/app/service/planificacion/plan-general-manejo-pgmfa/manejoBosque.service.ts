import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ManejoBosqueService {
  base = `${environment.urlProcesos}api/manejoBosque`;

  constructor(private http: HttpClient) {}

  listarManejoBosque(param: any) {
    const params = new HttpParams()
      .set('idPlanManejo', String(param.idPlanManejo))
      .set('codigoGeneral', String(param.codigoGeneral));
    return this.http.post(`${this.base}/ListarManejoBosque`, null, { params });
  }

  registrarManejoBosque(param: any) {
    return this.http.post(`${this.base}/registrarListaManejoBosque`, param);
  }

  eliminarManejoBosque(params: any) {
    return this.http.post(`${this.base}/eliminarManejoBosque`, params);
  }
}
