import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ResponseModel } from "@models";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class PotencialProduccionService {
  base = `${environment.urlProcesos}api/potencialProduccionForestal/`;

  constructor(private http: HttpClient) {}

  registrarPotencialProduccion(params: any) {
    return this.http.post(
      this.base + "registrarPotencialProducForestal",
      params
    );
  }

  listarPotencialProduccion(params: any) {
    const param = new HttpParams()
      .set("idPlanManejo", String(params.idPlanManejo))
      .set("codigo", params.codigo);
    return this.http.post(
      this.base + "listarPotencialProducForestal?" + param,
      null
    );
  }
  eliminarPotencialProducForestal(params: any) {
    return this.http.post(
      this.base + "eliminarPotencialProducForestal",
      params
    );
  }

  registrarPotencialProduccionExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codigoTipo: string,
    codigoSubTipo: string,
    codigoTipoDet: string,
    codigoSubTipoDet: string,
    idPlanManejo: number,
    idUsuarioRegistro: number): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codigoTipo', String(codigoTipo))
      .set('codigoSubTipo', String(codigoSubTipo))
      .set('codigoTipoDet', String(codigoTipoDet))
      .set('codigoSubTipoDet', String(codigoSubTipoDet))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(this.base + 'registrarPotencialProduccionExcel', body, { params, reportProgress: true, });
  }
}
