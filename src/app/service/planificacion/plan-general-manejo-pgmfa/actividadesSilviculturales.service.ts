import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ActividadesSilviculturalesService {
  base = `${environment.urlProcesos}api/actividadSilvicultural/`;

  constructor(private http: HttpClient) {}

  guardarSistemaManejoPFDM(params: any): Observable<any> {
    return this.http.post(`${this.base}guardarSistemaManejoPFDM`, params);
  }

  listarActividadSilviculturalPFDM(param: any) {
    const params = new HttpParams()
      .set("idPlanManejo", String(param.idPlanManejo))
      .set("idTipo", param.idTipo);
    return this.http.get(
      `${this.base}listarActividadSilviculturalPFDM?` + params
    );
  }

  listarActividadSilviculturalPFDMTitular(param: any) {
    const params = new HttpParams()
      .set("tipoDocumento", param.tipoDocumento)
      .set("nroDocumento", param.nroDocumento)
      .set("codigoProcesoTitular", param.codigoProcesoTitular)
      .set("codigoProceso", param.codigoProceso)
      .set("idPlanManejo", String(param.idPlanManejo));
    return this.http.get(`${this.base}listarActividadSilviculturalPFDMTitular?` + params);
  }

  eliminarActividadSilviculturalPFDM(params: any) {
    return this.http.post(
      `${this.base}eliminarActividadSilviculturalPFDM`,
      params
    );
  }
}
