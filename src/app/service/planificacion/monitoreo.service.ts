import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

@Injectable({ providedIn: "root" })
export class MonitoreoService {
  base = `${environment.urlProcesos}api/monitoreo/`;

  constructor(private http: HttpClient) {}

  listarMonitoreo(params: any) {
    return this.http.post(`${this.base}listarMonitoreo`, params);
  }

  listarMonitoreoPOCC(param: any) {
    const params = new HttpParams()
      .set("idPlanManejo", param.idPlanManejo.toString())
      .set("codigoMonitoreo", param.codigoMonitoreo);
    return this.http.post(`${this.base}listarMonitoreoPOCC`, params);
  }

  registrarMonitoreoPOCC(params: any) {
    return this.http.post(`${this.base}registrarMonitoreoPOCC`, params);
  }
  actualizarMonitoreoPOCC(params: any) {
    return this.http.post(`${this.base}actualizarMonitoreoPOCC`, params);
  }
  registrarMonitoreo(params: any) {
    return this.http.post(`${this.base}registrarMonitoreo`, params);
  }

  ActualizarMonitoreo(params: any) {
    return this.http.post(`${this.base}ActualizarMonitoreo`, params);
  }

  EliminarDetalleMonitoreo(params: any) {
    return this.http.post(`${this.base}EliminarDetalleMonitoreo`, params);
  }
  eliminarMonitoreoCabecera(params: any) {
    return this.http.post(`${this.base}EliminarMonitoreoCabera`, params);
  }
}
