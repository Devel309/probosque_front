import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import { environment as env } from '@env/environment';
import { InformacionGeneralDema, ResponseModel } from '@models';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class InformacionGeneralDemaService {

  private base = `${env.urlProcesos}api/informacionGeneralDema/`;
  private censoForestal = `${env.urlProcesos}api/censoForestal/`;

  constructor(private http: HttpClient) { }


  obtener(idPlanManejo: number, codigoProceso: string): Observable<ResponseModel<InformacionGeneralDema[]>> {
    const uri = `${this.base}listarInformacionGeneralDema`;
    const body = { idPlanManejo, codigoProceso }
    return this.http.post<ResponseModel<InformacionGeneralDema[]>>(uri, body)
      .pipe(tap({ error: err => console.log(err) }));
  }

  obtenerAprovechamiento(idPlanManejo: number, tipoEspecie: number): Observable<ResponseModel<any[]>> {
    const uri = `${this.censoForestal}ListaTipoRecursoForestalManejo`;

    const params = new HttpParams()
      .set('idPlanManejo', String(idPlanManejo))
      .set('tipoCenso', '')
      .set('tipoEspecie', String(tipoEspecie));

    return this.http.post<ResponseModel<any[]>>(uri, null, { params })
      .pipe(tap({ error: err => console.log(err) }));
  }

  registrar(body: InformacionGeneralDema): Observable<ResponseModel<InformacionGeneralDema>> {
    const uri = `${this.base}registrarInformacionGeneralDema`;
    return this.http.post<ResponseModel<InformacionGeneralDema>>(uri, body)
      .pipe(tap({ error: err => console.log(err) }));
  }

  actualizar(body: InformacionGeneralDema): Observable<ResponseModel<InformacionGeneralDema>> {
    const uri = `${this.base}actualizarInformacionGeneralDema`;
    return this.http.post<ResponseModel<InformacionGeneralDema>>(uri, body)
      .pipe(tap({ error: err => console.log(err) }));
  }

}