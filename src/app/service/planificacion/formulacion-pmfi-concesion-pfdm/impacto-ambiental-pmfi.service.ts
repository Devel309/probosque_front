import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ImpactoAmbientalPmfiService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  listarPmfiImpactoAmbiental(body: any) {
    return this.http.post(
      this.base + "api/impactoAmbiental/listarImpactoAmbientalPmfi",
      body
    );
  }

  registrarImpactoAmbientalPmfi(body: any) {
    return this.http.post(
      this.base + "api/impactoAmbiental/registrarImpactoAmbientalPmfi",
      body
    );
  }

  eliminarImpactoAmbientalDetalle(body: any) {
    return this.http.post(
      this.base + "api/impactoAmbiental/eliminarImpactoAmbientalDetalle",
      body
    );
  }
}
