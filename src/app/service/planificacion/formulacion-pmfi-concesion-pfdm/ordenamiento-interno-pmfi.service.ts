import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PGMFArchivoDto, ResponseModel } from "@models";
import { map } from "rxjs/operators";
import { PlanManejoArchivo } from "src/app/model/PlanManejoArchivo";
import { environment } from "../../../../environments/environment";
import {Observable} from 'rxjs';

@Injectable({ providedIn: "root" })
export class OrdenamientoInternoPmfiService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) { }

  listarOrdenamiento(params: any) {
    return this.http.post(
      this.base + "api/ordenamientoProteccion/ListarOrdenamientoInterno", params
    );
  }

  listarOrdenamientoDetalle(params: any) {
    return this.http.post(
      this.base + "api/ordenamientoProteccion/ListarOrdenamientoInternoDetalle", params
    );
  }

  eliminarOrdenamiento(params: any) {
    return this.http.post(
      this.base + "api/ordenamientoProteccion/eliminarOrdenamientoInterno", params
    );
  }

  registrarOrdenamiento(params: any) {
    return this.http.post(
      this.base + "api/ordenamientoProteccion/registrarOrdenamientoInterno", params
    );
  }

  /*proteccionVigilanciaCargarExcel(params: any) {
    return this.http.post(this.base + "api/ordenamientoProteccion/proteccionVigilancia/cargarExcel", params);
  }*/

  //t.file, item.nombreHoja, item.numeroFila,
  //           item.numeroColumna, item.codTipoOrdenamiento, item.codigoTipoOrdenamientoDet, item.idPlanManejo, item.idUsuarioRegistro
  proteccionVigilanciaCargarExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codTipoOrdenamiento: string,
    subCodTipoOrdenamiento:string,
    codigoTipoOrdenamientoDet: string,
    idPlanManejo: number,
    idUsuarioRegistro: number): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codTipoOrdenamiento', String(codTipoOrdenamiento))
      .set('subCodTipoOrdenamiento', String(subCodTipoOrdenamiento))
      .set('codigoTipoOrdenamientoDet', String(codigoTipoOrdenamientoDet))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(this.base + 'api/ordenamientoProteccion/proteccionVigilancia/cargarExcel', body, { params, reportProgress: true, });
  }



  registrarArchivo(body: PGMFArchivoDto) {
    const uri = `${this.base}api/demaAnexos/registrarArchivo`;
    return this.http.post<ResponseModel<any>>(uri, body);
  }

  obtenerArchivo(body: any) {
    const uri = `${this.base}api/demaAnexos/listarArchivoDema`;
    return this.http.post<ResponseModel<any>>(uri, body)
      .pipe(map(res => {
        if (res?.data?.length > 0) return res?.data[0];
        return null;
      }))
  }

  obtenerRelacionArchivo(codigoProceso: string, idPlanManejo: number, idTipoDocumento: string) {
    const uri = `${this.base}api/demaAnexos/listarArchivoDema`;
    const body = { codigoProceso, idPlanManejo, idTipoDocumento };
    return this.http.post<ResponseModel<any>>(uri, body)
      .pipe(map(res => {
        if (res?.data?.length > 0) return res?.data[0];
        return null;
      }))
  }

  eliminarArchivo(body: PlanManejoArchivo) {
    const uri = `${this.base}api/planManejo/eliminarPlanManejoArchivo`;
    return this.http.post<ResponseModel<any>>(uri, body);
  }



}
