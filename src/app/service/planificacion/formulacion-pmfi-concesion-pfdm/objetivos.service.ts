import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class ObjetivosService {
  base = `${environment.urlProcesos}api/objetivoManejo/`;

  constructor(private http: HttpClient) {}

  listarObjetivoManejo(params: any) {
    return this.http.post(`${this.base}listarObjetivoManejo`, params);
  }

  registrarObjetivoManejo(params: any) {
    return this.http.post(`${this.base}registrarObjetivoManejo`, params);
  }

  actualizarObjetivoManejo(params: any) {
    return this.http.post(`${this.base}actualizarObjetivoManejo`, params);
  }

  eliminarObjetivoManejo(params: any) {
    return this.http.post(`${this.base}eliminarObjetivoManejo`, params);
  }

  listarObjetivo(param: any) {
    const params = new HttpParams()
      .set("codigoObjetivo", param.codigoObjetivo)
      .set("idPlanManejo", String(param.idPlanManejo));
    return this.http.get(`${this.base}listarObjetivo?`, { params });
  }

  registrarObjetivo(params: any): Observable<any> {
    return this.http.post(`${this.base}registrarObjetivo`, params);
  }
}
