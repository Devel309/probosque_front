import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class RecursoServicioPmfiService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}


  listarIdentificacionEspecieAprovechar(params: any) {
    return this.http.post(this.base + "api/recursoForestal/listarRecursoForestal", params);
  }

  registrarRecursosyServicios(params: any) {
    return this.http.post(this.base + "api/recursoForestal/registrarRecursoForestal", params);
  }

  eliminarIdentiEspecieAprovechar(params: any) {
    return this.http.post(this.base + "api/recursoForestal/eliminarRecursoForestal", params);
  }

  listarCenso(params: any) {
    return this.http.post(this.base + "api/censoForestal/listaCensoForestalDetalle", params);
  }


  listarResumenesCenso(params: any) {
    return this.http.post(this.base + "api/censoForestal/listaResumenEspeciesInventario", params);
  }


}
