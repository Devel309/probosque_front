import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ResumenService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }

  consolidado(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo))
    return this.http.get(`${this.base}api/planManejo/consolidadoPMFI`, { params });
  }

  consolidadoPGMFA(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoPGMFA`, { params });
  }

  consolidadoPFCR(idPermisoForestal: number) {
    const params = new HttpParams().set('idPermisoForestal', String(idPermisoForestal));
    return this.http.get(`${this.base}api/planManejo/consolidadoPFCR`, { params });
  }

  obtenerInformacionPlan(param: any) {
    const params = new HttpParams()
    .set('idPlanManejo', String(param.idPlanManejo))
    .set('tipoPlan', param.tipoPlan);
    return this.http.get(`${this.base}api/planManejo/obtenerInformacionPlan`, { params });
  }

  generarDispositivoLegal(idPlanManejo: number, tipoPlan:string) {
    const params = new HttpParams().set('tipoPlan', String(tipoPlan));
    return this.http.get(`${this.base}api/evaluacion/plantillaResolucion/${idPlanManejo}`, { params });
  }

}
