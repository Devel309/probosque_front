import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNullOrEmpty } from '@shared';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class InformacionAreaPmfiService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  listarInformacionBasica(param: any) {
    let params;
    if (param?.codCabecera) {
      params = new HttpParams()
      .set('idInfBasica', param.idInfBasica)
      .set('idPlanManejo', param.idPlanManejo.toString())
      .set('codCabecera', param.codCabecera);
    }else{
      params = new HttpParams()
      .set('idInfBasica', param.idInfBasica)
      .set('idPlanManejo', param.idPlanManejo.toString());
    }

    return this.http.post(this.base + 'api/informacionBasica/listarInfBasicaAerea?' + params, null);
  }

  listarInformacionBasicaDetalle(param: any) {
    const params = new HttpParams()
      .set('codTipo', param.codCabecera)
      .set('codSubTipo', param.codSubTipo)
      .set('idInfBasica', param.idInfBasica);

    return this.http.post(
      this.base + 'api/informacionBasica/listarInfBasicaAreaDetalle?' + params,
      null
    );
  }

  registrarInformacionBasica(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/registrarInformacionBasicaDetalle',
      params
    );
  }
  registrarInformacionBasicaAspectosFisicos(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/registrarInformacionBasicaDetalle',
      params
    );
  }

  actualizarInformacionBasica(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/actualizarInformacionBasicaDetalle',
      params
    );
  }

  eliminarInformacionBasica(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/eliminarInfBasicaAerea',
      params
    );
  }

  listarUbigeoContratos(idContrato: number) {
    return this.http.get(
      this.base +
        `api/planGeneralManejoForestal/listarUbigeoContratos/${idContrato}`
    );
  }

  listarMedioTransporte(idMedio: number) {
    const params = new HttpParams().set('idMedio', idMedio.toString());

    return this.http.post(
      this.base + `api/medioTransporte/ListarMedioTransporte?` + params,
      null
    );
  }

  obtenerHidrografia(param: any) {
    const params = new HttpParams()
      .set('idHidrografia', param.id.toString())
      .set('tipoHidrografia', param.tipo);

    return this.http.post(
      this.base + `api/informacionBasica/obtenerHidrografia?` + params,
      null
    );
  }

  obtenerFauna(param: any) {
    const params = new HttpParams()
    .set('idPlanManejo', param.idPlanManejo.toString())
    .set('codigoTipo', param.codigoTipo);

    return this.http.get(
      this.base + `api/informacionBasica/obtenerFauna?` + params
    );
  }

  registrarSolicitudFauna(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/registrarSolicitudFauna',
      params
    );
  }

  eliminarFauna(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/EliminarFauna',
      params
    );
  }


  registrarInformacionBasicaUbigeo(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/registrarInformacionBasicaUbigeo',
      params
    );
  }

  listarInformacionBasicaUbigeo(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/listarInfBasicaUbigeo',
      params
    );
  }

  eliminarInformacionBasicaUbigeo(params: any) {
    return this.http.post(
      this.base + 'api/informacionBasica/eliminarInformacionBasicaUbigeo',
      params
    );
  }
}
