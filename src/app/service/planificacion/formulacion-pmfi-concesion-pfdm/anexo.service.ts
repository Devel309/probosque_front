import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AnexosService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) { }

  Anexo2(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan)
      .set("tipoProceso", body.tipoProceso.toString());

    return this.http.post(
      this.base + "api/censoForestal/POConcesiones/Anexos/Anexo2?" + params,
      null
    );
  }

  ResultadosInventarios(body: any) {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan)
      .set("tipoProceso", body.tipoProceso.toString());

    return this.http.post(
      this.base + "api/censoForestal/PMFI/ResultadosInventarios?" + params,
      null
    );
  }
}
