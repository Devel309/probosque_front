import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNullOrEmpty } from '@shared';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class InformacionGeneralService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  listarInformacionGeneralDema(params: any) {
    return this.http.post(
      this.base + 'api/informacionGeneralDema/listarInformacionGeneralDema',
      params
    );
  }

  registrarInformacionGeneralDema(params: any) {
    return this.http.post(
      this.base + 'api/informacionGeneralDema/registrarInformacionGeneralDema',
      params
    );
  }

  actualizarInformacionGeneralDema(params: any) {
    return this.http.post(
      this.base + 'api/informacionGeneralDema/actualizarInformacionGeneralDema',
      params
    );
  }

  listarPorFiltroOtorgamiento(param: any) {
    let params = new HttpParams();
    if (!isNullOrEmpty(param.nroDocumento)) params = params.set('nroDocumento', String(param.nroDocumento));
    if (!isNullOrEmpty(param.nroRucEmpresa)) params = params.set('nroRucEmpresa', String(param.nroRucEmpresa));
    if (!isNullOrEmpty(param.idPlanManejo)) params = params.set('idPlanManejo', String(param.idPlanManejo));
    
    return this.http.post(
      this.base + 'api/otorgamiento/listarPorFiltroOtorgamiento?' + params,
      null
    );
  }

  listarPorFiltroInformacionGeneral(param: any) {
    let params = new HttpParams();
    if (!isNullOrEmpty(param.nroDocumento)) params = params.set('nroDocumento', String(param.nroDocumento));
    if (!isNullOrEmpty(param.nroRucEmpresa)) params = params.set('nroRucEmpresa', String(param.nroRucEmpresa));
    if (!isNullOrEmpty(param.codTipoPlan)) params = params.set('codTipoPlan', String(param.codTipoPlan));
    if (!isNullOrEmpty(param.idPlanManejo)) params = params.set('idPlanManejo', String(param.idPlanManejo));
    
    return this.http.post(
      this.base + 'api/informacionGeneralDema/listarPorFiltroInformacionGeneral?' + params,
      null
    );
  }
  actualizarInfGeneralResumido(params: any) {
    return this.http.post(
      this.base + 'api/informacionGeneral/actualizarInfGeneralResumido',
      params
    );
  }
  listarInfGeneralResumido(params: any) {
    return this.http.post(
      this.base + 'api/informacionGeneral/listarInfGeneralResumido',
      params
    );
  }
}
