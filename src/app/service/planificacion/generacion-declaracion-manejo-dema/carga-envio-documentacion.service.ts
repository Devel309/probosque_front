import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class CargaEnvioDocumentacionService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}
  listarEstadosPlanManejo(body: any) {
    return this.http.post(
      this.base + "api/planManejo/listarEstadoPlanManejo",
      body
    );
  }

  registrarEstadosPlanManejo(body: any) {
    return this.http.post(
      this.base + "api/planManejo/registrarEstadoPlanManejo",
      body
    );
  }

  actualizarEstadosPlanManejo(body: any) {
    return this.http.post(
      this.base + "api/planManejo/actualizarEstadoPlanManejo",
      body
    );
  }

  listarTabsRegistrado(body: any) {
    return this.http.post(
      this.base + "api/planManejo/ListarTabsRegistrado",
      body
    );
  }
}
