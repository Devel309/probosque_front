import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class InformacionGeneralService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }

  listarInformacionGeneralDema(params: any){
    return this.http.post(this.base +'api/informacionGeneralDema/listarInformacionGeneralDema', params );
  }

  actualizarInformacionGeneralDema(params: any){
    return this.http.post(this.base +'api/informacionGeneralDema/actualizarInformacionGeneralDema', params );
  }

}
