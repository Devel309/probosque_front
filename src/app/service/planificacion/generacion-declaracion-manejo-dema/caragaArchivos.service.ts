import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CargaArchivosService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  consolidado(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/consolidado`, { params });
  }

  anexos(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/consolidado`, { params });
  }

  consolidadoPOCC(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoPOCC`, { params });
  }

  consolidadoPMFIC(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoPMFIC`, { params });
  }

  consolidadoPOAC(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoPOAC`, { params });
  }


  consolidadoPOPAC(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoPOPAC`, { params });
  }

  consolidadoDEMA(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoDEMA`, { params });
  }

  consolidadoPMFI(idPlanManejo: number) {
    const params = new HttpParams().set('idPlanManejo', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/pdf/consolidadoPMFI`, { params });
  }

}
