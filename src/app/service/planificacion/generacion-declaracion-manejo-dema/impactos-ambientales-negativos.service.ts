
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ImpactosAmbientalesNegativosService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }

  listarImpactoAmbiental(params: any){
    return this.http.post(this.base +'api/impactoAmbiental/listarImpactoAmbiental', params );
  }

  registrarImpactoAmbiental(params: any){
    return this.http.post(this.base +'api/impactoAmbiental/registrarImpactoAmbiental', params );
  }

  eliminarImpactoAmbiental(params: any){
    return this.http.post(this.base +'api/impactoAmbiental/eliminarImpactoAmbiental', params );
  }

  eliminarImpactoAmbientalDetalle(params: any){
    return this.http.post(this.base +'api/impactoAmbiental/eliminarImpactoAmbientalDetalle', params );
  }
   
}
