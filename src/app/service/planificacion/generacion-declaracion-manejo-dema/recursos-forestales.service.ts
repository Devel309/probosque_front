import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class RecursoForestalService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }


  listarRecurosForestales(params: any){
    return this.http.post(this.base +'api/censoForestal/listaCensoForestalDetalle', params );
  }


  listarRecurosResumen(params: any){
    return this.http.post(this.base +'api/censoForestal/listaResumenEspecies', params );
  }

  listarRecursosMaderablesCensoForestalDetalle(params: any){
    return this.http.post(this.base +'api/censoForestal/listarRecursosMaderablesCensoForestalDetalle', params );
  }

  actualizarCensoForestal(params: any){
    return this.http.post(this.base +'api/censoForestal/actualizarCensoForestal', params );
  }

  obtenerInfoAnexoCensoForestalDetalle(idPlanManejo: any){
    return this.http.get(this.base + `api/censoForestal/obtenerInfoAnexoCensoForestalDetalle/${idPlanManejo}` );
  }
  
  
}
