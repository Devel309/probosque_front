import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ZonificacionOrdenamientoInternoAreaService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }

  listarZonificacion(params: any){
    return this.http.post(this.base +'api/zonificacionDema/listarZonificacion', params );
  }
  listarZona(params: any){
    return this.http.post(this.base +'api/zonificacionDema/listarZona', params );
  }
  registrarZona(params: any){
    return this.http.post(this.base +'api/zonificacionDema/registrarZona', params );
  }
  eliminarZona(params: any){
    return this.http.post(this.base +'api/zonificacionDema/eliminarZona', params );
  }
  registrarZonificacion(params: any){
    return this.http.post(this.base +'api/zonificacionDema/registrarZonificacion', params );
  }

  eliminarZonificacion(params: any){
    return this.http.post(this.base +'api/zonificacionDema/eliminarZonificacion', params );
  }
  
}
