import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseModel } from '@models';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CronogrmaActividadesService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }

  registrarCronogramaActividades(params: any){
    return this.http.post(this.base +'api/CronogramaActividades/RegistrarCronogramaActividad', params );
  }

  registrarMarcaCronogramaActividadDetalle(params: any){
    return this.http.post(this.base +'api/CronogramaActividades/registrarMarcaCronogramaActividadDetalle', params );
  
    } 

  listarCronogramaActividad(params: any){
    return this.http.post(this.base +'api/CronogramaActividades/ListarCronogramaActividad', params );
  }

  eliminarCronogramaActividad(params: any){
    return this.http.post(this.base +'api/CronogramaActividades/EliminarCronogramaActividad', params ); 
  }

  eliminarCronogramaActividadDetalle(params: any){
    return this.http.post(this.base +'api/CronogramaActividades/EliminarCronogramaActividadDetalle', params );
  }

  registrarCronogramaActividadExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codigoProceso: string,
    codigoActividad: string,
    idPlanManejo: number,
    idUsuarioRegistro: number): Observable<ResponseModel<number>> {
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codigoProceso', String(codigoProceso))
      .set('codigoActividad', String(codigoActividad))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(this.base + 'api/CronogramaActividades/registrarCronogramaActividadExcel', body, { params, reportProgress: true, });
  }

  /*********************/ 
  registrarConfigCronoActi(params: any){
    return this.http.post<ResponseModel<any>>(this.base +'api/CronogramaActividades/registrarConfiguracionCronogramaActividad', params );
  }

  listarPorFiltroCronogramaActividad(params: any){
    return this.http.post(this.base +'api/CronogramaActividades/listarPorFiltroCronogramaActividad', params );
  }
   
}
