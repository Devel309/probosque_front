import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseModel } from '@models';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { map } from "rxjs/operators";

@Injectable({ providedIn: 'root' })
export class AnexosService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  listarCensoForestalAnexo2(params: any) {
    return this.http.post(
      this.base + 'api/censoForestal/ListarCensoForestalAnexo2?' + params,
      null
    );
  }

  listarCensoForestalAnexo3(params: any) {
    return this.http.post(
      this.base + 'api/censoForestal/ListarCensoForestalAnexo3?' + params,
      null
    );
  }

  registrarArchivo(params: any) {
    return this.http.post(
      this.base + 'api/demaAnexos/registrarArchivo',
      params
    );
  }

  listarArchivoDema(params: any) {
    return this.http.post(
      this.base + 'api/demaAnexos/listarArchivoDema',
      params
    );
  }

  cargarAnexos(
    idUsuario: number,
    tipoDocumento: number,
    archivo: File
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}api/archivo/cargarArchivoGeneral`;

    const params = new HttpParams()
      .set('IdUsuarioCreacion', String(idUsuario))
      .set('TipoDocumento', String(tipoDocumento));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(uri, body, {
      params,
      reportProgress: true,
    });
  }

  generarAnexo3Dema(params: any) {
    return this.http.post(
      this.base + 'api/demaAnexos/generarAnexo3Dema',
      params
    );
  }

  generarAnexo2Dema(params: any) {
    return this.http.post(
      this.base + 'api/demaAnexos/generarAnexo2Dema',
      params
    );
  }

  eliminarArchivo(params: any) {
    return this.http.post(
      this.base + 'api/archivo/eliminarArchivo?' + params,
      null
    );
  }

  listarPlanManejoListar(param: any) {
    const params = new HttpParams()
      .set('idPlanManejo', String(param.idPlanManejo))
      .set('codigoProceso', String(param.codigoProceso))
      .set('tipoDocumento', String(param.idTipoDocumento));
    return this.http.post<ResponseModel<any>>(
      this.base + 'api/planManejo/listarPlanManejoListar?' + params,
      null
    ).pipe(map(res => {
      if (res?.data?.length > 0) return res?.data[0];
      return null;
    }));
  }

  listarPlanManejoArchivo(param: any) {
    const params = new HttpParams()
      .set('idPlanManejo', String(param.idPlanManejo))
      .set('codigoProceso', String(param.codigoProceso))
      .set('tipoDocumento', String(param.idTipoDocumento))
      .set('idArchivo', String(param.idArchivo));
    return this.http.post<ResponseModel<any>>(
      this.base + 'api/planManejo/listarPlanManejoListar?' + params,
      null
    ).pipe(map(res => {
      if (res?.data?.length > 0) return res?.data[0];
      return null;
    }));
  }

  listarArchivosPlanManejo(param: any) {
    let params = new HttpParams()
      .set('idPlanManejo', String(param.idPlanManejo));
      if (param.extension) {
        params = params.set('extension', param.extension);
      }
      if (param.tipoDocumento) {
        params = params.set('tipoDocumento', param.tipoDocumento);
      }
    return this.http.post(
      this.base + 'api/planManejo/listarPlanManejoListar?' + params,
      null
    );
  }

  planManejoArchivoEntidad(param: any) {
    const params = new HttpParams()
      .set('idPlanManejoArchivo', String(param.idPlanManejoArchivo))
    
    return this.http.post<ResponseModel<any>>(
      this.base + 'api/planManejo/PlanManejoArchivoEntidad?' + params,
      null
    ).pipe(map(res => {
      if (res?.data?.length > 0) return res?.data[0];
      return null;
    }));
  }

  Anexo2(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan)
      .set("tipoProceso", body.tipoProceso.toString());

    return this.http.post(
      this.base + "api/censoForestal/POConcesiones/Anexos/Anexo2?" + params,
      null
    );
  }

  AprovechamientoRecursoForestalNoMaderableResumen(body: any) {
    const params = new HttpParams()
      .set("idPlanManejo", body.idPlanManejo.toString())
      .set("tipoPlan", body.tipoPlan)
      .set("tipoProceso", body.tipoProceso.toString());

    return this.http.post(
      this.base + "api/censoForestal/DEMA/AprovechamientoRecursoForestalNoMaderableResumen?" + params,
      null
    );
  }

  Anexo5(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/POConcesiones/Anexos/Anexo2?" + params,
      null
    );
  }

  Anexo6(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(this.base + "api/censoForestal/Anexo6", null, {
      params,
    });
  }

  Anexo7NoMaderable(body: any) {
    const params = new HttpParams()
      .set("idPlanDeManejo", body.idPlanDeManejo.toString())
      .set("tipoPlan", body.tipoPlan);

    return this.http.post(
      this.base + "api/censoForestal/ListaAnexo7NoMaderable",
      null,
      { params }
    );
  }
}
