import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Injectable({ providedIn: "root" })
export class OrdenamientoProteccionService {
  base = `${environment.urlProcesos}api/ordenamientoProteccion`;

  constructor(private http: HttpClient) {}

  listarOrdenamientoInternoDetalle(params: any) {
    return this.http.post(
      this.base + "/ListarOrdenamientoInternoDetalle",
      params
    );
  }
  ListarOrdenamientoProteccionPorFiltro(params: any) {
    return this.http.get(this.base + "/ListarOrdenamientoProteccionPorFiltro", {
      params,
    });
  }
  registrarOrdenamientoInterno(params: any) {
    return this.http.post(this.base + "/registrarOrdenamientoInterno", params);
  }
  eliminarOrdenamientoInterno(params: any) {
    return this.http.post(this.base + "/eliminarOrdenamientoInterno", params);
  }
}
