import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ResponseModel } from '@models';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CargaEnvioDocumentacionService {

  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient,) { }

  generarEvaluacionCompiladaPGMF(idPlanManejo: number, nombreUsuarioArffs: string, tipoDocumento: string) {
    const params = new HttpParams()
      .set('idPlanManejo', String(idPlanManejo))
      .set('nombreUsuarioArffs', nombreUsuarioArffs)
      .set('tipoDocumento', tipoDocumento)
    return this.http.get(`${this.base}api/planManejo/generarEvaluacionCompiladaPGMF`, { params });
  }


  generarEvaluacionPGMF(idPlanManejo: number, nombreUsuarioArffs: string, tipoDocumento: string, idPlanManejoEvaluacion: number) {
    const params = new HttpParams()
      .set('idPlanManejo', idPlanManejo.toString())
      .set('nombreUsuarioArffs', nombreUsuarioArffs)
      .set('tipoDocumento', tipoDocumento)
      .set('idPlanManejoEvaluacion', idPlanManejoEvaluacion.toString())
    return this.http.get(`${this.base}api/planManejo/generarEvaluacionCompiladaPGMF`, { params });
  }

  compilarPGMF(params: any) {
    return this.http.post(`${this.base}api/planManejo/generarEvaluacionCompiladaPGMF`, params);
  }

  cargarCompiladoFirmadoPGMF(idUsuario: number, tipoDocumento: number, archivo: File): Observable<ResponseModel<number>> {

    const uri = `${this.base}api/archivo/cargarArchivoGeneral`;

    const params = new HttpParams()
      .set('IdUsuarioCreacion', String(idUsuario))
      .set('TipoDocumento', String(tipoDocumento));

    const body = new FormData();
    body.append("file", archivo);

    return this.http.post<ResponseModel<number>>(uri, body, { params, reportProgress: true });

  }

  subirPdf() {

  }

  registrarArchivo(params: any) {
    return this.http.post(this.base + 'api/postulacionPFDM/registrarArchivo', params);
  }

  listarArchivosPGMF(idPlanManejo: number, codigoTipoPGMF: string) {
    return this.http.get(`${this.base}api/postulacionPFDM/listarDetalleArchivo/${idPlanManejo}/${codigoTipoPGMF}`);
  }

  eliminarPdf(idArchivo: number, idUsuario: number) {
    return this.http.get(`${this.base}api/postulacionPFDM/eliminarDetalleArchivo/${idArchivo}/${idUsuario}`);
  }

  obtenerArchivo(params: any) {
    return this.http.post(`${this.base}api/archivo/DescargarArchivoGeneral`, params);
  }

  consolidadoPGMF(idPlanManejo: number) {
    const params = new HttpParams().set('idPGMF', String(idPlanManejo));
    return this.http.get(`${this.base}api/planManejo/consolidadoPGMF`, { params });

  }

  consolidadoPGMF_PDF(params: any) {
    return this.http.post(`${this.base}api/planManejo/descargarConsolidadoPGMF_PDF`, params);
  }

  listarEstadosPlanManejo(body: any) {
    return this.http.post(
      this.base + "api/planManejo/listarEstadoPlanManejo",
      body
    );
  }

  registrarEstadosPlanManejo(body: any) {
    return this.http.post(
      this.base + "api/planManejo/registrarEstadoPlanManejo",
      body
    );
  }

  listarTabsRegistrado(body: any) {
    return this.http.post(
      this.base + "api/planManejo/ListarTabsRegistrado",
      body
    );
  }

  actualizarEstadosPlanManejo(body: any) {
    return this.http.post(
      this.base + "api/planManejo/actualizarEstadoPlanManejo",
      body
    );
  }
}
