import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment as env } from '@env/environment';
import { ResponseModel } from '@models';
import { Observable } from 'rxjs';
import { ArchivoModel } from '../model/Comun/ArchivoModel';

@Injectable({
	providedIn: 'root'
})
export class ArchivoService {

	base = `${env.urlProcesos}api/archivo`;


	constructor(private http: HttpClient) { }

	cargar(idUsuario: number, tipoDocumento: string, archivo: File): Observable<ResponseModel<number>> {

		const uri = `${this.base}/cargarArchivoGeneral`;

		const params = new HttpParams()
			.set('IdUsuarioCreacion', String(idUsuario))
			.set('TipoDocumento', tipoDocumento);

		const body = new FormData();
		body.append("file", archivo);

		return this.http.post<ResponseModel<number>>(uri, body, { params, reportProgress: true });

	}

	obtenerArchivo(idArchivo: number): Observable<ResponseModel<ArchivoModel>> {

		const uri = `${this.base}/obtenerArchivo/` + idArchivo;
		return this.http.get<ResponseModel<ArchivoModel>>(uri);

	}

	obtenerArchivoGeneral(idArchivo: number) {

		const uri = `${this.base}/obtenerArchivoGeneral`;

		const params = new HttpParams()
			.set('idArchivo', String(idArchivo));

		return this.http.get<any>(uri, { params });
	}

	descargarArchivoGeneral(params: any) {
		return this.http.post(`${this.base}/DescargarArchivoGeneral`, params);
	}

	eliminarArchivo(idArchivo: number, idUsuarioElimina: number) {
		const uri = `${this.base}/eliminarArchivo`;

		const params = new HttpParams()
			.set('idArchivo', String(idArchivo))
			.set('idUsuarioElimina', String(idUsuarioElimina));
		return this.http.post<ResponseModel<any>>(uri, null, { params, reportProgress: true });
	}

	descargarPlantilla(nombreGenerado: string) {
		const body = new FormData();
		body.append("nombreGeneradoExtension", nombreGenerado);

		return this.http.post<ResponseModel<any>>(`${this.base}/descargarPlantillas`, body, { reportProgress: true });
	}
}