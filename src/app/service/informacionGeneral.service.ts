
import { HttpClient,HttpClientModule, HttpEvent, HttpHandler, HttpHeaders, HttpRequest ,HttpInterceptor} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class InformacionGeneralService {


  base =environment.urlProcesos;// 'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http : HttpClient, private config : ConfigService) { }

  //Información General
  guardarInformacionGeneral(params : any){
    return this.http.post(this.base +'api/solicitudAcceso/ListarSolicitudAcceso', params);
  }

  // //listarSolicitudAccesoUsuario
  // listarSolicitudAccesoUsuario(params : any){
  //   return this.http.post(this.base +'api/solicitudAcceso/ListarSolicitudAccesoUsuario', params);
  // }
  // obtenerSolicitudAcceso(params : any){
  //   return this.http.post(this.base +'api/solicitudAcceso/ObtenerSolicitudAcceso', params);
  // }
  // registrarSolicitudAcceso(params : any){
  //   return this.http.post(this.base +'api/solicitudAcceso/RegistrarSolicitudAcceso', params);
  // }
  // actualizarRevisionSolicitudAcceso(params : any){
  //   return this.http.post(this.base +'api/solicitudAcceso/ActualizarRevisionSolicitudAcceso', params);
  // }

  // enviarCorreo(params : any){
  //   return this.http.post(this.base +'api/email/send', params);
  // }

  // cargarArchivoSolicitudAcceso(archivo:File, idSolicitudAcceso:any) {
  //   let formData = new FormData();
  //   formData.append("file", archivo);
  //   formData.append("idSolicitudAcceso", idSolicitudAcceso);
  //   formData.append("tipoArchivo", 'SOLACCSUS');
  //   const req = new HttpRequest('POST',this.base +'api/archivoSolicitud/cargarArchivo', formData, {
  //     reportProgress: true
  //   });
  //   return this.http.request(req);
  // }

  // descargarArchivoSolicitudAcceso(idArchivoSolicitud: any){
  //   return this.http.get(this.base + `api/archivoSolicitud/descargarArchivo/${idArchivoSolicitud}` );
  // }

  // obtenerArchivoSolicitud(idArchivoSolicitud: any){
  //   return this.http.get(this.base +`api/archivoSolicitud/obtenerArchivoSolicitud/${idArchivoSolicitud}`);
  // }

}

