import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpParams,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AprovechamientoResponse,
  CensoForestalDetalle,
  FactorActividadModel,
  PlanManejoContrato,
  ResponseModel,
} from '@models';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root',
})
export class PlanificacionService {
  // base = 'http://127.0.0.1:8080/mcsniffs-rest/';
  //base = 'http://10.6.1.162/mcsniffs-rest/';
  //base = 'http://10.81.234.18:8080/mcsniffs-rest/';
  base = environment.urlProcesos; // 'http://10.6.1.162/mcsniffs-rest/';
  coreCentral = environment.urlCoreCentral;
  //base = 'http://10.81.234.10:8082/mcsniffs-rest/';

  constructor(private http: HttpClient, private config: ConfigService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let auth = req;
    const token = localStorage.getItem('token');
    //console.log('token : '+ token);
    if (token != null && token != '0') {
      //console.log('token : '+ token);
      auth = req.clone({ headers: req.headers.set('Authorization', token) });
    }

    return next.handle(auth);
  }

  /*
  loginInicial(){

      var params_defoult =
      {
        "username" :"ADMIN_SEG",
        "password" :"123"
      }
    return this.http.post(this.base +'api/autenticacion/token' ,params_defoult);
  }
 */
  //CoreCentral
  registrarCronogramaActividad(params: any) {
    return this.http.post(
      this.base + 'api/CronogramaActividades/RegistrarCronogramaActividad',
      params
    );
  }
  protecionVigilanciaObtener(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/protecionVigilanciaObtener',
      params
    );
  }
  protecionVigilanciaArchivo(formd: FormData, params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/protecionVigilanciaArchivo?' + params,
      formd
    );
  }
  cronogramaActividadDetalle_listarDetalle(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/cronogramaActividadDetalle_listarDetalle',
      params
    );
  }
  cronogramaActividadEliminar(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/cronogramaActividadEliminar',
      params
    );
  }
  monitoreoActualizar(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/monitoreoActualizar',
      params
    );
  }
  programaInversionEliminar(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/programaInversionEliminar',
      params
    );
  }
  programaInversionObtener(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/programaInversionObtener',
      params
    );
  }
  programaInversionRegistrar(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/programaInversionRegistrar',
      params
    );
  }
  organizacionManejo(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/organizacionManejo',
      params
    );
  }
  PGMFAbreviadoDetalleRegistrar(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/PGMFAbreviadoDetalleRegistrar',
      params
    );
  }
  monitoreo(params: any) {
    return this.http.post(this.base + 'api/PGMFAbreviado/monitoreo', params);
  }

  detalleEliminar(params: any) {
    return this.http.post(
      this.base + 'api/capacitacion/eliminarCapacitacion',
      params
    );
  }

  organizacionManejoDetalleEliminar(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/detalleEliminar',
      params
    );
  }
  detalleRegistrar(params: any) {
    return this.http.post(
      this.base + 'api/capacitacion/registrarCapacitacionDetalle',
      params
    );
  }
  participacionCiudadana(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/participacionCiudadana',
      params
    );
  }
  capacitacion(params: any) {
    return this.http.post(
      this.base + 'api/capacitacion/listarCapacitacion',
      params
    );
  }

  listarPorFiltroDepartamento_core_central(params: any) {
    return this.http.post(
      this.coreCentral + 'api/coreCentral/listarPorFiltroDepartamento',
      params
    );
  }
  listarPorFilroProvincia(params: any) {
    return this.http.post(
      this.coreCentral + 'api/coreCentral/listarPorFiltroProvincia',
      params
    );
  }
  listarPorFilroDistrito(params: any) {
    return this.http.post(
      this.coreCentral + 'api/coreCentral/listarPorFiltroDistrito',
      params
    );
  }
  listarPorFiltroRegenteComunidad(params: any) {
    return this.http.post(
      this.base + 'api/planGeneralManejo/ListarPorFiltroDepartamento',
      params
    );
  }
  //

  eliminarDocumentoAdjunto(params: any) {
    return this.http.post(
      this.base + 'api/anexo/eliminarDocumentoAdjunto',
      params
    );
  }

  obtenerAdjuntos(params: any) {
    return this.http.post(this.base + 'api/anexo/obtenerAdjuntos', params);
  }
  validarAnexo(params: any) {
    return this.http.post(this.base + 'api/anexo/validarAnexo', params);
  }
  listarProcesoOferta(params: any) {

    return this.http.post(
      this.base + 'api/procesoOferta/listarProcesoOferta',
      params
    );
  }
  listarTipoProceso() {
    return this.http.get(this.base + 'api/tipoProceso/listarTipoProceso');
  }
  guardarProcesoOferta(params: any) {
    return this.http.post(
      this.base + 'api/procesoOferta/guardarProcesoOferta',
      params
    );
  }
  guardarProcesoOfertaDetalle(params: any) {
    return this.http.post(
      this.base + 'api/procesoOfertaDetalle/guardarProcesoOfertaDetalle',
      params
    );
  }
  guardarUnidadAprovechamiento(params: any) {
    return this.http.post(
      this.base + 'api/unidadAprovechamiento/guardarUnidadAprovechamiento',
      params
    );
  }
  listarEntidades() {
    return this.http.get(this.base + 'api/consultaEntidad/listarEntidades');
  }
  enviarConsulta(params: any) {
    return this.http.post(
      this.base + 'api/enviarConsulta/enviarConsulta',
      params
    );
  }
  generaranexo1(params: any) {
    return this.http.post(this.base + 'api/anexo/generaranexo1', params);
  }
  generaranexo2(params: any) {
    return this.http.post(this.base + 'api/anexo/generaranexo2', params);
  }
  generaranexo3(params: any) {
    return this.http.post(this.base + 'api/anexo/generaranexo3', params);
  }
  generaranexo4(params: any) {
    return this.http.post(this.base + 'api/anexo/generaranexo4', params);
  }
  descargarguianexo4() {
    return this.http.get(this.base + 'api/anexo/descargarAnexo4');
  }
  obtenerAnexo(params: any) {
    return this.http.post(this.base + 'api/anexo/obtenerAnexo', params);
  }
  guardaProcesoPostulacion(params: any) {
    return this.http.post(
      this.base + 'api/procesoPostulacion/guardaProcesoPostulacion',
      params
    );
  }
  adjuntarAnexo(params: any, formd: FormData) {
    return this.http.post(
      this.base + 'api/anexo/adjuntarAnexo?' + params,
      formd
    );
  }
  listarProcesoPostulacion(params: any) {
    return this.http.post(
      this.base + 'api/procesoPostulacion/listarProcesoPostulacion',
      params
    );
  }

  obtenerObjetivo2(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/obtenerObjetivo2',
      params
    );
  }
  insertarObjetivo2(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/insertarObjetivo2',
      params
    );
  }
  obtenerObjetivo1(params: any) {
    return this.http.post(
      this.base + 'api/PGMFAbreviado/obtenerObjetivo1',
      params
    );
  }
  listarUnidadAprovechamiento() {
    return this.http.get(this.base + 'api/unidadAprovechamiento/listar');
  }
  buscarUnidadAprovechamientobyIdProcesoOferta(id: any) {
    return this.http.get(
      this.base + `api/unidadAprovechamiento/buscarByIdProcesoOferta/${id}`
    );
  }
  getAprovechamiento(params: any) {
    return this.http.post(
      this.base +
        'api/evaluacionAmbiental/listarFiltrosEvalAmbientalAprovechamiento',
      params
    );
  }
  listarFiltrosEvalAmbientalActividad(params: any) {
    return this.http.post(
      this.base + 'api/evaluacionAmbiental/listarFiltrosEvalAmbientalActividad',
      params
    );
  }
  registrarAprovechamientoEvalAmbiental(params: any) {
    return this.http.post(
      this.base +
        'api/evaluacionAmbiental/registrarAprovechamientoEvalAmbiental',
      params
    );
  }
  registrarPlanGestionExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    idPlanManejo: number,
    codTipoAprovechamiento: string,
    tipoAprovechamiento: string,
    tipoNombreAprovechamiento: string,
    idUsuarioRegistro: number
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}api/evaluacionAmbiental/registrarPlanGestionExcel`;
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('idPlanManejo', String(idPlanManejo))
      .set('codTipoAprovechamiento', String(codTipoAprovechamiento))
      .set('tipoAprovechamiento', String(tipoAprovechamiento))
      .set('tipoNombreAprovechamiento', String(tipoNombreAprovechamiento))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));
    const body = new FormData();

    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(uri, body, {
      params,
      reportProgress: true,
    });
  }

  registrarPlanAccionExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    idPlanManejo: number,
    codTipoAprovechamiento: string,
    tipoAprovechamiento: string,
    tipoNombreAprovechamiento: string,
    idUsuarioRegistro: number
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}api/evaluacionAmbiental/registrarPlanAccionExcel`;
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('idPlanManejo', String(idPlanManejo))
      .set('codTipoAprovechamiento', String(codTipoAprovechamiento))
      .set('tipoAprovechamiento', String(tipoAprovechamiento))
      .set('tipoNombreAprovechamiento', String(tipoNombreAprovechamiento))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));
    const body = new FormData();

    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(uri, body, {
      params,
      reportProgress: true,
    });
  }

  registrarPlanVigilanciaExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    idPlanManejo: number,
    codTipoAprovechamiento: string,
    tipoAprovechamiento: string,
    tipoNombreAprovechamiento: string,
    idUsuarioRegistro: number
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}api/evaluacionAmbiental/registrarPlanVigilanciaExcel`;
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('idPlanManejo', String(idPlanManejo))
      .set('codTipoAprovechamiento', String(codTipoAprovechamiento))
      .set('tipoAprovechamiento', String(tipoAprovechamiento))
      .set('tipoNombreAprovechamiento', String(tipoNombreAprovechamiento))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));
    const body = new FormData();

    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(uri, body, {
      params,
      reportProgress: true,
    });
  }

  registrarPlanContingenciaExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    idPlanManejo: number,
    codTipoAprovechamiento: string,
    tipoAprovechamiento: string,
    tipoNombreAprovechamiento: string,
    idUsuarioRegistro: number
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}api/evaluacionAmbiental/registrarPlanContingenciaExcel`;
    const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('idPlanManejo', String(idPlanManejo))
      .set('codTipoAprovechamiento', String(codTipoAprovechamiento))
      .set('tipoAprovechamiento', String(tipoAprovechamiento))
      .set('tipoNombreAprovechamiento', String(tipoNombreAprovechamiento))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));
    const body = new FormData();

    body.append('file', archivo);
    return this.http.post<ResponseModel<number>>(uri, body, {
      params,
      reportProgress: true,
    });
  }

  registrarActividadEvalAmbiental(params: any) {
    return this.http.post(
      this.base + 'api/evaluacionAmbiental/registrarActividadEvalAmbiental',
      params
    );
  }

  registrarEvaluacionAmbiental(params: any) {
    return this.http.post(
      this.base + 'api/evaluacionAmbiental/registrarEvaluacionAmbiental',
      params
    );
  }

  registrarListEvaluacionAmbiental(params: any) {
    return this.http.post(
      this.base + 'api/evaluacionAmbiental/registrarListEvaluacionAmbiental',
      params
    );
  }

  registrarResumenDetalle(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/registrarResumenDetalle',
      params
    );
  }

  listarResumenDetalle(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/listarResumenDetalle',
      params
    );
  }

  listarResumenDetallePOAC(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/listarResumen_Detalle',
      params
    );
  }

  listarResumen_Detalle(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/listarResumen_Detalle',
      params
    );
  }

  getActividades(
    params: any
  ): Observable<ResponseModel<AprovechamientoResponse[]>> {
    return this.http.post<ResponseModel<AprovechamientoResponse[]>>(
      this.base +
        'api/evaluacionAmbiental/listarFiltrosEvalAmbientalAprovechamiento',
      params
    );
  }

  listarFactoresActividades(
    idPlanManejo: number
  ): Observable<ResponseModel<FactorActividadModel[]>> {
    const url = `${this.base}api/evaluacionAmbiental/listarEvaluacionAmbiental`;
    return this.http.post<ResponseModel<FactorActividadModel[]>>(url, {
      idPlanManejo,
    });
  }

  registrarFactoresActividades(
    body: FactorActividadModel[]
  ): Observable<ResponseModel<FactorActividadModel[]>> {
    const url = `${this.base}api/evaluacionAmbiental/registrarEvaluacionAmbiental`;
    return this.http.post<ResponseModel<FactorActividadModel[]>>(url, body);
  }

  //#region CENSO FORESTAL BEGIN

  registrarMasivoCensoForestalDetalle(formd: FormData) {
    return this.http.post(
      this.base + 'api/censoForestal/registrarMasivoCensoForestalDetalle',
      formd,
      {
        reportProgress: true,
      }
    );
  }

  listarCensoForestalDetalle(
    idPlanManejo: number
  ): Observable<ResponseModel<CensoForestalDetalle[]>> {
    const uri = `${this.base}api/censoForestal/listarCensoForestalDetalle`;

    const params = new HttpParams().set('IdPlanManejo', String(idPlanManejo));
    return this.http.post<ResponseModel<CensoForestalDetalle[]>>(uri, null, {
      params,
    });
  }

  listarCensoForestalDetalle2(
    item: any
  ): Observable<ResponseModel<CensoForestalDetalle[]>> {
    const uri = `${this.base}api/censoForestal/listaCensoForestalDetalle`;

    const params = new HttpParams()
      .set('idPlanManejo', String(item.idPlanManejo))
      .set('tipoCenso', String(item.tipoCenso))
      .set('tipoRecurso', String(item.tipoRecurso));
    return this.http.post<ResponseModel<CensoForestalDetalle[]>>(uri, null, {
      params,
    });
  }

  listarCensoForestalDetalleMaderable(
    item: any
  ): Observable<ResponseModel<CensoForestalDetalle[]>> {
    const uri = `${this.base}api/censoForestal/listaCensoForestalDetalle`;

    const params = new HttpParams()
      .set('idPlanManejo', String(item.idPlanManejo))
      .set('tipoRecurso', String(item.tipoRecurso));
    return this.http.post<ResponseModel<CensoForestalDetalle[]>>(uri, null, {
      params,
    });
  }
  //#endregion CENSO FORESTAL END

  registrarInfoGeneral(params: any) {
    return this.http.post(
      this.base + 'api/informacionGeneralPlanificacion/registrarInfoGeneral',
      params
    );
  }

  obtenerFauna(params: any) {
    return this.http.get(
      this.base + 'api/informacionBasica/obtenerFauna?' + params
    );
  }
  obtenerPoblacion() {
    return this.http.get(this.base + 'api/informacionBasica/obtenerPoblacion');
  }
  registroOrdenamientoProteccionDivAdm(params: any) {
    return this.http.post(
      this.base +
        'api/ordenamientoProteccion/RegistrarOrdenamientoProteccionDivAdm',
      params
    );
  }
  obtenerOrdenamientoProteccionDivAdm(params: any) {
    return this.http.post(
      this.base +
        'api/ordenamientoProteccion/ObtenerOrdenamientoProteccionDivAdm',
      params
    );
  }
  listarResumenActividadPO(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/listarResumenActividadPO',
      params
    );
  }
  registrarResumenActividadPO(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/registrarResumenActividadPO',
      params
    );
  }

  eliminarResumenDetalle(params: any) {
    return this.http.post(
      this.base + 'api/resumenActividad/eliminarResumenDetalle',
      params
    );
  }

  listarActividadSilvicultural(params: any) {
    return this.http.get(
      this.base +
        'api/actividadSilvicultural/listarActividadSilvicultural?' +
        params
    );
  }
  registrarActividadSilvicultural(params: any) {
    return this.http.post(
      this.base + 'api/actividadSilvicultural/registrarActividadSilvicultural',
      params
    );
  }
  listarOrdenamientoProteccion(params: any) {
    return this.http.post(
      this.base + 'api/ordenamientoProteccion/ListarOrdenamientoProteccion',
      params
    );
  }
  registrarOrdenamientoProteccion(params: any) {
    return this.http.post(
      this.base + 'api/ordenamientoProteccion/RegistrarOrdenamientoProteccion',
      params
    );
  }

  actualizarOrdenamientoProteccion(params: any) {
    return this.http.post(
      this.base + 'api/ordenamientoProteccion/ActualizarOrdenamientoProteccion',
      params
    );
  }

  actualizarOrdenamientoProteccionDetalle(params: any) {
    return this.http.post(
      this.base +
        'api/ordenamientoProteccion/ActualizarOrdenamientoProteccionDetalle',
      params
    );
  }

  listarOrdenamientoProteccionPorFiltro(params: any) {
    return this.http.get(
      this.base +
        'api/ordenamientoProteccion/ListarOrdenamientoProteccionPorFiltro?idPlanManejo=' +
        params.idPlanManejo +
        '&codTipoOrdenamiento=' +
        params.codTipoOrdenamiento
    );
  }

  listarActividadSilviculturalPgmf(params: any) {
    return this.http.get(
      this.base +
        'api/actividadSilvicultural/listarActividadSilviculturalPgmf?idPlanManejo=' +
        params.idPlanManejo +
        '&idTipo=' +
        params.idTipo
    );
  }

  configurarActividadSilviculturalPgmf(params: any) {
    return this.http.post(
      this.base +
        'api/actividadSilvicultural/configurarActividadSilviculturalPgmf',
      params
    );
  }

  eliminarActividadSilviculturalPgmf(params: any) {
    return this.http.post(
      this.base +
        'api/actividadSilvicultural/eliminarActividadSilviculturalPgmf',
      params
    );
  }

  actualizarActividadSilviculturalPgmf(params: any) {
    return this.http.post(
      this.base +
        'api/actividadSilvicultural/actualizarActividadSilviculturalPgmf',
      params
    );
  }

  insertarPlanManejoForestal(params: any) {
    return this.http.post(
      this.base + 'api/planGeneralManejoForestal/insertarPlanManejoForestal',
      params
    );
  }

  obtenerContrato(params: any) {
    return this.http.post(
      this.base + 'api/planGeneralManejoForestal/obtenerContrato',
      params
    );
  }

  listarComboContrato(params: any) {
    return this.http.post(
      this.base + 'api/contrato/listarComboContrato',
      params
    );
  }

  obtenerContratoPoligono(idsContrato: any) {
    return this.http.get(
      this.base + `api/contrato/listarContratoGeometria/${idsContrato}`
    );
  }

  obtenerPlanTrabajoContrato(params: any) {
    return this.http.post(
      this.base + 'api/planManejo/obtenerPlanManejoContrato',
      params
    );
  }

  listarContratosVigentes(
    params: any
  ): Observable<ResponseModel<PlanManejoContrato[]>> {
    return this.http.get<ResponseModel<PlanManejoContrato[]>>(
      this.base +
        'api/planGeneralManejoForestal/listarContratosVigentes/' +
        params
    );
  }

  enviarEvaluacion(params: any) {
    return this.http.post(
      this.base + 'api/planGeneralManejoForestal/enviarEvaluacion',
      params
    );
  }

  eliminarEvaluacionAmbientalAprovechamiento(params: any) {
    return this.http.post(
      this.base +
        'api/evaluacionAmbiental/eliminarEvaluacionAmbientalAprovechamiento',
      params
    );
  }

  eliminarEvaluacionAmbientalAprovechamientoList(params: any) {
    return this.http.post(
      this.base +
        'api/evaluacionAmbiental/eliminarEvaluacionAmbientalAprovechamientoList',
      params
    );
  }

  eliminarEvaluacionAmbientalActividad(params: any) {
    return this.http.post(
      this.base +
        'api/evaluacionAmbiental/eliminarEvaluacionAmbientalActividad',
      params
    );
  }

  listarDivisionAdministrativa(params: any) {
    return this.http.post(
      this.base +
        'api/planManejo/listarDivisionAdministrativa',
      params
    );
  }

}
