
import { Injectable } from '@angular/core';
import { UsuarioModel } from 'src/app/model/seguridad/usuario';


@Injectable({ providedIn: 'root' })
export class UsuarioService {

  private KEY = 'usuario';

  get usuario(): UsuarioModel {
    return JSON.parse(`${localStorage.getItem(this.KEY)}`) as UsuarioModel;
  }

  get idUsuario(): number {
    return this.usuario.idusuario;
  }

  get idPerfilUsuario(): number {
    return this.usuario.perfiles[0].idPerfil;
  }

  get nroDocumento(): string {
    return this.usuario.numeroDocumento;
  }

  get idtipoDocumento(): number {
    return this.usuario.idtipoDocumento;
  }


}
