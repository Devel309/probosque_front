import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class RentabilidadManejoForestalService {
    base = environment.urlProcesos;
    constructor(private http: HttpClient, private config: ConfigService) { }


    comboRubroRentabilidadForestal(params: any) {
        return this.http.post(this.base + 'api/rentabilidadManejoForestal/comboRubroRentabilidadForestal', params);
    }

    registrarRentabilidadManejoForestal(params: any) {
        return this.http.post(this.base + 'api/rentabilidadManejoForestal/registrarRentabilidadManejoForestal', params);
    }

    eliminarRentabilidadManejoForestal(params: any) {
        return this.http.post(this.base + 'api/rentabilidadManejoForestal/eliminarRentabilidadManejoForestal', params);
    }


    listarRentabilidadManejoForestal(params: any) {
        return this.http.post(this.base + 'api/rentabilidadManejoForestal/listarRentabilidadManejoForestal', params);
    }

    descargarFormato() {
        return this.http.get(this.base + 'api/rentabilidadManejoForestal/descargarFormato');
    }

    registrarProgramaInversionesExcel(archivo: any, parametro: any) {
        //return this.http.post(this.base + 'api/rentabilidadManejoForestal/registrarProgramaInversionesExcel', params);

        const uri = `${this.base}api/rentabilidadManejoForestal/registrarProgramaInversionesExcel`;
        const params = new HttpParams()
          .set('codigoRentabilidad', String(parametro.codigoRentabilidad))
          .set('idPlanManejo', String(parametro.idPlanManejo))
          .set('idUsuarioRegistro', String(parametro.idUsuarioRegistro));
        const body = new FormData();
        
        body.append('file', archivo);
        return this.http.post(uri, body, {
          params,
          reportProgress: true,
        });



    }
}


