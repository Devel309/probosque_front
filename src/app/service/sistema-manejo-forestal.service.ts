import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { environment as env } from "@env/environment";
import {
  ResponseModel,
  SistemaManejoForestal,
  SistemaManejoForestalDetalle,
} from "@models";

@Injectable({ providedIn: "root" })
export class SistemaManejoForestalService {
  base = `${env.urlProcesos}api/sistemaManejoForestal`;

  constructor(private http: HttpClient) {}

  obtener(
    idPlanManejo: number,
    codigoProceso: any = null
  ): Observable<ResponseModel<SistemaManejoForestal>> {
    const params = new HttpParams()
      .set("idPlanManejo", String(idPlanManejo))
      .set("codigoProceso", codigoProceso);

    const uri = `${this.base}`;
    return this.http.get<ResponseModel<SistemaManejoForestal>>(uri, { params });
  }

  guardar(
    body: SistemaManejoForestal
  ): Observable<ResponseModel<SistemaManejoForestal>> {
    const uri = `${this.base}`;
    return this.http.post<ResponseModel<SistemaManejoForestal>>(uri, body);
  }

  guardarDetalle(
    body: SistemaManejoForestalDetalle
  ): Observable<ResponseModel<SistemaManejoForestalDetalle>> {
    const uri = `${this.base}/detalle`;
    return this.http.post<ResponseModel<SistemaManejoForestalDetalle>>(
      uri,
      body
    );
  }

  eliminarDetalle(
    idDetalle: number | null,
    idUsuario: number
  ): Observable<ResponseModel<number>> {
    const uri = `${this.base}/detalle/eliminar`;

    const params = new HttpParams()
      .set("idSistemaManejoForestalDetalle", String(idDetalle))
      .set("idUsuarioElimina", String(idUsuario));
    return this.http.post<ResponseModel<number>>(uri, null, { params });
  }


  guardarMedidasProteccion(body: any): Observable<ResponseModel<any>> {
    const uri = `${this.base}`;
    return this.http.post<ResponseModel<any>>(uri, body);
  }

  sistemaManejoForestal(body: any) {
    const params = new HttpParams()
      .set("idPlanManejo", String(body.idPlanManejo))
      .set("codigoProceso", body.codigoProceso);

    const uri = `${this.base}`;
    return this.http.get<ResponseModel<SistemaManejoForestal>>(uri, { params });
  }
  guardarSistemaManejoForestal(params: any) {
    return this.http.post(this.base, params);
  }
}
