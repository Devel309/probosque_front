
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ParticipacionComunalService  {


  base = environment.urlProcesos;
  constructor(private http : HttpClient, private config : ConfigService) { }


  registrarParticipacionComunal(params : any){
    return this.http.post(this.base +'api/participacionComunal/registrarParticipacionComunal', params);
  }
  eliminarParticipacionComunal(params : any){
    return this.http.post(this.base +'api/participacionComunal/eliminarParticipacionComunal', params);
  }
  obtenerParticipacionComunal(params : any){
    return this.http.post(this.base +'api/participacionComunal/obtenerParticipacionComunal', params);
  }
  listarPorFiltroParticipacionComunal(params : any){
    return this.http.post(this.base +'api/participacionComunal/listarPorFiltroParticipacionComunal', params);
  }

}

