import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

@Injectable({ providedIn: 'root' })
export class PfcrDatosOtorgamientoService {
  base = `${environment.urlProcesos}`;

  constructor(private http: HttpClient) {}

  listarInformacionOtorgamiento(params: any) {
    return this.http.post(
      this.base + 'api/otorgamiento/listarOtorgamiento',
      params
    );
  }

  registrarInformacionOtorgamiento(params: any) {
    return this.http.post(
      this.base + 'api/otorgamiento/registrarOtorgamiento',
      params
    );
  }

  registrarOtorgamientoArchivo(params: any) {
    return this.http.post(
      this.base + 'api/otorgamiento/registrarOtorgamientoArchivo',
      params
    );
  }

  listarOtorgamientoArchivo(params: any) {
    return this.http.post(
      this.base + 'api/otorgamiento/listarOtorgamientoArchivo',
      params
    );
  }

  eliminarOtorgamientoArchivo(params: any) {
    return this.http.post(
      this.base + 'api/otorgamiento/eliminarOtorgamientoArchivo',
      params
    );
  }
}
