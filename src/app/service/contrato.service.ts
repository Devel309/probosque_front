import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseModel } from '@models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContratoService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }

  listarGeneracionContrato(params: any) {
    return this.http.post<ResponseModel<any>>(this.base + 'api/contrato/listarGeneracionContrato', params);
  };

  listarContrato = (params: any) => {
    return this.http.post(this.base + 'api/contrato/listarContrato', params);
  };

  registrarContrato = (params: any) => {
    return this.http.post(this.base + 'api/contrato/registrarContrato', params);
  };

  obtenerPostulacion = (params: any) => {
    return this.http.post(this.base + 'api/contrato/obtenerPostulacion', params);
  };

  adjuntarArchivosContrato = (params: any) => {
    return this.http.post(
      this.base + 'api/contrato/adjuntarArchivosContrato',
      params
    );
  };

  obtenerContrato = (idContrato: number) => {
    return this.http.get(
      this.base + `api/contrato/obtenerContrato/${idContrato}`
    );
  };

  obtenerArchivosContrato = (idContrato: number) => {
    return this.http.get(
      this.base + `api/contrato/obtenerArchivosContrato/${idContrato}`
    );
  };

  descargarContrato = (idContrato: number) => {
    return this.http.get(
      this.base + `api/contrato/descargarContrato/${idContrato}`
    );
  };

  actualizarContrato = (params: any) => {
    return this.http.post(
      this.base + 'api/contrato/actualizarContrato',
      params
    );
  };

  actualizarStatusContrato = (params: any) => {
    return this.http.post(
      this.base + 'api/contrato/actualizarStatusContrato',
      params
    );
  };

  obtenerContratoGeneral = (params: any) => {
    return this.http.post(
      this.base + `api/contrato/obtenerContratoGeneral`, params
    );
  };

  generarCodigoContrato = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + `api/contrato/generarCodigoContrato`, params);
  };

  descargarContratoPersonaJuridica = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + `api/contrato/descargarContratoPersonaJuridica`, params);
  };

  listarContratoArchivo = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + `api/contrato/listarContratoArchivo`, params);
  };

  obtenerArchivoContrato = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + `api/contrato/obtenerArchivoContrato`, params);
  };

  actualizarEstadoContrato = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + `api/contrato/actualizarEstadoContrato`, params);
  };

  registrarResolucion = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + 'api/contrato/registrarResolucion', params);
  };

  notificarResolucion = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + 'api/contrato/notificarResolucion', params);
  };

  obtenerResolucion = (idResolucion: any) => {
    return this.http.get<ResponseModel<any>>(this.base + 'api/contrato/obtenerResolucion/'+ idResolucion);
  };

  actualizarGeometria = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + 'api/contrato/actualizarGeometria/', params);
  };

  actualizarEstadoRemitido = (params: any) => {
    return this.http.post<ResponseModel<any>>(this.base + 'api/contrato/actualizarEstadoRemitido/', params);
  };
  
  descargarModeloConcesionesMaderables(idPlanManejo:any) {
    return this.http.get<ResponseModel<any>>(this.base + 'api/contrato/descargarModeloConcesionesMaderables/'+ idPlanManejo);
  }
}
