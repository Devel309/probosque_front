import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ResponseModel } from '@models';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ActividadesAprovechamientoService {
  base = `${environment.urlProcesos}api/actividadAprovechamiento/`;

  constructor(private http: HttpClient) {}

  listarActvidadAprovechamiento(params: any) {
    return this.http.post(`${this.base}listarActvidadAprovechamiento`, params);
  }

  registrarActvidadAprovechamiento(params: any) {
    return this.http.post(
      `${this.base}registrarActvidadAprovechamiento`,
      params
    );
  }
  eliminarActvidadAprovechamiento(params: any) {
    return this.http.post(
      `${this.base}eliminarActvidadAprovechamiento`,
      params
    );
  }
  eliminarActvidadAprovechamientoDetSub(params: any) {
    return this.http.post(
      `${this.base}eliminarActvidadAprovechamientoDetSub`,
      params
    );
  }

  eliminarActvidadAprovechamientoDet(params: any) {
    return this.http.post(
      `${this.base}eliminarActvidadAprovechamientoDet`,
      params
    );
  }

  listarResultadosForestalesMaderables(data: any) {
    const params = new HttpParams()
      .set('idPlanDeManejo', data.idPlanDeManejo)
      .set('tipoPlan', data.tipoPlan);

    return this.http.post(
      `${environment.urlProcesos}api/censoForestal/actividadesDeAprovechamiento`,
      null,
      { params }
    );
  }



  listarSincronizacionEspeciesMaderables(data: any) {
    const params = new HttpParams()
      .set('idPlanDeManejo', data.idPlanDeManejo)
      .set('tipoPlan', data.tipoPlan);

    return this.http.post(
      `${environment.urlProcesos}api/censoForestal/ListaEspeciesInventariadasMaderablesObtener`,
      null,
      { params }
    );
  }



  registrarCensoComercialEspecieExcel(
    archivo: File,
    nombreHoja: string,
    numeroFila: number,
    numeroColumna: number,
    codActvAprove: string,
    codSubActvAprove: string,
    codActvAproveDet: string,
    idActvAprove: number,
    idPlanManejo: number,
    idUsuarioRegistro: number): Observable<ResponseModel<number>> {
      const params = new HttpParams()
      .set('nombreHoja', String(nombreHoja))
      .set('numeroFila', String(numeroFila))
      .set('numeroColumna', String(numeroColumna))
      .set('codActvAprove', String(codActvAprove))
      .set('codSubActvAprove', String(codSubActvAprove))
      .set('codActvAproveDet', String(codActvAproveDet))
      .set('idActvAprove', String(idActvAprove))
      .set('idPlanManejo', String(idPlanManejo))
      .set('idUsuarioRegistro', String(idUsuarioRegistro));

    const body = new FormData();
    body.append('file', archivo);

    return this.http.post<ResponseModel<number>>(this.base + 'registrarCensoComercialEspecieExcel', body, { params, reportProgress: true, });
  }
  
  obtenerTotalVolumenCorta(params: any) {
    return this.http.post(`${this.base}ObtenerTotalVolumenCorta`, params);
  }
  
  obtenerTotalAprovechamientoNoMaderable(params: any) {
    return this.http.post(`${this.base}ObtenerTotalAprovechamientoNoMaderable`, params);
  }

}
