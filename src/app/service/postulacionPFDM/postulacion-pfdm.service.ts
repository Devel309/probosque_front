import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PostulacionPFDMService {
  private base: string = '';
  constructor(private http: HttpClient) {
    this.base = environment.urlProcesos;
  }
  adjuntarArchivosPostulacionPFDM (parametro:any) {
    return this.http.post(this.base + 'api/postulacionPFDM/adjuntarArchivosPostulacionPFDM/', parametro);
  };
  obtenerAnexo1PFDM (parametro:any) {
    return this.http.post(this.base + 'api/postulacionPFDM/obtenerAnexo1PFDM/', parametro);
  };
  guardarAnexo1(parametro:any){
    return this.http.post(this.base + 'api/postulacionPFDM/guardaPostulacion', parametro);
  }
  registrarArchivoDetalle(parametro:any){
    return this.http.post(this.base + 'api/postulacionPFDM/registrarArchivo', parametro);
  }
  obtenerArchivoDetalle(idPlanManejo:Number,codigoTipoPGMF:string){
    return this.http.get(`${this.base}api/postulacionPFDM/listarDetalleArchivo/${idPlanManejo}/${codigoTipoPGMF}`);
  }
  eliminarArchivoDetalle(idArchivo:Number,idUsuario:Number){
    return this.http.get(`${this.base}api/postulacionPFDM/eliminarDetalleArchivo/${idArchivo}/${idUsuario}`);
  }
  eliminarPlanManejoArchivo(params: any){
    return this.http.post(`${this.base}api/planManejo/eliminarPlanManejoArchivo`, params);
  }
}