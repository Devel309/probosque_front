import { HttpClient,HttpClientModule, HttpEvent, HttpHandler, HttpHeaders, HttpRequest,HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PlanManejoCcnnService  {


  base = environment.urlProcesos; //'http://10.6.1.162/mcsniffs-rest/';
  constructor(private http : HttpClient, private config : ConfigService) { }


  registrarResumenEjecutivoEscalaAlta(params : any,formd: FormData){
    return this.http.post(this.base +'api/planGeneralManejo/RegistrarResumenEjecutivoEscalaAlta?'+ params,formd);
  }
  obtenerResumenEjecutivoEscalaAlta(params : any){
    return this.http.post(this.base +'api/planGeneralManejo/ObtenerResumenEjecutivoEscalaAlta', params);
  }
  actualizarResumenEjecutivoEscalaAlta(params : any,formd: FormData){
    return this.http.post(this.base +'api/planGeneralManejo/ActualizarResumenEjecutivoEscalaAlta?'+ params,formd);
  }
  actualizarDuracionResumenEjecutivo(params : any){
    return this.http.post(this.base +'api/planGeneralManejo/ActualizarDuracionResumenEjecutivo', params);
  }
  obtenerSolicitudAprovechamientoCcnn(params : any){
    return this.http.post(this.base +'api/obtenerSolicitudAprovechamientoCcnn/obtenerSolicitudAprovechamientoCcnn', params);
  }
  registrarObjetivoManejo(params : any){
    return this.http.post(this.base +'api/objetivoManejo/registrarObjetivoManejo', params);
  }
  actualizarrObjetivoManejo(params : any){
    return this.http.post(this.base +'api/objetivoManejo/actualizarObjetivoManejo', params);
  }
  obtenerObjetivoManejo(params : any){
    return this.http.post(this.base +'api/objetivoManejo/obtenerObjetivoManejo', params);
  }
  registrarObjetivoEspecificoManejo(params : any){
    return this.http.post(this.base +'api/objetivoManejo/registrarObjetivoEspecificoManejo', params);
  }
  actualizarObjetivoEspecificoManejo(params : any){
    return this.http.post(this.base +'api/objetivoManejo/actualizarObjetivoEspecificoManejo', params);
  }
  obtenerObjetivoEspecificoManejo(params : any){
    return this.http.post(this.base +'api/objetivoManejo/obtenerObjetivoEspecificoManejo', params);
  }
  comboPorFiltroObjetivoEspecifico(params : any){
    return this.http.post(this.base +'api/objetivoManejo/comboPorFiltroObjetivoEspecifico', params);
  }
  obtenerPlanManejo(params : any){
    return this.http.post(this.base +'api/planGeneralManejo/obtenerPlanManejo', params);
  }

  actualizarAspectoComplementarioResumenEjecutivo(params : any){
    return this.http.post(this.base +'api/planGeneralManejo/actualizarAspectoComplementarioPlanManejo', params);
  }


  /********coronograma de actividdes */
  EliminarCronogramaActividades(params : any){
    return this.http.post(this.base +'api/CronogramaActividades/EliminarCronogramaActividades', params);
  }
  ListarCronogramaActividades(params : any){
    return this.http.post(this.base +'api/CronogramaActividades/ListarCronogramaActividades', params);
  }
  RegistrarCronogramaActividades(params : any){
    return this.http.post(this.base +'api/CronogramaActividades/RegistrarCronogramaActividades', params);
  }



/*
{
  "codigo": "TACTECO"
    64	TACTECO
    65	TINFRA
    66	TIPACTIV
}
*/
  listarPorFiltroParametro(params : any){
    return this.http.post(this.base +'api/generico/listarPorFiltroParametro', params);
  }

  
}
