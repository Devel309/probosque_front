import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { distinctUntilChanged, map } from "rxjs/operators";

import { ButtonsCreateStore } from "./buttons-create.store";

@Injectable({
  providedIn: "root",
})
export class ButtonsCreateQuery {
  constructor(private store: ButtonsCreateStore) {}

  selectLoading(): Observable<boolean> {
    return this.store.state.pipe(
      map((state) => state.isLoading),
      distinctUntilChanged()
    );
  }

  selectSubmitting(): Observable<boolean> {
    return this.store.state.pipe(
      map((state) => state.submitting),
      distinctUntilChanged()
    );
  }

  selectError(): Observable<any> {
    return this.store.state.pipe(
      map((state) => state.error),
      distinctUntilChanged()
    );
  }
}
