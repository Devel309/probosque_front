if (!Array.prototype.groupBy) {
    Array.prototype.groupBy = function (callback) {
        var items = this.reduce(function (store, item) {
            return store[callback(item)] ? store[callback(item)].push(item) : store[callback(item)] = [item], store;
        }, {});
        var items2 = [];
        for (var i in items)
            items2.push({ key: i, value: items[i] });
        return items2;
    };
    Object.defineProperty(Array.prototype, "groupBy", { enumerable: false });
}
HTMLElement.prototype._addFromHTML = function (position, html) {
    "use strict";

    var self = this;
    var container = self.ownerDocument.createElementNS("http://www.w3.org/1999/xhtml", "_");
    var parent = self.parentNode;
    var node;
    container.innerHTML = html;
    switch (position.toLowerCase()) {
        case "beforebegin":
            while ((node = container.firstChild)) {
                return parent.insertBefore(node, self);
            }
            break;
        case "afterbegin":
            var firstChild = self.firstChild;
            while ((node = container.lastChild)) {
                return firstChild = self.insertBefore(node, firstChild);
            }
            break;
        case "beforeend":
            // if(container.firstChild=== undefined ||
            // container.firstChild === null)
            // return self.appendChild(node);
            while ((node = container.firstChild)) {
                return self.appendChild(node);
            }
            break;
        case "afterend":
            var nextSibling = self.nextSibling;
            while ((node = container.lastChild)) {
                return nextSibling = parent.insertBefore(node, nextSibling);
            }
            break;
    }
};
HTMLElement.prototype.prependHTML = function (value) {
    return this._addFromHTML('afterbegin', value);
}
HTMLElement.prototype.appendHTML = function (value) {
    return this._addFromHTML('beforeend', value);
};
HTMLElement.prototype.insertAfterHTML = function (value) {
    return this._addFromHTML('afterend', value);
};
HTMLElement.prototype.insertBeforeHTML = function (value) {
    return this._addFromHTML('beforebegin', value);
};
HTMLElement.prototype.empty = function () {
    this.innerHTML = '';
    return this;
};
HTMLElement.prototype.show2 = function () {
    this.style.display = 'block';
};
HTMLElement.prototype.hide2 = function (value) {
    this.style.display = 'none';
};
HTMLElement.prototype.appendHTML = function (value) {
    return this._addFromHTML('beforeend', value);
};