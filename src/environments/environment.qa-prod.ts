export const environment = {
  production: false,
  urlbase:"https://sniffs.serfor.gob.pe/",
  urlSeguridad:"seguridad-rest/",
  urlProcesos:"proceso-rest/",
  urlSerfor: 'http://ws.serfor.gob.pe',
  urlApiGeoforestal:'https://sniffs.serfor.gob.pe/',
  urlServiciosExternos: 'extserv-rest/',
  urlCoreCentral:"corecentral-rest/",
  varAssets:"mcsqa",
  varUrl:"/mcsqa"
};
