export const environment = {
    production: false,
    urlbase:"https://sniffs.serfor.gob.pe/",//URL DEL AMBIENTE:https://sniffs.serfor.gob.pe/
    urlSeguridad:"seguridad-rest/",//CONTECTO SEGURIDAD: seguridad-rest/
    urlProcesos:"proceso-rest/",//CONTECTO SEGURIDAD: proceso-rest/
    urlServiciosExternos: 'extserv-rest/', //URL DE LOS SERVICIOS DE SERFOR: extserv-rest/
    urlSerfor: 'http://ws.serfor.gob.pe', //URL DE LOS SERVICIOS DE SERFOR:X
    urlApiGeoforestal:'https://sniffs.serfor.gob.pe/',//URL DE LOS SERVICIOS DEL GEOESPACIAL: https://sniffs.serfor.gob.pe/
    urlCoreCentral:"mcsniffsentcore-rest/",
};