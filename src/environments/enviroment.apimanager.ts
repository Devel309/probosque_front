export const environment = {
  production: false,
  urlbase:"https://serforlimqab00.serfor.gob.pe/",
  urlSeguridad:"seguridad-rest/",
  urlProcesos:"proceso-rest/",
  urlSerfor: 'http://ws.serfor.gob.pe',
  urlApiGeoforestal:'https://serforlimqab00.serfor.gob.pe/',
  urlServiciosExternos: 'extserv-rest/',
  urlCoreCentral:"corecentral-rest/",
  varAssets:"mcsqa",
  varUrl:"/mcsqa"
};

