export const environment = {
  production: false,
  urlbase:"http://10.6.1.162/",//URL DEL AMBIENTE:https://sniffs.serfor.gob.pe/
  urlSeguridad:"mcsniffsseguridad-rest/",//CONTECTO SEGURIDAD: seguridad-rest/
  urlProcesos:"mcsniffs-rest/",//DEV mcsniffs-rest-dev
  urlServiciosExternos: 'mcsniffsextserv-rest/', //URL DE LOS SERVICIOS DE SERFOR: extserv-rest/
  urlSerfor: 'http://ws.serfor.gob.pe', //URL DE LOS SERVICIOS DE SERFOR:X
  urlApiGeoforestal:'https://sniffs.serfor.gob.pe/',
  urlCoreCentral:"mcsniffsentcore-rest/",//prueba,
  varAssets:"",
  varUrl:""
};
//OTI SERFOR
//LUIS RAMIREZ = WIDFLY
//MARTIN CHOSE =NGINX
