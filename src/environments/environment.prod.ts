export const environment = {
  production: true,
  urlbase:"https://sniffs.serfor.gob.pe/",
  urlSeguridad:"seguridad-rest/1.0/",
  urlProcesos:"proceso-rest/1.0/",
  urlSerfor: 'http://ws.serfor.gob.pe',
  urlApiGeoforestal:'https://sniffs.serfor.gob.pe/',
  urlServiciosExternos: 'extserv-rest/1.0/',
  urlCoreCentral:"corecentral-rest/1.0/",
  varAssets:"mcsqa",
  varUrl:"/mcsqa"
};
//OTI SERFOR
//LUIS RAMIREZ = WIDFLY
//MARTIN CHOSE =NGINX
