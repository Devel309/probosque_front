
<a name="0.0.3"></a>
## [0.0.3](https://dev.azure.com/proyecto-mcsniffs/mcsniffs-web/_git/mcsniffs-web/compare/0.0.2...0.0.3)

> 2021-10-01

### Docs

* Update changelog.

### Feat

* [3.1.3 HU04] Se enlaza btn eliminar y eliminar todas las capas.
*  [MAPA]  Se agrega funcionalidad para eliminar todas las capas, eliminar capa individual, eliminar capa por grupo. funcionalidad descargar, capa individual, capa por grupo.
* [MAPA] se agrega botón para eliminar todas las capas.
* [3.1.3 HU03] se obtiene flora y fauna
* [3.1.3 HU03] consumo de servicio listar rios, editar, eliminar Cuerpos de Agua
* [3.1.3 HU04] Se agrega funciones mapa, pasando layers view y archivo base64, para setear capas.
* [3.1.3 HU3] consumo de servicio para listado medio trasporte, editar, eliminar
* Se agrega color capa a la lista, para idientificar las capas.
* [3.1.3 HU04] Se agrega funcionalidad para agregar capa, descargar shapefile desde la lista de Ordenamiento interno.
* [3.1.3 HU04] Archivo zip, se agrega valiación al descargar zip.
* [3.1.3 HU04] Mapa: Se agrega función para calcular área., se agrega parametro grupoId y color, para crear capa.
* [3.1.3 HU04] Mapa: Se agregan funciones para descargar shapefile por grupo, SE pasa parámetro de grupoId y color para generar las capas.
* [3.1.3 HU04] Se crean funciones para normalizar nombres.(remover ;)
* Se agrega detección cargar varias veces el mismo archivo: UploadButtonComponent
* Se agrega propiedad de entrada style a componente UploadButtonComponent
* [3.1.3 HU03] sonsumo de servicio para obtener ubigeo
* [3.1.3 HU04] Se agrega validación de archivo repetido, al cargar shapefile.
* [3.1.3 - HU10 ] consumo de servicio para el consolidado pmfi
* [3.1.3 - HU02] se agrega eliminar archivo
* [3.1.3 HU04] Se implementa lógica de "actualizar" archivo, relación archivo- planManejo, (se elimina internamente para modificr archivo.)
* [3.1.3 HU04] Se crea modelo PlanManejoArchivo, se agrega servicio eliminar relacion archivo - plan manejo
* [N3.1.3.-HU03 crud para tablas Accesibilidad 1 y Accesibilidad 2 dentro de Informacion de Area]
* [N3.1.3.-HU03 avance integracion Informacion del area]
* [General] se cambia value en compoente carga de archivos
* login usuario
* [merge N3.1.3.-HU05 integracion servicio recurso forestal y censo forestal]
* [3.1.3  HU10] se agregaron validaciones a botones
* [3.1.9] carga, descarga, elimina de anexo 1
* [3.1.9] descarga de archivos
* Se crea servicio mapa.
* [3.1.9 ] validaciones
* [3.1.9 HU12] eliminar archivos
* [merge N3.1.3.-HU05 avance integracion, listado de recurso forestal]
* [3.1.3 Anexos] eliminar archivos
* [3.1.9 ] se agrega idPersonaComunidad
* [hu00] refactorizacion
* [3.1.3 - HU08] se optiene la vigencia y se pinta en tabla de cronograma
* [3.1.3 - HU02] carga de archivo
* [3.1.3 - HU02] consumo de servicio guardar
* [3.1.3 HU04] Se agrega función  zip/unzip componente mapa.
* [3.1.3 HU04] función para descargar blob.
* [3.1.3 HU04] Se agrega librería para  crear archivos zip y extraer infromación de un archivo zip.
* [3.1.3 HU04]Se crea modelos zip
* [3.1.3 HU04] Se agrega estilo a lista de capas.
* [3.1.3 HU04] Se agrega capas al mapa, y a la tabla de capas.
* [3.1.3 HU04] Avance componente mapa, cargar y leer shapefile.
* [3.1.3 general] se agregan botones Regresar - Siguiente
* [N3.1.3 HU06] Se crea servicio emisor de eventos para compartir metodos entre componentes.
* [HU00] mejoras en validar condiciones
* [N3.1.3 HU06] Se realiza integracion de boton guardar y elimnado de registro
* [3.1.3 Ordenamiento Interno] Se avanza componente mapa, Se está creando componente mapa genérico.
* [N3.1.3 HU06] Se realizan cambios en el frontend para guardar todas las listas.
* [3.1.3 HU08 ] avance
* [3.1.3 HU08 ] avance cronograma
* [3.1.3 HU02 ] consumo de servicios para objetivos, consumo de servicio para obtener lista de contratos vigentes
* [N3.1.3.-HU07 integracion con el servicio listar impacto ambiental pmfi]
* [3.1.3 HU02] se crean servicios para objetivos
* [3.1.3 HU01] Se implementa paginación y ordenamiento Listar planes pmfi.
* [3.1.3 HU01] Se agrega modelos para paginar resultados.
* [3.1.3 HU01] Se agrega parámetro de entrada idPlanManejo a los tabs de formulación PMFI.
* [3.1.3 HU01] Se crea bandeja PMFI
* [3.1.3 - HU08]  columnas dinamicas en cronograma
* [N3.1.3 HU06] Se completa funcionalidad de modal y lista de aprovechamiento y labores silviculturales
* [3.1.3 - HU10] se agrego listado
* [3.1.3] objetivos
* [3.1.9 Anexos] Se agrega spinners de carga descargar anexos 2 y 2.
* [3.19 - HU06, HU07, HU08, HU09, HU10] validacion de campos y limite de tamaño
* [Consolidado DEMA] Descargar archivo consolidado.
* [3.1.9 HU11] generar anexos
* [3.1.9 HU11] carga de archivos
* generar dema
* [3.1.9 HU02] Se agrega usuario registra información general.
* [N3.1.3 HU06] Se realizan componentes para manejo aprovechamiento de labores
* [3.1.9 HU11] recuperar archivo
* [3.1.9 HU11] carga y desarga de archivos
* [3.1.9 HU11 - HU12] se agrega carga de archivo
* [3.1.9 HU02] Se termina validaciones Formulario Información General
* [3.1.9 HU02] Se cambia servicio core central departamento provincia distrito.
* [3.1.9 HU02] Se cambia base ubigeo  a core central.
* [3.1.9 HU02] se agregaron cuencas y subCuencas
* [3.1.9 HU11]  se cambio nombre de componente
* [3.1.9 HU11]  se conecto servicio para listado anexo2 y anexo3
* [3.1.9 HU03]  se agregan campos a las tablas
* [3.1.9 HU02] Se agrega toast mensaje registrar/actualziar InformacionGeneralDEMA.
* [3.1.9 HU02] Se consume servicio aprovechamiento, se implementa lógica setear valores aprovechamiento, se termina informacionGeneralDema obtener,registrar, actualizar.
* [N3.1.9 HU06] Se realiza implementacion de eliminado de labor silvicultural.
* [3.1.9 HU02] Se avanza obtener, registrar, actualizar Información General DEMA.
* [3.1.9 HU02] Se agrega nuevos campos modelo Informacion General DEMA, se crea constante detalle Info General.
* [3.1.9] Avance Formulario Información General
* [N3.1.9.-HU05] Se consume el api de censo forestal
* [N3.1.9.-HU04] FRONT se consume servicio censo forestal para recursos maderables forestales
* [3.1.9 HU02] Ver plan Manejo.
* [3.1.9 HU01] Se termina filtrar PlanesManejo por dni, ruc e id plan manejo.
* [3.1.9 HU01] Se avanza consumir servicio listar plan manejo.
* [N3.1.9  HU09] consumo de servicio listar
* [N3.1.9  HU09] se agrega lista en modal
* [N3.1.9  HU03] consumo de servicios, listar, editar, eliminar
* Toast estándar en HU07 y HU08 3.1.9
* [DEMA] Se obtiene id plan manejo como parámetro de url, se cambia tipo de carga de tabs a lazyload.
* [DEMA] Se crea nuevo plan manejo en bandeja DEMA(registrar plan manejo).
* [N3.1.9  HU10] consumo de servicios, listar, editar, eliminar
* cambios de ultimo momento.
* [N3.1.9  HU06] Se realizan cambos en front en de acuerdo a flujo DEMA
* [3.1.9 HU08] Se termina lógica modal actividades AAE, Se termina editar, agregar, guardar, eliminar Actividades aprovechamiento, se termina integración con el backend.
* [3.1.9 HU08] Se crea modal actividades AAE
* [3.1.9 HU08] Se avanza listar actividades AAE, se cambia tabla por tabla ng prime.
* [N3.1.9.-HU06] Se concluye el listado de labores silviculturales, pero al mometno el backend no trae el id de la lista.
* [3.1.9 - HU03] consumo de servicio listar
* 3.1.9 Botones siguiente y regresar
* [3.1.9 - HU10] consumo de servicio para crear tipo de actividad
* 3.1.9-HU01 Unificar bandeja con Generación DEMA// feat 3.1.9-HU10 Modal de Tipo Actividad
* [3.1.9 - HU10] consumo de servicio para listado de actividades por primera vez
* [3.1.9 - HU10] se agrego boton para modal crear tipo de actividad
* [3.1.9 - HU07] [front]  Se implementa lógica de guardar, editar, eliminar  DEMA-MPUMF. Se termina integración con el Backend.
* [3.1.9 - HU07] Se agregan campos a modelos SistemaManejoForestal y SistemaManejoForestalDetalle
* [GLOBAL] Se agrega función utilitaria para mostrar un toast con errores de validación de un objeto, concatenado por comas(,)
* [3.1.9 -  HU07] Se crea función utilitaria para remover tags de texto en componente editor
* [3.1.9 -  HU07] Se separa lógica y crea modal Actividades MPUMF
* 3.1.9. HU01 Bandeja de Entrada de la Declaración de Manejo
* [N3.1.9.-HU06] Se termina modal de guardado y modificacion
*  [N3.1.9 - HU09] creacion de modal para registro
*  [N3.1.9 - HU03] creacion de modal para registro
*  [N3.1.9 - HU10] creacion de modal para registro
* [N3.1.9.-HU06] Se realiza listado y modal para guardar labor silvicultural
* [GLOBAL] Se agrega función a ToastService, para validar campos obligatorios de cualquier objeto.
* [3.1.9 -  HU07]  Se avanza lógica MPUMF, obtener  por plan manejo y código proceso
* [3.1.1 - HU01] Se Avanza lógica de guardado Elaboración Contrato PGMF, y archivo Constancia Represetante legal.
* Se crea ToastService Global,  wrapper de messageService(NG PRIME),
* N3.1.2 modo edicion ordenamiento parcela proteccion
* [N3.1.1 - HU8] se agregaron modales en tablas correspondientes
* N3.1.2 Hu6 - actualizar ordenamiento parcela proteccion
* N3.1.2 estandar boton
* [3.1.1 HU09] Se termina cargar archivo Evaluación Ambiental, Se vuelve a subir cambios [Subir Archivo PGMF] que descartaron en un merge.

### Fiat

* [N3.1.3.-HU05 avance integracion, listado de recurso forestal]

### Fix

* add scale in map
* [N3.1.3.-HU05 actualización consulta censo]
* [N3.1.3.-HU05 actualización consulta censo]
* [N3.1.3.-HU05 actualización flujo]
* [N3.1.3.-HU03 updates]
* cambios en zonificacion
* años en cronograma
* avance de carga de documentos en anexo
* [N3.1.3.-HU07 correccion al actualizar]
* validador de correo
* [N3.1.3.-HU05 avance crud]
* [3.1.3] general
* [3.1.3 - HU08] vigencia en cronograma
* HU09 Avance de listado de files
* [merge N3.1.3.-HU05 se agrega fecha de realizacion del censo a la cabececera]
* [N3.2.1 hu01] se levantan observaciones
* se configura para la carga de shp
* se configura para la carga de shp
* se añade mapa y export en pmfi anexos
* [3.1.3 HU2] campo objetivo requerido
* fecha de elaboracion PMFI
* impl registrar fauna silvestre en ccn
* impl registrar fauna silvestre en ccn
* vigencia
* se limpia los layers al listar capas
* max para dni
* cambios en listar anexo
* formato numero
* [3.1.3] numeracion
* cambios en listar anexo
* [3.1.3 - HU02] coneccion de servicio actualizar
* [3.1.3 - HU02]
* [3.1.3 - HU08]
* [N3.1.3.-HU04 Integración ordenamiento interno, editar y guardar registro]
* [N3.1.3.-HU04 Integración ordenamiento interno]
* [N3.1.3.-HU04 Integración ordenamiento interno]
* [N3.1.3.-HU04 Integración ordenamiento interno]
* [N3.1.3.-HU04 avance crud ordenamiento Interno]
* [3.1.3 - HU02] cambio en formato de fecha
* se cambia para añadir nueva zona
* nuevos cambios en registrar zona archivo
* [N3.1.3.- HU07 se agrega validación al registrar impacto ambiental
* [3.1.3 general] observaciones
* cambios para eliminar y registrar zona
* [N3.13 HU06] se crean validaciones para el modal y el ingreso de datos.
* [3.1.9] registros duplicados
* [N3.1.3.- HU01 se agrega id PLAN  a DEMA y PMFI]
* [N3.1.3.- HU01 Se agrega dni y ruc comunidad al listado de PMFI]
* [N3.1.9.- HU01 Se agrega dni y ruc comunidad al listado de demas]
* [3.1.3 HU08] se arreglo listado
* [3.1.3 HU02] se agregan apartados
* cambios en eliminar zona archivo
* [N3.1.9.- se agrega paginacion a bandeja de entrada]
* [N3.1.3 HU06] Cambios acordados con Harry
* cambios en zonificacon zona
* se implementa zona, carga de archivos y eliminar de la misma
* [N3.1.9.-HU02 Información general, se corrige carga de resumen recursos maderables y no maderables]
* [3.1.3 HU08]  listado cronograma
* [N3.1.9.-HU05 se corrige texto en recursos forestales no maderables]
* [N3.1.3.-HU10 se adiciona logica para carga de archivos]
* [N3.1.9.-HU10 QA - iteracion 1 - log 53]
* [N3.1.9.-HU10 QA - iteracion 1 - log 53]
* implementacion registrar zona
* [General] se quita el debugger
* [3.1.3 HU07]  separacion de componentes, sonsumo de servicios item 7 y 8
* [N3.1.9.-HU10 QA - iteracion 1 - log 83,84]
* [N3.1.9.-HU06 QA  Iteración 1 :  Se levanta log 53]
* [N3.1.9.-HU07 QA  Iteración 1 :  Se levanta log 58]
* [N3.1.9.-HU08-HU09 QA  Iteración 1 :  67, 76]
* [3.1.3 HU08 ] avance
* cambios para zonificacion
* [3.1.9]  se agregan validaciones
* [3.1.3] Correccion de tabs epica actualizada 19-09, quitado de objetivos informacion general
* [3.1.9] HU02 informacion general montos numericos 12,2
* [3.1.9 - HU03]  caraga de archivo
* [3.1.9 - HU03]  extencion de archivo
* [3.1.9 - HU11, HU12]  extencion de archivo
* [3.1.9 - HU04, HU05] correcion de listado y listado resumen
* impl fauns silvestres
* [N3.1.9.-HU02] validacion de dni fecha inicio fecha fin
* 3.1.5 mensaje
* 3.1.5 mensaje
* [N3.1.9.-HU12] se consume servicio de registros de estados de plan manejo
* se implementa export mapa
* se añade metodo guardarcapas
* [3.1.9 HU07-HU08] Se cambia método y url de delete a post para eliminar detalle SMF
* 3.1.5
* event para botones Anterior y Siguiente
* [N3.1.9 HU06] Se arregla eliminacion de labor silvicultural.
* cambio a idPlanManejo
* impl carga de zonas
* [3.1.9 - HU10] se cambio la estructura del json que se manda al servicio
* [N3.1.9.-HU04 HU05] se agrega campo nombreComun, nombreCientifico a la vista
* [3.1.9 HU10]  correccion en guardar detalle
* avance zonificacion files
* Se modifica logica para mostrar informacion de lista
* [N3.1.9 HU06] Se realiza correccion de campos al guardar labor silvicultural
* [3.1.9 HU03]  se adapto la tabla
* [N3.1.9 HU6] Se corrige funcionalidad al guardar operacion
* [N3.1.9.-HU04] FRONT, Se elimina fila TOTAL, se cambia nombre de columnas a Horizontales
* n2.3.1 obs excel qa
* N2.3.1 1 al 19
* N2.3.1obs
* se modifica id region para obtener uas
* [3.1.9 HU02] Se corrige seteo campo detalle Informacion General.
* impl delete files dema anexo1
* impl carga files dema anexo1
* [3.1.9 HU02] Correción nombre componente InformacionGeneral
* avance impl save files
* [N3.1.9 HU06] Se corrige cambio en front end Dema segun nuevo flujo
* se añade structura para fonts offline
* [N3.1.9  HU03] console.log
* [N3.1.9  HU10] correccion en listado
* se implementa carga de archivos en anexo
* avance crud files
* se corrige lista de tipo de bosque
* se implementa consumo de tipo de bosque
* se corrige carga shp inf basica area manejo
* se corrige empty value anda p-1
* se adaptaron los conflictos
* N3.1.9 HU03 - se implementa carga de shp
*  [N3.1.9] numeracion en titulos y sub titulos
* [3.1.2] obs de forma
*  [N3.1.9] cambio en nombre de tabs
*  [N3.1.2-HU04] correccion en modal
*  [N3.1.2-HU05] cambio en nombre de parametros
* [N3.1.2-HU04] Se corrige caso 18, se cambia asunto por Objetivo especifico
* [N3.1.2-HU10] se corrige punto 54
* [N3.1.1 - HU8] se aplicaron estandares
*  [N3.1.1 - HU4] adaptado a los estandares
* [N3.1.2-HU11] Se corrige caso 60, se modifica sp pa_ParticipacionCiudadanaPo_RegistrarList

### Merge

* [N3.1.9.-HU12] se consume servicio de registros de estados de plan manejo

### Refactor

* Se quita código no utilizado, se corrige nombre componente.
* Se quita logs, validación por directiva.
* Refactorizacion del codigo

### Style

* [general] maximo de caracteres
* [3.1.3] Labores silviculturales se estandariza el mensaje de bloqueo
* [3.1.3-HU02] deshabilitar la edicion del campo regente
* [3.1.3-HU06] validacion de tamaño de texto y agregado de placeholder
* [3.1.3] se corrige tildes y campos mal mencionados
* [General] se corrige tildes y palabras mal escritas
* obs 3.1.9
* obs 3.1.9
* [3.1.9 - HU02] se cambia el nombre del campo comunidad
* obs QA 2321
* Corrección front HU01-3.1.9
* corección FRONT a estándares HU07 y HU08
* 3.1.9.
* Se quita todos los "DEBUGGER", incomoda en el desarrollo.

### Upd

* solución temporal


<a name="0.0.2"></a>
## [0.0.2](https://dev.azure.com/proyecto-mcsniffs/mcsniffs-web/_git/mcsniffs-web/compare/0.0.1...0.0.2)

> 2021-09-01

### Chore

* Se cambia configuración angular.json, se quita librerías de allowedCommonJsDependencies.

### Feact

* N3.1.1 hu1 conexion de servicios

### Feat

* [N3.1.1 HU1] consumo de servicios y validaciones
* [3.1.1 - HU02] Se termina integración backend, guardar y obtener Archivos(Contrato y Certificado Regente) PGMF.
* N3.1.2 Grilla
* N3.1.2 HU 01
* Se crea nuevo componente botón para cargar archivos(enlazado a [(ngModel)])
* Se crea componente para cargar archivos.
* [3.1.1 - HU06] Se Avanza integración backend, listar Censo Forestal Detalle.
* [3.1.1 - HU06] Se crea componente mapa, se muven funciones mapa al Servicio de mapa.
* [3.1.1 - HU06] Se agrega servicio para leer excel y convertir a json.
* [3.1.1 - HU10] Se agrega servicio para leer excel y convertir a json.
* Se agrega módulo shared, se agrega directivas para validar input solo números.decimales y alfanumérico.
* [SMF] Se avanza modal Categoria Usos.
* [3.1.1 - HU02] Se agrega campo bloques quinquenales
* [Información General] Avance Información General, Guardar obtener, Setear Regente(falta limpiar código).
* adjuntar archivo
* adjuntar anexo capacitacion , participacion ciudadna y organizacion manejo
* [3.1.7 -HU10] Se avanza integración backend eliminar/editar SistemaManejoForestalDetalle.
* [3.1.7 - HU10] Se agrega campos a modelo Usuario, se crea servicio para obtener(localStorage) los datos del usaurio logueado.
* [3.1.7 - HU10] Se avanza Sistema Manejo Forestal Obtener/Listar.
* Avance guardar/obtener Plan General(refactor), se crea nuevos modelos.
* se agrega boton de validacion y modal para hu07 2.3.2.3
* se agrega resultado detalle para evaluacion
* se implementa resultado evaluacion
* [S2_Eq2_N2.4_10 - HU09-2.4] Se creó PAS.
* [Restaurando Archivos]
* [2.3.2.3 - HU13] Se termina vista Notificar Otorgamiento Permiso Entidades Competentes Detalle.
* firmeza resolucion modal
* se crea el modal de plantilla para firmeza resolucion - solictud forestal
* [HU13] Avance: Notificar otorgamiento permiso entidades competentes.
* resultado evaluacion solicitud de ororgamiento permiso forestal
* [2.3.2.3 - HU06] Se agrega Bandeja de Evaluación Técnica y Generación de Resolución
* agregamos un modal para las pruebas de publicacion
* [2.3.1 -HU05] Avance Validar requisitos
* [2.3.1 -HU05] Avance Recepción documentos físicos
* agregamos icono de revision
* Se avanza información Básica aspecto hidrográfico(obtener hidrografía) consulta - autocomplete
* informacion general 1.8 regente
* se reutiliza el componente regente en el plan operativo pmfi
* Se agrega campo nombres a modelo regente, se muestra campo nombre y numero documento en regente form.
* Se cambio servicio obtener regente dejar de utilizar datos mock
* Integración backend - obtener y guardar Regente Forestal (Se obtiene regente mock).
*  de habilitan campos del plan general
* Campos editables Regente Forestal.
* Se agregó buscar regente, para cambiar/editar Regente Forestal.
* Se agrega CHANGELOG.md

### Fix

*  [N3.1.1 - HU4]  correccion en editar en tabla de la seccion 3.2.1
*  [N3.1.1 - HU12]  cambio en diseño de botones
* [N3.1.1 - HU11] cambio en diseño de botones
* [N3.1.2-HU12] se corrige caso 64
* [3.1.1 - HU02] Se corrige tipo de párametro al cargar archivoPGMF
* [N3.1.2-HU14] se corrige caso 64 de observaciones
* [N3.1.2-HU11] se corrige observacion caso 61
* Se comenta código que genera error Tab Información General.
* [N3.1.2-HU11] Caso 58 - Se coloca numeración en tabs
* [3.1.1 - HU06] Se corrige error en componente mapa,cuando lista de archivos es null.
* [N3.1.2-HU02] se quita boton guardar desde ficha regente forestal y se guarda desde info general
* [N3.1.2_hu06] se integró los metodos y modificaciones de html
* [N3.1.2-HU03] se agrega componente toast al guardar concesiones maderables
* Se corrige error en Regente Form, al borrar el número de documento y la selección de un nuevo regente.
* se deshabilita campo bloque quinquenal no contempla el backend
* toast corregido actividad silvicultural
* Evaluacion ambiental - se adiciona una validacion y se correge el listado
* Guardar/Listar contingencia
* Se corrige Merge
* salto de linea en texto
* nombre boton
* Agregando validación, para evitar error.
* se descomenta informacion general
* se corrige el menu concesion forestal
* se corrige fecha de part. ciudadana, se comenta monitoreo

### Refactor

* [3.1.1 - HU06] Se utiliza componente mapa, se quitan(mueven) funciones relacionadas al componente mapa, hacia el servicio de mapa.
* Se prueba nuevas credenciales.
* HU11 Mejora replica evaluacion ambiental 3.1.1  en 3.1.2, 3.1.7 y 3.1.8 terminado.
* HU11 Mejora replica evaluacion ambiental 3.1.1  en 3.1.2, 3.1.7 y 3.1.8 terminado.
* HU11 Mejora replica evaluacion ambiental 3.1.1  en 3.1.2, 3.1.7 y 3.1.8, revisión sp listar aprovechamiento.

### Style

* [3.1.1 - HU02] Se quita código no utilizado.
* Quitando code smells

### Upd

* actualización objetivos marca
* anexo adjunto guardar marca
* creación de model regente , servicie regente


<a name="0.0.1"></a>
## 0.0.1

> 2021-07-30

### Feat

* Tabla dinámica: Se agrega campo para medidas mitigación
* HU11 Se terminó integración backend tabla dinámica, mostrar y guardar respuestas.
* Integración backend mock
* Dejando de usar lodash para clonar arrays.
* [Tabla dinámica]: Se terminó lógica de seleccionar respuesta y mostar respuesta.
* Se terminó estilo tabla dinámica evaluación ambiental.
* se direcciona el servicio de guardado a oitro endpoint
* se redirecciona el servicio participacion comunal
* Avance estilo tabla dinámica.
* HU 13.  Avance programa inversión POCM.
* [integración] se terminó participación ciudadan maderable.
* Se agregó mensaje cambios locales.
* Se agregó mensajes al eliminar ,registrar, editar capacitación maderable.
* Seteando id usuarui registro.
* Se terminó eliminar capacitacion detalle maderable.
* modificacion eliminar
* Cambio tipo parametro listar capacitacion maderable.
* Se terminó operación listar, guardar, actualizar capacitación/actividades
* Se terminó modal editar, agregar capacitación/actividad.
* modifcacion modelo capacitacion maderable.
* Se agregó paths a model,service, shared.
* Avance [Integración] capacitacion maderable.
* Avance vista Cronograma Actividades.
* Hu-14 Primera versión.
* HU10 - Se terminó vista monitoreo.
* Se terminó vista tab informacion basica parcela corta.
* Avance vista formulario info basica parcela corta.
* Creación tab informacion basica parcela corta.
* N3.1.7-HU11 avance de vistas 10.1,10.1.1,10.1.2,10.2

### Fix

* se corrige el guardado y el editado de las tablas
* Limpiar data al recargar lista.
* Correción eliminar capacitacion vista .
* Correción color alerta al registrar,editar, eliminar.

### Refactor

* Comentando debugger.

### Test

* Se comentó líneas de posible error en servidor desarrollo.

### Upd

* ordenamiento forestal
* ajustes
* ajustes capacitación
* ajuste paginado
* ajustes
* ajuste capacitacion
* objetivos
* ajustes
* objetivos 317-318
* cambios
* base para epica 318- PO PMFI Cncc
* objetivos
* creación de tabs Plan Concesiones Maderables
* cambios
* ajuste proceso oferta
* ajustes
* ajustes
* Mejoras Solicitud de acceso
* cambios
* se obtiene datos de solicitud en form.
* solicitud acceso
* solicitud acceso
* ajustes solicitud acceso
* validacion de registro solicitud
* se concluye primera versión del registro de solicitud de acceso
* ajuste solicitud Acceso
* bandeja solicitud acceso
* ajustes solicitud acceso

